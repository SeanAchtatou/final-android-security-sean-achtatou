package udk.android.reader.pdf;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import com.unidocs.commonlib.util.b;
import com.unidocs.commonlib.util.c;
import com.unidocs.commonlib.util.i;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.ac;
import udk.android.reader.pdf.annotation.d;
import udk.android.reader.pdf.annotation.p;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.v;
import udk.android.reader.pdf.annotation.w;
import udk.android.reader.pdf.annotation.z;
import udk.android.reader.pdf.b.e;
import udk.android.reader.pdf.b.f;
import udk.android.reader.pdf.b.g;
import udk.android.reader.pdf.b.h;
import udk.android.reader.pdf.b.j;
import udk.android.reader.pdf.b.k;
import udk.android.reader.pdf.b.l;
import udk.android.reader.pdf.b.m;
import udk.android.reader.pdf.b.n;
import udk.android.reader.pdf.b.q;

public class PDF {
    private static PDF a;
    private List b = new ArrayList();
    private Object c = new Object();
    private Object d = new Object();
    private Object e = new Object();
    private String f;
    private String g;
    private String h;
    private String i;
    private int j;
    private float k;
    private int l;
    private int m;
    private boolean n;
    private boolean o;
    private int p;
    private boolean q;
    private boolean r;
    private boolean s;
    private long t;
    private int u;
    private ag v;
    private int w;
    private Paint x;
    private Paint y;

    static {
        System.loadLibrary("ezpdf");
    }

    private PDF() {
        if (a.g) {
            this.x = new Paint(1);
            this.x.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DARKEN));
            this.x.setColor(a.N);
            this.y = new Paint(1);
            this.y.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DARKEN));
            this.y.setColor(a.M);
        }
    }

    private void K() {
        free();
        if (b.a(this.h)) {
            c.a(new File(this.h));
        }
        if (b.a(this.i)) {
            File file = new File(this.i);
            if (file.exists()) {
                c.a(file);
            }
        }
        this.h = null;
        this.i = null;
    }

    private int L() {
        return "R2L".equalsIgnoreCase(lookupNameTypeViewerPreference("Direction")) ? 2 : 1;
    }

    private void M() {
        for (ar f2 : this.b) {
            f2.f();
        }
    }

    private int a(int i2, float f2, Bitmap bitmap, int i3, int i4, int i5, int i6) {
        synchronized (this.c) {
            if (!this.r) {
                return -1;
            }
            setRenderingState(1);
            int renderSlice2bitmap = renderSlice2bitmap(i2, (double) (100.0f * f2), bitmap, i3, i4, i5, i6, true, false);
            setRenderingState(0);
            return renderSlice2bitmap;
        }
    }

    private int a(int i2, float f2, ByteBuffer byteBuffer, int i3, int i4, int i5, int i6) {
        synchronized (this.c) {
            if (!this.r) {
                return -1;
            }
            setRenderingState(1);
            int renderSlice2buffer = renderSlice2buffer(i2, (double) (100.0f * f2), byteBuffer, i3, i4, i5, i6, true, false);
            setRenderingState(0);
            return renderSlice2buffer;
        }
    }

    private int a(Annotation annotation, int i2) {
        int annotFindAnnot = annotFindAnnot(i2);
        annotSetNM(annotFindAnnot, annotation.O());
        annotSetAuthor(annotFindAnnot, annotation.P());
        annotation.d(i2);
        annotation.b(annotGetRect(annotFindAnnot, false));
        annotation.a(annotIsNoZoom(annotFindAnnot));
        annotation.g(annotGetStringValue(annotFindAnnot, "CreationDate"));
        annotation.f(annotGetStringValue(annotFindAnnot, "M"));
        return annotFindAnnot;
    }

    private int a(y yVar) {
        String a2 = yVar.a();
        if (a.aN) {
            File[] listFiles = new File(yVar.a()).listFiles(new e(this));
            if (b.a((Object[]) listFiles)) {
                for (File a3 : listFiles) {
                    c.a(a3);
                }
            }
            this.h = String.valueOf(yVar.a()) + "/p_" + System.currentTimeMillis();
            new File(this.h).mkdirs();
            a2 = this.h;
        }
        this.i = yVar.h();
        return init(yVar.g(), a2, yVar.b(), yVar.c());
    }

    private int a(y yVar, InputStream inputStream, int i2, boolean z, String str, String str2, String str3, String str4) {
        synchronized (this.c) {
            a.f = false;
            K();
            int a2 = a(yVar);
            if (a2 != 1) {
                throw new Error("ezPDF Reader initialize failure : " + a2);
            }
            setCacheFileEncryptionParams(a.V);
            setPageImageCacheParams(a.c(), a.d(), a.e());
            setSplashImageCacheParams(a.f(), a.g(), a.h(), a.b());
            setStreamCacheParams(a.i(), a.j(), a.k());
            this.s = true;
            this.t = 0;
            int openStreamInitialize = openStreamInitialize(i2, z);
            if (openStreamInitialize <= 0) {
                udk.android.reader.b.c.a("## OPEN STREAM FAILURE - INITIALIZE");
                return openStreamInitialize;
            }
            try {
                byte[] bArr = new byte[10240];
                ByteBuffer allocateDirect = ByteBuffer.allocateDirect(10240);
                do {
                    int read = inputStream.read(bArr);
                    if (read < 0) {
                        i.a(inputStream);
                        int openStreamFinalize = openStreamFinalize(str, str2, str3, str4);
                        udk.android.reader.b.c.a("## OPEN STREAM RESULT : " + openStreamFinalize);
                        this.s = false;
                        return openStreamFinalize;
                    }
                    this.t += (long) read;
                    allocateDirect.clear();
                    allocateDirect.put(bArr, 0, read);
                    int openStreamPutData = openStreamPutData(allocateDirect, read);
                    if (openStreamPutData <= 0) {
                        udk.android.reader.b.c.a("## OPEN STREAM FAILURE - PUT DATA");
                        i.a(inputStream);
                        return openStreamPutData;
                    }
                } while (this.s);
                udk.android.reader.b.c.a("## OPEN STREAM CANCELED");
                return 0;
            } catch (Exception e2) {
                udk.android.reader.b.c.a((Throwable) e2);
                return 0;
            } finally {
                i.a(inputStream);
            }
        }
    }

    private int a(y yVar, String str, String str2, String str3, String str4, String str5) {
        int open;
        synchronized (this.c) {
            K();
            int a2 = a(yVar);
            if (a2 != 1) {
                throw new Error("ezPDF Reader initialize failure : " + a2);
            }
            setCacheFileEncryptionParams(a.V);
            setPageImageCacheParams(a.c(), a.d(), a.e());
            setSplashImageCacheParams(a.f(), a.g(), a.h(), a.b());
            setStreamCacheParams(a.i(), a.j(), a.k());
            open = open(str, str2, str3, str4, str5);
        }
        return open;
    }

    private List a(int i2, float f2, int i3) {
        int annotGetPathNum = annotGetPathNum(i3);
        ArrayList arrayList = new ArrayList();
        for (int i4 = 0; i4 < annotGetPathNum; i4++) {
            Path path = new Path();
            double[] annotGetPathPoints = annotGetPathPoints(i3, i4);
            if (!b.b(annotGetPathPoints)) {
                int[] a2 = a(i2, f2, annotGetPathPoints);
                int length = a2.length / 2;
                path.moveTo((float) a2[0], (float) a2[1]);
                for (int i5 = 1; i5 < length; i5++) {
                    float f3 = (float) a2[(i5 - 1) * 2];
                    float f4 = (float) a2[((i5 - 1) * 2) + 1];
                    path.quadTo(f3, f4, (((float) a2[i5 * 2]) + f3) / 2.0f, (((float) a2[(i5 * 2) + 1]) + f4) / 2.0f);
                }
                path.lineTo((float) a2[(length - 1) * 2], (float) a2[((length - 1) * 2) + 1]);
                arrayList.add(path);
            }
        }
        return arrayList;
    }

    private static List a(int i2, double[] dArr) {
        if (b.b(dArr)) {
            return null;
        }
        int length = dArr.length / 8;
        ArrayList arrayList = new ArrayList(length);
        for (int i3 = 0; i3 < length; i3++) {
            arrayList.add(new udk.android.reader.pdf.a.a(i2, new double[]{dArr[i3 * 8], dArr[(i3 * 8) + 1], dArr[(i3 * 8) + 2], dArr[(i3 * 8) + 3], dArr[(i3 * 8) + 4], dArr[(i3 * 8) + 5], dArr[(i3 * 8) + 6], dArr[(i3 * 8) + 7]}));
        }
        return arrayList;
    }

    public static PDF a() {
        if (a == null) {
            a = new PDF();
        }
        return a;
    }

    private void a(Canvas canvas, int i2, float f2) {
        List h2 = s.a().h(i2);
        if (b.a((Collection) h2)) {
            int size = h2.size();
            for (int i3 = 0; i3 < size; i3++) {
                try {
                    Annotation annotation = (Annotation) h2.get(i3);
                    if (a.g && (annotation instanceof e)) {
                        canvas.drawRect(annotation.b(f2), ((e) annotation).a().h() ? this.y : this.x);
                    }
                    if (!annotation.h() && annotation.U()) {
                        annotation.a(canvas, f2);
                    }
                } catch (Exception e2) {
                    udk.android.reader.b.c.a((Throwable) e2);
                }
            }
        }
    }

    private void a(z zVar) {
        int[] annotGetReplies = annotGetReplies(annotFindAnnot(zVar.K()), Integer.MAX_VALUE);
        if (b.a(annotGetReplies)) {
            for (int d2 : annotGetReplies) {
                z d3 = d(zVar.ag(), d2);
                zVar.a(d3);
                a(d3);
            }
        }
    }

    private void a(k kVar, Map map, q qVar) {
        String e2 = qVar.e();
        int lastIndexOf = e2.lastIndexOf(46);
        if (lastIndexOf < 0) {
            qVar.a(kVar);
            kVar.b(qVar);
            map.put(e2, qVar);
            return;
        }
        String substring = e2.substring(0, lastIndexOf);
        q qVar2 = (q) map.get(substring);
        if (qVar2 == null) {
            a(kVar, map, new g(substring));
            qVar2 = (q) map.get(substring);
        }
        qVar.a(qVar2);
        qVar2.b(qVar);
        map.put(e2, qVar);
    }

    private native int annotAdd(String str, double d2, double d3, double d4, double d5, int i2, int i3, int i4, double d6, String str2, String str3, boolean z);

    private native int annotAddNote(double d2, double d3, int i2, int i3, int i4, double d4, String str, String str2, String str3, boolean z);

    private native int annotAddReply(int i2, int i3, int i4, int i5, double d2, String str, String str2);

    private native int annotAddTextMarkup(String str, double d2, double d3, double d4, double d5, int i2, int i3, int i4, double d6, String str2, String str3, boolean z);

    private native int annotFindAnnot(int i2);

    private native String annotGetArrow(int i2, int i3);

    private native String annotGetAuthor(int i2);

    private native int annotGetBorderType(int i2);

    private native double annotGetBorderWidth(int i2);

    private native double[] annotGetColor(int i2);

    private native String annotGetContents(int i2);

    private native double annotGetFontSize(int i2);

    private native double[] annotGetInnerColor(int i2);

    private native String annotGetNM(int i2);

    private native String annotGetNameValue(int i2, String str);

    private native int annotGetPathNum(int i2);

    private native double[] annotGetPathPoints(int i2, int i3);

    private native int annotGetQuadding(int i2);

    private native double[] annotGetRect(int i2, boolean z);

    private native int annotGetRefNum(int i2);

    private native int[] annotGetReplies(int i2, int i3);

    private native String annotGetStringValue(int i2, String str);

    private native String annotGetSubject(int i2);

    private native double[] annotGetTextColor(int i2);

    private native double annotGetTransparency(int i2);

    private native String annotGetType(int i2);

    private native boolean annotIsNoZoom(int i2);

    private native boolean annotIsVisible(int i2);

    private native int annotLockAnnotsInPage(int i2);

    private native int annotRemove(int i2);

    private native int annotSetArrow(int i2, String str, String str2);

    private native int annotSetAuthor(int i2, String str);

    private native int annotSetBorderStyle(int i2, int i3, double d2, double[] dArr);

    private native int annotSetColor(int i2, double d2, double d3, double d4);

    private native int annotSetContents(int i2, String str);

    private native int annotSetEditable(int i2, boolean z);

    private native int annotSetFontSize(int i2, double d2);

    private native int annotSetInnerColor(int i2, double d2, double d3, double d4);

    private native int annotSetNM(int i2, String str);

    private native int annotSetNameValue(int i2, String str, String str2);

    private native int annotSetPathPoints(int i2, double[] dArr, boolean z);

    private native int annotSetRect(int i2, double[] dArr, boolean z);

    private native int annotSetSubject(int i2, String str);

    private native int annotSetTextColor(int i2, double d2, double d3, double d4);

    private native int annotSetTransparency(int i2, double d2);

    private native int annotUnlockAnnotsInPage();

    private int b(int i2, float f2, Bitmap bitmap, int i3, int i4, int i5, int i6) {
        synchronized (this.c) {
            if (!this.r) {
                return -1;
            }
            setRenderingState(1);
            int renderSlice2bitmap = renderSlice2bitmap(i2, (double) (100.0f * f2), bitmap, i3, i4, i5, i6, true, true);
            setRenderingState(0);
            return renderSlice2bitmap;
        }
    }

    private void b(Annotation annotation, int i2) {
        annotation.g(annotGetStringValue(i2, "CreationDate"));
        annotation.f(annotGetStringValue(i2, "M"));
    }

    private List c(int i2, int i3) {
        int annotGetPathNum = annotGetPathNum(i3);
        ArrayList arrayList = new ArrayList();
        for (int i4 = 0; i4 < annotGetPathNum; i4++) {
            List a2 = a(i2, annotGetPathPoints(i3, i4));
            if (b.a((Collection) a2)) {
                arrayList.addAll(a2);
            }
        }
        return arrayList;
    }

    private native int close();

    private z d(int i2, int i3) {
        int annotFindAnnot = annotFindAnnot(i3);
        z zVar = new z(i2);
        zVar.d(i3);
        zVar.e(annotGetAuthor(annotFindAnnot));
        zVar.a(annotGetContents(annotFindAnnot));
        return zVar;
    }

    private native int descendOutline(int i2);

    private native int dp2pg(int i2, double d2, int[] iArr, double[] dArr);

    private native int fieldBtnGetShape(int i2, int i3);

    private native int fieldBtnGetState(int i2, int i3);

    private native int fieldBtnSetState(int i2, int i3, int i4);

    private native int fieldChGetNumOpt(int i2);

    private native String fieldChGetOptString(int i2, int i3);

    private native String fieldChGetOptValue(int i2, int i3);

    private native int fieldFindAnnot(int i2, int i3);

    private native int fieldFindByAnnot(int i2);

    private native int fieldFlatten(int i2, boolean z);

    private native int fieldGetAnnotPage(int i2, int i3);

    private native int fieldGetAnnotRef(int i2, int i3);

    private native int fieldGetFlags(int i2);

    private native int fieldGetNumAnnots(int i2);

    private native int fieldGetNumFields();

    private native String fieldGetTitle(int i2);

    private native String fieldGetType(int i2);

    private native String fieldGetValue(int i2);

    private native int fieldSetValue(int i2, String str);

    private native int fieldTxGetMaxLen(int i2);

    private native int findCaretPos(int i2, double d2, int i3, int i4, int[] iArr);

    private native double[] findTextInPageIntoArray(int i2, String str, boolean z, boolean z2, int i3);

    private native void free();

    private native String getEncryptFilter();

    private native double[] getHighlightInRange(int i2, int i3, int i4, int i5, int i6);

    private native int getImageBlockBBox(int i2, int i3, double[] dArr);

    private native int getImageBlockCount(int i2);

    private native String getLinkActionScript(int i2);

    private native int getLinkActionScriptDestRef(int i2);

    private native int getLinkDataClose(int i2);

    private native boolean getLinkDataOpen(int i2);

    private native int getLinkDataRead(int i2, ByteBuffer byteBuffer, int i3);

    private native int getLinkDestPage(int i2);

    private native String getLinkDestURI(int i2);

    private native int getLinkRect(int i2, double[] dArr);

    private native int getLinkRefNo(int i2);

    private native int getLinkType(int i2);

    private native int getNumLinks();

    private native int getNumPages();

    private native int getOutlineCount();

    private native int getOutlineDestPage(int i2);

    private native String getOutlineDestURI(int i2);

    private native String getOutlineTitle(int i2);

    private native int getOutlineType(int i2);

    private native int getPageHeight(int i2, double d2);

    private native String getPageTextAsXML(int i2);

    private native int getPageWidth(int i2, double d2);

    private native int getRenderingState();

    private native int getTextColumnBBox(int i2, int i3, int i4, double[] dArr);

    private native int getTextColumnCount(int i2, int i3);

    private native int getTextColumnRotation(int i2, int i3, int i4);

    private native int getTextFlowCount(int i2);

    private native String getTextInColumnAsXML(int i2, int i3, int i4);

    private native String getTextInRange(int i2, int i3, int i4, int i5, int i6);

    private native boolean hasOutlineKids(int i2);

    private native int init(Context context, String str, String str2, String str3);

    private native boolean isEncrypted();

    private List k(int i2) {
        ArrayList arrayList = new ArrayList();
        int fieldChGetNumOpt = fieldChGetNumOpt(i2);
        for (int i3 = 0; i3 < fieldChGetNumOpt; i3++) {
            f fVar = new f();
            fVar.a(fieldChGetOptString(i2, i3));
            fVar.b(fieldChGetOptValue(i2, i3));
            arrayList.add(fVar);
        }
        return arrayList;
    }

    private void l(Annotation annotation) {
        int annotFindAnnot = annotFindAnnot(annotation.K());
        annotation.R();
        annotation.e(annotGetAuthor(annotFindAnnot));
        annotation.c(annotGetSubject(annotFindAnnot));
        annotation.g(annotGetStringValue(annotFindAnnot, "CreationDate"));
        annotation.f(annotGetStringValue(annotFindAnnot, "M"));
        if (annotation instanceof e) {
            annotation.a(((e) annotation).a().g());
        } else {
            annotation.a(annotGetContents(annotFindAnnot));
        }
    }

    private native int lockLinksInPage(int i2);

    private native String lookupDocInfo(String str);

    private native String lookupNameTypeViewerPreference(String str);

    private native boolean lookupRenderedPageSlice(int i2, double d2, int i3, int i4, int i5, int i6);

    private native int moveToRootOutline();

    private native int open(String str, String str2, String str3, String str4, String str5);

    private native int openStreamFinalize(String str, String str2, String str3, String str4);

    private native int openStreamInitialize(int i2, boolean z);

    private native int openStreamPutData(ByteBuffer byteBuffer, int i2);

    private native int pg2dp(int i2, double d2, double[] dArr, int[] iArr);

    private native int renderSlice2bitmap(int i2, double d2, Bitmap bitmap, int i3, int i4, int i5, int i6, boolean z, boolean z2);

    private native int renderSlice2buffer(int i2, double d2, ByteBuffer byteBuffer, int i3, int i4, int i5, int i6, boolean z, boolean z2);

    private native int saveAs(String str);

    private native int setCacheFileEncryptionParams(boolean z);

    private native int setPageImageCacheParams(int i2, int i3, int i4);

    private native int setRenderingState(int i2);

    private native int setSplashImageCacheParams(int i2, int i3, int i4, int i5);

    private native int setStreamCacheParams(int i2, int i3, int i4);

    private native int unlockLinksInPage();

    public final int A() {
        return this.l;
    }

    public final int B() {
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, int):int
      udk.android.reader.pdf.PDF.a(int, double[]):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.c, java.lang.String):java.util.List
      udk.android.reader.pdf.PDF.a(float, float):udk.android.reader.pdf.ai
      udk.android.reader.pdf.PDF.a(int, int):void
      udk.android.reader.pdf.PDF.a(int, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.c, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, udk.android.reader.pdf.annotation.z):boolean
      udk.android.reader.pdf.PDF.a(int, float):int */
    public final int C() {
        return a(this.j, 1.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.b(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.annotation.Annotation, int):void
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.lang.String
      udk.android.reader.pdf.PDF.b(int, int):void
      udk.android.reader.pdf.PDF.b(int, float):int */
    public final int D() {
        return b(this.j, 1.0f);
    }

    public final int E() {
        return this.j;
    }

    public final float F() {
        return this.k;
    }

    public final boolean G() {
        return this.n;
    }

    public final boolean H() {
        return this.o;
    }

    public final void I() {
        if (f(this.j - 1)) {
            j(this.j - 1);
        }
    }

    public final void J() {
        if (f(this.j + 1)) {
            j(this.j + 1);
        }
    }

    public final float a(float f2) {
        return f2 / ((float) C());
    }

    public final int a(int i2, float f2) {
        if (this.r) {
            return getPageWidth(i2, (double) (100.0f * f2));
        }
        return 0;
    }

    public final int a(int i2, int i3, int i4) {
        return getTextColumnRotation(i2, i3, i4);
    }

    public final Bitmap a(int i2, float f2, int i3, int i4, int i5, int i6) {
        int i7;
        Bitmap bitmap;
        if (!this.r) {
            return null;
        }
        try {
            if (a.f || a.g) {
                s a2 = s.a();
                if (!a2.f(i2)) {
                    a2.g(i2);
                }
            }
            if (a.X) {
                Bitmap createBitmap = Bitmap.createBitmap(i5, i6, Bitmap.Config.RGB_565);
                i7 = a(i2, f2, createBitmap, i3, i4, i5, i6);
                bitmap = createBitmap;
            } else {
                ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i5 * i6 * 2);
                int a3 = a(i2, f2, allocateDirect, i3, i4, i5, i6);
                if (a3 == 1) {
                    Bitmap createBitmap2 = Bitmap.createBitmap(i5, i6, Bitmap.Config.RGB_565);
                    createBitmap2.copyPixelsFromBuffer(allocateDirect);
                    Bitmap bitmap2 = createBitmap2;
                    i7 = a3;
                    bitmap = bitmap2;
                } else {
                    i7 = a3;
                    bitmap = null;
                }
            }
            if (i7 != 1 || bitmap == null) {
                return null;
            }
            if (!a.f && !a.g) {
                return bitmap;
            }
            Canvas canvas = new Canvas(bitmap);
            canvas.save();
            canvas.translate((float) (-i3), (float) (-i4));
            a(canvas, i2, f2);
            canvas.restore();
            return bitmap;
        } catch (OutOfMemoryError e2) {
            udk.android.reader.b.c.a(e2.getMessage(), e2);
            this.u++;
            M();
            System.gc();
            return null;
        }
    }

    /* JADX INFO: additional move instructions added (4) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:116:0x0335 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r4v12 */
    /* JADX WARN: Type inference failed for: r4v13, types: [udk.android.reader.pdf.annotation.Annotation] */
    /* JADX WARN: Type inference failed for: r0v9, types: [udk.android.reader.pdf.annotation.b] */
    /* JADX WARN: Type inference failed for: r0v11, types: [udk.android.reader.pdf.annotation.d] */
    /* JADX WARN: Type inference failed for: r0v13, types: [udk.android.reader.pdf.annotation.i] */
    /* JADX WARN: Type inference failed for: r0v15, types: [udk.android.reader.pdf.annotation.v] */
    /* JADX WARN: Type inference failed for: r0v17, types: [udk.android.reader.pdf.annotation.h] */
    /* JADX WARN: Type inference failed for: r0v19, types: [udk.android.reader.pdf.annotation.y] */
    /* JADX WARN: Type inference failed for: r4v76 */
    /* JADX WARN: Type inference failed for: r4v78 */
    /* JADX WARN: Type inference failed for: r4v80 */
    /* JADX WARN: Type inference failed for: r4v82 */
    /* JADX WARN: Type inference failed for: r0v71, types: [udk.android.reader.pdf.annotation.e] */
    /* JADX WARN: Type inference failed for: r0v72, types: [udk.android.reader.pdf.annotation.n] */
    /* JADX WARN: Type inference failed for: r0v73, types: [udk.android.reader.pdf.annotation.o] */
    /* JADX WARN: Type inference failed for: r0v74, types: [udk.android.reader.pdf.annotation.c] */
    /* JADX WARN: Type inference failed for: r0v75, types: [udk.android.reader.pdf.annotation.g] */
    /* JADX WARN: Type inference failed for: r4v87 */
    /* JADX WARN: Type inference failed for: r0v76, types: [udk.android.reader.pdf.annotation.a] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02f9 A[SYNTHETIC, Splitter:B:106:0x02f9] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x041d  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0034 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x017b  */
    public final java.util.List a(int r20) {
        /*
            r19 = this;
            r0 = r19
            java.lang.Object r0 = r0.e
            r10 = r0
            monitor-enter(r10)
            r4 = 0
            int r11 = r19.annotLockAnnotsInPage(r20)     // Catch:{ all -> 0x018c }
            if (r11 <= 0) goto L_0x0017
            java.util.ArrayList r12 = new java.util.ArrayList     // Catch:{ all -> 0x018c }
            r12.<init>()     // Catch:{ all -> 0x018c }
            r4 = 0
            r13 = r4
        L_0x0014:
            if (r13 < r11) goto L_0x0023
            r4 = r12
        L_0x0017:
            r19.annotUnlockAnnotsInPage()     // Catch:{ all -> 0x018c }
            boolean r5 = com.unidocs.commonlib.util.b.b(r4)     // Catch:{ all -> 0x018c }
            if (r5 == 0) goto L_0x0021
            r4 = 0
        L_0x0021:
            monitor-exit(r10)     // Catch:{ all -> 0x018c }
            return r4
        L_0x0023:
            r14 = 0
            r0 = r19
            r1 = r13
            java.lang.String r15 = r0.annotGetType(r1)     // Catch:{ Exception -> 0x0234 }
            r0 = r19
            r1 = r13
            boolean r4 = r0.annotIsVisible(r1)     // Catch:{ Exception -> 0x0234 }
            if (r4 != 0) goto L_0x0038
        L_0x0034:
            int r4 = r13 + 1
            r13 = r4
            goto L_0x0014
        L_0x0038:
            boolean r4 = udk.android.reader.b.a.f     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x0429
            java.lang.String r4 = "Highlight"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x018f
            r0 = r19
            r1 = r20
            r2 = r13
            java.util.List r4 = r0.c(r1, r2)     // Catch:{ Exception -> 0x0234 }
            udk.android.reader.pdf.annotation.t r5 = new udk.android.reader.pdf.annotation.t     // Catch:{ Exception -> 0x0234 }
            r6 = 0
            r0 = r19
            r1 = r13
            r2 = r6
            double[] r6 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r5
            r1 = r20
            r2 = r6
            r3 = r4
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0234 }
            r4 = r5
        L_0x0061:
            if (r4 == 0) goto L_0x02f9
            r0 = r19
            r1 = r13
            int r5 = r0.annotGetRefNum(r1)     // Catch:{ Exception -> 0x0421 }
            r4.d(r5)     // Catch:{ Exception -> 0x0421 }
            r5 = r4
        L_0x006e:
            if (r5 == 0) goto L_0x0034
            r0 = r19
            r1 = r13
            java.lang.String r4 = r0.annotGetNM(r1)     // Catch:{ Exception -> 0x0167 }
            r5.d(r4)     // Catch:{ Exception -> 0x0167 }
            r0 = r19
            r1 = r13
            boolean r4 = r0.annotIsNoZoom(r1)     // Catch:{ Exception -> 0x0167 }
            r5.a(r4)     // Catch:{ Exception -> 0x0167 }
            r0 = r19
            r1 = r13
            double r6 = r0.annotGetTransparency(r1)     // Catch:{ Exception -> 0x0167 }
            r0 = r19
            r1 = r13
            double[] r4 = r0.annotGetColor(r1)     // Catch:{ Exception -> 0x0167 }
            if (r4 == 0) goto L_0x0097
            r5.a(r6, r4)     // Catch:{ Exception -> 0x0167 }
        L_0x0097:
            boolean r8 = r5.s()     // Catch:{ Exception -> 0x0167 }
            if (r8 == 0) goto L_0x00d9
            r0 = r19
            r1 = r13
            double[] r8 = r0.annotGetInnerColor(r1)     // Catch:{ Exception -> 0x0167 }
            if (r8 == 0) goto L_0x03d5
            r9 = 1
            r5.c(r9)     // Catch:{ Exception -> 0x0167 }
            r14 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r6 = r6 * r14
            int r6 = (int) r6     // Catch:{ Exception -> 0x0167 }
            r7 = 0
            r14 = r8[r7]     // Catch:{ Exception -> 0x0167 }
            r16 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r14 = r14 * r16
            int r7 = (int) r14     // Catch:{ Exception -> 0x0167 }
            r9 = 1
            r14 = r8[r9]     // Catch:{ Exception -> 0x0167 }
            r16 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r14 = r14 * r16
            int r9 = (int) r14     // Catch:{ Exception -> 0x0167 }
            r14 = 2
            r14 = r8[r14]     // Catch:{ Exception -> 0x0167 }
            r16 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r14 = r14 * r16
            int r8 = (int) r14     // Catch:{ Exception -> 0x0167 }
            int r6 = udk.android.b.c.a(r6, r7, r9, r8)     // Catch:{ Exception -> 0x0167 }
            r5.f(r6)     // Catch:{ Exception -> 0x0167 }
        L_0x00d9:
            boolean r6 = r5.l()     // Catch:{ Exception -> 0x0167 }
            if (r6 == 0) goto L_0x00f4
            r0 = r19
            r1 = r13
            double r6 = r0.annotGetBorderWidth(r1)     // Catch:{ Exception -> 0x0167 }
            float r6 = (float) r6     // Catch:{ Exception -> 0x0167 }
            r5.a(r6)     // Catch:{ Exception -> 0x0167 }
            boolean r6 = r5 instanceof udk.android.reader.pdf.b.e     // Catch:{ Exception -> 0x0167 }
            if (r6 == 0) goto L_0x00f4
            if (r4 != 0) goto L_0x03e8
            r4 = 0
            r5.a(r4)     // Catch:{ Exception -> 0x0167 }
        L_0x00f4:
            boolean r4 = r5.t()     // Catch:{ Exception -> 0x0167 }
            if (r4 == 0) goto L_0x0104
            r0 = r19
            r1 = r13
            int r4 = r0.annotGetBorderType(r1)     // Catch:{ Exception -> 0x0167 }
            r5.e(r4)     // Catch:{ Exception -> 0x0167 }
        L_0x0104:
            boolean r4 = r5 instanceof udk.android.reader.pdf.annotation.v     // Catch:{ Exception -> 0x0167 }
            if (r4 == 0) goto L_0x03f6
            r0 = r5
            udk.android.reader.pdf.annotation.v r0 = (udk.android.reader.pdf.annotation.v) r0     // Catch:{ Exception -> 0x0167 }
            r4 = r0
            udk.android.reader.pdf.b.q r6 = r4.a()     // Catch:{ Exception -> 0x0167 }
            int r6 = r6.f()     // Catch:{ Exception -> 0x0167 }
            int r7 = r4.K()     // Catch:{ Exception -> 0x0167 }
            r0 = r19
            r1 = r6
            r2 = r7
            int r7 = r0.fieldFindAnnot(r1, r2)     // Catch:{ Exception -> 0x0167 }
            r0 = r19
            r1 = r6
            r2 = r7
            int r8 = r0.fieldBtnGetState(r1, r2)     // Catch:{ Exception -> 0x0167 }
            r4.b(r8)     // Catch:{ Exception -> 0x0167 }
            r0 = r19
            r1 = r6
            r2 = r7
            int r6 = r0.fieldBtnGetShape(r1, r2)     // Catch:{ Exception -> 0x0167 }
            r4.c(r6)     // Catch:{ Exception -> 0x0167 }
        L_0x0136:
            boolean r4 = r5 instanceof udk.android.reader.pdf.annotation.e     // Catch:{ Exception -> 0x0167 }
            if (r4 == 0) goto L_0x0154
            r0 = r19
            r1 = r13
            double r6 = r0.annotGetFontSize(r1)     // Catch:{ Exception -> 0x0167 }
            float r4 = (float) r6     // Catch:{ Exception -> 0x0167 }
            r6 = 0
            int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x014e
            r0 = r5
            udk.android.reader.pdf.annotation.e r0 = (udk.android.reader.pdf.annotation.e) r0     // Catch:{ Exception -> 0x0167 }
            r6 = r0
            r6.e(r4)     // Catch:{ Exception -> 0x0167 }
        L_0x014e:
            r0 = r19
            r1 = r5
            r0.l(r1)     // Catch:{ Exception -> 0x0167 }
        L_0x0154:
            boolean r4 = r5.h()     // Catch:{ Exception -> 0x0167 }
            if (r4 != 0) goto L_0x0162
            r4 = 1
            r0 = r19
            r1 = r13
            r2 = r4
            r0.annotSetEditable(r1, r2)     // Catch:{ Exception -> 0x0167 }
        L_0x0162:
            r12.add(r5)     // Catch:{ Exception -> 0x0167 }
            goto L_0x0034
        L_0x0167:
            r4 = move-exception
        L_0x0168:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x018c }
            java.lang.String r7 = "## ANNOTATION LOOKUP FAILURE "
            r6.<init>(r7)     // Catch:{ all -> 0x018c }
            java.lang.StringBuilder r6 = r6.append(r13)     // Catch:{ all -> 0x018c }
            java.lang.String r7 = " : "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x018c }
            if (r5 == 0) goto L_0x041d
            java.lang.String r5 = r5.k()     // Catch:{ all -> 0x018c }
        L_0x017f:
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ all -> 0x018c }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x018c }
            udk.android.reader.b.c.a(r5, r4)     // Catch:{ all -> 0x018c }
            goto L_0x0034
        L_0x018c:
            r4 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x018c }
            throw r4
        L_0x018f:
            java.lang.String r4 = "StrikeOut"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x01b6
            r0 = r19
            r1 = r20
            r2 = r13
            java.util.List r4 = r0.c(r1, r2)     // Catch:{ Exception -> 0x0234 }
            udk.android.reader.pdf.annotation.ad r5 = new udk.android.reader.pdf.annotation.ad     // Catch:{ Exception -> 0x0234 }
            r6 = 0
            r0 = r19
            r1 = r13
            r2 = r6
            double[] r6 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r5
            r1 = r20
            r2 = r6
            r3 = r4
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0234 }
            r4 = r5
            goto L_0x0061
        L_0x01b6:
            java.lang.String r4 = "Underline"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x01dd
            r0 = r19
            r1 = r20
            r2 = r13
            java.util.List r4 = r0.c(r1, r2)     // Catch:{ Exception -> 0x0234 }
            udk.android.reader.pdf.annotation.u r5 = new udk.android.reader.pdf.annotation.u     // Catch:{ Exception -> 0x0234 }
            r6 = 0
            r0 = r19
            r1 = r13
            r2 = r6
            double[] r6 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r5
            r1 = r20
            r2 = r6
            r3 = r4
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0234 }
            r4 = r5
            goto L_0x0061
        L_0x01dd:
            java.lang.String r4 = "Text"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x0205
            java.lang.String r4 = "Name"
            r0 = r19
            r1 = r13
            r2 = r4
            java.lang.String r4 = r0.annotGetNameValue(r1, r2)     // Catch:{ Exception -> 0x0234 }
            udk.android.reader.pdf.annotation.p r5 = new udk.android.reader.pdf.annotation.p     // Catch:{ Exception -> 0x0234 }
            r6 = 0
            r0 = r19
            r1 = r13
            r2 = r6
            double[] r6 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r5
            r1 = r20
            r2 = r6
            r3 = r4
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0234 }
            r4 = r5
            goto L_0x0061
        L_0x0205:
            java.lang.String r4 = "Ink"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x0238
            udk.android.reader.pdf.annotation.a r4 = new udk.android.reader.pdf.annotation.a     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r5 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r6 = 1
            r0 = r4
            r1 = r20
            r2 = r5
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0234 }
            float r5 = r4.g()     // Catch:{ Exception -> 0x0234 }
            r0 = r19
            r1 = r20
            r2 = r5
            r3 = r13
            java.util.List r5 = r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x0234 }
            r4.a(r5)     // Catch:{ Exception -> 0x0234 }
            goto L_0x0061
        L_0x0234:
            r4 = move-exception
            r5 = r14
            goto L_0x0168
        L_0x0238:
            java.lang.String r4 = "Line"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x026d
            udk.android.reader.pdf.annotation.w r4 = new udk.android.reader.pdf.annotation.w     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r6 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r7 = r0.annotGetPathPoints(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            java.lang.String r8 = r0.annotGetArrow(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r5 = 1
            r0 = r19
            r1 = r13
            r2 = r5
            java.lang.String r9 = r0.annotGetArrow(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r5 = r20
            r4.<init>(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0234 }
            goto L_0x0061
        L_0x026d:
            java.lang.String r4 = "Polygon"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x0289
            udk.android.reader.pdf.annotation.g r4 = new udk.android.reader.pdf.annotation.g     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r5 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r4
            r1 = r20
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0234 }
            goto L_0x0061
        L_0x0289:
            java.lang.String r4 = "PolyLine"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x02a5
            udk.android.reader.pdf.annotation.c r4 = new udk.android.reader.pdf.annotation.c     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r5 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r4
            r1 = r20
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0234 }
            goto L_0x0061
        L_0x02a5:
            java.lang.String r4 = "Square"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x02c1
            udk.android.reader.pdf.annotation.o r4 = new udk.android.reader.pdf.annotation.o     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r5 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r4
            r1 = r20
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0234 }
            goto L_0x0061
        L_0x02c1:
            java.lang.String r4 = "Circle"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x02dd
            udk.android.reader.pdf.annotation.n r4 = new udk.android.reader.pdf.annotation.n     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r5 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r4
            r1 = r20
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0234 }
            goto L_0x0061
        L_0x02dd:
            java.lang.String r4 = "FreeText"
            boolean r4 = r4.equals(r15)     // Catch:{ Exception -> 0x0234 }
            if (r4 == 0) goto L_0x0429
            udk.android.reader.pdf.annotation.e r4 = new udk.android.reader.pdf.annotation.e     // Catch:{ Exception -> 0x0234 }
            r5 = 0
            r0 = r19
            r1 = r13
            r2 = r5
            double[] r5 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0234 }
            r0 = r4
            r1 = r20
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0234 }
            goto L_0x0061
        L_0x02f9:
            boolean r5 = udk.android.reader.b.a.g     // Catch:{ Exception -> 0x0421 }
            if (r5 == 0) goto L_0x033a
            java.lang.String r5 = "Widget"
            boolean r5 = r5.equals(r15)     // Catch:{ Exception -> 0x0421 }
            if (r5 == 0) goto L_0x033a
            r0 = r19
            r1 = r13
            int r5 = r0.annotGetRefNum(r1)     // Catch:{ Exception -> 0x0421 }
            udk.android.reader.pdf.b.d r6 = udk.android.reader.pdf.b.d.a()     // Catch:{ Exception -> 0x0421 }
            r0 = r19
            r1 = r5
            int r7 = r0.fieldFindByAnnot(r1)     // Catch:{ Exception -> 0x0421 }
            udk.android.reader.pdf.b.q r6 = r6.a(r7)     // Catch:{ Exception -> 0x0421 }
            if (r6 == 0) goto L_0x033a
            boolean r7 = r6 instanceof udk.android.reader.pdf.b.n     // Catch:{ Exception -> 0x0421 }
            if (r7 == 0) goto L_0x033d
            udk.android.reader.pdf.annotation.y r7 = new udk.android.reader.pdf.annotation.y     // Catch:{ Exception -> 0x0421 }
            r8 = 0
            r0 = r19
            r1 = r13
            r2 = r8
            double[] r8 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0421 }
            r0 = r7
            r1 = r20
            r2 = r8
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0421 }
            r4 = r7
        L_0x0335:
            if (r4 == 0) goto L_0x033a
            r4.d(r5)     // Catch:{ Exception -> 0x0421 }
        L_0x033a:
            r5 = r4
            goto L_0x006e
        L_0x033d:
            boolean r7 = r6 instanceof udk.android.reader.pdf.b.b     // Catch:{ Exception -> 0x0421 }
            if (r7 == 0) goto L_0x0356
            udk.android.reader.pdf.annotation.h r7 = new udk.android.reader.pdf.annotation.h     // Catch:{ Exception -> 0x0421 }
            r8 = 0
            r0 = r19
            r1 = r13
            r2 = r8
            double[] r8 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0421 }
            r0 = r7
            r1 = r20
            r2 = r8
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0421 }
            r4 = r7
            goto L_0x0335
        L_0x0356:
            boolean r7 = r6 instanceof udk.android.reader.pdf.b.j     // Catch:{ Exception -> 0x0421 }
            if (r7 == 0) goto L_0x036f
            udk.android.reader.pdf.annotation.v r7 = new udk.android.reader.pdf.annotation.v     // Catch:{ Exception -> 0x0421 }
            r8 = 0
            r0 = r19
            r1 = r13
            r2 = r8
            double[] r8 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0421 }
            r0 = r7
            r1 = r20
            r2 = r8
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0421 }
            r4 = r7
            goto L_0x0335
        L_0x036f:
            boolean r7 = r6 instanceof udk.android.reader.pdf.b.l     // Catch:{ Exception -> 0x0421 }
            if (r7 == 0) goto L_0x0388
            udk.android.reader.pdf.annotation.i r7 = new udk.android.reader.pdf.annotation.i     // Catch:{ Exception -> 0x0421 }
            r8 = 0
            r0 = r19
            r1 = r13
            r2 = r8
            double[] r8 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0421 }
            r0 = r7
            r1 = r20
            r2 = r8
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0421 }
            r4 = r7
            goto L_0x0335
        L_0x0388:
            boolean r7 = r6 instanceof udk.android.reader.pdf.b.i     // Catch:{ Exception -> 0x0421 }
            if (r7 == 0) goto L_0x03a1
            udk.android.reader.pdf.annotation.d r7 = new udk.android.reader.pdf.annotation.d     // Catch:{ Exception -> 0x0421 }
            r8 = 0
            r0 = r19
            r1 = r13
            r2 = r8
            double[] r8 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0421 }
            r0 = r7
            r1 = r20
            r2 = r8
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0421 }
            r4 = r7
            goto L_0x0335
        L_0x03a1:
            boolean r7 = r6 instanceof udk.android.reader.pdf.b.h     // Catch:{ Exception -> 0x0421 }
            if (r7 == 0) goto L_0x03bb
            udk.android.reader.pdf.annotation.b r7 = new udk.android.reader.pdf.annotation.b     // Catch:{ Exception -> 0x0421 }
            r8 = 0
            r0 = r19
            r1 = r13
            r2 = r8
            double[] r8 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0421 }
            r0 = r7
            r1 = r20
            r2 = r8
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0421 }
            r4 = r7
            goto L_0x0335
        L_0x03bb:
            boolean r7 = r6 instanceof udk.android.reader.pdf.b.m     // Catch:{ Exception -> 0x0421 }
            if (r7 == 0) goto L_0x0335
            udk.android.reader.pdf.annotation.r r7 = new udk.android.reader.pdf.annotation.r     // Catch:{ Exception -> 0x0421 }
            r8 = 0
            r0 = r19
            r1 = r13
            r2 = r8
            double[] r8 = r0.annotGetRect(r1, r2)     // Catch:{ Exception -> 0x0421 }
            r0 = r7
            r1 = r20
            r2 = r8
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Exception -> 0x0421 }
            r4 = r7
            goto L_0x0335
        L_0x03d5:
            r6 = 0
            r5.c(r6)     // Catch:{ Exception -> 0x0167 }
            int r6 = udk.android.reader.b.a.A     // Catch:{ Exception -> 0x0167 }
            int r7 = r5.A()     // Catch:{ Exception -> 0x0167 }
            int r6 = udk.android.b.c.a(r6, r7)     // Catch:{ Exception -> 0x0167 }
            r5.f(r6)     // Catch:{ Exception -> 0x0167 }
            goto L_0x00d9
        L_0x03e8:
            float r4 = r5.S()     // Catch:{ Exception -> 0x0167 }
            r6 = 0
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x00f4
            r5.ac()     // Catch:{ Exception -> 0x0167 }
            goto L_0x00f4
        L_0x03f6:
            boolean r4 = r5 instanceof udk.android.reader.pdf.annotation.d     // Catch:{ Exception -> 0x0167 }
            if (r4 == 0) goto L_0x0136
            r0 = r5
            udk.android.reader.pdf.annotation.d r0 = (udk.android.reader.pdf.annotation.d) r0     // Catch:{ Exception -> 0x0167 }
            r6 = r0
            r0 = r19
            r1 = r13
            double[] r4 = r0.annotGetTextColor(r1)     // Catch:{ Exception -> 0x0167 }
            if (r4 == 0) goto L_0x0416
        L_0x0407:
            r6.a(r4)     // Catch:{ Exception -> 0x0167 }
            r0 = r19
            r1 = r13
            int r4 = r0.annotGetQuadding(r1)     // Catch:{ Exception -> 0x0167 }
            r6.c(r4)     // Catch:{ Exception -> 0x0167 }
            goto L_0x0136
        L_0x0416:
            r4 = 3
            double[] r4 = new double[r4]     // Catch:{ Exception -> 0x0167 }
            r4 = {0, 0, 0} // fill-array
            goto L_0x0407
        L_0x041d:
            java.lang.String r5 = "UNKOWN"
            goto L_0x017f
        L_0x0421:
            r5 = move-exception
            r18 = r5
            r5 = r4
            r4 = r18
            goto L_0x0168
        L_0x0429:
            r4 = r14
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.pdf.PDF.a(int):java.util.List");
    }

    /* access modifiers changed from: package-private */
    public final List a(int i2, String str, boolean z, boolean z2, int i3) {
        return a(i2, findTextInPageIntoArray(i2, str, z, z2, i3));
    }

    public final List a(ai aiVar, ai aiVar2) {
        if (aiVar == null || aiVar2 == null) {
            return null;
        }
        return a(this.j, getHighlightInRange(this.j, aiVar.a, aiVar.b, aiVar2.a, aiVar2.b));
    }

    public final List a(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        ArrayList arrayList = new ArrayList();
        int[] annotGetReplies = annotGetReplies(annotFindAnnot(annotation.K()), Integer.MAX_VALUE);
        if (b.a(annotGetReplies)) {
            for (int d2 : annotGetReplies) {
                z d3 = d(annotation.ag(), d2);
                a(d3);
                arrayList.add(d3);
            }
        }
        annotUnlockAnnotsInPage();
        return arrayList;
    }

    public final List a(d dVar) {
        ArrayList arrayList = new ArrayList();
        q a2 = dVar.a();
        int f2 = a2.f();
        int fieldGetNumAnnots = fieldGetNumAnnots(f2);
        s a3 = s.a();
        a2.a(dVar.M());
        fieldSetValue(f2, a2.g());
        for (int i2 = 0; i2 < fieldGetNumAnnots; i2++) {
            d dVar2 = (d) a3.a(fieldGetAnnotPage(f2, i2), fieldGetAnnotRef(f2, i2));
            if (dVar2 != null) {
                dVar2.a(a2.g());
                arrayList.add(dVar2);
            }
        }
        return arrayList;
    }

    public final List a(v vVar) {
        ArrayList arrayList = new ArrayList();
        q a2 = vVar.a();
        int f2 = a2.f();
        int fieldFindAnnot = fieldFindAnnot(f2, vVar.K());
        boolean z = fieldBtnGetState(f2, fieldFindAnnot) > 0;
        if ((a2 instanceof udk.android.reader.pdf.b.b) && z) {
            return arrayList;
        }
        fieldBtnSetState(f2, fieldFindAnnot, z ? 0 : 1);
        a2.a(fieldGetValue(f2));
        int fieldGetNumAnnots = fieldGetNumAnnots(a2.f());
        s a3 = s.a();
        for (int i2 = 0; i2 < fieldGetNumAnnots; i2++) {
            v vVar2 = (v) a3.a(fieldGetAnnotPage(f2, i2), fieldGetAnnotRef(f2, i2));
            if (vVar2 != null) {
                vVar2.b(fieldBtnGetState(a2.f(), i2));
                arrayList.add(vVar2);
            }
        }
        return arrayList;
    }

    public final List a(udk.android.reader.pdf.b.c cVar, String str) {
        ArrayList arrayList = new ArrayList();
        int f2 = cVar.f();
        int fieldGetNumAnnots = fieldGetNumAnnots(f2);
        s a2 = s.a();
        cVar.a(str);
        fieldSetValue(f2, cVar.g());
        for (int i2 = 0; i2 < fieldGetNumAnnots; i2++) {
            Annotation a3 = a2.a(fieldGetAnnotPage(f2, i2), fieldGetAnnotRef(f2, i2));
            if (a3 != null) {
                arrayList.add(a3);
            }
        }
        return arrayList;
    }

    public final ai a(float f2, float f3) {
        int[] iArr = new int[2];
        if (findCaretPos(this.j, (double) (this.k * 100.0f), (int) f2, (int) f3, iArr) != 1) {
            return null;
        }
        return new ai(iArr[0], iArr[1]);
    }

    public final void a(int i2, int i3) {
        f(i2, d(i2, (float) i3));
        this.n = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, boolean):void
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, int):int
      udk.android.reader.pdf.PDF.a(int, double[]):java.util.List
      udk.android.reader.pdf.PDF.a(int, float):int
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.c, java.lang.String):java.util.List
      udk.android.reader.pdf.PDF.a(float, float):udk.android.reader.pdf.ai
      udk.android.reader.pdf.PDF.a(int, int):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.c, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, udk.android.reader.pdf.annotation.z):boolean
      udk.android.reader.pdf.PDF.a(int, boolean):void */
    public final void a(int i2, boolean z) {
        if (i2 == 1 || i2 == 2) {
            this.w = i2;
            if (z) {
                this.v.b("bookreaddirection", Integer.valueOf(i2));
            }
        } else if (i2 == 9) {
            this.w = L();
        } else if (i2 == 0) {
            a(a.aM, false);
            if (z) {
                this.v.b("bookreaddirection", Integer.valueOf(i2));
            }
        }
    }

    public final void a(ak akVar) {
        moveToRootOutline();
        List<Integer> c2 = akVar.c();
        if (b.a((Collection) c2)) {
            for (Integer intValue : c2) {
                descendOutline(intValue.intValue());
            }
        }
        int outlineCount = getOutlineCount();
        for (int i2 = 0; i2 < outlineCount; i2++) {
            ak akVar2 = new ak(akVar);
            akVar2.a(hasOutlineKids(i2));
            akVar2.a(getOutlineTitle(i2));
            akVar2.a(getOutlineType(i2));
            if (akVar2.j() == 1) {
                akVar2.b(getOutlineDestPage(i2));
            } else if (akVar2.j() == 4) {
                akVar2.b(getOutlineDestURI(i2));
            }
            akVar.a(akVar2);
        }
    }

    public final void a(udk.android.reader.pdf.annotation.e eVar) {
        annotLockAnnotsInPage(eVar.ag());
        int annotFindAnnot = annotFindAnnot(eVar.K());
        if (annotFindAnnot >= 0) {
            annotSetFontSize(annotFindAnnot, (double) eVar.f());
            b(eVar, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final void a(p pVar) {
        annotLockAnnotsInPage(pVar.ag());
        int annotFindAnnot = annotFindAnnot(pVar.K());
        if (annotFindAnnot >= 0) {
            annotSetNameValue(annotFindAnnot, "Name", pVar.a());
            b(pVar, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final void a(w wVar) {
        annotLockAnnotsInPage(wVar.ag());
        int annotFindAnnot = annotFindAnnot(wVar.K());
        if (annotFindAnnot >= 0) {
            annotSetArrow(annotFindAnnot, wVar.c(), wVar.d());
            b(wVar, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final void a(ar arVar) {
        if (!this.b.contains(arVar)) {
            this.b.add(arVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00f0, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f1, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0104, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0105, code lost:
        r3 = r2;
        r2 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f0 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x004b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(udk.android.reader.pdf.c r9, boolean r10) {
        /*
            r8 = this;
            r5 = 0
            boolean r0 = r8.r
            if (r0 != 0) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            int r0 = r9.d()
            r8.lockLinksInPage(r0)
            int r0 = r9.e()
            int r1 = r8.getLinkDestPage(r0)
            r9.b(r1)
            java.lang.String r1 = r8.getLinkDestURI(r0)
            r9.a(r1)
            r1 = 12
            int r2 = r9.f()
            if (r1 != r2) goto L_0x0035
            java.lang.String r1 = r8.getLinkActionScript(r0)
            r9.c(r1)
            int r0 = r8.getLinkActionScriptDestRef(r0)
            r9.c(r0)
        L_0x0035:
            if (r10 == 0) goto L_0x00cc
            int r0 = r9.e()
            java.io.File r1 = new java.io.File
            java.lang.String r2 = r8.i
            r1.<init>(r2)
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x004b
            r1.mkdirs()
        L_0x004b:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r1 = r1.getAbsolutePath()     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r1 = "/"
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r3 = r8.g     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = com.unidocs.commonlib.a.a.a(r3, r4)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r3 = "_"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            int r3 = r9.d()     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r3 = "_"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            int r3 = r9.e()     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0100, all -> 0x00f0 }
            boolean r1 = r8.getLinkDataOpen(r0)     // Catch:{ Exception -> 0x0104, all -> 0x00f0 }
            if (r1 == 0) goto L_0x0108
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0104, all -> 0x00f0 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0104, all -> 0x00f0 }
            r3 = 10240(0x2800, float:1.4349E-41)
            java.nio.ByteBuffer r3 = java.nio.ByteBuffer.allocateDirect(r3)     // Catch:{ Exception -> 0x00e0, all -> 0x00f9 }
            r4 = 10240(0x2800, float:1.4349E-41)
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x00e0, all -> 0x00f9 }
        L_0x00a4:
            r5 = 10240(0x2800, float:1.4349E-41)
            int r5 = r8.getLinkDataRead(r0, r3, r5)     // Catch:{ Exception -> 0x00e0, all -> 0x00f9 }
            if (r5 > 0) goto L_0x00d1
        L_0x00ac:
            com.unidocs.commonlib.util.i.a(r1)
            r8.getLinkDataClose(r0)
            r0 = r2
        L_0x00b3:
            if (r0 == 0) goto L_0x00cc
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x00cc
            long r1 = r0.length()
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x00cc
            java.lang.String r0 = r0.getAbsolutePath()
            r9.b(r0)
        L_0x00cc:
            r8.unlockLinksInPage()
            goto L_0x0005
        L_0x00d1:
            r3.clear()     // Catch:{ Exception -> 0x00e0, all -> 0x00f9 }
            r6 = 0
            r3.get(r4, r6, r5)     // Catch:{ Exception -> 0x00e0, all -> 0x00f9 }
            r6 = 0
            r1.write(r4, r6, r5)     // Catch:{ Exception -> 0x00e0, all -> 0x00f9 }
            r1.flush()     // Catch:{ Exception -> 0x00e0, all -> 0x00f9 }
            goto L_0x00a4
        L_0x00e0:
            r3 = move-exception
            r7 = r3
            r3 = r2
            r2 = r1
            r1 = r7
        L_0x00e5:
            udk.android.reader.b.c.a(r1)     // Catch:{ all -> 0x00fe }
            com.unidocs.commonlib.util.i.a(r2)
            r8.getLinkDataClose(r0)
            r0 = r3
            goto L_0x00b3
        L_0x00f0:
            r1 = move-exception
            r2 = r5
        L_0x00f2:
            com.unidocs.commonlib.util.i.a(r2)
            r8.getLinkDataClose(r0)
            throw r1
        L_0x00f9:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x00f2
        L_0x00fe:
            r1 = move-exception
            goto L_0x00f2
        L_0x0100:
            r1 = move-exception
            r2 = r5
            r3 = r5
            goto L_0x00e5
        L_0x0104:
            r1 = move-exception
            r3 = r2
            r2 = r5
            goto L_0x00e5
        L_0x0108:
            r1 = r5
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.c, boolean):void");
    }

    /* access modifiers changed from: package-private */
    public final void a(x xVar) {
        xVar.a(getTextInColumnAsXML(xVar.e(), xVar.b(), xVar.c()));
    }

    public final boolean a(String str) {
        boolean z;
        synchronized (this.c) {
            z = saveAs(str) != 0;
        }
        return z;
    }

    public final boolean a(Annotation annotation, z zVar) {
        annotLockAnnotsInPage(annotation.ag());
        int annotAddReply = annotAddReply(annotFindAnnot(annotation.K()), -1, -1, -1, 0.0d, zVar.M(), zVar.N());
        a(zVar, annotAddReply);
        annotUnlockAnnotsInPage();
        return annotAddReply != 0;
    }

    public final boolean a(udk.android.reader.pdf.annotation.a aVar) {
        annotLockAnnotsInPage(aVar.ag());
        float g2 = aVar.g();
        double[] a2 = a(aVar.ag(), g2, aVar.b(g2));
        int annotAdd = annotAdd("Ink", a2[0], a2[1], a2[2], a2[3], aVar.x(), aVar.y(), aVar.z(), aVar.B(), aVar.M(), aVar.N(), true);
        int a3 = a(aVar, annotAdd);
        annotSetBorderStyle(a3, aVar.T(), (double) aVar.S(), null);
        List f2 = aVar.f();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= f2.size()) {
                break;
            }
            List list = (List) f2.get(i3);
            int[] iArr = new int[list.size()];
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 >= list.size()) {
                    break;
                }
                iArr[i5] = ((Integer) list.get(i5)).intValue();
                i4 = i5 + 1;
            }
            annotSetPathPoints(a3, a(aVar.ag(), g2, iArr), i3 == 0);
            i2 = i3 + 1;
        }
        aVar.a(a(aVar.ag(), aVar.g(), a3));
        annotUnlockAnnotsInPage();
        return annotAdd != 0;
    }

    public final boolean a(ac acVar, ai aiVar, ai aiVar2) {
        annotLockAnnotsInPage(acVar.ag());
        int annotAddTextMarkup = annotAddTextMarkup(acVar.k(), (double) aiVar.a, (double) aiVar.b, (double) aiVar2.a, (double) aiVar2.b, acVar.x(), acVar.y(), acVar.z(), acVar.B(), acVar.M(), acVar.N(), !acVar.h());
        acVar.a(c(acVar.ag(), a(acVar, annotAddTextMarkup)));
        annotUnlockAnnotsInPage();
        return annotAddTextMarkup != 0;
    }

    public final boolean a(p pVar, float f2, int i2, int i3) {
        annotLockAnnotsInPage(pVar.ag());
        double[] a2 = a(pVar.ag(), f2, new int[]{i2, i3});
        int annotAddNote = annotAddNote(a2[0], a2[1], pVar.x(), pVar.y(), pVar.z(), pVar.B(), pVar.a(), pVar.M(), pVar.N(), !pVar.h());
        a(pVar, annotAddNote);
        annotUnlockAnnotsInPage();
        return annotAddNote != 0;
    }

    public final boolean a(udk.android.reader.pdf.annotation.q qVar) {
        annotLockAnnotsInPage(qVar.ag());
        double[] a2 = a(qVar.ag(), 100.0f, qVar.b(100.0f));
        int annotAdd = annotAdd(qVar.k(), a2[0], a2[1], a2[2], a2[3], qVar.x(), qVar.y(), qVar.z(), qVar.B(), qVar.M(), qVar.N(), true);
        int a3 = a(qVar, annotAdd);
        annotSetBorderStyle(a3, qVar.T(), (double) qVar.S(), null);
        if (qVar.aa()) {
            annotSetInnerColor(a3, qVar.F(), qVar.G(), qVar.H());
        } else {
            annotSetInnerColor(a3, -1.0d, -1.0d, -1.0d);
        }
        if (qVar instanceof udk.android.reader.pdf.annotation.e) {
            annotSetFontSize(a3, (double) ((udk.android.reader.pdf.annotation.e) qVar).f());
        } else if (qVar instanceof w) {
            w wVar = (w) qVar;
            annotSetPathPoints(a3, wVar.a(), true);
            annotSetArrow(a3, wVar.c(), wVar.d());
        }
        annotUnlockAnnotsInPage();
        return annotAdd != 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(c cVar) {
        if (!this.r) {
            return false;
        }
        int f2 = cVar.f();
        if (f2 != 6 && f2 != 7 && f2 != 8) {
            return false;
        }
        lockLinksInPage(cVar.d());
        boolean linkDataOpen = getLinkDataOpen(cVar.e());
        getLinkDataClose(cVar.e());
        unlockLinksInPage();
        return linkDataOpen;
    }

    public final boolean a(y yVar, String str, InputStream inputStream) {
        return a(yVar, str, inputStream, "", "");
    }

    public final boolean a(y yVar, String str, InputStream inputStream, String str2, String str3) {
        this.j = 0;
        int i2 = 0;
        if (b.a(str)) {
            this.f = str;
            this.g = str;
            udk.android.reader.b.c.a("## TRY TO OPEN PDF WITH FILEPATH");
            i2 = a(yVar, str, str2, str3, str2, str3);
        } else if (inputStream != null) {
            this.g = com.unidocs.commonlib.a.b.a(24);
            udk.android.reader.b.c.a("## TRY TO OPEN PDF WITH STREAM");
            i2 = a(yVar, inputStream, a.a(), b.a(new String[]{str2, str3}), str2, str3, str2, str3);
        }
        if (i2 <= 0) {
            return false;
        }
        this.v = new ag(yVar, this.g);
        this.w = t();
        if (this.w == 0) {
            this.w = a.aM == 9 ? L() : a.aM;
        }
        if (a.g) {
            udk.android.reader.pdf.b.d.a().c();
        }
        this.p = getNumPages();
        this.k = 0.01f;
        this.j = 1;
        this.l = a(this.j, this.k);
        this.m = b(this.j, this.k);
        this.n = false;
        this.o = false;
        this.r = true;
        ah ahVar = new ah();
        ahVar.b = this.p;
        ahVar.a = 1;
        for (ar g2 : this.b) {
            g2.g();
        }
        return true;
    }

    public final double[] a(int i2, float f2, RectF rectF) {
        return a(i2, f2, new int[]{(int) Math.min(rectF.left, rectF.right), (int) Math.max(rectF.top, rectF.bottom), (int) Math.max(rectF.left, rectF.right), (int) Math.min(rectF.top, rectF.bottom)});
    }

    public final double[] a(int i2, float f2, int[] iArr) {
        if (!this.r) {
            return new double[]{0.0d, 0.0d, 0.0d, 0.0d};
        }
        double[] dArr = new double[iArr.length];
        dp2pg(i2, (double) (100.0f * f2), iArr, dArr);
        return dArr;
    }

    public final int[] a(int i2, float f2, double[] dArr) {
        if (!this.r) {
            return new int[4];
        }
        int[] iArr = new int[dArr.length];
        pg2dp(i2, (double) (100.0f * f2), dArr, iArr);
        return iArr;
    }

    public native int abortRendering();

    public final float b(float f2) {
        return f2 / ((float) D());
    }

    public final int b(int i2, float f2) {
        if (this.r) {
            return getPageHeight(i2, (double) (100.0f * f2));
        }
        return 0;
    }

    public final Bitmap b(int i2, float f2, int i3, int i4, int i5, int i6) {
        int renderSlice2buffer;
        int i7;
        Bitmap bitmap;
        if (!this.r) {
            return null;
        }
        if (!lookupRenderedPageSlice(i2, (double) (100.0f * f2), i3, i4, i5, i6)) {
            return null;
        }
        try {
            if (a.f || a.g) {
                s a2 = s.a();
                if (!a2.f(i2)) {
                    a2.g(i2);
                }
            }
            if (a.X) {
                Bitmap createBitmap = Bitmap.createBitmap(i5, i6, Bitmap.Config.RGB_565);
                i7 = b(i2, f2, createBitmap, i3, i4, i5, i6);
                bitmap = createBitmap;
            } else {
                ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i5 * i6 * 2);
                if (!this.r) {
                    renderSlice2buffer = -1;
                } else {
                    setRenderingState(1);
                    renderSlice2buffer = renderSlice2buffer(i2, (double) (100.0f * f2), allocateDirect, i3, i4, i5, i6, true, true);
                    setRenderingState(0);
                }
                if (renderSlice2buffer == 1) {
                    Bitmap createBitmap2 = Bitmap.createBitmap(i5, i6, Bitmap.Config.RGB_565);
                    createBitmap2.copyPixelsFromBuffer(allocateDirect);
                    Bitmap bitmap2 = createBitmap2;
                    i7 = renderSlice2buffer;
                    bitmap = bitmap2;
                } else {
                    i7 = renderSlice2buffer;
                    bitmap = null;
                }
            }
            if (i7 != 1 || bitmap == null) {
                return null;
            }
            if (!a.f && !a.g) {
                return bitmap;
            }
            Canvas canvas = new Canvas(bitmap);
            canvas.save();
            canvas.translate((float) (-i3), (float) (-i4));
            a(canvas, i2, f2);
            canvas.restore();
            return bitmap;
        } catch (OutOfMemoryError e2) {
            udk.android.reader.b.c.a(e2.getMessage(), e2);
            this.u = this.u + 1;
            M();
            System.gc();
            return null;
        }
    }

    public final String b(String str) {
        return lookupDocInfo(str);
    }

    public final String b(ai aiVar, ai aiVar2) {
        if (aiVar == null || aiVar2 == null) {
            return null;
        }
        return getTextInRange(this.j, aiVar.a, aiVar.b, aiVar2.a, aiVar2.b);
    }

    /* access modifiers changed from: package-private */
    public final List b(int i2) {
        if (!this.r) {
            return null;
        }
        lockLinksInPage(i2);
        int numLinks = getNumLinks();
        if (numLinks <= 0) {
            unlockLinksInPage();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < numLinks; i3++) {
            c cVar = new c(i2, i3, getLinkRefNo(i3));
            cVar.a(getLinkType(i3));
            double[] dArr = new double[4];
            getLinkRect(i3, dArr);
            cVar.a(dArr);
            arrayList.add(cVar);
        }
        unlockLinksInPage();
        return arrayList;
    }

    public final void b(int i2, int i3) {
        f(i2, e(i2, (float) i3));
        this.o = true;
    }

    public final void b(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            annotRemove(annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final void b(udk.android.reader.pdf.annotation.a aVar) {
        annotLockAnnotsInPage(aVar.ag());
        int annotFindAnnot = annotFindAnnot(aVar.K());
        if (annotFindAnnot >= 0) {
            float g2 = aVar.g();
            RectF b2 = aVar.b(g2);
            float X = aVar.X();
            float Y = aVar.Y();
            float V = ((aVar.V() * g2) / 1.0f) * X;
            float W = ((aVar.W() * g2) / 1.0f) * Y;
            int i2 = Integer.MIN_VALUE;
            int annotGetPathNum = annotGetPathNum(annotFindAnnot);
            ArrayList arrayList = new ArrayList(annotGetPathNum);
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MIN_VALUE;
            int i5 = Integer.MAX_VALUE;
            for (int i6 = 0; i6 < annotGetPathNum; i6++) {
                double[] annotGetPathPoints = annotGetPathPoints(annotFindAnnot, i6);
                if (!b.b(annotGetPathPoints)) {
                    int[] a2 = a(aVar.ag(), g2, annotGetPathPoints);
                    int i7 = i3;
                    int i8 = i5;
                    int i9 = i2;
                    int i10 = i4;
                    for (int i11 = 0; i11 < a2.length; i11++) {
                        if (i11 % 2 != 1) {
                            a2[i11] = (int) (b2.left + ((((float) a2[i11]) - b2.left) * X) + V);
                            if (a2[i11] < i7) {
                                i7 = a2[i11];
                            } else if (a2[i11] > i9) {
                                i9 = a2[i11];
                            }
                        } else {
                            a2[i11] = (int) (b2.top + ((((float) a2[i11]) - b2.top) * Y) + W);
                            if (a2[i11] < i8) {
                                i8 = a2[i11];
                            } else if (a2[i11] > i10) {
                                i10 = a2[i11];
                            }
                        }
                    }
                    arrayList.add(a(aVar.ag(), g2, a2));
                    i4 = i10;
                    i2 = i9;
                    i5 = i8;
                    i3 = i7;
                }
            }
            if (arrayList.size() > 0) {
                int i12 = 0;
                while (true) {
                    int i13 = i12;
                    if (i13 >= arrayList.size()) {
                        break;
                    }
                    annotSetPathPoints(annotFindAnnot, (double[]) arrayList.get(i13), i13 == 0);
                    i12 = i13 + 1;
                }
                aVar.a(a(aVar.ag(), g2, annotFindAnnot));
                double[] a3 = a(aVar.ag(), g2, new int[]{i3, i5, i2, i4});
                annotSetRect(annotFindAnnot, a3, false);
                aVar.b(a3);
            }
            b(aVar, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final void b(w wVar) {
        annotLockAnnotsInPage(wVar.ag());
        int annotFindAnnot = annotFindAnnot(wVar.K());
        if (annotFindAnnot >= 0) {
            annotSetPathPoints(annotFindAnnot, wVar.a(), true);
            wVar.a(annotGetPathPoints(annotFindAnnot, 0));
            wVar.b(annotGetRect(annotFindAnnot, false));
            b(wVar, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final void b(ar arVar) {
        this.b.remove(arVar);
    }

    public final boolean b() {
        return this.s;
    }

    public final Bitmap c(int i2, float f2) {
        int a2 = a(i2, f2);
        int b2 = b(i2, f2);
        if (a2 <= 0 || b2 <= 0) {
            return null;
        }
        return a(i2, f2, 0, 0, a2, b2);
    }

    public final String c(int i2) {
        return getPageTextAsXML(i2);
    }

    public final void c() {
        this.s = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, float, int[]):double[]
     arg types: [int, int, int[]]
     candidates:
      udk.android.reader.pdf.PDF.a(int, float, int):java.util.List
      udk.android.reader.pdf.PDF.a(android.graphics.Canvas, int, float):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.k, java.util.Map, udk.android.reader.pdf.b.q):void
      udk.android.reader.pdf.PDF.a(int, int, int):int
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.ac, udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):boolean
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.y, java.lang.String, java.io.InputStream):boolean
      udk.android.reader.pdf.PDF.a(int, float, android.graphics.RectF):double[]
      udk.android.reader.pdf.PDF.a(int, float, double[]):int[]
      udk.android.reader.pdf.PDF.a(int, float, int[]):double[] */
    public final void c(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            RectF b2 = annotation.b(100.0f);
            float X = annotation.X();
            float Y = annotation.Y();
            float V = ((annotation.V() * 100.0f) / 1.0f) * X;
            float W = ((annotation.W() * 100.0f) / 1.0f) * Y;
            int[] a2 = a(annotation.ag(), 100.0f, annotation.af());
            for (int i2 = 0; i2 < a2.length; i2++) {
                if (i2 % 2 != 1) {
                    a2[i2] = (int) (b2.left + ((((float) a2[i2]) - b2.left) * X) + V);
                } else {
                    a2[i2] = (int) (b2.top + ((((float) a2[i2]) - b2.top) * Y) + W);
                }
            }
            annotSetRect(annotFindAnnot, a(annotation.ag(), 100.0f, a2), false);
            annotation.b(annotGetRect(annotFindAnnot, false));
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public native boolean checkPrivatePieceInfo();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, int):int
      udk.android.reader.pdf.PDF.a(int, double[]):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.c, java.lang.String):java.util.List
      udk.android.reader.pdf.PDF.a(float, float):udk.android.reader.pdf.ai
      udk.android.reader.pdf.PDF.a(int, int):void
      udk.android.reader.pdf.PDF.a(int, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.c, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, udk.android.reader.pdf.annotation.z):boolean
      udk.android.reader.pdf.PDF.a(int, float):int */
    public final float d(int i2, float f2) {
        return f2 / ((float) a(i2, 1.0f));
    }

    public final long d() {
        return this.t;
    }

    public final List d(int i2) {
        ArrayList arrayList = new ArrayList();
        int textFlowCount = getTextFlowCount(i2);
        for (int i3 = 0; i3 < textFlowCount; i3++) {
            int textColumnCount = getTextColumnCount(i2, i3);
            for (int i4 = 0; i4 < textColumnCount; i4++) {
                double[] dArr = new double[4];
                if (getTextColumnBBox(i2, i3, i4, dArr) > 0) {
                    x xVar = new x();
                    xVar.c(i2);
                    xVar.a(i3);
                    xVar.b(i4);
                    xVar.a(new udk.android.reader.pdf.a.b(i2, dArr));
                    arrayList.add(xVar);
                }
            }
        }
        return arrayList;
    }

    public final void d(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            annotSetRect(annotFindAnnot, annotation.af(), false);
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.b(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.annotation.Annotation, int):void
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.lang.String
      udk.android.reader.pdf.PDF.b(int, int):void
      udk.android.reader.pdf.PDF.b(int, float):int */
    public final float e(int i2, float f2) {
        return f2 / ((float) b(i2, 1.0f));
    }

    public final List e(int i2) {
        int imageBlockCount = getImageBlockCount(i2);
        if (imageBlockCount <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < imageBlockCount; i3++) {
            double[] dArr = new double[4];
            if (getImageBlockBBox(i2, i3, dArr) == 1) {
                arrayList.add(new udk.android.reader.pdf.a.b(i2, dArr));
            }
        }
        return arrayList;
    }

    public final void e() {
        if (this.r) {
            this.r = false;
            this.q = true;
            for (int size = this.b.size() - 1; size >= 0; size--) {
                ((ar) this.b.get(size)).h();
            }
            if (this.f != null && new File(this.f).exists()) {
                try {
                    new RandomAccessFile(this.f, "rws").getFD().sync();
                } catch (Exception e2) {
                    udk.android.reader.b.c.a((Throwable) e2);
                }
            }
            this.f = null;
            this.p = 0;
            synchronized (this.c) {
                close();
            }
            this.q = false;
            K();
        }
    }

    public final void e(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            annotSetBorderStyle(annotFindAnnot, annotation.T(), (double) annotation.S(), null);
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, int):int
      udk.android.reader.pdf.PDF.a(int, double[]):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.c, java.lang.String):java.util.List
      udk.android.reader.pdf.PDF.a(float, float):udk.android.reader.pdf.ai
      udk.android.reader.pdf.PDF.a(int, int):void
      udk.android.reader.pdf.PDF.a(int, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.c, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, udk.android.reader.pdf.annotation.z):boolean
      udk.android.reader.pdf.PDF.a(int, float):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.b(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.annotation.Annotation, int):void
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.lang.String
      udk.android.reader.pdf.PDF.b(int, int):void
      udk.android.reader.pdf.PDF.b(int, float):int */
    public final void f(int i2, float f2) {
        this.l = a(i2, f2);
        this.m = b(i2, f2);
        ah ahVar = new ah();
        int i3 = this.j;
        if (i2 != this.j) {
            ahVar.c = true;
            ahVar.b = this.p;
            ahVar.a = i2;
            for (ar a2 : this.b) {
                a2.a(ahVar);
            }
            this.j = i2;
        }
        if (f2 != this.k) {
            this.n = false;
            this.o = false;
            ahVar.b = this.p;
            ahVar.a = this.j;
            ahVar.d = true;
            this.k = f2;
        }
        if (ahVar.c || ahVar.d) {
            for (ar b2 : this.b) {
                b2.b(ahVar);
            }
        }
        if (this.n && i3 != this.j) {
            float a3 = ((float) a(i2, 1.0f)) / ((float) a(i3, 1.0f));
            if (a3 != 1.0f) {
                f(this.j, this.k / a3);
                this.n = true;
            }
        } else if (this.o && i3 != this.j) {
            float b3 = ((float) b(i2, 1.0f)) / ((float) b(i3, 1.0f));
            if (b3 != 1.0f) {
                f(this.j, this.k / b3);
                this.o = true;
            }
        }
    }

    public final void f(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            annotSetColor(annotFindAnnot, annotation.C(), annotation.D(), annotation.E());
            if (annotation instanceof udk.android.reader.pdf.annotation.e) {
                annotSetTextColor(annotFindAnnot, annotation.C(), annotation.D(), annotation.E());
            }
            annotSetTransparency(annotFindAnnot, annotation.B());
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final boolean f() {
        return this.r;
    }

    public final boolean f(int i2) {
        return i2 > 0 && i2 <= this.p;
    }

    public final void g(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            if (annotation.aa()) {
                annotSetInnerColor(annotFindAnnot, annotation.F(), annotation.G(), annotation.H());
            } else {
                annotSetInnerColor(annotFindAnnot, -1.0d, -1.0d, -1.0d);
            }
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final boolean g() {
        return !this.r || this.q;
    }

    public final boolean g(int i2) {
        return f(i2) && i2 >= this.j - 1 && i2 <= this.j + 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, int):int
      udk.android.reader.pdf.PDF.a(int, double[]):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.util.List
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.c, java.lang.String):java.util.List
      udk.android.reader.pdf.PDF.a(float, float):udk.android.reader.pdf.ai
      udk.android.reader.pdf.PDF.a(int, int):void
      udk.android.reader.pdf.PDF.a(int, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.c, boolean):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.Annotation, udk.android.reader.pdf.annotation.z):boolean
      udk.android.reader.pdf.PDF.a(int, float):int */
    public final int h(int i2) {
        return a(i2, 1.0f);
    }

    public final void h(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            annotSetSubject(annotFindAnnot, annotation.N());
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final boolean h() {
        return isEncrypted();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.b(int, float):int
     arg types: [int, int]
     candidates:
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.annotation.Annotation, int):void
      udk.android.reader.pdf.PDF.b(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):java.lang.String
      udk.android.reader.pdf.PDF.b(int, int):void
      udk.android.reader.pdf.PDF.b(int, float):int */
    public final int i(int i2) {
        return b(i2, 1.0f);
    }

    public final String i() {
        return getEncryptFilter();
    }

    public final void i(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            annotSetAuthor(annotFindAnnot, annotation.P());
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final void j(int i2) {
        f(i2, this.k);
    }

    public final void j(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        int annotFindAnnot = annotFindAnnot(annotation.K());
        if (annotFindAnnot >= 0) {
            annotSetContents(annotFindAnnot, annotation.M());
            b(annotation, annotFindAnnot);
        }
        annotUnlockAnnotsInPage();
    }

    public final boolean j() {
        return isEncrypted() && "Standard".equalsIgnoreCase(getEncryptFilter());
    }

    public final void k(Annotation annotation) {
        annotLockAnnotsInPage(annotation.ag());
        l(annotation);
        annotUnlockAnnotsInPage();
    }

    public final boolean k() {
        return isEncrypted() && "UDOC_EZDRM".equalsIgnoreCase(getEncryptFilter());
    }

    public final int l() {
        return this.p;
    }

    public final int m() {
        if (!this.r) {
            return -1;
        }
        moveToRootOutline();
        return getOutlineCount();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public final k n() {
        udk.android.reader.pdf.b.i iVar;
        ArrayList<q> arrayList = new ArrayList<>();
        int fieldGetNumFields = fieldGetNumFields();
        for (int i2 = 0; i2 < fieldGetNumFields; i2++) {
            String fieldGetType = fieldGetType(i2);
            String fieldGetTitle = fieldGetTitle(i2);
            String fieldGetValue = fieldGetValue(i2);
            int fieldGetFlags = fieldGetFlags(i2);
            if ("Tx".equals(fieldGetType)) {
                udk.android.reader.pdf.b.i iVar2 = new udk.android.reader.pdf.b.i(i2, fieldGetTitle, fieldGetFlags, fieldGetValue);
                iVar2.a((fieldGetFlags & 4096) != 0);
                iVar2.b((fieldGetFlags & 8192) != 0);
                iVar2.a(fieldTxGetMaxLen(i2));
                iVar = iVar2;
            } else if (n.a(fieldGetType, fieldGetFlags)) {
                iVar = new n(i2, fieldGetTitle, fieldGetFlags, fieldGetValue);
            } else if (udk.android.reader.pdf.b.b.a(fieldGetType, fieldGetFlags)) {
                iVar = new udk.android.reader.pdf.b.b(i2, fieldGetTitle, fieldGetFlags, fieldGetValue);
            } else if (j.a(fieldGetType, fieldGetFlags)) {
                iVar = new j(i2, fieldGetTitle, fieldGetFlags, fieldGetValue);
            } else {
                if ("Ch".equals(fieldGetType) && (fieldGetFlags & 131072) != 0) {
                    iVar = new l(i2, fieldGetTitle, fieldGetFlags, fieldGetValue, k(i2));
                } else {
                    iVar = "Ch".equals(fieldGetType) && (fieldGetFlags & 131072) == 0 ? new udk.android.reader.pdf.b.a(i2, fieldGetTitle, fieldGetFlags, fieldGetValue, k(i2)) : "Sig".equals(fieldGetType) ? new h(i2, fieldGetTitle, fieldGetFlags, fieldGetValue) : "Pen".equals(fieldGetType) ? new m(i2, fieldGetTitle, fieldGetFlags, fieldGetValue) : null;
                }
            }
            if (iVar != null) {
                iVar.c((fieldGetFlags & 2) != 0);
                iVar.d((fieldGetFlags & 1) != 0);
                arrayList.add(iVar);
            }
        }
        k kVar = new k();
        if (b.b((Collection) arrayList)) {
            return kVar;
        }
        Collections.sort(arrayList);
        HashMap hashMap = new HashMap();
        for (q a2 : arrayList) {
            a(kVar, hashMap, a2);
        }
        return kVar;
    }

    public final boolean o() {
        return fieldFlatten(-1, true) > 0;
    }

    public native boolean okToAddNotes();

    public native boolean okToCopy();

    public final boolean p() {
        return a((String) null);
    }

    public final int q() {
        return this.u;
    }

    public final boolean r() {
        return getRenderingState() != 0;
    }

    public final int s() {
        return this.w;
    }

    public final int t() {
        return this.v.a("bookreaddirection", 0);
    }

    public final boolean u() {
        return this.w == 2;
    }

    public final ag v() {
        return this.v;
    }

    public final boolean w() {
        return f(this.j - 1);
    }

    public final boolean x() {
        return f(this.j + 1);
    }

    public final String y() {
        return this.f;
    }

    public final String z() {
        return this.g;
    }
}
