package udk.android.reader.pdf;

import android.content.DialogInterface;
import android.widget.TextView;
import java.util.List;

final class al implements DialogInterface.OnClickListener {
    private /* synthetic */ l a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ String c;
    private final /* synthetic */ List d;

    al(l lVar, TextView textView, String str, List list) {
        this.a = lVar;
        this.b = textView;
        this.c = str;
        this.d = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.setText(String.valueOf(this.c) + " " + ((String) this.d.get(i)));
        this.a.a.i.dismiss();
    }
}
