package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.List;
import udk.android.reader.b.c;

public final class a extends Annotation {
    private int a;
    private List b;
    private List c;
    private Paint d;
    private float e = 100.0f;

    public a(int i, double[] dArr, boolean z) {
        super(i, dArr);
        b(z);
        a((List) null);
        this.d = new Paint(1);
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeJoin(Paint.Join.ROUND);
        this.d.setStrokeCap(Paint.Cap.ROUND);
    }

    private void m() {
        int i = this.a + 1;
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < i; i2++) {
            List list = (List) this.b.get(i2);
            Path path = new Path();
            int size = list.size() / 2;
            path.moveTo((float) ((Integer) list.get(0)).intValue(), (float) ((Integer) list.get(1)).intValue());
            for (int i3 = 1; i3 < size; i3++) {
                float intValue = (float) ((Integer) list.get((i3 - 1) * 2)).intValue();
                float intValue2 = (float) ((Integer) list.get(((i3 - 1) * 2) + 1)).intValue();
                path.quadTo(intValue, intValue2, (((float) ((Integer) list.get(i3 * 2)).intValue()) + intValue) / 2.0f, (((float) ((Integer) list.get((i3 * 2) + 1)).intValue()) + intValue2) / 2.0f);
            }
            path.lineTo((float) ((Integer) list.get((size - 1) * 2)).intValue(), (float) ((Integer) list.get(((size - 1) * 2) + 1)).intValue());
            arrayList.add(path);
        }
        this.c = arrayList;
    }

    public final void a() {
        List list = (List) this.b.get(this.a);
        ((Path) this.c.get(this.c.size() - 1)).lineTo((float) ((Integer) list.get(list.size() - 2)).intValue(), (float) ((Integer) list.get(list.size() - 1)).intValue());
    }

    public final void a(double d2, double[] dArr) {
        super.a(d2, dArr);
        this.d.setColor(L());
    }

    public final void a(float f) {
        super.a(f);
        this.d.setStrokeWidth(S() * this.e);
    }

    public final void a(float f, float f2, float f3) {
        int i = (int) ((f / f3) * this.e);
        int i2 = (int) ((f2 / f3) * this.e);
        this.a++;
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf(i));
        arrayList.add(Integer.valueOf(i2));
        for (int size = this.b.size() - 1; size >= this.a; size--) {
            this.b.remove(size);
        }
        this.b.add(arrayList);
        Path path = new Path();
        path.moveTo((float) i, (float) i2);
        this.c.add(path);
    }

    public final void a(int i) {
        super.a(i);
        this.d.setColor(L());
    }

    public final void a(Canvas canvas, float f) {
        canvas.save();
        canvas.scale(f / this.e, f / this.e);
        if (!(X() == 1.0f && Y() == 1.0f)) {
            RectF b2 = b(this.e);
            canvas.scale(X(), Y(), b2.left, b2.top);
        }
        if (!(V() == 0.0f && W() == 0.0f)) {
            canvas.translate(V() * this.e, W() * this.e);
        }
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            try {
                canvas.drawPath((Path) this.c.get(i), this.d);
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
        canvas.restore();
    }

    public final void a(List list) {
        this.b = new ArrayList();
        this.a = -1;
        this.c = b.b(list) ? new ArrayList() : list;
    }

    public final RectF b(float f) {
        RectF rectF;
        if (af() != null) {
            rectF = super.b(f);
        } else {
            RectF rectF2 = null;
            for (Path path : this.c) {
                if (rectF2 == null) {
                    rectF2 = new RectF();
                    path.computeBounds(rectF2, true);
                    udk.android.b.c.c(rectF2, f / this.e);
                } else {
                    RectF rectF3 = new RectF();
                    path.computeBounds(rectF3, true);
                    udk.android.b.c.c(rectF3, f / this.e);
                    rectF2.union(rectF3);
                }
            }
            rectF = rectF2;
        }
        if (rectF != null) {
            rectF.right = rectF.left + (rectF.width() * X());
            rectF.bottom = rectF.top + (rectF.height() * Y());
            rectF.offset((V() * f) / 1.0f, (W() * f) / 1.0f);
        }
        return rectF;
    }

    public final void b(float f, float f2, float f3) {
        int i = (int) ((f / f3) * this.e);
        int i2 = (int) ((f2 / f3) * this.e);
        List list = (List) this.b.get(this.a);
        float intValue = (float) ((Integer) list.get(list.size() - 2)).intValue();
        float intValue2 = (float) ((Integer) list.get(list.size() - 1)).intValue();
        list.add(Integer.valueOf(i));
        list.add(Integer.valueOf(i2));
        ((Path) this.c.get(this.c.size() - 1)).quadTo(intValue, intValue2, (((float) i) + intValue) / 2.0f, (((float) i2) + intValue2) / 2.0f);
    }

    public final boolean b() {
        return this.a >= 0;
    }

    public final void c() {
        if (b()) {
            this.a--;
            m();
            ab abVar = new ab();
            abVar.a = this;
            s.a().a(abVar);
        }
    }

    public final boolean c(float f, float f2, float f3) {
        RectF rectF = new RectF();
        for (Path computeBounds : this.c) {
            computeBounds.computeBounds(rectF, true);
            udk.android.b.c.c(rectF, f / this.e);
            if (rectF.contains(f2, f3)) {
                return true;
            }
        }
        return false;
    }

    public final boolean d() {
        return this.a < this.b.size() - 1;
    }

    public final void e() {
        if (d()) {
            this.a++;
            m();
            ab abVar = new ab();
            abVar.a = this;
            s.a().a(abVar);
        }
    }

    public final List f() {
        for (int size = this.b.size() - 1; size > this.a; size--) {
            this.b.remove(size);
        }
        return this.b;
    }

    public final float g() {
        return this.e;
    }

    public final boolean h() {
        return false;
    }

    public final boolean i() {
        return true;
    }

    public final boolean j() {
        return true;
    }

    public final String k() {
        return "Ink";
    }

    public final boolean l() {
        return true;
    }
}
