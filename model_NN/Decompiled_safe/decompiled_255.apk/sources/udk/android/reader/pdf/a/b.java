package udk.android.reader.pdf.a;

import android.graphics.RectF;
import udk.android.reader.pdf.PDF;

public class b extends c {
    public b(int i, double[] dArr) {
        g(i);
        b(dArr);
    }

    public RectF b(float f) {
        int ag = ag();
        double[] af = af();
        if (ag <= 0 || com.unidocs.commonlib.util.b.b(af)) {
            return null;
        }
        int[] a = PDF.a().a(ag, f, af);
        return new RectF((float) Math.min(a[0], a[2]), (float) Math.min(a[1], a[3]), (float) Math.max(a[2], a[0]), (float) Math.max(a[3], a[1]));
    }
}
