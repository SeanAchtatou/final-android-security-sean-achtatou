package udk.android.reader.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.TextView;
import java.util.List;

final class l implements Runnable {
    final /* synthetic */ b a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ String c;
    private final /* synthetic */ Context d;
    private final /* synthetic */ List e;

    l(b bVar, TextView textView, String str, Context context, List list) {
        this.a = bVar;
        this.b = textView;
        this.c = str;
        this.d = context;
        this.e = list;
    }

    public final void run() {
        this.a.i = new AlertDialog.Builder(this.d).setSingleChoiceItems((CharSequence[]) this.e.toArray(new String[0]), this.e.indexOf(this.b.getText().toString().replaceAll(this.c, "").trim()), new al(this, this.b, this.c, this.e)).show();
    }
}
