package udk.android.reader.pdf.annotation;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import udk.android.b.c;
import udk.android.b.l;
import udk.android.reader.b.a;
import udk.android.reader.pdf.a.b;

public abstract class Annotation extends b {
    private Object a;
    private int b;
    private boolean c;
    private int d;
    private int e;
    private boolean f;
    private String g;
    private String h;
    private String i;
    private boolean j;
    private String k;
    private String l;
    private String m;
    private float n;
    private int o = 1;
    private boolean p;
    private boolean q = false;
    private Boolean r;
    private float s;
    private float t;
    private float u;
    private float v;
    private float w;

    public enum TransformingType {
        MOVE,
        SCALE_LEFT_TOP,
        SCALE_LEFT_MIDDLE,
        SCALE_LEFT_BOTTOM,
        SCALE_CENTER_TOP,
        SCALE_CENTER_BOTTOM,
        SCALE_RIGHT_TOP,
        SCALE_RIGHT_MIDDLE,
        SCALE_RIGHT_BOTTOM,
        START_HEAD,
        END_HEAD
    }

    public Annotation(int i2, double[] dArr) {
        super(i2, dArr);
        I();
    }

    public final int A() {
        return c.a(this.d);
    }

    public final double B() {
        return ((double) c.a(this.d)) / 255.0d;
    }

    public final double C() {
        return ((double) c.b(this.d)) / 255.0d;
    }

    public final double D() {
        return ((double) c.c(this.d)) / 255.0d;
    }

    public final double E() {
        return ((double) c.d(this.d)) / 255.0d;
    }

    public final double F() {
        return ((double) c.b(this.e)) / 255.0d;
    }

    public final double G() {
        return ((double) c.c(this.e)) / 255.0d;
    }

    public final double H() {
        return ((double) c.d(this.e)) / 255.0d;
    }

    public final void I() {
        this.t = 0.0f;
        this.u = 0.0f;
        this.v = 1.0f;
        this.w = 1.0f;
    }

    public final Object J() {
        return this.a;
    }

    public final int K() {
        return this.b;
    }

    public final int L() {
        return this.d;
    }

    public final String M() {
        return this.l;
    }

    public final String N() {
        return this.k;
    }

    public final String O() {
        return this.g;
    }

    public final String P() {
        return this.m;
    }

    public final boolean Q() {
        return this.j;
    }

    public final void R() {
        this.j = true;
    }

    public final float S() {
        return this.n;
    }

    public final int T() {
        return this.o;
    }

    public final boolean U() {
        return this.q;
    }

    public final float V() {
        return this.t;
    }

    public final float W() {
        return this.u;
    }

    public final float X() {
        return this.v;
    }

    public final float Y() {
        return this.w;
    }

    public final int Z() {
        return this.e;
    }

    public TransformingType a(PointF pointF, float f2) {
        float u2 = u();
        RectF b2 = b(f2);
        if (i() && b2.contains(pointF.x, pointF.y)) {
            return TransformingType.MOVE;
        }
        if (j()) {
            if (c.a(pointF, new PointF(b2.right + 16.0f, b2.bottom + 16.0f)) <= u2) {
                return TransformingType.SCALE_RIGHT_BOTTOM;
            }
            if (c.a(pointF, new PointF(b2.right + 16.0f, b2.top - 16.0f)) <= u2) {
                return TransformingType.SCALE_RIGHT_TOP;
            }
            if (c.a(pointF, new PointF(b2.right + 16.0f, b2.centerY())) <= u2) {
                return TransformingType.SCALE_RIGHT_MIDDLE;
            }
            if (c.a(pointF, new PointF(b2.left - 16.0f, b2.bottom + 16.0f)) <= u2) {
                return TransformingType.SCALE_LEFT_BOTTOM;
            }
            if (c.a(pointF, new PointF(b2.left - 16.0f, b2.top - 16.0f)) <= u2) {
                return TransformingType.SCALE_LEFT_TOP;
            }
            if (c.a(pointF, new PointF(b2.left - 16.0f, b2.centerY())) <= u2) {
                return TransformingType.SCALE_LEFT_MIDDLE;
            }
            if (c.a(pointF, new PointF(b2.centerX(), b2.bottom + 16.0f)) <= u2) {
                return TransformingType.SCALE_CENTER_BOTTOM;
            }
            if (c.a(pointF, new PointF(b2.centerX(), b2.top - 16.0f)) <= u2) {
                return TransformingType.SCALE_CENTER_TOP;
            }
        }
        return null;
    }

    public void a(double d2, double[] dArr) {
        a(c.a((int) (d2 * 255.0d), (int) (dArr[0] * 255.0d), (int) (dArr[1] * 255.0d), (int) (dArr[2] * 255.0d)));
    }

    public void a(float f2) {
        this.n = f2;
    }

    public void a(int i2) {
        this.d = i2;
    }

    public abstract void a(Canvas canvas, float f2);

    public final void a(Object obj) {
        this.a = obj;
    }

    public void a(String str) {
        this.l = str != null ? str.replaceAll("\\r\\n", "\n") : str;
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public boolean a_() {
        return i() || j();
    }

    public final boolean aa() {
        return this.f;
    }

    public final boolean ab() {
        return this.p;
    }

    public final void ac() {
        this.p = true;
    }

    public RectF b(float f2) {
        if (!this.c) {
            return super.b(f2);
        }
        RectF b2 = super.b(f2);
        RectF b3 = super.b(1.0f);
        b2.right = b2.left + b3.width();
        b2.bottom = b3.height() + b2.top;
        return b2;
    }

    public final void b(boolean z) {
        this.q = z;
    }

    public final void c(String str) {
        this.k = str;
    }

    public final void c(boolean z) {
        this.f = z;
    }

    public final void d(int i2) {
        this.b = i2;
    }

    public final void d(String str) {
        this.g = str;
    }

    public final void e(int i2) {
        this.o = i2;
    }

    public final void e(String str) {
        this.m = str;
    }

    public final void f(float f2) {
        this.t = f2;
    }

    public void f(int i2) {
        this.e = i2;
    }

    public final void f(String str) {
        this.i = str;
    }

    public final void g(float f2) {
        this.u = f2;
    }

    public final void g(String str) {
        this.h = str;
    }

    public final void h(float f2) {
        this.v = f2;
    }

    public boolean h() {
        return true;
    }

    public final void i(float f2) {
        this.w = f2;
    }

    public boolean i() {
        return false;
    }

    public boolean j() {
        return false;
    }

    public abstract String k();

    public boolean l() {
        return false;
    }

    public final String q() {
        if (com.unidocs.commonlib.util.b.b(this.h)) {
            return "";
        }
        String str = this.h;
        try {
            return l.a(this.h).toString();
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Object) e2);
            return str;
        }
    }

    public final String r() {
        if (com.unidocs.commonlib.util.b.b(this.i)) {
            return "";
        }
        String str = this.i;
        try {
            return l.a(this.i).toString();
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Object) e2);
            return str;
        }
    }

    public boolean s() {
        return false;
    }

    public boolean t() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final float u() {
        if (this.s < 1.0f) {
            this.s = (a.a(16.0f) / 2.0f) * 4.0f;
        }
        return this.s;
    }

    public final boolean v() {
        this.r = Boolean.valueOf(this.q);
        this.q = false;
        return !this.r.equals(Boolean.valueOf(this.q));
    }

    public final boolean w() {
        boolean z = this.r != null && !this.r.equals(Boolean.valueOf(this.q));
        if (this.r != null) {
            this.q = this.r.booleanValue();
        }
        this.r = null;
        return z;
    }

    public final int x() {
        return c.b(this.d);
    }

    public final int y() {
        return c.c(this.d);
    }

    public final int z() {
        return c.d(this.d);
    }
}
