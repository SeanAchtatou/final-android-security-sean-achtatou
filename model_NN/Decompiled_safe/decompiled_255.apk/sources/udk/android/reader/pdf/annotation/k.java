package udk.android.reader.pdf.annotation;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import com.unidocs.commonlib.util.b;
import udk.android.b.m;
import udk.android.reader.b.a;

final class k implements DialogInterface.OnClickListener {
    private /* synthetic */ s a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ EditText c;
    private final /* synthetic */ Context d;
    private final /* synthetic */ Annotation e;
    private final /* synthetic */ Runnable f;

    k(s sVar, EditText editText, EditText editText2, Context context, Annotation annotation, Runnable runnable) {
        this.a = sVar;
        this.b = editText;
        this.c = editText2;
        this.d = context;
        this.e = annotation;
        this.f = runnable;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        m.a(this.b.hasFocus() ? this.b : this.c);
        String editable = this.b.getText().toString();
        if (b.b(editable)) {
            editable = a.t;
        }
        String editable2 = this.c.getText().toString();
        if (b.b(editable2)) {
            editable2 = udk.android.reader.b.b.S;
        }
        if (this.a.a(this.d, this.e, editable, editable2) != null && this.f != null) {
            this.f.run();
        }
    }
}
