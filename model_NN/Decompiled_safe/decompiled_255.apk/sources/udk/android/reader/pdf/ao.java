package udk.android.reader.pdf;

import com.unidocs.commonlib.util.b;
import java.util.Collection;
import java.util.List;

final class ao extends Thread {
    boolean a;
    private String b;
    private boolean c;
    private boolean d;
    private int e;
    private /* synthetic */ b f;

    public ao(b bVar, String str, boolean z, boolean z2, int i) {
        this.f = bVar;
        this.b = str;
        this.c = z;
        this.d = z2;
        this.e = i;
    }

    public final void run() {
        b.a(this.f);
        int l = PDF.a().l();
        this.a = true;
        for (int i = 1; i <= l; i++) {
            this.f.g = i;
            List a2 = PDF.a().a(i, this.b, this.c, this.d, this.e);
            if (b.a((Collection) a2)) {
                this.f.f.add(Integer.valueOf(i));
                this.f.d.put(Integer.valueOf(i), a2);
                b bVar = this.f;
                bVar.e = a2.size() + bVar.e;
                d dVar = new d();
                dVar.b = i;
                dVar.a = this.f.e;
                b.a(this.f, dVar);
            }
            if (!this.a) {
                break;
            }
        }
        new d().a = this.f.e;
        this.f.b = null;
        b.d(this.f);
    }
}
