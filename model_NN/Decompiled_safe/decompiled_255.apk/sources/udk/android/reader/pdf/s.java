package udk.android.reader.pdf;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class s implements Runnable {
    private /* synthetic */ a a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ EditText c;

    s(a aVar, Context context, EditText editText) {
        this.a = aVar;
        this.b = context;
        this.c = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.getSystemService("input_method")).showSoftInput(this.c, 1);
    }
}
