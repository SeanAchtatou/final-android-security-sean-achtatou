package udk.android.reader.c.a;

import android.graphics.Paint;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class b {
    private String a;
    private List b;
    private float c;

    public b(String str, Paint paint, float f, Paint.Align align) {
        this.a = str;
        this.c = paint.getTextSize();
        a(paint, f, align);
    }

    private void a(Paint paint, float f, Paint.Align align) {
        int i;
        this.b = new ArrayList();
        if (this.a != null) {
            int length = this.a.length();
            int i2 = 0;
            int i3 = -1;
            while (i2 < length) {
                char charAt = this.a.charAt(i2);
                if (charAt == 10 ? true : charAt == 13 && i2 + 1 < this.a.length() && this.a.charAt(i2 + 1) != 10) {
                    if (i3 >= 0) {
                        this.b.add(new c(this.a, paint, f, align, i3, i2));
                    } else {
                        this.b.add(new c(this.a, paint, f, align, i2, i2));
                    }
                    i = -1;
                } else {
                    i = i3 < 0 ? i2 : i3;
                }
                i2++;
                i3 = i;
            }
            if (i3 >= 0) {
                this.b.add(new c(this.a, paint, f, align, i3, length));
            } else {
                this.b.add(new c(this.a, paint, f, align, length, length));
            }
        }
    }

    public final float a(float f) {
        float f2 = 0.0f;
        Iterator it = this.b.iterator();
        while (true) {
            float f3 = f2;
            if (!it.hasNext()) {
                return this.c * f * f3;
            }
            f2 = ((float) ((c) it.next()).a().size()) + f3;
        }
    }

    public final float a(int i, float f) {
        float f2 = (this.c * f) - this.c;
        return (f2 * ((float) i)) + (this.c * ((float) (i + 1)));
    }

    public final List a() {
        ArrayList arrayList = new ArrayList();
        for (c a2 : this.b) {
            arrayList.addAll(a2.a());
        }
        return arrayList;
    }

    public final String b() {
        return this.a;
    }
}
