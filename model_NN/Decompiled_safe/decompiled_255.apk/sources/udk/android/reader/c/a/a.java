package udk.android.reader.c.a;

public final class a {
    private String a;
    private int b;
    private int c;
    private float d;

    public a(String str, int i, int i2, float f) {
        this.a = str;
        this.b = i;
        this.c = i2;
        this.d = f;
    }

    public final int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a(float f) {
        this.d += f;
    }

    public final int b() {
        return this.c;
    }

    public final float c() {
        return this.d;
    }
}
