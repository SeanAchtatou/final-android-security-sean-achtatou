package udk.android.reader.c.a;

import android.graphics.Paint;
import com.unidocs.commonlib.util.b;
import com.unidocs.commonlib.util.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;

public final class d {
    private static boolean a = true;
    private String b;
    private int c;
    private int d;
    private List e;
    private float f;
    private boolean g = false;

    public d(String str, Paint paint, float f2, int i, int i2) {
        this.b = str;
        this.f = f2;
        this.c = i;
        this.d = i2;
        a(paint);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.String):boolean
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher */
    private void a(Paint paint) {
        this.e = new ArrayList();
        int i = this.c;
        float f2 = 0.0f;
        if (a) {
            int i2 = i;
            int i3 = this.c;
            while (i3 < this.d) {
                int i4 = i3 + 1;
                int i5 = i3 + 1;
                if (i2 != i3) {
                    float[] fArr = new float[(i3 - i2)];
                    paint.getTextWidths(this.b, i2, i3, fArr);
                    for (float f3 : fArr) {
                        f2 += f3;
                    }
                }
                this.e.add(new a(this.b, i3, i4, f2));
                i2 = i3;
                i3 = i5;
            }
            return;
        }
        Matcher a2 = h.a("[^\\s]+", (CharSequence) this.b);
        a2.region(this.c, this.d);
        int i6 = i;
        float f4 = 0.0f;
        while (a2.find()) {
            int start = a2.start();
            int end = a2.end();
            if (i6 != start) {
                float[] fArr2 = new float[(start - i6)];
                paint.getTextWidths(this.b, i6, start, fArr2);
                float f5 = f4;
                for (float f6 : fArr2) {
                    f5 += f6;
                }
                f4 = f5;
            }
            this.e.add(new a(this.b, start, end, f4));
            i6 = start;
        }
        h.a(a2);
    }

    public final int a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final void a(Paint.Align align, Paint paint) {
        if (!this.g && align != Paint.Align.LEFT && !b.b((Collection) this.e)) {
            a aVar = (a) this.e.get(this.e.size() - 1);
            float[] fArr = new float[(aVar.b() - aVar.a())];
            paint.getTextWidths(this.b, aVar.a(), aVar.b(), fArr);
            float c2 = aVar.c();
            for (float f2 : fArr) {
                c2 += f2;
            }
            float f3 = align == Paint.Align.RIGHT ? this.f - c2 : align == Paint.Align.CENTER ? (this.f - c2) / 2.0f : 0.0f;
            for (a a2 : this.e) {
                a2.a(f3);
            }
            this.g = true;
        }
    }

    public final int b() {
        return this.d;
    }

    public final List c() {
        return this.e;
    }
}
