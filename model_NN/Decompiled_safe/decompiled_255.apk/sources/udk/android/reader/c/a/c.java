package udk.android.reader.c.a;

import android.graphics.Paint;
import java.util.ArrayList;
import java.util.List;

public final class c {
    private String a;
    private int b;
    private int c;
    private List d;

    public c(String str, Paint paint, float f, Paint.Align align, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
        a(paint, f, align);
    }

    private void a(Paint paint, float f, Paint.Align align) {
        int breakText;
        boolean z;
        this.d = new ArrayList();
        if (this.b == this.c) {
            this.d.add(new d(this.a, paint, f, this.b, this.c));
            return;
        }
        int i = this.b;
        while (i < this.c && (breakText = paint.breakText(this.a, i, this.c, true, f, null)) > 0) {
            int i2 = i + breakText;
            if (i2 < this.c) {
                int i3 = i2;
                while (true) {
                    if (i3 >= i) {
                        if (i3 == i) {
                            this.d.add(new d(this.a, paint, f, i, i2));
                            i = i2;
                        } else {
                            char charAt = this.a.charAt(i3 - 1);
                            char charAt2 = this.a.charAt(i3);
                            if (charAt != ' ' && charAt2 != ' ') {
                                switch (charAt2) {
                                    case '\"':
                                    case '\'':
                                    case ')':
                                    case ',':
                                    case '.':
                                    case ':':
                                    case ';':
                                    case '>':
                                    case ']':
                                    case '}':
                                        z = false;
                                        break;
                                    case '(':
                                    case '<':
                                    case '[':
                                    case '{':
                                        z = true;
                                        break;
                                    default:
                                        switch (charAt) {
                                            case '\"':
                                            case '\'':
                                            case '(':
                                            case '<':
                                            case '[':
                                            case '{':
                                                z = false;
                                                break;
                                            case ')':
                                            case '>':
                                            case ']':
                                            case '}':
                                                z = true;
                                                break;
                                            default:
                                                if ((charAt & 65280) != 0 || (65280 & charAt2) != 0) {
                                                    z = true;
                                                    break;
                                                } else {
                                                    z = false;
                                                    break;
                                                }
                                                break;
                                        }
                                }
                            } else {
                                z = true;
                            }
                            if (z) {
                                this.d.add(new d(this.a, paint, f, i, i3));
                                i = i3;
                            } else {
                                i3--;
                            }
                        }
                    }
                }
            } else {
                this.d.add(new d(this.a, paint, f, i, i2));
                i = i2;
            }
        }
        for (d a2 : this.d) {
            a2.a(align, paint);
        }
    }

    public final List a() {
        return this.d;
    }
}
