package udk.android.reader;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import udk.android.reader.b.a;
import udk.android.reader.b.e;
import udk.android.reader.pdf.av;
import udk.android.reader.view.pdf.PDFView;

final class bt implements DialogInterface.OnClickListener {
    final /* synthetic */ ap a;
    private final /* synthetic */ String[] b;

    bt(ap apVar, String[] strArr) {
        this.a = apVar;
        this.b = strArr;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = this.b[i];
        if (str.equals(this.a.a.getString(C0000R.string.f1))) {
            this.a.a.g.a(true);
            this.a.a.d.a(PDFView.ViewMode.TEXTREFLOW);
        } else if (str.equals(this.a.a.getString(C0000R.string.f2))) {
            this.a.a.d.a(PDFView.ViewMode.THUMBNAIL);
        } else if (str.equals(this.a.a.getString(C0000R.string.f0))) {
            this.a.a.d.a(PDFView.ViewMode.PDF);
        } else if (str.equals(this.a.a.getString(C0000R.string.f3))) {
            this.a.a.g.b();
        } else if (str.equals(this.a.a.getString(C0000R.string.f4))) {
            this.a.a.d.I();
        } else if (str.equals(this.a.a.getString(C0000R.string.f5))) {
            this.a.a.d.a(false);
            e.a(this.a.a, this.a.a.getString(C0000R.string.conf_nightmode_activate), Boolean.valueOf(a.ar));
        } else if (str.equals(this.a.a.getString(C0000R.string.f6))) {
            this.a.a.d.a(true);
            e.a(this.a.a, this.a.a.getString(C0000R.string.conf_nightmode_activate), Boolean.valueOf(a.ar));
        } else if (str.equals(this.a.a.getString(C0000R.string.f8))) {
            this.a.a.d.U().e();
        } else if (str.equals(this.a.a.getString(C0000R.string.f7))) {
            this.a.a.d.U().a();
        } else if (str.equals(this.a.a.getString(C0000R.string.f16))) {
            av.a().a(this.a.a, this.a.a.getResources().getDrawable(C0000R.drawable.line), this.a.a.getString(C0000R.string.f16), this.a.a.getString(C0000R.string.f54), this.a.a.getString(C0000R.string.f168));
        } else if (str.equals(this.a.a.getString(C0000R.string.f17))) {
            this.a.a.startActivity(new Intent(this.a.a, PDFReaderConfigurationActivity.class));
        } else if (str.equals(this.a.a.getString(C0000R.string.about))) {
            this.a.a.startActivity(new Intent(this.a.a, AppInfomationActivity.class));
        } else if (str.equals(this.a.a.getString(C0000R.string.f18))) {
            String[] strArr = {this.a.a.getString(C0000R.string.f20), this.a.a.getString(C0000R.string.f21), this.a.a.getString(C0000R.string.f22)};
            int L = this.a.a.d.L();
            this.a.a.n = new AlertDialog.Builder(this.a.a).setTitle(this.a.a.getString(C0000R.string.f19)).setSingleChoiceItems(strArr, L, new bs(this, L)).show();
        } else if (str.equals(this.a.a.getString(C0000R.string.f9))) {
            this.a.a.d.a(this.a.a.getString(C0000R.string.f49), this.a.a.getString(C0000R.string.f78), this.a.a.getString(C0000R.string.f79), this.a.a.getString(C0000R.string.f50), this.a.a.getString(C0000R.string.f80), this.a.a.getString(C0000R.string.f55), this.a.a.getString(C0000R.string.f81), this.a.a.getString(C0000R.string.f171), this.a.a.getString(C0000R.string.f172));
        } else if (str.equals(this.a.a.getString(C0000R.string.f10))) {
            this.a.a.d.N();
        } else if (str.equals(this.a.a.getString(C0000R.string.f14))) {
            if (this.a.a.d.F() != PDFView.ViewMode.PDF) {
                this.a.a.d.a(PDFView.ViewMode.PDF);
            }
            this.a.a.d.b(this.a.a.getString(C0000R.string.f14), this.a.a.getString(C0000R.string.f50));
        } else if (str.equals(this.a.a.getString(C0000R.string.f15))) {
            if (this.a.a.d.F() != PDFView.ViewMode.PDF) {
                this.a.a.d.a(PDFView.ViewMode.PDF);
            }
            this.a.a.d.a(this.a.a.getString(C0000R.string.f15), this.a.a.getString(C0000R.string.f50));
        } else if (str.equals(this.a.a.getString(C0000R.string.f12))) {
            this.a.a.d.a(this.a.a.getString(C0000R.string.f164_), this.a.a.getString(C0000R.string.f50), this.a.a.getString(C0000R.string.f53), this.a.a.getString(C0000R.string.f165x), "#interval#", this.a.a.getString(C0000R.string.f166));
        } else if (str.equals(this.a.a.getString(C0000R.string.f13))) {
            this.a.a.d.k();
        }
    }
}
