package udk.android.reader;

import android.app.Activity;
import android.content.DialogInterface;
import java.io.File;
import udk.android.a.b;

final class k implements DialogInterface.OnClickListener {
    private /* synthetic */ n a;
    private final /* synthetic */ b b;

    k(n nVar, b bVar) {
        this.a = nVar;
        this.b = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.contents.b.a(android.app.Activity, java.io.File[]):void
     arg types: [udk.android.reader.ApplicationActivity, java.io.File[]]
     candidates:
      udk.android.reader.contents.b.a(udk.android.reader.contents.b, java.util.List):void
      udk.android.reader.contents.b.a(udk.android.reader.contents.b, boolean):void
      udk.android.reader.contents.b.a(android.content.Context, java.io.File):void
      udk.android.reader.contents.b.a(android.content.Context, java.io.File[]):void
      udk.android.reader.contents.b.a(android.app.Activity, java.io.File[]):void */
    public final void onClick(DialogInterface dialogInterface, int i) {
        File file = new File(this.b.a());
        if (file.exists()) {
            this.a.a.b = file;
            udk.android.reader.contents.b.a().a((Activity) this.a.a, new File[]{file});
        }
    }
}
