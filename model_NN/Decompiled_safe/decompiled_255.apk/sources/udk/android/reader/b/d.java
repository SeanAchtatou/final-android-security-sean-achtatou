package udk.android.reader.b;

public final class d {
    public static final int a = 1073741824;
    public static String b = "conf_annotation_author";
    public static String c = "conf_annotation_color_highlight";
    public static String d = "conf_annotation_color_underline";
    public static String e = "conf_annotation_color_strikeout";
    public static String f = "conf_annotation_color_note";
    public static String g = "conf_annotation_color_freehand";
    public static String h = "conf_annotation_borderwidth_freehand";
    public static String i = "conf_annotation_color_figure";
    public static String j = "conf_annotation_color_figure_inner_exists";
    public static String k = "conf_annotation_color_figure_inner";
    public static String l = "conf_annotation_borderwidth_figure";
    public static String m = "conf_annotation_color_textbox";
    public static String n = "conf_annotation_color_textbox_inner_exists";
    public static String o = "conf_annotation_color_textbox_inner";
    public static String p = "conf_annotation_borderwidth_textbox";
    public static String q = "conf_annotation_fontsize_textbox";
    public static String r = "conf_annotation_note_name";
    private static int s;

    static {
        s = 1073741823;
        s = 1073741824;
    }
}
