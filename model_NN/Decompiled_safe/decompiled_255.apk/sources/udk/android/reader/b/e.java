package udk.android.reader.b;

import android.content.Context;
import android.os.Environment;
import com.unidocs.commonlib.util.a;
import java.io.File;
import udk.android.reader.C0000R;
import udk.android.reader.view.contents.web.g;

public final class e {
    public static boolean A = true;
    public static int B = 4;
    public static boolean C = true;
    public static boolean D = true;
    public static boolean E = false;
    private static a F;
    public static boolean a = true;
    public static boolean b = false;
    public static long c = 259200000;
    public static String d = "none";
    public static boolean e = true;
    public static boolean f = true;
    public static Class g = g.class;
    public static boolean h = true;
    public static boolean i = true;
    public static boolean j = true;
    public static boolean k = true;
    public static int l = 0;
    public static boolean m = false;
    public static boolean n = true;
    public static boolean o = true;
    public static boolean p = true;
    public static boolean q = true;
    public static boolean r = true;
    public static boolean s = true;
    public static boolean t = false;
    public static boolean u = true;
    public static boolean v = true;
    public static boolean w = false;
    public static boolean x = false;
    public static boolean y = true;
    public static boolean z = false;

    public static File a() {
        return Environment.getExternalStorageDirectory();
    }

    public static File a(Context context) {
        File a2 = a.a(context);
        if (!a2.exists() || !a2.isDirectory()) {
            a2.mkdirs();
        }
        return new File(String.valueOf(a2.getAbsolutePath()) + File.separator + "fsindex");
    }

    public static void a(Context context, String str, Object obj) {
        a b2 = b(context);
        if (b2 != null) {
            try {
                b2.a(str, obj);
                b2.a("UTF-8");
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
    }

    public static a b(Context context) {
        if (F == null) {
            try {
                F = new a(a.a(context) + "/settings");
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
        return F;
    }

    public static void c(Context context) {
        a b2 = b(context);
        if (b2 != null) {
            E = b2.a(context.getString(C0000R.string.conf_dpad_invert), E);
            D = b2.a(context.getString(C0000R.string.conf_link_needconfirm), D);
            C = b2.a(context.getString(C0000R.string.conf_screen_wakelock), C);
            B = b2.a(context.getString(C0000R.string.conf_screen_orientation), B);
            l = b2.a(context.getString(C0000R.string.conf_key_action_volume), l);
            a.R = b2.a(context.getString(C0000R.string.conf_pagescrolling_type), a.R);
            a.T = b2.a(context.getString(C0000R.string.conf_pagescrolling_sensitivity), a.T);
            a.U = b2.a(context.getString(C0000R.string.conf_pagescrolling_disable_with_columnfitting), a.U);
            a.Z = b2.a(context.getString(C0000R.string.conf_basic_zoom_ratio), a.Z);
            a.aa = b2.a(context.getString(C0000R.string.conf_pinchzoom_movable), a.aa);
            a.aq = b2.a(context.getString(C0000R.string.conf_pagefliping_speed), a.aq);
            a.ar = b2.a(context.getString(C0000R.string.conf_nightmode_activate), a.ar);
            a.as = b2.a(context.getString(C0000R.string.conf_nightmode_line_height_percent), a.as);
            a.at = b2.a(context.getString(C0000R.string.conf_nightmode_fontcolor_r), a.at);
            a.au = b2.a(context.getString(C0000R.string.conf_nightmode_fontcolor_g), a.au);
            a.av = b2.a(context.getString(C0000R.string.conf_nightmode_fontcolor_b), a.av);
            a.aw = b2.a(context.getString(C0000R.string.conf_nightmode_bgcolor_r), a.aw);
            a.ax = b2.a(context.getString(C0000R.string.conf_nightmode_bgcolor_g), a.ax);
            a.ay = b2.a(context.getString(C0000R.string.conf_nightmode_bgcolor_b), a.ay);
            a.aM = b2.a(context.getString(C0000R.string.conf_default_reading_direction), a.aM);
            a.aL = b2.a(context.getString(C0000R.string.conf_notificationbar_readingtime_enable), a.aL);
            A = b2.a(context.getString(C0000R.string.conf_tts_detect_headset_plug), A);
            j = b2.a(context.getString(C0000R.string.conf_menu_opentime_enable), j);
            k = b2.a(context.getString(C0000R.string.conf_menu_auto_disable), k);
            d = b2.a(context.getString(C0000R.string.conf_webbookcase_homepage), d);
        }
    }
}
