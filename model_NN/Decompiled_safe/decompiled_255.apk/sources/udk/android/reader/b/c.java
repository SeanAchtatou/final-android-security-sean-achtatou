package udk.android.reader.b;

import android.util.Log;

public final class c {
    private static String a = "YLOGEX";

    public static void a(Object obj) {
        if (a.b) {
            Log.d(a, String.valueOf(Thread.currentThread().getName()) + obj.toString());
        }
    }

    public static void a(Object obj, Throwable th) {
        Log.e(a, obj == null ? "" : obj.toString(), th);
    }

    public static void a(Throwable th) {
        a(th.getMessage(), th);
    }
}
