package udk.android.reader.b;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.view.Display;
import java.io.File;
import udk.android.b.m;
import udk.android.reader.pdf.y;

public final class a {
    public static int A = 35071;
    public static float B = 3.0f;
    public static int C = -30720;
    public static boolean D = false;
    public static int E = 35071;
    public static float F = 3.0f;
    public static float G = 12.0f;
    public static int H = -14352640;
    public static int I = -13421773;
    public static int J = -13421773;
    public static int K = 200;
    public static boolean L = false;
    public static int M = 866994954;
    public static int N = 856006011;
    public static float O = 2.0f;
    public static boolean P = true;
    public static boolean Q = true;
    public static int R = 1;
    public static float S = 5.0f;
    public static float T = 0.3f;
    public static boolean U = true;
    public static boolean V = true;
    public static boolean W = true;
    public static boolean X = false;
    public static boolean Y = true;
    public static int Z = 3;
    public static float a;
    public static float aA = 10.0f;
    public static boolean aB = true;
    public static int aC = 25;
    public static int aD = -16777216;
    public static int aE = -13732253;
    public static int aF = 14;
    public static int aG = -1;
    public static int aH = -536450;
    public static int aI = -1;
    public static int aJ = -16777216;
    public static boolean aK = false;
    public static boolean aL = false;
    public static int aM = 9;
    public static boolean aN = false;
    public static int aO = 0;
    public static int aP = 0;
    public static int aQ = 0;
    public static boolean aR = false;
    public static int aS = -2000962816;
    public static int aT = -1996523776;
    public static int aU = -1;
    private static boolean aV = true;
    private static boolean aW = true;
    private static int aX = 4;
    private static int aY = 12;
    private static int aZ = 60;
    public static boolean aa = false;
    public static float ab = 0.05f;
    public static float ac = 8.0f;
    public static float ad = 1.0f;
    public static float ae = 2.0f;
    public static boolean af = true;
    public static boolean ag = false;
    public static boolean ah = false;
    public static boolean ai = false;
    public static int aj = 3;
    public static float ak = 1.0f;
    public static boolean al = true;
    public static boolean am = true;
    public static boolean an = true;
    public static boolean ao = true;
    public static float ap = 0.2f;
    public static float aq = 1.5f;
    public static boolean ar = false;
    public static int as = 150;
    public static int at = 255;
    public static int au = 255;
    public static int av = 255;
    public static int aw = 0;
    public static int ax = 0;
    public static int ay = 0;
    public static float az = 0.2f;
    public static boolean b = false;
    private static int ba = 5;
    private static int bb = -1;
    private static int bc = 60;
    private static int bd = 5;
    private static int be = -1;
    private static int bf = 60;
    private static int bg = 5;
    private static int bh = -1;
    private static boolean bi = false;
    private static com.unidocs.commonlib.util.a bj;
    public static File c;
    public static boolean d = true;
    public static boolean e = true;
    public static boolean f = false;
    public static boolean g = false;
    public static boolean h = true;
    public static boolean i = true;
    public static boolean j = true;
    public static boolean k = true;
    public static boolean l = true;
    public static boolean m = false;
    public static boolean n = false;
    public static boolean o = true;
    public static boolean p = true;
    public static boolean q = false;
    public static boolean r = true;
    public static boolean s = true;
    public static String t = Build.DEVICE;
    public static String u = "Note";
    public static int v = -472214;
    public static int w = -13421773;
    public static float x = 3.0f;
    public static int y = -30720;
    public static boolean z = false;

    public static float a(float f2) {
        return a != 1.0f ? (a * f2) + 0.5f : f2;
    }

    public static int a() {
        if (W) {
            return 200;
        }
        return aX;
    }

    public static File a(Context context) {
        if (c != null) {
            return c;
        }
        return new File(String.valueOf(e ? context.getFilesDir().getAbsolutePath() : Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator + (d ? context.getApplicationContext().getPackageName() : "ezPDFReader"));
    }

    public static String a(y yVar, String str) {
        try {
            return String.valueOf(yVar.e()) + "/" + com.unidocs.commonlib.a.a.a(str, "UTF-8");
        } catch (Exception e2) {
            c.a((Throwable) e2);
            return null;
        }
    }

    public static void a(Context context, String str, Object obj) {
        com.unidocs.commonlib.util.a f2 = f(context);
        if (f2 != null) {
            try {
                f2.a(str, obj);
                f2.a("UTF-8");
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
    }

    public static boolean a(Context context, File file) {
        return file.getAbsolutePath().indexOf(b(context).getAbsolutePath()) >= 0;
    }

    public static int b() {
        if (W) {
            return 4;
        }
        return aY;
    }

    public static File b(Context context) {
        File file = new File(String.valueOf(a(context).getAbsolutePath()) + "/bookdata/library");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static String b(Context context, File file) {
        return a(c(context), file.getAbsolutePath());
    }

    public static int c() {
        if (W) {
            return 0;
        }
        return aZ;
    }

    public static String c(Context context, File file) {
        String b2 = b(context, file);
        if (b2 == null) {
            return null;
        }
        return String.valueOf(b2) + "/cover";
    }

    public static y c(Context context) {
        y yVar = new y();
        yVar.a(context);
        Display e2 = m.e(context);
        yVar.a(e2.getWidth());
        yVar.b(e2.getHeight());
        File a2 = a(context);
        yVar.a(String.valueOf(a2.getAbsolutePath()) + "/temp");
        yVar.g(String.valueOf(yVar.a()) + "/m");
        yVar.d(String.valueOf(a2.getAbsolutePath()) + "/resource");
        yVar.e(String.valueOf(a2.getAbsolutePath()) + "/bookdata/meta");
        yVar.b(String.valueOf(yVar.d()) + "/Fonts");
        yVar.c(String.valueOf(a2.getAbsolutePath()) + "/configuration");
        yVar.f(String.valueOf(a2.getAbsolutePath()) + "/version");
        return yVar;
    }

    public static int d() {
        if (W) {
            return 0;
        }
        return ba;
    }

    public static final String d(Context context) {
        return String.valueOf(a(context).getAbsolutePath()) + "/bookdata/openhistory";
    }

    public static String d(Context context, File file) {
        String b2 = b(context, file);
        if (b2 == null) {
            return null;
        }
        return String.valueOf(b2) + "/bookmark";
    }

    public static int e() {
        if (W) {
            return 0;
        }
        return bb;
    }

    public static void e(Context context) {
        com.unidocs.commonlib.util.a f2 = f(context);
        if (f2 != null) {
            t = f2.a(d.b, t);
            u = f2.a(d.r, u);
            H = f2.a(d.c, H);
            I = f2.a(d.d, I);
            J = f2.a(d.e, J);
            v = f2.a(d.f, v);
            w = f2.a(d.g, w);
            x = f2.a(d.h, x);
            y = f2.a(d.i, y);
            z = f2.a(d.j, z);
            A = f2.a(d.k, A);
            B = f2.a(d.l, B);
            C = f2.a(d.m, C);
            D = f2.a(d.n, D);
            E = f2.a(d.o, E);
            F = f2.a(d.p, F);
            G = f2.a(d.q, G);
        }
    }

    public static int f() {
        if (W) {
            return 0;
        }
        return bc;
    }

    private static com.unidocs.commonlib.util.a f(Context context) {
        if (bj == null) {
            try {
                bj = new com.unidocs.commonlib.util.a(a(context) + "/settings.view");
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
        return bj;
    }

    public static int g() {
        if (W) {
            return 0;
        }
        return bd;
    }

    public static int h() {
        if (W) {
            return 0;
        }
        return be;
    }

    public static int i() {
        if (W) {
            return 0;
        }
        return bf;
    }

    public static int j() {
        if (W) {
            return 0;
        }
        return bg;
    }

    public static int k() {
        if (W) {
            return 0;
        }
        return bh;
    }
}
