package udk.android.reader;

import android.preference.PreferenceManager;
import udk.android.a.e;

final class c implements e {
    private /* synthetic */ PDFReaderConfigurationActivity a;

    c(PDFReaderConfigurationActivity pDFReaderConfigurationActivity) {
        this.a = pDFReaderConfigurationActivity;
    }

    public final void a(int i) {
        PreferenceManager.getDefaultSharedPreferences(this.a).edit().putInt(this.a.getString(C0000R.string.conf_nightmode_fontcolor), i).commit();
    }
}
