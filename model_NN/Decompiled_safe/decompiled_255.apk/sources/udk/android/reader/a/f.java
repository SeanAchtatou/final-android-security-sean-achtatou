package udk.android.reader.a;

import android.app.Activity;
import android.os.PowerManager;

public final class f {
    private Activity a;
    private PowerManager.WakeLock b;

    public f(Activity activity) {
        this.a = activity;
    }

    public final void a() {
        if (this.b == null) {
            this.b = ((PowerManager) this.a.getSystemService("power")).newWakeLock(10, this.a.getClass().getName());
            if (this.b != null) {
                this.b.acquire();
            }
        }
    }

    public final void b() {
        if (this.b != null) {
            if (this.b.isHeld()) {
                this.b.release();
            }
            this.b = null;
        }
    }
}
