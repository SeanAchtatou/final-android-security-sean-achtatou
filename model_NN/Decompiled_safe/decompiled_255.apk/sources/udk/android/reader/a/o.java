package udk.android.reader.a;

import com.android.vending.licensing.Policy;
import udk.android.reader.b.c;

public final class o implements Policy {
    private Policy.LicenseResponse a = Policy.LicenseResponse.LICENSED;

    public final void a(Policy.LicenseResponse licenseResponse) {
        this.a = licenseResponse;
    }

    public final boolean a() {
        c.a("## LICENSE CHEK : " + this.a);
        return this.a != Policy.LicenseResponse.NOT_LICENSED ? true : true;
    }
}
