package udk.android.reader.a;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

final class g implements DialogInterface.OnClickListener {
    private /* synthetic */ n a;
    private final /* synthetic */ Activity b;

    g(n nVar, Activity activity) {
        this.a = nVar;
        this.b = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=udk.android.reader")));
        this.b.finish();
    }
}
