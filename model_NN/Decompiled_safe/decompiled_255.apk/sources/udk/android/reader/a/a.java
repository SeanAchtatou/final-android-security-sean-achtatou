package udk.android.reader.a;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import com.unidocs.commonlib.util.b;
import com.unidocs.commonlib.util.h;
import java.io.File;
import udk.android.b.m;
import udk.android.reader.C0000R;
import udk.android.reader.PDFReaderActivity;
import udk.android.reader.b.c;
import udk.android.reader.b.e;

public final class a {
    public static void a(Activity activity) {
        String packageName = activity.getPackageName();
        if (packageName.startsWith("udk.android.reader") || packageName.startsWith("udk.android.csp")) {
            udk.android.reader.b.a.e = false;
        } else {
            udk.android.reader.b.a.e = true;
        }
        if (packageName.startsWith("udk.android.reader")) {
            udk.android.reader.b.a.d = false;
        } else {
            udk.android.reader.b.a.d = true;
        }
        udk.android.reader.b.a.b = false;
        Rect g = m.g(activity);
        if (g.height() * g.width() >= 614400) {
            activity.getResources().getDisplayMetrics().density = 1.5f;
        } else if (m.a()) {
            SharedPreferences sharedPreferences = activity.getSharedPreferences("pdr", 0);
            String string = sharedPreferences.getString("pdr", null);
            if (!b.a(string) || !new File(string).exists()) {
                File[] fileArr = {Environment.getExternalStorageDirectory(), new File("/mnt/media"), new File("/media")};
                int length = fileArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    File file = new File(String.valueOf(fileArr[i].getAbsolutePath()) + File.separator + "ezPDFReader");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    if (file.exists()) {
                        udk.android.reader.b.a.c = file;
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("pdr", file.getAbsolutePath());
                        edit.commit();
                        break;
                    }
                    i++;
                }
            } else {
                udk.android.reader.b.a.c = new File(string);
            }
            c.a("## PROGRAM DATA ROOT : " + udk.android.reader.b.a.a(activity));
            activity.getResources().getDisplayMetrics().density = 1.5f;
            udk.android.reader.b.a.aU = -12303292;
            e.z = true;
            e.y = false;
            e.x = true;
            udk.android.reader.b.a.aL = true;
        }
    }

    public static void a(Activity activity, View view, String str) {
        if (!b.b(str)) {
            view.post(new e(activity, str));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Activity activity, String str) {
        Intent intent = new Intent(activity, PDFReaderActivity.class);
        intent.setDataAndType(Uri.parse(str), "application/pdf");
        intent.putExtra("calledFromAppInner", true);
        activity.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Activity activity, String str, String str2) {
        Intent intent = new Intent(activity, PDFReaderActivity.class);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/pdf");
        intent.putExtra("calledFromAppInner", true);
        if (b.a(str2)) {
            intent.putExtra("params", str2);
        }
        activity.startActivity(intent);
    }

    public static void a(Activity activity, String str, boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (str.startsWith("androidExec")) {
            String a = h.a(str, "androidExec\\[(.+?)\\]", 1);
            stringBuffer.append(activity.getString(C0000R.string.f162));
            stringBuffer.append("\n\n");
            stringBuffer.append(b.b(a) ? "" : a.replaceAll("&", "\n"));
        } else {
            stringBuffer.append(activity.getString(C0000R.string.f160));
            stringBuffer.append("\n\n");
            stringBuffer.append(str);
            stringBuffer.append("\n\n");
            stringBuffer.append(activity.getString(C0000R.string.f1613g));
        }
        String stringBuffer2 = stringBuffer.toString();
        c cVar = new c(str, activity);
        boolean z2 = str.startsWith("androidExec") && e.v;
        if (z || z2) {
            activity.runOnUiThread(new b(activity, stringBuffer2, cVar));
        } else {
            cVar.run();
        }
    }

    public static void b(Activity activity, View view, String str) {
        view.post(new d(activity, str));
    }
}
