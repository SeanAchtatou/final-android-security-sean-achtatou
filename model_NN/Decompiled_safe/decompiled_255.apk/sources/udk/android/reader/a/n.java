package udk.android.reader.a;

import android.app.Activity;
import android.app.AlertDialog;

final class n implements Runnable {
    private /* synthetic */ i a;
    private final /* synthetic */ Activity b;

    n(i iVar, Activity activity) {
        this.a = iVar;
        this.b = activity;
    }

    public final void run() {
        new AlertDialog.Builder(this.b).setMessage("You don't have license for this application.\nYou must buy ezPDF Reader from market.").setCancelable(false).setPositiveButton("OK", new g(this, this.b)).show();
    }
}
