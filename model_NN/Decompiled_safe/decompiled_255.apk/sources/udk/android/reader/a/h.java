package udk.android.reader.a;

import android.app.Activity;
import android.content.DialogInterface;
import udk.android.b.j;
import udk.android.reader.C0000R;

final class h implements DialogInterface.OnClickListener {
    private /* synthetic */ d a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ String c;

    h(d dVar, Activity activity, String str) {
        this.a = dVar;
        this.b = activity;
        this.c = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        j.a(this.b, this.c, this.b.getString(C0000R.string.f158URI));
    }
}
