package udk.android.reader.a;

import android.app.Activity;
import android.view.View;
import com.android.vending.licensing.LicenseCheckerCallback;
import udk.android.reader.b.c;

final class i implements LicenseCheckerCallback {
    private /* synthetic */ l a;
    private final /* synthetic */ View b;
    private final /* synthetic */ Activity c;

    i(l lVar, View view, Activity activity) {
        this.a = lVar;
        this.b = view;
        this.c = activity;
    }

    public final void a() {
        c.a("## LICENSE CHECK RESULT SUCCESS");
        j.a = true;
    }

    public final void a(LicenseCheckerCallback.ApplicationErrorCode applicationErrorCode) {
        c.a("## LICENSE CHECK RESULT ERROR : " + applicationErrorCode);
    }

    public final void b() {
        c.a("## LICENSE CHECK RESULT FAILURE");
        this.b.post(new n(this, this.c));
    }
}
