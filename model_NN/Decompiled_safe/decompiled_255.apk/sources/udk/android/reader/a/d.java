package udk.android.reader.a;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import udk.android.reader.C0000R;

final class d implements Runnable {
    private final /* synthetic */ Activity a;
    private final /* synthetic */ String b;

    d(Activity activity, String str) {
        this.a = activity;
        this.b = str;
    }

    public final void run() {
        new AlertDialog.Builder(this.a).setTitle((int) C0000R.string.f49).setMessage(String.valueOf(this.a.getString(C0000R.string.f160)) + "\n\n" + this.b + "\n\n" + this.a.getString(C0000R.string.f1613g)).setPositiveButton((int) C0000R.string.f50, new h(this, this.a, this.b)).setNegativeButton((int) C0000R.string.f53, (DialogInterface.OnClickListener) null).show();
    }
}
