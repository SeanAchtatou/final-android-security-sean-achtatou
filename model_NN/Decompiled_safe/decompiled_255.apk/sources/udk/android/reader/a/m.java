package udk.android.reader.a;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

final class m implements DialogInterface.OnClickListener {
    private /* synthetic */ e a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Activity c;

    m(e eVar, String str, Activity activity) {
        this.a = eVar;
        this.b = str;
        this.c = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.WEB_SEARCH");
        intent.putExtra("query", this.b);
        this.c.startActivity(intent);
    }
}
