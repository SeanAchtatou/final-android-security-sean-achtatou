package udk.android.reader.a;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import com.unidocs.commonlib.util.b;
import com.unidocs.commonlib.util.h;
import udk.android.b.j;
import udk.android.reader.C0000R;

final class c implements Runnable {
    private final /* synthetic */ String a;
    private final /* synthetic */ Activity b;

    c(String str, Activity activity) {
        this.a = str;
        this.b = activity;
    }

    public final void run() {
        if (this.a.startsWith("androidExec")) {
            Activity activity = this.b;
            String a2 = h.a(this.a, "androidExec\\[(.+?)\\]", 1);
            if (!b.b(a2)) {
                String b2 = h.b(a2, "action");
                String b3 = h.b(a2, "package");
                String b4 = h.b(a2, "class");
                if (!b.b(new String[]{b2, b3, b4})) {
                    try {
                        Intent intent = new Intent(b2);
                        intent.setComponent(new ComponentName(b3, b4));
                        activity.startActivity(intent);
                    } catch (Throwable th) {
                        udk.android.reader.b.c.a(th);
                    }
                }
            }
        } else {
            j.a(this.b, this.a, this.b.getString(C0000R.string.f158URI));
        }
    }
}
