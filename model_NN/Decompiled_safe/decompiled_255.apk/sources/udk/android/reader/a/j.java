package udk.android.reader.a;

import android.app.Activity;
import android.view.View;
import udk.android.reader.b.c;

public final class j {
    /* access modifiers changed from: private */
    public static boolean a;

    public static void a(Activity activity, View view) {
        if (a) {
            c.a("## LICENSE CHECK PASS WITH CACHED RESULT");
            return;
        }
        l lVar = new l(view, activity);
        lVar.setDaemon(true);
        lVar.start();
    }
}
