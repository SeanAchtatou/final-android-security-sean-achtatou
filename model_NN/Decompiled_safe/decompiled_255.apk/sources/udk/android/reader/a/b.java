package udk.android.reader.a;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import udk.android.reader.C0000R;

final class b implements Runnable {
    private final /* synthetic */ Activity a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Runnable c;

    b(Activity activity, String str, Runnable runnable) {
        this.a = activity;
        this.b = str;
        this.c = runnable;
    }

    public final void run() {
        new AlertDialog.Builder(this.a).setTitle((int) C0000R.string.f49).setMessage(this.b).setPositiveButton((int) C0000R.string.f50, new k(this, this.c)).setNegativeButton((int) C0000R.string.f53, (DialogInterface.OnClickListener) null).show();
    }
}
