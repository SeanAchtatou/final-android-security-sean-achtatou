package udk.android.reader.view.pdf;

import android.view.MotionEvent;
import udk.android.reader.pdf.annotation.q;
import udk.android.reader.pdf.annotation.s;

final class ey implements ae {
    final /* synthetic */ PDFView a;
    private final /* synthetic */ q b;
    private final /* synthetic */ s c;
    private final /* synthetic */ Runnable d;

    ey(PDFView pDFView, q qVar, s sVar, Runnable runnable) {
        this.a = pDFView;
        this.b = qVar;
        this.c = sVar;
        this.d = runnable;
    }

    public final void a(MotionEvent motionEvent) {
        this.b.b(motionEvent.getX() - this.a.k.a, motionEvent.getY() - this.a.k.b, this.a.k.n());
        this.a.k.p();
    }

    public final void b(MotionEvent motionEvent) {
        this.b.e(motionEvent.getX() - this.a.k.a, motionEvent.getY() - this.a.k.b, this.a.k.n());
        this.a.k.p();
    }

    public final void c(MotionEvent motionEvent) {
        this.b.d(motionEvent.getX() - this.a.k.a, motionEvent.getY() - this.a.k.b, this.a.k.n());
        this.c.a(this.b);
        if (this.d != null) {
            this.d.run();
        }
        this.a.postDelayed(new p(this), 100);
    }
}
