package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.AdapterView;

final class m implements AdapterView.OnItemClickListener {
    final /* synthetic */ BookmarkNavigationView a;

    m(BookmarkNavigationView bookmarkNavigationView) {
        this.a = bookmarkNavigationView;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.postDelayed(new d(this, i), 300);
    }
}
