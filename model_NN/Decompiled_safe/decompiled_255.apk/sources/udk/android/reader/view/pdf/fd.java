package udk.android.reader.view.pdf;

import android.content.DialogInterface;
import java.io.File;

final class fd implements DialogInterface.OnClickListener {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ Runnable b;

    fd(PDFView pDFView, Runnable runnable) {
        this.a = pDFView;
        this.b = runnable;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String y = this.a.b.y();
        String replaceAll = y.toLowerCase().endsWith(".pdf") ? y.replaceAll("(?i)\\.pdf$", ".annotated.pdf") : String.valueOf(y) + ".annotated.pdf";
        if (new File(replaceAll).exists()) {
            String str = replaceAll;
            int i2 = 1;
            while (true) {
                str = str.replaceAll("\\.annotated.*\\.pdf", ".annotated_" + i2 + ".pdf");
                if (!new File(str).exists()) {
                    break;
                }
                i2++;
            }
            replaceAll = str;
        }
        this.a.a(replaceAll, this.b);
    }
}
