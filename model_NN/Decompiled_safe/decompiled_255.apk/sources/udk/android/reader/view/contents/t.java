package udk.android.reader.view.contents;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import java.io.File;
import udk.android.reader.C0000R;
import udk.android.reader.a.a;
import udk.android.reader.pdf.ae;

final class t implements View.OnClickListener {
    private /* synthetic */ u a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Activity c;

    t(u uVar, String str, Activity activity) {
        this.a = uVar;
        this.b = str;
        this.c = activity;
    }

    public final void onClick(View view) {
        if (!new File(this.b).exists()) {
            new AlertDialog.Builder(this.c).setTitle((int) C0000R.string.f49).setMessage((int) C0000R.string.f178_).setPositiveButton((int) C0000R.string.f54, (DialogInterface.OnClickListener) null).show();
            ae.a().a(this.a.b, this.b);
            return;
        }
        a.a(this.c, this.b, (String) null);
    }
}
