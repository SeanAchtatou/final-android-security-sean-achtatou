package udk.android.reader.view.pdf;

import android.content.DialogInterface;

final class fc implements DialogInterface.OnClickListener {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ Runnable b;

    fc(PDFView pDFView, Runnable runnable) {
        this.a = pDFView;
        this.b = runnable;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.run();
    }
}
