package udk.android.reader.view.pdf.menu;

import android.text.Layout;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.widget.TextView;

public final class b extends LinkMovementMethod {
    private static b a;

    public static b a() {
        if (a == null) {
            a = new b();
        }
        return a;
    }

    public final boolean onTouchEvent(TextView textView, Spannable spannable, MotionEvent motionEvent) {
        MenuCommandSpan menuCommandSpan;
        int action = motionEvent.getAction();
        if (action == 1 || action == 0) {
            int x = ((int) motionEvent.getX()) - textView.getTotalPaddingLeft();
            int y = ((int) motionEvent.getY()) - textView.getTotalPaddingTop();
            int scrollX = x + textView.getScrollX();
            Layout layout = textView.getLayout();
            int offsetForHorizontal = layout.getOffsetForHorizontal(layout.getLineForVertical(y + textView.getScrollY()), (float) scrollX);
            int i = offsetForHorizontal < textView.length() / 2 ? 1 : -1;
            int i2 = offsetForHorizontal;
            while (true) {
                if (i2 >= 0 && i2 <= textView.length()) {
                    ClickableSpan[] clickableSpanArr = (ClickableSpan[]) spannable.getSpans(i2, i2, ClickableSpan.class);
                    if (clickableSpanArr != null && clickableSpanArr.length > 0) {
                        menuCommandSpan = (MenuCommandSpan) clickableSpanArr[0];
                        break;
                    }
                    i2 += i;
                } else {
                    menuCommandSpan = null;
                }
            }
            menuCommandSpan = null;
            if (menuCommandSpan != null) {
                if (action == 1) {
                    menuCommandSpan.onClick(textView);
                    textView.postDelayed(new h(this, menuCommandSpan, textView), 200);
                } else if (action == 0) {
                    menuCommandSpan.a = true;
                }
                return true;
            }
        }
        return true;
    }
}
