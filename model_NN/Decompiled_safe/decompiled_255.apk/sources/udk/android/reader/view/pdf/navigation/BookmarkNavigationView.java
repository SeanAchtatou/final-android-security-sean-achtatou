package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.a;
import udk.android.reader.pdf.ac;
import udk.android.reader.pdf.af;
import udk.android.reader.view.pdf.hx;

public class BookmarkNavigationView extends Gallery implements af {
    /* access modifiers changed from: private */
    public i a;
    private a b;
    /* access modifiers changed from: private */
    public NavigationService c;
    /* access modifiers changed from: private */
    public PDF d;
    /* access modifiers changed from: private */
    public int e;

    public BookmarkNavigationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
    }

    public BookmarkNavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        e();
    }

    private void e() {
        setCallbackDuringFling(false);
        this.c = NavigationService.a();
        this.d = PDF.a();
        this.a = new i(getContext());
        setAdapter((SpinnerAdapter) this.a);
        setOnItemClickListener(new m(this));
        setOnItemSelectedListener(new n(this));
        this.b = a.a();
        this.b.a(this);
    }

    public final void a() {
        try {
            if (this.c.n() == NavigationService.b) {
                int firstVisiblePosition = getFirstVisiblePosition();
                float a2 = this.d.a(getContext().getResources().getDimension(C0000R.dimen.navigation_thumbnail_width) - (getContext().getResources().getDimension(C0000R.dimen.navigation_thumbnail_padding) * 2.0f));
                String z = this.d.z();
                int childCount = getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = getChildAt(i);
                    if (childAt != null) {
                        int page = this.a.getItem(firstVisiblePosition + i).getPage();
                        ImageView imageView = (ImageView) childAt.findViewById(C0000R.id.thumbnail);
                        if (imageView.getDrawable() == null) {
                            new o(this, z, childAt, page, a2, imageView).start();
                        }
                    }
                }
            }
        } catch (Throwable th) {
            c.a(th.getMessage(), th);
        }
    }

    public final void a(ac acVar) {
    }

    public final void b() {
    }

    public final void c() {
    }

    public final void d() {
        hx.a().p();
        post(new q(this));
        postDelayed(new s(this), 1000);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        c.a("## DISPOSE BookmarkNavigationView");
        this.b.b(this);
        super.onDetachedFromWindow();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return false;
    }
}
