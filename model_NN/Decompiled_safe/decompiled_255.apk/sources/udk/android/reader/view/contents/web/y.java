package udk.android.reader.view.contents.web;

import android.webkit.WebView;

final class y implements Runnable {
    private /* synthetic */ ab a;
    private final /* synthetic */ WebView b;
    private final /* synthetic */ String c;

    y(ab abVar, WebView webView, String str) {
        this.a = abVar;
        this.b = webView;
        this.c = str;
    }

    public final void run() {
        this.b.loadUrl(this.c);
    }
}
