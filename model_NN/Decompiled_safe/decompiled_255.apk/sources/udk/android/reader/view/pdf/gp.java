package udk.android.reader.view.pdf;

import udk.android.reader.pdf.annotation.p;
import udk.android.reader.pdf.annotation.s;

final class gp implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ int b;
    private final /* synthetic */ int c;

    gp(PDFView pDFView, int i, int i2) {
        this.a = pDFView;
        this.b = i;
        this.c = i2;
    }

    public final void run() {
        s a2 = s.a();
        p a3 = a2.a(this.a.z(), this.a.B(), this.b, this.c);
        if (a3 != null) {
            this.a.a(a2, a3).run();
        }
    }
}
