package udk.android.reader.view.pdf.navigation;

import android.widget.Toast;
import com.unidocs.commonlib.util.b;
import udk.android.reader.pdf.Bookmark;

final class d implements Runnable {
    private /* synthetic */ m a;
    private final /* synthetic */ int b;

    d(m mVar, int i) {
        this.a = mVar;
        this.b = i;
    }

    public final void run() {
        Bookmark a2 = this.a.a.a.getItem(this.b);
        this.a.a.c.a(a2.getPage());
        if (b.a(a2.getDesc())) {
            Toast.makeText(this.a.a.getContext(), a2.getDesc(), 0).show();
        }
    }
}
