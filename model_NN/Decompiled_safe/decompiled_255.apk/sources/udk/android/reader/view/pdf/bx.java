package udk.android.reader.view.pdf;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

final class bx extends Drawable {
    private /* synthetic */ PDFReadingToolbar a;
    private final /* synthetic */ int b;
    private final /* synthetic */ Paint c;
    private final /* synthetic */ Paint d;

    bx(PDFReadingToolbar pDFReadingToolbar, int i, Paint paint, Paint paint2) {
        this.a = pDFReadingToolbar;
        this.b = i;
        this.c = paint;
        this.d = paint2;
    }

    public final void draw(Canvas canvas) {
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) this.a.getWidth(), (float) this.a.getHeight()), (float) this.b, (float) this.b, this.c);
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) this.a.getWidth(), (float) this.a.getHeight()), (float) this.b, (float) this.b, this.d);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
