package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.SeekBar;
import udk.android.reader.view.pdf.navigation.NavigationService;

final class ac implements SeekBar.OnSeekBarChangeListener {
    final /* synthetic */ NavigationService a;
    private boolean b;
    private final /* synthetic */ View c;

    ac(NavigationService navigationService, View view) {
        this.a = navigationService;
        this.c = view;
    }

    public final void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (this.b) {
            this.a.k.a(this.a.A.getProgress(), this.a.A.getMax());
            this.c.postDelayed(new f(this), 100);
        }
    }

    public final void onStartTrackingTouch(SeekBar seekBar) {
        this.a.k.s();
        this.b = true;
    }

    public final void onStopTrackingTouch(SeekBar seekBar) {
        this.b = false;
        this.a.k.t();
        this.a.a(NavigationService.SubMode.THUMBNAIL);
    }
}
