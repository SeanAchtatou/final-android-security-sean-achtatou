package udk.android.reader.view.contents.web;

import android.app.AlertDialog;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public final class w extends WebChromeClient {
    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        new AlertDialog.Builder(webView.getContext()).setMessage(str2).setPositiveButton(17039370, new c(this, jsResult)).setCancelable(false).show();
        return true;
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        return super.onJsBeforeUnload(webView, str, str2, jsResult);
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        new AlertDialog.Builder(webView.getContext()).setMessage(str2).setPositiveButton(17039370, new b(this, jsResult)).setNegativeButton(17039360, new a(this, jsResult)).setCancelable(false).show();
        return true;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
    }

    public final boolean onJsTimeout() {
        return super.onJsTimeout();
    }
}
