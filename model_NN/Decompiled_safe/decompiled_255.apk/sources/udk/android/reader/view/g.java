package udk.android.reader.view;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import udk.android.reader.C0000R;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.view.pdf.PDFView;
import udk.android.reader.view.pdf.co;

public final class g {
    private LinearLayout a;
    /* access modifiers changed from: private */
    public PDFView b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public View d;
    private boolean e;
    private boolean f;
    private a g;
    private int h = -1626865628;

    public g(LinearLayout linearLayout, PDFView pDFView, View view, View view2, a aVar) {
        this.a = linearLayout;
        this.b = pDFView;
        this.c = view;
        this.d = view2;
        this.g = aVar;
        while (linearLayout.findViewById(this.h) != null) {
            this.h++;
        }
    }

    public final void a() {
        int i = 0;
        while (i < this.a.getChildCount()) {
            if (this.a.getChildAt(i).getId() == this.h) {
                this.a.removeViewAt(i);
                i--;
            } else {
                this.a.getChildAt(i).setVisibility(0);
            }
            i++;
        }
    }

    public final void a(boolean z) {
        if (this.g != null) {
            this.g.a(z);
        }
        if (!z) {
            if (this.f) {
                Animation loadAnimation = AnimationUtils.loadAnimation(this.c.getContext(), C0000R.anim.slide_down_out);
                loadAnimation.setAnimationListener(new i(this));
                this.c.startAnimation(loadAnimation);
                Animation loadAnimation2 = AnimationUtils.loadAnimation(this.c.getContext(), C0000R.anim.slide_up_out);
                loadAnimation2.setAnimationListener(new j(this));
                this.d.startAnimation(loadAnimation2);
            } else {
                return;
            }
        } else if (!this.f) {
            this.c.setVisibility(0);
            this.c.startAnimation(AnimationUtils.loadAnimation(this.c.getContext(), C0000R.anim.slide_up_in));
            this.d.setVisibility(0);
            this.d.startAnimation(AnimationUtils.loadAnimation(this.c.getContext(), C0000R.anim.slide_down_in));
        } else {
            return;
        }
        b(z);
        this.f = z;
    }

    public final void b() {
        a();
        for (int i = 0; i < this.a.getChildCount(); i++) {
            this.a.getChildAt(i).setVisibility(8);
        }
        Context context = this.a.getContext();
        co coVar = new co(context);
        coVar.setCacheColorHint(0);
        coVar.setDivider(context.getResources().getDrawable(C0000R.drawable.line));
        coVar.setChildDivider(context.getResources().getDrawable(C0000R.drawable.line));
        coVar.setId(this.h);
        coVar.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.a.addView(coVar, 0);
        coVar.a(new h(this));
        s.a().i();
    }

    public final void b(boolean z) {
        this.b.b(!z ? 0 : (int) this.b.getContext().getResources().getDimension(C0000R.dimen.title_height), !z ? 0 : (int) this.b.getContext().getResources().getDimension(C0000R.dimen.reader_navigation_var_height));
    }

    public final void c() {
        this.e = this.c.getVisibility() == 0;
        a(true);
    }

    public final void d() {
        if (!this.e) {
            a(false);
        }
    }

    public final boolean e() {
        return this.f;
    }

    public final void f() {
        if (this.f) {
            a(false);
        } else {
            a(true);
        }
    }
}
