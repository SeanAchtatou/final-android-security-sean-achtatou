package udk.android.reader.view.pdf;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import udk.android.reader.b.b;

final class cz implements Runnable {
    private /* synthetic */ dx a;
    private final /* synthetic */ ProgressDialog b;
    private final /* synthetic */ Context c;

    cz(dx dxVar, ProgressDialog progressDialog, Context context) {
        this.a = dxVar;
        this.b = progressDialog;
        this.c = context;
    }

    public final void run() {
        this.b.dismiss();
        Toast.makeText(this.c, b.k, 0).show();
    }
}
