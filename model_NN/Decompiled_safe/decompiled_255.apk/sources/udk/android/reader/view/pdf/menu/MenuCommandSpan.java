package udk.android.reader.view.pdf.menu;

import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.View;
import java.util.List;

public class MenuCommandSpan extends URLSpan {
    boolean a = false;
    private c b;
    private int c;
    private int d;

    private MenuCommandSpan(c cVar, int i, int i2) {
        super("");
        this.b = cVar;
        this.d = i2;
        this.c = i;
    }

    public static CharSequence a(List list, CharSequence charSequence, int i, int i2) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        for (int i3 = 0; i3 < list.size(); i3++) {
            c cVar = (c) list.get(i3);
            String a2 = cVar.a();
            int length = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence) a2);
            spannableStringBuilder.setSpan(new MenuCommandSpan(cVar, i, i2), length, a2.length() + length, 33);
            if (i3 < list.size() - 1) {
                spannableStringBuilder.append(charSequence);
            }
        }
        return spannableStringBuilder.subSequence(0, spannableStringBuilder.length());
    }

    public void onClick(View view) {
        this.b.b().run();
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setUnderlineText(false);
        textPaint.setColor(this.a ? this.d : this.c);
    }
}
