package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import java.io.InputStream;

final class dy implements Runnable {
    final /* synthetic */ PDFView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ InputStream c;
    private final /* synthetic */ long d;
    private final /* synthetic */ int e;
    private final /* synthetic */ float f;
    private final /* synthetic */ boolean g;
    private final /* synthetic */ float h;
    private final /* synthetic */ float i;
    private final /* synthetic */ int j;
    private final /* synthetic */ int k;

    dy(PDFView pDFView, String str, InputStream inputStream, long j2, int i2, float f2, boolean z, float f3, float f4, int i3, int i4) {
        this.a = pDFView;
        this.b = str;
        this.c = inputStream;
        this.d = j2;
        this.e = i2;
        this.f = f2;
        this.g = z;
        this.h = f3;
        this.i = f4;
        this.j = i3;
        this.k = i4;
    }

    public final void run() {
        EditText editText = new EditText(this.a.getContext());
        editText.setInputType(128);
        editText.setTransformationMethod(new PasswordTransformationMethod());
        editText.setImeOptions(6);
        editText.setOnEditorActionListener(new hy(this, editText, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k));
        editText.setSingleLine();
        this.a.V = new AlertDialog.Builder(this.a.getContext()).setTitle("PASSWORD").setView(editText).setCancelable(false).setPositiveButton("OK", new hz(this, editText, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k)).show();
        editText.postDelayed(new ib(this, editText), 100);
    }
}
