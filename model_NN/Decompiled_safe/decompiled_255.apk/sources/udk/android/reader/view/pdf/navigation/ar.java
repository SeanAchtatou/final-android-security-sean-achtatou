package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unidocs.commonlib.util.e;
import java.io.InputStream;
import java.util.List;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.a.a;
import udk.android.reader.pdf.b;

public final class ar extends BaseAdapter {
    private b a = b.a();
    private Context b;
    private NavigationService c = NavigationService.a();

    public ar(Context context) {
        this.b = context;
    }

    /* renamed from: a */
    public final List getItem(int i) {
        return this.a.a(i);
    }

    public final int getCount() {
        return this.a.j();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        InputStream inputStream;
        View inflate = view != null ? view : View.inflate(this.b, C0000R.layout.pdf_page, null);
        try {
            List a2 = getItem(i);
            if (this.c.n() == NavigationService.a) {
                try {
                    inputStream = this.b.getAssets().open("book/thumbnail/" + e.a(new StringBuilder(String.valueOf(((a) a2.get(0)).ag())).toString()) + ".png");
                } catch (Exception e) {
                    c.a(e.getMessage(), e);
                    inputStream = null;
                }
                if (inputStream != null) {
                    ((ImageView) inflate.findViewById(C0000R.id.thumbnail)).setImageBitmap(BitmapFactory.decodeStream(inputStream));
                }
            }
            ((TextView) inflate.findViewById(C0000R.id.desc)).setText(new StringBuilder().append(((a) a2.get(0)).ag()).toString());
        } catch (Exception e2) {
            c.a((Throwable) e2);
        }
        return inflate;
    }
}
