package udk.android.reader.view.pdf;

import android.app.ProgressDialog;
import android.view.MotionEvent;
import udk.android.reader.pdf.c;

final class id implements Runnable {
    private /* synthetic */ gb a;
    private final /* synthetic */ ProgressDialog b;
    private final /* synthetic */ c c;
    private final /* synthetic */ MotionEvent d;

    id(gb gbVar, ProgressDialog progressDialog, c cVar, MotionEvent motionEvent) {
        this.a = gbVar;
        this.b = progressDialog;
        this.c = cVar;
        this.d = motionEvent;
    }

    public final void run() {
        this.b.dismiss();
        this.a.a.b(this.c, this.d);
    }
}
