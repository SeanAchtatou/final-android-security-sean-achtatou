package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import udk.android.b.d;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.z;

final class i implements View.OnClickListener {
    private /* synthetic */ ay a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ z c;
    private final /* synthetic */ LinearLayout d;

    i(ay ayVar, Context context, z zVar, LinearLayout linearLayout) {
        this.a = ayVar;
        this.b = context;
        this.c = zVar;
        this.d = linearLayout;
    }

    public final void onClick(View view) {
        s.a().a(this.b, this.c, this.a.c);
        d.a(this.d, 570425344, 200);
    }
}
