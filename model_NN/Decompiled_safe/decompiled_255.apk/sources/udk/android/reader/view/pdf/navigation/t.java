package udk.android.reader.view.pdf.navigation;

final class t implements Runnable {
    private /* synthetic */ PageNavigationView a;
    private final /* synthetic */ int b;

    t(PageNavigationView pageNavigationView, int i) {
        this.a = pageNavigationView;
        this.b = i;
    }

    public final void run() {
        if (!this.a.c.u()) {
            this.a.setSelection(this.b - 1);
        } else {
            this.a.setSelection(this.a.c.l() - this.b);
        }
    }
}
