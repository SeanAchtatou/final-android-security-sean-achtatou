package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.AdapterView;

final class n implements AdapterView.OnItemSelectedListener {
    final /* synthetic */ BookmarkNavigationView a;

    n(BookmarkNavigationView bookmarkNavigationView) {
        this.a = bookmarkNavigationView;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (view != null) {
            this.a.e = i;
            view.postDelayed(new am(this, i), 1000);
        }
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
