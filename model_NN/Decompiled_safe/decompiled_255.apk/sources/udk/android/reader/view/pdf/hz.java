package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import java.io.InputStream;

final class hz implements DialogInterface.OnClickListener {
    private /* synthetic */ dy a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ String c;
    private final /* synthetic */ InputStream d;
    private final /* synthetic */ long e;
    private final /* synthetic */ int f;
    private final /* synthetic */ float g;
    private final /* synthetic */ boolean h;
    private final /* synthetic */ float i;
    private final /* synthetic */ float j;
    private final /* synthetic */ int k;
    private final /* synthetic */ int l;

    hz(dy dyVar, EditText editText, String str, InputStream inputStream, long j2, int i2, float f2, boolean z, float f3, float f4, int i3, int i4) {
        this.a = dyVar;
        this.b = editText;
        this.c = str;
        this.d = inputStream;
        this.e = j2;
        this.f = i2;
        this.g = f2;
        this.h = z;
        this.i = f3;
        this.j = f4;
        this.k = i3;
        this.l = i4;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        String editable = this.b.getText().toString();
        PDFView.a(this.a.a, this.c, this.d, this.e, editable, editable, this.f, this.g, this.h, this.i, this.j, this.k, this.l + 1);
        this.a.a.V = (AlertDialog) null;
    }
}
