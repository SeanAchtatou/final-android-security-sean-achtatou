package udk.android.reader.view.pdf.navigation;

final class av implements Runnable {
    private /* synthetic */ NavigationService a;
    private final /* synthetic */ int b;
    private final /* synthetic */ int c;
    private final /* synthetic */ boolean d;

    av(NavigationService navigationService, int i, int i2, boolean z) {
        this.a = navigationService;
        this.b = i;
        this.c = i2;
        this.d = z;
    }

    public final void run() {
        this.a.o.setText(String.valueOf(this.b) + "/" + this.c);
        this.a.p.setText(this.d ? "completed" : "now searching");
    }
}
