package udk.android.reader.view.pdf;

import udk.android.reader.view.pdf.ZoomService;

final class ea extends Thread {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ int b;
    private final /* synthetic */ float c;
    private final /* synthetic */ boolean d;

    ea(PDFView pDFView, int i, float f, boolean z) {
        this.a = pDFView;
        this.b = i;
        this.c = f;
        this.d = z;
    }

    public final void run() {
        this.a.l.a(this.a.getWidth(), this.a.getHeight());
        if (this.b != -1) {
            this.a.b.f(this.a.c, this.c);
            this.a.V().a(this.a.c, this.b);
        } else if (this.d) {
            ZoomService.FittingType a2 = this.a.A.a(this.a.c);
            if (a2 == ZoomService.FittingType.WIDTHFIT) {
                this.a.b.a(this.a.c, this.a.l.a);
            } else if (a2 == ZoomService.FittingType.HEIGHTFIT) {
                this.a.b.b(this.a.c, this.a.l.b);
            }
        } else {
            this.a.b.f(this.a.c, this.c);
        }
        this.a.F.b().a(true);
        this.a.j = false;
    }
}
