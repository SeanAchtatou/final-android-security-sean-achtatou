package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.view.View;
import udk.android.b.m;

final class fi extends View {
    private /* synthetic */ au a;
    private final /* synthetic */ int b;
    private final /* synthetic */ int c;
    private final /* synthetic */ Context d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fi(au auVar, Context context, int i, int i2, Context context2) {
        super(context);
        this.a = auVar;
        this.b = i;
        this.c = i2;
        this.d = context2;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) this.b, (float) this.c), (float) (m.a(this.d, 5) + 1), (float) (m.a(this.d, 5) + 1), this.a.a);
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) this.b, (float) this.c), (float) m.a(this.d, 5), (float) m.a(this.d, 5), this.a.c);
    }
}
