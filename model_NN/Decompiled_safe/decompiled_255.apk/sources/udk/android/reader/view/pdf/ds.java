package udk.android.reader.view.pdf;

import android.view.View;

final class ds implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ View b;
    private final /* synthetic */ boolean c;

    ds(PDFView pDFView, View view, boolean z) {
        this.a = pDFView;
        this.b = view;
        this.c = z;
    }

    public final void run() {
        this.b.setVisibility(this.c ? 0 : 8);
    }
}
