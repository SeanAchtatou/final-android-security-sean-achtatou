package udk.android.reader.view.contents;

import udk.android.reader.contents.l;

final class q implements Runnable {
    private /* synthetic */ AllPDFListView a;
    private final /* synthetic */ l b;

    q(AllPDFListView allPDFListView, l lVar) {
        this.a = allPDFListView;
        this.b = lVar;
    }

    public final void run() {
        if (this.a.b.getVisibility() != 0) {
            this.a.b.setVisibility(0);
        }
        this.a.b.setProgress((int) ((((double) this.b.d) / ((double) this.b.c)) * 100.0d));
    }
}
