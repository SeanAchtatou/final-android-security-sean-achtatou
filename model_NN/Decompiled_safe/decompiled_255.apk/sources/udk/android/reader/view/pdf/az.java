package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.RectF;
import android.view.View;
import android.widget.AbsoluteLayout;
import java.util.HashMap;
import udk.android.reader.pdf.PDF;

public final class az extends AbsoluteLayout {
    private hx a = hx.a();
    private PDF b = PDF.a();
    private HashMap c = new HashMap();

    az(Context context) {
        super(context);
    }

    private RectF b(RectF rectF) {
        float n = this.a.n();
        return new RectF((rectF.left * n) + this.a.a, (rectF.top * n) + this.a.b, (rectF.right * n) + this.a.a, (n * rectF.bottom) + this.a.b);
    }

    /* access modifiers changed from: package-private */
    public final void a(RectF rectF) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            RectF rectF2 = (RectF) this.c.get(childAt);
            float width = rectF.width() / ((float) this.b.C());
            float height = rectF.height() / ((float) this.b.D());
            RectF rectF3 = new RectF((rectF2.left * width) + rectF.left, (rectF2.top * height) + rectF.top, (width * rectF2.right) + rectF.left, (rectF2.bottom * height) + rectF.top);
            AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) childAt.getLayoutParams();
            layoutParams.x = (int) rectF3.left;
            layoutParams.y = (int) rectF3.top;
            layoutParams.width = (int) rectF3.width();
            layoutParams.height = (int) rectF3.height();
        }
        post(new ec(this));
    }

    /* access modifiers changed from: package-private */
    public final void a(View view, RectF rectF) {
        RectF b2 = b(rectF);
        AbsoluteLayout.LayoutParams layoutParams = new AbsoluteLayout.LayoutParams((int) b2.width(), (int) b2.height(), (int) b2.left, (int) b2.top);
        this.c.put(view, rectF);
        addView(view, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return !this.c.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(View view) {
        return this.c.containsKey(view);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.c.clear();
        removeAllViews();
    }

    /* access modifiers changed from: package-private */
    public final void b(View view) {
        this.c.remove(view);
        removeView(view);
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            RectF b2 = b((RectF) this.c.get(childAt));
            AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) childAt.getLayoutParams();
            layoutParams.x = (int) b2.left;
            layoutParams.y = (int) b2.top;
            layoutParams.width = (int) b2.width();
            layoutParams.height = (int) b2.height();
        }
        post(new eb(this));
    }
}
