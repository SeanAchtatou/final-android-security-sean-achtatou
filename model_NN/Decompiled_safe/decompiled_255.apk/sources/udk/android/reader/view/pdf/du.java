package udk.android.reader.view.pdf;

import android.app.ProgressDialog;
import android.content.Context;

final class du implements Runnable {
    final /* synthetic */ PDFView a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ long c;

    du(PDFView pDFView, Context context, long j) {
        this.a = pDFView;
        this.b = context;
        this.c = j;
    }

    public final void run() {
        if (this.a.i == null) {
            this.a.i = new ProgressDialog(this.b);
            this.a.i.setOnCancelListener(new im(this));
        }
        this.a.i.setProgressStyle(1);
        this.a.i.setMax((int) this.c);
        this.a.i.setProgress(0);
        this.a.i.show();
    }
}
