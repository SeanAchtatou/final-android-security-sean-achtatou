package udk.android.reader.view.pdf;

import android.view.View;
import udk.android.a.e;
import udk.android.b.c;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.p;
import udk.android.reader.pdf.c.a;

final class ao implements e {
    private /* synthetic */ fo a;
    private final /* synthetic */ View b;
    private final /* synthetic */ Annotation c;

    ao(fo foVar, View view, Annotation annotation) {
        this.a = foVar;
        this.b = view;
        this.c = annotation;
    }

    public final void a(int i) {
        this.a.a.b.setColor(i);
        this.b.invalidate();
        if (this.c.s() && this.a.a.m != null) {
            this.a.a.c.setColor(c.a(this.a.a.c.getColor(), c.a(i)));
            this.a.a.m.invalidate();
        }
        if ((this.c instanceof p) && this.a.a.h != null) {
            this.a.a.h.setImageBitmap(a.a().a(this.a.a.g.getText().toString(), i));
        }
    }
}
