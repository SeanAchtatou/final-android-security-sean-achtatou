package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unidocs.commonlib.util.e;
import java.io.InputStream;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.a;

final class b extends BaseAdapter {
    private Context a;
    private NavigationService b = NavigationService.a();
    private PDF c = PDF.a();

    public b(Context context) {
        this.a = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Integer getItem(int i) {
        return Integer.valueOf(!this.c.u() ? i + 1 : this.c.l() - i);
    }

    public final int getCount() {
        return this.c.l();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        InputStream inputStream;
        View inflate = view != null ? view : View.inflate(this.a, C0000R.layout.pdf_page, null);
        if (this.b.n() == NavigationService.a) {
            try {
                inputStream = this.a.getAssets().open("book/thumbnail/" + e.a(new StringBuilder().append(getItem(i)).toString()) + ".png");
            } catch (Exception e) {
                c.a(e.getMessage(), e);
                inputStream = null;
            }
            if (inputStream != null) {
                ((ImageView) inflate.findViewById(C0000R.id.thumbnail)).setImageBitmap(BitmapFactory.decodeStream(inputStream));
            }
        }
        int intValue = getItem(i).intValue();
        ((TextView) inflate.findViewById(C0000R.id.desc)).setText(new StringBuilder().append(intValue).toString());
        ((ImageView) inflate.findViewById(C0000R.id.bookmark_ribon)).setVisibility(a.a().a(intValue) ? 0 : 8);
        return inflate;
    }
}
