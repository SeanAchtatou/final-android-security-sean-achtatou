package udk.android.reader.view.pdf;

import android.os.ResultReceiver;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import udk.android.b.m;
import udk.android.e.a;
import udk.android.reader.b.c;

public final class ax extends BaseInputConnection {
    private SpannableStringBuilder a;
    private a b;
    private int c;

    public ax(View view) {
        super(view, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder}
     arg types: [int, java.lang.String]
     candidates:
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.Editable}
      ClspMth{android.text.SpannableStringBuilder.insert(int, java.lang.CharSequence):android.text.SpannableStringBuilder} */
    private void a(String str) {
        this.a.insert(b(), (CharSequence) str);
    }

    private int b() {
        return this.a.length() - this.b.m();
    }

    public final a a() {
        return this.b;
    }

    public final void a(int i) {
        this.b.b(this.a.length() - (i < 0 ? 0 : i > this.a.length() ? this.a.length() : i));
        setSelection(b(), b());
    }

    /* access modifiers changed from: package-private */
    public final void a(KeyEvent keyEvent) {
        c.a("## PROCESS KEY UP EVENT : " + keyEvent);
        switch (keyEvent.getKeyCode()) {
            case 7:
                a("0");
                break;
            case 8:
                a("1");
                break;
            case 9:
                a("2");
                break;
            case 10:
                a("3");
                break;
            case 11:
                a("4");
                break;
            case 12:
                a("5");
                break;
            case 13:
                a("6");
                break;
            case 14:
                a("7");
                break;
            case 15:
                a("8");
                break;
            case 16:
                a("9");
                break;
            case 21:
                a(b() - 1);
                break;
            case 22:
                a(b() + 1);
                break;
            case 29:
                a("a");
                break;
            case 30:
                a("b");
                break;
            case 31:
                a("c");
                break;
            case 32:
                a("d");
                break;
            case 33:
                a("e");
                break;
            case 34:
                a("f");
                break;
            case 35:
                a("g");
                break;
            case 36:
                a("h");
                break;
            case 37:
                a("i");
                break;
            case 38:
                a("j");
                break;
            case 39:
                a("k");
                break;
            case 40:
                a("l");
                break;
            case 41:
                a("m");
                break;
            case 42:
                a("n");
                break;
            case 43:
                a("o");
                break;
            case 44:
                a("p");
                break;
            case 45:
                a("q");
                break;
            case 46:
                a("r");
                break;
            case 47:
                a("s");
                break;
            case 48:
                a("t");
                break;
            case 49:
                a("u");
                break;
            case 50:
                a("v");
                break;
            case 51:
                a("w");
                break;
            case 52:
                a("x");
                break;
            case 53:
                a("y");
                break;
            case 54:
                a("z");
                break;
            case 66:
                if (this.b.d()) {
                    a("\n");
                    break;
                }
                break;
            case 67:
                deleteSurroundingText(1, 0);
                break;
        }
        beginBatchEdit();
        endBatchEdit();
    }

    public final void a(View view, Runnable runnable, boolean z) {
        if (z) {
            view.post(new ap(this, view));
        }
        if (this.b != null) {
            this.b.o();
            this.b = null;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public final void a(View view, a aVar, ResultReceiver resultReceiver) {
        this.b = aVar;
        String g = aVar.g();
        if (g == null) {
            g = "";
        }
        this.a = new SpannableStringBuilder(g);
        a(this.a.length());
        view.setFocusableInTouchMode(true);
        view.setFocusable(true);
        view.requestFocus();
        m.a(view, resultReceiver);
        this.c = aVar.e();
        aVar.n();
    }

    public final boolean beginBatchEdit() {
        return super.beginBatchEdit();
    }

    public final boolean endBatchEdit() {
        if (this.c > 0) {
            while (this.a.length() > this.c) {
                deleteSurroundingText(1, 0);
            }
        }
        if (this.b != null) {
            this.b.b(this.a.toString());
            hx.a().p();
        }
        return super.endBatchEdit();
    }

    public final Editable getEditable() {
        return this.a;
    }
}
