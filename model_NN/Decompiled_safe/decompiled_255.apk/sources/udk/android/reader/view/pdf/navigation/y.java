package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.ImageView;
import udk.android.reader.b.c;
import udk.android.reader.view.pdf.n;

final class y extends Thread {
    private /* synthetic */ SearchNavigationView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ View c;
    private final /* synthetic */ int d;
    private final /* synthetic */ float e;
    private final /* synthetic */ ImageView f;

    y(SearchNavigationView searchNavigationView, String str, View view, int i, float f2, ImageView imageView) {
        this.a = searchNavigationView;
        this.b = str;
        this.c = view;
        this.d = i;
        this.e = f2;
        this.f = imageView;
    }

    public final void run() {
        while (this.a.d.z().equals(this.b) && !this.a.d.g()) {
            try {
                Thread.sleep(400);
                if (n.b().d() || this.a.d.r()) {
                }
            } catch (Exception e2) {
                c.a(e2.getMessage(), e2);
            }
            try {
                if (this.c.getParent() != null) {
                    int positionForView = this.a.getPositionForView(this.c);
                    if (this.a.isShown() && positionForView >= this.a.getFirstVisiblePosition() && positionForView <= this.a.getLastVisiblePosition() && this.a.d.z().equals(this.b) && !this.a.d.g()) {
                        this.f.post(new al(this, this.f, this.a.d.c(this.d, this.e)));
                        return;
                    }
                    return;
                }
                return;
            } catch (Exception e3) {
                c.a((Throwable) e3);
                return;
            }
        }
    }
}
