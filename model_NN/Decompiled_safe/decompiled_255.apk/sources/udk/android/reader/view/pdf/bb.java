package udk.android.reader.view.pdf;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;
import udk.android.reader.b.c;

final class bb implements DialogInterface.OnClickListener {
    private /* synthetic */ dc a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ Context e;
    private final /* synthetic */ PDFView f;
    private final /* synthetic */ Runnable g;
    private final /* synthetic */ String h;

    bb(dc dcVar, EditText editText, String str, String str2, Context context, PDFView pDFView, Runnable runnable, String str3) {
        this.a = dcVar;
        this.b = editText;
        this.c = str;
        this.d = str2;
        this.e = context;
        this.f = pDFView;
        this.g = runnable;
        this.h = str3;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2;
        try {
            i2 = Integer.parseInt(this.b.getText().toString()) * 1000;
        } catch (Exception e2) {
            c.a((Throwable) e2);
            i2 = 60000;
        }
        if (i2 < 1000) {
            i2 = 60000;
        }
        Toast.makeText(this.e, this.c.replaceFirst(this.d, new StringBuilder().append(i2 / 1000).toString()), 0).show();
        dc.a(this.a, i2, this.f, this.g, this.h);
    }
}
