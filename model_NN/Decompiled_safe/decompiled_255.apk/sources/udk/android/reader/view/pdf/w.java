package udk.android.reader.view.pdf;

import android.content.DialogInterface;
import udk.android.reader.b.b;

final class w implements DialogInterface.OnClickListener {
    private /* synthetic */ gw a;
    private final /* synthetic */ String[] b;

    w(gw gwVar, String[] strArr) {
        this.a = gwVar;
        this.b = strArr;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        o V = this.a.a.V();
        if (V.o()) {
            V.v();
        }
        if (this.a.a.K.b()) {
            this.a.a.K.a();
        }
        if (b.G.equals(this.b[i])) {
            this.a.a.a(new eu(this.a.a));
        } else if (b.H.equals(this.b[i])) {
            this.a.a.a(new et(this.a.a));
        } else if (b.I.equals(this.b[i])) {
            this.a.a.a(new ep(this.a.a));
        } else if (b.J.equals(this.b[i])) {
            this.a.a.a(new eo(this.a.a));
        }
    }
}
