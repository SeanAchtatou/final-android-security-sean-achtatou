package udk.android.reader.view.pdf;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;
import com.unidocs.commonlib.util.b;
import com.unidocs.commonlib.util.c;
import com.unidocs.commonlib.util.f;
import com.unidocs.commonlib.util.j;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import udk.android.b.i;
import udk.android.b.m;
import udk.android.reader.C0000R;
import udk.android.reader.b.a;
import udk.android.reader.b.d;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ag;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.aj;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.aa;
import udk.android.reader.pdf.annotation.ab;
import udk.android.reader.pdf.annotation.q;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.v;
import udk.android.reader.pdf.ar;
import udk.android.reader.pdf.b.e;
import udk.android.reader.pdf.y;
import udk.android.reader.view.pdf.ZoomService;
import udk.android.reader.view.pdf.menu.g;

public final class PDFView extends FrameLayout implements aa, ar, b, gv, hq, io, t {
    /* access modifiers changed from: private */
    public ZoomService A;
    private float B;
    private ce C;
    private cv D;
    /* access modifiers changed from: private */
    public View E;
    /* access modifiers changed from: private */
    public cm F;
    /* access modifiers changed from: private */
    public hn G;
    private e H;
    /* access modifiers changed from: private */
    public az I;
    /* access modifiers changed from: private */
    public ImageView J;
    /* access modifiers changed from: private */
    public g K;
    /* access modifiers changed from: private */
    public boolean L;
    private boolean M;
    private boolean N;
    /* access modifiers changed from: private */
    public View O;
    private Thread P;
    /* access modifiers changed from: private */
    public int Q;
    /* access modifiers changed from: private */
    public int R;
    /* access modifiers changed from: private */
    public int S;
    /* access modifiers changed from: private */
    public int T;
    /* access modifiers changed from: private */
    public Thread U;
    /* access modifiers changed from: private */
    public AlertDialog V;
    MediaPlayer a;
    /* access modifiers changed from: private */
    public PDF b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    private boolean f;
    private boolean g;
    /* access modifiers changed from: private */
    public Runnable h;
    /* access modifiers changed from: private */
    public ProgressDialog i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public hx k;
    /* access modifiers changed from: private */
    public ic l;
    private cl m;
    private in n;
    /* access modifiers changed from: private */
    public List o;
    /* access modifiers changed from: private */
    public List p;
    /* access modifiers changed from: private */
    public View[] q;
    private int r;
    private int s;
    /* access modifiers changed from: private */
    public int t;
    private int u;
    private int v;
    private int w;
    private View[] x;
    private View[] y;
    /* access modifiers changed from: private */
    public ax z;

    public enum SmartNavDirection {
        LEFT,
        TOP,
        RIGHT,
        BOTTOM
    }

    public enum ViewMode {
        PDF,
        TEXTREFLOW,
        THUMBNAIL
    }

    public PDFView(Context context) {
        super(context);
        b(context);
    }

    public PDFView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b(context);
    }

    static /* synthetic */ void E(PDFView pDFView) {
        if (pDFView.O != null) {
            pDFView.a(pDFView.O);
        }
    }

    /* access modifiers changed from: private */
    public Runnable a(s sVar, Annotation annotation) {
        return new fa(this, sVar, annotation);
    }

    public static void a(Activity activity, View view, String str, Runnable runnable, String str2) {
        if (a(activity)) {
            String str3 = b.a(str2) ? str2 : udk.android.reader.b.b.i;
            String str4 = b.b(str) ? udk.android.reader.b.b.a : str;
            ProgressDialog progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(str4);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            new ad(view, activity, runnable, str3, progressDialog).start();
        } else if (runnable != null) {
            new af(runnable).start();
        }
    }

    public static void a(Context context, y yVar) {
        if (a(context)) {
            File file = new File(yVar.a());
            File file2 = new File(yVar.d());
            File file3 = new File(yVar.e());
            File file4 = new File(yVar.c());
            File file5 = new File(yVar.f());
            if (file2.exists()) {
                c.a(file2);
            }
            file2.mkdirs();
            file.mkdirs();
            file3.mkdirs();
            file5.mkdirs();
            File[] listFiles = file5.listFiles(new di());
            if (b.a((Object[]) listFiles)) {
                for (File delete : listFiles) {
                    delete.delete();
                }
            }
            c.a(String.valueOf(file5.getAbsolutePath()) + "/res_" + 8);
            InputStream open = context.getAssets().open("ezpdfresource.zip");
            File file6 = new File(String.valueOf(file.getAbsolutePath()) + "/ezpdfresource.zip");
            j.a(file6, open);
            f.a(file6, file2);
            c.a(file6);
            j.a(file4, new String(j.a(context.getAssets().open("ezpdfconfiguration"))).replaceAll("RESDIR_PREFIX", file2.getAbsolutePath()), null);
        }
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        this.I.b(view);
        if (view == this.O) {
            this.O = null;
            if (this.F.getVisibility() != 0) {
                post(new dr(this));
            }
        }
        if (this.P != null && this.P.isAlive()) {
            try {
                this.P.join();
            } catch (Exception e2) {
                udk.android.reader.b.c.a((Throwable) e2);
            }
        }
        this.P = null;
    }

    /* access modifiers changed from: private */
    public void a(Runnable runnable) {
        if (a.f && !b.b(this.b.y())) {
            if (!this.b.okToAddNotes()) {
                post(new fe(this));
            } else if (a.q || s.a().e()) {
                runnable.run();
            } else {
                new AlertDialog.Builder(getContext()).setMessage(udk.android.reader.b.b.O).setPositiveButton(udk.android.reader.b.b.t, new fd(this, runnable)).setNegativeButton(udk.android.reader.b.b.u, new fc(this, runnable)).show();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, Runnable runnable) {
        Context context = getContext();
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(udk.android.reader.b.b.b);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        dx dxVar = new dx(this, str, progressDialog, context, runnable);
        dxVar.setDaemon(true);
        dxVar.start();
    }

    static /* synthetic */ void a(PDFView pDFView, String str, InputStream inputStream, long j2, String str2, String str3, int i2, float f2, boolean z2, float f3, float f4, int i3, int i4) {
        long j3 = j2;
        int i5 = i4;
        int i6 = i3;
        float f5 = f4;
        float f6 = f3;
        boolean z3 = z2;
        float f7 = f2;
        int i7 = i2;
        String str4 = str3;
        String str5 = str2;
        InputStream inputStream2 = inputStream;
        String str6 = str;
        PDFView pDFView2 = pDFView;
        while (!pDFView2.a(str6, inputStream2, j3, str5, str4, i7, f7, z3, f6, f5, i6)) {
            if (pDFView2.b.j() && i5 < 4) {
                pDFView2.post(new dy(pDFView2, str6, inputStream2, j3, i7, f7, z3, f6, f5, i6, i5));
                return;
            } else if (!pDFView2.b.k() || i5 >= 4) {
                m.a((Activity) pDFView2.getContext(), pDFView2, udk.android.reader.b.b.j);
                return;
            } else {
                udk.android.d.a.a();
                String a2 = udk.android.d.a.a(pDFView2.getContext());
                pDFView2.getContext();
                i5++;
                str4 = "android";
                str5 = a2;
            }
        }
    }

    static /* synthetic */ void a(PDFView pDFView, s sVar, udk.android.reader.pdf.annotation.a aVar, boolean z2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.r, new en(pDFView, sVar, aVar)));
        arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.s, new em(pDFView, sVar, aVar)));
        if (aVar.d()) {
            arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.y, new es(pDFView, aVar, sVar)));
        }
        if (aVar.b()) {
            arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.x, new er(pDFView, aVar, sVar)));
        }
        if (z2 || !pDFView.K.b()) {
            pDFView.K.a(com.unidocs.commonlib.a.b.a(24), aVar, new RectF((float) pDFView.Q, (float) pDFView.R, (float) (pDFView.getWidth() - pDFView.S), (float) (pDFView.getHeight() - pDFView.T)), arrayList, null);
        }
    }

    static /* synthetic */ void a(PDFView pDFView, s sVar, q qVar, Runnable runnable) {
        pDFView.m.a((ae) new ey(pDFView, qVar, sVar, runnable));
        pDFView.post(new ex(pDFView));
    }

    private static boolean a(Context context) {
        boolean z2;
        int i2;
        y c2 = a.c(context);
        File file = new File(c2.a());
        File file2 = new File(c2.d());
        File file3 = new File(c2.e());
        File file4 = new File(c2.c());
        File file5 = new File(c2.f());
        boolean z3 = false;
        File[] listFiles = file5.listFiles(new dh());
        if (b.a((Object[]) listFiles)) {
            int length = listFiles.length;
            int i3 = 0;
            while (true) {
                if (i3 >= length) {
                    break;
                }
                try {
                    i2 = Integer.parseInt(listFiles[i3].getName().replaceAll("res_", ""));
                } catch (Exception e2) {
                    udk.android.reader.b.c.a((Throwable) e2);
                    i2 = -1;
                }
                if (i2 >= 8) {
                    z3 = true;
                    break;
                }
                i3++;
            }
        }
        if (!z3) {
            File file6 = new File(String.valueOf(c2.d()) + "/" + 8);
            z2 = file6.exists() && file6.isDirectory();
        } else {
            z2 = z3;
        }
        return !file5.exists() || !file2.exists() || !file.exists() || !file3.exists() || !file4.exists() || !z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.ag.a(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      udk.android.reader.pdf.ag.a(java.lang.String, int):int
      udk.android.reader.pdf.ag.a(java.lang.String, java.lang.Object):void
      udk.android.reader.pdf.ag.a(java.lang.String, float):float */
    private boolean a(String str, InputStream inputStream, long j2, String str2, String str3, int i2, float f2, boolean z2, float f3, float f4, int i3) {
        boolean z3;
        float f5;
        float f6;
        boolean z4;
        int i4;
        this.F.b().a(false);
        if (this.b.f()) {
            this.b.e();
        }
        while (getHeight() <= 0) {
            try {
                Thread.sleep(100);
            } catch (Exception e2) {
                udk.android.reader.b.c.a((Throwable) e2);
            }
        }
        X();
        boolean z5 = (b.b(str) && inputStream != null) && j2 > 0;
        if (z5) {
            post(new du(this, getContext(), j2));
            while (true) {
                if (this.i != null && this.i.isShowing()) {
                    break;
                }
                try {
                    Thread.sleep(100);
                } catch (Exception e3) {
                    udk.android.reader.b.c.a((Throwable) e3);
                }
            }
            new dv(this).start();
        }
        boolean a2 = b.a(new String[]{str2, str3}) ? this.b.a(a.c(getContext()), str, inputStream, str2, str3) : this.b.a(a.c(getContext()), str, inputStream);
        if (z5 && this.i != null) {
            post(new dw(this));
        }
        if (!a2) {
            this.F.b().a(true);
            Y();
            return false;
        }
        this.k.x();
        this.d = str2;
        this.e = str3;
        this.f = i2 <= 0 && a.aR && b.a(str);
        this.c = this.f ? this.b.v().a("lastreadpage", 1) : (i2 <= 0 || i2 > this.b.l()) ? 1 : i2;
        this.g = f2 <= 0.0f && a.aR && b.a(str);
        float b2 = this.A.b(this.c);
        if (this.g) {
            ag v2 = this.b.v();
            int a3 = v2.a("lastreadcolumn", i3);
            float a4 = v2.a("lastreadzoom", b2);
            boolean z6 = a4 <= b2 || v2.a("lastreadforefit");
            this.k.a = v2.a("lastreadx", 0.0f);
            this.k.b = v2.a("lastready", 0.0f);
            z4 = z6;
            i4 = a3;
            f6 = a4;
        } else {
            if (!z2) {
                f5 = f2 > a.ac ? a.ac : f2 < a.ab ? a.ab : f2;
                z3 = f5 <= b2;
            } else {
                z3 = z2;
                f5 = f2;
            }
            this.k.a = f3;
            this.k.b = f4;
            f6 = f5;
            z4 = z3;
            i4 = i3;
        }
        new ea(this, i4, f6, z4).start();
        return true;
    }

    private void ah() {
        ViewMode F2 = F();
        if (F2 == ViewMode.PDF) {
            d(aj.a().a(this.b.E()));
            W();
            am();
            aj();
            ak();
            if (a.R == 1) {
                al();
                return;
            }
            b(false);
            c(false);
        } else if (F2 == ViewMode.TEXTREFLOW) {
            d(false);
            ak();
            f(false);
            g(false);
            h(false);
            b(true);
            c(true);
        } else if (F2 == ViewMode.THUMBNAIL) {
            ai();
        }
    }

    private void ai() {
        d(false);
        e(false);
        f(false);
        g(false);
        h(false);
        b(false);
        c(false);
    }

    /* access modifiers changed from: private */
    public void aj() {
        if (this.n.d()) {
            f(true);
        } else {
            f(false);
        }
    }

    /* access modifiers changed from: private */
    public void ak() {
        if (dc.a().b()) {
            e(true);
        } else {
            e(false);
        }
    }

    private void al() {
        b(this.M && this.b.w());
        c(this.N && this.b.x());
    }

    private void am() {
        h(this.F.c().d());
    }

    private void an() {
        if (b.a((Object[]) this.q)) {
            for (View view : this.q) {
                view.findViewById(this.r).setOnClickListener(new fu(this));
                view.findViewById(this.s).setOnClickListener(new fw(this));
                view.findViewById(this.u).setOnClickListener(new fv(this));
                view.findViewById(this.v).setOnClickListener(new fy(this));
                view.findViewById(this.w).setOnClickListener(new fx(this));
            }
        }
        if (b.a((Object[]) this.x)) {
            for (View onClickListener : this.x) {
                onClickListener.setOnClickListener(new ga(this));
            }
        }
        if (b.a((Object[]) this.y)) {
            for (View onClickListener2 : this.y) {
                onClickListener2.setOnClickListener(new fz(this));
            }
        }
    }

    private void ao() {
        if (this.a != null && this.a.isPlaying()) {
            this.a.seekTo(this.a.getDuration());
        }
        this.a = null;
    }

    private VideoView ap() {
        if (this.O != null) {
            return (VideoView) this.O.findViewById(d.a);
        }
        return null;
    }

    private Runnable aq() {
        return new he(this);
    }

    private Runnable ar() {
        return new hf(this);
    }

    private Runnable as() {
        return new gw(this);
    }

    private Runnable at() {
        return new gx(this);
    }

    private boolean au() {
        float f2 = (float) this.l.b;
        if (this.k.i() <= this.l.b || this.k.b >= 0.0f) {
            return false;
        }
        float f3 = f2 + this.k.b;
        if (f3 > 0.0f) {
            f3 = 0.0f;
        }
        ar.a().a(this.k.a, f3);
        return true;
    }

    private boolean av() {
        float f2 = (float) this.l.b;
        if (this.k.i() <= this.l.b || this.k.d() <= ((float) this.l.b)) {
            return false;
        }
        float f3 = this.k.b - f2;
        if (((float) this.k.i()) + f3 <= ((float) this.l.b)) {
            f3 = (float) (this.l.b - this.k.i());
        }
        ar.a().a(this.k.a, f3);
        return true;
    }

    private void b(int i2, boolean z2) {
        if (getVisibility() == 0 && !b.b((Object[]) this.q)) {
            post(new fj(this, i2, z2));
        }
    }

    private void b(Context context) {
        try {
            a.a = m.f(context);
            a.e(getContext());
            this.b = PDF.a();
            this.b.a(this);
            this.k = hx.a();
            this.l = ic.a();
            this.E = new dz(this, getContext());
            addView(this.E, new FrameLayout.LayoutParams(-1, -1));
            this.H = new e(this);
            this.H.setVisibility(8);
            addView(this.H, new FrameLayout.LayoutParams(-1, -1));
            this.G = new hn(this);
            this.G.setVisibility(8);
            addView(this.G, new FrameLayout.LayoutParams(-1, -1));
            this.F = new cm(this);
            addView(this.F, new FrameLayout.LayoutParams(-1, -1));
            this.I = new az(getContext());
            addView(this.I, new FrameLayout.LayoutParams(-1, -1));
            this.K = new g(getContext());
            addView(this.K, new FrameLayout.LayoutParams(-1, -1));
            this.J = new ImageView(getContext());
            this.J.setVisibility(4);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            layoutParams.gravity = 17;
            addView(this.J, layoutParams);
            this.o = new ArrayList();
            if (a.h && a.i) {
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.v, new df(this)));
            }
            if (a.f) {
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.L, new dg(this)));
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.N, new dd(this)));
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.M, new de(this)));
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.D, aq()));
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.E, ar()));
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.F, as()));
                this.o.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.K, at()));
            }
            this.p = new ArrayList();
            if (a.f) {
                this.p.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.D, aq()));
                this.p.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.E, ar()));
                this.p.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.F, as()));
                this.p.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.K, at()));
            }
            this.m = new cl(this);
            this.A = ZoomService.a();
            this.A.a(this);
            this.C = ce.a();
            s.a().a(this);
            ar.a().a(this);
            this.D = null;
            if (a.aK) {
                this.z = new ax(this);
            }
        } catch (Throwable th) {
            udk.android.reader.b.c.a(th);
        }
    }

    /* access modifiers changed from: private */
    public void b(udk.android.reader.pdf.c cVar, MotionEvent motionEvent) {
        VideoView ap;
        int l2;
        switch (cVar.f()) {
            case 5:
                String h2 = cVar.h();
                if ("PrevPage".equals(h2)) {
                    n();
                    return;
                } else if ("NextPage".equals(h2)) {
                    o();
                    return;
                } else if ("FirstPage".equals(h2)) {
                    if (this.b.E() != 1 && !this.L) {
                        X();
                        ar.a().e();
                        this.b.j(1);
                        return;
                    }
                    return;
                } else if ("LastPage".equals(h2) && this.b.E() != (l2 = this.b.l()) && !this.L) {
                    X();
                    ar.a().e();
                    this.b.j(l2);
                    return;
                } else {
                    return;
                }
            case 6:
            case 7:
            case 8:
                Uri c2 = cVar.c();
                if (cVar.f() == 8) {
                    RectF b2 = cVar.b();
                    ao();
                    Context context = getContext();
                    this.a = MediaPlayer.create(context, c2);
                    if (this.a == null) {
                        post(new gd(this, context));
                        return;
                    }
                    SeekBar seekBar = new SeekBar(context);
                    FrameLayout frameLayout = new FrameLayout(context);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2);
                    layoutParams.gravity = 17;
                    frameLayout.addView(seekBar, layoutParams);
                    View view = new View(context);
                    view.setOnTouchListener(new gk(this));
                    frameLayout.addView(view, new FrameLayout.LayoutParams(-1, -1));
                    this.a.setOnErrorListener(new gi(this, frameLayout));
                    this.a.setOnCompletionListener(new go(this, frameLayout));
                    this.a.start();
                    this.I.a(frameLayout, b2);
                    new gn(this, frameLayout, seekBar).start();
                    return;
                }
                RectF b3 = cVar.b();
                FrameLayout frameLayout2 = new FrameLayout(getContext());
                FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -1);
                layoutParams2.gravity = 17;
                View view2 = new View(getContext());
                view2.setBackgroundColor(-1);
                view2.setOnTouchListener(new gm(this));
                frameLayout2.addView(view2, layoutParams2);
                gl glVar = new gl(this, getContext(), c2);
                glVar.setOnCompletionListener(new gt(this, frameLayout2));
                glVar.setId(d.a);
                glVar.setTag(c2.toString());
                glVar.setMediaController(null);
                glVar.setVideoURI(c2);
                glVar.start();
                frameLayout2.addView(glVar, layoutParams2);
                if (this.O != null) {
                    a(this.O);
                }
                this.I.a(frameLayout2, b3);
                this.O = frameLayout2;
                this.P = new dq(this);
                this.P.start();
                this.F.setVisibility(8);
                return;
            case 9:
            case 10:
            case 11:
            default:
                if (a.k && cVar.f() == 1) {
                    b(cVar.g());
                }
                hg hgVar = new hg();
                hgVar.a = this.b.E();
                hgVar.b = this.b.F();
                hgVar.c = motionEvent;
                hgVar.d = cVar;
                if (this.D != null) {
                    this.D.c(hgVar);
                    return;
                }
                return;
            case 12:
                udk.android.reader.pdf.c a2 = aj.a().a(cVar.d(), cVar.k());
                if (a2 == null) {
                    return;
                }
                if ("multimedia_play".equals(cVar.j())) {
                    VideoView ap2 = ap();
                    if (ap2 == null || !ap2.getTag().equals(a2.c().toString())) {
                        a(a2, (MotionEvent) null);
                        return;
                    } else if (!ap2.isPlaying()) {
                        ap2.start();
                        return;
                    } else {
                        return;
                    }
                } else if ("multimedia_pause".equals(cVar.j()) && (ap = ap()) != null && ap.getTag().equals(a2.c().toString())) {
                    ap.pause();
                    return;
                } else {
                    return;
                }
        }
    }

    private void b(boolean z2) {
        if (getVisibility() == 0 && !b.b((Object[]) this.x)) {
            for (View dsVar : this.x) {
                post(new ds(this, dsVar, z2));
            }
        }
    }

    private void c(boolean z2) {
        if (getVisibility() == 0 && !b.b((Object[]) this.y)) {
            for (View dtVar : this.y) {
                post(new dt(this, dtVar, z2));
            }
        }
    }

    private void d(int i2) {
        if (a.j) {
            new gc(this, i2).start();
        }
    }

    /* access modifiers changed from: private */
    public void d(boolean z2) {
        b(this.r, z2);
    }

    private void e(boolean z2) {
        b(this.s, z2);
    }

    private void f(boolean z2) {
        b(this.t, z2);
        post(new fh(this, z2));
    }

    private void g(boolean z2) {
        if (a.Q) {
            b(this.u, z2);
        }
    }

    private void h(boolean z2) {
        b(this.v, z2);
        b(this.w, z2);
    }

    /* access modifiers changed from: private */
    public void i(boolean z2) {
        if (this.b.w()) {
            if (z2) {
                X();
            }
            if (this.k.e() > this.l.a && !this.m.b()) {
                this.k.a = 0.0f;
            }
            if (this.k.i() > this.l.b && !this.m.d()) {
                this.k.b = (float) (this.l.b - this.k.i());
            }
            this.b.I();
        }
    }

    /* access modifiers changed from: private */
    public void j(boolean z2) {
        if (this.b.x()) {
            if (z2) {
                X();
            }
            ar.a().e();
            if (this.k.e() > this.l.a && !this.m.b()) {
                this.k.a = 0.0f;
            }
            if (this.k.i() > this.l.b && !this.m.d()) {
                this.k.b = 0.0f;
            }
            this.b.J();
        }
    }

    public final int A() {
        return this.b.l();
    }

    public final float B() {
        return this.b.F();
    }

    public final float C() {
        return this.A.f() ? this.B : this.b.F();
    }

    public final float D() {
        return this.G.a();
    }

    public final String E() {
        return this.b.y();
    }

    public final ViewMode F() {
        return this.G.getVisibility() == 0 ? ViewMode.TEXTREFLOW : this.H.getVisibility() == 0 ? ViewMode.THUMBNAIL : ViewMode.PDF;
    }

    public final String G() {
        if (!a.h) {
            return null;
        }
        return this.F.c().n();
    }

    public final void H() {
        this.F.c().v();
    }

    public final void I() {
        if (a.l) {
            this.F.c().l();
        }
    }

    public final boolean J() {
        return this.F.c().d();
    }

    public final int K() {
        return this.b.s();
    }

    public final int L() {
        return this.b.t();
    }

    public final in M() {
        return this.n;
    }

    public final void N() {
        if (a.n) {
            this.n.a();
            aj();
        }
    }

    public final boolean O() {
        return this.n.d();
    }

    public final void P() {
        if (a.n) {
            this.n.e();
        }
    }

    public final void Q() {
        if (a.n) {
            this.n.i();
        }
    }

    /* access modifiers changed from: package-private */
    public final void R() {
        TextView textView;
        try {
            s a2 = s.a();
            if (!a.f || !a2.b()) {
                if (a.h && this.F.c().o() && !b.b((Collection) this.o)) {
                    o c2 = this.F.c();
                    post(new dn(this, String.valueOf(c2.q().toString()) + c2.r().toString() + "_" + this.k.n() + this.k.a + this.k.b + this.k.c() + this.k.d(), this.F.c().p()));
                }
            } else if (!(a2.c() instanceof e)) {
                s a3 = s.a();
                Annotation c3 = a3.c();
                if (a.f && c3 != null) {
                    Context context = getContext();
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.d, a(a3, c3)));
                    arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.g, new hb(this, a3, c3)));
                    if (a.aK && (c3 instanceof udk.android.reader.pdf.annotation.e)) {
                        arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.e, new hc(this, (udk.android.reader.pdf.annotation.e) c3)));
                    }
                    arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.c, new fb(this, c3, a3)));
                    arrayList.add(new udk.android.reader.view.pdf.menu.c(udk.android.reader.b.b.f, new hd(this, a3, c3)));
                    if (!(c3 instanceof udk.android.reader.pdf.annotation.e)) {
                        TextView textView2 = new TextView(context);
                        textView2.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                        textView2.setTextSize(1, 10.0f);
                        String M2 = c3.M();
                        if (b.b(M2)) {
                            M2 = udk.android.reader.b.b.ab;
                        }
                        if (M2.length() > a.K) {
                            M2 = String.valueOf(M2.substring(0, a.K)) + "...";
                        }
                        textView2.setText(M2);
                        textView = textView2;
                    } else {
                        textView = null;
                    }
                    post(new Cdo(this, String.valueOf(c3.K()) + "_" + this.k.n() + this.k.a + this.k.b + this.k.c() + this.k.d(), c3, arrayList, textView));
                }
            }
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Throwable) e2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void S() {
        post(new dj(this));
    }

    /* access modifiers changed from: package-private */
    public final boolean T() {
        return this.K.b();
    }

    public final cl U() {
        return this.m;
    }

    public final o V() {
        return this.F.c();
    }

    /* access modifiers changed from: protected */
    public final void W() {
        cl clVar = this.m;
        if (clVar.b() || clVar.d()) {
            g(true);
        } else {
            g(false);
        }
    }

    /* access modifiers changed from: protected */
    public final void X() {
        if (!this.L && F() == ViewMode.PDF) {
            this.L = true;
            postDelayed(new gs(this), 500);
        }
    }

    /* access modifiers changed from: package-private */
    public final void Y() {
        if (this.L) {
            this.L = false;
            this.k.p();
            post(new gr(this));
        }
    }

    public final void Z() {
        s.a().d();
        R();
    }

    public final void a() {
        this.k.p();
    }

    public final void a(float f2) {
        float b2 = ZoomService.a().b(this.b.E());
        v();
        ViewMode F2 = F();
        if (F2 == ViewMode.PDF) {
            if (f2 > 0.0f && C() >= a.ac) {
                return;
            }
            if (f2 < 0.0f && C() <= b2) {
                return;
            }
        } else if (F2 == ViewMode.TEXTREFLOW) {
            if (f2 > 0.0f && this.G.a() >= a.ac) {
                return;
            }
            if (f2 < 0.0f && this.G.a() <= a.ab) {
                return;
            }
        }
        this.U = new eh(this, F2, f2);
        this.U.setDaemon(true);
        this.U.start();
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2, float f3) {
        if (!b.b((Collection) this.p)) {
            s a2 = s.a();
            if (a2.b()) {
                a2.d();
            }
            o c2 = this.F.c();
            if (c2.o()) {
                c2.v();
            }
            post(new dm(this, String.valueOf(f2) + "_" + f3 + "_" + this.k.n() + this.k.a + this.k.b + this.k.c() + this.k.d(), new udk.android.reader.pdf.a.b(this.b.E(), this.b.a(this.b.E(), this.b.F(), new int[]{(int) ((f2 - 5.0f) - this.k.a), (int) ((f3 - 5.0f) - this.k.b), (int) ((f2 + 5.0f) - this.k.a), (int) ((f3 + 5.0f) - this.k.b)}))));
        }
    }

    public final void a(int i2) {
        ZoomService.FittingType a2 = this.A.a(i2);
        if (a2 == ZoomService.FittingType.WIDTHFIT) {
            this.b.a(i2, getWidth());
            ar.a().e();
            PointF a3 = udk.android.b.c.a(new RectF(this.k.a, this.k.b, this.k.c(), this.k.d()), (float) this.l.a, (float) this.l.b);
            this.k.a = a3.x;
            this.k.b = a3.y;
            this.k.p();
        } else if (a2 == ZoomService.FittingType.HEIGHTFIT) {
            this.b.b(i2, getHeight());
            ar.a().e();
            PointF a4 = udk.android.b.c.a(new RectF(this.k.a, this.k.b, this.k.c(), this.k.d()), (float) this.l.a, (float) this.l.b);
            this.k.a = a4.x;
            this.k.b = a4.y;
            this.k.p();
        }
    }

    public final void a(int i2, float f2, RectF rectF) {
        this.b.f(i2, f2);
        ar.a().e();
        PointF a2 = udk.android.b.c.a(rectF, (float) this.l.a, (float) this.l.b);
        this.k.a = a2.x;
        this.k.b = a2.y;
        this.k.p();
    }

    public final void a(int i2, int i3) {
        ViewMode F2 = F();
        if (F2 == ViewMode.PDF) {
            try {
                float b2 = ZoomService.a().b(this.b.E());
                float f2 = b2 + (((a.ac - b2) / ((float) i3)) * ((float) i2));
                if (F() == ViewMode.PDF) {
                    ZoomService.a().b(f2);
                }
            } catch (Exception e2) {
                udk.android.reader.b.c.a((Throwable) e2);
            }
        } else if (F2 == ViewMode.TEXTREFLOW) {
            this.G.b(0.7f + ((7.3f / ((float) i3)) * ((float) i2)));
        }
    }

    public final void a(int i2, boolean z2) {
        this.b.a(i2, z2);
    }

    public final void a(AnimationDrawable animationDrawable) {
        this.J.setImageDrawable(animationDrawable);
    }

    /* access modifiers changed from: package-private */
    public final void a(MotionEvent motionEvent, udk.android.reader.pdf.annotation.e eVar) {
        if (this.z != null && this.z.a() == eVar) {
            int a2 = eVar.a(this.b.F(), motionEvent.getX() - this.k.a, motionEvent.getY() - this.k.b);
            if (a2 == eVar.M().length() - 1) {
                a2++;
            }
            this.z.a(a2);
            this.k.p();
        }
    }

    public final void a(InputStream inputStream, String str, String str2, int i2) {
        a((Activity) getContext(), this, (String) null, new ab(this, inputStream, str, str2, i2), (String) null);
    }

    public final void a(String str) {
        if (a.g) {
            this.b.o();
            a(str, (Runnable) null);
        }
    }

    public final void a(String str, String str2) {
        if (a.g && this.b.f()) {
            Context context = getContext();
            String y2 = this.b.y();
            udk.android.a.b bVar = new udk.android.a.b(context, new File(b.a(y2) ? y2.toLowerCase().endsWith(".pdf") ? y2.replaceAll("(?i)\\.pdf$", ".flatten.pdf") : String.valueOf(y2) + ".flatten.pdf" : a.b(context) + File.separator + this.b.z() + ".pdf"), null, null, true);
            bVar.setTitle(str);
            bVar.setButton(str2, new ag(this, bVar, context));
            bVar.show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String, java.lang.String, int, float, boolean, float, float, int):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, int]
     candidates:
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):void
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String, java.lang.String, int, float, boolean, float, float, int):void */
    public final void a(String str, String str2, String str3, int i2) {
        a(str, str2, str3, i2, 0.0f, true, 0.0f, 0.0f, -1);
    }

    public final void a(String str, String str2, String str3, int i2, float f2, boolean z2, float f3, float f4, int i3) {
        a((Activity) getContext(), this, (String) null, new ac(this, str, str2, str3, i2, f2, z2, f3, f4, i3), (String) null);
    }

    public final void a(String str, String str2, String str3, String str4, String str5, String str6) {
        if (this.n.d()) {
            N();
        }
        post(new ed(this, str, str2, str3, str4, str5, str6));
    }

    public final void a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        if (a.n) {
            if (dc.a().b()) {
                k();
            }
            this.n.a(new dp(this), str, str2, str3, str4, str5, str6, str7, str8, str9);
        }
    }

    public final void a(URL url, String str, String str2, int i2, i iVar) {
        a((Activity) getContext(), this, (String) null, new aa(this, url, str, str2, i2, iVar), (String) null);
    }

    public final void a(List list) {
        if (a.h) {
            this.o.addAll(0, list);
        }
    }

    public final void a(ah ahVar) {
        if (ahVar.c) {
            post(new hi(this));
            ao();
        }
    }

    public final void a(ab abVar) {
        Annotation annotation = abVar.a;
        if (annotation.h()) {
            this.k.c(abVar.a.ag(), this.b.F());
        } else if (annotation.U()) {
            this.k.c(abVar.a.ag(), this.b.F());
        } else {
            this.k.p();
        }
        if (a.s) {
            s.a().a(abVar.a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.aj.a(udk.android.reader.pdf.c, boolean):void
     arg types: [udk.android.reader.pdf.c, int]
     candidates:
      udk.android.reader.pdf.aj.a(int, int):udk.android.reader.pdf.c
      udk.android.reader.pdf.aj.a(udk.android.reader.pdf.c, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(udk.android.reader.pdf.c cVar, MotionEvent motionEvent) {
        aj a2 = aj.a();
        if (a2.a(cVar)) {
            new gb(this, a2, cVar, ProgressDialog.show(getContext(), null, udk.android.reader.b.b.b, true, false), motionEvent).start();
            return;
        }
        a2.a(cVar, false);
        a2.b(cVar);
        b(cVar, motionEvent);
    }

    public final void a(SmartNavDirection smartNavDirection) {
        if (this.b.f() && a.o && F() == ViewMode.PDF) {
            if (!this.k.t()) {
                o c2 = this.F.c();
                if (smartNavDirection == SmartNavDirection.TOP) {
                    if (!this.m.d() && !au() && !c2.d()) {
                        n();
                    }
                } else if (smartNavDirection == SmartNavDirection.BOTTOM) {
                    if (!this.m.d() && !av() && !c2.d()) {
                        o();
                    }
                } else if (smartNavDirection == SmartNavDirection.LEFT) {
                    if (c2.d()) {
                        c2.k();
                    } else if (!this.m.b()) {
                        float f2 = (float) this.l.a;
                        if (this.k.e() > this.l.a && this.k.a < 0.0f) {
                            float f3 = f2 + this.k.a;
                            if (f3 > 0.0f) {
                                f3 = 0.0f;
                            }
                            ar.a().a(f3, this.k.b);
                        }
                    }
                } else if (smartNavDirection != SmartNavDirection.RIGHT) {
                } else {
                    if (c2.d()) {
                        c2.l();
                    } else if (!this.m.b()) {
                        float f4 = (float) this.l.a;
                        if (this.k.e() > this.l.a && this.k.c() > ((float) this.l.a)) {
                            float f5 = this.k.a - f4;
                            if (((float) this.k.e()) + f5 <= ((float) this.l.b)) {
                                f5 = (float) (this.l.a - this.k.e());
                            }
                            ar.a().a(f5, this.k.b);
                        }
                    }
                }
            } else if (smartNavDirection == SmartNavDirection.RIGHT) {
                if (!this.b.u()) {
                    o();
                } else {
                    n();
                }
            } else if (smartNavDirection != SmartNavDirection.LEFT) {
            } else {
                if (!this.b.u()) {
                    n();
                } else {
                    o();
                }
            }
        }
    }

    public final void a(ViewMode viewMode) {
        if (a.m && F() != viewMode) {
            o c2 = this.F.c();
            if (c2.o()) {
                c2.v();
            }
            s a2 = s.a();
            if (a2.b()) {
                a2.d();
            }
            if (this.K.b()) {
                post(new ei(this));
            }
            View view = null;
            View[] viewArr = new View[2];
            if (viewMode == ViewMode.PDF) {
                view = this.F;
                viewArr[0] = this.G;
                viewArr[1] = this.H;
            } else if (viewMode == ViewMode.TEXTREFLOW) {
                view = this.G;
                viewArr[0] = this.F;
                viewArr[1] = this.H;
            } else if (viewMode == ViewMode.THUMBNAIL) {
                view = this.H;
                viewArr[0] = this.F;
                viewArr[1] = this.G;
            }
            view.setVisibility(0);
            for (View visibility : viewArr) {
                visibility.setVisibility(8);
            }
            if (viewMode == ViewMode.PDF) {
                X();
                this.k.p();
            }
            ah();
            if (this.D != null) {
                this.D.f();
            }
        }
    }

    public final void a(bm bmVar) {
    }

    public final void a(cg cgVar) {
        post(new ha(this, cgVar));
    }

    public final void a(cv cvVar) {
        this.D = cvVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(hg hgVar) {
        if (this.D != null) {
            this.D.b(hgVar);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(udk.android.reader.view.pdf.u r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            boolean r0 = r6.j
            if (r0 == 0) goto L_0x00f1
            r0 = r5
        L_0x0007:
            if (r0 == 0) goto L_0x003f
            r6.j = r4
            udk.android.reader.view.pdf.hg r1 = new udk.android.reader.view.pdf.hg
            r1.<init>()
            int r2 = r7.b
            r1.a = r2
            float r2 = r7.d
            r1.b = r2
            udk.android.reader.view.pdf.ar r2 = udk.android.reader.view.pdf.ar.a()
            r2.c()
            r6.d(r5)
            int r2 = r7.b
            r6.d(r2)
            r6.S()
            java.lang.Runnable r2 = r6.h
            if (r2 == 0) goto L_0x00f4
            java.lang.Runnable r1 = r6.h
            r1.run()
            r1 = 0
            r6.h = r1
            udk.android.reader.view.pdf.cv r1 = r6.D
            if (r1 == 0) goto L_0x003f
            udk.android.reader.view.pdf.cv r1 = r6.D
            r1.b()
        L_0x003f:
            boolean r1 = r7.a
            if (r1 == 0) goto L_0x0070
            boolean r1 = r6.L
            if (r1 == 0) goto L_0x004f
            udk.android.reader.view.pdf.gy r1 = new udk.android.reader.view.pdf.gy
            r1.<init>(r6)
            r6.post(r1)
        L_0x004f:
            if (r0 != 0) goto L_0x0070
            udk.android.reader.view.pdf.hg r1 = new udk.android.reader.view.pdf.hg
            r1.<init>()
            int r2 = r7.b
            r1.a = r2
            float r2 = r7.d
            r1.b = r2
            udk.android.reader.view.pdf.ar r2 = udk.android.reader.view.pdf.ar.a()
            r2.b()
            int r2 = r7.b
            r6.d(r2)
            r6.S()
            r6.a(r1)
        L_0x0070:
            boolean r1 = r7.c
            if (r1 == 0) goto L_0x008d
            if (r0 != 0) goto L_0x008d
            udk.android.reader.view.pdf.hg r0 = new udk.android.reader.view.pdf.hg
            r0.<init>()
            int r1 = r7.b
            r0.a = r1
            float r1 = r7.d
            r0.b = r1
            udk.android.reader.view.pdf.ar r0 = udk.android.reader.view.pdf.ar.a()
            r0.b()
            r6.af()
        L_0x008d:
            int r0 = r7.f
            if (r0 != r4) goto L_0x010b
            int r0 = udk.android.reader.b.a.R
            if (r0 != r4) goto L_0x00e3
            udk.android.reader.view.pdf.hx r0 = r6.k
            int r0 = r0.e()
            udk.android.reader.view.pdf.ic r1 = r6.l
            int r1 = r1.a
            if (r0 <= r1) goto L_0x00ff
            r0 = r4
        L_0x00a2:
            udk.android.reader.view.pdf.hx r1 = r6.k
            int r1 = r1.i()
            udk.android.reader.view.pdf.ic r2 = r6.l
            int r2 = r2.b
            if (r1 <= r2) goto L_0x0101
            r1 = r4
        L_0x00af:
            if (r0 != 0) goto L_0x00b3
            if (r1 == 0) goto L_0x0105
        L_0x00b3:
            udk.android.reader.view.pdf.hx r2 = r6.k
            float r2 = r2.b
            r3 = 0
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 < 0) goto L_0x0103
            r2 = r4
        L_0x00bd:
            if (r2 == 0) goto L_0x0105
            r2 = r4
        L_0x00c0:
            if (r0 != 0) goto L_0x00c4
            if (r1 == 0) goto L_0x0107
        L_0x00c4:
            udk.android.reader.view.pdf.hx r0 = r6.k
            boolean r0 = r0.o()
            if (r0 == 0) goto L_0x0107
            r0 = r4
        L_0x00cd:
            boolean r1 = r7.a
            if (r1 != 0) goto L_0x0109
            boolean r1 = r6.M
            if (r2 != r1) goto L_0x0109
            boolean r1 = r6.N
            if (r0 != r1) goto L_0x0109
            r1 = r5
        L_0x00da:
            r6.M = r2
            r6.N = r0
            if (r1 == 0) goto L_0x00e3
            r6.al()
        L_0x00e3:
            udk.android.reader.view.pdf.az r0 = r6.I
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00f0
            udk.android.reader.view.pdf.az r0 = r6.I
            r0.c()
        L_0x00f0:
            return
        L_0x00f1:
            r0 = r4
            goto L_0x0007
        L_0x00f4:
            udk.android.reader.view.pdf.cv r2 = r6.D
            if (r2 == 0) goto L_0x003f
            udk.android.reader.view.pdf.cv r2 = r6.D
            r2.a(r1)
            goto L_0x003f
        L_0x00ff:
            r0 = r5
            goto L_0x00a2
        L_0x0101:
            r1 = r5
            goto L_0x00af
        L_0x0103:
            r2 = r5
            goto L_0x00bd
        L_0x0105:
            r2 = r5
            goto L_0x00c0
        L_0x0107:
            r0 = r5
            goto L_0x00cd
        L_0x0109:
            r1 = r4
            goto L_0x00da
        L_0x010b:
            int r0 = r7.f
            r1 = 2
            if (r0 != r1) goto L_0x00f0
            android.graphics.RectF r0 = r7.g
            if (r0 == 0) goto L_0x0123
            udk.android.reader.view.pdf.az r0 = r6.I
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0123
            udk.android.reader.view.pdf.az r0 = r6.I
            android.graphics.RectF r1 = r7.g
            r0.a(r1)
        L_0x0123:
            float r0 = r7.e
            r6.B = r0
            goto L_0x00f0
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.u):void");
    }

    public final void a(v vVar) {
        this.F.b().a(vVar);
    }

    public final void a(boolean z2) {
        this.G.a(z2);
    }

    public final void a(View[] viewArr, View[] viewArr2, View[] viewArr3) {
        this.q = viewArr;
        this.r = C0000R.id.toolbtn_link;
        this.s = C0000R.id.toolbtn_auto_page_flipping;
        this.t = C0000R.id.toolbtn_voice_reading;
        this.u = C0000R.id.toolbtn_scrolllock_deactivate;
        this.v = C0000R.id.toolbtn_column_prev;
        this.w = C0000R.id.toolbtn_column_next;
        this.x = viewArr2;
        this.y = viewArr3;
        an();
    }

    public final void aa() {
        this.k.p();
        S();
    }

    public final void ab() {
        post(new hj(this));
    }

    public final void ac() {
    }

    public final void ad() {
        am();
        hg hgVar = new hg();
        hgVar.a = this.b.E();
        hgVar.b = this.b.F();
        if (this.D != null) {
            this.D.e();
        }
    }

    public final void ae() {
        am();
        hg hgVar = new hg();
        hgVar.a = this.b.E();
        hgVar.b = this.b.F();
    }

    /* access modifiers changed from: package-private */
    public final void af() {
        if (this.D != null) {
            this.D.c();
        }
    }

    /* access modifiers changed from: package-private */
    public final void ag() {
        if (this.D != null) {
            this.D.d();
        }
    }

    public final void b() {
        X();
    }

    public final void b(int i2) {
        if (this.b.f(i2) && this.b.E() != i2) {
            X();
            ar.a().e();
            this.b.j(i2);
        }
    }

    public final void b(int i2, int i3) {
        ViewMode F2 = F();
        this.Q = 0;
        this.R = i2;
        this.S = 0;
        this.T = i3;
        if (F2 == ViewMode.TEXTREFLOW) {
            hn hnVar = this.G;
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.leftMargin = 0;
            layoutParams.topMargin = i2;
            layoutParams.rightMargin = 0;
            layoutParams.bottomMargin = i3;
            hnVar.b.setLayoutParams(layoutParams);
        }
    }

    public final void b(String str) {
        if (this.b.f()) {
            a(str, new y(this));
        }
    }

    public final void b(String str, String str2) {
        if (this.b.f()) {
            Context context = getContext();
            String y2 = this.b.y();
            udk.android.a.b bVar = new udk.android.a.b(context, new File(b.a(y2) ? y2.toLowerCase().endsWith(".pdf") ? y2.replaceAll("(?i)\\.pdf$", ".copy.pdf") : String.valueOf(y2) + ".copy.pdf" : a.b(context) + File.separator + this.b.z() + ".pdf"), null, null, true);
            bVar.setTitle(str);
            bVar.setButton(str2, new z(this, bVar, context));
            bVar.show();
        }
    }

    public final void b(ah ahVar) {
    }

    public final void b(ab abVar) {
        if (abVar.a != null) {
            Annotation annotation = abVar.a;
            if (annotation.h()) {
                this.k.c(annotation.ag(), this.b.F());
            } else if (annotation.U()) {
                this.k.c(annotation.ag(), this.b.F());
            } else {
                this.k.p();
            }
        } else if (b.a((Collection) abVar.b)) {
            TreeSet<Integer> treeSet = new TreeSet<>();
            boolean z2 = false;
            for (Annotation annotation2 : abVar.b) {
                if (annotation2.h()) {
                    treeSet.add(Integer.valueOf(annotation2.ag()));
                } else if (annotation2.U()) {
                    treeSet.add(Integer.valueOf(annotation2.ag()));
                } else {
                    z2 = true;
                }
            }
            boolean z3 = false;
            for (Integer intValue : treeSet) {
                int intValue2 = intValue.intValue();
                this.k.c(intValue2, this.b.F());
                if (intValue2 == this.b.E()) {
                    z3 = true;
                }
            }
            if (z2 && !z3) {
                this.k.p();
            }
        }
    }

    public final void b(bm bmVar) {
        this.k.a = bmVar.e.left;
        this.k.b = bmVar.e.top;
        if (this.b.F() != bmVar.d) {
            if (bmVar.d <= this.A.b(this.b.E())) {
                ZoomService.FittingType a2 = this.A.a(this.b.E());
                if (a2 == ZoomService.FittingType.WIDTHFIT) {
                    this.b.a(this.b.E(), this.l.a);
                } else if (a2 == ZoomService.FittingType.HEIGHTFIT) {
                    this.b.b(this.b.E(), this.l.b);
                }
            } else {
                this.b.f(this.b.E(), bmVar.d);
            }
        }
        if (!bmVar.f) {
            this.m.e();
        }
        ar.a().b();
    }

    public final void b_() {
    }

    public final int c(int i2) {
        ViewMode F2 = F();
        if (F2 == ViewMode.PDF) {
            try {
                float b2 = ZoomService.a().b(this.b.E());
                return (int) ((((this.A.f() ? this.B : this.k.n()) - b2) / (a.ac - b2)) * ((float) i2));
            } catch (Exception e2) {
                return 0;
            }
        } else if (F2 == ViewMode.TEXTREFLOW) {
            return (int) (((this.G.a() - 0.7f) / 7.3f) * ((float) i2));
        } else {
            return 0;
        }
    }

    public final void c() {
        Y();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.hx.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.hx.a(int, float):float
      udk.android.reader.view.pdf.hx.a(java.lang.String, float):void
      udk.android.reader.view.pdf.hx.a(boolean, boolean):boolean */
    public final void c(int i2, int i3) {
        if (!(this.l.a == i2 && this.l.b == i3)) {
            this.k.y();
        }
        this.k.x();
        n.b().c();
        this.l.a(i2, i3);
        PDF a2 = PDF.a();
        if (a2.f()) {
            if (this.F.c().d()) {
                this.F.c().m();
            } else {
                float b2 = this.A.b(a2.E());
                if (a2.G() || a2.H() || a2.F() <= b2) {
                    ZoomService.FittingType a3 = this.A.a(a2.E());
                    if (a3 == ZoomService.FittingType.WIDTHFIT) {
                        a2.a(a2.E(), this.l.a);
                    } else if (a3 == ZoomService.FittingType.HEIGHTFIT) {
                        a2.b(a2.E(), this.l.b);
                    }
                }
                if (a2.A() > 0 && a2.B() > 0) {
                    this.k.a(false, false);
                    this.k.p();
                }
            }
        }
        hx.a().p();
    }

    public final void c(ab abVar) {
        Annotation annotation = abVar.a;
        if (annotation.h()) {
            this.k.c(abVar.a.ag(), this.b.F());
        } else if (annotation.U()) {
            this.k.c(abVar.a.ag(), this.b.F());
        } else {
            this.k.p();
        }
    }

    public final void d() {
        R();
    }

    public final void d(ab abVar) {
        if ((abVar.a instanceof e) && !((e) abVar.a).a().i()) {
            if (abVar.a instanceof v) {
                udk.android.reader.pdf.b.d.a().a((v) abVar.a);
            } else if (abVar.a instanceof udk.android.reader.pdf.annotation.i) {
                udk.android.reader.pdf.b.d.a().a((udk.android.reader.pdf.annotation.i) abVar.a, getContext());
            }
        }
    }

    public final void e() {
        post(new gz(this));
    }

    public final void e(ab abVar) {
        if (abVar.a == null) {
            S();
        }
        boolean z2 = abVar.c != null && a.aK && (abVar.c instanceof udk.android.reader.pdf.annotation.e);
        boolean z3 = abVar.a != null && a.aK && (abVar.a instanceof udk.android.reader.pdf.annotation.d) && !((udk.android.reader.pdf.annotation.d) abVar.a).a().i();
        if (z2) {
            this.z.a(this, abVar.c != this.z.a() ? null : new hk(this, abVar), !z3);
        }
        if (abVar.a != null) {
            this.F.c().v();
            R();
        }
        boolean z4 = abVar.a != null && abVar.a.a_() && abVar.a.U() && abVar.a.v();
        if (z3) {
            post(new hl(this, abVar));
        }
        if ((abVar.c == null || abVar.c == null || !abVar.c.a_() || !abVar.c.w()) ? z4 : true) {
            this.k.c(this.b.E(), this.b.F());
        } else {
            this.k.p();
        }
    }

    public final void f() {
        if (this.b.q() >= 3) {
            a.ah = true;
            a.ag = true;
            a.af = false;
            a.al = false;
        }
    }

    public final void g() {
    }

    public final void h() {
        ao();
    }

    public final boolean i() {
        if (!this.b.f()) {
            return false;
        }
        if (dc.a().b()) {
            post(new x(this));
        }
        j();
        s a2 = s.a();
        if (a2.b()) {
            a2.d();
        }
        this.b.e();
        return true;
    }

    public final void j() {
        if (a.aR && this.b.f() && b.a(this.b.y()) && this.b.E() > 0) {
            ag v2 = this.b.v();
            v2.a("lastreadpage", Integer.valueOf(this.b.E()));
            v2.a("lastreadzoom", Float.valueOf(this.b.F()));
            v2.a("lastreadx", Float.valueOf(this.k.a));
            v2.a("lastready", Float.valueOf(this.k.b));
            v2.a("lastreadforefit", Boolean.valueOf(this.b.G() || this.b.H()));
            o c2 = this.F.c();
            v2.a("lastreadcolumn", Integer.valueOf(c2.d() ? c2.c() : -1));
            v2.a();
        }
    }

    public final void k() {
        post(new ee(this));
    }

    public final boolean l() {
        if (F() == ViewMode.PDF) {
            if (!this.k.t() && au()) {
                return true;
            }
            if (!this.b.w()) {
                return false;
            }
            n();
            return true;
        } else if (F() != ViewMode.TEXTREFLOW) {
            return false;
        } else {
            post(new ef(this));
            return this.b.w();
        }
    }

    public final boolean m() {
        if (F() == ViewMode.PDF) {
            if (!this.k.t() && av()) {
                return true;
            }
            if (!this.b.x()) {
                return false;
            }
            o();
            return true;
        } else if (F() != ViewMode.TEXTREFLOW) {
            return false;
        } else {
            post(new eg(this));
            return this.b.x();
        }
    }

    public final void n() {
        if (!cl.f() && this.b.w()) {
            if (!this.F.d()) {
                if (this.k.t()) {
                    a(this.b.E() - 1);
                } else {
                    a(this.b.E() - 1, this.b.F(), this.k.b());
                }
            } else if (!this.k.t() || !a.an) {
                i(true);
            } else if (!this.C.b()) {
                new ej(this.C).start();
            }
        }
    }

    public final void o() {
        if (!cl.f() && this.b.x()) {
            if (!this.F.d()) {
                if (this.k.t()) {
                    a(this.b.E() + 1);
                } else {
                    a(this.b.E() + 1, this.b.F(), this.k.b());
                }
            } else if (!this.k.t() || !a.an) {
                j(true);
            } else if (!this.C.b()) {
                new ek(this.C).start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        udk.android.reader.b.c.a("## PDFVIEW ON ATTACHED TO WINDOW");
        super.onAttachedToWindow();
        this.n = new in(this);
        this.C.a = this;
    }

    public final boolean onCheckIsTextEditor() {
        return a.aK;
    }

    public final InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        udk.android.reader.b.c.a("## ON CREATE INPUT CONNECTION");
        editorInfo.actionLabel = null;
        editorInfo.label = "ezPDFView";
        editorInfo.inputType = 524288;
        editorInfo.imeOptions = 268435461;
        return this.z;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        udk.android.reader.b.c.a("## PDFVIEW ON DETACHED FROM WINDOW");
        ao();
        this.F.e();
        this.n.k();
        this.b.b(this);
        this.A.b(this);
        this.C.a = null;
        s.a().b(this);
        ar.a().b(this);
        super.onDetachedFromWindow();
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (a.aK && this.z != null) {
            this.z.a(keyEvent);
        }
        return super.onKeyUp(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i2, int i3, int i4, int i5) {
        udk.android.reader.b.c.a("## ON SIZE CHANGED");
        super.onSizeChanged(i2, i3, i4, i5);
        c(i2, i3);
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        return this.m.a(motionEvent);
    }

    public final void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        this.k.p();
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (i2 == 0) {
            ah();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean p() {
        if (this.k.t()) {
            return false;
        }
        if ((a.R == 3 && this.k.k()) || (a.R == 2 && this.k.m())) {
            return false;
        }
        if (a.U && this.F.c().d()) {
            return false;
        }
        int E2 = this.b.E();
        float F2 = this.b.F();
        if (a.R == 2) {
            int i2 = E2 - 1;
            int i3 = E2 + 1;
            float f2 = ((float) this.l.b) * a.T;
            if (this.k.b - a.S > f2 && this.b.f(i2)) {
                float a2 = this.k.a(i2, F2);
                float b2 = this.k.b(i2, F2);
                this.F.b().a(false);
                this.k.a -= (a2 - ((float) this.k.e())) / 2.0f;
                this.k.b -= b2 + a.S;
                this.b.j(i2);
                this.F.b().a(true);
                return true;
            } else if (this.k.d() + a.S < ((float) this.l.b) - f2 && this.b.f(i3)) {
                float a3 = this.k.a(i3, F2);
                this.F.b().a(false);
                this.k.a -= (a3 - ((float) this.k.e())) / 2.0f;
                this.k.b += ((float) this.k.i()) + a.S;
                this.b.j(i3);
                this.F.b().a(true);
                return true;
            }
        } else if (a.R == 3) {
            int i4 = this.b.s() == 1 ? 1 : -1;
            int i5 = E2 - i4;
            int i6 = E2 + i4;
            float f3 = ((float) this.l.a) * a.T;
            if (this.k.a - a.S > f3 && this.b.f(i5)) {
                float a4 = this.k.a(i5, F2);
                float b3 = this.k.b(i5, F2);
                this.F.b().a(false);
                this.k.a -= a4 + a.S;
                this.k.b -= (b3 - ((float) this.k.i())) / 2.0f;
                this.b.j(i5);
                this.F.b().a(true);
                return true;
            } else if (this.k.c() + a.S < ((float) this.l.a) - f3 && this.b.f(i6)) {
                float b4 = this.k.b(i6, F2);
                this.F.b().a(false);
                this.k.a += ((float) this.k.e()) + a.S;
                this.k.b -= (b4 - ((float) this.k.i())) / 2.0f;
                this.b.j(i6);
                this.F.b().a(true);
                return true;
            }
        }
        return false;
    }

    public final void q() {
        ZoomService.a().b((float) (getWidth() / 2), (float) (getHeight() / 2));
    }

    public final void r() {
        ViewMode F2 = F();
        if (F2 == ViewMode.PDF) {
            q();
        } else if (F2 == ViewMode.TEXTREFLOW) {
            this.G.b(1.0f);
        }
    }

    public final void s() {
        if (F() == ViewMode.PDF) {
            ZoomService.a().c();
        }
    }

    public final void setVisibility(int i2) {
        if (i2 != 0) {
            ai();
        }
        super.setVisibility(i2);
        this.F.a(i2);
        if (i2 == 0) {
            ah();
            an();
        }
    }

    public final void t() {
        if (F() == ViewMode.PDF) {
            ZoomService.a().d();
        }
    }

    public final boolean u() {
        return this.U != null && this.U.isAlive();
    }

    public final void v() {
        if (this.U != null && this.U.isAlive()) {
            Thread thread = this.U;
            this.U = null;
            try {
                if (!Thread.currentThread().equals(thread)) {
                    thread.join();
                }
            } catch (Exception e2) {
                udk.android.reader.b.c.a((Throwable) e2);
            }
        }
    }

    public final boolean w() {
        return this.b.f();
    }

    public final boolean x() {
        return this.b.h();
    }

    public final boolean y() {
        return this.k.t();
    }

    public final int z() {
        return this.b.E();
    }
}
