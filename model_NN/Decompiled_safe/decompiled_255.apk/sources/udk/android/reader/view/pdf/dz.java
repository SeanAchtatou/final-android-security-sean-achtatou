package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;
import udk.android.reader.b.c;

final class dz extends View {
    private /* synthetic */ PDFView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    dz(PDFView pDFView, Context context) {
        super(context);
        this.a = pDFView;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        Rect clipBounds = canvas.getClipBounds();
        boolean z = (clipBounds.width() == getWidth() && clipBounds.height() == getHeight()) ? false : true;
        super.onDraw(canvas);
        if (this.a.O != null && !z) {
            try {
                this.a.F.b().a(canvas, this.a.k.u());
            } catch (Exception e) {
                c.a((Throwable) e);
            }
        }
    }
}
