package udk.android.reader.view.pdf;

import android.app.ProgressDialog;
import android.view.MotionEvent;
import udk.android.reader.pdf.aj;
import udk.android.reader.pdf.c;

final class gb extends Thread {
    final /* synthetic */ PDFView a;
    private final /* synthetic */ aj b;
    private final /* synthetic */ c c;
    private final /* synthetic */ ProgressDialog d;
    private final /* synthetic */ MotionEvent e;

    gb(PDFView pDFView, aj ajVar, c cVar, ProgressDialog progressDialog, MotionEvent motionEvent) {
        this.a = pDFView;
        this.b = ajVar;
        this.c = cVar;
        this.d = progressDialog;
        this.e = motionEvent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.aj.a(udk.android.reader.pdf.c, boolean):void
     arg types: [udk.android.reader.pdf.c, int]
     candidates:
      udk.android.reader.pdf.aj.a(int, int):udk.android.reader.pdf.c
      udk.android.reader.pdf.aj.a(udk.android.reader.pdf.c, boolean):void */
    public final void run() {
        this.b.a(this.c, true);
        this.b.b(this.c);
        this.a.post(new id(this, this.d, this.c, this.e));
    }
}
