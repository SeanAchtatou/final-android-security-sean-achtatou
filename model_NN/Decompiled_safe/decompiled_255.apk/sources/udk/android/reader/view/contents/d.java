package udk.android.reader.view.contents;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.io.File;
import java.util.Collection;
import java.util.List;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.contents.FileDirectory;
import udk.android.reader.contents.b;

final class d extends BaseExpandableListAdapter {
    private b a = b.a();

    /* access modifiers changed from: private */
    /* renamed from: a */
    public File getChild(int i, int i2) {
        return getGroup(i).getFile(i2);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public FileDirectory getGroup(int i) {
        return (FileDirectory) this.a.b().get(i);
    }

    public final long getChildId(int i, int i2) {
        return getGroupId(i) + ((long) i2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View getChildView(int r7, int r8, boolean r9, android.view.View r10, android.view.ViewGroup r11) {
        /*
            r6 = this;
            r2 = 0
            r5 = 2130837554(0x7f020032, float:1.7280065E38)
            if (r10 != 0) goto L_0x00ca
            android.content.Context r0 = r11.getContext()
            r1 = 2130903045(0x7f030005, float:1.7412897E38)
            android.view.View r1 = android.view.View.inflate(r0, r1, r2)
            android.content.Context r0 = r11.getContext()
            android.app.Activity r0 = (android.app.Activity) r0
            r0.registerForContextMenu(r1)
        L_0x001a:
            java.io.File r0 = r6.getChild(r7, r8)     // Catch:{ Throwable -> 0x0091 }
            r2 = r0
        L_0x001f:
            r0 = 2131427344(0x7f0b0010, float:1.8476302E38)
            android.view.View r0 = r1.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            if (r2 == 0) goto L_0x00a1
            android.content.Context r3 = r11.getContext()
            java.lang.String r3 = udk.android.reader.b.a.c(r3, r2)
            if (r3 == 0) goto L_0x00b1
            java.io.File r4 = new java.io.File
            r4.<init>(r3)
            boolean r4 = r4.exists()
            if (r4 == 0) goto L_0x00b1
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeFile(r3)     // Catch:{ OutOfMemoryError -> 0x0096 }
            r0.setImageBitmap(r3)     // Catch:{ OutOfMemoryError -> 0x0096 }
        L_0x0046:
            udk.android.reader.view.contents.e r0 = new udk.android.reader.view.contents.e
            r0.<init>(r6, r2, r11, r1)
            r1.setOnClickListener(r0)
            r0 = 2131427345(0x7f0b0011, float:1.8476304E38)
            android.view.View r0 = r1.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            if (r2 == 0) goto L_0x00c1
            java.lang.String r3 = r2.getName()
        L_0x005d:
            r0.setText(r3)
            r0 = 2131427346(0x7f0b0012, float:1.8476306E38)
            android.view.View r0 = r1.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            if (r2 == 0) goto L_0x00c4
            java.lang.String r3 = udk.android.reader.contents.ai.a(r2)
        L_0x006f:
            r0.setText(r3)
            r0 = 2131427347(0x7f0b0013, float:1.8476308E38)
            android.view.View r0 = r1.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            if (r2 == 0) goto L_0x00c7
            java.lang.String r3 = udk.android.reader.contents.ai.b(r2)
        L_0x0081:
            r0.setText(r3)
            if (r2 != 0) goto L_0x0090
            udk.android.reader.view.contents.g r0 = new udk.android.reader.view.contents.g
            r0.<init>(r6)
            r2 = 100
            r1.postDelayed(r0, r2)
        L_0x0090:
            return r1
        L_0x0091:
            r0 = move-exception
            udk.android.reader.b.c.a(r0)
            goto L_0x001f
        L_0x0096:
            r3 = move-exception
            java.lang.String r4 = r3.getMessage()
            udk.android.reader.b.c.a(r4, r3)
            java.lang.System.gc()
        L_0x00a1:
            android.content.Context r3 = r11.getContext()
            android.content.res.Resources r3 = r3.getResources()
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeResource(r3, r5)
            r0.setImageBitmap(r3)
            goto L_0x0046
        L_0x00b1:
            android.content.Context r3 = r11.getContext()
            android.content.res.Resources r3 = r3.getResources()
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeResource(r3, r5)
            r0.setImageBitmap(r3)
            goto L_0x0046
        L_0x00c1:
            java.lang.String r3 = ""
            goto L_0x005d
        L_0x00c4:
            java.lang.String r3 = ""
            goto L_0x006f
        L_0x00c7:
            java.lang.String r3 = ""
            goto L_0x0081
        L_0x00ca:
            r1 = r10
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.contents.d.getChildView(int, int, boolean, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public final int getChildrenCount(int i) {
        return getGroup(i).size();
    }

    public final int getGroupCount() {
        List b = this.a.b();
        if (com.unidocs.commonlib.util.b.a((Collection) b)) {
            return b.size();
        }
        return 0;
    }

    public final long getGroupId(int i) {
        return (long) (i * 10000);
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        FileDirectory fileDirectory = null;
        View inflate = view == null ? View.inflate(viewGroup.getContext(), C0000R.layout.fs_directory, null) : view;
        try {
            fileDirectory = getGroup(i);
        } catch (Throwable th) {
            c.a(th);
        }
        ((TextView) inflate.findViewById(C0000R.id.title)).setText(fileDirectory != null ? fileDirectory.getDir().getAbsolutePath() : "");
        if (fileDirectory != null) {
            try {
                File dir = fileDirectory.getDir();
                inflate.setOnClickListener(new f(this, viewGroup, i));
                inflate.setOnLongClickListener(new i(this, viewGroup, dir));
            } catch (Throwable th2) {
                c.a(th2);
            }
        }
        if (fileDirectory == null) {
            inflate.postDelayed(new h(this), 100);
        }
        return inflate;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
