package udk.android.reader.view.pdf;

import udk.android.reader.b.c;

final class dq extends Thread {
    private /* synthetic */ PDFView a;

    dq(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void run() {
        while (this.a.O != null) {
            try {
                Thread.sleep(10);
            } catch (Exception e) {
                c.a((Throwable) e);
            }
            this.a.E.postInvalidate();
        }
    }
}
