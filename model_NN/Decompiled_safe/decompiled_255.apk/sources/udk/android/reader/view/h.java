package udk.android.reader.view;

import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.view.pdf.PDFView;
import udk.android.reader.view.pdf.db;

final class h implements db {
    private /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    public final void a() {
        this.a.a();
    }

    public final void a(Annotation annotation) {
        if (this.a.b.F() != PDFView.ViewMode.PDF) {
            this.a.b.a(PDFView.ViewMode.PDF);
        }
        this.a.b.a(annotation.ag());
        s.a().a(annotation);
        this.a.a();
    }
}
