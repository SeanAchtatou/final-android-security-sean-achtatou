package udk.android.reader.view.pdf.navigation;

import android.widget.AdapterView;

final class j implements Runnable {
    private /* synthetic */ l a;
    private final /* synthetic */ AdapterView b;
    private final /* synthetic */ int c;

    j(l lVar, AdapterView adapterView, int i) {
        this.a = lVar;
        this.b = adapterView;
        this.c = i;
    }

    public final void run() {
        this.a.a.a.a(((Integer) this.b.getAdapter().getItem(this.c)).intValue());
    }
}
