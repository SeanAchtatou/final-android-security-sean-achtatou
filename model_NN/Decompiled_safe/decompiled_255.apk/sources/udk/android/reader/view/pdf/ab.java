package udk.android.reader.view.pdf;

import java.io.InputStream;

final class ab implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ InputStream b;
    private final /* synthetic */ long c = 0;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ int f;
    private final /* synthetic */ float g;
    private final /* synthetic */ boolean h;
    private final /* synthetic */ float i;
    private final /* synthetic */ float j;
    private final /* synthetic */ int k;

    ab(PDFView pDFView, InputStream inputStream, String str, String str2, int i2) {
        this.a = pDFView;
        this.b = inputStream;
        this.d = str;
        this.e = str2;
        this.f = i2;
        this.g = 0.0f;
        this.h = true;
        this.i = 0.0f;
        this.j = 0.0f;
        this.k = -1;
    }

    public final void run() {
        PDFView.a(this.a, null, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, 0);
    }
}
