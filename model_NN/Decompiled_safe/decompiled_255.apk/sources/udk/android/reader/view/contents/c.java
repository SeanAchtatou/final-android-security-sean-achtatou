package udk.android.reader.view.contents;

import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import udk.android.reader.C0000R;
import udk.android.reader.ContentsManagerActivity;
import udk.android.reader.contents.ai;
import udk.android.reader.contents.an;

public final class c extends BaseAdapter {
    /* access modifiers changed from: private */
    public an a = an.c();
    /* access modifiers changed from: private */
    public ContentsManagerActivity b;

    public c(ContentsManagerActivity contentsManagerActivity) {
        this.b = contentsManagerActivity;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public ai getItem(int i) {
        return (ai) this.a.e(this.b).get(i);
    }

    public final int getCount() {
        try {
            return this.a.e(this.b).size();
        } catch (Exception e) {
            udk.android.reader.b.c.a(e.getMessage(), e);
            return 0;
        }
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        ai a2 = getItem(i);
        if (view == null) {
            View inflate = View.inflate(this.b, C0000R.layout.content, null);
            this.b.registerForContextMenu(inflate);
            view2 = inflate;
        } else {
            view2 = view;
        }
        CheckBox checkBox = (CheckBox) view2.findViewById(C0000R.id.select_content);
        checkBox.setVisibility((!this.a.d() || a2.c().equals("..")) ? 8 : 0);
        checkBox.setChecked(this.a.c(a2));
        checkBox.setOnClickListener(new m(this, checkBox, a2));
        ImageView imageView = (ImageView) view2.findViewById(C0000R.id.thumbnail);
        if (a2.f() == 21 && a2.e() != null) {
            try {
                imageView.setImageBitmap(BitmapFactory.decodeFile(a2.e()));
            } catch (OutOfMemoryError e) {
                udk.android.reader.b.c.a(e.getMessage(), e);
                System.gc();
                if (a2.f() == 11) {
                    imageView.setImageBitmap(BitmapFactory.decodeResource(view2.getContext().getResources(), C0000R.drawable.icon_folder));
                } else {
                    imageView.setImageBitmap(BitmapFactory.decodeResource(viewGroup.getContext().getResources(), C0000R.drawable.icon_pdf));
                }
            }
        } else if (a2.f() == 11) {
            imageView.setImageBitmap(BitmapFactory.decodeResource(view2.getContext().getResources(), C0000R.drawable.icon_folder));
        } else {
            imageView.setImageBitmap(BitmapFactory.decodeResource(viewGroup.getContext().getResources(), C0000R.drawable.icon_pdf));
        }
        ((TextView) view2.findViewById(C0000R.id.title)).setText(a2.c());
        ((TextView) view2.findViewById(C0000R.id.last_modified)).setText(a2.a());
        ((TextView) view2.findViewById(C0000R.id.size)).setText(a2.b());
        view2.setOnClickListener(new l(this, a2, checkBox, view2));
        return view2;
    }
}
