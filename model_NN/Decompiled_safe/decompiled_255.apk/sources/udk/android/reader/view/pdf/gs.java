package udk.android.reader.view.pdf;

import android.graphics.drawable.AnimationDrawable;
import udk.android.reader.b.c;

final class gs implements Runnable {
    private /* synthetic */ PDFView a;

    gs(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void run() {
        if (this.a.L) {
            while (this.a.L && this.a.J.getVisibility() != 0) {
                this.a.J.setVisibility(0);
                if (this.a.J.getVisibility() == 0) {
                    break;
                }
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                    c.a((Throwable) e);
                }
            }
            AnimationDrawable animationDrawable = (AnimationDrawable) this.a.J.getDrawable();
            if (animationDrawable != null) {
                animationDrawable.start();
            }
        }
    }
}
