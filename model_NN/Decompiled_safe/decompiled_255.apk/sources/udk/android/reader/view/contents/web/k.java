package udk.android.reader.view.contents.web;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.KeyEvent;
import android.widget.TextView;

final class k implements TextView.OnEditorActionListener {
    private /* synthetic */ u a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ Runnable c;
    private final /* synthetic */ AlertDialog d;

    k(u uVar, Activity activity, Runnable runnable, AlertDialog alertDialog) {
        this.a = uVar;
        this.b = activity;
        this.c = runnable;
        this.d = alertDialog;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        this.b.runOnUiThread(this.c);
        this.d.dismiss();
        return true;
    }
}
