package udk.android.reader.view.pdf;

import com.unidocs.commonlib.util.b;
import udk.android.reader.pdf.annotation.s;

final class aj implements Runnable {
    private /* synthetic */ gq a;

    aj(gq gqVar) {
        this.a = gqVar;
    }

    public final void run() {
        s a2 = s.a();
        String editable = this.a.e.getText().toString();
        if (!editable.equals(b.b(this.a.a.M()) ? "" : this.a.a.M())) {
            a2.a(this.a.a, editable, !editable.equals(this.a.a.M()));
        }
        this.a.c();
        if (this.a.c != null) {
            this.a.c.run();
        }
    }
}
