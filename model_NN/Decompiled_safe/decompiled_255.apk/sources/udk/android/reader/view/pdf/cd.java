package udk.android.reader.view.pdf;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import udk.android.reader.b.c;

final class cd implements View.OnClickListener {
    private /* synthetic */ PDFReadingToolbar a;
    private final /* synthetic */ Context b;

    cd(PDFReadingToolbar pDFReadingToolbar, Context context) {
        this.a = pDFReadingToolbar;
        this.b = context;
    }

    public final void onClick(View view) {
        try {
            this.b.startActivity(new Intent("com.android.settings.TTS_SETTINGS"));
        } catch (Exception e) {
            c.a((Throwable) e);
        }
    }
}
