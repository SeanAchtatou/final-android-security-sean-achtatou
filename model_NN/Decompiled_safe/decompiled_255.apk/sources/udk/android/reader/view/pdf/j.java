package udk.android.reader.view.pdf;

final class j implements Runnable {
    private /* synthetic */ in a;
    private final /* synthetic */ Runnable b;

    j(in inVar, Runnable runnable) {
        this.a = inVar;
        this.b = runnable;
    }

    public final void run() {
        this.a.f = true;
        in.b(this.a);
        this.a.l();
        if (this.b != null) {
            this.b.run();
        }
    }
}
