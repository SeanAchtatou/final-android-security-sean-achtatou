package udk.android.reader.view.pdf.navigation;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.a;
import udk.android.reader.pdf.ac;
import udk.android.reader.pdf.af;
import udk.android.reader.pdf.at;
import udk.android.reader.pdf.b;
import udk.android.reader.pdf.d;
import udk.android.reader.view.pdf.PDFView;
import udk.android.reader.view.pdf.hx;

public final class NavigationService implements af, at {
    public static int a = 2;
    public static int b = 3;
    private static int c = 1;
    private static NavigationService d;
    /* access modifiers changed from: private */
    public SeekBar A;
    /* access modifiers changed from: private */
    public TextView B;
    /* access modifiers changed from: private */
    public TextView C;
    private LinkedList e;
    private int f;
    private int g;
    /* access modifiers changed from: private */
    public b h = b.a();
    /* access modifiers changed from: private */
    public a i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public PDFView k;
    /* access modifiers changed from: private */
    public View l;
    /* access modifiers changed from: private */
    public View m;
    /* access modifiers changed from: private */
    public View n;
    /* access modifiers changed from: private */
    public TextView o;
    /* access modifiers changed from: private */
    public TextView p;
    /* access modifiers changed from: private */
    public View q;
    private View r;
    private View s;
    private int t;
    private List u;
    /* access modifiers changed from: private */
    public AlertDialog v;
    private View w;
    /* access modifiers changed from: private */
    public TextView x;
    private View y;
    private View z;

    public enum SubMode {
        THUMBNAIL,
        ZOOM
    }

    private NavigationService() {
        this.h.a(this);
        this.u = new ArrayList();
    }

    public static NavigationService a() {
        if (d == null) {
            d = new NavigationService();
        }
        return d;
    }

    static /* synthetic */ void a(NavigationService navigationService, int i2) {
        if (i2 == 999901) {
            udk.android.reader.b.a.b = true;
        } else if (i2 == 999902) {
            navigationService.j.post(new ba(navigationService));
        }
        navigationService.a(i2);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, boolean z3) {
        if (this.k.w()) {
            if (z3) {
                a(z2, z3, this.k.F() == PDFView.ViewMode.TEXTREFLOW ? this.k.D() : this.k.C());
            } else {
                a(z2, z3, 0.0f);
            }
        }
    }

    private void a(boolean z2, boolean z3, float f2) {
        if (this.k.w()) {
            this.j.post(new aw(this, z2, z3, f2));
        }
    }

    private void e(int i2) {
        if (this.e == null) {
            this.e = new LinkedList();
        }
        if (this.f > 0) {
            this.f = 0;
            return;
        }
        while (this.e.size() - 1 > this.g) {
            this.e.removeLast();
        }
        this.e.add(Integer.valueOf(i2));
        if (this.e.size() > 100) {
            this.e.removeFirst();
        }
        this.f = 0;
        this.g = this.e.size() - 1;
    }

    static /* synthetic */ void s(NavigationService navigationService) {
        for (ab h2 : navigationService.u) {
            h2.h();
        }
    }

    static /* synthetic */ void t(NavigationService navigationService) {
        navigationService.r.setEnabled(navigationService.w());
        navigationService.r.setBackgroundResource(navigationService.w() ? C0000R.drawable.butt_history_backward : C0000R.drawable.butt_history_backward_disabled);
        navigationService.s.setEnabled(navigationService.v());
        navigationService.s.setBackgroundResource(navigationService.v() ? C0000R.drawable.butt_history_forward : C0000R.drawable.butt_history_forward_disabled);
    }

    private void u() {
        this.A.setProgress(this.k.c(this.A.getMax()));
    }

    private boolean v() {
        return this.e != null && this.g < this.e.size() - 1;
    }

    private boolean w() {
        return this.g > 0;
    }

    private void x() {
        TextView textView = (TextView) this.j.findViewById(C0000R.id.info_bookmark);
        if (textView != null) {
            new au(this, textView).start();
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        this.j.post(new av(this, this.h.i(), this.h.e(), !this.h.d()));
    }

    private void z() {
        if (this.n.getVisibility() == 0) {
            this.j.post(new ax(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        if (this.k.F() == PDFView.ViewMode.THUMBNAIL) {
            this.k.a(PDFView.ViewMode.PDF);
        }
        this.k.b(i2);
    }

    public final void a(View view, PDFView pDFView, int i2) {
        this.i = a.a();
        this.i.a(this);
        this.t = i2;
        this.j = view;
        this.k = pDFView;
        this.l = view.findViewById(C0000R.id.pdf_nav_page);
        this.m = view.findViewById(C0000R.id.pdf_nav_search);
        this.n = view.findViewById(C0000R.id.pdf_nav_bookmark);
        view.findViewById(C0000R.id.btn_stop_search).setOnClickListener(new ai(this));
        this.o = (TextView) view.findViewById(C0000R.id.search_state_detail);
        this.p = (TextView) view.findViewById(C0000R.id.search_state);
        this.q = view.findViewById(C0000R.id.btn_action_bookmark);
        this.q.setOnClickListener(new aj(this, view));
        view.findViewById(C0000R.id.btn_end_bookmark_view).setOnClickListener(new ak(this));
        this.r = view.findViewById(C0000R.id.btn_history_backward);
        this.r.setOnClickListener(new ae(this));
        this.s = view.findViewById(C0000R.id.btn_history_forward);
        this.s.setOnClickListener(new af(this));
        view.findViewById(C0000R.id.btn_goto).setOnClickListener(new ag(this));
        this.B = (TextView) view.findViewById(C0000R.id.disp_cur_page);
        this.C = (TextView) view.findViewById(C0000R.id.disp_total_page);
        this.w = view.findViewById(C0000R.id.btn_zoom);
        this.w.setOnClickListener(new ah(this));
        this.x = (TextView) view.findViewById(C0000R.id.disp_zoom);
        this.y = view.findViewById(C0000R.id.submode_thumb);
        this.z = view.findViewById(C0000R.id.submode_zoom);
        this.A = (SeekBar) view.findViewById(C0000R.id.zoom_progress);
        this.A.setOnSeekBarChangeListener(new ac(this, view));
    }

    public final void a(ac acVar) {
    }

    public final void a(d dVar) {
        y();
    }

    public final void a(SubMode subMode) {
        View[] viewArr = {this.y, this.z};
        View view = null;
        if (subMode == SubMode.THUMBNAIL) {
            view = this.y;
        } else if (subMode == SubMode.ZOOM) {
            view = this.z;
        }
        for (View view2 : viewArr) {
            if (view2 == view) {
                view2.setVisibility(0);
            } else {
                view2.setVisibility(8);
            }
        }
    }

    public final void a(ab abVar) {
        if (!this.u.contains(abVar)) {
            this.u.add(abVar);
        }
    }

    public final void a(boolean z2) {
        ImageView imageView = (ImageView) this.j.findViewById(C0000R.id.btn_bookmark);
        imageView.setEnabled(z2);
        imageView.setBackgroundResource(z2 ? C0000R.drawable.butt_bookmark : C0000R.drawable.butt_bookmark_disabled);
    }

    public final void b() {
        b.a().c();
        this.l.setVisibility(8);
        this.m.setVisibility(8);
        this.n.setVisibility(0);
        z();
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        if (this.k.F() == PDFView.ViewMode.THUMBNAIL) {
            this.k.a(PDFView.ViewMode.PDF);
        }
        this.k.a(i2);
    }

    public final void b(ab abVar) {
        this.u.remove(abVar);
    }

    public final void c() {
        this.l.setVisibility(0);
        this.m.setVisibility(8);
        this.n.setVisibility(8);
        for (ab g2 : this.u) {
            g2.g();
        }
        hx.a().p();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.navigation.NavigationService.a(udk.android.reader.view.pdf.navigation.NavigationService, int):void
      udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void */
    public final void c(int i2) {
        try {
            u();
            a(true, true);
            e(i2);
            this.j.post(new ap(this));
        } catch (Exception e2) {
            c.a((Throwable) e2);
        }
    }

    public final void d() {
        z();
        x();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.navigation.NavigationService.a(udk.android.reader.view.pdf.navigation.NavigationService, int):void
      udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void */
    public final void d(int i2) {
        z();
        e(i2);
        a(true, false);
        this.j.post(new aq(this));
    }

    public final void e() {
        this.i.b(this);
    }

    public final void f() {
        a((this.y.getVisibility() == 0 ? SubMode.THUMBNAIL : this.z.getVisibility() == 0 ? SubMode.ZOOM : null) == SubMode.THUMBNAIL ? SubMode.ZOOM : SubMode.THUMBNAIL);
    }

    public final boolean g() {
        return this.m.isShown();
    }

    public final boolean h() {
        return this.n.isShown();
    }

    public final void i() {
        TextView textView = (TextView) this.j.findViewById(C0000R.id.info_toc);
        if (textView != null) {
            new bb(this, textView).start();
        }
        x();
    }

    public final boolean j() {
        return ((ImageView) this.j.findViewById(C0000R.id.btn_bookmark)).isEnabled();
    }

    public final void k() {
        if (v()) {
            this.g++;
            this.f = ((Integer) this.e.get(this.g)).intValue();
            a(this.f);
        }
    }

    public final void l() {
        if (w()) {
            this.g--;
            this.f = ((Integer) this.e.get(this.g)).intValue();
            c.a("## HISTORY BACK " + this.f + " SIZE " + this.e.size());
            a(this.f);
        }
    }

    public final void m() {
        Context context = this.j.getContext();
        EditText editText = new EditText(context);
        editText.setInputType(2);
        editText.setImeOptions(2);
        editText.setOnEditorActionListener(new ad(this, editText));
        editText.setSingleLine();
        this.v = new AlertDialog.Builder(context).setTitle((int) C0000R.string.f139).setView(editText).setPositiveButton((int) C0000R.string.f50, new as(this, editText)).setNegativeButton((int) C0000R.string.f53, new ay(this)).show();
        editText.postDelayed(new az(this, context, editText), 100);
    }

    public final int n() {
        return this.t;
    }

    public final void o() {
        y();
        this.j.post(new at(this));
        new ao(this).start();
    }

    public final void p() {
        y();
    }

    public final void q() {
        this.j.post(new an(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.navigation.NavigationService.a(udk.android.reader.view.pdf.navigation.NavigationService, int):void
      udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void */
    public final void r() {
        u();
        a(true, true);
    }

    public final void s() {
        this.e = null;
        this.f = 0;
        this.g = -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.navigation.NavigationService.a(udk.android.reader.view.pdf.navigation.NavigationService, int):void
      udk.android.reader.view.pdf.navigation.NavigationService.a(boolean, boolean):void */
    public final void t() {
        u();
        a(false, true);
    }
}
