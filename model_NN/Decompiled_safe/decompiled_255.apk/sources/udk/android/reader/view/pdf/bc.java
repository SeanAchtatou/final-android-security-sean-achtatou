package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

final class bc implements TextView.OnEditorActionListener {
    private /* synthetic */ dc a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ EditText c;

    bc(dc dcVar, Context context, EditText editText) {
        this.a = dcVar;
        this.b = context;
        this.c = editText;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        ((InputMethodManager) this.b.getSystemService("input_method")).hideSoftInputFromWindow(this.c.getWindowToken(), 0);
        return true;
    }
}
