package udk.android.reader.view.pdf.navigation;

import android.graphics.Bitmap;
import android.widget.ImageView;

final class al implements Runnable {
    private /* synthetic */ y a;
    private final /* synthetic */ ImageView b;
    private final /* synthetic */ Bitmap c;

    al(y yVar, ImageView imageView, Bitmap bitmap) {
        this.a = yVar;
        this.b = imageView;
        this.c = bitmap;
    }

    public final void run() {
        this.b.setImageBitmap(this.c);
    }
}
