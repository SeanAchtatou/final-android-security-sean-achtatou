package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import udk.android.b.k;
import udk.android.b.m;
import udk.android.reader.b.b;
import udk.android.reader.b.c;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.e;
import udk.android.reader.pdf.annotation.p;
import udk.android.reader.pdf.annotation.w;
import udk.android.reader.pdf.c.a;

public final class au extends LinearLayout {
    /* access modifiers changed from: private */
    public Paint a;
    /* access modifiers changed from: private */
    public Paint b = new Paint(1);
    /* access modifiers changed from: private */
    public Paint c;
    private int d;
    private EditText e;
    private EditText f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public ImageView h;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public EditText j;
    /* access modifiers changed from: private */
    public EditText k;
    /* access modifiers changed from: private */
    public EditText l;
    /* access modifiers changed from: private */
    public View m;
    private CheckBox n;

    public au(Context context, Annotation annotation) {
        super(context);
        String str = b.Q;
        String str2 = b.R;
        String str3 = b.S;
        String str4 = b.W;
        String str5 = b.Y;
        String str6 = b.aa;
        String str7 = b.T;
        String str8 = b.U;
        String str9 = b.V;
        String str10 = b.ad;
        String str11 = b.ae;
        String str12 = b.A;
        String str13 = b.B;
        this.d = m.a(context, 5);
        this.b.setColor(annotation.L());
        if (annotation.s()) {
            this.c = new Paint(1);
            this.c.setColor(annotation.Z());
        }
        this.a = new Paint(1);
        this.a.setShader(k.a(m.a(context, 10)));
        setOrientation(1);
        setPadding(this.d, this.d, this.d, this.d);
        TextView textView = new TextView(context);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(str4);
        arrayList.add(str5);
        arrayList.add(str);
        if (annotation.s()) {
            arrayList.add(str2);
        }
        if (annotation instanceof p) {
            arrayList.add(str6);
        }
        if (annotation.l()) {
            arrayList.add(str8);
        }
        if (annotation instanceof e) {
            arrayList.add(str9);
        }
        if (annotation instanceof w) {
            arrayList.add(str10);
            arrayList.add(str11);
        }
        arrayList.add(str13);
        arrayList.add(str12);
        float f2 = 0.0f;
        for (String measureText : arrayList) {
            float measureText2 = textView.getPaint().measureText(measureText);
            if (measureText2 > f2) {
                f2 = measureText2;
            }
        }
        int i2 = ((int) f2) + (this.d * 2);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView.setText(str4);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i2, -2);
        layoutParams.weight = 0.0f;
        textView.setLayoutParams(layoutParams);
        linearLayout.addView(textView);
        this.e = new EditText(context);
        this.e.setText(annotation.N());
        this.e.setSingleLine(true);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.weight = 1.0f;
        this.e.setLayoutParams(layoutParams2);
        linearLayout.addView(this.e);
        addView(linearLayout);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setPadding(0, this.d, 0, 0);
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(16);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        TextView textView2 = new TextView(context);
        textView2.setText(str5);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(i2, -2);
        layoutParams3.weight = 0.0f;
        textView2.setLayoutParams(layoutParams3);
        linearLayout2.addView(textView2);
        this.f = new EditText(context);
        this.f.setText(annotation.P());
        this.f.setSingleLine(true);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.weight = 1.0f;
        this.f.setLayoutParams(layoutParams4);
        linearLayout2.addView(this.f);
        addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setPadding(0, this.d, 0, 0);
        linearLayout3.setOrientation(0);
        linearLayout3.setGravity(16);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        TextView textView3 = new TextView(context);
        textView3.setText(str);
        textView3.setLayoutParams(new LinearLayout.LayoutParams(i2, -2));
        linearLayout3.addView(textView3);
        int textSize = ((int) textView.getTextSize()) * 4;
        int textSize2 = ((int) textView.getTextSize()) * 2;
        fn fnVar = new fn(this, context, textSize, textSize2, context);
        fnVar.setOnClickListener(new fo(this, context, str7, fnVar, annotation));
        fnVar.setLayoutParams(new LinearLayout.LayoutParams(textSize, textSize2));
        linearLayout3.addView(fnVar);
        addView(linearLayout3);
        if (annotation.s()) {
            LinearLayout linearLayout4 = new LinearLayout(context);
            linearLayout4.setPadding(0, this.d, 0, 0);
            linearLayout4.setOrientation(0);
            linearLayout4.setGravity(16);
            linearLayout4.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            TextView textView4 = new TextView(context);
            textView4.setText(str2);
            textView4.setLayoutParams(new LinearLayout.LayoutParams(i2, -2));
            linearLayout4.addView(textView4);
            this.m = new fi(this, context, textSize, textSize2, context);
            this.m.setVisibility(annotation.aa() ? 0 : 8);
            this.m.setOnClickListener(new fk(this, context, str7));
            this.m.setLayoutParams(new LinearLayout.LayoutParams(textSize, textSize2));
            linearLayout4.addView(this.m);
            this.n = new CheckBox(context);
            this.n.setChecked(!annotation.aa());
            this.n.setOnCheckedChangeListener(new fl(this));
            linearLayout4.addView(this.n, new LinearLayout.LayoutParams(-2, -2));
            TextView textView5 = new TextView(context);
            textView5.setText(str3);
            linearLayout4.addView(textView5, new LinearLayout.LayoutParams(-2, -2));
            addView(linearLayout4);
        }
        if (annotation instanceof p) {
            p pVar = (p) annotation;
            LinearLayout linearLayout5 = new LinearLayout(context);
            linearLayout5.setPadding(0, this.d * 2, 0, this.d);
            linearLayout5.setOrientation(0);
            linearLayout5.setGravity(16);
            linearLayout5.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            TextView textView6 = new TextView(context);
            textView6.setText(str6);
            textView6.setLayoutParams(new LinearLayout.LayoutParams(i2, -2));
            linearLayout5.addView(textView6);
            this.h = new ImageView(context);
            this.h.setImageBitmap(a.a().a(pVar.a(), this.b.getColor()));
            this.h.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            linearLayout5.addView(this.h);
            this.g = new TextView(context);
            this.g.setPadding(this.d, 0, 0, 0);
            this.g.setText(pVar.a());
            this.g.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            linearLayout5.addView(this.g);
            addView(linearLayout5);
            linearLayout5.setOnClickListener(new fq(this, new fm(this, context)));
        }
        if (annotation.l()) {
            LinearLayout linearLayout6 = new LinearLayout(context);
            linearLayout6.setPadding(0, this.d, 0, 0);
            linearLayout6.setOrientation(0);
            linearLayout6.setGravity(16);
            linearLayout6.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            TextView textView7 = new TextView(context);
            textView7.setText(str8);
            LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(i2, -2);
            layoutParams5.weight = 0.0f;
            textView7.setLayoutParams(layoutParams5);
            linearLayout6.addView(textView7);
            this.i = new EditText(context);
            this.i.setFocusable(false);
            this.i.setFocusableInTouchMode(false);
            this.i.setText(new StringBuilder().append(annotation.S()).toString());
            LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams6.weight = 1.0f;
            this.i.setLayoutParams(layoutParams6);
            linearLayout6.addView(this.i);
            addView(linearLayout6);
            this.i.setOnClickListener(new fr(this, annotation, context, str8));
        }
        if (annotation instanceof e) {
            LinearLayout linearLayout7 = new LinearLayout(context);
            linearLayout7.setPadding(0, this.d, 0, 0);
            linearLayout7.setOrientation(0);
            linearLayout7.setGravity(16);
            linearLayout7.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            TextView textView8 = new TextView(context);
            textView8.setText(str9);
            LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(i2, -2);
            layoutParams7.weight = 0.0f;
            textView8.setLayoutParams(layoutParams7);
            linearLayout7.addView(textView8);
            this.j = new EditText(context);
            this.j.setFocusable(false);
            this.j.setFocusableInTouchMode(false);
            this.j.setText(new StringBuilder().append(((e) annotation).f()).toString());
            LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams8.weight = 1.0f;
            this.j.setLayoutParams(layoutParams8);
            linearLayout7.addView(this.j);
            addView(linearLayout7);
            this.j.setOnClickListener(new fs(this, context, str9));
        }
        if (annotation instanceof w) {
            w wVar = (w) annotation;
            LinearLayout linearLayout8 = new LinearLayout(context);
            linearLayout8.setPadding(0, this.d * 2, 0, this.d);
            linearLayout8.setOrientation(0);
            linearLayout8.setGravity(16);
            linearLayout8.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            TextView textView9 = new TextView(context);
            textView9.setText(str10);
            LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(i2, -2);
            layoutParams9.weight = 0.0f;
            textView9.setLayoutParams(layoutParams9);
            linearLayout8.addView(textView9);
            this.k = new EditText(context);
            this.k.setFocusable(false);
            this.k.setFocusableInTouchMode(false);
            this.k.setText(w.b(wVar.c()));
            LinearLayout.LayoutParams layoutParams10 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams10.weight = 1.0f;
            this.k.setLayoutParams(layoutParams10);
            linearLayout8.addView(this.k);
            addView(linearLayout8);
            this.k.setOnClickListener(new s(this, context, str10));
            LinearLayout linearLayout9 = new LinearLayout(context);
            linearLayout9.setPadding(0, this.d * 2, 0, this.d);
            linearLayout9.setOrientation(0);
            linearLayout9.setGravity(16);
            linearLayout9.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            TextView textView10 = new TextView(context);
            textView10.setText(str11);
            LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(i2, -2);
            layoutParams11.weight = 0.0f;
            textView10.setLayoutParams(layoutParams11);
            linearLayout9.addView(textView10);
            this.l = new EditText(context);
            this.l.setFocusable(false);
            this.l.setFocusableInTouchMode(false);
            this.l.setText(w.b(wVar.d()));
            LinearLayout.LayoutParams layoutParams12 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams12.weight = 1.0f;
            this.l.setLayoutParams(layoutParams12);
            linearLayout9.addView(this.l);
            addView(linearLayout9);
            this.l.setOnClickListener(new r(this, context, str11));
        }
        LinearLayout linearLayout10 = new LinearLayout(context);
        linearLayout10.setPadding(0, this.d * 2, 0, this.d);
        linearLayout10.setOrientation(0);
        linearLayout10.setGravity(16);
        linearLayout10.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        TextView textView11 = new TextView(context);
        textView11.setText(str12);
        LinearLayout.LayoutParams layoutParams13 = new LinearLayout.LayoutParams(i2, -2);
        layoutParams13.weight = 0.0f;
        textView11.setLayoutParams(layoutParams13);
        linearLayout10.addView(textView11);
        TextView textView12 = new TextView(context);
        textView12.setText(annotation.q());
        LinearLayout.LayoutParams layoutParams14 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams14.weight = 1.0f;
        textView12.setLayoutParams(layoutParams14);
        linearLayout10.addView(textView12);
        addView(linearLayout10);
        LinearLayout linearLayout11 = new LinearLayout(context);
        linearLayout11.setPadding(0, this.d * 2, 0, this.d);
        linearLayout11.setOrientation(0);
        linearLayout11.setGravity(16);
        linearLayout11.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        TextView textView13 = new TextView(context);
        textView13.setText(str13);
        LinearLayout.LayoutParams layoutParams15 = new LinearLayout.LayoutParams(i2, -2);
        layoutParams15.weight = 0.0f;
        textView13.setLayoutParams(layoutParams15);
        linearLayout11.addView(textView13);
        TextView textView14 = new TextView(context);
        textView14.setText(annotation.r());
        LinearLayout.LayoutParams layoutParams16 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams16.weight = 1.0f;
        textView14.setLayoutParams(layoutParams16);
        linearLayout11.addView(textView14);
        addView(linearLayout11);
        View view = new View(context);
        view.setLayoutParams(new LinearLayout.LayoutParams(this.d, this.d));
        addView(view);
    }

    public final int a() {
        return this.b.getColor();
    }

    public final boolean b() {
        return !this.n.isChecked();
    }

    public final int c() {
        return this.c.getColor();
    }

    public final String d() {
        return this.e.getText().toString();
    }

    public final String e() {
        return this.f.getText().toString();
    }

    public final String f() {
        return this.g.getText().toString();
    }

    public final float g() {
        try {
            return Float.parseFloat(this.i.getText().toString());
        } catch (Exception e2) {
            c.a((Throwable) e2);
            return 1.0f;
        }
    }

    public final float h() {
        try {
            return Float.parseFloat(this.j.getText().toString());
        } catch (Exception e2) {
            c.a((Throwable) e2);
            return 12.0f;
        }
    }

    public final String i() {
        return w.h(this.k.getText().toString());
    }

    public final String j() {
        return w.h(this.l.getText().toString());
    }
}
