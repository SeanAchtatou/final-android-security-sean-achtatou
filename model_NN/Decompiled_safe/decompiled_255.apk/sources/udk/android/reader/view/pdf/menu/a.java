package udk.android.reader.view.pdf.menu;

import android.graphics.RectF;
import android.widget.LinearLayout;
import udk.android.reader.pdf.a.c;

final class a implements Runnable {
    private /* synthetic */ e a;
    private final /* synthetic */ LinearLayout b;
    private final /* synthetic */ c c;
    private final /* synthetic */ RectF d;

    a(e eVar, LinearLayout linearLayout, c cVar, RectF rectF) {
        this.a = eVar;
        this.b = linearLayout;
        this.c = cVar;
        this.d = rectF;
    }

    public final void run() {
        try {
            this.a.a.a(this.b, this.c, this.d);
        } catch (Exception e) {
            udk.android.reader.b.c.a((Throwable) e);
        }
    }
}
