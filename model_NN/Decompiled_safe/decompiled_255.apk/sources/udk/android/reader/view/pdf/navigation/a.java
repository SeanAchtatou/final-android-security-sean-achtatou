package udk.android.reader.view.pdf.navigation;

import android.graphics.Bitmap;
import android.widget.ImageView;

final class a implements Runnable {
    private /* synthetic */ u a;
    private final /* synthetic */ ImageView b;
    private final /* synthetic */ Bitmap c;

    a(u uVar, ImageView imageView, Bitmap bitmap) {
        this.a = uVar;
        this.b = imageView;
        this.c = bitmap;
    }

    public final void run() {
        this.b.setImageBitmap(this.c);
    }
}
