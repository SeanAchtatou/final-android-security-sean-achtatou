package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.a.a;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ar;
import udk.android.reader.pdf.at;
import udk.android.reader.pdf.b;
import udk.android.reader.pdf.d;
import udk.android.reader.view.pdf.hx;

public class SearchNavigationView extends Gallery implements ar, at {
    /* access modifiers changed from: private */
    public NavigationService a;
    /* access modifiers changed from: private */
    public ar b;
    /* access modifiers changed from: private */
    public b c;
    /* access modifiers changed from: private */
    public PDF d;
    private boolean e;
    /* access modifiers changed from: private */
    public int f;

    public SearchNavigationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    public SearchNavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        b();
    }

    private void b() {
        setCallbackDuringFling(false);
        this.a = NavigationService.a();
        this.b = new ar(getContext());
        setAdapter((SpinnerAdapter) this.b);
        setOnItemClickListener(new v(this));
        setOnItemSelectedListener(new z(this));
        this.c = b.a();
        this.c.a(this);
        this.d = PDF.a();
        this.d.a(this);
    }

    public final void a() {
        try {
            if (this.a.n() == NavigationService.b) {
                int firstVisiblePosition = getFirstVisiblePosition();
                float a2 = this.d.a(getContext().getResources().getDimension(C0000R.dimen.navigation_thumbnail_width) - (getContext().getResources().getDimension(C0000R.dimen.navigation_thumbnail_padding) * 2.0f));
                String z = this.d.z();
                int childCount = getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = getChildAt(i);
                    if (childAt != null) {
                        int ag = ((a) this.b.getItem(firstVisiblePosition + i).get(0)).ag();
                        ImageView imageView = (ImageView) childAt.findViewById(C0000R.id.thumbnail);
                        if (imageView.getDrawable() == null) {
                            new y(this, z, childAt, ag, a2, imageView).start();
                        }
                    }
                }
            }
        } catch (Throwable th) {
            c.a(th.getMessage(), th);
        }
    }

    public final void a(ah ahVar) {
    }

    public final void a(d dVar) {
        post(new w(this));
        if (!this.e) {
            this.e = true;
            try {
                this.a.b(dVar.b);
                hx.a().p();
                Thread.sleep(500);
            } catch (Exception e2) {
                c.a(e2.getMessage(), e2);
            }
        }
    }

    public final void b(ah ahVar) {
    }

    public final void f() {
    }

    public final void g() {
        this.b.notifyDataSetChanged();
    }

    public final void h() {
    }

    public final void o() {
        this.e = false;
        post(new x(this));
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        c.a("## DISPOSE SearchNavigationView");
        this.d.b(this);
        this.c.b(this);
        super.onDetachedFromWindow();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return false;
    }

    public final void p() {
        a();
    }

    public final void q() {
    }
}
