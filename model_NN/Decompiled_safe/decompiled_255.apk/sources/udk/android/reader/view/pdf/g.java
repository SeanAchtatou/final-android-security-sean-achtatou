package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.View;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.z;

final class g implements View.OnClickListener {
    private /* synthetic */ ia a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ z c;
    private final /* synthetic */ Runnable d;

    g(ia iaVar, Context context, z zVar, Runnable runnable) {
        this.a = iaVar;
        this.b = context;
        this.c = zVar;
        this.d = runnable;
    }

    public final void onClick(View view) {
        s.a().a(this.b, this.c, this.d);
    }
}
