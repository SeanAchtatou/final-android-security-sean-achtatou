package udk.android.reader.view.pdf;

import android.graphics.PointF;

public final class bn {
    private static bn r;
    public boolean a;
    public int b;
    float c;
    float d;
    float e;
    float f;
    float g;
    float h;
    float i;
    float j;
    float k;
    float l;
    float m;
    float n;
    boolean o;
    boolean p;
    PointF q;

    private bn() {
    }

    public static bn a() {
        if (r == null) {
            r = new bn();
        }
        return r;
    }
}
