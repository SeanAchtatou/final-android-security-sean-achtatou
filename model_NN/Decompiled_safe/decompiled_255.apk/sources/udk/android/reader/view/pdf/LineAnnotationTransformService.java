package udk.android.reader.view.pdf;

import android.graphics.PointF;
import android.view.MotionEvent;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.w;

public final class LineAnnotationTransformService implements ae {
    private PDFView a;
    private hx b = hx.a();
    /* access modifiers changed from: private */
    public cl c;
    private bn d = bn.a();
    private w e;
    private Annotation.TransformingType f;
    private PointF g;
    private PointF h;
    private boolean i;

    enum ScalingType {
        NONE,
        SCALE_ONLY,
        MOVE_SCALE
    }

    public LineAnnotationTransformService(PDFView pDFView, cl clVar, w wVar, Annotation.TransformingType transformingType) {
        this.a = pDFView;
        this.c = clVar;
        this.e = wVar;
        this.f = transformingType;
        this.g = wVar.c(1.0f);
        this.h = wVar.d(1.0f);
    }

    public final void a() {
        this.i = true;
        this.c.a((ae) this);
    }

    public final void a(MotionEvent motionEvent) {
    }

    public final void b(MotionEvent motionEvent) {
        if (this.i) {
            boolean z = this.f == Annotation.TransformingType.MOVE || this.f == Annotation.TransformingType.START_HEAD;
            boolean z2 = this.f == Annotation.TransformingType.MOVE || this.f == Annotation.TransformingType.END_HEAD;
            float x = (motionEvent.getX() - this.d.g) / this.b.n();
            float y = (motionEvent.getY() - this.d.h) / this.b.n();
            if (z) {
                this.e.a(new PointF(this.g.x + x, this.g.y + y));
            }
            if (z2) {
                this.e.b(new PointF(x + this.h.x, y + this.h.y));
            }
            this.b.p();
        }
    }

    public final void c(MotionEvent motionEvent) {
        this.e.a((double[]) null);
        s.a().c(this.e);
        this.i = false;
        this.a.R();
        new m(this).start();
    }
}
