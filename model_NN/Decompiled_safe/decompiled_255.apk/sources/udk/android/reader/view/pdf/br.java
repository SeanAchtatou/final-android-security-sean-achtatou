package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;

final class br extends View {
    private /* synthetic */ PDFReadingToolbar a;
    private final /* synthetic */ int b;
    private final /* synthetic */ Paint c;
    private final /* synthetic */ Paint d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    br(PDFReadingToolbar pDFReadingToolbar, Context context, int i, Paint paint, Paint paint2) {
        super(context);
        this.a = pDFReadingToolbar;
        this.b = i;
        this.c = paint;
        this.d = paint2;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight()), (float) (this.b * 2), (float) (this.b * 2), this.c);
        int i = 0;
        int i2 = this.b / 2;
        while (true) {
            int i3 = i;
            float f = (float) (this.b + (i3 * i2));
            float f2 = f + (((float) i2) * 0.5f);
            if (f2 <= ((float) (getHeight() - this.b))) {
                canvas.drawRect((float) this.b, f, (float) (getWidth() - this.b), f2, this.d);
                i = i3 + 1;
            } else {
                Canvas canvas2 = canvas;
                canvas2.drawRect((float) (this.b + i2), (float) this.b, (((float) i2) * 1.5f) + ((float) this.b), (float) (getHeight() - this.b), this.c);
                return;
            }
        }
    }
}
