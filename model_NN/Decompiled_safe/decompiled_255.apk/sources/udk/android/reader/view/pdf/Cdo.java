package udk.android.reader.view.pdf;

import android.graphics.RectF;
import android.view.View;
import java.util.List;
import udk.android.reader.pdf.annotation.Annotation;

/* renamed from: udk.android.reader.view.pdf.do  reason: invalid class name */
final class Cdo implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Annotation c;
    private final /* synthetic */ List d;
    private final /* synthetic */ View e;

    Cdo(PDFView pDFView, String str, Annotation annotation, List list, View view) {
        this.a = pDFView;
        this.b = str;
        this.c = annotation;
        this.d = list;
        this.e = view;
    }

    public final void run() {
        this.a.K.a(this.b, this.c, new RectF((float) this.a.Q, (float) this.a.R, (float) (this.a.getWidth() - this.a.S), (float) (this.a.getHeight() - this.a.T)), this.d, this.e);
    }
}
