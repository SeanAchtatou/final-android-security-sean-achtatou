package udk.android.reader.view.pdf;

import android.graphics.Bitmap;
import android.widget.ImageView;

final class ih implements Runnable {
    private /* synthetic */ Cif a;
    private final /* synthetic */ ImageView b;
    private final /* synthetic */ Bitmap c;

    ih(Cif ifVar, ImageView imageView, Bitmap bitmap) {
        this.a = ifVar;
        this.b = imageView;
        this.c = bitmap;
    }

    public final void run() {
        this.b.setImageBitmap(this.c);
    }
}
