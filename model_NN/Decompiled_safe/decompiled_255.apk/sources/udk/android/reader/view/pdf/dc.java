package udk.android.reader.view.pdf;

import android.content.Context;
import android.widget.Toast;
import udk.android.b.f;

public final class dc {
    private static dc a;
    /* access modifiers changed from: private */
    public f b;

    private dc() {
    }

    public static dc a() {
        if (a == null) {
            a = new dc();
        }
        return a;
    }

    static /* synthetic */ void a(dc dcVar, int i, PDFView pDFView, Runnable runnable, String str) {
        dcVar.c();
        dcVar.b = new f();
        dcVar.b.a = true;
        dcVar.b.b = str;
        pDFView.q();
        bd bdVar = new bd(dcVar, i, pDFView, runnable);
        bdVar.setDaemon(true);
        bdVar.start();
        runnable.run();
    }

    static /* synthetic */ void a(dc dcVar, f fVar, int i, PDFView pDFView, Runnable runnable) {
        Runnable runnable2 = runnable;
        PDFView pDFView2 = pDFView;
        int i2 = i;
        f fVar2 = fVar;
        dc dcVar2 = dcVar;
        while (pDFView2.w() && fVar2.a) {
            Thread.sleep((long) i2);
            if (pDFView2.w() && fVar2.a) {
                if (!pDFView2.m()) {
                    if (dcVar2.b == fVar2) {
                        pDFView2.post(new bf(dcVar2, pDFView2, runnable2));
                        return;
                    }
                    return;
                }
            } else {
                return;
            }
        }
    }

    private void c() {
        if (this.b != null) {
            this.b.a = false;
            this.b = null;
        }
    }

    public final void a(Context context) {
        if (this.b != null) {
            Toast.makeText(context, this.b.b, 0).show();
        }
        c();
    }

    public final boolean b() {
        return this.b != null;
    }
}
