package udk.android.reader.view.pdf.menu;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.widget.LinearLayout;

final class d extends Drawable {
    private /* synthetic */ g a;
    private final /* synthetic */ LinearLayout b;
    private final /* synthetic */ int c;

    d(g gVar, LinearLayout linearLayout, int i) {
        this.a = gVar;
        this.b = linearLayout;
        this.c = i;
    }

    public final void draw(Canvas canvas) {
        int width = this.b.getWidth();
        int height = this.b.getHeight();
        Path path = new Path();
        path.moveTo((float) this.a.g, (float) this.a.g);
        if (this.c == 3) {
            path.lineTo((float) ((width / 2) - (this.a.g / 2)), (float) this.a.g);
            path.lineTo((float) (width / 2), (float) (this.a.g / 2));
            path.lineTo((float) ((width / 2) + (this.a.g / 2)), (float) this.a.g);
        }
        path.lineTo((float) (width - this.a.g), (float) this.a.g);
        path.quadTo((float) width, (float) this.a.g, (float) width, (float) (this.a.g * 2));
        path.lineTo((float) width, (float) (height - (this.a.g * 2)));
        path.quadTo((float) width, (float) (height - this.a.g), (float) (width - this.a.g), (float) (height - this.a.g));
        if (this.c == 1) {
            path.lineTo((float) ((width / 2) + (this.a.g / 2)), (float) (height - this.a.g));
            path.lineTo((float) (width / 2), (float) (height - (this.a.g / 2)));
            path.lineTo((float) ((width / 2) - (this.a.g / 2)), (float) (height - this.a.g));
        }
        path.lineTo((float) this.a.g, (float) (height - this.a.g));
        path.quadTo(0.0f, (float) (height - this.a.g), 0.0f, (float) (height - (this.a.g * 2)));
        path.lineTo(0.0f, (float) (this.a.g * 2));
        path.quadTo(0.0f, (float) this.a.g, (float) this.a.g, (float) this.a.g);
        canvas.save();
        canvas.translate(0.0f, (float) (this.a.g / 4));
        canvas.drawPath(path, this.a.e);
        canvas.restore();
        canvas.drawPath(path, this.a.c);
        canvas.drawPath(path, this.a.d);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
