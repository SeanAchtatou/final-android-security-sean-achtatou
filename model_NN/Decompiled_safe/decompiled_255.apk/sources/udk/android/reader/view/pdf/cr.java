package udk.android.reader.view.pdf;

import android.view.MotionEvent;

final class cr implements Runnable {
    private /* synthetic */ cl a;
    private final /* synthetic */ MotionEvent b;

    cr(cl clVar, MotionEvent motionEvent) {
        this.a = clVar;
        this.b = motionEvent;
    }

    public final void run() {
        if (this.a.o != -1 && !cl.a(this.a, this.b)) {
            hg hgVar = new hg();
            hgVar.a = this.a.g.E();
            hgVar.b = this.a.g.F();
            hgVar.c = this.b;
            this.a.a.ag();
        }
    }
}
