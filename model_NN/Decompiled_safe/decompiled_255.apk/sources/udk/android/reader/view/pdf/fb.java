package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.ScrollView;
import udk.android.reader.b.b;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;

final class fb implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ Annotation b;
    private final /* synthetic */ s c;

    fb(PDFView pDFView, Annotation annotation, s sVar) {
        this.a = pDFView;
        this.b = annotation;
        this.c = sVar;
    }

    public final void run() {
        Context context = this.a.getContext();
        ScrollView scrollView = new ScrollView(context);
        au auVar = new au(context, this.b);
        scrollView.addView(auVar);
        new AlertDialog.Builder(context).setTitle(b.c).setView(scrollView).setPositiveButton(b.r, new d(this, auVar, this.b, this.c, context)).setNegativeButton(b.s, new c(this)).create().show();
    }
}
