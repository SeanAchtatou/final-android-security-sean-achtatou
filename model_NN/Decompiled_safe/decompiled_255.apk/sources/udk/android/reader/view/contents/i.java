package udk.android.reader.view.contents;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.io.File;
import udk.android.reader.C0000R;

final class i implements View.OnLongClickListener {
    private /* synthetic */ d a;
    private final /* synthetic */ ViewGroup b;
    private final /* synthetic */ File c;

    i(d dVar, ViewGroup viewGroup, File file) {
        this.a = dVar;
        this.b = viewGroup;
        this.c = file;
    }

    public final boolean onLongClick(View view) {
        Context context = this.b.getContext();
        String[] strArr = {context.getString(C0000R.string.f126), context.getString(C0000R.string.f96__)};
        new AlertDialog.Builder(context).setTitle(String.valueOf(context.getString(C0000R.string.f89)) + " : " + this.c.getAbsolutePath()).setItems(strArr, new a(this, strArr, context, this.c)).show();
        return true;
    }
}
