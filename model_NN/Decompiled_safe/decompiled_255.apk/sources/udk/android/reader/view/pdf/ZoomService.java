package udk.android.reader.view.pdf;

import android.graphics.PointF;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;
import udk.android.b.c;
import udk.android.reader.b.a;
import udk.android.reader.pdf.PDF;

public final class ZoomService {
    private static ZoomService b;
    ic a = ic.a();
    private boolean c;
    private int d;
    private float e;
    private RectF f;
    private hx g = hx.a();
    private PDF h = PDF.a();
    private bn i = bn.a();
    private List j = new ArrayList();
    private bm k;
    private float l;

    public enum FittingType {
        WIDTHFIT,
        HEIGHTFIT
    }

    private ZoomService() {
    }

    public static ZoomService a() {
        if (b == null) {
            b = new ZoomService();
        }
        return b;
    }

    private void a(float f2, float f3, float f4) {
        a(this.h.F(), this.g.b(), f2, f3, f4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF, float, android.graphics.RectF, boolean):void
     arg types: [float, android.graphics.RectF, float, android.graphics.RectF, int]
     candidates:
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF, float, float, float):void
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF, float, android.graphics.RectF, boolean):void */
    private void a(float f2, RectF rectF, float f3, float f4, float f5) {
        try {
            a(f2, rectF, f3, c.a(rectF, new PointF(f4, f5), new PointF(f4, f5), f3 / f2), false);
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Throwable) e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.ZoomService.a(boolean, int):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.ZoomService.a(float, float):void
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF):void
      udk.android.reader.view.pdf.ZoomService.a(boolean, int):void */
    private void a(float f2, RectF rectF, float f3, RectF rectF2, boolean z) {
        cl.a(this);
        this.k = new bm();
        this.k.a = f2;
        this.k.b = rectF;
        this.k.c = new PointF((float) (this.a.a / 2), (float) (this.a.b / 2));
        this.k.d = f3;
        this.k.e = rectF2;
        this.k.f = z;
        this.f = this.k.b;
        float f4 = this.k.e.right - this.k.e.left;
        float f5 = this.k.e.bottom - this.k.e.top;
        if (this.a.a(f4)) {
            this.k.e.left = ((float) (this.a.a / 2)) - (f4 / 2.0f);
            this.k.e.right = f4 + this.k.e.left;
        } else if (this.k.e.left > 0.0f) {
            this.k.e.left = 0.0f;
            this.k.e.right = f4 + this.k.e.left;
        } else if (this.k.e.right < ((float) this.a.a)) {
            this.k.e.right = (float) this.a.a;
            this.k.e.left = this.k.e.right - f4;
        }
        if (this.a.b(f5)) {
            this.k.e.top = ((float) (this.a.b / 2)) - (f5 / 2.0f);
            this.k.e.bottom = f5 + this.k.e.top;
        } else if (this.k.e.top > 0.0f) {
            this.k.e.top = 0.0f;
            this.k.e.bottom = f5 + this.k.e.top;
        } else if (this.k.e.bottom < ((float) this.a.b)) {
            this.k.e.bottom = (float) this.a.b;
            this.k.e.top = this.k.e.bottom - f5;
        }
        a(true, 3);
    }

    private void a(boolean z, int i2) {
        this.l = 0.0f;
        if (z != this.c) {
            this.c = z;
            if (this.c) {
                for (b b_ : this.j) {
                    b_.b_();
                }
            }
        }
        this.d = i2;
        if (!this.c) {
            cl.b(this);
        }
    }

    private FittingType d(int i2) {
        return c(i2) == FittingType.WIDTHFIT ? FittingType.HEIGHTFIT : FittingType.WIDTHFIT;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.ZoomService.a(boolean, int):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.ZoomService.a(float, float):void
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF):void
      udk.android.reader.view.pdf.ZoomService.a(boolean, int):void */
    private void h() {
        bm bmVar;
        if (this.k == null) {
            if (this.d == 1) {
                if (this.e > a.ac) {
                    this.e = a.ac;
                } else if (this.e < a.ab) {
                    this.e = a.ab;
                }
                float n = this.e / this.g.n();
                RectF b2 = this.g.b();
                PointF i2 = i();
                this.f = c.a(b2, i2, i2, n);
            }
            bm bmVar2 = new bm();
            bmVar2.a = this.g.n();
            bmVar2.b = this.g.b();
            bmVar2.d = this.e;
            bmVar2.e = this.f;
            bmVar = bmVar2;
        } else {
            bmVar = this.k;
        }
        for (b a2 : this.j) {
            a2.a(bmVar);
        }
        a(false, 0);
        for (b b3 : this.j) {
            b3.b(bmVar);
        }
        this.k = null;
    }

    private PointF i() {
        return c.b(this.i.g, this.i.h, this.i.i, this.i.j);
    }

    /* access modifiers changed from: package-private */
    public final FittingType a(int i2) {
        switch (a.Z) {
            case 1:
                return FittingType.WIDTHFIT;
            case 2:
                return FittingType.HEIGHTFIT;
            case 3:
                return c(i2);
            case 4:
                return d(i2);
            default:
                return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2, float f3) {
        if (this.g.n() >= a.ac) {
            b(f2, f3);
            return;
        }
        float n = this.g.n() * a.ae;
        if (n > a.ac) {
            n = a.ac;
        }
        a(n, f2, f3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF, float, android.graphics.RectF, boolean):void
     arg types: [float, android.graphics.RectF, float, android.graphics.RectF, int]
     candidates:
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF, float, float, float):void
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF, float, android.graphics.RectF, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(float f2, RectF rectF) {
        a(this.h.F(), this.g.b(), f2, rectF, true);
    }

    public final void a(b bVar) {
        if (!this.j.contains(bVar)) {
            this.j.add(bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(float f2) {
        return b(this.e + (this.e * f2));
    }

    /* access modifiers changed from: package-private */
    public final float b(int i2) {
        switch (a.Z) {
            case 1:
                return this.h.d(i2, (float) this.a.a);
            case 2:
                return this.h.e(i2, (float) this.a.b);
            case 3:
                return c(i2) == FittingType.WIDTHFIT ? this.h.d(i2, (float) this.a.a) : this.h.e(i2, (float) this.a.b);
            case 4:
                return d(i2) == FittingType.WIDTHFIT ? this.h.d(i2, (float) this.a.a) : this.h.e(i2, (float) this.a.b);
            default:
                return 0.5f;
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.e > a.ac) {
            PointF b2 = this.a.b();
            a(this.e, this.f, a.ac, b2.x, b2.y);
        } else if (this.e < b(this.h.E())) {
            FittingType a2 = a(this.h.E());
            if (a2 == FittingType.WIDTHFIT) {
                PointF b3 = this.a.b();
                a(this.e, this.f, this.h.a((float) this.a.a), b3.x, b3.y);
            } else if (a2 == FittingType.HEIGHTFIT) {
                PointF b4 = this.a.b();
                a(this.e, this.f, this.h.b((float) this.a.b), b4.x, b4.y);
            }
        } else {
            h();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(float f2, float f3) {
        FittingType a2 = a(this.h.E());
        if (a2 == FittingType.WIDTHFIT) {
            a(this.h.a((float) this.a.a), f2, f3);
        } else if (a2 == FittingType.HEIGHTFIT) {
            a(this.h.b((float) this.a.b), f2, f3);
        }
    }

    public final void b(b bVar) {
        this.j.add(bVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean b(float f2) {
        this.e = f2;
        if (this.e < a.ab) {
            this.e = a.ab;
            return false;
        } else if (this.e <= a.ac) {
            return true;
        } else {
            this.e = a.ac;
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final FittingType c(int i2) {
        return ((float) this.a.a) / ((float) this.h.h(i2)) > ((float) this.a.b) / ((float) this.h.i(i2)) ? FittingType.HEIGHTFIT : FittingType.WIDTHFIT;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.ZoomService.a(boolean, int):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.ZoomService.a(float, float):void
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF):void
      udk.android.reader.view.pdf.ZoomService.a(boolean, int):void */
    /* access modifiers changed from: package-private */
    public final void c() {
        cl.a(this);
        this.e = this.g.n();
        a(true, 2);
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        b();
    }

    /* access modifiers changed from: package-private */
    public final RectF e() {
        if (this.d == 1) {
            float a2 = c.a(this.i.c, this.i.d, this.i.e, this.i.f);
            if (a2 < 20.0f) {
                return this.f;
            }
            if (this.l <= 0.0f) {
                this.l = a2;
            }
            this.l = a2;
            RectF b2 = this.g.b();
            float a3 = ((c.a(new RectF(this.i.g, this.i.h, this.i.i, this.i.j), new RectF(this.i.c, this.i.d, this.i.e, this.i.f)) * a.ad) - a.ad) + 1.0f;
            float n = this.g.n() * a3;
            PointF i2 = i();
            this.f = c.a(b2, i2, a.aa ? c.b(this.i.c, this.i.d, this.i.e, this.i.f) : i2, a3);
            this.e = n;
            return this.f;
        } else if (this.d == 3) {
            float f2 = this.k.e.left - this.f.left;
            float f3 = this.k.e.right - this.f.right;
            float f4 = this.k.e.top - this.f.top;
            float f5 = this.k.e.bottom - this.f.bottom;
            this.f.left += f2 * 0.2f;
            this.f.right += f3 * 0.2f;
            this.f.top += f4 * 0.2f;
            this.f.bottom += f5 * 0.2f;
            if (f2 >= 1.0f || f3 >= 1.0f || f4 >= 1.0f || f5 >= 1.0f) {
                return this.f;
            }
            h();
            return null;
        } else if (this.d != 2) {
            return null;
        } else {
            PointF b3 = ic.a().b();
            this.f = c.a(this.g.b(), b3, b3, this.e / this.g.n());
            return this.f;
        }
    }

    public final boolean f() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.ZoomService.a(boolean, int):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.ZoomService.a(float, float):void
      udk.android.reader.view.pdf.ZoomService.a(float, android.graphics.RectF):void
      udk.android.reader.view.pdf.ZoomService.a(boolean, int):void */
    public final void g() {
        a(true, 1);
    }
}
