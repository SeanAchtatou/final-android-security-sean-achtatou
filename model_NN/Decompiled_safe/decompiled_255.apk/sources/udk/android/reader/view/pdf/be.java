package udk.android.reader.view.pdf;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import udk.android.b.m;

final class be extends RelativeLayout {
    private ImageView a;
    private TextView b;

    public be(Context context) {
        super(context);
        setGravity(17);
        Context context2 = getContext();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(m.a(context2, 90), m.a(context2, 130));
        this.a = new ImageView(context2);
        this.a.setLayoutParams(layoutParams);
        this.a.setBackgroundColor(-1);
        this.a.setPadding(m.a(context2, 2), m.a(context2, 2), m.a(context2, 2), m.a(context2, 2));
        this.a.setScaleType(ImageView.ScaleType.FIT_CENTER);
        addView(this.a);
        this.b = new TextView(context2);
        this.b.setLayoutParams(layoutParams);
        this.b.setGravity(17);
        addView(this.b);
    }

    public final ImageView a() {
        return this.a;
    }

    public final TextView b() {
        return this.b;
    }
}
