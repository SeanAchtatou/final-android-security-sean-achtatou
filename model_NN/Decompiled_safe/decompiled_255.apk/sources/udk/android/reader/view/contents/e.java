package udk.android.reader.view.contents;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import java.io.File;
import udk.android.b.d;
import udk.android.reader.a.a;

final class e implements View.OnClickListener {
    private /* synthetic */ d a;
    private final /* synthetic */ File b;
    private final /* synthetic */ ViewGroup c;
    private final /* synthetic */ View d;

    e(d dVar, File file, ViewGroup viewGroup, View view) {
        this.a = dVar;
        this.b = file;
        this.c = viewGroup;
        this.d = view;
    }

    public final void onClick(View view) {
        if (this.b != null) {
            a.a((Activity) this.c.getContext(), this.b.getAbsolutePath(), (String) null);
            d.a(this.d, 1157627903, 500);
        }
    }
}
