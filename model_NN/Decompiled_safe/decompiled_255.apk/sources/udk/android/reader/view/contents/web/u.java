package udk.android.reader.view.contents.web;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import com.unidocs.commonlib.a.b;
import udk.android.reader.C0000R;
import udk.android.reader.a.a;

final class u implements DialogInterface.OnClickListener {
    private /* synthetic */ g a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ Activity c;
    private final /* synthetic */ String d;

    u(g gVar, String[] strArr, Activity activity, String str) {
        this.a = gVar;
        this.b = strArr;
        this.c = activity;
        this.d = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = this.b[i];
        if (this.c.getString(C0000R.string.f91_).equals(str)) {
            a.a(this.c, this.d);
        } else if (this.c.getString(C0000R.string.f123).equals(str)) {
            EditText editText = new EditText(this.c);
            h hVar = new h(this, editText, this.c, this.d);
            editText.setText("download_" + b.a(0) + ".pdf");
            editText.setSelectAllOnFocus(true);
            editText.setInputType(1);
            editText.setSingleLine();
            editText.setOnEditorActionListener(new k(this, this.c, hVar, new AlertDialog.Builder(this.c).setTitle((int) C0000R.string.f94).setView(editText).setPositiveButton((int) C0000R.string.f50, new i(this, this.c, hVar)).setNegativeButton((int) C0000R.string.f53, new j(this)).show()));
            editText.postDelayed(new l(this, this.c, editText), 100);
        }
    }
}
