package udk.android.reader.view.pdf;

import android.app.Activity;
import android.os.Bundle;
import com.unidocs.commonlib.util.b;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import udk.android.b.j;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;

public class AnnotationMemoActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Annotation annotation;
        super.onCreate(bundle);
        int intExtra = getIntent().getIntExtra("refNo", Integer.MIN_VALUE);
        List h = s.a().h(getIntent().getIntExtra("page", Integer.MIN_VALUE));
        if (b.a((Collection) h)) {
            Iterator it = h.iterator();
            while (true) {
                if (it.hasNext()) {
                    annotation = (Annotation) it.next();
                    if (annotation.K() == intExtra) {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        annotation = null;
        if (annotation == null) {
            finish();
            return;
        }
        gq gqVar = new gq(this, annotation);
        gqVar.a(new ig(this));
        setContentView(gqVar);
        if (getIntent().getBooleanExtra("startWithReply", false)) {
            gqVar.a().run();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        j.a(this, !a.aL);
    }
}
