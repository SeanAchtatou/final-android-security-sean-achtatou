package udk.android.reader.view.pdf;

import android.view.SurfaceHolder;
import android.view.SurfaceView;
import udk.android.reader.b.c;

final class cm extends SurfaceView implements SurfaceHolder.Callback {
    private as a;
    private o b = new o(this);
    private PDFView c;
    private boolean d;

    cm(PDFView pDFView) {
        super(pDFView.getContext());
        this.c = pDFView;
        getHolder().addCallback(this);
        this.b.a(this.c);
        this.a = new as(this);
    }

    /* access modifiers changed from: package-private */
    public final PDFView a() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        this.a.a(i == 0);
    }

    /* access modifiers changed from: package-private */
    public final as b() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final o c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        c.a("## DISPOSE PDFViewInner");
        this.a.b(this.c);
        this.a.e();
        this.b.b(this.c);
        this.b.b();
    }

    public final void setVisibility(int i) {
        this.a.a(i == 0);
        super.setVisibility(i);
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        c.a("## SURFACE CHANGED - " + i2 + " * " + i3);
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        c.a("## SURFACE CREATED");
        as asVar = new as(this);
        asVar.a(this.a.b());
        asVar.a(this.a.d());
        asVar.a(this.a.c());
        this.a.b(this.c);
        this.a.e();
        this.a = asVar;
        this.a.a(this.c);
        this.a.a(getVisibility() == 0);
        this.a.f();
        this.d = true;
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.d = false;
        this.a.a(false);
        c.a("## SURFACE DESTROYED");
    }
}
