package udk.android.reader.view.contents;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.ae;
import udk.android.reader.pdf.au;

public class RecentPDFListView extends ListView implements au {
    /* access modifiers changed from: private */
    public u a;

    public RecentPDFListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public RecentPDFListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void a() {
        post(new k(this));
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setCacheColorHint(0);
        setDivider(getContext().getResources().getDrawable(C0000R.drawable.line));
        ae.a().a(this);
        this.a = new u(getContext());
        setAdapter((ListAdapter) this.a);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        c.a("## DISPOSE RecentPDFListView");
        ae.a().b(this);
        super.onDetachedFromWindow();
    }
}
