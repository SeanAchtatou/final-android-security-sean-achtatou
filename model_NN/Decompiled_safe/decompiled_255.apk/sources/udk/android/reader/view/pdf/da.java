package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.unidocs.commonlib.util.b;
import java.util.List;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;

public final class da extends BaseExpandableListAdapter {
    private static int a;
    private static final int b = 360;
    private static final int c;
    private static final int d;
    private s e = s.a();
    /* access modifiers changed from: private */
    public db f;
    private String g;

    static {
        a = 360;
        a = 361;
        int i = a;
        a = i + 1;
        c = i;
        int i2 = a;
        a = i2 + 1;
        d = i2;
    }

    public da(db dbVar) {
        this.f = dbVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public List getGroup(int i) {
        return b.b(this.g) ? this.e.h(this.e.i(i)) : this.e.a(this.e.b(i, this.g), this.g);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Annotation getChild(int i, int i2) {
        return (Annotation) getGroup(i).get(i2);
    }

    public final void a(String str) {
        this.g = str;
        notifyDataSetInvalidated();
    }

    public final long getChildId(int i, int i2) {
        return getGroupId(i) + ((long) i2);
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        LinearLayout linearLayout;
        if (view == null) {
            Context context = viewGroup.getContext();
            int a2 = (int) a.a(5.0f);
            LinearLayout linearLayout2 = new LinearLayout(context);
            linearLayout2.setOrientation(1);
            linearLayout2.setPadding(a2, a2, a2, a2);
            TextView textView = new TextView(context);
            textView.setId(c);
            textView.setGravity(16);
            textView.setPadding(a2, a2, a2, a2);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.weight = 0.0f;
            linearLayout2.addView(textView, layoutParams);
            TextView textView2 = new TextView(context);
            textView2.setId(d);
            textView2.setTextSize(textView.getTextSize() * 0.8f);
            textView2.setGravity(16);
            textView2.setPadding(a2, 0, a2, a2);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams2.weight = 0.0f;
            linearLayout2.addView(textView2, layoutParams2);
            linearLayout = linearLayout2;
        } else {
            linearLayout = view;
        }
        Annotation a3 = getChild(i, i2);
        if (!a3.Q()) {
            s.a().d(a3);
        }
        String N = a3.N();
        String str = b.b(N) ? udk.android.reader.b.b.X : N;
        String P = a3.P();
        ((TextView) linearLayout.findViewById(c)).setText(String.valueOf(str) + " ( " + (b.b(P) ? udk.android.reader.b.b.Z : P) + " )");
        TextView textView3 = (TextView) linearLayout.findViewById(d);
        if (b.a(a3.M())) {
            textView3.setText(a3.M());
            textView3.setVisibility(0);
        } else {
            textView3.setText("");
            textView3.setVisibility(8);
        }
        linearLayout.setOnClickListener(new cf(this, a3));
        return linearLayout;
    }

    public final int getChildrenCount(int i) {
        return getGroup(i).size();
    }

    public final int getGroupCount() {
        return b.b(this.g) ? this.e.j().size() : this.e.a(this.g).size();
    }

    public final long getGroupId(int i) {
        return (long) (i * 10000);
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        LinearLayout linearLayout;
        if (view == null) {
            Context context = viewGroup.getContext();
            int a2 = (int) a.a(5.0f);
            LinearLayout linearLayout2 = new LinearLayout(context);
            linearLayout2.setOrientation(0);
            linearLayout2.setPadding(a2, a2, a2, a2);
            View view2 = new View(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) a.a(50.0f), (int) a.a(30.0f));
            layoutParams.weight = 0.0f;
            linearLayout2.addView(view2, layoutParams);
            TextView textView = new TextView(context);
            textView.setId(b);
            textView.setGravity(16);
            textView.setPadding(a2, a2, a2, a2);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, (int) a.a(30.0f));
            layoutParams2.weight = 1.0f;
            linearLayout2.addView(textView, layoutParams2);
            linearLayout = linearLayout2;
        } else {
            linearLayout = view;
        }
        ((TextView) linearLayout.findViewById(b)).setText(String.valueOf(b.b(this.g) ? this.e.i(i) : this.e.b(i, this.g)) + " page ( " + getChildrenCount(i) + " )");
        return linearLayout;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
