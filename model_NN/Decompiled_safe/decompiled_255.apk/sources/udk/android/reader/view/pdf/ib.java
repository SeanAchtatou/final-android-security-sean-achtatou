package udk.android.reader.view.pdf;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class ib implements Runnable {
    private /* synthetic */ dy a;
    private final /* synthetic */ EditText b;

    ib(dy dyVar, EditText editText) {
        this.a = dyVar;
        this.b = editText;
    }

    public final void run() {
        ((InputMethodManager) this.a.a.getContext().getSystemService("input_method")).showSoftInput(this.b, 1);
    }
}
