package udk.android.reader.view.pdf;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.List;

public final class ez {
    int a;
    int b;
    float c;
    int d;
    int e;
    boolean f;
    Rect g;
    Rect h;
    Point i;
    Point j;
    private List k = new ArrayList();

    public final int a() {
        return this.k.size();
    }

    public final hh a(int i2) {
        return (hh) this.k.get(i2);
    }

    public final hh a(int i2, int i3) {
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= this.k.size()) {
                return null;
            }
            hh hhVar = (hh) this.k.get(i5);
            if (hhVar.c == i2 && hhVar.d == i3) {
                return hhVar;
            }
            i4 = i5 + 1;
        }
    }

    public final void a(hh hhVar) {
        this.k.add(hhVar);
    }

    public final List b() {
        ArrayList arrayList = new ArrayList(this.k.size());
        arrayList.addAll(this.k);
        return arrayList;
    }

    public final void b(int i2) {
        this.k.remove(i2);
    }

    public final boolean b(hh hhVar) {
        return this.k.remove(hhVar);
    }

    public final Bitmap c() {
        hh a2 = a(0);
        if (a2 == null) {
            return null;
        }
        return a2.e;
    }

    public final boolean c(hh hhVar) {
        return this.k.contains(hhVar);
    }

    public final Rect d() {
        Rect rect = new Rect();
        if (this.g.left - this.i.x < 0) {
            rect.left = 0;
        } else {
            rect.left = (((this.g.left - this.i.x) / this.j.x) * this.j.x) + this.i.x;
        }
        rect.right = (((this.g.right - this.i.x) / this.j.x) * this.j.x) + this.i.x;
        if (this.g.right > rect.right) {
            rect.right += this.j.x;
        }
        if (rect.right > this.d) {
            rect.right = this.d;
        }
        if (this.g.top - this.i.y < 0) {
            rect.top = 0;
        } else {
            rect.top = (((this.g.top - this.i.y) / this.j.y) * this.j.y) + this.i.y;
        }
        rect.bottom = (((this.g.bottom - this.i.y) / this.j.y) * this.j.y) + this.i.y;
        if (this.g.bottom > rect.bottom) {
            rect.bottom += this.j.y;
        }
        if (rect.bottom > this.e) {
            rect.bottom = this.e;
        }
        return rect;
    }

    public final Rect e() {
        if (this.k.size() <= 0) {
            return new Rect(0, 0, 0, 0);
        }
        Rect rect = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
        for (int i2 = 0; i2 < this.k.size(); i2++) {
            hh hhVar = (hh) this.k.get(i2);
            if (hhVar.c < rect.left) {
                rect.left = hhVar.c;
            }
            if (hhVar.c + hhVar.e.getWidth() > rect.right) {
                rect.right = hhVar.c + hhVar.e.getWidth();
            }
            if (hhVar.d < rect.top) {
                rect.top = hhVar.d;
            }
            if (hhVar.d + hhVar.e.getHeight() > rect.bottom) {
                rect.bottom = hhVar.e.getHeight() + hhVar.d;
            }
        }
        return rect;
    }
}
