package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.View;
import udk.android.a.i;
import udk.android.b.c;
import udk.android.reader.b.b;

final class fk implements View.OnClickListener {
    final /* synthetic */ au a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;

    fk(au auVar, Context context, String str) {
        this.a = auVar;
        this.b = context;
        this.c = str;
    }

    public final void onClick(View view) {
        i.a(this.b, this.a.c.getColor(), new an(this), this.c, b.r, false, c.a(this.a.c.getColor()));
    }
}
