package udk.android.reader.view.pdf.navigation;

import udk.android.reader.C0000R;

final class ax implements Runnable {
    private /* synthetic */ NavigationService a;

    ax(NavigationService navigationService) {
        this.a = navigationService;
    }

    public final void run() {
        this.a.q.setBackgroundResource(this.a.i.b(this.a.j.getContext(), this.a.k.z()) != null ? C0000R.drawable.btn_nav_remove_bookmark : C0000R.drawable.btn_nav_add_bookmark);
    }
}
