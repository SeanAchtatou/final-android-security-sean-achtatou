package udk.android.reader.view.pdf;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import udk.android.b.c;
import udk.android.reader.b.a;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ar;

final class hn extends RelativeLayout implements GestureDetector.OnGestureListener, ar {
    PDFView a;
    WebView b;
    GestureDetector c = new GestureDetector(this);
    private View d;
    /* access modifiers changed from: private */
    public Object e = new Object();
    /* access modifiers changed from: private */
    public long f;
    /* access modifiers changed from: private */
    public PDF g = PDF.a();
    private float h;
    /* access modifiers changed from: private */
    public float i = 1.0f;
    /* access modifiers changed from: private */
    public long j;
    /* access modifiers changed from: private */
    public bn k = bn.a();
    private String l;

    hn(PDFView pDFView) {
        super(pDFView.getContext());
        this.a = pDFView;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.b = new WebView(getContext());
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.setHorizontalScrollbarOverlay(true);
        this.b.setVerticalScrollbarOverlay(true);
        addView(this.b, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        this.d = new by(this, getContext());
        addView(this.d, layoutParams2);
    }

    private void d() {
        post(new bu(this));
    }

    static /* synthetic */ void e(hn hnVar) {
        String str = String.valueOf(hnVar.g.z()) + hnVar.g.E() + a.ar + a.aw + a.ax + a.ay + a.at + a.au + a.av + a.as;
        if (!str.equals(hnVar.l)) {
            hnVar.l = str;
            int a2 = a.ar ? c.a(255, a.aw, a.ax, a.ay) : -1;
            hnVar.setBackgroundColor(a2);
            hnVar.b.setBackgroundColor(a2);
            String str2 = a.ar ? String.valueOf(a.at) + "," + a.au + "," + a.av : "0,0,0";
            synchronized (hnVar.e) {
                hnVar.b.loadData("<div style='color:rgb( " + str2 + " );'>now loading...</div>", "text/html", "UTF-8");
            }
            new bs(hnVar).start();
        }
    }

    public final float a() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2) {
        float f3 = this.i * f2;
        float f4 = f3 < 0.7f ? 0.7f / this.i : f3 > 8.0f ? 8.0f / this.i : f2;
        this.b.loadUrl("javascript:zoom(" + f4 + ")");
        this.i = f4 * this.i;
        hg hgVar = new hg();
        hgVar.a = this.g.E();
        hgVar.b = this.i;
        this.a.af();
    }

    public final void a(ah ahVar) {
    }

    public final void a(boolean z) {
        a.ar = z;
        d();
    }

    public final void b() {
        int scrollY = this.b.getScrollY();
        long currentTimeMillis = System.currentTimeMillis();
        this.f = currentTimeMillis;
        this.b.pageUp(false);
        new cc(this, currentTimeMillis, scrollY).start();
    }

    /* access modifiers changed from: package-private */
    public final void b(float f2) {
        a(f2 / this.i);
    }

    public final void b(ah ahVar) {
        if (ahVar.c) {
            d();
            hg hgVar = new hg();
            hgVar.a = ahVar.a;
            hgVar.b = this.g.F();
            this.a.a(hgVar);
        }
    }

    public final void c() {
        int scrollY = this.b.getScrollY();
        long currentTimeMillis = System.currentTimeMillis();
        this.f = currentTimeMillis;
        this.b.pageDown(false);
        new ca(this, currentTimeMillis, scrollY).start();
    }

    public final void f() {
    }

    public final void g() {
    }

    public final void h() {
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.g.b(this);
        super.onDetachedFromWindow();
    }

    public final boolean onDown(MotionEvent motionEvent) {
        this.b.flingScroll(0, 0);
        this.h = -1.0f;
        return true;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        this.b.flingScroll(0, (int) (0.0f - f3));
        return true;
    }

    public final void onLongPress(MotionEvent motionEvent) {
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (motionEvent2.getPointerCount() == 1) {
            this.b.scrollBy(0, (int) f3);
        } else if (motionEvent2.getPointerCount() == 2) {
            float a2 = c.a(motionEvent2.getX(0), motionEvent2.getY(0), motionEvent2.getX(1), motionEvent2.getY(1));
            if (this.h > -1.0f) {
                a(a2 / this.h);
            }
            this.h = a2;
        }
        return true;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        if (System.currentTimeMillis() - this.j < 300) {
            this.j = -1;
            a(2.0f);
            return true;
        }
        this.j = System.currentTimeMillis();
        postDelayed(new bw(this, motionEvent), 300);
        return true;
    }

    public final void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z && getVisibility() == 0) {
            d();
        }
    }

    public final void setVisibility(int i2) {
        if (i2 == 0) {
            this.g.a(this);
        } else {
            this.g.b(this);
        }
        super.setVisibility(i2);
        if (i2 == 0) {
            d();
        }
    }
}
