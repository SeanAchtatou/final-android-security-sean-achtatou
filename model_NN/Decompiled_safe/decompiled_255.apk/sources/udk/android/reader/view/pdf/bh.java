package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class bh implements Runnable {
    private /* synthetic */ dc a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ EditText c;

    bh(dc dcVar, Context context, EditText editText) {
        this.a = dcVar;
        this.b = context;
        this.c = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.getSystemService("input_method")).showSoftInput(this.c, 1);
    }
}
