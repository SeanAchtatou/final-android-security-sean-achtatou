package udk.android.reader.view.contents;

import android.view.View;
import udk.android.reader.b.c;
import udk.android.reader.contents.b;

final class p extends Thread {
    private /* synthetic */ AllPDFListView a;

    p(AllPDFListView allPDFListView) {
        this.a = allPDFListView;
    }

    public final void run() {
        while (this.a.getWidth() <= 0) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                c.a((Throwable) e);
            }
        }
        b.a().a((View) this.a);
    }
}
