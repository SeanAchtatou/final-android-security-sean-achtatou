package udk.android.reader.view.pdf;

import android.view.View;
import udk.android.reader.pdf.PDF;
import udk.android.reader.view.pdf.PDFView;

final class hp implements View.OnClickListener {
    private /* synthetic */ gu a;
    private final /* synthetic */ int b;

    hp(gu guVar, int i) {
        this.a = guVar;
        this.b = i;
    }

    public final void onClick(View view) {
        PDF.a().j(Integer.valueOf(this.b + 1).intValue());
        this.a.a.a.a(PDFView.ViewMode.PDF);
    }
}
