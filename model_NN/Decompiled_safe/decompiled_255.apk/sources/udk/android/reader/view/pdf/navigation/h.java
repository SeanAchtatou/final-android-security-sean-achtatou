package udk.android.reader.view.pdf.navigation;

import android.graphics.Bitmap;
import android.widget.ImageView;

final class h implements Runnable {
    private /* synthetic */ o a;
    private final /* synthetic */ ImageView b;
    private final /* synthetic */ Bitmap c;

    h(o oVar, ImageView imageView, Bitmap bitmap) {
        this.a = oVar;
        this.b = imageView;
        this.c = bitmap;
    }

    public final void run() {
        this.b.setImageBitmap(this.c);
    }
}
