package udk.android.reader.view.pdf;

import android.view.View;
import udk.android.b.m;

final class ap implements Runnable {
    private /* synthetic */ ax a;
    private final /* synthetic */ View b;

    ap(ax axVar, View view) {
        this.a = axVar;
        this.b = view;
    }

    public final void run() {
        this.b.setFocusableInTouchMode(false);
        this.b.setFocusable(false);
        m.a(this.b);
        ar.a().c();
    }
}
