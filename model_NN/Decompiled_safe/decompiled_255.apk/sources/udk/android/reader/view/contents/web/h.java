package udk.android.reader.view.contents.web;

import android.app.Activity;
import android.widget.EditText;
import udk.android.reader.contents.b;

final class h implements Runnable {
    private /* synthetic */ u a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Activity c;
    private final /* synthetic */ String d;

    h(u uVar, EditText editText, Activity activity, String str) {
        this.a = uVar;
        this.b = editText;
        this.c = activity;
        this.d = str;
    }

    public final void run() {
        String editable = this.b.getText().toString();
        if (!editable.toLowerCase().endsWith(".pdf")) {
            editable = String.valueOf(editable) + ".pdf";
        }
        b.a().a(this.c, this.d, editable);
    }
}
