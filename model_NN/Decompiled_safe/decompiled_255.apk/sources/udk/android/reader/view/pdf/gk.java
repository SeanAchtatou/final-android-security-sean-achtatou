package udk.android.reader.view.pdf;

import android.view.MotionEvent;
import android.view.View;

final class gk implements View.OnTouchListener {
    private /* synthetic */ PDFView a;

    gk(PDFView pDFView) {
        this.a = pDFView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.a.a.seekTo(this.a.a.getDuration());
        return true;
    }
}
