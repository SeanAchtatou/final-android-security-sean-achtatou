package udk.android.reader.view.contents.web;

import android.view.MotionEvent;
import android.view.View;

final class m implements View.OnTouchListener {
    private /* synthetic */ g a;

    m(g gVar) {
        this.a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.contents.web.g.a(udk.android.reader.view.contents.web.g, boolean):void
     arg types: [udk.android.reader.view.contents.web.g, int]
     candidates:
      udk.android.reader.view.contents.web.g.a(android.webkit.WebView, java.lang.String):boolean
      udk.android.reader.view.contents.web.g.a(udk.android.reader.view.contents.web.g, boolean):void */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        g.a(this.a, false);
        return false;
    }
}
