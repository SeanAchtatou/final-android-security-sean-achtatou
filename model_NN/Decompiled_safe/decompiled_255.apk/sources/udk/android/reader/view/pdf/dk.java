package udk.android.reader.view.pdf;

import android.view.View;
import udk.android.reader.b.c;

final class dk implements View.OnClickListener {
    private /* synthetic */ PDFReadingToolbar a;
    private final /* synthetic */ PDFView b;

    dk(PDFReadingToolbar pDFReadingToolbar, PDFView pDFView) {
        this.a = pDFReadingToolbar;
        this.b = pDFView;
    }

    public final void onClick(View view) {
        try {
            this.b.M().j();
        } catch (Exception e) {
            c.a((Throwable) e);
        }
    }
}
