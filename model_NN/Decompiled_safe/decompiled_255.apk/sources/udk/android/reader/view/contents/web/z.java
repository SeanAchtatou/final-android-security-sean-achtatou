package udk.android.reader.view.contents.web;

import java.net.URL;
import udk.android.reader.b.c;

final class z extends Thread {
    private /* synthetic */ g a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Runnable c;
    private final /* synthetic */ Runnable d;

    z(g gVar, String str, Runnable runnable, Runnable runnable2) {
        this.a = gVar;
        this.b = str;
        this.c = runnable;
        this.d = runnable2;
    }

    public final void run() {
        boolean z;
        if (this.b.toLowerCase().endsWith(".pdf")) {
            z = true;
        } else {
            try {
                String headerField = new URL(this.b).openConnection().getHeaderField("Content-Type");
                z = headerField != null && headerField.toLowerCase().indexOf("pdf") >= 0;
            } catch (Exception e) {
                c.a((Throwable) e);
                z = false;
            }
        }
        if (z && this.c != null) {
            this.c.run();
        } else if (!z && this.d != null) {
            this.d.run();
        }
    }
}
