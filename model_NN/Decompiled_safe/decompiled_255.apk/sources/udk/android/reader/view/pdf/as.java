package udk.android.reader.view.pdf;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.SurfaceHolder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import udk.android.b.m;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.a;
import udk.android.reader.pdf.aj;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.w;
import udk.android.reader.pdf.b;

public final class as implements b {
    private static Paint b;
    private static Paint c;
    private static Paint d;
    private static Paint e;
    private static Paint f;
    private static Paint g;
    private static Paint h = null;
    private static Paint i;
    private int A;
    private float B;
    private float C;
    private float D;
    private boolean E;
    private v F;
    private Bitmap G;
    private int H;
    private aq I;
    /* access modifiers changed from: private */
    public int J;
    /* access modifiers changed from: private */
    public long K;
    private List L = new ArrayList();
    private SurfaceHolder a;
    private Timer j;
    /* access modifiers changed from: private */
    public PDF k;
    private hx l;
    private ic m;
    private ar n;
    private ZoomService o;
    private ce p;
    private b q;
    private a r;
    private o s;
    private s t;
    private boolean u;
    private boolean v;
    private Bitmap w;
    private PointF x;
    private int y;
    private boolean z;

    static {
        Paint paint = new Paint(1);
        b = paint;
        paint.setColor(udk.android.reader.b.a.aT);
        Paint paint2 = new Paint(1);
        c = paint2;
        paint2.setColor(-2013248512);
        Paint paint3 = new Paint(1);
        d = paint3;
        paint3.setColor(-1);
        Paint paint4 = new Paint(1);
        g = paint4;
        paint4.setColor(-1);
        Paint paint5 = new Paint(1);
        e = paint5;
        paint5.setStyle(Paint.Style.STROKE);
        e.setColor(udk.android.reader.b.a.aS);
        e.setStrokeWidth(0.0f);
        Paint paint6 = new Paint(e);
        f = paint6;
        paint6.setStyle(Paint.Style.FILL);
        Paint paint7 = new Paint();
        i = paint7;
        paint7.setColor(-1);
    }

    public as(cm cmVar) {
        this.H = m.a(cmVar.getContext(), 16);
        this.a = cmVar.getHolder();
        this.s = cmVar.c();
    }

    private void a(Canvas canvas, int i2, float f2, RectF rectF) {
        if (!this.l.t()) {
            if (udk.android.reader.b.a.R == 2) {
                float centerX = rectF.centerX();
                float f3 = rectF.bottom + udk.android.reader.b.a.S;
                int i3 = i2 + 1;
                while (f3 <= ((float) this.m.b) && i3 <= this.k.l()) {
                    float a2 = this.l.a(i3, f2);
                    float b2 = this.l.b(i3, f2);
                    float f4 = centerX - (a2 / 2.0f);
                    RectF rectF2 = new RectF(f4, f3, a2 + f4, f3 + b2);
                    Bitmap a3 = hx.a(i3);
                    if (a3 != null) {
                        canvas.drawBitmap(a3, new Rect(0, 0, a3.getWidth(), a3.getHeight()), rectF2, h);
                    } else {
                        canvas.drawRect(rectF2, g);
                    }
                    i3++;
                    f3 += udk.android.reader.b.a.S + b2;
                }
                float f5 = rectF.top - udk.android.reader.b.a.S;
                int i4 = i2 - 1;
                while (f5 >= 0.0f && i4 > 0) {
                    float a4 = this.l.a(i4, f2);
                    float b3 = this.l.b(i4, f2);
                    float f6 = centerX - (a4 / 2.0f);
                    RectF rectF3 = new RectF(f6, f5 - b3, a4 + f6, f5);
                    Bitmap a5 = hx.a(i4);
                    if (a5 != null) {
                        canvas.drawBitmap(a5, new Rect(0, 0, a5.getWidth(), a5.getHeight()), rectF3, h);
                    } else {
                        canvas.drawRect(rectF3, g);
                    }
                    i4--;
                    f5 -= udk.android.reader.b.a.S + b3;
                }
            } else if (udk.android.reader.b.a.R == 3) {
                float centerY = rectF.centerY();
                int i5 = this.k.s() == 1 ? 1 : -1;
                float f7 = rectF.right + udk.android.reader.b.a.S;
                int i6 = i2 + i5;
                while (f7 <= ((float) this.m.a) && this.k.f(i6)) {
                    float a6 = this.l.a(i6, f2);
                    float b4 = this.l.b(i6, f2);
                    float f8 = centerY - (b4 / 2.0f);
                    RectF rectF4 = new RectF(f7, f8, f7 + a6, b4 + f8);
                    Bitmap a7 = hx.a(i6);
                    if (a7 != null) {
                        canvas.drawBitmap(a7, new Rect(0, 0, a7.getWidth(), a7.getHeight()), rectF4, h);
                    } else {
                        canvas.drawRect(rectF4, g);
                    }
                    i6 += i5;
                    f7 += a6 + udk.android.reader.b.a.S;
                }
                float f9 = rectF.left - udk.android.reader.b.a.S;
                int i7 = i2 - i5;
                while (f9 >= 0.0f && this.k.f(i7)) {
                    float a8 = this.l.a(i7, f2);
                    float b5 = this.l.b(i7, f2);
                    float f10 = centerY - (b5 / 2.0f);
                    RectF rectF5 = new RectF(f9 - a8, f10, f9, b5 + f10);
                    Bitmap a9 = hx.a(i7);
                    if (a9 != null) {
                        canvas.drawBitmap(a9, new Rect(0, 0, a9.getWidth(), a9.getHeight()), rectF5, h);
                    } else {
                        canvas.drawRect(rectF5, g);
                    }
                    i7 -= i5;
                    f9 -= a8 + udk.android.reader.b.a.S;
                }
            }
        }
    }

    private void a(Canvas canvas, ez ezVar, int i2, float f2, float f3) {
        u uVar = new u();
        if (this.o.f()) {
            RectF e2 = this.o.e();
            uVar.f = 2;
            if (e2 != null) {
                RectF b2 = this.l.b();
                uVar.g = e2;
                uVar.e = (e2.width() / b2.width()) * f2;
                if (ezVar != null) {
                    a(canvas, ezVar, i2, f2, b2, e2);
                }
                a(canvas, i2, uVar.e, e2.left, e2.top);
            }
        } else {
            this.n.f();
            if (ezVar != null) {
                canvas.drawARGB(255, udk.android.reader.b.a.aO, udk.android.reader.b.a.aP, udk.android.reader.b.a.aQ);
                Bitmap bitmap = null;
                if (f2 > f3) {
                    bitmap = hx.a(i2);
                    if (bitmap != null) {
                        canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), this.l.b(), h);
                    } else {
                        canvas.drawRect(this.l.b(), g);
                    }
                    if (this.w != null) {
                        if (ezVar.f || i2 != this.y) {
                            this.w = null;
                        } else {
                            canvas.drawBitmap(this.w, this.l.a - this.x.x, this.l.b - this.x.y, h);
                        }
                    }
                }
                int a2 = ezVar.a();
                if (i2 != this.k.E()) {
                    c.a("## INVALID RENDER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    this.l.p();
                } else {
                    for (int i3 = 0; i3 < a2; i3++) {
                        hh a3 = ezVar.a(i3);
                        if (a3.a == i2) {
                            if (a3.b == f2) {
                                canvas.drawBitmap(a3.e, this.l.a + ((float) a3.c), ((float) a3.d) + this.l.b, h);
                            } else {
                                float f4 = f2 / a3.b;
                                canvas.drawBitmap(a3.e, new Rect(0, 0, a3.e.getWidth(), a3.e.getHeight()), new RectF(this.l.a + (((float) a3.c) * f4), this.l.b + (((float) a3.d) * f4), this.l.a + (((float) (a3.c + a3.e.getWidth())) * f4), (f4 * ((float) (a3.d + a3.e.getHeight()))) + this.l.b), h);
                            }
                        }
                    }
                }
                if (a2 == 0 && bitmap == null) {
                    this.z = true;
                    h();
                } else if (this.z) {
                    this.z = false;
                    i();
                }
            }
            a(canvas, i2, f2, this.l.a, this.l.b);
            if (!this.r.d() || !this.r.a(i2)) {
                this.G = null;
            } else {
                if (this.G == null && this.F != null) {
                    this.G = this.F.a();
                }
                if (this.G != null) {
                    canvas.drawBitmap(this.G, this.l.a + 10.0f, this.l.b, h);
                }
            }
            RectF b3 = this.s.b(f2, udk.android.reader.b.a.O / 2.0f);
            if (b3 != null) {
                canvas.drawRect(b3.left + this.l.a, b3.top + this.l.b, b3.right + this.l.a, this.l.b + b3.bottom, e);
            }
            if (this.l.e() < this.m.a - 1 && i2 > 1 && udk.android.reader.b.a.an && this.l.t()) {
                cg e3 = this.p.e();
                if (!(this.p.b() && this.k.E() == 2 && e3 != null && e3.b == 2)) {
                    int s2 = this.k.s();
                    canvas.drawRect(new RectF(s2 == 1 ? this.l.a - ((float) this.l.e()) : this.l.c(), this.l.b, s2 == 1 ? this.l.a : this.l.c() + ((float) this.l.e()), this.l.b + ((float) this.l.i())), i);
                    canvas.drawLine(s2 == 1 ? this.l.a : this.l.c(), this.l.b, s2 == 1 ? this.l.a : this.l.c(), ((float) this.l.i()) + this.l.b, this.p.d());
                    canvas.save();
                    canvas.rotate(-90.0f, s2 == 1 ? this.l.a : this.l.c(), this.l.b);
                    canvas.translate(s2 == 1 ? this.l.a : this.l.c(), this.l.b);
                    canvas.drawRect(new RectF((float) (0 - this.l.i()), 0.0f, 0.0f, 40.0f), this.p.c());
                    canvas.scale(1.0f, -1.0f);
                    canvas.drawRect(new RectF((float) (0 - this.l.i()), 0.0f, 0.0f, 40.0f), this.p.c());
                    canvas.restore();
                }
            }
            if (this.p.b()) {
                this.p.a(canvas, this);
            }
            a(canvas, i2, f2, this.l.b());
            uVar.f = 1;
        }
        this.C = this.l.a;
        this.D = this.l.b;
        this.E = ezVar != null && ezVar.f;
        uVar.a = this.A != i2;
        uVar.b = i2;
        this.A = i2;
        uVar.c = this.B != f2;
        uVar.d = f2;
        this.B = f2;
        a(uVar);
    }

    private static void a(Canvas canvas, ez ezVar, int i2, float f2, RectF rectF, RectF rectF2) {
        canvas.drawARGB(255, udk.android.reader.b.a.aO, udk.android.reader.b.a.aP, udk.android.reader.b.a.aQ);
        Bitmap a2 = hx.a(i2);
        if (a2 != null) {
            canvas.drawBitmap(a2, new Rect(0, 0, a2.getWidth(), a2.getHeight()), rectF2, h);
        }
        float width = rectF2.width() / rectF.width();
        float height = rectF2.height() / rectF.height();
        if (ezVar != null) {
            int a3 = ezVar.a();
            for (int i3 = 0; i3 < a3; i3++) {
                hh a4 = ezVar.a(i3);
                if (a4.b == f2 && a4.a == i2) {
                    RectF rectF3 = new RectF(rectF2.left + (((float) a4.c) * width), rectF2.top + (((float) a4.d) * height), rectF2.left + (((float) (a4.c + a4.e.getWidth())) * width), rectF2.top + (((float) (a4.d + a4.e.getHeight())) * height));
                    canvas.drawBitmap(a4.e, new Rect(0, 0, a4.e.getWidth(), a4.e.getHeight()), rectF3, h);
                }
            }
        }
    }

    private void a(u uVar) {
        for (t a2 : this.L) {
            a2.a(uVar);
        }
    }

    private static boolean a(List list, Canvas canvas, Paint paint, float f2) {
        if (list == null) {
            return false;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            canvas.drawPath(((udk.android.reader.pdf.a.a) it.next()).a(f2), paint);
        }
        return true;
    }

    private void h() {
        for (t b2 : this.L) {
            b2.b();
        }
    }

    private void i() {
        for (t c2 : this.L) {
            c2.c();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Canvas canvas, int i2, float f2, float f3, float f4) {
        canvas.save();
        canvas.translate(f3, f4);
        a(this.q.b(i2), canvas, b, f2);
        List<Annotation> c2 = aj.a().c(i2);
        if (com.unidocs.commonlib.util.b.a((Collection) c2)) {
            for (Annotation a2 : c2) {
                a2.a(canvas, f2);
            }
        }
        if (i2 == this.k.E() && a(this.s.s(), canvas, c, f2)) {
            o oVar = this.s;
            PointF a3 = oVar.a(f2);
            if (a3 != null) {
                canvas.drawCircle(a3.x, a3.y - ((float) (this.H / 2)), (float) (this.H / 2), c);
                canvas.drawCircle(a3.x, a3.y - ((float) (this.H / 2)), (float) (this.H / 4), d);
            }
            PointF b2 = oVar.b(f2);
            if (b2 != null) {
                canvas.drawCircle(b2.x, b2.y + ((float) (this.H / 2)), (float) (this.H / 2), c);
                canvas.drawCircle(b2.x, b2.y + ((float) (this.H / 2)), (float) (this.H / 4), d);
            }
        }
        List h2 = this.t.h(i2);
        if (com.unidocs.commonlib.util.b.a((Collection) h2)) {
            int size = h2.size();
            boolean a4 = n.b().a(i2, f2);
            for (int i3 = 0; i3 < size; i3++) {
                try {
                    Annotation annotation = (Annotation) h2.get(i3);
                    if (!annotation.h() && (a4 || !annotation.U())) {
                        annotation.a(canvas, f2);
                    }
                } catch (Exception e2) {
                    c.a((Throwable) e2);
                }
            }
            if (this.t.b()) {
                Annotation c3 = this.t.c();
                if (c3.ag() == i2) {
                    RectF a5 = udk.android.b.c.a(c3.b(f2), 3.0f);
                    if (!(c3 instanceof w)) {
                        canvas.drawRect(a5, e);
                    }
                    if (c3.i() || c3.j()) {
                        if (c3 instanceof w) {
                            w wVar = (w) c3;
                            PointF c4 = wVar.c(f2);
                            PointF d2 = wVar.d(f2);
                            canvas.drawCircle(c4.x, c4.y, (float) (this.H / 2), f);
                            canvas.drawCircle(c4.x, c4.y, (float) (this.H / 4), d);
                            canvas.drawCircle(d2.x, d2.y, (float) (this.H / 2), f);
                            canvas.drawCircle(d2.x, d2.y, (float) (this.H / 4), d);
                        } else {
                            boolean j2 = c3.j();
                            float[] fArr = {a5.left, a5.centerX(), a5.right};
                            float[] fArr2 = {a5.top, a5.centerY(), a5.bottom};
                            for (int i4 = 0; i4 < fArr2.length; i4++) {
                                for (int i5 = 0; i5 < fArr.length; i5++) {
                                    if (!(i5 == 1 && i4 == 1) && j2) {
                                        canvas.drawCircle(fArr[i5], fArr2[i4], (float) (this.H / 2), f);
                                        canvas.drawCircle(fArr[i5], fArr2[i4], (float) (this.H / 4), d);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        canvas.restore();
    }

    /* access modifiers changed from: package-private */
    public final void a(Canvas canvas, ez ezVar) {
        a(canvas, ezVar, this.k.E(), this.l.n(), this.l.w());
    }

    /* access modifiers changed from: package-private */
    public final void a(aq aqVar) {
        this.I = aqVar;
    }

    public final void a(bm bmVar) {
        if (udk.android.reader.b.a.af) {
            float F2 = this.k.F();
            if (F2 != bmVar.d) {
                try {
                    RectF rectF = bmVar.e;
                    ez u2 = this.l.u();
                    ic a2 = ic.a();
                    this.w = Bitmap.createBitmap(a2.a, a2.b, Bitmap.Config.RGB_565);
                    Canvas canvas = new Canvas(this.w);
                    int E2 = this.k.E();
                    this.l.w();
                    a(canvas, u2, E2, F2, this.l.b(), rectF);
                    this.x = new PointF(rectF.left, rectF.top);
                    this.y = this.k.E();
                } catch (Throwable th) {
                    c.a(th);
                    this.w = null;
                }
            }
        }
    }

    public final void a(t tVar) {
        if (!this.L.contains(tVar)) {
            this.L.add(tVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(v vVar) {
        this.F = vVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2) {
        this.v = !z2;
    }

    /* access modifiers changed from: package-private */
    public final v b() {
        return this.F;
    }

    public final void b(bm bmVar) {
    }

    public final void b(t tVar) {
        this.L.remove(tVar);
    }

    public final void b_() {
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        return !this.v;
    }

    /* access modifiers changed from: package-private */
    public final aq d() {
        return this.I;
    }

    public final void e() {
        if (this.o != null) {
            this.o.b(this);
        }
        if (this.j != null) {
            this.j.cancel();
            this.j = null;
        }
    }

    public final void f() {
        this.k = PDF.a();
        this.l = hx.a();
        this.m = ic.a();
        this.n = ar.a();
        this.p = ce.a();
        this.q = b.a();
        this.r = a.a();
        this.o = ZoomService.a();
        this.o.a(this);
        this.t = s.a();
        this.u = true;
        this.j = new Timer(true);
        this.j.schedule(new ch(this), 0, 16);
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        if (!this.v && this.k.f()) {
            int E2 = this.k.E();
            float F2 = this.k.F();
            float w2 = this.l.w();
            if (F2 != 0.01f) {
                if (this.l.r() || this.n.d() || this.o.f() || this.p.b()) {
                    this.l.q();
                } else if (this.l.s()) {
                    return;
                } else {
                    if (this.E && this.l.a == this.C && this.l.b == this.D && E2 == this.A && F2 == this.B) {
                        return;
                    }
                }
                ez u2 = this.l.u();
                if (this.u) {
                    Canvas lockCanvas = this.a.lockCanvas();
                    a(lockCanvas, u2, E2, F2, w2);
                    this.a.unlockCanvasAndPost(lockCanvas);
                    return;
                }
                this.E = false;
            }
        }
    }
}
