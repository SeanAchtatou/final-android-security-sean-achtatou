package udk.android.reader.view.contents.web;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import com.unidocs.commonlib.util.b;
import udk.android.reader.C0000R;
import udk.android.reader.b.e;

final class x implements DialogInterface.OnClickListener {
    private /* synthetic */ p a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ Context c;

    x(p pVar, String[] strArr, Context context) {
        this.a = pVar;
        this.b = strArr;
        this.c = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = this.b[i];
        if (this.c.getString(C0000R.string.f85).equals(str)) {
            this.a.a.a(e.d);
        } else if (this.c.getString(C0000R.string.f86).equals(str)) {
            String url = this.a.a.a.getUrl();
            if (b.b(url) || url.startsWith("data:")) {
                url = "none";
            }
            e.d = url;
            e.a(this.c, this.c.getString(C0000R.string.conf_webbookcase_homepage), e.d);
            Toast.makeText(this.c, (int) C0000R.string.f84, 0).show();
        }
    }
}
