package udk.android.reader.view.pdf;

import android.graphics.RectF;
import udk.android.reader.pdf.a.c;

final class dm implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ c c;

    dm(PDFView pDFView, String str, c cVar) {
        this.a = pDFView;
        this.b = str;
        this.c = cVar;
    }

    public final void run() {
        this.a.K.a(this.b, this.c, new RectF((float) this.a.Q, (float) this.a.R, (float) (this.a.getWidth() - this.a.S), (float) (this.a.getHeight() - this.a.T)), this.a.p, null);
    }
}
