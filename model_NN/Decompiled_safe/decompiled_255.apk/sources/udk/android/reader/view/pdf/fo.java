package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.View;
import udk.android.a.i;
import udk.android.reader.b.b;
import udk.android.reader.pdf.annotation.Annotation;

final class fo implements View.OnClickListener {
    final /* synthetic */ au a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;
    private final /* synthetic */ View d;
    private final /* synthetic */ Annotation e;

    fo(au auVar, Context context, String str, View view, Annotation annotation) {
        this.a = auVar;
        this.b = context;
        this.c = str;
        this.d = view;
        this.e = annotation;
    }

    public final void onClick(View view) {
        i.a(this.b, this.a.b.getColor(), new ao(this, this.d, this.e), this.c, b.r, true);
    }
}
