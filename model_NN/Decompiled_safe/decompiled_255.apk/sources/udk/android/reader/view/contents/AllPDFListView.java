package udk.android.reader.view.contents;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import java.util.Collection;
import java.util.List;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.b.e;
import udk.android.reader.contents.a;
import udk.android.reader.contents.b;
import udk.android.reader.contents.l;

public class AllPDFListView extends ExpandableListView implements a {
    /* access modifiers changed from: private */
    public d a;
    /* access modifiers changed from: private */
    public ProgressBar b;

    public AllPDFListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AllPDFListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: private */
    public void c() {
        new p(this).start();
    }

    /* access modifiers changed from: private */
    public void d() {
        post(new o(this));
    }

    public final void a() {
        d();
    }

    public final void a(ProgressBar progressBar) {
        this.b = progressBar;
    }

    public final void a(l lVar) {
        if (!udk.android.reader.b.a.a(getContext(), lVar.a)) {
            d();
        }
    }

    public final void b() {
        if (this.b != null) {
            post(new s(this));
        }
    }

    public final void b(l lVar) {
        if (!udk.android.reader.b.a.a(getContext(), lVar.a)) {
            d();
        }
    }

    public final void c(l lVar) {
        if (!udk.android.reader.b.a.a(getContext(), lVar.a)) {
            d();
        }
    }

    public final void d(l lVar) {
        if (!udk.android.reader.b.a.a(getContext(), lVar.a)) {
            d();
        }
    }

    public final void e(l lVar) {
        if (this.b != null) {
            post(new q(this, lVar));
        }
    }

    public final void f(l lVar) {
        d();
    }

    /* access modifiers changed from: protected */
    public void layoutChildren() {
        try {
            super.layoutChildren();
        } catch (Exception e) {
            c.a((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setCacheColorHint(0);
        setDivider(getContext().getResources().getDrawable(C0000R.drawable.line));
        setChildDivider(getContext().getResources().getDrawable(C0000R.drawable.line));
        this.a = new d();
        setAdapter(this.a);
        b a2 = b.a();
        a2.a((a) this);
        List b2 = a2.b();
        if (e.b && com.unidocs.commonlib.util.b.b((Collection) b2)) {
            b2 = b.b(getContext());
        }
        if (com.unidocs.commonlib.util.b.a((Collection) b2)) {
            a2.a(b2);
            d();
            new n(this, a2, b2).start();
            return;
        }
        c();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        b.a().b(this);
        c.a("## DISPOSE AllPDFListView");
        super.onDetachedFromWindow();
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        try {
            super.onRestoreInstanceState(parcelable);
        } catch (Exception e) {
            c.a((Throwable) e);
            new r(this).start();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return super.onTouchEvent(motionEvent);
        } catch (Exception e) {
            c.a((Throwable) e);
            return true;
        }
    }
}
