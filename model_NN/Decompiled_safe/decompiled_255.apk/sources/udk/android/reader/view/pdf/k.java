package udk.android.reader.view.pdf;

import android.content.DialogInterface;
import android.widget.EditText;
import com.unidocs.commonlib.util.b;

final class k implements DialogInterface.OnClickListener {
    private /* synthetic */ in a;
    private final /* synthetic */ EditText b;

    k(in inVar, EditText editText) {
        this.a = inVar;
        this.b = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String[] strArr = null;
        String editable = this.b.getText().toString();
        if (b.a(editable)) {
            strArr = editable.split("\n");
        }
        in.a(this.a, strArr);
    }
}
