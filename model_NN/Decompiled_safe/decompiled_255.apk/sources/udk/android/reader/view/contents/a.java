package udk.android.reader.view.contents;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import java.io.File;
import udk.android.reader.C0000R;
import udk.android.reader.contents.b;

final class a implements DialogInterface.OnClickListener {
    private /* synthetic */ i a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ Context c;
    private final /* synthetic */ File d;

    a(i iVar, String[] strArr, Context context, File file) {
        this.a = iVar;
        this.b = strArr;
        this.c = context;
        this.d = file;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = this.b[i];
        if (this.c.getString(C0000R.string.f126).equals(str)) {
            b.a().a((Activity) this.c, new File[]{this.d});
        } else if (this.c.getString(C0000R.string.f96__).equals(str)) {
            b.a().a(this.c, new File[]{this.d});
        }
    }
}
