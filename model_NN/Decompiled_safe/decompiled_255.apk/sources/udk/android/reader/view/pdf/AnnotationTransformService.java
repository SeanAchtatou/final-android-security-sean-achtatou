package udk.android.reader.view.pdf;

import android.graphics.RectF;
import android.view.MotionEvent;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;

public final class AnnotationTransformService implements ae {
    private static /* synthetic */ int[] i;
    private PDFView a;
    private hx b = hx.a();
    /* access modifiers changed from: private */
    public cl c;
    private bn d = bn.a();
    private Annotation e;
    private Annotation.TransformingType f;
    private RectF g;
    private boolean h;

    enum ScalingType {
        NONE,
        SCALE_ONLY,
        MOVE_SCALE
    }

    public AnnotationTransformService(PDFView pDFView, cl clVar, Annotation annotation, Annotation.TransformingType transformingType) {
        this.a = pDFView;
        this.c = clVar;
        this.e = annotation;
        this.f = transformingType;
        this.g = annotation.b(this.b.n());
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = i;
        if (iArr == null) {
            iArr = new int[Annotation.TransformingType.values().length];
            try {
                iArr[Annotation.TransformingType.END_HEAD.ordinal()] = 11;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Annotation.TransformingType.MOVE.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_CENTER_BOTTOM.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_CENTER_TOP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_LEFT_BOTTOM.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_LEFT_MIDDLE.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_LEFT_TOP.ordinal()] = 2;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_RIGHT_BOTTOM.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_RIGHT_MIDDLE.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[Annotation.TransformingType.SCALE_RIGHT_TOP.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[Annotation.TransformingType.START_HEAD.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            i = iArr;
        }
        return iArr;
    }

    public final void a() {
        this.h = true;
        this.c.a((ae) this);
    }

    public final void a(MotionEvent motionEvent) {
    }

    public final void b(MotionEvent motionEvent) {
        ScalingType scalingType;
        ScalingType scalingType2;
        if (this.h) {
            if (this.f == Annotation.TransformingType.MOVE) {
                float x = (motionEvent.getX() - this.d.g) / this.b.n();
                float y = (motionEvent.getY() - this.d.h) / this.b.n();
                this.e.f(x);
                this.e.g(y);
            } else {
                switch (b()[this.f.ordinal()]) {
                    case 2:
                        scalingType = ScalingType.MOVE_SCALE;
                        scalingType2 = ScalingType.MOVE_SCALE;
                        break;
                    case 3:
                        scalingType = ScalingType.MOVE_SCALE;
                        scalingType2 = ScalingType.NONE;
                        break;
                    case 4:
                        scalingType = ScalingType.MOVE_SCALE;
                        scalingType2 = ScalingType.SCALE_ONLY;
                        break;
                    case 5:
                        scalingType = ScalingType.NONE;
                        scalingType2 = ScalingType.MOVE_SCALE;
                        break;
                    case 6:
                        scalingType = ScalingType.NONE;
                        scalingType2 = ScalingType.SCALE_ONLY;
                        break;
                    case 7:
                        scalingType = ScalingType.SCALE_ONLY;
                        scalingType2 = ScalingType.MOVE_SCALE;
                        break;
                    case 8:
                        scalingType = ScalingType.SCALE_ONLY;
                        scalingType2 = ScalingType.NONE;
                        break;
                    case 9:
                        scalingType = ScalingType.SCALE_ONLY;
                        scalingType2 = ScalingType.SCALE_ONLY;
                        break;
                    default:
                        scalingType2 = null;
                        scalingType = null;
                        break;
                }
                if (scalingType == ScalingType.SCALE_ONLY) {
                    this.e.h(((motionEvent.getX() - this.b.a) - this.g.left) / this.g.width());
                } else if (scalingType == ScalingType.MOVE_SCALE) {
                    float x2 = (this.g.right - (motionEvent.getX() - this.b.a)) / this.g.width();
                    this.e.h(x2);
                    this.e.f((this.g.width() - (x2 * this.g.width())) / this.b.n());
                } else {
                    this.e.h(1.0f);
                }
                if (scalingType2 == ScalingType.SCALE_ONLY) {
                    this.e.i(((motionEvent.getY() - this.b.b) - this.g.top) / this.g.height());
                } else if (scalingType2 == ScalingType.MOVE_SCALE) {
                    float y2 = (this.g.bottom - (motionEvent.getY() - this.b.b)) / this.g.height();
                    this.e.i(y2);
                    this.e.g((this.g.height() - (y2 * this.g.height())) / this.b.n());
                } else {
                    this.e.i(1.0f);
                }
            }
            this.b.p();
        }
    }

    public final void c(MotionEvent motionEvent) {
        s.a().c(this.e);
        this.h = false;
        this.a.R();
        new ba(this).start();
    }
}
