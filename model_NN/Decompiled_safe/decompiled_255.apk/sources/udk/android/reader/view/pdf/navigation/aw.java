package udk.android.reader.view.pdf.navigation;

final class aw implements Runnable {
    private /* synthetic */ NavigationService a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ float d;

    aw(NavigationService navigationService, boolean z, boolean z2, float f) {
        this.a = navigationService;
        this.b = z;
        this.c = z2;
        this.d = f;
    }

    public final void run() {
        if (this.b) {
            this.a.B.setText(new StringBuilder(String.valueOf(this.a.k.z())).toString());
            this.a.C.setText("/" + this.a.k.A());
        }
        if (this.c) {
            this.a.x.setText(String.valueOf((int) (this.d * 100.0f)) + "%");
        }
    }
}
