package udk.android.reader.view.pdf;

import udk.android.reader.pdf.annotation.ab;
import udk.android.reader.pdf.annotation.d;
import udk.android.reader.pdf.annotation.e;
import udk.android.reader.pdf.annotation.s;

final class hk implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ ab b;

    hk(PDFView pDFView, ab abVar) {
        this.a = pDFView;
        this.b = abVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.annotation.s.a(udk.android.reader.pdf.annotation.Annotation, java.lang.String, boolean):void
     arg types: [udk.android.reader.pdf.annotation.Annotation, java.lang.String, int]
     candidates:
      udk.android.reader.pdf.annotation.s.a(udk.android.reader.pdf.annotation.ac, udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):void
      udk.android.reader.pdf.annotation.s.a(int, udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.Annotation, int):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.Annotation, java.lang.Runnable):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.Annotation, java.lang.String):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.e, float):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.p, java.lang.String):void
      udk.android.reader.pdf.annotation.s.a(android.content.Context, udk.android.reader.pdf.annotation.z, java.lang.Runnable):void
      udk.android.reader.pdf.annotation.s.a(udk.android.reader.pdf.annotation.w, java.lang.String, java.lang.String):void
      udk.android.reader.pdf.annotation.s.a(udk.android.reader.pdf.annotation.Annotation, java.lang.String, boolean):void */
    public final void run() {
        if (this.b.c instanceof d) {
            udk.android.reader.pdf.b.d.a().a((d) this.b.c);
        } else if (this.b.c instanceof e) {
            s.a().a(this.b.c, this.b.c.M(), true);
        }
    }
}
