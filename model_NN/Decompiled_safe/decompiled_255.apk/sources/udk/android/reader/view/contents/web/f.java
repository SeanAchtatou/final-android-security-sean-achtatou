package udk.android.reader.view.contents.web;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class f extends WebViewClient {
    private /* synthetic */ g a;

    f(g gVar) {
        this.a = gVar;
    }

    public final void onLoadResource(WebView webView, String str) {
    }

    public final void onPageFinished(WebView webView, String str) {
        g.c(this.a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.contents.web.g.a(udk.android.reader.view.contents.web.g, boolean):void
     arg types: [udk.android.reader.view.contents.web.g, int]
     candidates:
      udk.android.reader.view.contents.web.g.a(android.webkit.WebView, java.lang.String):boolean
      udk.android.reader.view.contents.web.g.a(udk.android.reader.view.contents.web.g, boolean):void */
    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.c();
        g.a(this.a, false);
        if (!str.startsWith("data:")) {
            this.a.b.setText(str);
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return this.a.a(webView, str);
    }
}
