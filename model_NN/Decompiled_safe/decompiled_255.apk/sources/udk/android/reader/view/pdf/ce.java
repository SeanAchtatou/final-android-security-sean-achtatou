package udk.android.reader.view.pdf;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import udk.android.b.c;
import udk.android.reader.b.a;
import udk.android.reader.pdf.PDF;

public final class ce {
    private static ce b;
    hq a;
    private boolean c;
    private Paint d = new Paint(1);
    private Paint e;
    private Paint f;
    private Paint g;
    private hx h;
    private ic i;
    private PDF j;
    private bn k;
    private n l;
    private cg m;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, int, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private ce() {
        this.d.setStyle(Paint.Style.FILL);
        this.d.setColor(c.a(215, 255, 255, 255));
        this.e = new Paint(1);
        this.e.setStyle(Paint.Style.STROKE);
        this.e.setStrokeWidth(1.0f);
        this.e.setColor(-13421773);
        this.f = new Paint(1);
        this.f.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, 20.0f, -1728053248, 0, Shader.TileMode.CLAMP));
        this.g = null;
        this.j = PDF.a();
        this.h = hx.a();
        this.i = ic.a();
        this.k = bn.a();
        this.l = n.b();
    }

    private static int a(float f2, int i2) {
        int i3 = (int) ((i2 == 1 ? f2 : 1.0f - f2) * 255.0f * 15.0f);
        if (i3 > 40) {
            return 40;
        }
        if (i3 < 0) {
            return 0;
        }
        return i3;
    }

    public static ce a() {
        if (b == null) {
            b = new ce();
        }
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.n.a(int, boolean):udk.android.reader.view.pdf.ez
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.n.a(int, float):boolean
      udk.android.reader.view.pdf.n.a(int, boolean):udk.android.reader.view.pdf.ez */
    private void a(Canvas canvas, as asVar, PointF pointF, int i2, float f2) {
        Path path;
        PointF pointF2;
        PointF pointF3;
        PointF pointF4;
        Path path2;
        Path path3;
        Path path4;
        PointF pointF5;
        PointF pointF6;
        Path path5;
        Path path6;
        if (!this.j.u()) {
            RectF b2 = this.h.b();
            PointF pointF7 = new PointF(b2.left, b2.top);
            PointF pointF8 = new PointF(b2.right, b2.top);
            PointF pointF9 = new PointF(b2.left, b2.bottom);
            PointF pointF10 = new PointF(b2.right, b2.bottom);
            PointF b3 = c.b(pointF, pointF10);
            double asin = c.a - (1.5707963267948966d - Math.asin((double) ((pointF10.y - b3.y) / c.a(pointF10, b3))));
            PointF a2 = c.a(pointF7, pointF9, b3, asin);
            PointF a3 = c.a(pointF8, pointF10, b3, asin);
            PointF a4 = c.a(pointF7, pointF8, b3, asin);
            PointF a5 = c.a(pointF9, pointF10, b3, asin);
            if (a2.y < b2.top || a2.y > b2.bottom) {
                if (a3.y >= b2.top && a3.y <= b2.bottom && a5.x >= b2.left && a5.x <= b2.right) {
                    Path path7 = new Path();
                    path7.moveTo(pointF.x, pointF.y);
                    path7.lineTo(a3.x, a3.y);
                    path7.lineTo(a5.x, a5.y);
                    path7.lineTo(pointF.x, pointF.y);
                    path6 = new Path();
                    path6.moveTo(pointF10.x, pointF10.y);
                    path6.lineTo(a3.x, a3.y);
                    path6.lineTo(a5.x, a5.y);
                    path6.lineTo(pointF10.x, pointF10.y);
                    if (i2 == 1) {
                        path5 = path7;
                        pointF5 = pointF;
                        pointF6 = a5;
                        path4 = path6;
                    } else if (i2 == 2) {
                        Path path8 = new Path();
                        path8.moveTo(pointF7.x, pointF7.y);
                        path8.lineTo(pointF8.x, pointF8.y);
                        path8.lineTo(a3.x, a3.y);
                        path8.lineTo(a5.x, a5.y);
                        path8.lineTo(pointF9.x, pointF9.y);
                        path8.lineTo(pointF7.x, pointF7.y);
                        path5 = path7;
                        pointF5 = pointF;
                        pointF6 = a5;
                        path4 = path8;
                    } else {
                        path5 = path7;
                        pointF5 = pointF;
                        pointF6 = a5;
                        path4 = null;
                    }
                } else if (a3.y < b2.top && a5.x >= b2.left && a5.x <= b2.right) {
                    PointF a6 = c.a(pointF, a3, a4);
                    Path path9 = new Path();
                    path9.moveTo(pointF.x, pointF.y);
                    path9.lineTo(a5.x, a5.y);
                    path9.lineTo(a4.x, a4.y);
                    path9.lineTo(a6.x, a6.y);
                    path9.lineTo(pointF.x, pointF.y);
                    path4 = new Path();
                    path4.moveTo(pointF10.x, pointF10.y);
                    path4.lineTo(pointF8.x, pointF8.y);
                    path4.lineTo(a4.x, a4.y);
                    path4.lineTo(a5.x, a5.y);
                    path4.lineTo(pointF10.x, pointF10.y);
                    if (i2 == 1) {
                        path5 = path9;
                        pointF5 = pointF;
                        a3 = a4;
                        path6 = path4;
                        pointF6 = a5;
                    } else if (i2 == 2) {
                        Path path10 = new Path();
                        path10.moveTo(pointF7.x, pointF7.y);
                        path10.lineTo(a4.x, a4.y);
                        path10.lineTo(a5.x, a5.y);
                        path10.lineTo(pointF9.x, pointF9.y);
                        path10.lineTo(pointF7.x, pointF7.y);
                        path5 = path9;
                        pointF5 = pointF;
                        a3 = a4;
                        path6 = path4;
                        pointF6 = a5;
                        path4 = path10;
                    } else {
                        path5 = path9;
                        pointF5 = pointF;
                        a3 = a4;
                        path6 = path4;
                        pointF6 = a5;
                        path4 = null;
                    }
                } else if (a3.y <= b2.bottom || a5.x < b2.left || a5.x > b2.right) {
                    path4 = null;
                    pointF5 = null;
                    a3 = null;
                    pointF6 = null;
                    path5 = null;
                    path6 = null;
                } else {
                    PointF a7 = c.a(pointF, a3, a4);
                    Path path11 = new Path();
                    path11.moveTo(pointF.x, pointF.y);
                    path11.lineTo(a5.x, a5.y);
                    path11.lineTo(a4.x, a4.y);
                    path11.lineTo(a7.x, a7.y);
                    path11.lineTo(pointF.x, pointF.y);
                    path4 = new Path();
                    path4.moveTo(pointF10.x, pointF10.y);
                    path4.lineTo(pointF8.x, pointF8.y);
                    path4.lineTo(a4.x, a4.y);
                    path4.lineTo(a5.x, a5.y);
                    path4.lineTo(pointF10.x, pointF10.y);
                    if (i2 == 1) {
                        path5 = path11;
                        pointF5 = pointF;
                        a3 = a4;
                        path6 = path4;
                        pointF6 = a5;
                    } else if (i2 == 2) {
                        Path path12 = new Path();
                        path12.moveTo(pointF7.x, pointF7.y);
                        path12.lineTo(a4.x, a4.y);
                        path12.lineTo(a5.x, a5.y);
                        path12.lineTo(pointF9.x, pointF9.y);
                        path12.lineTo(pointF7.x, pointF7.y);
                        path5 = path11;
                        pointF5 = pointF;
                        a3 = a4;
                        path6 = path4;
                        pointF6 = a5;
                        path4 = path12;
                    } else {
                        path5 = path11;
                        pointF5 = pointF;
                        a3 = a4;
                        path6 = path4;
                        pointF6 = a5;
                        path4 = null;
                    }
                }
            } else if (pointF.y < b2.bottom) {
                PointF a8 = c.a(pointF9, a3, pointF10);
                PointF pointF11 = new PointF(a8.x - (pointF10.x - a8.x), a8.y - (pointF10.y - a8.y));
                Path path13 = new Path();
                path13.moveTo(pointF11.x, pointF11.y);
                path13.lineTo(a3.x, a3.y);
                path13.lineTo(pointF9.x, pointF9.y);
                path13.lineTo(pointF11.x, pointF11.y);
                path6 = new Path();
                path6.moveTo(pointF10.x, pointF10.y);
                path6.lineTo(a3.x, a3.y);
                path6.lineTo(pointF9.x, pointF9.y);
                path6.lineTo(pointF10.x, pointF10.y);
                if (i2 == 1) {
                    path5 = path13;
                    pointF5 = pointF11;
                    pointF6 = pointF9;
                    path4 = path6;
                } else if (i2 == 2) {
                    Path path14 = new Path();
                    path14.moveTo(pointF7.x, pointF7.y);
                    path14.lineTo(pointF8.x, pointF8.y);
                    path14.lineTo(a3.x, a3.y);
                    path14.lineTo(pointF9.x, pointF9.y);
                    path14.lineTo(pointF7.x, pointF7.y);
                    path5 = path13;
                    pointF5 = pointF11;
                    pointF6 = pointF9;
                    path4 = path14;
                } else {
                    path5 = path13;
                    pointF5 = pointF11;
                    pointF6 = pointF9;
                    path4 = null;
                }
            } else {
                PointF a9 = c.a(pointF7, a3, pointF8);
                PointF pointF12 = new PointF(a9.x - (pointF8.x - a9.x), (a9.y - pointF8.y) + a9.y);
                PointF a10 = c.a(pointF7, a3, pointF9, pointF10);
                PointF a11 = c.a(pointF12, a3, a10);
                path5 = new Path();
                path5.moveTo(a11.x, a11.y);
                path5.lineTo(a10.x, a10.y);
                path5.lineTo(pointF7.x, pointF7.y);
                path5.lineTo(pointF12.x, pointF12.y);
                path5.lineTo(a11.x, a11.y);
                Path path15 = new Path();
                path15.moveTo(pointF10.x, pointF10.y);
                path15.lineTo(pointF8.x, pointF8.y);
                path15.lineTo(pointF7.x, pointF7.y);
                path15.lineTo(a10.x, a10.y);
                path15.lineTo(pointF10.x, pointF10.y);
                if (i2 == 1) {
                    pointF5 = a11;
                    a3 = pointF7;
                    pointF6 = a10;
                    path4 = path15;
                    path6 = path15;
                } else if (i2 == 2) {
                    Path path16 = new Path();
                    path16.moveTo(pointF7.x, pointF7.y);
                    path16.lineTo(pointF9.x, pointF9.y);
                    path16.lineTo(a10.x, a10.y);
                    path16.lineTo(pointF7.x, pointF7.y);
                    pointF5 = a11;
                    a3 = pointF7;
                    pointF6 = a10;
                    path4 = path16;
                    path6 = path15;
                } else {
                    pointF5 = a11;
                    a3 = pointF7;
                    pointF6 = a10;
                    path4 = null;
                    path6 = path15;
                }
            }
            if (path5 != null && path4 != null && path6 != null) {
                canvas.save();
                canvas.clipPath(path4);
                canvas.translate(this.h.a, this.h.b);
                Bitmap bitmap = null;
                int E = i2 == 1 ? this.j.E() + 1 : i2 == 2 ? this.j.E() - 1 : 0;
                ez a12 = this.l.a(E, false);
                if (a12 != null) {
                    bitmap = a12.c();
                }
                if (bitmap != null) {
                    canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0.0f, 0.0f, (float) this.h.e(), (float) this.h.i()), (Paint) null);
                } else {
                    canvas.drawARGB(255, 255, 255, 255);
                }
                asVar.a(canvas, E, this.h.n(), 0.0f, 0.0f);
                canvas.restore();
                if (i2 == 2 && this.j.E() > 2 && this.h.e() < this.i.a - 1) {
                    canvas.save();
                    canvas.rotate(-90.0f, this.h.a, this.h.b);
                    canvas.translate(this.h.a, this.h.b);
                    canvas.drawRect(new RectF((float) (0 - this.h.i()), 0.0f, 0.0f, 40.0f), this.f);
                    canvas.restore();
                }
                int E2 = i2 == 1 ? this.j.E() : i2 == 2 ? this.j.E() - 1 : 0;
                ez a13 = this.l.a(E2, false);
                Bitmap c2 = a13 != null ? a13.c() : null;
                canvas.save();
                canvas.clipPath(path5);
                float a14 = (float) c.a(Math.atan2((double) (pointF6.y - pointF5.y), (double) (pointF6.x - pointF5.x)));
                canvas.translate(pointF5.x, pointF5.y);
                canvas.rotate(a14);
                canvas.translate((float) this.h.e(), (float) (-this.h.i()));
                canvas.scale(-1.0f, 1.0f);
                if (c2 != null) {
                    canvas.drawBitmap(c2, new Rect(0, 0, c2.getWidth(), c2.getHeight()), new RectF(0.0f, 0.0f, (float) this.h.e(), (float) this.h.i()), this.g);
                }
                asVar.a(canvas, E2, this.h.n(), 0.0f, 0.0f);
                canvas.restore();
                if (a.ao) {
                    this.d.setAlpha(255 - a(f2, i2));
                }
                canvas.drawPath(path5, this.d);
                canvas.save();
                canvas.clipPath(path6);
                float f3 = a3.x - pointF6.x;
                float a15 = c.a(pointF6, a3);
                float a16 = (float) c.a(Math.atan2((double) (a3.y - pointF6.y), (double) f3));
                canvas.rotate(a16, pointF6.x, pointF6.y);
                canvas.translate(pointF6.x, pointF6.y);
                canvas.drawRect(-20.0f, 0.0f, a15 + 20.0f, 40.0f, this.f);
                canvas.restore();
                canvas.save();
                canvas.clipPath(path5);
                canvas.rotate(a16, pointF6.x, pointF6.y);
                canvas.translate(pointF6.x, pointF6.y);
                canvas.scale(1.0f, -1.0f);
                canvas.drawRect(-20.0f, 0.0f, a15 + 20.0f, 40.0f, this.f);
                canvas.restore();
                canvas.drawPath(path5, this.e);
                return;
            }
            return;
        }
        RectF b4 = this.h.b();
        PointF pointF13 = new PointF(b4.left, b4.top);
        PointF pointF14 = new PointF(b4.right, b4.top);
        PointF pointF15 = new PointF(b4.left, b4.bottom);
        PointF pointF16 = new PointF(b4.right, b4.bottom);
        PointF b5 = c.b(pointF, pointF15);
        double asin2 = 1.5707963267948966d - Math.asin((double) ((pointF15.y - b5.y) / c.a(pointF15, b5)));
        PointF a17 = c.a(pointF13, pointF15, b5, asin2);
        PointF a18 = c.a(pointF14, pointF16, b5, asin2);
        PointF a19 = c.a(pointF13, pointF14, b5, asin2);
        PointF a20 = c.a(pointF15, pointF16, b5, asin2);
        if (a18.y < b4.top || a18.y > b4.bottom) {
            if (a17.y >= b4.top && a17.y <= b4.bottom && a20.x >= b4.left && a20.x <= b4.right) {
                Path path17 = new Path();
                path17.moveTo(pointF.x, pointF.y);
                path17.lineTo(a17.x, a17.y);
                path17.lineTo(a20.x, a20.y);
                path17.lineTo(pointF.x, pointF.y);
                path3 = new Path();
                path3.moveTo(pointF15.x, pointF15.y);
                path3.lineTo(a17.x, a17.y);
                path3.lineTo(a20.x, a20.y);
                path3.lineTo(pointF15.x, pointF15.y);
                if (i2 == 1) {
                    path2 = path17;
                    pointF3 = a17;
                    pointF4 = a20;
                    path = path3;
                    pointF2 = pointF;
                } else if (i2 == 2) {
                    Path path18 = new Path();
                    path18.moveTo(pointF13.x, pointF13.y);
                    path18.lineTo(pointF14.x, pointF14.y);
                    path18.lineTo(pointF16.x, pointF16.y);
                    path18.lineTo(a20.x, a20.y);
                    path18.lineTo(a17.x, a17.y);
                    path18.lineTo(pointF13.x, pointF13.y);
                    path2 = path17;
                    pointF3 = a17;
                    pointF4 = a20;
                    path = path18;
                    pointF2 = pointF;
                } else {
                    path2 = path17;
                    pointF3 = a17;
                    pointF4 = a20;
                    path = null;
                    pointF2 = pointF;
                }
            } else if (a17.y < b4.top && a20.x >= b4.left && a20.x <= b4.right) {
                PointF a21 = c.a(pointF, a17, a19);
                Path path19 = new Path();
                path19.moveTo(pointF.x, pointF.y);
                path19.lineTo(a20.x, a20.y);
                path19.lineTo(a19.x, a19.y);
                path19.lineTo(a21.x, a21.y);
                path19.lineTo(pointF.x, pointF.y);
                path = new Path();
                path.moveTo(pointF15.x, pointF15.y);
                path.lineTo(pointF13.x, pointF13.y);
                path.lineTo(a19.x, a19.y);
                path.lineTo(a20.x, a20.y);
                path.lineTo(pointF15.x, pointF15.y);
                if (i2 == 1) {
                    path2 = path19;
                    pointF2 = pointF;
                    pointF3 = a19;
                    path3 = path;
                    pointF4 = a20;
                } else if (i2 == 2) {
                    Path path20 = new Path();
                    path20.moveTo(pointF14.x, pointF14.y);
                    path20.lineTo(a19.x, a19.y);
                    path20.lineTo(a20.x, a20.y);
                    path20.lineTo(pointF16.x, pointF16.y);
                    path20.lineTo(pointF14.x, pointF14.y);
                    path2 = path19;
                    pointF2 = pointF;
                    pointF3 = a19;
                    path3 = path;
                    pointF4 = a20;
                    path = path20;
                } else {
                    path2 = path19;
                    pointF2 = pointF;
                    pointF3 = a19;
                    path3 = path;
                    pointF4 = a20;
                    path = null;
                }
            } else if (a17.y <= b4.bottom || a20.x < b4.left || a20.x > b4.right) {
                path = null;
                pointF2 = null;
                pointF3 = null;
                pointF4 = null;
                path2 = null;
                path3 = null;
            } else {
                PointF a22 = c.a(pointF, a17, a19);
                Path path21 = new Path();
                path21.moveTo(pointF.x, pointF.y);
                path21.lineTo(a20.x, a20.y);
                path21.lineTo(a19.x, a19.y);
                path21.lineTo(a22.x, a22.y);
                path21.lineTo(pointF.x, pointF.y);
                path = new Path();
                path.moveTo(pointF15.x, pointF15.y);
                path.lineTo(pointF13.x, pointF13.y);
                path.lineTo(a19.x, a19.y);
                path.lineTo(a20.x, a20.y);
                path.lineTo(pointF15.x, pointF15.y);
                if (i2 == 1) {
                    path2 = path21;
                    pointF2 = pointF;
                    pointF3 = a19;
                    path3 = path;
                    pointF4 = a20;
                } else if (i2 == 2) {
                    Path path22 = new Path();
                    path22.moveTo(pointF14.x, pointF14.y);
                    path22.lineTo(a19.x, a19.y);
                    path22.lineTo(a20.x, a20.y);
                    path22.lineTo(pointF16.x, pointF16.y);
                    path22.lineTo(pointF14.x, pointF14.y);
                    path2 = path21;
                    pointF2 = pointF;
                    pointF3 = a19;
                    path3 = path;
                    pointF4 = a20;
                    path = path22;
                } else {
                    path2 = path21;
                    pointF2 = pointF;
                    pointF3 = a19;
                    path3 = path;
                    pointF4 = a20;
                    path = null;
                }
            }
        } else if (pointF.y < b4.bottom) {
            PointF a23 = c.a(pointF16, a17, pointF15);
            PointF pointF17 = new PointF(a23.x + (a23.x - pointF15.x), a23.y - (pointF15.y - a23.y));
            Path path23 = new Path();
            path23.moveTo(pointF17.x, pointF17.y);
            path23.lineTo(a17.x, a17.y);
            path23.lineTo(pointF16.x, pointF16.y);
            path23.lineTo(pointF17.x, pointF17.y);
            path3 = new Path();
            path3.moveTo(pointF15.x, pointF15.y);
            path3.lineTo(a17.x, a17.y);
            path3.lineTo(pointF16.x, pointF16.y);
            path3.lineTo(pointF15.x, pointF15.y);
            if (i2 == 1) {
                path2 = path23;
                pointF3 = a17;
                pointF4 = pointF16;
                path = path3;
                pointF2 = pointF17;
            } else if (i2 == 2) {
                Path path24 = new Path();
                path24.moveTo(pointF13.x, pointF13.y);
                path24.lineTo(pointF14.x, pointF14.y);
                path24.lineTo(pointF16.x, pointF16.y);
                path24.lineTo(a17.x, a17.y);
                path24.lineTo(pointF13.x, pointF13.y);
                path2 = path23;
                pointF3 = a17;
                pointF4 = pointF16;
                path = path24;
                pointF2 = pointF17;
            } else {
                path2 = path23;
                pointF3 = a17;
                pointF4 = pointF16;
                path = null;
                pointF2 = pointF17;
            }
        } else {
            PointF a24 = c.a(pointF14, a17, pointF13);
            PointF pointF18 = new PointF(a24.x + (a24.x - pointF13.x), (a24.y - pointF14.y) + a24.y);
            PointF a25 = c.a(pointF14, a17, pointF15, pointF16);
            PointF a26 = c.a(pointF18, a17, a25);
            path2 = new Path();
            path2.moveTo(a26.x, a26.y);
            path2.lineTo(a25.x, a25.y);
            path2.lineTo(pointF14.x, pointF14.y);
            path2.lineTo(pointF18.x, pointF18.y);
            path2.lineTo(a26.x, a26.y);
            Path path25 = new Path();
            path25.moveTo(pointF15.x, pointF15.y);
            path25.lineTo(pointF13.x, pointF13.y);
            path25.lineTo(pointF14.x, pointF14.y);
            path25.lineTo(a25.x, a25.y);
            path25.lineTo(pointF15.x, pointF15.y);
            if (i2 == 1) {
                pointF2 = a26;
                pointF3 = pointF14;
                pointF4 = a25;
                path = path25;
                path3 = path25;
            } else if (i2 == 2) {
                Path path26 = new Path();
                path26.moveTo(pointF14.x, pointF14.y);
                path26.lineTo(pointF16.x, pointF16.y);
                path26.lineTo(a25.x, a25.y);
                path26.lineTo(pointF14.x, pointF14.y);
                pointF2 = a26;
                pointF3 = pointF14;
                pointF4 = a25;
                path = path26;
                path3 = path25;
            } else {
                pointF2 = a26;
                pointF3 = pointF14;
                pointF4 = a25;
                path = null;
                path3 = path25;
            }
        }
        if (path2 != null && path != null && path3 != null) {
            canvas.save();
            canvas.clipPath(path);
            canvas.translate(this.h.a, this.h.b);
            Bitmap bitmap2 = null;
            int E3 = i2 == 1 ? this.j.E() + 1 : i2 == 2 ? this.j.E() - 1 : 0;
            ez a27 = this.l.a(E3, false);
            if (a27 != null) {
                bitmap2 = a27.c();
            }
            if (bitmap2 != null) {
                canvas.drawBitmap(bitmap2, new Rect(0, 0, bitmap2.getWidth(), bitmap2.getHeight()), new RectF(0.0f, 0.0f, (float) this.h.e(), (float) this.h.i()), (Paint) null);
            } else {
                canvas.drawARGB(255, 255, 255, 255);
            }
            asVar.a(canvas, E3, this.h.n(), 0.0f, 0.0f);
            canvas.restore();
            if (i2 == 2 && this.j.E() > 2 && this.h.e() < this.i.a - 1) {
                canvas.save();
                canvas.rotate(-90.0f, this.h.c(), this.h.b);
                canvas.translate(this.h.c(), this.h.b);
                canvas.scale(1.0f, -1.0f);
                canvas.drawRect(new RectF((float) (0 - this.h.i()), 0.0f, 0.0f, 40.0f), this.f);
                canvas.restore();
            }
            int E4 = i2 == 1 ? this.j.E() : i2 == 2 ? this.j.E() - 1 : 0;
            ez a28 = this.l.a(E4, false);
            Bitmap c3 = a28 != null ? a28.c() : null;
            canvas.save();
            canvas.clipPath(path2);
            float a29 = (float) c.a(Math.atan2((double) (pointF4.y - pointF2.y), (double) (pointF4.x - pointF2.x)));
            canvas.translate(pointF2.x, pointF2.y);
            canvas.rotate(a29);
            canvas.translate(0.0f, (float) this.h.i());
            canvas.scale(1.0f, -1.0f);
            if (c3 != null) {
                canvas.drawBitmap(c3, new Rect(0, 0, c3.getWidth(), c3.getHeight()), new RectF(0.0f, 0.0f, (float) this.h.e(), (float) this.h.i()), this.g);
            }
            asVar.a(canvas, E4, this.h.n(), 0.0f, 0.0f);
            canvas.restore();
            if (a.ao) {
                this.d.setAlpha(255 - a(f2, i2));
            }
            canvas.drawPath(path2, this.d);
            canvas.save();
            canvas.clipPath(path3);
            float f4 = pointF4.x - pointF3.x;
            float a30 = c.a(pointF4, pointF3);
            float a31 = (float) c.a(Math.atan2((double) (pointF4.y - pointF3.y), (double) f4));
            canvas.rotate(a31, pointF3.x, pointF3.y);
            canvas.translate(pointF3.x, pointF3.y);
            canvas.drawRect(-20.0f, 0.0f, a30 + 20.0f, 40.0f, this.f);
            canvas.restore();
            canvas.save();
            canvas.clipPath(path2);
            canvas.rotate(a31, pointF3.x, pointF3.y);
            canvas.translate(pointF3.x, pointF3.y);
            canvas.scale(1.0f, -1.0f);
            canvas.drawRect(-20.0f, 0.0f, a30 + 20.0f, 40.0f, this.f);
            canvas.restore();
            canvas.drawPath(path2, this.e);
        }
    }

    static /* synthetic */ void a(ce ceVar) {
        if (ceVar.j.E() < ceVar.j.l()) {
            PointF pointF = new PointF(!ceVar.j.u() ? ceVar.h.c() - 3.0f : ceVar.h.a + 3.0f, (ceVar.h.b + ((float) ceVar.h.i())) - 3.0f);
            ceVar.m = new cg();
            ceVar.m.b = 1;
            ceVar.m.c = 1;
            ceVar.m.d = pointF;
            cl.a(ceVar);
            ceVar.f();
            ceVar.l.b(ceVar.j.E() + 1);
            ceVar.g();
            ceVar.m.a = true;
            ceVar.a(true);
        }
    }

    static /* synthetic */ void b(ce ceVar) {
        if (ceVar.j.w()) {
            PointF pointF = new PointF(!ceVar.j.u() ? (ceVar.h.a - ((float) ceVar.h.e())) + 3.0f : (ceVar.h.c() + ((float) ceVar.h.e())) - 3.0f, (ceVar.h.b + ((float) ceVar.h.i())) - 3.0f);
            ceVar.m = new cg();
            ceVar.m.b = 2;
            ceVar.m.c = 1;
            ceVar.m.d = pointF;
            cl.a(ceVar);
            ceVar.f();
            ceVar.l.b(ceVar.j.E() - 1);
            ceVar.g();
            ceVar.a(true);
        }
    }

    private void f() {
        if (this.a != null) {
            this.a.b();
        }
    }

    private void g() {
        if (this.a != null) {
            this.a.c();
        }
    }

    public final void a(Canvas canvas, as asVar) {
        float f2;
        float f3;
        float d2;
        if (this.m != null) {
            float i2 = this.h.b + (((float) this.h.i()) * 0.8f);
            float e2 = ((float) this.h.e()) * 0.05f * a.aq;
            float d3 = (this.h.d() - i2) * 0.05f * a.aq * -1.0f;
            if (!this.j.u()) {
                float f4 = this.h.a;
                if (this.m.b == 1) {
                    e2 *= -1.0f;
                    f2 = (this.h.a - ((float) this.h.e())) - 1.0f;
                    f3 = f4;
                    d2 = this.h.d() + 1.0f;
                } else {
                    f2 = this.h.c() - 1.0f;
                    f3 = f4;
                    d2 = this.h.d() - 1.0f;
                }
            } else {
                float c2 = this.h.c();
                if (this.m.b == 1) {
                    f2 = this.h.c() + ((float) this.h.e()) + 1.0f;
                    f3 = c2;
                    d2 = this.h.d() + 1.0f;
                } else {
                    e2 *= -1.0f;
                    f2 = this.h.a + 1.0f;
                    f3 = c2;
                    d2 = this.h.d() - 1.0f;
                }
            }
            float f5 = f2 - this.m.d.x;
            float f6 = d2 - this.m.d.y;
            if (this.m.c != 1 || (Math.abs(this.m.d.x - f3) >= 1.0f && this.m.d.y <= i2)) {
                this.m.c = 2;
                this.m.d.x += f5 * 0.3f;
                this.m.d.y += f6 * 0.3f;
            } else {
                PointF pointF = this.m.d;
                pointF.x = e2 + pointF.x;
                this.m.d.y += d3;
            }
            a(canvas, asVar, this.m.d, this.m.b, Math.abs(f5) / ((float) (this.h.e() * 2)));
            if (Math.abs(f5) < 3.0f && Math.abs(f6) < 3.0f) {
                cg cgVar = this.m;
                if (this.a != null) {
                    this.a.a(cgVar);
                }
                cl.b(this);
                a(false);
                this.m = null;
                return;
            }
            return;
        }
        PointF pointF2 = new PointF(this.k.c, this.k.d);
        if (pointF2.x >= this.h.c()) {
            pointF2.x = this.h.c() - 1.0f;
        }
        a(canvas, asVar, pointF2, 1, (!this.j.u() ? pointF2.x - (this.h.a - ((float) this.h.e())) : (this.h.c() + ((float) this.h.e())) - pointF2.x) / ((float) (this.h.e() * 2)));
    }

    public final void a(PointF pointF) {
        if (this.j.x()) {
            this.m = new cg();
            this.m.b = 1;
            this.m.c = 2;
            this.m.d = pointF;
            a(true);
            cl.a(this);
        }
    }

    public final void a(boolean z) {
        boolean z2 = false;
        if (!this.c && z) {
            z2 = true;
        }
        this.c = z;
        if (z2 && this.a != null) {
            this.a.e();
        }
    }

    public final boolean b() {
        return this.c;
    }

    public final Paint c() {
        return this.f;
    }

    public final Paint d() {
        return this.e;
    }

    public final cg e() {
        return this.m;
    }
}
