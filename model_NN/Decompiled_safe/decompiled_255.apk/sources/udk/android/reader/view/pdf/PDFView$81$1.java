package udk.android.reader.view.pdf;

import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.e;

class PDFView$81$1 extends ResultReceiver {
    private /* synthetic */ hc a;
    private final /* synthetic */ e b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    PDFView$81$1(hc hcVar, Handler handler, e eVar) {
        super(handler);
        this.a = hcVar;
        this.b = eVar;
    }

    /* access modifiers changed from: protected */
    public void onReceiveResult(int i, Bundle bundle) {
        switch (i) {
            case 0:
            case 2:
                float a2 = a.a(5.0f);
                ar.a().a(this.b.b(this.a.a.B()), new RectF(a2, a2, ((float) this.a.a.l.a) - a2, ((float) (this.a.a.l.b / 2)) - a2));
                break;
        }
        this.a.a.R();
    }
}
