package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class az implements Runnable {
    private /* synthetic */ NavigationService a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ EditText c;

    az(NavigationService navigationService, Context context, EditText editText) {
        this.a = navigationService;
        this.b = context;
        this.c = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.getSystemService("input_method")).showSoftInput(this.c, 1);
    }
}
