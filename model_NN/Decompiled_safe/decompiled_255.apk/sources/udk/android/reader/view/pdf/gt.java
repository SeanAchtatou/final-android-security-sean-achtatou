package udk.android.reader.view.pdf;

import android.media.MediaPlayer;
import android.widget.FrameLayout;

final class gt implements MediaPlayer.OnCompletionListener {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ FrameLayout b;

    gt(PDFView pDFView, FrameLayout frameLayout) {
        this.a = pDFView;
        this.b = frameLayout;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        this.a.a(this.b);
    }
}
