package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.a;
import udk.android.reader.pdf.ac;
import udk.android.reader.pdf.af;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ar;

public class PageNavigationView extends Gallery implements af, ar {
    /* access modifiers changed from: private */
    public NavigationService a;
    /* access modifiers changed from: private */
    public b b;
    /* access modifiers changed from: private */
    public PDF c;
    /* access modifiers changed from: private */
    public int d;

    public PageNavigationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
    }

    public PageNavigationView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        e();
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        post(new t(this, i));
    }

    private void e() {
        setCallbackDuringFling(false);
        this.a = NavigationService.a();
        this.b = new b(getContext());
        setAdapter((SpinnerAdapter) this.b);
        setOnItemClickListener(new l(this));
        setOnItemSelectedListener(new k(this));
        this.c = PDF.a();
        this.c.a(this);
        a.a().a(this);
    }

    public final void a() {
        if (this.a.n() == NavigationService.b) {
            float a2 = this.c.a(getContext().getResources().getDimension(C0000R.dimen.navigation_thumbnail_width) - (getContext().getResources().getDimension(C0000R.dimen.navigation_thumbnail_padding) * 2.0f));
            String z = this.c.z();
            try {
                int childCount = getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = getChildAt(i);
                    int positionForView = getPositionForView(childAt);
                    int intValue = ((Integer) getItemAtPosition(positionForView)).intValue();
                    ImageView imageView = (ImageView) childAt.findViewById(C0000R.id.thumbnail);
                    if (imageView.getDrawable() == null) {
                        new u(this, z, positionForView, imageView, intValue, a2).start();
                    }
                }
            } catch (Exception e) {
                c.a(e.getMessage(), e);
            }
        }
    }

    public final void a(ac acVar) {
    }

    public final void a(ah ahVar) {
    }

    public final void b() {
    }

    public final void b(ah ahVar) {
        if (ahVar.c) {
            a(ahVar.a);
        }
    }

    public final void c() {
    }

    public final void d() {
        post(new p(this));
    }

    public final void f() {
    }

    public final void g() {
        post(new r(this));
    }

    public final void h() {
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        a.a().b(this);
        this.c.b(this);
        super.onDetachedFromWindow();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return false;
    }
}
