package udk.android.reader.view.pdf;

final class gj extends Thread {
    private /* synthetic */ o a;

    gj(o oVar) {
        this.a = oVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, boolean):void
     arg types: [udk.android.reader.view.pdf.o, int]
     candidates:
      udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, int):boolean
      udk.android.reader.view.pdf.o.a(float, float):udk.android.reader.pdf.x
      udk.android.reader.view.pdf.o.a(int, int):void
      udk.android.reader.view.pdf.o.a(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):void
      udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, int, boolean):void
     arg types: [udk.android.reader.view.pdf.o, int, int]
     candidates:
      udk.android.reader.view.pdf.o.a(int, float, android.graphics.RectF):android.graphics.RectF
      udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, int, int):void
      udk.android.reader.view.pdf.o.a(float, float, java.lang.Runnable):void
      udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, int, boolean):void */
    public final void run() {
        int E = this.a.b.E();
        while (E <= this.a.b.l()) {
            o.a(this.a, E, true);
            if (this.a.i != null) {
                break;
            }
            E++;
        }
        this.a.w();
        if (!this.a.a(E)) {
            this.a.a(false);
        } else if (!this.a.d()) {
            this.a.a(true);
        }
    }
}
