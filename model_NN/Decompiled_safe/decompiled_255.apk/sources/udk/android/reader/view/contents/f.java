package udk.android.reader.view.contents;

import android.view.View;
import android.view.ViewGroup;

final class f implements View.OnClickListener {
    private /* synthetic */ d a;
    private final /* synthetic */ ViewGroup b;
    private final /* synthetic */ int c;

    f(d dVar, ViewGroup viewGroup, int i) {
        this.a = dVar;
        this.b = viewGroup;
        this.c = i;
    }

    public final void onClick(View view) {
        AllPDFListView allPDFListView = (AllPDFListView) this.b;
        if (allPDFListView.isGroupExpanded(this.c)) {
            allPDFListView.collapseGroup(this.c);
        } else {
            allPDFListView.expandGroup(this.c);
        }
    }
}
