package udk.android.reader.view.pdf;

import android.view.View;

final class fj implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ int b;
    private final /* synthetic */ boolean c;

    fj(PDFView pDFView, int i, boolean z) {
        this.a = pDFView;
        this.b = i;
        this.c = z;
    }

    public final void run() {
        for (View findViewById : this.a.q) {
            findViewById.findViewById(this.b).setVisibility(this.c ? 0 : 8);
        }
    }
}
