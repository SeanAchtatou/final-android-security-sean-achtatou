package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.unidocs.commonlib.util.b;
import udk.android.c.a;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ar;

public final class in implements ar, io {
    /* access modifiers changed from: private */
    public PDFView a;
    private a b = new a();
    /* access modifiers changed from: private */
    public o c;
    private String d;
    private String e;
    /* access modifiers changed from: private */
    public boolean f;
    private String[] g;

    public in(PDFView pDFView) {
        this.a = pDFView;
    }

    private String a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.g != null) {
            for (int i = 0; i < this.g.length; i++) {
                if (b.a(this.g[i])) {
                    stringBuffer.append(this.g[i]);
                    stringBuffer.append(str);
                }
            }
        }
        return stringBuffer.toString().replaceAll(String.valueOf(str) + "$", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.String):boolean
      com.unidocs.commonlib.util.h.a(java.lang.String, java.lang.CharSequence):java.util.regex.Matcher */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0052, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0055, code lost:
        com.unidocs.commonlib.util.h.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005d, code lost:
        r4 = r2;
        r2 = null;
        r1 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0027  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0052 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r5, java.lang.String r6) {
        /*
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r1 = 0
            boolean r2 = com.unidocs.commonlib.util.b.a(r5)     // Catch:{ Exception -> 0x005c, all -> 0x0052 }
            if (r2 == 0) goto L_0x0018
            java.lang.String r2 = "(?s)<Paragraph>(.*?)</Paragraph>"
            java.util.regex.Matcher r1 = com.unidocs.commonlib.util.h.a(r2, r5)     // Catch:{ Exception -> 0x005c, all -> 0x0052 }
        L_0x0012:
            boolean r2 = r1.find()     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            if (r2 != 0) goto L_0x0029
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            com.unidocs.commonlib.util.h.a(r1)
        L_0x001d:
            java.lang.String r0 = r0.toString()
            boolean r1 = com.unidocs.commonlib.util.b.b(r0)
            if (r1 == 0) goto L_0x0028
            r0 = r6
        L_0x0028:
            return r0
        L_0x0029:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            r3 = 1
            java.lang.String r3 = r1.group(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            java.lang.String r3 = "\n\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            r0.append(r2)     // Catch:{ Exception -> 0x0045, all -> 0x0052 }
            goto L_0x0012
        L_0x0045:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
        L_0x0049:
            udk.android.reader.b.c.a(r1)     // Catch:{ all -> 0x0059 }
            if (r2 == 0) goto L_0x001d
            com.unidocs.commonlib.util.h.a(r2)
            goto L_0x001d
        L_0x0052:
            r0 = move-exception
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            com.unidocs.commonlib.util.h.a(r1)
        L_0x0058:
            throw r0
        L_0x0059:
            r0 = move-exception
            r1 = r2
            goto L_0x0053
        L_0x005c:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r4
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.pdf.in.a(java.lang.String, java.lang.String):java.lang.String");
    }

    static /* synthetic */ void a(in inVar, String[] strArr) {
        try {
            inVar.g = strArr;
            PDF.a().v().b("ttsexceptions", inVar.a("#DM@"));
        } catch (Exception e2) {
            c.a((Throwable) e2);
        }
    }

    static /* synthetic */ void b(in inVar) {
        try {
            inVar.g = null;
            String b2 = PDF.a().v().b("ttsexceptions");
            if (b.a(b2)) {
                inVar.g = b2.split("#DM@");
            }
        } catch (Exception e2) {
            c.a((Throwable) e2);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        String a2;
        String str;
        Exception e2;
        if (this.c.d()) {
            a2 = a(this.c.j(), this.e);
        } else {
            a2 = a(udk.android.reader.b.a.p ? PDF.a().c(this.a.z()) : null, this.d);
        }
        try {
            if (this.g != null) {
                String[] strArr = this.g;
                int length = strArr.length;
                String str2 = a2;
                int i = 0;
                while (i < length) {
                    try {
                        String str3 = strArr[i];
                        if (str3 != null) {
                            str2 = str2.replace(str3, "");
                        }
                        i++;
                    } catch (Exception e3) {
                        e2 = e3;
                        str = str2;
                        c.a((Throwable) e2);
                        a2 = str;
                        this.b.a(this.a.getContext(), a2, new l(this));
                    }
                }
                a2 = str2;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            str = a2;
            e2 = exc;
            c.a((Throwable) e2);
            a2 = str;
            this.b.a(this.a.getContext(), a2, new l(this));
        }
        this.b.a(this.a.getContext(), a2, new l(this));
    }

    public final void Z() {
    }

    public final void a() {
        PDF.a().b(this);
        if (this.c != null) {
            this.c.b(this);
            this.c = null;
        }
        this.b.a(this.a.getContext());
        this.g = null;
        this.f = false;
    }

    public final void a(Runnable runnable, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        if (this.c == null) {
            this.c = this.a.V();
            this.c.a(this);
        }
        PDF.a().a(this);
        this.d = str8;
        this.e = str9;
        this.b.a(this.a.getContext(), new j(this, runnable), str, str2, str3, str4, str5, str6, str7);
    }

    public final void a(ah ahVar) {
    }

    public final void aa() {
    }

    public final void ab() {
    }

    public final void ac() {
        if (this.f) {
            l();
        }
    }

    public final void ad() {
        if (this.f) {
            l();
        }
    }

    public final void ae() {
        if (this.f) {
            l();
        }
    }

    public final void b() {
    }

    public final void b(ah ahVar) {
        if (this.f && ahVar.c && !this.c.d()) {
            l();
        }
    }

    public final void c() {
    }

    public final boolean d() {
        return this.f;
    }

    public final void e() {
        if (this.f) {
            this.b.b(this.a.getContext());
        }
    }

    public final void f() {
    }

    public final void g() {
    }

    public final void h() {
        if (this.f) {
            a();
        }
    }

    public final void i() {
        if (this.f) {
            this.b.c(this.a.getContext());
        }
    }

    public final void j() {
        if (this.f) {
            Context context = this.a.getContext();
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            EditText editText = new EditText(context);
            try {
                editText.setText(a("\n"));
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
            linearLayout.addView(editText);
            new AlertDialog.Builder(context).setTitle(udk.android.reader.b.b.aj).setView(linearLayout).setPositiveButton(udk.android.reader.b.b.r, new k(this, editText)).setNegativeButton(udk.android.reader.b.b.s, (DialogInterface.OnClickListener) null).show();
        }
    }

    public final void k() {
        this.b.a();
    }
}
