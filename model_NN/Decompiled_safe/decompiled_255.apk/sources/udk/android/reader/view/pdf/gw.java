package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import udk.android.reader.b.b;

final class gw implements Runnable {
    final /* synthetic */ PDFView a;

    gw(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void run() {
        Context context = this.a.getContext();
        String[] strArr = {b.G, b.H, b.I, b.J};
        new AlertDialog.Builder(context).setItems(strArr, new w(this, strArr)).show();
    }
}
