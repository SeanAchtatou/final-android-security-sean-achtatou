package udk.android.reader.view.pdf;

import android.graphics.RectF;
import udk.android.reader.b.a;
import udk.android.reader.b.c;

final class ct extends Thread {
    private /* synthetic */ cl a;

    ct(cl clVar) {
        this.a = clVar;
    }

    public final void run() {
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            c.a((Throwable) e);
        }
        if (!this.a.d.a && System.currentTimeMillis() > this.a.o + 300) {
            if ((a.R != 1 && !this.a.i.f() && !this.a.f.b().contains(new RectF(0.0f, 0.0f, (float) this.a.h.a, (float) this.a.h.b))) && !this.a.a.p()) {
                this.a.b.b();
            }
        }
    }
}
