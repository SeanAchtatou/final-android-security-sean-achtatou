package udk.android.reader.view.pdf;

import java.io.File;
import java.io.FileFilter;

final class dh implements FileFilter {
    dh() {
    }

    public final boolean accept(File file) {
        return !file.isDirectory() && file.getName().startsWith("res_");
    }
}
