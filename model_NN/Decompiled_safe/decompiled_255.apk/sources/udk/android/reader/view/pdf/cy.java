package udk.android.reader.view.pdf;

import android.app.ProgressDialog;
import udk.android.reader.b.c;
import udk.android.reader.pdf.ai;

final class cy implements Runnable {
    private /* synthetic */ dx a;
    private final /* synthetic */ ProgressDialog b;
    private final /* synthetic */ ai c;
    private final /* synthetic */ ai d;
    private final /* synthetic */ Runnable e;

    cy(dx dxVar, ProgressDialog progressDialog, ai aiVar, ai aiVar2, Runnable runnable) {
        this.a = dxVar;
        this.b = progressDialog;
        this.c = aiVar;
        this.d = aiVar2;
        this.e = runnable;
    }

    public final void run() {
        while (this.a.a.k.v()) {
            try {
                Thread.sleep(100);
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
        this.a.a.Y();
        this.b.dismiss();
        if (!(this.c == null || this.d == null)) {
            this.a.a.V().a(this.c, this.d);
        }
        if (this.e != null) {
            this.e.run();
        }
    }
}
