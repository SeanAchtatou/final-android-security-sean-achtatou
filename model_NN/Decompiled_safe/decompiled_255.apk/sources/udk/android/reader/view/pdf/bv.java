package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;

final class bv extends View {
    private /* synthetic */ PDFReadingToolbar a;
    private final /* synthetic */ int b;
    private final /* synthetic */ Paint c;
    private final /* synthetic */ Paint d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bv(PDFReadingToolbar pDFReadingToolbar, Context context, int i, Paint paint, Paint paint2) {
        super(context);
        this.a = pDFReadingToolbar;
        this.b = i;
        this.c = paint;
        this.d = paint2;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight()), (float) (this.b * 2), (float) (this.b * 2), this.c);
        canvas.drawRect((float) this.b, (float) this.b, (float) (getWidth() - this.b), (float) (getHeight() - this.b), this.d);
    }
}
