package udk.android.reader.view.pdf;

import udk.android.reader.b.c;
import udk.android.reader.view.pdf.PDFView;

final class eh extends Thread {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ PDFView.ViewMode b;
    private final /* synthetic */ float c;

    eh(PDFView pDFView, PDFView.ViewMode viewMode, float f) {
        this.a = pDFView;
        this.b = viewMode;
        this.c = f;
    }

    public final void run() {
        if (this.b == PDFView.ViewMode.PDF) {
            this.a.A.c();
        }
        while (this.a.U != null) {
            if (this.b == PDFView.ViewMode.PDF) {
                if (!this.a.A.a(this.c)) {
                    this.a.v();
                }
            } else if (this.b == PDFView.ViewMode.TEXTREFLOW) {
                this.a.G.a(1.0f + this.c);
            }
            try {
                Thread.sleep(48);
            } catch (Exception e) {
                c.a((Throwable) e);
            }
        }
        this.a.A.d();
    }
}
