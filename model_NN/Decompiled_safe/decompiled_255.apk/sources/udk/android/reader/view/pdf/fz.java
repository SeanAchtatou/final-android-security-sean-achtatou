package udk.android.reader.view.pdf;

import android.view.View;
import udk.android.reader.view.pdf.PDFView;

final class fz implements View.OnClickListener {
    private /* synthetic */ PDFView a;

    fz(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void onClick(View view) {
        if (this.a.F() == PDFView.ViewMode.PDF) {
            this.a.o();
        } else if (this.a.F() == PDFView.ViewMode.TEXTREFLOW) {
            this.a.G.c();
        }
    }
}
