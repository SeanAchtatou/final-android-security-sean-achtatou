package udk.android.reader.view.pdf;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Iterator;
import java.util.List;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.z;

public final class ay extends LinearLayout {
    private int a = ((int) a.a(5.0f));
    private int b = ((int) a.a(10.0f));
    /* access modifiers changed from: private */
    public Runnable c;

    public ay(Context context, List list, Runnable runnable) {
        super(context);
        this.c = runnable;
        setOrientation(1);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            a(context, (z) it.next(), 0);
        }
    }

    private void a(Context context, z zVar, int i) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        linearLayout.setPadding(this.b * i, this.a, 0, this.a);
        TextView textView = new TextView(context);
        textView.setText("Re : ");
        textView.setTextColor(-2013265920);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.weight = 0.0f;
        linearLayout.addView(textView, layoutParams);
        TextView textView2 = new TextView(context);
        textView2.setText(zVar.M());
        textView2.setTextColor(a.aJ);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.weight = 1.0f;
        linearLayout.addView(textView2, layoutParams2);
        TextView textView3 = new TextView(context);
        textView3.setText(zVar.P());
        textView3.setTextColor(a.aJ);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.weight = 0.0f;
        linearLayout.addView(textView3, layoutParams3);
        addView(linearLayout, new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOnClickListener(new i(this, context, zVar, linearLayout));
        if (zVar.a()) {
            for (z a2 : zVar.c()) {
                a(context, a2, i + 1);
            }
        }
    }
}
