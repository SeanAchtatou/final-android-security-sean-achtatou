package udk.android.reader.view.contents.web;

import android.view.KeyEvent;
import android.widget.TextView;

final class o implements TextView.OnEditorActionListener {
    private /* synthetic */ g a;

    o(g gVar) {
        this.a = gVar;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        this.a.a(this.a.c.getText().toString());
        return true;
    }
}
