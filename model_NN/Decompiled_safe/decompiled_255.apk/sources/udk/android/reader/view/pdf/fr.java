package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.e;

final class fr implements View.OnClickListener {
    final /* synthetic */ au a;
    private final /* synthetic */ Annotation b;
    private final /* synthetic */ Context c;
    private final /* synthetic */ String d;

    fr(au auVar, Annotation annotation, Context context, String str) {
        this.a = auVar;
        this.b = annotation;
        this.c = context;
        this.d = str;
    }

    public final void onClick(View view) {
        String[] strArr = this.b instanceof e ? new String[]{"0.0", "0.5", "1.0", "2.0", "3.0", "4.0", "5.0", "6.0", "7.0", "8.0", "9.0", "10.0", "11.0", "12.0"} : new String[]{"0.5", "1.0", "2.0", "3.0", "4.0", "5.0", "6.0", "7.0", "8.0", "9.0", "10.0", "11.0", "12.0"};
        new AlertDialog.Builder(this.c).setTitle(this.d).setItems(strArr, new il(this, strArr)).show();
    }
}
