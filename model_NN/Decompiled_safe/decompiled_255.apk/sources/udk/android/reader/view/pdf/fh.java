package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.widget.LinearLayout;
import udk.android.reader.b.a;

final class fh implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ boolean b;

    fh(PDFView pDFView, boolean z) {
        this.a = pDFView;
        this.b = z;
    }

    public final void run() {
        if (this.b) {
            View[] B = this.a.q;
            int length = B.length;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < length) {
                    PDFReadingToolbar pDFReadingToolbar = (PDFReadingToolbar) B[i2].findViewById(this.a.t);
                    PDFView pDFView = this.a;
                    int a2 = (int) a.a(5.0f);
                    int a3 = (int) a.a(23.0f);
                    Context context = pDFReadingToolbar.getContext();
                    pDFReadingToolbar.removeAllViews();
                    Paint paint = new Paint(1);
                    paint.setColor(-1996488705);
                    Paint paint2 = new Paint(1);
                    paint2.setStyle(Paint.Style.STROKE);
                    paint2.setColor(-2013265920);
                    Paint paint3 = new Paint(1);
                    paint3.setColor(-16777216);
                    Paint paint4 = new Paint(1);
                    paint4.setColor(-1);
                    paint4.setTextSize(((float) a2) * 1.5f);
                    pDFReadingToolbar.setPadding(a2, a2, a2, a2);
                    pDFReadingToolbar.setBackgroundDrawable(new bx(pDFReadingToolbar, a2, paint, paint2));
                    pDFReadingToolbar.setOrientation(0);
                    bv bvVar = new bv(pDFReadingToolbar, context, a2, paint3, paint4);
                    bvVar.setOnClickListener(new cb(pDFReadingToolbar, pDFView));
                    pDFReadingToolbar.addView(bvVar, new LinearLayout.LayoutParams(a3, a3));
                    pDFReadingToolbar.addView(new View(context), new LinearLayout.LayoutParams(a2, a2));
                    bz bzVar = new bz(pDFReadingToolbar, context, a2, paint3, paint4);
                    bzVar.setOnClickListener(new bq(pDFReadingToolbar, pDFView));
                    pDFReadingToolbar.addView(bzVar, new LinearLayout.LayoutParams(a3, a3));
                    pDFReadingToolbar.addView(new View(context), new LinearLayout.LayoutParams(a2, a2));
                    bp bpVar = new bp(pDFReadingToolbar, context, a2, paint3, paint4);
                    bpVar.setOnClickListener(new bt(pDFReadingToolbar, pDFView));
                    pDFReadingToolbar.addView(bpVar, new LinearLayout.LayoutParams(a3, a3));
                    pDFReadingToolbar.addView(new View(context), new LinearLayout.LayoutParams(a2, a2));
                    br brVar = new br(pDFReadingToolbar, context, a2, paint3, paint4);
                    brVar.setOnClickListener(new cd(pDFReadingToolbar, context));
                    pDFReadingToolbar.addView(brVar, new LinearLayout.LayoutParams(a3, a3));
                    pDFReadingToolbar.addView(new View(context), new LinearLayout.LayoutParams(a2, a2));
                    Rect rect = new Rect();
                    paint4.getTextBounds("Skip", 0, "Skip".length(), rect);
                    dl dlVar = new dl(pDFReadingToolbar, context, a2, paint3, rect, paint4);
                    dlVar.setOnClickListener(new dk(pDFReadingToolbar, pDFView));
                    pDFReadingToolbar.addView(dlVar, new LinearLayout.LayoutParams(a3, a3));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
