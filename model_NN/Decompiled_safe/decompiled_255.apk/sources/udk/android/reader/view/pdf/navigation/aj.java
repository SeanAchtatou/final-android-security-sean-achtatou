package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.view.View;
import udk.android.reader.C0000R;
import udk.android.reader.pdf.a;

final class aj implements View.OnClickListener {
    private /* synthetic */ NavigationService a;
    private final /* synthetic */ View b;

    aj(NavigationService navigationService, View view) {
        this.a = navigationService;
        this.b = view;
    }

    public final void onClick(View view) {
        Context context = this.b.getContext();
        a.a().a(context, context.getString(C0000R.string.f152), context.getString(C0000R.string.f156), context.getString(C0000R.string.f155), context.getString(C0000R.string.f50), context.getString(C0000R.string.f53));
    }
}
