package udk.android.reader.view.pdf;

import android.graphics.RectF;
import udk.android.reader.pdf.a.b;

final class dn implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ b c;

    dn(PDFView pDFView, String str, b bVar) {
        this.a = pDFView;
        this.b = str;
        this.c = bVar;
    }

    public final void run() {
        this.a.K.a(this.b, this.c, new RectF((float) this.a.Q, (float) this.a.R, (float) (this.a.getWidth() - this.a.S), (float) (this.a.getHeight() - this.a.T)), this.a.o, null);
    }
}
