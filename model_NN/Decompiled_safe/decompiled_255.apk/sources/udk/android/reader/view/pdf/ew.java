package udk.android.reader.view.pdf;

import udk.android.reader.pdf.annotation.s;

final class ew implements Runnable {
    private /* synthetic */ PDFView a;

    ew(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void run() {
        o V = this.a.V();
        if (V.o()) {
            s.a().a(this.a.z(), V.t(), V.u());
            this.a.postDelayed(new ht(this, V), 100);
        }
    }
}
