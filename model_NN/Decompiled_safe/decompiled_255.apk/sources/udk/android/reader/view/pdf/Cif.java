package udk.android.reader.view.pdf;

import android.widget.ImageView;

/* renamed from: udk.android.reader.view.pdf.if  reason: invalid class name */
final class Cif extends Thread {
    private /* synthetic */ e a;
    private final /* synthetic */ be b;
    private final /* synthetic */ int c;
    private final /* synthetic */ float d;
    private final /* synthetic */ ImageView e;

    Cif(e eVar, be beVar, int i, float f, ImageView imageView) {
        this.a = eVar;
        this.b = beVar;
        this.c = i;
        this.d = f;
        this.e = imageView;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r0 = 500(0x1f4, double:2.47E-321)
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0016 }
        L_0x0005:
            udk.android.reader.view.pdf.e r0 = r5.a
            java.lang.Object r0 = r0.c
            monitor-enter(r0)
            udk.android.reader.view.pdf.e r1 = r5.a     // Catch:{ all -> 0x003a }
            int r1 = r1.getVisibility()     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x001f
            monitor-exit(r0)     // Catch:{ all -> 0x003a }
        L_0x0015:
            return
        L_0x0016:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            udk.android.reader.b.c.a(r1, r0)
            goto L_0x0005
        L_0x001f:
            udk.android.reader.view.pdf.be r1 = r5.b     // Catch:{ all -> 0x003a }
            int r1 = r1.getBottom()     // Catch:{ all -> 0x003a }
            udk.android.reader.view.pdf.be r2 = r5.b     // Catch:{ all -> 0x003a }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x003a }
            int r2 = r2 / 2
            int r1 = r1 - r2
            if (r1 < 0) goto L_0x0038
            udk.android.reader.view.pdf.e r2 = r5.a     // Catch:{ all -> 0x003a }
            int r2 = r2.getHeight()     // Catch:{ all -> 0x003a }
            if (r1 <= r2) goto L_0x003d
        L_0x0038:
            monitor-exit(r0)     // Catch:{ all -> 0x003a }
            goto L_0x0015
        L_0x003a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003a }
            throw r1
        L_0x003d:
            udk.android.reader.view.pdf.e r1 = r5.a     // Catch:{ all -> 0x003a }
            udk.android.reader.pdf.PDF r1 = r1.b     // Catch:{ all -> 0x003a }
            int r2 = r5.c     // Catch:{ all -> 0x003a }
            float r3 = r5.d     // Catch:{ all -> 0x003a }
            android.graphics.Bitmap r1 = r1.c(r2, r3)     // Catch:{ all -> 0x003a }
            android.widget.ImageView r2 = r5.e     // Catch:{ all -> 0x003a }
            udk.android.reader.view.pdf.ih r3 = new udk.android.reader.view.pdf.ih     // Catch:{ all -> 0x003a }
            android.widget.ImageView r4 = r5.e     // Catch:{ all -> 0x003a }
            r3.<init>(r5, r4, r1)     // Catch:{ all -> 0x003a }
            r2.post(r3)     // Catch:{ all -> 0x003a }
            monitor-exit(r0)     // Catch:{ all -> 0x003a }
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.pdf.Cif.run():void");
    }
}
