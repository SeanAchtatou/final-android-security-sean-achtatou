package udk.android.reader.view.pdf;

final class ge extends Thread {
    private /* synthetic */ o a;
    private final /* synthetic */ int b;
    private final /* synthetic */ float c;
    private final /* synthetic */ float d;
    private final /* synthetic */ Runnable e;

    ge(o oVar, int i, float f, float f2, Runnable runnable) {
        this.a = oVar;
        this.b = i;
        this.c = f;
        this.d = f2;
        this.e = runnable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, boolean):void
     arg types: [udk.android.reader.view.pdf.o, int]
     candidates:
      udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, int):boolean
      udk.android.reader.view.pdf.o.a(float, float):udk.android.reader.pdf.x
      udk.android.reader.view.pdf.o.a(int, int):void
      udk.android.reader.view.pdf.o.a(udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):void
      udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, boolean):void */
    public final void run() {
        o.a(this.a, this.b, this.c, this.d);
        this.a.w();
        if (this.b != this.a.b.E()) {
            return;
        }
        if (this.a.a(this.b)) {
            this.a.a(true);
        } else if (this.e != null) {
            this.e.run();
        }
    }
}
