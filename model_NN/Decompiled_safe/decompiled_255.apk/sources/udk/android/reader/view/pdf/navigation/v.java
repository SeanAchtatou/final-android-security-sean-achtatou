package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.AdapterView;

final class v implements AdapterView.OnItemClickListener {
    final /* synthetic */ SearchNavigationView a;

    v(SearchNavigationView searchNavigationView) {
        this.a = searchNavigationView;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.postDelayed(new aa(this, i), 300);
    }
}
