package udk.android.reader.view.pdf;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import com.unidocs.commonlib.util.i;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ar;
import udk.android.reader.view.pdf.ZoomService;

public final class hx implements ar {
    private static hx c;
    public float a;
    public float b;
    private PDF d = PDF.a();
    private boolean e;
    private ic f;
    private boolean g;
    private Map h;
    private Point i;
    private int j;
    private int k;
    private boolean l;
    private Point m;

    private hx() {
        this.d.a(this);
        this.f = ic.a();
    }

    public static Bitmap a(int i2) {
        ez a2 = n.b().a(i2);
        if (a2 == null) {
            return null;
        }
        return a2.c();
    }

    public static hx a() {
        if (c == null) {
            c = new hx();
        }
        return c;
    }

    private Point z() {
        int i2;
        int i3;
        if (!(this.i != null && this.j == this.f.a && this.k == this.f.b)) {
            int i4 = this.f.a;
            int i5 = this.f.b;
            if (a.ak > 1.0f) {
                int i6 = (int) (((float) this.f.b) * a.ak);
                i2 = (int) (((float) this.f.a) * a.ak);
                i3 = i6;
            } else {
                int i7 = i5;
                i2 = i4;
                i3 = i7;
            }
            this.i = new Point(i2, i3);
            this.j = this.f.a;
            this.k = this.f.b;
        }
        return this.i;
    }

    public final float a(int i2, float f2) {
        if (this.d.G()) {
            return (float) this.f.a;
        }
        return (float) this.d.a(i2, this.d.H() ? this.d.e(i2, (float) this.f.b) : f2);
    }

    public final void a(String str, float f2) {
        Bitmap c2 = this.d.c(1, this.d.a(f2));
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(new File(str));
            try {
                c2.compress(Bitmap.CompressFormat.PNG, 50, fileOutputStream2);
                i.a(fileOutputStream2);
            } catch (Throwable th) {
                th = th;
                fileOutputStream = fileOutputStream2;
                i.a(fileOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            c.a(th);
            i.a(fileOutputStream);
        }
    }

    public final void a(ah ahVar) {
    }

    public final boolean a(boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        int A = this.d.A();
        int B = this.d.B();
        if (z) {
            z3 = true;
        } else if (A < this.f.a) {
            this.a = (float) ((this.f.a / 2) - (A / 2));
            z3 = true;
        } else if (this.a > 0.0f) {
            this.a = 0.0f;
            z3 = true;
        } else if (this.a + ((float) A) < ((float) this.f.a)) {
            this.a = (float) (this.f.a - A);
            z3 = true;
        } else {
            z3 = false;
        }
        if (z2) {
            z4 = true;
        } else if (B < this.f.b) {
            this.b = (float) ((this.f.b / 2) - (B / 2));
            z4 = true;
        } else if (this.b > 0.0f) {
            this.b = 0.0f;
            z4 = true;
        } else if (this.b + ((float) B) < ((float) this.f.b)) {
            this.b = (float) (this.f.b - B);
            z4 = true;
        } else {
            z4 = false;
        }
        return z3 && z4;
    }

    public final float b(int i2) {
        Float f2 = (Float) this.h.get(Integer.valueOf(i2));
        if (f2 == null) {
            if (this.f.a == 0) {
                return 0.0f;
            }
            f2 = Float.valueOf(ZoomService.a().c(i2) == ZoomService.FittingType.WIDTHFIT ? this.d.d(i2, (float) this.f.a) : this.d.e(i2, (float) this.f.b));
            this.h.put(Integer.valueOf(i2), f2);
        }
        return f2.floatValue();
    }

    public final float b(int i2, float f2) {
        if (this.d.H()) {
            return (float) this.f.b;
        }
        return (float) this.d.b(i2, this.d.G() ? this.d.d(i2, (float) this.f.a) : f2);
    }

    public final RectF b() {
        return new RectF(this.a, this.b, this.a + ((float) this.d.A()), this.b + ((float) this.d.B()));
    }

    public final void b(ah ahVar) {
        if (ahVar.d || ahVar.c) {
            this.l = true;
            this.g = false;
        }
    }

    public final float c() {
        return this.a + ((float) this.d.A());
    }

    public final void c(int i2, float f2) {
        new bo(this, i2, f2).start();
    }

    public final float d() {
        return this.b + ((float) this.d.B());
    }

    public final int e() {
        return this.d.A();
    }

    public final void f() {
    }

    public final void g() {
        this.h = new HashMap();
        this.a = 0.0f;
        this.b = 0.0f;
        this.l = true;
    }

    public final void h() {
    }

    public final int i() {
        return this.d.B();
    }

    public final boolean j() {
        return this.f.a((float) this.d.A());
    }

    public final boolean k() {
        return this.a > 0.0f && c() < ((float) this.f.a);
    }

    public final boolean l() {
        return this.f.b((float) this.d.B());
    }

    public final boolean m() {
        return this.b > 0.0f && d() < ((float) this.f.b);
    }

    public final float n() {
        return this.d.F();
    }

    public final boolean o() {
        return this.b + ((float) this.d.B()) <= ((float) this.f.b);
    }

    public final void p() {
        this.e = true;
    }

    public final void q() {
        this.e = false;
    }

    public final boolean r() {
        return !this.g || this.e;
    }

    public final boolean s() {
        return this.d.F() <= w() + 0.001f;
    }

    public final boolean t() {
        return j() && l();
    }

    public final ez u() {
        ez a2;
        int E = this.d.E();
        float F = this.d.F();
        if (E <= 0) {
            return null;
        }
        n b2 = n.b();
        if (s()) {
            a2 = b2.b(E);
        } else {
            int i2 = (int) (0.0f - this.a);
            int i3 = i2 < 0 ? 0 : i2;
            int i4 = (int) (0.0f - this.b);
            int i5 = i4 < 0 ? 0 : i4;
            int i6 = this.f.a + i3;
            int a3 = this.d.a(E, F);
            if (i6 <= a3) {
                a3 = i6;
            }
            int i7 = this.f.b + i5;
            int b3 = this.d.b(E, F);
            if (i7 <= b3) {
                b3 = i7;
            }
            Rect rect = new Rect(i3, i5, a3, b3);
            Point z = z();
            if (this.l) {
                Point z2 = z();
                int i8 = (int) (0.0f - this.a);
                if (z2.x > this.f.a) {
                    i8 = (int) ((0.0f - this.a) - (((float) (z2.x - this.f.a)) * 0.5f));
                }
                if (i8 < 0) {
                    i8 = 0;
                }
                int i9 = (int) (0.0f - this.b);
                if (z2.y > this.f.b) {
                    i9 = (int) ((0.0f - this.b) - (((float) (z2.y - this.f.b)) * 0.5f));
                }
                if (i9 < 0) {
                    i9 = 0;
                }
                if (z2.x > this.d.A() || z2.y > this.d.B()) {
                    this.m = new Point(0, 0);
                } else {
                    this.m = new Point(i8 % z2.x, i9 % z2.y);
                }
                this.l = false;
            }
            a2 = b2.a(E, F, rect, z, this.m);
        }
        if (this.g || a2 == null) {
            return a2;
        }
        this.g = true;
        return a2;
    }

    public final boolean v() {
        n b2 = n.b();
        return s() ? !b2.c(this.d.E()) : b2.e();
    }

    public final float w() {
        return b(this.d.E());
    }

    public final void x() {
        if (this.h != null) {
            this.h.clear();
        }
    }

    public final void y() {
        this.l = true;
    }
}
