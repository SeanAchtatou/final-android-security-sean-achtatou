package udk.android.reader.view.pdf;

import android.widget.FrameLayout;
import android.widget.SeekBar;
import udk.android.reader.b.c;

final class gn extends Thread {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ FrameLayout b;
    private final /* synthetic */ SeekBar c;

    gn(PDFView pDFView, FrameLayout frameLayout, SeekBar seekBar) {
        this.a = pDFView;
        this.b = frameLayout;
        this.c = seekBar;
    }

    public final void run() {
        while (this.a.a != null && this.a.I.a(this.b)) {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                c.a((Throwable) e);
            }
            try {
                this.a.post(new hm(this, this.c, (int) ((((float) this.a.a.getCurrentPosition()) / ((float) this.a.a.getDuration())) * ((float) this.c.getMax()))));
            } catch (Exception e2) {
                c.a((Throwable) e2);
                return;
            }
        }
    }
}
