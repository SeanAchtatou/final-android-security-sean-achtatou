package udk.android.reader.view.contents.web;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class WebPageView extends WebView {
    public WebPageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBackgroundColor(-1);
    }

    public WebPageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setBackgroundColor(-1);
    }
}
