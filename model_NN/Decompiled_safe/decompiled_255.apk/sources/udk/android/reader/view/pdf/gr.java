package udk.android.reader.view.pdf;

import android.graphics.drawable.AnimationDrawable;

final class gr implements Runnable {
    private /* synthetic */ PDFView a;

    gr(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void run() {
        this.a.J.setVisibility(4);
        AnimationDrawable animationDrawable = (AnimationDrawable) this.a.J.getDrawable();
        if (animationDrawable != null) {
            animationDrawable.stop();
        }
    }
}
