package udk.android.reader.view.pdf;

import android.view.MotionEvent;

final class cs implements Runnable {
    final /* synthetic */ cl a;
    private final /* synthetic */ int b;
    private final /* synthetic */ float c;
    private final /* synthetic */ float d;
    private final /* synthetic */ MotionEvent e;

    cs(cl clVar, int i, float f, float f2, MotionEvent motionEvent) {
        this.a = clVar;
        this.b = i;
        this.c = f;
        this.d = f2;
        this.e = motionEvent;
    }

    public final void run() {
        this.a.k.a(this.b, this.c, this.d, new q(this, this.e));
    }
}
