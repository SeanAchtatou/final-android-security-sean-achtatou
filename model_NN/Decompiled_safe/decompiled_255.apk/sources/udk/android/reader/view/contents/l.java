package udk.android.reader.view.contents;

import android.view.View;
import android.widget.CheckBox;
import udk.android.b.d;
import udk.android.reader.a.a;
import udk.android.reader.contents.ai;
import udk.android.reader.contents.an;

final class l implements View.OnClickListener {
    private /* synthetic */ c a;
    private final /* synthetic */ ai b;
    private final /* synthetic */ CheckBox c;
    private final /* synthetic */ View d;

    l(c cVar, ai aiVar, CheckBox checkBox, View view) {
        this.a = cVar;
        this.b = aiVar;
        this.c = checkBox;
        this.d = view;
    }

    public final void onClick(View view) {
        if (this.b.f() == 21) {
            if (this.a.a.d()) {
                this.c.setChecked(!this.c.isChecked());
                if (this.c.isChecked()) {
                    this.a.a.a(this.b);
                } else {
                    this.a.a.b(this.b);
                }
            } else {
                a.a(this.a.b, this.b.d(), (String) null);
                d.a(this.d, 1157627903, 500);
            }
        } else if (this.b.f() == 11) {
            an.c().a(view.getContext(), this.b.d());
        }
    }
}
