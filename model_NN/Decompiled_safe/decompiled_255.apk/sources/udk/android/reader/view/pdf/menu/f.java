package udk.android.reader.view.pdf.menu;

import android.text.Spannable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

final class f implements View.OnTouchListener {
    private /* synthetic */ g a;
    private final /* synthetic */ LinearLayout b;

    f(g gVar, LinearLayout linearLayout) {
        this.a = gVar;
        this.b = linearLayout;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        TextView textView = (TextView) this.b.findViewById(Integer.MAX_VALUE);
        motionEvent.offsetLocation((float) (0 - this.b.getPaddingLeft()), (float) (0 - this.b.getPaddingTop()));
        b.a().onTouchEvent(textView, (Spannable) textView.getText(), motionEvent);
        return true;
    }
}
