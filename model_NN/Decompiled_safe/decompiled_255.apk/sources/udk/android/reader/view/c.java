package udk.android.reader.view;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.view.View;

final class c extends Drawable {
    private /* synthetic */ d a;
    private final /* synthetic */ View b;
    private final /* synthetic */ boolean c;

    c(d dVar, View view, boolean z) {
        this.a = dVar;
        this.b = view;
        this.c = z;
    }

    public final void draw(Canvas canvas) {
        int width = this.a.getWidth();
        int height = this.b.getHeight() + (this.a.a * 4);
        int i = height / 2;
        Path path = new Path();
        path.moveTo((float) (this.a.b + (this.a.a * 2)), (float) this.a.a);
        path.quadTo((float) (this.a.b + this.a.a), (float) this.a.a, (float) (this.a.b + this.a.a), (float) (this.a.a * 2));
        if (!this.c) {
            path.lineTo((float) (this.a.b + this.a.a), ((float) i) - (((float) (this.a.a * 2)) * 0.5f));
            path.lineTo((float) ((this.a.b + this.a.a) - (this.a.a * 2)), (float) i);
            path.lineTo((float) (this.a.b + this.a.a), ((float) i) + (((float) (this.a.a * 2)) * 0.5f));
        }
        path.lineTo((float) (this.a.b + this.a.a), (float) (height - (this.a.a * 2)));
        path.quadTo((float) (this.a.b + this.a.a), (float) (height - this.a.a), (float) (this.a.b + (this.a.a * 2)), (float) (height - this.a.a));
        path.lineTo((float) ((width - this.a.c) - (this.a.a * 2)), (float) (height - this.a.a));
        path.quadTo((float) ((width - this.a.c) - this.a.a), (float) (height - this.a.a), (float) ((width - this.a.c) - this.a.a), (float) (height - (this.a.a * 2)));
        if (this.c) {
            path.lineTo((float) ((width - this.a.c) - this.a.a), ((float) i) + (((float) (this.a.a * 2)) * 0.5f));
            path.lineTo((float) (((width - this.a.c) - this.a.a) + (this.a.a * 2)), (float) i);
            path.lineTo((float) ((width - this.a.c) - this.a.a), ((float) i) - (((float) (this.a.a * 2)) * 0.5f));
        }
        path.lineTo((float) ((width - this.a.c) - this.a.a), (float) (this.a.a * 2));
        path.quadTo((float) ((width - this.a.c) - this.a.a), (float) this.a.a, (float) ((width - this.a.c) - (this.a.a * 2)), (float) this.a.a);
        path.lineTo((float) (this.a.b + (this.a.a * 2)), (float) this.a.a);
        canvas.save();
        canvas.translate(0.0f, (float) ((this.a.a * 2) / 4));
        canvas.drawPath(path, this.a.e);
        canvas.restore();
        canvas.drawPath(path, this.a.d);
    }

    public final int getOpacity() {
        return 0;
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
