package udk.android.reader.view.pdf.menu;

import android.widget.TextView;

final class h implements Runnable {
    private /* synthetic */ b a;
    private final /* synthetic */ MenuCommandSpan b;
    private final /* synthetic */ TextView c;

    h(b bVar, MenuCommandSpan menuCommandSpan, TextView textView) {
        this.a = bVar;
        this.b = menuCommandSpan;
        this.c = textView;
    }

    public final void run() {
        this.b.a = false;
        this.c.invalidate();
    }
}
