package udk.android.reader.view.pdf.menu;

import android.graphics.RectF;
import android.widget.LinearLayout;
import android.widget.TextView;
import udk.android.reader.pdf.a.c;

final class e extends Thread {
    final /* synthetic */ g a;
    private final /* synthetic */ LinearLayout b;
    private final /* synthetic */ TextView c;
    private final /* synthetic */ c d;
    private final /* synthetic */ RectF e;

    e(g gVar, LinearLayout linearLayout, TextView textView, c cVar, RectF rectF) {
        this.a = gVar;
        this.b = linearLayout;
        this.c = textView;
        this.d = cVar;
        this.e = rectF;
    }

    public final void run() {
        int i = 0;
        while (this.a.i != null && this.a.i == this.b && ((this.c == null || this.b.getWidth() <= (this.a.g * 2) + 10 || this.b.getHeight() <= (this.a.g * 2) + 10 || this.c.getWidth() <= 0 || this.c.getHeight() <= 0) && i <= 10)) {
            i++;
            try {
                Thread.sleep(100);
            } catch (Exception e2) {
                udk.android.reader.b.c.a((Throwable) e2);
            }
        }
        if (this.a.i != null && this.a.i == this.b) {
            this.a.post(new a(this, this.b, this.d, this.e));
        }
    }
}
