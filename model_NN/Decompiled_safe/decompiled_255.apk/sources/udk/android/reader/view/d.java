package udk.android.reader.view;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import android.widget.LinearLayout;
import udk.android.b.m;

public final class d extends LinearLayout {
    /* access modifiers changed from: private */
    public int a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public Paint d = new Paint(1);
    /* access modifiers changed from: private */
    public Paint e;

    public d(Context context, View view, View view2, boolean z, int i, int i2) {
        super(context);
        this.a = m.a(context, 5);
        this.d.setColor(z ? -2559533 : -471923);
        this.e = new Paint(1);
        this.e.setColor(-2013265920);
        this.e.setStyle(Paint.Style.FILL);
        this.e.setMaskFilter(new BlurMaskFilter((float) (this.a / 4), BlurMaskFilter.Blur.NORMAL));
        this.b = z ? i2 : i;
        this.c = z ? i : i2;
        setOrientation(1);
        addView(view, new LinearLayout.LayoutParams(-1, -2));
        setBackgroundDrawable(new c(this, view, z));
        a(context, view2);
    }

    private void a(Context context, View view) {
        setPadding(this.b + (this.a * 2), this.a * 2, this.c + (this.a * 2), view == null ? this.a * 2 : this.a);
        if (getChildCount() > 1) {
            while (getChildCount() > 1) {
                removeViewAt(getChildCount() - 1);
            }
        }
        if (view != null) {
            addView(new View(context), new LinearLayout.LayoutParams(-1, this.a * 2));
            addView(view, new LinearLayout.LayoutParams(-1, -2));
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
