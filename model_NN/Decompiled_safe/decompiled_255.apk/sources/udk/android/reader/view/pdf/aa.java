package udk.android.reader.view.pdf;

import java.net.URL;
import java.net.URLConnection;
import udk.android.b.i;
import udk.android.reader.b.c;

final class aa implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ URL b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ int e;
    private final /* synthetic */ float f = 0.0f;
    private final /* synthetic */ boolean g = true;
    private final /* synthetic */ float h = 0.0f;
    private final /* synthetic */ float i = 0.0f;
    private final /* synthetic */ int j = -1;
    private final /* synthetic */ i k;

    aa(PDFView pDFView, URL url, String str, String str2, int i2, i iVar) {
        this.a = pDFView;
        this.b = url;
        this.c = str;
        this.d = str2;
        this.e = i2;
        this.k = iVar;
    }

    public final void run() {
        long j2;
        while (this.a.getHeight() <= 0) {
            try {
                try {
                    Thread.sleep(100);
                } catch (Exception e2) {
                    c.a((Throwable) e2);
                }
            } catch (Exception e3) {
                if (this.k != null) {
                    this.k.a(e3);
                    return;
                }
                return;
            }
        }
        this.a.X();
        URLConnection openConnection = this.b.openConnection();
        try {
            j2 = Long.parseLong(openConnection.getHeaderField("Content-Length"));
        } catch (Exception e4) {
            j2 = 0;
        }
        PDFView.a(this.a, null, openConnection.getInputStream(), j2, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, 0);
    }
}
