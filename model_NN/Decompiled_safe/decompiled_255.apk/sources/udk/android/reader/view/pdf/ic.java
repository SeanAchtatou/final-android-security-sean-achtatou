package udk.android.reader.view.pdf;

import android.graphics.PointF;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ic {
    private static ic c;
    public int a;
    public int b;
    private List d = new ArrayList();

    private ic() {
    }

    public static ic a() {
        if (c == null) {
            c = new ic();
        }
        return c;
    }

    public final void a(int i, int i2) {
        this.a = i;
        this.b = i2;
        ft ftVar = new ft();
        ftVar.a = i;
        ftVar.b = i2;
        Iterator it = this.d.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final boolean a(float f) {
        return f <= ((float) this.a) + 1.0f;
    }

    public final PointF b() {
        return new PointF((float) (this.a / 2), (float) (this.b / 2));
    }

    public final boolean b(float f) {
        return f <= ((float) this.b) + 1.0f;
    }
}
