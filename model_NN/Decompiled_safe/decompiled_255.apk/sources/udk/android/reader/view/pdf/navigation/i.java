package udk.android.reader.view.pdf.navigation;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unidocs.commonlib.util.e;
import java.io.InputStream;
import udk.android.reader.C0000R;
import udk.android.reader.b.c;
import udk.android.reader.pdf.Bookmark;
import udk.android.reader.pdf.a;

public final class i extends BaseAdapter {
    private Context a;
    private NavigationService b = NavigationService.a();
    private a c = a.a();

    public i(Context context) {
        this.a = context;
    }

    /* renamed from: a */
    public final Bookmark getItem(int i) {
        return this.c.a(this.a, i);
    }

    public final int getCount() {
        return this.c.a(this.a);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        InputStream inputStream;
        View inflate = view != null ? view : View.inflate(this.a, C0000R.layout.pdf_page, null);
        Bookmark a2 = getItem(i);
        if (this.b.n() == NavigationService.a) {
            try {
                inputStream = this.a.getAssets().open("book/thumbnail/" + e.a(new StringBuilder(String.valueOf(a2.getPage())).toString()) + ".png");
            } catch (Exception e) {
                c.a(e.getMessage(), e);
                inputStream = null;
            }
            if (inputStream != null) {
                ((ImageView) inflate.findViewById(C0000R.id.thumbnail)).setImageBitmap(BitmapFactory.decodeStream(inputStream));
            }
        }
        ((TextView) inflate.findViewById(C0000R.id.desc)).setText(new StringBuilder().append(a2.getPage()).toString());
        return inflate;
    }
}
