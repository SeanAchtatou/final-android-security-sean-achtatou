package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

public final class ci extends ListView {
    private ia a;

    public ci(Context context, View view, ia iaVar) {
        super(context);
        this.a = iaVar;
        setDivider(null);
        setCacheColorHint(0);
        if (view != null) {
            addHeaderView(view);
        }
        setAdapter((ListAdapter) iaVar);
    }

    public final void a() {
        this.a.a(this);
    }
}
