package udk.android.reader.view.pdf;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.MotionEvent;
import android.widget.VideoView;

final class gl extends VideoView {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ Uri b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    gl(PDFView pDFView, Context context, Uri uri) {
        super(context);
        this.a = pDFView;
        this.b = uri;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        PDFView.E(this.a);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(this.b, "video/*");
        getContext().startActivity(intent);
        return true;
    }
}
