package udk.android.reader.view.pdf;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class PDFReadingToolbar extends LinearLayout {
    public PDFReadingToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
