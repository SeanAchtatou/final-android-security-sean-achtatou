package udk.android.reader.view.contents.web;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class l implements Runnable {
    private /* synthetic */ u a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ EditText c;

    l(u uVar, Activity activity, EditText editText) {
        this.a = uVar;
        this.b = activity;
        this.c = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.getSystemService("input_method")).showSoftInput(this.c, 1);
    }
}
