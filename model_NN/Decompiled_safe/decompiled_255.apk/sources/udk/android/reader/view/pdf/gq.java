package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import udk.android.b.m;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.view.pdf.menu.MenuCommandSpan;
import udk.android.reader.view.pdf.menu.c;

public final class gq extends LinearLayout {
    /* access modifiers changed from: private */
    public Annotation a;
    private Drawable b = this.e.getBackground();
    /* access modifiers changed from: private */
    public Runnable c;
    private TextView d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public ci f;

    public gq(Context context, Annotation annotation) {
        super(context);
        this.a = annotation;
        List e2 = s.a().e(annotation);
        boolean a2 = b.a((Collection) e2);
        setBackgroundColor(a.aI);
        int a3 = m.a(context, 5);
        setPadding(a3, a3, a3, a3);
        setOrientation(1);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setGravity(17);
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.weight = 0.0f;
        addView(linearLayout, layoutParams);
        View view = new View(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, 1);
        layoutParams2.weight = 1.0f;
        linearLayout.addView(view, layoutParams2);
        this.d = new TextView(context);
        this.d.setMovementMethod(udk.android.reader.view.pdf.menu.b.a());
        this.d.setPadding(a3, 0, a3, 0);
        this.d.setTextScaleX(0.8f);
        this.d.setTextSize(1, (float) a.aC);
        this.d.setTypeface(Typeface.defaultFromStyle(1));
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.weight = 0.0f;
        linearLayout.addView(this.d, layoutParams3);
        this.e = new EditText(context);
        c();
        if (b.b(annotation.M())) {
            b();
        }
        this.e.setOnClickListener(new ak(this));
        this.e.setHint(udk.android.reader.b.b.ac);
        this.e.setText(annotation.M());
        this.e.setGravity(48);
        this.e.setTextColor(a.aJ);
        new LinearLayout.LayoutParams(-1, -2).weight = (float) (a2 ? 0 : 1);
        this.f = new ci(context, this.e, new ia(annotation, e2));
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.weight = 1.0f;
        addView(this.f, layoutParams4);
        d();
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!(this.e.getBackground() != null)) {
            this.e.setBackgroundDrawable(this.b);
            this.e.setFocusable(true);
            this.e.setFocusableInTouchMode(true);
            this.e.requestFocus();
            this.e.postDelayed(new al(this), 100);
            d();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.e.setBackgroundDrawable(null);
        this.e.setFocusable(false);
        this.e.setFocusableInTouchMode(false);
        m.a(this.e);
        d();
    }

    /* access modifiers changed from: private */
    public void d() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new c(udk.android.reader.b.b.r, new aj(this)));
        arrayList.add(new c(udk.android.reader.b.b.g, a()));
        this.d.setText(MenuCommandSpan.a(arrayList, "  .  ", a.aD, a.aE), TextView.BufferType.SPANNABLE);
    }

    public final Runnable a() {
        return new ai(this);
    }

    public final void a(Runnable runnable) {
        this.c = runnable;
        post(new ah(this));
    }
}
