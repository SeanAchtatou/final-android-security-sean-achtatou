package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.AdapterView;

final class l implements AdapterView.OnItemClickListener {
    final /* synthetic */ PageNavigationView a;

    l(PageNavigationView pageNavigationView) {
        this.a = pageNavigationView;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.postDelayed(new j(this, adapterView, i), 300);
    }
}
