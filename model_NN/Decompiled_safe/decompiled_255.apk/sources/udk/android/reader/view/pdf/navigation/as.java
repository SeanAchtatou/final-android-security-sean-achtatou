package udk.android.reader.view.pdf.navigation;

import android.content.DialogInterface;
import android.widget.EditText;
import udk.android.reader.b.c;

final class as implements DialogInterface.OnClickListener {
    private /* synthetic */ NavigationService a;
    private final /* synthetic */ EditText b;

    as(NavigationService navigationService, EditText editText) {
        this.a = navigationService;
        this.b = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int i2 = -1;
        try {
            i2 = Integer.parseInt(this.b.getText().toString());
        } catch (Exception e) {
            c.a((Throwable) e);
        }
        if (i2 > 0) {
            NavigationService.a(this.a, i2);
        }
        this.a.v = null;
    }
}
