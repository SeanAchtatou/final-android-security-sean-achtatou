package udk.android.reader.view.contents;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import udk.android.reader.C0000R;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.contents.ai;
import udk.android.reader.pdf.ae;

public final class u extends BaseAdapter {
    private ae a = ae.a();
    /* access modifiers changed from: private */
    public Context b;

    public u(Context context) {
        this.b = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String getItem(int i) {
        return this.a.a(i);
    }

    public final int getCount() {
        return this.a.b(this.b);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        Activity activity = (Activity) viewGroup.getContext();
        String a2 = getItem(i);
        File file = new File(a2);
        if (view == null) {
            view2 = View.inflate(viewGroup.getContext(), C0000R.layout.content, null);
            ((Activity) viewGroup.getContext()).registerForContextMenu(view2);
        } else {
            view2 = view;
        }
        ImageView imageView = (ImageView) view2.findViewById(C0000R.id.thumbnail);
        String c = a.c(activity, file);
        if (c == null || !new File(c).exists()) {
            imageView.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), C0000R.drawable.icon_pdf));
        } else {
            try {
                imageView.setImageBitmap(BitmapFactory.decodeFile(c));
            } catch (OutOfMemoryError e) {
                c.a(e.getMessage(), e);
                System.gc();
                imageView.setImageBitmap(BitmapFactory.decodeResource(viewGroup.getContext().getResources(), C0000R.drawable.icon_pdf));
            }
        }
        view2.setOnClickListener(new t(this, a2, activity));
        ((TextView) view2.findViewById(C0000R.id.title)).setText(file.getName());
        ((TextView) view2.findViewById(C0000R.id.last_modified)).setText(ai.a(file));
        ((TextView) view2.findViewById(C0000R.id.size)).setText(ai.b(file));
        return view2;
    }
}
