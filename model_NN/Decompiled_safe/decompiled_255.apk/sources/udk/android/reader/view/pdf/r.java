package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import udk.android.reader.pdf.annotation.w;

final class r implements View.OnClickListener {
    final /* synthetic */ au a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;

    r(au auVar, Context context, String str) {
        this.a = auVar;
        this.b = context;
        this.c = str;
    }

    public final void onClick(View view) {
        String[] strArr = {w.b("OpenArrow"), w.b("Circle"), w.b("Square"), w.b("None")};
        new AlertDialog.Builder(this.b).setTitle(this.c).setItems(strArr, new fp(this, strArr)).show();
    }
}
