package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

final class by extends View {
    private /* synthetic */ hn a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    by(hn hnVar, Context context) {
        super(context);
        this.a = hnVar;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        if (pointerCount > this.a.k.b) {
            this.a.k.b = pointerCount;
        }
        if (motionEvent.getAction() == 0) {
            this.a.k.b = pointerCount;
        }
        return this.a.c.onTouchEvent(motionEvent);
    }
}
