package udk.android.reader.view.pdf;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import udk.android.reader.b.a;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.z;
import udk.android.reader.view.d;

public final class ia extends BaseAdapter {
    /* access modifiers changed from: private */
    public Annotation a;
    /* access modifiers changed from: private */
    public List b;

    public ia(Annotation annotation, List list) {
        this.a = annotation;
        this.b = list != null ? list : s.a().e(annotation);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public z getItem(int i) {
        return (z) this.b.get(i);
    }

    /* access modifiers changed from: package-private */
    public final void a(View view) {
        view.post(new f(this));
    }

    public final int getCount() {
        return this.b.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        Context context = viewGroup.getContext();
        z a2 = getItem(i);
        String P = this.a.P();
        String P2 = a2.P();
        boolean z = (P == null || P2 == null || !P.equals(P2)) ? false : true;
        int a3 = (int) a.a(10.0f);
        int a4 = (int) a.a(20.0f);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        TextView textView = new TextView(context);
        textView.setText(a2.P());
        textView.setTextColor(a.aJ);
        textView.setTypeface(Typeface.defaultFromStyle(1));
        linearLayout.addView(textView);
        TextView textView2 = new TextView(context);
        textView2.setText(a2.M());
        textView2.setTextColor(a.aJ);
        linearLayout.addView(textView2);
        h hVar = new h(this, viewGroup);
        d dVar = new d(context, linearLayout, a2.a() ? new ay(context, a2.c(), hVar) : null, z, a3, a4);
        linearLayout.setOnClickListener(new g(this, context, a2, hVar));
        return dVar;
    }

    public final boolean isEnabled(int i) {
        return false;
    }
}
