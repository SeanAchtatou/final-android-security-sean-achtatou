package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import udk.android.reader.pdf.annotation.p;

final class fm implements Runnable {
    final /* synthetic */ au a;
    private final /* synthetic */ Context b;

    fm(au auVar, Context context) {
        this.a = auVar;
        this.b = context;
    }

    public final void run() {
        new AlertDialog.Builder(this.b).setItems(p.a, new ii(this)).create().show();
    }
}
