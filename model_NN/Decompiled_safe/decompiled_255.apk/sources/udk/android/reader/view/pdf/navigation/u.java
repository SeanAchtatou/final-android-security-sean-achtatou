package udk.android.reader.view.pdf.navigation;

import android.widget.ImageView;
import udk.android.reader.b.c;
import udk.android.reader.view.pdf.n;

final class u extends Thread {
    private /* synthetic */ PageNavigationView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ int c;
    private final /* synthetic */ ImageView d;
    private final /* synthetic */ int e;
    private final /* synthetic */ float f;

    u(PageNavigationView pageNavigationView, String str, int i, ImageView imageView, int i2, float f2) {
        this.a = pageNavigationView;
        this.b = str;
        this.c = i;
        this.d = imageView;
        this.e = i2;
        this.f = f2;
    }

    public final void run() {
        while (this.a.c.z().equals(this.b) && !this.a.c.g()) {
            try {
                Thread.sleep(400);
                if (!n.b().d() && !this.a.c.r()) {
                    if (this.a.isShown() && this.a.getFirstVisiblePosition() <= this.c && this.a.getLastVisiblePosition() >= this.c && this.d.getDrawable() == null && this.a.c.z().equals(this.b) && !this.a.c.g()) {
                        this.d.post(new a(this, this.d, this.a.c.c(this.e, this.f)));
                        return;
                    }
                    return;
                }
            } catch (Exception e2) {
                c.a(e2.getMessage(), e2);
            }
        }
    }
}
