package udk.android.reader.view.pdf;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import com.unidocs.commonlib.util.c;
import java.io.File;
import udk.android.a.b;

final class z implements DialogInterface.OnClickListener {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ b b;
    private final /* synthetic */ Context c;

    z(PDFView pDFView, b bVar, Context context) {
        this.a = pDFView;
        this.b = bVar;
        this.c = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String a2 = this.b.a();
        try {
            if (!a2.toLowerCase().endsWith(".pdf")) {
                a2 = String.valueOf(a2) + ".pdf";
            }
            String str = a2;
            int i2 = 1;
            while (true) {
                File file = new File(str);
                if (file.exists()) {
                    str = i2 == 1 ? str.replaceAll("\\.pdf$", "_" + i2 + ".pdf") : str.replaceAll("_" + (i2 - 1) + "\\.pdf$", "_" + i2 + ".pdf");
                    i2++;
                } else {
                    c.a(str);
                    file.delete();
                    this.a.b(str);
                    return;
                }
            }
        } catch (Exception e) {
            udk.android.reader.b.c.a((Throwable) e);
            Toast.makeText(this.c, udk.android.reader.b.b.l, 0).show();
        }
    }
}
