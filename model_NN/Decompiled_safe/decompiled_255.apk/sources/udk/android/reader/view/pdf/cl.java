package udk.android.reader.view.pdf;

import android.graphics.PointF;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import udk.android.b.m;
import udk.android.reader.b.a;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.aj;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.ab;
import udk.android.reader.pdf.annotation.e;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.w;
import udk.android.reader.pdf.c;
import udk.android.reader.pdf.x;
import udk.android.reader.view.pdf.PDFView;

public final class cl implements GestureDetector.OnGestureListener {
    private static List p;
    /* access modifiers changed from: private */
    public PDFView a;
    /* access modifiers changed from: private */
    public ar b = ar.a();
    private s c = s.a();
    /* access modifiers changed from: private */
    public bn d = bn.a();
    private GestureDetector e = new GestureDetector(this);
    /* access modifiers changed from: private */
    public hx f = hx.a();
    /* access modifiers changed from: private */
    public PDF g = PDF.a();
    /* access modifiers changed from: private */
    public ic h = ic.a();
    /* access modifiers changed from: private */
    public ZoomService i = ZoomService.a();
    private ce j = ce.a();
    /* access modifiers changed from: private */
    public o k;
    private ae l;
    private boolean m;
    private boolean n;
    /* access modifiers changed from: private */
    public long o;
    private int q;

    cl(PDFView pDFView) {
        p = new ArrayList();
        this.a = pDFView;
        this.k = pDFView.V();
        this.q = m.a(pDFView.getContext(), 16);
    }

    public static void a(Object obj) {
        if (!p.contains(obj)) {
            p.add(obj);
        }
    }

    private boolean a(float f2, float f3) {
        if (!a.an || !this.f.j() || !this.f.l()) {
            return false;
        }
        return !this.g.u() ? (this.f.a + ((float) this.f.e())) - f2 < ((float) this.f.e()) * a.ap && (this.f.b + ((float) this.f.i())) - f3 < ((float) this.f.i()) * a.ap : f2 - this.f.a < ((float) this.f.e()) * a.ap && (this.f.b + ((float) this.f.i())) - f3 < ((float) this.f.i()) * a.ap;
    }

    static /* synthetic */ boolean a(cl clVar, MotionEvent motionEvent) {
        boolean z;
        if (!clVar.g.f()) {
            return true;
        }
        if (clVar.d.b > 1) {
            return false;
        }
        List c2 = aj.a().c(clVar.a.z());
        if (!b.b((Collection) c2)) {
            Iterator it = c2.iterator();
            while (true) {
                if (it.hasNext()) {
                    Annotation annotation = (Annotation) it.next();
                    if (annotation.c(clVar.f.n(), motionEvent.getX() - clVar.f.a, motionEvent.getY() - clVar.f.b)) {
                        clVar.a.a((c) annotation.J(), motionEvent);
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
        } else {
            z = false;
        }
        if (!z) {
            if (!clVar.k.o()) {
                z = false;
            } else {
                clVar.k.v();
                z = true;
            }
        }
        if (!z) {
            if (a.o) {
                PDFView.SmartNavDirection smartNavDirection = null;
                if (!clVar.k.d() && motionEvent.getY() < ((float) clVar.h.b) * a.az) {
                    smartNavDirection = PDFView.SmartNavDirection.TOP;
                } else if (!clVar.k.d() && motionEvent.getY() > ((float) clVar.h.b) * (1.0f - a.az)) {
                    smartNavDirection = PDFView.SmartNavDirection.BOTTOM;
                } else if (motionEvent.getX() < ((float) clVar.h.a) * a.az) {
                    smartNavDirection = PDFView.SmartNavDirection.LEFT;
                } else if (motionEvent.getX() > ((float) clVar.h.a) * (1.0f - a.az)) {
                    smartNavDirection = PDFView.SmartNavDirection.RIGHT;
                }
                if (smartNavDirection != null && (!clVar.f.t() || !clVar.a(motionEvent.getX(), motionEvent.getY()))) {
                    clVar.a.a(smartNavDirection);
                    z = true;
                }
            }
            z = false;
        }
        if (z || !clVar.f.t() || !clVar.a(motionEvent.getX(), motionEvent.getY())) {
            return z;
        }
        return true;
    }

    private PointF b(float f2, float f3) {
        try {
            PointF a2 = this.k.a(this.f.n());
            a2.x = this.f.a + a2.x;
            a2.y = (this.f.b + a2.y) - ((float) (this.q / 2));
            float f4 = (((float) this.q) * 4.0f) / 2.0f;
            if (new RectF(a2.x - (f4 * 2.0f), a2.y - (2.0f * f4), a2.x + f4, f4 + a2.y).contains(f2, f3)) {
                return new PointF(a2.x - f2, (a2.y + ((float) (this.q / 2))) - f3);
            }
            return null;
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Throwable) e2);
            return null;
        }
    }

    public static void b(Object obj) {
        p.remove(obj);
    }

    private PointF c(float f2, float f3) {
        try {
            PointF b2 = this.k.b(this.f.n());
            b2.x = this.f.a + b2.x;
            b2.y = this.f.b + b2.y + ((float) (this.q / 2));
            float f4 = (((float) this.q) * 4.0f) / 2.0f;
            if (new RectF(b2.x - f4, b2.y - f4, b2.x + (f4 * 2.0f), (f4 * 2.0f) + b2.y).contains(f2, f3)) {
                return new PointF(b2.x - f2, (b2.y - ((float) (this.q / 2))) - f3);
            }
            return null;
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Throwable) e2);
            return null;
        }
    }

    public static boolean f() {
        return b.a((Collection) p);
    }

    public final void a() {
        this.m = true;
        this.a.W();
    }

    public final void a(ae aeVar) {
        this.l = aeVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(MotionEvent motionEvent) {
        Annotation c2;
        if (!this.g.f() || b.a((Collection) p)) {
            return false;
        }
        int action = motionEvent.getAction();
        this.d.c = motionEvent.getX(0);
        this.d.d = motionEvent.getY(0);
        if (motionEvent.getPointerCount() > 1) {
            this.d.e = motionEvent.getX(1);
            this.d.f = motionEvent.getY(1);
        }
        int pointerCount = motionEvent.getPointerCount();
        if (pointerCount > this.d.b) {
            this.d.b = pointerCount;
        }
        if (action == 0) {
            this.d.b = pointerCount;
            this.d.a = false;
            this.d.g = motionEvent.getX(0);
            this.d.h = motionEvent.getY(0);
            this.d.k = this.d.g;
            this.d.l = this.d.h;
            this.d.i = Float.MIN_VALUE;
            this.d.j = Float.MIN_VALUE;
            this.d.m = Float.MIN_VALUE;
            this.d.n = Float.MIN_VALUE;
            this.d.e = Float.MIN_VALUE;
            this.d.f = Float.MIN_VALUE;
            if (this.a.T()) {
                this.a.S();
            }
            if (this.c.b() && (c2 = this.c.c()) != null) {
                Annotation.TransformingType a2 = c2.a(new PointF(motionEvent.getX() - this.f.a, motionEvent.getY() - this.f.b), this.f.n());
                if (a2 != null) {
                    if (c2 instanceof w) {
                        new LineAnnotationTransformService(this.a, this, (w) c2, a2).a();
                    } else {
                        new AnnotationTransformService(this.a, this, c2, a2).a();
                    }
                }
                if (c2 instanceof e) {
                    this.a.a(motionEvent, (e) c2);
                }
            }
            if (this.l != null && motionEvent.getPointerCount() == 1) {
                this.l.a(motionEvent);
            }
        } else if (action == 261 && motionEvent.getPointerCount() > 1) {
            this.d.i = motionEvent.getX(1);
            this.d.j = motionEvent.getY(1);
            this.d.m = this.d.i;
            this.d.n = this.d.j;
        } else if (action == 5 && motionEvent.getPointerCount() > 1) {
            this.d.i = motionEvent.getX(0);
            this.d.j = motionEvent.getY(0);
            this.d.m = this.d.i;
            this.d.n = this.d.j;
        } else if (action == 2) {
            if (this.l != null && motionEvent.getPointerCount() == 1) {
                this.l.b(motionEvent);
            }
        } else if (action == 1) {
            this.d.o = false;
            this.d.p = false;
            this.d.q = null;
            if (this.i.f()) {
                this.i.b();
            } else if (this.j.b()) {
                this.j.a(new PointF(motionEvent.getX(), motionEvent.getY()));
            } else if (this.l == null) {
                new cu(this).start();
                new ct(this).start();
            }
            if (this.l != null) {
                this.l.c(motionEvent);
            }
        }
        boolean onTouchEvent = this.l == null ? this.e.onTouchEvent(motionEvent) : true;
        this.d.k = this.d.c;
        this.d.l = this.d.d;
        this.d.m = this.d.e;
        this.d.n = this.d.f;
        return onTouchEvent;
    }

    public final boolean b() {
        return this.m;
    }

    public final void c() {
        this.n = true;
        this.a.W();
    }

    public final boolean d() {
        return this.n;
    }

    public final void e() {
        this.m = false;
        this.n = false;
        if (this.k.d()) {
            this.k.e();
            this.f.p();
        }
        this.a.W();
    }

    public final boolean onDown(MotionEvent motionEvent) {
        this.b.e();
        return true;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (this.d.b > 1 || this.i.f() || this.j.b()) {
            return true;
        }
        if (!this.k.d() || !this.m || Math.abs(f2) <= Math.abs(f3) * 4.0f) {
            if (!this.f.j() || !this.f.l()) {
                if (!((a.R == 2 && (this.f.b > 0.0f || this.f.d() < ((float) this.h.b))) || (a.R == 3 && (this.f.a > 0.0f || this.f.c() < ((float) this.h.a))))) {
                    float f4 = this.m ? 0.0f : f2;
                    float f5 = this.n ? 0.0f : f3;
                    if (!(f4 == 0.0f && f5 == 0.0f)) {
                        this.b.b(f4, f5);
                    }
                }
            } else if (this.k.o()) {
                return true;
            } else {
                if (Math.abs(f2) > Math.abs(f3)) {
                    if (f2 > 0.0f) {
                        if (!this.g.u()) {
                            this.a.n();
                        } else {
                            this.a.o();
                        }
                    } else if (!this.g.u()) {
                        this.a.o();
                    } else {
                        this.a.n();
                    }
                }
            }
        } else if (f2 > 0.0f) {
            this.k.k();
        } else {
            this.k.l();
        }
        return true;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        if (!this.j.b()) {
            if (a.h && this.k.c(motionEvent.getX() - this.f.a, motionEvent.getY() - this.f.b)) {
                this.f.p();
            } else if (this.f.b().contains(motionEvent.getX(), motionEvent.getY())) {
                this.a.a(motionEvent.getX(), motionEvent.getY());
            }
            hg hgVar = new hg();
            hgVar.a = this.g.E();
            hgVar.b = this.g.F();
            hgVar.c = motionEvent;
        }
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (motionEvent2.getPointerCount() != 1 || this.i.f()) {
            if (motionEvent2.getPointerCount() == 2 && this.d.g != Float.MIN_VALUE && this.d.h != Float.MIN_VALUE && this.d.i != Float.MIN_VALUE && this.d.j != Float.MIN_VALUE && !this.j.b() && !this.i.f() && a.Y) {
                this.i.g();
            }
        } else if (this.d.o) {
            this.k.d((motionEvent2.getX(0) - this.f.a) + this.d.q.x, (motionEvent2.getY(0) - this.f.b) + this.d.q.y);
        } else if (this.d.p) {
            this.k.e((motionEvent2.getX(0) - this.f.a) + this.d.q.x, (motionEvent2.getY(0) - this.f.b) + this.d.q.y);
        } else if (!this.f.t()) {
            if ((!this.f.j() || a.R == 3) && !this.m) {
                this.f.a += motionEvent2.getX(0) - this.d.k;
            }
            if ((!this.f.l() || a.R == 2) && !this.n) {
                this.f.b += motionEvent2.getY(0) - this.d.l;
            }
        }
        this.f.a(this.h.a >= this.f.e() || a.R == 3, this.h.b >= this.f.i() || a.R == 2);
        this.f.p();
        return true;
    }

    public final void onShowPress(MotionEvent motionEvent) {
        if (this.k.o()) {
            bn bnVar = this.d;
            PointF c2 = c(motionEvent.getX(), motionEvent.getY());
            bnVar.q = c2;
            if (c2 != null) {
                this.d.o = false;
                this.d.p = true;
                return;
            }
            bn bnVar2 = this.d;
            PointF b2 = b(motionEvent.getX(), motionEvent.getY());
            bnVar2.q = b2;
            if (b2 != null) {
                this.d.o = true;
                this.d.p = false;
            }
        } else if (a(motionEvent.getX(), motionEvent.getY()) && !this.j.b() && !this.i.f() && this.g.x()) {
            this.j.a(true);
        }
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        boolean z;
        Annotation annotation;
        float f2;
        Annotation annotation2;
        this.d.a = true;
        if (!a.aB || System.currentTimeMillis() - this.o >= 300) {
            this.o = System.currentTimeMillis();
            if (!this.g.f()) {
                z = true;
            } else {
                if (this.d.b <= 1 && (a.f || a.g)) {
                    List h2 = this.c.h(this.a.z());
                    if (!b.b((Collection) h2)) {
                        float n2 = this.f.n();
                        float x = motionEvent.getX() - this.f.a;
                        float y = motionEvent.getY() - this.f.b;
                        int size = h2.size() - 1;
                        float f3 = Float.MAX_VALUE;
                        Annotation annotation3 = null;
                        while (true) {
                            if (size >= 0) {
                                annotation = (Annotation) h2.get(size);
                                if (annotation.c(n2, x, y)) {
                                    break;
                                }
                                RectF b2 = annotation.b(n2);
                                float a2 = udk.android.b.c.a(b2.centerX(), b2.centerY(), x, y);
                                if (a2 < f3) {
                                    annotation2 = annotation;
                                    f2 = a2;
                                } else {
                                    f2 = f3;
                                    annotation2 = annotation3;
                                }
                                size--;
                                annotation3 = annotation2;
                                f3 = f2;
                            } else {
                                annotation = null;
                                break;
                            }
                        }
                        if (annotation == null && annotation3 != null && udk.android.b.c.a(annotation3.b(n2), a.aA).contains(x, y)) {
                            annotation = annotation3;
                        }
                        if (annotation != null) {
                            ab abVar = new ab();
                            abVar.a = annotation;
                            this.c.b(abVar);
                            this.c.a(annotation);
                            z = true;
                        } else if (this.c.b()) {
                            this.c.d();
                            z = true;
                        }
                    }
                }
                z = false;
            }
            if (!z) {
                this.a.postDelayed(new cr(this, motionEvent), 300);
            }
        } else {
            this.o = -1;
            if (!a.l) {
                this.i.a(motionEvent.getX(), motionEvent.getY());
            } else {
                int E = this.g.E();
                float x2 = motionEvent.getX() - this.f.a;
                float y2 = motionEvent.getY() - this.f.b;
                cs csVar = new cs(this, E, x2, y2, motionEvent);
                if (this.k.d()) {
                    x a3 = this.k.a(x2, y2);
                    if (a3 == null || a3 == this.k.i()) {
                        this.k.e();
                        this.k.a(E, x2, y2, new cp(this, motionEvent));
                    } else {
                        RectF b3 = a3.d().b(this.f.n());
                        this.k.a(b3.centerX(), b3.centerY(), csVar);
                    }
                } else {
                    this.k.a(x2, y2, csVar);
                }
            }
            hg hgVar = new hg();
            hgVar.a = this.g.E();
            hgVar.b = this.g.F();
            hgVar.c = motionEvent;
        }
        return true;
    }
}
