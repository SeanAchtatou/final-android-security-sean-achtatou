package udk.android.reader.view.contents.web;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import udk.android.reader.C0000R;

final class p implements View.OnClickListener {
    final /* synthetic */ g a;

    p(g gVar) {
        this.a = gVar;
    }

    public final void onClick(View view) {
        Context context = this.a.getContext();
        String[] strArr = {context.getString(C0000R.string.f85), context.getString(C0000R.string.f86)};
        new AlertDialog.Builder(context).setTitle(context.getString(C0000R.string.f89)).setItems(strArr, new x(this, strArr, context)).show();
    }
}
