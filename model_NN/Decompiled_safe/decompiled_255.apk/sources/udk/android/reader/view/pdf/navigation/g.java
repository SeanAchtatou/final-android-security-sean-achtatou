package udk.android.reader.view.pdf.navigation;

import android.widget.TextView;

final class g implements Runnable {
    private /* synthetic */ bb a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ int c;

    g(bb bbVar, TextView textView, int i) {
        this.a = bbVar;
        this.b = textView;
        this.c = i;
    }

    public final void run() {
        this.b.setText(new StringBuilder(String.valueOf(this.c)).toString());
    }
}
