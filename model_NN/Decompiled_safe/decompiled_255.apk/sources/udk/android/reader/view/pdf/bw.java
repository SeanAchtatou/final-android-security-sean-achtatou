package udk.android.reader.view.pdf;

import android.view.MotionEvent;
import udk.android.reader.b.a;

final class bw implements Runnable {
    private /* synthetic */ hn a;
    private final /* synthetic */ MotionEvent b;

    bw(hn hnVar, MotionEvent motionEvent) {
        this.a = hnVar;
        this.b = motionEvent;
    }

    public final void run() {
        if (this.a.j == -1) {
            return;
        }
        if (this.a.k.b > 1) {
            hg hgVar = new hg();
            hgVar.c = this.b;
            hgVar.a = this.a.g.E();
            hgVar.b = this.a.g.F();
            this.a.a.ag();
        } else if (this.b.getY() < ((float) this.a.getHeight()) * a.az) {
            this.a.b();
        } else if (this.b.getY() > ((float) this.a.getHeight()) * (1.0f - a.az)) {
            this.a.c();
        } else if (this.b.getX() < ((float) this.a.getWidth()) * a.az) {
            if (this.a.g.w()) {
                this.a.g.I();
            }
        } else if (this.b.getX() <= ((float) this.a.getWidth()) * (1.0f - a.az)) {
            hg hgVar2 = new hg();
            hgVar2.c = this.b;
            hgVar2.a = this.a.g.E();
            hgVar2.b = this.a.g.F();
            this.a.a.ag();
        } else if (this.a.g.x()) {
            this.a.g.J();
        }
    }
}
