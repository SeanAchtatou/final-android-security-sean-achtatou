package udk.android.reader.view.contents;

import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.unidocs.commonlib.util.b;
import java.io.File;
import java.util.Collection;
import java.util.List;
import udk.android.reader.C0000R;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.contents.FileDirectory;
import udk.android.reader.contents.ai;

public final class j extends BaseExpandableListAdapter {
    private List a;

    public j(List list) {
        this.a = list;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public File getChild(int i, int i2) {
        return ((FileDirectory) this.a.get(i)).getFile(i2);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public FileDirectory getGroup(int i) {
        return (FileDirectory) this.a.get(i);
    }

    public final long getChildId(int i, int i2) {
        return (long) ((i * 1000) + i2);
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        View inflate = view == null ? View.inflate(viewGroup.getContext(), C0000R.layout.content, null) : view;
        File a2 = getChild(i, i2);
        ImageView imageView = (ImageView) inflate.findViewById(C0000R.id.thumbnail);
        String c = a.c(viewGroup.getContext(), a2);
        if (c != null && new File(c).exists()) {
            try {
                imageView.setImageBitmap(BitmapFactory.decodeFile(c));
            } catch (OutOfMemoryError e) {
                c.a(e.getMessage(), e);
                System.gc();
            }
            inflate.setOnClickListener(new b(this, viewGroup, a2, inflate));
            ((TextView) inflate.findViewById(C0000R.id.title)).setText(a2.getName());
            ((TextView) inflate.findViewById(C0000R.id.last_modified)).setText(ai.a(a2));
            ((TextView) inflate.findViewById(C0000R.id.size)).setText(ai.b(a2));
            return inflate;
        }
        imageView.setImageBitmap(BitmapFactory.decodeResource(viewGroup.getContext().getResources(), C0000R.drawable.icon_pdf));
        inflate.setOnClickListener(new b(this, viewGroup, a2, inflate));
        ((TextView) inflate.findViewById(C0000R.id.title)).setText(a2.getName());
        ((TextView) inflate.findViewById(C0000R.id.last_modified)).setText(ai.a(a2));
        ((TextView) inflate.findViewById(C0000R.id.size)).setText(ai.b(a2));
        return inflate;
    }

    public final int getChildrenCount(int i) {
        return ((FileDirectory) this.a.get(i)).size();
    }

    public final int getGroupCount() {
        if (b.a((Collection) this.a)) {
            return this.a.size();
        }
        return 0;
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            View inflate = View.inflate(viewGroup.getContext(), C0000R.layout.fs_directory, null);
            inflate.setBackgroundColor(0);
            view2 = inflate;
        } else {
            view2 = view;
        }
        ((TextView) view2.findViewById(C0000R.id.title)).setText("MyDocuments" + getGroup(i).getDir().getAbsolutePath().replaceAll(a.b(viewGroup.getContext()).getAbsolutePath(), ""));
        return view2;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
