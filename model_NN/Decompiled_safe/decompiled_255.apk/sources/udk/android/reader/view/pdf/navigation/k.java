package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.AdapterView;

final class k implements AdapterView.OnItemSelectedListener {
    final /* synthetic */ PageNavigationView a;

    k(PageNavigationView pageNavigationView) {
        this.a = pageNavigationView;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (view != null) {
            this.a.d = i;
            view.postDelayed(new bc(this, i), 1000);
        }
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
