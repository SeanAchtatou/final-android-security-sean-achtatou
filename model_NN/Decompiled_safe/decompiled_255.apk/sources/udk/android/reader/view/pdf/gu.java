package udk.android.reader.view.pdf;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

final class gu extends BaseAdapter {
    final /* synthetic */ e a;
    /* access modifiers changed from: private */
    public long b;

    public gu(e eVar) {
        this.a = eVar;
    }

    public final int getCount() {
        return this.a.b.l();
    }

    public final /* bridge */ /* synthetic */ Object getItem(int i) {
        return Integer.valueOf(i + 1);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        be beVar = new be(this.a.getContext());
        beVar.setOnClickListener(new hp(this, i));
        beVar.b().setText(new StringBuilder().append(Integer.valueOf(i + 1)).toString());
        new ho(this).start();
        return beVar;
    }
}
