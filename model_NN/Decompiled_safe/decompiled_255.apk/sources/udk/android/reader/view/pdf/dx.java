package udk.android.reader.view.pdf;

import android.app.ProgressDialog;
import android.content.Context;
import com.unidocs.commonlib.util.b;
import udk.android.reader.pdf.ai;

final class dx extends Thread {
    final /* synthetic */ PDFView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ ProgressDialog c;
    private final /* synthetic */ Context d;
    private final /* synthetic */ Runnable e;

    dx(PDFView pDFView, String str, ProgressDialog progressDialog, Context context, Runnable runnable) {
        this.a = pDFView;
        this.b = str;
        this.c = progressDialog;
        this.d = context;
        this.e = runnable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String, java.lang.String, int, float, boolean, float, float, int):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, float, int, float, float, int]
     candidates:
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):void
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String, java.lang.String, int, float, boolean, float, float, int):void */
    public final void run() {
        o V = this.a.V();
        ai t = V.o() ? V.t() : null;
        ai u = V.o() ? V.u() : null;
        int c2 = V.d() ? V.c() : -1;
        String y = this.a.d;
        String z = this.a.e;
        int z2 = this.a.z();
        float B = this.a.B();
        float f = this.a.k.a;
        float f2 = this.a.k.b;
        if (!(b.a(this.b) ? this.a.b.a(this.b) : this.a.b.p())) {
            this.a.post(new cz(this, this.c, this.d));
            return;
        }
        String y2 = this.a.b.y();
        this.a.i();
        this.a.h = new cy(this, this.c, t, u, this.e);
        this.a.a(this.b == null ? y2 : this.b, y, z, z2, B, false, f, f2, c2);
    }
}
