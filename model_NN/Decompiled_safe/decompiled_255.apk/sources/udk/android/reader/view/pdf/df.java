package udk.android.reader.view.pdf;

import com.unidocs.commonlib.util.b;
import udk.android.b.m;

final class df implements Runnable {
    final /* synthetic */ PDFView a;

    df(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void run() {
        if (!this.a.b.okToCopy()) {
            this.a.post(new hr(this));
            return;
        }
        String G = this.a.G();
        if (!b.b(G)) {
            m.a(this.a, G, (String) null);
            this.a.H();
        }
    }
}
