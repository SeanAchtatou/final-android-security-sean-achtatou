package udk.android.reader.view.pdf;

import android.content.Context;
import android.content.Intent;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;

final class fa implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ s b;
    private final /* synthetic */ Annotation c;

    fa(PDFView pDFView, s sVar, Annotation annotation) {
        this.a = pDFView;
        this.b = sVar;
        this.c = annotation;
    }

    public final void run() {
        this.b.d();
        Context context = this.a.getContext();
        Intent intent = new Intent(context, AnnotationMemoActivity.class);
        intent.putExtra("page", this.c.ag());
        intent.putExtra("refNo", this.c.K());
        context.startActivity(intent);
    }
}
