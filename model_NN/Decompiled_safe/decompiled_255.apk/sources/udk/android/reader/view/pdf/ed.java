package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.EditText;

final class ed implements Runnable {
    final /* synthetic */ PDFView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ String f;
    private final /* synthetic */ String g;

    ed(PDFView pDFView, String str, String str2, String str3, String str4, String str5, String str6) {
        this.a = pDFView;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
    }

    public final void run() {
        dc a2 = dc.a();
        PDFView pDFView = this.a;
        fg fgVar = new fg(this);
        String str = this.b;
        String str2 = this.c;
        String str3 = this.d;
        String str4 = this.e;
        String str5 = this.f;
        String str6 = this.g;
        Context context = pDFView.getContext();
        EditText editText = new EditText(context);
        editText.setInputType(2);
        editText.setImeOptions(5);
        editText.setOnEditorActionListener(new bc(a2, context, editText));
        editText.setSingleLine();
        new AlertDialog.Builder(context).setTitle(str).setView(editText).setPositiveButton(str2, new bb(a2, editText, str4, str5, context, pDFView, fgVar, str6)).setNegativeButton(str3, new bg(a2)).show();
        editText.postDelayed(new bh(a2, context, editText), 100);
    }
}
