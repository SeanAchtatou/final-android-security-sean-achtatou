package udk.android.reader.view.pdf.menu;

public final class c {
    private String a;
    private Runnable b;

    public c(String str, Runnable runnable) {
        this.a = str;
        this.b = runnable;
    }

    public final String a() {
        return this.a;
    }

    public final Runnable b() {
        return this.b;
    }
}
