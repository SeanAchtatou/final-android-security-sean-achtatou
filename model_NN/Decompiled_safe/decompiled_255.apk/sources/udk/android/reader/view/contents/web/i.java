package udk.android.reader.view.contents.web;

import android.app.Activity;
import android.content.DialogInterface;

final class i implements DialogInterface.OnClickListener {
    private /* synthetic */ u a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ Runnable c;

    i(u uVar, Activity activity, Runnable runnable) {
        this.a = uVar;
        this.b = activity;
        this.c = runnable;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.b.runOnUiThread(this.c);
    }
}
