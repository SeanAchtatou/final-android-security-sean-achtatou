package udk.android.reader.view.pdf;

import android.graphics.RectF;
import udk.android.b.c;
import udk.android.reader.b.a;

final class gh extends Thread {
    private /* synthetic */ o a;
    private final /* synthetic */ int b;
    private final /* synthetic */ float c;
    private final /* synthetic */ float d;
    private final /* synthetic */ Runnable e;

    gh(o oVar, int i, float f, float f2, Runnable runnable) {
        this.a = oVar;
        this.b = i;
        this.c = f;
        this.d = f2;
        this.e = runnable;
    }

    public final void run() {
        RectF b2 = o.b(this.a, this.b, this.c, this.d);
        this.a.w();
        if (b2 != null && this.b == this.a.b.E()) {
            hx a2 = hx.a();
            RectF a3 = c.a(b2, a.O);
            float min = Math.min(((float) ic.a().a) / a3.width(), ((float) ic.a().b) / a3.height());
            if (Math.abs(min - ZoomService.a().b(this.b)) >= 0.1f) {
                ZoomService.a().a(min, o.a(this.a, this.b, min, a3));
            } else if (!a2.t()) {
                ZoomService a4 = ZoomService.a();
                a4.b((float) (a4.a.a / 2), (float) (a4.a.b / 2));
            }
        } else if (this.e != null) {
            this.e.run();
        }
    }
}
