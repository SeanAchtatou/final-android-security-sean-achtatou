package udk.android.reader.view.pdf;

import android.content.DialogInterface;
import udk.android.reader.pdf.annotation.p;
import udk.android.reader.pdf.c.a;

final class ii implements DialogInterface.OnClickListener {
    private /* synthetic */ fm a;

    ii(fm fmVar) {
        this.a = fmVar;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        String str = p.a[i];
        this.a.a.g.setText(str);
        this.a.a.h.setImageBitmap(a.a().a(str, this.a.a.b.getColor()));
    }
}
