package udk.android.reader.view.contents;

import android.view.View;
import android.widget.CheckBox;
import udk.android.reader.contents.ai;

final class m implements View.OnClickListener {
    private /* synthetic */ c a;
    private final /* synthetic */ CheckBox b;
    private final /* synthetic */ ai c;

    m(c cVar, CheckBox checkBox, ai aiVar) {
        this.a = cVar;
        this.b = checkBox;
        this.c = aiVar;
    }

    public final void onClick(View view) {
        if (this.b.isChecked()) {
            this.a.a.a(this.c);
        } else {
            this.a.a.b(this.c);
        }
    }
}
