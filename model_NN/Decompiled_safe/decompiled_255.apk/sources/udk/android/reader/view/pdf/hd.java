package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.DialogInterface;
import udk.android.reader.b.b;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.s;

final class hd implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ s b;
    private final /* synthetic */ Annotation c;

    hd(PDFView pDFView, s sVar, Annotation annotation) {
        this.a = pDFView;
        this.b = sVar;
        this.c = annotation;
    }

    public final void run() {
        new AlertDialog.Builder(this.a.getContext()).setTitle(b.w).setMessage(b.P).setPositiveButton(b.r, new bi(this, this.b, this.c)).setNegativeButton(b.s, (DialogInterface.OnClickListener) null).show();
    }
}
