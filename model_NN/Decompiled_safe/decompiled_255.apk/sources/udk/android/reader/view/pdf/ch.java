package udk.android.reader.view.pdf;

import java.util.TimerTask;
import udk.android.reader.b.a;
import udk.android.reader.b.c;

final class ch extends TimerTask {
    private /* synthetic */ as a;

    ch(as asVar) {
        this.a = asVar;
    }

    public final void run() {
        try {
            this.a.g();
        } catch (Throwable th) {
            c.a(th);
            if (a.am) {
                if (System.currentTimeMillis() - this.a.K < 200) {
                    as asVar = this.a;
                    asVar.J = asVar.J + 1;
                    if (this.a.J > 3) {
                        this.a.k.f(this.a.k.E(), this.a.k.F() - 0.001f);
                    }
                } else {
                    this.a.J = 1;
                }
                this.a.K = System.currentTimeMillis();
            }
            if (a.b) {
                System.exit(0);
            }
        }
    }
}
