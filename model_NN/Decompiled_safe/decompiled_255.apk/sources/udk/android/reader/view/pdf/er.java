package udk.android.reader.view.pdf;

import udk.android.reader.pdf.annotation.a;
import udk.android.reader.pdf.annotation.s;

final class er implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ a b;
    private final /* synthetic */ s c;

    er(PDFView pDFView, a aVar, s sVar) {
        this.a = pDFView;
        this.b = aVar;
        this.c = sVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, udk.android.reader.pdf.annotation.s, udk.android.reader.pdf.annotation.a, boolean):void
     arg types: [udk.android.reader.view.pdf.PDFView, udk.android.reader.pdf.annotation.s, udk.android.reader.pdf.annotation.a, int]
     candidates:
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, udk.android.reader.pdf.annotation.s, udk.android.reader.pdf.annotation.q, java.lang.Runnable):void
      udk.android.reader.view.pdf.PDFView.a(java.io.InputStream, java.lang.String, java.lang.String, int):void
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String, java.lang.String, int):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, udk.android.reader.pdf.annotation.s, udk.android.reader.pdf.annotation.a, boolean):void */
    public final void run() {
        this.b.c();
        if (this.b.b()) {
            PDFView.a(this.a, this.c, this.b, true);
        } else {
            this.a.K.a();
        }
    }
}
