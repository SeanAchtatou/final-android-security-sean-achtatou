package udk.android.reader.view.pdf;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.EditText;
import udk.android.reader.b.c;

final class hw extends EditText {
    private /* synthetic */ co a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    hw(co coVar, Context context) {
        super(context);
        this.a = coVar;
    }

    public final void onEndBatchEdit() {
        super.onEndBatchEdit();
        try {
            this.a.b.a(getText().toString());
        } catch (Exception e) {
            c.a((Throwable) e);
        }
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        boolean onKeyUp = super.onKeyUp(i, keyEvent);
        try {
            this.a.b.a(getText().toString());
        } catch (Exception e) {
            c.a((Throwable) e);
        }
        return onKeyUp;
    }
}
