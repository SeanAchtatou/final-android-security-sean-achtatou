package udk.android.reader.view.pdf;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ar;

public final class n extends Thread implements ar, b {
    private static n a;
    private PDF b = PDF.a();
    private hx c;
    private ZoomService d;
    private boolean e;
    private boolean f;
    private Object g;
    private Object h;
    private Object i;
    private boolean j;
    private long k;
    private Map l;
    private Set m;
    private ez n;
    private ez o;
    private ez p;
    private int q;
    private boolean r;
    private String s;

    private n() {
        this.b.a(this);
        this.c = hx.a();
        this.d = ZoomService.a();
        this.d.a(this);
        this.g = new Object();
        this.l = new HashMap();
        this.m = new TreeSet();
        this.h = new Object();
        this.i = new Object();
    }

    public static n b() {
        if (a == null) {
            n nVar = new n();
            a = nVar;
            nVar.setDaemon(true);
            a.start();
        }
        return a;
    }

    private static String c(int i2, float f2) {
        return "#" + i2 + "#" + f2;
    }

    private ez d(int i2, float f2) {
        ez ezVar = new ez();
        ezVar.a = 1;
        ezVar.b = i2;
        ezVar.c = f2;
        this.p = ezVar;
        Bitmap c2 = this.b.c(i2, f2);
        this.p = null;
        if (c2 == null) {
            return null;
        }
        hh hhVar = new hh();
        hhVar.c = 0;
        hhVar.d = 0;
        hhVar.b = f2;
        hhVar.e = c2;
        hhVar.a = i2;
        ezVar.a(hhVar);
        return ezVar;
    }

    private boolean i() {
        int E = this.b.E();
        if (!c(E)) {
            return true;
        }
        if (a.ah && !this.c.s()) {
            return false;
        }
        if (!a.ag || this.c.t()) {
            return (this.b.x() && !c(E + 1)) || (this.b.w() && !c(E - 1));
        }
        return false;
    }

    private void j() {
        synchronized (this.i) {
            if (this.n != null) {
                if (this.o == null || this.o.b != this.n.b || this.o.c != this.n.c || !this.o.j.equals(this.n.j) || !this.o.i.equals(this.n.i)) {
                    this.o = new ez();
                    this.o.a = 2;
                    this.o.b = this.n.b;
                    this.o.c = this.n.c;
                    this.o.j = this.n.j;
                    this.o.i = this.n.i;
                    this.o.d = this.n.d;
                    this.o.e = this.n.e;
                }
                Rect d2 = this.n.d();
                int i2 = 0;
                while (i2 < this.o.a()) {
                    hh a2 = this.o.a(i2);
                    if (!Rect.intersects(d2, new Rect(a2.c, a2.d, a2.c + a2.e.getWidth(), a2.e.getHeight() + a2.d))) {
                        this.o.b(i2);
                        i2--;
                    }
                    i2++;
                }
            }
        }
    }

    private void k() {
        int E = this.b.E() + 2;
        int i2 = (E - 1) + a.aj;
        while (E <= i2) {
            if (!this.m.contains(Integer.valueOf(E))) {
                if (this.b.f(E)) {
                    d(E, this.c.b(E));
                }
                this.m.add(Integer.valueOf(E));
                return;
            }
            E++;
        }
    }

    private void l() {
        if (this.b.r()) {
            c.a("## CANCEL GENERATING BITMAP");
            this.b.abortRendering();
        }
    }

    public final ez a(int i2) {
        return (ez) this.l.get(Integer.valueOf(i2));
    }

    public final ez a(int i2, float f2, Rect rect, Point point, Point point2) {
        ez ezVar = new ez();
        ezVar.a = 2;
        ezVar.b = i2;
        ezVar.c = f2;
        ezVar.d = this.b.a(i2, f2);
        ezVar.e = this.b.b(i2, f2);
        ezVar.j = point;
        ezVar.i = point2;
        ezVar.g = rect;
        if (this.o != null && this.o.b == i2 && this.o.c == f2) {
            int a2 = this.o.a();
            Rect d2 = ezVar.d();
            int width = d2.width() / ezVar.j.x;
            if (d2.width() % ezVar.j.x > 0) {
                width++;
            }
            int height = d2.height() / ezVar.j.y;
            if (a2 == (d2.height() % ezVar.j.y > 0 ? height + 1 : height) * width && this.o.e().contains(rect)) {
                this.o.f = true;
                return this.o;
            }
        }
        if (this.o != null) {
            this.o.f = false;
        }
        if (!this.b.f()) {
            this.n = null;
            if (this.o != null) {
                this.o.f = false;
            }
            return this.o;
        } else if (this.d.f()) {
            this.n = null;
            if (this.o != null) {
                this.o.f = false;
            }
            return this.o;
        } else {
            if (this.p != null) {
                Rect d3 = ezVar.d();
                if ((this.p.a == 2 && !(this.p.b == i2 && this.p.c == f2 && Rect.intersects(d3, this.p.h))) || (!this.r && this.p.a == 1)) {
                    c.a("## CALL CANCEL GENERATE FROM GET TILES ASYNC");
                    l();
                }
            }
            this.n = ezVar;
            j();
            if (this.o != null) {
                this.o.f = false;
            }
            synchronized (this.g) {
                try {
                    this.g.notify();
                } catch (Exception e2) {
                }
            }
            return this.o;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return null;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final udk.android.reader.view.pdf.ez a(int r5, boolean r6) {
        /*
            r4 = this;
            r3 = 0
            java.lang.Object r1 = r4.h
            monitor-enter(r1)
            udk.android.reader.pdf.PDF r0 = r4.b     // Catch:{ all -> 0x0027 }
            boolean r0 = r0.f()     // Catch:{ all -> 0x0027 }
            if (r0 == 0) goto L_0x0014
            udk.android.reader.pdf.PDF r0 = r4.b     // Catch:{ all -> 0x0027 }
            boolean r0 = r0.g(r5)     // Catch:{ all -> 0x0027 }
            if (r0 != 0) goto L_0x0017
        L_0x0014:
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            r0 = r3
        L_0x0016:
            return r0
        L_0x0017:
            java.util.Map r0 = r4.l     // Catch:{ all -> 0x0027 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0027 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0027 }
            udk.android.reader.view.pdf.ez r0 = (udk.android.reader.view.pdf.ez) r0     // Catch:{ all -> 0x0027 }
            if (r0 == 0) goto L_0x002a
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            goto L_0x0016
        L_0x0027:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            throw r0
        L_0x002a:
            udk.android.reader.view.pdf.ZoomService r0 = r4.d     // Catch:{ all -> 0x0027 }
            boolean r0 = r0.f()     // Catch:{ all -> 0x0027 }
            if (r0 == 0) goto L_0x0035
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            r0 = r3
            goto L_0x0016
        L_0x0035:
            r4.q = r5     // Catch:{ all -> 0x0027 }
            udk.android.reader.view.pdf.ez r0 = r4.p     // Catch:{ all -> 0x0027 }
            if (r0 == 0) goto L_0x0063
            if (r6 == 0) goto L_0x0063
            udk.android.reader.view.pdf.ez r0 = r4.p     // Catch:{ all -> 0x0027 }
            int r0 = r0.a     // Catch:{ all -> 0x0027 }
            r2 = 1
            if (r0 != r2) goto L_0x0058
            udk.android.reader.view.pdf.ez r0 = r4.p     // Catch:{ all -> 0x0027 }
            int r0 = r0.b     // Catch:{ all -> 0x0027 }
            if (r0 != r5) goto L_0x0058
            udk.android.reader.view.pdf.ez r0 = r4.p     // Catch:{ all -> 0x0027 }
            float r0 = r0.c     // Catch:{ all -> 0x0027 }
            udk.android.reader.view.pdf.hx r2 = r4.c     // Catch:{ all -> 0x0027 }
            float r2 = r2.b(r5)     // Catch:{ all -> 0x0027 }
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0060
        L_0x0058:
            java.lang.String r0 = "## CALL CANCEL GENERATE FROM BASIC ASYNC"
            udk.android.reader.b.c.a(r0)     // Catch:{ all -> 0x0027 }
            r4.l()     // Catch:{ all -> 0x0027 }
        L_0x0060:
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            r0 = r3
            goto L_0x0016
        L_0x0063:
            java.lang.Object r0 = r4.g     // Catch:{ all -> 0x0027 }
            monitor-enter(r0)     // Catch:{ all -> 0x0027 }
            java.lang.Object r2 = r4.g     // Catch:{ Exception -> 0x0070 }
            r2.notify()     // Catch:{ Exception -> 0x0070 }
        L_0x006b:
            monitor-exit(r0)     // Catch:{ all -> 0x006d }
            goto L_0x0060
        L_0x006d:
            r2 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x006d }
            throw r2     // Catch:{ all -> 0x0027 }
        L_0x0070:
            r2 = move-exception
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.pdf.n.a(int, boolean):udk.android.reader.view.pdf.ez");
    }

    public final void a(ah ahVar) {
    }

    public final void a(bm bmVar) {
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i2, float f2) {
        return c(i2, f2).equals(this.s);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.n.a(int, boolean):udk.android.reader.view.pdf.ez
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.n.a(int, float):boolean
      udk.android.reader.view.pdf.n.a(int, boolean):udk.android.reader.view.pdf.ez */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final udk.android.reader.view.pdf.ez b(int r5) {
        /*
            r4 = this;
            r3 = 0
            java.lang.Object r0 = r4.h
            monitor-enter(r0)
            r1 = 1
            r4.r = r1     // Catch:{ all -> 0x0079 }
            r1 = 0
            r4.n = r1     // Catch:{ all -> 0x0079 }
            r1 = 0
            r4.o = r1     // Catch:{ all -> 0x0079 }
        L_0x000d:
            udk.android.reader.pdf.PDF r1 = r4.b     // Catch:{ all -> 0x0079 }
            boolean r1 = r1.f()     // Catch:{ all -> 0x0079 }
            if (r1 == 0) goto L_0x0025
            udk.android.reader.view.pdf.hx r1 = r4.c     // Catch:{ all -> 0x0079 }
            boolean r1 = r1.s()     // Catch:{ all -> 0x0079 }
            if (r1 == 0) goto L_0x0025
            udk.android.reader.pdf.PDF r1 = r4.b     // Catch:{ all -> 0x0079 }
            boolean r1 = r1.g(r5)     // Catch:{ all -> 0x0079 }
            if (r1 != 0) goto L_0x002b
        L_0x0025:
            r1 = 0
            r4.r = r1     // Catch:{ all -> 0x0079 }
            monitor-exit(r0)     // Catch:{ all -> 0x0079 }
            r0 = r3
        L_0x002a:
            return r0
        L_0x002b:
            boolean r1 = udk.android.reader.b.a.ag     // Catch:{ all -> 0x0079 }
            if (r1 == 0) goto L_0x0045
            udk.android.reader.view.pdf.hx r1 = r4.c     // Catch:{ all -> 0x0079 }
            boolean r1 = r1.t()     // Catch:{ all -> 0x0079 }
            if (r1 != 0) goto L_0x0045
            udk.android.reader.pdf.PDF r1 = r4.b     // Catch:{ all -> 0x0079 }
            int r1 = r1.E()     // Catch:{ all -> 0x0079 }
            if (r1 == r5) goto L_0x0045
            r1 = 0
            r4.r = r1     // Catch:{ all -> 0x0079 }
            monitor-exit(r0)     // Catch:{ all -> 0x0079 }
            r0 = r3
            goto L_0x002a
        L_0x0045:
            r1 = 1
            udk.android.reader.view.pdf.ez r1 = r4.a(r5, r1)     // Catch:{ all -> 0x0079 }
            if (r1 == 0) goto L_0x0052
            r2 = 0
            r4.r = r2     // Catch:{ all -> 0x0079 }
            monitor-exit(r0)     // Catch:{ all -> 0x0079 }
            r0 = r1
            goto L_0x002a
        L_0x0052:
            udk.android.reader.view.pdf.hx r1 = r4.c     // Catch:{ all -> 0x0079 }
            boolean r1 = r1.s()     // Catch:{ all -> 0x0079 }
            if (r1 != 0) goto L_0x0060
            r1 = 0
            r4.r = r1     // Catch:{ all -> 0x0079 }
            monitor-exit(r0)     // Catch:{ all -> 0x0079 }
            r0 = r3
            goto L_0x002a
        L_0x0060:
            udk.android.reader.view.pdf.ZoomService r1 = r4.d     // Catch:{ all -> 0x0079 }
            boolean r1 = r1.f()     // Catch:{ all -> 0x0079 }
            if (r1 == 0) goto L_0x006e
            r1 = 0
            r4.r = r1     // Catch:{ all -> 0x0079 }
            monitor-exit(r0)     // Catch:{ all -> 0x0079 }
            r0 = r3
            goto L_0x002a
        L_0x006e:
            r1 = 10
            java.lang.Thread.sleep(r1)     // Catch:{ Exception -> 0x0074 }
            goto L_0x000d
        L_0x0074:
            r1 = move-exception
            udk.android.reader.b.c.a(r1)     // Catch:{ all -> 0x0079 }
            goto L_0x000d
        L_0x0079:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0079 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.pdf.n.b(int):udk.android.reader.view.pdf.ez");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(int r11, float r12) {
        /*
            r10 = this;
            java.lang.String r1 = c(r11, r12)     // Catch:{ Throwable -> 0x005e }
            java.lang.String r2 = r10.s     // Catch:{ Throwable -> 0x005e }
            boolean r2 = r1.equals(r2)     // Catch:{ Throwable -> 0x005e }
            if (r2 == 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            r10.s = r1     // Catch:{ Throwable -> 0x005e }
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            if (r1 == 0) goto L_0x0031
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            int r1 = r1.b     // Catch:{ Throwable -> 0x005e }
            if (r1 != r11) goto L_0x0031
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            float r1 = r1.c     // Catch:{ Throwable -> 0x005e }
            int r1 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r1 != 0) goto L_0x0031
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            java.util.List r1 = r1.b()     // Catch:{ Throwable -> 0x005e }
            java.util.Iterator r9 = r1.iterator()     // Catch:{ Throwable -> 0x005e }
        L_0x002b:
            boolean r1 = r9.hasNext()     // Catch:{ Throwable -> 0x005e }
            if (r1 != 0) goto L_0x0063
        L_0x0031:
            java.util.Map r1 = r10.l     // Catch:{ OutOfMemoryError -> 0x00df }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ OutOfMemoryError -> 0x00df }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ OutOfMemoryError -> 0x00df }
            udk.android.reader.view.pdf.ez r1 = (udk.android.reader.view.pdf.ez) r1     // Catch:{ OutOfMemoryError -> 0x00df }
            if (r1 == 0) goto L_0x0050
            r2 = 0
            udk.android.reader.view.pdf.hh r1 = r1.a(r2)     // Catch:{ OutOfMemoryError -> 0x00df }
            udk.android.reader.pdf.PDF r2 = r10.b     // Catch:{ OutOfMemoryError -> 0x00df }
            float r3 = r1.b     // Catch:{ OutOfMemoryError -> 0x00df }
            android.graphics.Bitmap r2 = r2.c(r11, r3)     // Catch:{ OutOfMemoryError -> 0x00df }
            if (r2 == 0) goto L_0x00d4
            r1.e = r2     // Catch:{ OutOfMemoryError -> 0x00df }
        L_0x0050:
            udk.android.reader.view.pdf.hx r1 = r10.c     // Catch:{ OutOfMemoryError -> 0x00df }
            r1.p()     // Catch:{ OutOfMemoryError -> 0x00df }
        L_0x0055:
            r1 = 0
            r10.s = r1     // Catch:{ Throwable -> 0x005e }
            udk.android.reader.view.pdf.hx r1 = r10.c     // Catch:{ Throwable -> 0x005e }
            r1.p()     // Catch:{ Throwable -> 0x005e }
            goto L_0x000c
        L_0x005e:
            r1 = move-exception
            udk.android.reader.b.c.a(r1)
            goto L_0x000c
        L_0x0063:
            java.lang.Object r1 = r9.next()     // Catch:{ Throwable -> 0x005e }
            r0 = r1
            udk.android.reader.view.pdf.hh r0 = (udk.android.reader.view.pdf.hh) r0     // Catch:{ Throwable -> 0x005e }
            r8 = r0
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            if (r1 == 0) goto L_0x002b
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            boolean r1 = r1.c(r8)     // Catch:{ Throwable -> 0x005e }
            if (r1 == 0) goto L_0x002b
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            int r1 = r1.b     // Catch:{ Throwable -> 0x005e }
            if (r1 != r11) goto L_0x002b
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            float r1 = r1.c     // Catch:{ Throwable -> 0x005e }
            int r1 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r1 != 0) goto L_0x002b
            android.graphics.Bitmap r1 = r8.e     // Catch:{ OutOfMemoryError -> 0x00ab }
            int r6 = r1.getWidth()     // Catch:{ OutOfMemoryError -> 0x00ab }
            android.graphics.Bitmap r1 = r8.e     // Catch:{ OutOfMemoryError -> 0x00ab }
            int r7 = r1.getHeight()     // Catch:{ OutOfMemoryError -> 0x00ab }
            udk.android.reader.pdf.PDF r1 = r10.b     // Catch:{ OutOfMemoryError -> 0x00ab }
            udk.android.reader.view.pdf.ez r2 = r10.o     // Catch:{ OutOfMemoryError -> 0x00ab }
            int r2 = r2.b     // Catch:{ OutOfMemoryError -> 0x00ab }
            float r3 = r8.b     // Catch:{ OutOfMemoryError -> 0x00ab }
            int r4 = r8.c     // Catch:{ OutOfMemoryError -> 0x00ab }
            int r5 = r8.d     // Catch:{ OutOfMemoryError -> 0x00ab }
            android.graphics.Bitmap r1 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ OutOfMemoryError -> 0x00ab }
            if (r1 == 0) goto L_0x00c6
            r8.e = r1     // Catch:{ OutOfMemoryError -> 0x00ab }
        L_0x00a5:
            udk.android.reader.view.pdf.hx r1 = r10.c     // Catch:{ OutOfMemoryError -> 0x00ab }
            r1.p()     // Catch:{ OutOfMemoryError -> 0x00ab }
            goto L_0x002b
        L_0x00ab:
            r1 = move-exception
            udk.android.reader.b.c.a(r1)     // Catch:{ Throwable -> 0x005e }
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            boolean r1 = r1.b(r8)     // Catch:{ Throwable -> 0x005e }
            if (r1 == 0) goto L_0x00bc
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ Throwable -> 0x005e }
            r2 = 0
            r1.f = r2     // Catch:{ Throwable -> 0x005e }
        L_0x00bc:
            java.lang.System.gc()     // Catch:{ Throwable -> 0x005e }
            udk.android.reader.view.pdf.hx r1 = r10.c     // Catch:{ Throwable -> 0x005e }
            r1.p()     // Catch:{ Throwable -> 0x005e }
            goto L_0x002b
        L_0x00c6:
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ OutOfMemoryError -> 0x00ab }
            boolean r1 = r1.b(r8)     // Catch:{ OutOfMemoryError -> 0x00ab }
            if (r1 == 0) goto L_0x00a5
            udk.android.reader.view.pdf.ez r1 = r10.o     // Catch:{ OutOfMemoryError -> 0x00ab }
            r2 = 0
            r1.f = r2     // Catch:{ OutOfMemoryError -> 0x00ab }
            goto L_0x00a5
        L_0x00d4:
            java.util.Map r1 = r10.l     // Catch:{ OutOfMemoryError -> 0x00df }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ OutOfMemoryError -> 0x00df }
            r1.remove(r2)     // Catch:{ OutOfMemoryError -> 0x00df }
            goto L_0x0050
        L_0x00df:
            r1 = move-exception
            udk.android.reader.b.c.a(r1)     // Catch:{ Throwable -> 0x005e }
            java.util.Map r1 = r10.l     // Catch:{ Throwable -> 0x005e }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ Throwable -> 0x005e }
            r1.remove(r2)     // Catch:{ Throwable -> 0x005e }
            java.lang.System.gc()     // Catch:{ Throwable -> 0x005e }
            udk.android.reader.view.pdf.hx r1 = r10.c     // Catch:{ Throwable -> 0x005e }
            r1.p()     // Catch:{ Throwable -> 0x005e }
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.pdf.n.b(int, float):void");
    }

    public final void b(ah ahVar) {
        if (ahVar.c && this.b.f(ahVar.a)) {
            int i2 = ahVar.a;
            ez ezVar = (ez) this.l.get(Integer.valueOf(i2));
            ez ezVar2 = (ez) this.l.get(Integer.valueOf(i2 + 1));
            ez ezVar3 = (ez) this.l.get(Integer.valueOf(i2 - 1));
            this.l.clear();
            if (!a.ah || this.c.s()) {
                if (ezVar != null) {
                    this.l.put(Integer.valueOf(i2), ezVar);
                }
                if (ezVar2 != null && this.b.x()) {
                    this.l.put(Integer.valueOf(i2 + 1), ezVar2);
                }
                if (ezVar3 != null && this.b.w()) {
                    this.l.put(Integer.valueOf(i2 - 1), ezVar3);
                }
            }
            int i3 = ahVar.a;
            ArrayList arrayList = new ArrayList();
            for (Integer num : this.m) {
                if (num.intValue() > i3 + 1 && num.intValue() <= i3 + 1 + a.aj) {
                    arrayList.add(num);
                }
            }
            this.m.clear();
            this.m.addAll(arrayList);
            synchronized (this.g) {
                try {
                    this.g.notify();
                } catch (Exception e2) {
                    c.a((Throwable) e2);
                }
            }
            return;
        }
        return;
    }

    public final void b(bm bmVar) {
        if (this.b.F() != bmVar.d && this.c.w() >= bmVar.d) {
            this.o = null;
        }
    }

    public final void b_() {
        if (a.ah && !this.c.s()) {
            this.l.clear();
            this.l.put(Integer.valueOf(this.b.E()), (ez) this.l.get(Integer.valueOf(this.b.E())));
        }
        if (this.d.f() && !this.c.s()) {
            this.n = null;
            c.a("## CALL CANCEL GENERATE FROM ON ZOOM ACTIVATED");
            l();
        }
    }

    public final void c() {
        if (this.p != null && this.p.a == 1) {
            c.a("## CALL CANCEL GENERATE FROM CLEAR BASIC");
            l();
        }
        this.l.clear();
    }

    public final boolean c(int i2) {
        return this.l.get(Integer.valueOf(i2)) != null;
    }

    public final boolean d() {
        return e() || i();
    }

    public final boolean e() {
        return this.n != null && !this.c.s();
    }

    public final void f() {
    }

    public final void g() {
        this.l = new HashMap();
        this.m = new TreeSet();
        this.f = true;
    }

    public final void h() {
        this.m.clear();
        this.l.clear();
        this.f = false;
        this.n = null;
        this.o = null;
        long j2 = this.k;
        while (!this.j && j2 == this.k) {
            try {
                c.a("## CALL CANCEL GENERATE FROM ON CLOSE");
                l();
                Thread.sleep(100);
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
    }

    public final void run() {
        ez d2;
        Bitmap bitmap;
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5;
        this.e = true;
        while (this.e) {
            this.k = System.currentTimeMillis();
            if (this.f && !this.d.f()) {
                if (e()) {
                    try {
                        j();
                        Rect d3 = this.n.d();
                        int width = d3.width() / this.n.j.x;
                        if (d3.width() % this.n.j.x > 0) {
                            width++;
                        }
                        int height = d3.height() / this.n.j.y;
                        if (d3.height() % this.n.j.y > 0) {
                            height++;
                        }
                        ArrayList<Rect> arrayList = new ArrayList<>();
                        Rect rect = null;
                        int i6 = 0;
                        int i7 = 0;
                        while (i7 < width) {
                            Rect rect2 = rect;
                            int i8 = i6;
                            for (int i9 = 0; i9 < height; i9++) {
                                if (d3.left != 0 || this.o.i.x <= 0) {
                                    int i10 = d3.left + (this.n.j.x * i7);
                                    if (this.n.j.x + i10 > this.n.d) {
                                        i2 = i10;
                                        i3 = this.n.d - i10;
                                    } else {
                                        i2 = i10;
                                        i3 = this.n.j.x;
                                    }
                                } else if (i7 == 0) {
                                    i2 = 0;
                                    i3 = this.n.i.x;
                                } else {
                                    int i11 = this.n.i.x + ((i7 - 1) * this.n.i.x);
                                    i2 = i11;
                                    i3 = this.n.j.x + i11 > this.n.d ? this.n.d - i11 : this.n.j.x;
                                }
                                if (d3.top != 0 || this.o.i.y <= 0) {
                                    int i12 = d3.top + (this.n.j.y * i9);
                                    if (this.n.j.y + i12 > this.n.e) {
                                        i4 = i12;
                                        i5 = this.n.e - i12;
                                    } else {
                                        i4 = i12;
                                        i5 = this.n.j.y;
                                    }
                                } else if (i9 == 0) {
                                    i4 = 0;
                                    i5 = this.n.i.y;
                                } else {
                                    int i13 = this.n.i.y + ((i9 - 1) * this.n.i.y);
                                    i4 = i13;
                                    i5 = this.n.j.y + i13 > this.n.e ? this.n.e - i13 : this.n.j.y;
                                }
                                if (this.o.a(i2, i4) == null) {
                                    Rect rect3 = new Rect(this.n.g);
                                    Rect rect4 = new Rect(i2, i4, i3 + i2, i5 + i4);
                                    rect3.intersect(rect4);
                                    int width2 = rect3.width() * rect3.height();
                                    if (width2 > i8) {
                                        if (rect2 != null) {
                                            arrayList.add(rect2);
                                        }
                                        rect2 = rect4;
                                        i8 = width2;
                                    } else {
                                        arrayList.add(rect4);
                                    }
                                }
                            }
                            i7++;
                            rect = rect2;
                            i6 = i8;
                        }
                        if (a.al) {
                            for (Rect rect5 : arrayList) {
                                Bitmap b2 = this.b.b(this.n.b, this.n.c, rect5.left, rect5.top, rect5.width(), rect5.height());
                                if (b2 != null) {
                                    hh hhVar = new hh();
                                    hhVar.c = rect5.left;
                                    hhVar.d = rect5.top;
                                    hhVar.e = b2;
                                    hhVar.b = this.n.c;
                                    hhVar.a = this.n.b;
                                    this.o.a(hhVar);
                                }
                            }
                        }
                        float f2 = this.n.c;
                        int i14 = this.n.b;
                        if (i6 > 0) {
                            int i15 = rect.left;
                            int i16 = rect.top;
                            int width3 = rect.width();
                            int height2 = rect.height();
                            ez ezVar = new ez();
                            ezVar.a = 2;
                            ezVar.b = i14;
                            ezVar.c = f2;
                            ezVar.h = new Rect(i15, i16, i15 + width3, i16 + height2);
                            this.p = ezVar;
                            bitmap = this.b.a(i14, f2, i15, i16, width3, height2);
                            this.p = null;
                            z = true;
                        } else {
                            bitmap = null;
                            z = false;
                        }
                        if (bitmap != null && this.n != null && f2 == this.n.c && i14 == this.n.b && Rect.intersects(this.n.d(), new Rect(rect.left, rect.top, rect.left + bitmap.getWidth(), rect.top + bitmap.getHeight()))) {
                            hh hhVar2 = new hh();
                            hhVar2.c = rect.left;
                            hhVar2.d = rect.top;
                            hhVar2.e = bitmap;
                            hhVar2.b = f2;
                            hhVar2.a = i14;
                            this.o.a(hhVar2);
                        }
                        if (!z) {
                            synchronized (this.i) {
                                this.n = null;
                            }
                        }
                    } catch (Exception e2) {
                        c.a((Throwable) e2);
                    }
                } else if (i()) {
                    int i17 = 0;
                    if (this.q > 0) {
                        if (this.b.g(this.q) && !c(this.q)) {
                            i17 = this.q;
                        }
                        this.q = 0;
                    }
                    if (i17 == 0) {
                        if (!c(this.b.E())) {
                            i17 = this.b.E();
                        } else if (!a.ag || this.c.t()) {
                            if (this.b.x() && !c(this.b.E() + 1)) {
                                i17 = this.b.E() + 1;
                            } else if (this.b.w() && !c(this.b.E() - 1)) {
                                i17 = this.b.E() - 1;
                            }
                        }
                    }
                    float b3 = this.c.b(i17);
                    if (b3 > 0.0f && this.b.g(i17) && (d2 = d(i17, b3)) != null && this.f && this.b.g(i17)) {
                        d2.f = true;
                        this.l.put(Integer.valueOf(i17), d2);
                    }
                } else {
                    if (this.c.s() && this.m.size() < a.aj && a.ai) {
                        k();
                    }
                }
            }
            this.j = true;
            synchronized (this.g) {
                try {
                    if (this.e) {
                        this.g.wait(3000);
                    }
                } catch (Exception e3) {
                    c.a((Throwable) e3);
                }
            }
            this.j = false;
        }
    }
}
