package udk.android.reader.view.pdf.navigation;

import android.view.View;
import android.widget.AdapterView;

final class z implements AdapterView.OnItemSelectedListener {
    final /* synthetic */ SearchNavigationView a;

    z(SearchNavigationView searchNavigationView) {
        this.a = searchNavigationView;
    }

    public final void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (view != null && !this.a.c.d()) {
            this.a.f = i;
            view.postDelayed(new e(this, i), 2000);
        }
    }

    public final void onNothingSelected(AdapterView adapterView) {
    }
}
