package udk.android.reader.view.pdf;

import android.view.MotionEvent;
import udk.android.b.f;
import udk.android.reader.pdf.annotation.a;
import udk.android.reader.pdf.annotation.s;

final class cw implements ae {
    final /* synthetic */ el a;
    private final /* synthetic */ a b;
    private final /* synthetic */ f c;
    private final /* synthetic */ s d;

    cw(el elVar, a aVar, f fVar, s sVar) {
        this.a = elVar;
        this.b = aVar;
        this.c = fVar;
        this.d = sVar;
    }

    public final void a(MotionEvent motionEvent) {
        this.b.a(motionEvent.getX() - this.a.a.k.a, motionEvent.getY() - this.a.a.k.b, this.a.a.B());
        this.c.a = true;
    }

    public final void b(MotionEvent motionEvent) {
        this.b.b(motionEvent.getX() - this.a.a.k.a, motionEvent.getY() - this.a.a.k.b, this.a.a.B());
        this.a.a.k.p();
    }

    public final void c(MotionEvent motionEvent) {
        this.b.a();
        this.a.a.k.p();
        this.c.a = false;
        this.a.a.postDelayed(new av(this, this.c, this.d, this.b), 500);
    }
}
