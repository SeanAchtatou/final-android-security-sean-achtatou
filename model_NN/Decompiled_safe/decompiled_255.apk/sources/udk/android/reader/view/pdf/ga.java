package udk.android.reader.view.pdf;

import android.view.View;
import udk.android.reader.view.pdf.PDFView;

final class ga implements View.OnClickListener {
    private /* synthetic */ PDFView a;

    ga(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void onClick(View view) {
        if (this.a.F() == PDFView.ViewMode.PDF) {
            this.a.n();
        } else if (this.a.F() == PDFView.ViewMode.TEXTREFLOW) {
            this.a.G.b();
        }
    }
}
