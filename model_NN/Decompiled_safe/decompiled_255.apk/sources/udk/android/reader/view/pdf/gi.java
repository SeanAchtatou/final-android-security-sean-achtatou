package udk.android.reader.view.pdf;

import android.media.MediaPlayer;
import android.widget.FrameLayout;

final class gi implements MediaPlayer.OnErrorListener {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ FrameLayout b;

    gi(PDFView pDFView, FrameLayout frameLayout) {
        this.a = pDFView;
        this.b = frameLayout;
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        this.a.I.b(this.b);
        return true;
    }
}
