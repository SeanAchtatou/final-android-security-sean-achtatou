package udk.android.reader.view.pdf;

import android.graphics.PointF;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;
import udk.android.b.c;

public final class ar {
    private static ar a;
    private hx b = hx.a();
    private ic c = ic.a();
    private boolean d;
    private boolean e;
    private float f;
    private float g;
    private float h;
    private float i;
    private float j;
    private List k = new ArrayList();

    private ar() {
    }

    public static ar a() {
        if (a == null) {
            a = new ar();
        }
        return a;
    }

    private void h() {
        if (this.b.e() <= this.c.a) {
            this.f = (float) ((this.c.a / 2) - (this.b.e() / 2));
        } else if (this.b.a > 0.0f) {
            this.f = 0.0f;
        } else if (this.b.a + ((float) this.b.e()) < ((float) this.c.a)) {
            this.f = (float) (this.c.a - this.b.e());
        } else {
            this.f = this.b.a;
        }
        if (this.b.i() <= this.c.b) {
            this.g = (float) ((this.c.b / 2) - (this.b.i() / 2));
        } else if (this.b.b > 0.0f) {
            this.g = 0.0f;
        } else if (this.b.b + ((float) this.b.i()) < ((float) this.c.b)) {
            this.g = (float) (this.c.b - this.b.i());
        } else {
            this.g = this.b.b;
        }
    }

    private float i() {
        return this.f - this.b.a;
    }

    private float j() {
        return this.g - this.b.b;
    }

    public final void a(float f2, float f3) {
        e();
        this.f = f2;
        this.g = f3;
        this.j = 0.2f;
        this.d = true;
    }

    public final void a(RectF rectF, RectF rectF2) {
        PointF b2 = c.b(rectF2, new RectF(this.b.a + rectF.left, this.b.b + rectF.top, this.b.a + rectF.right, this.b.b + rectF.bottom));
        this.b.a += rectF2.left - b2.x;
        hx hxVar = this.b;
        hxVar.b = (rectF2.top - b2.y) + hxVar.b;
        this.b.p();
    }

    public final void a(gv gvVar) {
        if (!this.k.contains(gvVar)) {
            this.k.add(gvVar);
        }
    }

    public final void b() {
        if (!this.d) {
            e();
            h();
            this.j = 0.2f;
            this.d = true;
        }
    }

    public final void b(float f2, float f3) {
        e();
        this.h = f2 * 0.03f;
        this.i = f3 * 0.03f;
        this.j = 0.9f;
        this.e = true;
    }

    public final void b(gv gvVar) {
        this.k.remove(gvVar);
    }

    public final void c() {
        e();
        h();
        this.b.a = this.f;
        this.b.b = this.g;
        this.b.p();
    }

    public final boolean d() {
        return this.d || this.e;
    }

    public final void e() {
        this.d = false;
        this.e = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.hx.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.hx.a(int, float):float
      udk.android.reader.view.pdf.hx.a(java.lang.String, float):void
      udk.android.reader.view.pdf.hx.a(boolean, boolean):boolean */
    public final void f() {
        if (this.d) {
            this.h = i() * this.j;
            this.i = j() * this.j;
            this.b.a += this.h;
            this.b.b += this.i;
            float i2 = i();
            float j2 = j();
            if (Math.abs((float) Math.sqrt((double) ((i2 * i2) + (j2 * j2)))) < 1.0f) {
                this.b.a = this.f;
                this.b.b = this.g;
                this.d = false;
                new am(this).start();
            }
        } else if (this.e) {
            this.h *= this.j;
            this.i *= this.j;
            if (this.b.e() > this.c.a) {
                this.b.a += this.h;
            }
            if (this.b.i() > this.c.b) {
                this.b.b += this.i;
            }
            if (this.b.a(false, false) || (Math.abs(this.h) < 1.0f && Math.abs(this.i) < 1.0f)) {
                this.e = false;
                b();
            }
        }
    }

    public final void g() {
        for (gv d2 : this.k) {
            d2.d();
        }
    }
}
