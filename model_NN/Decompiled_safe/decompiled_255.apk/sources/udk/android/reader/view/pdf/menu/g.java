package udk.android.reader.view.pdf.menu;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.unidocs.commonlib.util.b;
import java.util.Collection;
import java.util.List;
import udk.android.b.d;
import udk.android.b.m;
import udk.android.reader.b.a;
import udk.android.reader.pdf.a.c;
import udk.android.reader.view.pdf.hx;

public final class g extends LinearLayout {
    private static final int[] a = {-14540254, -8947849, -16777216};
    private static final float[] b = {0.0f, 0.2f, 1.0f};
    /* access modifiers changed from: private */
    public Paint c = new Paint(1);
    /* access modifiers changed from: private */
    public Paint d;
    /* access modifiers changed from: private */
    public Paint e;
    private int f;
    /* access modifiers changed from: private */
    public int g;
    private int h;
    /* access modifiers changed from: private */
    public LinearLayout i;
    private String j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    public g(Context context) {
        super(context);
        this.g = m.a(context, 10);
        this.f = m.a(context, 15);
        this.h = m.a(context, 1);
        setPadding(this.g, this.g, this.g, this.g);
        this.c.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) m.a(context, 30), a, b, Shader.TileMode.CLAMP));
        this.e = new Paint(1);
        this.e.setColor(-1442840576);
        this.e.setStyle(Paint.Style.FILL);
        this.e.setMaskFilter(new BlurMaskFilter((float) (this.g / 4), BlurMaskFilter.Blur.NORMAL));
        this.d = new Paint(1);
        this.d.setColor(-1);
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeWidth((float) this.h);
    }

    /* access modifiers changed from: private */
    public synchronized void a(LinearLayout linearLayout, c cVar, RectF rectF) {
        int i2;
        int i3;
        if (linearLayout != null) {
            int height = linearLayout.getHeight() + this.f;
            hx a2 = hx.a();
            RectF b2 = cVar.b(a2.n());
            b2.offset(a2.a, a2.b);
            float f2 = b2.top - rectF.top;
            float f3 = rectF.bottom - b2.bottom;
            if (f2 < ((float) height) && f3 < ((float) height)) {
                i2 = 2;
                i3 = (int) (rectF.centerY() - ((float) (linearLayout.getHeight() / 2)));
            } else if (f2 >= ((float) height)) {
                i2 = 1;
                i3 = (int) (b2.top - ((float) height));
                if (((float) i3) > (rectF.bottom - ((float) linearLayout.getHeight())) - ((float) this.g)) {
                    i3 = (int) ((rectF.bottom - ((float) linearLayout.getHeight())) - ((float) this.g));
                }
            } else {
                int i4 = (int) (b2.bottom + ((float) this.f));
                if (((float) i4) < ((float) this.g) + rectF.top) {
                    i2 = 3;
                    i3 = (int) (((float) this.g) + rectF.top);
                } else {
                    int i5 = i4;
                    i2 = 3;
                    i3 = i5;
                }
            }
            int centerX = (int) (b2.centerX() - ((float) (linearLayout.getWidth() / 2)));
            if (centerX < this.g) {
                centerX = this.g;
            }
            if (linearLayout.getWidth() + centerX > getWidth() - this.g) {
                centerX = (getWidth() - this.g) - linearLayout.getWidth();
            }
            setPadding(centerX, i3, this.g, this.g);
            ((TextView) linearLayout.findViewById(Integer.MAX_VALUE)).setLinkTextColor(-1);
            linearLayout.setBackgroundDrawable(new d(this, linearLayout, i2));
            d.a(this, 1.0f);
        }
    }

    public final synchronized void a() {
        if (this.i != null) {
            this.i.removeAllViews();
            this.i = null;
        }
        removeAllViews();
        this.j = null;
        setPadding(this.g, this.g, this.g, this.g);
        d.a(this, 0.0f);
    }

    public final synchronized void a(String str, c cVar, RectF rectF, List list, View view) {
        if (cVar != null) {
            if (!b.b((Collection) list) && str != null) {
                if (!str.equals(this.j)) {
                    this.j = str;
                    CharSequence a2 = MenuCommandSpan.a(list, "   ", a.aG, a.aH);
                    removeAllViews();
                    Context context = getContext();
                    LinearLayout linearLayout = new LinearLayout(context);
                    linearLayout.setOnTouchListener(new f(this, linearLayout));
                    linearLayout.setOrientation(1);
                    linearLayout.setPadding(this.g, this.g, this.g, view == null ? this.g : this.g * 2);
                    TextView textView = new TextView(context);
                    textView.setPadding(0, m.a(context, a.aF / 2), 0, 0);
                    textView.setId(Integer.MAX_VALUE);
                    textView.setText(a2, TextView.BufferType.SPANNABLE);
                    textView.setGravity(16);
                    textView.setTextScaleX(0.8f);
                    textView.setTextSize(1, (float) a.aF);
                    textView.setTypeface(Typeface.defaultFromStyle(1));
                    textView.setLineSpacing(0.0f, 1.5f);
                    textView.setMovementMethod(b.a());
                    linearLayout.addView(textView, new LinearLayout.LayoutParams(-2, -2));
                    if (view != null) {
                        linearLayout.addView(view);
                    }
                    addView(linearLayout, new LinearLayout.LayoutParams(-2, -2));
                    this.i = linearLayout;
                    e eVar = new e(this, linearLayout, (TextView) this.i.findViewById(Integer.MAX_VALUE), cVar, rectF);
                    eVar.setDaemon(true);
                    eVar.start();
                }
            }
        }
    }

    public final synchronized boolean b() {
        return getChildCount() > 0;
    }
}
