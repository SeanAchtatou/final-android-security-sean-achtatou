package udk.android.reader.view.pdf.navigation;

import android.widget.TextView;

final class c implements Runnable {
    private /* synthetic */ au a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ int c;

    c(au auVar, TextView textView, int i) {
        this.a = auVar;
        this.b = textView;
        this.c = i;
    }

    public final void run() {
        this.b.setText(new StringBuilder(String.valueOf(this.c)).toString());
    }
}
