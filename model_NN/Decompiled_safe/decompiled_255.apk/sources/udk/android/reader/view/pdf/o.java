package udk.android.reader.view.pdf;

import android.graphics.PointF;
import android.graphics.RectF;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import udk.android.b.c;
import udk.android.reader.b.a;
import udk.android.reader.pdf.PDF;
import udk.android.reader.pdf.ah;
import udk.android.reader.pdf.ai;
import udk.android.reader.pdf.ar;
import udk.android.reader.pdf.x;

public final class o implements ar, b {
    private cm a;
    /* access modifiers changed from: private */
    public PDF b;
    private List c;
    private ai d;
    private ai e;
    private List f;
    private Object g = new Object();
    private List h;
    /* access modifiers changed from: private */
    public x i;
    private boolean j;
    private int k;
    private int l;

    o(cm cmVar) {
        this.a = cmVar;
        this.b = PDF.a();
        this.b.a(this);
        ZoomService.a().a(this);
        this.f = new ArrayList();
    }

    private void A() {
        for (io ac : this.f) {
            ac.ac();
        }
    }

    private RectF a(int i2, float f2, RectF rectF) {
        ic a2 = ic.a();
        float f3 = 0.0f - (rectF.left * f2);
        float a3 = ((float) this.b.a(i2, f2)) + f3;
        float centerY = rectF.height() * f2 > ((float) a2.b) ? 0.0f - (rectF.top * f2) : ((float) (a2.b / 2)) - (rectF.centerY() * f2);
        return new RectF(f3, centerY, a3, ((float) this.b.b(i2, f2)) + centerY);
    }

    static /* synthetic */ RectF a(o oVar, int i2, float f2, RectF rectF) {
        ic a2 = ic.a();
        float centerX = ((float) (a2.a / 2)) - (rectF.centerX() * f2);
        float centerY = ((float) (a2.b / 2)) - (rectF.centerY() * f2);
        return new RectF(centerX, centerY, ((float) oVar.b.a(i2, f2)) + centerX, ((float) oVar.b.b(i2, f2)) + centerY);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(udk.android.reader.view.pdf.o r4, int r5, float r6, float r7) {
        /*
            java.lang.Object r1 = r4.g
            monitor-enter(r1)
            int r0 = r4.k     // Catch:{ all -> 0x0055 }
            if (r5 == r0) goto L_0x0011
            udk.android.reader.pdf.PDF r0 = r4.b     // Catch:{ all -> 0x0055 }
            java.util.List r0 = r0.d(r5)     // Catch:{ all -> 0x0055 }
            r4.h = r0     // Catch:{ all -> 0x0055 }
            r4.k = r5     // Catch:{ all -> 0x0055 }
        L_0x0011:
            r0 = -1
            r4.l = r0     // Catch:{ all -> 0x0055 }
            r0 = 0
            r4.i = r0     // Catch:{ all -> 0x0055 }
            java.util.List r0 = r4.h     // Catch:{ all -> 0x0055 }
            boolean r0 = com.unidocs.commonlib.util.b.a(r0)     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x002f
            udk.android.reader.pdf.PDF r0 = r4.b     // Catch:{ all -> 0x0055 }
            float r2 = r0.F()     // Catch:{ all -> 0x0055 }
            r0 = 0
            r3 = r0
        L_0x0027:
            java.util.List r0 = r4.h     // Catch:{ all -> 0x0055 }
            int r0 = r0.size()     // Catch:{ all -> 0x0055 }
            if (r3 < r0) goto L_0x0031
        L_0x002f:
            monitor-exit(r1)     // Catch:{ all -> 0x0055 }
        L_0x0030:
            return
        L_0x0031:
            java.util.List r0 = r4.h     // Catch:{ all -> 0x0055 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x0055 }
            udk.android.reader.pdf.x r0 = (udk.android.reader.pdf.x) r0     // Catch:{ all -> 0x0055 }
            udk.android.reader.pdf.a.b r0 = r0.d()     // Catch:{ all -> 0x0055 }
            android.graphics.RectF r0 = r0.b(r2)     // Catch:{ all -> 0x0055 }
            boolean r0 = r0.contains(r6, r7)     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x0058
            r4.l = r3     // Catch:{ all -> 0x0055 }
            java.util.List r0 = r4.h     // Catch:{ all -> 0x0055 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ all -> 0x0055 }
            udk.android.reader.pdf.x r0 = (udk.android.reader.pdf.x) r0     // Catch:{ all -> 0x0055 }
            r4.i = r0     // Catch:{ all -> 0x0055 }
            monitor-exit(r1)     // Catch:{ all -> 0x0055 }
            goto L_0x0030
        L_0x0055:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0055 }
            throw r0
        L_0x0058:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.view.pdf.o.a(udk.android.reader.view.pdf.o, int, float, float):void");
    }

    static /* synthetic */ void a(o oVar, int i2, int i3) {
        synchronized (oVar.g) {
            oVar.h = oVar.b.d(i2);
            oVar.k = i2;
            if (b.b((Collection) oVar.h) || oVar.h.size() <= i3) {
                oVar.i = null;
            } else {
                oVar.i = (x) oVar.h.get(i3);
                oVar.l = i3;
            }
        }
    }

    static /* synthetic */ void a(o oVar, int i2, boolean z) {
        synchronized (oVar.g) {
            if (oVar.h == null || i2 != oVar.k) {
                oVar.h = oVar.b.d(i2);
                oVar.k = i2;
                if (b.b((Collection) oVar.h)) {
                    oVar.i = null;
                    return;
                }
                oVar.l = z ? -1 : oVar.h.size();
            }
            if (z && oVar.l + 1 >= oVar.h.size()) {
                oVar.i = null;
            } else if (z || oVar.l - 1 >= 0) {
                if (z) {
                    oVar.l++;
                } else {
                    oVar.l--;
                }
                oVar.i = (x) oVar.h.get(oVar.l);
            } else {
                oVar.i = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (z) {
            this.j = true;
            for (io ad : this.f) {
                ad.ad();
            }
            return;
        }
        boolean z2 = this.j;
        this.j = false;
        cl U = this.a.a().U();
        if (U.b() || U.d()) {
            U.e();
        }
        if (z2) {
            for (io ae : this.f) {
                ae.ae();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean a(int i2) {
        RectF rectF;
        if (this.i == null) {
            return false;
        }
        ic a2 = ic.a();
        RectF a3 = c.a(this.i.d().b(1.0f), a.O);
        int a4 = (((this.b.a(this.i.e(), this.i.b(), this.i.c()) % 360) + 360) % 360) / 90;
        if (!a.P) {
            a4 = 0;
        }
        float height = (a4 == 1 || a4 == 3) ? ((float) a2.b) / a3.height() : ((float) a2.a) / a3.width();
        switch (a4) {
            case 0:
                rectF = a(i2, height, a3);
                break;
            case 1:
                ic a5 = ic.a();
                int a6 = this.b.a(i2, height);
                float f2 = 0.0f - (a3.top * height);
                float b2 = ((float) this.b.b(i2, height)) + f2;
                float centerX = a3.width() * height > ((float) a5.a) ? (((float) a6) - (a3.right * height)) + ((float) a5.a) : (((float) a6) - (a3.centerX() * height)) + ((float) (a5.a / 2));
                rectF = new RectF(centerX - ((float) a6), f2, centerX, b2);
                break;
            case 2:
                ic a7 = ic.a();
                int b3 = this.b.b(i2, height);
                float f3 = 0.0f - (a3.left * height);
                float a8 = ((float) this.b.a(i2, height)) + f3;
                float centerY = a3.height() * height > ((float) a7.b) ? (((float) b3) - (a3.bottom * height)) + ((float) a7.b) : (((float) b3) - (a3.centerY() * height)) + ((float) (a7.b / 2));
                rectF = new RectF(f3, centerY - ((float) b3), a8, centerY);
                break;
            case 3:
                ic a9 = ic.a();
                float f4 = 0.0f - (a3.top * height);
                float b4 = ((float) this.b.b(i2, height)) + f4;
                float centerX2 = a3.width() * height > ((float) a9.a) ? 0.0f - (a3.left * height) : ((float) (a9.a / 2)) - (a3.centerX() * height);
                rectF = new RectF(centerX2, f4, ((float) this.b.a(i2, height)) + centerX2, b4);
                break;
            default:
                rectF = a(i2, height, a3);
                break;
        }
        if (i2 != this.b.E()) {
            this.a.a().a(i2, height, rectF);
        } else {
            ZoomService.a().a(height, rectF);
        }
        if (a4 == 1 || a4 == 3) {
            this.a.a().U().c();
        } else {
            this.a.a().U().a();
        }
        if (this.j) {
            A();
        }
        return true;
    }

    static /* synthetic */ RectF b(o oVar, int i2, float f2, float f3) {
        List<udk.android.reader.pdf.a.b> e2 = oVar.b.e(i2);
        if (b.b((Collection) e2)) {
            return null;
        }
        float F = oVar.b.F();
        float f4 = f2 / F;
        float f5 = f3 / F;
        RectF rectF = null;
        for (udk.android.reader.pdf.a.b b2 : e2) {
            RectF b3 = b2.b(1.0f);
            if (b3.contains(f4, f5)) {
                if (rectF == null) {
                    rectF = b3;
                } else {
                    rectF.union(b3);
                }
            } else if (rectF != null && RectF.intersects(rectF, b3)) {
                rectF.union(b3);
            }
        }
        if (rectF == null) {
            return null;
        }
        return new RectF(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    private void x() {
        for (io Z : this.f) {
            Z.Z();
        }
    }

    private void y() {
        for (io ab : this.f) {
            ab.ab();
        }
    }

    private void z() {
        for (io b2 : this.f) {
            b2.b();
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized PointF a(float f2) {
        PointF pointF;
        try {
            if (this.c == null) {
                pointF = null;
            } else {
                RectF b2 = ((udk.android.reader.pdf.a.a) this.c.get(0)).b(f2);
                pointF = new PointF(b2.left, b2.top);
            }
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Throwable) e2);
            pointF = null;
        }
        return pointF;
    }

    /* access modifiers changed from: package-private */
    public final x a(float f2, float f3) {
        int E = this.b.E();
        float F = this.b.F();
        if (b.a((Collection) this.h)) {
            for (x xVar : this.h) {
                if (E == xVar.e() && xVar.d().b(F).contains(f2, f3)) {
                    return xVar;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(float f2, float f3, Runnable runnable) {
        z();
        new ge(this, this.b.E(), f2, f3, runnable).start();
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, float f2, float f3, Runnable runnable) {
        z();
        new gh(this, i2, f2, f3, runnable).start();
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3) {
        z();
        new gg(this, i2, i3).start();
    }

    public final void a(ah ahVar) {
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(ai aiVar, ai aiVar2) {
        List a2 = this.b.a(aiVar, aiVar2);
        if (b.a((Collection) a2)) {
            this.c = a2;
            this.d = aiVar;
            this.e = aiVar2;
            x();
        } else {
            y();
        }
    }

    public final void a(bm bmVar) {
    }

    /* access modifiers changed from: package-private */
    public final void a(io ioVar) {
        if (!this.f.contains(ioVar)) {
            this.f.add(ioVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized PointF b(float f2) {
        PointF pointF;
        try {
            if (this.c == null) {
                pointF = null;
            } else {
                RectF b2 = ((udk.android.reader.pdf.a.a) this.c.get(this.c.size() - 1)).b(f2);
                pointF = new PointF(b2.right, b2.bottom);
            }
        } catch (Exception e2) {
            udk.android.reader.b.c.a((Throwable) e2);
            pointF = null;
        }
        return pointF;
    }

    /* access modifiers changed from: package-private */
    public final RectF b(float f2, float f3) {
        if (!this.j || this.i == null) {
            return null;
        }
        RectF b2 = this.i.d().b(1.0f);
        return new RectF((b2.left - f3) * f2, (b2.top - f3) * f2, (b2.right + f3) * f2, (b2.bottom + f3) * f2);
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        ZoomService.a().b(this);
        this.b.b(this);
    }

    public final void b(ah ahVar) {
        if (ahVar.c) {
            v();
            if (this.j && this.k != ahVar.a) {
                a(false);
            }
        }
    }

    public final void b(bm bmVar) {
        if (!bmVar.f) {
            a(false);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(io ioVar) {
        this.f.remove(ioVar);
    }

    public final void b_() {
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean c(float f2, float f3) {
        boolean z;
        ai a2 = this.b.a(f2, f3);
        if (a2 == null) {
            z = false;
        } else {
            ai aiVar = new ai(a2.a, 0);
            ai aiVar2 = new ai(a2.a, 1000);
            List a3 = this.b.a(aiVar, aiVar2);
            if (b.a((Collection) a3)) {
                this.c = a3;
                this.d = aiVar;
                this.e = aiVar2;
                x();
                z = true;
            } else {
                y();
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void d(float f2, float f3) {
        ai a2 = this.b.a(f2, f3);
        if (a2 != null) {
            List a3 = this.b.a(a2, this.e);
            if (b.a((Collection) a3) && !(this.d.a != 0 && a2.a == 0 && a2.b == 0)) {
                this.c = a3;
                this.d = a2;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        a(false);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void e(float f2, float f3) {
        ai a2 = this.b.a(f2, f3);
        if (a2 != null) {
            List a3 = this.b.a(this.d, a2);
            if (b.a((Collection) a3) && !(this.e.a != 0 && a2.a == 0 && a2.b == 0)) {
                this.c = a3;
                this.e = a2;
            }
        }
    }

    public final void f() {
    }

    public final void g() {
        v();
    }

    public final void h() {
    }

    /* access modifiers changed from: package-private */
    public final x i() {
        if (this.j) {
            return this.i;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final String j() {
        if (!this.j || this.i == null) {
            return null;
        }
        return this.i.a();
    }

    /* access modifiers changed from: package-private */
    public final void k() {
        z();
        new gf(this).start();
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        z();
        new gj(this).start();
    }

    /* access modifiers changed from: package-private */
    public final void m() {
        if (!a(this.b.E())) {
            a(false);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized String n() {
        return (this.d == null || this.e == null) ? null : this.b.b(this.d, this.e);
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean o() {
        return this.c != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.pdf.PDF.a(int, float, int[]):double[]
     arg types: [int, int, int[]]
     candidates:
      udk.android.reader.pdf.PDF.a(int, float, int):java.util.List
      udk.android.reader.pdf.PDF.a(android.graphics.Canvas, int, float):void
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.b.k, java.util.Map, udk.android.reader.pdf.b.q):void
      udk.android.reader.pdf.PDF.a(int, int, int):int
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.annotation.ac, udk.android.reader.pdf.ai, udk.android.reader.pdf.ai):boolean
      udk.android.reader.pdf.PDF.a(udk.android.reader.pdf.y, java.lang.String, java.io.InputStream):boolean
      udk.android.reader.pdf.PDF.a(int, float, android.graphics.RectF):double[]
      udk.android.reader.pdf.PDF.a(int, float, double[]):int[]
      udk.android.reader.pdf.PDF.a(int, float, int[]):double[] */
    /* access modifiers changed from: package-private */
    public final synchronized udk.android.reader.pdf.a.b p() {
        udk.android.reader.pdf.a.b bVar;
        if (this.c == null) {
            bVar = null;
        } else {
            int i2 = 0;
            RectF rectF = null;
            for (udk.android.reader.pdf.a.a aVar : this.c) {
                if (rectF == null) {
                    rectF = aVar.b(1.0f);
                    i2 = aVar.ag();
                } else {
                    rectF.union(aVar.b(1.0f));
                }
            }
            bVar = new udk.android.reader.pdf.a.b(i2, this.b.a(this.b.E(), 1.0f, new int[]{(int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom}));
        }
        return bVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized ai q() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final synchronized ai r() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final synchronized List s() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final synchronized ai t() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final synchronized ai u() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void v() {
        if (o()) {
            this.c = null;
            this.d = null;
            this.e = null;
            for (io aa : this.f) {
                aa.aa();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void w() {
        for (io c2 : this.f) {
            c2.c();
        }
    }
}
