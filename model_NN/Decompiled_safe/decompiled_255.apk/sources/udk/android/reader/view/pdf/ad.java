package udk.android.reader.view.pdf;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;
import udk.android.b.m;
import udk.android.reader.b.a;
import udk.android.reader.b.c;

final class ad extends Thread {
    private final /* synthetic */ View a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ Runnable c;
    private final /* synthetic */ String d;
    private final /* synthetic */ ProgressDialog e;

    ad(View view, Activity activity, Runnable runnable, String str, ProgressDialog progressDialog) {
        this.a = view;
        this.b = activity;
        this.c = runnable;
        this.d = str;
        this.e = progressDialog;
    }

    public final void run() {
        while (this.a.getHeight() <= 0) {
            try {
                sleep(100);
            } catch (Exception e2) {
                c.a((Throwable) e2);
                this.a.post(new bl(this, this.e));
                m.a(this.b, this.a, this.d);
                return;
            }
        }
        this.a.post(new bj(this, this.e));
        PDFView.a(this.b, a.c(this.b));
        this.a.post(new bk(this, this.e));
        if (this.c != null) {
            this.c.run();
        }
    }
}
