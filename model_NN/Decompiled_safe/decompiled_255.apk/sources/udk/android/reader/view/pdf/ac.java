package udk.android.reader.view.pdf;

final class ac implements Runnable {
    private /* synthetic */ PDFView a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ int e;
    private final /* synthetic */ float f;
    private final /* synthetic */ boolean g;
    private final /* synthetic */ float h;
    private final /* synthetic */ float i;
    private final /* synthetic */ int j;

    ac(PDFView pDFView, String str, String str2, String str3, int i2, float f2, boolean z, float f3, float f4, int i3) {
        this.a = pDFView;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = i2;
        this.f = f2;
        this.g = z;
        this.h = f3;
        this.i = f4;
        this.j = i3;
    }

    public final void run() {
        PDFView.a(this.a, this.b, null, 0, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, 0);
    }
}
