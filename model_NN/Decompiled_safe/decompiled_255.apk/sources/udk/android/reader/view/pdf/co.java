package udk.android.reader.view.pdf;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import udk.android.reader.b.a;
import udk.android.reader.b.b;
import udk.android.reader.b.c;
import udk.android.reader.pdf.annotation.aa;
import udk.android.reader.pdf.annotation.ab;
import udk.android.reader.pdf.annotation.s;

public final class co extends ExpandableListView implements aa {
    /* access modifiers changed from: private */
    public db a;
    /* access modifiers changed from: private */
    public da b;

    public co(Context context) {
        super(context);
        int a2 = (int) a.a(5.0f);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(80);
        linearLayout.setPadding(a2, 0, a2, 0);
        hw hwVar = new hw(this, context);
        hwVar.setSingleLine(true);
        hwVar.setEllipsize(TextUtils.TruncateAt.START);
        hwVar.setHint(b.h);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.weight = 1.0f;
        linearLayout.addView(hwVar, layoutParams);
        View view = new View(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((int) a.a(5.0f), 1);
        layoutParams2.weight = 0.0f;
        linearLayout.addView(view, layoutParams2);
        TextView textView = new TextView(context);
        textView.setText(b.q);
        textView.setTextColor(-1);
        textView.setTextSize(1, 25.0f);
        textView.setShadowLayer(3.0f, 0.0f, 0.0f, -16777216);
        textView.setOnClickListener(new hu(this, hwVar));
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.weight = 0.0f;
        linearLayout.addView(textView, layoutParams3);
        addHeaderView(linearLayout);
    }

    private void b() {
        post(new hv(this));
    }

    public final void a() {
        b();
    }

    public final void a(ab abVar) {
        b();
    }

    public final void a(db dbVar) {
        s.a().a(this);
        this.a = dbVar;
        this.b = new da(dbVar);
        setAdapter(this.b);
        c.a("## AnnotationListView ACTIVATED");
    }

    public final void b(ab abVar) {
    }

    public final void c(ab abVar) {
        b();
    }

    public final void d(ab abVar) {
    }

    public final void e(ab abVar) {
    }

    /* access modifiers changed from: protected */
    public final void layoutChildren() {
        try {
            super.layoutChildren();
        } catch (Exception e) {
            c.a((Throwable) e);
            b();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.b = null;
        setAdapter(this.b);
        s.a().b(this);
        c.a("## AnnotationListView DEACTIVATED");
        super.onDetachedFromWindow();
    }
}
