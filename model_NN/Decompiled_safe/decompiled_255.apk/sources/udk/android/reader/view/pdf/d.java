package udk.android.reader.view.pdf;

import android.content.Context;
import android.content.DialogInterface;
import udk.android.reader.pdf.annotation.Annotation;
import udk.android.reader.pdf.annotation.e;
import udk.android.reader.pdf.annotation.p;
import udk.android.reader.pdf.annotation.s;
import udk.android.reader.pdf.annotation.w;

final class d implements DialogInterface.OnClickListener {
    private /* synthetic */ fb a;
    private final /* synthetic */ au b;
    private final /* synthetic */ Annotation c;
    private final /* synthetic */ s d;
    private final /* synthetic */ Context e;

    d(fb fbVar, au auVar, Annotation annotation, s sVar, Context context) {
        this.a = fbVar;
        this.b = auVar;
        this.c = annotation;
        this.d = sVar;
        this.e = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        int a2 = this.b.a();
        if (a2 != this.c.L()) {
            this.d.a(this.e, this.c, a2);
        }
        String d2 = this.b.d();
        if (!d2.equals(this.c.N())) {
            this.d.a(this.c, d2);
        }
        String e2 = this.b.e();
        if (!e2.equals(this.c.P())) {
            this.d.a(this.e, this.c, e2);
        }
        if (this.c instanceof p) {
            p pVar = (p) this.c;
            String f = this.b.f();
            if (!f.equals(pVar.a())) {
                this.d.a(this.e, pVar, f);
            }
        }
        if (this.c.l() || this.c.t()) {
            float g = this.b.g();
            int T = this.c.T();
            if (!(g == this.c.S() && T == this.c.T())) {
                s sVar = this.d;
                Context context = this.e;
                Annotation annotation = this.c;
                if (!this.c.l()) {
                    g = this.c.S();
                }
                if (!this.c.t()) {
                    T = this.c.T();
                }
                sVar.a(context, annotation, g, T);
            }
        }
        if (this.c.s()) {
            boolean b2 = this.b.b();
            int c2 = this.b.c();
            if (!(this.c.aa() == b2 && this.c.Z() == c2)) {
                this.d.a(this.e, this.c, b2, c2);
            }
        }
        if (this.c instanceof e) {
            e eVar = (e) this.c;
            float h = this.b.h();
            if (h != eVar.f()) {
                this.d.a(this.e, eVar, h);
            }
        }
        if (this.c instanceof w) {
            w wVar = (w) this.c;
            String i2 = this.b.i();
            String j = this.b.j();
            if (!i2.equals(wVar.c()) || !j.equals(wVar.d())) {
                this.d.a(wVar, i2, j);
            }
        }
    }
}
