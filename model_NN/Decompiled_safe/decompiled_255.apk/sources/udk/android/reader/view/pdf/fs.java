package udk.android.reader.view.pdf;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

final class fs implements View.OnClickListener {
    final /* synthetic */ au a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;

    fs(au auVar, Context context, String str) {
        this.a = auVar;
        this.b = context;
        this.c = str;
    }

    public final void onClick(View view) {
        String[] strArr = {"8.0", "9.0", "10.0", "11.0", "12.0", "13.0", "14.0", "15.0", "16.0", "17.0", "18.0", "19.0", "20.0"};
        new AlertDialog.Builder(this.b).setTitle(this.c).setItems(strArr, new a(this, strArr)).show();
    }
}
