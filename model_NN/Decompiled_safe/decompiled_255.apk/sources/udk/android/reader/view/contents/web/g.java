package udk.android.reader.view.contents.web;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.unidocs.commonlib.util.h;
import udk.android.reader.C0000R;
import udk.android.reader.b.e;

public class g extends FrameLayout {
    /* access modifiers changed from: private */
    public WebPageView a;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public EditText c;
    private View d;

    public g(Context context) {
        super(context);
        View inflate = View.inflate(getContext(), C0000R.layout.web_browser, null);
        if (!e.f) {
            inflate.findViewById(C0000R.id.address_bar).setVisibility(8);
        }
        inflate.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(inflate);
        this.a = (WebPageView) findViewById(C0000R.id.web_page_view);
        this.b = (TextView) findViewById(C0000R.id.display_address);
        this.c = (EditText) findViewById(C0000R.id.input_address);
        this.d = findViewById(C0000R.id.btn_home);
        findViewById(C0000R.id.web_page_view_mask).setOnTouchListener(new m(this));
        this.a.setWebViewClient(new f(this));
        this.a.setWebChromeClient(new w());
        this.a.setHorizontalScrollbarOverlay(true);
        this.a.setVerticalScrollbarOverlay(true);
        WebSettings settings = this.a.getSettings();
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setTextSize(WebSettings.TextSize.NORMAL);
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        settings.setSaveFormData(e.h);
        settings.setSavePassword(e.i);
        this.b.setOnClickListener(new n(this));
        this.c.setInputType(17);
        this.c.setImeOptions(2);
        this.c.setOnEditorActionListener(new o(this));
        this.d.setOnClickListener(new p(this));
    }

    private void a(String str, Runnable runnable, Runnable runnable2) {
        new z(this, str, runnable, runnable2).start();
    }

    static /* synthetic */ void a(g gVar, boolean z) {
        if (z) {
            gVar.c.setText(gVar.b.getText());
            gVar.b.setVisibility(8);
            gVar.c.setVisibility(0);
            gVar.c.requestFocus();
            gVar.postDelayed(new r(gVar), 100);
            return;
        }
        if (gVar.c.getVisibility() == 0) {
            gVar.c.setVisibility(8);
        }
        if (gVar.b.getVisibility() == 8) {
            gVar.b.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        View findViewById = findViewById(C0000R.id.progress);
        if (findViewById.getVisibility() != 0) {
            findViewById.setVisibility(0);
        }
    }

    static /* synthetic */ void c(g gVar) {
        View findViewById = gVar.findViewById(C0000R.id.progress);
        if (findViewById.getVisibility() == 0) {
            findViewById.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.c.getWindowToken(), 0);
        if ("none".equals(str)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("<html>");
            stringBuffer.append("<head>");
            stringBuffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"/>");
            stringBuffer.append("<style type=\"text/css\">");
            stringBuffer.append("body {");
            stringBuffer.append("background : rgb( 255, 255, 255 );");
            stringBuffer.append("text-align : left;");
            stringBuffer.append("}");
            stringBuffer.append("</style>");
            stringBuffer.append("</head>");
            stringBuffer.append("<body>");
            stringBuffer.append(getContext().getString(C0000R.string.f87PDFPDFURL));
            stringBuffer.append("</body>");
            stringBuffer.append("</html>");
            this.a.loadDataWithBaseURL(null, stringBuffer.toString(), "text/html", "UTF-8", null);
            return;
        }
        String str2 = !h.a(str, "^https?://") ? "http://" + str : str;
        c();
        a(str2, new s(this, str2), new t(this, str2));
    }

    public final boolean a() {
        return this.a.canGoBack();
    }

    /* access modifiers changed from: protected */
    public final boolean a(WebView webView, String str) {
        String replaceAll = str.replaceAll("ezpdf://", "http://");
        if (!replaceAll.startsWith("http")) {
            return true;
        }
        c();
        a(replaceAll, new aa(this, replaceAll), new ab(this, webView, replaceAll));
        return true;
    }

    public final void b() {
        this.a.goBack();
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        Activity activity = (Activity) getContext();
        String[] strArr = {activity.getString(C0000R.string.f91_), activity.getString(C0000R.string.f123)};
        new AlertDialog.Builder(activity).setTitle(activity.getString(C0000R.string.f89)).setItems(strArr, new u(this, strArr, activity, str)).show();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        new q(this).start();
    }
}
