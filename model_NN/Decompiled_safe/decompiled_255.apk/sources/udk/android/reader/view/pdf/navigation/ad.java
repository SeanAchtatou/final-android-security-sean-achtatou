package udk.android.reader.view.pdf.navigation;

import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

final class ad implements TextView.OnEditorActionListener {
    private /* synthetic */ NavigationService a;
    private final /* synthetic */ EditText b;

    ad(NavigationService navigationService, EditText editText) {
        this.a = navigationService;
        this.b = editText;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        if (this.a.v != null && this.a.v.isShowing()) {
            this.a.v.dismiss();
        }
        NavigationService.a(this.a, Integer.parseInt(this.b.getText().toString()));
        this.a.v = null;
        return true;
    }
}
