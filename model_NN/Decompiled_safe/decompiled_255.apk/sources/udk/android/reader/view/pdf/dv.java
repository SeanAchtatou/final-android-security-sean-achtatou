package udk.android.reader.view.pdf;

import udk.android.reader.b.c;

final class dv extends Thread {
    final /* synthetic */ PDFView a;

    dv(PDFView pDFView) {
        this.a = pDFView;
    }

    public final void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                c.a((Throwable) e);
            }
            if (!this.a.b.b() || this.a.i == null || !this.a.i.isShowing()) {
                c.a("## OBSERVING END");
            } else {
                this.a.post(new cq(this));
            }
        }
        c.a("## OBSERVING END");
    }
}
