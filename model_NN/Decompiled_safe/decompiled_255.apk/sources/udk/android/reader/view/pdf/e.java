package udk.android.reader.view.pdf;

import android.content.Context;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import udk.android.b.m;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;

public final class e extends GridView {
    PDFView a;
    /* access modifiers changed from: private */
    public PDF b;
    /* access modifiers changed from: private */
    public Object c = new Object();

    e(PDFView pDFView) {
        super(pDFView.getContext());
        this.a = pDFView;
        this.b = PDF.a();
        Context context = getContext();
        setVerticalSpacing(m.a(context, 10));
        setHorizontalSpacing(m.a(context, 5));
        setNumColumns(-1);
        setColumnWidth(m.a(context, 90));
        setStretchMode(2);
        setGravity(17);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setPadding(m.a(context, 2), m.a(context, 2), m.a(context, 2), m.a(context, 2));
    }

    public final void a() {
        int firstVisiblePosition = getFirstVisiblePosition();
        float a2 = this.b.a((float) m.a(getContext(), 86));
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            try {
                be beVar = (be) getChildAt(i);
                int i2 = firstVisiblePosition + i + 1;
                ImageView a3 = beVar.a();
                if (a3.getDrawable() == null) {
                    new Cif(this, beVar, i2, a2, a3).start();
                }
            } catch (Exception e) {
                c.a((Throwable) e);
            }
        }
    }

    public final void setVisibility(int i) {
        super.setVisibility(i);
        if (i == 0) {
            setAdapter((ListAdapter) new gu(this));
        } else {
            setAdapter((ListAdapter) null);
        }
    }
}
