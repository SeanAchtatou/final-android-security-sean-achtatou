package udk.android.reader.view;

import android.view.MotionEvent;
import android.view.View;

final class b implements View.OnTouchListener {
    private /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.a.b || !this.a.c.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
            return false;
        }
        this.a.g = System.currentTimeMillis();
        return false;
    }
}
