package udk.android.reader.view.contents;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import java.io.File;
import udk.android.b.d;
import udk.android.reader.a.a;

final class b implements View.OnClickListener {
    private /* synthetic */ j a;
    private final /* synthetic */ ViewGroup b;
    private final /* synthetic */ File c;
    private final /* synthetic */ View d;

    b(j jVar, ViewGroup viewGroup, File file, View view) {
        this.a = jVar;
        this.b = viewGroup;
        this.c = file;
        this.d = view;
    }

    public final void onClick(View view) {
        a.a((Activity) this.b.getContext(), this.c.getAbsolutePath(), (String) null);
        d.a(this.d, 1157627903, 500);
    }
}
