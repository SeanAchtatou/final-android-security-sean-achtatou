package udk.android.reader.view;

import android.graphics.Rect;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import udk.android.reader.b.c;
import udk.android.reader.b.e;

public final class a extends Thread {
    private View a;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public Rect c;
    private View d;
    private List e = new ArrayList();
    private boolean f;
    /* access modifiers changed from: private */
    public long g;

    public a(View view, View view2) {
        this.a = view;
        this.d = view2;
    }

    private void b() {
        for (e i : this.e) {
            i.i();
        }
    }

    public final void a() {
        this.f = false;
    }

    public final void a(e eVar) {
        if (!this.e.contains(eVar)) {
            this.e.add(eVar);
        }
    }

    public final void a(boolean z) {
        this.b = z;
        if (this.b) {
            this.g = System.currentTimeMillis();
        }
    }

    public final void b(e eVar) {
        this.e.remove(eVar);
    }

    public final void run() {
        a(false);
        while (this.a.getHeight() <= 0) {
            try {
                Thread.sleep(100);
            } catch (Exception e2) {
                c.a(e2.getMessage(), e2);
            }
        }
        this.c = new Rect(this.a.getLeft(), this.a.getTop(), this.a.getRight(), this.a.getBottom());
        this.d.setOnTouchListener(new b(this));
        this.f = true;
        while (this.f) {
            try {
                Thread.sleep(1000);
            } catch (Exception e3) {
                c.a(e3.getMessage(), e3);
            }
            if (this.b && e.k && System.currentTimeMillis() - this.g > 7000) {
                b();
            }
        }
    }
}
