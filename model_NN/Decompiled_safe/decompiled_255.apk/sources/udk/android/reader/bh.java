package udk.android.reader;

import android.view.MenuItem;
import udk.android.reader.a.a;
import udk.android.reader.contents.ai;

final class bh implements MenuItem.OnMenuItemClickListener {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ ai b;

    bh(ContentsManagerActivity contentsManagerActivity, ai aiVar) {
        this.a = contentsManagerActivity;
        this.b = aiVar;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        a.a(this.a, this.b.d(), (String) null);
        return true;
    }
}
