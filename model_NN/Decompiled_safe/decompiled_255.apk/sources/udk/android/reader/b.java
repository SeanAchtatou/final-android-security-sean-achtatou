package udk.android.reader;

import android.preference.PreferenceManager;
import udk.android.a.e;

final class b implements e {
    private /* synthetic */ PDFReaderConfigurationActivity a;

    b(PDFReaderConfigurationActivity pDFReaderConfigurationActivity) {
        this.a = pDFReaderConfigurationActivity;
    }

    public final void a(int i) {
        PreferenceManager.getDefaultSharedPreferences(this.a).edit().putInt(this.a.getString(C0000R.string.conf_nightmode_bgcolor), i).commit();
    }
}
