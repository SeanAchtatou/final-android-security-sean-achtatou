package udk.android.reader;

final class aw implements Runnable {
    private /* synthetic */ PDFReaderActivity a;
    private final /* synthetic */ String b;

    aw(PDFReaderActivity pDFReaderActivity, String str) {
        this.a = pDFReaderActivity;
        this.b = str;
    }

    public final void run() {
        if (!this.a.h.j()) {
            this.a.h.a(true);
        }
        this.a.a(this.a.getIntent(), this.b);
    }
}
