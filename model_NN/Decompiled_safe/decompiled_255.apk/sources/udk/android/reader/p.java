package udk.android.reader;

import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;

final class p extends ShapeDrawable.ShaderFactory {
    private /* synthetic */ ApplicationActivity a;

    p(ApplicationActivity applicationActivity) {
        this.a = applicationActivity;
    }

    public final Shader resize(int i, int i2) {
        return new BitmapShader(BitmapFactory.decodeResource(this.a.getResources(), C0000R.drawable.back), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
    }
}
