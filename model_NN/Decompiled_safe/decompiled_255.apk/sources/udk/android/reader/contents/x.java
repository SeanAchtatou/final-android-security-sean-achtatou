package udk.android.reader.contents;

import android.app.Activity;
import android.content.DialogInterface;
import java.io.InputStream;

final class x implements DialogInterface.OnClickListener {
    private /* synthetic */ am a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ InputStream c;
    private final /* synthetic */ long d;
    private final /* synthetic */ String e;
    private final /* synthetic */ Runnable f;
    private final /* synthetic */ Runnable g;

    x(am amVar, Activity activity, InputStream inputStream, long j, String str, Runnable runnable, Runnable runnable2) {
        this.a = amVar;
        this.b = activity;
        this.c = inputStream;
        this.d = j;
        this.e = str;
        this.f = runnable;
        this.g = runnable2;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.a.a(this.b, this.c, this.d, this.e, this.f, this.g);
    }
}
