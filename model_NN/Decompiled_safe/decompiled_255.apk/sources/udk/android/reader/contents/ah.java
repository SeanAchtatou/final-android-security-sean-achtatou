package udk.android.reader.contents;

import android.app.Activity;
import android.content.DialogInterface;
import java.io.File;

final class ah implements DialogInterface.OnClickListener {
    private /* synthetic */ b a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ File[] c;
    private final /* synthetic */ File d;
    private final /* synthetic */ String e;

    ah(b bVar, Activity activity, File[] fileArr, File file, String str) {
        this.a = bVar;
        this.b = activity;
        this.c = fileArr;
        this.d = file;
        this.e = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.d(this.b, this.c, this.d, this.e);
    }
}
