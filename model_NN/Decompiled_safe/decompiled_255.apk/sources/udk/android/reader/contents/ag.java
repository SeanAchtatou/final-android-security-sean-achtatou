package udk.android.reader.contents;

import android.app.Activity;
import android.app.ProgressDialog;
import java.io.File;

final class ag extends Thread {
    private /* synthetic */ b a;
    private final /* synthetic */ File[] b;
    private final /* synthetic */ File c;
    private final /* synthetic */ Activity d;
    private final /* synthetic */ ProgressDialog e;
    private final /* synthetic */ String f;

    ag(b bVar, File[] fileArr, File file, Activity activity, ProgressDialog progressDialog, String str) {
        this.a = bVar;
        this.b = fileArr;
        this.c = file;
        this.d = activity;
        this.e = progressDialog;
        this.f = str;
    }

    public final void run() {
        for (File file : this.b) {
            File file2 = new File(String.valueOf(this.c.getAbsolutePath()) + "/" + file.getName());
            if (file.isDirectory()) {
                this.a.c(this.d, file, this.c);
            } else if (b.a(file)) {
                this.a.b(this.d, file, file2);
            }
        }
        this.d.runOnUiThread(new y(this, this.e, this.f, this.d));
    }
}
