package udk.android.reader.contents;

import android.app.Activity;
import android.app.ProgressDialog;
import java.io.File;
import java.io.InputStream;

final class z extends Thread {
    private /* synthetic */ b a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ InputStream c;
    private final /* synthetic */ String d;
    private final /* synthetic */ ap e;
    private final /* synthetic */ Runnable f;
    private final /* synthetic */ Runnable g;
    private final /* synthetic */ ProgressDialog h;

    z(b bVar, Activity activity, InputStream inputStream, String str, ap apVar, Runnable runnable, Runnable runnable2, ProgressDialog progressDialog) {
        this.a = bVar;
        this.b = activity;
        this.c = inputStream;
        this.d = str;
        this.e = apVar;
        this.f = runnable;
        this.g = runnable2;
        this.h = progressDialog;
    }

    public final void run() {
        this.b.runOnUiThread(new al(this, this.h, this.a.a(this.b, this.c, new File(this.d), this.e) ? this.f : this.g));
    }
}
