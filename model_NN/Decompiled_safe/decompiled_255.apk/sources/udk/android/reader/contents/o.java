package udk.android.reader.contents;

import java.io.File;
import java.io.FileFilter;

final class o implements FileFilter {
    private /* synthetic */ an a;

    o(an anVar) {
        this.a = anVar;
    }

    public final boolean accept(File file) {
        return file.isDirectory();
    }
}
