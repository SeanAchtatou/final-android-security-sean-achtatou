package udk.android.reader.contents;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import java.io.File;
import java.io.InputStream;
import udk.android.reader.C0000R;

final class am implements Runnable {
    final /* synthetic */ ab a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Activity c;
    private final /* synthetic */ InputStream d;
    private final /* synthetic */ long e;
    private final /* synthetic */ Runnable f;
    private final /* synthetic */ Runnable g;

    am(ab abVar, String str, Activity activity, InputStream inputStream, long j, Runnable runnable, Runnable runnable2) {
        this.a = abVar;
        this.b = str;
        this.c = activity;
        this.d = inputStream;
        this.e = j;
        this.f = runnable;
        this.g = runnable2;
    }

    public final void run() {
        if (!new File(this.b).exists()) {
            this.a.a.a(this.c, this.d, this.e, this.b, this.f, this.g);
            return;
        }
        new AlertDialog.Builder(this.c).setTitle((int) C0000R.string.f100__).setMessage(String.valueOf(this.c.getResources().getString(C0000R.string.f102_)) + "\n" + this.b).setPositiveButton((int) C0000R.string.f50, new x(this, this.c, this.d, this.e, this.b, this.f, this.g)).setNegativeButton((int) C0000R.string.f53, (DialogInterface.OnClickListener) null).show();
    }
}
