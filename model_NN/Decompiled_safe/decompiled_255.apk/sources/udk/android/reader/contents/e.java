package udk.android.reader.contents;

import android.content.Context;
import android.content.DialogInterface;
import java.io.File;

final class e implements DialogInterface.OnClickListener {
    private /* synthetic */ b a;
    private final /* synthetic */ File[] b;
    private final /* synthetic */ Context c;

    e(b bVar, File[] fileArr, Context context) {
        this.a = bVar;
        this.b = fileArr;
        this.c = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        for (File file : this.b) {
            if (file.isDirectory()) {
                this.a.d(this.c, file);
            } else if (b.a(file)) {
                this.a.c(this.c, file);
            }
        }
    }
}
