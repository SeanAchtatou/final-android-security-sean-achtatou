package udk.android.reader.contents;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class ao implements Runnable {
    private /* synthetic */ an a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ EditText c;

    ao(an anVar, Context context, EditText editText) {
        this.a = anVar;
        this.b = context;
        this.c = editText;
    }

    public final void run() {
        ((InputMethodManager) this.b.getSystemService("input_method")).showSoftInput(this.c, 1);
    }
}
