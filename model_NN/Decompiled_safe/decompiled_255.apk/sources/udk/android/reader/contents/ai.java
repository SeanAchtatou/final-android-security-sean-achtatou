package udk.android.reader.contents;

import java.io.File;
import java.text.SimpleDateFormat;
import org.apache.commons.io.b;

public final class ai {
    private static final SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private int b;
    private String c;
    private String d;
    private String e;
    private String f;

    public static String a(File file) {
        return a.format(Long.valueOf(file.lastModified()));
    }

    public static String b(File file) {
        return file.isDirectory() ? b.a(b.b(file)) : b.a(file.length());
    }

    public final String a() {
        return a(new File(this.e));
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final String b() {
        return b(new File(this.e));
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final String d() {
        return this.e;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final String e() {
        return this.f;
    }

    public final int f() {
        return this.b;
    }
}
