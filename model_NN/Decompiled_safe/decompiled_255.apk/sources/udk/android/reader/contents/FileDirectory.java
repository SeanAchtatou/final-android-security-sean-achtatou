package udk.android.reader.contents;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileDirectory implements Serializable, Comparable {
    private static final long serialVersionUID = -2641862015193253786L;
    private File dir;
    private List files;

    public boolean checkAndAdd(File file) {
        if (this.files == null) {
            this.files = new ArrayList();
        }
        if (this.files.contains(file)) {
            return false;
        }
        this.files.add(file);
        Collections.sort(this.files);
        return true;
    }

    public int compareTo(FileDirectory fileDirectory) {
        return this.dir.compareTo(fileDirectory.getDir());
    }

    public FileDirectory copy() {
        FileDirectory fileDirectory = new FileDirectory();
        fileDirectory.setDir(getDir());
        fileDirectory.setFiles(new ArrayList(this.files));
        return fileDirectory;
    }

    public File getDir() {
        return this.dir;
    }

    public File getFile(int i) {
        return (File) this.files.get(i);
    }

    public boolean isEqualDirectory(File file) {
        return this.dir.equals(file);
    }

    public boolean isSubDirectoryOf(File file) {
        return !isEqualDirectory(file) && this.dir.getAbsolutePath().indexOf(file.getAbsolutePath()) >= 0;
    }

    public boolean seekAndRemove(File file) {
        return this.files.remove(file);
    }

    public void setDir(File file) {
        this.dir = file;
    }

    public void setFiles(List list) {
        this.files = list;
    }

    public int size() {
        return this.files.size();
    }
}
