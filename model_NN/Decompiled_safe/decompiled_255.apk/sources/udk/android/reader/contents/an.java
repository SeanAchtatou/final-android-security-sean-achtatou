package udk.android.reader.contents;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.Toast;
import com.unidocs.commonlib.util.b;
import com.unidocs.commonlib.util.c;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import udk.android.reader.C0000R;
import udk.android.reader.b.a;

public final class an implements a {
    private static an a;
    private List b;
    private List c;
    private List d;
    private List e;
    private int f = 1;
    private File g;
    private boolean h;
    /* access modifiers changed from: private */
    public AlertDialog i;
    private List j = new ArrayList();

    private an() {
        b.a().a(this);
    }

    static /* synthetic */ void a(an anVar, Context context, File file, String str) {
        b.a().a(context, file, new File(String.valueOf(anVar.g.getAbsolutePath()) + "/" + ((!b.a(file) || str.toLowerCase().endsWith(".pdf")) ? str : String.valueOf(str) + ".pdf")));
    }

    public static an c() {
        if (a == null) {
            a = new an();
        }
        return a;
    }

    private void f() {
        for (m a2 : this.j) {
            a2.a();
        }
    }

    public final void a() {
    }

    public final void a(int i2) {
        this.f = i2;
    }

    public final void a(Activity activity) {
        int i2 = 0;
        if (b.a((Collection) this.d)) {
            File[] fileArr = new File[this.d.size()];
            while (i2 < this.d.size()) {
                fileArr[i2] = new File(((ai) this.d.get(i2)).d());
                i2++;
            }
            b.a().a(activity, fileArr, this.g, activity.getString(C0000R.string.f133_));
        } else if (b.a((Collection) this.e)) {
            File[] fileArr2 = new File[this.e.size()];
            while (i2 < this.e.size()) {
                fileArr2[i2] = new File(((ai) this.e.get(i2)).d());
                i2++;
            }
            b.a().c(activity, fileArr2, this.g, activity.getString(C0000R.string.f133_));
        }
        this.c = null;
        this.d = null;
        this.e = null;
        f();
    }

    public final void a(Context context) {
        if (b.b((Collection) this.c)) {
            new AlertDialog.Builder(context).setTitle((int) C0000R.string.f49).setMessage((int) C0000R.string.f130).setPositiveButton((int) C0000R.string.f50, (DialogInterface.OnClickListener) null).show();
            return;
        }
        this.e = null;
        this.d = this.c;
        this.c = null;
        Toast.makeText(context, (int) C0000R.string.f131_, 0).show();
        f();
    }

    public final void a(Context context, File file) {
        EditText editText = new EditText(context);
        editText.setInputType(1);
        editText.setImeOptions(6);
        editText.setOnEditorActionListener(new q(this, editText, context, file));
        editText.setSingleLine();
        this.i = new AlertDialog.Builder(context).setTitle((int) C0000R.string.f94).setView(editText).setPositiveButton((int) C0000R.string.f50, new p(this, editText, context, file)).setNegativeButton((int) C0000R.string.f53, new s(this)).show();
        editText.postDelayed(new t(this, context, editText), 100);
    }

    public final void a(Context context, String str) {
        File file = new File(str);
        if (file.exists() && file.isDirectory()) {
            this.g = file;
            f(context);
            this.c = null;
        }
    }

    public final void a(File file) {
        this.g = file;
    }

    public final void a(ai aiVar) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        if (!this.c.contains(aiVar)) {
            this.c.add(aiVar);
        }
    }

    public final void a(l lVar) {
        if (lVar.a.getParentFile().equals(this.g)) {
            f(lVar.b);
        }
    }

    public final void a(m mVar) {
        if (!this.j.contains(mVar)) {
            this.j.add(mVar);
        }
    }

    public final void a(boolean z) {
        if (this.h != z) {
            this.h = z;
            c cVar = new c();
            cVar.a = z;
            for (m a2 : this.j) {
                a2.a(cVar);
            }
        }
    }

    public final void b() {
    }

    public final void b(Context context) {
        if (b.b((Collection) this.c)) {
            new AlertDialog.Builder(context).setTitle((int) C0000R.string.f49).setMessage((int) C0000R.string.f130).setPositiveButton((int) C0000R.string.f50, (DialogInterface.OnClickListener) null).show();
            return;
        }
        this.d = null;
        this.e = this.c;
        this.c = null;
        Toast.makeText(context, (int) C0000R.string.f132_, 0).show();
        f();
    }

    public final void b(ai aiVar) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        if (this.c.contains(aiVar)) {
            this.c.remove(aiVar);
        }
    }

    public final void b(l lVar) {
        if (lVar.a.getParentFile().equals(this.g)) {
            f(lVar.b);
        }
    }

    public final void b(m mVar) {
        this.j.remove(mVar);
    }

    public final void c(Context context) {
        if (b.b((Collection) this.c)) {
            new AlertDialog.Builder(context).setTitle((int) C0000R.string.f49).setMessage((int) C0000R.string.f130).setPositiveButton((int) C0000R.string.f50, (DialogInterface.OnClickListener) null).show();
            return;
        }
        File[] fileArr = new File[this.c.size()];
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.c.size()) {
                b.a().a(context, fileArr);
                this.d = null;
                this.e = null;
                this.c = null;
                f();
                return;
            }
            fileArr[i3] = new File(((ai) this.c.get(i3)).d());
            i2 = i3 + 1;
        }
    }

    public final void c(l lVar) {
        if (lVar.a.getParentFile().equals(this.g)) {
            f(lVar.b);
        }
    }

    public final boolean c(ai aiVar) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        return this.c.contains(aiVar);
    }

    public final void d(l lVar) {
        if (lVar.a.getParentFile().equals(this.g) || this.g.getAbsolutePath().indexOf(lVar.a.getAbsolutePath()) >= 0) {
            this.g = a.b(lVar.b);
            f(lVar.b);
        }
    }

    public final boolean d() {
        return this.h;
    }

    public final boolean d(Context context) {
        if (this.g == null || this.g.equals(a.b(context))) {
            return false;
        }
        a(context, this.g.getParentFile().getAbsolutePath());
        return true;
    }

    public final int e() {
        return this.f;
    }

    public final List e(Context context) {
        if (this.b == null) {
            f(context);
        }
        return this.b;
    }

    public final void e(l lVar) {
    }

    public final void f(Context context) {
        ArrayList arrayList = new ArrayList();
        if (!this.g.equals(a.b(context))) {
            File parentFile = this.g.getParentFile();
            ai aiVar = new ai();
            aiVar.a(11);
            aiVar.a("..");
            aiVar.b(parentFile.getName());
            aiVar.c(parentFile.getAbsolutePath());
            arrayList.add(aiVar);
        }
        File[] listFiles = this.g.listFiles(new o(this));
        try {
            listFiles = c.a(this.f, listFiles);
        } catch (Exception e2) {
            udk.android.reader.b.c.a(e2.getMessage(), e2);
        }
        for (File file : listFiles) {
            ai aiVar2 = new ai();
            aiVar2.a(11);
            aiVar2.a(file.getName());
            aiVar2.b(file.getName());
            aiVar2.c(file.getAbsolutePath());
            arrayList.add(aiVar2);
        }
        File[] listFiles2 = this.g.listFiles(new n(this));
        try {
            listFiles2 = c.a(this.f, listFiles2);
        } catch (Exception e3) {
            udk.android.reader.b.c.a(e3.getMessage(), e3);
        }
        for (File file2 : listFiles2) {
            ai aiVar3 = new ai();
            aiVar3.a(21);
            aiVar3.a(file2.getName());
            aiVar3.b(file2.getName());
            aiVar3.c(file2.getAbsolutePath());
            String c2 = a.c(context, file2);
            if (new File(c2).exists()) {
                aiVar3.d(c2);
            }
            arrayList.add(aiVar3);
        }
        this.b = arrayList;
        f();
    }

    public final void f(l lVar) {
        f(lVar.b);
    }

    public final void g(Context context) {
        EditText editText = new EditText(context);
        editText.setInputType(1);
        editText.setImeOptions(6);
        editText.setOnEditorActionListener(new u(this, editText, context));
        editText.setSingleLine();
        this.i = new AlertDialog.Builder(context).setTitle((int) C0000R.string.f157).setView(editText).setPositiveButton((int) C0000R.string.f50, new v(this, editText, context)).setNegativeButton((int) C0000R.string.f53, new r(this)).show();
        editText.postDelayed(new ao(this, context, editText), 100);
    }
}
