package udk.android.reader.contents;

import android.app.ProgressDialog;

final class al implements Runnable {
    private /* synthetic */ z a;
    private final /* synthetic */ ProgressDialog b;
    private final /* synthetic */ Runnable c;

    al(z zVar, ProgressDialog progressDialog, Runnable runnable) {
        this.a = zVar;
        this.b = progressDialog;
        this.c = runnable;
    }

    public final void run() {
        this.b.dismiss();
        if (this.c != null) {
            this.c.run();
        }
    }
}
