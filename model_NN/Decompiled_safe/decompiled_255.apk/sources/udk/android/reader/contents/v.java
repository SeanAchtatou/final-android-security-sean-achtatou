package udk.android.reader.contents;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import java.io.File;

final class v implements DialogInterface.OnClickListener {
    private /* synthetic */ an a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Context c;

    v(an anVar, EditText editText, Context context) {
        this.a = anVar;
        this.b = editText;
        this.c = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        b.a().a(this.c, new File(String.valueOf(this.a.g.getAbsolutePath()) + "/" + this.b.getText().toString()));
        this.a.i = null;
    }
}
