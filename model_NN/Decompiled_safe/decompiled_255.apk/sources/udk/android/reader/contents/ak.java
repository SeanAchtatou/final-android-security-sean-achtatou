package udk.android.reader.contents;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.Toast;
import com.unidocs.commonlib.util.b;

final class ak implements Runnable {
    private /* synthetic */ d a;
    private final /* synthetic */ ProgressDialog b;
    private final /* synthetic */ String c;
    private final /* synthetic */ Activity d;

    ak(d dVar, ProgressDialog progressDialog, String str, Activity activity) {
        this.a = dVar;
        this.b = progressDialog;
        this.c = str;
        this.d = activity;
    }

    public final void run() {
        this.b.dismiss();
        if (b.a(this.c)) {
            Toast.makeText(this.d, this.c, 0).show();
        }
    }
}
