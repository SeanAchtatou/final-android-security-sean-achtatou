package udk.android.reader.contents;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;
import com.unidocs.commonlib.util.i;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import udk.android.reader.C0000R;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.b.e;

public final class b {
    private static b a;
    private List b = new ArrayList();
    /* access modifiers changed from: private */
    public List c;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public boolean e;

    private b() {
    }

    public static b a() {
        if (a == null) {
            a = new b();
        }
        return a;
    }

    /* access modifiers changed from: private */
    public boolean a(Context context, InputStream inputStream, File file, ap apVar) {
        FileOutputStream fileOutputStream;
        Throwable th;
        boolean z;
        int read;
        try {
            File parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();
            }
            try {
                fileOutputStream = new FileOutputStream(file);
                try {
                    byte[] bArr = new byte[10240];
                    while (!apVar.a && (read = inputStream.read(bArr)) >= 0) {
                        fileOutputStream.write(bArr, 0, read);
                        apVar.b += (long) read;
                    }
                    fileOutputStream.flush();
                    i.a(new Object[]{inputStream, fileOutputStream});
                    if (apVar.a) {
                        file.delete();
                        return false;
                    }
                    if (this.c != null && file.getAbsolutePath().indexOf(a.b(context).getAbsolutePath()) < 0) {
                        File parentFile2 = file.getParentFile();
                        Iterator it = this.c.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                FileDirectory fileDirectory = (FileDirectory) it.next();
                                if (fileDirectory.isEqualDirectory(parentFile2)) {
                                    fileDirectory.checkAndAdd(file);
                                    z = true;
                                    break;
                                }
                            } else {
                                z = false;
                                break;
                            }
                        }
                        if (!z) {
                            FileDirectory fileDirectory2 = new FileDirectory();
                            fileDirectory2.setDir(parentFile2);
                            fileDirectory2.checkAndAdd(file);
                            this.c.add(fileDirectory2);
                            Collections.sort(this.c);
                        }
                    }
                    l lVar = new l();
                    lVar.a = file;
                    lVar.b = context;
                    b(lVar);
                    return true;
                } catch (Throwable th2) {
                    th = th2;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                fileOutputStream = null;
                th = th4;
                i.a(new Object[]{inputStream, fileOutputStream});
                throw th;
            }
        } catch (Exception e2) {
            c.a(e2.getMessage(), e2);
            return false;
        }
    }

    public static boolean a(File file) {
        return file.isFile() && file.getAbsolutePath().toLowerCase().endsWith(".pdf");
    }

    public static List b(Context context) {
        ObjectInputStream objectInputStream;
        File a2 = e.a(context);
        if (!a2.exists() || System.currentTimeMillis() - a2.lastModified() >= e.c) {
            return null;
        }
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(a2));
            try {
                List list = (List) objectInputStream.readObject();
                i.a(objectInputStream);
                return list;
            } catch (Exception e2) {
                e = e2;
                try {
                    c.a((Throwable) e);
                    i.a(objectInputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    i.a(objectInputStream);
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            objectInputStream = null;
            c.a((Throwable) e);
            i.a(objectInputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            objectInputStream = null;
            i.a(objectInputStream);
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void b(Context context, File file, File file2) {
        boolean z;
        try {
            if (!file.equals(file2)) {
                org.apache.commons.io.b.a(file, file2);
                File file3 = new File(a.b(context, file));
                if (file3.exists()) {
                    org.apache.commons.io.b.b(file3, new File(a.b(context, file2)));
                }
                if (this.c != null && file2.getAbsolutePath().indexOf(a.b(context).getAbsolutePath()) < 0) {
                    File parentFile = file2.getParentFile();
                    Iterator it = this.c.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            FileDirectory fileDirectory = (FileDirectory) it.next();
                            if (fileDirectory.isEqualDirectory(parentFile)) {
                                fileDirectory.checkAndAdd(file2);
                                z = true;
                                break;
                            }
                        } else {
                            z = false;
                            break;
                        }
                    }
                    if (!z) {
                        FileDirectory fileDirectory2 = new FileDirectory();
                        fileDirectory2.setDir(parentFile);
                        fileDirectory2.checkAndAdd(file2);
                        this.c.add(fileDirectory2);
                        Collections.sort(this.c);
                    }
                }
                l lVar = new l();
                lVar.a = file2;
                lVar.b = context;
                b(lVar);
            }
        } catch (Exception e2) {
            c.a(e2.getMessage(), e2);
        }
    }

    static /* synthetic */ void b(b bVar) {
        Iterator it = bVar.b.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    private void b(l lVar) {
        for (a a2 : this.b) {
            a2.a(lVar);
        }
    }

    public static boolean b(List list) {
        try {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                FileDirectory fileDirectory = (FileDirectory) it.next();
                int i = 0;
                while (true) {
                    if (i < fileDirectory.size()) {
                        if (!fileDirectory.getFile(i).exists()) {
                            return false;
                        }
                        i++;
                    }
                }
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void c(Context context, File file) {
        file.delete();
        String b2 = a.b(context, file);
        if (com.unidocs.commonlib.util.b.a(b2)) {
            File file2 = new File(b2);
            if (file2.exists()) {
                com.unidocs.commonlib.util.c.a(file2);
            }
        }
        if (this.c != null && file.getAbsolutePath().indexOf(a.b(context).getAbsolutePath()) < 0) {
            int size = this.c.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                FileDirectory fileDirectory = (FileDirectory) this.c.get(size);
                if (!fileDirectory.seekAndRemove(file)) {
                    size--;
                } else if (fileDirectory.size() <= 0) {
                    this.c.remove(fileDirectory);
                }
            }
        }
        l lVar = new l();
        lVar.a = file;
        lVar.b = context;
        c(lVar);
    }

    /* access modifiers changed from: private */
    public void c(Context context, File file, File file2) {
        try {
            if (!file.equals(file2)) {
                File file3 = new File(String.valueOf(file2.getAbsolutePath()) + File.separator + file.getName());
                if (!file3.exists()) {
                    file3.mkdirs();
                }
                org.apache.commons.io.b.a(file, file3, new j(this));
                e(context, file3);
            }
        } catch (Exception e2) {
            c.a(e2.getMessage(), e2);
        }
    }

    static /* synthetic */ void c(b bVar, Context context, File file, File file2) {
        if (!file.equals(file2)) {
            bVar.c(context, file, file2);
            bVar.d(context, file);
        }
    }

    private void c(l lVar) {
        for (a b2 : this.b) {
            b2.b(lVar);
        }
    }

    /* access modifiers changed from: private */
    public void d(Context context, File file) {
        com.unidocs.commonlib.util.c.a(file);
        if (this.c != null && file.getAbsolutePath().indexOf(a.b(context).getAbsolutePath()) < 0) {
            for (int size = this.c.size() - 1; size >= 0; size--) {
                FileDirectory fileDirectory = (FileDirectory) this.c.get(size);
                if (fileDirectory.isEqualDirectory(file) || fileDirectory.isSubDirectoryOf(file)) {
                    this.c.remove(fileDirectory);
                }
            }
        }
        l lVar = new l();
        lVar.a = file;
        lVar.b = context;
        e(lVar);
    }

    static /* synthetic */ void d(b bVar) {
        for (a b2 : bVar.b) {
            b2.b();
        }
    }

    static /* synthetic */ void d(b bVar, Context context, File file, File file2) {
        if (!file.equals(file2)) {
            bVar.b(context, file, file2);
            bVar.c(context, file);
        }
    }

    private void d(l lVar) {
        for (a c2 : this.b) {
            c2.c(lVar);
        }
    }

    private void e(Context context, File file) {
        l lVar = new l();
        lVar.b = context;
        lVar.a = file;
        if (file.isDirectory()) {
            d(lVar);
        } else if (a(file)) {
            b(lVar);
        }
        File[] listFiles = file.listFiles(new k(this));
        if (com.unidocs.commonlib.util.b.a((Object[]) listFiles)) {
            for (File e2 : listFiles) {
                e(context, e2);
            }
        }
    }

    static /* synthetic */ void e(b bVar) {
        for (a a2 : bVar.b) {
            a2.a();
        }
    }

    private void e(l lVar) {
        for (a d2 : this.b) {
            d2.d(lVar);
        }
    }

    private void f(l lVar) {
        for (a e2 : this.b) {
            e2.e(lVar);
        }
    }

    public final void a(Activity activity, InputStream inputStream, long j, String str, Runnable runnable, Runnable runnable2) {
        ap apVar = new ap();
        boolean z = j > 0;
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(true);
        if (z) {
            progressDialog.setProgressStyle(1);
            progressDialog.setMax((int) j);
            progressDialog.setProgress(0);
        } else {
            progressDialog.setProgressStyle(0);
            progressDialog.setMessage(activity.getString(C0000R.string.f82));
        }
        progressDialog.setOnCancelListener(new aa(this, apVar));
        progressDialog.show();
        z zVar = new z(this, activity, inputStream, str, apVar, runnable, runnable2, progressDialog);
        zVar.start();
        if (z) {
            new ae(this, zVar, progressDialog, activity, apVar).start();
        }
    }

    public final void a(Activity activity, String str, String str2) {
        ad adVar = new ad(this, activity);
        ac acVar = new ac(this, activity);
        Toast.makeText(activity, (int) C0000R.string.f124, 0).show();
        new ab(this, str, activity, acVar, a.b(activity) + "/" + str2, adVar).start();
    }

    public final void a(Activity activity, File[] fileArr) {
        a(activity, fileArr, a.b(activity), activity.getString(C0000R.string.f127));
    }

    public final void a(Activity activity, File[] fileArr, File file, String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        int length = fileArr.length;
        int i = 0;
        while (i < length) {
            File file2 = fileArr[i];
            File file3 = new File(String.valueOf(file.getAbsolutePath()) + "/" + file2.getName());
            if (file3.exists()) {
                arrayList.add(file3.getName());
            }
            if (!file2.isDirectory() || file3.getAbsolutePath().indexOf(file2.getAbsolutePath()) < 0) {
                i++;
            } else {
                Toast.makeText(activity, activity.getString(C0000R.string.f103___), 0).show();
                return;
            }
        }
        if (com.unidocs.commonlib.util.b.b((Collection) arrayList)) {
            b(activity, fileArr, file, str);
            return;
        }
        String string = activity.getResources().getString(C0000R.string.f102_);
        for (String str2 : arrayList) {
            string = String.valueOf(string) + "\n" + str2;
        }
        new AlertDialog.Builder(activity).setTitle((int) C0000R.string.f100__).setMessage(string).setPositiveButton((int) C0000R.string.f50, new af(this, activity, fileArr, file, str)).setNegativeButton((int) C0000R.string.f53, (DialogInterface.OnClickListener) null).show();
    }

    public final void a(Context context) {
        File a2 = e.a(context);
        if (!e.b || this.d) {
            if (a2.exists()) {
                a2.delete();
            }
        } else if (!com.unidocs.commonlib.util.b.b((Collection) this.c)) {
            ObjectOutputStream objectOutputStream = null;
            try {
                ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(new FileOutputStream(a2));
                try {
                    objectOutputStream2.writeObject(this.c);
                    objectOutputStream2.flush();
                    i.a(objectOutputStream2);
                } catch (Exception e2) {
                    e = e2;
                    objectOutputStream = objectOutputStream2;
                    try {
                        c.a((Throwable) e);
                        i.a(objectOutputStream);
                    } catch (Throwable th) {
                        th = th;
                        i.a(objectOutputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    objectOutputStream = objectOutputStream2;
                    i.a(objectOutputStream);
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                c.a((Throwable) e);
                i.a(objectOutputStream);
            }
        }
    }

    public final void a(Context context, File file) {
        if (file.exists()) {
            new AlertDialog.Builder(context).setTitle((int) C0000R.string.f92_).setMessage((int) C0000R.string.f95).setPositiveButton((int) C0000R.string.f50, (DialogInterface.OnClickListener) null).show();
            return;
        }
        file.mkdirs();
        l lVar = new l();
        lVar.a = file;
        lVar.b = context;
        d(lVar);
    }

    public final void a(Context context, File file, File file2) {
        l lVar = new l();
        lVar.a = file;
        lVar.b = context;
        if (file2.isDirectory()) {
            e(lVar);
        } else if (a(file2)) {
            c(lVar);
        }
        File file3 = new File(a.b(context, file));
        File file4 = new File(a.b(context, file2));
        file.renameTo(file2);
        file3.renameTo(file4);
        l lVar2 = new l();
        lVar2.a = file2;
        lVar2.b = context;
        if (file2.isDirectory()) {
            d(lVar2);
        } else if (a(file2)) {
            b(lVar2);
        }
    }

    public final void a(Context context, File[] fileArr) {
        new AlertDialog.Builder(context).setTitle((int) C0000R.string.f96__).setMessage((int) C0000R.string.f98).setPositiveButton((int) C0000R.string.f50, new e(this, fileArr, context)).setNegativeButton((int) C0000R.string.f53, (DialogInterface.OnClickListener) null).show();
    }

    public final void a(View view) {
        if (!this.d) {
            new f(this, view).start();
        }
    }

    public final void a(View view, File file, List list, File[] fileArr, String str, l lVar) {
        if (file != null && !com.unidocs.commonlib.util.b.a(file, fileArr)) {
            File[] listFiles = file.listFiles(new g(this, str));
            if (com.unidocs.commonlib.util.b.a((Object[]) listFiles)) {
                try {
                    listFiles = com.unidocs.commonlib.util.c.a(1, listFiles);
                } catch (Exception e2) {
                    c.a(e2.getMessage(), e2);
                }
                FileDirectory fileDirectory = new FileDirectory();
                fileDirectory.setDir(file);
                ArrayList arrayList = new ArrayList();
                for (File add : listFiles) {
                    arrayList.add(add);
                }
                fileDirectory.setFiles(arrayList);
                if (lVar == null) {
                    list.add(fileDirectory);
                } else {
                    view.post(new h(this, list, fileDirectory, lVar));
                }
            }
            File[] listFiles2 = file.listFiles(new i(this));
            if (lVar != null) {
                lVar.d++;
                if (com.unidocs.commonlib.util.b.a((Object[]) listFiles2)) {
                    lVar.c += listFiles2.length;
                }
                f(lVar);
            }
            if (com.unidocs.commonlib.util.b.a((Object[]) listFiles2)) {
                for (File a2 : listFiles2) {
                    a(view, a2, list, fileArr, str, lVar);
                }
            }
        }
    }

    public final void a(List list) {
        this.c = list;
    }

    public final void a(a aVar) {
        if (!this.b.contains(aVar)) {
            this.b.add(aVar);
        }
    }

    public final void a(l lVar) {
        for (a f : this.b) {
            f.f(lVar);
        }
    }

    public final List b() {
        return this.c;
    }

    public final void b(Activity activity, File[] fileArr) {
        c(activity, fileArr, a.b(activity), activity.getString(C0000R.string.f129));
    }

    public final void b(Activity activity, File[] fileArr, File file, String str) {
        new ag(this, fileArr, file, activity, ProgressDialog.show(activity, null, activity.getString(C0000R.string.f82), true, false), str).start();
    }

    public final void b(Context context, File file) {
        FileDirectory fileDirectory;
        if (this.c != null && !a.a(context, file)) {
            File parentFile = file.getParentFile();
            Iterator it = this.c.iterator();
            while (true) {
                if (it.hasNext()) {
                    fileDirectory = (FileDirectory) it.next();
                    if (fileDirectory.isEqualDirectory(parentFile)) {
                        break;
                    }
                } else {
                    fileDirectory = null;
                    break;
                }
            }
            if (fileDirectory == null) {
                fileDirectory = new FileDirectory();
                fileDirectory.setDir(parentFile);
                this.c.add(fileDirectory);
                Collections.sort(this.c);
            }
            fileDirectory.checkAndAdd(file);
        }
        l lVar = new l();
        lVar.a = file;
        lVar.b = context;
        b(lVar);
    }

    public final void b(a aVar) {
        this.b.remove(aVar);
    }

    public final void c(Activity activity, File[] fileArr, File file, String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        int length = fileArr.length;
        int i = 0;
        while (i < length) {
            File file2 = fileArr[i];
            File file3 = new File(String.valueOf(file.getAbsolutePath()) + "/" + file2.getName());
            if (file3.exists()) {
                arrayList.add(file3.getName());
            }
            if (!file2.isDirectory() || file3.getAbsolutePath().indexOf(file2.getAbsolutePath()) < 0) {
                i++;
            } else {
                Toast.makeText(activity, activity.getString(C0000R.string.f103___), 0).show();
                return;
            }
        }
        if (com.unidocs.commonlib.util.b.b((Collection) arrayList)) {
            d(activity, fileArr, file, str);
            return;
        }
        String string = activity.getResources().getString(C0000R.string.f102_);
        for (String str2 : arrayList) {
            string = String.valueOf(string) + "\n" + str2;
        }
        new AlertDialog.Builder(activity).setTitle((int) C0000R.string.f101__).setMessage(string).setPositiveButton((int) C0000R.string.f50, new ah(this, activity, fileArr, file, str)).setNegativeButton((int) C0000R.string.f53, (DialogInterface.OnClickListener) null).show();
    }

    public final void d(Activity activity, File[] fileArr, File file, String str) {
        new d(this, fileArr, file, activity, ProgressDialog.show(activity, null, activity.getString(C0000R.string.f82), true, false), str).start();
    }
}
