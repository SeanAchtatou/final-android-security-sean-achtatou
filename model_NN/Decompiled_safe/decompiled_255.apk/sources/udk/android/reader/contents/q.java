package udk.android.reader.contents;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import java.io.File;

final class q implements TextView.OnEditorActionListener {
    private /* synthetic */ an a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Context c;
    private final /* synthetic */ File d;

    q(an anVar, EditText editText, Context context, File file) {
        this.a = anVar;
        this.b = editText;
        this.c = context;
        this.d = file;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return true;
        }
        if (this.a.i != null && this.a.i.isShowing()) {
            this.a.i.dismiss();
        }
        an.a(this.a, this.c, this.d, this.b.getText().toString());
        this.a.i = null;
        return true;
    }
}
