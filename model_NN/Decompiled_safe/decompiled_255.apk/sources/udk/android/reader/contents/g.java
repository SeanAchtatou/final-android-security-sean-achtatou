package udk.android.reader.contents;

import com.unidocs.commonlib.util.b;
import java.io.File;
import java.io.FileFilter;

final class g implements FileFilter {
    private /* synthetic */ b a;
    private final /* synthetic */ String b;

    g(b bVar, String str) {
        this.a = bVar;
        this.b = str;
    }

    public final boolean accept(File file) {
        if (!b.a(file)) {
            return false;
        }
        if (b.b(this.b)) {
            return true;
        }
        return file.getName().toLowerCase().indexOf(this.b.toLowerCase()) >= 0;
    }
}
