package udk.android.reader.contents;

import android.app.Activity;
import android.app.ProgressDialog;
import udk.android.reader.b.c;

final class ae extends Thread {
    private /* synthetic */ b a;
    private final /* synthetic */ Thread b;
    private final /* synthetic */ ProgressDialog c;
    private final /* synthetic */ Activity d;
    private final /* synthetic */ ap e;

    ae(b bVar, Thread thread, ProgressDialog progressDialog, Activity activity, ap apVar) {
        this.a = bVar;
        this.b = thread;
        this.c = progressDialog;
        this.d = activity;
        this.e = apVar;
    }

    public final void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
            if (!this.b.isAlive() || !this.c.isShowing()) {
                c.a("## OBSERVING END");
            } else {
                this.d.runOnUiThread(new w(this, this.c, this.e));
            }
        }
        c.a("## OBSERVING END");
    }
}
