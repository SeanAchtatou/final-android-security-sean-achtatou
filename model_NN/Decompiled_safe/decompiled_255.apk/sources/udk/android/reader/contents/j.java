package udk.android.reader.contents;

import java.io.File;
import java.io.FileFilter;

final class j implements FileFilter {
    private /* synthetic */ b a;

    j(b bVar) {
        this.a = bVar;
    }

    public final boolean accept(File file) {
        return file.isDirectory() || b.a(file);
    }
}
