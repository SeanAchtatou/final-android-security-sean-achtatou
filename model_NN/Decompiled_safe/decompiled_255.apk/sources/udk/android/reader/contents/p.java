package udk.android.reader.contents;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import java.io.File;

final class p implements DialogInterface.OnClickListener {
    private /* synthetic */ an a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Context c;
    private final /* synthetic */ File d;

    p(an anVar, EditText editText, Context context, File file) {
        this.a = anVar;
        this.b = editText;
        this.c = context;
        this.d = file;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        an.a(this.a, this.c, this.d, this.b.getText().toString());
        this.a.i = null;
    }
}
