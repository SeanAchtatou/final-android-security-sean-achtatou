package udk.android.reader.contents;

import java.io.File;
import java.io.FileFilter;

final class i implements FileFilter {
    private /* synthetic */ b a;

    i(b bVar) {
        this.a = bVar;
    }

    public final boolean accept(File file) {
        return file.isDirectory() && !file.getName().startsWith(".");
    }
}
