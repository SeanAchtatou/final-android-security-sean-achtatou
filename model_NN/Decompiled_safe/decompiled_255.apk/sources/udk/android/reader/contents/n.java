package udk.android.reader.contents;

import java.io.File;
import java.io.FileFilter;

final class n implements FileFilter {
    private /* synthetic */ an a;

    n(an anVar) {
        this.a = anVar;
    }

    public final boolean accept(File file) {
        return file.isFile() && file.getAbsolutePath().toLowerCase().endsWith(".pdf");
    }
}
