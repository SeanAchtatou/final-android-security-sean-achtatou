package udk.android.reader.contents;

import android.view.View;
import java.io.File;
import udk.android.b.m;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.b.e;

final class f extends Thread {
    final /* synthetic */ b a;
    private final /* synthetic */ View b;

    f(b bVar, View view) {
        this.a = bVar;
        this.b = view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.contents.b.a(udk.android.reader.contents.b, boolean):void
     arg types: [udk.android.reader.contents.b, int]
     candidates:
      udk.android.reader.contents.b.a(udk.android.reader.contents.b, java.util.List):void
      udk.android.reader.contents.b.a(android.app.Activity, java.io.File[]):void
      udk.android.reader.contents.b.a(android.content.Context, java.io.File):void
      udk.android.reader.contents.b.a(android.content.Context, java.io.File[]):void
      udk.android.reader.contents.b.a(udk.android.reader.contents.b, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.contents.b.b(udk.android.reader.contents.b, boolean):void
     arg types: [udk.android.reader.contents.b, int]
     candidates:
      udk.android.reader.contents.b.b(android.app.Activity, java.io.File[]):void
      udk.android.reader.contents.b.b(android.content.Context, java.io.File):void
      udk.android.reader.contents.b.b(udk.android.reader.contents.b, boolean):void */
    public final void run() {
        this.a.d = true;
        this.a.e = false;
        this.b.post(new aj(this));
        while (!this.a.e) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                c.a((Throwable) e);
            }
        }
        l lVar = new l();
        b.b(this.a);
        lVar.c = 1;
        lVar.d = 0;
        this.a.a(this.b, e.a(), this.a.c, new File[]{a.a(this.b.getContext()), a.b(this.b.getContext())}, (String) null, lVar);
        if (m.a()) {
            try {
                File file = new File("/mnt/media");
                if (file.exists() && file.isDirectory()) {
                    this.a.a(this.b, file, this.a.c, new File[]{a.a(this.b.getContext()), a.b(this.b.getContext())}, (String) null, lVar);
                } else {
                    File file2 = new File("/media");
                    if (file2.exists() && file2.isDirectory()) {
                        this.a.a(this.b, file2, this.a.c, new File[]{a.a(this.b.getContext()), a.b(this.b.getContext())}, (String) null, lVar);
                    }
                }
            } catch (Exception e2) {
                c.a((Throwable) e2);
            }
        }
        b.d(this.a);
        this.a.d = false;
    }
}
