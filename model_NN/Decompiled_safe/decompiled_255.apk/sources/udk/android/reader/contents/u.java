package udk.android.reader.contents;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import java.io.File;

final class u implements TextView.OnEditorActionListener {
    private /* synthetic */ an a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Context c;

    u(an anVar, EditText editText, Context context) {
        this.a = anVar;
        this.b = editText;
        this.c = context;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return true;
        }
        if (this.a.i != null && this.a.i.isShowing()) {
            this.a.i.dismiss();
        }
        b.a().a(this.c, new File(String.valueOf(this.a.g.getAbsolutePath()) + "/" + this.b.getText().toString()));
        this.a.i = null;
        return true;
    }
}
