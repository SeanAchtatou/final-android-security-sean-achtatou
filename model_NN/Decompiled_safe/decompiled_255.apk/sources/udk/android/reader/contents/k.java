package udk.android.reader.contents;

import java.io.File;
import java.io.FileFilter;

final class k implements FileFilter {
    private /* synthetic */ b a;

    k(b bVar) {
        this.a = bVar;
    }

    public final boolean accept(File file) {
        return file.isDirectory() || b.a(file);
    }
}
