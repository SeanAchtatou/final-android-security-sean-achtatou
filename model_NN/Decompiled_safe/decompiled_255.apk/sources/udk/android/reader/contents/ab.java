package udk.android.reader.contents;

import android.app.Activity;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import udk.android.reader.b.c;

final class ab extends Thread {
    final /* synthetic */ b a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Activity c;
    private final /* synthetic */ Runnable d;
    private final /* synthetic */ String e;
    private final /* synthetic */ Runnable f;

    ab(b bVar, String str, Activity activity, Runnable runnable, String str2, Runnable runnable2) {
        this.a = bVar;
        this.b = str;
        this.c = activity;
        this.d = runnable;
        this.e = str2;
        this.f = runnable2;
    }

    public final void run() {
        long j;
        InputStream inputStream;
        long j2 = 0;
        try {
            URLConnection openConnection = new URL(this.b).openConnection();
            try {
                j2 = Long.parseLong(openConnection.getHeaderField("Content-Length"));
            } catch (Exception e2) {
            }
            j = j2;
            inputStream = openConnection.getInputStream();
        } catch (Exception e3) {
            c.a((Throwable) e3);
            this.c.runOnUiThread(this.d);
            j = j2;
            inputStream = null;
        }
        this.c.runOnUiThread(new am(this, this.e, this.c, inputStream, j, this.f, this.d));
    }
}
