package udk.android.reader;

import android.app.Activity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import java.io.File;
import udk.android.reader.b.c;
import udk.android.reader.view.contents.AllPDFListView;

public class AllPDFListActivity extends Activity {
    private AllPDFListView a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.AllPDFListActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public void onBackPressed() {
        if (ApplicationActivity.a(this)) {
            ApplicationActivity.a((Activity) this, false);
        } else {
            finish();
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        return true;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        View inflate = View.inflate(this, C0000R.layout.all_pdf_list, null);
        this.a = (AllPDFListView) inflate.findViewById(C0000R.id.list);
        this.a.a((ProgressBar) inflate.findViewById(C0000R.id.progress));
        setContentView(inflate);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        try {
            if (view.getId() == C0000R.id.content) {
                File file = (File) this.a.getItemAtPosition(this.a.getPositionForView(view));
                contextMenu.setHeaderTitle(String.valueOf(getString(C0000R.string.f89)) + " : " + file.getName());
                contextMenu.add(0, 1, 0, getString(C0000R.string.f126)).setOnMenuItemClickListener(new bo(this, view, file));
                contextMenu.add(0, 2, 0, getString(C0000R.string.f128)).setOnMenuItemClickListener(new bn(this, view, file));
                contextMenu.add(0, 3, 0, getString(C0000R.string.f97_)).setOnMenuItemClickListener(new bp(this, file));
            }
        } catch (Throwable th) {
            c.a(th);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.AllPDFListActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!ApplicationActivity.a(this)) {
            return true;
        }
        ApplicationActivity.a((Activity) this, false);
        return true;
    }
}
