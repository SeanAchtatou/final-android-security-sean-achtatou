package udk.android.reader.broadcast;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import udk.android.reader.b.c;

public class HeadsetPlugReceiver extends BroadcastReceiver {
    private static Runnable a;
    private static Runnable b;
    private static HeadsetPlugReceiver c;
    private static long d;
    private boolean e;

    private HeadsetPlugReceiver() {
    }

    public static void a(Activity activity) {
        try {
            if (c != null) {
                activity.unregisterReceiver(c);
            }
        } catch (Throwable th) {
            c.a(th);
        }
    }

    public static void a(Activity activity, Runnable runnable, Runnable runnable2) {
        try {
            if (c == null) {
                c = new HeadsetPlugReceiver();
            }
            d = System.currentTimeMillis();
            a = runnable;
            b = runnable2;
            activity.registerReceiver(c, new IntentFilter("android.intent.action.HEADSET_PLUG"));
        } catch (Throwable th) {
            c.a(th);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r5, android.content.Intent r6) {
        /*
            r4 = this;
            r2 = 1
            r1 = 0
            java.lang.String r0 = "state"
            boolean r0 = r6.hasExtra(r0)
            if (r0 == 0) goto L_0x0054
            boolean r0 = r4.e
            if (r0 == 0) goto L_0x0036
            java.lang.String r0 = "state"
            int r0 = r6.getIntExtra(r0, r1)
            if (r0 != 0) goto L_0x0036
            r4.e = r1
            r0 = r2
        L_0x0019:
            if (r0 == 0) goto L_0x0035
            boolean r0 = r4.e
            if (r0 == 0) goto L_0x0046
            java.lang.Runnable r0 = udk.android.reader.broadcast.HeadsetPlugReceiver.a
            if (r0 == 0) goto L_0x0046
            long r0 = java.lang.System.currentTimeMillis()
            long r2 = udk.android.reader.broadcast.HeadsetPlugReceiver.d
            long r0 = r0 - r2
            r2 = 1000(0x3e8, double:4.94E-321)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0035
            java.lang.Runnable r0 = udk.android.reader.broadcast.HeadsetPlugReceiver.a
            r0.run()
        L_0x0035:
            return
        L_0x0036:
            boolean r0 = r4.e
            if (r0 != 0) goto L_0x0054
            java.lang.String r0 = "state"
            int r0 = r6.getIntExtra(r0, r1)
            if (r0 != r2) goto L_0x0054
            r4.e = r2
            r0 = r2
            goto L_0x0019
        L_0x0046:
            boolean r0 = r4.e
            if (r0 != 0) goto L_0x0035
            java.lang.Runnable r0 = udk.android.reader.broadcast.HeadsetPlugReceiver.b
            if (r0 == 0) goto L_0x0035
            java.lang.Runnable r0 = udk.android.reader.broadcast.HeadsetPlugReceiver.b
            r0.run()
            goto L_0x0035
        L_0x0054:
            r0 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.broadcast.HeadsetPlugReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }
}
