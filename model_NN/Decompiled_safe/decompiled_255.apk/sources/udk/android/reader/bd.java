package udk.android.reader;

import android.content.Intent;
import android.widget.Toast;
import udk.android.reader.b.b;
import udk.android.reader.b.c;
import udk.android.reader.pdf.PDF;

final class bd implements Runnable {
    private /* synthetic */ aa a;

    bd(aa aaVar) {
        this.a = aaVar;
    }

    public final void run() {
        if (!PDF.a().okToCopy()) {
            Toast.makeText(this.a.a, b.m, 0).show();
            return;
        }
        PDFReaderActivity pDFReaderActivity = this.a.a;
        String G = this.a.a.d.G();
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.TEXT", G);
            intent.setType("text/plain");
            pDFReaderActivity.startActivity(intent);
        } catch (Exception e) {
            c.a((Throwable) e);
        }
        this.a.a.d.H();
    }
}
