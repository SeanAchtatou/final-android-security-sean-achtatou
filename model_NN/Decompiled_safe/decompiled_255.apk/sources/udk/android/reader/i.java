package udk.android.reader;

import android.app.Activity;
import android.view.MenuItem;
import android.view.View;
import java.io.File;
import udk.android.reader.contents.b;

final class i implements MenuItem.OnMenuItemClickListener {
    private /* synthetic */ RecentPDFListActivity a;
    private final /* synthetic */ View b;
    private final /* synthetic */ File c;

    i(RecentPDFListActivity recentPDFListActivity, View view, File file) {
        this.a = recentPDFListActivity;
        this.b = view;
        this.c = file;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        b.a().a((Activity) this.b.getContext(), new File[]{this.c});
        return true;
    }
}
