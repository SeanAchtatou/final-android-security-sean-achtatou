package udk.android.reader;

import udk.android.reader.pdf.as;
import udk.android.reader.view.pdf.PDFView;
import udk.android.reader.view.pdf.cl;

final class av implements as {
    private /* synthetic */ PDFReaderActivity a;

    av(PDFReaderActivity pDFReaderActivity) {
        this.a = pDFReaderActivity;
    }

    public final void a(int i) {
        if (this.a.d.F() == PDFView.ViewMode.THUMBNAIL) {
            this.a.d.a(PDFView.ViewMode.PDF);
        }
        this.a.d.a(i);
        cl U = this.a.d.U();
        if (U.d() || U.b()) {
            U.e();
        }
    }
}
