package udk.android.reader;

import java.io.File;
import udk.android.b.m;
import udk.android.reader.contents.b;
import udk.android.reader.contents.l;
import udk.android.reader.view.pdf.hx;

final class ax extends Thread {
    private /* synthetic */ PDFReaderActivity a;
    private final /* synthetic */ String b;
    private final /* synthetic */ File c;

    ax(PDFReaderActivity pDFReaderActivity, String str, File file) {
        this.a = pDFReaderActivity;
        this.b = str;
        this.c = file;
    }

    public final void run() {
        File file = new File(this.b);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        hx.a().a(this.b, (float) m.a(this.a, 100));
        l lVar = new l();
        lVar.b = this.a;
        lVar.a = this.c;
        b.a().a(lVar);
    }
}
