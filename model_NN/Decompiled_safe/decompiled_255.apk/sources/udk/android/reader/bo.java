package udk.android.reader;

import android.app.Activity;
import android.view.MenuItem;
import android.view.View;
import java.io.File;
import udk.android.reader.contents.b;

final class bo implements MenuItem.OnMenuItemClickListener {
    private /* synthetic */ AllPDFListActivity a;
    private final /* synthetic */ View b;
    private final /* synthetic */ File c;

    bo(AllPDFListActivity allPDFListActivity, View view, File file) {
        this.a = allPDFListActivity;
        this.b = view;
        this.c = file;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        b.a().a((Activity) this.b.getContext(), new File[]{this.c});
        return true;
    }
}
