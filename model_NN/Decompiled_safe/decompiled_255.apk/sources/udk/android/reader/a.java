package udk.android.reader;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

final class a implements Runnable {
    private /* synthetic */ bv a;
    private final /* synthetic */ EditText b;

    a(bv bvVar, EditText editText) {
        this.a = bvVar;
        this.b = editText;
    }

    public final void run() {
        ((InputMethodManager) this.a.a.getSystemService("input_method")).showSoftInput(this.b, 1);
    }
}
