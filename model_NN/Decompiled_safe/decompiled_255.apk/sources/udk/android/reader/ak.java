package udk.android.reader;

import android.view.View;
import udk.android.reader.pdf.a;
import udk.android.reader.view.pdf.PDFView;

final class ak implements View.OnClickListener {
    final /* synthetic */ PDFReaderActivity a;

    ak(PDFReaderActivity pDFReaderActivity) {
        this.a = pDFReaderActivity;
    }

    public final void onClick(View view) {
        if (this.a.d.F() == PDFView.ViewMode.THUMBNAIL) {
            this.a.d.a(PDFView.ViewMode.PDF);
        }
        a a2 = a.a();
        if (a2.d()) {
            a2.a(new az(this), this.a, this.a.getResources().getDrawable(C0000R.drawable.line), this.a.getString(C0000R.string.f153), this.a.getString(C0000R.string.f54));
            return;
        }
        a2.b();
        this.a.g.c();
    }
}
