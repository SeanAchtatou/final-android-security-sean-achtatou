package udk.android.reader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.unidocs.commonlib.util.b;
import java.io.File;
import java.io.InputStream;
import udk.android.b.j;
import udk.android.reader.a.f;
import udk.android.reader.b.c;
import udk.android.reader.broadcast.HeadsetPlugReceiver;
import udk.android.reader.pdf.ae;
import udk.android.reader.pdf.w;
import udk.android.reader.view.a;
import udk.android.reader.view.e;
import udk.android.reader.view.g;
import udk.android.reader.view.pdf.PDFView;
import udk.android.reader.view.pdf.bn;
import udk.android.reader.view.pdf.cv;
import udk.android.reader.view.pdf.hg;
import udk.android.reader.view.pdf.navigation.NavigationService;
import udk.android.reader.view.pdf.navigation.ab;

public class PDFReaderActivity extends Activity implements e, cv, ab {
    private static InputStream a;
    private static PDFReaderActivity b;
    /* access modifiers changed from: private */
    public ViewGroup c;
    /* access modifiers changed from: private */
    public PDFView d;
    private View e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public g g;
    /* access modifiers changed from: private */
    public NavigationService h;
    private a i;
    private boolean j;
    private boolean k;
    private boolean l;
    private f m;
    /* access modifiers changed from: private */
    public AlertDialog n;
    /* access modifiers changed from: private */
    public long o;

    /* access modifiers changed from: private */
    public void a(Intent intent, String str) {
        String stringExtra = intent.getStringExtra("booktitle");
        if (b.b(stringExtra)) {
            stringExtra = b.a(str) ? new File(str).getName() : intent.getData().getPath();
        }
        if (b.a(stringExtra)) {
            ((TextView) findViewById(C0000R.id.title_text)).setText(stringExtra);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, char]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private void a(String str) {
        String str2;
        String str3;
        String replace = str.replace('\\', File.separatorChar);
        replace.replaceAll("^file:[/]+", File.separator);
        String E = this.d.E();
        this.d.i();
        a();
        if (replace.startsWith("http://") || replace.startsWith("https://")) {
            udk.android.reader.a.a.a(this, replace);
            return;
        }
        int indexOf = replace.indexOf(35);
        if (indexOf >= 0) {
            String substring = replace.substring(indexOf + 1);
            str2 = replace.substring(0, indexOf);
            str3 = substring;
        } else {
            str2 = replace;
            str3 = null;
        }
        udk.android.reader.a.a.a(this, (!b.a(E) || str2.startsWith(File.separator)) ? str2 : new File(E).getParentFile() + File.separator + str2, str3);
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.d != null) {
            this.d.i();
            this.d = null;
            this.h.s();
            this.h.e();
            this.h.b(this);
            if (this.i != null) {
                this.i.b(this);
                this.i.a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        this.d.a(PDFView.ViewMode.PDF);
        udk.android.reader.pdf.a a2 = udk.android.reader.pdf.a.a();
        if (a2.d()) {
            a2.c();
        }
        udk.android.reader.pdf.b.a().a(this, getString(C0000R.string.f140_), getString(C0000R.string.f141_), getString(C0000R.string.f142_), getString(C0000R.string.f143_), getString(C0000R.string.f144_OR), getString(C0000R.string.f145_AND), getString(C0000R.string.f146), getString(C0000R.string.f50), getString(C0000R.string.f53), getResources().getDrawable(C0000R.drawable.expander_close));
        this.g.c();
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.d.F() == PDFView.ViewMode.THUMBNAIL) {
            this.d.a(PDFView.ViewMode.PDF);
        }
        w.a().a(this, new av(this), getResources().getDrawable(C0000R.drawable.expander_close), getResources().getDrawable(C0000R.drawable.expander_open), getResources().getDrawable(C0000R.drawable.item), getString(C0000R.string.f134), getString(C0000R.string.toc), getString(C0000R.string.f54), getString(C0000R.string.f135), getString(C0000R.string.f136), getString(C0000R.string.f137));
    }

    public final void a() {
        c.a("## ENDREADER CALLED");
        new ao(this, ProgressDialog.show(this, null, getString(C0000R.string.f82), true, false)).start();
    }

    public final void a(hg hgVar) {
        try {
            if (udk.android.reader.b.e.j) {
                this.d.post(new au(this));
            }
            this.h.i();
            this.h.c(hgVar.a);
            String E = this.d.E();
            if (b.a(E) && !this.d.x() && hgVar.a == 1) {
                File file = new File(E);
                String c2 = udk.android.reader.b.a.c(this, file);
                if (!new File(c2).exists() && !this.j) {
                    new ax(this, c2, file).start();
                }
            }
        } catch (Exception e2) {
            c.a((Throwable) e2);
        }
    }

    public final void b() {
        String E = this.d.E();
        if (b.a(E)) {
            runOnUiThread(new aw(this, E));
            udk.android.reader.contents.b.a().b(this, new File(E));
            ae.a().b(this, E);
        }
    }

    public final void b(hg hgVar) {
        this.h.d(hgVar.a);
    }

    public final void c() {
        this.h.t();
    }

    public final void c(hg hgVar) {
        udk.android.reader.pdf.c cVar = hgVar.d;
        int f2 = cVar.f();
        if (f2 == 1) {
            this.d.post(new ay(this, cVar));
        } else if (f2 == 4) {
            String h2 = cVar.h();
            if (!b.a(h2)) {
                return;
            }
            if (h2.replaceAll("#.*$", "").toLowerCase().endsWith(".pdf")) {
                a(h2);
            } else {
                udk.android.reader.a.a.a(this, h2.trim(), udk.android.reader.b.e.D);
            }
        } else if (f2 == 3) {
            a(cVar.h());
        }
    }

    public final void d() {
        int i2 = bn.a().b;
        if (i2 > 3) {
            if (!udk.android.reader.b.e.x) {
                j.a(this, !((getWindow().getAttributes().flags & 1024) != 0));
            }
        } else if (i2 > 2) {
            PDFView.ViewMode F = this.d.F();
            if (F == PDFView.ViewMode.PDF) {
                this.d.a(PDFView.ViewMode.TEXTREFLOW);
            } else if (F == PDFView.ViewMode.TEXTREFLOW) {
                this.d.a(PDFView.ViewMode.PDF);
            }
        } else if (i2 > 1) {
            this.d.r();
        } else {
            this.g.f();
        }
    }

    public final void e() {
        this.d.post(new at(this));
    }

    public final void f() {
        ImageView imageView = (ImageView) this.f.findViewById(C0000R.id.btn_viewmode);
        if (this.d.F() == PDFView.ViewMode.PDF) {
            imageView.setImageDrawable(this.d.getContext().getResources().getDrawable(C0000R.drawable.butt_text));
        } else {
            imageView.setImageDrawable(this.d.getContext().getResources().getDrawable(C0000R.drawable.butt_pdf));
        }
        g gVar = this.g;
        gVar.b(gVar.e());
        this.h.r();
    }

    public final void g() {
        this.g.d();
    }

    public final void h() {
        this.g.d();
    }

    public final void i() {
        if (!this.h.g() && !this.h.h() && !udk.android.reader.pdf.b.a().b()) {
            runOnUiThread(new as(this));
        }
    }

    public void onBackPressed() {
        c.a("## ON BACK PROCESSED");
        if (this.d.w()) {
            udk.android.reader.pdf.a a2 = udk.android.reader.pdf.a.a();
            if (a2.d()) {
                a2.c();
            } else if (this.h.g()) {
                udk.android.reader.pdf.b.a().c();
            } else if (this.d.F() != PDFView.ViewMode.PDF) {
                this.d.a(PDFView.ViewMode.PDF);
            } else if (this.k || !this.g.e()) {
                a();
            } else {
                this.g.a(false);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x04bb A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x04f5 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0506 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0516 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x05df A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0643 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0661 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x06a7 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0716 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x07d3 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0802 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0819 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x081e A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0852 A[Catch:{ Exception -> 0x0812, Throwable -> 0x0367 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r23) {
        /*
            r22 = this;
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "## ON CREATE PDFReader : "
            r5.<init>(r6)
            int r6 = android.os.Process.myPid()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = " "
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r5
            r1 = r22
            java.lang.StringBuilder r5 = r0.append(r1)
            java.lang.String r5 = r5.toString()
            udk.android.reader.b.c.a(r5)
            udk.android.reader.a.a.a(r22)
            udk.android.reader.PDFReaderActivity r5 = udk.android.reader.PDFReaderActivity.b
            if (r5 != 0) goto L_0x02d6
            r5 = 2131165346(0x7f0700a2, float:1.7944907E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.h = r5
            r5 = 2131165255(0x7f070047, float:1.7944722E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.g = r5
            r5 = 2131165246(0x7f07003e, float:1.7944704E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.q = r5
            r5 = 2131165241(0x7f070039, float:1.7944694E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.w = r5
            r5 = 2131165242(0x7f07003a, float:1.7944696E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.r = r5
            r5 = 2131165245(0x7f07003d, float:1.7944702E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.s = r5
            r5 = 2131165243(0x7f07003b, float:1.7944698E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.t = r5
            r5 = 2131165244(0x7f07003c, float:1.79447E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.u = r5
            r5 = 2131165248(0x7f070040, float:1.7944708E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.c = r5
            r5 = 2131165249(0x7f070041, float:1.794471E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.f = r5
            r5 = 2131165250(0x7f070042, float:1.7944712E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.d = r5
            r5 = 2131165251(0x7f070043, float:1.7944714E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.e = r5
            r5 = 2131165194(0x7f07000a, float:1.7944598E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.C = r5
            r5 = 2131165299(0x7f070073, float:1.7944811E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.O = r5
            r5 = 2131165300(0x7f070074, float:1.7944813E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.P = r5
            r5 = 2131165277(0x7f07005d, float:1.7944767E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.a = r5
            r5 = 2131165276(0x7f07005c, float:1.7944765E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.b = r5
            r5 = 2131165371(0x7f0700bb, float:1.7944957E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.l = r5
            r5 = 2131165369(0x7f0700b9, float:1.7944953E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.j = r5
            r5 = 2131165370(0x7f0700ba, float:1.7944955E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.k = r5
            r5 = 2131165372(0x7f0700bc, float:1.794496E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.i = r5
            r5 = 2131165202(0x7f070012, float:1.7944614E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.aj = r5
            r5 = 2131165261(0x7f07004d, float:1.7944734E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.D = r5
            r5 = 2131165262(0x7f07004e, float:1.7944736E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.E = r5
            r5 = 2131165263(0x7f07004f, float:1.7944738E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.F = r5
            r5 = 2131165264(0x7f070050, float:1.794474E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.G = r5
            r5 = 2131165265(0x7f070051, float:1.7944742E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.H = r5
            r5 = 2131165266(0x7f070052, float:1.7944744E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.I = r5
            r5 = 2131165267(0x7f070053, float:1.7944746E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.J = r5
            r5 = 2131165268(0x7f070054, float:1.7944748E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.K = r5
            r5 = 2131165269(0x7f070055, float:1.794475E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.L = r5
            r5 = 2131165270(0x7f070056, float:1.7944752E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.M = r5
            r5 = 2131165271(0x7f070057, float:1.7944754E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.N = r5
            r5 = 2131165256(0x7f070048, float:1.7944724E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.T = r5
            r5 = 2131165257(0x7f070049, float:1.7944726E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.V = r5
            r5 = 2131165305(0x7f070079, float:1.7944823E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.Q = r5
            r5 = 2131165306(0x7f07007a, float:1.7944825E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.R = r5
            r5 = 2131165254(0x7f070046, float:1.794472E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.S = r5
            r5 = 2131165301(0x7f070075, float:1.7944815E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.W = r5
            r5 = 2131165302(0x7f070076, float:1.7944817E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.Y = r5
            r5 = 2131165307(0x7f07007b, float:1.7944827E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.aa = r5
            r5 = 2131165308(0x7f07007c, float:1.794483E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.U = r5
            r5 = 2131165309(0x7f07007d, float:1.7944831E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.ad = r5
            r5 = 2131165310(0x7f07007e, float:1.7944834E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.ae = r5
            r5 = 2131165311(0x7f07007f, float:1.7944836E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.af = r5
            r5 = 2131165312(0x7f070080, float:1.7944838E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.ag = r5
            r5 = 2131165313(0x7f070081, float:1.794484E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.ah = r5
            r5 = 2131165314(0x7f070082, float:1.7944842E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.ai = r5
            r5 = 2131165252(0x7f070044, float:1.7944716E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.x = r5
            r5 = 2131165253(0x7f070045, float:1.7944718E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.y = r5
            r5 = 2131165315(0x7f070083, float:1.7944844E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.z = r5
            r5 = 2131165316(0x7f070084, float:1.7944846E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.A = r5
            r5 = 2131165317(0x7f070085, float:1.7944848E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.B = r5
            r5 = 2131165303(0x7f070077, float:1.794482E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.ab = r5
            r5 = 2131165304(0x7f070078, float:1.7944821E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.ac = r5
            r5 = 2131165364(0x7f0700b4, float:1.7944943E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.m = r5
            r5 = 2131165365(0x7f0700b5, float:1.7944945E38)
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)
            udk.android.reader.b.b.n = r5
        L_0x02d6:
            udk.android.reader.PDFReaderActivity r5 = udk.android.reader.PDFReaderActivity.b
            if (r5 == 0) goto L_0x02eb
            udk.android.reader.PDFReaderActivity r5 = udk.android.reader.PDFReaderActivity.b
            r0 = r5
            r1 = r22
            if (r0 == r1) goto L_0x02eb
            udk.android.reader.PDFReaderActivity r5 = udk.android.reader.PDFReaderActivity.b
            r5.j()
            udk.android.reader.PDFReaderActivity r5 = udk.android.reader.PDFReaderActivity.b
            r5.finish()
        L_0x02eb:
            udk.android.reader.PDFReaderActivity.b = r22
            udk.android.reader.a.f r5 = new udk.android.reader.a.f
            r0 = r5
            r1 = r22
            r0.<init>(r1)
            r0 = r5
            r1 = r22
            r1.m = r0
            udk.android.reader.w r11 = new udk.android.reader.w
            r0 = r11
            r1 = r22
            r0.<init>(r1)
            super.onCreate(r23)     // Catch:{ Throwable -> 0x0367 }
            android.content.Intent r5 = r22.getIntent()     // Catch:{ Throwable -> 0x0367 }
            android.net.Uri r6 = r5.getData()     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            if (r6 == 0) goto L_0x0317
            java.lang.String r10 = r6.getScheme()     // Catch:{ Throwable -> 0x0367 }
        L_0x0317:
            java.lang.String r12 = "params"
            java.lang.String r12 = r5.getStringExtra(r12)     // Catch:{ Throwable -> 0x0367 }
            if (r12 != 0) goto L_0x0321
            java.lang.String r12 = ""
        L_0x0321:
            java.lang.String r13 = "file"
            boolean r13 = r13.equals(r10)     // Catch:{ Throwable -> 0x0367 }
            if (r13 == 0) goto L_0x036c
            java.lang.String r6 = r6.getPath()     // Catch:{ Throwable -> 0x0367 }
            r13 = r8
            r14 = r6
            r6 = r12
            r12 = r9
        L_0x0331:
            if (r12 != 0) goto L_0x03d1
            if (r14 == 0) goto L_0x03d1
            java.io.File r7 = new java.io.File     // Catch:{ Throwable -> 0x0367 }
            r7.<init>(r14)     // Catch:{ Throwable -> 0x0367 }
            boolean r7 = r7.exists()     // Catch:{ Throwable -> 0x0367 }
            if (r7 != 0) goto L_0x03d1
            java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Throwable -> 0x0367 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0367 }
            r7 = 2131165373(0x7f0700bd, float:1.7944961E38)
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Throwable -> 0x0367 }
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "\n"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0367 }
            java.lang.StringBuilder r6 = r6.append(r14)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0367 }
            r5.<init>(r6)     // Catch:{ Throwable -> 0x0367 }
            throw r5     // Catch:{ Throwable -> 0x0367 }
        L_0x0367:
            r5 = move-exception
            r11.a(r5)
        L_0x036b:
            return
        L_0x036c:
            if (r10 == 0) goto L_0x03ac
            r13 = 2
            java.lang.String[] r13 = new java.lang.String[r13]     // Catch:{ Throwable -> 0x0367 }
            r14 = 0
            java.lang.String r15 = "http"
            r13[r14] = r15     // Catch:{ Throwable -> 0x0367 }
            r14 = 1
            java.lang.String r15 = "ezpdf"
            r13[r14] = r15     // Catch:{ Throwable -> 0x0367 }
            r14 = 1
            boolean r10 = com.unidocs.commonlib.util.b.a(r10, r13, r14)     // Catch:{ Throwable -> 0x0367 }
            if (r10 == 0) goto L_0x03ac
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r8 = "^ezpdf"
            java.lang.String r10 = "http"
            java.lang.String r6 = r6.replaceAll(r8, r10)     // Catch:{ Throwable -> 0x0367 }
            boolean r8 = com.unidocs.commonlib.util.b.b(r12)     // Catch:{ Throwable -> 0x0367 }
            if (r8 == 0) goto L_0x0870
            r8 = 35
            int r8 = r6.indexOf(r8)     // Catch:{ Throwable -> 0x0367 }
            if (r8 < 0) goto L_0x0870
            int r8 = r8 + 1
            java.lang.String r8 = r6.substring(r8)     // Catch:{ Throwable -> 0x0367 }
        L_0x03a2:
            java.net.URL r10 = new java.net.URL     // Catch:{ Throwable -> 0x0367 }
            r10.<init>(r6)     // Catch:{ Throwable -> 0x0367 }
            r6 = r8
            r12 = r9
            r13 = r10
            r14 = r7
            goto L_0x0331
        L_0x03ac:
            if (r6 == 0) goto L_0x03bf
            android.content.ContentResolver r9 = r22.getContentResolver()     // Catch:{ Throwable -> 0x0367 }
            java.io.InputStream r6 = r9.openInputStream(r6)     // Catch:{ Throwable -> 0x0367 }
            r13 = r8
            r14 = r7
            r21 = r12
            r12 = r6
            r6 = r21
            goto L_0x0331
        L_0x03bf:
            java.io.InputStream r6 = udk.android.reader.PDFReaderActivity.a     // Catch:{ Throwable -> 0x0367 }
            if (r6 == 0) goto L_0x086a
            java.io.InputStream r6 = udk.android.reader.PDFReaderActivity.a     // Catch:{ Throwable -> 0x0367 }
            r9 = 0
            udk.android.reader.PDFReaderActivity.a = r9     // Catch:{ Throwable -> 0x0367 }
            r13 = r8
            r14 = r7
            r21 = r12
            r12 = r6
            r6 = r21
            goto L_0x0331
        L_0x03d1:
            boolean r7 = udk.android.reader.b.e.m     // Catch:{ Throwable -> 0x0367 }
            if (r7 != 0) goto L_0x041b
            r7 = 0
            udk.android.reader.b.a.V = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            udk.android.reader.b.a.W = r7     // Catch:{ Throwable -> 0x0367 }
            if (r12 != 0) goto L_0x080f
            if (r14 == 0) goto L_0x080f
            r7 = 1
        L_0x03e0:
            udk.android.reader.b.a.f = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.l = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.aR = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.m = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.n = r7     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "activateLink"
            r8 = 1
            boolean r7 = r5.getBooleanExtra(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.b.a.j = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            udk.android.reader.b.a.k = r7     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "activateWordSelection"
            r8 = 1
            boolean r7 = r5.getBooleanExtra(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.b.a.h = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            udk.android.reader.b.a.i = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.o = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.p = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 2
            udk.android.reader.b.a.R = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.Z = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.g = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.L = r7     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            udk.android.reader.b.a.aK = r7     // Catch:{ Throwable -> 0x0367 }
        L_0x041b:
            udk.android.reader.b.e.c(r22)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "maxZoom"
            r8 = 0
            float r7 = r5.getFloatExtra(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            r8 = 1073741824(0x40000000, float:2.0)
            int r8 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r8 <= 0) goto L_0x042d
            udk.android.reader.b.a.ac = r7     // Catch:{ Throwable -> 0x0367 }
        L_0x042d:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r8 = "## OPEN PARAMS : "
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0367 }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.b.c.a(r7)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "prohibitGenerateThumbnail"
            r8 = 0
            boolean r7 = r5.getBooleanExtra(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            r0 = r7
            r1 = r22
            r1.j = r0     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            java.lang.String r8 = "page"
            java.lang.String r6 = com.unidocs.commonlib.util.h.b(r6, r8)     // Catch:{ Throwable -> 0x0367 }
            boolean r8 = com.unidocs.commonlib.util.b.a(r6)     // Catch:{ Throwable -> 0x0367 }
            if (r8 == 0) goto L_0x0816
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Exception -> 0x0812 }
        L_0x045c:
            java.lang.String r7 = "page"
            int r15 = r5.getIntExtra(r7, r6)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r6 = "navigationPageThumbnailType"
            int r7 = udk.android.reader.view.pdf.navigation.NavigationService.b     // Catch:{ Throwable -> 0x0367 }
            int r6 = r5.getIntExtra(r6, r7)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "keys1"
            java.lang.String r16 = r5.getStringExtra(r7)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "keys2"
            java.lang.String r17 = r5.getStringExtra(r7)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "navigationFix"
            r8 = 0
            boolean r7 = r5.getBooleanExtra(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            r0 = r7
            r1 = r22
            r1.k = r0     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "navigationMenuActivate"
            r8 = 1
            boolean r7 = r5.getBooleanExtra(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r8 = "noTitleBackground"
            r9 = 0
            boolean r8 = r5.getBooleanExtra(r8, r9)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r9 = "noTitleMenu"
            r10 = 0
            boolean r9 = r5.getBooleanExtra(r9, r10)     // Catch:{ Throwable -> 0x0367 }
            r0 = r9
            r1 = r22
            r1.l = r0     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r9 = "titleAlignLeft"
            r10 = 0
            boolean r9 = r5.getBooleanExtra(r9, r10)     // Catch:{ Throwable -> 0x0367 }
            android.widget.LinearLayout r10 = new android.widget.LinearLayout     // Catch:{ Throwable -> 0x0367 }
            r0 = r10
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r18 = 0
            r0 = r10
            r1 = r18
            r0.setOrientation(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            boolean r0 = r0.k     // Catch:{ Throwable -> 0x0367 }
            r18 = r0
            if (r18 == 0) goto L_0x0819
            r18 = 2130903058(0x7f030012, float:1.7412923E38)
        L_0x04be:
            r19 = 0
            r0 = r22
            r1 = r18
            r2 = r19
            android.view.View r23 = android.view.View.inflate(r0, r1, r2)     // Catch:{ Throwable -> 0x0367 }
            android.view.ViewGroup r23 = (android.view.ViewGroup) r23     // Catch:{ Throwable -> 0x0367 }
            android.widget.LinearLayout$LayoutParams r18 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Throwable -> 0x0367 }
            r19 = -1
            r20 = -1
            r18.<init>(r19, r20)     // Catch:{ Throwable -> 0x0367 }
            r19 = 1065353216(0x3f800000, float:1.0)
            r0 = r19
            r1 = r18
            r1.weight = r0     // Catch:{ Throwable -> 0x0367 }
            r0 = r10
            r1 = r23
            r0.addView(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r10
            r1 = r22
            r1.c = r0     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            android.view.ViewGroup r0 = r0.c     // Catch:{ Throwable -> 0x0367 }
            r10 = r0
            r0 = r22
            r1 = r10
            r0.setContentView(r1)     // Catch:{ Throwable -> 0x0367 }
            if (r7 != 0) goto L_0x0504
            r7 = 2131427387(0x7f0b003b, float:1.8476389E38)
            r0 = r22
            r1 = r7
            android.view.View r7 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            r10 = 8
            r7.setVisibility(r10)     // Catch:{ Throwable -> 0x0367 }
        L_0x0504:
            if (r8 == 0) goto L_0x0514
            r7 = 2131427406(0x7f0b004e, float:1.8476427E38)
            r0 = r22
            r1 = r7
            android.view.View r7 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            r8 = 0
            r7.setBackgroundColor(r8)     // Catch:{ Throwable -> 0x0367 }
        L_0x0514:
            if (r9 != 0) goto L_0x052a
            r7 = 2131427410(0x7f0b0052, float:1.8476435E38)
            r0 = r22
            r1 = r7
            android.view.View r23 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            android.widget.TextView r23 = (android.widget.TextView) r23     // Catch:{ Throwable -> 0x0367 }
            r7 = 17
            r0 = r23
            r1 = r7
            r0.setGravity(r1)     // Catch:{ Throwable -> 0x0367 }
        L_0x052a:
            r0 = r22
            r1 = r5
            r2 = r14
            r0.a(r1, r2)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427397(0x7f0b0045, float:1.847641E38)
            r0 = r22
            r1 = r5
            android.view.View r23 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.view.pdf.PDFView r23 = (udk.android.reader.view.pdf.PDFView) r23     // Catch:{ Throwable -> 0x0367 }
            r0 = r23
            r1 = r22
            r1.d = r0     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r0 = r5
            r1 = r22
            r0.a(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            android.content.res.Resources r7 = r22.getResources()     // Catch:{ Throwable -> 0x0367 }
            r8 = 2130968576(0x7f040000, float:1.754581E38)
            android.graphics.drawable.Drawable r23 = r7.getDrawable(r8)     // Catch:{ Throwable -> 0x0367 }
            android.graphics.drawable.AnimationDrawable r23 = (android.graphics.drawable.AnimationDrawable) r23     // Catch:{ Throwable -> 0x0367 }
            r0 = r5
            r1 = r23
            r0.a(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            udk.android.reader.x r7 = new udk.android.reader.x     // Catch:{ Throwable -> 0x0367 }
            r0 = r7
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r5.a(r7)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427406(0x7f0b004e, float:1.8476427E38)
            r0 = r22
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r5
            r1 = r22
            r1.f = r0     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            android.view.View r0 = r0.f     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r7 = 2131427416(0x7f0b0058, float:1.8476448E38)
            android.view.View r5 = r5.findViewById(r7)     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            r5.setVisibility(r7)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427369(0x7f0b0029, float:1.8476352E38)
            r0 = r22
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r5
            r1 = r22
            r1.e = r0     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.view.pdf.navigation.NavigationService r5 = udk.android.reader.view.pdf.navigation.NavigationService.a()     // Catch:{ Throwable -> 0x0367 }
            r0 = r5
            r1 = r22
            r1.h = r0     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.navigation.NavigationService r0 = r0.h     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r0 = r5
            r1 = r22
            r0.a(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.navigation.NavigationService r0 = r0.h     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r0 = r22
            android.view.View r0 = r0.e     // Catch:{ Throwable -> 0x0367 }
            r7 = r0
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r8 = r0
            r5.a(r7, r8, r6)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.navigation.NavigationService r0 = r0.h     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            udk.android.reader.view.pdf.navigation.NavigationService$SubMode r6 = udk.android.reader.view.pdf.navigation.NavigationService.SubMode.THUMBNAIL     // Catch:{ Throwable -> 0x0367 }
            r5.a(r6)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427395(0x7f0b0043, float:1.8476405E38)
            r0 = r22
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            if (r5 == 0) goto L_0x0606
            java.lang.String r6 = "## INIT NAVIGATION INTERACTION OBSERVING SERVICE"
            udk.android.reader.b.c.a(r6)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.view.a r6 = new udk.android.reader.view.a     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            android.view.View r0 = r0.e     // Catch:{ Throwable -> 0x0367 }
            r7 = r0
            r6.<init>(r7, r5)     // Catch:{ Throwable -> 0x0367 }
            r0 = r6
            r1 = r22
            r1.i = r0     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.a r0 = r0.i     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r0 = r5
            r1 = r22
            r0.a(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.a r0 = r0.i     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r5.start()     // Catch:{ Throwable -> 0x0367 }
        L_0x0606:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Throwable -> 0x0367 }
            r5.<init>()     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.view.pdf.menu.c r6 = new udk.android.reader.view.pdf.menu.c     // Catch:{ Throwable -> 0x0367 }
            r7 = 2131165258(0x7f07004a, float:1.7944728E38)
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.y r8 = new udk.android.reader.y     // Catch:{ Throwable -> 0x0367 }
            r0 = r8
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r6.<init>(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            r5.add(r6)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.view.pdf.menu.c r6 = new udk.android.reader.view.pdf.menu.c     // Catch:{ Throwable -> 0x0367 }
            r7 = 2131165259(0x7f07004b, float:1.794473E38)
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.z r8 = new udk.android.reader.z     // Catch:{ Throwable -> 0x0367 }
            r0 = r8
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r6.<init>(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            r5.add(r6)     // Catch:{ Throwable -> 0x0367 }
            boolean r6 = udk.android.reader.b.e.y     // Catch:{ Throwable -> 0x0367 }
            if (r6 == 0) goto L_0x065d
            udk.android.reader.view.pdf.menu.c r6 = new udk.android.reader.view.pdf.menu.c     // Catch:{ Throwable -> 0x0367 }
            r7 = 2131165260(0x7f07004c, float:1.7944732E38)
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.aa r8 = new udk.android.reader.aa     // Catch:{ Throwable -> 0x0367 }
            r0 = r8
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r6.<init>(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            r5.add(r6)     // Catch:{ Throwable -> 0x0367 }
        L_0x065d:
            boolean r6 = udk.android.reader.b.e.u     // Catch:{ Throwable -> 0x0367 }
            if (r6 == 0) goto L_0x0673
            udk.android.reader.view.pdf.menu.c r6 = new udk.android.reader.view.pdf.menu.c     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r7 = "ColorDict"
            udk.android.reader.ab r8 = new udk.android.reader.ab     // Catch:{ Throwable -> 0x0367 }
            r0 = r8
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r6.<init>(r7, r8)     // Catch:{ Throwable -> 0x0367 }
            r5.add(r6)     // Catch:{ Throwable -> 0x0367 }
        L_0x0673:
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r6 = r0
            r6.a(r5)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427396(0x7f0b0044, float:1.8476407E38)
            r0 = r22
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            r6 = 2
            android.view.View[] r6 = new android.view.View[r6]     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            r8 = 2131427417(0x7f0b0059, float:1.847645E38)
            android.view.View r8 = r5.findViewById(r8)     // Catch:{ Throwable -> 0x0367 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0367 }
            r7 = 1
            r0 = r22
            android.view.View r0 = r0.e     // Catch:{ Throwable -> 0x0367 }
            r8 = r0
            r9 = 2131427417(0x7f0b0059, float:1.847645E38)
            android.view.View r8 = r8.findViewById(r9)     // Catch:{ Throwable -> 0x0367 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0367 }
            boolean r7 = com.unidocs.commonlib.util.b.a(r6)     // Catch:{ Throwable -> 0x0367 }
            if (r7 == 0) goto L_0x06ea
            r7 = 2
            android.view.View[] r7 = new android.view.View[r7]     // Catch:{ Throwable -> 0x0367 }
            r8 = 0
            r0 = r22
            android.view.View r0 = r0.f     // Catch:{ Throwable -> 0x0367 }
            r9 = r0
            r10 = 2131427365(0x7f0b0025, float:1.8476344E38)
            android.view.View r9 = r9.findViewById(r10)     // Catch:{ Throwable -> 0x0367 }
            r7[r8] = r9     // Catch:{ Throwable -> 0x0367 }
            r8 = 1
            r9 = 2131427365(0x7f0b0025, float:1.8476344E38)
            android.view.View r9 = r5.findViewById(r9)     // Catch:{ Throwable -> 0x0367 }
            r7[r8] = r9     // Catch:{ Throwable -> 0x0367 }
            r8 = 2
            android.view.View[] r8 = new android.view.View[r8]     // Catch:{ Throwable -> 0x0367 }
            r9 = 0
            r0 = r22
            android.view.View r0 = r0.e     // Catch:{ Throwable -> 0x0367 }
            r10 = r0
            r18 = 2131427364(0x7f0b0024, float:1.8476342E38)
            r0 = r10
            r1 = r18
            android.view.View r10 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            r8[r9] = r10     // Catch:{ Throwable -> 0x0367 }
            r9 = 1
            r10 = 2131427364(0x7f0b0024, float:1.8476342E38)
            android.view.View r5 = r5.findViewById(r10)     // Catch:{ Throwable -> 0x0367 }
            r8[r9] = r5     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r5.a(r6, r7, r8)     // Catch:{ Throwable -> 0x0367 }
        L_0x06ea:
            udk.android.reader.view.g r5 = new udk.android.reader.view.g     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            android.view.ViewGroup r0 = r0.c     // Catch:{ Throwable -> 0x0367 }
            r6 = r0
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r7 = r0
            r0 = r22
            android.view.View r0 = r0.e     // Catch:{ Throwable -> 0x0367 }
            r8 = r0
            r0 = r22
            android.view.View r0 = r0.f     // Catch:{ Throwable -> 0x0367 }
            r9 = r0
            r0 = r22
            udk.android.reader.view.a r0 = r0.i     // Catch:{ Throwable -> 0x0367 }
            r10 = r0
            r5.<init>(r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x0367 }
            r0 = r5
            r1 = r22
            r1.g = r0     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            boolean r0 = r0.l     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            if (r5 != 0) goto L_0x0777
            udk.android.reader.ai r5 = new udk.android.reader.ai     // Catch:{ Throwable -> 0x0367 }
            r0 = r5
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.ah r6 = new udk.android.reader.ah     // Catch:{ Throwable -> 0x0367 }
            r0 = r6
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            android.view.View r0 = r0.f     // Catch:{ Throwable -> 0x0367 }
            r7 = r0
            r8 = 2131427407(0x7f0b004f, float:1.847643E38)
            android.view.View r7 = r7.findViewById(r8)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.ag r8 = new udk.android.reader.ag     // Catch:{ Throwable -> 0x0367 }
            r0 = r8
            r1 = r22
            r2 = r5
            r3 = r6
            r0.<init>(r1, r2, r3)     // Catch:{ Throwable -> 0x0367 }
            r7.setOnTouchListener(r8)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            android.view.View r0 = r0.f     // Catch:{ Throwable -> 0x0367 }
            r7 = r0
            r8 = 2131427408(0x7f0b0050, float:1.8476431E38)
            android.view.View r7 = r7.findViewById(r8)     // Catch:{ Throwable -> 0x0367 }
            r8 = 0
            r7.setVisibility(r8)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.af r8 = new udk.android.reader.af     // Catch:{ Throwable -> 0x0367 }
            r0 = r8
            r1 = r22
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ Throwable -> 0x0367 }
            r7.setOnClickListener(r8)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            android.view.View r0 = r0.f     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r7 = 2131427411(0x7f0b0053, float:1.8476437E38)
            android.view.View r5 = r5.findViewById(r7)     // Catch:{ Throwable -> 0x0367 }
            r7 = 0
            r5.setVisibility(r7)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.am r7 = new udk.android.reader.am     // Catch:{ Throwable -> 0x0367 }
            r0 = r7
            r1 = r22
            r2 = r6
            r0.<init>(r1, r2)     // Catch:{ Throwable -> 0x0367 }
            r5.setOnClickListener(r7)     // Catch:{ Throwable -> 0x0367 }
        L_0x0777:
            r0 = r22
            android.view.View r0 = r0.e     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r6 = 2131427362(0x7f0b0022, float:1.8476338E38)
            android.view.View r5 = r5.findViewById(r6)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.al r6 = new udk.android.reader.al     // Catch:{ Throwable -> 0x0367 }
            r0 = r6
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r5.setOnClickListener(r6)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427389(0x7f0b003d, float:1.8476393E38)
            r0 = r22
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.ak r6 = new udk.android.reader.ak     // Catch:{ Throwable -> 0x0367 }
            r0 = r6
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r5.setOnClickListener(r6)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427341(0x7f0b000d, float:1.8476296E38)
            r0 = r22
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.aj r6 = new udk.android.reader.aj     // Catch:{ Throwable -> 0x0367 }
            r0 = r6
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r5.setOnClickListener(r6)     // Catch:{ Throwable -> 0x0367 }
            r5 = 2131427393(0x7f0b0041, float:1.84764E38)
            r0 = r22
            r1 = r5
            android.view.View r5 = r0.findViewById(r1)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.ap r6 = new udk.android.reader.ap     // Catch:{ Throwable -> 0x0367 }
            r0 = r6
            r1 = r22
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0367 }
            r5.setOnClickListener(r6)     // Catch:{ Throwable -> 0x0367 }
            boolean r5 = com.unidocs.commonlib.util.b.a(r14)     // Catch:{ Throwable -> 0x0367 }
            if (r5 == 0) goto L_0x081e
            r0 = r22
            udk.android.reader.view.pdf.navigation.NavigationService r0 = r0.h     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r6 = 1
            r5.a(r6)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r0 = r5
            r1 = r14
            r2 = r16
            r3 = r17
            r4 = r15
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.pdf.ae r5 = udk.android.reader.pdf.ae.a()     // Catch:{ Throwable -> 0x0367 }
            r0 = r5
            r1 = r22
            r2 = r14
            r0.b(r1, r2)     // Catch:{ Throwable -> 0x0367 }
        L_0x07f6:
            java.lang.String r5 = r22.getPackageName()     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r6 = "udk.android.reader"
            boolean r5 = r5.equals(r6)     // Catch:{ Throwable -> 0x0367 }
            if (r5 == 0) goto L_0x0852
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r0 = r22
            r1 = r5
            udk.android.reader.a.j.a(r0, r1)     // Catch:{ Throwable -> 0x0367 }
            goto L_0x036b
        L_0x080f:
            r7 = 0
            goto L_0x03e0
        L_0x0812:
            r6 = move-exception
            udk.android.reader.b.c.a(r6)     // Catch:{ Throwable -> 0x0367 }
        L_0x0816:
            r6 = r7
            goto L_0x045c
        L_0x0819:
            r18 = 2130903056(0x7f030010, float:1.741292E38)
            goto L_0x04be
        L_0x081e:
            if (r13 == 0) goto L_0x0839
            r0 = r22
            udk.android.reader.view.pdf.navigation.NavigationService r0 = r0.h     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r6 = 0
            r5.a(r6)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r6 = r13
            r7 = r16
            r8 = r17
            r9 = r15
            r10 = r11
            r5.a(r6, r7, r8, r9, r10)     // Catch:{ Throwable -> 0x0367 }
            goto L_0x07f6
        L_0x0839:
            r0 = r22
            udk.android.reader.view.pdf.navigation.NavigationService r0 = r0.h     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r6 = 0
            r5.a(r6)     // Catch:{ Throwable -> 0x0367 }
            r0 = r22
            udk.android.reader.view.pdf.PDFView r0 = r0.d     // Catch:{ Throwable -> 0x0367 }
            r5 = r0
            r0 = r5
            r1 = r12
            r2 = r16
            r3 = r17
            r4 = r15
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x0367 }
            goto L_0x07f6
        L_0x0852:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r6 = "## DONT CHECK MARKET LICENSING : "
            r5.<init>(r6)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r6 = r22.getPackageName()     // Catch:{ Throwable -> 0x0367 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x0367 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0367 }
            udk.android.reader.b.c.a(r5)     // Catch:{ Throwable -> 0x0367 }
            goto L_0x036b
        L_0x086a:
            r6 = r12
            r13 = r8
            r14 = r7
            r12 = r9
            goto L_0x0331
        L_0x0870:
            r8 = r12
            goto L_0x03a2
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.reader.PDFReaderActivity.onCreate(android.os.Bundle):void");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (!this.k) {
            return true;
        }
        getMenuInflater().inflate(C0000R.menu.pdfview, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        c.a("## ON DESTROY");
        j();
        this.m.b();
        super.onDestroy();
        if (b == this) {
            b = null;
        }
        c.a("## PDFReader DESTROYED");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (udk.android.reader.b.e.l != 0) {
            if (i2 == 24) {
                if (udk.android.reader.b.e.l == 1) {
                    this.d.l();
                } else if (udk.android.reader.b.e.l == 2 && !this.d.u() && this.o < 1) {
                    this.d.a(0.01f);
                    this.o = System.currentTimeMillis();
                }
                return true;
            } else if (i2 == 25) {
                if (udk.android.reader.b.e.l == 1) {
                    this.d.m();
                } else if (udk.android.reader.b.e.l == 2 && !this.d.u() && this.o < 1) {
                    this.d.a(-0.01f);
                    this.o = System.currentTimeMillis();
                }
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (udk.android.reader.b.e.l == 0 || !(i2 == 24 || i2 == 25)) {
            PDFView.SmartNavDirection smartNavDirection = null;
            if (i2 == 20) {
                smartNavDirection = udk.android.reader.b.e.E ? PDFView.SmartNavDirection.BOTTOM : PDFView.SmartNavDirection.TOP;
            } else if (i2 == 51) {
                smartNavDirection = PDFView.SmartNavDirection.TOP;
            } else if (i2 == 19) {
                smartNavDirection = udk.android.reader.b.e.E ? PDFView.SmartNavDirection.TOP : PDFView.SmartNavDirection.BOTTOM;
            } else if (i2 == 47) {
                smartNavDirection = PDFView.SmartNavDirection.BOTTOM;
            } else if (i2 == 22) {
                smartNavDirection = udk.android.reader.b.e.E ? PDFView.SmartNavDirection.RIGHT : PDFView.SmartNavDirection.LEFT;
            } else if (i2 == 29) {
                smartNavDirection = PDFView.SmartNavDirection.LEFT;
            } else if (i2 == 21) {
                smartNavDirection = udk.android.reader.b.e.E ? PDFView.SmartNavDirection.LEFT : PDFView.SmartNavDirection.RIGHT;
            } else if (i2 == 32) {
                smartNavDirection = PDFView.SmartNavDirection.RIGHT;
            }
            if (smartNavDirection != null) {
                this.d.a(smartNavDirection);
            }
            return super.onKeyUp(i2, keyEvent);
        }
        if (udk.android.reader.b.e.l != 1 && udk.android.reader.b.e.l == 2) {
            if (System.currentTimeMillis() - this.o > 500) {
                this.d.v();
                this.o = 0;
            } else {
                this.d.postDelayed(new aq(this), 500);
            }
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == C0000R.id.menu_toc) {
            l();
            return true;
        } else if (menuItem.getItemId() == C0000R.id.menu_bookmark) {
            if (this.d.F() == PDFView.ViewMode.THUMBNAIL) {
                this.d.a(PDFView.ViewMode.PDF);
            }
            udk.android.reader.pdf.a.a().b();
            this.g.c();
            return true;
        } else if (menuItem.getItemId() != C0000R.id.menu_search) {
            return true;
        } else {
            k();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.m.b();
        if (udk.android.reader.b.e.A) {
            HeadsetPlugReceiver.a(this);
        }
        if (this.d != null) {
            this.d.j();
        }
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (this.k) {
            return true;
        }
        this.g.f();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.view.pdf.PDFView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.pdf.annotation.s, udk.android.reader.pdf.annotation.Annotation):java.lang.Runnable
      udk.android.reader.view.pdf.PDFView.a(android.content.Context, udk.android.reader.pdf.y):void
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.Runnable):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, android.app.AlertDialog):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, android.app.ProgressDialog):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, android.view.View):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, java.lang.Runnable):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.view.pdf.PDFView, boolean):void
      udk.android.reader.view.pdf.PDFView.a(float, float):void
      udk.android.reader.view.pdf.PDFView.a(int, int):void
      udk.android.reader.view.pdf.PDFView.a(android.view.MotionEvent, udk.android.reader.pdf.annotation.e):void
      udk.android.reader.view.pdf.PDFView.a(java.lang.String, java.lang.String):void
      udk.android.reader.view.pdf.PDFView.a(udk.android.reader.pdf.c, android.view.MotionEvent):void
      udk.android.reader.view.pdf.PDFView.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (udk.android.reader.b.e.t) {
            this.d.c(this.d.getWidth(), this.d.getHeight());
        }
        udk.android.reader.b.e.t = false;
        j.a(this, !udk.android.reader.b.a.aL);
        if (udk.android.reader.b.e.C) {
            this.m.a();
        } else {
            this.m.b();
        }
        if (getRequestedOrientation() != udk.android.reader.b.e.B) {
            setRequestedOrientation(udk.android.reader.b.e.B);
        }
        if (this.d != null && this.d.w() && this.d.L() == 0) {
            int K = this.d.K();
            this.d.a(udk.android.reader.b.a.aM, false);
            if (this.d.K() != K) {
                int z = this.d.z();
                if (z > 1) {
                    this.d.a(z - 1);
                    this.d.o();
                } else if (z < this.d.A()) {
                    this.d.a(z + 1);
                    this.d.n();
                }
            }
        }
        if (udk.android.reader.b.e.A) {
            HeadsetPlugReceiver.a(this, new ac(this), new ad(this));
        }
    }

    public boolean onSearchRequested() {
        k();
        return true;
    }
}
