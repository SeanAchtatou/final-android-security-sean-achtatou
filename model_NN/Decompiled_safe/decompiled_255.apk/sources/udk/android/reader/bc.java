package udk.android.reader;

import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;
import java.io.File;
import udk.android.reader.b.c;
import udk.android.reader.contents.ai;

final class bc implements MenuItem.OnMenuItemClickListener {
    private /* synthetic */ ContentsManagerActivity a;
    private final /* synthetic */ ai b;

    bc(ContentsManagerActivity contentsManagerActivity, ai aiVar) {
        this.a = contentsManagerActivity;
        this.b = aiVar;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        ContentsManagerActivity contentsManagerActivity = this.a;
        File file = new File(this.b.d());
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
            intent.setType("application/*");
            contentsManagerActivity.startActivity(intent);
            return true;
        } catch (Exception e) {
            c.a((Throwable) e);
            return true;
        }
    }
}
