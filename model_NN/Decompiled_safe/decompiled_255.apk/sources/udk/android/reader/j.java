package udk.android.reader;

import android.widget.Toast;
import udk.android.reader.a.a;
import udk.android.reader.b.b;
import udk.android.reader.pdf.PDF;

final class j implements Runnable {
    private /* synthetic */ z a;

    j(z zVar) {
        this.a = zVar;
    }

    public final void run() {
        if (!PDF.a().okToCopy()) {
            Toast.makeText(this.a.a, b.m, 0).show();
            return;
        }
        a.a(this.a.a, this.a.a.d, this.a.a.d.G());
        this.a.a.d.H();
    }
}
