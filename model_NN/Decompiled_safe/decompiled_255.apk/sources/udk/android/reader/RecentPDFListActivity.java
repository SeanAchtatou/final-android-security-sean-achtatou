package udk.android.reader;

import android.app.Activity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import java.io.File;
import udk.android.reader.b.a;
import udk.android.reader.pdf.ae;
import udk.android.reader.view.contents.RecentPDFListView;

public class RecentPDFListActivity extends Activity {
    private RecentPDFListView a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.RecentPDFListActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public void onBackPressed() {
        if (ApplicationActivity.a(this)) {
            ApplicationActivity.a((Activity) this, false);
        } else {
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.recent_pdf_list);
        this.a = (RecentPDFListView) findViewById(C0000R.id.recentpdf_list);
        findViewById(C0000R.id.btn_menu_reset_recentpdflist).setOnClickListener(new g(this));
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        if (view.getId() == C0000R.id.content) {
            contextMenu.setHeaderTitle((int) C0000R.string.f89);
            String str = (String) this.a.getItemAtPosition(this.a.getPositionForView(view));
            File file = new File(str);
            if (!file.exists()) {
                this.a.post(new h(this));
                ae.a().a(this, str);
                return;
            }
            if (str.indexOf(a.b(this).getAbsolutePath()) < 0) {
                contextMenu.add(0, 1, 0, getString(C0000R.string.f126)).setOnMenuItemClickListener(new i(this, view, file));
            }
            contextMenu.add(0, 2, 0, getString(C0000R.string.f88)).setOnMenuItemClickListener(new f(this, str));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void
     arg types: [udk.android.reader.RecentPDFListActivity, int]
     candidates:
      udk.android.reader.ApplicationActivity.a(udk.android.reader.ApplicationActivity, java.io.File):void
      udk.android.reader.ApplicationActivity.a(android.app.Activity, boolean):void */
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (ApplicationActivity.a(this)) {
            ApplicationActivity.a((Activity) this, false);
            return true;
        }
        View findViewById = findViewById(C0000R.id.option_menubar);
        if (findViewById.getVisibility() == 0) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this, C0000R.anim.slide_down_out);
            loadAnimation.setAnimationListener(new e(this, findViewById));
            findViewById.startAnimation(loadAnimation);
            return true;
        }
        findViewById.setVisibility(0);
        return true;
    }
}
