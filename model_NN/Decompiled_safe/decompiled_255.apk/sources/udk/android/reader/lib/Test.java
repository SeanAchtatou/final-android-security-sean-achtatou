package udk.android.reader.lib;

import android.app.Activity;
import android.os.Bundle;
import udk.android.reader.b.a;
import udk.android.reader.b.c;
import udk.android.reader.view.pdf.PDFView;

public class Test extends Activity {
    private PDFView a;

    public void onBackPressed() {
        c.a("## ON BACK PRESSED");
        if (this.a != null && this.a.w()) {
            this.a.i();
        }
        super.onBackPressed();
    }

    public void onCreate(Bundle bundle) {
        a.d = false;
        a.e = false;
        a.f = true;
        a.o = false;
        a.ap = 0.01f;
        a.aR = true;
        a.g = true;
        a.b = true;
        super.onCreate(bundle);
        this.a = new PDFView(this);
        setContentView(this.a);
        this.a.a("/sdcard/empty.pdf", (String) null, (String) null, 0);
    }
}
