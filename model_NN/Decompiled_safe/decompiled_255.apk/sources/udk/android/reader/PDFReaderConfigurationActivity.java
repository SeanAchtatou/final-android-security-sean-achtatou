package udk.android.reader;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import udk.android.a.e;
import udk.android.a.i;
import udk.android.b.c;
import udk.android.reader.b.a;

public class PDFReaderConfigurationActivity extends PreferenceActivity {
    private static int a() {
        return c.a(255, a.at, a.au, a.av);
    }

    private void a(e eVar, int i) {
        i.a(this, i, eVar, getString(C0000R.string.f62), getString(C0000R.string.f50), false);
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        PreferenceManager preferenceManager = getPreferenceManager();
        for (Preference enabled : new Preference[]{preferenceManager.findPreference(getString(C0000R.string.conf_pagescrolling_sensitivity)), preferenceManager.findPreference(getString(C0000R.string.conf_pagescrolling_disable_with_columnfitting))}) {
            enabled.setEnabled(z);
        }
    }

    private static int b() {
        return c.a(255, a.aw, a.ax, a.ay);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        PreferenceCategory preferenceCategory;
        udk.android.reader.a.a.a(this);
        super.onCreate(bundle);
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putString(getString(C0000R.string.conf_pagescrolling_type), new StringBuilder(String.valueOf(a.R)).toString());
        edit.putString(getString(C0000R.string.conf_pagescrolling_sensitivity), new StringBuilder(String.valueOf(a.T)).toString());
        edit.putBoolean(getString(C0000R.string.conf_pagescrolling_disable_with_columnfitting), a.U);
        edit.putString(getString(C0000R.string.conf_basic_zoom_ratio), new StringBuilder(String.valueOf(a.Z)).toString());
        edit.putBoolean(getString(C0000R.string.conf_pinchzoom_movable), a.aa);
        edit.putString(getString(C0000R.string.conf_key_action_volume), new StringBuilder(String.valueOf(udk.android.reader.b.e.l)).toString());
        edit.putString(getString(C0000R.string.conf_pagefliping_speed), new StringBuilder(String.valueOf(a.aq)).toString());
        edit.putString(getString(C0000R.string.conf_default_reading_direction), new StringBuilder(String.valueOf(a.aM)).toString());
        edit.putString(getString(C0000R.string.conf_screen_orientation), new StringBuilder(String.valueOf(udk.android.reader.b.e.B)).toString());
        edit.putBoolean(getString(C0000R.string.conf_screen_wakelock), udk.android.reader.b.e.C);
        edit.putBoolean(getString(C0000R.string.conf_dpad_invert), udk.android.reader.b.e.E);
        edit.putBoolean(getString(C0000R.string.conf_link_needconfirm), udk.android.reader.b.e.D);
        edit.putString(getString(C0000R.string.conf_nightmode_line_height_percent), new StringBuilder(String.valueOf(a.as)).toString());
        edit.putInt(getString(C0000R.string.conf_nightmode_fontcolor), a());
        edit.putInt(getString(C0000R.string.conf_nightmode_bgcolor), b());
        edit.putBoolean(getString(C0000R.string.conf_menu_opentime_enable), udk.android.reader.b.e.j);
        edit.putBoolean(getString(C0000R.string.conf_menu_auto_disable), udk.android.reader.b.e.k);
        edit.putBoolean(getString(C0000R.string.conf_tts_detect_headset_plug), udk.android.reader.b.e.A);
        edit.putBoolean(getString(C0000R.string.conf_notificationbar_readingtime_enable), a.aL);
        edit.commit();
        addPreferencesFromResource(C0000R.layout.pdf_reader_configuration);
        PreferenceManager preferenceManager = getPreferenceManager();
        if (udk.android.reader.b.e.x && (preferenceCategory = (PreferenceCategory) preferenceManager.findPreference(getString(C0000R.string.f23_))) != null) {
            preferenceCategory.removePreference(getPreferenceManager().findPreference(getString(C0000R.string.conf_notificationbar_readingtime_enable)));
        }
        ((ListPreference) preferenceManager.findPreference(getString(C0000R.string.conf_pagescrolling_type))).setOnPreferenceChangeListener(new d(this));
        a(a.R != 1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        int parseInt;
        int parseInt2;
        int parseInt3;
        float parseFloat;
        int parseInt4;
        int parseInt5;
        float parseFloat2;
        int parseInt6;
        udk.android.reader.b.c.a("## ON PDF READER CONFIGURATION PAUSED : TRY SYNC CONFIGURATION");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        com.unidocs.commonlib.util.a b = udk.android.reader.b.e.b(this);
        if (b != null) {
            try {
                int i = a.R;
                try {
                    parseInt6 = Integer.parseInt(defaultSharedPreferences.getString(getString(C0000R.string.conf_pagescrolling_type), new StringBuilder(String.valueOf(a.R)).toString()));
                } catch (Exception e) {
                    udk.android.reader.b.c.a((Throwable) e);
                }
                b.a(getString(C0000R.string.conf_pagescrolling_type), Integer.valueOf(parseInt6));
                float f = a.T;
                try {
                    parseFloat2 = Float.parseFloat(defaultSharedPreferences.getString(getString(C0000R.string.conf_pagescrolling_sensitivity), new StringBuilder(String.valueOf(a.T)).toString()));
                } catch (Exception e2) {
                    udk.android.reader.b.c.a((Throwable) e2);
                }
                b.a(getString(C0000R.string.conf_pagescrolling_sensitivity), Float.valueOf(parseFloat2));
                b.a(getString(C0000R.string.conf_pagescrolling_disable_with_columnfitting), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_pagescrolling_disable_with_columnfitting), a.U)));
                int i2 = a.Z;
                int i3 = a.Z;
                try {
                    parseInt5 = Integer.parseInt(defaultSharedPreferences.getString(getString(C0000R.string.conf_basic_zoom_ratio), new StringBuilder(String.valueOf(a.Z)).toString()));
                } catch (Exception e3) {
                    udk.android.reader.b.c.a((Throwable) e3);
                }
                if (i2 != parseInt5) {
                    udk.android.reader.b.e.t = true;
                }
                b.a(getString(C0000R.string.conf_basic_zoom_ratio), Integer.valueOf(parseInt5));
                int i4 = udk.android.reader.b.e.l;
                try {
                    parseInt4 = Integer.parseInt(defaultSharedPreferences.getString(getString(C0000R.string.conf_key_action_volume), new StringBuilder(String.valueOf(udk.android.reader.b.e.l)).toString()));
                } catch (Exception e4) {
                    udk.android.reader.b.c.a((Throwable) e4);
                }
                b.a(getString(C0000R.string.conf_key_action_volume), Integer.valueOf(parseInt4));
                b.a(getString(C0000R.string.conf_pinchzoom_movable), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_pinchzoom_movable), a.aa)));
                float f2 = a.aq;
                try {
                    parseFloat = Float.parseFloat(defaultSharedPreferences.getString(getString(C0000R.string.conf_pagefliping_speed), new StringBuilder(String.valueOf(a.aq)).toString()));
                } catch (Exception e5) {
                    udk.android.reader.b.c.a((Throwable) e5);
                }
                b.a(getString(C0000R.string.conf_pagefliping_speed), Float.valueOf(parseFloat));
                int i5 = udk.android.reader.b.e.B;
                try {
                    parseInt3 = Integer.parseInt(defaultSharedPreferences.getString(getString(C0000R.string.conf_screen_orientation), new StringBuilder(String.valueOf(udk.android.reader.b.e.B)).toString()));
                } catch (Exception e6) {
                    udk.android.reader.b.c.a((Throwable) e6);
                }
                b.a(getString(C0000R.string.conf_screen_orientation), Integer.valueOf(parseInt3));
                int i6 = a.aM;
                try {
                    parseInt2 = Integer.parseInt(defaultSharedPreferences.getString(getString(C0000R.string.conf_default_reading_direction), new StringBuilder(String.valueOf(a.aM)).toString()));
                } catch (Exception e7) {
                    udk.android.reader.b.c.a((Throwable) e7);
                }
                b.a(getString(C0000R.string.conf_default_reading_direction), Integer.valueOf(parseInt2));
                b.a(getString(C0000R.string.conf_screen_wakelock), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_screen_wakelock), udk.android.reader.b.e.C)));
                b.a(getString(C0000R.string.conf_dpad_invert), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_dpad_invert), udk.android.reader.b.e.E)));
                b.a(getString(C0000R.string.conf_link_needconfirm), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_link_needconfirm), udk.android.reader.b.e.D)));
                int i7 = a.as;
                try {
                    parseInt = Integer.parseInt(defaultSharedPreferences.getString(getString(C0000R.string.conf_nightmode_line_height_percent), new StringBuilder(String.valueOf(a.as)).toString()));
                } catch (Exception e8) {
                    udk.android.reader.b.c.a((Throwable) e8);
                }
                b.a(getString(C0000R.string.conf_nightmode_line_height_percent), Integer.valueOf(parseInt));
                int i8 = defaultSharedPreferences.getInt(getString(C0000R.string.conf_nightmode_fontcolor), a());
                b.a(getString(C0000R.string.conf_nightmode_fontcolor_r), Integer.valueOf(c.b(i8)));
                b.a(getString(C0000R.string.conf_nightmode_fontcolor_g), Integer.valueOf(c.c(i8)));
                b.a(getString(C0000R.string.conf_nightmode_fontcolor_b), Integer.valueOf(c.d(i8)));
                int i9 = defaultSharedPreferences.getInt(getString(C0000R.string.conf_nightmode_bgcolor), b());
                b.a(getString(C0000R.string.conf_nightmode_bgcolor_r), Integer.valueOf(c.b(i9)));
                b.a(getString(C0000R.string.conf_nightmode_bgcolor_g), Integer.valueOf(c.c(i9)));
                b.a(getString(C0000R.string.conf_nightmode_bgcolor_b), Integer.valueOf(c.d(i9)));
                b.a(getString(C0000R.string.conf_menu_opentime_enable), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_menu_opentime_enable), udk.android.reader.b.e.j)));
                b.a(getString(C0000R.string.conf_menu_auto_disable), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_menu_auto_disable), udk.android.reader.b.e.k)));
                b.a(getString(C0000R.string.conf_tts_detect_headset_plug), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_tts_detect_headset_plug), udk.android.reader.b.e.A)));
                if (!udk.android.reader.b.e.x) {
                    b.a(getString(C0000R.string.conf_notificationbar_readingtime_enable), Boolean.valueOf(defaultSharedPreferences.getBoolean(getString(C0000R.string.conf_notificationbar_readingtime_enable), a.aL)));
                }
                b.a("UTF-8");
            } catch (Exception e9) {
                udk.android.reader.b.c.a((Throwable) e9);
            }
            udk.android.reader.b.e.c(this);
            super.onPause();
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        boolean onPreferenceTreeClick = super.onPreferenceTreeClick(preferenceScreen, preference);
        if (getString(C0000R.string.conf_nightmode_fontcolor).equals(preference.getKey())) {
            a(new c(this), PreferenceManager.getDefaultSharedPreferences(this).getInt(getString(C0000R.string.conf_nightmode_fontcolor), a()));
        } else if (getString(C0000R.string.conf_nightmode_bgcolor).equals(preference.getKey())) {
            a(new b(this), PreferenceManager.getDefaultSharedPreferences(this).getInt(getString(C0000R.string.conf_nightmode_bgcolor), b()));
        }
        return onPreferenceTreeClick;
    }
}
