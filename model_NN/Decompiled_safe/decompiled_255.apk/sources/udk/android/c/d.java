package udk.android.c;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import udk.android.reader.b.c;

final class d implements DialogInterface.OnClickListener {
    private /* synthetic */ h a;
    private final /* synthetic */ Context b;

    d(h hVar, Context context) {
        this.a = hVar;
        this.b = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        try {
            this.b.startActivity(new Intent("android.speech.tts.engine.INSTALL_TTS_DATA"));
        } catch (Exception e) {
            c.a((Throwable) e);
        }
    }
}
