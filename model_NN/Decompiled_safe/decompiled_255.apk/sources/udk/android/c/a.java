package udk.android.c;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import com.unidocs.commonlib.util.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public final class a {
    /* access modifiers changed from: private */
    public TextToSpeech a;
    /* access modifiers changed from: private */
    public int b = -1;
    private boolean c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public Runnable e;

    private void a(Context context, Runnable runnable) {
        if (this.a == null) {
            this.a = new TextToSpeech(context, new k(this, runnable));
        } else if (this.b != 0) {
            if (this.b == -1) {
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }

    /* access modifiers changed from: private */
    public void b(Context context, String str, Runnable runnable) {
        String str2;
        String str3;
        String str4;
        String str5;
        if (!this.c) {
            this.d = str;
            this.e = runnable;
            String trim = str.trim();
            int indexOf = trim.indexOf("\n");
            String trim2 = indexOf < 0 ? trim : trim.substring(0, indexOf + 1).trim();
            String trim3 = indexOf < 0 ? "" : trim.substring(indexOf + 1).trim();
            int indexOf2 = trim2.indexOf(".");
            if (trim2.length() <= 70 || indexOf2 < 0 || indexOf2 >= trim2.length() - 1) {
                str2 = trim3;
                str3 = trim2;
            } else {
                String substring = trim2.substring(indexOf2 + 1);
                String substring2 = trim2.substring(0, indexOf2 + 1);
                str2 = String.valueOf(substring) + trim3;
                str3 = substring2;
            }
            int indexOf3 = str3.indexOf(",");
            if (str3.length() <= 70 || indexOf3 < 0 || indexOf3 >= str3.length() - 1) {
                str4 = str3;
                str5 = str2;
            } else {
                String substring3 = str3.substring(indexOf3 + 1);
                str4 = str3.substring(0, indexOf3 + 1);
                str5 = String.valueOf(substring3) + str2;
            }
            a(context, new j(this, str4, runnable, str5, context));
        }
    }

    public final void a() {
        if (this.a != null) {
            this.a.stop();
            this.a.shutdown();
            this.b = -1;
            this.a = null;
        }
    }

    public final void a(Context context) {
        b(context);
        this.d = null;
        this.e = null;
    }

    public final void a(Context context, Runnable runnable, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        a(context, new h(this, context, str, str5, str6, str7, str2, str4, str3, runnable));
    }

    public final void a(Context context, String str, Runnable runnable) {
        this.c = false;
        b(context, str, runnable);
    }

    public final List b() {
        if (this.a == null || this.b != 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (Locale locale : Locale.getAvailableLocales()) {
            if (this.a.isLanguageAvailable(locale) == 0) {
                if (Locale.getDefault().getLanguage().equals(locale.getLanguage())) {
                    arrayList.add(locale);
                } else {
                    arrayList2.add(locale);
                }
            }
        }
        if (!b.a((Collection) arrayList2)) {
            return arrayList;
        }
        arrayList.addAll(arrayList2);
        return arrayList;
    }

    public final void b(Context context) {
        this.c = true;
        a(context, new i(this));
    }

    public final void c(Context context) {
        if (this.c && this.d != null && this.e != null) {
            a(context, this.d, this.e);
        }
    }
}
