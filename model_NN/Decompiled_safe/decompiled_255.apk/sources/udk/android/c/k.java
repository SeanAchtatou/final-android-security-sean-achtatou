package udk.android.c;

import android.speech.tts.TextToSpeech;

final class k implements TextToSpeech.OnInitListener {
    private /* synthetic */ a a;
    private final /* synthetic */ Runnable b;
    private final /* synthetic */ Runnable c = null;

    k(a aVar, Runnable runnable) {
        this.a = aVar;
        this.b = runnable;
    }

    public final void onInit(int i) {
        this.a.b = i;
        if (i == 0) {
            if (this.b != null) {
                this.b.run();
            }
        } else if (i == -1 && this.c != null) {
            this.c.run();
        }
    }
}
