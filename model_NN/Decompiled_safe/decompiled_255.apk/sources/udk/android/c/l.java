package udk.android.c;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import com.unidocs.commonlib.util.b;

final class l implements TextToSpeech.OnUtteranceCompletedListener {
    private /* synthetic */ j a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Runnable c;
    private final /* synthetic */ String d;
    private final /* synthetic */ Context e;

    l(j jVar, String str, Runnable runnable, String str2, Context context) {
        this.a = jVar;
        this.b = str;
        this.c = runnable;
        this.d = str2;
        this.e = context;
    }

    public final void onUtteranceCompleted(String str) {
        if (this.b.equals(str) && this.c != null) {
            if (b.b(this.d)) {
                this.a.a.d = null;
                this.a.a.e = null;
                this.c.run();
                return;
            }
            this.a.a.b(this.e, this.d, this.c);
        }
    }
}
