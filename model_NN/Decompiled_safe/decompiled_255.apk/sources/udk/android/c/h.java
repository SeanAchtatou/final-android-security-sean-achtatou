package udk.android.c;

import android.app.AlertDialog;
import android.content.Context;
import com.unidocs.commonlib.util.b;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

final class h implements Runnable {
    final /* synthetic */ a a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ String e;
    private final /* synthetic */ String f;
    private final /* synthetic */ String g;
    private final /* synthetic */ String h;
    private final /* synthetic */ String i;
    private final /* synthetic */ Runnable j;

    h(a aVar, Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, Runnable runnable) {
        this.a = aVar;
        this.b = context;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = str6;
        this.i = str7;
        this.j = runnable;
    }

    public final void run() {
        int i2;
        int i3;
        Locale locale;
        List b2 = this.a.b();
        if (b.b((Collection) b2)) {
            new AlertDialog.Builder(this.b).setTitle(this.c).setMessage(this.d).setPositiveButton(this.e, new b(this, this.j)).setNegativeButton(this.f, new d(this, this.b)).show();
            return;
        }
        String[] strArr = new String[b2.size()];
        for (int i4 = 0; i4 < b2.size(); i4++) {
            strArr[i4] = ((Locale) b2.get(i4)).getDisplayName();
        }
        Locale language = this.a.a.getLanguage();
        if (language != null) {
            int i5 = 0;
            while (true) {
                if (i5 >= b2.size()) {
                    break;
                } else if (((Locale) b2.get(i5)).getDisplayName().equals(language.getDisplayName())) {
                    i2 = i5;
                    break;
                } else {
                    i5++;
                }
            }
        }
        i2 = -1;
        if (i2 < 0) {
            locale = (Locale) b2.get(0);
            this.a.a.setLanguage(locale);
            i3 = 0;
        } else {
            i3 = i2;
            locale = language;
        }
        new AlertDialog.Builder(this.b).setTitle(this.c).setMessage(String.valueOf(this.g) + "\n" + locale.getDisplayName()).setPositiveButton(this.h, new e(this, this.j)).setNegativeButton(this.i, new c(this, this.b, strArr, i3, this.h, b2, this.j)).show();
    }
}
