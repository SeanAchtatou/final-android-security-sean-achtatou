package udk.android.c;

import android.content.Context;
import com.unidocs.commonlib.a.b;
import java.util.HashMap;

final class j implements Runnable {
    final /* synthetic */ a a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Runnable c;
    private final /* synthetic */ String d;
    private final /* synthetic */ Context e;

    j(a aVar, String str, Runnable runnable, String str2, Context context) {
        this.a = aVar;
        this.b = str;
        this.c = runnable;
        this.d = str2;
        this.e = context;
    }

    public final void run() {
        if (this.a.a.isSpeaking()) {
            this.a.a.stop();
        }
        String a2 = b.a(24);
        HashMap hashMap = new HashMap();
        hashMap.put("utteranceId", a2);
        this.a.a.setOnUtteranceCompletedListener(new l(this, a2, this.c, this.d, this.e));
        this.a.a.speak(this.b, 0, hashMap);
    }
}
