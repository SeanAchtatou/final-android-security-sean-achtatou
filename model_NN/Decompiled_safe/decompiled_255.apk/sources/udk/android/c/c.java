package udk.android.c;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.util.List;

final class c implements DialogInterface.OnClickListener {
    final /* synthetic */ h a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String[] c;
    private final /* synthetic */ int d;
    private final /* synthetic */ String e;
    private final /* synthetic */ List f;
    private final /* synthetic */ Runnable g;

    c(h hVar, Context context, String[] strArr, int i, String str, List list, Runnable runnable) {
        this.a = hVar;
        this.b = context;
        this.c = strArr;
        this.d = i;
        this.e = str;
        this.f = list;
        this.g = runnable;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new AlertDialog.Builder(this.b).setSingleChoiceItems(this.c, this.d, new g(this, this.f)).setPositiveButton(this.e, new f(this, this.g)).show();
    }
}
