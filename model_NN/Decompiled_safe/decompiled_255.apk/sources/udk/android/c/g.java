package udk.android.c;

import android.content.DialogInterface;
import java.util.List;
import java.util.Locale;

final class g implements DialogInterface.OnClickListener {
    private /* synthetic */ c a;
    private final /* synthetic */ List b;

    g(c cVar, List list) {
        this.a = cVar;
        this.b = list;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.a.a.setLanguage((Locale) this.b.get(i));
    }
}
