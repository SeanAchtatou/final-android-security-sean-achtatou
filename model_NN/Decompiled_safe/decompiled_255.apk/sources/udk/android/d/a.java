package udk.android.d;

import android.content.Context;
import android.os.Build;
import udk.android.b.m;
import udk.android.reader.b.c;

public final class a {
    private static a a;

    private a() {
    }

    public static String a(Context context) {
        String str = String.valueOf(Build.DEVICE) + "_" + m.d(context) + "_" + m.c(context);
        try {
            return com.unidocs.commonlib.a.a.a(str, "UTF-8");
        } catch (Exception e) {
            c.a((Throwable) e);
            return str;
        }
    }

    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }
}
