package udk.android.a;

import android.app.AlertDialog;
import android.content.Context;

public final class i extends AlertDialog {
    private a a;
    /* access modifiers changed from: private */
    public e b;
    private int c;

    private i(Context context, e eVar, int i, int i2) {
        super(context);
        this.b = eVar;
        this.c = i;
        this.a = new a(getContext(), new g(this), this.c, i2);
        setView(this.a);
    }

    public static i a(Context context, int i, e eVar, String str, String str2, boolean z) {
        return a(context, i, eVar, str, str2, z, 255);
    }

    public static i a(Context context, int i, e eVar, String str, String str2, boolean z, int i2) {
        i iVar = new i(context, eVar, i, i2);
        iVar.setTitle(str);
        iVar.a.a(z);
        iVar.setButton(str2, new f(eVar, iVar));
        iVar.show();
        return iVar;
    }

    public final int a() {
        return this.a.a();
    }
}
