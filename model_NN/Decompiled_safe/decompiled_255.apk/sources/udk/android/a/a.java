package udk.android.a;

import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import udk.android.b.m;

final class a extends LinearLayout {
    /* access modifiers changed from: private */
    public SeekBar a;
    private TextView b;
    /* access modifiers changed from: private */
    public SeekBar c;
    private TextView d;
    /* access modifiers changed from: private */
    public SeekBar e;
    private TextView f;
    /* access modifiers changed from: private */
    public SeekBar g;
    private TextView h;
    /* access modifiers changed from: private */
    public h i;

    a(Context context, e eVar, int i2, int i3) {
        super(context);
        int a2 = m.a(context, 5);
        setOrientation(0);
        setPadding(a2, a2, a2, a2);
        setGravity(16);
        this.i = new h(context, eVar, i2, i3);
        this.i.a(new d(this));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.weight = 0.0f;
        addView(this.i, layoutParams);
        c cVar = new c(this);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(m.a(context, a2), 0, 0, 0);
        linearLayout.setOrientation(1);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.weight = 1.0f;
        addView(linearLayout, layoutParams2);
        this.b = new TextView(context);
        this.b.setTextSize(1, 10.0f);
        linearLayout.addView(this.b, new LinearLayout.LayoutParams(-2, -2));
        this.a = new SeekBar(context);
        this.a.setMax(255);
        this.a.setOnSeekBarChangeListener(cVar);
        linearLayout.addView(this.a, new LinearLayout.LayoutParams(-1, -2));
        this.d = new TextView(context);
        this.d.setTextSize(1, 10.0f);
        linearLayout.addView(this.d, new LinearLayout.LayoutParams(-2, -2));
        this.c = new SeekBar(context);
        this.c.setMax(255);
        this.c.setOnSeekBarChangeListener(cVar);
        linearLayout.addView(this.c, new LinearLayout.LayoutParams(-1, -2));
        this.f = new TextView(context);
        this.f.setTextSize(1, 10.0f);
        linearLayout.addView(this.f, new LinearLayout.LayoutParams(-2, -2));
        this.e = new SeekBar(context);
        this.e.setMax(255);
        this.e.setOnSeekBarChangeListener(cVar);
        linearLayout.addView(this.e, new LinearLayout.LayoutParams(-1, -2));
        this.h = new TextView(context);
        this.h.setTextSize(1, 10.0f);
        linearLayout.addView(this.h, new LinearLayout.LayoutParams(-2, -2));
        this.g = new SeekBar(context);
        this.g.setMax(255);
        this.g.setOnSeekBarChangeListener(cVar);
        linearLayout.addView(this.g, new LinearLayout.LayoutParams(-1, -2));
        a(i2);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        b(i2);
        int red = Color.red(i2);
        int green = Color.green(i2);
        int blue = Color.blue(i2);
        int alpha = Color.alpha(i2);
        this.a.setProgress(red);
        this.c.setProgress(green);
        this.e.setProgress(blue);
        this.g.setProgress(alpha);
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        int red = Color.red(i2);
        int green = Color.green(i2);
        int blue = Color.blue(i2);
        int alpha = Color.alpha(i2);
        this.b.setText("Red " + red);
        this.d.setText("Green " + green);
        this.f.setText("Blue " + blue);
        this.h.setText("Alpha " + alpha);
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        return this.i.a();
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.g.setVisibility(z ? 0 : 8);
        this.h.setVisibility(z ? 0 : 8);
    }
}
