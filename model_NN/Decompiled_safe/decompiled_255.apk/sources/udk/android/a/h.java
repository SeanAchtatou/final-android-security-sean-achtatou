package udk.android.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.view.View;
import udk.android.b.c;
import udk.android.b.k;
import udk.android.b.m;

final class h extends View {
    private Paint a = new Paint(1);
    private Paint b;
    private Paint c;
    private Paint d;
    private final int[] e;
    private e f;
    private Runnable g;
    private boolean h;
    private boolean i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
     arg types: [int, int, int[], ?[OBJECT, ARRAY]]
     candidates:
      ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
    h(Context context, e eVar, int i2, int i3) {
        super(context);
        this.f = eVar;
        this.e = new int[]{c.a(-65536, i3), c.a(-65281, i3), c.a(-16776961, i3), c.a(-16711681, i3), c.a(-16711936, i3), c.a(-256, i3), c.a(-65536, i3)};
        SweepGradient sweepGradient = new SweepGradient(0.0f, 0.0f, this.e, (float[]) null);
        this.a.setShader(sweepGradient);
        this.a.setStyle(Paint.Style.STROKE);
        this.a.setStrokeWidth(32.0f);
        this.b = new Paint(this.a);
        this.b.setShader(k.a(m.a(context, 10)));
        this.c = new Paint(1);
        this.c.setColor(i2);
        this.c.setStrokeWidth(5.0f);
        this.d = new Paint(1);
        this.d.setShader(this.b.getShader());
    }

    private static int a(int i2, int i3, float f2) {
        return Math.round(((float) (i3 - i2)) * f2) + i2;
    }

    public final int a() {
        return this.c.getColor();
    }

    public final void a(int i2) {
        this.c.setColor(i2);
        invalidate();
    }

    public final void a(Runnable runnable) {
        this.g = runnable;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        float strokeWidth = 100.0f - (this.a.getStrokeWidth() * 0.5f);
        canvas.translate(100.0f, 100.0f);
        canvas.drawOval(new RectF(-strokeWidth, -strokeWidth, strokeWidth, strokeWidth), this.b);
        canvas.drawOval(new RectF(-strokeWidth, -strokeWidth, strokeWidth, strokeWidth), this.a);
        canvas.drawCircle(0.0f, 0.0f, 31.0f, this.d);
        canvas.drawCircle(0.0f, 0.0f, 32.0f, this.c);
        if (this.h) {
            int color = this.c.getColor();
            this.c.setStyle(Paint.Style.STROKE);
            if (this.i) {
                this.c.setAlpha(255);
            } else {
                this.c.setAlpha(128);
            }
            canvas.drawCircle(0.0f, 0.0f, this.c.getStrokeWidth() + 32.0f, this.c);
            this.c.setStyle(Paint.Style.FILL);
            this.c.setColor(color);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        setMeasuredDimension(200, 200);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean onTouchEvent(android.view.MotionEvent r11) {
        /*
            r10 = this;
            r2 = 1120403456(0x42c80000, float:100.0)
            r9 = 1065353216(0x3f800000, float:1.0)
            r7 = 0
            r6 = 0
            r8 = 1
            float r0 = r11.getX()
            float r0 = r0 - r2
            float r1 = r11.getY()
            float r1 = r1 - r2
            float r2 = r0 * r0
            float r3 = r1 * r1
            float r2 = r2 + r3
            double r2 = (double) r2
            double r2 = java.lang.Math.sqrt(r2)
            r4 = 4629700416936869888(0x4040000000000000, double:32.0)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 > 0) goto L_0x002a
            r2 = r8
        L_0x0022:
            int r3 = r11.getAction()
            switch(r3) {
                case 0: goto L_0x002c;
                case 1: goto L_0x00b9;
                case 2: goto L_0x0036;
                default: goto L_0x0029;
            }
        L_0x0029:
            return r8
        L_0x002a:
            r2 = r6
            goto L_0x0022
        L_0x002c:
            r10.h = r2
            if (r2 == 0) goto L_0x0036
            r10.i = r8
            r10.invalidate()
            goto L_0x0029
        L_0x0036:
            boolean r3 = r10.h
            if (r3 == 0) goto L_0x0044
            boolean r0 = r10.i
            if (r0 == r2) goto L_0x0029
            r10.i = r2
            r10.invalidate()
            goto L_0x0029
        L_0x0044:
            double r1 = (double) r1
            double r3 = (double) r0
            double r0 = java.lang.Math.atan2(r1, r3)
            float r0 = (float) r0
            r1 = 1086918618(0x40c90fda, float:6.283185)
            float r0 = r0 / r1
            int r1 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r1 >= 0) goto L_0x0054
            float r0 = r0 + r9
        L_0x0054:
            android.graphics.Paint r1 = r10.c
            int[] r2 = r10.e
            int r3 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r3 > 0) goto L_0x006e
            r0 = r2[r6]
        L_0x005e:
            r1.setColor(r0)
            r10.invalidate()
            java.lang.Runnable r0 = r10.g
            if (r0 == 0) goto L_0x0029
            java.lang.Runnable r0 = r10.g
            r0.run()
            goto L_0x0029
        L_0x006e:
            int r3 = (r0 > r9 ? 1 : (r0 == r9 ? 0 : -1))
            if (r3 < 0) goto L_0x0077
            int r0 = r2.length
            int r0 = r0 - r8
            r0 = r2[r0]
            goto L_0x005e
        L_0x0077:
            int r3 = r2.length
            int r3 = r3 - r8
            float r3 = (float) r3
            float r0 = r0 * r3
            int r3 = (int) r0
            float r4 = (float) r3
            float r0 = r0 - r4
            r4 = r2[r3]
            int r3 = r3 + 1
            r2 = r2[r3]
            int r3 = android.graphics.Color.alpha(r4)
            int r5 = android.graphics.Color.alpha(r2)
            int r3 = a(r3, r5, r0)
            int r5 = android.graphics.Color.red(r4)
            int r6 = android.graphics.Color.red(r2)
            int r5 = a(r5, r6, r0)
            int r6 = android.graphics.Color.green(r4)
            int r7 = android.graphics.Color.green(r2)
            int r6 = a(r6, r7, r0)
            int r4 = android.graphics.Color.blue(r4)
            int r2 = android.graphics.Color.blue(r2)
            int r0 = a(r4, r2, r0)
            int r0 = android.graphics.Color.argb(r3, r5, r6, r0)
            goto L_0x005e
        L_0x00b9:
            boolean r0 = r10.h
            if (r0 == 0) goto L_0x0029
            if (r2 == 0) goto L_0x00ca
            udk.android.a.e r0 = r10.f
            android.graphics.Paint r1 = r10.c
            int r1 = r1.getColor()
            r0.a(r1)
        L_0x00ca:
            r10.h = r6
            r10.invalidate()
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: udk.android.a.h.onTouchEvent(android.view.MotionEvent):boolean");
    }
}
