package udk.android.a;

import android.content.Context;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

final class n implements TextView.OnEditorActionListener {
    private /* synthetic */ b a;
    private final /* synthetic */ Context b;

    n(b bVar, Context context) {
        this.a = bVar;
        this.b = context;
    }

    public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getAction() != 1) {
            return true;
        }
        ((InputMethodManager) this.b.getSystemService("input_method")).hideSoftInputFromWindow(this.a.h.getWindowToken(), 0);
        return true;
    }
}
