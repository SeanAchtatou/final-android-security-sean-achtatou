package udk.android.a;

import com.unidocs.commonlib.util.b;
import java.io.File;
import java.io.FileFilter;

final class j implements FileFilter {
    private /* synthetic */ b a;

    j(b bVar) {
        this.a = bVar;
    }

    public final boolean accept(File file) {
        if (b.a(this.a.g)) {
            String name = file.getName();
            int lastIndexOf = name.lastIndexOf(".");
            if (lastIndexOf != -1) {
                name = name.substring(lastIndexOf + 1);
            }
            if (!b.a(name, this.a.g, false)) {
                return false;
            }
        }
        return !file.isDirectory() && (!this.a.e || !file.getName().startsWith("."));
    }
}
