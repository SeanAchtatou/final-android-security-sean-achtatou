package udk.android.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;

final class m extends BaseAdapter {
    private /* synthetic */ b a;

    m(b bVar) {
        this.a = bVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public File getItem(int i) {
        return (File) this.a.c.get(i);
    }

    public final int getCount() {
        return this.a.c.size();
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView;
        LinearLayout linearLayout;
        Context context = viewGroup.getContext();
        if (view == null) {
            linearLayout = new LinearLayout(context);
            linearLayout.setGravity(16);
            int a2 = udk.android.b.m.a(context, 10);
            linearLayout.setPadding(a2, a2, a2, a2);
            linearLayout.setOrientation(1);
            TextView textView2 = new TextView(context);
            textView2.setId(6784);
            textView2.setGravity(16);
            linearLayout.addView(textView2, new LinearLayout.LayoutParams(-2, -2));
            textView = textView2;
        } else {
            LinearLayout linearLayout2 = (LinearLayout) view;
            textView = (TextView) linearLayout2.findViewById(6784);
            linearLayout = linearLayout2;
        }
        File a3 = getItem(i);
        String name = a3.getName();
        if (i == 0 && a3.equals(this.a.a.getParentFile())) {
            name = "..";
        }
        textView.setText(a3.isDirectory() ? "[ " + name + " ]" : name);
        return linearLayout;
    }
}
