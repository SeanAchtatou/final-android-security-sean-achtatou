package udk.android.a;

import android.widget.SeekBar;

final class c implements SeekBar.OnSeekBarChangeListener {
    private /* synthetic */ a a;

    c(a aVar) {
        this.a = aVar;
    }

    public final void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        int a2 = udk.android.b.c.a(this.a.g.getProgress(), this.a.a.getProgress(), this.a.c.getProgress(), this.a.e.getProgress());
        this.a.b(a2);
        if (this.a.i.a() != a2) {
            this.a.i.a(a2);
        }
    }

    public final void onStartTrackingTouch(SeekBar seekBar) {
    }

    public final void onStopTrackingTouch(SeekBar seekBar) {
    }
}
