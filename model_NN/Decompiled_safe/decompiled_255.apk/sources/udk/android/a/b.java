package udk.android.a;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import udk.android.reader.b.c;

public final class b extends AlertDialog {
    /* access modifiers changed from: private */
    public File a;
    private File b;
    /* access modifiers changed from: private */
    public List c;
    private boolean d;
    /* access modifiers changed from: private */
    public boolean e;
    private File f;
    /* access modifiers changed from: private */
    public String[] g;
    /* access modifiers changed from: private */
    public EditText h;

    public b(Context context, File file, File file2, String[] strArr, boolean z) {
        super(context);
        this.h = new EditText(context);
        this.h.setSingleLine(true);
        this.h.setEnabled(z);
        this.h.setFocusable(z);
        if (z) {
            this.h.setInputType(1);
            this.h.setImeOptions(6);
            this.h.setOnEditorActionListener(new n(this, context));
        }
        a(file, null);
        this.d = false;
        this.e = true;
        this.g = strArr;
        this.f = file2;
    }

    /* access modifiers changed from: private */
    public void a(File file, BaseAdapter baseAdapter) {
        File file2 = this.a;
        File file3 = this.b;
        try {
            if (file.isDirectory()) {
                this.a = file;
                this.b = file;
                b();
                if (baseAdapter != null) {
                    baseAdapter.notifyDataSetChanged();
                }
            } else {
                this.a = file.getParentFile();
                this.b = file;
            }
        } catch (Exception e2) {
            this.a = file2;
            this.b = file3;
            c.a((Throwable) e2);
        }
        this.h.setText((!this.b.isDirectory() || this.b.getAbsolutePath().endsWith(File.separator)) ? this.b.getAbsolutePath() : String.valueOf(this.b.getAbsolutePath()) + File.separator);
        this.h.setSelection(this.h.getText().toString().length());
    }

    private void b() {
        ArrayList arrayList = new ArrayList();
        File parentFile = this.a.getParentFile();
        File[] listFiles = this.a.listFiles(new k(this));
        File[] fileArr = null;
        if (!this.d) {
            fileArr = this.a.listFiles(new j(this));
        }
        if (parentFile != null && !this.a.equals(this.f)) {
            arrayList.add(parentFile);
        }
        ArrayList arrayList2 = new ArrayList();
        if (com.unidocs.commonlib.util.b.a((Object[]) listFiles)) {
            arrayList2.clear();
            for (File add : listFiles) {
                arrayList2.add(add);
            }
            Collections.sort(arrayList2);
            arrayList.addAll(arrayList2);
        }
        if (com.unidocs.commonlib.util.b.a((Object[]) fileArr)) {
            arrayList2.clear();
            for (File add2 : fileArr) {
                arrayList2.add(add2);
            }
            Collections.sort(arrayList2);
            arrayList.addAll(arrayList2);
        }
        this.c = arrayList;
    }

    public final String a() {
        return this.h.getText().toString();
    }

    public final void show() {
        b();
        Context context = getContext();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.weight = 0.0f;
        linearLayout.addView(this.h, layoutParams);
        ListView listView = new ListView(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.weight = 0.0f;
        linearLayout.addView(listView, layoutParams2);
        View view = new View(context);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams3.weight = 1.0f;
        linearLayout.addView(view, layoutParams3);
        setView(linearLayout);
        listView.setAdapter((ListAdapter) new m(this));
        listView.setOnItemClickListener(new l(this));
        super.show();
    }
}
