package com.google.android.apps.analytics;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

class NetworkRequestUtil {
    private static final String FAKE_DOMAIN_HASH = "999";
    private static final String GOOGLE_ANALYTICS_GIF_PATH = "/__utm.gif";

    NetworkRequestUtil() {
    }

    public static String constructEventRequestPath(Event event, String str) {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(String.format("5(%s*%s", event.category, event.action));
        if (event.label != null) {
            sb2.append("*").append(event.label);
        }
        sb2.append(")");
        if (event.value > -1) {
            sb2.append(String.format("(%d)", Integer.valueOf(event.value)));
        }
        sb.append(GOOGLE_ANALYTICS_GIF_PATH);
        sb.append("?utmwv=4.3");
        sb.append("&utmn=").append(event.randomVal);
        sb.append("&utmt=event");
        sb.append("&utme=").append(sb2.toString());
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(event.screenWidth), Integer.valueOf(event.screenHeight)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmac=").append(event.accountId);
        sb.append("&utmcc=").append(getEscapedCookieString(event, str));
        return sb.toString();
    }

    public static String constructPageviewRequestPath(Event event, String str) {
        String str2 = "";
        if (event.action != null) {
            str2 = event.action;
        }
        if (!str2.startsWith("/")) {
            str2 = "/" + str2;
        }
        String encode = encode(str2);
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        sb.append(GOOGLE_ANALYTICS_GIF_PATH);
        sb.append("?utmwv=4.3");
        sb.append("&utmn=").append(event.randomVal);
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(event.screenWidth), Integer.valueOf(event.screenHeight)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmp=").append(encode);
        sb.append("&utmac=").append(event.accountId);
        sb.append("&utmcc=").append(getEscapedCookieString(event, str));
        return sb.toString();
    }

    private static String encode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String getEscapedCookieString(Event event, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("__utma=");
        sb.append(FAKE_DOMAIN_HASH).append(".");
        sb.append(event.userId).append(".");
        sb.append(event.timestampFirst).append(".");
        sb.append(event.timestampPrevious).append(".");
        sb.append(event.timestampCurrent).append(".");
        sb.append(event.visits);
        if (str != null) {
            sb.append("+__utmz=");
            sb.append(FAKE_DOMAIN_HASH).append(".");
            sb.append(event.timestampFirst).append(".");
            sb.append("1.1.");
            sb.append(str);
        }
        return encode(sb.toString());
    }
}
