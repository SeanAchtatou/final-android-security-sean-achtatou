package com.bitpocket.ILoveUSA;

public class Constants {
    public static final String APP_PNAME = "com.bitpocket.ILoveUSA";
    public static final String APP_TITLE = "I Love USA";
    public static final int DAYS_UNTIL_PROMPT = 0;
    public static final String FB_API_KEY = "a4bf4f7bcc129f37ad12b3546af3a303";
    public static final String ID = "131604233525922";
    public static final int LAUNCHES_UNTIL_PROMPT = 5;
    public static final String LOGTAG = "I_LOVE_USA";
    public static final int MAX_POINTS = 255;
    public static final int NUM_PREGUNTAS = 51;
    public static final int NUM_PREGUNTAS_GAME = 5;
    public static final int NUM_PREGUNTAS_OLD = 50;
    public static final String[] PERMISSIONS = {"publish_stream", "offline_access"};
    public static final int POINTS_PREGUNTA = 5;
    public static final String email = "uloveandroid@gmail.com";
}
