package com.bitpocket.ILoveUSA.Facebook;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import com.agi.fbsdk.AsyncFacebookRunner;
import com.agi.fbsdk.DialogError;
import com.agi.fbsdk.Facebook;
import com.agi.fbsdk.FacebookError;
import com.bitpocket.ILoveUSA.Constants;
import com.bitpocket.ILoveUSA.Facebook.SessionEvents;
import com.bitpocket.ILoveUSA.R;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public class LoginButton extends ImageButton {
    private static final String CLASSTAG = LoginButton.class.getSimpleName();
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public AsyncFacebookRunner mAsyncRunner;
    /* access modifiers changed from: private */
    public Facebook mFb;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public String[] mPermissions;
    private SessionListener mSessionListener = new SessionListener(this, null);

    public LoginButton(Context context) {
        super(context);
    }

    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(Activity activity, Facebook fb) {
        init(activity, fb, new String[0]);
    }

    public void init(Activity activity, Facebook fb, String[] permissions) {
        int i;
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " init");
        this.mActivity = activity;
        this.mFb = fb;
        this.mAsyncRunner = new AsyncFacebookRunner(this.mFb);
        this.mPermissions = permissions;
        this.mHandler = new Handler();
        setBackgroundColor(0);
        setAdjustViewBounds(true);
        if (fb.isSessionValid()) {
            i = R.drawable.logout_button;
        } else {
            i = R.drawable.login_button;
        }
        setImageResource(i);
        drawableStateChanged();
        SessionEvents.addAuthListener(this.mSessionListener);
        SessionEvents.addLogoutListener(this.mSessionListener);
        setOnClickListener(new ButtonOnClickListener(this, null));
    }

    public void init(Facebook fb, String[] permissions) {
        int i;
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " init");
        this.mFb = fb;
        this.mPermissions = permissions;
        this.mHandler = new Handler();
        setBackgroundColor(0);
        setAdjustViewBounds(true);
        if (fb.isSessionValid()) {
            i = R.drawable.logout_button;
        } else {
            i = R.drawable.login_button;
        }
        setImageResource(i);
        drawableStateChanged();
        SessionEvents.addAuthListener(this.mSessionListener);
        SessionEvents.addLogoutListener(this.mSessionListener);
        setOnClickListener(new ButtonOnClickListener(this, null));
    }

    private final class ButtonOnClickListener implements View.OnClickListener {
        private ButtonOnClickListener() {
        }

        /* synthetic */ ButtonOnClickListener(LoginButton loginButton, ButtonOnClickListener buttonOnClickListener) {
            this();
        }

        public void onClick(View arg0) {
            if (LoginButton.this.mFb.isSessionValid()) {
                SessionEvents.onLogoutBegin();
                LoginButton.this.mAsyncRunner = new AsyncFacebookRunner(LoginButton.this.mFb);
                LoginButton.this.mAsyncRunner.logout(LoginButton.this.getContext(), new LogoutRequestListener(LoginButton.this, null));
                return;
            }
            LoginButton.this.mFb.authorize(LoginButton.this.mActivity, LoginButton.this.mPermissions, new LoginDialogListener(LoginButton.this, null));
        }
    }

    private final class LoginDialogListener implements Facebook.DialogListener {
        private final String CLASSTAG2;

        private LoginDialogListener() {
            this.CLASSTAG2 = LoginDialogListener.class.getSimpleName();
        }

        /* synthetic */ LoginDialogListener(LoginButton loginButton, LoginDialogListener loginDialogListener) {
            this();
        }

        public void onComplete(Bundle values) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onComplete");
            SessionEvents.onLoginSuccess();
        }

        public void onFacebookError(FacebookError error) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " init");
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onError(DialogError error) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " init");
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onCancel() {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " init");
            SessionEvents.onLoginError("Action Canceled");
        }
    }

    private class LogoutRequestListener extends BaseRequestListener {
        private final String CLASSTAG2;

        private LogoutRequestListener() {
            this.CLASSTAG2 = LogoutRequestListener.class.getSimpleName();
        }

        /* synthetic */ LogoutRequestListener(LoginButton loginButton, LogoutRequestListener logoutRequestListener) {
            this();
        }

        public void onComplete(String response, Object state) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onComplete");
            LoginButton.this.mHandler.post(new Runnable() {
                public void run() {
                    SessionEvents.onLogoutFinish();
                }
            });
        }

        public void onFacebookError(FacebookError e, Object state) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onFacebookError");
            e.printStackTrace();
            SessionEvents.onLogoutError(e.getMessage());
        }

        public void onFileNotFoundException(FileNotFoundException e, Object state) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onFileNotFoundException");
            e.printStackTrace();
            SessionEvents.onLogoutError(e.getMessage());
        }

        public void onIOException(IOException e, Object state) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onIOException");
            e.printStackTrace();
            SessionEvents.onLogoutError(e.getMessage());
        }

        public void onMalformedURLException(MalformedURLException e, Object state) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onMalformedURLException");
            e.printStackTrace();
            SessionEvents.onLogoutError(e.getMessage());
        }
    }

    private class SessionListener implements SessionEvents.AuthListener, SessionEvents.LogoutListener {
        private final String CLASSTAG2;

        private SessionListener() {
            this.CLASSTAG2 = SessionListener.class.getSimpleName();
        }

        /* synthetic */ SessionListener(LoginButton loginButton, SessionListener sessionListener) {
            this();
        }

        public void onAuthSucceed() {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onAuthSucceed");
            LoginButton.this.setImageResource(R.drawable.logout_button);
            SessionStore.save(LoginButton.this.mFb, LoginButton.this.getContext());
        }

        public void onAuthFail(String error) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onAuthFail");
        }

        public void onLogoutBegin() {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onLogoutBegin");
        }

        public void onLogoutError(String error) {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onLogoutBegin");
        }

        public void onLogoutFinish() {
            Log.v(Constants.LOGTAG, " " + this.CLASSTAG2 + " onLogoutFinish");
            SessionStore.clear(LoginButton.this.getContext());
            LoginButton.this.setImageResource(R.drawable.login_button);
        }
    }
}
