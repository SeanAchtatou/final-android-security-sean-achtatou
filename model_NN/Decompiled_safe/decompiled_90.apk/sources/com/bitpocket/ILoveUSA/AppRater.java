package com.bitpocket.ILoveUSA;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bitpocket.ILoveUSA.Prefs.Prefs;

public class AppRater {
    private static final String CLASSTAG = AppRater.class.getSimpleName();
    /* access modifiers changed from: private */
    public static Prefs myPrefs;

    public static void app_launched(Activity mContext) {
        myPrefs = new Prefs(mContext);
        if (myPrefs.getDontShowAgain()) {
            Log.i(Constants.LOGTAG, " " + CLASSTAG + " dont show again - return");
            return;
        }
        Long launch_count = Long.valueOf(myPrefs.getLaunchCount() + 1);
        myPrefs.setLaunchCount(launch_count.longValue());
        Long date_firstLaunch = Long.valueOf(myPrefs.getDateFirstLaunchCount());
        if (date_firstLaunch.longValue() == 0) {
            date_firstLaunch = Long.valueOf(System.currentTimeMillis());
            myPrefs.setDateFirstLaunchCount(date_firstLaunch.longValue());
        }
        Log.i(Constants.LOGTAG, " " + CLASSTAG + " launch_count: " + launch_count + " y date_firstLaunch: " + date_firstLaunch);
        if (launch_count.longValue() >= 5 && System.currentTimeMillis() >= date_firstLaunch.longValue() + 0) {
            showRateDialog(mContext);
        }
        myPrefs.save();
    }

    public static void showRateDialog(final Activity mContext) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle("Rate I Love USA");
        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(1);
        ll.setMinimumWidth(mContext.getWindow().getWindowManager().getDefaultDisplay().getWidth() - 50);
        TextView tv = new TextView(mContext);
        tv.setText("If you enjoy using I Love USA, please take a moment to rate it. Thanks for your support!");
        tv.setWidth(240);
        tv.setPadding(4, 0, 4, 10);
        ll.addView(tv);
        Button b1 = new Button(mContext);
        b1.setText("Rate I Love USA");
        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.bitpocket.ILoveUSA")));
                dialog.dismiss();
            }
        });
        ll.addView(b1);
        Button b2 = new Button(mContext);
        b2.setText("Remind me later");
        b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ll.addView(b2);
        Button b3 = new Button(mContext);
        b3.setText("No, thanks");
        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppRater.myPrefs.setDontShowAgain(true);
                AppRater.myPrefs.save();
                dialog.dismiss();
            }
        });
        ll.addView(b3);
        dialog.setContentView(ll);
        dialog.show();
    }
}
