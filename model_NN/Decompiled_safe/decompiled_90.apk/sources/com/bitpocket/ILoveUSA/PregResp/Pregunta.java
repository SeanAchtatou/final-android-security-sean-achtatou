package com.bitpocket.ILoveUSA.PregResp;

import java.util.ArrayList;

public class Pregunta {
    String categoria;
    String creator;
    long idPregunta;
    long idPregunta_db;
    String img_ref;
    int isShown = 0;
    ArrayList<Respuesta> respuestas;
    String texto;

    public long getId() {
        return this.idPregunta;
    }

    public void setId(long id) {
        this.idPregunta = id;
    }

    public long getId_db() {
        return this.idPregunta_db;
    }

    public void setId_db(long id) {
        this.idPregunta_db = id;
    }

    public String getImg_ref() {
        return this.img_ref;
    }

    public void setImg_ref(String imgRef) {
        this.img_ref = imgRef;
    }

    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto2) {
        this.texto = texto2;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCategoria(String categoria2) {
        this.categoria = categoria2;
    }

    public void setCreator(String creator2) {
        this.creator = creator2;
    }

    public ArrayList<Respuesta> getRespuestas() {
        return this.respuestas;
    }

    public void setRespuestas(ArrayList<Respuesta> respuestas2) {
        this.respuestas = respuestas2;
    }

    public int getIsShown() {
        return this.isShown;
    }

    public void setIsShown(int isShown2) {
        this.isShown = isShown2;
    }

    public Pregunta(long id, long id_db, String categoria2, String img, String texto2, String creator2, int isShown2) {
        this.idPregunta = id;
        this.idPregunta_db = id_db;
        this.categoria = categoria2;
        this.creator = creator2;
        this.img_ref = img;
        this.texto = texto2;
        this.isShown = isShown2;
    }
}
