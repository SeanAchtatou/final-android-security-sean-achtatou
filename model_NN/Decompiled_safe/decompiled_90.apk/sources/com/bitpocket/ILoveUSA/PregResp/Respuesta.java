package com.bitpocket.ILoveUSA.PregResp;

public class Respuesta {
    long idPregunta;
    long idRespuesta;
    int puntuacion;
    String texto;

    public long getIdRespuesta() {
        return this.idRespuesta;
    }

    public void setIdRespuesta(long idRespuesta2) {
        this.idRespuesta = idRespuesta2;
    }

    public long getIdPregunta() {
        return this.idPregunta;
    }

    public void setIdPregunta(long idPregunta2) {
        this.idPregunta = idPregunta2;
    }

    public String getTexto() {
        return this.texto;
    }

    public void setTexto(String texto2) {
        this.texto = texto2;
    }

    public int getPuntuacion() {
        return this.puntuacion;
    }

    public void setPuntuacion(int puntuacion2) {
        this.puntuacion = puntuacion2;
    }

    public Respuesta(long idPregunta2, String texto2, int puntuacion2) {
        this.idPregunta = idPregunta2;
        this.texto = texto2;
        this.puntuacion = puntuacion2;
    }

    public Respuesta(long idRespuesta2, long idPregunta2, String texto2, int puntuacion2) {
        this.idRespuesta = idRespuesta2;
        this.idPregunta = idPregunta2;
        this.texto = texto2;
        this.puntuacion = puntuacion2;
    }
}
