package com.bitpocket.ILoveUSA.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.bitpocket.ILoveUSA.Constants;
import com.bitpocket.ILoveUSA.R;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class send extends Activity {
    private static final int BACK = 1;
    private Button btnSend;
    private EditText etAnswer;
    private EditText etComments;
    private EditText etName;
    private EditText etQuestion;
    private GoogleAnalyticsTracker tracker;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(4);
        setContentView((int) R.layout.send);
        setFeatureDrawableResource(4, R.drawable.icon);
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start(getText(R.string.analytics).toString(), this);
        this.tracker.trackPageView("/send");
        setUpViews();
    }

    private void setUpViews() {
        this.btnSend = (Button) findViewById(R.id.btnSend);
        this.etName = (EditText) findViewById(R.id.etName);
        this.etQuestion = (EditText) findViewById(R.id.etQuestion);
        this.etAnswer = (EditText) findViewById(R.id.etAnswer);
        this.etComments = (EditText) findViewById(R.id.etAnswer);
        this.btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                send.this.sendEmail();
            }
        });
    }

    /* access modifiers changed from: private */
    public void sendEmail() {
        String body = "Name: " + ((Object) this.etName.getText()) + "\nQuestion: " + ((Object) this.etQuestion.getText()) + "\nAnswers: " + ((Object) this.etAnswer.getText()) + "\nComments: " + ((Object) this.etComments.getText());
        try {
            Intent emailIntent = new Intent("android.intent.action.SEND");
            emailIntent.setType("plain/text");
            emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{Constants.email});
            emailIntent.putExtra("android.intent.extra.SUBJECT", getString(R.string.my_question_about_usa));
            emailIntent.putExtra("android.intent.extra.TEXT", body);
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
        } catch (Exception e) {
            Exception e2 = e;
            Log.e("SendMail", e2.getMessage(), e2);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.back).setIcon(17301560);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case 1:
                finish();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.tracker.dispatch();
        this.tracker.stop();
    }
}
