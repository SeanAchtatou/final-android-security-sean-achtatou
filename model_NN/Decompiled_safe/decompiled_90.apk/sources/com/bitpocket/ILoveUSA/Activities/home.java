package com.bitpocket.ILoveUSA.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.bitpocket.ILoveUSA.AppRater;
import com.bitpocket.ILoveUSA.Constants;
import com.bitpocket.ILoveUSA.CuestionSQLiteOpenHelper;
import com.bitpocket.ILoveUSA.ILoveApplication;
import com.bitpocket.ILoveUSA.Prefs.Prefs;
import com.bitpocket.ILoveUSA.PregResp.Pregunta;
import com.bitpocket.ILoveUSA.R;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;
import java.util.List;

public class home extends Activity implements MobclixAdViewListener {
    private static final String CLASSTAG = home.class.getSimpleName();
    private static final int MORE_APPS = 3;
    private static final int POINTS = 2;
    private static final int QUIT = 4;
    private static final int SEND = 1;
    private static final int SENT_EMAIL = 1;
    private ILoveApplication app;
    /* access modifiers changed from: private */
    public int[] arrayRespuestas = new int[51];
    /* access modifiers changed from: private */
    public Button button;
    private int count = 0;
    private List<Pregunta> currentQuestions;
    private ImageView imagen;
    private Prefs myPrefs;
    /* access modifiers changed from: private */
    public ProgressDialog myProgressDialog;
    /* access modifiers changed from: private */
    public int num_preg = 0;
    /* access modifiers changed from: private */
    public RadioGroup radioGroup;
    private TextView text;
    private GoogleAnalyticsTracker tracker;
    private TextView tvCategory;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " onCreate");
        if (getWindow().getWindowManager().getDefaultDisplay().getHeight() <= 320) {
            requestWindowFeature(1);
            setContentView((int) R.layout.main);
        } else {
            requestWindowFeature(4);
            setContentView((int) R.layout.main);
            setFeatureDrawableResource(4, R.drawable.icon);
        }
        AppRater.app_launched(this);
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start(getText(R.string.analytics).toString(), this);
        this.tracker.trackPageView("/home");
        ((MobclixMMABannerXLAdView) findViewById(R.id.advertising_banner_view)).addMobclixAdViewListener(this);
        setUpViews();
        this.myProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.wait), getResources().getString(R.string.loading), true);
        new Thread() {
            public void run() {
                try {
                    sleep(1000);
                    home.this.myProgressDialog.dismiss();
                } catch (Exception e) {
                }
            }
        }.start();
        if (!this.app.isFinished()) {
            showPregunta(0);
            return;
        }
        try {
            this.myProgressDialog.dismiss();
        } catch (Exception e) {
        }
        End();
    }

    private void setUpViews() {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " setUpViews");
        this.app = (ILoveApplication) getApplication();
        this.myPrefs = new Prefs(getApplicationContext());
        if (this.myPrefs.getFirstTime()) {
            this.tracker.trackPageView("/first_time");
            this.myPrefs.setFirstTime(false);
            this.myPrefs.save();
            new AlertDialog.Builder(this).setTitle(getText(R.string.welcome)).setIcon(17301659).setMessage(getText(R.string.instructions)).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).create().show();
        }
        this.app.mixQuestions();
        this.num_preg = 5;
        this.currentQuestions = this.app.getCurrentQuestions();
        this.text = (TextView) findViewById(R.id.tvPregunta);
        this.tvCategory = (TextView) findViewById(R.id.tvCategory);
        this.imagen = (ImageView) findViewById(R.id.imgPregunta);
        this.button = (Button) findViewById(R.id.Button01);
        this.radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        if (getWindow().getWindowManager().getDefaultDisplay().getHeight() <= 320) {
            this.text.setTextSize(10.0f);
            this.tvCategory.setTextSize(10.0f);
            this.button.setMaxHeight(6);
            this.imagen.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void showPregunta(final int preg) {
        Log.v(Constants.LOGTAG, " " + CLASSTAG + " showingPregunta: " + preg);
        if (preg < 50) {
            final int idPregunta = (int) this.currentQuestions.get(preg).getId();
            Log.v(Constants.LOGTAG, " " + CLASSTAG + " showingPregunta: " + preg + "-id: " + idPregunta);
            Pregunta pregunta = this.currentQuestions.get(idPregunta);
            if (pregunta.getIsShown() == 1) {
                this.num_preg++;
                showPregunta(preg + 1);
                return;
            }
            this.radioGroup.removeAllViews();
            this.radioGroup.clearCheck();
            String texto = pregunta.getTexto();
            Log.v(Constants.LOGTAG, " " + CLASSTAG + ": " + texto);
            String img = pregunta.getImg_ref();
            this.text.setText(texto);
            if (pregunta.getCreator().length() > 0) {
                this.tvCategory.setText(String.valueOf(pregunta.getCategoria()) + getString(R.string.by) + " " + pregunta.getCreator());
            } else {
                this.tvCategory.setText(pregunta.getCategoria());
            }
            this.imagen.setImageResource(getResources().getIdentifier(img, "drawable", getPackageName()));
            if (preg >= this.num_preg - 1 || preg == 51) {
                this.button.setText("Finish");
            }
            for (int j = 0; j < pregunta.getRespuestas().size(); j++) {
                RadioButton rb = new RadioButton(this);
                rb.setLayoutParams(new RadioGroup.LayoutParams(-1, -2));
                if (getWindow().getWindowManager().getDefaultDisplay().getHeight() <= 320) {
                    rb.setHeight(6);
                    rb.setTextSize(10.0f);
                }
                rb.setText(pregunta.getRespuestas().get(j).getTexto());
                rb.setId(j);
                this.radioGroup.addView(rb);
            }
            this.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (home.this.radioGroup.getCheckedRadioButtonId() != -1) {
                        home.this.button.setEnabled(true);
                    } else {
                        home.this.button.setEnabled(false);
                    }
                }
            });
            this.button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    home.this.arrayRespuestas[idPregunta] = home.this.radioGroup.getCheckedRadioButtonId();
                    if (preg < home.this.num_preg - 1) {
                        home.this.showPregunta(preg + 1);
                    } else {
                        home.this.End();
                    }
                }
            });
            if (this.radioGroup.getCheckedRadioButtonId() == -1) {
                this.button.setEnabled(false);
            }
            this.count++;
            return;
        }
        End();
    }

    public void End() {
        Intent intent = new Intent(this, end.class);
        intent.putExtra(CuestionSQLiteOpenHelper.RESPUESTAS_TABLE, this.arrayRespuestas);
        intent.putExtra("count", this.count);
        startActivity(intent);
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.send).setIcon(17301584);
        menu.add(0, 2, 0, (int) R.string.points).setIcon(17301569);
        menu.add(0, 3, 0, (int) R.string.more_apps).setIcon(17301555);
        menu.add(0, 4, 0, (int) R.string.quit).setIcon(17301560);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case 1:
                startActivityForResult(new Intent(this, send.class), -1);
                return true;
            case 2:
                checkPoints();
                return true;
            case 3:
                this.tracker.trackPageView("/more_apps");
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:bitpocket")));
                return true;
            case 4:
                finish();
                return true;
            default:
                return true;
        }
    }

    private void checkPoints() {
        Toast.makeText(this, String.format(getResources().getString(R.string.youvegot), Integer.valueOf(this.app.loadPoints()), Integer.valueOf((int) Constants.MAX_POINTS)), 1).show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (1 == requestCode && -1 == resultCode) {
            new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.version)).setIcon(17301545).setMessage(getResources().getString(R.string.emailsent)).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).create().show();
        } else {
            new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.version)).setIcon(17301545).setMessage(getResources().getString(R.string.emailerror)).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).create().show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.tracker.dispatch();
        this.tracker.stop();
    }

    public void onSuccessfulLoad(MobclixAdView view) {
        Log.v(Constants.LOGTAG, "The ad request was successful!");
        view.setVisibility(0);
    }

    public void onFailedLoad(MobclixAdView view, int errorCode) {
        Log.v(Constants.LOGTAG, "The ad request failed with error code: " + errorCode);
        view.setVisibility(8);
    }

    public void onAdClick(MobclixAdView adView) {
        Log.v(Constants.LOGTAG, "Ad clicked!");
    }

    public void onCustomAdTouchThrough(MobclixAdView adView, String string) {
        Log.v(Constants.LOGTAG, "The custom ad responded with '" + string + "' when touched!");
    }

    public boolean onOpenAllocationLoad(MobclixAdView adView, int openAllocationCode) {
        Log.v(Constants.LOGTAG, "The ad request returned open allocation code: " + openAllocationCode);
        return false;
    }

    public String keywords() {
        return "quiz,trivia,usa,us,eeuu,america,questions,mobclix";
    }

    public String query() {
        return "query";
    }
}
