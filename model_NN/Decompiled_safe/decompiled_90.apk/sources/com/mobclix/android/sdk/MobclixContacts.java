package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import org.json.JSONObject;

public abstract class MobclixContacts {
    private static MobclixContacts sInstance;

    public abstract void addContact(JSONObject jSONObject, Activity activity) throws Exception;

    public abstract Intent getAddContactIntent(JSONObject jSONObject);

    public abstract Intent getPickContactIntent();

    public abstract JSONObject loadContact(ContentResolver contentResolver, Uri uri);

    public static MobclixContacts getInstance() {
        String className;
        if (sInstance == null) {
            if (Integer.parseInt(Build.VERSION.SDK) < 5) {
                className = "com.mobclix.android.sdk.MobclixContactsSdk3_4";
            } else {
                className = "com.mobclix.android.sdk.MobclixContactsSdk5";
            }
            try {
                sInstance = (MobclixContacts) Class.forName(className).asSubclass(MobclixContacts.class).newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return sInstance;
    }
}
