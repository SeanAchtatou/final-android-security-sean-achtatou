package com.mobclix.android.sdk;

import java.util.HashMap;

public class MobclixAdUnitSettings {
    private boolean autoplay = false;
    private long autoplayInterval = 120000;
    private boolean customAdSet = false;
    private String customAdUrl = "";
    private boolean enabled = true;
    HashMap<String, HashMap<String, String>> openAllocationSettings = new HashMap<>();
    private long refreshTime = 30000;
    private boolean rmRequireUser = true;

    MobclixAdUnitSettings() {
    }

    /* access modifiers changed from: package-private */
    public void setEnabled(boolean v) {
        this.enabled = v;
    }

    /* access modifiers changed from: package-private */
    public boolean isEnabled() {
        return this.enabled;
    }

    /* access modifiers changed from: package-private */
    public void setAutoplay(boolean v) {
        this.autoplay = v;
    }

    /* access modifiers changed from: package-private */
    public boolean isAutoplay() {
        return this.autoplay;
    }

    /* access modifiers changed from: package-private */
    public void setRichMediaRequireUser(boolean v) {
        this.rmRequireUser = v;
    }

    /* access modifiers changed from: package-private */
    public boolean isRichMediaRequireUser() {
        return this.rmRequireUser;
    }

    /* access modifiers changed from: package-private */
    public void setRefreshTime(long v) {
        this.refreshTime = v;
    }

    /* access modifiers changed from: package-private */
    public long getRefreshTime() {
        return this.refreshTime;
    }

    /* access modifiers changed from: package-private */
    public void setAutoplayInterval(long v) {
        this.autoplayInterval = v;
    }

    /* access modifiers changed from: package-private */
    public long getAutoplayInterval() {
        return this.autoplayInterval;
    }

    /* access modifiers changed from: package-private */
    public void setCustomAdSet(boolean v) {
        this.customAdSet = v;
    }

    /* access modifiers changed from: package-private */
    public boolean isCustomAdSet() {
        return this.customAdSet;
    }

    /* access modifiers changed from: package-private */
    public void setCustomAdUrl(String v) {
        this.customAdUrl = v;
    }

    /* access modifiers changed from: package-private */
    public String getCustomAdUrl() {
        return this.customAdUrl;
    }

    /* access modifiers changed from: package-private */
    public String generatePreferenceString() {
        return String.valueOf(Boolean.toString(this.enabled)) + "," + Long.toString(this.refreshTime) + "," + Boolean.toString(this.autoplay) + "," + Long.toString(this.autoplayInterval) + "," + Boolean.toString(this.rmRequireUser);
    }
}
