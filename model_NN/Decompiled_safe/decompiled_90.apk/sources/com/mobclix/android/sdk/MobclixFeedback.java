package com.mobclix.android.sdk;

import android.app.Activity;
import com.mobclix.android.sdk.MobclixUtility;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MobclixFeedback {
    private static String TAG = "mobclixFeedback";
    static Mobclix controller = Mobclix.getInstance();

    public interface Listener {
        void onFailure();

        void onSuccess();
    }

    public static void sendComment(Activity a, String comment) {
        sendComment(a, comment, null);
    }

    public static void sendComment(Activity a, String comment, Listener l) {
        if (comment != null) {
            comment.trim();
            if (comment.length() != 0) {
                Mobclix.onCreate(a);
                String url = controller.getFeedbackServer();
                StringBuffer params = new StringBuffer();
                try {
                    params.append("p=android&t=com");
                    params.append("&a=").append(URLEncoder.encode(controller.getApplicationId(), "UTF-8"));
                    params.append("&v=").append(URLEncoder.encode(controller.getApplicationVersion(), "UTF-8"));
                    params.append("&m=").append(URLEncoder.encode(controller.getMobclixVersion(), "UTF-8"));
                    params.append("&d=").append(URLEncoder.encode(controller.getDeviceId(), "UTF-8"));
                    params.append("&dt=").append(URLEncoder.encode(controller.getDeviceModel(), "UTF-8"));
                    params.append("&os=").append(URLEncoder.encode(controller.getAndroidVersion(), "UTF-8"));
                    params.append("&c=").append(URLEncoder.encode(comment, "UTF-8"));
                    new Thread(new MobclixUtility.POSTThread(url, params.toString(), a, l)).run();
                } catch (UnsupportedEncodingException e) {
                }
            }
        }
    }

    public static void sendRatings(Activity a, Ratings ratings) {
        sendRatings(a, ratings, null);
    }

    public static void sendRatings(Activity a, Ratings ratings, Listener l) {
        if (ratings != null) {
            Mobclix.onCreate(a);
            String url = controller.getFeedbackServer();
            StringBuffer params = new StringBuffer();
            try {
                params.append("p=android&t=rat");
                params.append("&a=").append(URLEncoder.encode(controller.getApplicationId(), "UTF-8"));
                params.append("&v=").append(URLEncoder.encode(controller.getApplicationVersion(), "UTF-8"));
                params.append("&m=").append(URLEncoder.encode(controller.getMobclixVersion(), "UTF-8"));
                params.append("&d=").append(URLEncoder.encode(controller.getDeviceId(), "UTF-8"));
                params.append("&dt=").append(URLEncoder.encode(controller.getDeviceModel(), "UTF-8"));
                params.append("&os=").append(URLEncoder.encode(controller.getAndroidVersion(), "UTF-8"));
                params.append("&1=").append(ratings.a);
                params.append("&2=").append(ratings.b);
                params.append("&3=").append(ratings.c);
                params.append("&4=").append(ratings.d);
                params.append("&5=").append(ratings.e);
                new Thread(new MobclixUtility.POSTThread(url, params.toString(), a, l)).run();
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    public static class Ratings {
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;

        public void setCategoryA(int v) {
            this.a = v;
        }

        public void setCategoryB(int v) {
            this.b = v;
        }

        public void setCategoryC(int v) {
            this.c = v;
        }

        public void setCategoryD(int v) {
            this.d = v;
        }

        public void setCategoryE(int v) {
            this.e = v;
        }
    }
}
