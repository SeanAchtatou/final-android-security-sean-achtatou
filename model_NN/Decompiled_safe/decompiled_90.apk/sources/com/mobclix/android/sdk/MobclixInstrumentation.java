package com.mobclix.android.sdk;

import com.mobclix.android.sdk.MobclixUtility;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.json.JSONObject;

public class MobclixInstrumentation {
    static String ADVIEW = "adview";
    static final String[] MC_DEBUG_CATS = {STARTUP, ADVIEW};
    static String STARTUP = "startup";
    private static String TAG = "MobclixInstrumentation";
    private static final MobclixInstrumentation singleton = new MobclixInstrumentation();
    private ArrayList<String> autocloseGroups = new ArrayList<>();
    private HashMap<String, Long> currentBenchmarks = new HashMap<>();
    private JSONObject groups = new JSONObject();
    private HashMap<String, Integer> groupsStartCount = new HashMap<>();

    private MobclixInstrumentation() {
    }

    static MobclixInstrumentation getInstance() {
        return singleton;
    }

    /* access modifiers changed from: package-private */
    public void addInfo(Object object, String desc, String group) {
        if (object != null && desc != null && !desc.equals("") && group != null && !group.equals("")) {
            try {
                this.groups.getJSONObject(group).getJSONObject("data").put(desc, object);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String startGroup(String group) {
        return startGroup(group, null);
    }

    /* access modifiers changed from: package-private */
    public String startGroup(String group, String tag) {
        if (group == null || group.equals("")) {
            return null;
        }
        if (this.groups.has(group)) {
            return group;
        }
        if (tag == null || tag.equals("")) {
            tag = group;
        }
        try {
            Mobclix controller = Mobclix.getInstance();
            int startCount = 0;
            try {
                startCount = this.groupsStartCount.get(tag).intValue();
            } catch (Exception e) {
            }
            if (controller.getDebugConfig(tag) == null) {
                return null;
            }
            int groupFreq = Integer.parseInt(controller.getDebugConfig(tag));
            this.groupsStartCount.put(tag, Integer.valueOf(startCount + 1));
            if (groupFreq < 0 || (groupFreq != 0 && startCount % groupFreq != 0)) {
                return null;
            }
            JSONObject skeleton = new JSONObject();
            skeleton.put("benchmarks", new JSONObject());
            skeleton.put("data", new JSONObject());
            skeleton.put("startDate", new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ").format(new Date(System.currentTimeMillis())));
            skeleton.put("startDateNanoTime", System.nanoTime());
            this.groups.put(group, skeleton);
            return group;
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasPathStarted(String path) {
        String group;
        String[] pathComponents = path.split("/");
        if (pathComponents.length == 1) {
            group = path;
        } else {
            group = pathComponents[0];
        }
        return this.groups.has(group);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasPathFinishedBenchmarks(java.lang.String r7) {
        /*
            r6 = this;
            r5 = 0
            if (r7 == 0) goto L_0x000b
            java.lang.String r2 = ""
            boolean r2 = r7.equals(r2)
            if (r2 == 0) goto L_0x000d
        L_0x000b:
            r2 = r5
        L_0x000c:
            return r2
        L_0x000d:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r7)
            r2.<init>(r3)
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r1 = r2.toString()
            java.lang.String r2 = com.mobclix.android.sdk.MobclixInstrumentation.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Current benchmarks: "
            r3.<init>(r4)
            java.util.HashMap<java.lang.String, java.lang.Long> r4 = r6.currentBenchmarks
            java.util.Set r4 = r4.keySet()
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.v(r2, r3)
            java.util.HashMap<java.lang.String, java.lang.Long> r2 = r6.currentBenchmarks
            java.util.Set r2 = r2.keySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0048:
            boolean r3 = r2.hasNext()
            if (r3 != 0) goto L_0x0050
            r2 = 1
            goto L_0x000c
        L_0x0050:
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            int r3 = r0.length()
            int r4 = r1.length()
            if (r3 < r4) goto L_0x006e
            int r3 = r1.length()
            java.lang.String r3 = r0.substring(r5, r3)
            boolean r3 = r3.equals(r1)
            if (r3 != 0) goto L_0x0074
        L_0x006e:
            boolean r3 = r0.equals(r7)
            if (r3 == 0) goto L_0x0048
        L_0x0074:
            r2 = r5
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixInstrumentation.hasPathFinishedBenchmarks(java.lang.String):boolean");
    }

    /* access modifiers changed from: package-private */
    public void finishGroup(String group) {
        if (group != null && !group.equals("") && hasPathStarted(group)) {
            if (hasPathFinishedBenchmarks(group)) {
                try {
                    Long endDate = Long.valueOf(System.currentTimeMillis());
                    JSONObject data = this.groups.getJSONObject(group);
                    this.groups.remove(group);
                    data.put("endDate", new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ").format(new Date(endDate.longValue())));
                    data.put("totalElapsedTime", ((double) (System.nanoTime() - data.getLong("startDateNanoTime"))) / 1.0E9d);
                    data.remove("startDateNanoTime");
                    Mobclix controller = Mobclix.getInstance();
                    JSONObject envData = new JSONObject();
                    envData.put("app_id", controller.getApplicationId());
                    envData.put("platform", controller.getPlatform());
                    envData.put("sdk_ver", controller.getMobclixVersion());
                    envData.put("app_ver", controller.getApplicationVersion());
                    envData.put("udid", controller.getDeviceId());
                    envData.put("dev_model", controller.getDeviceModel());
                    envData.put("dev_vers", controller.getAndroidVersion());
                    envData.put("hw_dev_model", controller.getDeviceHardwareModel());
                    envData.put("conn", controller.getConnectionType());
                    data.put("environment", envData);
                    StringBuilder params = new StringBuilder("cat=");
                    params.append(group).append("&payload=");
                    params.append(URLEncoder.encode(data.toString(), "UTF-8"));
                    new Thread(new MobclixUtility.POSTThread(controller.getDebugServer(), params.toString(), null, null)).run();
                } catch (Exception e) {
                }
            } else if (!this.autocloseGroups.contains(group)) {
                this.autocloseGroups.add(group);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String benchmarkStart(String path, String name) {
        if (name == null || name.equals("") || path == null || path.equals("") || !hasPathStarted(path)) {
            return null;
        }
        String newPath = String.valueOf(path) + "/" + name;
        this.currentBenchmarks.put(newPath, Long.valueOf(System.nanoTime()));
        return newPath;
    }

    /* access modifiers changed from: package-private */
    public String benchmarkFinishPath(String path) {
        if (path == null || path.equals("") || !hasPathStarted(path)) {
            return null;
        }
        try {
            String[] pathComponents = path.split("/");
            if (pathComponents.length == 1) {
                return null;
            }
            Long endDate = Long.valueOf(System.nanoTime());
            String group = pathComponents[0];
            this.groups.getJSONObject(group).getJSONObject("benchmarks").put(path, ((double) (endDate.longValue() - this.currentBenchmarks.get(path).longValue())) / 1.0E9d);
            this.currentBenchmarks.remove(path);
            if (this.autocloseGroups.contains(group)) {
                finishGroup(group);
            }
            if (pathComponents.length <= 1) {
                return null;
            }
            StringBuilder newPathBuilder = new StringBuilder(pathComponents[0]);
            for (int i = 1; i < pathComponents.length - 1; i++) {
                newPathBuilder.append("/").append(pathComponents[i]);
            }
            String newPath = newPathBuilder.toString().replace(" ", "").replace("\\r", "").replace("\\n", "");
            if (newPath.length() == 0) {
                return null;
            }
            if (newPath.equals("/")) {
                return null;
            }
            return newPath;
        } catch (Exception e) {
            return null;
        }
    }
}
