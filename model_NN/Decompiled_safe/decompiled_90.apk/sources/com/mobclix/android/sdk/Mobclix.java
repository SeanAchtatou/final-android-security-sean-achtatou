package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.mobclix.android.sdk.MobclixAnalytics;
import com.mobclix.android.sdk.MobclixLocation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

public final class Mobclix {
    static final boolean DEBUG = false;
    public static final int LOG_LEVEL_DEBUG = 1;
    public static final int LOG_LEVEL_ERROR = 8;
    public static final int LOG_LEVEL_FATAL = 16;
    public static final int LOG_LEVEL_INFO = 2;
    public static final int LOG_LEVEL_WARN = 4;
    static final String[] MC_AD_SIZES = {"320x50", "300x250"};
    static final String MC_CUSTOM_AD_FILENAME = "_mc_cached_custom_ad.png";
    static final String MC_CUSTOM_AD_PREF = "CustomAdUrl";
    static final String MC_KEY_CONNECTION_TYPE = "g";
    private static final String MC_KEY_EVENT_DESCRIPTION = "ed";
    private static final String MC_KEY_EVENT_LOG_LEVEL = "el";
    private static final String MC_KEY_EVENT_NAME = "en";
    private static final String MC_KEY_EVENT_PROCESS_NAME = "ep";
    private static final String MC_KEY_EVENT_STOP = "es";
    private static final String MC_KEY_EVENT_THREAD_ID = "et";
    static final String MC_KEY_LATITUDE_LONGITUDE = "ll";
    static final String MC_KEY_SESSION_ID = "id";
    private static final String MC_KEY_TIMESTAMP = "ts";
    public static final String MC_LIBRARY_VERSION = "3.1.1";
    static final String[] MC_OPEN_ALLOCATIONS = {"openmillennial"};
    static final String PREFS_CONFIG = ".MCConfig";
    private static final String TAG = "mobclix-controller";
    static HashMap<String, MobclixAdUnitSettings> adUnitSettings = new HashMap<>();
    /* access modifiers changed from: private */
    public static final Mobclix controller = new Mobclix();
    static HashMap<String, String> debugConfig = new HashMap<>();
    private static boolean isInitialized = DEBUG;
    private String adMobApplicationId = "null";
    String adServer = "http://ads.mobclix.com/";
    String analyticsServer = "http://data.mobclix.com/post/sendData";
    private String androidId = "null";
    private String androidVersion = "null";
    private String applicationId = "null";
    private String applicationVersion = "null";
    /* access modifiers changed from: private */
    public int batteryLevel = -1;
    SoftReference<MobclixWebView> cameraWebview;
    String configServer = "http://data.mobclix.com/post/config";
    private String connectionType = "null";
    private Context context;
    String debugServer = "http://data.mobclix.com/post/debug";
    private String deviceHardwareModel = "null";
    String deviceId = "null";
    private String deviceModel = "null";
    String feedbackServer = "http://data.mobclix.com/post/feedback";
    private boolean haveLocationPermission = DEBUG;
    private MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    boolean isNewUser = DEBUG;
    boolean isOfflineSession = DEBUG;
    private String language = "null";
    /* access modifiers changed from: private */
    public String latitude = "null";
    private String locale = "null";
    MobclixLocation location = new MobclixLocation();
    private Criteria locationCriteria;
    private Handler locationHandler;
    private int logLevel = 16;
    /* access modifiers changed from: private */
    public String longitude = "null";
    private String mcc = "null";
    private String mnc = "null";
    List<String> nativeUrls = new ArrayList();
    int passiveSessionTimeout = 120000;
    HashMap<String, Boolean> permissions = new HashMap<>();
    private String platform = "android";
    String previousDeviceId = null;
    int remoteConfigSet = 0;
    private int rooted = -1;
    private String runtimePlatform = "mcnative";
    private String runtimePlatformVersion = "0";
    SoftReference<View> secondaryView;
    JSONObject session = new JSONObject();
    private SharedPreferences sharedPrefs = null;
    private String userAgent = "null";
    String vcServer = "http://vc.mobclix.com";
    SoftReference<MobclixWebView> webview;

    /* access modifiers changed from: package-private */
    public void setContext(Activity a) {
        this.context = a.getApplicationContext();
    }

    /* access modifiers changed from: package-private */
    public Context getContext() {
        return this.context;
    }

    public String getApplicationId() {
        return this.applicationId == null ? "null" : this.applicationId;
    }

    /* access modifiers changed from: package-private */
    public String getAdMobApplicationId() {
        return this.adMobApplicationId == null ? "null" : this.adMobApplicationId;
    }

    /* access modifiers changed from: package-private */
    public String getPlatform() {
        return this.platform == null ? "null" : this.platform;
    }

    /* access modifiers changed from: package-private */
    public String getRuntimePlatform() {
        return this.runtimePlatform == null ? "null" : this.runtimePlatform;
    }

    /* access modifiers changed from: package-private */
    public String getRuntimePlatformVersion() {
        return this.runtimePlatformVersion == null ? "null" : this.runtimePlatformVersion;
    }

    /* access modifiers changed from: package-private */
    public String getAndroidVersion() {
        return this.androidVersion == null ? "null" : this.androidVersion;
    }

    /* access modifiers changed from: package-private */
    public String getApplicationVersion() {
        return this.applicationVersion == null ? "null" : this.applicationVersion;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceId() {
        return this.deviceId == null ? "null" : this.deviceId;
    }

    /* access modifiers changed from: package-private */
    public String getAndroidId() {
        return this.androidId == null ? "null" : this.androidId;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceModel() {
        return this.deviceModel == null ? "null" : this.deviceModel;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceHardwareModel() {
        return this.deviceHardwareModel == null ? "null" : this.deviceHardwareModel;
    }

    /* access modifiers changed from: package-private */
    public String getConnectionType() {
        return this.connectionType == null ? "null" : this.connectionType;
    }

    /* access modifiers changed from: package-private */
    public String getLatitude() {
        return this.latitude == null ? "null" : this.latitude;
    }

    /* access modifiers changed from: package-private */
    public String getLongitude() {
        return this.longitude == null ? "null" : this.longitude;
    }

    /* access modifiers changed from: package-private */
    public String getGPS() {
        if (getLatitude().equals("null") || getLongitude().equals("null")) {
            return "null";
        }
        return String.valueOf(getLatitude()) + "," + getLongitude();
    }

    /* access modifiers changed from: package-private */
    public String getLocale() {
        return this.locale == null ? "null" : this.locale;
    }

    /* access modifiers changed from: package-private */
    public String getLanguage() {
        return this.language == null ? "null" : this.language;
    }

    /* access modifiers changed from: package-private */
    public String getMcc() {
        return this.mcc == null ? "null" : this.mcc;
    }

    /* access modifiers changed from: package-private */
    public String getMnc() {
        return this.mnc == null ? "null" : this.mnc;
    }

    /* access modifiers changed from: package-private */
    public int getBatteryLevel() {
        return this.batteryLevel;
    }

    /* access modifiers changed from: package-private */
    public String getMobclixVersion() {
        return MC_LIBRARY_VERSION;
    }

    /* access modifiers changed from: package-private */
    public int getLogLevel() {
        return this.logLevel;
    }

    /* access modifiers changed from: package-private */
    public boolean hasLocationPermission() {
        return this.haveLocationPermission;
    }

    /* access modifiers changed from: package-private */
    public String getDebugConfig(String tag) {
        try {
            return debugConfig.get(tag);
        } catch (Exception e) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isEnabled(String size) {
        try {
            return adUnitSettings.get(size).isEnabled();
        } catch (Exception e) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public long getRefreshTime(String size) {
        try {
            return adUnitSettings.get(size).getRefreshTime();
        } catch (Exception e) {
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean shouldAutoplay(String size) {
        try {
            return adUnitSettings.get(size).isAutoplay();
        } catch (Exception e) {
            return DEBUG;
        }
    }

    /* access modifiers changed from: package-private */
    public Long getAutoplayInterval(String size) {
        try {
            return Long.valueOf(adUnitSettings.get(size).getAutoplayInterval());
        } catch (Exception e) {
            return 0L;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasBeenIntervalSinceLastAutoplay(String size) {
        if (MobclixAdView.lastAutoplayTime.get(size).longValue() + getInstance().getAutoplayInterval(size).longValue() < System.currentTimeMillis()) {
            return true;
        }
        return DEBUG;
    }

    /* access modifiers changed from: package-private */
    public boolean rmRequireUserInteraction(String size) {
        try {
            return adUnitSettings.get(size).isRichMediaRequireUser();
        } catch (Exception e) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public String getCustomAdUrl(String size) {
        try {
            return adUnitSettings.get(size).getCustomAdUrl();
        } catch (Exception e) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public String getDebugServer() {
        return this.debugServer;
    }

    /* access modifiers changed from: package-private */
    public String getAdServer() {
        return this.adServer;
    }

    /* access modifiers changed from: package-private */
    public String getConfigServer() {
        return this.configServer;
    }

    /* access modifiers changed from: package-private */
    public String getAnalyticsServer() {
        return this.analyticsServer;
    }

    /* access modifiers changed from: package-private */
    public String getVcServer() {
        return this.vcServer;
    }

    /* access modifiers changed from: package-private */
    public String getFeedbackServer() {
        return this.feedbackServer;
    }

    /* access modifiers changed from: package-private */
    public List<String> getNativeUrls() {
        return this.nativeUrls;
    }

    /* access modifiers changed from: package-private */
    public int isRemoteConfigSet() {
        return this.remoteConfigSet;
    }

    /* access modifiers changed from: package-private */
    public String getUserAgent() {
        if (this.userAgent.equals("null") && hasPref("UserAgent")) {
            this.userAgent = getPref("UserAgent");
        }
        return this.userAgent;
    }

    /* access modifiers changed from: package-private */
    public void setUserAgent(String u) {
        this.userAgent = u;
        addPref("UserAgent", u);
    }

    /* access modifiers changed from: package-private */
    public boolean isRootedSet() {
        if (this.rooted != -1) {
            return true;
        }
        return DEBUG;
    }

    public boolean isDeviceRooted() {
        if (this.rooted == -1) {
            try {
                Process exec = Runtime.getRuntime().exec("su");
                this.rooted = 1;
            } catch (Exception e) {
                this.rooted = 0;
            }
            if (this.rooted == 1) {
                return true;
            }
            return DEBUG;
        } else if (this.rooted == 1) {
            return true;
        } else {
            return DEBUG;
        }
    }

    static HashMap<String, String> getAllPref() {
        try {
            return (HashMap) controller.sharedPrefs.getAll();
        } catch (Exception e) {
            return new HashMap<>();
        }
    }

    static String getPref(String k) {
        try {
            return controller.sharedPrefs.getString(k, "");
        } catch (Exception e) {
            return "";
        }
    }

    static boolean hasPref(String k) {
        try {
            return controller.sharedPrefs.contains(k);
        } catch (Exception e) {
            return DEBUG;
        }
    }

    static void addPref(String k, String v) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.putString(k, v);
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void addPref(Map<String, String> m) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            for (Map.Entry<String, String> pair : m.entrySet()) {
                spe.putString((String) pair.getKey(), (String) pair.getValue());
            }
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void removePref(String k) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.remove(k);
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void removePref(List<String> k) {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            for (int i = 0; i < k.size(); i++) {
                spe.remove(k.get(i));
            }
            spe.commit();
        } catch (Exception e) {
        }
    }

    static void clearPref() {
        try {
            SharedPreferences.Editor spe = controller.sharedPrefs.edit();
            spe.clear();
            spe.commit();
        } catch (Exception e) {
        }
    }

    private static String sha1(String string) {
        byte[] bArr = new byte[40];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(string.getBytes(), 0, string.length());
            byte[] shaHash = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : shaHash) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateSession() {
        updateConnectivity();
        try {
            if (this.haveLocationPermission) {
                this.locationHandler.sendEmptyMessage(0);
            }
        } catch (Exception e) {
        }
        try {
            this.session.put(MC_KEY_TIMESTAMP, System.currentTimeMillis());
            String loc = getGPS();
            if (!loc.equals("null")) {
                this.session.put(MC_KEY_LATITUDE_LONGITUDE, loc);
            } else {
                this.session.remove(MC_KEY_LATITUDE_LONGITUDE);
            }
            this.session.put(MC_KEY_CONNECTION_TYPE, this.connectionType);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void updateLocation() {
        this.location.getLocation(this.context, new MobclixLocation.LocationResult() {
            public void gotLocation(Location location) {
                try {
                    Mobclix.this.latitude = Double.toString(location.getLatitude());
                    Mobclix.this.longitude = Double.toString(location.getLongitude());
                } catch (Exception e) {
                }
            }
        });
    }

    private void updateConnectivity() {
        NetworkInfo network_info;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
            String network_type = "u";
            if (this.permissions.get("android.permission.ACCESS_NETWORK_STATE").booleanValue() && (network_info = connectivityManager.getActiveNetworkInfo()) != null) {
                network_type = network_info.getTypeName();
            }
            if (network_type.equals("WI_FI") || network_type.equals("WIFI")) {
                this.connectionType = "wifi";
            } else if (network_type.equals("MOBILE")) {
                this.connectionType = Integer.toString(((TelephonyManager) this.context.getSystemService("phone")).getNetworkType());
            } else {
                this.connectionType = "null";
            }
            if (this.connectionType == null) {
                this.connectionType = "null";
            }
        } catch (Exception e) {
            this.connectionType = "null";
        }
    }

    private Mobclix() {
    }

    public static Mobclix getInstance() {
        return controller;
    }

    public static final synchronized void onCreateWithApplicationId(Activity a, String appId) {
        synchronized (Mobclix.class) {
            onCreateWithApplicationId(a, appId, true);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (r5.equals(com.mobclix.android.sdk.Mobclix.controller.applicationId) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final synchronized void onCreateWithApplicationId(android.app.Activity r4, java.lang.String r5, boolean r6) {
        /*
            java.lang.Class<com.mobclix.android.sdk.Mobclix> r1 = com.mobclix.android.sdk.Mobclix.class
            monitor-enter(r1)
            if (r4 != 0) goto L_0x0010
            android.content.res.Resources$NotFoundException r2 = new android.content.res.Resources$NotFoundException     // Catch:{ all -> 0x000d }
            java.lang.String r3 = "Activity not provided."
            r2.<init>(r3)     // Catch:{ all -> 0x000d }
            throw r2     // Catch:{ all -> 0x000d }
        L_0x000d:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x0010:
            if (r5 == 0) goto L_0x0026
            if (r6 == 0) goto L_0x001e
            com.mobclix.android.sdk.Mobclix r2 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ all -> 0x000d }
            java.lang.String r2 = r2.applicationId     // Catch:{ all -> 0x000d }
            boolean r2 = r5.equals(r2)     // Catch:{ all -> 0x000d }
            if (r2 != 0) goto L_0x0026
        L_0x001e:
            com.mobclix.android.sdk.Mobclix r2 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ all -> 0x000d }
            r3 = 0
            r2.remoteConfigSet = r3     // Catch:{ all -> 0x000d }
            r2 = 0
            com.mobclix.android.sdk.Mobclix.isInitialized = r2     // Catch:{ all -> 0x000d }
        L_0x0026:
            com.mobclix.android.sdk.Mobclix r2 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ Exception -> 0x0049 }
            android.content.Context r3 = r4.getApplicationContext()     // Catch:{ Exception -> 0x0049 }
            r2.context = r3     // Catch:{ Exception -> 0x0049 }
        L_0x002e:
            boolean r2 = com.mobclix.android.sdk.Mobclix.isInitialized     // Catch:{ all -> 0x000d }
            if (r2 != 0) goto L_0x0041
            com.mobclix.android.sdk.Mobclix r2 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ MobclixPermissionException -> 0x003e, Exception -> 0x0047 }
            r2.initialize(r5)     // Catch:{ MobclixPermissionException -> 0x003e, Exception -> 0x0047 }
            com.mobclix.android.sdk.Mobclix r2 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ MobclixPermissionException -> 0x003e, Exception -> 0x0047 }
            r2.startNewSession()     // Catch:{ MobclixPermissionException -> 0x003e, Exception -> 0x0047 }
        L_0x003c:
            monitor-exit(r1)
            return
        L_0x003e:
            r2 = move-exception
            r0 = r2
            throw r0     // Catch:{ all -> 0x000d }
        L_0x0041:
            com.mobclix.android.sdk.Mobclix r2 = com.mobclix.android.sdk.Mobclix.controller     // Catch:{ all -> 0x000d }
            r2.sessionEvent()     // Catch:{ all -> 0x000d }
            goto L_0x003c
        L_0x0047:
            r2 = move-exception
            goto L_0x003c
        L_0x0049:
            r2 = move-exception
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.onCreateWithApplicationId(android.app.Activity, java.lang.String, boolean):void");
    }

    public static final synchronized void onCreate(Activity a) {
        synchronized (Mobclix.class) {
            onCreateWithApplicationId(a, null);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v0, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v110, resolved type: android.telephony.TelephonyManager} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initialize(java.lang.String r22) {
        /*
            r21 = this;
            r0 = r21
            android.content.Context r0 = r0.context
            r15 = r0
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            r0 = r21
            android.content.Context r0 = r0.context
            r17 = r0
            java.lang.String r17 = r17.getPackageName()
            java.lang.String r17 = java.lang.String.valueOf(r17)
            r16.<init>(r17)
            java.lang.String r17 = ".MCConfig"
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r16 = r16.toString()
            r17 = 0
            android.content.SharedPreferences r15 = r15.getSharedPreferences(r16, r17)
            r0 = r15
            r1 = r21
            r1.sharedPrefs = r0
            java.lang.String[] r15 = com.mobclix.android.sdk.MobclixInstrumentation.MC_DEBUG_CATS
            r0 = r15
            int r0 = r0.length
            r16 = r0
            r17 = 0
        L_0x0035:
            r0 = r17
            r1 = r16
            if (r0 < r1) goto L_0x00a7
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            if (r15 != 0) goto L_0x004b
            com.mobclix.android.sdk.MobclixInstrumentation r15 = com.mobclix.android.sdk.MobclixInstrumentation.getInstance()
            r0 = r15
            r1 = r21
            r1.instrumentation = r0
        L_0x004b:
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            java.lang.String r16 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP
            java.lang.String r7 = r15.startGroup(r16)
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            java.lang.String r16 = "init"
            r0 = r15
            r1 = r7
            r2 = r16
            java.lang.String r7 = r0.benchmarkStart(r1, r2)
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            java.lang.String r16 = "environment"
            r0 = r15
            r1 = r7
            r2 = r16
            java.lang.String r7 = r0.benchmarkStart(r1, r2)
            java.lang.String r11 = ""
            r4 = 0
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x0554 }
            r15 = r0
            java.lang.String r11 = r15.getPackageName()     // Catch:{ Exception -> 0x0554 }
        L_0x0080:
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ NameNotFoundException -> 0x00e5 }
            r15 = r0
            android.content.pm.PackageManager r15 = r15.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00e5 }
            r16 = 128(0x80, float:1.794E-43)
            r0 = r15
            r1 = r11
            r2 = r16
            android.content.pm.ApplicationInfo r4 = r0.getApplicationInfo(r1, r2)     // Catch:{ NameNotFoundException -> 0x00e5 }
        L_0x0093:
            if (r22 != 0) goto L_0x00f9
            android.os.Bundle r15 = r4.metaData     // Catch:{ NullPointerException -> 0x00ef }
            java.lang.String r16 = "com.mobclix.APPLICATION_ID"
            java.lang.String r22 = r15.getString(r16)     // Catch:{ NullPointerException -> 0x00ef }
            if (r22 != 0) goto L_0x00f9
            android.content.res.Resources$NotFoundException r15 = new android.content.res.Resources$NotFoundException
            java.lang.String r16 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r15.<init>(r16)
            throw r15
        L_0x00a7:
            r14 = r15[r17]
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            java.lang.String r19 = "debug_"
            r18.<init>(r19)
            r0 = r18
            r1 = r14
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            boolean r18 = hasPref(r18)
            if (r18 == 0) goto L_0x00e1
            java.util.HashMap<java.lang.String, java.lang.String> r18 = com.mobclix.android.sdk.Mobclix.debugConfig
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            java.lang.String r20 = "debug_"
            r19.<init>(r20)
            r0 = r19
            r1 = r14
            java.lang.StringBuilder r19 = r0.append(r1)
            java.lang.String r19 = r19.toString()
            java.lang.String r19 = getPref(r19)
            r0 = r18
            r1 = r14
            r2 = r19
            r0.put(r1, r2)
        L_0x00e1:
            int r17 = r17 + 1
            goto L_0x0035
        L_0x00e5:
            r15 = move-exception
            r6 = r15
            java.lang.String r15 = "mobclix-controller"
            java.lang.String r16 = "Application Key Started"
            android.util.Log.e(r15, r16)
            goto L_0x0093
        L_0x00ef:
            r15 = move-exception
            r6 = r15
            android.content.res.Resources$NotFoundException r15 = new android.content.res.Resources$NotFoundException
            java.lang.String r16 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r15.<init>(r16)
            throw r15
        L_0x00f9:
            r0 = r22
            r1 = r21
            r1.applicationId = r0
            android.os.Bundle r15 = r4.metaData     // Catch:{ NullPointerException -> 0x019b }
            java.lang.String r16 = "ADMOB_PUBLISHER_ID"
            java.lang.String r15 = r15.getString(r16)     // Catch:{ NullPointerException -> 0x019b }
            r0 = r15
            r1 = r21
            r1.adMobApplicationId = r0     // Catch:{ NullPointerException -> 0x019b }
            r0 = r21
            java.lang.String r0 = r0.adMobApplicationId     // Catch:{ NullPointerException -> 0x019b }
            r15 = r0
            if (r15 != 0) goto L_0x011a
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.adMobApplicationId = r0     // Catch:{ NullPointerException -> 0x019b }
        L_0x011a:
            r9 = 0
            android.os.Bundle r15 = r4.metaData     // Catch:{ Exception -> 0x0551 }
            java.lang.String r16 = "com.mobclix.LOG_LEVEL"
            java.lang.String r9 = r15.getString(r16)     // Catch:{ Exception -> 0x0551 }
        L_0x0123:
            r8 = 16
            if (r9 == 0) goto L_0x0130
            java.lang.String r15 = "debug"
            boolean r15 = r9.equalsIgnoreCase(r15)
            if (r15 == 0) goto L_0x01a6
            r8 = 1
        L_0x0130:
            r0 = r8
            r1 = r21
            r1.logLevel = r0
            r0 = r21
            java.lang.String r0 = r0.applicationId
            r15 = r0
            if (r15 == 0) goto L_0x0149
            r0 = r21
            java.lang.String r0 = r0.applicationId
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x0150
        L_0x0149:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.applicationId = r0
        L_0x0150:
            java.lang.String r15 = android.os.Build.VERSION.RELEASE
            r0 = r15
            r1 = r21
            r1.androidVersion = r0
            r0 = r21
            java.lang.String r0 = r0.androidVersion
            r15 = r0
            if (r15 == 0) goto L_0x016b
            r0 = r21
            java.lang.String r0 = r0.androidVersion
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x0172
        L_0x016b:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.androidVersion = r0
        L_0x0172:
            java.lang.String r3 = ""
            r0 = r21
            android.content.Context r0 = r0.context
            r15 = r0
            android.content.pm.PackageManager r12 = r15.getPackageManager()
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x054e }
            r15 = r0
            java.lang.String r3 = r15.getPackageName()     // Catch:{ Exception -> 0x054e }
        L_0x0186:
            java.lang.String r15 = "android.permission.INTERNET"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 == 0) goto L_0x01d3
            com.mobclix.android.sdk.Mobclix$MobclixPermissionException r15 = new com.mobclix.android.sdk.Mobclix$MobclixPermissionException
            java.lang.String r16 = "Missing required permission INTERNET."
            r0 = r15
            r1 = r21
            r2 = r16
            r0.<init>(r2)
            throw r15
        L_0x019b:
            r15 = move-exception
            r6 = r15
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.adMobApplicationId = r0
            goto L_0x011a
        L_0x01a6:
            java.lang.String r15 = "info"
            boolean r15 = r9.equalsIgnoreCase(r15)
            if (r15 == 0) goto L_0x01b0
            r8 = 2
            goto L_0x0130
        L_0x01b0:
            java.lang.String r15 = "warn"
            boolean r15 = r9.equalsIgnoreCase(r15)
            if (r15 == 0) goto L_0x01bb
            r8 = 4
            goto L_0x0130
        L_0x01bb:
            java.lang.String r15 = "error"
            boolean r15 = r9.equalsIgnoreCase(r15)
            if (r15 == 0) goto L_0x01c7
            r8 = 8
            goto L_0x0130
        L_0x01c7:
            java.lang.String r15 = "fatal"
            boolean r15 = r9.equalsIgnoreCase(r15)
            if (r15 == 0) goto L_0x0130
            r8 = 16
            goto L_0x0130
        L_0x01d3:
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.INTERNET"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
            java.lang.String r15 = "android.permission.READ_PHONE_STATE"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 == 0) goto L_0x01f8
            com.mobclix.android.sdk.Mobclix$MobclixPermissionException r15 = new com.mobclix.android.sdk.Mobclix$MobclixPermissionException
            java.lang.String r16 = "Missing required permission READ_PHONE_STATE."
            r0 = r15
            r1 = r21
            r2 = r16
            r0.<init>(r2)
            throw r15
        L_0x01f8:
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.READ_PHONE_STATE"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
            java.lang.String r15 = "android.permission.BATTERY_STATS"
            int r15 = r12.checkPermission(r15, r3)     // Catch:{ Exception -> 0x054b }
            if (r15 != 0) goto L_0x022c
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x054b }
            r15 = r0
            android.content.Context r15 = r15.getApplicationContext()     // Catch:{ Exception -> 0x054b }
            com.mobclix.android.sdk.Mobclix$2 r16 = new com.mobclix.android.sdk.Mobclix$2     // Catch:{ Exception -> 0x054b }
            r0 = r16
            r1 = r21
            r0.<init>()     // Catch:{ Exception -> 0x054b }
            android.content.IntentFilter r17 = new android.content.IntentFilter     // Catch:{ Exception -> 0x054b }
            java.lang.String r18 = "android.intent.action.BATTERY_CHANGED"
            r17.<init>(r18)     // Catch:{ Exception -> 0x054b }
            r15.registerReceiver(r16, r17)     // Catch:{ Exception -> 0x054b }
        L_0x022c:
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x054b }
            r15 = r0
            java.lang.String r16 = "android.permission.BATTERY_STATS"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)     // Catch:{ Exception -> 0x054b }
            r15.put(r16, r17)     // Catch:{ Exception -> 0x054b }
        L_0x023c:
            java.lang.String r15 = "android.permission.CAMERA"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 != 0) goto L_0x0254
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.CAMERA"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
        L_0x0254:
            java.lang.String r15 = "android.permission.READ_CALENDAR"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 != 0) goto L_0x026c
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.READ_CALENDAR"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
        L_0x026c:
            java.lang.String r15 = "android.permission.WRITE_CALENDAR"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 != 0) goto L_0x0284
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.WRITE_CALENDAR"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
        L_0x0284:
            java.lang.String r15 = "android.permission.READ_CONTACTS"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 != 0) goto L_0x029c
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.READ_CONTACTS"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
        L_0x029c:
            java.lang.String r15 = "android.permission.WRITE_CONTACTS"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 != 0) goto L_0x02b4
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.WRITE_CONTACTS"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
        L_0x02b4:
            java.lang.String r15 = "android.permission.GET_ACCOUNTS"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 != 0) goto L_0x02cc
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.GET_ACCOUNTS"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
        L_0x02cc:
            java.lang.String r15 = "android.permission.VIBRATE"
            int r15 = r12.checkPermission(r15, r3)
            if (r15 != 0) goto L_0x02e4
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions
            r15 = r0
            java.lang.String r16 = "android.permission.VIBRATE"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)
            r15.put(r16, r17)
        L_0x02e4:
            java.lang.String r15 = "android.permission.ACCESS_FINE_LOCATION"
            int r15 = r12.checkPermission(r15, r3)     // Catch:{ Exception -> 0x0510 }
            if (r15 != 0) goto L_0x04dc
            android.location.Criteria r15 = new android.location.Criteria     // Catch:{ Exception -> 0x0510 }
            r15.<init>()     // Catch:{ Exception -> 0x0510 }
            r0 = r15
            r1 = r21
            r1.locationCriteria = r0     // Catch:{ Exception -> 0x0510 }
            r0 = r21
            android.location.Criteria r0 = r0.locationCriteria     // Catch:{ Exception -> 0x0510 }
            r15 = r0
            r16 = 1
            r15.setAccuracy(r16)     // Catch:{ Exception -> 0x0510 }
            r15 = 1
            r0 = r15
            r1 = r21
            r1.haveLocationPermission = r0     // Catch:{ Exception -> 0x0510 }
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x0510 }
            r15 = r0
            java.lang.String r16 = "android.permission.ACCESS_FINE_LOCATION"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)     // Catch:{ Exception -> 0x0510 }
            r15.put(r16, r17)     // Catch:{ Exception -> 0x0510 }
        L_0x0316:
            java.lang.String r15 = "android.permission.ACCESS_NETWORK_STATE"
            int r15 = r12.checkPermission(r15, r3)     // Catch:{ Exception -> 0x0510 }
            if (r15 != 0) goto L_0x032e
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x0510 }
            r15 = r0
            java.lang.String r16 = "android.permission.ACCESS_NETWORK_STATE"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)     // Catch:{ Exception -> 0x0510 }
            r15.put(r16, r17)     // Catch:{ Exception -> 0x0510 }
        L_0x032e:
            r13 = 0
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x0548 }
            r15 = r0
            java.lang.String r16 = "phone"
            java.lang.Object r14 = r15.getSystemService(r16)     // Catch:{ Exception -> 0x0548 }
            r0 = r14
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0548 }
            r13 = r0
        L_0x033e:
            r15 = 0
            android.content.pm.PackageInfo r15 = r12.getPackageInfo(r3, r15)     // Catch:{ Exception -> 0x0545 }
            java.lang.String r15 = r15.versionName     // Catch:{ Exception -> 0x0545 }
            r0 = r15
            r1 = r21
            r1.applicationVersion = r0     // Catch:{ Exception -> 0x0545 }
        L_0x034a:
            r0 = r21
            java.lang.String r0 = r0.applicationVersion
            r15 = r0
            if (r15 == 0) goto L_0x035e
            r0 = r21
            java.lang.String r0 = r0.applicationVersion
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x0365
        L_0x035e:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.applicationVersion = r0
        L_0x0365:
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x051b }
            r15 = r0
            android.content.ContentResolver r15 = r15.getContentResolver()     // Catch:{ Exception -> 0x051b }
            java.lang.String r16 = "android_id"
            java.lang.String r15 = android.provider.Settings.System.getString(r15, r16)     // Catch:{ Exception -> 0x051b }
            r0 = r15
            r1 = r21
            r1.androidId = r0     // Catch:{ Exception -> 0x051b }
        L_0x0379:
            r0 = r21
            java.lang.String r0 = r0.androidId
            r15 = r0
            if (r15 == 0) goto L_0x038d
            r0 = r21
            java.lang.String r0 = r0.androidId
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x0394
        L_0x038d:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.androidId = r0
        L_0x0394:
            java.lang.String r15 = r13.getDeviceId()     // Catch:{ Exception -> 0x0542 }
            r0 = r15
            r1 = r21
            r1.deviceId = r0     // Catch:{ Exception -> 0x0542 }
        L_0x039d:
            r0 = r21
            java.lang.String r0 = r0.deviceId
            r15 = r0
            if (r15 == 0) goto L_0x03b1
            r0 = r21
            java.lang.String r0 = r0.deviceId
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x03bb
        L_0x03b1:
            r0 = r21
            java.lang.String r0 = r0.androidId
            r15 = r0
            r0 = r15
            r1 = r21
            r1.deviceId = r0
        L_0x03bb:
            java.lang.String r15 = android.os.Build.MODEL     // Catch:{ Exception -> 0x053f }
            r0 = r15
            r1 = r21
            r1.deviceModel = r0     // Catch:{ Exception -> 0x053f }
        L_0x03c2:
            r0 = r21
            java.lang.String r0 = r0.deviceModel
            r15 = r0
            if (r15 == 0) goto L_0x03d6
            r0 = r21
            java.lang.String r0 = r0.deviceModel
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x03dd
        L_0x03d6:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.deviceModel = r0
        L_0x03dd:
            java.lang.String r15 = android.os.Build.DEVICE     // Catch:{ Exception -> 0x053c }
            r0 = r15
            r1 = r21
            r1.deviceHardwareModel = r0     // Catch:{ Exception -> 0x053c }
        L_0x03e4:
            r0 = r21
            java.lang.String r0 = r0.deviceHardwareModel
            r15 = r0
            if (r15 == 0) goto L_0x03f8
            r0 = r21
            java.lang.String r0 = r0.deviceHardwareModel
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x03ff
        L_0x03f8:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.deviceHardwareModel = r0
        L_0x03ff:
            java.lang.String r10 = r13.getNetworkOperator()     // Catch:{ Exception -> 0x0539 }
            if (r10 == 0) goto L_0x041f
            r15 = 0
            r16 = 3
            r0 = r10
            r1 = r15
            r2 = r16
            java.lang.String r15 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0539 }
            r0 = r15
            r1 = r21
            r1.mcc = r0     // Catch:{ Exception -> 0x0539 }
            r15 = 3
            java.lang.String r15 = r10.substring(r15)     // Catch:{ Exception -> 0x0539 }
            r0 = r15
            r1 = r21
            r1.mnc = r0     // Catch:{ Exception -> 0x0539 }
        L_0x041f:
            r0 = r21
            java.lang.String r0 = r0.mcc
            r15 = r0
            if (r15 == 0) goto L_0x0433
            r0 = r21
            java.lang.String r0 = r0.mcc
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x043a
        L_0x0433:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.mcc = r0
        L_0x043a:
            r0 = r21
            java.lang.String r0 = r0.mnc
            r15 = r0
            if (r15 == 0) goto L_0x044e
            r0 = r21
            java.lang.String r0 = r0.mnc
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x0455
        L_0x044e:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.mnc = r0
        L_0x0455:
            java.util.Locale r5 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0536 }
            java.lang.String r15 = r5.toString()     // Catch:{ Exception -> 0x0536 }
            r0 = r15
            r1 = r21
            r1.locale = r0     // Catch:{ Exception -> 0x0536 }
            java.lang.String r15 = r5.getLanguage()     // Catch:{ Exception -> 0x0536 }
            r0 = r15
            r1 = r21
            r1.language = r0     // Catch:{ Exception -> 0x0536 }
        L_0x046b:
            r0 = r21
            java.lang.String r0 = r0.locale
            r15 = r0
            if (r15 == 0) goto L_0x047f
            r0 = r21
            java.lang.String r0 = r0.locale
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x0486
        L_0x047f:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.locale = r0
        L_0x0486:
            r0 = r21
            java.lang.String r0 = r0.language
            r15 = r0
            if (r15 == 0) goto L_0x049a
            r0 = r21
            java.lang.String r0 = r0.language
            r15 = r0
            java.lang.String r16 = ""
            boolean r15 = r15.equals(r16)
            if (r15 == 0) goto L_0x04a1
        L_0x049a:
            java.lang.String r15 = "null"
            r0 = r15
            r1 = r21
            r1.language = r0
        L_0x04a1:
            com.mobclix.android.sdk.Mobclix$3 r15 = new com.mobclix.android.sdk.Mobclix$3
            r0 = r15
            r1 = r21
            r0.<init>()
            r0 = r15
            r1 = r21
            r1.locationHandler = r0
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            java.lang.String r7 = r15.benchmarkFinishPath(r7)
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            java.lang.String r16 = "session"
            r0 = r15
            r1 = r7
            r2 = r16
            java.lang.String r7 = r0.benchmarkStart(r1, r2)
            r15 = 1
            com.mobclix.android.sdk.Mobclix.isInitialized = r15
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            java.lang.String r7 = r15.benchmarkFinishPath(r7)
            r0 = r21
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation
            r15 = r0
            java.lang.String r7 = r15.benchmarkFinishPath(r7)
            return
        L_0x04dc:
            java.lang.String r15 = "android.permission.ACCESS_COARSE_LOCATION"
            int r15 = r12.checkPermission(r15, r3)     // Catch:{ Exception -> 0x0510 }
            if (r15 != 0) goto L_0x0513
            android.location.Criteria r15 = new android.location.Criteria     // Catch:{ Exception -> 0x0510 }
            r15.<init>()     // Catch:{ Exception -> 0x0510 }
            r0 = r15
            r1 = r21
            r1.locationCriteria = r0     // Catch:{ Exception -> 0x0510 }
            r0 = r21
            android.location.Criteria r0 = r0.locationCriteria     // Catch:{ Exception -> 0x0510 }
            r15 = r0
            r16 = 2
            r15.setAccuracy(r16)     // Catch:{ Exception -> 0x0510 }
            r15 = 1
            r0 = r15
            r1 = r21
            r1.haveLocationPermission = r0     // Catch:{ Exception -> 0x0510 }
            r0 = r21
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r0.permissions     // Catch:{ Exception -> 0x0510 }
            r15 = r0
            java.lang.String r16 = "android.permission.ACCESS_COARSE_LOCATION"
            r17 = 1
            java.lang.Boolean r17 = java.lang.Boolean.valueOf(r17)     // Catch:{ Exception -> 0x0510 }
            r15.put(r16, r17)     // Catch:{ Exception -> 0x0510 }
            goto L_0x0316
        L_0x0510:
            r15 = move-exception
            goto L_0x032e
        L_0x0513:
            r15 = 0
            r0 = r15
            r1 = r21
            r1.haveLocationPermission = r0     // Catch:{ Exception -> 0x0510 }
            goto L_0x0316
        L_0x051b:
            r15 = move-exception
            r6 = r15
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ Exception -> 0x0533 }
            r15 = r0
            android.content.ContentResolver r15 = r15.getContentResolver()     // Catch:{ Exception -> 0x0533 }
            java.lang.String r16 = "android_id"
            java.lang.String r15 = android.provider.Settings.System.getString(r15, r16)     // Catch:{ Exception -> 0x0533 }
            r0 = r15
            r1 = r21
            r1.androidId = r0     // Catch:{ Exception -> 0x0533 }
            goto L_0x0379
        L_0x0533:
            r15 = move-exception
            goto L_0x0379
        L_0x0536:
            r15 = move-exception
            goto L_0x046b
        L_0x0539:
            r15 = move-exception
            goto L_0x041f
        L_0x053c:
            r15 = move-exception
            goto L_0x03e4
        L_0x053f:
            r15 = move-exception
            goto L_0x03c2
        L_0x0542:
            r15 = move-exception
            goto L_0x039d
        L_0x0545:
            r15 = move-exception
            goto L_0x034a
        L_0x0548:
            r15 = move-exception
            goto L_0x033e
        L_0x054b:
            r15 = move-exception
            goto L_0x023c
        L_0x054e:
            r15 = move-exception
            goto L_0x0186
        L_0x0551:
            r15 = move-exception
            goto L_0x0123
        L_0x0554:
            r15 = move-exception
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.initialize(java.lang.String):void");
    }

    public static final synchronized void onStop(Activity a) {
        synchronized (Mobclix.class) {
            controller.sessionEvent();
        }
    }

    public static final void logEvent(int eventLogLevel, String processName, String eventName, String description, boolean stopProcess) {
        if (!isInitialized) {
            Log.v(TAG, "logEvent failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (eventLogLevel >= controller.logLevel) {
            String logString = String.valueOf(processName) + ", " + eventName + ": " + description;
            switch (eventLogLevel) {
                case 1:
                    Log.d("Mobclix", logString);
                    break;
                case 2:
                    Log.i("Mobclix", logString);
                    break;
                case 4:
                    Log.w("Mobclix", logString);
                    break;
                case 8:
                    Log.e("Mobclix", logString);
                    break;
                case 16:
                    Log.e("Mobclix", logString);
                    break;
            }
            try {
                JSONObject event = new JSONObject(controller.session, new String[]{MC_KEY_TIMESTAMP, MC_KEY_LATITUDE_LONGITUDE, MC_KEY_CONNECTION_TYPE, MC_KEY_SESSION_ID});
                event.put(MC_KEY_EVENT_LOG_LEVEL, Integer.toString(eventLogLevel));
                event.put(MC_KEY_EVENT_PROCESS_NAME, URLEncoder.encode(processName, "UTF-8"));
                event.put(MC_KEY_EVENT_NAME, URLEncoder.encode(eventName, "UTF-8"));
                event.put(MC_KEY_EVENT_DESCRIPTION, URLEncoder.encode(description, "UTF-8"));
                event.put(MC_KEY_EVENT_THREAD_ID, Long.toString(Thread.currentThread().getId()));
                event.put(MC_KEY_EVENT_STOP, stopProcess ? "1" : "0");
                new Thread(new MobclixAnalytics.LogEvent(event)).start();
            } catch (Exception e) {
            }
        }
    }

    public static final void sync() {
        if (!isInitialized) {
            Log.v(TAG, "sync failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (MobclixAnalytics.getSyncStatus() == MobclixAnalytics.SYNC_READY) {
            new Thread(new MobclixAnalytics.Sync()).start();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void sessionEvent() {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            java.lang.String r2 = "lastSessionEvent"
            boolean r2 = hasPref(r2)     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            if (r2 == 0) goto L_0x0015
            java.lang.String r2 = "lastSessionEvent"
            java.lang.String r2 = getPref(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0036 }
            long r0 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0036 }
        L_0x0015:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            int r4 = r6.passiveSessionTimeout     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            long r4 = (long) r4     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            long r4 = r4 + r0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0026
            r6.startNewSession()     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
        L_0x0024:
            monitor-exit(r6)
            return
        L_0x0026:
            java.lang.String r2 = "lastSessionEvent"
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            java.lang.String r3 = java.lang.Long.toString(r3)     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            addPref(r2, r3)     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            goto L_0x0024
        L_0x0034:
            r2 = move-exception
            goto L_0x0024
        L_0x0036:
            r2 = move-exception
            monitor-exit(r6)
            throw r2
        L_0x0039:
            r2 = move-exception
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.sessionEvent():void");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void startNewSession() {
        /*
            r19 = this;
            monitor-enter(r19)
            r2 = 0
            r5 = 0
            java.lang.String r14 = "firstSessionEvent"
            boolean r14 = hasPref(r14)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            if (r14 == 0) goto L_0x0017
            java.lang.String r14 = "firstSessionEvent"
            java.lang.String r14 = getPref(r14)     // Catch:{ Exception -> 0x013b, all -> 0x012b }
            long r2 = java.lang.Long.parseLong(r14)     // Catch:{ Exception -> 0x013b, all -> 0x012b }
        L_0x0017:
            java.lang.String r14 = "lastSessionEvent"
            boolean r14 = hasPref(r14)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            if (r14 == 0) goto L_0x0029
            java.lang.String r14 = "lastSessionEvent"
            java.lang.String r14 = getPref(r14)     // Catch:{ Exception -> 0x0138, all -> 0x012b }
            long r5 = java.lang.Long.parseLong(r14)     // Catch:{ Exception -> 0x0138, all -> 0x012b }
        L_0x0029:
            long r10 = r5 - r2
            r14 = 0
            int r14 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r14 == 0) goto L_0x0037
            r14 = 0
            int r14 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1))
            if (r14 != 0) goto L_0x0039
        L_0x0037:
            r10 = 1000(0x3e8, double:4.94E-321)
        L_0x0039:
            java.lang.String r14 = "totalSessionTime"
            boolean r14 = hasPref(r14)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            if (r14 == 0) goto L_0x004c
            java.lang.String r14 = "totalSessionTime"
            java.lang.String r14 = getPref(r14)     // Catch:{ Exception -> 0x0135, all -> 0x012b }
            long r14 = java.lang.Long.parseLong(r14)     // Catch:{ Exception -> 0x0135, all -> 0x012b }
            long r10 = r10 + r14
        L_0x004c:
            java.util.HashMap r9 = new java.util.HashMap     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r9.<init>()     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.String r14 = "totalSessionTime"
            java.lang.String r15 = java.lang.Long.toString(r10)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r9.put(r14, r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            boolean r0 = r0.isOfflineSession     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            if (r14 == 0) goto L_0x007f
            r7 = 1
            java.lang.String r14 = "offlineSessions"
            boolean r14 = hasPref(r14)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            if (r14 == 0) goto L_0x0075
            java.lang.String r14 = "offlineSessions"
            java.lang.String r14 = getPref(r14)     // Catch:{ Exception -> 0x0132, all -> 0x012b }
            int r14 = java.lang.Integer.parseInt(r14)     // Catch:{ Exception -> 0x0132, all -> 0x012b }
            int r7 = r7 + r14
        L_0x0075:
            java.lang.String r14 = "offlineSessions"
            long r15 = (long) r7     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.String r15 = java.lang.Long.toString(r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r9.put(r14, r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
        L_0x007f:
            java.lang.String r14 = "firstSessionEvent"
            long r15 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r17 = 1000(0x3e8, double:4.94E-321)
            long r15 = r15 - r17
            java.lang.String r15 = java.lang.Long.toString(r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r9.put(r14, r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.String r14 = "lastSessionEvent"
            long r15 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.String r15 = java.lang.Long.toString(r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r9.put(r14, r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            addPref(r9)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            java.lang.String r15 = com.mobclix.android.sdk.MobclixInstrumentation.STARTUP     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.String r4 = r14.startGroup(r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            java.lang.String r15 = "session"
            java.lang.String r4 = r14.benchmarkStart(r4, r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            java.lang.String r15 = "init"
            java.lang.String r4 = r14.benchmarkStart(r4, r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            java.lang.String r0 = r0.deviceId     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r15 = r0
            java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14.<init>(r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.StringBuilder r14 = r14.append(r12)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            java.lang.String r8 = sha1(r14)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            org.json.JSONObject r0 = r0.session     // Catch:{ Exception -> 0x0130, all -> 0x012b }
            r14 = r0
            java.lang.String r15 = "id"
            java.lang.String r16 = "UTF-8"
            r0 = r8
            r1 = r16
            java.lang.String r16 = java.net.URLEncoder.encode(r0, r1)     // Catch:{ Exception -> 0x0130, all -> 0x012b }
            r14.put(r15, r16)     // Catch:{ Exception -> 0x0130, all -> 0x012b }
        L_0x00f2:
            r0 = r19
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            java.lang.String r4 = r14.benchmarkFinishPath(r4)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            java.lang.String r15 = "config"
            java.lang.String r4 = r14.benchmarkStart(r4, r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = 0
            r0 = r14
            r1 = r19
            r1.remoteConfigSet = r0     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            com.mobclix.android.sdk.FetchRemoteConfig r14 = new com.mobclix.android.sdk.FetchRemoteConfig     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14.<init>()     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r15 = 0
            java.lang.String[] r15 = new java.lang.String[r15]     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14.execute(r15)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            java.lang.String r4 = r14.benchmarkFinishPath(r4)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r0 = r19
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r0.instrumentation     // Catch:{ Exception -> 0x012e, all -> 0x012b }
            r14 = r0
            java.lang.String r4 = r14.benchmarkFinishPath(r4)     // Catch:{ Exception -> 0x012e, all -> 0x012b }
        L_0x0129:
            monitor-exit(r19)
            return
        L_0x012b:
            r14 = move-exception
            monitor-exit(r19)
            throw r14
        L_0x012e:
            r14 = move-exception
            goto L_0x0129
        L_0x0130:
            r14 = move-exception
            goto L_0x00f2
        L_0x0132:
            r14 = move-exception
            goto L_0x0075
        L_0x0135:
            r14 = move-exception
            goto L_0x004c
        L_0x0138:
            r14 = move-exception
            goto L_0x0029
        L_0x013b:
            r14 = move-exception
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.startNewSession():void");
    }

    class MobclixPermissionException extends RuntimeException {
        private static final long serialVersionUID = -2362572513974509340L;

        MobclixPermissionException(String detailMessage) {
            super(detailMessage);
        }
    }

    static String getCookieStringFromCookieManager(String url) {
        try {
            return CookieManager.getInstance().getCookie(url);
        } catch (Exception e) {
            return "";
        }
    }

    static void syncCookiesToCookieManager(CookieStore cs, String url) {
        try {
            CookieManager cookieManager = CookieManager.getInstance();
            List<Cookie> cookies = cs.getCookies();
            StringBuffer cookieStringBuffer = new StringBuffer();
            if (!cookies.isEmpty()) {
                for (int i = 0; i < cookies.size(); i++) {
                    Cookie c = (Cookie) cookies.get(i);
                    cookieStringBuffer.append(c.getName()).append("=").append(c.getValue());
                    if (c.getExpiryDate() != null) {
                        cookieStringBuffer.append("; expires=").append(new SimpleDateFormat("E, dd-MMM-yyyy HH:mm:ss").format(c.getExpiryDate())).append(" GMT");
                    }
                    if (c.getPath() != null) {
                        cookieStringBuffer.append("; path=").append(c.getPath());
                    }
                    if (c.getDomain() != null) {
                        cookieStringBuffer.append("; domain=").append(c.getDomain());
                    }
                    cookieManager.setCookie(url, cookieStringBuffer.toString());
                }
                CookieSyncManager.getInstance().sync();
                CookieSyncManager.getInstance().stopSync();
            }
        } catch (Exception e) {
        }
    }

    static class MobclixHttpClient extends DefaultHttpClient {
        HttpGet httpGet = new HttpGet(this.url);
        String url;

        public MobclixHttpClient(String u) {
            this.url = u;
            this.httpGet.setHeader("Cookie", Mobclix.getCookieStringFromCookieManager(this.url));
            this.httpGet.setHeader("User-Agent", Mobclix.controller.getUserAgent());
        }

        public HttpResponse execute() throws ClientProtocolException, IOException {
            try {
                HttpResponse httpResponse = Mobclix.super.execute(this.httpGet);
                Mobclix.syncCookiesToCookieManager(getCookieStore(), this.url);
                return httpResponse;
            } catch (Throwable th) {
                return null;
            }
        }
    }

    static class BitmapHandler extends Handler {
        protected Bitmap bmImg = null;
        protected Object state = null;

        BitmapHandler() {
        }

        public void setBitmap(Bitmap bm) {
            this.bmImg = bm;
        }

        public void setState(Object o) {
            this.state = o;
        }

        public void handleMessage(Message msg) {
            if (this.bmImg != null) {
                this.bmImg.recycle();
            }
        }
    }

    static class FetchImageThread implements Runnable {
        private Bitmap bmImg;
        private BitmapHandler handler;
        private String imageUrl;

        FetchImageThread(String url, BitmapHandler h) {
            this.imageUrl = url;
            this.handler = h;
        }

        public void run() {
            try {
                HttpEntity httpEntity = new MobclixHttpClient(this.imageUrl).execute().getEntity();
                this.bmImg = BitmapFactory.decodeStream(httpEntity.getContent());
                httpEntity.consumeContent();
                this.handler.setBitmap(this.bmImg);
            } catch (Throwable th) {
            }
            this.handler.sendEmptyMessage(0);
        }
    }

    static class FetchResponseThread extends TimerTask implements Runnable {
        private Handler handler;
        private String url;

        FetchResponseThread(String u, Handler h) {
            this.url = u;
            this.handler = h;
        }

        public void run() {
            int errorCode;
            Mobclix.controller.updateSession();
            if (this.url.equals("")) {
                sendErrorCode(-503);
            }
            String response = "";
            BufferedReader br = null;
            try {
                HttpResponse httpResponse = new MobclixHttpClient(this.url).execute();
                HttpEntity httpEntity = httpResponse.getEntity();
                int responseCode = httpResponse.getStatusLine().getStatusCode();
                if ((responseCode != 200 && responseCode != 251) || httpEntity == null) {
                    switch (responseCode) {
                        case 251:
                            String sub = httpResponse.getFirstHeader("X-Mobclix-Suballocation").getValue();
                            if (sub == null) {
                                errorCode = -503;
                            } else {
                                errorCode = Integer.parseInt(sub);
                            }
                            sendErrorCode(errorCode);
                            break;
                        default:
                            errorCode = -503;
                            sendErrorCode(errorCode);
                            break;
                    }
                } else {
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(httpEntity.getContent()), 8000);
                    try {
                        for (String tmp = br2.readLine(); tmp != null; tmp = br2.readLine()) {
                            response = String.valueOf(response) + tmp;
                        }
                        httpEntity.consumeContent();
                        if (!response.equals("")) {
                            Message msg = new Message();
                            Bundle bundle = new Bundle();
                            bundle.putString("type", "success");
                            bundle.putString("response", response);
                            msg.setData(bundle);
                            this.handler.sendMessage(msg);
                            br = br2;
                        } else {
                            br = br2;
                        }
                    } catch (Throwable th) {
                        th = th;
                        br = br2;
                        try {
                            br.close();
                        } catch (Exception e) {
                        }
                        throw th;
                    }
                }
                try {
                    br.close();
                    return;
                } catch (Exception e2) {
                    return;
                }
            } catch (Throwable th2) {
            }
            try {
                sendErrorCode(-503);
                try {
                    br.close();
                } catch (Exception e3) {
                }
            } catch (Throwable th3) {
                th = th3;
                br.close();
                throw th;
            }
        }

        /* access modifiers changed from: package-private */
        public void setUrl(String u) {
            this.url = u;
        }

        private void sendErrorCode(int errorCode) {
            Message msg = new Message();
            Bundle bundle = new Bundle();
            bundle.putString("type", "failure");
            bundle.putInt("errorCode", errorCode);
            msg.setData(bundle);
            this.handler.sendMessage(msg);
        }
    }

    static class ObjectOnClickListener implements DialogInterface.OnClickListener {
        Object obj1;
        Object obj2;
        Object obj3;

        public ObjectOnClickListener(Object o) {
            this.obj1 = o;
        }

        public ObjectOnClickListener(Object o, Object o2) {
            this.obj1 = o;
            this.obj2 = o2;
        }

        public ObjectOnClickListener(Object o, Object o2, Object o3) {
            this.obj1 = o;
            this.obj2 = o2;
            this.obj3 = o3;
        }

        public void onClick(DialogInterface dialog, int which) {
        }
    }
}
