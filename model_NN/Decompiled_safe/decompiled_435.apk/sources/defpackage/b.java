package defpackage;

import android.content.Context;
import android.os.AsyncTask;
import com.google.ads.AdRequest;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;
import org.objectweb.asm.Opcodes;

/* renamed from: b  reason: default package */
public final class b extends AsyncTask<String, Void, Void> {
    private c a;
    private d b;
    private Context c;

    b(c cVar, d dVar, Context context) {
        this.a = cVar;
        this.b = dVar;
        this.c = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(String... strArr) {
        HttpURLConnection httpURLConnection;
        int responseCode;
        String str = strArr[0];
        while (true) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                AdUtil.a(httpURLConnection, this.c);
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                if (300 <= responseCode && responseCode < 400) {
                    str = httpURLConnection.getHeaderField("Location");
                    if (str == null) {
                        a.c("Could not get redirect location from a " + responseCode + " redirect.");
                        this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                        return null;
                    }
                    a(httpURLConnection);
                }
            } catch (MalformedURLException e) {
                a.a("Received malformed ad url from javascript.", e);
                this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                return null;
            } catch (IOException e2) {
                a.c("IOException connecting to ad url.", e2);
                this.a.a(AdRequest.ErrorCode.NETWORK_ERROR);
                return null;
            }
        }
        if (responseCode == 200) {
            a(httpURLConnection);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()), Opcodes.ACC_SYNTHETIC);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\n");
            }
            String sb2 = sb.toString();
            a.a("Response content is: " + sb2);
            if (sb2 == null || sb2.trim().length() <= 0) {
                a.a("Response message is null or zero length: " + sb2);
                this.a.a(AdRequest.ErrorCode.NO_FILL);
                return null;
            }
            this.a.a(sb2, str);
            return null;
        } else if (responseCode == 400) {
            a.c("Bad request");
            this.a.a(AdRequest.ErrorCode.INVALID_REQUEST);
            return null;
        } else {
            a.c("Invalid response code: " + responseCode);
            this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
            return null;
        }
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                this.b.a(Float.parseFloat(headerField2));
                if (!this.b.p()) {
                    this.b.d();
                }
            } catch (NumberFormatException e) {
                a.c("Could not get refresh value: " + headerField2, e);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.b.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e2) {
                a.c("Could not get timeout value: " + headerField3, e2);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 == null) {
            return;
        }
        if (headerField4.equals("portrait")) {
            this.b.a(1);
        } else if (headerField4.equals("landscape")) {
            this.b.a(0);
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.b(stringTokenizer.nextToken());
            }
        }
    }
}
