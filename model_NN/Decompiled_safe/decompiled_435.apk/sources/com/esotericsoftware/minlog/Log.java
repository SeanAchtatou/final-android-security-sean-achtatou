package com.esotericsoftware.minlog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import org.objectweb.asm.Opcodes;

public class Log {
    public static boolean DEBUG = false;
    public static boolean ERROR = (level <= 5);
    public static boolean INFO = false;
    public static final int LEVEL_DEBUG = 2;
    public static final int LEVEL_ERROR = 5;
    public static final int LEVEL_INFO = 3;
    public static final int LEVEL_NONE = 6;
    public static final int LEVEL_TRACE = 1;
    public static final int LEVEL_WARN = 4;
    public static boolean TRACE;
    public static boolean WARN;
    private static int level = 3;
    private static Logger logger = new Logger();

    static {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (level <= 4) {
            z = true;
        } else {
            z = false;
        }
        WARN = z;
        if (level <= 3) {
            z2 = true;
        } else {
            z2 = false;
        }
        INFO = z2;
        if (level <= 2) {
            z3 = true;
        } else {
            z3 = false;
        }
        DEBUG = z3;
        if (level <= 1) {
            z4 = true;
        } else {
            z4 = false;
        }
        TRACE = z4;
    }

    public static void set(int i) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        level = i;
        ERROR = i <= 5;
        if (i <= 4) {
            z = true;
        } else {
            z = false;
        }
        WARN = z;
        if (i <= 3) {
            z2 = true;
        } else {
            z2 = false;
        }
        INFO = z2;
        if (i <= 2) {
            z3 = true;
        } else {
            z3 = false;
        }
        DEBUG = z3;
        if (i <= 1) {
            z4 = true;
        } else {
            z4 = false;
        }
        TRACE = z4;
    }

    public static void NONE() {
        set(6);
    }

    public static void ERROR() {
        set(5);
    }

    public static void WARN() {
        set(4);
    }

    public static void INFO() {
        set(3);
    }

    public static void DEBUG() {
        set(2);
    }

    public static void TRACE() {
        set(1);
    }

    public static void setLogger(Logger logger2) {
        logger = logger2;
    }

    public static void error(String str, Throwable th) {
        if (ERROR) {
            logger.log(5, null, str, th);
        }
    }

    public static void error(String str, String str2, Throwable th) {
        if (ERROR) {
            logger.log(5, str, str2, th);
        }
    }

    public static void error(String str) {
        if (ERROR) {
            logger.log(5, null, str, null);
        }
    }

    public static void error(String str, String str2) {
        if (ERROR) {
            logger.log(5, str, str2, null);
        }
    }

    public static void warn(String str, Throwable th) {
        if (WARN) {
            logger.log(4, null, str, th);
        }
    }

    public static void warn(String str, String str2, Throwable th) {
        if (WARN) {
            logger.log(4, str, str2, th);
        }
    }

    public static void warn(String str) {
        if (WARN) {
            logger.log(4, null, str, null);
        }
    }

    public static void warn(String str, String str2) {
        if (WARN) {
            logger.log(4, str, str2, null);
        }
    }

    public static void info(String str, Throwable th) {
        if (INFO) {
            logger.log(3, null, str, th);
        }
    }

    public static void info(String str, String str2, Throwable th) {
        if (INFO) {
            logger.log(3, str, str2, th);
        }
    }

    public static void info(String str) {
        if (INFO) {
            logger.log(3, null, str, null);
        }
    }

    public static void info(String str, String str2) {
        if (INFO) {
            logger.log(3, str, str2, null);
        }
    }

    public static void debug(String str, Throwable th) {
        if (DEBUG) {
            logger.log(2, null, str, th);
        }
    }

    public static void debug(String str, String str2, Throwable th) {
        if (DEBUG) {
            logger.log(2, str, str2, th);
        }
    }

    public static void debug(String str) {
        if (DEBUG) {
            logger.log(2, null, str, null);
        }
    }

    public static void debug(String str, String str2) {
        if (DEBUG) {
            logger.log(2, str, str2, null);
        }
    }

    public static void trace(String str, Throwable th) {
        if (TRACE) {
            logger.log(1, null, str, th);
        }
    }

    public static void trace(String str, String str2, Throwable th) {
        if (TRACE) {
            logger.log(1, str, str2, th);
        }
    }

    public static void trace(String str) {
        if (TRACE) {
            logger.log(1, null, str, null);
        }
    }

    public static void trace(String str, String str2) {
        if (TRACE) {
            logger.log(1, str, str2, null);
        }
    }

    private Log() {
    }

    public static class Logger {
        private long firstLogTime = new Date().getTime();

        public void log(int i, String str, String str2, Throwable th) {
            StringBuilder sb = new StringBuilder((int) Opcodes.ACC_NATIVE);
            long time = new Date().getTime() - this.firstLogTime;
            long j = time / 60000;
            long j2 = (time / 1000) % 60;
            if (j <= 9) {
                sb.append('0');
            }
            sb.append(j);
            sb.append(':');
            if (j2 <= 9) {
                sb.append('0');
            }
            sb.append(j2);
            switch (i) {
                case 1:
                    sb.append(" TRACE: ");
                    break;
                case 2:
                    sb.append(" DEBUG: ");
                    break;
                case 3:
                    sb.append("  INFO: ");
                    break;
                case 4:
                    sb.append("  WARN: ");
                    break;
                case 5:
                    sb.append(" ERROR: ");
                    break;
            }
            if (str != null) {
                sb.append('[');
                sb.append(str);
                sb.append("] ");
            }
            sb.append(str2);
            if (th != null) {
                StringWriter stringWriter = new StringWriter(Opcodes.ACC_NATIVE);
                th.printStackTrace(new PrintWriter(stringWriter));
                sb.append(10);
                sb.append(stringWriter.toString().trim());
            }
            print(sb.toString());
        }

        /* access modifiers changed from: protected */
        public void print(String str) {
            System.out.println(str);
        }
    }
}
