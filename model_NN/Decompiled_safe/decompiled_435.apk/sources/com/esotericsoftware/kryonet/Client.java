package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.serialize.IntSerializer;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.minlog.Log;
import com.mocelet.fourinrow.online.Network;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.security.AccessControlException;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import org.objectweb.asm.Opcodes;

public class Client extends Connection implements EndPoint {
    private InetAddress connectHost;
    private int connectTcpPort;
    private int connectTimeout;
    private int connectUdpPort;
    private final Kryo kryo;
    private Selector selector;
    private volatile boolean shutdown;
    private volatile boolean tcpRegistered;
    private Object tcpRegistrationLock;
    private volatile boolean udpRegistered;
    private Object udpRegistrationLock;
    private final Object updateLock;
    private Thread updateThread;

    static {
        try {
            System.setProperty("java.net.preferIPv6Addresses", "false");
        } catch (AccessControlException e) {
        }
    }

    public Client() {
        this(Opcodes.ACC_ANNOTATION, Opcodes.ACC_STRICT);
    }

    public Client(int i, int i2) {
        this(i, i2, new Kryo());
    }

    public Client(int i, int i2, Kryo kryo2) {
        this.tcpRegistrationLock = new Object();
        this.udpRegistrationLock = new Object();
        this.updateLock = new Object();
        this.endPoint = this;
        this.kryo = kryo2;
        kryo2.register(FrameworkMessage.RegisterTCP.class);
        kryo2.register(FrameworkMessage.RegisterUDP.class);
        kryo2.register(FrameworkMessage.KeepAlive.class);
        kryo2.register(FrameworkMessage.DiscoverHost.class);
        kryo2.register(FrameworkMessage.Ping.class);
        initialize(kryo2, i, i2);
        try {
            this.selector = Selector.open();
        } catch (IOException e) {
            throw new RuntimeException("Error opening selector.", e);
        }
    }

    public Kryo getKryo() {
        return this.kryo;
    }

    public void connect(int i, String str, int i2) throws IOException {
        connect(i, InetAddress.getByName(str), i2, -1);
    }

    public void connect(int i, String str, int i2, int i3) throws IOException {
        connect(i, InetAddress.getByName(str), i2, i3);
    }

    public void connect(int i, InetAddress inetAddress, int i2) throws IOException {
        connect(i, inetAddress, i2, -1);
    }

    public void connect(int i, InetAddress inetAddress, int i2, int i3) throws IOException {
        if (inetAddress == null) {
            throw new IllegalArgumentException("host cannot be null.");
        } else if (Thread.currentThread() == getUpdateThread()) {
            throw new IllegalStateException("Cannot connect on the connection's update thread.");
        } else {
            this.connectTimeout = i;
            this.connectHost = inetAddress;
            this.connectTcpPort = i2;
            this.connectUdpPort = i3;
            close();
            if (Log.INFO) {
                if (i3 != -1) {
                    Log.info("Connecting: " + inetAddress + ":" + i2 + "/" + i3);
                } else {
                    Log.info("Connecting: " + inetAddress + ":" + i2);
                }
            }
            this.id = -1;
            if (i3 != -1) {
                try {
                    this.udp = new UdpConnection(this.kryo, this.tcp.readBuffer.capacity());
                } catch (IOException e) {
                    close();
                    throw e;
                }
            }
            synchronized (this.updateLock) {
                this.tcpRegistered = false;
                this.selector.wakeup();
                long currentTimeMillis = System.currentTimeMillis() + ((long) i);
                this.tcp.connect(this.selector, new InetSocketAddress(inetAddress, i2), 5000);
            }
            synchronized (this.tcpRegistrationLock) {
                while (!this.tcpRegistered && System.currentTimeMillis() < currentTimeMillis) {
                    try {
                        this.tcpRegistrationLock.wait(100);
                    } catch (InterruptedException e2) {
                    }
                }
                if (!this.tcpRegistered) {
                    throw new SocketTimeoutException("Connected, but timed out during TCP registration.\nNote: Client#update must be called in a separate thread during connect.");
                }
            }
            if (i3 != -1) {
                InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, i3);
                synchronized (this.updateLock) {
                    this.udpRegistered = false;
                    this.selector.wakeup();
                    this.udp.connect(this.selector, inetSocketAddress);
                }
                synchronized (this.udpRegistrationLock) {
                    while (!this.udpRegistered && System.currentTimeMillis() < currentTimeMillis) {
                        FrameworkMessage.RegisterUDP registerUDP = new FrameworkMessage.RegisterUDP();
                        registerUDP.connectionID = this.id;
                        this.udp.send(this, registerUDP, inetSocketAddress);
                        try {
                            this.udpRegistrationLock.wait(100);
                        } catch (InterruptedException e3) {
                        }
                    }
                    if (!this.udpRegistered) {
                        throw new SocketTimeoutException("Connected, but timed out during UDP registration: " + inetAddress + ":" + i3);
                    }
                }
            }
        }
    }

    public void reconnect() throws IOException {
        reconnect(this.connectTimeout);
    }

    public void reconnect(int i) throws IOException {
        if (this.connectHost == null) {
            throw new IllegalStateException("This client has never been connected.");
        }
        connect(this.connectTimeout, this.connectHost, this.connectTcpPort, this.connectUdpPort);
    }

    public void update(int i) throws IOException {
        Object readObject;
        String simpleName;
        this.updateThread = Thread.currentThread();
        synchronized (this.updateLock) {
        }
        if (i > 0) {
            this.selector.select((long) i);
        } else {
            this.selector.selectNow();
        }
        Set<SelectionKey> selectedKeys = this.selector.selectedKeys();
        synchronized (selectedKeys) {
            Iterator<SelectionKey> it = selectedKeys.iterator();
            while (it.hasNext()) {
                SelectionKey next = it.next();
                it.remove();
                try {
                    int readyOps = next.readyOps();
                    if ((readyOps & 1) == 1) {
                        if (next.attachment() == this.tcp) {
                            while (true) {
                                Object readObject2 = this.tcp.readObject(this);
                                if (readObject2 == null) {
                                    break;
                                } else if (!this.tcpRegistered) {
                                    if (readObject2 instanceof FrameworkMessage.RegisterTCP) {
                                        this.id = ((FrameworkMessage.RegisterTCP) readObject2).connectionID;
                                        synchronized (this.tcpRegistrationLock) {
                                            this.tcpRegistered = true;
                                            this.tcpRegistrationLock.notifyAll();
                                        }
                                        if (Log.TRACE) {
                                            Log.trace("kryonet", this + " received TCP: RegisterTCP");
                                        }
                                        if (this.udp == null) {
                                            setConnected(true);
                                            notifyConnected();
                                        }
                                    } else {
                                        continue;
                                    }
                                } else if (this.udp == null || this.udpRegistered) {
                                    if (this.isConnected) {
                                        if (Log.DEBUG) {
                                            String simpleName2 = readObject2 == null ? "null" : readObject2.getClass().getSimpleName();
                                            if (!(readObject2 instanceof FrameworkMessage)) {
                                                Log.debug("kryonet", this + " received TCP: " + simpleName2);
                                            } else if (Log.TRACE) {
                                                Log.trace("kryonet", this + " received TCP: " + simpleName2);
                                            }
                                        }
                                        notifyReceived(readObject2);
                                    }
                                } else if (readObject2 instanceof FrameworkMessage.RegisterUDP) {
                                    synchronized (this.udpRegistrationLock) {
                                        this.udpRegistered = true;
                                        this.udpRegistrationLock.notifyAll();
                                    }
                                    if (Log.TRACE) {
                                        Log.trace("kryonet", this + " received UDP: RegisterUDP");
                                    }
                                    if (Log.DEBUG) {
                                        Log.debug("kryonet", "Port " + this.udp.datagramChannel.socket().getLocalPort() + "/UDP connected to: " + this.udp.connectedAddress);
                                    }
                                    setConnected(true);
                                    notifyConnected();
                                } else {
                                    continue;
                                }
                            }
                            while (true) {
                            }
                        } else if (!(this.udp.readFromAddress() == null || (readObject = this.udp.readObject(this)) == null)) {
                            if (Log.DEBUG) {
                                if (readObject == null) {
                                    simpleName = "null";
                                } else {
                                    simpleName = readObject.getClass().getSimpleName();
                                }
                                Log.debug("kryonet", this + " received UDP: " + simpleName);
                            }
                            notifyReceived(readObject);
                        }
                    }
                    if ((readyOps & 4) == 4) {
                        this.tcp.writeOperation();
                    }
                } catch (CancelledKeyException e) {
                }
            }
        }
        if (this.isConnected) {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.tcp.isTimedOut(currentTimeMillis)) {
                if (Log.DEBUG) {
                    Log.debug("kryonet", this + " timed out.");
                }
                close();
                return;
            }
            if (this.tcp.needsKeepAlive(currentTimeMillis)) {
                sendTCP(FrameworkMessage.keepAlive);
            }
            if (this.udp != null && this.udpRegistered && this.udp.needsKeepAlive(currentTimeMillis)) {
                sendUDP(FrameworkMessage.keepAlive);
            }
        }
    }

    public void run() {
        if (Log.TRACE) {
            Log.trace("kryonet", "Client thread started.");
        }
        this.shutdown = false;
        while (!this.shutdown) {
            try {
                update(Network.SERVER_ERROR_RESPONSE);
            } catch (IOException e) {
                if (Log.TRACE) {
                    if (this.isConnected) {
                        Log.trace("kryonet", "Unable to update connection: " + this, e);
                    } else {
                        Log.trace("kryonet", "Unable to update connection.", e);
                    }
                } else if (Log.DEBUG) {
                    if (this.isConnected) {
                        Log.debug("kryonet", this + " update: " + e.getMessage());
                    } else {
                        Log.debug("kryonet", "Unable to update connection: " + e.getMessage());
                    }
                }
                close();
            } catch (SerializationException e2) {
                if (Log.ERROR) {
                    if (this.isConnected) {
                        Log.error("kryonet", "Error updating connection: " + this, e2);
                    } else {
                        Log.error("kryonet", "Error updating connection.", e2);
                    }
                }
                close();
                throw e2;
            }
        }
        if (Log.TRACE) {
            Log.trace("kryonet", "Client thread stopped.");
        }
    }

    public void start() {
        new Thread(this, "Client").start();
    }

    public void stop() {
        if (!this.shutdown) {
            close();
            if (Log.TRACE) {
                Log.trace("kryonet", "Client thread stopping.");
            }
            this.shutdown = true;
            this.selector.wakeup();
        }
    }

    public void close() {
        super.close();
        synchronized (this.updateLock) {
            this.selector.wakeup();
            try {
                this.selector.selectNow();
            } catch (IOException e) {
            }
        }
    }

    public void addListener(Listener listener) {
        super.addListener(listener);
        if (Log.TRACE) {
            Log.trace("kryonet", "Client listener added.");
        }
    }

    public void removeListener(Listener listener) {
        super.removeListener(listener);
        if (Log.TRACE) {
            Log.trace("kryonet", "Client listener removed.");
        }
    }

    public void setKeepAliveUDP(int i) {
        if (this.udp == null) {
            throw new IllegalStateException("Not connected via UDP.");
        }
        this.udp.keepAliveMillis = i;
    }

    public Thread getUpdateThread() {
        return this.updateThread;
    }

    private void broadcast(int i, DatagramSocket datagramSocket) throws IOException {
        int id = this.kryo.getRegisteredClass(FrameworkMessage.DiscoverHost.class).getID();
        ByteBuffer allocate = ByteBuffer.allocate(4);
        IntSerializer.put(allocate, id, true);
        allocate.flip();
        byte[] bArr = new byte[allocate.limit()];
        allocate.get(bArr);
        Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
        while (it.hasNext()) {
            Iterator it2 = Collections.list(((NetworkInterface) it.next()).getInetAddresses()).iterator();
            while (it2.hasNext()) {
                InetAddress inetAddress = (InetAddress) it2.next();
                if (inetAddress.isSiteLocalAddress()) {
                    byte[] address = inetAddress.getAddress();
                    address[3] = -1;
                    datagramSocket.send(new DatagramPacket(bArr, bArr.length, InetAddress.getByAddress(address), i));
                    address[2] = -1;
                    datagramSocket.send(new DatagramPacket(bArr, bArr.length, InetAddress.getByAddress(address), i));
                }
            }
        }
        if (Log.DEBUG) {
            Log.debug("kryonet", "Broadcasted host discovery on port: " + i);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x005c A[Catch:{ all -> 0x0077 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.net.InetAddress discoverHost(int r8, int r9) {
        /*
            r7 = this;
            r5 = 0
            java.net.DatagramSocket r0 = new java.net.DatagramSocket     // Catch:{ IOException -> 0x0056, all -> 0x006a }
            r0.<init>()     // Catch:{ IOException -> 0x0056, all -> 0x006a }
            r7.broadcast(r8, r0)     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            r0.setSoTimeout(r9)     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            java.net.DatagramPacket r1 = new java.net.DatagramPacket     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            r2 = 0
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            r3 = 0
            r1.<init>(r2, r3)     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            r0.receive(r1)     // Catch:{ SocketTimeoutException -> 0x0043 }
            boolean r2 = com.esotericsoftware.minlog.Log.INFO     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            if (r2 == 0) goto L_0x0038
            java.lang.String r2 = "kryonet"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            r3.<init>()     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            java.lang.String r4 = "Discovered server: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            java.net.InetAddress r4 = r1.getAddress()     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            com.esotericsoftware.minlog.Log.info(r2, r3)     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
        L_0x0038:
            java.net.InetAddress r1 = r1.getAddress()     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            if (r0 == 0) goto L_0x0041
            r0.close()
        L_0x0041:
            r0 = r1
        L_0x0042:
            return r0
        L_0x0043:
            r1 = move-exception
            boolean r1 = com.esotericsoftware.minlog.Log.INFO     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
            if (r1 == 0) goto L_0x004f
            java.lang.String r1 = "kryonet"
            java.lang.String r2 = "Host discovery timed out."
            com.esotericsoftware.minlog.Log.info(r1, r2)     // Catch:{ IOException -> 0x0079, all -> 0x0072 }
        L_0x004f:
            if (r0 == 0) goto L_0x0054
            r0.close()
        L_0x0054:
            r0 = r5
            goto L_0x0042
        L_0x0056:
            r0 = move-exception
            r1 = r5
        L_0x0058:
            boolean r2 = com.esotericsoftware.minlog.Log.ERROR     // Catch:{ all -> 0x0077 }
            if (r2 == 0) goto L_0x0063
            java.lang.String r2 = "kryonet"
            java.lang.String r3 = "Host discovery failed."
            com.esotericsoftware.minlog.Log.error(r2, r3, r0)     // Catch:{ all -> 0x0077 }
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()
        L_0x0068:
            r0 = r5
            goto L_0x0042
        L_0x006a:
            r0 = move-exception
            r1 = r5
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()
        L_0x0071:
            throw r0
        L_0x0072:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x006c
        L_0x0077:
            r0 = move-exception
            goto L_0x006c
        L_0x0079:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Client.discoverHost(int, int):java.net.InetAddress");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004a A[Catch:{ all -> 0x0070 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.net.InetAddress> discoverHosts(int r8, int r9) {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            java.net.DatagramSocket r2 = new java.net.DatagramSocket     // Catch:{ IOException -> 0x0073, all -> 0x0069 }
            r2.<init>()     // Catch:{ IOException -> 0x0073, all -> 0x0069 }
            r7.broadcast(r8, r2)     // Catch:{ IOException -> 0x0045 }
            r2.setSoTimeout(r9)     // Catch:{ IOException -> 0x0045 }
        L_0x0011:
            java.net.DatagramPacket r1 = new java.net.DatagramPacket     // Catch:{ IOException -> 0x0045 }
            r3 = 0
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0045 }
            r4 = 0
            r1.<init>(r3, r4)     // Catch:{ IOException -> 0x0045 }
            r2.receive(r1)     // Catch:{ SocketTimeoutException -> 0x0057 }
            boolean r3 = com.esotericsoftware.minlog.Log.INFO     // Catch:{ IOException -> 0x0045 }
            if (r3 == 0) goto L_0x003d
            java.lang.String r3 = "kryonet"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0045 }
            r4.<init>()     // Catch:{ IOException -> 0x0045 }
            java.lang.String r5 = "Discovered server: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0045 }
            java.net.InetAddress r5 = r1.getAddress()     // Catch:{ IOException -> 0x0045 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0045 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0045 }
            com.esotericsoftware.minlog.Log.info(r3, r4)     // Catch:{ IOException -> 0x0045 }
        L_0x003d:
            java.net.InetAddress r1 = r1.getAddress()     // Catch:{ IOException -> 0x0045 }
            r0.add(r1)     // Catch:{ IOException -> 0x0045 }
            goto L_0x0011
        L_0x0045:
            r1 = move-exception
        L_0x0046:
            boolean r3 = com.esotericsoftware.minlog.Log.ERROR     // Catch:{ all -> 0x0070 }
            if (r3 == 0) goto L_0x0051
            java.lang.String r3 = "kryonet"
            java.lang.String r4 = "Host discovery failed."
            com.esotericsoftware.minlog.Log.error(r3, r4, r1)     // Catch:{ all -> 0x0070 }
        L_0x0051:
            if (r2 == 0) goto L_0x0056
            r2.close()
        L_0x0056:
            return r0
        L_0x0057:
            r1 = move-exception
            boolean r1 = com.esotericsoftware.minlog.Log.INFO     // Catch:{ IOException -> 0x0045 }
            if (r1 == 0) goto L_0x0063
            java.lang.String r1 = "kryonet"
            java.lang.String r3 = "Host discovery timed out."
            com.esotericsoftware.minlog.Log.info(r1, r3)     // Catch:{ IOException -> 0x0045 }
        L_0x0063:
            if (r2 == 0) goto L_0x0056
            r2.close()
            goto L_0x0056
        L_0x0069:
            r0 = move-exception
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()
        L_0x006f:
            throw r0
        L_0x0070:
            r0 = move-exception
            r1 = r2
            goto L_0x006a
        L_0x0073:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Client.discoverHosts(int, int):java.util.List");
    }
}
