package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.util.IntHashMap;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.minlog.Log;
import com.mocelet.fourinrow.online.Network;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import org.objectweb.asm.Opcodes;

public class Server implements EndPoint {
    private Connection[] connections;
    private Listener dispatchListener;
    private ByteBuffer emptyBuffer;
    private final Kryo kryo;
    private Object listenerLock;
    Listener[] listeners;
    private int nextConnectionID;
    private final int objectBufferSize;
    private IntHashMap<Connection> pendingConnections;
    private final Selector selector;
    private ServerSocketChannel serverChannel;
    private volatile boolean shutdown;
    private UdpConnection udp;
    private Object updateLock;
    private Thread updateThread;
    private final int writeBufferSize;

    public Server() {
        this(Opcodes.ACC_ENUM, Opcodes.ACC_STRICT);
    }

    public Server(int i, int i2) {
        this(i, i2, new Kryo());
    }

    public Server(int i, int i2, Kryo kryo2) {
        this.connections = new Connection[0];
        this.pendingConnections = new IntHashMap<>();
        this.listeners = new Listener[0];
        this.listenerLock = new Object();
        this.nextConnectionID = 1;
        this.updateLock = new Object();
        this.emptyBuffer = ByteBuffer.allocate(0);
        this.dispatchListener = new Listener() {
            public void connected(Connection connection) {
                for (Listener connected : Server.this.listeners) {
                    connected.connected(connection);
                }
            }

            public void disconnected(Connection connection) {
                Server.this.removeConnection(connection);
                for (Listener disconnected : Server.this.listeners) {
                    disconnected.disconnected(connection);
                }
            }

            public void received(Connection connection, Object obj) {
                for (Listener received : Server.this.listeners) {
                    received.received(connection, obj);
                }
            }
        };
        this.writeBufferSize = i;
        this.objectBufferSize = i2;
        this.kryo = kryo2;
        kryo2.register(FrameworkMessage.RegisterTCP.class);
        kryo2.register(FrameworkMessage.RegisterUDP.class);
        kryo2.register(FrameworkMessage.KeepAlive.class);
        kryo2.register(FrameworkMessage.DiscoverHost.class);
        kryo2.register(FrameworkMessage.Ping.class);
        try {
            this.selector = Selector.open();
        } catch (IOException e) {
            throw new RuntimeException("Error opening selector.", e);
        }
    }

    public Kryo getKryo() {
        return this.kryo;
    }

    public void bind(int i) throws IOException {
        bind(i, -1);
    }

    public void bind(int i, int i2) throws IOException {
        close();
        synchronized (this.updateLock) {
            this.selector.wakeup();
            try {
                this.serverChannel = this.selector.provider().openServerSocketChannel();
                this.serverChannel.socket().bind(new InetSocketAddress(i));
                this.serverChannel.configureBlocking(false);
                this.serverChannel.register(this.selector, 16);
                if (Log.DEBUG) {
                    Log.debug("kryonet", "Accepting connections on port: " + i + "/TCP");
                }
                if (i2 != -1) {
                    this.udp = new UdpConnection(this.kryo, this.objectBufferSize);
                    this.udp.bind(this.selector, i2);
                    if (Log.DEBUG) {
                        Log.debug("kryonet", "Accepting connections on port: " + i2 + "/UDP");
                    }
                }
            } catch (IOException e) {
                close();
                throw e;
            }
        }
        if (Log.INFO) {
            Log.info("kryonet", "Server opened.");
        }
    }

    public void update(int i) throws IOException {
        Connection connection;
        this.updateThread = Thread.currentThread();
        synchronized (this.updateLock) {
        }
        if (i > 0) {
            this.selector.select((long) i);
        } else {
            this.selector.selectNow();
        }
        Set<SelectionKey> selectedKeys = this.selector.selectedKeys();
        synchronized (selectedKeys) {
            UdpConnection udpConnection = this.udp;
            Iterator<SelectionKey> it = selectedKeys.iterator();
            while (it.hasNext()) {
                SelectionKey next = it.next();
                it.remove();
                try {
                    int readyOps = next.readyOps();
                    Connection connection2 = (Connection) next.attachment();
                    if (connection2 != null) {
                        if (udpConnection == null || connection2.udpRemoteAddress != null) {
                            if ((readyOps & 1) == 1) {
                                while (true) {
                                    try {
                                        Object readObject = connection2.tcp.readObject(connection2);
                                        if (readObject == null) {
                                            break;
                                        }
                                        if (Log.DEBUG) {
                                            String simpleName = readObject == null ? "null" : readObject.getClass().getSimpleName();
                                            if (!(readObject instanceof FrameworkMessage)) {
                                                Log.debug("kryonet", connection2 + " received TCP: " + simpleName);
                                            } else if (Log.TRACE) {
                                                Log.trace("kryonet", connection2 + " received TCP: " + simpleName);
                                            }
                                        }
                                        connection2.notifyReceived(readObject);
                                    } catch (IOException e) {
                                        if (Log.TRACE) {
                                            Log.trace("kryonet", "Unable to read TCP from: " + connection2, e);
                                        } else if (Log.DEBUG) {
                                            Log.debug("kryonet", connection2 + " update: " + e.getMessage());
                                        }
                                        connection2.close();
                                    } catch (SerializationException e2) {
                                        if (Log.ERROR) {
                                            Log.error("kryonet", "Error reading TCP from connection: " + connection2, e2);
                                        }
                                        connection2.close();
                                    }
                                }
                            }
                            if ((readyOps & 4) == 4) {
                                try {
                                    connection2.tcp.writeOperation();
                                } catch (IOException e3) {
                                    if (Log.TRACE) {
                                        Log.trace("kryonet", "Unable to write TCP to connection: " + connection2, e3);
                                    } else if (Log.DEBUG) {
                                        Log.debug("kryonet", connection2 + " update: " + e3.getMessage());
                                    }
                                    connection2.close();
                                }
                            }
                        }
                    } else if ((readyOps & 16) == 16) {
                        ServerSocketChannel serverSocketChannel = this.serverChannel;
                        if (serverSocketChannel != null) {
                            try {
                                SocketChannel accept = serverSocketChannel.accept();
                                if (accept != null) {
                                    acceptOperation(accept);
                                }
                            } catch (IOException e4) {
                                if (Log.DEBUG) {
                                    Log.debug("kryonet", "Unable to accept new connection.", e4);
                                }
                            }
                        }
                    } else if (udpConnection != null) {
                        try {
                            InetSocketAddress readFromAddress = udpConnection.readFromAddress();
                            if (readFromAddress != null) {
                                Connection[] connectionArr = this.connections;
                                int length = connectionArr.length;
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= length) {
                                        connection = connection2;
                                        break;
                                    }
                                    Connection connection3 = connectionArr[i2];
                                    if (readFromAddress.equals(connection3.udpRemoteAddress)) {
                                        connection = connection3;
                                        break;
                                    }
                                    i2++;
                                }
                                try {
                                    Object readObject2 = udpConnection.readObject(connection);
                                    if (readObject2 instanceof FrameworkMessage) {
                                        if (readObject2 instanceof FrameworkMessage.RegisterUDP) {
                                            int i3 = ((FrameworkMessage.RegisterUDP) readObject2).connectionID;
                                            Connection remove = this.pendingConnections.remove(i3);
                                            if (remove != null) {
                                                if (remove.udpRemoteAddress == null) {
                                                    remove.udpRemoteAddress = readFromAddress;
                                                    addConnection(remove);
                                                    remove.sendTCP(new FrameworkMessage.RegisterUDP());
                                                    if (Log.DEBUG) {
                                                        Log.debug("kryonet", "Port " + udpConnection.datagramChannel.socket().getLocalPort() + "/UDP connected to: " + readFromAddress);
                                                    }
                                                    remove.notifyConnected();
                                                }
                                            } else if (Log.DEBUG) {
                                                Log.debug("kryonet", "Ignoring incoming RegisterUDP with invalid connection ID: " + i3);
                                            }
                                        } else if (readObject2 instanceof FrameworkMessage.DiscoverHost) {
                                            try {
                                                udpConnection.datagramChannel.send(this.emptyBuffer, readFromAddress);
                                                if (Log.DEBUG) {
                                                    Log.debug("kryonet", "Responded to host discovery from: " + readFromAddress);
                                                }
                                            } catch (IOException e5) {
                                                if (Log.WARN) {
                                                    Log.warn("kryonet", "Error replying to host discovery from: " + readFromAddress, e5);
                                                }
                                            }
                                        }
                                    }
                                    if (connection != null) {
                                        if (Log.DEBUG) {
                                            String simpleName2 = readObject2 == null ? "null" : readObject2.getClass().getSimpleName();
                                            if (!(readObject2 instanceof FrameworkMessage)) {
                                                Log.debug("kryonet", connection + " received UDP: " + simpleName2);
                                            } else if (Log.TRACE) {
                                                Log.trace("kryonet", connection + " received UDP: " + simpleName2);
                                            }
                                        }
                                        connection.notifyReceived(readObject2);
                                    } else if (Log.DEBUG) {
                                        Log.debug("kryonet", "Ignoring UDP from unregistered address: " + readFromAddress);
                                    }
                                } catch (SerializationException e6) {
                                    if (Log.WARN) {
                                        if (connection == null) {
                                            Log.warn("kryonet", "Error reading UDP from unregistered address: " + readFromAddress, e6);
                                        } else if (Log.ERROR) {
                                            Log.error("kryonet", "Error reading UDP from connection: " + connection, e6);
                                        }
                                    }
                                }
                            }
                        } catch (IOException e7) {
                            if (Log.WARN) {
                                Log.warn("kryonet", "Error reading UDP data.", e7);
                            }
                        }
                    }
                } catch (CancelledKeyException e8) {
                }
            }
        }
        long currentTimeMillis = System.currentTimeMillis();
        for (Connection connection4 : this.connections) {
            if (connection4.tcp.isTimedOut(currentTimeMillis)) {
                if (Log.DEBUG) {
                    Log.debug("kryonet", connection4 + " timed out.");
                }
                connection4.close();
            } else if (connection4.tcp.needsKeepAlive(currentTimeMillis)) {
                connection4.sendTCP(FrameworkMessage.keepAlive);
            }
        }
    }

    public void run() {
        if (Log.TRACE) {
            Log.trace("kryonet", "Server thread started.");
        }
        this.shutdown = false;
        while (!this.shutdown) {
            try {
                update(Network.SERVER_ERROR_RESPONSE);
            } catch (IOException e) {
                if (Log.ERROR) {
                    Log.error("kryonet", "Error updating server connections.", e);
                }
                close();
            }
        }
        if (Log.TRACE) {
            Log.trace("kryonet", "Server thread stopped.");
        }
    }

    public void start() {
        new Thread(this, "Server").start();
    }

    public void stop() {
        if (!this.shutdown) {
            close();
            if (Log.TRACE) {
                Log.trace("kryonet", "Server thread stopping.");
            }
            this.shutdown = true;
        }
    }

    private void acceptOperation(SocketChannel socketChannel) {
        Connection newConnection = newConnection();
        newConnection.initialize(this.kryo, this.writeBufferSize, this.objectBufferSize);
        newConnection.endPoint = this;
        UdpConnection udpConnection = this.udp;
        if (udpConnection != null) {
            newConnection.udp = udpConnection;
        }
        try {
            newConnection.tcp.accept(this.selector, socketChannel).attach(newConnection);
            int i = this.nextConnectionID;
            this.nextConnectionID = i + 1;
            if (this.nextConnectionID == -1) {
                this.nextConnectionID = 1;
            }
            newConnection.id = i;
            newConnection.setConnected(true);
            newConnection.addListener(this.dispatchListener);
            if (udpConnection == null) {
                addConnection(newConnection);
            } else {
                this.pendingConnections.put(i, newConnection);
            }
            FrameworkMessage.RegisterTCP registerTCP = new FrameworkMessage.RegisterTCP();
            registerTCP.connectionID = i;
            newConnection.sendTCP(registerTCP);
            if (udpConnection == null) {
                newConnection.notifyConnected();
            }
        } catch (IOException e) {
            newConnection.close();
            if (Log.DEBUG) {
                Log.debug("kryonet", "Unable to accept TCP connection.", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Connection newConnection() {
        return new Connection();
    }

    private void addConnection(Connection connection) {
        Connection[] connectionArr = new Connection[(this.connections.length + 1)];
        connectionArr[0] = connection;
        System.arraycopy(this.connections, 0, connectionArr, 1, this.connections.length);
        this.connections = connectionArr;
    }

    /* access modifiers changed from: package-private */
    public void removeConnection(Connection connection) {
        ArrayList arrayList = new ArrayList(Arrays.asList(this.connections));
        arrayList.remove(connection);
        this.connections = (Connection[]) arrayList.toArray(new Connection[arrayList.size()]);
        this.pendingConnections.remove(connection.id);
    }

    public void sendToAllTCP(Object obj) {
        for (Connection sendTCP : this.connections) {
            sendTCP.sendTCP(obj);
        }
    }

    public void sendToAllExceptTCP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id != i) {
                connection.sendTCP(obj);
            }
        }
    }

    public void sendToTCP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id == i) {
                connection.sendTCP(obj);
                return;
            }
        }
    }

    public void sendToAllUDP(Object obj) {
        for (Connection sendUDP : this.connections) {
            sendUDP.sendUDP(obj);
        }
    }

    public void sendToAllExceptUDP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id != i) {
                connection.sendUDP(obj);
            }
        }
    }

    public void sendToUDP(int i, Object obj) {
        for (Connection connection : this.connections) {
            if (connection.id == i) {
                connection.sendUDP(obj);
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002d, code lost:
        if (com.esotericsoftware.minlog.Log.TRACE == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        com.esotericsoftware.minlog.Log.trace("kryonet", "Server listener added: " + r7.getClass().getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addListener(com.esotericsoftware.kryonet.Listener r7) {
        /*
            r6 = this;
            r3 = 0
            if (r7 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "listener cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.lang.Object r0 = r6.listenerLock
            monitor-enter(r0)
            com.esotericsoftware.kryonet.Listener[] r1 = r6.listeners     // Catch:{ all -> 0x0050 }
            int r2 = r1.length     // Catch:{ all -> 0x0050 }
        L_0x0011:
            if (r3 >= r2) goto L_0x001c
            r4 = r1[r3]     // Catch:{ all -> 0x0050 }
            if (r7 != r4) goto L_0x0019
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
        L_0x0018:
            return
        L_0x0019:
            int r3 = r3 + 1
            goto L_0x0011
        L_0x001c:
            int r3 = r2 + 1
            com.esotericsoftware.kryonet.Listener[] r3 = new com.esotericsoftware.kryonet.Listener[r3]     // Catch:{ all -> 0x0050 }
            r4 = 0
            r3[r4] = r7     // Catch:{ all -> 0x0050 }
            r4 = 0
            r5 = 1
            java.lang.System.arraycopy(r1, r4, r3, r5, r2)     // Catch:{ all -> 0x0050 }
            r6.listeners = r3     // Catch:{ all -> 0x0050 }
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            boolean r0 = com.esotericsoftware.minlog.Log.TRACE
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = "kryonet"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Server listener added: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r7.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.esotericsoftware.minlog.Log.trace(r0, r1)
            goto L_0x0018
        L_0x0050:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Server.addListener(com.esotericsoftware.kryonet.Listener):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
        if (com.esotericsoftware.minlog.Log.TRACE == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0033, code lost:
        com.esotericsoftware.minlog.Log.trace("kryonet", "Server listener removed: " + r10.getClass().getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeListener(com.esotericsoftware.kryonet.Listener r10) {
        /*
            r9 = this;
            r8 = 1
            r4 = 0
            if (r10 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "listener cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            java.lang.Object r0 = r9.listenerLock
            monitor-enter(r0)
            com.esotericsoftware.kryonet.Listener[] r1 = r9.listeners     // Catch:{ all -> 0x0054 }
            int r2 = r1.length     // Catch:{ all -> 0x0054 }
            int r3 = r2 - r8
            com.esotericsoftware.kryonet.Listener[] r3 = new com.esotericsoftware.kryonet.Listener[r3]     // Catch:{ all -> 0x0054 }
            r5 = r4
        L_0x0017:
            if (r5 >= r2) goto L_0x002c
            r6 = r1[r5]     // Catch:{ all -> 0x0054 }
            if (r10 != r6) goto L_0x0020
        L_0x001d:
            int r5 = r5 + 1
            goto L_0x0017
        L_0x0020:
            int r7 = r2 - r8
            if (r4 != r7) goto L_0x0026
            monitor-exit(r0)     // Catch:{ all -> 0x0054 }
        L_0x0025:
            return
        L_0x0026:
            int r7 = r4 + 1
            r3[r4] = r6     // Catch:{ all -> 0x0054 }
            r4 = r7
            goto L_0x001d
        L_0x002c:
            r9.listeners = r3     // Catch:{ all -> 0x0054 }
            monitor-exit(r0)     // Catch:{ all -> 0x0054 }
            boolean r0 = com.esotericsoftware.minlog.Log.TRACE
            if (r0 == 0) goto L_0x0025
            java.lang.String r0 = "kryonet"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Server listener removed: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r10.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.esotericsoftware.minlog.Log.trace(r0, r1)
            goto L_0x0025
        L_0x0054:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0054 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Server.removeListener(com.esotericsoftware.kryonet.Listener):void");
    }

    public void close() {
        Connection[] connectionArr = this.connections;
        if (Log.INFO && connectionArr.length > 0) {
            Log.info("kryonet", "Closing server connections...");
        }
        for (Connection close : connectionArr) {
            close.close();
        }
        Connection[] connectionArr2 = new Connection[0];
        ServerSocketChannel serverSocketChannel = this.serverChannel;
        if (serverSocketChannel != null) {
            try {
                serverSocketChannel.close();
                if (Log.INFO) {
                    Log.info("kryonet", "Server closed.");
                }
            } catch (IOException e) {
                if (Log.DEBUG) {
                    Log.debug("kryonet", "Unable to close server.", e);
                }
            }
            this.serverChannel = null;
        }
        UdpConnection udpConnection = this.udp;
        if (udpConnection != null) {
            udpConnection.close();
            this.udp = null;
        }
        synchronized (this.updateLock) {
            this.selector.wakeup();
            try {
                this.selector.selectNow();
            } catch (IOException e2) {
            }
        }
    }

    public Thread getUpdateThread() {
        return this.updateThread;
    }

    public Connection[] getConnections() {
        return this.connections;
    }
}
