package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.serialize.IntSerializer;
import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

class TcpConnection {
    private static final int IPTOS_LOWDELAY = 16;
    boolean bufferPositionFix;
    private int currentObjectLength;
    int keepAliveMillis = 8000;
    private final Kryo kryo;
    private long lastReadTime;
    private long lastWriteTime;
    final ByteBuffer readBuffer;
    private SelectionKey selectionKey;
    SocketChannel socketChannel;
    final ByteBuffer tempWriteBuffer;
    int timeoutMillis = 12000;
    final ByteBuffer writeBuffer;
    private final Object writeLock = new Object();

    public TcpConnection(Kryo kryo2, int i, int i2) {
        this.kryo = kryo2;
        this.writeBuffer = ByteBuffer.allocate(i);
        this.tempWriteBuffer = ByteBuffer.allocate(i2);
        this.readBuffer = ByteBuffer.allocate(i2);
        this.readBuffer.flip();
    }

    public SelectionKey accept(Selector selector, SocketChannel socketChannel2) throws IOException {
        try {
            this.socketChannel = socketChannel2;
            socketChannel2.configureBlocking(false);
            socketChannel2.socket().setTcpNoDelay(true);
            this.selectionKey = socketChannel2.register(selector, 1);
            if (Log.DEBUG) {
                Log.debug("kryonet", "Port " + socketChannel2.socket().getLocalPort() + "/TCP connected to: " + socketChannel2.socket().getRemoteSocketAddress());
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.lastWriteTime = currentTimeMillis;
            this.lastReadTime = currentTimeMillis;
            return this.selectionKey;
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    public void connect(Selector selector, SocketAddress socketAddress, int i) throws IOException {
        close();
        this.writeBuffer.clear();
        this.readBuffer.clear();
        this.readBuffer.flip();
        try {
            SocketChannel openSocketChannel = selector.provider().openSocketChannel();
            Socket socket = openSocketChannel.socket();
            socket.setTcpNoDelay(true);
            socket.setTrafficClass(16);
            socket.connect(socketAddress, i);
            openSocketChannel.configureBlocking(false);
            this.socketChannel = openSocketChannel;
            this.selectionKey = openSocketChannel.register(selector, 1);
            this.selectionKey.attach(this);
            if (Log.DEBUG) {
                Log.debug("kryonet", "Port " + openSocketChannel.socket().getLocalPort() + "/TCP connected to: " + openSocketChannel.socket().getRemoteSocketAddress());
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.lastWriteTime = currentTimeMillis;
            this.lastReadTime = currentTimeMillis;
        } catch (IOException e) {
            close();
            IOException iOException = new IOException("Unable to connect to: " + socketAddress);
            iOException.initCause(e);
            throw iOException;
        }
    }

    public Object readObject(Connection connection) throws IOException {
        SocketChannel socketChannel2 = this.socketChannel;
        if (socketChannel2 == null) {
            throw new SocketException("Connection is closed.");
        }
        if (this.currentObjectLength == 0) {
            if (!IntSerializer.canRead(this.readBuffer, true)) {
                this.readBuffer.compact();
                int read = socketChannel2.read(this.readBuffer);
                this.readBuffer.flip();
                if (read == -1) {
                    throw new SocketException("Connection is closed.");
                }
                this.lastReadTime = System.currentTimeMillis();
                if (!IntSerializer.canRead(this.readBuffer, true)) {
                    return null;
                }
            }
            this.currentObjectLength = IntSerializer.get(this.readBuffer, true);
            if (this.currentObjectLength <= 0) {
                throw new SerializationException("Invalid object length: " + this.currentObjectLength);
            } else if (this.currentObjectLength > this.readBuffer.capacity()) {
                throw new SerializationException("Unable to read object larger than read buffer: " + this.currentObjectLength);
            }
        }
        int i = this.currentObjectLength;
        if (this.readBuffer.remaining() < i) {
            this.readBuffer.compact();
            int read2 = socketChannel2.read(this.readBuffer);
            this.readBuffer.flip();
            if (read2 == -1) {
                throw new SocketException("Connection is closed.");
            }
            this.lastReadTime = System.currentTimeMillis();
            if (this.readBuffer.remaining() < i) {
                return null;
            }
        }
        this.currentObjectLength = 0;
        int position = this.readBuffer.position();
        int limit = this.readBuffer.limit();
        this.readBuffer.limit(position + i);
        Context context = Kryo.getContext();
        context.put("connection", connection);
        context.setRemoteEntityID(connection.id);
        Object readClassAndObject = this.kryo.readClassAndObject(this.readBuffer);
        this.readBuffer.limit(limit);
        if (this.readBuffer.position() - position == i) {
            return readClassAndObject;
        }
        throw new SerializationException("Incorrect number of bytes (" + ((position + i) - this.readBuffer.position()) + " remaining) used to deserialize object: " + readClassAndObject);
    }

    public void writeOperation() throws IOException {
        synchronized (this.writeLock) {
            this.writeBuffer.flip();
            if (writeToSocket(this.writeBuffer)) {
                this.selectionKey.interestOps(1);
            }
            this.writeBuffer.compact();
        }
    }

    private boolean writeToSocket(ByteBuffer byteBuffer) throws IOException {
        SocketChannel socketChannel2 = this.socketChannel;
        if (socketChannel2 == null) {
            throw new SocketException("Connection is closed.");
        }
        while (byteBuffer.hasRemaining()) {
            if (this.bufferPositionFix) {
                byteBuffer.compact();
                byteBuffer.flip();
            }
            if (socketChannel2.write(byteBuffer) == 0) {
                break;
            }
        }
        this.lastWriteTime = System.currentTimeMillis();
        return !byteBuffer.hasRemaining();
    }

    public int send(Connection connection, Object obj) throws IOException {
        int limit;
        if (this.socketChannel == null) {
            throw new SocketException("Connection is closed.");
        }
        synchronized (this.writeLock) {
            this.tempWriteBuffer.clear();
            this.tempWriteBuffer.position(5);
            Context context = Kryo.getContext();
            context.put("connection", connection);
            context.setRemoteEntityID(connection.id);
            try {
                this.kryo.writeClassAndObject(this.tempWriteBuffer, obj);
                this.tempWriteBuffer.flip();
                int limit2 = this.tempWriteBuffer.limit() - 5;
                int length = 5 - IntSerializer.length(limit2, true);
                this.tempWriteBuffer.position(length);
                IntSerializer.put(this.tempWriteBuffer, limit2, true);
                this.tempWriteBuffer.position(length);
                if (this.writeBuffer.position() > 0) {
                    this.writeBuffer.put(this.tempWriteBuffer);
                } else if (!writeToSocket(this.tempWriteBuffer)) {
                    this.writeBuffer.put(this.tempWriteBuffer);
                    this.selectionKey.interestOps(5);
                }
                if (Log.DEBUG || Log.TRACE) {
                    float position = ((float) this.writeBuffer.position()) / ((float) this.writeBuffer.capacity());
                    if (Log.DEBUG && position > 0.75f) {
                        Log.debug("kryonet", connection + " TCP write buffer is approaching capacity: " + position + "%");
                    } else if (Log.TRACE && position > 0.25f) {
                        Log.trace("kryonet", connection + " TCP write buffer utilization: " + position + "%");
                    }
                }
                this.lastWriteTime = System.currentTimeMillis();
                limit = this.tempWriteBuffer.limit() - length;
            } catch (BufferOverflowException e) {
                throw new SerializationException("Write buffer limit exceeded writing object of type: " + obj.getClass().getName(), e);
            } catch (SerializationException e2) {
                throw new SerializationException("Unable to serialize object of type: " + obj.getClass().getName(), e2);
            }
        }
        return limit;
    }

    public void close() {
        try {
            if (this.socketChannel != null) {
                this.socketChannel.close();
                this.socketChannel = null;
                if (this.selectionKey != null) {
                    this.selectionKey.selector().wakeup();
                }
            }
        } catch (IOException e) {
            if (Log.DEBUG) {
                Log.debug("kryonet", "Unable to close TCP connection.", e);
            }
        }
    }

    public boolean needsKeepAlive(long j) {
        return this.socketChannel != null && this.keepAliveMillis > 0 && j - this.lastWriteTime > ((long) this.keepAliveMillis);
    }

    public boolean isTimedOut(long j) {
        return this.socketChannel != null && this.timeoutMillis > 0 && j - this.lastReadTime > ((long) this.timeoutMillis);
    }
}
