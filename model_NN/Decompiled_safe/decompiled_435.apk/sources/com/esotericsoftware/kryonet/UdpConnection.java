package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

class UdpConnection {
    InetSocketAddress connectedAddress;
    DatagramChannel datagramChannel;
    int keepAliveMillis = 19000;
    private final Kryo kryo;
    private long lastCommunicationTime;
    final ByteBuffer readBuffer;
    private SelectionKey selectionKey;
    final ByteBuffer writeBuffer;
    private final Object writeLock = new Object();

    public UdpConnection(Kryo kryo2, int i) {
        this.kryo = kryo2;
        this.readBuffer = ByteBuffer.allocateDirect(i);
        this.writeBuffer = ByteBuffer.allocateDirect(i);
    }

    public void bind(Selector selector, int i) throws IOException {
        close();
        try {
            this.datagramChannel = selector.provider().openDatagramChannel();
            this.datagramChannel.socket().bind(new InetSocketAddress(i));
            this.datagramChannel.configureBlocking(false);
            this.selectionKey = this.datagramChannel.register(selector, 1);
            this.lastCommunicationTime = System.currentTimeMillis();
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    public void connect(Selector selector, InetSocketAddress inetSocketAddress) throws IOException {
        close();
        try {
            this.datagramChannel = selector.provider().openDatagramChannel();
            this.datagramChannel.socket().bind(null);
            this.datagramChannel.socket().connect(inetSocketAddress);
            this.datagramChannel.configureBlocking(false);
            this.selectionKey = this.datagramChannel.register(selector, 1);
            this.lastCommunicationTime = System.currentTimeMillis();
            this.connectedAddress = inetSocketAddress;
        } catch (IOException e) {
            close();
            IOException iOException = new IOException("Unable to connect to: " + inetSocketAddress);
            iOException.initCause(e);
            throw iOException;
        }
    }

    public InetSocketAddress readFromAddress() throws IOException {
        DatagramChannel datagramChannel2 = this.datagramChannel;
        if (datagramChannel2 == null) {
            throw new SocketException("Connection is closed.");
        }
        this.lastCommunicationTime = System.currentTimeMillis();
        return (InetSocketAddress) datagramChannel2.receive(this.readBuffer);
    }

    public Object readObject(Connection connection) {
        this.readBuffer.flip();
        try {
            Context context = Kryo.getContext();
            context.put("connection", connection);
            if (connection != null) {
                context.setRemoteEntityID(connection.id);
            }
            Object readClassAndObject = this.kryo.readClassAndObject(this.readBuffer);
            if (!this.readBuffer.hasRemaining()) {
                return readClassAndObject;
            }
            throw new SerializationException("Incorrect number of bytes (" + this.readBuffer.remaining() + " remaining) used to deserialize object: " + readClassAndObject);
        } finally {
            this.readBuffer.clear();
        }
    }

    public int send(Connection connection, Object obj, SocketAddress socketAddress) throws IOException {
        int i;
        DatagramChannel datagramChannel2 = this.datagramChannel;
        if (datagramChannel2 == null) {
            throw new SocketException("Connection is closed.");
        }
        synchronized (this.writeLock) {
            try {
                Context context = Kryo.getContext();
                context.put("connection", connection);
                context.setRemoteEntityID(connection.id);
                this.kryo.writeClassAndObject(this.writeBuffer, obj);
                this.writeBuffer.flip();
                int limit = this.writeBuffer.limit();
                datagramChannel2.send(this.writeBuffer, socketAddress);
                this.lastCommunicationTime = System.currentTimeMillis();
                i = !this.writeBuffer.hasRemaining() ? limit : -1;
                this.writeBuffer.clear();
            } catch (Throwable th) {
                this.writeBuffer.clear();
                throw th;
            }
        }
        return i;
    }

    public void close() {
        this.connectedAddress = null;
        try {
            if (this.datagramChannel != null) {
                this.datagramChannel.close();
                this.datagramChannel = null;
                if (this.selectionKey != null) {
                    this.selectionKey.selector().wakeup();
                }
            }
        } catch (IOException e) {
            if (Log.DEBUG) {
                Log.debug("kryonet", "Unable to close UDP connection.", e);
            }
        }
    }

    public boolean needsKeepAlive(long j) {
        return this.connectedAddress != null && this.keepAliveMillis > 0 && j - this.lastCommunicationTime > ((long) this.keepAliveMillis);
    }
}
