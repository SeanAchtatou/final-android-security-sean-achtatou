package com.esotericsoftware.kryonet.rmi;

import com.esotericsoftware.kryo.CustomSerialization;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.serialize.FieldSerializer;
import com.esotericsoftware.kryo.serialize.IntSerializer;
import com.esotericsoftware.kryo.util.IntHashMap;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.minlog.Log;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

public class ObjectSpace {
    static ObjectSpace[] instances = new ObjectSpace[0];
    private static final Object instancesLock = new Object();
    private static final HashMap<Class, CachedMethod[]> methodCache = new HashMap<>();
    Connection[] connections;
    final Object connectionsLock;
    final IntHashMap idToObject;
    private final Listener invokeListener;

    public static class InvokeMethodResult implements FrameworkMessage {
        public int objectID;
        public byte responseID;
        public Object result;
    }

    public ObjectSpace() {
        this.idToObject = new IntHashMap();
        this.connections = new Connection[0];
        this.connectionsLock = new Object();
        this.invokeListener = new Listener() {
            public void received(Connection connection, Object obj) {
                if (obj instanceof InvokeMethod) {
                    if (ObjectSpace.this.connections != null) {
                        int i = 0;
                        int length = ObjectSpace.this.connections.length;
                        while (i < length && connection != ObjectSpace.this.connections[i]) {
                            i++;
                        }
                        if (i == length) {
                            return;
                        }
                    }
                    InvokeMethod invokeMethod = (InvokeMethod) obj;
                    Object obj2 = ObjectSpace.this.idToObject.get(invokeMethod.objectID);
                    if (obj2 != null) {
                        ObjectSpace.this.invoke(connection, obj2, invokeMethod);
                    } else if (Log.WARN) {
                        Log.warn("kryonet", "Ignoring remote invocation request for unknown object ID: " + invokeMethod.objectID);
                    }
                }
            }

            public void disconnected(Connection connection) {
                ObjectSpace.this.removeConnection(connection);
            }
        };
        synchronized (instancesLock) {
            ObjectSpace[] objectSpaceArr = instances;
            ObjectSpace[] objectSpaceArr2 = new ObjectSpace[(objectSpaceArr.length + 1)];
            objectSpaceArr2[0] = this;
            System.arraycopy(objectSpaceArr, 0, objectSpaceArr2, 1, objectSpaceArr.length);
            instances = objectSpaceArr2;
        }
    }

    public ObjectSpace(Connection connection) {
        this();
        addConnection(connection);
    }

    public void register(int i, Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        }
        this.idToObject.put(i, obj);
        if (Log.TRACE) {
            Log.trace("kryonet", "Object registered with ObjectSpace as " + i + ": " + obj);
        }
    }

    public void close() {
        Connection[] connectionArr = this.connections;
        for (Connection removeListener : connectionArr) {
            removeListener.removeListener(this.invokeListener);
        }
        synchronized (instancesLock) {
            ArrayList arrayList = new ArrayList(Arrays.asList(instances));
            arrayList.remove(this);
            instances = (ObjectSpace[]) arrayList.toArray(new ObjectSpace[arrayList.size()]);
        }
        if (Log.TRACE) {
            Log.trace("kryonet", "Closed ObjectSpace.");
        }
    }

    public void addConnection(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException("connection cannot be null.");
        }
        synchronized (this.connectionsLock) {
            Connection[] connectionArr = new Connection[(this.connections.length + 1)];
            connectionArr[0] = connection;
            System.arraycopy(this.connections, 0, connectionArr, 1, this.connections.length);
            this.connections = connectionArr;
        }
        connection.addListener(this.invokeListener);
        if (Log.TRACE) {
            Log.trace("kryonet", "Added connection to ObjectSpace: " + connection);
        }
    }

    public void removeConnection(Connection connection) {
        if (connection == null) {
            throw new IllegalArgumentException("connection cannot be null.");
        }
        connection.removeListener(this.invokeListener);
        synchronized (this.connectionsLock) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.connections));
            arrayList.remove(connection);
            this.connections = (Connection[]) arrayList.toArray(new Connection[arrayList.size()]);
        }
        if (Log.TRACE) {
            Log.trace("kryonet", "Removed connection from ObjectSpace: " + connection);
        }
    }

    /* access modifiers changed from: protected */
    public void invoke(Connection connection, Object obj, InvokeMethod invokeMethod) {
        if (Log.DEBUG) {
            String str = "";
            if (invokeMethod.args != null) {
                String deepToString = Arrays.deepToString(invokeMethod.args);
                str = deepToString.substring(1, deepToString.length() - 1);
            }
            Log.debug("kryonet", connection + " received: " + obj.getClass().getSimpleName() + "#" + invokeMethod.method.getName() + "(" + str + ")");
        }
        Method method = invokeMethod.method;
        try {
            Object invoke = method.invoke(obj, invokeMethod.args);
            byte b = invokeMethod.responseID;
            if (method.getReturnType() != Void.TYPE && b != 0) {
                InvokeMethodResult invokeMethodResult = new InvokeMethodResult();
                invokeMethodResult.objectID = invokeMethod.objectID;
                invokeMethodResult.responseID = b;
                invokeMethodResult.result = invoke;
                int sendTCP = connection.sendTCP(invokeMethodResult);
                if (Log.DEBUG) {
                    Log.debug("kryonet", connection + " sent: " + invoke + " (" + sendTCP + ")");
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Error invoking method: " + method.getDeclaringClass().getName() + "." + method.getName(), e);
        }
    }

    public static <T> T getRemoteObject(Connection connection, int i, Class<T> cls) {
        return getRemoteObject(connection, i, cls);
    }

    public static RemoteObject getRemoteObject(Connection connection, int i, Class... clsArr) {
        if (connection == null) {
            throw new IllegalArgumentException("connection cannot be null.");
        } else if (clsArr == null) {
            throw new IllegalArgumentException("ifaces cannot be null.");
        } else {
            Class[] clsArr2 = new Class[(clsArr.length + 1)];
            clsArr2[0] = RemoteObject.class;
            System.arraycopy(clsArr, 0, clsArr2, 1, clsArr.length);
            return (RemoteObject) Proxy.newProxyInstance(ObjectSpace.class.getClassLoader(), clsArr2, new RemoteInvocationHandler(connection, i));
        }
    }

    private static class RemoteInvocationHandler implements InvocationHandler {
        private final Connection connection;
        private boolean ignoreResponses;
        private Byte lastResponseID;
        private byte nextResponseID = 1;
        private boolean nonBlocking;
        final int objectID;
        private Listener responseListener;
        final ArrayList<InvokeMethodResult> responseQueue = new ArrayList<>();
        private int timeoutMillis = 3000;

        public RemoteInvocationHandler(Connection connection2, final int i) {
            this.connection = connection2;
            this.objectID = i;
            this.responseListener = new Listener() {
                public void received(Connection connection, Object obj) {
                    if (obj instanceof InvokeMethodResult) {
                        InvokeMethodResult invokeMethodResult = (InvokeMethodResult) obj;
                        if (invokeMethodResult.objectID == i) {
                            synchronized (RemoteInvocationHandler.this.responseQueue) {
                                RemoteInvocationHandler.this.responseQueue.add(invokeMethodResult);
                                RemoteInvocationHandler.this.responseQueue.notifyAll();
                            }
                        }
                    }
                }

                public void disconnected(Connection connection) {
                    RemoteInvocationHandler.this.close();
                }
            };
            connection2.addListener(this.responseListener);
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            if (method.getDeclaringClass() == RemoteObject.class) {
                String name = method.getName();
                if (name.equals("close")) {
                    close();
                    return null;
                } else if (name.equals("setResponseTimeout")) {
                    this.timeoutMillis = ((Integer) objArr[0]).intValue();
                    return null;
                } else if (name.equals("setNonBlocking")) {
                    this.nonBlocking = ((Boolean) objArr[0]).booleanValue();
                    this.ignoreResponses = ((Boolean) objArr[1]).booleanValue();
                    return null;
                } else if (name.equals("waitForLastResponse")) {
                    if (this.lastResponseID != null) {
                        return waitForResponse(this.lastResponseID.byteValue());
                    }
                    throw new IllegalStateException("There is no last response to wait for.");
                } else if (name.equals("getLastResponseID")) {
                    if (this.lastResponseID != null) {
                        return this.lastResponseID;
                    }
                    throw new IllegalStateException("There is no last response ID.");
                } else if (name.equals("waitForResponse")) {
                    if (!this.ignoreResponses) {
                        return waitForResponse(((Byte) objArr[0]).byteValue());
                    }
                    throw new IllegalStateException("This RemoteObject is configured to ignore all responses.");
                }
            }
            InvokeMethod invokeMethod = new InvokeMethod();
            invokeMethod.objectID = this.objectID;
            invokeMethod.method = method;
            invokeMethod.args = objArr;
            boolean z = method.getReturnType() != Void.TYPE;
            if (z && !this.ignoreResponses) {
                byte b = this.nextResponseID;
                this.nextResponseID = (byte) (b + 1);
                if (this.nextResponseID == 0) {
                    this.nextResponseID = (byte) (this.nextResponseID + 1);
                }
                invokeMethod.responseID = b;
            }
            int sendTCP = this.connection.sendTCP(invokeMethod);
            if (Log.DEBUG) {
                String str = "";
                if (objArr != null) {
                    String deepToString = Arrays.deepToString(objArr);
                    str = deepToString.substring(1, deepToString.length() - 1);
                }
                Log.debug("kryonet", this.connection + " sent: " + method.getDeclaringClass().getSimpleName() + "#" + method.getName() + "(" + str + ") (" + sendTCP + ")");
            }
            if (!z) {
                return null;
            }
            if (this.nonBlocking) {
                if (!this.ignoreResponses) {
                    this.lastResponseID = Byte.valueOf(invokeMethod.responseID);
                }
                Class<?> returnType = method.getReturnType();
                if (returnType.isPrimitive()) {
                    if (returnType == Integer.TYPE) {
                        return 0;
                    }
                    if (returnType == Boolean.TYPE) {
                        return Boolean.FALSE;
                    }
                    if (returnType == Float.TYPE) {
                        return Float.valueOf(0.0f);
                    }
                    if (returnType == Character.TYPE) {
                        return 0;
                    }
                    if (returnType == Long.TYPE) {
                        return 0L;
                    }
                    if (returnType == Short.TYPE) {
                        return (short) 0;
                    }
                    if (returnType == Byte.TYPE) {
                        return (byte) 0;
                    }
                    if (returnType == Double.TYPE) {
                        return Double.valueOf(0.0d);
                    }
                }
                return null;
            }
            try {
                return waitForResponse(invokeMethod.responseID);
            } catch (TimeoutException e) {
                throw new TimeoutException("Response timed out: " + method.getDeclaringClass().getName() + "." + method.getName());
            }
        }

        private Object waitForResponse(int i) {
            Object obj;
            if (this.connection.getEndPoint().getUpdateThread() == Thread.currentThread()) {
                throw new IllegalStateException("Cannot wait for an RMI response on the connection's update thread.");
            }
            long currentTimeMillis = System.currentTimeMillis() + ((long) this.timeoutMillis);
            synchronized (this.responseQueue) {
                while (true) {
                    int currentTimeMillis2 = (int) (currentTimeMillis - System.currentTimeMillis());
                    int size = this.responseQueue.size() - 1;
                    while (size >= 0) {
                        InvokeMethodResult invokeMethodResult = this.responseQueue.get(size);
                        if (invokeMethodResult.responseID == i) {
                            this.responseQueue.remove(invokeMethodResult);
                            this.lastResponseID = null;
                            obj = invokeMethodResult.result;
                        } else {
                            size++;
                        }
                    }
                    if (currentTimeMillis2 <= 0) {
                        throw new TimeoutException("Response timed out.");
                    }
                    try {
                        this.responseQueue.wait((long) currentTimeMillis2);
                    } catch (InterruptedException e) {
                    }
                }
            }
            return obj;
        }

        /* access modifiers changed from: package-private */
        public void close() {
            this.connection.removeListener(this.responseListener);
        }
    }

    public static class InvokeMethod implements FrameworkMessage, CustomSerialization {
        public Object[] args;
        public Method method;
        public int objectID;
        public byte responseID;

        public void writeObjectData(Kryo kryo, ByteBuffer byteBuffer) {
            CachedMethod cachedMethod;
            IntSerializer.put(byteBuffer, this.objectID, true);
            IntSerializer.put(byteBuffer, kryo.getRegisteredClass(this.method.getDeclaringClass()).getID(), true);
            CachedMethod[] methods = ObjectSpace.getMethods(kryo, this.method.getDeclaringClass());
            int length = methods.length;
            CachedMethod cachedMethod2 = null;
            int i = 0;
            while (true) {
                if (i >= length) {
                    cachedMethod = cachedMethod2;
                    break;
                }
                cachedMethod2 = methods[i];
                if (cachedMethod2.method.equals(this.method)) {
                    byteBuffer.put((byte) i);
                    cachedMethod = cachedMethod2;
                    break;
                }
                i++;
            }
            int length2 = cachedMethod.serializers.length;
            for (int i2 = 0; i2 < length2; i2++) {
                Serializer serializer = cachedMethod.serializers[i2];
                if (serializer != null) {
                    serializer.writeObject(byteBuffer, this.args[i2]);
                } else {
                    kryo.writeClassAndObject(byteBuffer, this.args[i2]);
                }
            }
            if (this.method.getReturnType() != Void.TYPE) {
                byteBuffer.put(this.responseID);
            }
        }

        public void readObjectData(Kryo kryo, ByteBuffer byteBuffer) {
            this.objectID = IntSerializer.get(byteBuffer, true);
            Class type = kryo.getRegisteredClass(IntSerializer.get(byteBuffer, true)).getType();
            byte b = byteBuffer.get();
            try {
                CachedMethod cachedMethod = ObjectSpace.getMethods(kryo, type)[b];
                this.method = cachedMethod.method;
                this.args = new Object[cachedMethod.serializers.length];
                int length = this.args.length;
                for (int i = 0; i < length; i++) {
                    Serializer serializer = cachedMethod.serializers[i];
                    if (serializer != null) {
                        this.args[i] = serializer.readObject(byteBuffer, this.method.getParameterTypes()[i]);
                    } else {
                        this.args[i] = kryo.readClassAndObject(byteBuffer);
                    }
                }
                if (this.method.getReturnType() != Void.TYPE) {
                    this.responseID = byteBuffer.get();
                }
            } catch (IndexOutOfBoundsException e) {
                throw new SerializationException("Invalid method index " + ((int) b) + " for class: " + type.getName());
            }
        }
    }

    static CachedMethod[] getMethods(Kryo kryo, Class cls) {
        CachedMethod[] cachedMethodArr = methodCache.get(cls);
        if (cachedMethodArr != null) {
            return cachedMethodArr;
        }
        ArrayList arrayList = new ArrayList();
        Class cls2 = cls;
        while (cls2 != null && cls2 != Object.class) {
            Collections.addAll(arrayList, cls2.getDeclaredMethods());
            cls2 = cls2.getSuperclass();
        }
        PriorityQueue priorityQueue = new PriorityQueue(Math.max(1, arrayList.size()), new Comparator<Method>() {
            public int compare(Method method, Method method2) {
                int compareTo = method.getName().compareTo(method2.getName());
                if (compareTo != 0) {
                    return compareTo;
                }
                Class<?>[] parameterTypes = method.getParameterTypes();
                Class<?>[] parameterTypes2 = method2.getParameterTypes();
                if (parameterTypes.length > parameterTypes2.length) {
                    return 1;
                }
                if (parameterTypes.length < parameterTypes2.length) {
                    return -1;
                }
                for (int i = 0; i < parameterTypes.length; i++) {
                    int compareTo2 = parameterTypes[i].getName().compareTo(parameterTypes2[i].getName());
                    if (compareTo2 != 0) {
                        return compareTo2;
                    }
                }
                throw new RuntimeException("Two methods with same signature!");
            }
        });
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Method method = (Method) arrayList.get(i);
            int modifiers = method.getModifiers();
            if (!Modifier.isStatic(modifiers) && !Modifier.isPrivate(modifiers) && !method.isSynthetic()) {
                priorityQueue.add(method);
            }
        }
        int size2 = priorityQueue.size();
        CachedMethod[] cachedMethodArr2 = new CachedMethod[size2];
        for (int i2 = 0; i2 < size2; i2++) {
            CachedMethod cachedMethod = new CachedMethod();
            cachedMethod.method = (Method) priorityQueue.poll();
            Class<?>[] parameterTypes = cachedMethod.method.getParameterTypes();
            cachedMethod.serializers = new Serializer[parameterTypes.length];
            int length = parameterTypes.length;
            for (int i3 = 0; i3 < length; i3++) {
                if (Kryo.isFinal(parameterTypes[i3])) {
                    cachedMethod.serializers[i3] = kryo.getSerializer(parameterTypes[i3]);
                }
            }
            cachedMethodArr2[i2] = cachedMethod;
        }
        methodCache.put(cls, cachedMethodArr2);
        return cachedMethodArr2;
    }

    static Object getRegisteredObject(Connection connection, int i) {
        Object obj;
        for (ObjectSpace objectSpace : instances) {
            Connection[] connectionArr = objectSpace.connections;
            for (Connection connection2 : connectionArr) {
                if (connection2 == connection && (obj = objectSpace.idToObject.get(i)) != null) {
                    return obj;
                }
            }
        }
        return null;
    }

    public static void registerClasses(Kryo kryo) {
        kryo.register(Object[].class);
        kryo.register(InvokeMethod.class);
        ((FieldSerializer) kryo.register(InvokeMethodResult.class).getSerializer()).getField("objectID").setClass(Integer.TYPE, new IntSerializer(true));
        kryo.register(InvocationHandler.class, new Serializer() {
            public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
                IntSerializer.put(byteBuffer, ((RemoteInvocationHandler) Proxy.getInvocationHandler(obj)).objectID, true);
            }

            public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
                int i = IntSerializer.get(byteBuffer, true);
                Connection connection = (Connection) Kryo.getContext().get("connection");
                T registeredObject = ObjectSpace.getRegisteredObject(connection, i);
                if (Log.WARN && registeredObject == null) {
                    Log.warn("kryonet", "Unknown object ID " + i + " for connection: " + connection);
                }
                return registeredObject;
            }
        });
    }

    static class CachedMethod {
        Method method;
        Serializer[] serializers;

        CachedMethod() {
        }
    }
}
