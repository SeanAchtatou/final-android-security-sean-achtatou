package com.esotericsoftware.kryonet.rmi;

public interface RemoteObject {
    void close();

    byte getLastResponseID();

    void setNonBlocking(boolean z, boolean z2);

    void setResponseTimeout(int i);

    Object waitForLastResponse();

    Object waitForResponse(byte b);
}
