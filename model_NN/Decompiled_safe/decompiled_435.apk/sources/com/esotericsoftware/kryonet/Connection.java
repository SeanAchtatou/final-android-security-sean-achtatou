package com.esotericsoftware.kryonet;

import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.BufferOverflowException;

public class Connection {
    EndPoint endPoint;
    int id = -1;
    volatile boolean isConnected;
    private int lastPingID;
    private long lastPingSendTime;
    private Object listenerLock = new Object();
    private Listener[] listeners = new Listener[0];
    private String name;
    private int returnTripTime;
    TcpConnection tcp;
    UdpConnection udp;
    InetSocketAddress udpRemoteAddress;

    protected Connection() {
    }

    /* access modifiers changed from: package-private */
    public void initialize(Kryo kryo, int i, int i2) {
        this.tcp = new TcpConnection(kryo, i, i2);
    }

    public int getID() {
        return this.id;
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public int sendTCP(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        }
        try {
            int send = this.tcp.send(this, obj);
            if (send == 0) {
                if (!Log.TRACE) {
                    return send;
                }
                Log.trace("kryonet", this + " TCP had nothing to send.");
                return send;
            } else if (!Log.DEBUG) {
                return send;
            } else {
                String simpleName = obj == null ? "null" : obj.getClass().getSimpleName();
                if (!(obj instanceof FrameworkMessage)) {
                    Log.debug("kryonet", this + " sent TCP: " + simpleName + " (" + send + ")");
                    return send;
                } else if (!Log.TRACE) {
                    return send;
                } else {
                    Log.trace("kryonet", this + " sent TCP: " + simpleName + " (" + send + ")");
                    return send;
                }
            }
        } catch (IOException e) {
            if (Log.DEBUG) {
                Log.debug("kryonet", "Unable to send TCP with connection: " + this, e);
            }
            close();
            return 0;
        } catch (SerializationException e2) {
            if (e2.causedBy(BufferOverflowException.class)) {
                if (Log.DEBUG) {
                    Log.debug("kryonet", "Unable to send TCP with connection: " + this, e2);
                }
            } else if (Log.ERROR) {
                Log.error("kryonet", "Unable to send TCP with connection: " + this, e2);
            }
            close();
            return 0;
        }
    }

    public int sendUDP(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("object cannot be null.");
        }
        InetSocketAddress inetSocketAddress = this.udpRemoteAddress;
        if (inetSocketAddress == null && this.udp != null) {
            inetSocketAddress = this.udp.connectedAddress;
        }
        if (inetSocketAddress != null || !this.isConnected) {
            Context context = Kryo.getContext();
            context.put("connection", this);
            context.put("connectionID", Integer.valueOf(this.id));
            if (inetSocketAddress == null) {
                try {
                    throw new SocketException("Connection is closed.");
                } catch (IOException e) {
                    if (Log.DEBUG) {
                        Log.debug("kryonet", "Unable to send UDP with connection: " + this, e);
                    }
                    close();
                    return 0;
                } catch (SerializationException e2) {
                    if (e2.causedBy(BufferOverflowException.class)) {
                        if (Log.DEBUG) {
                            Log.debug("kryonet", "Unable to send UDP with connection: " + this, e2);
                        }
                    } else if (Log.ERROR) {
                        Log.error("kryonet", "Unable to send UDP with connection: " + this, e2);
                    }
                    close();
                    return 0;
                }
            } else {
                int send = this.udp.send(this, obj, inetSocketAddress);
                if (send == 0) {
                    if (!Log.TRACE) {
                        return send;
                    }
                    Log.trace("kryonet", this + " UDP had nothing to send.");
                    return send;
                } else if (!Log.DEBUG) {
                    return send;
                } else {
                    if (send != -1) {
                        String simpleName = obj == null ? "null" : obj.getClass().getSimpleName();
                        if (!(obj instanceof FrameworkMessage)) {
                            Log.debug("kryonet", this + " sent UDP: " + simpleName + " (" + send + ")");
                            return send;
                        } else if (!Log.TRACE) {
                            return send;
                        } else {
                            Log.trace("kryonet", this + " sent UDP: " + simpleName + " (" + send + ")");
                            return send;
                        }
                    } else {
                        Log.debug("kryonet", this + " was unable to send, UDP socket buffer full.");
                        return send;
                    }
                }
            }
        } else {
            throw new IllegalStateException("Connection is not connected via UDP.");
        }
    }

    public void close() {
        boolean z = this.isConnected;
        this.isConnected = false;
        this.tcp.close();
        if (!(this.udp == null || this.udp.connectedAddress == null)) {
            this.udp.close();
        }
        if (z) {
            notifyDisconnected();
            if (Log.INFO) {
                Log.info("kryonet", this + " disconnected.");
            }
        }
        setConnected(false);
    }

    public void updateReturnTripTime() {
        FrameworkMessage.Ping ping = new FrameworkMessage.Ping();
        int i = this.lastPingID;
        this.lastPingID = i + 1;
        ping.id = i;
        this.lastPingSendTime = System.currentTimeMillis();
        sendTCP(ping);
    }

    public int getReturnTripTime() {
        return this.returnTripTime;
    }

    public void setKeepAliveTCP(int i) {
        this.tcp.keepAliveMillis = i;
    }

    public void setTimeout(int i) {
        this.tcp.timeoutMillis = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002d, code lost:
        if (com.esotericsoftware.minlog.Log.TRACE == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        com.esotericsoftware.minlog.Log.trace("kryonet", "Connection listener added: " + r7.getClass().getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addListener(com.esotericsoftware.kryonet.Listener r7) {
        /*
            r6 = this;
            r3 = 0
            if (r7 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "listener cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.lang.Object r0 = r6.listenerLock
            monitor-enter(r0)
            com.esotericsoftware.kryonet.Listener[] r1 = r6.listeners     // Catch:{ all -> 0x0050 }
            int r2 = r1.length     // Catch:{ all -> 0x0050 }
        L_0x0011:
            if (r3 >= r2) goto L_0x001c
            r4 = r1[r3]     // Catch:{ all -> 0x0050 }
            if (r7 != r4) goto L_0x0019
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
        L_0x0018:
            return
        L_0x0019:
            int r3 = r3 + 1
            goto L_0x0011
        L_0x001c:
            int r3 = r2 + 1
            com.esotericsoftware.kryonet.Listener[] r3 = new com.esotericsoftware.kryonet.Listener[r3]     // Catch:{ all -> 0x0050 }
            r4 = 0
            r3[r4] = r7     // Catch:{ all -> 0x0050 }
            r4 = 0
            r5 = 1
            java.lang.System.arraycopy(r1, r4, r3, r5, r2)     // Catch:{ all -> 0x0050 }
            r6.listeners = r3     // Catch:{ all -> 0x0050 }
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            boolean r0 = com.esotericsoftware.minlog.Log.TRACE
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = "kryonet"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Connection listener added: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r7.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.esotericsoftware.minlog.Log.trace(r0, r1)
            goto L_0x0018
        L_0x0050:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Connection.addListener(com.esotericsoftware.kryonet.Listener):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0038, code lost:
        if (com.esotericsoftware.minlog.Log.TRACE == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x003a, code lost:
        com.esotericsoftware.minlog.Log.trace("kryonet", "Connection listener removed: " + r10.getClass().getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeListener(com.esotericsoftware.kryonet.Listener r10) {
        /*
            r9 = this;
            r8 = 1
            r4 = 0
            if (r10 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "listener cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            java.lang.Object r0 = r9.listenerLock
            monitor-enter(r0)
            com.esotericsoftware.kryonet.Listener[] r1 = r9.listeners     // Catch:{ all -> 0x002a }
            int r2 = r1.length     // Catch:{ all -> 0x002a }
            if (r2 != 0) goto L_0x0016
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
        L_0x0015:
            return
        L_0x0016:
            int r3 = r2 - r8
            com.esotericsoftware.kryonet.Listener[] r3 = new com.esotericsoftware.kryonet.Listener[r3]     // Catch:{ all -> 0x002a }
            r5 = r4
        L_0x001b:
            if (r5 >= r2) goto L_0x0033
            r6 = r1[r5]     // Catch:{ all -> 0x002a }
            if (r10 != r6) goto L_0x0024
        L_0x0021:
            int r5 = r5 + 1
            goto L_0x001b
        L_0x0024:
            int r7 = r2 - r8
            if (r4 != r7) goto L_0x002d
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            goto L_0x0015
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        L_0x002d:
            int r7 = r4 + 1
            r3[r4] = r6     // Catch:{ all -> 0x002a }
            r4 = r7
            goto L_0x0021
        L_0x0033:
            r9.listeners = r3     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            boolean r0 = com.esotericsoftware.minlog.Log.TRACE
            if (r0 == 0) goto L_0x0015
            java.lang.String r0 = "kryonet"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Connection listener removed: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r10.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.esotericsoftware.minlog.Log.trace(r0, r1)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryonet.Connection.removeListener(com.esotericsoftware.kryonet.Listener):void");
    }

    /* access modifiers changed from: package-private */
    public void notifyConnected() {
        Socket socket;
        InetSocketAddress inetSocketAddress;
        if (!(!Log.INFO || this.tcp.socketChannel == null || (socket = this.tcp.socketChannel.socket()) == null || (inetSocketAddress = (InetSocketAddress) socket.getRemoteSocketAddress()) == null)) {
            Log.info("kryonet", this + " connected: " + inetSocketAddress.getAddress());
        }
        for (Listener connected : this.listeners) {
            connected.connected(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyDisconnected() {
        for (Listener disconnected : this.listeners) {
            disconnected.disconnected(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void notifyReceived(Object obj) {
        if (obj instanceof FrameworkMessage.Ping) {
            FrameworkMessage.Ping ping = (FrameworkMessage.Ping) obj;
            if (!ping.isReply) {
                ping.isReply = true;
                sendTCP(ping);
            } else if (ping.id == this.lastPingID - 1) {
                this.returnTripTime = (int) (System.currentTimeMillis() - this.lastPingSendTime);
                if (Log.TRACE) {
                    Log.trace("kryonet", this + " return trip time: " + this.returnTripTime);
                }
            }
        }
        for (Listener received : this.listeners) {
            received.received(this, obj);
        }
    }

    public EndPoint getEndPoint() {
        return this.endPoint;
    }

    public InetSocketAddress getRemoteAddressTCP() {
        Socket socket;
        if (this.tcp.socketChannel == null || (socket = this.tcp.socketChannel.socket()) == null) {
            return null;
        }
        return (InetSocketAddress) socket.getRemoteSocketAddress();
    }

    public InetSocketAddress getRemoteAddressUDP() {
        InetSocketAddress inetSocketAddress = this.udp.connectedAddress;
        return inetSocketAddress != null ? inetSocketAddress : this.udpRemoteAddress;
    }

    public void setBufferPositionFix(boolean z) {
        this.tcp.bufferPositionFix = z;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String toString() {
        if (this.name != null) {
            return this.name;
        }
        return "Connection " + this.id;
    }

    /* access modifiers changed from: package-private */
    public void setConnected(boolean z) {
        this.isConnected = z;
        if (z && this.name == null) {
            this.name = "Connection " + this.id;
        }
    }
}
