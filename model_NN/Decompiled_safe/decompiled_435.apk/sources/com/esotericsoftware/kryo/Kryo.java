package com.esotericsoftware.kryo;

import com.esotericsoftware.kryo.serialize.ArraySerializer;
import com.esotericsoftware.kryo.serialize.BooleanSerializer;
import com.esotericsoftware.kryo.serialize.ByteSerializer;
import com.esotericsoftware.kryo.serialize.CharSerializer;
import com.esotericsoftware.kryo.serialize.CollectionSerializer;
import com.esotericsoftware.kryo.serialize.CustomSerializer;
import com.esotericsoftware.kryo.serialize.DoubleSerializer;
import com.esotericsoftware.kryo.serialize.EnumSerializer;
import com.esotericsoftware.kryo.serialize.FieldSerializer;
import com.esotericsoftware.kryo.serialize.FloatSerializer;
import com.esotericsoftware.kryo.serialize.IntSerializer;
import com.esotericsoftware.kryo.serialize.LongSerializer;
import com.esotericsoftware.kryo.serialize.MapSerializer;
import com.esotericsoftware.kryo.serialize.ShortSerializer;
import com.esotericsoftware.kryo.serialize.StringSerializer;
import com.esotericsoftware.kryo.util.IntHashMap;
import com.esotericsoftware.minlog.Log;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Kryo {
    private static final int ID_CLASS_NAME = 16383;
    private static final byte ID_NULL_OBJECT = 0;
    private static ThreadLocal<Context> contextThreadLocal = new ThreadLocal<Context>() {
        /* access modifiers changed from: protected */
        public Context initialValue() {
            return new Context();
        }
    };
    public static final String version = "1.03";
    private final ArraySerializer arraySerializer = new ArraySerializer(this);
    private ClassLoader classLoader = getClass().getClassLoader();
    private final ConcurrentHashMap<Class, RegisteredClass> classToRegisteredClass = new ConcurrentHashMap<>(64);
    private final CollectionSerializer collectionSerializer = new CollectionSerializer(this);
    private final CustomSerializer customSerializer = new CustomSerializer(this);
    private final ConcurrentHashMap<Integer, RegisteredClass> idToRegisteredClass = new ConcurrentHashMap<>(64);
    private Object listenerLock = new Object();
    private volatile Listener[] listeners = new Listener[0];
    private final MapSerializer mapSerializer = new MapSerializer(this);
    private AtomicInteger nextClassID = new AtomicInteger(1);
    private boolean registrationOptional;

    public interface Listener {
        void remoteEntityRemoved(int i);
    }

    public Kryo() {
        register(Boolean.TYPE, new BooleanSerializer());
        register(Byte.TYPE, new ByteSerializer());
        register(Character.TYPE, new CharSerializer());
        register(Short.TYPE, new ShortSerializer());
        register(Integer.TYPE, new IntSerializer());
        register(Long.TYPE, new LongSerializer());
        register(Float.TYPE, new FloatSerializer());
        register(Double.TYPE, new DoubleSerializer());
        register(Boolean.class, new BooleanSerializer());
        register(Byte.class, new ByteSerializer());
        register(Character.class, new CharSerializer());
        register(Short.class, new ShortSerializer());
        register(Integer.class, new IntSerializer());
        register(Long.class, new LongSerializer());
        register(Float.class, new FloatSerializer());
        register(Double.class, new DoubleSerializer());
        register(String.class, new StringSerializer());
    }

    public void setRegistrationOptional(boolean z) {
        this.registrationOptional = z;
    }

    public RegisteredClass register(Class cls, Serializer serializer, boolean z) {
        int andIncrement;
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (serializer == null) {
            throw new IllegalArgumentException("serializer cannot be null.");
        } else {
            if (cls.isPrimitive()) {
                serializer.setCanBeNull(false);
            }
            RegisteredClass registeredClass = this.classToRegisteredClass.get(cls);
            if (z) {
                andIncrement = ID_CLASS_NAME;
            } else if (registeredClass != null) {
                andIncrement = registeredClass.id;
            } else {
                andIncrement = this.nextClassID.getAndIncrement();
                if (andIncrement == ID_CLASS_NAME) {
                    andIncrement = this.nextClassID.getAndIncrement();
                }
            }
            RegisteredClass registeredClass2 = new RegisteredClass(cls, andIncrement, serializer);
            if (!z) {
                this.idToRegisteredClass.put(Integer.valueOf(andIncrement), registeredClass2);
            }
            this.classToRegisteredClass.put(cls, registeredClass2);
            if (Log.TRACE && andIncrement > 17) {
                String name = cls.getName();
                if (cls.isArray()) {
                    Class elementClass = ArraySerializer.getElementClass(cls);
                    StringBuilder sb = new StringBuilder(16);
                    int dimensionCount = ArraySerializer.getDimensionCount(cls);
                    for (int i = 0; i < dimensionCount; i++) {
                        sb.append("[]");
                    }
                    name = elementClass.getName() + ((Object) sb);
                }
                if (z) {
                    Log.trace("kryo", "Registered class name: " + name + " (" + serializer.getClass().getName() + ")");
                } else {
                    Log.trace("kryo", "Registered class ID " + andIncrement + ": " + name + " (" + serializer.getClass().getName() + ")");
                }
            }
            return registeredClass2;
        }
    }

    public RegisteredClass register(Class cls, Serializer serializer) {
        return register(cls, serializer, false);
    }

    public RegisteredClass register(Class cls) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        }
        RegisteredClass registeredClass = this.classToRegisteredClass.get(cls);
        if (registeredClass == null || registeredClass.id < 1 || registeredClass.id > 17) {
            return register(cls, newSerializer(cls));
        }
        throw new IllegalArgumentException("Class is registered by default: " + cls.getName());
    }

    public void register(Class cls, RegisteredClass registeredClass) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (registeredClass == null) {
            throw new IllegalArgumentException("registeredClass cannot be null.");
        } else {
            this.classToRegisteredClass.put(cls, registeredClass);
            if (Log.TRACE) {
                String name = cls.getName();
                if (cls.isArray()) {
                    Class elementClass = ArraySerializer.getElementClass(cls);
                    StringBuilder sb = new StringBuilder(16);
                    int dimensionCount = ArraySerializer.getDimensionCount(cls);
                    for (int i = 0; i < dimensionCount; i++) {
                        sb.append("[]");
                    }
                    name = elementClass.getName() + ((Object) sb);
                }
                if (registeredClass.id == ID_CLASS_NAME) {
                    Log.trace("kryo", "Registered class name: " + name + " (" + registeredClass.serializer.getClass().getName() + ")");
                } else {
                    Log.trace("kryo", "Registered class ID " + registeredClass.id + ": " + name + " (" + registeredClass.serializer.getClass().getName() + ")");
                }
            }
        }
    }

    public Serializer newSerializer(Class cls) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (cls.isArray()) {
            return this.arraySerializer;
        } else {
            if (CustomSerialization.class.isAssignableFrom(cls)) {
                return this.customSerializer;
            }
            if (Collection.class.isAssignableFrom(cls)) {
                return this.collectionSerializer;
            }
            if (Map.class.isAssignableFrom(cls)) {
                return this.mapSerializer;
            }
            if (Enum.class.isAssignableFrom(cls)) {
                return new EnumSerializer(cls);
            }
            if (!cls.isAnnotationPresent(DefaultSerializer.class)) {
                return newDefaultSerializer(cls);
            }
            Class<? extends Serializer> value = ((DefaultSerializer) cls.getAnnotation(DefaultSerializer.class)).value();
            try {
                return (Serializer) value.getConstructor(Kryo.class, Class.class).newInstance(this, cls);
            } catch (NoSuchMethodException e) {
                try {
                    return (Serializer) value.getConstructor(Kryo.class).newInstance(this);
                } catch (NoSuchMethodException e2) {
                    try {
                        return (Serializer) value.getConstructor(Class.class).newInstance(this, cls);
                    } catch (NoSuchMethodException e3) {
                        try {
                            return (Serializer) value.newInstance();
                        } catch (Exception e4) {
                            throw new IllegalArgumentException("Unable to create serializer \"" + value.getName() + "\" for class: " + cls.getName(), e4);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Serializer newDefaultSerializer(Class cls) {
        return new FieldSerializer(this, cls);
    }

    public RegisteredClass getRegisteredClass(Class cls) {
        Class<?> cls2;
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        }
        RegisteredClass registeredClass = this.classToRegisteredClass.get(cls);
        if (registeredClass != null) {
            return registeredClass;
        }
        if (Proxy.isProxyClass(cls)) {
            return getRegisteredClass(InvocationHandler.class);
        }
        if (cls.isEnum() || !Enum.class.isAssignableFrom(cls)) {
            cls2 = cls;
        } else {
            Class<?> enclosingClass = cls.getEnclosingClass();
            RegisteredClass registeredClass2 = this.classToRegisteredClass.get(enclosingClass);
            if (registeredClass2 != null) {
                return registeredClass2;
            }
            cls2 = enclosingClass;
        }
        if (this.registrationOptional) {
            handleUnregisteredClass(cls2);
            RegisteredClass registeredClass3 = this.classToRegisteredClass.get(cls2);
            if (registeredClass3 != null) {
                return registeredClass3;
            }
        }
        if (cls2.isArray()) {
            Class elementClass = ArraySerializer.getElementClass(cls2);
            StringBuilder sb = new StringBuilder(16);
            int dimensionCount = ArraySerializer.getDimensionCount(cls2);
            for (int i = 0; i < dimensionCount; i++) {
                sb.append("[]");
            }
            throw new IllegalArgumentException("Class is not registered: " + cls2.getName() + "\nNote: To register this class use: kryo.register(" + elementClass.getName() + ((Object) sb) + ".class);");
        }
        throw new IllegalArgumentException("Class is not registered: " + cls2.getName());
    }

    /* access modifiers changed from: protected */
    public void handleUnregisteredClass(Class cls) {
        register(cls, newSerializer(cls), true);
    }

    public RegisteredClass getRegisteredClass(int i) {
        RegisteredClass registeredClass = this.idToRegisteredClass.get(Integer.valueOf(i));
        if (registeredClass != null) {
            return registeredClass;
        }
        throw new IllegalArgumentException("Class ID is not registered: " + i);
    }

    public void setClassLoader(ClassLoader classLoader2) {
        this.classLoader = classLoader2;
    }

    public ClassLoader getClassLoader() {
        return this.classLoader;
    }

    public Serializer getSerializer(Class cls) {
        return getRegisteredClass(cls).serializer;
    }

    public void setSerializer(Class cls, Serializer serializer) {
        getRegisteredClass(cls).serializer = serializer;
    }

    public RegisteredClass writeClass(ByteBuffer byteBuffer, Class cls) {
        ClassReferences classReferences;
        ClassReferences classReferences2;
        if (cls == null) {
            try {
                byteBuffer.put((byte) ID_NULL_OBJECT);
                if (Log.TRACE) {
                    Log.trace("kryo", "Wrote object: null");
                }
                return null;
            } catch (BufferOverflowException e) {
                throw new SerializationException("Buffer limit exceeded writing null object.", e);
            }
        } else {
            try {
                RegisteredClass registeredClass = getRegisteredClass(cls);
                IntSerializer.put(byteBuffer, registeredClass.id, true);
                if (registeredClass.id == ID_CLASS_NAME) {
                    Context context = getContext();
                    ClassReferences classReferences3 = (ClassReferences) context.getTemp("classReferences");
                    if (classReferences3 == null) {
                        ClassReferences classReferences4 = (ClassReferences) context.get("classReferences");
                        if (classReferences4 == null) {
                            ClassReferences classReferences5 = new ClassReferences();
                            context.put("classReferences", classReferences5);
                            classReferences2 = classReferences5;
                        } else {
                            classReferences4.reset();
                            classReferences2 = classReferences4;
                        }
                        context.putTemp("classReferences", classReferences2);
                        classReferences = classReferences2;
                    } else {
                        classReferences = classReferences3;
                    }
                    Integer num = classReferences.classToReference.get(cls);
                    if (num != null) {
                        IntSerializer.put(byteBuffer, num.intValue(), true);
                        if (!Log.TRACE) {
                            return registeredClass;
                        }
                        Log.trace("kryo", "Wrote class name reference " + num + ": " + cls.getName());
                        return registeredClass;
                    }
                    byteBuffer.put((byte) ID_NULL_OBJECT);
                    HashMap<Class, Integer> hashMap = classReferences.classToReference;
                    int i = classReferences.referenceCount;
                    classReferences.referenceCount = i + 1;
                    hashMap.put(cls, Integer.valueOf(i));
                    StringSerializer.put(byteBuffer, cls.getName());
                    if (!Log.TRACE) {
                        return registeredClass;
                    }
                    Log.trace("kryo", "Wrote class name: " + cls.getName());
                    return registeredClass;
                } else if (!Log.TRACE) {
                    return registeredClass;
                } else {
                    Log.trace("kryo", "Wrote class " + registeredClass.id + ": " + cls.getName());
                    return registeredClass;
                }
            } catch (SerializationException e2) {
                if (e2.causedBy(BufferOverflowException.class)) {
                    throw new SerializationException("Buffer limit exceeded writing class ID: " + cls, e2);
                }
                throw e2;
            } catch (BufferOverflowException e3) {
                throw new SerializationException("Buffer limit exceeded writing class ID: " + cls, e3);
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.esotericsoftware.kryo.Kryo.RegisteredClass readClass(java.nio.ByteBuffer r6) {
        /*
            r5 = this;
            r0 = 1
            int r0 = com.esotericsoftware.kryo.serialize.IntSerializer.get(r6, r0)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r0 != 0) goto L_0x0014
            boolean r0 = com.esotericsoftware.minlog.Log.TRACE     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r0 == 0) goto L_0x0012
            java.lang.String r0 = "kryo"
            java.lang.String r1 = "Read object: null"
            com.esotericsoftware.minlog.Log.trace(r0, r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
        L_0x0012:
            r0 = 0
        L_0x0013:
            return r0
        L_0x0014:
            r1 = 16383(0x3fff, float:2.2957E-41)
            if (r0 != r1) goto L_0x0107
            com.esotericsoftware.kryo.Context r1 = getContext()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r0 = "classReferences"
            java.lang.Object r0 = r1.getTemp(r0)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            com.esotericsoftware.kryo.Kryo$ClassReferences r0 = (com.esotericsoftware.kryo.Kryo.ClassReferences) r0     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r0 != 0) goto L_0x0040
            java.lang.String r0 = "classReferences"
            java.lang.Object r0 = r1.get(r0)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            com.esotericsoftware.kryo.Kryo$ClassReferences r0 = (com.esotericsoftware.kryo.Kryo.ClassReferences) r0     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r0 != 0) goto L_0x007b
            java.lang.String r0 = "classReferences"
            com.esotericsoftware.kryo.Kryo$ClassReferences r2 = new com.esotericsoftware.kryo.Kryo$ClassReferences     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r2.<init>()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r1.put(r0, r2)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r0 = r2
        L_0x003b:
            java.lang.String r2 = "classReferences"
            r1.putTemp(r2, r0)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
        L_0x0040:
            r1 = 1
            int r1 = com.esotericsoftware.kryo.serialize.IntSerializer.get(r6, r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r1 == 0) goto L_0x00b8
            com.esotericsoftware.kryo.util.IntHashMap r0 = r0.referenceToClass     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.Class r0 = (java.lang.Class) r0     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r0 != 0) goto L_0x0088
            com.esotericsoftware.kryo.SerializationException r0 = new com.esotericsoftware.kryo.SerializationException     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r2.<init>()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r3 = "Invalid class name reference: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r1 = r1.toString()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r0.<init>(r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            throw r0     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
        L_0x006a:
            r0 = move-exception
            java.lang.Class<java.nio.BufferUnderflowException> r1 = java.nio.BufferUnderflowException.class
            boolean r1 = r0.causedBy(r1)
            if (r1 == 0) goto L_0x0106
            com.esotericsoftware.kryo.SerializationException r1 = new com.esotericsoftware.kryo.SerializationException
            java.lang.String r2 = "Buffer limit exceeded reading class ID."
            r1.<init>(r2, r0)
            throw r1
        L_0x007b:
            r0.reset()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            goto L_0x003b
        L_0x007f:
            r0 = move-exception
            com.esotericsoftware.kryo.SerializationException r1 = new com.esotericsoftware.kryo.SerializationException
            java.lang.String r2 = "Buffer limit exceeded reading class ID."
            r1.<init>(r2, r0)
            throw r1
        L_0x0088:
            boolean r2 = com.esotericsoftware.minlog.Log.TRACE     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r2 == 0) goto L_0x00b2
            java.lang.String r2 = "kryo"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r3.<init>()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r4 = "Read class name reference "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r3 = ": "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r3 = r0.getName()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r1 = r1.toString()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            com.esotericsoftware.minlog.Log.trace(r2, r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
        L_0x00b2:
            com.esotericsoftware.kryo.Kryo$RegisteredClass r0 = r5.getRegisteredClass(r0)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            goto L_0x0013
        L_0x00b8:
            java.lang.String r1 = com.esotericsoftware.kryo.serialize.StringSerializer.get(r6)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            boolean r2 = com.esotericsoftware.minlog.Log.TRACE     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            if (r2 == 0) goto L_0x00d8
            java.lang.String r2 = "kryo"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r3.<init>()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r4 = "Read class name: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r3 = r3.toString()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            com.esotericsoftware.minlog.Log.trace(r2, r3)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
        L_0x00d8:
            r2 = 0
            java.lang.ClassLoader r3 = r5.classLoader     // Catch:{ ClassNotFoundException -> 0x00ec }
            java.lang.Class r1 = java.lang.Class.forName(r1, r2, r3)     // Catch:{ ClassNotFoundException -> 0x00ec }
            com.esotericsoftware.kryo.util.IntHashMap r2 = r0.referenceToClass     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            int r3 = r0.referenceCount     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            int r4 = r3 + 1
            r0.referenceCount = r4     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r2.put(r3, r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r0 = r1
            goto L_0x00b2
        L_0x00ec:
            r0 = move-exception
            com.esotericsoftware.kryo.SerializationException r2 = new com.esotericsoftware.kryo.SerializationException     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r3.<init>()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r4 = "Unable to find class: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            java.lang.String r1 = r1.toString()     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            r2.<init>(r1, r0)     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
            throw r2     // Catch:{ SerializationException -> 0x006a, BufferUnderflowException -> 0x007f }
        L_0x0106:
            throw r0
        L_0x0107:
            java.util.concurrent.ConcurrentHashMap<java.lang.Integer, com.esotericsoftware.kryo.Kryo$RegisteredClass> r1 = r5.idToRegisteredClass
            java.lang.Integer r2 = java.lang.Integer.valueOf(r0)
            java.lang.Object r5 = r1.get(r2)
            com.esotericsoftware.kryo.Kryo$RegisteredClass r5 = (com.esotericsoftware.kryo.Kryo.RegisteredClass) r5
            if (r5 != 0) goto L_0x012e
            com.esotericsoftware.kryo.SerializationException r1 = new com.esotericsoftware.kryo.SerializationException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Encountered unregistered class ID: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x012e:
            boolean r1 = com.esotericsoftware.minlog.Log.TRACE
            if (r1 == 0) goto L_0x015a
            java.lang.String r1 = "kryo"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Read class "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = ": "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.Class r2 = r5.type
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.esotericsoftware.minlog.Log.trace(r1, r0)
        L_0x015a:
            r0 = r5
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryo.Kryo.readClass(java.nio.ByteBuffer):com.esotericsoftware.kryo.Kryo$RegisteredClass");
    }

    public void writeClassAndObject(ByteBuffer byteBuffer, Object obj) {
        if (obj == null) {
            try {
                byteBuffer.put((byte) ID_NULL_OBJECT);
                if (Log.TRACE) {
                    Log.trace("kryo", "Wrote object: null");
                }
            } catch (BufferOverflowException e) {
                throw new SerializationException("Buffer limit exceeded writing null object.", e);
            }
        } else {
            RegisteredClass writeClass = writeClass(byteBuffer, obj.getClass());
            if (writeClass != null) {
                Context context = getContext();
                context.objectGraphLevel++;
                try {
                    writeClass.serializer.writeObjectData(byteBuffer, obj);
                    context.objectGraphLevel--;
                    if (context.objectGraphLevel == 0) {
                        context.reset();
                    }
                } catch (SerializationException e2) {
                    if (e2.causedBy(BufferOverflowException.class)) {
                        throw new SerializationException("Buffer limit exceeded writing object of type: " + obj.getClass().getName(), e2);
                    }
                    throw new SerializationException("Unable to serialize object of type: " + obj.getClass().getName(), e2);
                } catch (BufferOverflowException e3) {
                    throw new SerializationException("Buffer limit exceeded writing object of type: " + obj.getClass().getName(), e3);
                } catch (Throwable th) {
                    context.objectGraphLevel--;
                    if (context.objectGraphLevel == 0) {
                        context.reset();
                    }
                    throw th;
                }
            }
        }
    }

    public void writeObject(ByteBuffer byteBuffer, Object obj) {
        if (obj == null) {
            try {
                byteBuffer.put((byte) ID_NULL_OBJECT);
                if (Log.TRACE) {
                    Log.trace("kryo", "Wrote object: null");
                }
            } catch (BufferOverflowException e) {
                throw new SerializationException("Buffer limit exceeded writing null object.", e);
            }
        } else {
            Context context = getContext();
            context.objectGraphLevel++;
            try {
                getRegisteredClass(obj.getClass()).serializer.writeObject(byteBuffer, obj);
                context.objectGraphLevel--;
                if (context.objectGraphLevel == 0) {
                    context.reset();
                }
            } catch (SerializationException e2) {
                if (e2.causedBy(BufferOverflowException.class)) {
                    throw new SerializationException("Buffer limit exceeded writing object of type: " + obj.getClass().getName(), e2);
                }
                throw new SerializationException("Unable to serialize object of type: " + obj.getClass().getName(), e2);
            } catch (BufferOverflowException e3) {
                throw new SerializationException("Buffer limit exceeded writing object of type: " + obj.getClass().getName(), e3);
            } catch (Throwable th) {
                context.objectGraphLevel--;
                if (context.objectGraphLevel == 0) {
                    context.reset();
                }
                throw th;
            }
        }
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        Context context = getContext();
        context.objectGraphLevel++;
        try {
            getRegisteredClass(obj.getClass()).serializer.writeObjectData(byteBuffer, obj);
            context.objectGraphLevel--;
            if (context.objectGraphLevel == 0) {
                context.reset();
            }
        } catch (SerializationException e) {
            if (e.causedBy(BufferOverflowException.class)) {
                throw new SerializationException("Buffer limit exceeded writing object of type: " + obj.getClass().getName(), e);
            }
            throw new SerializationException("Unable to serialize object of type: " + obj.getClass().getName(), e);
        } catch (BufferOverflowException e2) {
            throw new SerializationException("Buffer limit exceeded writing object of type: " + obj.getClass().getName(), e2);
        } catch (Throwable th) {
            context.objectGraphLevel--;
            if (context.objectGraphLevel == 0) {
                context.reset();
            }
            throw th;
        }
    }

    public Object readClassAndObject(ByteBuffer byteBuffer) {
        RegisteredClass readClass = readClass(byteBuffer);
        if (readClass == null) {
            return null;
        }
        Context context = getContext();
        context.objectGraphLevel++;
        try {
            Object readObjectData = readClass.serializer.readObjectData(byteBuffer, readClass.type);
            context.objectGraphLevel--;
            if (context.objectGraphLevel != 0) {
                return readObjectData;
            }
            context.reset();
            return readObjectData;
        } catch (SerializationException e) {
            if (e.causedBy(BufferUnderflowException.class)) {
                throw new SerializationException("Buffer limit exceeded reading object of type: " + readClass.type.getName(), e);
            }
            throw new SerializationException("Unable to deserialize object of type: " + readClass.type.getName(), e);
        } catch (BufferUnderflowException e2) {
            throw new SerializationException("Buffer limit exceeded reading object of type: " + readClass.type.getName(), e2);
        } catch (Throwable th) {
            context.objectGraphLevel--;
            if (context.objectGraphLevel == 0) {
                context.reset();
            }
            throw th;
        }
    }

    public <T> T readObject(ByteBuffer byteBuffer, Class<T> cls) {
        Context context = getContext();
        context.objectGraphLevel++;
        try {
            T readObject = getRegisteredClass(cls).serializer.readObject(byteBuffer, cls);
            context.objectGraphLevel--;
            if (context.objectGraphLevel == 0) {
                context.reset();
            }
            return readObject;
        } catch (SerializationException e) {
            if (e.causedBy(BufferUnderflowException.class)) {
                throw new SerializationException("Buffer limit exceeded reading object of type: " + cls.getName(), e);
            }
            throw new SerializationException("Unable to deserialize object of type: " + cls.getName(), e);
        } catch (BufferUnderflowException e2) {
            throw new SerializationException("Buffer limit exceeded reading object of type: " + cls.getName(), e2);
        } catch (Throwable th) {
            context.objectGraphLevel--;
            if (context.objectGraphLevel == 0) {
                context.reset();
            }
            throw th;
        }
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        Context context = getContext();
        context.objectGraphLevel++;
        try {
            T readObjectData = getRegisteredClass(cls).serializer.readObjectData(byteBuffer, cls);
            context.objectGraphLevel--;
            if (context.objectGraphLevel == 0) {
                context.reset();
            }
            return readObjectData;
        } catch (SerializationException e) {
            if (e.causedBy(BufferUnderflowException.class)) {
                throw new SerializationException("Buffer limit exceeded reading object of type: " + cls.getName(), e);
            }
            throw new SerializationException("Unable to deserialize object of type: " + cls.getName(), e);
        } catch (BufferUnderflowException e2) {
            throw new SerializationException("Buffer limit exceeded reading object of type: " + cls.getName(), e2);
        } catch (Throwable th) {
            context.objectGraphLevel--;
            if (context.objectGraphLevel == 0) {
                context.reset();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002d, code lost:
        if (com.esotericsoftware.minlog.Log.TRACE == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        com.esotericsoftware.minlog.Log.trace("kryo", "Kryo listener added: " + r7.getClass().getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addListener(com.esotericsoftware.kryo.Kryo.Listener r7) {
        /*
            r6 = this;
            r3 = 0
            if (r7 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "listener cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.lang.Object r0 = r6.listenerLock
            monitor-enter(r0)
            com.esotericsoftware.kryo.Kryo$Listener[] r1 = r6.listeners     // Catch:{ all -> 0x0050 }
            int r2 = r1.length     // Catch:{ all -> 0x0050 }
        L_0x0011:
            if (r3 >= r2) goto L_0x001c
            r4 = r1[r3]     // Catch:{ all -> 0x0050 }
            if (r7 != r4) goto L_0x0019
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
        L_0x0018:
            return
        L_0x0019:
            int r3 = r3 + 1
            goto L_0x0011
        L_0x001c:
            int r3 = r2 + 1
            com.esotericsoftware.kryo.Kryo$Listener[] r3 = new com.esotericsoftware.kryo.Kryo.Listener[r3]     // Catch:{ all -> 0x0050 }
            r4 = 0
            r3[r4] = r7     // Catch:{ all -> 0x0050 }
            r4 = 0
            r5 = 1
            java.lang.System.arraycopy(r1, r4, r3, r5, r2)     // Catch:{ all -> 0x0050 }
            r6.listeners = r3     // Catch:{ all -> 0x0050 }
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            boolean r0 = com.esotericsoftware.minlog.Log.TRACE
            if (r0 == 0) goto L_0x0018
            java.lang.String r0 = "kryo"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Kryo listener added: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r7.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.esotericsoftware.minlog.Log.trace(r0, r1)
            goto L_0x0018
        L_0x0050:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0050 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryo.Kryo.addListener(com.esotericsoftware.kryo.Kryo$Listener):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003d, code lost:
        if (com.esotericsoftware.minlog.Log.TRACE == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x003f, code lost:
        com.esotericsoftware.minlog.Log.trace("kryo", "Kryo listener removed: " + r9.getClass().getName());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeListener(com.esotericsoftware.kryo.Kryo.Listener r9) {
        /*
            r8 = this;
            r7 = 1
            r4 = 0
            if (r9 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "listener cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            java.lang.Object r0 = r8.listenerLock
            monitor-enter(r0)
            com.esotericsoftware.kryo.Kryo$Listener[] r1 = r8.listeners     // Catch:{ all -> 0x002a }
            int r2 = r1.length     // Catch:{ all -> 0x002a }
            if (r2 != 0) goto L_0x0016
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
        L_0x0015:
            return
        L_0x0016:
            int r3 = r2 - r7
            com.esotericsoftware.kryo.Kryo$Listener[] r3 = new com.esotericsoftware.kryo.Kryo.Listener[r3]     // Catch:{ all -> 0x002a }
            r5 = r4
        L_0x001b:
            if (r5 >= r2) goto L_0x0033
            r6 = r1[r5]     // Catch:{ all -> 0x002a }
            if (r9 != r6) goto L_0x0024
        L_0x0021:
            int r5 = r5 + 1
            goto L_0x001b
        L_0x0024:
            int r6 = r2 - r7
            if (r4 != r6) goto L_0x002d
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            goto L_0x0015
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        L_0x002d:
            int r6 = r4 + 1
            r3[r4] = r9     // Catch:{ all -> 0x002a }
            r4 = r6
            goto L_0x0021
        L_0x0033:
            r4 = 0
            r5 = 1
            java.lang.System.arraycopy(r1, r4, r3, r5, r2)     // Catch:{ all -> 0x002a }
            r8.listeners = r3     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            boolean r0 = com.esotericsoftware.minlog.Log.TRACE
            if (r0 == 0) goto L_0x0015
            java.lang.String r0 = "kryo"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Kryo listener removed: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r9.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.esotericsoftware.minlog.Log.trace(r0, r1)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryo.Kryo.removeListener(com.esotericsoftware.kryo.Kryo$Listener):void");
    }

    public void removeRemoteEntity(int i) {
        Listener[] listenerArr = this.listeners;
        if (Log.TRACE) {
            Log.trace("kryo", "Remote ID removed: " + i);
        }
        for (Listener remoteEntityRemoved : listenerArr) {
            remoteEntityRemoved.remoteEntityRemoved(i);
        }
    }

    public <T> T newInstance(Class<T> cls) {
        try {
            return cls.newInstance();
        } catch (Exception e) {
            e = e;
            try {
                Constructor<T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
                declaredConstructor.setAccessible(true);
                return declaredConstructor.newInstance(new Object[0]);
            } catch (SecurityException e2) {
            } catch (NoSuchMethodException e3) {
                if (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) {
                    throw new SerializationException("Class cannot be created (missing no-arg constructor): " + cls.getName(), e);
                }
                throw new SerializationException("Class cannot be created (non-static member class): " + cls.getName(), e);
            } catch (Exception e4) {
                e = e4;
            }
        }
        throw new SerializationException("Error constructing instance of class: " + cls.getName(), e);
    }

    public static boolean isFinal(Class cls) {
        if (cls.isArray()) {
            return Modifier.isFinal(ArraySerializer.getElementClass(cls).getModifiers());
        }
        return Modifier.isFinal(cls.getModifiers());
    }

    public static Context getContext() {
        return contextThreadLocal.get();
    }

    public static class RegisteredClass {
        final int id;
        Serializer serializer;
        final Class type;

        RegisteredClass(Class cls, int i, Serializer serializer2) {
            this.type = cls;
            this.id = i;
            this.serializer = serializer2;
        }

        public Class getType() {
            return this.type;
        }

        public Serializer getSerializer() {
            return this.serializer;
        }

        public void setSerializer(Serializer serializer2) {
            this.serializer = serializer2;
        }

        public int getID() {
            return this.id;
        }
    }

    static class ClassReferences {
        public HashMap<Class, Integer> classToReference = new HashMap<>();
        public int referenceCount = 1;
        public IntHashMap referenceToClass = new IntHashMap();

        ClassReferences() {
        }

        public void reset() {
            this.classToReference.clear();
            this.referenceToClass.clear();
            this.referenceCount = 1;
        }
    }
}
