package com.esotericsoftware.kryo;

import com.esotericsoftware.minlog.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public class ObjectBuffer {
    private ByteBuffer buffer;
    private byte[] bytes;
    private Kryo kryo;
    private final int maxCapacity;

    public ObjectBuffer(Kryo kryo2) {
        this(kryo2, Opcodes.ACC_STRICT, Opcodes.ACC_ENUM);
    }

    public ObjectBuffer(Kryo kryo2, int i) {
        this(kryo2, i, i);
    }

    public ObjectBuffer(Kryo kryo2, int i, int i2) {
        this.kryo = kryo2;
        this.buffer = ByteBuffer.allocate(i);
        this.bytes = this.buffer.array();
        this.maxCapacity = i2;
    }

    public void setKryo(Kryo kryo2) {
        this.kryo = kryo2;
    }

    private void readToBuffer(InputStream inputStream, int i) {
        int i2;
        int i3 = 0;
        if (i == -1) {
            i2 = Integer.MAX_VALUE;
        } else {
            i2 = i;
        }
        while (i3 < i2) {
            try {
                int read = inputStream.read(this.bytes, i3, Math.min(this.bytes.length - i3, i2 - i3));
                if (read == -1) {
                    break;
                }
                i3 += read;
                if (i3 == this.bytes.length && !resizeBuffer(true)) {
                    throw new SerializationException("Buffer limit exceeded: " + this.maxCapacity);
                }
            } catch (IOException e) {
                throw new SerializationException("Error reading object bytes.", e);
            }
        }
        this.buffer.position(0);
        this.buffer.limit(i3);
    }

    public Object readClassAndObject(InputStream inputStream) {
        readToBuffer(inputStream, -1);
        return this.kryo.readClassAndObject(this.buffer);
    }

    public Object readClassAndObject(InputStream inputStream, int i) {
        readToBuffer(inputStream, i);
        return this.kryo.readClassAndObject(this.buffer);
    }

    public <T> T readObject(InputStream inputStream, Class<T> cls) {
        readToBuffer(inputStream, -1);
        return this.kryo.readObject(this.buffer, cls);
    }

    public <T> T readObject(InputStream inputStream, int i, Class<T> cls) {
        readToBuffer(inputStream, i);
        return this.kryo.readObject(this.buffer, cls);
    }

    public <T> T readObjectData(InputStream inputStream, Class<T> cls) {
        readToBuffer(inputStream, -1);
        return this.kryo.readObjectData(this.buffer, cls);
    }

    public <T> T readObjectData(InputStream inputStream, int i, Class<T> cls) {
        readToBuffer(inputStream, i);
        return this.kryo.readObjectData(this.buffer, cls);
    }

    public void writeClassAndObject(OutputStream outputStream, Object obj) {
        this.buffer.clear();
        while (true) {
            try {
                this.kryo.writeClassAndObject(this.buffer, obj);
                writeToStream(outputStream);
                return;
            } catch (SerializationException e) {
                if (!e.causedBy(BufferOverflowException.class)) {
                    throw e;
                } else if (!resizeBuffer(false)) {
                    throw new SerializationException("Buffer limit exceeded serializing object of type: " + obj.getClass().getName(), e);
                } else {
                    Kryo.getContext().reset();
                }
            }
        }
    }

    public void writeObject(OutputStream outputStream, Object obj) {
        this.buffer.clear();
        while (true) {
            try {
                this.kryo.writeObject(this.buffer, obj);
                writeToStream(outputStream);
                return;
            } catch (SerializationException e) {
                if (!e.causedBy(BufferOverflowException.class)) {
                    throw e;
                } else if (!resizeBuffer(false)) {
                    throw new SerializationException("Buffer limit exceeded serializing object of type: " + obj.getClass().getName(), e);
                } else {
                    Kryo.getContext().reset();
                }
            }
        }
    }

    public void writeObjectData(OutputStream outputStream, Object obj) {
        this.buffer.clear();
        while (true) {
            try {
                this.kryo.writeObjectData(this.buffer, obj);
                writeToStream(outputStream);
                return;
            } catch (SerializationException e) {
                if (!e.causedBy(BufferOverflowException.class)) {
                    throw e;
                } else if (!resizeBuffer(false)) {
                    throw new SerializationException("Buffer limit exceeded serializing object of type: " + obj.getClass().getName(), e);
                } else {
                    Kryo.getContext().reset();
                }
            }
        }
    }

    private void writeToStream(OutputStream outputStream) {
        try {
            outputStream.write(this.bytes, 0, this.buffer.position());
        } catch (IOException e) {
            throw new SerializationException("Error writing object bytes.", e);
        }
    }

    public Object readClassAndObject(byte[] bArr) {
        return this.kryo.readClassAndObject(ByteBuffer.wrap(bArr));
    }

    public <T> T readObject(byte[] bArr, Class<T> cls) {
        return this.kryo.readObject(ByteBuffer.wrap(bArr), cls);
    }

    public <T> T readObjectData(byte[] bArr, Class<T> cls) {
        return this.kryo.readObjectData(ByteBuffer.wrap(bArr), cls);
    }

    public byte[] writeClassAndObject(Object obj) {
        this.buffer.clear();
        while (true) {
            try {
                this.kryo.writeClassAndObject(this.buffer, obj);
                return writeToBytes();
            } catch (SerializationException e) {
                if (!e.causedBy(BufferOverflowException.class)) {
                    throw e;
                } else if (!resizeBuffer(false)) {
                    throw new SerializationException("Buffer limit exceeded serializing object of type: " + obj.getClass().getName(), e);
                } else {
                    Kryo.getContext().reset();
                }
            }
        }
    }

    public byte[] writeObject(Object obj) {
        this.buffer.clear();
        while (true) {
            try {
                this.kryo.writeObject(this.buffer, obj);
                return writeToBytes();
            } catch (SerializationException e) {
                if (!e.causedBy(BufferOverflowException.class)) {
                    throw e;
                } else if (!resizeBuffer(false)) {
                    throw new SerializationException("Buffer limit exceeded serializing object of type: " + obj.getClass().getName(), e);
                } else {
                    Kryo.getContext().reset();
                }
            }
        }
    }

    public byte[] writeObjectData(Object obj) {
        this.buffer.clear();
        while (true) {
            try {
                this.kryo.writeObjectData(this.buffer, obj);
                return writeToBytes();
            } catch (SerializationException e) {
                if (!e.causedBy(BufferOverflowException.class)) {
                    throw e;
                } else if (!resizeBuffer(false)) {
                    throw new SerializationException("Buffer limit exceeded serializing object of type: " + obj.getClass().getName(), e);
                } else {
                    Kryo.getContext().reset();
                }
            }
        }
    }

    private byte[] writeToBytes() {
        byte[] bArr = new byte[this.buffer.position()];
        System.arraycopy(this.bytes, 0, bArr, 0, bArr.length);
        return bArr;
    }

    private boolean resizeBuffer(boolean z) {
        int capacity = this.buffer.capacity();
        if (capacity == this.maxCapacity) {
            return false;
        }
        int min = Math.min(this.maxCapacity, capacity * 2);
        ByteBuffer allocate = ByteBuffer.allocate(min);
        byte[] array = allocate.array();
        if (z) {
            System.arraycopy(this.bytes, 0, array, 0, this.bytes.length);
        }
        this.buffer = allocate;
        this.bytes = array;
        if (Log.DEBUG) {
            Log.debug("kryo", "Resized ObjectBuffer to: " + min);
        }
        return true;
    }
}
