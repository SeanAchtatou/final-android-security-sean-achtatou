package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public class CharSerializer extends Serializer {
    public Character readObjectData(ByteBuffer byteBuffer, Class cls) {
        char c = byteBuffer.getChar();
        if (Log.TRACE) {
            Log.trace("kryo", "Read char: " + c);
        }
        return Character.valueOf(c);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        byteBuffer.putChar(((Character) obj).charValue());
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote char: " + obj);
        }
    }
}
