package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.NotNull;
import com.esotericsoftware.kryo.Optional;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.util.Util;
import com.esotericsoftware.minlog.Log;
import com.esotericsoftware.reflectasm.FieldAccess;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;

public class FieldSerializer extends Serializer {
    Object access;
    private CachedField[] fields;
    private boolean fieldsCanBeNull = true;
    private boolean ignoreSyntheticFields = true;
    final Kryo kryo;
    private boolean setFieldsAsAccessible = true;
    final Class type;

    public FieldSerializer(Kryo kryo2, Class cls) {
        this.kryo = kryo2;
        this.type = cls;
        rebuildCachedFields();
    }

    private void rebuildCachedFields() {
        if (this.type.isInterface()) {
            this.fields = new CachedField[0];
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (Class<? super Object> cls = this.type; cls != Object.class; cls = cls.getSuperclass()) {
            Collections.addAll(arrayList, cls.getDeclaredFields());
        }
        ArrayList arrayList2 = new ArrayList();
        PriorityQueue priorityQueue = new PriorityQueue(Math.max(1, arrayList.size()), new Comparator<CachedField>() {
            public int compare(CachedField cachedField, CachedField cachedField2) {
                return cachedField.field.getName().compareTo(cachedField2.field.getName());
            }
        });
        Context context = Kryo.getContext();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Field field = (Field) arrayList.get(i);
            int modifiers = field.getModifiers();
            if (!Modifier.isTransient(modifiers) && !Modifier.isStatic(modifiers) && (!field.isSynthetic() || !this.ignoreSyntheticFields)) {
                if (!field.isAccessible()) {
                    if (this.setFieldsAsAccessible) {
                        try {
                            field.setAccessible(true);
                        } catch (AccessControlException e) {
                        }
                    }
                }
                Optional optional = (Optional) field.getAnnotation(Optional.class);
                if (optional == null || context.get(optional.value()) != null) {
                    Class<?> type2 = field.getType();
                    CachedField cachedField = new CachedField();
                    cachedField.field = field;
                    if (this.fieldsCanBeNull) {
                        cachedField.canBeNull = !type2.isPrimitive() && !field.isAnnotationPresent(NotNull.class);
                    } else {
                        cachedField.canBeNull = false;
                    }
                    if (isFinal(type2)) {
                        cachedField.fieldClass = type2;
                    }
                    priorityQueue.add(cachedField);
                    if (!Modifier.isFinal(modifiers) && Modifier.isPublic(modifiers) && Modifier.isPublic(type2.getModifiers())) {
                        arrayList2.add(cachedField);
                    }
                }
            }
        }
        if (!Util.isAndroid && Modifier.isPublic(this.type.getModifiers()) && !arrayList2.isEmpty()) {
            try {
                this.access = FieldAccess.get(this.type);
                int size2 = arrayList2.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    CachedField cachedField2 = (CachedField) arrayList2.get(i2);
                    cachedField2.accessIndex = ((FieldAccess) this.access).getIndex(cachedField2.field.getName());
                }
            } catch (RuntimeException e2) {
            }
        }
        int size3 = priorityQueue.size();
        this.fields = new CachedField[size3];
        for (int i3 = 0; i3 < size3; i3++) {
            this.fields[i3] = (CachedField) priorityQueue.poll();
        }
    }

    public void setFieldsCanBeNull(boolean z) {
        this.fieldsCanBeNull = z;
        rebuildCachedFields();
    }

    public void setFieldsAsAccessible(boolean z) {
        this.setFieldsAsAccessible = z;
        rebuildCachedFields();
    }

    public void setIgnoreSyntheticFields(boolean z) {
        this.ignoreSyntheticFields = z;
        rebuildCachedFields();
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        int i = 0;
        int length = this.fields.length;
        while (i < length) {
            CachedField cachedField = this.fields[i];
            try {
                if (Log.TRACE) {
                    Log.trace("kryo", "Writing field: " + cachedField + " (" + obj.getClass().getName() + ")");
                }
                Object obj2 = cachedField.get(obj);
                Serializer serializer = cachedField.serializer;
                if (cachedField.fieldClass != null) {
                    if (serializer == null) {
                        serializer = this.kryo.getRegisteredClass(cachedField.fieldClass).getSerializer();
                        cachedField.serializer = serializer;
                    }
                    if (!cachedField.canBeNull) {
                        serializer.writeObjectData(byteBuffer, obj2);
                    } else {
                        serializer.writeObject(byteBuffer, obj2);
                    }
                } else if (obj2 == null) {
                    this.kryo.writeClass(byteBuffer, null);
                } else {
                    Kryo.RegisteredClass writeClass = this.kryo.writeClass(byteBuffer, obj2.getClass());
                    if (serializer == null) {
                        serializer = writeClass.getSerializer();
                    }
                    serializer.writeObjectData(byteBuffer, obj2);
                }
                i++;
            } catch (IllegalAccessException e) {
                throw new SerializationException("Error accessing field: " + cachedField + " (" + obj.getClass().getName() + ")", e);
            } catch (SerializationException e2) {
                e2.addTrace(cachedField + " (" + obj.getClass().getName() + ")");
                throw e2;
            } catch (RuntimeException e3) {
                SerializationException serializationException = new SerializationException(e3);
                serializationException.addTrace(cachedField + " (" + obj.getClass().getName() + ")");
                throw serializationException;
            }
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote object: " + obj);
        }
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        return readObjectData(newInstance(this.kryo, cls), byteBuffer, cls);
    }

    /* access modifiers changed from: protected */
    public <T> T readObjectData(T t, ByteBuffer byteBuffer, Class<T> cls) {
        Object readObject;
        Serializer serializer;
        int i = 0;
        int length = this.fields.length;
        while (i < length) {
            CachedField cachedField = this.fields[i];
            try {
                if (Log.TRACE) {
                    Log.trace("kryo", "Reading field: " + cachedField + " (" + cls.getName() + ")");
                }
                Class cls2 = cachedField.fieldClass;
                Serializer serializer2 = cachedField.serializer;
                if (cls2 == null) {
                    Kryo.RegisteredClass readClass = this.kryo.readClass(byteBuffer);
                    if (readClass == null) {
                        readObject = null;
                    } else {
                        Class type2 = readClass.getType();
                        if (serializer2 == null) {
                            serializer = readClass.getSerializer();
                        } else {
                            serializer = serializer2;
                        }
                        readObject = serializer.readObjectData(byteBuffer, type2);
                    }
                } else {
                    if (serializer2 == null) {
                        serializer2 = this.kryo.getRegisteredClass(cls2).getSerializer();
                        cachedField.serializer = serializer2;
                    }
                    if (!cachedField.canBeNull) {
                        readObject = serializer2.readObjectData(byteBuffer, cls2);
                    } else {
                        readObject = serializer2.readObject(byteBuffer, cls2);
                    }
                }
                cachedField.set(t, readObject);
                i++;
            } catch (IllegalAccessException e) {
                throw new SerializationException("Error accessing field: " + cachedField + " (" + cls.getName() + ")", e);
            } catch (SerializationException e2) {
                e2.addTrace(cachedField + " (" + cls.getName() + ")");
                throw e2;
            } catch (RuntimeException e3) {
                SerializationException serializationException = new SerializationException(e3);
                serializationException.addTrace(cachedField + " (" + cls.getName() + ")");
                throw serializationException;
            }
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Read object: " + ((Object) t));
        }
        return t;
    }

    public CachedField getField(String str) {
        for (CachedField cachedField : this.fields) {
            if (cachedField.field.getName().equals(str)) {
                return cachedField;
            }
        }
        throw new IllegalArgumentException("Field \"" + str + "\" not found on class: " + this.type.getName());
    }

    public void removeField(String str) {
        for (int i = 0; i < this.fields.length; i++) {
            if (this.fields[i].field.getName().equals(str)) {
                CachedField[] cachedFieldArr = new CachedField[(this.fields.length - 1)];
                System.arraycopy(this.fields, 0, cachedFieldArr, 0, i);
                System.arraycopy(this.fields, i + 1, cachedFieldArr, i, cachedFieldArr.length - i);
                this.fields = cachedFieldArr;
                return;
            }
        }
        throw new IllegalArgumentException("Field \"" + str + "\" not found on class: " + this.type.getName());
    }

    public class CachedField {
        int accessIndex = -1;
        boolean canBeNull;
        Field field;
        Class fieldClass;
        Serializer serializer;

        public CachedField() {
        }

        public void setClass(Class cls) {
            this.fieldClass = cls;
            this.serializer = null;
        }

        public void setClass(Class cls, Serializer serializer2) {
            this.fieldClass = cls;
            this.serializer = serializer2;
        }

        public void setCanBeNull(boolean z) {
            this.canBeNull = z;
        }

        public String toString() {
            return this.field.getName();
        }

        /* access modifiers changed from: package-private */
        public Object get(Object obj) throws IllegalAccessException {
            if (this.accessIndex != -1) {
                return ((FieldAccess) FieldSerializer.this.access).get(obj, this.accessIndex);
            }
            return this.field.get(obj);
        }

        /* access modifiers changed from: package-private */
        public void set(Object obj, Object obj2) throws IllegalAccessException {
            if (this.accessIndex != -1) {
                ((FieldAccess) FieldSerializer.this.access).set(obj, this.accessIndex, obj2);
            } else {
                this.field.set(obj, obj2);
            }
        }
    }
}
