package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Kryo;
import java.nio.ByteBuffer;

public class ClassSerializer extends SimpleSerializer<Class> {
    private final Kryo kryo;

    public ClassSerializer(Kryo kryo2) {
        this.kryo = kryo2;
    }

    public void write(ByteBuffer byteBuffer, Class cls) {
        this.kryo.writeClass(byteBuffer, cls);
    }

    public Class read(ByteBuffer byteBuffer) {
        return this.kryo.readClass(byteBuffer).getType();
    }
}
