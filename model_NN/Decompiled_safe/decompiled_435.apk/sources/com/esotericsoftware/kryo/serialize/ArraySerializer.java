package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.lang.reflect.Array;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;

public class ArraySerializer extends Serializer {
    private int[] dimensions;
    private boolean elementsAreSameType;
    private boolean elementsCanBeNull = true;
    private Integer fixedDimensionCount;
    private final Kryo kryo;

    public ArraySerializer(Kryo kryo2) {
        this.kryo = kryo2;
    }

    public void setDimensionCount(Integer num) {
        this.fixedDimensionCount = num;
    }

    public void setLength(int i) {
        this.dimensions = new int[]{i};
    }

    public void setLengths(int[] iArr) {
        this.dimensions = iArr;
    }

    public void setElementsCanBeNull(boolean z) {
        this.elementsCanBeNull = z;
    }

    public void setElementsAreSameType(boolean z) {
        this.elementsAreSameType = z;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        Serializer serializer;
        int[] iArr = this.dimensions;
        if (iArr == null) {
            iArr = getDimensions(obj);
            if (this.fixedDimensionCount == null) {
                ByteSerializer.putUnsigned(byteBuffer, iArr.length);
            }
            for (int put : iArr) {
                IntSerializer.put(byteBuffer, put, true);
            }
        }
        int[] iArr2 = iArr;
        Class elementClass = getElementClass(obj.getClass());
        boolean z = this.elementsCanBeNull && !elementClass.isPrimitive();
        if (this.elementsAreSameType || Modifier.isFinal(elementClass.getModifiers())) {
            serializer = this.kryo.getRegisteredClass(elementClass).getSerializer();
        } else {
            serializer = null;
        }
        writeArray(byteBuffer, obj, serializer, 0, iArr2.length, z);
        if (Log.TRACE) {
            StringBuilder sb = new StringBuilder(16);
            for (int append : iArr2) {
                sb.append('[');
                sb.append(append);
                sb.append(']');
            }
            Log.trace("kryo", "Wrote array: " + elementClass.getName() + ((Object) sb));
        }
    }

    private void writeArray(ByteBuffer byteBuffer, Object obj, Serializer serializer, int i, int i2, boolean z) {
        int length = Array.getLength(obj);
        if (i > 0) {
            IntSerializer.put(byteBuffer, length, true);
        }
        boolean z2 = i < i2 - 1;
        for (int i3 = 0; i3 < length; i3++) {
            Object obj2 = Array.get(obj, i3);
            if (z2) {
                if (obj2 != null) {
                    writeArray(byteBuffer, obj2, serializer, i + 1, i2, z);
                }
            } else if (serializer == null) {
                this.kryo.writeClassAndObject(byteBuffer, obj2);
            } else if (z) {
                serializer.writeObject(byteBuffer, obj2);
            } else {
                serializer.writeObjectData(byteBuffer, obj2);
            }
        }
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        int length;
        int[] iArr;
        Serializer serializer;
        boolean z = true;
        int[] iArr2 = this.dimensions;
        if (iArr2 == null) {
            int intValue = this.fixedDimensionCount != null ? this.fixedDimensionCount.intValue() : ByteSerializer.getUnsigned(byteBuffer);
            int[] iArr3 = new int[intValue];
            for (int i = 0; i < intValue; i++) {
                iArr3[i] = IntSerializer.get(byteBuffer, true);
            }
            length = intValue;
            iArr = iArr3;
        } else {
            length = iArr2.length;
            iArr = iArr2;
        }
        Class elementClass = getElementClass(cls);
        if (!this.elementsCanBeNull || elementClass.isPrimitive()) {
            z = false;
        }
        if (this.elementsAreSameType || Modifier.isFinal(elementClass.getModifiers())) {
            serializer = this.kryo.getRegisteredClass(elementClass).getSerializer();
        } else {
            serializer = null;
        }
        T newInstance = Array.newInstance(elementClass, iArr);
        readArray(byteBuffer, newInstance, serializer, elementClass, 0, iArr, z);
        if (Log.TRACE) {
            StringBuilder sb = new StringBuilder(16);
            for (int i2 = 0; i2 < length; i2++) {
                sb.append('[');
                sb.append(iArr[i2]);
                sb.append(']');
            }
            Log.trace("kryo", "Read array: " + elementClass.getName() + ((Object) sb));
        }
        return newInstance;
    }

    private void readArray(ByteBuffer byteBuffer, Object obj, Serializer serializer, Class cls, int i, int[] iArr, boolean z) {
        int i2;
        boolean z2 = i < iArr.length - 1;
        if (i == 0) {
            i2 = iArr[0];
        } else {
            i2 = IntSerializer.get(byteBuffer, true);
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (z2) {
                Object obj2 = Array.get(obj, i3);
                if (obj2 != null) {
                    readArray(byteBuffer, obj2, serializer, cls, i + 1, iArr, z);
                }
            } else if (serializer == null) {
                Array.set(obj, i3, this.kryo.readClassAndObject(byteBuffer));
            } else if (z) {
                Array.set(obj, i3, serializer.readObject(byteBuffer, cls));
            } else {
                Array.set(obj, i3, serializer.readObjectData(byteBuffer, cls));
            }
        }
    }

    public static int getDimensionCount(Class cls) {
        int i = 0;
        for (Class<?> componentType = cls.getComponentType(); componentType != null; componentType = componentType.getComponentType()) {
            i++;
        }
        return i;
    }

    public static int[] getDimensions(Object obj) {
        int i = 0;
        for (Class<?> componentType = obj.getClass().getComponentType(); componentType != null; componentType = componentType.getComponentType()) {
            i++;
        }
        int[] iArr = new int[i];
        iArr[0] = Array.getLength(obj);
        if (i > 1) {
            collectDimensions(obj, 1, iArr);
        }
        return iArr;
    }

    private static void collectDimensions(Object obj, int i, int[] iArr) {
        boolean z;
        if (i < iArr.length - 1) {
            z = true;
        } else {
            z = false;
        }
        int length = Array.getLength(obj);
        for (int i2 = 0; i2 < length; i2++) {
            Object obj2 = Array.get(obj, i2);
            if (obj2 != null) {
                iArr[i] = Math.max(iArr[i], Array.getLength(obj2));
                if (z) {
                    collectDimensions(obj2, i + 1, iArr);
                }
            }
        }
    }

    public static Class getElementClass(Class cls) {
        Class cls2 = cls;
        while (cls2.getComponentType() != null) {
            cls2 = cls2.getComponentType();
        }
        return cls2;
    }
}
