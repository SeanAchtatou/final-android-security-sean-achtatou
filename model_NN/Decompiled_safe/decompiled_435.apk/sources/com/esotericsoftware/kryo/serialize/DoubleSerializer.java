package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public class DoubleSerializer extends Serializer {
    private final boolean optimizePositive;
    private final double precision;

    public DoubleSerializer() {
        this.precision = 0.0d;
        this.optimizePositive = false;
    }

    public DoubleSerializer(double d, boolean z) {
        this.precision = d;
        this.optimizePositive = z;
    }

    public Double readObjectData(ByteBuffer byteBuffer, Class cls) {
        double d;
        if (this.precision == 0.0d) {
            d = byteBuffer.getDouble();
        } else {
            d = ((double) LongSerializer.get(byteBuffer, this.optimizePositive)) / this.precision;
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Read double: " + d);
        }
        return Double.valueOf(d);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        if (this.precision == 0.0d) {
            byteBuffer.putDouble(((Double) obj).doubleValue());
        } else {
            LongSerializer.put(byteBuffer, (long) (((Double) obj).doubleValue() * this.precision), this.optimizePositive);
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote double: " + obj);
        }
    }
}
