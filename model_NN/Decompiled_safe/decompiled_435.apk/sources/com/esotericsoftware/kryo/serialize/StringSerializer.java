package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public class StringSerializer extends Serializer {
    public String readObjectData(ByteBuffer byteBuffer, Class cls) {
        String str = get(byteBuffer);
        if (Log.TRACE) {
            Log.trace("kryo", "Read string: " + str);
        }
        return str;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        put(byteBuffer, (String) obj);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote string: " + obj);
        }
    }

    public static void put(ByteBuffer byteBuffer, String str) {
        int length = str.length();
        IntSerializer.put(byteBuffer, length, true);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt <= 127) {
                byteBuffer.put((byte) charAt);
            } else if (charAt > 2047) {
                byteBuffer.put((byte) (((charAt >> 12) & 15) | 224));
                byteBuffer.put((byte) (((charAt >> 6) & 63) | 128));
                byteBuffer.put((byte) (((charAt >> 0) & 63) | 128));
            } else {
                byteBuffer.put((byte) (((charAt >> 6) & 31) | Opcodes.CHECKCAST));
                byteBuffer.put((byte) (((charAt >> 0) & 63) | 128));
            }
        }
    }

    public static String get(ByteBuffer byteBuffer) {
        int i = IntSerializer.get(byteBuffer, true);
        char[] charArray = Kryo.getContext().getCharArray(i);
        int i2 = 0;
        while (i2 < i) {
            byte b = byteBuffer.get() & 255;
            switch (b >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    charArray[i2] = (char) b;
                    i2++;
                    break;
                case 12:
                case 13:
                    charArray[i2] = (char) (((b & 31) << 6) | (byteBuffer.get() & 63));
                    i2++;
                    break;
                case 14:
                    charArray[i2] = (char) (((b & 15) << 12) | ((byteBuffer.get() & 63) << 6) | ((byteBuffer.get() & 63) << 0));
                    i2++;
                    break;
            }
        }
        return new String(charArray, 0, i);
    }
}
