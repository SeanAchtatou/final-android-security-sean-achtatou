package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import java.nio.ByteBuffer;

public abstract class SimpleSerializer<T> extends Serializer {
    public abstract T read(ByteBuffer byteBuffer);

    public abstract void write(ByteBuffer byteBuffer, Object obj);

    public <E> E readObjectData(ByteBuffer byteBuffer, Class<E> cls) {
        return read(byteBuffer);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        write(byteBuffer, obj);
    }
}
