package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import com.esotericsoftware.reflectasm.MethodAccess;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class BeanSerializer extends Serializer {
    static final Object[] noArgs = new Object[0];
    Object access;
    private final Kryo kryo;
    private CachedProperty[] properties;

    public BeanSerializer(Kryo kryo2, Class cls) {
        this.kryo = kryo2;
        try {
            PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(cls).getPropertyDescriptors();
            Arrays.sort(propertyDescriptors, new Comparator<PropertyDescriptor>() {
                public int compare(PropertyDescriptor propertyDescriptor, PropertyDescriptor propertyDescriptor2) {
                    return propertyDescriptor.getName().compareTo(propertyDescriptor2.getName());
                }
            });
            ArrayList arrayList = new ArrayList(propertyDescriptors.length);
            for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                String name = propertyDescriptor.getName();
                if (!name.equals("class")) {
                    Method readMethod = propertyDescriptor.getReadMethod();
                    Method writeMethod = propertyDescriptor.getWriteMethod();
                    if (!(readMethod == null || writeMethod == null)) {
                        Serializer serializer = null;
                        Class<?> returnType = readMethod.getReturnType();
                        serializer = isFinal(returnType) ? kryo2.getRegisteredClass(returnType).getSerializer() : serializer;
                        CachedProperty cachedProperty = new CachedProperty();
                        cachedProperty.name = name;
                        cachedProperty.getMethod = readMethod;
                        cachedProperty.setMethod = writeMethod;
                        cachedProperty.serializer = serializer;
                        cachedProperty.setMethodType = writeMethod.getParameterTypes()[0];
                        arrayList.add(cachedProperty);
                    }
                }
            }
            this.properties = (CachedProperty[]) arrayList.toArray(new CachedProperty[arrayList.size()]);
            try {
                this.access = MethodAccess.get(cls);
                for (CachedProperty cachedProperty2 : this.properties) {
                    cachedProperty2.getterAccessIndex = ((MethodAccess) this.access).getIndex(cachedProperty2.getMethod.getName());
                    cachedProperty2.setterAccessIndex = ((MethodAccess) this.access).getIndex(cachedProperty2.setMethod.getName());
                }
            } catch (Throwable th) {
            }
        } catch (IntrospectionException e) {
            throw new SerializationException("Error getting bean info.", e);
        }
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        Class<?> cls = obj.getClass();
        int i = 0;
        int length = this.properties.length;
        while (i < length) {
            CachedProperty cachedProperty = this.properties[i];
            try {
                if (Log.TRACE) {
                    Log.trace("kryo", "Writing property: " + cachedProperty + " (" + cls.getName() + ")");
                }
                Object obj2 = cachedProperty.get(obj);
                Serializer serializer = cachedProperty.serializer;
                if (serializer != null) {
                    serializer.writeObject(byteBuffer, obj2);
                } else {
                    this.kryo.writeClassAndObject(byteBuffer, obj2);
                }
                i++;
            } catch (IllegalAccessException e) {
                throw new SerializationException("Error accessing getter method: " + cachedProperty + " (" + cls.getName() + ")", e);
            } catch (InvocationTargetException e2) {
                throw new SerializationException("Error invoking getter method: " + cachedProperty + " (" + cls.getName() + ")", e2);
            } catch (SerializationException e3) {
                e3.addTrace(cachedProperty + " (" + cls.getName() + ")");
                throw e3;
            } catch (RuntimeException e4) {
                SerializationException serializationException = new SerializationException(e4);
                serializationException.addTrace(cachedProperty + " (" + cls.getName() + ")");
                throw serializationException;
            }
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote bean: " + obj);
        }
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        return readObjectData(newInstance(this.kryo, cls), byteBuffer, cls);
    }

    /* access modifiers changed from: protected */
    public <T> T readObjectData(T t, ByteBuffer byteBuffer, Class<T> cls) {
        Object readClassAndObject;
        int i = 0;
        int length = this.properties.length;
        while (i < length) {
            CachedProperty cachedProperty = this.properties[i];
            try {
                if (Log.TRACE) {
                    Log.trace("kryo", "Reading property: " + cachedProperty + " (" + t.getClass() + ")");
                }
                Serializer serializer = cachedProperty.serializer;
                if (serializer != null) {
                    readClassAndObject = serializer.readObject(byteBuffer, cachedProperty.setMethodType);
                } else {
                    readClassAndObject = this.kryo.readClassAndObject(byteBuffer);
                }
                cachedProperty.set(t, readClassAndObject);
                i++;
            } catch (IllegalAccessException e) {
                throw new SerializationException("Error accessing setter method: " + cachedProperty + " (" + cls.getName() + ")", e);
            } catch (InvocationTargetException e2) {
                throw new SerializationException("Error invoking setter method: " + cachedProperty + " (" + cls.getName() + ")", e2);
            } catch (SerializationException e3) {
                e3.addTrace(cachedProperty + " (" + cls.getName() + ")");
                throw e3;
            } catch (RuntimeException e4) {
                SerializationException serializationException = new SerializationException(e4);
                serializationException.addTrace(cachedProperty + " (" + cls.getName() + ")");
                throw serializationException;
            }
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Read bean: " + ((Object) t));
        }
        return t;
    }

    class CachedProperty {
        Method getMethod;
        int getterAccessIndex;
        String name;
        Serializer serializer;
        Method setMethod;
        Class setMethodType;
        int setterAccessIndex;

        CachedProperty() {
        }

        public String toString() {
            return this.name;
        }

        /* access modifiers changed from: package-private */
        public Object get(Object obj) throws IllegalAccessException, InvocationTargetException {
            if (BeanSerializer.this.access != null) {
                return ((MethodAccess) BeanSerializer.this.access).invoke(obj, this.getterAccessIndex, new Object[0]);
            }
            return this.getMethod.invoke(obj, BeanSerializer.noArgs);
        }

        /* access modifiers changed from: package-private */
        public void set(Object obj, Object obj2) throws IllegalAccessException, InvocationTargetException {
            if (BeanSerializer.this.access != null) {
                ((MethodAccess) BeanSerializer.this.access).invoke(obj, this.setterAccessIndex, obj2);
                return;
            }
            this.setMethod.invoke(obj, obj2);
        }
    }
}
