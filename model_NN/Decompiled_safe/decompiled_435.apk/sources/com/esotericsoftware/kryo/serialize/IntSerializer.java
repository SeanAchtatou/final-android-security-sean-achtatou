package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public class IntSerializer extends Serializer {
    private boolean optimizePositive = true;

    public IntSerializer() {
    }

    public IntSerializer(boolean z) {
        this.optimizePositive = z;
    }

    public Integer readObjectData(ByteBuffer byteBuffer, Class cls) {
        int i = get(byteBuffer, this.optimizePositive);
        if (Log.TRACE) {
            Log.trace("kryo", "Read int: " + i);
        }
        return Integer.valueOf(i);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        put(byteBuffer, ((Integer) obj).intValue(), this.optimizePositive);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote int: " + obj);
        }
    }

    public static int put(ByteBuffer byteBuffer, int i, boolean z) {
        int i2;
        if (!z) {
            i2 = (i << 1) ^ (i >> 31);
        } else {
            i2 = i;
        }
        if ((i2 & -128) == 0) {
            byteBuffer.put((byte) i2);
            return 1;
        }
        byteBuffer.put((byte) ((i2 & Opcodes.LAND) | 128));
        int i3 = i2 >>> 7;
        if ((i3 & -128) == 0) {
            byteBuffer.put((byte) i3);
            return 2;
        }
        byteBuffer.put((byte) ((i3 & Opcodes.LAND) | 128));
        int i4 = i3 >>> 7;
        if ((i4 & -128) == 0) {
            byteBuffer.put((byte) i4);
            return 3;
        }
        byteBuffer.put((byte) ((i4 & Opcodes.LAND) | 128));
        int i5 = i4 >>> 7;
        if ((i5 & -128) == 0) {
            byteBuffer.put((byte) i5);
            return 4;
        }
        byteBuffer.put((byte) ((i5 & Opcodes.LAND) | 128));
        byteBuffer.put((byte) (i5 >>> 7));
        return 5;
    }

    public static int get(ByteBuffer byteBuffer, boolean z) {
        int i = 0;
        int i2 = 0;
        while (i2 < 32) {
            byte b = byteBuffer.get();
            i |= (b & Byte.MAX_VALUE) << i2;
            if ((b & 128) != 0) {
                i2 += 7;
            } else if (z) {
                return i;
            } else {
                return (-(i & 1)) ^ (i >>> 1);
            }
        }
        throw new SerializationException("Malformed integer.");
    }

    /* JADX INFO: finally extract failed */
    public static boolean canRead(ByteBuffer byteBuffer, boolean z) {
        int position = byteBuffer.position();
        try {
            int remaining = byteBuffer.remaining();
            int i = 0;
            while (i < 32 && remaining > 0) {
                if ((byteBuffer.get() & 128) == 0) {
                    byteBuffer.position(position);
                    return true;
                }
                i += 7;
                remaining--;
            }
            byteBuffer.position(position);
            return false;
        } catch (Throwable th) {
            byteBuffer.position(position);
            throw th;
        }
    }

    public static int length(int i, boolean z) {
        int i2;
        if (!z) {
            i2 = (i << 1) ^ (i >> 31);
        } else {
            i2 = i;
        }
        if ((i2 & -128) == 0) {
            return 1;
        }
        int i3 = i2 >>> 7;
        if ((i3 & -128) == 0) {
            return 2;
        }
        int i4 = i3 >>> 7;
        if ((i4 & -128) == 0) {
            return 3;
        }
        int i5 = i4 >>> 7;
        if ((i5 & -128) == 0) {
            return 4;
        }
        int i6 = i5 >>> 7;
        return 5;
    }
}
