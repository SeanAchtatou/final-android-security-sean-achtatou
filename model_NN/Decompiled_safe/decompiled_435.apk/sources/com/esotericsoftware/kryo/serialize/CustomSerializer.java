package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.CustomSerialization;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import java.nio.ByteBuffer;

public class CustomSerializer extends Serializer {
    private final Kryo kryo;

    public CustomSerializer(Kryo kryo2) {
        this.kryo = kryo2;
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        T newInstance = newInstance(this.kryo, cls);
        ((CustomSerialization) newInstance).readObjectData(this.kryo, byteBuffer);
        return newInstance;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        ((CustomSerialization) obj).writeObjectData(this.kryo, byteBuffer);
    }
}
