package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import java.util.Date;

public class DateSerializer extends Serializer {
    private static LongSerializer longSerializer = new LongSerializer(true);

    public Date readObjectData(ByteBuffer byteBuffer, Class cls) {
        Date date = new Date(LongSerializer.get(byteBuffer, true));
        if (Log.TRACE) {
            Log.trace("kryo", "Read date: " + date);
        }
        return date;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        LongSerializer.put(byteBuffer, ((Date) obj).getTime(), true);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote date: " + obj);
        }
    }
}
