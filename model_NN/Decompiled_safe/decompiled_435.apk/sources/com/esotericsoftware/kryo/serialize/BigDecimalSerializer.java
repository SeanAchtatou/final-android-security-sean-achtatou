package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;

public class BigDecimalSerializer extends Serializer {
    private BigIntegerSerializer bigIntegerSerializer = new BigIntegerSerializer();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.esotericsoftware.kryo.serialize.BigIntegerSerializer.readObjectData(java.nio.ByteBuffer, java.lang.Class):java.lang.Object
     arg types: [java.nio.ByteBuffer, ?[OBJECT, ARRAY]]
     candidates:
      com.esotericsoftware.kryo.serialize.BigIntegerSerializer.readObjectData(java.nio.ByteBuffer, java.lang.Class):java.math.BigInteger
      com.esotericsoftware.kryo.serialize.BigIntegerSerializer.readObjectData(java.nio.ByteBuffer, java.lang.Class):java.lang.Object */
    public BigDecimal readObjectData(ByteBuffer byteBuffer, Class cls) {
        BigDecimal bigDecimal = new BigDecimal((BigInteger) this.bigIntegerSerializer.readObjectData(byteBuffer, (Class) null), IntSerializer.get(byteBuffer, false));
        if (Log.TRACE) {
            Log.trace("kryo", "Read BigDecimal: " + bigDecimal);
        }
        return bigDecimal;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        BigDecimal bigDecimal = (BigDecimal) obj;
        this.bigIntegerSerializer.writeObjectData(byteBuffer, bigDecimal.unscaledValue());
        IntSerializer.put(byteBuffer, bigDecimal.scale(), false);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote BigDecimal: " + bigDecimal);
        }
    }
}
