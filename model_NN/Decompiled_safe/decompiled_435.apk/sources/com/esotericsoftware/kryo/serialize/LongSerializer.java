package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public class LongSerializer extends Serializer {
    private boolean optimizePositive;

    public LongSerializer() {
    }

    public LongSerializer(boolean z) {
        this.optimizePositive = z;
    }

    public Long readObjectData(ByteBuffer byteBuffer, Class cls) {
        long j = get(byteBuffer, this.optimizePositive);
        if (Log.TRACE) {
            Log.trace("kryo", "Read long: " + j);
        }
        return Long.valueOf(j);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        put(byteBuffer, ((Long) obj).longValue(), this.optimizePositive);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote long: " + obj);
        }
    }

    public static long put(ByteBuffer byteBuffer, long j, boolean z) {
        long j2;
        if (!z) {
            j2 = (j << 1) ^ (j >> 63);
        } else {
            j2 = j;
        }
        if ((j2 & -128) == 0) {
            byteBuffer.put((byte) ((int) j2));
            return 1;
        }
        byteBuffer.put((byte) ((((int) j2) & Opcodes.LAND) | 128));
        long j3 = j2 >>> 7;
        if ((j3 & -128) == 0) {
            byteBuffer.put((byte) ((int) j3));
            return 2;
        }
        byteBuffer.put((byte) ((((int) j3) & Opcodes.LAND) | 128));
        long j4 = j3 >>> 7;
        if ((j4 & -128) == 0) {
            byteBuffer.put((byte) ((int) j4));
            return 3;
        }
        byteBuffer.put((byte) ((((int) j4) & Opcodes.LAND) | 128));
        long j5 = j4 >>> 7;
        if ((j5 & -128) == 0) {
            byteBuffer.put((byte) ((int) j5));
            return 4;
        }
        byteBuffer.put((byte) ((((int) j5) & Opcodes.LAND) | 128));
        long j6 = j5 >>> 7;
        if ((j6 & -128) == 0) {
            byteBuffer.put((byte) ((int) j6));
            return 5;
        }
        byteBuffer.put((byte) ((((int) j6) & Opcodes.LAND) | 128));
        long j7 = j6 >>> 7;
        if ((j7 & -128) == 0) {
            byteBuffer.put((byte) ((int) j7));
            return 6;
        }
        byteBuffer.put((byte) ((((int) j7) & Opcodes.LAND) | 128));
        long j8 = j7 >>> 7;
        if ((j8 & -128) == 0) {
            byteBuffer.put((byte) ((int) j8));
            return 7;
        }
        byteBuffer.put((byte) ((((int) j8) & Opcodes.LAND) | 128));
        long j9 = j8 >>> 7;
        if ((j9 & -128) == 0) {
            byteBuffer.put((byte) ((int) j9));
            return 8;
        }
        byteBuffer.put((byte) ((((int) j9) & Opcodes.LAND) | 128));
        long j10 = j9 >>> 7;
        if ((j10 & -128) == 0) {
            byteBuffer.put((byte) ((int) j10));
            return 9;
        }
        byteBuffer.put((byte) ((((int) j10) & Opcodes.LAND) | 128));
        byteBuffer.put((byte) ((int) (j10 >>> 7)));
        return 10;
    }

    public static long get(ByteBuffer byteBuffer, boolean z) {
        long j = 0;
        int i = 0;
        while (i < 64) {
            byte b = byteBuffer.get();
            j |= ((long) (b & Byte.MAX_VALUE)) << i;
            if ((b & 128) != 0) {
                i += 7;
            } else if (z) {
                return j;
            } else {
                return (-(j & 1)) ^ (j >>> 1);
            }
        }
        throw new SerializationException("Malformed long.");
    }
}
