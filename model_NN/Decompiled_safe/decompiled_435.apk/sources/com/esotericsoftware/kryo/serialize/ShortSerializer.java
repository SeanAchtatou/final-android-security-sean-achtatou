package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public class ShortSerializer extends Serializer {
    private static final byte SHORT = Byte.MIN_VALUE;
    private static final byte SHORT_POSITIVE = -1;
    private boolean optimizePositive = true;

    public ShortSerializer() {
    }

    public ShortSerializer(Boolean bool) {
        this.optimizePositive = bool.booleanValue();
    }

    public Short readObjectData(ByteBuffer byteBuffer, Class cls) {
        short s = get(byteBuffer, this.optimizePositive);
        if (Log.TRACE) {
            Log.trace("kryo", "Read short: " + ((int) s));
        }
        return Short.valueOf(s);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        put(byteBuffer, ((Short) obj).shortValue(), this.optimizePositive);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote short: " + obj);
        }
    }

    public static short put(ByteBuffer byteBuffer, short s, boolean z) {
        if (z) {
            if (s < 0 || s > 254) {
                byteBuffer.put((byte) SHORT_POSITIVE);
                byteBuffer.putShort(s);
            } else {
                byteBuffer.put((byte) s);
                return 1;
            }
        } else if (s < -127 || s > 127) {
            byteBuffer.put((byte) SHORT);
            byteBuffer.putShort(s);
        } else {
            byteBuffer.put((byte) s);
            return 1;
        }
        return 3;
    }

    public static short get(ByteBuffer byteBuffer, boolean z) {
        byte b = byteBuffer.get();
        if (z) {
            if (b == -1) {
                return byteBuffer.getShort();
            }
            if (b < 0) {
                return (short) (b + 256);
            }
        } else if (b == Byte.MIN_VALUE) {
            return byteBuffer.getShort();
        }
        return (short) b;
    }
}
