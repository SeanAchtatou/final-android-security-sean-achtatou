package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import java.util.Map;

public class MapSerializer extends Serializer {
    private Class keyClass;
    private Serializer keySerializer;
    private boolean keysCanBeNull = true;
    private final Kryo kryo;
    private Class valueClass;
    private Serializer valueSerializer;
    private boolean valuesCanBeNull = true;

    public MapSerializer(Kryo kryo2) {
        this.kryo = kryo2;
    }

    public void setKeysCanBeNull(boolean z) {
        this.keysCanBeNull = z;
    }

    public void setKeyClass(Class cls) {
        this.keyClass = cls;
        this.keySerializer = cls == null ? null : this.kryo.getRegisteredClass(cls).getSerializer();
    }

    public void setKeyClass(Class cls, Serializer serializer) {
        this.keyClass = cls;
        this.keySerializer = serializer;
    }

    public void setValueClass(Class cls) {
        this.valueClass = cls;
        this.valueSerializer = cls == null ? null : this.kryo.getRegisteredClass(cls).getSerializer();
    }

    public void setValueClass(Class cls, Serializer serializer) {
        this.valueClass = cls;
        this.valueSerializer = serializer;
    }

    public void setValuesCanBeNull(boolean z) {
        this.valuesCanBeNull = z;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        Map map = (Map) obj;
        int size = map.size();
        IntSerializer.put(byteBuffer, size, true);
        if (size != 0) {
            for (Map.Entry entry : map.entrySet()) {
                if (this.keySerializer == null) {
                    this.kryo.writeClassAndObject(byteBuffer, entry.getKey());
                } else if (this.keysCanBeNull) {
                    this.keySerializer.writeObject(byteBuffer, entry.getKey());
                } else {
                    this.keySerializer.writeObjectData(byteBuffer, entry.getKey());
                }
                if (this.valueSerializer == null) {
                    this.kryo.writeClassAndObject(byteBuffer, entry.getValue());
                } else if (this.valuesCanBeNull) {
                    this.valueSerializer.writeObject(byteBuffer, entry.getValue());
                } else {
                    this.valueSerializer.writeObjectData(byteBuffer, entry.getValue());
                }
            }
            if (Log.TRACE) {
                Log.trace("kryo", "Wrote map: " + obj);
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public <T> T readObjectData(java.nio.ByteBuffer r7, java.lang.Class<T> r8) {
        /*
            r6 = this;
            com.esotericsoftware.kryo.Kryo r0 = r6.kryo
            java.lang.Object r0 = r6.newInstance(r0, r8)
            java.util.Map r0 = (java.util.Map) r0
            r1 = 1
            int r1 = com.esotericsoftware.kryo.serialize.IntSerializer.get(r7, r1)
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            return r0
        L_0x0010:
            r2 = 0
        L_0x0011:
            if (r2 >= r1) goto L_0x0059
            com.esotericsoftware.kryo.Serializer r3 = r6.keySerializer
            if (r3 == 0) goto L_0x0042
            boolean r3 = r6.keysCanBeNull
            if (r3 == 0) goto L_0x0039
            com.esotericsoftware.kryo.Serializer r3 = r6.keySerializer
            java.lang.Class r4 = r6.keyClass
            java.lang.Object r3 = r3.readObject(r7, r4)
        L_0x0023:
            com.esotericsoftware.kryo.Serializer r4 = r6.valueSerializer
            if (r4 == 0) goto L_0x0052
            boolean r4 = r6.valuesCanBeNull
            if (r4 == 0) goto L_0x0049
            com.esotericsoftware.kryo.Serializer r4 = r6.valueSerializer
            java.lang.Class r5 = r6.valueClass
            java.lang.Object r4 = r4.readObject(r7, r5)
        L_0x0033:
            r0.put(r3, r4)
            int r2 = r2 + 1
            goto L_0x0011
        L_0x0039:
            com.esotericsoftware.kryo.Serializer r3 = r6.keySerializer
            java.lang.Class r4 = r6.keyClass
            java.lang.Object r3 = r3.readObjectData(r7, r4)
            goto L_0x0023
        L_0x0042:
            com.esotericsoftware.kryo.Kryo r3 = r6.kryo
            java.lang.Object r3 = r3.readClassAndObject(r7)
            goto L_0x0023
        L_0x0049:
            com.esotericsoftware.kryo.Serializer r4 = r6.valueSerializer
            java.lang.Class r5 = r6.valueClass
            java.lang.Object r4 = r4.readObjectData(r7, r5)
            goto L_0x0033
        L_0x0052:
            com.esotericsoftware.kryo.Kryo r4 = r6.kryo
            java.lang.Object r4 = r4.readClassAndObject(r7)
            goto L_0x0033
        L_0x0059:
            boolean r1 = com.esotericsoftware.minlog.Log.TRACE
            if (r1 == 0) goto L_0x000f
            java.lang.String r1 = "kryo"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Read map: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.esotericsoftware.minlog.Log.trace(r1, r2)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryo.serialize.MapSerializer.readObjectData(java.nio.ByteBuffer, java.lang.Class):java.lang.Object");
    }
}
