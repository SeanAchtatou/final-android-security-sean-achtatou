package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.math.BigInteger;
import java.nio.ByteBuffer;

public class BigIntegerSerializer extends Serializer {
    public BigInteger readObjectData(ByteBuffer byteBuffer, Class cls) {
        int i = IntSerializer.get(byteBuffer, true);
        byte[] bArr = new byte[i];
        byteBuffer.get(bArr, 0, i);
        BigInteger bigInteger = new BigInteger(bArr);
        if (Log.TRACE) {
            Log.trace("kryo", "Read BigInteger: " + bigInteger);
        }
        return bigInteger;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        BigInteger bigInteger = (BigInteger) obj;
        byte[] byteArray = bigInteger.toByteArray();
        IntSerializer.put(byteBuffer, byteArray.length, true);
        byteBuffer.put(byteArray);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote BigInteger: " + bigInteger);
        }
    }
}
