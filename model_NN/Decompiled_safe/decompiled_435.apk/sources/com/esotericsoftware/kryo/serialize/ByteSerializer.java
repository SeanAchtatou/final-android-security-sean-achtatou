package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public class ByteSerializer extends Serializer {
    public Byte readObjectData(ByteBuffer byteBuffer, Class cls) {
        byte b = byteBuffer.get();
        if (Log.TRACE) {
            Log.trace("kryo", "Read byte: " + ((int) b));
        }
        return Byte.valueOf(b);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        byteBuffer.put(((Byte) obj).byteValue());
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote byte: " + obj);
        }
    }

    public static void putUnsigned(ByteBuffer byteBuffer, int i) {
        if (i < 0) {
            throw new IllegalArgumentException("value cannot be less than zero: " + i);
        }
        byteBuffer.put((byte) i);
    }

    public static int getUnsigned(ByteBuffer byteBuffer) {
        byte b = byteBuffer.get();
        if (b < 0) {
            return b + 256;
        }
        return b;
    }
}
