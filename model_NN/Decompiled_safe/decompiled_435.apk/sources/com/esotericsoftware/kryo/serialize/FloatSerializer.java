package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public class FloatSerializer extends Serializer {
    private final boolean optimizePositive;
    private final float precision;

    public FloatSerializer() {
        this.precision = 0.0f;
        this.optimizePositive = false;
    }

    public FloatSerializer(float f, boolean z) {
        this.precision = f;
        this.optimizePositive = z;
    }

    public Float readObjectData(ByteBuffer byteBuffer, Class cls) {
        float f;
        if (this.precision == 0.0f) {
            f = byteBuffer.getFloat();
        } else {
            f = ((float) IntSerializer.get(byteBuffer, this.optimizePositive)) / this.precision;
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Read float: " + f);
        }
        return Float.valueOf(f);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        if (this.precision == 0.0f) {
            byteBuffer.putFloat(((Float) obj).floatValue());
        } else {
            IntSerializer.put(byteBuffer, (int) (((Float) obj).floatValue() * this.precision), this.optimizePositive);
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote float: " + obj);
        }
    }
}
