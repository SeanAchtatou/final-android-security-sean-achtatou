package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public class SerializableSerializer extends Serializer {
    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        T t = get(byteBuffer, cls);
        if (Log.TRACE) {
            Log.trace("kryo", "Read object: " + ((Object) t));
        }
        return t;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        put(byteBuffer, obj);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote object: " + obj);
        }
    }

    public static void put(ByteBuffer byteBuffer, Object obj) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Opcodes.ACC_NATIVE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            objectOutputStream.close();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            IntSerializer.put(byteBuffer, byteArray.length, true);
            byteBuffer.put(byteArray);
        } catch (BufferOverflowException e) {
            throw e;
        } catch (Exception e2) {
            throw new SerializationException("Error during Java serialization.", e2);
        }
    }

    public static <T> T get(ByteBuffer byteBuffer, Class<T> cls) {
        byte[] bArr = new byte[IntSerializer.get(byteBuffer, true)];
        byteBuffer.get(bArr);
        try {
            return new ObjectInputStream(new ByteArrayInputStream(bArr)).readObject();
        } catch (Exception e) {
            throw new SerializationException("Error during Java deserialization.", e);
        }
    }
}
