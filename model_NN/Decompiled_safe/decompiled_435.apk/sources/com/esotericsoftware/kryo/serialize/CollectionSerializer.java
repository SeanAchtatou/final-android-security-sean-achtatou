package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import java.util.Collection;

public class CollectionSerializer extends Serializer {
    private Class elementClass;
    private boolean elementsCanBeNull = true;
    private final Kryo kryo;
    private Integer length;
    private Serializer serializer;

    public CollectionSerializer(Kryo kryo2) {
        this.kryo = kryo2;
    }

    public void setElementsCanBeNull(boolean z) {
        this.elementsCanBeNull = z;
    }

    public void setElementClass(Class cls) {
        this.elementClass = cls;
        this.serializer = cls == null ? null : this.kryo.getRegisteredClass(cls).getSerializer();
    }

    public void setLength(int i) {
        this.length = Integer.valueOf(i);
    }

    public void setElementClass(Class cls, Serializer serializer2) {
        this.elementClass = cls;
        this.serializer = serializer2;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        int size;
        Collection<Object> collection = (Collection) obj;
        if (this.length != null) {
            size = this.length.intValue();
        } else {
            size = collection.size();
            IntSerializer.put(byteBuffer, size, true);
        }
        if (size != 0) {
            if (this.serializer == null) {
                for (Object writeClassAndObject : collection) {
                    this.kryo.writeClassAndObject(byteBuffer, writeClassAndObject);
                }
            } else if (this.elementsCanBeNull) {
                for (Object writeObject : collection) {
                    this.serializer.writeObject(byteBuffer, writeObject);
                }
            } else {
                for (Object writeObjectData : collection) {
                    this.serializer.writeObjectData(byteBuffer, writeObjectData);
                }
            }
            if (Log.TRACE) {
                Log.trace("kryo", "Wrote collection: " + obj);
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public <T> T readObjectData(java.nio.ByteBuffer r6, java.lang.Class<T> r7) {
        /*
            r5 = this;
            r3 = 0
            java.lang.Integer r0 = r5.length
            if (r0 == 0) goto L_0x0018
            java.lang.Integer r0 = r5.length
            int r0 = r0.intValue()
            r1 = r0
        L_0x000c:
            java.lang.Class<java.util.ArrayList> r0 = java.util.ArrayList.class
            if (r7 != r0) goto L_0x001f
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>(r1)
        L_0x0015:
            if (r1 != 0) goto L_0x0028
        L_0x0017:
            return r0
        L_0x0018:
            r0 = 1
            int r0 = com.esotericsoftware.kryo.serialize.IntSerializer.get(r6, r0)
            r1 = r0
            goto L_0x000c
        L_0x001f:
            com.esotericsoftware.kryo.Kryo r0 = r5.kryo
            java.lang.Object r0 = r5.newInstance(r0, r7)
            java.util.Collection r0 = (java.util.Collection) r0
            goto L_0x0015
        L_0x0028:
            com.esotericsoftware.kryo.Serializer r2 = r5.serializer
            if (r2 == 0) goto L_0x0052
            boolean r2 = r5.elementsCanBeNull
            if (r2 == 0) goto L_0x0041
            r2 = r3
        L_0x0031:
            if (r2 >= r1) goto L_0x0061
            com.esotericsoftware.kryo.Serializer r3 = r5.serializer
            java.lang.Class r4 = r5.elementClass
            java.lang.Object r3 = r3.readObject(r6, r4)
            r0.add(r3)
            int r2 = r2 + 1
            goto L_0x0031
        L_0x0041:
            r2 = r3
        L_0x0042:
            if (r2 >= r1) goto L_0x0061
            com.esotericsoftware.kryo.Serializer r3 = r5.serializer
            java.lang.Class r4 = r5.elementClass
            java.lang.Object r3 = r3.readObjectData(r6, r4)
            r0.add(r3)
            int r2 = r2 + 1
            goto L_0x0042
        L_0x0052:
            r2 = r3
        L_0x0053:
            if (r2 >= r1) goto L_0x0061
            com.esotericsoftware.kryo.Kryo r3 = r5.kryo
            java.lang.Object r3 = r3.readClassAndObject(r6)
            r0.add(r3)
            int r2 = r2 + 1
            goto L_0x0053
        L_0x0061:
            boolean r1 = com.esotericsoftware.minlog.Log.TRACE
            if (r1 == 0) goto L_0x0017
            java.lang.String r1 = "kryo"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Read collection: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.esotericsoftware.minlog.Log.trace(r1, r2)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryo.serialize.CollectionSerializer.readObjectData(java.nio.ByteBuffer, java.lang.Class):java.lang.Object");
    }
}
