package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.NotNull;
import com.esotericsoftware.kryo.Optional;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.util.Util;
import com.esotericsoftware.minlog.Log;
import com.esotericsoftware.reflectasm.FieldAccess;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;

public class CompatibleFieldSerializer extends Serializer {
    Object access;
    private CachedField[] fields;
    private boolean fieldsCanBeNull = true;
    final Kryo kryo;
    private boolean setFieldsAsAccessible = true;
    final Class type;

    public CompatibleFieldSerializer(Kryo kryo2, Class cls) {
        this.kryo = kryo2;
        this.type = cls;
        rebuildCachedFields();
    }

    private void rebuildCachedFields() {
        if (this.type.isInterface()) {
            this.fields = new CachedField[0];
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (Class<? super Object> cls = this.type; cls != Object.class; cls = cls.getSuperclass()) {
            Collections.addAll(arrayList, cls.getDeclaredFields());
        }
        ArrayList arrayList2 = new ArrayList();
        PriorityQueue priorityQueue = new PriorityQueue(Math.max(1, arrayList.size()), new Comparator<CachedField>() {
            public int compare(CachedField cachedField, CachedField cachedField2) {
                return cachedField.field.getName().compareTo(cachedField2.field.getName());
            }
        });
        Context context = Kryo.getContext();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Field field = (Field) arrayList.get(i);
            int modifiers = field.getModifiers();
            if (!Modifier.isTransient(modifiers) && !Modifier.isStatic(modifiers) && !field.isSynthetic()) {
                if (!field.isAccessible()) {
                    if (this.setFieldsAsAccessible) {
                        try {
                            field.setAccessible(true);
                        } catch (AccessControlException e) {
                        }
                    }
                }
                Optional optional = (Optional) field.getAnnotation(Optional.class);
                if (optional == null || context.get(optional.value()) != null) {
                    Class<?> type2 = field.getType();
                    CachedField cachedField = new CachedField();
                    cachedField.field = field;
                    if (this.fieldsCanBeNull) {
                        cachedField.canBeNull = !type2.isPrimitive() && !field.isAnnotationPresent(NotNull.class);
                    } else {
                        cachedField.canBeNull = false;
                    }
                    if (isFinal(type2)) {
                        cachedField.fieldClass = type2;
                    }
                    priorityQueue.add(cachedField);
                    if (Modifier.isPublic(modifiers) && Modifier.isPublic(type2.getModifiers())) {
                        arrayList2.add(cachedField);
                    }
                }
            }
        }
        if (!Util.isAndroid && Modifier.isPublic(this.type.getModifiers()) && !arrayList2.isEmpty()) {
            try {
                this.access = FieldAccess.get(this.type);
                int size2 = arrayList2.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    CachedField cachedField2 = (CachedField) arrayList2.get(i2);
                    cachedField2.accessIndex = ((FieldAccess) this.access).getIndex(cachedField2.field.getName());
                }
            } catch (RuntimeException e2) {
            }
        }
        int size3 = priorityQueue.size();
        this.fields = new CachedField[size3];
        for (int i3 = 0; i3 < size3; i3++) {
            this.fields[i3] = (CachedField) priorityQueue.poll();
        }
    }

    public void setFieldsCanBeNull(boolean z) {
        this.fieldsCanBeNull = z;
        rebuildCachedFields();
    }

    public void setFieldsAsAccessible(boolean z) {
        this.setFieldsAsAccessible = z;
        rebuildCachedFields();
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        Context context = Kryo.getContext();
        if (context.getTemp(this, "schemaWritten") == null) {
            context.putTemp(this, "schemaWritten", Boolean.TRUE);
            if (Log.TRACE) {
                Log.trace("kryo", "Writing " + this.fields.length + " field names.");
            }
            IntSerializer.put(byteBuffer, this.fields.length, true);
            for (CachedField cachedField : this.fields) {
                StringSerializer.put(byteBuffer, cachedField.field.getName());
            }
        }
        int length = this.fields.length;
        int i = 0;
        while (i < length) {
            CachedField cachedField2 = this.fields[i];
            try {
                if (Log.TRACE) {
                    Log.trace("kryo", "Writing field: " + cachedField2 + " (" + obj.getClass().getName() + ")");
                }
                Object obj2 = cachedField2.get(obj);
                if (obj2 == null) {
                    this.kryo.writeClass(byteBuffer, null);
                } else {
                    int position = byteBuffer.position();
                    try {
                        byteBuffer.position(position + 1);
                    } catch (IllegalArgumentException e) {
                        new BufferOverflowException();
                    }
                    Serializer serializer = cachedField2.serializer;
                    if (cachedField2.fieldClass == null) {
                        Kryo.RegisteredClass writeClass = this.kryo.writeClass(byteBuffer, obj2.getClass());
                        if (serializer == null) {
                            serializer = writeClass.getSerializer();
                        }
                        serializer.writeObjectData(byteBuffer, obj2);
                    } else {
                        if (serializer == null) {
                            serializer = this.kryo.getRegisteredClass(cachedField2.fieldClass).getSerializer();
                            cachedField2.serializer = serializer;
                        }
                        if (!cachedField2.canBeNull) {
                            serializer.writeObjectData(byteBuffer, obj2);
                        } else {
                            serializer.writeObject(byteBuffer, obj2);
                        }
                    }
                    int position2 = (byteBuffer.position() - position) - 1;
                    if (position2 <= 127) {
                        byteBuffer.put(position, (byte) position2);
                    } else {
                        byte[] byteArray = context.getByteArray(position2);
                        byteBuffer.position(position + 1);
                        byteBuffer.get(byteArray, 0, position2);
                        byteBuffer.position(position);
                        IntSerializer.put(byteBuffer, position2, true);
                        byteBuffer.put(byteArray, 0, position2);
                    }
                }
                i++;
            } catch (IllegalAccessException e2) {
                throw new SerializationException("Error accessing field in class: " + obj.getClass().getName(), e2);
            } catch (SerializationException e3) {
                e3.addTrace(cachedField2 + " (" + obj.getClass().getName() + ")");
                throw e3;
            } catch (RuntimeException e4) {
                SerializationException serializationException = new SerializationException(e4);
                serializationException.addTrace(cachedField2 + " (" + obj.getClass().getName() + ")");
                throw serializationException;
            }
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote object: " + obj);
        }
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        return readObjectData(newInstance(this.kryo, cls), byteBuffer, cls);
    }

    /* access modifiers changed from: protected */
    public <T> T readObjectData(T t, ByteBuffer byteBuffer, Class<T> cls) {
        Object readObject;
        Serializer serializer;
        Context context = Kryo.getContext();
        CachedField[] cachedFieldArr = (CachedField[]) context.getTemp(this, "schema");
        if (cachedFieldArr == null) {
            int i = IntSerializer.get(byteBuffer, true);
            if (Log.TRACE) {
                Log.trace("kryo", "Reading " + i + " field names.");
            }
            String[] strArr = new String[i];
            for (int i2 = 0; i2 < i; i2++) {
                strArr[i2] = StringSerializer.get(byteBuffer);
            }
            cachedFieldArr = new CachedField[i];
            CachedField[] cachedFieldArr2 = this.fields;
            int length = strArr.length;
            for (int i3 = 0; i3 < length; i3++) {
                String str = strArr[i3];
                int i4 = 0;
                int length2 = cachedFieldArr2.length;
                while (true) {
                    if (i4 < length2) {
                        if (cachedFieldArr2[i4].field.getName().equals(str)) {
                            cachedFieldArr[i3] = cachedFieldArr2[i4];
                            break;
                        }
                        i4++;
                    } else if (Log.TRACE) {
                        Log.trace("kryo", "Ignoring obsolete field: " + str);
                    }
                }
            }
            context.putTemp(this, "schema", cachedFieldArr);
        }
        for (CachedField cachedField : cachedFieldArr) {
            int i5 = IntSerializer.get(byteBuffer, true);
            if (cachedField == null) {
                try {
                    if (Log.TRACE) {
                        Log.trace("kryo", "Skipping obsolete field bytes: " + i5);
                    }
                    try {
                        byteBuffer.position(i5 + byteBuffer.position());
                    } catch (IllegalArgumentException e) {
                        new BufferOverflowException();
                    }
                } catch (IllegalAccessException e2) {
                    throw new SerializationException("Error accessing field in class: " + cls.getName(), e2);
                } catch (SerializationException e3) {
                    e3.addTrace(cachedField + " (" + cls.getName() + ")");
                    throw e3;
                } catch (RuntimeException e4) {
                    SerializationException serializationException = new SerializationException(e4);
                    serializationException.addTrace(cachedField + " (" + cls.getName() + ")");
                    throw serializationException;
                }
            } else {
                if (Log.TRACE) {
                    Log.trace("kryo", "Reading field: " + cachedField + " (" + cls.getName() + ")");
                }
                if (i5 == 0) {
                    readObject = null;
                } else {
                    Class cls2 = cachedField.fieldClass;
                    Serializer serializer2 = cachedField.serializer;
                    if (cls2 == null) {
                        Kryo.RegisteredClass readClass = this.kryo.readClass(byteBuffer);
                        if (readClass == null) {
                            readObject = null;
                        } else {
                            Class type2 = readClass.getType();
                            if (serializer2 == null) {
                                serializer = readClass.getSerializer();
                            } else {
                                serializer = serializer2;
                            }
                            readObject = serializer.readObjectData(byteBuffer, type2);
                        }
                    } else {
                        if (serializer2 == null) {
                            serializer2 = this.kryo.getRegisteredClass(cls2).getSerializer();
                            cachedField.serializer = serializer2;
                        }
                        if (!cachedField.canBeNull) {
                            readObject = serializer2.readObjectData(byteBuffer, cls2);
                        } else {
                            readObject = serializer2.readObject(byteBuffer, cls2);
                        }
                    }
                }
                cachedField.set(t, readObject);
            }
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Read object: " + ((Object) t));
        }
        return t;
    }

    public CachedField getField(String str) {
        for (CachedField cachedField : this.fields) {
            if (cachedField.field.getName().equals(str)) {
                return cachedField;
            }
        }
        throw new IllegalArgumentException("Field \"" + str + "\" not found on class: " + this.type.getName());
    }

    public void removeField(String str) {
        for (int i = 0; i < this.fields.length; i++) {
            if (this.fields[i].field.getName().equals(str)) {
                CachedField[] cachedFieldArr = new CachedField[(this.fields.length - 1)];
                System.arraycopy(this.fields, 0, cachedFieldArr, 0, i);
                System.arraycopy(this.fields, i + 1, cachedFieldArr, i, cachedFieldArr.length - i);
                this.fields = cachedFieldArr;
                return;
            }
        }
        throw new IllegalArgumentException("Field \"" + str + "\" not found on class: " + this.type.getName());
    }

    public class CachedField {
        int accessIndex = -1;
        boolean canBeNull;
        Field field;
        Class fieldClass;
        Serializer serializer;

        public CachedField() {
        }

        public void setClass(Class cls) {
            this.fieldClass = cls;
            this.serializer = null;
        }

        public void setClass(Class cls, Serializer serializer2) {
            this.fieldClass = cls;
            this.serializer = serializer2;
        }

        public void setCanBeNull(boolean z) {
            this.canBeNull = z;
        }

        public String toString() {
            return this.field.getName();
        }

        /* access modifiers changed from: package-private */
        public Object get(Object obj) throws IllegalAccessException {
            if (this.accessIndex != -1) {
                return ((FieldAccess) CompatibleFieldSerializer.this.access).get(obj, this.accessIndex);
            }
            return this.field.get(obj);
        }

        /* access modifiers changed from: package-private */
        public void set(Object obj, Object obj2) throws IllegalAccessException {
            if (this.accessIndex != -1) {
                ((FieldAccess) CompatibleFieldSerializer.this.access).set(obj, this.accessIndex, obj2);
            } else {
                this.field.set(obj, obj2);
            }
        }
    }
}
