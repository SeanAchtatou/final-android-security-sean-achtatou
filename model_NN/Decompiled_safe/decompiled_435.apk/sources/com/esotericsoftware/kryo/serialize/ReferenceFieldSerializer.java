package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.util.IntHashMap;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import java.util.IdentityHashMap;

public class ReferenceFieldSerializer extends FieldSerializer {
    public ReferenceFieldSerializer(Kryo kryo, Class cls) {
        super(kryo, cls);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        References references;
        Context context = Kryo.getContext();
        References references2 = (References) context.getTemp("references");
        if (references2 == null) {
            References references3 = (References) context.get("references");
            if (references3 == null) {
                References references4 = new References();
                context.put("references", references4);
                references3 = references4;
            } else {
                references3.reset();
            }
            context.putTemp("references", references3);
            references = references3;
        } else {
            references = references2;
        }
        Integer num = references.objectToReference.get(obj);
        if (num != null) {
            IntSerializer.put(byteBuffer, num.intValue(), true);
            if (Log.TRACE) {
                Log.trace("kryo", "Wrote object reference " + num + ": " + obj);
                return;
            }
            return;
        }
        byteBuffer.put((byte) 0);
        references.referenceCount++;
        references.objectToReference.put(obj, Integer.valueOf(references.referenceCount));
        super.writeObjectData(byteBuffer, obj);
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        Context context = Kryo.getContext();
        References references = (References) context.getTemp("references");
        if (references == null) {
            references = (References) context.get("references");
            if (references == null) {
                References references2 = new References();
                context.put("references", references2);
                references = references2;
            } else {
                references.reset();
            }
            context.putTemp("references", references);
        }
        int i = IntSerializer.get(byteBuffer, true);
        if (i != 0) {
            T t = references.referenceToObject.get(i);
            if (t == null) {
                throw new SerializationException("Invalid object reference: " + i);
            } else if (!Log.TRACE) {
                return t;
            } else {
                Log.trace("kryo", "Read object reference " + i + ": " + ((Object) t));
                return t;
            }
        } else {
            T newInstance = newInstance(this.kryo, cls);
            references.referenceCount++;
            references.referenceToObject.put(references.referenceCount, newInstance);
            return super.readObjectData(newInstance, byteBuffer, cls);
        }
    }

    static class References {
        public IdentityHashMap<Object, Integer> objectToReference = new IdentityHashMap<>();
        public int referenceCount = 1;
        public IntHashMap referenceToObject = new IntHashMap();

        References() {
        }

        public void reset() {
            this.objectToReference.clear();
            this.referenceToObject.clear();
            this.referenceCount = 1;
        }
    }
}
