package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public class BooleanSerializer extends Serializer {
    public Boolean readObjectData(ByteBuffer byteBuffer, Class cls) {
        boolean z = byteBuffer.get() == 1;
        if (Log.TRACE) {
            Log.trace("kryo", "Read boolean: " + z);
        }
        return Boolean.valueOf(z);
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        byteBuffer.put(((Boolean) obj).booleanValue() ? (byte) 1 : 0);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote boolean: " + obj);
        }
    }
}
