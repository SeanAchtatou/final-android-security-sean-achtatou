package com.esotericsoftware.kryo.serialize;

import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public class EnumSerializer extends Serializer {
    private Object[] enumConstants;

    public EnumSerializer(Class<? extends Enum> cls) {
        this.enumConstants = cls.getEnumConstants();
        if (this.enumConstants == null) {
            throw new IllegalArgumentException("The type must be an enum: " + cls);
        }
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        int i = IntSerializer.get(byteBuffer, true);
        if (i < 0 || i > this.enumConstants.length - 1) {
            throw new SerializationException("Invalid ordinal for enum \"" + cls.getName() + "\": " + i);
        }
        T t = this.enumConstants[i];
        if (Log.TRACE) {
            Log.trace("kryo", "Read enum: " + ((Object) t));
        }
        return t;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        put(byteBuffer, (Enum) obj);
        if (Log.TRACE) {
            Log.trace("kryo", "Wrote enum: " + obj);
        }
    }

    public static void put(ByteBuffer byteBuffer, Enum enumR) {
        IntSerializer.put(byteBuffer, enumR.ordinal(), true);
    }

    public static <T> T get(ByteBuffer byteBuffer, Class<T> cls) {
        T[] enumConstants2 = cls.getEnumConstants();
        if (enumConstants2 == null) {
            throw new SerializationException("Class is not an enum: " + cls.getName());
        }
        int i = IntSerializer.get(byteBuffer, true);
        if (i >= 0 && i <= enumConstants2.length - 1) {
            return enumConstants2[i];
        }
        throw new SerializationException("Invalid ordinal for enum \"" + cls.getName() + "\": " + i);
    }
}
