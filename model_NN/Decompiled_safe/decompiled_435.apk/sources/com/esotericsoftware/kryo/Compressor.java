package com.esotericsoftware.kryo;

import com.esotericsoftware.minlog.Log;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public abstract class Compressor extends Serializer {
    protected final int bufferSize;
    private boolean compress;
    private boolean decompress;
    private Serializer serializer;

    public abstract void compress(ByteBuffer byteBuffer, Object obj, ByteBuffer byteBuffer2);

    public abstract void decompress(ByteBuffer byteBuffer, Class cls, ByteBuffer byteBuffer2);

    public Compressor(Serializer serializer2) {
        this(serializer2, Opcodes.ACC_STRICT);
    }

    public Compressor(Serializer serializer2, int i) {
        this.compress = true;
        this.decompress = true;
        this.serializer = serializer2;
        this.bufferSize = i;
    }

    public void setCompress(boolean z) {
        this.compress = z;
    }

    public void setDecompress(boolean z) {
        this.decompress = z;
    }

    public void writeObjectData(ByteBuffer byteBuffer, Object obj) {
        if (!this.compress) {
            this.serializer.writeObjectData(byteBuffer, obj);
            return;
        }
        int position = byteBuffer.position() + 2;
        try {
            byteBuffer.position(position);
        } catch (IllegalArgumentException e) {
            new BufferOverflowException();
        }
        this.serializer.writeObjectData(byteBuffer, obj);
        int position2 = byteBuffer.position();
        byteBuffer.position(position);
        byteBuffer.limit(position2);
        ByteBuffer buffer = Kryo.getContext().getBuffer(this.bufferSize);
        compress(byteBuffer, obj, buffer);
        buffer.flip();
        byteBuffer.position(position - 2);
        byteBuffer.limit(byteBuffer.capacity());
        byteBuffer.putShort((short) buffer.limit());
        byteBuffer.put(buffer);
        if (Log.TRACE) {
            Log.trace("kryo", "Compressed to " + (((float) ((int) ((((float) buffer.limit()) / ((float) (position2 - position))) * 10000.0f))) / 100.0f) + "% using: " + getClass().getName());
        }
    }

    public <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls) {
        if (!this.decompress) {
            return this.serializer.readObjectData(byteBuffer, cls);
        }
        int limit = byteBuffer.limit();
        short s = byteBuffer.getShort();
        try {
            byteBuffer.limit(byteBuffer.position() + s);
            ByteBuffer buffer = Kryo.getContext().getBuffer(this.bufferSize);
            decompress(byteBuffer, cls, buffer);
            buffer.flip();
            byteBuffer.limit(limit);
            if (Log.TRACE) {
                Log.trace("kryo", "Decompressed using: " + getClass().getName());
            }
            return this.serializer.readObjectData(buffer, cls);
        } catch (IllegalArgumentException e) {
            throw new SerializationException("Compressed data length exceeds buffer capacity: " + byteBuffer.position() + ((int) s), e);
        }
    }
}
