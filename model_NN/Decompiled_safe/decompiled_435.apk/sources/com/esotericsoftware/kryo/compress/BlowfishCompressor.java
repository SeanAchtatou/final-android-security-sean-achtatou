package com.esotericsoftware.kryo.compress;

import com.esotericsoftware.kryo.Compressor;
import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.objectweb.asm.Opcodes;

public class BlowfishCompressor extends Compressor {
    private SecretKeySpec keySpec;

    public BlowfishCompressor(Serializer serializer, byte[] bArr) {
        this(serializer, bArr, Opcodes.ACC_STRICT);
    }

    public BlowfishCompressor(Serializer serializer, byte[] bArr, int i) {
        super(serializer, i);
        this.keySpec = new SecretKeySpec(bArr, "Blowfish");
    }

    public void compress(ByteBuffer byteBuffer, Object obj, ByteBuffer byteBuffer2) {
        Context context = Kryo.getContext();
        Cipher cipher = (Cipher) context.get(this, "encryptCipher");
        if (cipher == null) {
            try {
                cipher = Cipher.getInstance("Blowfish");
                cipher.init(1, this.keySpec);
                context.put(this, "encryptCipher", cipher);
            } catch (GeneralSecurityException e) {
                throw new SerializationException(e);
            }
        }
        cipher.doFinal(byteBuffer, byteBuffer2);
    }

    public void decompress(ByteBuffer byteBuffer, Class cls, ByteBuffer byteBuffer2) {
        Context context = Kryo.getContext();
        Cipher cipher = (Cipher) context.get(this, "decryptCipher");
        if (cipher == null) {
            try {
                cipher = Cipher.getInstance("Blowfish");
                cipher.init(2, this.keySpec);
                context.put(this, "decryptCipher", cipher);
            } catch (GeneralSecurityException e) {
                throw new SerializationException(e);
            }
        }
        cipher.doFinal(byteBuffer, byteBuffer2);
    }
}
