package com.esotericsoftware.kryo.compress;

import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import java.nio.ByteBuffer;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.objectweb.asm.Opcodes;

public class DeflateCompressor extends ByteArrayCompressor {
    private Deflater deflater;
    private Inflater inflater;

    public DeflateCompressor(Serializer serializer) {
        this(serializer, Opcodes.ACC_STRICT);
    }

    public DeflateCompressor(Serializer serializer, int i) {
        super(serializer, i);
        this.deflater = new Deflater();
        this.inflater = new Inflater();
    }

    public void compress(byte[] bArr, int i, ByteBuffer byteBuffer) {
        this.deflater.reset();
        this.deflater.setInput(bArr, 0, i);
        this.deflater.finish();
        byteBuffer.position(this.deflater.deflate(byteBuffer.array()));
    }

    public void decompress(byte[] bArr, int i, ByteBuffer byteBuffer) {
        this.inflater.reset();
        this.inflater.setInput(bArr, 0, i);
        try {
            byteBuffer.position(this.inflater.inflate(byteBuffer.array()));
        } catch (DataFormatException e) {
            throw new SerializationException("Error inflating data.", e);
        }
    }
}
