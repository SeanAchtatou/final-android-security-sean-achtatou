package com.esotericsoftware.kryo.compress;

import com.esotericsoftware.kryo.serialize.IntSerializer;
import com.esotericsoftware.kryo.util.LongToIntHashMap;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public class Delta {
    private static final byte COMMAND_APPEND = -1;
    private static final byte COMMAND_COPY = 0;
    private static final int DATA_MAX = 253;
    private static final boolean debug = false;
    private static final ByteBuffer emptyBuffer = ByteBuffer.allocate(0);
    private static final char[] single_hash;
    private int appendLength;
    private int appendPosition;
    private final LongToIntHashMap checksums;
    private int chunkSize;
    private boolean eof;
    private long hash;
    private boolean hashReset;
    private ByteBuffer sbuf;
    private ByteBuffer targetBuffer;
    private ByteBuffer tbuf;

    static {
        char[] cArr = new char[Opcodes.ACC_NATIVE];
        // fill-array-data instruction
        cArr[0] = -17199;
        cArr[1] = -17563;
        cArr[2] = 17090;
        cArr[3] = -8194;
        cArr[4] = -27034;
        cArr[5] = 17179;
        cArr[6] = -31484;
        cArr[7] = -5306;
        cArr[8] = 25465;
        cArr[9] = -11168;
        cArr[10] = -12524;
        cArr[11] = 21455;
        cArr[12] = -9391;
        cArr[13] = -9464;
        cArr[14] = 4808;
        cArr[15] = -2558;
        cArr[16] = -6298;
        cArr[17] = 9108;
        cArr[18] = 9485;
        cArr[19] = -9029;
        cArr[20] = -22920;
        cArr[21] = 687;
        cArr[22] = -23098;
        cArr[23] = 32422;
        cArr[24] = -18875;
        cArr[25] = -13491;
        cArr[26] = -15285;
        cArr[27] = -6692;
        cArr[28] = -24602;
        cArr[29] = 23388;
        cArr[30] = 13813;
        cArr[31] = 28698;
        cArr[32] = 8719;
        cArr[33] = 27704;
        cArr[34] = 6742;
        cArr[35] = 19619;
        cArr[36] = -58;
        cArr[37] = -20142;
        cArr[38] = -29343;
        cArr[39] = 31320;
        cArr[40] = -28635;
        cArr[41] = -29891;
        cArr[42] = -16625;
        cArr[43] = -27229;
        cArr[44] = -6668;
        cArr[45] = -16089;
        cArr[46] = 15341;
        cArr[47] = 12811;
        cArr[48] = -18445;
        cArr[49] = 24660;
        cArr[50] = 13116;
        cArr[51] = -11389;
        cArr[52] = -32428;
        cArr[53] = 21058;
        cArr[54] = 19981;
        cArr[55] = 2708;
        cArr[56] = 28712;
        cArr[57] = -31095;
        cArr[58] = 14882;
        cArr[59] = 2432;
        cArr[60] = 6215;
        cArr[61] = -20239;
        cArr[62] = -25764;
        cArr[63] = 16758;
        cArr[64] = -18344;
        cArr[65] = -10942;
        cArr[66] = 8044;
        cArr[67] = 9367;
        cArr[68] = 27226;
        cArr[69] = -24663;
        cArr[70] = -29606;
        cArr[71] = 30531;
        cArr[72] = -22359;
        cArr[73] = -26110;
        cArr[74] = 18712;
        cArr[75] = 17292;
        cArr[76] = -15480;
        cArr[77] = -25045;
        cArr[78] = 19629;
        cArr[79] = 438;
        cArr[80] = -21735;
        cArr[81] = -2185;
        cArr[82] = 13919;
        cArr[83] = 7858;
        cArr[84] = 2334;
        cArr[85] = 31736;
        cArr[86] = 31374;
        cArr[87] = 21031;
        cArr[88] = -5455;
        cArr[89] = 8308;
        cArr[90] = 17699;
        cArr[91] = -6271;
        cArr[92] = 419;
        cArr[93] = 5693;
        cArr[94] = 15150;
        cArr[95] = 10365;
        cArr[96] = 24191;
        cArr[97] = -24477;
        cArr[98] = -20172;
        cArr[99] = -28754;
        cArr[100] = 24206;
        cArr[101] = -18505;
        cArr[102] = 17736;
        cArr[103] = 8026;
        cArr[104] = -1450;
        cArr[105] = 31268;
        cArr[106] = -28657;
        cArr[107] = 17116;
        cArr[108] = -13207;
        cArr[109] = 672;
        cArr[110] = 2850;
        cArr[111] = -9423;
        cArr[112] = 29182;
        cArr[113] = 3197;
        cArr[114] = 5938;
        cArr[115] = 4441;
        cArr[116] = -13559;
        cArr[117] = -7726;
        cArr[118] = 4945;
        cArr[119] = 21225;
        cArr[120] = -2762;
        cArr[121] = 23119;
        cArr[122] = -15594;
        cArr[123] = 27641;
        cArr[124] = -30316;
        cArr[125] = -18572;
        cArr[126] = 24382;
        cArr[127] = -2346;
        cArr[128] = 14945;
        cArr[129] = -2004;
        cArr[130] = -13278;
        cArr[131] = -25338;
        cArr[132] = 10652;
        cArr[133] = 2533;
        cArr[134] = 7916;
        cArr[135] = 20815;
        cArr[136] = -29357;
        cArr[137] = -22960;
        cArr[138] = 23662;
        cArr[139] = -14985;
        cArr[140] = 31064;
        cArr[141] = 29100;
        cArr[142] = -30442;
        cArr[143] = -25777;
        cArr[144] = 11273;
        cArr[145] = 21009;
        cArr[146] = -2344;
        cArr[147] = -13654;
        cArr[148] = -2065;
        cArr[149] = 10367;
        cArr[150] = 31380;
        cArr[151] = -21687;
        cArr[152] = -1492;
        cArr[153] = 29218;
        cArr[154] = -7081;
        cArr[155] = -10470;
        cArr[156] = 195;
        cArr[157] = 6774;
        cArr[158] = -5748;
        cArr[159] = -16329;
        cArr[160] = -32248;
        cArr[161] = 23597;
        cArr[162] = -8230;
        cArr[163] = -6667;
        cArr[164] = 2885;
        cArr[165] = 5582;
        cArr[166] = -30082;
        cArr[167] = -851;
        cArr[168] = -21971;
        cArr[169] = 19292;
        cArr[170] = -11218;
        cArr[171] = -19887;
        cArr[172] = -28546;
        cArr[173] = -26041;
        cArr[174] = -13914;
        cArr[175] = -9921;
        cArr[176] = 2142;
        cArr[177] = 13774;
        cArr[178] = -24237;
        cArr[179] = 32379;
        cArr[180] = -24821;
        cArr[181] = 9642;
        cArr[182] = 23967;
        cArr[183] = -16307;
        cArr[184] = -30194;
        cArr[185] = 10357;
        cArr[186] = 18972;
        cArr[187] = 10591;
        cArr[188] = 5011;
        cArr[189] = -2208;
        cArr[190] = -28296;
        cArr[191] = 3931;
        cArr[192] = -1411;
        cArr[193] = -31820;
        cArr[194] = 8322;
        cArr[195] = 29213;
        cArr[196] = 25698;
        cArr[197] = 872;
        cArr[198] = 26594;
        cArr[199] = -31196;
        cArr[200] = 6477;
        cArr[201] = 8950;
        cArr[202] = 30971;
        cArr[203] = 26513;
        cArr[204] = -19912;
        cArr[205] = -19662;
        cArr[206] = 29302;
        cArr[207] = -3470;
        cArr[208] = 18412;
        cArr[209] = 17668;
        cArr[210] = -22175;
        cArr[211] = -24632;
        cArr[212] = 16348;
        cArr[213] = -19437;
        cArr[214] = 122;
        cArr[215] = 2054;
        cArr[216] = 29784;
        cArr[217] = -27194;
        cArr[218] = -13142;
        cArr[219] = 6358;
        cArr[220] = -7506;
        cArr[221] = 6918;
        cArr[222] = -3082;
        cArr[223] = 20560;
        cArr[224] = -14104;
        cArr[225] = -2900;
        cArr[226] = -16308;
        cArr[227] = -3044;
        cArr[228] = -26321;
        cArr[229] = -20924;
        cArr[230] = 24347;
        cArr[231] = 4371;
        cArr[232] = 5944;
        cArr[233] = -9816;
        cArr[234] = 6634;
        cArr[235] = 11571;
        cArr[236] = -26984;
        cArr[237] = 12265;
        cArr[238] = 12863;
        cArr[239] = -12830;
        cArr[240] = 28017;
        cArr[241] = -7299;
        cArr[242] = -18793;
        cArr[243] = 11343;
        cArr[244] = 17267;
        cArr[245] = -28414;
        cArr[246] = 1885;
        cArr[247] = -29147;
        cArr[248] = 5746;
        cArr[249] = -5080;
        cArr[250] = 27339;
        cArr[251] = -31028;
        cArr[252] = 6254;
        cArr[253] = -27628;
        cArr[254] = -10636;
        cArr[255] = -11867;
        single_hash = cArr;
    }

    public Delta() {
        this(Opcodes.ACC_STRICT, 8);
    }

    public Delta(int i, int i2) {
        this.checksums = new LongToIntHashMap();
        this.chunkSize = i2;
        int min = Math.min((int) Opcodes.ACC_ENUM, i2 * 4);
        this.tbuf = ByteBuffer.allocate(min);
        this.sbuf = ByteBuffer.allocate(min);
    }

    public void compress(ByteBuffer byteBuffer, ByteBuffer byteBuffer2, ByteBuffer byteBuffer3) {
        ByteBuffer byteBuffer4;
        if (byteBuffer == null) {
            byteBuffer4 = emptyBuffer;
        } else {
            byteBuffer4 = byteBuffer;
        }
        this.checksums.clear();
        this.eof = false;
        this.hashReset = true;
        this.tbuf.limit(0);
        this.targetBuffer = byteBuffer2;
        int i = 0;
        while (byteBuffer4.remaining() >= this.chunkSize) {
            int i2 = 0;
            int i3 = 0;
            for (int i4 = 0; i4 < this.chunkSize; i4++) {
                i2 += single_hash[byteBuffer4.get() + 128];
                i3 += i2;
            }
            long j = (long) (((i3 & 65535) << 16) | (i2 & 65535));
            if (!this.checksums.containsKey(j)) {
                this.checksums.put(j, i);
                i++;
            }
        }
        while (!this.eof) {
            int find = find();
            if (find != -1) {
                int i5 = find * this.chunkSize;
                byteBuffer4.position(i5);
                int longestMatch = longestMatch(byteBuffer4);
                if (longestMatch >= this.chunkSize) {
                    writeAppend(byteBuffer3);
                    byteBuffer3.put((byte) COMMAND_COPY);
                    IntSerializer.put(byteBuffer3, longestMatch, true);
                    IntSerializer.put(byteBuffer3, i5, true);
                } else {
                    this.tbuf.position(this.tbuf.position() - longestMatch);
                    appendData();
                }
            } else {
                appendData();
            }
        }
        writeAppend(byteBuffer3);
    }

    private void appendData() {
        int position = this.targetBuffer.position() - this.tbuf.remaining();
        if (this.tbuf.remaining() > this.chunkSize || readMore()) {
            byte b = this.tbuf.get();
            if (this.tbuf.remaining() >= this.chunkSize) {
                byte b2 = this.tbuf.get((this.tbuf.position() + this.chunkSize) - 1);
                char c = single_hash[b + 128];
                int i = (single_hash[b2 + 128] + (((int) (this.hash & 65535)) - c)) & 65535;
                this.hash = (long) (((((((int) (this.hash >> 16)) - (c * this.chunkSize)) + i) & 65535) << 16) | (i & 65535));
            }
            if (this.appendLength == 0) {
                this.appendPosition = position;
            }
            this.appendLength++;
        }
    }

    private void writeAppend(ByteBuffer byteBuffer) {
        if (this.appendLength != 0) {
            int i = this.appendLength;
            if (i <= DATA_MAX) {
                byteBuffer.put((byte) i);
            } else {
                byteBuffer.put((byte) COMMAND_APPEND);
                IntSerializer.put(byteBuffer, i, true);
            }
            int i2 = i + this.appendPosition;
            for (int i3 = this.appendPosition; i3 < i2; i3++) {
                byteBuffer.put(this.targetBuffer.get(i3));
            }
            this.appendLength = 0;
        }
    }

    private int find() {
        this.sbuf.clear();
        this.sbuf.limit(0);
        if (this.hashReset) {
            while (this.tbuf.remaining() < this.chunkSize) {
                this.tbuf.compact();
                try {
                    if (!this.targetBuffer.hasRemaining()) {
                        this.tbuf.flip();
                        return -1;
                    }
                    while (this.targetBuffer.hasRemaining() && this.tbuf.hasRemaining()) {
                        this.tbuf.put(this.targetBuffer.get());
                    }
                } finally {
                    this.tbuf.flip();
                }
            }
            this.tbuf.mark();
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < this.chunkSize; i3++) {
                i += single_hash[this.tbuf.get() + 128];
                i2 += i;
            }
            this.hash = (long) (((i2 & 65535) << 16) | (i & 65535));
            this.tbuf.reset();
            this.hashReset = false;
        }
        if (!this.checksums.containsKey(this.hash)) {
            return -1;
        }
        return this.checksums.get(this.hash);
    }

    private int longestMatch(ByteBuffer byteBuffer) {
        int i = 0;
        this.hashReset = true;
        while (true) {
            if (!this.sbuf.hasRemaining()) {
                this.sbuf.clear();
                try {
                    if (!byteBuffer.hasRemaining()) {
                        this.sbuf.flip();
                        break;
                    }
                    while (byteBuffer.hasRemaining() && this.sbuf.hasRemaining()) {
                        this.sbuf.put(byteBuffer.get());
                    }
                } finally {
                    this.sbuf.flip();
                }
            }
            if (!this.tbuf.hasRemaining() && !readMore()) {
                break;
            } else if (this.sbuf.get() != this.tbuf.get()) {
                this.tbuf.position(this.tbuf.position() - 1);
                break;
            } else {
                i++;
            }
        }
        return i;
    }

    private boolean readMore() {
        this.tbuf.compact();
        while (this.targetBuffer.hasRemaining() && this.tbuf.hasRemaining()) {
            this.tbuf.put(this.targetBuffer.get());
        }
        this.tbuf.flip();
        if (this.tbuf.hasRemaining()) {
            return true;
        }
        this.eof = true;
        return false;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [int] */
    /* JADX WARN: Type inference failed for: r1v7, types: [int] */
    /* JADX WARN: Type inference failed for: r1v8, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void decompress(java.nio.ByteBuffer r6, java.nio.ByteBuffer r7, java.nio.ByteBuffer r8) {
        /*
            r5 = this;
            r2 = 1
        L_0x0001:
            int r0 = r7.remaining()
            if (r0 <= 0) goto L_0x0061
            byte r0 = r7.get()
            if (r0 <= 0) goto L_0x002e
            r1 = 253(0xfd, float:3.55E-43)
            if (r0 > r1) goto L_0x002e
            r1 = -1
        L_0x0012:
            switch(r1) {
                case -1: goto L_0x0036;
                case 0: goto L_0x0043;
                default: goto L_0x0015;
            }
        L_0x0015:
            com.esotericsoftware.kryo.SerializationException r0 = new com.esotericsoftware.kryo.SerializationException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid delta command: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002e:
            int r1 = com.esotericsoftware.kryo.serialize.IntSerializer.get(r7, r2)
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0012
        L_0x0036:
            int r1 = r0 + -1
            if (r0 <= 0) goto L_0x0001
            byte r0 = r7.get()
            r8.put(r0)
            r0 = r1
            goto L_0x0036
        L_0x0043:
            if (r6 != 0) goto L_0x004d
            com.esotericsoftware.kryo.SerializationException r0 = new com.esotericsoftware.kryo.SerializationException
            java.lang.String r1 = "Delta copy command received without previous data."
            r0.<init>(r1)
            throw r0
        L_0x004d:
            int r1 = com.esotericsoftware.kryo.serialize.IntSerializer.get(r7, r2)
            r6.position(r1)
        L_0x0054:
            int r1 = r0 + -1
            if (r0 <= 0) goto L_0x0001
            byte r0 = r6.get()
            r8.put(r0)
            r0 = r1
            goto L_0x0054
        L_0x0061:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryo.compress.Delta.decompress(java.nio.ByteBuffer, java.nio.ByteBuffer, java.nio.ByteBuffer):void");
    }

    private static StringBuilder dump(ByteBuffer byteBuffer, int i, int i2) {
        int position = byteBuffer.position();
        byteBuffer.position(i);
        StringBuilder sb = new StringBuilder();
        int i3 = i2;
        while (true) {
            int i4 = i3 - 1;
            if (i3 > 0) {
                sb.append((int) byteBuffer.get());
                sb.append(',');
                i3 = i4;
            } else {
                byteBuffer.position(position);
                return sb;
            }
        }
    }
}
