package com.esotericsoftware.kryo.compress;

import com.esotericsoftware.kryo.SerializationException;
import com.esotericsoftware.kryo.Serializer;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public abstract class StreamCompressor extends ByteArrayCompressor {
    public abstract FilterOutputStream getCompressionStream(OutputStream outputStream) throws IOException;

    public abstract FilterInputStream getDecompressionStream(InputStream inputStream) throws IOException;

    public StreamCompressor(Serializer serializer) {
        super(serializer, Opcodes.ACC_STRICT);
    }

    public StreamCompressor(Serializer serializer, int i) {
        super(serializer, i);
    }

    public void compress(byte[] bArr, int i, ByteBuffer byteBuffer) {
        FilterOutputStream compressionStream;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(byteBuffer.array());
        try {
            compressionStream = getCompressionStream(byteArrayOutputStream);
            compressionStream.write(bArr, 0, i);
            compressionStream.close();
            byteBuffer.position(byteArrayOutputStream.size());
        } catch (IOException e) {
            throw new SerializationException(e);
        } catch (Throwable th) {
            compressionStream.close();
            throw th;
        }
    }

    public void decompress(byte[] bArr, int i, ByteBuffer byteBuffer) {
        FilterInputStream decompressionStream;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        byteArrayInputStream.setCount(i);
        try {
            decompressionStream = getDecompressionStream(byteArrayInputStream);
            byteBuffer.position(decompressionStream.read(byteBuffer.array(), 0, byteBuffer.capacity()));
            decompressionStream.close();
        } catch (IOException e) {
            throw new SerializationException(e);
        } catch (Throwable th) {
            decompressionStream.close();
            throw th;
        }
    }

    private static class ByteArrayOutputStream extends java.io.ByteArrayOutputStream {
        public ByteArrayOutputStream(byte[] bArr) {
            super(0);
            this.buf = bArr;
        }
    }

    private static class ByteArrayInputStream extends java.io.ByteArrayInputStream {
        public ByteArrayInputStream(byte[] bArr) {
            super(bArr);
        }

        public void setCount(int i) {
            this.count = i;
        }
    }
}
