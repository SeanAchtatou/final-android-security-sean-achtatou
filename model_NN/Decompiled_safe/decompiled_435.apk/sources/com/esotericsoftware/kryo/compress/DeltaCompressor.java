package com.esotericsoftware.kryo.compress;

import com.esotericsoftware.kryo.Compressor;
import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.util.IntHashMap;
import java.nio.ByteBuffer;
import org.objectweb.asm.Opcodes;

public class DeltaCompressor extends Compressor {
    private final int chunkSize;
    final IntHashMap<ByteBuffer> contextToLocalData;
    final IntHashMap<ByteBuffer> contextToRemoteData;
    private final Kryo kryo;
    private Kryo.Listener removeBuffersListener;

    public DeltaCompressor(Kryo kryo2, Serializer serializer) {
        this(kryo2, serializer, Opcodes.ACC_STRICT, 8);
    }

    public DeltaCompressor(Kryo kryo2, Serializer serializer, int i, int i2) {
        super(serializer, i);
        this.contextToRemoteData = new IntHashMap<>();
        this.contextToLocalData = new IntHashMap<>();
        this.removeBuffersListener = new Kryo.Listener() {
            public void remoteEntityRemoved(int i) {
                DeltaCompressor.this.contextToRemoteData.remove(i);
                DeltaCompressor.this.contextToLocalData.remove(i);
            }
        };
        this.kryo = kryo2;
        this.chunkSize = i2;
    }

    public void compress(ByteBuffer byteBuffer, Object obj, ByteBuffer byteBuffer2) {
        int position = byteBuffer.position();
        Context context = Kryo.getContext();
        int remoteEntityID = context.getRemoteEntityID();
        ByteBuffer byteBuffer3 = this.contextToRemoteData.get(remoteEntityID);
        Delta delta = (Delta) context.get(this, "delta");
        if (delta == null) {
            delta = new Delta(this.bufferSize, this.chunkSize);
            context.put(this, "delta", delta);
        }
        delta.compress(byteBuffer3, byteBuffer, byteBuffer2);
        if (byteBuffer3 == null) {
            byteBuffer3 = ByteBuffer.allocate(this.bufferSize);
            this.contextToRemoteData.put(remoteEntityID, byteBuffer3);
            this.kryo.addListener(this.removeBuffersListener);
        }
        byteBuffer3.clear();
        byteBuffer.position(position);
        byteBuffer3.put(byteBuffer);
        byteBuffer3.flip();
    }

    public void decompress(ByteBuffer byteBuffer, Class cls, ByteBuffer byteBuffer2) {
        Context context = Kryo.getContext();
        int remoteEntityID = context.getRemoteEntityID();
        ByteBuffer byteBuffer3 = this.contextToLocalData.get(remoteEntityID);
        Delta delta = (Delta) context.get(this, "delta");
        if (delta == null) {
            delta = new Delta(this.bufferSize, this.chunkSize);
            context.put(this, "delta", delta);
        }
        delta.decompress(byteBuffer3, byteBuffer, byteBuffer2);
        if (byteBuffer3 == null) {
            byteBuffer3 = ByteBuffer.allocate(this.bufferSize);
            this.contextToLocalData.put(remoteEntityID, byteBuffer3);
            this.kryo.addListener(this.removeBuffersListener);
        }
        byteBuffer3.clear();
        byteBuffer2.flip();
        byteBuffer3.put(byteBuffer2);
        byteBuffer3.flip();
    }
}
