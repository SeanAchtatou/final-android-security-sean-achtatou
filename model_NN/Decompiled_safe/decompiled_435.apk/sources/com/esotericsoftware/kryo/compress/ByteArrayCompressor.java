package com.esotericsoftware.kryo.compress;

import com.esotericsoftware.kryo.Compressor;
import com.esotericsoftware.kryo.Context;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import java.nio.ByteBuffer;

public abstract class ByteArrayCompressor extends Compressor {
    public abstract void compress(byte[] bArr, int i, ByteBuffer byteBuffer);

    public abstract void decompress(byte[] bArr, int i, ByteBuffer byteBuffer);

    public ByteArrayCompressor(Serializer serializer) {
        super(serializer);
    }

    public ByteArrayCompressor(Serializer serializer, int i) {
        super(serializer, i);
    }

    public void compress(ByteBuffer byteBuffer, Object obj, ByteBuffer byteBuffer2) {
        Context context = Kryo.getContext();
        int remaining = byteBuffer.remaining();
        byte[] array = context.getBuffer(Math.max(remaining, this.bufferSize)).array();
        byteBuffer.get(array, 0, remaining);
        compress(array, remaining, byteBuffer2);
    }

    public void decompress(ByteBuffer byteBuffer, Class cls, ByteBuffer byteBuffer2) {
        Context context = Kryo.getContext();
        int remaining = byteBuffer.remaining();
        byte[] array = context.getBuffer(Math.max(remaining, this.bufferSize)).array();
        byteBuffer.get(array, 0, remaining);
        decompress(array, remaining, byteBuffer2);
    }
}
