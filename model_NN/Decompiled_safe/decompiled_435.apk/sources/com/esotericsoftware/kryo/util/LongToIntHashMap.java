package com.esotericsoftware.kryo.util;

public class LongToIntHashMap {
    private int capacity;
    private float loadFactor;
    private int mask;
    private int size;
    private Entry[] table;
    private int threshold;

    public LongToIntHashMap() {
        this(16, 0.75f);
    }

    public LongToIntHashMap(int i) {
        this(i, 0.75f);
    }

    public LongToIntHashMap(int i, float f) {
        if (i > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large.");
        } else if (i < 0) {
            throw new IllegalArgumentException("initialCapacity must be greater than zero.");
        } else if (f <= 0.0f) {
            throw new IllegalArgumentException("initialCapacity must be greater than zero.");
        } else {
            this.capacity = 1;
            while (this.capacity < i) {
                this.capacity <<= 1;
            }
            this.loadFactor = f;
            this.threshold = (int) (((float) this.capacity) * f);
            this.table = new Entry[this.capacity];
            this.mask = this.capacity - 1;
        }
    }

    public boolean containsValue(int i) {
        Entry[] entryArr = this.table;
        int length = entryArr.length;
        while (true) {
            int i2 = length - 1;
            if (length <= 0) {
                return false;
            }
            for (Entry entry = entryArr[i2]; entry != null; entry = entry.next) {
                if (entry.value == i) {
                    return true;
                }
            }
            length = i2;
        }
    }

    public boolean containsKey(long j) {
        for (Entry entry = this.table[((int) j) & this.mask]; entry != null; entry = entry.next) {
            if (entry.key == j) {
                return true;
            }
        }
        return false;
    }

    public int get(long j) {
        for (Entry entry = this.table[((int) j) & this.mask]; entry != null; entry = entry.next) {
            if (entry.key == j) {
                return entry.value;
            }
        }
        return 0;
    }

    public int put(long j, int i) {
        int i2 = ((int) j) & this.mask;
        Entry entry = this.table[i2];
        while (entry != null) {
            if (entry.key != j) {
                entry = entry.next;
            } else {
                int i3 = entry.value;
                entry.value = i;
                return i3;
            }
        }
        this.table[i2] = new Entry(j, i, this.table[i2]);
        int i4 = this.size;
        this.size = i4 + 1;
        if (i4 >= this.threshold) {
            int i5 = this.capacity * 2;
            Entry[] entryArr = new Entry[i5];
            Entry[] entryArr2 = this.table;
            int i6 = i5 - 1;
            for (int i7 = 0; i7 < entryArr2.length; i7++) {
                Entry entry2 = entryArr2[i7];
                if (entry2 != null) {
                    entryArr2[i7] = null;
                    while (true) {
                        Entry entry3 = entry2.next;
                        int i8 = ((int) entry2.key) & i6;
                        entry2.next = entryArr[i8];
                        entryArr[i8] = entry2;
                        if (entry3 == null) {
                            break;
                        }
                        entry2 = entry3;
                    }
                }
            }
            this.table = entryArr;
            this.capacity = i5;
            this.threshold = (int) (((float) i5) * this.loadFactor);
            this.mask = this.capacity - 1;
        }
        return 0;
    }

    public int remove(long j) {
        int i = ((int) j) & this.mask;
        Entry entry = this.table[i];
        Entry entry2 = entry;
        while (entry != null) {
            Entry entry3 = entry.next;
            if (entry.key == j) {
                this.size--;
                if (entry2 == entry) {
                    this.table[i] = entry3;
                } else {
                    entry2.next = entry3;
                }
                return entry.value;
            }
            entry2 = entry;
            entry = entry3;
        }
        return 0;
    }

    public int size() {
        return this.size;
    }

    public void clear() {
        Entry[] entryArr = this.table;
        int length = entryArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                entryArr[length] = null;
            } else {
                this.size = 0;
                return;
            }
        }
    }

    static class Entry {
        final long key;
        Entry next;
        int value;

        Entry(long j, int i, Entry entry) {
            this.key = j;
            this.value = i;
            this.next = entry;
        }
    }
}
