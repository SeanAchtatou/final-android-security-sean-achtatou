package com.esotericsoftware.kryo.util;

public class IntHashMap<T> {
    private int capacity;
    private float loadFactor;
    private int mask;
    private int size;
    private Entry[] table;
    private int threshold;

    public IntHashMap() {
        this(16, 0.75f);
    }

    public IntHashMap(int i) {
        this(i, 0.75f);
    }

    public IntHashMap(int i, float f) {
        if (i > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large.");
        } else if (i < 0) {
            throw new IllegalArgumentException("initialCapacity must be greater than zero.");
        } else if (f <= 0.0f) {
            throw new IllegalArgumentException("initialCapacity must be greater than zero.");
        } else {
            this.capacity = 1;
            while (this.capacity < i) {
                this.capacity <<= 1;
            }
            this.loadFactor = f;
            this.threshold = (int) (((float) this.capacity) * f);
            this.table = new Entry[this.capacity];
            this.mask = this.capacity - 1;
        }
    }

    public boolean containsValue(Object obj) {
        Entry[] entryArr = this.table;
        int length = entryArr.length;
        while (true) {
            int i = length - 1;
            if (length <= 0) {
                return false;
            }
            for (Entry entry = entryArr[i]; entry != null; entry = entry.next) {
                if (entry.value.equals(obj)) {
                    return true;
                }
            }
            length = i;
        }
    }

    public boolean containsKey(int i) {
        for (Entry entry = this.table[this.mask & i]; entry != null; entry = entry.next) {
            if (entry.key == i) {
                return true;
            }
        }
        return false;
    }

    public T get(int i) {
        for (Entry entry = this.table[this.mask & i]; entry != null; entry = entry.next) {
            if (entry.key == i) {
                return entry.value;
            }
        }
        return null;
    }

    public T put(int i, T t) {
        int i2 = this.mask & i;
        Entry entry = this.table[i2];
        while (entry != null) {
            if (entry.key != i) {
                entry = entry.next;
            } else {
                T t2 = entry.value;
                entry.value = t;
                return t2;
            }
        }
        this.table[i2] = new Entry(i, t, this.table[i2]);
        int i3 = this.size;
        this.size = i3 + 1;
        if (i3 >= this.threshold) {
            int i4 = this.capacity * 2;
            Entry[] entryArr = new Entry[i4];
            Entry[] entryArr2 = this.table;
            int i5 = i4 - 1;
            for (int i6 = 0; i6 < entryArr2.length; i6++) {
                Entry entry2 = entryArr2[i6];
                if (entry2 != null) {
                    entryArr2[i6] = null;
                    while (true) {
                        Entry entry3 = entry2.next;
                        int i7 = entry2.key & i5;
                        entry2.next = entryArr[i7];
                        entryArr[i7] = entry2;
                        if (entry3 == null) {
                            break;
                        }
                        entry2 = entry3;
                    }
                }
            }
            this.table = entryArr;
            this.capacity = i4;
            this.threshold = (int) (((float) i4) * this.loadFactor);
            this.mask = this.capacity - 1;
        }
        return null;
    }

    public T remove(int i) {
        int i2 = this.mask & i;
        Entry entry = this.table[i2];
        Entry entry2 = entry;
        while (entry != null) {
            Entry entry3 = entry.next;
            if (entry.key == i) {
                this.size--;
                if (entry2 == entry) {
                    this.table[i2] = entry3;
                } else {
                    entry2.next = entry3;
                }
                return entry.value;
            }
            entry2 = entry;
            entry = entry3;
        }
        return null;
    }

    public int size() {
        return this.size;
    }

    public void clear() {
        Entry[] entryArr = this.table;
        int length = entryArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                entryArr[length] = null;
            } else {
                this.size = 0;
                return;
            }
        }
    }

    static class Entry {
        final int key;
        Entry next;
        Object value;

        Entry(int i, Object obj, Entry entry) {
            this.key = i;
            this.value = obj;
            this.next = entry;
        }
    }
}
