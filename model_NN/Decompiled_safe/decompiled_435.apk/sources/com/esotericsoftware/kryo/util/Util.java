package com.esotericsoftware.kryo.util;

public class Util {
    public static boolean isAndroid;

    static {
        try {
            Class.forName("android.os.Process");
            isAndroid = true;
        } catch (Exception e) {
            isAndroid = false;
        }
    }
}
