package com.esotericsoftware.kryo.util;

public class ShortHashMap<T> {
    private int capacity;
    private float loadFactor;
    private int mask;
    private int size;
    private Entry[] table;
    private int threshold;

    public ShortHashMap() {
        this(16, 0.75f);
    }

    public ShortHashMap(int i) {
        this(i, 0.75f);
    }

    public ShortHashMap(int i, float f) {
        if (i > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large.");
        } else if (i < 0) {
            throw new IllegalArgumentException("initialCapacity must be greater than zero.");
        } else if (f <= 0.0f) {
            throw new IllegalArgumentException("initialCapacity must be greater than zero.");
        } else {
            this.capacity = 1;
            while (this.capacity < i) {
                this.capacity <<= 1;
            }
            this.loadFactor = f;
            this.threshold = (int) (((float) this.capacity) * f);
            this.table = new Entry[this.capacity];
            this.mask = this.capacity - 1;
        }
    }

    public boolean containsValue(Object obj) {
        Entry[] entryArr = this.table;
        int length = entryArr.length;
        while (true) {
            int i = length - 1;
            if (length <= 0) {
                return false;
            }
            for (Entry entry = entryArr[i]; entry != null; entry = entry.next) {
                if (entry.value.equals(obj)) {
                    return true;
                }
            }
            length = i;
        }
    }

    public boolean containsKey(short s) {
        for (Entry entry = this.table[this.mask & s]; entry != null; entry = entry.next) {
            if (entry.key == s) {
                return true;
            }
        }
        return false;
    }

    public T get(short s) {
        for (Entry entry = this.table[this.mask & s]; entry != null; entry = entry.next) {
            if (entry.key == s) {
                return entry.value;
            }
        }
        return null;
    }

    public T put(short s, T t) {
        short s2 = this.mask & s;
        Entry entry = this.table[s2];
        while (entry != null) {
            if (entry.key != s) {
                entry = entry.next;
            } else {
                T t2 = entry.value;
                entry.value = t;
                return t2;
            }
        }
        this.table[s2] = new Entry(s, t, this.table[s2]);
        int i = this.size;
        this.size = i + 1;
        if (i >= this.threshold) {
            int i2 = this.capacity * 2;
            Entry[] entryArr = new Entry[i2];
            Entry[] entryArr2 = this.table;
            int i3 = i2 - 1;
            for (int i4 = 0; i4 < entryArr2.length; i4++) {
                Entry entry2 = entryArr2[i4];
                if (entry2 != null) {
                    entryArr2[i4] = null;
                    while (true) {
                        Entry entry3 = entry2.next;
                        short s3 = entry2.key & i3;
                        entry2.next = entryArr[s3];
                        entryArr[s3] = entry2;
                        if (entry3 == null) {
                            break;
                        }
                        entry2 = entry3;
                    }
                }
            }
            this.table = entryArr;
            this.capacity = i2;
            this.mask = i3;
            this.threshold = (int) (((float) i2) * this.loadFactor);
        }
        return null;
    }

    public T remove(short s) {
        short s2 = this.mask & s;
        Entry entry = this.table[s2];
        Entry entry2 = entry;
        while (entry != null) {
            Entry entry3 = entry.next;
            if (entry.key == s) {
                this.size--;
                if (entry2 == entry) {
                    this.table[s2] = entry3;
                } else {
                    entry2.next = entry3;
                }
                return entry.value;
            }
            entry2 = entry;
            entry = entry3;
        }
        return null;
    }

    public int size() {
        return this.size;
    }

    public void clear() {
        Entry[] entryArr = this.table;
        int length = entryArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                entryArr[length] = null;
            } else {
                this.size = 0;
                return;
            }
        }
    }

    static class Entry {
        final short key;
        Entry next;
        Object value;

        Entry(short s, Object obj, Entry entry) {
            this.key = s;
            this.value = obj;
            this.next = entry;
        }
    }
}
