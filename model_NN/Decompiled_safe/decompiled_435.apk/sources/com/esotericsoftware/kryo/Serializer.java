package com.esotericsoftware.kryo;

import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;

public abstract class Serializer {
    private static final byte NOT_NULL_OBJECT = 1;
    private static final byte NULL_OBJECT = 0;
    private boolean canBeNull = true;

    public abstract <T> T readObjectData(ByteBuffer byteBuffer, Class<T> cls);

    public abstract void writeObjectData(ByteBuffer byteBuffer, Object obj);

    public void setCanBeNull(boolean z) {
        this.canBeNull = z;
    }

    public final void writeObject(ByteBuffer byteBuffer, Object obj) {
        if (this.canBeNull) {
            if (obj == null) {
                if (Log.TRACE) {
                    Log.trace("kryo", "Wrote object: null");
                }
                byteBuffer.put((byte) 0);
                return;
            }
            byteBuffer.put((byte) NOT_NULL_OBJECT);
        }
        writeObjectData(byteBuffer, obj);
    }

    public final <T> T readObject(ByteBuffer byteBuffer, Class<T> cls) {
        if (!this.canBeNull || byteBuffer.get() != 0) {
            return readObjectData(byteBuffer, cls);
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Read object: null");
        }
        return null;
    }

    public <T> T newInstance(Kryo kryo, Class<T> cls) {
        return kryo.newInstance(cls);
    }

    public boolean isFinal(Class cls) {
        return Kryo.isFinal(cls);
    }
}
