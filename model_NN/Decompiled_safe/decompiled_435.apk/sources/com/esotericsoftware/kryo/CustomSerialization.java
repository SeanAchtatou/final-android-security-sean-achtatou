package com.esotericsoftware.kryo;

import java.nio.ByteBuffer;

public interface CustomSerialization {
    void readObjectData(Kryo kryo, ByteBuffer byteBuffer);

    void writeObjectData(Kryo kryo, ByteBuffer byteBuffer);
}
