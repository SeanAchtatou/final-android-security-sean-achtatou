package com.esotericsoftware.kryo;

import com.esotericsoftware.minlog.Log;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import org.objectweb.asm.Opcodes;

public class Context {
    private final ArrayList<ByteBuffer> buffers = new ArrayList<>(2);
    private byte[] byteArray;
    private char[] charArray = new char[Opcodes.ACC_NATIVE];
    private int[] intArray;
    private HashMap<Object, Object> map;
    int objectGraphLevel;
    private int remoteEntityID;
    private final SerializerKey tempKey = new SerializerKey(null, null);
    private HashMap<Object, Object> tempMap;

    public ByteBuffer getBuffer(int i) {
        ByteBuffer byteBuffer = this.buffers.isEmpty() ? null : this.buffers.get(0);
        if (byteBuffer == null || byteBuffer.capacity() < i) {
            return ByteBuffer.allocate(i);
        }
        byteBuffer.clear();
        return byteBuffer;
    }

    public ArrayList<ByteBuffer> getBuffers(int i, int i2) {
        int min = Math.min(i, this.buffers.size());
        int i3 = 0;
        while (i3 < min) {
            if (this.buffers.get(i3).capacity() < i2) {
                this.buffers.set(i3, ByteBuffer.allocate(i2));
            }
            i3++;
        }
        for (int i4 = i3; i4 < i; i4++) {
            this.buffers.add(ByteBuffer.allocate(i2));
        }
        return this.buffers;
    }

    public char[] getCharArray(int i) {
        if (this.charArray.length < i) {
            this.charArray = new char[i];
        }
        return this.charArray;
    }

    public int[] getIntArray(int i) {
        if (this.intArray == null || this.intArray.length < i) {
            this.intArray = new int[i];
        }
        return this.intArray;
    }

    public byte[] getByteArray(int i) {
        if (this.byteArray == null || this.byteArray.length < i) {
            this.byteArray = new byte[i];
        }
        return this.byteArray;
    }

    public void put(String str, Object obj) {
        if (this.map == null) {
            this.map = new HashMap<>();
        }
        this.map.put(str, obj);
    }

    public Object get(String str) {
        if (this.map == null) {
            this.map = new HashMap<>();
        }
        return this.map.get(str);
    }

    public void put(Serializer serializer, String str, Object obj) {
        if (this.map == null) {
            this.map = new HashMap<>();
        }
        this.map.put(new SerializerKey(serializer, str), obj);
    }

    public Object get(Serializer serializer, String str) {
        if (this.map == null) {
            this.map = new HashMap<>();
        }
        this.tempKey.serializer = serializer;
        this.tempKey.key = str;
        return this.map.get(this.tempKey);
    }

    public void putTemp(String str, Object obj) {
        if (this.tempMap == null) {
            this.tempMap = new HashMap<>();
        }
        this.tempMap.put(str, obj);
    }

    public Object getTemp(String str) {
        if (this.tempMap == null) {
            this.tempMap = new HashMap<>();
        }
        return this.tempMap.get(str);
    }

    public void putTemp(Serializer serializer, String str, Object obj) {
        if (this.tempMap == null) {
            this.tempMap = new HashMap<>();
        }
        this.tempMap.put(new SerializerKey(serializer, str), obj);
    }

    public Object getTemp(Serializer serializer, String str) {
        if (this.tempMap == null) {
            this.tempMap = new HashMap<>();
        }
        this.tempKey.serializer = serializer;
        this.tempKey.key = str;
        return this.tempMap.get(this.tempKey);
    }

    public void reset() {
        if (this.tempMap != null) {
            this.tempMap.clear();
        }
        if (Log.TRACE) {
            Log.trace("kryo", "Context reset.");
        }
    }

    public int getRemoteEntityID() {
        return this.remoteEntityID;
    }

    public void setRemoteEntityID(int i) {
        this.remoteEntityID = i;
    }

    private static class SerializerKey {
        String key;
        Serializer serializer;

        public SerializerKey(Serializer serializer2, String str) {
            this.serializer = serializer2;
            this.key = str;
        }

        public int hashCode() {
            return ((this.key.hashCode() + 31) * 31) + this.serializer.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            SerializerKey serializerKey = (SerializerKey) obj;
            if (!this.key.equals(serializerKey.key)) {
                return false;
            }
            return this.serializer.equals(serializerKey.serializer);
        }
    }
}
