package com.esotericsoftware.reflectasm;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public abstract class MethodAccess {
    private Method[] methods;

    public abstract Object invoke(Object obj, int i, Object... objArr);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static MethodAccess get(Class cls) {
        Class<?> cls2;
        AccessClassLoader accessClassLoader = new AccessClassLoader(cls.getClassLoader());
        ArrayList arrayList = new ArrayList();
        for (Class cls3 = cls; cls3 != Object.class; cls3 = cls3.getSuperclass()) {
            for (Method method : cls3.getDeclaredMethods()) {
                int modifiers = method.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isPrivate(modifiers)) {
                    arrayList.add(method);
                }
            }
        }
        String name = cls.getName();
        String str = name + "MethodAccess";
        Class<?> cls4 = null;
        try {
            cls4 = accessClassLoader.loadClass(str);
        } catch (ClassNotFoundException e) {
        }
        if (cls4 == null) {
            String replace = str.replace('.', '/');
            String replace2 = name.replace('.', '/');
            ClassWriter classWriter = new ClassWriter(0);
            classWriter.visit(Opcodes.V1_1, 1, replace, null, "com/esotericsoftware/reflectasm/MethodAccess", null);
            MethodVisitor visitMethod = classWriter.visitMethod(1, "<init>", "()V", null, null);
            visitMethod.visitCode();
            visitMethod.visitVarInsn(25, 0);
            visitMethod.visitMethodInsn(Opcodes.INVOKESPECIAL, "com/esotericsoftware/reflectasm/MethodAccess", "<init>", "()V");
            visitMethod.visitInsn(Opcodes.RETURN);
            visitMethod.visitMaxs(1, 1);
            visitMethod.visitEnd();
            MethodVisitor visitMethod2 = classWriter.visitMethod(Opcodes.LOR, "invoke", "(Ljava/lang/Object;I[Ljava/lang/Object;)Ljava/lang/Object;", null, null);
            visitMethod2.visitCode();
            visitMethod2.visitVarInsn(25, 1);
            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, replace2);
            visitMethod2.visitVarInsn(58, 4);
            visitMethod2.visitVarInsn(21, 2);
            Label[] labelArr = new Label[arrayList.size()];
            int length = labelArr.length;
            for (int i = 0; i < length; i++) {
                labelArr[i] = new Label();
            }
            Label label = new Label();
            visitMethod2.visitTableSwitchInsn(0, labelArr.length - 1, label, labelArr);
            StringBuilder sb = new StringBuilder(128);
            int length2 = labelArr.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length2) {
                visitMethod2.visitLabel(labelArr[i2]);
                if (i2 == 0) {
                    visitMethod2.visitFrame(1, 1, new Object[]{replace2}, 0, null);
                } else {
                    visitMethod2.visitFrame(3, 0, null, 0, null);
                }
                visitMethod2.visitVarInsn(25, 4);
                sb.setLength(0);
                sb.append('(');
                Method method2 = (Method) arrayList.get(i2);
                Class<?>[] parameterTypes = method2.getParameterTypes();
                int max = Math.max(i3, parameterTypes.length);
                for (int i4 = 0; i4 < parameterTypes.length; i4++) {
                    visitMethod2.visitVarInsn(25, 3);
                    visitMethod2.visitIntInsn(16, i4);
                    visitMethod2.visitInsn(50);
                    Type type = Type.getType(parameterTypes[i4]);
                    switch (type.getSort()) {
                        case 1:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Boolean");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z");
                            break;
                        case 2:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Character");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C");
                            break;
                        case 3:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Byte");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Byte", "byteValue", "()B");
                            break;
                        case 4:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Short");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S");
                            break;
                        case 5:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Integer");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I");
                            break;
                        case 6:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Float");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Float", "floatValue", "()F");
                            break;
                        case 7:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Long");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Long", "longValue", "()J");
                            break;
                        case 8:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Double");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D");
                            break;
                        case 9:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, type.getDescriptor());
                            break;
                        case 10:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, type.getInternalName());
                            break;
                    }
                    sb.append(type.getDescriptor());
                }
                sb.append(')');
                sb.append(Type.getDescriptor(method2.getReturnType()));
                visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, replace2, method2.getName(), sb.toString());
                switch (Type.getType(method2.getReturnType()).getSort()) {
                    case 0:
                        visitMethod2.visitInsn(1);
                        break;
                    case 1:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
                        break;
                    case 2:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
                        break;
                    case 3:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
                        break;
                    case 4:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
                        break;
                    case 5:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                        break;
                    case 6:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
                        break;
                    case 7:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
                        break;
                    case 8:
                        visitMethod2.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
                        break;
                }
                visitMethod2.visitInsn(Opcodes.ARETURN);
                i2++;
                i3 = max;
            }
            visitMethod2.visitLabel(label);
            visitMethod2.visitFrame(3, 0, null, 0, null);
            visitMethod2.visitTypeInsn(Opcodes.NEW, "java/lang/IllegalArgumentException");
            visitMethod2.visitInsn(89);
            visitMethod2.visitTypeInsn(Opcodes.NEW, "java/lang/StringBuilder");
            visitMethod2.visitInsn(89);
            visitMethod2.visitLdcInsn("Method not found: ");
            visitMethod2.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V");
            visitMethod2.visitVarInsn(21, 2);
            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
            visitMethod2.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
            visitMethod2.visitInsn(Opcodes.ATHROW);
            visitMethod2.visitMaxs(Math.max(5, i3 + 2), 5);
            visitMethod2.visitEnd();
            classWriter.visitEnd();
            cls2 = accessClassLoader.defineClass(str, classWriter.toByteArray());
        } else {
            cls2 = cls4;
        }
        try {
            MethodAccess methodAccess = (MethodAccess) cls2.newInstance();
            methodAccess.methods = (Method[]) arrayList.toArray(new Method[arrayList.size()]);
            return methodAccess;
        } catch (Exception e2) {
            throw new RuntimeException("Error constructing method access class: " + str, e2);
        }
    }

    public Object invoke(Object obj, String str, Object... objArr) {
        return invoke(obj, getIndex(str), objArr);
    }

    public int getIndex(String str) {
        int length = this.methods.length;
        for (int i = 0; i < length; i++) {
            if (this.methods[i].getName().equals(str)) {
                return i;
            }
        }
        throw new IllegalArgumentException("Unable to find public method: " + str);
    }

    public int getIndex(String str, Class... clsArr) {
        int length = this.methods.length;
        for (int i = 0; i < length; i++) {
            Method method = this.methods[i];
            if (method.getName().equals(str) && Arrays.equals(clsArr, method.getParameterTypes())) {
                return i;
            }
        }
        throw new IllegalArgumentException("Unable to find public method: " + str + " " + Arrays.toString(clsArr));
    }
}
