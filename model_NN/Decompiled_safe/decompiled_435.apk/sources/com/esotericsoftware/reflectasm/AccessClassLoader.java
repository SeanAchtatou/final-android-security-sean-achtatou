package com.esotericsoftware.reflectasm;

import java.lang.reflect.Method;

class AccessClassLoader extends ClassLoader {
    AccessClassLoader(ClassLoader classLoader) {
        super(classLoader);
    }

    /* access modifiers changed from: protected */
    public synchronized Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
        Class<?> loadClass;
        if (str.equals(FieldAccess.class.getName())) {
            loadClass = FieldAccess.class;
        } else if (str.equals(MethodAccess.class.getName())) {
            loadClass = MethodAccess.class;
        } else {
            loadClass = super.loadClass(str, z);
        }
        return loadClass;
    }

    /* access modifiers changed from: package-private */
    public Class<?> defineClass(String str, byte[] bArr) throws ClassFormatError {
        Class<ClassLoader> cls = ClassLoader.class;
        try {
            Method declaredMethod = cls.getDeclaredMethod("defineClass", String.class, byte[].class, Integer.TYPE, Integer.TYPE);
            declaredMethod.setAccessible(true);
            return (Class) declaredMethod.invoke(getParent(), str, bArr, new Integer(0), new Integer(bArr.length));
        } catch (Exception e) {
            return this.defineClass(str, bArr, 0, bArr.length);
        }
    }
}
