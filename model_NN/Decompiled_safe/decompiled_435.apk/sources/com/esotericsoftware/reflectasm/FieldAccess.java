package com.esotericsoftware.reflectasm;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public abstract class FieldAccess {
    private Field[] fields;

    public abstract Object get(Object obj, int i);

    public abstract void set(Object obj, int i, Object obj2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static FieldAccess get(Class cls) {
        Class<?> cls2;
        AccessClassLoader accessClassLoader = new AccessClassLoader(cls.getClassLoader());
        ArrayList arrayList = new ArrayList();
        for (Class cls3 = cls; cls3 != Object.class; cls3 = cls3.getSuperclass()) {
            for (Field field : cls3.getDeclaredFields()) {
                int modifiers = field.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isPrivate(modifiers)) {
                    arrayList.add(field);
                }
            }
        }
        String name = cls.getName();
        String str = name + "FieldAccess";
        Class<?> cls4 = null;
        try {
            cls4 = accessClassLoader.loadClass(str);
        } catch (ClassNotFoundException e) {
        }
        if (cls4 == null) {
            String replace = str.replace('.', '/');
            String replace2 = name.replace('.', '/');
            ClassWriter classWriter = new ClassWriter(0);
            classWriter.visit(Opcodes.V1_1, 1, replace, null, "com/esotericsoftware/reflectasm/FieldAccess", null);
            MethodVisitor visitMethod = classWriter.visitMethod(1, "<init>", "()V", null, null);
            visitMethod.visitCode();
            visitMethod.visitVarInsn(25, 0);
            visitMethod.visitMethodInsn(Opcodes.INVOKESPECIAL, "com/esotericsoftware/reflectasm/FieldAccess", "<init>", "()V");
            visitMethod.visitInsn(Opcodes.RETURN);
            visitMethod.visitMaxs(1, 1);
            visitMethod.visitEnd();
            MethodVisitor visitMethod2 = classWriter.visitMethod(1, "set", "(Ljava/lang/Object;ILjava/lang/Object;)V", null, null);
            visitMethod2.visitCode();
            visitMethod2.visitVarInsn(21, 2);
            Label[] labelArr = new Label[arrayList.size()];
            int length = labelArr.length;
            for (int i = 0; i < length; i++) {
                labelArr[i] = new Label();
            }
            Label label = new Label();
            visitMethod2.visitTableSwitchInsn(0, labelArr.length - 1, label, labelArr);
            int i2 = 0;
            int length2 = labelArr.length;
            while (true) {
                int i3 = i2;
                if (i3 < length2) {
                    Field field2 = (Field) arrayList.get(i3);
                    Type type = Type.getType(field2.getType());
                    visitMethod2.visitLabel(labelArr[i3]);
                    visitMethod2.visitFrame(3, 0, null, 0, null);
                    visitMethod2.visitVarInsn(25, 1);
                    visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, replace2);
                    visitMethod2.visitVarInsn(25, 3);
                    switch (type.getSort()) {
                        case 1:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Boolean");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z");
                            break;
                        case 2:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Character");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C");
                            break;
                        case 3:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Byte");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Byte", "byteValue", "()B");
                            break;
                        case 4:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Short");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S");
                            break;
                        case 5:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Integer");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I");
                            break;
                        case 6:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Float");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Float", "floatValue", "()F");
                            break;
                        case 7:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Long");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Long", "longValue", "()J");
                            break;
                        case 8:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Double");
                            visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Double", "doubleValue", "()D");
                            break;
                        case 9:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, type.getDescriptor());
                            break;
                        case 10:
                            visitMethod2.visitTypeInsn(Opcodes.CHECKCAST, type.getInternalName());
                            break;
                    }
                    visitMethod2.visitFieldInsn(Opcodes.PUTFIELD, replace2, field2.getName(), type.getDescriptor());
                    visitMethod2.visitInsn(Opcodes.RETURN);
                    i2 = i3 + 1;
                } else {
                    visitMethod2.visitLabel(label);
                    visitMethod2.visitFrame(3, 0, null, 0, null);
                    visitMethod2.visitTypeInsn(Opcodes.NEW, "java/lang/IllegalArgumentException");
                    visitMethod2.visitInsn(89);
                    visitMethod2.visitTypeInsn(Opcodes.NEW, "java/lang/StringBuilder");
                    visitMethod2.visitInsn(89);
                    visitMethod2.visitLdcInsn("Field not found: ");
                    visitMethod2.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V");
                    visitMethod2.visitVarInsn(21, 2);
                    visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
                    visitMethod2.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
                    visitMethod2.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
                    visitMethod2.visitInsn(Opcodes.ATHROW);
                    visitMethod2.visitMaxs(5, 4);
                    visitMethod2.visitEnd();
                    MethodVisitor visitMethod3 = classWriter.visitMethod(1, "get", "(Ljava/lang/Object;I)Ljava/lang/Object;", null, null);
                    visitMethod3.visitCode();
                    visitMethod3.visitVarInsn(21, 2);
                    Label[] labelArr2 = new Label[arrayList.size()];
                    int length3 = labelArr2.length;
                    for (int i4 = 0; i4 < length3; i4++) {
                        labelArr2[i4] = new Label();
                    }
                    Label label2 = new Label();
                    visitMethod3.visitTableSwitchInsn(0, labelArr2.length - 1, label2, labelArr2);
                    int i5 = 0;
                    int length4 = labelArr2.length;
                    while (true) {
                        int i6 = i5;
                        if (i6 < length4) {
                            Field field3 = (Field) arrayList.get(i6);
                            visitMethod3.visitLabel(labelArr2[i6]);
                            visitMethod3.visitFrame(3, 0, null, 0, null);
                            visitMethod3.visitVarInsn(25, 1);
                            visitMethod3.visitTypeInsn(Opcodes.CHECKCAST, replace2);
                            visitMethod3.visitFieldInsn(Opcodes.GETFIELD, replace2, field3.getName(), Type.getDescriptor(field3.getType()));
                            switch (Type.getType(field3.getType()).getSort()) {
                                case 1:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
                                    break;
                                case 2:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
                                    break;
                                case 3:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
                                    break;
                                case 4:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
                                    break;
                                case 5:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
                                    break;
                                case 6:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
                                    break;
                                case 7:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
                                    break;
                                case 8:
                                    visitMethod3.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
                                    break;
                            }
                            visitMethod3.visitInsn(Opcodes.ARETURN);
                            i5 = i6 + 1;
                        } else {
                            visitMethod3.visitLabel(label2);
                            visitMethod3.visitFrame(3, 0, null, 0, null);
                            visitMethod3.visitTypeInsn(Opcodes.NEW, "java/lang/IllegalArgumentException");
                            visitMethod3.visitInsn(89);
                            visitMethod3.visitTypeInsn(Opcodes.NEW, "java/lang/StringBuilder");
                            visitMethod3.visitInsn(89);
                            visitMethod3.visitLdcInsn("Field not found: ");
                            visitMethod3.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "(Ljava/lang/String;)V");
                            visitMethod3.visitVarInsn(21, 2);
                            visitMethod3.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
                            visitMethod3.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");
                            visitMethod3.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V");
                            visitMethod3.visitInsn(Opcodes.ATHROW);
                            visitMethod3.visitMaxs(5, 3);
                            visitMethod3.visitEnd();
                            classWriter.visitEnd();
                            cls2 = accessClassLoader.defineClass(str, classWriter.toByteArray());
                        }
                    }
                }
            }
        } else {
            cls2 = cls4;
        }
        try {
            FieldAccess fieldAccess = (FieldAccess) cls2.newInstance();
            fieldAccess.fields = (Field[]) arrayList.toArray(new Field[arrayList.size()]);
            return fieldAccess;
        } catch (Exception e2) {
            throw new RuntimeException("Error constructing field access class: " + str, e2);
        }
    }

    public int getIndex(String str) {
        int length = this.fields.length;
        for (int i = 0; i < length; i++) {
            if (this.fields[i].getName().equals(str)) {
                return i;
            }
        }
        throw new IllegalArgumentException("Unable to find public field: " + str);
    }

    public void set(Object obj, String str, Object obj2) {
        set(obj, getIndex(str), obj2);
    }

    public Object get(Object obj, String str) {
        return get(obj, getIndex(str));
    }
}
