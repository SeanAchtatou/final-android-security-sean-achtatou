package com.mocelet.fourinrow;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class RiddleCategoryListActivity extends Activity {
    public static final int RC_DONT_CONNECT_FOUR = 1;
    public static final int RC_DONT_CONNECT_FOUR_2P = 2;
    public static final int RC_FILL_ALL = 3;
    public static final int RC_FILL_ALL_2P = 4;
    public static final int RC_MOVES_A = 11;
    public static final int RC_MOVES_B = 12;
    public static final int RC_MOVES_C = 13;
    public static final int RC_MOVES_D = 14;
    public static final int RC_MOVES_E = 15;
    public static final int RC_MOVES_F = 16;
    public static final int RC_MOVES_G = 17;
    private AdView adView;
    private RiddleCategoryInfo[] categories;
    private ArrayAdapter listAdapter;
    private boolean showingAds;
    private boolean solidBlackBackground;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.categories = new RiddleCategoryInfo[]{new RiddleCategoryInfo(0, getString(R.string.riddles_category_riddles), getString(R.string.riddles_category_riddles_desc), 0, 0, true), new RiddleCategoryInfo(11, getString(R.string.riddles_riddle_killer_move), getString(R.string.riddles_riddle_killer_move_desc), 0, 0, false), new RiddleCategoryInfo(12, getString(R.string.riddles_riddle_killer_dance), getString(R.string.riddles_riddle_killer_dance_desc), 0, 0, false), new RiddleCategoryInfo(13, getString(R.string.riddles_riddle_killer_vals), getString(R.string.riddles_riddle_killer_vals_desc), 0, 0, false), new RiddleCategoryInfo(14, getString(R.string.riddles_riddle_killer_black_swan), getString(R.string.riddles_riddle_killer_black_swan_desc), 0, 0, false), new RiddleCategoryInfo(15, getString(R.string.riddles_riddle_killer_tango), getString(R.string.riddles_riddle_killer_tango_desc), 0, 0, false), new RiddleCategoryInfo(16, getString(R.string.riddles_riddle_claustrophobia), getString(R.string.riddles_riddle_claustrophobia_desc), 0, 0, false), new RiddleCategoryInfo(0, getString(R.string.riddles_category_game_variations), getString(R.string.riddles_category_game_variations_desc), 0, 0, true), new RiddleCategoryInfo(1, getString(R.string.riddles_variation_dontconnectfour), getString(R.string.riddles_variation_dontconnectfour_desc), 0, 0, false), new RiddleCategoryInfo(2, getString(R.string.riddles_variation_dontconnectfour_2p), getString(R.string.riddles_variation_dontconnectfour_2p_desc), 0, 0, false), new RiddleCategoryInfo(3, getString(R.string.riddles_variation_fill_all), getString(R.string.riddles_variation_fill_all_desc), 0, 0, false), new RiddleCategoryInfo(4, getString(R.string.riddles_variation_fill_all_2p), getString(R.string.riddles_variation_fill_all_2p_desc), 0, 0, false)};
        setTitle((int) R.string.riddles_category_activity_title);
        setContentView((int) R.layout.riddle_list_layout);
        ListView lv = (ListView) findViewById(R.id.riddle_list_view);
        this.listAdapter = new RiddleCategoryListAdapter(this, this.categories);
        lv.setAdapter((ListAdapter) this.listAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                RiddleCategoryInfo category = (RiddleCategoryInfo) view.getTag();
                if (!category.isSeparator) {
                    Intent intent = null;
                    switch (category.categoryId) {
                        case 1:
                            intent = new Intent(RiddleCategoryListActivity.this, InGameVariationActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.PlayTag", InGameVariationActivity.VS_MACHINE_PLAY_TAG);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleTypeTag", 1);
                            break;
                        case 2:
                            intent = new Intent(RiddleCategoryListActivity.this, InGameVariationActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.PlayTag", InGameVariationActivity.VS_HUMAN_PLAY_TAG);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleTypeTag", 1);
                            break;
                        case 3:
                            intent = new Intent(RiddleCategoryListActivity.this, InGameVariationActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.PlayTag", InGameVariationActivity.VS_MACHINE_PLAY_TAG);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleTypeTag", 2);
                            break;
                        case 4:
                            intent = new Intent(RiddleCategoryListActivity.this, InGameVariationActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.PlayTag", InGameVariationActivity.VS_HUMAN_PLAY_TAG);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleTypeTag", 2);
                            break;
                        case 11:
                            intent = new Intent(RiddleCategoryListActivity.this, RiddleSelectorListActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag", "A");
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag", category.category);
                            break;
                        case 12:
                            intent = new Intent(RiddleCategoryListActivity.this, RiddleSelectorListActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag", "B");
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag", category.category);
                            break;
                        case 13:
                            intent = new Intent(RiddleCategoryListActivity.this, RiddleSelectorListActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag", "C");
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag", category.category);
                            break;
                        case 14:
                            intent = new Intent(RiddleCategoryListActivity.this, RiddleSelectorListActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag", "D");
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag", category.category);
                            break;
                        case 15:
                            intent = new Intent(RiddleCategoryListActivity.this, RiddleSelectorListActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag", "E");
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag", category.category);
                            break;
                        case 16:
                            intent = new Intent(RiddleCategoryListActivity.this, RiddleSelectorListActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag", "F");
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag", category.category);
                            break;
                        case 17:
                            intent = new Intent(RiddleCategoryListActivity.this, RiddleSelectorListActivity.class);
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag", "G");
                            intent.putExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag", category.category);
                            break;
                    }
                    if (intent != null) {
                        RiddleCategoryListActivity.this.startActivity(intent);
                    }
                }
            }
        });
        this.adView = (AdView) findViewById(R.id.adView);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.solidBlackBackground = prefs.getBoolean("black_background", false);
        RiddleManager rm = new RiddleManager();
        rm.loadFromPrefs(prefs);
        for (RiddleCategoryInfo cat : this.categories) {
            String categoryLetter = null;
            if (cat.categoryId == 11) {
                categoryLetter = "A";
            }
            if (cat.categoryId == 12) {
                categoryLetter = "B";
            }
            if (cat.categoryId == 13) {
                categoryLetter = "C";
            }
            if (cat.categoryId == 14) {
                categoryLetter = "D";
            }
            if (cat.categoryId == 15) {
                categoryLetter = "E";
            }
            if (cat.categoryId == 16) {
                categoryLetter = "F";
            }
            if (cat.categoryId == 17) {
                categoryLetter = "G";
            }
            if (categoryLetter != null) {
                cat.riddleCount = RiddlesDepot.getRiddleCount(categoryLetter);
                cat.riddlesDone = rm.getRiddlesDoneCount(categoryLetter);
                if (cat.riddlesDone > cat.riddleCount) {
                    cat.riddlesDone = cat.riddleCount;
                }
            }
        }
        this.listAdapter.notifyDataSetChanged();
        setTiledBackground();
    }

    private void setTiledBackground() {
        if (this.solidBlackBackground) {
            getWindow().setBackgroundDrawableResource(R.drawable.black_background);
            return;
        }
        BitmapDrawable tiledBg = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.tile_black_stripes));
        tiledBg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        getWindow().setBackgroundDrawable(tiledBg);
    }

    private void hideAds() {
        if (this.adView != null) {
            this.adView.setVisibility(8);
            this.showingAds = false;
        }
    }

    private void showAds() {
        if (!this.showingAds && this.adView != null) {
            AdRequest request = new AdRequest();
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            this.adView.setVisibility(0);
            this.adView.loadAd(request);
            this.showingAds = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.adView != null) {
            this.adView.destroy();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent intent = new Intent(this, CuatroEnLinea.class);
                intent.addFlags(67108864);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
