package com.mocelet.fourinrow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class RiddleCategoryListAdapter extends ArrayAdapter {
    public RiddleCategoryListAdapter(Context context, RiddleCategoryInfo[] items) {
        super(context, (int) R.layout.riddle_list_row, items);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            row = LayoutInflater.from(getContext()).inflate((int) R.layout.riddle_list_row, parent, false);
        }
        RiddleCategoryInfo category = (RiddleCategoryInfo) getItem(position);
        row.setTag(category);
        ((TextView) row.findViewById(R.id.label)).setText(category.category);
        TextView descriptionView = (TextView) row.findViewById(R.id.description);
        if (category.description == null || category.description.length() <= 0) {
            descriptionView.setVisibility(4);
        } else {
            descriptionView.setText(category.description);
            descriptionView.setVisibility(0);
        }
        if (category.isSeparator) {
            row.setBackgroundResource(R.drawable.blue_button);
        } else {
            row.setBackgroundResource(R.drawable.translucent_black_button);
        }
        TextView progressText = (TextView) row.findViewById(R.id.progress_text);
        ProgressBar progressBar = (ProgressBar) row.findViewById(R.id.progress_bar);
        if (category.riddleCount < 1) {
            progressText.setVisibility(8);
            progressBar.setVisibility(8);
        } else {
            progressBar.setMax(category.riddleCount);
            progressBar.setProgress(category.riddlesDone);
            progressText.setText(String.valueOf(category.riddlesDone) + "/" + category.riddleCount);
            progressText.setVisibility(0);
            progressBar.setVisibility(0);
        }
        return row;
    }
}
