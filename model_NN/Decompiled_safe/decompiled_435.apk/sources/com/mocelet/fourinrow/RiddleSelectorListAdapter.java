package com.mocelet.fourinrow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RiddleSelectorListAdapter extends ArrayAdapter {
    private String riddlePrefix;
    private boolean switchColors;
    private int theme;

    public RiddleSelectorListAdapter(Context context, RiddleInfo[] items, String riddlePrefix2, int theme2, boolean switchColors2) {
        super(context, (int) R.layout.riddle_selector_list_row, items);
        this.riddlePrefix = riddlePrefix2;
        this.theme = theme2;
        this.switchColors = switchColors2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            row = LayoutInflater.from(getContext()).inflate((int) R.layout.riddle_selector_list_row, parent, false);
        }
        RiddleInfo riddle = (RiddleInfo) getItem(position);
        ((TextView) row.findViewById(R.id.riddleLabel)).setText(riddle.id);
        row.setTag(riddle);
        row.setBackgroundResource(R.drawable.translucent_black_button);
        TextView infoLabel = (TextView) row.findViewById(R.id.riddleInfoLabel);
        MiniBoardView board = (MiniBoardView) row.findViewById(R.id.riddleBoard);
        GameState gs = new GameState();
        gs.restoreGame(riddle.rows, riddle.cols, riddle.initialMoves, riddle.firstPlayer);
        board.setGameState(gs);
        board.setTheme(this.theme);
        board.setSwitchColors(this.switchColors);
        if (riddle.stat == null) {
            infoLabel.setText((int) R.string.riddles_riddle_info_not_solved);
        } else if (riddle.stat.completed) {
            infoLabel.setText((int) R.string.riddles_riddle_info_solved);
            row.setBackgroundResource(R.drawable.riddle_solved_background);
        } else {
            infoLabel.setText((int) R.string.riddles_riddle_info_not_solved);
        }
        return row;
    }
}
