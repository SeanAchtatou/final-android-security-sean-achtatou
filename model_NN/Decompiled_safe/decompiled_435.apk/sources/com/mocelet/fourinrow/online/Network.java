package com.mocelet.fourinrow.online;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

public class Network {
    public static final String AUTO_INVITE_METHOD = "auto_invite";
    public static final int BAD_REQUEST_RESPONSE = 400;
    public static final int BUSY_RESPONSE = 486;
    public static final String BYE_METHOD = "bye";
    public static final String CANCEL_METHOD = "cancel";
    public static final int FORBIDDEN_RESPONSE = 403;
    public static final String GET_CONNECTED_COUNT_METHOD = "get_connected_count";
    public static final String INFO_METHOD = "info";
    public static final String INVITE_METHOD = "invite";
    public static final String IN_WAITING_ROOM_END_METHOD = "waiting_end";
    public static final String IN_WAITING_ROOM_START_METHOD = "waiting_start";
    public static final int MAINTENANCE_RESPONSE = 601;
    public static final String MATCHMAKING_END_METHOD = "matchmaking_end";
    public static final String MATCHMAKING_START_METHOD = "matchmaking_start";
    public static final int NOT_IMPLEMENTED_RESPONSE = 501;
    public static final int OK_RESPONSE = 200;
    public static final String PING_METHOD = "ping";
    public static final String PLAYING_END_METHOD = "playing_end";
    public static final String PLAYING_START_METHOD = "playing_start";
    public static final String REGISTER_METHOD = "register";
    public static final int SERVER_ERROR_RESPONSE = 500;
    public static final String SERVER_HOST = "services.mocelet.com";
    public static final int SERVER_PORT = 33579;
    public static final int TOO_MANY_USERS_RESPONSE = 600;
    public static final int TRYING_RESPONSE = 100;
    public static final int UPDATE_NEEDED_RESPONSE = 406;
    public static final int USER_NOT_FOUND_RESPONSE = 404;

    public static class GenericRequest {
        public String alias;
        public String appId;
        public String[] attributes;
        public String from;
        public String hash;
        public String method;
        public String sessionId;
        public String to;
        public int version;
    }

    public static class GenericResponse {
        public String alias;
        public String appId;
        public String[] attributes;
        public int code;
        public String from;
        public String hash;
        public String method;
        public String sessionId;
        public String to;
        public int version;
    }

    public static void register(EndPoint endPoint) {
        Kryo kryo = endPoint.getKryo();
        kryo.register(GenericRequest.class);
        kryo.register(GenericResponse.class);
        kryo.register(String[].class);
    }
}
