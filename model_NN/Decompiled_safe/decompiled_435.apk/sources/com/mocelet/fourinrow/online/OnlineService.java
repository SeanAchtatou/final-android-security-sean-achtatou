package com.mocelet.fourinrow.online;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.mocelet.fourinrow.online.Network;
import com.mocelet.fourinrow.online.NetworkKryoManager;

public class OnlineService extends Service implements Handler.Callback {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates;
    private static final boolean DEBUG = false;
    private boolean connected;
    private boolean connecting;
    private DisconnectionReasons disconnectionReason;
    private OnlineServiceListener listener;
    private LocalBinder mBinder = new LocalBinder();
    /* access modifiers changed from: private */
    public NetworkKryoManager networkManager;
    private boolean started;

    public enum ConnectionProgressStates {
        ESTABLISHING,
        ESTABLISHED,
        LOGGING
    }

    public enum DisconnectionReasons {
        CONNECTION_FAILED,
        CONNECTION_LOST,
        UPDATE_NEEDED,
        TOO_MANY_USERS,
        MAINTENANCE,
        SERVER_ERROR,
        REGISTRATION_FAILED,
        DISCONNECTED
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates;
        if (iArr == null) {
            iArr = new int[NetworkKryoManager.ConnectionStates.values().length];
            try {
                iArr[NetworkKryoManager.ConnectionStates.CONNECTED.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.CONNECTING.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.CONNECTION_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.CONNECTION_LOST.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.DISCONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.MAINTENANCE.ordinal()] = 10;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.REGISTERED.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.REGISTERING.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.REGISTRATION_FAILED.ordinal()] = 12;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.SERVER_ERROR.ordinal()] = 11;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.TOO_MANY_USERS.ordinal()] = 9;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[NetworkKryoManager.ConnectionStates.UPDATE_NEEDED.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates = iArr;
        }
        return iArr;
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public OnlineService getService() {
            return OnlineService.this;
        }
    }

    public void onCreate() {
        super.onCreate();
        this.networkManager = NetworkKryoManager.getInstance(new Handler(this));
        this.started = false;
        this.connected = false;
        this.connecting = false;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    public void onDestroy() {
        super.onDestroy();
        logout();
        this.listener = null;
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    private void processDisconnection(DisconnectionReasons reason) {
        this.disconnectionReason = reason;
        this.connected = false;
        this.connecting = false;
        if (this.listener != null) {
            this.listener.onOnlineDisconnected(this.disconnectionReason);
        }
    }

    private void processConnectionUpdate(ConnectionProgressStates state) {
        this.connected = false;
        this.connecting = true;
        if (this.listener != null) {
            this.listener.onOnlineConnectionProgress(state);
        }
    }

    private void processConnected() {
        this.connected = true;
        this.connecting = false;
        if (this.listener != null) {
            this.listener.onOnlineConnected();
        }
    }

    private void processRequest(Network.GenericRequest r) {
        if (this.listener != null) {
            this.listener.onOnlineRequestReceived(r);
        }
    }

    private void processResponse(Network.GenericResponse r) {
        if (this.listener != null) {
            this.listener.onOnlineResponseReceived(r);
        }
    }

    private void processStatsUpdate() {
        if (this.listener != null) {
            this.listener.onOnlineStatsUpdate();
        }
    }

    public void setListener(OnlineServiceListener listener2) {
        this.listener = listener2;
    }

    public void login(String uuid, String alias, String appVersion, String stats) {
        if (!this.connected) {
            new AsyncTask<String, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(String... params) {
                    OnlineService.this.networkManager.connectAndRegister(params[0], params[1], params[2], params[3]);
                    return null;
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Void result) {
                    super.onPostExecute((Object) result);
                }
            }.execute(uuid, alias, appVersion, stats);
        } else if (this.listener != null) {
            this.listener.onOnlineConnected();
        }
    }

    public void logout() {
        if (this.connected) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    OnlineService.this.networkManager.disconnect();
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    public int getConnectedCount() {
        return this.networkManager.getConnectedCount();
    }

    public String getPublicId() {
        return this.networkManager.getPublicId();
    }

    public void refreshConnectedCount() {
        this.networkManager.refreshConnectedCount();
    }

    public void sendRequest(String method, String[] attrs) {
        this.networkManager.sendRequest(method, attrs);
    }

    public void sendRequestTo(String method, String[] attrs, String to) {
        this.networkManager.sendRequestTo(method, attrs, to);
    }

    public void sendResponse(Network.GenericRequest request, int code, String[] attrs) {
        Network.GenericResponse r = this.networkManager.generateResponse(request);
        r.code = code;
        r.attributes = attrs;
        this.networkManager.sendResponse(r);
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                switch ($SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates()[this.networkManager.getState().ordinal()]) {
                    case 1:
                        processDisconnection(DisconnectionReasons.DISCONNECTED);
                        return true;
                    case 2:
                        processDisconnection(DisconnectionReasons.CONNECTION_FAILED);
                        return true;
                    case 3:
                        processDisconnection(DisconnectionReasons.CONNECTION_LOST);
                        return true;
                    case 4:
                        processConnectionUpdate(ConnectionProgressStates.ESTABLISHING);
                        return true;
                    case 5:
                        processConnectionUpdate(ConnectionProgressStates.ESTABLISHED);
                        return true;
                    case 6:
                        processConnectionUpdate(ConnectionProgressStates.LOGGING);
                        return true;
                    case 7:
                        processConnected();
                        return true;
                    case 8:
                        processDisconnection(DisconnectionReasons.UPDATE_NEEDED);
                        return true;
                    case 9:
                        processDisconnection(DisconnectionReasons.TOO_MANY_USERS);
                        return true;
                    case 10:
                        processDisconnection(DisconnectionReasons.MAINTENANCE);
                        return true;
                    case 11:
                        processDisconnection(DisconnectionReasons.SERVER_ERROR);
                        return true;
                    case 12:
                        processDisconnection(DisconnectionReasons.REGISTRATION_FAILED);
                        return true;
                    default:
                        return true;
                }
            case 1:
                processRequest((Network.GenericRequest) msg.obj);
                return true;
            case 2:
                processResponse((Network.GenericResponse) msg.obj);
                return true;
            case 3:
                processStatsUpdate();
                return true;
            default:
                return true;
        }
    }
}
