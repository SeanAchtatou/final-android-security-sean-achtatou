package com.mocelet.fourinrow.online;

import com.mocelet.fourinrow.online.Network;
import com.mocelet.fourinrow.online.OnlineService;

public interface OnlineServiceListener {
    void onOnlineConnected();

    void onOnlineConnectionProgress(OnlineService.ConnectionProgressStates connectionProgressStates);

    void onOnlineDisconnected(OnlineService.DisconnectionReasons disconnectionReasons);

    void onOnlineRequestReceived(Network.GenericRequest genericRequest);

    void onOnlineResponseReceived(Network.GenericResponse genericResponse);

    void onOnlineStatsUpdate();
}
