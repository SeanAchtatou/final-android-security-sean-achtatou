package com.mocelet.fourinrow.online;

import android.os.Handler;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.mocelet.fourinrow.online.Network;
import java.io.IOException;
import java.util.Locale;

public class NetworkKryoManager extends Listener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates = null;
    private static final String APP_VERSION_ATTR = "v";
    private static final int CONNECTION_TIMEOUT_MS = 5000;
    private static final String LANGUAGE_ATTR = "L";
    private static final int MAX_RETRIES = 2;
    public static final int MESSAGE_REQUEST_RECEIVED = 1;
    public static final int MESSAGE_RESPONSE_RECEIVED = 2;
    public static final int MESSAGE_STATE_CHANGED = 0;
    public static final int MESSAGE_STATS_UPDATED = 3;
    private static final int PROTOCOL_VERSION = 1;
    private static final String STATS_ATTR = "s";
    private static NetworkKryoManager instance;
    private String alias = "";
    private Client client = new Client();
    private int connectedCount = 0;
    private Handler handler;
    private boolean isFinalState;
    private boolean isFinalStateHandled;
    private String publicId = "";
    private ConnectionStates state = ConnectionStates.DISCONNECTED;
    private boolean voluntaryDisconnect = false;

    public enum ConnectionStates {
        DISCONNECTED,
        CONNECTION_FAILED,
        CONNECTION_LOST,
        CONNECTING,
        CONNECTED,
        REGISTERING,
        REGISTERED,
        UPDATE_NEEDED,
        TOO_MANY_USERS,
        MAINTENANCE,
        SERVER_ERROR,
        REGISTRATION_FAILED
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates;
        if (iArr == null) {
            iArr = new int[ConnectionStates.values().length];
            try {
                iArr[ConnectionStates.CONNECTED.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ConnectionStates.CONNECTING.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ConnectionStates.CONNECTION_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ConnectionStates.CONNECTION_LOST.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ConnectionStates.DISCONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ConnectionStates.MAINTENANCE.ordinal()] = 10;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ConnectionStates.REGISTERED.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ConnectionStates.REGISTERING.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[ConnectionStates.REGISTRATION_FAILED.ordinal()] = 12;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[ConnectionStates.SERVER_ERROR.ordinal()] = 11;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[ConnectionStates.TOO_MANY_USERS.ordinal()] = 9;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[ConnectionStates.UPDATE_NEEDED.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates = iArr;
        }
        return iArr;
    }

    private NetworkKryoManager() {
        this.client.addListener(this);
        Network.register(this.client);
    }

    public static NetworkKryoManager getInstance(Handler handler2) {
        if (instance == null) {
            instance = new NetworkKryoManager();
        }
        instance.setHandler(handler2);
        return instance;
    }

    public void setHandler(Handler handler2) {
        this.handler = handler2;
    }

    private void setState(ConnectionStates state2) {
        this.state = state2;
        switch ($SWITCH_TABLE$com$mocelet$fourinrow$online$NetworkKryoManager$ConnectionStates()[state2.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 8:
            case 9:
            case 10:
            case 11:
                this.isFinalState = true;
                break;
        }
        if (!this.isFinalState || (this.isFinalState && !this.isFinalStateHandled)) {
            if (this.isFinalState) {
                this.isFinalStateHandled = true;
            }
            this.handler.sendMessage(this.handler.obtainMessage(0));
        }
    }

    public ConnectionStates getState() {
        return this.state;
    }

    public int getConnectedCount() {
        return this.connectedCount;
    }

    public String getPublicId() {
        return this.publicId;
    }

    public boolean isFinalState() {
        return this.isFinalState;
    }

    private boolean connect() {
        this.client.start();
        this.isFinalState = false;
        this.isFinalStateHandled = false;
        setState(ConnectionStates.CONNECTING);
        boolean connected = false;
        for (int i = 0; i < 2; i++) {
            try {
                this.client.connect((int) CONNECTION_TIMEOUT_MS, Network.SERVER_HOST, (int) Network.SERVER_PORT);
                connected = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!connected) {
            setState(ConnectionStates.CONNECTION_FAILED);
        }
        return connected;
    }

    public void connectAndRegister(String uuid, String alias2, String appVersion, String stats) {
        this.voluntaryDisconnect = false;
        this.alias = alias2;
        String language = "en";
        try {
            language = Locale.getDefault().getLanguage();
        } catch (Exception e) {
        }
        if (language.equals("")) {
            language = "unknown";
        }
        if (connect()) {
            setState(ConnectionStates.REGISTERING);
            Network.GenericRequest r = generateRequest(Network.REGISTER_METHOD);
            r.from = uuid;
            r.to = uuid;
            r.attributes = new String[]{((String) APP_VERSION_ATTR) + appVersion, ((String) LANGUAGE_ATTR) + language, ((String) STATS_ATTR) + stats};
            this.client.sendTCP(r);
        }
    }

    public void disconnect() {
        this.voluntaryDisconnect = true;
        this.client.stop();
    }

    public void refreshConnectedCount() {
        this.client.sendTCP(generateRequest(Network.GET_CONNECTED_COUNT_METHOD));
    }

    public Network.GenericRequest generateRequest(String method) {
        Network.GenericRequest r = new Network.GenericRequest();
        r.version = 1;
        r.from = this.publicId;
        r.to = this.publicId;
        r.alias = this.alias;
        r.method = method;
        return r;
    }

    public Network.GenericRequest generateRequestTo(String method, String to) {
        Network.GenericRequest r = generateRequest(method);
        r.to = to;
        return r;
    }

    public void sendResponse(Network.GenericResponse response) {
        this.client.sendTCP(response);
    }

    public void sendRequest(String method, String[] attrs) {
        Network.GenericRequest r = generateRequest(method);
        r.attributes = attrs;
        this.client.sendTCP(r);
    }

    public void sendRequestTo(String method, String[] attrs, String to) {
        Network.GenericRequest r = generateRequestTo(method, to);
        r.attributes = attrs;
        this.client.sendTCP(r);
    }

    public Network.GenericResponse generateResponse(Network.GenericRequest request) {
        Network.GenericResponse response = new Network.GenericResponse();
        response.from = request.from;
        response.to = request.to;
        response.alias = this.alias;
        response.appId = request.appId;
        response.method = request.method;
        response.sessionId = request.sessionId;
        return response;
    }

    public void connected(Connection connection) {
        setState(ConnectionStates.CONNECTED);
    }

    public void disconnected(Connection connection) {
        if (!(this.state == ConnectionStates.CONNECTING || this.state == ConnectionStates.CONNECTED || this.state == ConnectionStates.DISCONNECTED)) {
            if (this.voluntaryDisconnect) {
                setState(ConnectionStates.DISCONNECTED);
            } else {
                setState(ConnectionStates.CONNECTION_LOST);
            }
        }
        this.voluntaryDisconnect = false;
    }

    public void received(Connection connection, Object object) {
        if (object instanceof Network.GenericRequest) {
            dispatchRequest((Network.GenericRequest) object);
        }
        if (object instanceof Network.GenericResponse) {
            dispatchResponse((Network.GenericResponse) object);
        }
    }

    private void dispatchRequest(Network.GenericRequest request) {
        if (this.handler == null) {
            Network.GenericResponse r = generateResponse(request);
            r.code = Network.BUSY_RESPONSE;
            this.client.sendTCP(r);
            return;
        }
        this.handler.sendMessage(this.handler.obtainMessage(1, request));
    }

    private void dispatchResponse(Network.GenericResponse response) {
        if (response.method != null) {
            switch (response.code) {
                case Network.BAD_REQUEST_RESPONSE:
                case Network.NOT_IMPLEMENTED_RESPONSE:
                    return;
                case Network.UPDATE_NEEDED_RESPONSE:
                    setState(ConnectionStates.UPDATE_NEEDED);
                    return;
                case Network.SERVER_ERROR_RESPONSE:
                    setState(ConnectionStates.SERVER_ERROR);
                    return;
                case Network.TOO_MANY_USERS_RESPONSE:
                    setState(ConnectionStates.TOO_MANY_USERS);
                    return;
                case Network.MAINTENANCE_RESPONSE:
                    setState(ConnectionStates.MAINTENANCE);
                    return;
                default:
                    if (Network.REGISTER_METHOD.equals(response.method)) {
                        switch (response.code) {
                            case Network.OK_RESPONSE:
                                if (response.attributes == null || response.attributes.length < 2) {
                                    setState(ConnectionStates.SERVER_ERROR);
                                    return;
                                }
                                this.publicId = response.attributes[0];
                                try {
                                    this.connectedCount = Integer.parseInt(response.attributes[1]);
                                } catch (Exception e) {
                                }
                                setState(ConnectionStates.REGISTERED);
                                return;
                            default:
                                setState(ConnectionStates.REGISTRATION_FAILED);
                                return;
                        }
                    } else if (Network.BYE_METHOD.equals(response.method)) {
                        return;
                    } else {
                        if (Network.GET_CONNECTED_COUNT_METHOD.equals(response.method)) {
                            switch (response.code) {
                                case Network.OK_RESPONSE:
                                    if (response.attributes != null && response.attributes.length >= 1) {
                                        try {
                                            this.connectedCount = Integer.parseInt(response.attributes[0]);
                                            this.handler.sendMessage(this.handler.obtainMessage(3, response));
                                            return;
                                        } catch (NumberFormatException e2) {
                                            return;
                                        }
                                    } else {
                                        return;
                                    }
                                default:
                                    return;
                            }
                        } else if (this.handler != null) {
                            this.handler.sendMessage(this.handler.obtainMessage(2, response));
                            return;
                        } else {
                            return;
                        }
                    }
            }
        }
    }
}
