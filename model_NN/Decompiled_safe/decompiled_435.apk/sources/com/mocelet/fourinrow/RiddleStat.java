package com.mocelet.fourinrow;

/* compiled from: RiddleManager */
class RiddleStat {
    boolean completed = false;
    long firstCompleted = 0;
    String id = "";

    public RiddleStat(String id2, boolean completed2, long firstCompleted2) {
        this.id = id2;
        this.completed = completed2;
        this.firstCompleted = firstCompleted2;
    }
}
