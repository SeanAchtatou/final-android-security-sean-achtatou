package com.mocelet.fourinrow;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import com.mocelet.fourinrow.BoardThemeManager;
import com.mocelet.fourinrow.GameState;
import java.util.Random;

public class MiniBoardView extends View {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players = null;
    private static final float EXTRA_ROWS_FOR_SELECTOR = 1.1f;
    private int aboutToDropColumn;
    private int animatingCol;
    private GameState.Players animatingPlayer;
    private int animatingRow;
    private Paint backgroundPaint;
    private Bitmap boardBitmap;
    private Drawable boardDrawable;
    private float boardHeight;
    private boolean boardRecycled;
    private float boardWidth;
    private int containerHeight;
    private int containerWidth;
    private float depthOffsetX;
    private float depthOffsetY;
    private GameState gameState;
    private Paint holePaint;
    private int lastMoveCountKnown;
    private float pieceBorderSize;
    private float pieceSize;
    private Bitmap player1Disc;
    private Bitmap player2Disc;
    private float separation;
    private boolean showLastColumnMarker;
    private boolean switchColors;
    private Bitmap tempDisc;
    private float tempPosX;
    private float tempPosY;
    private int theme;
    BoardThemeManager themeManager;
    private Paint winningMarkerPaint;

    public enum States {
        INITIALIZING,
        STATIC
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players;
        if (iArr == null) {
            iArr = new int[GameState.Players.values().length];
            try {
                iArr[GameState.Players.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameState.Players.PLAYER_1.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameState.Players.PLAYER_2.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players = iArr;
        }
        return iArr;
    }

    public MiniBoardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public MiniBoardView(Context context) {
        super(context);
        init(context);
    }

    public MiniBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setGameState(GameState state) {
        GameState oldState = this.gameState;
        this.gameState = state;
        if (oldState == null) {
            return;
        }
        if ((oldState.getCols() != state.getCols() || oldState.getRows() != state.getRows()) && this.containerHeight != 0 && this.containerWidth != 0) {
            computeSizeAttributes(this.containerWidth, this.containerHeight);
        }
    }

    public void setLastColumnPlayedMarker(boolean showLastColumnMarker2) {
        this.showLastColumnMarker = showLastColumnMarker2;
    }

    public void setSwitchColors(boolean switchColors2) {
        if (this.switchColors != switchColors2) {
            recycleBitmaps();
        }
        this.switchColors = switchColors2;
    }

    public void setTheme(int theme2) {
        if (this.themeManager == null) {
            this.themeManager = new BoardThemeManager(theme2);
        }
        if (this.theme != theme2) {
            this.themeManager.setTheme(theme2);
            recycleBitmaps();
        }
        this.theme = theme2;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    private void init(Context context) {
        this.aboutToDropColumn = -1;
        this.theme = 0;
        this.boardRecycled = true;
        Resources myRes = getResources();
        this.themeManager = new BoardThemeManager(this.theme);
        this.backgroundPaint = new Paint(1);
        this.backgroundPaint.setColor(myRes.getColor(R.color.board_bg));
        this.backgroundPaint.setStyle(Paint.Style.FILL);
        this.holePaint = new Paint(1);
        this.holePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        if (isInEditMode()) {
            this.gameState = new GameState(6, 7);
            this.gameState.startGame();
            Random r = new Random();
            while (this.gameState.getState() == GameState.States.PLAYING) {
                int playColumn = r.nextInt(this.gameState.getCols());
                if (this.gameState.isColumnAvailable(playColumn)) {
                    this.gameState.put(playColumn);
                }
            }
        }
    }

    private void computeSizeAttributes(int w, int h) {
        this.containerWidth = w;
        this.containerHeight = h;
        float rowsNeeded = ((float) this.gameState.getRows()) + EXTRA_ROWS_FOR_SELECTOR;
        float colsNeeded = (float) this.gameState.getCols();
        float widthToHeightRatio = colsNeeded / rowsNeeded;
        float optimalHeight = (float) this.containerHeight;
        float optimalWidth = ((float) this.containerHeight) * widthToHeightRatio;
        if (optimalWidth > ((float) this.containerWidth)) {
            optimalHeight = ((float) this.containerWidth) / widthToHeightRatio;
            optimalWidth = (float) this.containerWidth;
        }
        if (optimalHeight > ((float) this.containerHeight)) {
            float optimalHeight2 = (float) this.containerHeight;
        }
        if (optimalWidth > ((float) this.containerWidth)) {
            optimalWidth = (float) this.containerWidth;
        }
        this.pieceSize = (float) (((double) optimalWidth) / (((double) colsNeeded) + (((double) (2.0f + colsNeeded)) / 5.0d)));
        this.separation = this.pieceSize / 5.0f;
        this.pieceBorderSize = this.separation / 5.0f;
        this.boardWidth = (this.pieceSize * ((float) this.gameState.getCols())) + (((float) (this.gameState.getCols() + 1)) * this.separation);
        this.boardHeight = (this.pieceSize * ((float) (this.gameState.getRows() + 1))) + (((float) (this.gameState.getRows() + 2)) * this.separation);
        generateBoardBitmaps();
    }

    private void generateDiscsBitmaps() {
        Bitmap miniPlayerDisc;
        Bitmap miniPlayerDisc2;
        this.winningMarkerPaint = this.themeManager.getWinningMarkerPaint(getResources(), this.separation);
        if (this.switchColors) {
            miniPlayerDisc = this.themeManager.getMiniPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_2, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        } else {
            miniPlayerDisc = this.themeManager.getMiniPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_1, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        }
        this.player1Disc = miniPlayerDisc;
        if (this.switchColors) {
            miniPlayerDisc2 = this.themeManager.getMiniPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_1, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        } else {
            miniPlayerDisc2 = this.themeManager.getMiniPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_2, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        }
        this.player2Disc = miniPlayerDisc2;
    }

    private void generateBoardBitmaps() {
        recycleBitmaps();
        generateDiscsBitmaps();
        this.boardDrawable = this.themeManager.getMiniBoardDrawable(getResources(), BoardThemeManager.BoardLayers.FRONT_NORMAL);
        this.boardBitmap = generateBoard(this.boardDrawable);
        this.boardRecycled = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private void drawBoard(Drawable boardDrawable2, Canvas canvas) {
        float bh = (this.boardHeight - this.separation) - this.pieceSize;
        boardDrawable2.setBounds(0, 0, (int) this.boardWidth, (int) bh);
        canvas.save();
        float radiusInPx = TypedValue.applyDimension(1, 3.0f, getResources().getDisplayMetrics());
        if (!isInEditMode()) {
            Path clipPath = new Path();
            clipPath.addRoundRect(new RectF(boardDrawable2.getBounds()), radiusInPx, radiusInPx, Path.Direction.CW);
            canvas.clipPath(clipPath);
        }
        boardDrawable2.draw(canvas);
        Paint gradient = new Paint();
        gradient.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, bh, Color.parseColor("#33FFFFFF"), Color.parseColor("#00FFFFFF"), Shader.TileMode.CLAMP));
        canvas.drawPaint(gradient);
        canvas.restore();
        float posX = 0.0f;
        float posY = 0.0f;
        for (int fila = this.gameState.getRows() - 1; fila >= 0; fila--) {
            float posX2 = posX + this.separation;
            float posY2 = posY + this.separation;
            for (int col = 0; col < this.gameState.getCols(); col++) {
                canvas.drawCircle((this.pieceSize / 2.0f) + posX2, (this.pieceSize / 2.0f) + posY2, this.pieceSize / 2.0f, this.holePaint);
                posX2 += this.pieceSize + this.separation;
            }
            posX = 0.0f;
            posY = posY2 + this.pieceSize;
        }
    }

    private Bitmap generateBoard(Drawable boardDrawable2) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap((int) this.boardWidth, (int) this.boardHeight, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawBoard(boardDrawable2, canvas);
        return bitmap;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.gameState != null) {
            if (this.boardRecycled) {
                generateBoardBitmaps();
            }
            this.tempPosX = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
            this.tempPosY = this.pieceSize;
            this.depthOffsetX = 0.0f;
            this.depthOffsetY = (float) (-((int) (this.separation / 2.0f)));
            float f = ((((float) this.containerWidth) - this.boardWidth) / 2.0f) + (this.depthOffsetX * 2.0f);
            float f2 = this.pieceSize + (this.depthOffsetY * 2.0f);
            drawLastColumnPlayedMarker(canvas, this.tempPosX);
            drawSelectorDisc(canvas, this.aboutToDropColumn, this.tempPosX);
            for (int fila = this.gameState.getRows() - 1; fila >= 0; fila--) {
                this.tempPosX += this.separation;
                this.tempPosY += this.separation;
                for (int col = 0; col < this.gameState.getCols(); col++) {
                    if (this.animatingRow == fila && this.animatingCol == col && this.animatingPlayer != null && this.gameState.getMoveCount() == this.lastMoveCountKnown) {
                        switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[this.animatingPlayer.ordinal()]) {
                            case 1:
                                this.tempDisc = this.player1Disc;
                                break;
                            case 2:
                                this.tempDisc = this.player2Disc;
                                break;
                            default:
                                this.tempDisc = null;
                                break;
                        }
                        if (this.tempDisc != null && !this.tempDisc.isRecycled()) {
                            canvas.drawBitmap(this.tempDisc, (float) ((int) (this.tempPosX - this.pieceBorderSize)), (float) ((int) (this.tempPosY - this.pieceBorderSize)), (Paint) null);
                        }
                    } else {
                        if (this.animatingRow == fila && this.animatingCol == col) {
                            this.lastMoveCountKnown = -1;
                        }
                        switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[this.gameState.getFieldState(fila, col).ordinal()]) {
                            case 1:
                                this.tempDisc = this.player1Disc;
                                break;
                            case 2:
                                this.tempDisc = this.player2Disc;
                                break;
                            default:
                                this.tempDisc = null;
                                break;
                        }
                        if (this.tempDisc != null && !this.tempDisc.isRecycled()) {
                            canvas.drawBitmap(this.tempDisc, (float) ((int) (this.tempPosX - this.pieceBorderSize)), (float) ((int) (this.tempPosY - this.pieceBorderSize)), (Paint) null);
                        }
                    }
                    this.tempPosX += this.pieceSize + this.separation;
                }
                this.tempPosX = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
                this.tempPosY += this.pieceSize;
            }
            drawWinningCombination(canvas);
            this.tempPosX = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
            this.tempPosY = this.pieceSize;
            if (!this.boardBitmap.isRecycled()) {
                canvas.drawBitmap(this.boardBitmap, this.tempPosX, this.tempPosY, this.backgroundPaint);
            }
        }
    }

    private void drawLastColumnPlayedMarker(Canvas canvas, float offset) {
        int column;
        if (this.showLastColumnMarker && (column = this.gameState.getLastColPlayed()) >= 0) {
            float cornerX = (float) ((int) (((double) (this.separation + offset + (((float) column) * (this.pieceSize + this.separation)))) + ((((double) this.pieceSize) * 1.0d) / 5.0d)));
            float cornerY = (float) ((int) ((this.pieceSize + (2.0f * this.depthOffsetY)) - (this.depthOffsetY / 2.0f)));
            Paint p = new Paint();
            p.setColor(Color.parseColor("#66000077"));
            canvas.drawRect(new Rect((int) cornerX, (int) cornerY, (int) (((double) cornerX) + ((((double) this.pieceSize) * 3.0d) / 5.0d)), (int) (cornerY - this.depthOffsetY)), p);
        }
    }

    private void drawWinningCombination(Canvas canvas) {
        float tempPosX2 = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
        float tempPosY2 = this.pieceSize;
        for (int fila = this.gameState.getRows() - 1; fila >= 0; fila--) {
            float tempPosX3 = tempPosX2 + this.separation;
            float tempPosY3 = tempPosY2 + this.separation;
            for (int col = 0; col < this.gameState.getCols(); col++) {
                if (this.gameState.belongsToWinningLine(fila, col)) {
                    canvas.drawOval(new RectF((float) ((int) tempPosX3), (float) ((int) tempPosY3), (float) ((int) (this.pieceSize + tempPosX3)), (float) ((int) (this.pieceSize + tempPosY3))), this.winningMarkerPaint);
                }
                tempPosX3 += this.pieceSize + this.separation;
            }
            tempPosX2 = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
            tempPosY2 = tempPosY3 + this.pieceSize;
        }
    }

    public void fakeSelectorDisc(int column) {
        this.aboutToDropColumn = column;
    }

    private void drawSelectorDisc(Canvas canvas, int column, float offset) {
        Bitmap selectorDisc;
        if (column >= 0) {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[this.gameState.getTurn().ordinal()]) {
                case 1:
                    selectorDisc = this.player1Disc;
                    break;
                case 2:
                    selectorDisc = this.player2Disc;
                    break;
                default:
                    return;
            }
            float cornerX = (float) ((int) (this.separation + offset + (((float) column) * (this.pieceSize + this.separation))));
            float cornerY = (float) (((int) this.pieceSize) / 2);
            if (!this.gameState.isColumnAvailable(column)) {
                cornerY = (float) (((int) this.pieceSize) / 6);
            }
            canvas.drawBitmap(selectorDisc, (float) ((int) (cornerX - this.pieceBorderSize)), (float) ((int) (cornerY - this.pieceBorderSize)), (Paint) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        computeSizeAttributes(w, h);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int availableHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        int availableWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        float widthToHeightRatio = ((float) this.gameState.getCols()) / (((float) this.gameState.getRows()) + EXTRA_ROWS_FOR_SELECTOR);
        float optimalHeight = (float) availableHeight;
        float optimalWidth = ((float) availableHeight) * widthToHeightRatio;
        if (optimalWidth > ((float) availableWidth)) {
            optimalHeight = ((float) availableWidth) / widthToHeightRatio;
            optimalWidth = (float) availableWidth;
        }
        if (optimalHeight > ((float) availableHeight)) {
            optimalHeight = (float) availableHeight;
        }
        if (optimalWidth > ((float) availableWidth)) {
            optimalWidth = (float) availableWidth;
        }
        setMeasuredDimension((int) optimalWidth, (int) optimalHeight);
    }

    public void recycleBitmaps() {
        this.boardRecycled = true;
        if (this.player1Disc != null) {
            this.player1Disc.recycle();
        }
        if (this.player2Disc != null) {
            this.player2Disc.recycle();
        }
        if (this.boardBitmap != null) {
            this.boardBitmap.recycle();
        }
        this.boardBitmap = null;
    }
}
