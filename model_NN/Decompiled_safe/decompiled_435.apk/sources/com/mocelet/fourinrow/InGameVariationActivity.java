package com.mocelet.fourinrow;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.mocelet.fourinrow.GameState;
import java.util.Random;

public class InGameVariationActivity extends Activity implements BoardListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States = null;
    public static final int DONT_CONNECT_FOUR_RIDDLE_TYPE_TAG = 1;
    public static final int FILL_ALL_RIDDLE_TYPE_TAG = 2;
    private static final int HARD_AI_LEVEL = 6;
    private static final int MIN_DELAY_AI_MOVE = 600;
    public static final int MOVES_RIDDLE_RIDDLE_TYPE_TAG = 3;
    private static final int NORMAL_AI_LEVEL = 4;
    private static final String TAG = "InGameVariationActivity";
    public static final String VS_HUMAN_PLAY_TAG = "vs_human";
    public static final String VS_MACHINE_PLAY_TAG = "vs_machine";
    private AdView adView;
    /* access modifiers changed from: private */
    public int aiLevel;
    private boolean aiPlaysFirst;
    private boolean animationEnabled;
    private BoardView boardView;
    private int colsPref;
    private boolean elementsSet;
    private boolean endSoundPlayed;
    /* access modifiers changed from: private */
    public GameState gameState;
    private TextView infoTextView;
    private int intelligentPlayerExpectedCall;
    private int lastCountReason;
    private boolean localOpponent;
    /* access modifiers changed from: private */
    public boolean newGameEnabled;
    /* access modifiers changed from: private */
    public int ownMovesCount;
    private int ownMovesLimit;
    private Button playAgainButton;
    private int player1WinCount;
    private int player2WinCount;
    private boolean riddleFinished;
    private String riddleId;
    private TextView riddleInfoTextView;
    private boolean riddleSuccess;
    private boolean riddleSuccessSaved;
    /* access modifiers changed from: private */
    public int riddleType;
    private int rowsPref;
    private ViewGroup scoreBoard;
    private TextView scoreBoardText;
    private boolean showLastMove;
    private boolean showingAds;
    private boolean solidBlackBackground;
    private boolean soundEnabled;
    private int startingMoveCount;
    private Button successButton;
    private boolean switchColors;
    private int theme;
    /* access modifiers changed from: private */
    public boolean thinking;
    private int tieCount;
    private Button tryAgainButton;
    private boolean undoEnabled;

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players;
        if (iArr == null) {
            iArr = new int[GameState.Players.values().length];
            try {
                iArr[GameState.Players.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameState.Players.PLAYER_1.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameState.Players.PLAYER_2.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States;
        if (iArr == null) {
            iArr = new int[GameState.States.values().length];
            try {
                iArr[GameState.States.FINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameState.States.PAUSED.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameState.States.PLAYING.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[GameState.States.RESET.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.newGameEnabled = true;
        this.thinking = false;
        this.elementsSet = false;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.registerOnSharedPreferenceChangeListener(this);
        onSharedPreferenceChanged(prefs, "");
        String vsTag = getIntent().getStringExtra("com.mocelet.games.fourinrow.PlayTag");
        this.riddleType = getIntent().getIntExtra("com.mocelet.games.fourinrow.RiddleTypeTag", 0);
        int variation = 0;
        if (this.riddleType == 1) {
            variation = 1;
        }
        if (this.riddleType == 2) {
            variation = 2;
        }
        if (vsTag == null) {
            this.localOpponent = true;
        } else if (vsTag.equals(VS_HUMAN_PLAY_TAG)) {
            this.localOpponent = true;
        } else if (vsTag.equals(VS_MACHINE_PLAY_TAG)) {
            this.localOpponent = false;
        }
        if (this.riddleType == 3) {
            this.aiLevel = 6;
            this.localOpponent = false;
            String moves = getIntent().getStringExtra("com.mocelet.games.fourinrow.RiddleInitialGameStateTag");
            int firstPlayer = getIntent().getIntExtra("com.mocelet.games.fourinrow.RiddleFirstPlayerTag", 1);
            int rows = getIntent().getIntExtra("com.mocelet.games.fourinrow.RiddleRowsTag", 6);
            int cols = getIntent().getIntExtra("com.mocelet.games.fourinrow.RiddleColsTag", 7);
            this.ownMovesLimit = getIntent().getIntExtra("com.mocelet.games.fourinrow.RiddleMovesLimitTag", 0);
            this.riddleId = getIntent().getStringExtra("com.mocelet.games.fourinrow.RiddleIdTag");
            this.ownMovesCount = 0;
            this.gameState = new GameState();
            this.gameState.restoreGame(rows, cols, moves, firstPlayer);
            this.startingMoveCount = this.gameState.getMoveCount();
        } else {
            this.aiLevel = 4;
            this.gameState = new GameState(this.rowsPref, this.colsPref, variation);
            if (savedInstanceState != null) {
                restoreState(savedInstanceState);
            }
            if (this.gameState.getState() == GameState.States.RESET) {
                if (this.localOpponent) {
                    this.gameState.startGame();
                } else {
                    this.gameState.startGame(this.aiPlaysFirst ? GameState.Players.PLAYER_2 : GameState.Players.PLAYER_1);
                }
            }
        }
        setupViewElements();
        onSharedPreferenceChanged(prefs, "");
        resetCounters();
        updateElements();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setTiledBackground();
        if (this.boardView != null) {
            this.boardView.setListener(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.boardView != null) {
            this.boardView.setListener(null);
        }
        if (this.boardView != null) {
            this.boardView.recycleBitmaps();
        }
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.boardView != null) {
            this.boardView.onPausing();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.boardView != null) {
            this.boardView.onResuming();
        }
    }

    private void setTiledBackground() {
        if (this.solidBlackBackground) {
            getWindow().setBackgroundDrawableResource(R.drawable.black_background);
            return;
        }
        BitmapDrawable tiledBg = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.tile_black_stripes));
        tiledBg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        getWindow().setBackgroundDrawable(tiledBg);
    }

    private void setupViewElements() {
        setContentView((int) R.layout.riddle_ingame);
        this.infoTextView = (TextView) findViewById(R.id.info_text_area);
        this.riddleInfoTextView = (TextView) findViewById(R.id.riddleInfoText);
        if (this.gameState.getVariation() == 1) {
            this.riddleInfoTextView.setText((int) R.string.riddles_variation_dontconnectfour_info);
        } else if (this.gameState.getVariation() == 2) {
            this.riddleInfoTextView.setText(String.format(getString(R.string.riddles_variation_fill_all_info), Integer.valueOf(this.gameState.getPlayer1LinesCount()), Integer.valueOf(this.gameState.getPlayer2LinesCount())));
        }
        if (this.boardView != null) {
            this.boardView.recycleBitmaps();
        }
        this.boardView = null;
        this.boardView = (BoardView) findViewById(R.id.boardView);
        this.boardView.setGameState(this.gameState);
        this.boardView.setListener(this);
        this.boardView.setSwitchColors(this.switchColors);
        if (this.riddleType == 3) {
            this.boardView.setLastColumnPlayedMarker(false);
        } else {
            this.boardView.setLastColumnPlayedMarker(this.showLastMove);
        }
        this.boardView.setTheme(this.theme);
        this.boardView.setAnimated(this.animationEnabled);
        this.boardView.setSoundEnabled(this.soundEnabled);
        this.scoreBoard = (ViewGroup) findViewById(R.id.scoreBoard);
        this.scoreBoardText = (TextView) findViewById(R.id.scoreTextView);
        this.playAgainButton = (Button) findViewById(R.id.play_again_button);
        this.tryAgainButton = (Button) findViewById(R.id.try_again_button);
        this.successButton = (Button) findViewById(R.id.success_button);
        this.adView = (AdView) findViewById(R.id.adView);
        hideAds();
        this.elementsSet = true;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("moves", this.gameState.getMoves());
        outState.putInt("first_player", this.gameState.getFirstTurn() == GameState.Players.PLAYER_1 ? 1 : 2);
        outState.putInt("rows", this.gameState.getRows());
        outState.putInt("cols", this.gameState.getCols());
        outState.putInt("riddle_type", this.riddleType);
        outState.putInt("riddle_starting_move_count", this.startingMoveCount);
        outState.putInt("riddle_own_moves_limit", this.ownMovesLimit);
        outState.putInt("riddle_own_moves_count", this.ownMovesCount);
        outState.putString("riddle_id", this.riddleId);
        outState.putBoolean("is_local_opponent", this.localOpponent);
        outState.putInt("player1_win_count", this.player1WinCount);
        outState.putInt("player2_win_count", this.player2WinCount);
        outState.putInt("tie_count", this.tieCount);
        outState.putInt("last_count_reason", this.lastCountReason);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restoreState(savedInstanceState);
        updateElements();
    }

    private void restoreState(Bundle savedInstanceState) {
        String moves = savedInstanceState.getString("moves");
        int firstPlayer = savedInstanceState.getInt("first_player");
        int rows = savedInstanceState.getInt("rows");
        int cols = savedInstanceState.getInt("cols");
        this.riddleType = savedInstanceState.getInt("riddle_type");
        this.startingMoveCount = savedInstanceState.getInt("riddle_starting_move_count");
        this.ownMovesLimit = savedInstanceState.getInt("riddle_own_moves_limit");
        this.ownMovesCount = savedInstanceState.getInt("riddle_own_moves_count");
        this.riddleId = savedInstanceState.getString("riddle_id");
        if (rows == 0 || cols == 0) {
            rows = 6;
            cols = 7;
        }
        int variation = 0;
        if (this.riddleType == 1) {
            variation = 1;
        }
        if (this.riddleType == 2) {
            variation = 2;
        }
        this.localOpponent = savedInstanceState.getBoolean("is_local_opponent");
        this.player1WinCount = savedInstanceState.getInt("player1_win_count");
        this.player2WinCount = savedInstanceState.getInt("player2_win_count");
        this.tieCount = savedInstanceState.getInt("tie_count");
        this.lastCountReason = savedInstanceState.getInt("last_count_reason");
        if (moves != null && firstPlayer > 0) {
            this.gameState.restoreGame(rows, cols, moves, firstPlayer, variation);
        }
    }

    private void testFill(GameState gameState2) {
        Random r = new Random();
        while (gameState2.getState() == GameState.States.PLAYING) {
            int playColumn = r.nextInt(gameState2.getCols());
            if (gameState2.isColumnAvailable(playColumn)) {
                gameState2.put(playColumn);
            }
        }
    }

    public void onSuccess(View v) {
        finish();
    }

    public void onTryAgain(View v) {
        onPlayAgain(v);
    }

    public void onPlayAgain(View v) {
        if (this.newGameEnabled) {
            GameState.Players previousFirstPlayer = GameState.Players.NONE;
            int previousRows = 0;
            int previousCols = 0;
            if (this.gameState != null) {
                previousFirstPlayer = this.gameState.getFirstTurn();
                previousRows = this.gameState.getRows();
                previousCols = this.gameState.getCols();
            }
            int variation = 0;
            if (this.riddleType == 1) {
                variation = 1;
            }
            if (this.riddleType == 2) {
                variation = 2;
            }
            if (this.riddleType == 3) {
                while (this.gameState.getMoveCount() > this.startingMoveCount) {
                    this.gameState.undo(1);
                }
                this.ownMovesCount = 0;
                this.riddleFinished = false;
            } else {
                this.gameState = new GameState(this.rowsPref, this.colsPref, variation);
                if (this.localOpponent) {
                    this.gameState.startGame(previousFirstPlayer == GameState.Players.PLAYER_1 ? GameState.Players.PLAYER_2 : GameState.Players.PLAYER_1);
                } else {
                    this.gameState.startGame(this.aiPlaysFirst ? GameState.Players.PLAYER_2 : GameState.Players.PLAYER_1);
                }
                this.boardView.setGameState(this.gameState);
                if (!(previousRows == 0 || previousCols == 0 || (previousRows == this.rowsPref && previousCols == this.colsPref))) {
                    setupViewElements();
                }
            }
            updateElements();
        }
    }

    public void onUndo(View v) {
        if (this.undoEnabled) {
            int movesToUndo = this.localOpponent ? 1 : 2;
            if (this.riddleType != 3 || this.gameState.getMoveCount() > this.startingMoveCount) {
                this.gameState.undo(movesToUndo);
                if (this.riddleType == 3) {
                    this.ownMovesCount--;
                    this.riddleFinished = false;
                }
                updateCounters(true);
                updateElements();
            }
        }
    }

    /* access modifiers changed from: private */
    public void onPlayColumnAgent(int column, int callKey) {
        if (this.intelligentPlayerExpectedCall == callKey) {
            onPlayColumn(column);
        }
    }

    public void onPlayColumn(View v) {
        onPlayColumn(Integer.parseInt((String) v.getTag()));
    }

    public void onPlayColumn(int column) {
        if (this.gameState.getState() == GameState.States.PLAYING) {
            if (!this.gameState.isColumnAvailable(column)) {
                Toast.makeText(this, (int) R.string.info_column_full, 0).show();
                return;
            }
            this.infoTextView.setVisibility(4);
            this.boardView.setTouchable(false);
            this.undoEnabled = false;
            this.newGameEnabled = false;
            HoneyWrapper.invalidateOptionsMenu(this);
            final int col = column;
            new AsyncTask<BoardView, Integer, Integer>() {
                /* access modifiers changed from: protected */
                public Integer doInBackground(BoardView... params) {
                    params[0].doDropPieceAnimation(col);
                    return null;
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Integer result) {
                    super.onPostExecute((Object) result);
                    if (InGameVariationActivity.this.riddleType == 3 && InGameVariationActivity.this.gameState.getTurn() == GameState.Players.PLAYER_1) {
                        InGameVariationActivity inGameVariationActivity = InGameVariationActivity.this;
                        inGameVariationActivity.ownMovesCount = inGameVariationActivity.ownMovesCount + 1;
                    }
                    InGameVariationActivity.this.gameState.put(col);
                    InGameVariationActivity.this.newGameEnabled = true;
                    InGameVariationActivity.this.updateCounters(false);
                    InGameVariationActivity.this.updateElements();
                }
            }.execute(this.boardView);
        }
    }

    public void onColumnPlayed(int c) {
        onPlayColumn(c);
    }

    private void resetCounters() {
        this.lastCountReason = 0;
        this.player1WinCount = 0;
        this.player2WinCount = 0;
        this.tieCount = 0;
    }

    /* access modifiers changed from: private */
    public void updateCounters(boolean undo) {
        if (!undo) {
            if (this.gameState.getState() == GameState.States.FINISHED && this.lastCountReason == 0) {
                switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[this.gameState.getWinner().ordinal()]) {
                    case 1:
                        this.player1WinCount++;
                        this.lastCountReason = 1;
                        break;
                    case 2:
                        this.player2WinCount++;
                        this.lastCountReason = 2;
                        break;
                    case 3:
                        this.tieCount++;
                        this.lastCountReason = 3;
                        break;
                }
            }
        } else {
            switch (this.lastCountReason) {
                case 1:
                    this.player1WinCount = this.player1WinCount > 0 ? this.player1WinCount - 1 : 0;
                    break;
                case 2:
                    this.player2WinCount = this.player2WinCount > 0 ? this.player2WinCount - 1 : 0;
                    break;
                case 3:
                    this.tieCount = this.tieCount > 0 ? this.tieCount - 1 : 0;
                    break;
            }
            this.lastCountReason = 0;
        }
        if (this.gameState.getState() != GameState.States.FINISHED) {
            this.lastCountReason = 0;
        }
    }

    /* access modifiers changed from: private */
    public void updateElements() {
        String discColor;
        boolean z;
        int i;
        int i2;
        int i3;
        boolean z2;
        boolean z3;
        if (this.elementsSet) {
            if (this.riddleType == 2) {
                this.riddleInfoTextView.setText(String.format(getString(R.string.riddles_variation_fill_all_info), Integer.valueOf(this.gameState.getPlayer1LinesCount()), Integer.valueOf(this.gameState.getPlayer2LinesCount())));
            } else if (this.riddleType == 3) {
                if (this.gameState.getFirstTurn() == GameState.Players.PLAYER_1) {
                    if (this.switchColors) {
                        discColor = getString(R.string.riddles_riddle_limit_move_red);
                    } else {
                        discColor = getString(R.string.riddles_riddle_limit_move_white);
                    }
                } else if (this.switchColors) {
                    discColor = getString(R.string.riddles_riddle_limit_move_white);
                } else {
                    discColor = getString(R.string.riddles_riddle_limit_move_red);
                }
                this.riddleInfoTextView.setText("[" + this.riddleId + "] " + String.format(getString(R.string.riddles_riddle_limit_move_info), discColor, Integer.valueOf(this.ownMovesLimit), Integer.valueOf(this.ownMovesLimit - this.ownMovesCount)));
                if (!this.riddleFinished && this.ownMovesCount == this.ownMovesLimit) {
                    this.riddleFinished = true;
                }
                if (!this.riddleFinished && this.gameState.getState() == GameState.States.FINISHED) {
                    this.riddleFinished = true;
                }
                if (this.riddleFinished) {
                    if (this.gameState.getWinner() == GameState.Players.PLAYER_1) {
                        this.riddleSuccess = true;
                        saveRiddleSuccess();
                    } else {
                        this.riddleSuccess = false;
                    }
                    showAds();
                    if (this.riddleSuccess) {
                        z = false;
                    } else {
                        z = true;
                    }
                    this.undoEnabled = z;
                    HoneyWrapper.invalidateOptionsMenu(this);
                    this.playAgainButton.setVisibility(8);
                    Button button = this.tryAgainButton;
                    if (this.riddleSuccess) {
                        i = 8;
                    } else {
                        i = 0;
                    }
                    button.setVisibility(i);
                    Button button2 = this.successButton;
                    if (this.riddleSuccess) {
                        i2 = 0;
                    } else {
                        i2 = 8;
                    }
                    button2.setVisibility(i2);
                    this.scoreBoardText.setVisibility(8);
                    this.scoreBoard.setVisibility(0);
                    if (this.animationEnabled) {
                        this.scoreBoard.startAnimation(AnimationUtils.loadAnimation(this, R.anim.winner_button));
                    }
                    this.infoTextView.setVisibility(0);
                    this.infoTextView.setText(this.riddleSuccess ? R.string.riddles_success_info : R.string.riddles_fail_info);
                    if (!this.endSoundPlayed) {
                        BoardView boardView2 = this.boardView;
                        if (this.riddleSuccess) {
                            i3 = 2;
                        } else {
                            i3 = 3;
                        }
                        boardView2.playSound(i3);
                        this.endSoundPlayed = true;
                    }
                    this.boardView.postInvalidate();
                    return;
                }
            }
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$States()[this.gameState.getState().ordinal()]) {
                case 2:
                    if (this.riddleType != 3) {
                        this.endSoundPlayed = false;
                    } else if (!this.riddleFinished) {
                        this.endSoundPlayed = false;
                    }
                    hideAds();
                    this.scoreBoard.setVisibility(4);
                    if (this.gameState.getTurn() != GameState.Players.PLAYER_1) {
                        if (this.gameState.getTurn() == GameState.Players.PLAYER_2) {
                            if (!this.localOpponent) {
                                this.undoEnabled = false;
                                this.newGameEnabled = false;
                                HoneyWrapper.invalidateOptionsMenu(this);
                                this.boardView.setTouchable(false);
                                this.infoTextView.setText((int) R.string.info_opponent_turn);
                                this.infoTextView.setVisibility(0);
                                callIntelligentPlayer();
                                break;
                            } else {
                                this.infoTextView.setText((int) R.string.info_opponent_turn_humans);
                                this.infoTextView.setVisibility(0);
                                this.boardView.setTouchable(true);
                                if (this.gameState.getMoveCount() >= 1) {
                                    z2 = true;
                                } else {
                                    z2 = false;
                                }
                                this.undoEnabled = z2;
                                HoneyWrapper.invalidateOptionsMenu(this);
                                break;
                            }
                        }
                    } else {
                        if (this.localOpponent) {
                            this.infoTextView.setText((int) R.string.info_your_turn_humans);
                        } else {
                            this.infoTextView.setText((int) R.string.info_your_turn);
                        }
                        if (this.gameState.getMoveCount() >= 1) {
                            z3 = true;
                        } else {
                            z3 = false;
                        }
                        this.undoEnabled = z3;
                        HoneyWrapper.invalidateOptionsMenu(this);
                        this.infoTextView.setVisibility(0);
                        this.boardView.setTouchable(true);
                        break;
                    }
                    break;
                case 4:
                    showAds();
                    this.undoEnabled = true;
                    HoneyWrapper.invalidateOptionsMenu(this);
                    this.scoreBoardText.setText(String.valueOf(this.player1WinCount) + " - " + this.player2WinCount);
                    this.scoreBoard.setVisibility(0);
                    if (this.animationEnabled) {
                        this.scoreBoard.startAnimation(AnimationUtils.loadAnimation(this, R.anim.winner_button));
                    }
                    this.infoTextView.setVisibility(0);
                    String linesScore = "";
                    if (this.gameState.getVariation() == 2) {
                        linesScore = " (" + String.format(getString(R.string.riddle_variation_fill_line_score), Integer.valueOf(this.gameState.getPlayer1LinesCount()), Integer.valueOf(this.gameState.getPlayer2LinesCount())) + ")";
                    }
                    if (this.gameState.getWinner() != GameState.Players.PLAYER_1 || this.localOpponent) {
                        if (this.gameState.getWinner() != GameState.Players.PLAYER_1 || !this.localOpponent) {
                            if (this.gameState.getWinner() != GameState.Players.PLAYER_2 || this.localOpponent) {
                                if (this.gameState.getWinner() != GameState.Players.PLAYER_2 || !this.localOpponent) {
                                    if (this.gameState.getWinner() == GameState.Players.NONE) {
                                        String infoText = String.valueOf(getString(R.string.info_you_tie)) + linesScore;
                                        this.infoTextView.setText((int) R.string.info_you_tie);
                                        break;
                                    }
                                } else {
                                    this.infoTextView.setText(String.valueOf(getString(R.string.player2)) + " " + getString(R.string.info_has_won) + linesScore);
                                    if (!this.endSoundPlayed) {
                                        this.boardView.playSound(2);
                                        this.endSoundPlayed = true;
                                        break;
                                    }
                                }
                            } else {
                                this.infoTextView.setText(String.valueOf(getString(R.string.info_you_lose)) + linesScore);
                                if (!this.endSoundPlayed) {
                                    this.boardView.playSound(3);
                                    this.endSoundPlayed = true;
                                    break;
                                }
                            }
                        } else {
                            this.infoTextView.setText(String.valueOf(getString(R.string.player1)) + " " + getString(R.string.info_has_won) + linesScore);
                            if (!this.endSoundPlayed) {
                                this.boardView.playSound(2);
                                this.endSoundPlayed = true;
                                break;
                            }
                        }
                    } else {
                        this.infoTextView.setText(String.valueOf(getString(R.string.info_you_win)) + linesScore);
                        if (!this.endSoundPlayed) {
                            this.boardView.playSound(2);
                            this.endSoundPlayed = true;
                            break;
                        }
                    }
                    break;
            }
            this.boardView.postInvalidate();
        }
    }

    private void saveRiddleSuccess() {
        if (!this.riddleSuccessSaved) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            RiddleManager rm = new RiddleManager();
            rm.loadFromPrefs(prefs);
            RiddleStat rs = rm.getRiddleStat(this.riddleId);
            if (rs == null) {
                rm.put(new RiddleStat(this.riddleId, true, System.currentTimeMillis()));
                rm.savePrefs(prefs);
            } else if (!rs.completed) {
                rs.completed = true;
                rs.firstCompleted = System.currentTimeMillis();
                rm.savePrefs(prefs);
            }
            this.riddleSuccessSaved = true;
        }
    }

    private void callIntelligentPlayer() {
        if (!this.thinking) {
            this.intelligentPlayerExpectedCall++;
            this.thinking = true;
            this.newGameEnabled = false;
            final int initialMoveCount = this.gameState.getMoveCount();
            final int intelligentPlayerCall = this.intelligentPlayerExpectedCall;
            new AsyncTask<Integer, Integer, Integer>() {
                private int column;
                private long now = System.currentTimeMillis();

                /* access modifiers changed from: protected */
                public Integer doInBackground(Integer... params) {
                    switch (InGameVariationActivity.this.gameState.getVariation()) {
                        case 1:
                            this.column = new GameAIDontConnectFour(InGameVariationActivity.this.aiLevel, InGameVariationActivity.this.gameState).getBestMove();
                            return null;
                        case 2:
                            this.column = new GameAIFillAll(InGameVariationActivity.this.aiLevel, InGameVariationActivity.this.gameState).getBestMove();
                            return null;
                        default:
                            this.column = new GameAI(InGameVariationActivity.this.aiLevel, InGameVariationActivity.this.gameState).getBestMove();
                            return null;
                    }
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Integer result) {
                    super.onPostExecute((Object) result);
                    long delay = System.currentTimeMillis() - this.now;
                    if (delay < 600) {
                        try {
                            Thread.sleep(600 - delay);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    InGameVariationActivity.this.thinking = false;
                    if (InGameVariationActivity.this.gameState.getMoveCount() == initialMoveCount) {
                        InGameVariationActivity.this.onPlayColumnAgent(this.column, intelligentPlayerCall);
                    }
                }
            }.execute(0);
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        this.soundEnabled = sharedPreferences.getBoolean("sound_enabled", true);
        this.animationEnabled = sharedPreferences.getBoolean("animation_enabled", true);
        this.switchColors = sharedPreferences.getBoolean("switch_colors", false);
        this.aiPlaysFirst = sharedPreferences.getBoolean("ai_plays_first", false);
        this.showLastMove = sharedPreferences.getBoolean("show_last_move", true);
        this.solidBlackBackground = sharedPreferences.getBoolean("black_background", false);
        if (this.riddleType != 3) {
            this.aiLevel = Integer.parseInt(sharedPreferences.getString("difficulty", "4"));
        }
        this.theme = 10;
        try {
            this.theme = Integer.parseInt(sharedPreferences.getString("skin", "10"));
        } catch (NumberFormatException e) {
        }
        String size = sharedPreferences.getString("size", "6x7");
        if (size.equals("6x7")) {
            this.rowsPref = 6;
            this.colsPref = 7;
        }
        if (size.equals("6x8")) {
            this.rowsPref = 6;
            this.colsPref = 8;
        }
        if (size.equals("7x7")) {
            this.rowsPref = 7;
            this.colsPref = 7;
        }
        if (size.equals("7x8")) {
            this.rowsPref = 7;
            this.colsPref = 8;
        }
        if (size.equals("8x8")) {
            this.rowsPref = 8;
            this.colsPref = 8;
        }
        if (size.equals("7x9")) {
            this.rowsPref = 7;
            this.colsPref = 9;
        }
        if (this.boardView != null) {
            this.boardView.setSoundEnabled(this.soundEnabled);
        }
        if (this.boardView != null) {
            this.boardView.setAnimated(this.animationEnabled);
        }
        if (this.boardView != null) {
            this.boardView.setSwitchColors(this.switchColors);
        }
        if (this.boardView != null) {
            if (this.riddleType == 3) {
                this.boardView.setLastColumnPlayedMarker(false);
            } else {
                this.boardView.setLastColumnPlayedMarker(this.showLastMove);
            }
        }
        if (this.boardView != null) {
            this.boardView.setTheme(this.theme);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.undo);
        item.setEnabled(this.undoEnabled);
        if (this.riddleType == 3) {
            item.setEnabled(false);
            if (HoneyWrapper.isHoneycomb()) {
                item.setVisible(false);
            }
        } else if (HoneyWrapper.isHoneycomb()) {
            item.setEnabled(true);
        }
        MenuItem item2 = menu.findItem(R.id.new_game);
        item2.setEnabled(this.newGameEnabled);
        if (HoneyWrapper.isHoneycomb()) {
            item2.setEnabled(true);
        }
        if (this.riddleType == 3) {
            item2.setTitle((int) R.string.riddles_menu_restart);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent homeIntent = new Intent(this, CuatroEnLinea.class);
                homeIntent.addFlags(67108864);
                startActivity(homeIntent);
                return true;
            case R.id.options /*2131558467*/:
                startActivity(new Intent(this, GamePreferenceActivity.class));
                return true;
            case R.id.about /*2131558468*/:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle((int) R.string.about);
                alertDialog.setView(((LayoutInflater) getApplicationContext().getSystemService("layout_inflater")).inflate((int) R.layout.about, (ViewGroup) null));
                alertDialog.setCanceledOnTouchOutside(true);
                alertDialog.setIcon((Drawable) null);
                alertDialog.show();
                return true;
            case R.id.new_game /*2131558469*/:
                if (!this.newGameEnabled) {
                    Toast.makeText(this, (int) R.string.toast_undo_start_not_available, 0).show();
                }
                onPlayAgain(this.boardView);
                return true;
            case R.id.undo /*2131558470*/:
                if (!this.undoEnabled) {
                    if (this.gameState.getMoveCount() == 0) {
                        Toast.makeText(this, (int) R.string.toast_undo_nothing_to_undo, 0).show();
                    } else {
                        Toast.makeText(this, (int) R.string.toast_undo_start_not_available, 0).show();
                    }
                }
                onUndo(this.boardView);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateElements();
    }

    private void hideAds() {
        this.adView.setVisibility(8);
        this.showingAds = false;
    }

    private void showAds() {
        if (!this.showingAds) {
            AdRequest request = new AdRequest();
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            this.adView.setVisibility(0);
            this.adView.loadAd(request);
            this.showingAds = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.adView != null) {
            this.adView.destroy();
        }
        System.gc();
    }
}
