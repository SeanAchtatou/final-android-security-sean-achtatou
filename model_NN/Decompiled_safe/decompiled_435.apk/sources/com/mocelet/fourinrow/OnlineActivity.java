package com.mocelet.fourinrow;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import com.mocelet.fourinrow.online.Network;
import com.mocelet.fourinrow.online.OnlineService;
import com.mocelet.fourinrow.online.OnlineServiceListener;
import java.util.UUID;

public class OnlineActivity extends Activity implements OnlineServiceListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$ConnectionProgressStates = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$DisconnectionReasons = null;
    private static final String NEWS_ATTR = "NEWS";
    /* access modifiers changed from: private */
    public String alias;
    /* access modifiers changed from: private */
    public String appVersion;
    private AlertDialog errorDialog;
    private boolean isOnline;
    /* access modifiers changed from: private */
    public boolean loginOnBind;
    /* access modifiers changed from: private */
    public OnlineService mBoundService;
    private ServiceConnection mConnection = new ServiceConnection() {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode;

        static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode() {
            int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode;
            if (iArr == null) {
                iArr = new int[StartMode.values().length];
                try {
                    iArr[StartMode.CONNECT_ON_START.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[StartMode.DISCONNECT_ON_START.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[StartMode.NO_ACTION_ON_START.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode = iArr;
            }
            return iArr;
        }

        public void onServiceConnected(ComponentName className, IBinder service) {
            OnlineActivity.this.mBoundService = ((OnlineService.LocalBinder) service).getService();
            OnlineActivity.this.mBoundService.setListener(OnlineActivity.this);
            if (OnlineActivity.this.loginOnBind) {
                OnlineActivity.this.mBoundService.login(OnlineActivity.this.uuid, OnlineActivity.this.alias, OnlineActivity.this.appVersion, OnlineActivity.this.stats);
                return;
            }
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode()[OnlineActivity.this.startMode.ordinal()]) {
                case 1:
                    OnlineActivity.this.mBoundService.logout();
                    return;
                case 2:
                    OnlineActivity.this.mBoundService.login(OnlineActivity.this.uuid, OnlineActivity.this.alias, OnlineActivity.this.appVersion, OnlineActivity.this.stats);
                    return;
                default:
                    return;
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            OnlineActivity.this.mBoundService = null;
        }
    };
    private boolean mIsBound;
    private ProgressDialog progressDialog;
    private boolean showingDialog;
    /* access modifiers changed from: private */
    public StartMode startMode;
    /* access modifiers changed from: private */
    public String stats;
    /* access modifiers changed from: private */
    public String uuid;

    enum StartMode {
        DISCONNECT_ON_START,
        CONNECT_ON_START,
        NO_ACTION_ON_START
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode;
        if (iArr == null) {
            iArr = new int[StartMode.values().length];
            try {
                iArr[StartMode.CONNECT_ON_START.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[StartMode.DISCONNECT_ON_START.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[StartMode.NO_ACTION_ON_START.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$ConnectionProgressStates() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$ConnectionProgressStates;
        if (iArr == null) {
            iArr = new int[OnlineService.ConnectionProgressStates.values().length];
            try {
                iArr[OnlineService.ConnectionProgressStates.ESTABLISHED.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OnlineService.ConnectionProgressStates.ESTABLISHING.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OnlineService.ConnectionProgressStates.LOGGING.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$ConnectionProgressStates = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$DisconnectionReasons() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$DisconnectionReasons;
        if (iArr == null) {
            iArr = new int[OnlineService.DisconnectionReasons.values().length];
            try {
                iArr[OnlineService.DisconnectionReasons.CONNECTION_FAILED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OnlineService.DisconnectionReasons.CONNECTION_LOST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OnlineService.DisconnectionReasons.DISCONNECTED.ordinal()] = 8;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[OnlineService.DisconnectionReasons.MAINTENANCE.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[OnlineService.DisconnectionReasons.REGISTRATION_FAILED.ordinal()] = 7;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[OnlineService.DisconnectionReasons.SERVER_ERROR.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[OnlineService.DisconnectionReasons.TOO_MANY_USERS.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[OnlineService.DisconnectionReasons.UPDATE_NEEDED.ordinal()] = 3;
            } catch (NoSuchFieldError e8) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$DisconnectionReasons = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.startMode = StartMode.NO_ACTION_ON_START;
        this.isOnline = false;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.uuid = prefs.getString("uuid", "");
        if ("".equals(this.uuid)) {
            this.uuid = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("uuid", this.uuid);
            editor.commit();
        }
        this.alias = prefs.getString("online_alias", "Guest");
        this.alias = this.alias.trim();
        if ("".equals(this.alias)) {
            this.alias = "Guest";
        }
        this.appVersion = "unknown";
        try {
            this.appVersion = getPackageManager().getPackageInfo("com.mocelet.fourinrow", 128).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        this.stats = prefs.getString(RiddleManager.PREF_NAME_SOLVED_STATS, "0");
    }

    private void initializeDialogs() {
        if (this.progressDialog == null || this.errorDialog == null) {
            this.progressDialog = new ProgressDialog(this);
            this.progressDialog.setIndeterminate(true);
            this.progressDialog.setTitle("");
            this.progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    OnlineActivity.this.finish();
                }
            });
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            this.errorDialog = builder.create();
            this.errorDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    OnlineActivity.this.finish();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.alias = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("online_alias", getString(R.string.options_online_alias_default));
        switch ($SWITCH_TABLE$com$mocelet$fourinrow$OnlineActivity$StartMode()[this.startMode.ordinal()]) {
            case 1:
            case 2:
                doBindService();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        doUnbindService();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private void doBindService() {
        bindService(new Intent(this, OnlineService.class), this.mConnection, 1);
        this.mIsBound = true;
    }

    private void doUnbindService() {
        if (this.mIsBound) {
            unbindService(this.mConnection);
            this.mIsBound = false;
            this.mBoundService = null;
        }
    }

    public void goOnline() {
        if (this.mBoundService != null) {
            this.mBoundService.login(this.uuid, this.alias, this.appVersion, this.stats);
            return;
        }
        this.loginOnBind = true;
        doBindService();
    }

    public void setStartMode(StartMode mode) {
        this.startMode = mode;
    }

    public boolean isOnline() {
        return this.isOnline;
    }

    public int getConnectedCount() {
        if (this.mBoundService != null) {
            return this.mBoundService.getConnectedCount();
        }
        return 0;
    }

    public String getPublicId() {
        if (this.mBoundService != null) {
            return this.mBoundService.getPublicId();
        }
        return "";
    }

    public void refreshConnectedCount() {
        if (this.mBoundService != null) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    OnlineActivity.this.mBoundService.refreshConnectedCount();
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    private void showConnectionProgress(int textId) {
        initializeDialogs();
        this.progressDialog.setMessage(String.valueOf(getString(textId)) + "...");
        this.progressDialog.show();
    }

    private void showConnectionError(int textId) {
        initializeDialogs();
        hideConnectionNotifications();
        this.errorDialog.setMessage(getString(textId));
        this.errorDialog.show();
        this.showingDialog = true;
    }

    private void showUpdateNeededDialog() {
        hideConnectionNotifications();
        this.showingDialog = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle((int) R.string.online_update_needed_title);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if (OnlineActivity.this.startMode == StartMode.CONNECT_ON_START) {
                    OnlineActivity.this.finish();
                }
            }
        });
        TextView message = new TextView(this);
        message.setLinksClickable(true);
        message.setMovementMethod(LinkMovementMethod.getInstance());
        message.setText(new SpannableString(Html.fromHtml(getString(R.string.online_update_needed) + "<br><br>" + "<a href=\"https://market.android.com/details?id=com.mocelet.fourinrow&feature=search_result\">" + getString(R.string.online_update_needed_link) + "</a>" + "\n\n")));
        message.setPadding(5, 5, 5, 5);
        builder.setView(message);
        builder.show();
    }

    private void hideConnectionNotifications() {
        initializeDialogs();
        this.showingDialog = false;
        this.progressDialog.dismiss();
        this.errorDialog.dismiss();
    }

    public void onlineSendCommand(String method) {
        final String methodTask = method;
        if (this.mBoundService != null) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    OnlineActivity.this.mBoundService.sendRequest(methodTask, null);
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    public void onlineSendCommand(String method, String[] attrs) {
        final String methodTask = method;
        final String[] attrsTask = attrs;
        if (this.mBoundService != null) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    OnlineActivity.this.mBoundService.sendRequest(methodTask, attrsTask);
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    public void onlineSendMessageTo(String method, String[] attrs, String to) {
        final String methodTask = method;
        final String[] attrsTask = attrs;
        final String toTask = to;
        if (this.mBoundService != null) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    OnlineActivity.this.mBoundService.sendRequestTo(methodTask, attrsTask, toTask);
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    public void onlineSendResponse(Network.GenericRequest request, int code, String[] attrs) {
        final Network.GenericRequest requestTask = request;
        final int codeTask = code;
        final String[] attrsTask = attrs;
        if (this.mBoundService != null) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    OnlineActivity.this.mBoundService.sendResponse(requestTask, codeTask, attrsTask);
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    public void onOnlineConnected() {
        this.isOnline = true;
        hideConnectionNotifications();
    }

    public void onOnlineConnectionProgress(OnlineService.ConnectionProgressStates state) {
        this.isOnline = false;
        switch ($SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$ConnectionProgressStates()[state.ordinal()]) {
            case 1:
                showConnectionProgress(R.string.online_connecting);
                return;
            case 2:
                showConnectionProgress(R.string.online_connected);
                return;
            case 3:
                showConnectionProgress(R.string.online_registering);
                return;
            default:
                return;
        }
    }

    public void onOnlineDisconnected(OnlineService.DisconnectionReasons reason) {
        this.isOnline = false;
        switch ($SWITCH_TABLE$com$mocelet$fourinrow$online$OnlineService$DisconnectionReasons()[reason.ordinal()]) {
            case 1:
                showConnectionError(R.string.online_no_connection);
                return;
            case 2:
                if (!this.showingDialog) {
                    showConnectionError(R.string.online_connection_lost);
                    return;
                }
                return;
            case 3:
                showUpdateNeededDialog();
                return;
            case 4:
                showConnectionError(R.string.online_too_many_users);
                return;
            case 5:
                showConnectionError(R.string.online_maintenance);
                return;
            case 6:
                showConnectionError(R.string.online_server_error);
                return;
            case 7:
                showConnectionError(R.string.online_no_registration);
                return;
            case 8:
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean checkNewsUpdate(Network.GenericResponse response) {
        if (response.method.equals(Network.INFO_METHOD) && response.attributes != null && response.attributes.length >= 4) {
            try {
                if (!response.attributes[0].equals(NEWS_ATTR)) {
                    return false;
                }
                String newsId = response.attributes[1];
                String newsTextHeadline = response.attributes[2];
                String newsTextFull = response.attributes[3];
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                editor.putString("online_latest_news_full", newsTextFull);
                editor.putString("online_latest_news_headline", newsTextHeadline);
                editor.putString("online_latest_news_id", newsId);
                editor.commit();
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    public void onOnlineRequestReceived(Network.GenericRequest request) {
    }

    public void onOnlineResponseReceived(Network.GenericResponse response) {
    }

    public void onOnlineStatsUpdate() {
    }
}
