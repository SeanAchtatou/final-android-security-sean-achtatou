package com.mocelet.fourinrow;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class BoardThemeManager {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers = null;
    public static final int GLASS_THEME = 30;
    public static final int GRAYSCALE_THEME = 100;
    public static final int METAL_THEME = 10;
    public static final int VECTOR_THEME = 0;
    public static final int WOOD_THEME = 20;
    private int theme;

    public enum BoardLayers {
        FRONT_NORMAL,
        MIDDLE_LIGHTER,
        BACK_DARKER
    }

    public enum Discs {
        DISC_PLAYER_1,
        DISC_PLAYER_2
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers;
        if (iArr == null) {
            iArr = new int[BoardLayers.values().length];
            try {
                iArr[BoardLayers.BACK_DARKER.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[BoardLayers.FRONT_NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[BoardLayers.MIDDLE_LIGHTER.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers = iArr;
        }
        return iArr;
    }

    public BoardThemeManager(int theme2) {
        setTheme(theme2);
    }

    public void setTheme(int theme2) {
        this.theme = theme2;
    }

    public int getTheme() {
        return this.theme;
    }

    public int getLastColumnMarkerColor() {
        switch (this.theme) {
            case 10:
                return Color.parseColor("#66acc465");
            case WOOD_THEME /*20*/:
                return Color.parseColor("#66fcd175");
            case GLASS_THEME /*30*/:
                return Color.parseColor("#66dbdbdb");
            case 100:
                return Color.parseColor("#FFFFFFFF");
            default:
                return Color.parseColor("#66b6d959");
        }
    }

    public Paint getWinningMarkerPaint(Resources res, float separation) {
        Paint winningMarkerPaint = new Paint(1);
        winningMarkerPaint.setStyle(Paint.Style.STROKE);
        winningMarkerPaint.setStrokeWidth(separation - 1.0f);
        switch (this.theme) {
            case 10:
                winningMarkerPaint.setColor(Color.parseColor("#bba1bb00"));
                break;
            case WOOD_THEME /*20*/:
                winningMarkerPaint.setColor(Color.parseColor("#bbdddb4b"));
                break;
            case GLASS_THEME /*30*/:
                winningMarkerPaint.setColor(Color.parseColor("#96ffff00"));
                break;
            case 100:
                winningMarkerPaint.setColor(Color.parseColor("#bb000000"));
                break;
            default:
                winningMarkerPaint.setColor(res.getColor(R.color.winning_marker));
                break;
        }
        return winningMarkerPaint;
    }

    public Bitmap getPlayerDisc(Resources res, Discs player, int diameter) {
        Bitmap disc = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(disc);
        if (this.theme == 10) {
            Bitmap metalDisc = BitmapFactory.decodeResource(res, player == Discs.DISC_PLAYER_1 ? R.drawable.metal_white_disc : R.drawable.metal_red_disc);
            Paint paint = new Paint();
            paint.setFilterBitmap(true);
            canvas.drawBitmap(metalDisc, (Rect) null, new Rect(0, 0, disc.getWidth(), disc.getHeight()), paint);
            metalDisc.recycle();
        } else if (this.theme == 20) {
            Bitmap woodDisc = BitmapFactory.decodeResource(res, player == Discs.DISC_PLAYER_1 ? R.drawable.wood_white_disc : R.drawable.wood_red_disc);
            Paint paint2 = new Paint();
            paint2.setFilterBitmap(true);
            canvas.drawBitmap(woodDisc, (Rect) null, new Rect(0, 0, disc.getWidth(), disc.getHeight()), paint2);
            woodDisc.recycle();
        } else if (this.theme == 30) {
            Bitmap glassDisc = BitmapFactory.decodeResource(res, player == Discs.DISC_PLAYER_1 ? R.drawable.glass_white_disc : R.drawable.glass_red_disc);
            Paint paint3 = new Paint();
            paint3.setFilterBitmap(true);
            canvas.drawBitmap(glassDisc, (Rect) null, new Rect(0, 0, disc.getWidth(), disc.getHeight()), paint3);
            glassDisc.recycle();
        } else if (this.theme == 100) {
            Bitmap grayscaleDisc = BitmapFactory.decodeResource(res, player == Discs.DISC_PLAYER_1 ? R.drawable.grayscale_white_disc : R.drawable.grayscale_red_disc);
            Paint paint4 = new Paint();
            paint4.setFilterBitmap(true);
            canvas.drawBitmap(grayscaleDisc, (Rect) null, new Rect(0, 0, disc.getWidth(), disc.getHeight()), paint4);
            grayscaleDisc.recycle();
        } else {
            Drawable vectorDisc = player == Discs.DISC_PLAYER_1 ? res.getDrawable(R.drawable.white_piece) : res.getDrawable(R.drawable.red_piece);
            vectorDisc.setBounds(0, 0, disc.getWidth(), disc.getHeight());
            vectorDisc.draw(canvas);
        }
        return disc;
    }

    public Drawable getBoardDrawable(Resources res, BoardLayers layer) {
        if (this.theme == 10) {
            BitmapDrawable drawable = new BitmapDrawable(BitmapFactory.decodeResource(res, R.drawable.metal_board));
            drawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    drawable.setColorFilter(Color.parseColor("#11FFFFFF"), PorterDuff.Mode.LIGHTEN);
                    break;
                case 3:
                    drawable.setColorFilter(Color.parseColor("#44000000"), PorterDuff.Mode.DARKEN);
                    break;
            }
            return drawable;
        } else if (this.theme == 20) {
            BitmapDrawable drawable2 = new BitmapDrawable(BitmapFactory.decodeResource(res, R.drawable.wood_board));
            drawable2.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    drawable2.setColorFilter(Color.parseColor("#11FFFFFF"), PorterDuff.Mode.LIGHTEN);
                    break;
                case 3:
                    drawable2.setColorFilter(Color.parseColor("#44000000"), PorterDuff.Mode.DARKEN);
                    break;
            }
            return drawable2;
        } else if (this.theme == 30) {
            BitmapDrawable drawable3 = new BitmapDrawable(BitmapFactory.decodeResource(res, R.drawable.glass_board));
            drawable3.setTileModeX(Shader.TileMode.MIRROR);
            drawable3.setTileModeY(Shader.TileMode.MIRROR);
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    drawable3.setColorFilter(Color.parseColor("#11FFFFFF"), PorterDuff.Mode.LIGHTEN);
                    break;
                case 3:
                    drawable3.setColorFilter(Color.parseColor("#77000000"), PorterDuff.Mode.DARKEN);
                    break;
            }
            return drawable3;
        } else if (this.theme == 100) {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    return res.getDrawable(R.drawable.grayscale_board_middle);
                case 3:
                    return res.getDrawable(R.drawable.grayscale_board_darker);
                default:
                    return res.getDrawable(R.drawable.grayscale_board);
            }
        } else {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    return res.getDrawable(R.drawable.board_middle);
                case 3:
                    return res.getDrawable(R.drawable.board_darker);
                default:
                    return res.getDrawable(R.drawable.board);
            }
        }
    }

    public Bitmap getMiniPlayerDisc(Resources res, Discs player, int diameter) {
        Bitmap disc = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(disc);
        if (this.theme == 10) {
            Bitmap metalDisc = BitmapFactory.decodeResource(res, player == Discs.DISC_PLAYER_1 ? R.drawable.mini_metal_white_disc : R.drawable.mini_metal_red_disc);
            Paint paint = new Paint();
            paint.setFilterBitmap(true);
            canvas.drawBitmap(metalDisc, (Rect) null, new Rect(0, 0, disc.getWidth(), disc.getHeight()), paint);
            metalDisc.recycle();
        } else if (this.theme == 20) {
            Bitmap woodDisc = BitmapFactory.decodeResource(res, player == Discs.DISC_PLAYER_1 ? R.drawable.mini_wood_white_disc : R.drawable.mini_wood_red_disc);
            Paint paint2 = new Paint();
            paint2.setFilterBitmap(true);
            canvas.drawBitmap(woodDisc, (Rect) null, new Rect(0, 0, disc.getWidth(), disc.getHeight()), paint2);
            woodDisc.recycle();
        } else if (this.theme == 30) {
            Bitmap glassDisc = BitmapFactory.decodeResource(res, player == Discs.DISC_PLAYER_1 ? R.drawable.mini_glass_white_disc : R.drawable.mini_glass_red_disc);
            Paint paint3 = new Paint();
            paint3.setFilterBitmap(true);
            canvas.drawBitmap(glassDisc, (Rect) null, new Rect(0, 0, disc.getWidth(), disc.getHeight()), paint3);
            glassDisc.recycle();
        } else {
            Drawable vectorDisc = player == Discs.DISC_PLAYER_1 ? res.getDrawable(R.drawable.white_piece) : res.getDrawable(R.drawable.red_piece);
            vectorDisc.setBounds(0, 0, disc.getWidth(), disc.getHeight());
            vectorDisc.draw(canvas);
        }
        return disc;
    }

    public Drawable getMiniBoardDrawable(Resources res, BoardLayers layer) {
        if (this.theme == 10) {
            BitmapDrawable drawable = new BitmapDrawable(BitmapFactory.decodeResource(res, R.drawable.mini_metal_board));
            drawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    drawable.setColorFilter(Color.parseColor("#11FFFFFF"), PorterDuff.Mode.LIGHTEN);
                    break;
                case 3:
                    drawable.setColorFilter(Color.parseColor("#77000000"), PorterDuff.Mode.DARKEN);
                    break;
            }
            return drawable;
        } else if (this.theme == 20) {
            BitmapDrawable drawable2 = new BitmapDrawable(BitmapFactory.decodeResource(res, R.drawable.mini_wood_board));
            drawable2.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    drawable2.setColorFilter(Color.parseColor("#11FFFFFF"), PorterDuff.Mode.LIGHTEN);
                    break;
                case 3:
                    drawable2.setColorFilter(Color.parseColor("#77000000"), PorterDuff.Mode.DARKEN);
                    break;
            }
            return drawable2;
        } else if (this.theme == 30) {
            BitmapDrawable drawable3 = new BitmapDrawable(BitmapFactory.decodeResource(res, R.drawable.mini_glass_board));
            drawable3.setTileModeX(Shader.TileMode.MIRROR);
            drawable3.setTileModeY(Shader.TileMode.MIRROR);
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    drawable3.setColorFilter(Color.parseColor("#11FFFFFF"), PorterDuff.Mode.LIGHTEN);
                    break;
                case 3:
                    drawable3.setColorFilter(Color.parseColor("#77000000"), PorterDuff.Mode.DARKEN);
                    break;
            }
            return drawable3;
        } else {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$BoardThemeManager$BoardLayers()[layer.ordinal()]) {
                case 2:
                    return res.getDrawable(R.drawable.board_middle);
                case 3:
                    return res.getDrawable(R.drawable.board_darker);
                default:
                    return res.getDrawable(R.drawable.mini_board);
            }
        }
    }
}
