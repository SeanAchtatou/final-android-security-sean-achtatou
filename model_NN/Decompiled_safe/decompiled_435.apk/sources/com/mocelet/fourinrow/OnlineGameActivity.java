package com.mocelet.fourinrow;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.mocelet.fourinrow.GameState;
import com.mocelet.fourinrow.OnlineActivity;
import com.mocelet.fourinrow.online.Network;

public class OnlineGameActivity extends OnlineActivity implements BoardListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$OnlineStates = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$RematchEvents = null;
    private static final String BYE_REASON_EARLY_ATTR = "EARLY";
    private static final String BYE_REASON_QUITTER_ATTR = "QUITTER";
    private static final String BYE_REASON_SERVER_GENERATED_ATTR = "SERVER_DISCONNECTION_NOTIFY";
    private static final String BYE_REASON_TIRED_ATTR = "TIRED";
    private static final String COL_ATTR = "COL";
    private static final int LEGIT_QUITTING_PERIOD_MS = 15000;
    private static final String ME_FIRST = "me_first";
    private static final int PING_TIMEOUT_MESSAGE = 4;
    private static final String REMATCH_ATTR = "REMATCH";
    private static final String REMATCH_SUPPORT = "rematch_support";
    private static final int RESEND_COL_MESSAGE = 5;
    private static final int SEND_PING_MESSAGE = 2;
    private static final int SEND_PING_REMATCH_MESSAGE = 200;
    private static final int SEND_REMATCH_MESSAGE = 7;
    private static final int START_REMATCH_MESSAGE = 9;
    private static final int SWITCH_STATE_FAIL_MESSAGE = 0;
    private static final String TAG = "OnlineGameActivity";
    private static final int WAITING_TIME_FOR_PERIODIC_COL_SENT = 12000;
    private static final int WAITING_TIME_FOR_PING_REPLY = 6000;
    private static final int WAITING_TIME_FOR_REMATCH_RESEND = 5000;
    private static final int WAITING_TIME_FOR_TURN_PING_TEST = 12000;
    private static final String YOU_FIRST = "you_first";
    private AdView adView;
    private boolean animationEnabled;
    private BoardView boardView;
    private boolean elementsSet;
    private boolean endSoundPlayed;
    private String failReasonPhrase;
    private GameState.Players firstPlayer;
    /* access modifiers changed from: private */
    public GameState gameState;
    private Handler handler;
    private boolean hideAlias;
    private TextView infoTextView;
    /* access modifiers changed from: private */
    public int lastColSent;
    private int lastGamemoveReceived;
    /* access modifiers changed from: private */
    public long lastRemoteTurnTime;
    private String localId;
    private GameState.Players localPlayer;
    private long matchStartTime;
    private boolean meFirst;
    private OnlineStates onlineState;
    private View quittersNoticeContainer;
    private boolean quittingNoticeVisible;
    private Drawable rematchAcceptBackground;
    private Button rematchButton;
    private boolean rematchRejected;
    private boolean rematchSupport;
    private boolean rematchWantedLocal;
    private boolean rematchWantedRemote;
    /* access modifiers changed from: private */
    public String remoteId;
    /* access modifiers changed from: private */
    public GameState.Players remotePlayer;
    private ViewGroup scoreBoard;
    private TextView scoreBoardText;
    private boolean showLastMove;
    private boolean showingAds;
    private boolean solidBlackBackground;
    private boolean soundEnabled;
    private boolean switchColors;
    private int theme;

    private enum OnlineStates {
        INIT,
        INITIAL_PING_WAIT,
        READY,
        FINISHED,
        FAIL
    }

    private enum RematchEvents {
        START,
        OFFER_SENT,
        OFFER_RECEIVED,
        REJECTED
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States;
        if (iArr == null) {
            iArr = new int[GameState.States.values().length];
            try {
                iArr[GameState.States.FINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameState.States.PAUSED.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameState.States.PLAYING.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[GameState.States.RESET.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$GameState$States = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$OnlineStates() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$OnlineStates;
        if (iArr == null) {
            iArr = new int[OnlineStates.values().length];
            try {
                iArr[OnlineStates.FAIL.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OnlineStates.FINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OnlineStates.INIT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[OnlineStates.INITIAL_PING_WAIT.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[OnlineStates.READY.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$OnlineStates = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$RematchEvents() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$RematchEvents;
        if (iArr == null) {
            iArr = new int[RematchEvents.values().length];
            try {
                iArr[RematchEvents.OFFER_RECEIVED.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[RematchEvents.OFFER_SENT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[RematchEvents.REJECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[RematchEvents.START.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$RematchEvents = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStartMode(OnlineActivity.StartMode.CONNECT_ON_START);
        setVolumeControlStream(3);
        this.elementsSet = false;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.registerOnSharedPreferenceChangeListener(this);
        this.switchColors = prefs.getBoolean("switch_colors", false);
        this.showLastMove = prefs.getBoolean("show_last_move", true);
        this.soundEnabled = prefs.getBoolean("sound_enabled", true);
        this.hideAlias = prefs.getBoolean("online_hide_alias", false);
        this.animationEnabled = prefs.getBoolean("animation_enabled", true);
        this.solidBlackBackground = prefs.getBoolean("black_background", false);
        try {
            this.theme = Integer.parseInt(prefs.getString("skin", "10"));
        } catch (NumberFormatException e) {
        }
        onSharedPreferenceChanged(prefs, "");
        this.localPlayer = GameState.Players.PLAYER_1;
        this.remotePlayer = GameState.Players.PLAYER_2;
        this.localId = getIntent().getExtras().getString("local_id");
        this.remoteId = getIntent().getExtras().getString("remote_id");
        this.meFirst = getIntent().getExtras().getBoolean(ME_FIRST);
        this.rematchSupport = getIntent().getBooleanExtra(REMATCH_SUPPORT, false);
        this.firstPlayer = this.meFirst ? GameState.Players.PLAYER_1 : GameState.Players.PLAYER_2;
        this.lastGamemoveReceived = -1;
        this.lastColSent = -1;
        this.endSoundPlayed = false;
        if (savedInstanceState != null) {
            restoreState(savedInstanceState);
        }
        this.gameState = new GameState();
        this.gameState.startGame(this.firstPlayer);
        this.matchStartTime = System.currentTimeMillis();
        this.lastRemoteTurnTime = 0;
        setupViewElements();
        onSharedPreferenceChanged(prefs, "");
        this.adView = (AdView) findViewById(R.id.adView);
        hideAds();
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 0) {
                    OnlineGameActivity.this.switchState(OnlineStates.FAIL);
                }
                if (msg.what == 4) {
                    OnlineGameActivity.this.switchState(OnlineStates.FAIL);
                }
                if (msg.what == 2 && OnlineGameActivity.this.isOnline()) {
                    OnlineGameActivity.this.onlineSendMessageTo(Network.PING_METHOD, null, OnlineGameActivity.this.remoteId);
                    OnlineGameActivity.this.startPingTimer();
                    OnlineGameActivity.this.startPeriodicPingTimer();
                }
                if (msg.what == 200 && OnlineGameActivity.this.isOnline()) {
                    OnlineGameActivity.this.onlineSendMessageTo(Network.PING_METHOD, null, OnlineGameActivity.this.remoteId);
                    OnlineGameActivity.this.startPeriodicPingDuringRematch();
                }
                if (msg.what == 7) {
                    OnlineGameActivity.this.sendRematchOffer();
                }
                if (msg.what == 5) {
                    OnlineGameActivity.this.sendMoveToParty(OnlineGameActivity.this.lastColSent);
                }
                if (msg.what == 9) {
                    OnlineGameActivity.this.startRematch();
                }
            }
        };
        switchState(OnlineStates.INIT);
        updateElements();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.boardView != null) {
            this.boardView.onPausing();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.boardView != null) {
            this.boardView.onResuming();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setTiledBackground();
        if (this.boardView != null) {
            this.boardView.setListener(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.boardView != null) {
            this.boardView.setListener(null);
        }
        if (this.boardView != null) {
            this.boardView.recycleBitmaps();
        }
        stopTimers();
        stopRematchTimers();
        updateRematchState(RematchEvents.REJECTED);
        System.gc();
    }

    private void setTiledBackground() {
        if (this.solidBlackBackground) {
            getWindow().setBackgroundDrawableResource(R.drawable.black_background);
            return;
        }
        BitmapDrawable tiledBg = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.tile_black_stripes));
        tiledBg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        getWindow().setBackgroundDrawable(tiledBg);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.adView != null) {
            this.adView.destroy();
        }
        System.gc();
    }

    private void restoreState(Bundle savedInstanceState) {
    }

    private void hideAds() {
        this.adView.setVisibility(8);
        this.showingAds = false;
    }

    private void showAds() {
        if (!this.showingAds) {
            AdRequest request = new AdRequest();
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            this.adView.setVisibility(0);
            this.adView.loadAd(request);
            this.showingAds = true;
        }
    }

    public void onQuitterKeepPlaying(View v) {
        setQuittersNotice(false);
    }

    public void onQuitterQuit(View v) {
        setQuittersNotice(false);
        doQuitterBye();
        finish();
    }

    public void onPlayAgain(View v) {
        if (isOnline()) {
            onlineSendMessageTo(Network.BYE_METHOD, null, this.remoteId);
        }
        startActivity(new Intent(this, OnlineMatchmakingActivity.class));
        finish();
    }

    /* access modifiers changed from: private */
    public void startRematch() {
        stopTimers();
        stopRematchTimers();
        Intent intent = new Intent(this, OnlineMatchPreviewActivity.class);
        intent.putExtra("local_id", this.localId);
        intent.putExtra("remote_id", this.remoteId);
        intent.putExtra(ME_FIRST, !this.meFirst);
        intent.putExtra(REMATCH_SUPPORT, this.rematchSupport);
        startActivity(intent);
        finish();
    }

    public void onRematch(View v) {
        if (!this.rematchRejected && !this.rematchWantedLocal) {
            sendRematchOffer();
        } else if (this.rematchRejected) {
            Toast.makeText(this, getString(R.string.online_playing_rematch_rejected_toast), 0).show();
        }
    }

    public void onPlayColumn(int column) {
        if (this.gameState.getState() == GameState.States.PLAYING) {
            if (!this.gameState.isColumnAvailable(column)) {
                Toast.makeText(this, (int) R.string.info_column_full, 0).show();
                return;
            }
            this.infoTextView.setVisibility(4);
            this.boardView.setTouchable(false);
            final int col = column;
            new AsyncTask<BoardView, Integer, Integer>() {
                /* access modifiers changed from: protected */
                public Integer doInBackground(BoardView... params) {
                    params[0].doDropPieceAnimation(col);
                    return null;
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Integer result) {
                    super.onPostExecute((Object) result);
                    OnlineGameActivity.this.gameState.put(col);
                    if (OnlineGameActivity.this.gameState.getTurn() == OnlineGameActivity.this.remotePlayer) {
                        OnlineGameActivity.this.lastRemoteTurnTime = System.currentTimeMillis();
                        OnlineGameActivity.this.sendMoveToParty(col);
                    }
                    OnlineGameActivity.this.updateElements();
                }
            }.execute(this.boardView);
        }
    }

    public void onColumnPlayed(int c) {
        onPlayColumn(c);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    }

    /* access modifiers changed from: private */
    public void updateElements() {
        if (this.elementsSet) {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$States()[this.gameState.getState().ordinal()]) {
                case 2:
                    hideAds();
                    this.endSoundPlayed = false;
                    this.scoreBoard.setVisibility(4);
                    this.rematchButton.setVisibility(4);
                    if (this.gameState.getTurn() != GameState.Players.PLAYER_1) {
                        if (this.gameState.getTurn() == GameState.Players.PLAYER_2) {
                            this.boardView.setTouchable(false);
                            this.infoTextView.setText(String.format(getString(R.string.online_opponent_turn_info), getNickFromId(this.remoteId)));
                            this.infoTextView.setVisibility(0);
                            break;
                        }
                    } else {
                        this.infoTextView.setText((int) R.string.info_your_turn);
                        this.infoTextView.setVisibility(0);
                        this.boardView.setTouchable(true);
                        break;
                    }
                    break;
                case 4:
                    showAds();
                    setQuittersNotice(false);
                    this.scoreBoardText.setText("");
                    this.scoreBoard.setVisibility(0);
                    if (this.rematchSupport && this.onlineState == OnlineStates.READY) {
                        this.rematchButton.setVisibility(0);
                    }
                    OnlineStates onlineStates = OnlineStates.READY;
                    if (this.animationEnabled) {
                        this.scoreBoard.startAnimation(AnimationUtils.loadAnimation(this, R.anim.winner_button));
                    }
                    this.infoTextView.setVisibility(0);
                    if (this.gameState.getWinner() != GameState.Players.PLAYER_1) {
                        if (this.gameState.getWinner() != GameState.Players.PLAYER_2) {
                            if (this.gameState.getWinner() == GameState.Players.NONE) {
                                this.infoTextView.setText((int) R.string.info_you_tie);
                                break;
                            }
                        } else {
                            this.infoTextView.setText(String.format(getString(R.string.info_someone_wins), getNickFromId(this.remoteId)));
                            if (!this.endSoundPlayed) {
                                this.boardView.playSound(3);
                                this.endSoundPlayed = true;
                                break;
                            }
                        }
                    } else {
                        this.infoTextView.setText(String.format(getString(R.string.info_you_win_someone), getNickFromId(this.remoteId)));
                        if (!this.endSoundPlayed) {
                            this.boardView.playSound(2);
                            this.endSoundPlayed = true;
                            break;
                        }
                    }
                    break;
            }
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$OnlineStates()[this.onlineState.ordinal()]) {
                case 1:
                case 2:
                    this.boardView.setTouchable(false);
                    this.infoTextView.setText((int) R.string.online_playing_pinging);
                    break;
                case 5:
                    this.boardView.setTouchable(false);
                    setQuittersNotice(false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setCancelable(true);
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            OnlineGameActivity.this.startActivity(new Intent(this, OnlineMatchmakingActivity.class));
                            OnlineGameActivity.this.finish();
                        }
                    });
                    if (this.failReasonPhrase == null || this.failReasonPhrase.length() < 1) {
                        builder.setMessage((int) R.string.online_playing_failed_desc);
                    } else {
                        builder.setMessage(this.failReasonPhrase);
                    }
                    if (this.gameState.getState() != GameState.States.FINISHED) {
                        this.infoTextView.setText((int) R.string.online_playing_failed);
                        builder.show();
                        break;
                    }
                    break;
            }
            this.boardView.postInvalidate();
        }
    }

    private void callTestPlayer() {
        new AsyncTask<Integer, Integer, Integer>() {
            private int column;

            /* access modifiers changed from: protected */
            public Integer doInBackground(Integer... params) {
                this.column = 0;
                while (!OnlineGameActivity.this.gameState.isColumnAvailable(this.column)) {
                    this.column++;
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Integer result) {
                super.onPostExecute((Object) result);
                OnlineGameActivity.this.onPlayColumn(this.column);
            }
        }.execute(0);
    }

    private void setupViewElements() {
        setContentView((int) R.layout.online_game);
        this.infoTextView = (TextView) findViewById(R.id.info_text_area);
        this.boardView = (BoardView) findViewById(R.id.boardView);
        this.boardView.setGameState(this.gameState);
        this.boardView.setListener(this);
        this.boardView.setTheme(this.theme);
        this.boardView.setAnimated(this.animationEnabled);
        this.boardView.setSoundEnabled(this.soundEnabled);
        this.boardView.setLastColumnPlayedMarker(this.showLastMove);
        if (this.localPlayer == this.firstPlayer) {
            this.boardView.setSwitchColors(this.switchColors);
        } else {
            this.boardView.setSwitchColors(!this.switchColors);
        }
        this.scoreBoard = (ViewGroup) findViewById(R.id.scoreBoard);
        this.scoreBoardText = (TextView) findViewById(R.id.scoreTextView);
        this.rematchButton = (Button) findViewById(R.id.rematch_button);
        this.rematchAcceptBackground = getResources().getDrawable(R.drawable.translucent_green_button);
        this.quittersNoticeContainer = findViewById(R.id.quittersNoticeContainer);
        setQuittersNotice(this.quittingNoticeVisible);
        this.elementsSet = true;
    }

    private String getCodeFromId(String id) {
        int index = id.lastIndexOf(35);
        if (index > 0) {
            return id.substring(index);
        }
        return "";
    }

    private String getNickFromId(String id) {
        if (this.hideAlias && !id.equals(this.localId)) {
            return getString(R.string.online_hide_alias_name);
        }
        int index = id.lastIndexOf(35);
        return index > 0 ? id.substring(0, index) : id;
    }

    /* access modifiers changed from: private */
    public void switchState(OnlineStates state) {
        OnlineStates onlineStates = this.onlineState;
        this.onlineState = state;
        switch ($SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$OnlineStates()[state.ordinal()]) {
            case 2:
                onlineSendMessageTo(Network.PING_METHOD, null, this.remoteId);
                startPingTimer();
                break;
            case 3:
                if (this.gameState.getMoveCount() == 0 && this.firstPlayer == this.remotePlayer) {
                    startPeriodicPingTimer();
                    break;
                }
            case 4:
                stopTimers();
                setQuittersNotice(false);
                break;
            case 5:
                stopTimers();
                setQuittersNotice(false);
                break;
        }
        updateElements();
    }

    public void onOnlineConnected() {
        super.onOnlineConnected();
        switchState(OnlineStates.INITIAL_PING_WAIT);
    }

    public void onOnlineRequestReceived(Network.GenericRequest request) {
        super.onOnlineRequestReceived(request);
        if (this.onlineState != OnlineStates.FAIL) {
            if (request.method.equals(Network.INFO_METHOD) && request.from.equals(this.remoteId) && request.attributes.length >= 3) {
                try {
                    String reason = request.attributes[0];
                    int col = Integer.parseInt(request.attributes[1]);
                    int gameMoveCount = Integer.parseInt(request.attributes[2]);
                    if (reason.equals(COL_ATTR) && gameMoveCount == this.gameState.getMoveCount() + 1 && gameMoveCount != this.lastGamemoveReceived) {
                        this.lastGamemoveReceived = gameMoveCount;
                        stopPeriodicPingTimer();
                        stopColSentTimer();
                        onPlayColumn(col);
                    }
                } catch (NumberFormatException e) {
                }
            }
            if (request.method.equals(Network.INFO_METHOD) && request.from.equals(this.remoteId) && request.attributes.length >= 1 && request.attributes[0].equals(REMATCH_ATTR)) {
                updateRematchState(RematchEvents.OFFER_RECEIVED);
                onlineSendResponse(request, 200, new String[0]);
            }
            if (request.method.equals(Network.PING_METHOD) && request.from.equals(this.remoteId)) {
                stopPingTimer();
                onlineSendResponse(request, 200, new String[0]);
                if (this.onlineState == OnlineStates.INITIAL_PING_WAIT) {
                    switchState(OnlineStates.READY);
                }
            }
            if (request.method.equals(Network.BYE_METHOD) && request.from.equals(this.remoteId)) {
                if (request.attributes == null) {
                    setRemotePartyGone();
                } else if (request.attributes.length > 0) {
                    setRemotePartyGone(request.attributes[0]);
                }
            }
        }
    }

    public void onOnlineResponseReceived(Network.GenericResponse response) {
        super.onOnlineResponseReceived(response);
        if ((!response.method.equals(Network.INFO_METHOD) || !response.to.equals(this.remoteId) || response.code != 404) && response.code != 486) {
            if (response.method.equals(Network.INFO_METHOD) && response.to.equals(this.remoteId) && response.code == 200) {
                return;
            }
            if ((!response.method.equals(Network.PING_METHOD) || !response.to.equals(this.remoteId) || response.code != 404) && response.code != 486) {
                if (response.method.equals(Network.PING_METHOD) && response.to.equals(this.remoteId) && response.code == 200) {
                    stopPingTimer();
                    if (this.onlineState == OnlineStates.INITIAL_PING_WAIT) {
                        switchState(OnlineStates.READY);
                    }
                }
            } else if (this.onlineState != OnlineStates.INITIAL_PING_WAIT) {
                setRemotePartyGone();
            }
        } else if (this.onlineState != OnlineStates.INITIAL_PING_WAIT) {
            setRemotePartyGone();
        }
    }

    /* access modifiers changed from: private */
    public void sendMoveToParty(int col) {
        this.lastColSent = col;
        startColSentTimer();
        onlineSendMessageTo(Network.INFO_METHOD, new String[]{COL_ATTR, new StringBuilder(String.valueOf(col)).toString(), new StringBuilder(String.valueOf(this.gameState.getMoveCount())).toString()}, this.remoteId);
    }

    /* access modifiers changed from: private */
    public void sendRematchOffer() {
        if (isOnline() && !this.rematchRejected) {
            updateRematchState(RematchEvents.OFFER_SENT);
            onlineSendMessageTo(Network.INFO_METHOD, new String[]{REMATCH_ATTR}, this.remoteId);
            startRematchTimer();
        }
    }

    /* access modifiers changed from: private */
    public void startPingTimer() {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(4), 6000);
    }

    private void stopPingTimer() {
        this.handler.removeMessages(4);
    }

    private void startRematchTimer() {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(7), 5000);
    }

    private void stopRematchTimers() {
        this.handler.removeMessages(7);
        this.handler.removeMessages(9);
        this.handler.removeMessages(200);
    }

    private void startColSentTimer() {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(5), 12000);
    }

    private void stopColSentTimer() {
        this.handler.removeMessages(5);
    }

    /* access modifiers changed from: private */
    public void startPeriodicPingTimer() {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(2), 12000);
    }

    /* access modifiers changed from: private */
    public void startPeriodicPingDuringRematch() {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(200), 12000);
    }

    private void stopPeriodicPingDuringRematch() {
        this.handler.removeMessages(200);
    }

    private void stopPeriodicPingTimer() {
        this.handler.removeMessages(2);
    }

    private void stopTimers() {
        this.handler.removeMessages(4);
        this.handler.removeMessages(0);
        this.handler.removeMessages(2);
        this.handler.removeMessages(5);
    }

    private void setRemotePartyGone() {
        this.failReasonPhrase = "";
        updateRematchState(RematchEvents.REJECTED);
        switchState(OnlineStates.FAIL);
    }

    private void setRemotePartyGone(String byeAttr) {
        if (BYE_REASON_EARLY_ATTR.equals(byeAttr)) {
            this.failReasonPhrase = "";
        } else if (BYE_REASON_QUITTER_ATTR.equals(byeAttr)) {
            this.failReasonPhrase = getString(R.string.online_playing_quit_desc);
        } else if (BYE_REASON_TIRED_ATTR.equals(byeAttr)) {
            this.failReasonPhrase = getString(R.string.online_playing_tired_desc);
        } else if (BYE_REASON_SERVER_GENERATED_ATTR.equals(byeAttr)) {
            this.failReasonPhrase = getString(R.string.online_playing_disconnected_desc);
        } else {
            this.failReasonPhrase = "";
        }
        updateRematchState(RematchEvents.REJECTED);
        switchState(OnlineStates.FAIL);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateElements();
    }

    private void updateRematchState(RematchEvents event) {
        if (this.rematchSupport) {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$OnlineGameActivity$RematchEvents()[event.ordinal()]) {
                case 2:
                    this.rematchWantedLocal = true;
                    break;
                case 3:
                    this.rematchWantedRemote = true;
                    break;
                case 4:
                    this.rematchWantedRemote = false;
                    this.rematchRejected = true;
                    stopTimers();
                    break;
            }
            if (this.rematchRejected) {
                stopPeriodicPingDuringRematch();
                this.rematchButton.setText((int) R.string.online_playing_rematch_rejected);
                this.rematchButton.setBackgroundDrawable(null);
            } else if (this.rematchWantedRemote && !this.rematchWantedLocal) {
                startPeriodicPingDuringRematch();
                this.rematchButton.setText((int) R.string.online_playing_rematch_accept);
                this.rematchButton.setBackgroundDrawable(this.rematchAcceptBackground);
            } else if (this.rematchWantedLocal && !this.rematchWantedRemote) {
                stopPeriodicPingDuringRematch();
                this.rematchButton.setText((int) R.string.online_playing_rematch_waiting);
                this.rematchButton.setBackgroundDrawable(null);
            }
            if (!this.rematchRejected && this.rematchWantedLocal && this.rematchWantedRemote) {
                stopPeriodicPingDuringRematch();
                this.handler.sendMessage(this.handler.obtainMessage(9));
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && this.onlineState != OnlineStates.FAIL) {
            if (this.quittingNoticeVisible) {
                setQuittersNotice(false);
                return true;
            } else if (this.gameState.getState() == GameState.States.PLAYING) {
                long timeSinceStart = System.currentTimeMillis() - this.matchStartTime;
                long timeSinceOpponentMove = System.currentTimeMillis() - this.lastRemoteTurnTime;
                if (timeSinceStart <= 15000) {
                    doEarlyBye();
                    finish();
                    return true;
                } else if (this.gameState.getTurn() != this.remotePlayer || timeSinceOpponentMove <= 15000) {
                    setQuittersNotice(true);
                    return true;
                } else {
                    doTiredOfWaitingBye();
                    startActivity(new Intent(this, OnlineMatchmakingActivity.class));
                    finish();
                    return true;
                }
            } else if (isOnline()) {
                onlineSendMessageTo(Network.BYE_METHOD, null, this.remoteId);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void setQuittersNotice(boolean visibility) {
        this.quittingNoticeVisible = visibility;
        this.quittersNoticeContainer.setVisibility(this.quittingNoticeVisible ? 0 : 4);
    }

    private void doEarlyBye() {
        if (isOnline()) {
            onlineSendMessageTo(Network.BYE_METHOD, new String[]{BYE_REASON_EARLY_ATTR}, this.remoteId);
        }
    }

    private void doTiredOfWaitingBye() {
        if (isOnline()) {
            onlineSendMessageTo(Network.BYE_METHOD, new String[]{BYE_REASON_TIRED_ATTR}, this.remoteId);
        }
    }

    private void doQuitterBye() {
        if (isOnline()) {
            onlineSendMessageTo(Network.BYE_METHOD, new String[]{BYE_REASON_QUITTER_ATTR}, this.remoteId);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                if (this.onlineState == OnlineStates.FAIL || this.gameState.getState() != GameState.States.PLAYING) {
                    Intent intent = new Intent(this, CuatroEnLinea.class);
                    intent.addFlags(67108864);
                    startActivity(intent);
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }
}
