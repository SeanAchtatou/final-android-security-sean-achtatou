package com.mocelet.fourinrow;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.mocelet.fourinrow.BoardThemeManager;
import com.mocelet.fourinrow.OnlineActivity;
import com.mocelet.fourinrow.online.Network;
import com.mocelet.fourinrow.online.OnlineService;
import org.objectweb.asm.Opcodes;

public class OnlineMatchPreviewActivity extends OnlineActivity {
    private static final String REMATCH_SUPPORT = "rematch_support";
    private static final int SEARCH_AGAIN_DELAY_AFTER_REJECTED = 5000;
    private static final int SEARCH_AGAIN_DELAY_AFTER_REJECTING = 1000;
    private static final int START_TICK_MESSAGE = 2;
    private static final int SWITCH_TO_PLAY_ACTIVITY_MESSAGE = 1;
    private static final int TICKS_TO_PLAY = 3;
    private static final int WAITING_TIME_PLAY_SCREEN = 3500;
    private TextView counterTextView;
    /* access modifiers changed from: private */
    public int currentStartingCount;
    private Handler handler;
    private boolean hideAlias;
    private TextView infoTextView;
    /* access modifiers changed from: private */
    public String localId;
    /* access modifiers changed from: private */
    public boolean meFirstTurn;
    private String player1id;
    private String player2id;
    private ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public boolean rematchSupport;
    /* access modifiers changed from: private */
    public String remoteId;
    /* access modifiers changed from: private */
    public Button searchAgainButton;
    private boolean soundEnabled;
    private boolean switchColors;
    private int theme;
    private BoardThemeManager themeManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStartMode(OnlineActivity.StartMode.CONNECT_ON_START);
        setVolumeControlStream(3);
        Intent intent = getIntent();
        this.localId = intent.getStringExtra("local_id");
        this.remoteId = intent.getStringExtra("remote_id");
        this.meFirstTurn = intent.getBooleanExtra("me_first", true);
        this.rematchSupport = intent.getBooleanExtra(REMATCH_SUPPORT, false);
        if (this.localId == null || this.remoteId == null) {
            finish();
        }
        this.player1id = this.meFirstTurn ? this.localId : this.remoteId;
        this.player2id = this.meFirstTurn ? this.remoteId : this.localId;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.switchColors = prefs.getBoolean("switch_colors", false);
        this.soundEnabled = prefs.getBoolean("sound_enabled", true);
        this.hideAlias = prefs.getBoolean("online_hide_alias", false);
        this.theme = 10;
        try {
            this.theme = Integer.parseInt(prefs.getString("skin", "10"));
        } catch (NumberFormatException e) {
        }
        this.themeManager = new BoardThemeManager(this.theme);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(true);
        this.progressDialog.setTitle("");
        this.progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                OnlineMatchPreviewActivity.this.finish();
            }
        });
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) {
                    OnlineMatchPreviewActivity.this.searchAgainButton.setVisibility(4);
                    OnlineMatchPreviewActivity.this.hideStartingText();
                    if (OnlineMatchPreviewActivity.this.isOnline()) {
                        OnlineMatchPreviewActivity.this.onlineSendCommand(Network.PLAYING_START_METHOD);
                    }
                    Intent intent = new Intent(this, OnlineGameActivity.class);
                    intent.putExtra("local_id", OnlineMatchPreviewActivity.this.localId);
                    intent.putExtra("remote_id", OnlineMatchPreviewActivity.this.remoteId);
                    intent.putExtra("me_first", OnlineMatchPreviewActivity.this.meFirstTurn);
                    intent.putExtra(OnlineMatchPreviewActivity.REMATCH_SUPPORT, OnlineMatchPreviewActivity.this.rematchSupport);
                    OnlineMatchPreviewActivity.this.startActivity(intent);
                    OnlineMatchPreviewActivity.this.finish();
                }
                if (msg.what == 2) {
                    if (OnlineMatchPreviewActivity.this.currentStartingCount <= 0) {
                        OnlineMatchPreviewActivity.this.searchAgainButton.setVisibility(4);
                    }
                    if (OnlineMatchPreviewActivity.this.currentStartingCount >= 0) {
                        OnlineMatchPreviewActivity.this.showAndUpdateStartingText();
                    }
                    OnlineMatchPreviewActivity onlineMatchPreviewActivity = OnlineMatchPreviewActivity.this;
                    onlineMatchPreviewActivity.currentStartingCount = onlineMatchPreviewActivity.currentStartingCount - 1;
                }
            }
        };
        updateElements();
        hideStartingText();
        startGoPlayTimer();
    }

    private void updateElements() {
        Bitmap playerDisc;
        Bitmap playerDisc2;
        Resources myRes = getResources();
        setContentView((int) R.layout.match_preview);
        this.searchAgainButton = (Button) findViewById(R.id.search_again_button);
        this.counterTextView = (TextView) findViewById(R.id.starting_counter_text);
        this.infoTextView = (TextView) findViewById(R.id.starting_info_text);
        ImageView imagePiece1 = (ImageView) findViewById(R.id.player1_piece);
        ImageView imagePiece2 = (ImageView) findViewById(R.id.player2_piece);
        int diameter = Opcodes.ISHL;
        if (imagePiece1.getWidth() > 0) {
            diameter = imagePiece1.getWidth();
        }
        if (this.switchColors) {
            playerDisc = this.themeManager.getPlayerDisc(myRes, BoardThemeManager.Discs.DISC_PLAYER_2, diameter);
        } else {
            playerDisc = this.themeManager.getPlayerDisc(myRes, BoardThemeManager.Discs.DISC_PLAYER_1, diameter);
        }
        imagePiece1.setImageBitmap(playerDisc);
        if (this.switchColors) {
            playerDisc2 = this.themeManager.getPlayerDisc(myRes, BoardThemeManager.Discs.DISC_PLAYER_1, diameter);
        } else {
            playerDisc2 = this.themeManager.getPlayerDisc(myRes, BoardThemeManager.Discs.DISC_PLAYER_2, diameter);
        }
        imagePiece2.setImageBitmap(playerDisc2);
        this.searchAgainButton.setVisibility(0);
        showAndUpdateStartingText();
        this.progressDialog.hide();
        ((TextView) findViewById(R.id.player1_name)).setText(getNickFromId(this.player1id));
        ((TextView) findViewById(R.id.player2_name)).setText(getNickFromId(this.player2id));
        ((TextView) findViewById(R.id.player1_code)).setText(getCodeFromId(this.player1id));
        ((TextView) findViewById(R.id.player2_code)).setText(getCodeFromId(this.player2id));
    }

    private void startMatchmakingActivity(String text, int delay) {
        this.searchAgainButton.setVisibility(4);
        hideStartingText();
        if (isOnline()) {
            onlineSendCommand(Network.PLAYING_START_METHOD);
        }
        Intent intent = new Intent(this, OnlineMatchmakingActivity.class);
        intent.putExtra("initial_message", text);
        intent.putExtra("initial_delay", delay);
        startActivity(intent);
        finish();
    }

    public void onSearchAnother(View v) {
        stopTimers();
        onlineSendMessageTo(Network.BYE_METHOD, null, this.remoteId);
        startMatchmakingActivity(getString(R.string.online_matchmaking_rejecting_match), SEARCH_AGAIN_DELAY_AFTER_REJECTING);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        updateElements();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        stopTimers();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateElements();
    }

    public void onOnlineConnected() {
        super.onOnlineConnected();
    }

    public void onOnlineDisconnected(OnlineService.DisconnectionReasons reason) {
        super.onOnlineDisconnected(reason);
        stopTimers();
    }

    public void onOnlineStatsUpdate() {
        updateElements();
    }

    public void onOnlineRequestReceived(Network.GenericRequest request) {
        super.onOnlineRequestReceived(request);
        if (request.method.equals(Network.BYE_METHOD) && request.from.equals(this.remoteId)) {
            stopTimers();
            startMatchmakingActivity(getString(R.string.online_matchmaking_rejected_match), SEARCH_AGAIN_DELAY_AFTER_REJECTED);
        } else if (!request.method.equals(Network.PING_METHOD) || !request.from.equals(this.remoteId)) {
            onlineSendResponse(request, Network.BUSY_RESPONSE, new String[0]);
        }
    }

    public void onOnlineResponseReceived(Network.GenericResponse response) {
        super.onOnlineResponseReceived(response);
    }

    private void startGoPlayTimer() {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(1), 3500);
        this.handler.sendMessage(this.handler.obtainMessage(2));
        this.handler.sendMessageDelayed(this.handler.obtainMessage(2), 1000);
        this.handler.sendMessageDelayed(this.handler.obtainMessage(2), 2000);
        this.handler.sendMessageDelayed(this.handler.obtainMessage(2), 3000);
    }

    private void stopTimers() {
        if (this.handler != null) {
            this.handler.removeMessages(1);
            this.handler.removeMessages(2);
        }
    }

    /* access modifiers changed from: private */
    public void showAndUpdateStartingText() {
        if (this.currentStartingCount <= 0) {
            this.infoTextView.setText((int) R.string.online_matchmaking_starting_match);
            this.counterTextView.setVisibility(4);
            return;
        }
        this.infoTextView.setText((int) R.string.online_matchmaking_starting_text);
        this.counterTextView.setText(String.format(getString(R.string.online_matchmaking_starting), Integer.valueOf(this.currentStartingCount)));
        this.counterTextView.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void hideStartingText() {
        this.currentStartingCount = 3;
        this.counterTextView.setVisibility(4);
        this.infoTextView.setVisibility(4);
        this.handler.removeMessages(2);
    }

    private String getCodeFromId(String id) {
        int index = id.lastIndexOf(35);
        if (index > 0) {
            return id.substring(index);
        }
        return "";
    }

    private String getNickFromId(String id) {
        if (this.hideAlias && !id.equals(this.localId)) {
            return getString(R.string.online_hide_alias_name);
        }
        int index = id.lastIndexOf(35);
        return index > 0 ? id.substring(0, index) : id;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent intent = new Intent(this, CuatroEnLinea.class);
                intent.addFlags(67108864);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
