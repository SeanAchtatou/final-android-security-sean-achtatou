package com.mocelet.fourinrow;

import com.mocelet.fourinrow.GameState;
import java.lang.reflect.Array;
import java.util.Random;

public class GameAIFillAll {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players;
    private static int BABY_1_DISCS_SCORE = -5;
    private static int BABY_2_DISCS_SCORE = -5;
    private static int BABY_3_DISCS_SCORE = -5;
    private static int BABY_4_DISCS_SCORE = -4;
    private static int EASY_1_DISCS_SCORE = -10;
    private static int EASY_2_DISCS_SCORE = -15;
    private static int EASY_3_DISCS_SCORE = -20;
    private static int EASY_4_DISCS_SCORE = -20;
    private static int OPTIMAL_1_DISCS_SCORE = -5;
    private static int OPTIMAL_2_DISCS_SCORE = -10;
    private static int OPTIMAL_3_DISCS_SCORE = -20;
    private static int OPTIMAL_4_DISCS_SCORE = -50;
    private byte[][] casillas;
    private int cols;
    private int columnaElegida;
    private Skills difficulty;
    private int[] libre;
    private int numeroFichas = 0;
    private int profundidad;
    private int puntos1 = OPTIMAL_1_DISCS_SCORE;
    private int puntos2 = OPTIMAL_2_DISCS_SCORE;
    private int puntos3 = OPTIMAL_3_DISCS_SCORE;
    private int puntos4 = OPTIMAL_4_DISCS_SCORE;
    private int rows;
    private GameState state;
    private int turno;

    private enum Skills {
        BABY,
        EASY,
        NORMAL,
        HARD
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players;
        if (iArr == null) {
            iArr = new int[GameState.Players.values().length];
            try {
                iArr[GameState.Players.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameState.Players.PLAYER_1.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameState.Players.PLAYER_2.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players = iArr;
        }
        return iArr;
    }

    public GameAIFillAll(int nivel, GameState state2) {
        this.profundidad = nivel;
        if (nivel <= 0) {
            this.difficulty = Skills.BABY;
            this.profundidad = 1;
            this.puntos1 = BABY_1_DISCS_SCORE;
            this.puntos2 = BABY_2_DISCS_SCORE;
            this.puntos3 = BABY_3_DISCS_SCORE;
            this.puntos3 = BABY_4_DISCS_SCORE;
        } else if (nivel <= 3) {
            this.difficulty = Skills.EASY;
            this.profundidad = 1;
            this.puntos1 = EASY_1_DISCS_SCORE;
            this.puntos2 = EASY_2_DISCS_SCORE;
            this.puntos3 = EASY_3_DISCS_SCORE;
            this.puntos4 = EASY_4_DISCS_SCORE;
        } else if (nivel <= 4) {
            this.difficulty = Skills.NORMAL;
            this.puntos1 = OPTIMAL_1_DISCS_SCORE;
            this.puntos2 = OPTIMAL_2_DISCS_SCORE;
            this.puntos3 = OPTIMAL_3_DISCS_SCORE;
            this.puntos4 = OPTIMAL_4_DISCS_SCORE;
        } else {
            this.difficulty = Skills.HARD;
            this.puntos1 = OPTIMAL_1_DISCS_SCORE;
            this.puntos2 = OPTIMAL_2_DISCS_SCORE;
            this.puntos3 = OPTIMAL_3_DISCS_SCORE;
            this.puntos4 = OPTIMAL_4_DISCS_SCORE;
        }
        this.state = state2;
    }

    public int getBestMove() {
        int turno2;
        this.rows = this.state.getRows();
        this.cols = this.state.getCols();
        byte[][] casillas2 = (byte[][]) Array.newInstance(Byte.TYPE, this.rows, this.cols);
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[this.state.getFieldState(i, j).ordinal()]) {
                    case 1:
                        casillas2[i][j] = 1;
                        break;
                    case 2:
                        casillas2[i][j] = -1;
                        break;
                    default:
                        casillas2[i][j] = 0;
                        break;
                }
            }
        }
        if (this.state.getTurn() == GameState.Players.PLAYER_1) {
            turno2 = 1;
        } else {
            turno2 = -1;
        }
        int[] libre2 = new int[this.cols];
        for (int i2 = 0; i2 < libre2.length; i2++) {
            libre2[i2] = this.state.getColumnPeak(i2);
        }
        return getJugada(casillas2, libre2, turno2);
    }

    public int getJugada(byte[][] casillas2, int[] libre2, int turno2) {
        this.casillas = casillas2;
        this.libre = libre2;
        this.turno = turno2;
        this.numeroFichas = 0;
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                if (casillas2[i][j] != 0) {
                    this.numeroFichas++;
                }
            }
        }
        return algoritmo();
    }

    private void ponerFicha(int col) {
        this.casillas[this.libre[col]][col] = (byte) this.turno;
        this.libre[col] = this.libre[col] + 1;
        this.turno = -this.turno;
        this.numeroFichas++;
    }

    private void quitarFicha(int col) {
        this.casillas[this.libre[col] - 1][col] = 0;
        this.libre[col] = this.libre[col] - 1;
        this.turno = -this.turno;
        this.numeroFichas--;
    }

    private int[] posibles() {
        int longitud = 0;
        for (int i : this.libre) {
            if (i < this.rows) {
                longitud++;
            }
        }
        int[] posibles = new int[longitud];
        int indice = 0;
        for (int i2 = 0; i2 < this.libre.length; i2++) {
            if (this.libre[i2] < this.rows) {
                posibles[indice] = i2;
                indice++;
            }
        }
        return posibles;
    }

    private boolean grupoDe4(int ultimaFila, int ultimaColumna, byte jugador, int incFila, int incCol) {
        int encontradas = 1;
        int filaActual = ultimaFila;
        int colActual = ultimaColumna;
        boolean vuelta = false;
        while (true) {
            filaActual += incFila;
            colActual += incCol;
            if (filaActual >= 0 && colActual >= 0 && filaActual < this.rows && colActual < this.cols && this.casillas[filaActual][colActual] == jugador) {
                encontradas++;
                if (encontradas == 4) {
                    return true;
                }
            } else if (vuelta) {
                return false;
            } else {
                vuelta = true;
                incFila *= -1;
                incCol *= -1;
                filaActual = ultimaFila;
                colActual = ultimaColumna;
            }
        }
    }

    private int puntuacion(int ultimaColumna) {
        int valor_parcial = 0;
        for (int fila = 0; fila < this.rows; fila++) {
            for (int i = 0; i < this.cols - 3; i++) {
                valor_parcial += analizaGrupo(-this.turno, fila, i, 0, 1);
            }
        }
        for (int fila2 = 0; fila2 < this.rows - 3; fila2++) {
            for (int col = 0; col < this.cols - 3; col++) {
                valor_parcial += analizaGrupo(-this.turno, fila2, col, 1, 1);
            }
        }
        for (int fila3 = this.rows - 1; fila3 >= 3; fila3--) {
            for (int col2 = 0; col2 < this.cols - 3; col2++) {
                valor_parcial += analizaGrupo(-this.turno, fila3, col2, -1, 1);
            }
        }
        return valor_parcial;
    }

    private int analizaGrupo(int jugador, int fil0, int col0, int inc_fil, int inc_col) {
        int num_mias = 0;
        int num_suyas = 0;
        int num_huecos = 0;
        for (int i = 0; i < 4; i++) {
            if (this.casillas[(i * inc_fil) + fil0][(i * inc_col) + col0] == jugador) {
                num_mias++;
            } else if (this.casillas[(i * inc_fil) + fil0][(i * inc_col) + col0] == 0) {
                num_huecos++;
            } else {
                num_suyas++;
            }
        }
        if (num_mias == 4 && num_huecos == 0) {
            return this.puntos4;
        }
        if (num_suyas == 4 && num_huecos == 0) {
            return -this.puntos4;
        }
        if (num_mias == 3 && num_huecos == 1) {
            return this.puntos3;
        }
        if (num_suyas == 3 && num_huecos == 1) {
            return -this.puntos3;
        }
        if (num_mias == 2 && num_huecos == 2) {
            return this.puntos2;
        }
        if (num_suyas == 2 && num_huecos == 2) {
            return -this.puntos2;
        }
        if (num_mias == 1 && num_huecos == 3) {
            return this.puntos1;
        }
        if (num_suyas == 1 && num_huecos == 3) {
            return -this.puntos1;
        }
        return 0;
    }

    private int alphabeta(int ultimaColumna, int depth, int alpha, int beta) {
        int localalpha = alpha;
        int bestvalue = -1000000;
        if (depth == 0) {
            return puntuacion(ultimaColumna);
        }
        int[] posibles = posibles();
        if (posibles.length == 0) {
            return -5000;
        }
        for (int actual : posibles) {
            ponerFicha(actual);
            int value = -alphabeta(actual, depth - 1, -beta, -localalpha);
            quitarFicha(actual);
            if (value > bestvalue) {
                bestvalue = value;
            }
            if (bestvalue >= beta) {
                break;
            }
            if (bestvalue > localalpha) {
                localalpha = bestvalue;
            }
        }
        return bestvalue;
    }

    private int algoritmo() {
        int[] posibles = posibles();
        int mejorcol = -1;
        int max_punt = -2000000;
        if ((this.difficulty == Skills.EASY || this.difficulty == Skills.BABY) && (this.numeroFichas == 0 || this.numeroFichas == 1)) {
            return new Random().nextInt(this.cols);
        }
        if (this.difficulty == Skills.BABY && (this.numeroFichas == 2 || this.numeroFichas == 3)) {
            return new Random().nextInt(this.cols);
        }
        int originalProfundidad = this.profundidad;
        if (originalProfundidad >= 6) {
            if (this.cols <= 7) {
                this.profundidad = 6;
            } else {
                this.profundidad = 5;
            }
            if (posibles.length == 2) {
                this.profundidad = 14;
            } else if (posibles.length == 3) {
                this.profundidad = 10;
            } else if (posibles.length == 4) {
                this.profundidad = 8;
            }
        }
        if (this.cols >= 9) {
            if (this.difficulty == Skills.EASY) {
                this.profundidad = 1;
            }
            if (this.difficulty == Skills.NORMAL) {
                this.profundidad = 2;
            }
            if (this.difficulty == Skills.HARD) {
                this.profundidad = 3;
                if (posibles.length == 2) {
                    this.profundidad = 14;
                } else if (posibles.length == 3) {
                    this.profundidad = 10;
                } else if (posibles.length == 4) {
                    this.profundidad = 8;
                } else if (posibles.length == 5) {
                    this.profundidad = 6;
                } else if (posibles.length == 6) {
                    this.profundidad = 4;
                }
            }
        }
        for (int actual : posibles) {
            ponerFicha(actual);
            int punt = -alphabeta(actual, this.profundidad, -1000000, 1000000);
            quitarFicha(actual);
            if (punt > max_punt) {
                mejorcol = actual;
                max_punt = punt;
            }
        }
        this.profundidad = originalProfundidad;
        return mejorcol;
    }
}
