package com.mocelet.fourinrow;

import java.lang.reflect.Array;

public class GameState {
    public static final int DEFAULT_COLS = 7;
    public static final int DEFAULT_ROWS = 6;
    public static final int GOAL_COUNT = 4;
    public static final int VARIATION_DONT_CONNECT_4 = 1;
    public static final int VARIATION_FILL_ALL = 2;
    public static final int VARIATION_NORMAL_RULES = 0;
    private Players[][] board;
    private int cols;
    private int fieldsCount;
    private int filledFieldsCount;
    private Players firstTurn;
    private int[] lastRowColPlayed;
    private int linesPlayer1;
    private int linesPlayer2;
    private String moves;
    private int rows;
    private States state;
    private int[] top;
    private Players turn;
    private int variation;
    private Players winner;

    public enum Players {
        PLAYER_1,
        PLAYER_2,
        NONE
    }

    public enum States {
        RESET,
        PLAYING,
        PAUSED,
        FINISHED
    }

    public GameState() {
        this(6, 7);
    }

    public GameState(int rows2, int cols2) {
        this(rows2, cols2, 0);
    }

    public GameState(int rows2, int cols2, int variation2) {
        this.variation = variation2;
        setSize(rows2, cols2);
        resetBoard();
    }

    public void restoreGame(int rows2, int cols2, String moves2, int firstPlayer) {
        restoreGame(rows2, cols2, moves2, firstPlayer, 0);
    }

    public void restoreGame(int rows2, int cols2, String moves2, int firstPlayer, int variation2) {
        this.variation = variation2;
        setSize(rows2, cols2);
        resetBoard();
        if (firstPlayer == 1) {
            this.firstTurn = Players.PLAYER_1;
        }
        if (firstPlayer == 2) {
            this.firstTurn = Players.PLAYER_2;
        }
        this.turn = this.firstTurn;
        this.state = States.PLAYING;
        try {
            for (String move : moves2.split(",")) {
                put(Integer.parseInt(move));
            }
        } catch (NumberFormatException e) {
            resetBoard();
        }
    }

    public void startGame() {
        startGame(Players.PLAYER_1);
    }

    public void startGame(Players p) {
        this.turn = p;
        this.firstTurn = p;
        this.state = States.PLAYING;
    }

    public boolean isColumnAvailable(int column) {
        return this.top[column] < this.rows;
    }

    public void put(int column) {
        if (isColumnAvailable(column)) {
            this.board[this.top[column]][column] = this.turn;
            this.lastRowColPlayed = new int[]{this.top[column], column};
            if (this.moves.equals("")) {
                this.moves = String.valueOf(this.moves) + column;
            } else {
                this.moves = String.valueOf(this.moves) + "," + column;
            }
            int[] iArr = this.top;
            iArr[column] = iArr[column] + 1;
            this.filledFieldsCount++;
            updateStateAfterPutOrUndo();
        }
    }

    public int getColumnPeak(int col) {
        return this.top[col];
    }

    public int getCols() {
        return this.cols;
    }

    public int getRows() {
        return this.rows;
    }

    public int getFieldsCount() {
        return this.fieldsCount;
    }

    public void setSize(int rows2, int cols2) {
        this.rows = rows2;
        this.cols = cols2;
        this.fieldsCount = rows2 * cols2;
        this.board = (Players[][]) Array.newInstance(Players.class, rows2, cols2);
        this.top = new int[cols2];
    }

    public int getMoveCount() {
        return this.filledFieldsCount;
    }

    public String getMoves() {
        return this.moves;
    }

    public Players getFirstTurn() {
        return this.firstTurn;
    }

    public int getLastColPlayed() {
        if (getMoveCount() == 0) {
            return -1;
        }
        int lastIndex = this.moves.lastIndexOf(",");
        int lastMove = -1;
        if (lastIndex == -1) {
            try {
                lastMove = Integer.parseInt(this.moves);
            } catch (NumberFormatException e) {
            }
        } else {
            lastMove = Integer.parseInt(new StringBuilder().append(this.moves.charAt(lastIndex + 1)).toString());
        }
        return lastMove;
    }

    public void undo(int movesCount) {
        for (int i = 0; i < movesCount && !this.moves.equals(""); i++) {
            int lastIndex = this.moves.lastIndexOf(",");
            int lastMove = lastIndex == -1 ? Integer.parseInt(this.moves) : Integer.parseInt(new StringBuilder().append(this.moves.charAt(lastIndex + 1)).toString());
            this.moves = lastIndex == -1 ? "" : this.moves.substring(0, lastIndex);
            this.filledFieldsCount--;
            int[] iArr = this.top;
            iArr[lastMove] = iArr[lastMove] - 1;
            this.board[this.top[lastMove]][lastMove] = Players.NONE;
            if (this.moves.equals("")) {
                this.lastRowColPlayed = new int[2];
            } else {
                int previousIndex = this.moves.lastIndexOf(",");
                int previousMove = previousIndex == -1 ? Integer.parseInt(this.moves) : Integer.parseInt(new StringBuilder().append(this.moves.charAt(previousIndex + 1)).toString());
                this.lastRowColPlayed = new int[]{this.top[previousMove] - 1, previousMove};
            }
            updateStateAfterPutOrUndo();
        }
    }

    public Players getTurn() {
        return this.turn;
    }

    public Players getWinner() {
        return this.winner;
    }

    public States getState() {
        return this.state;
    }

    public Players getFieldState(int row, int col) {
        return this.board[row][col];
    }

    public int[] getLastRowColPlayed() {
        return this.lastRowColPlayed;
    }

    public boolean isLastRowColPlayer(int row, int col) {
        return this.lastRowColPlayed[0] == row && this.lastRowColPlayed[1] == col;
    }

    public int getPlayer1LinesCount() {
        return this.linesPlayer1;
    }

    public int getPlayer2LinesCount() {
        return this.linesPlayer2;
    }

    public int getVariation() {
        return this.variation;
    }

    private void updateStateAfterPutOrUndo() {
        Players winning = getWinner(this.turn);
        if (winning != Players.NONE) {
            this.winner = winning;
            this.state = States.FINISHED;
        } else if (this.filledFieldsCount == this.fieldsCount) {
            this.winner = Players.NONE;
            this.state = States.FINISHED;
        } else {
            this.winner = Players.NONE;
            this.state = States.PLAYING;
        }
        this.turn = this.turn == Players.PLAYER_1 ? Players.PLAYER_2 : Players.PLAYER_1;
    }

    public boolean belongsToWinningLine(int row, int col) {
        if (this.winner == Players.NONE) {
            return false;
        }
        Players loser = getOpponent(this.winner);
        if (this.variation == 1) {
            if (this.board[row][col] != loser) {
                return false;
            }
            if (adjacentPiecesCount(row, col, -1, 0, loser) + adjacentPiecesCount(row, col, 1, 0, loser) >= 3) {
                return true;
            }
            if (adjacentPiecesCount(row, col, 0, -1, loser) + adjacentPiecesCount(row, col, 0, 1, loser) >= 3) {
                return true;
            }
            if (adjacentPiecesCount(row, col, -1, -1, loser) + adjacentPiecesCount(row, col, 1, 1, loser) >= 3) {
                return true;
            }
            if (adjacentPiecesCount(row, col, 1, -1, loser) + adjacentPiecesCount(row, col, -1, 1, loser) >= 3) {
                return true;
            }
        } else if (this.board[row][col] != this.winner) {
            return false;
        } else {
            if (adjacentPiecesCount(row, col, 1, 0, this.winner) + adjacentPiecesCount(row, col, -1, 0, this.winner) >= 3) {
                return true;
            }
            if (adjacentPiecesCount(row, col, 0, 1, this.winner) + adjacentPiecesCount(row, col, 0, -1, this.winner) >= 3) {
                return true;
            }
            if (adjacentPiecesCount(row, col, 1, 1, this.winner) + adjacentPiecesCount(row, col, -1, -1, this.winner) >= 3) {
                return true;
            }
            if (adjacentPiecesCount(row, col, -1, 1, this.winner) + adjacentPiecesCount(row, col, 1, -1, this.winner) >= 3) {
                return true;
            }
        }
        return false;
    }

    private int adjacentPiecesCount(int row, int col, int incRow, int incCol, Players p) {
        int count = 0;
        int r = row + incRow;
        int c = col + incCol;
        while (c >= 0 && c < this.cols && r >= 0 && r < this.rows && this.board[r][c] == p) {
            count++;
            r += incRow;
            c += incCol;
        }
        return count;
    }

    private Players getOpponent(Players player) {
        if (player == Players.PLAYER_1) {
            return Players.PLAYER_2;
        }
        if (player == Players.PLAYER_2) {
            return Players.PLAYER_1;
        }
        return Players.NONE;
    }

    private Players getWinner(Players player) {
        if (this.variation == 1) {
            for (int col = 0; col < this.cols; col++) {
                for (int fila = 0; fila < this.rows - 3; fila++) {
                    if (checkLine(player, fila, col, 1, 0)) {
                        return getOpponent(player);
                    }
                }
            }
            for (int fila2 = 0; fila2 < this.rows; fila2++) {
                for (int i = 0; i < this.cols - 3; i++) {
                    if (checkLine(player, fila2, i, 0, 1)) {
                        return getOpponent(player);
                    }
                }
            }
            for (int fila3 = 0; fila3 < this.rows - 3; fila3++) {
                for (int col2 = 0; col2 < this.cols - 3; col2++) {
                    if (checkLine(player, fila3, col2, 1, 1)) {
                        return getOpponent(player);
                    }
                }
            }
            for (int fila4 = this.rows - 1; fila4 >= 3; fila4--) {
                for (int col3 = 0; col3 < this.cols - 3; col3++) {
                    if (checkLine(player, fila4, col3, -1, 1)) {
                        return getOpponent(player);
                    }
                }
            }
            return Players.NONE;
        } else if (this.variation == 2) {
            int linesPlayer12 = 0;
            int linesPlayer22 = 0;
            for (int col4 = 0; col4 < this.cols; col4++) {
                for (int fila5 = 0; fila5 < this.rows - 3; fila5++) {
                    if (checkLine(Players.PLAYER_1, fila5, col4, 1, 0)) {
                        linesPlayer12++;
                    }
                }
            }
            for (int fila6 = 0; fila6 < this.rows; fila6++) {
                for (int i2 = 0; i2 < this.cols - 3; i2++) {
                    if (checkLine(Players.PLAYER_1, fila6, i2, 0, 1)) {
                        linesPlayer12++;
                    }
                }
            }
            for (int fila7 = 0; fila7 < this.rows - 3; fila7++) {
                for (int col5 = 0; col5 < this.cols - 3; col5++) {
                    if (checkLine(Players.PLAYER_1, fila7, col5, 1, 1)) {
                        linesPlayer12++;
                    }
                }
            }
            for (int fila8 = this.rows - 1; fila8 >= 3; fila8--) {
                for (int col6 = 0; col6 < this.cols - 3; col6++) {
                    if (checkLine(Players.PLAYER_1, fila8, col6, -1, 1)) {
                        linesPlayer12++;
                    }
                }
            }
            for (int col7 = 0; col7 < this.cols; col7++) {
                for (int fila9 = 0; fila9 < this.rows - 3; fila9++) {
                    if (checkLine(Players.PLAYER_2, fila9, col7, 1, 0)) {
                        linesPlayer22++;
                    }
                }
            }
            for (int fila10 = 0; fila10 < this.rows; fila10++) {
                for (int i3 = 0; i3 < this.cols - 3; i3++) {
                    if (checkLine(Players.PLAYER_2, fila10, i3, 0, 1)) {
                        linesPlayer22++;
                    }
                }
            }
            for (int fila11 = 0; fila11 < this.rows - 3; fila11++) {
                for (int col8 = 0; col8 < this.cols - 3; col8++) {
                    if (checkLine(Players.PLAYER_2, fila11, col8, 1, 1)) {
                        linesPlayer22++;
                    }
                }
            }
            for (int fila12 = this.rows - 1; fila12 >= 3; fila12--) {
                for (int col9 = 0; col9 < this.cols - 3; col9++) {
                    if (checkLine(Players.PLAYER_2, fila12, col9, -1, 1)) {
                        linesPlayer22++;
                    }
                }
            }
            this.linesPlayer1 = linesPlayer12;
            this.linesPlayer2 = linesPlayer22;
            if (this.filledFieldsCount < this.fieldsCount) {
                return Players.NONE;
            }
            if (linesPlayer12 == linesPlayer22) {
                return Players.NONE;
            }
            return linesPlayer12 > linesPlayer22 ? Players.PLAYER_1 : Players.PLAYER_2;
        } else {
            for (int col10 = 0; col10 < this.cols; col10++) {
                for (int fila13 = 0; fila13 < this.rows - 3; fila13++) {
                    if (checkLine(player, fila13, col10, 1, 0)) {
                        return player;
                    }
                }
            }
            for (int fila14 = 0; fila14 < this.rows; fila14++) {
                for (int i4 = 0; i4 < this.cols - 3; i4++) {
                    if (checkLine(player, fila14, i4, 0, 1)) {
                        return player;
                    }
                }
            }
            for (int fila15 = 0; fila15 < this.rows - 3; fila15++) {
                for (int col11 = 0; col11 < this.cols - 3; col11++) {
                    if (checkLine(player, fila15, col11, 1, 1)) {
                        return player;
                    }
                }
            }
            for (int fila16 = this.rows - 1; fila16 >= 3; fila16--) {
                for (int col12 = 0; col12 < this.cols - 3; col12++) {
                    if (checkLine(player, fila16, col12, -1, 1)) {
                        return player;
                    }
                }
            }
            return Players.NONE;
        }
    }

    private boolean checkLine(Players player, int row0, int col0, int incRow, int incCol) {
        int playerCount = 0;
        for (int i = 0; i < 4; i++) {
            if (this.board[(i * incRow) + row0][(i * incCol) + col0] == player) {
                playerCount++;
            }
        }
        return playerCount == 4;
    }

    private void resetBoard() {
        this.state = States.RESET;
        this.filledFieldsCount = 0;
        this.lastRowColPlayed = new int[2];
        this.turn = Players.NONE;
        this.firstTurn = Players.NONE;
        this.moves = "";
        this.winner = Players.NONE;
        for (int c = 0; c < this.cols; c++) {
            this.top[c] = 0;
            for (int r = 0; r < this.rows; r++) {
                this.board[r][c] = Players.NONE;
            }
        }
    }
}
