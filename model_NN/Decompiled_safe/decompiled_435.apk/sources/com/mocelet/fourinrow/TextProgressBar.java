package com.mocelet.fourinrow;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class TextProgressBar extends ProgressBar {
    private String text = "";
    private Paint textPaint = new Paint();

    public TextProgressBar(Context context) {
        super(context);
        this.textPaint.setColor(-16777216);
    }

    public TextProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.textPaint.setColor(-16777216);
    }

    public TextProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.textPaint.setColor(-16777216);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect bounds = new Rect();
        this.textPaint.getTextBounds(this.text, 0, this.text.length(), bounds);
        canvas.drawText(this.text, (float) ((getWidth() / 2) - bounds.centerX()), (float) ((getHeight() / 2) - bounds.centerY()), this.textPaint);
    }

    public synchronized void setText(String text2) {
        this.text = text2;
        drawableStateChanged();
    }

    public void setTextColor(int color) {
        this.textPaint.setColor(color);
        drawableStateChanged();
    }
}
