package com.mocelet.fourinrow;

public class RiddleInfo {
    int cols;
    int firstPlayer;
    String id;
    String initialMoves;
    int movesLimit;
    int rows;
    RiddleStat stat;

    public RiddleInfo(String id2, int rows2, int cols2, int firstPlayer2, int movesLimit2, String initialMoves2) {
        this.id = id2;
        this.rows = rows2;
        this.cols = cols2;
        this.firstPlayer = firstPlayer2;
        this.movesLimit = movesLimit2;
        this.initialMoves = initialMoves2;
    }
}
