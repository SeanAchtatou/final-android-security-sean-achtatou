package com.mocelet.fourinrow;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import com.mocelet.fourinrow.BoardThemeManager;
import com.mocelet.fourinrow.GameState;
import java.util.HashMap;
import java.util.Random;

public class BoardView extends View {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players = null;
    private static final int ANIMATION_TIME_PER_ROW_MILLIS = 150;
    private static final float EXTRA_ROWS_FOR_SELECTOR = 1.1f;
    public static final int SOUND_DROP = 1;
    public static final int SOUND_LOSE = 3;
    public static final int SOUND_WIN = 2;
    private int aboutToDropColumn;
    private boolean animated;
    private int animatingCol;
    private GameState.Players animatingPlayer;
    private int animatingRow;
    private Paint backgroundPaint;
    private Bitmap boardBitmap;
    private Bitmap boardDarkerBitmap;
    private Drawable boardDarkerDrawable;
    private Drawable boardDrawable;
    private float boardHeight;
    private float boardLeftMargin;
    private boolean boardRecycled;
    private float boardWidth;
    private int containerHeight;
    private int containerWidth;
    private float currentPosY;
    private float depthOffsetX;
    private float depthOffsetY;
    private long dropCurrentTimeMillis;
    private long dropStartTimeMillis;
    private Bitmap droppingDisc;
    private GameState gameState;
    private Paint holePaint;
    private int lastMoveCountKnown;
    private BoardListener listener;
    private boolean newHoles = true;
    private float pieceBorderSize;
    private float pieceSize;
    private Bitmap player1Disc;
    private Bitmap player2Disc;
    private boolean roundBorders;
    private volatile boolean running;
    private float separation;
    private boolean showLastColumnMarker;
    private boolean soundEnabled;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;
    private boolean switchColors;
    private Bitmap tempDisc;
    private float tempPosX;
    private float tempPosY;
    private int theme;
    BoardThemeManager themeManager;
    private boolean touchable;
    private States viewState;
    private Paint winningMarkerPaint;

    public enum States {
        INITIALIZING,
        STATIC,
        PIECE_DROPPING
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players;
        if (iArr == null) {
            iArr = new int[GameState.Players.values().length];
            try {
                iArr[GameState.Players.NONE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameState.Players.PLAYER_1.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameState.Players.PLAYER_2.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players = iArr;
        }
        return iArr;
    }

    public BoardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public BoardView(Context context) {
        super(context);
        init(context);
    }

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setGameState(GameState state) {
        GameState oldState = this.gameState;
        this.gameState = state;
        if (oldState == null) {
            return;
        }
        if ((oldState.getCols() != state.getCols() || oldState.getRows() != state.getRows()) && this.containerHeight != 0 && this.containerWidth != 0) {
            computeSizeAttributes(this.containerWidth, this.containerHeight);
        }
    }

    public void setRoundBorders(boolean roundBorders2) {
        this.roundBorders = roundBorders2;
    }

    public void setListener(BoardListener listener2) {
        this.listener = listener2;
    }

    public void setTouchable(boolean touchable2) {
        this.touchable = touchable2;
    }

    public void setAnimated(boolean animated2) {
        this.animated = animated2;
    }

    public void setLastColumnPlayedMarker(boolean showLastColumnMarker2) {
        this.showLastColumnMarker = showLastColumnMarker2;
    }

    public void setSwitchColors(boolean switchColors2) {
        if (this.switchColors != switchColors2) {
            recycleBitmaps();
        }
        this.switchColors = switchColors2;
    }

    public void setTheme(int theme2) {
        if (this.themeManager == null) {
            this.themeManager = new BoardThemeManager(theme2);
        }
        if (this.theme != theme2) {
            this.themeManager.setTheme(theme2);
            recycleBitmaps();
        }
        this.theme = theme2;
    }

    public void setSoundEnabled(boolean sound) {
        this.soundEnabled = sound;
    }

    public void doDropPieceAnimation(int col) {
        if (this.animated) {
            this.lastMoveCountKnown = this.gameState.getMoveCount();
            this.dropStartTimeMillis = 0;
            this.animatingRow = this.gameState.getColumnPeak(col);
            this.animatingCol = col;
            this.animatingPlayer = this.gameState.getTurn();
            this.viewState = States.PIECE_DROPPING;
            while (true) {
                if (this.viewState != States.PIECE_DROPPING) {
                    break;
                } else if (!this.running) {
                    this.viewState = States.STATIC;
                    break;
                } else {
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (this.running) {
                        postInvalidate();
                    }
                }
            }
            if (this.running) {
                playPieceDroppedSound();
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.touchable || this.listener == null) {
            return false;
        }
        int xPos = (int) event.getX();
        int offset = (int) ((((float) getWidth()) - this.boardWidth) / 2.0f);
        int column = (xPos - offset) / ((int) (this.boardWidth / ((float) this.gameState.getCols())));
        if (column < 0) {
            column = 0;
        }
        if (column >= this.gameState.getCols()) {
            column = this.gameState.getCols() - 1;
        }
        switch (event.getAction()) {
            case 0:
            case 2:
                if (column != this.aboutToDropColumn) {
                    this.aboutToDropColumn = column;
                    postInvalidate();
                    break;
                }
                break;
            case 1:
                this.listener.onColumnPlayed(column);
                this.aboutToDropColumn = -1;
                break;
        }
        return true;
    }

    private void init(Context context) {
        this.animated = true;
        this.soundEnabled = true;
        this.roundBorders = true;
        this.aboutToDropColumn = -1;
        this.theme = 0;
        this.boardRecycled = true;
        Resources myRes = getResources();
        this.themeManager = new BoardThemeManager(this.theme);
        this.backgroundPaint = new Paint(1);
        this.backgroundPaint.setColor(myRes.getColor(R.color.board_bg));
        this.backgroundPaint.setStyle(Paint.Style.FILL);
        this.holePaint = new Paint(1);
        this.holePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        if (isInEditMode()) {
            this.gameState = new GameState(6, 7);
            this.gameState.startGame();
            Random r = new Random();
            while (this.gameState.getState() == GameState.States.PLAYING) {
                int playColumn = r.nextInt(this.gameState.getCols());
                if (this.gameState.isColumnAvailable(playColumn)) {
                    this.gameState.put(playColumn);
                }
            }
            return;
        }
        initSounds();
    }

    private void initSounds() {
        this.soundPool = new SoundPool(4, 3, 100);
        this.soundPoolMap = new HashMap<>();
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(getContext(), R.raw.click, 1)));
        this.soundPoolMap.put(2, Integer.valueOf(this.soundPool.load(getContext(), R.raw.win, 1)));
        this.soundPoolMap.put(3, Integer.valueOf(this.soundPool.load(getContext(), R.raw.lose, 1)));
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public void playSound(int sound) {
        if (this.soundEnabled) {
            AudioManager mgr = (AudioManager) getContext().getSystemService("audio");
            float volume = ((float) mgr.getStreamVolume(3)) / ((float) mgr.getStreamMaxVolume(3));
            this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), volume, volume, 1, 0, 1.0f);
        }
    }

    private void computeSizeAttributes(int w, int h) {
        this.containerWidth = w;
        this.containerHeight = h;
        float rowsNeeded = ((float) this.gameState.getRows()) + EXTRA_ROWS_FOR_SELECTOR;
        float colsNeeded = (float) this.gameState.getCols();
        float widthToHeightRatio = colsNeeded / rowsNeeded;
        float optimalHeight = (float) this.containerHeight;
        float optimalWidth = ((float) this.containerHeight) * widthToHeightRatio;
        if (optimalWidth > ((float) this.containerWidth)) {
            optimalHeight = ((float) this.containerWidth) / widthToHeightRatio;
            optimalWidth = (float) this.containerWidth;
        }
        if (optimalHeight > ((float) this.containerHeight)) {
            float optimalHeight2 = (float) this.containerHeight;
        }
        if (optimalWidth > ((float) this.containerWidth)) {
            optimalWidth = (float) this.containerWidth;
        }
        this.pieceSize = (float) (((double) optimalWidth) / (((double) colsNeeded) + (((double) (2.0f + colsNeeded)) / 5.0d)));
        this.separation = this.pieceSize / 5.0f;
        this.pieceBorderSize = this.separation / 5.0f;
        this.boardWidth = (this.pieceSize * ((float) this.gameState.getCols())) + (((float) (this.gameState.getCols() + 1)) * this.separation);
        this.boardHeight = (this.pieceSize * ((float) (this.gameState.getRows() + 1))) + (((float) (this.gameState.getRows() + 2)) * this.separation);
        generateBoardBitmaps();
    }

    private void generateDiscsBitmaps() {
        Bitmap playerDisc;
        Bitmap playerDisc2;
        this.winningMarkerPaint = this.themeManager.getWinningMarkerPaint(getResources(), this.separation);
        if (this.switchColors) {
            playerDisc = this.themeManager.getPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_2, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        } else {
            playerDisc = this.themeManager.getPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_1, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        }
        this.player1Disc = playerDisc;
        if (this.switchColors) {
            playerDisc2 = this.themeManager.getPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_1, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        } else {
            playerDisc2 = this.themeManager.getPlayerDisc(getResources(), BoardThemeManager.Discs.DISC_PLAYER_2, (int) (this.pieceSize + (this.pieceBorderSize * 2.0f)));
        }
        this.player2Disc = playerDisc2;
    }

    private void generateBoardBitmaps() {
        recycleBitmaps();
        generateDiscsBitmaps();
        this.boardDrawable = this.themeManager.getBoardDrawable(getResources(), BoardThemeManager.BoardLayers.FRONT_NORMAL);
        this.boardDarkerDrawable = this.themeManager.getBoardDrawable(getResources(), BoardThemeManager.BoardLayers.BACK_DARKER);
        this.boardBitmap = generateBoard(this.boardDrawable, this.newHoles, true);
        this.boardDarkerBitmap = generateBoard(this.boardDarkerDrawable, false, false);
        this.boardRecycled = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private void drawBoard(Drawable boardDrawable2, Canvas canvas, boolean embossHoles, boolean faceGradient) {
        float bh = (this.boardHeight - this.separation) - this.pieceSize;
        boardDrawable2.setBounds(0, 0, (int) this.boardWidth, (int) bh);
        canvas.save();
        float radiusInPx = TypedValue.applyDimension(1, 15.0f, getResources().getDisplayMetrics());
        if (this.roundBorders && !isInEditMode()) {
            Path clipPath = new Path();
            clipPath.addRoundRect(new RectF(boardDrawable2.getBounds()), radiusInPx, radiusInPx, Path.Direction.CW);
            canvas.clipPath(clipPath);
        }
        boardDrawable2.draw(canvas);
        if (faceGradient) {
            Paint gradient = new Paint();
            gradient.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, bh, Color.parseColor("#33FFFFFF"), Color.parseColor("#00FFFFFF"), Shader.TileMode.CLAMP));
            canvas.drawPaint(gradient);
        }
        canvas.restore();
        Bitmap embossBitmap = null;
        Paint embossPaint = null;
        if (embossHoles) {
            embossBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hole_emboss);
            embossPaint = new Paint();
            embossPaint.setFilterBitmap(true);
        }
        float posX = 0.0f;
        float posY = 0.0f;
        for (int fila = this.gameState.getRows() - 1; fila >= 0; fila--) {
            float posX2 = posX + this.separation;
            float posY2 = posY + this.separation;
            for (int col = 0; col < this.gameState.getCols(); col++) {
                float radius = (float) (((double) this.pieceSize) / 2.0d);
                canvas.drawCircle(posX2 + radius, posY2 + radius, radius, this.holePaint);
                if (embossHoles) {
                    double delta = 0.04477611940298507d * ((double) this.pieceSize);
                    canvas.drawBitmap(embossBitmap, (Rect) null, new Rect((int) (((double) posX2) - delta), (int) (((double) posY2) - delta), (int) (((double) (this.pieceSize + posX2)) + (2.0d * delta)), (int) (((double) (this.pieceSize + posY2)) + (2.0d * delta))), embossPaint);
                }
                posX2 += this.pieceSize + this.separation;
            }
            posX = 0.0f;
            posY = posY2 + this.pieceSize;
        }
        if (embossBitmap != null) {
            embossBitmap.recycle();
        }
    }

    private Bitmap generateBoard(Drawable boardDrawable2, boolean embossHoles, boolean gradient) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap((int) this.boardWidth, (int) this.boardHeight, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawBoard(boardDrawable2, canvas, embossHoles, gradient);
        return bitmap;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        GameState toDrawGameState = this.gameState;
        if (toDrawGameState != null) {
            if (this.boardRecycled) {
                generateBoardBitmaps();
            }
            this.tempPosX = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
            this.tempPosY = this.pieceSize;
            this.depthOffsetX = 0.0f;
            if (this.newHoles) {
                this.depthOffsetY = (float) (-((int) (this.separation / 3.0f)));
            } else {
                this.depthOffsetY = (float) (-((int) (this.separation / 2.0f)));
            }
            if (!this.boardDarkerBitmap.isRecycled()) {
                canvas.drawBitmap(this.boardDarkerBitmap, ((((float) this.containerWidth) - this.boardWidth) / 2.0f) + (this.depthOffsetX * 2.0f), this.pieceSize + (this.depthOffsetY * 2.0f), this.backgroundPaint);
            }
            drawLastColumnPlayedMarker(canvas, this.tempPosX, toDrawGameState);
            drawSelectorDisc(canvas, this.aboutToDropColumn, this.tempPosX, toDrawGameState);
            for (int fila = toDrawGameState.getRows() - 1; fila >= 0; fila--) {
                this.tempPosX += this.separation;
                this.tempPosY += this.separation;
                for (int col = 0; col < toDrawGameState.getCols(); col++) {
                    if (this.viewState == States.PIECE_DROPPING && this.animatingRow == fila && this.animatingCol == col) {
                        drawDroppingPieceFrame(canvas, fila, col, this.tempPosX, this.tempPosY);
                    } else if (this.animatingRow == fila && this.animatingCol == col && this.animatingPlayer != null && toDrawGameState.getMoveCount() == this.lastMoveCountKnown) {
                        switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[this.animatingPlayer.ordinal()]) {
                            case 1:
                                this.tempDisc = this.player1Disc;
                                break;
                            case 2:
                                this.tempDisc = this.player2Disc;
                                break;
                            default:
                                this.tempDisc = null;
                                break;
                        }
                        if (this.tempDisc != null && !this.tempDisc.isRecycled()) {
                            canvas.drawBitmap(this.tempDisc, (float) ((int) (this.tempPosX - this.pieceBorderSize)), (float) ((int) (this.tempPosY - this.pieceBorderSize)), (Paint) null);
                        }
                    } else {
                        if (this.animatingRow == fila && this.animatingCol == col) {
                            this.lastMoveCountKnown = -1;
                        }
                        switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[toDrawGameState.getFieldState(fila, col).ordinal()]) {
                            case 1:
                                this.tempDisc = this.player1Disc;
                                break;
                            case 2:
                                this.tempDisc = this.player2Disc;
                                break;
                            default:
                                this.tempDisc = null;
                                break;
                        }
                        if (this.tempDisc != null && !this.tempDisc.isRecycled()) {
                            canvas.drawBitmap(this.tempDisc, (float) ((int) (this.tempPosX - this.pieceBorderSize)), (float) ((int) (this.tempPosY - this.pieceBorderSize)), (Paint) null);
                        }
                    }
                    this.tempPosX += this.pieceSize + this.separation;
                }
                this.tempPosX = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
                this.tempPosY += this.pieceSize;
            }
            drawWinningCombination(canvas, toDrawGameState);
            this.tempPosX = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
            this.tempPosY = this.pieceSize;
            if (!this.boardBitmap.isRecycled()) {
                canvas.drawBitmap(this.boardBitmap, this.tempPosX, this.tempPosY, this.backgroundPaint);
            }
        }
    }

    private void playPieceDroppedSound() {
        playSound(1);
    }

    private void drawLastColumnPlayedMarker(Canvas canvas, float offset, GameState state) {
        int column;
        if (this.showLastColumnMarker && (column = state.getLastColPlayed()) >= 0) {
            float cornerX = (float) ((int) (((double) (this.separation + offset + (((float) column) * (this.pieceSize + this.separation)))) + ((((double) this.pieceSize) * 1.0d) / 5.0d)));
            float cornerY = (float) ((int) ((this.pieceSize + (2.0f * this.depthOffsetY)) - (this.depthOffsetY / 2.0f)));
            Paint p = new Paint();
            p.setColor(this.themeManager.getLastColumnMarkerColor());
            canvas.drawRect(new Rect((int) cornerX, (int) cornerY, (int) (((double) cornerX) + ((((double) this.pieceSize) * 3.0d) / 5.0d)), (int) (cornerY - this.depthOffsetY)), p);
        }
    }

    private void drawWinningCombination(Canvas canvas, GameState state) {
        float tempPosX2 = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
        float tempPosY2 = this.pieceSize;
        for (int fila = state.getRows() - 1; fila >= 0; fila--) {
            float tempPosX3 = tempPosX2 + this.separation;
            float tempPosY3 = tempPosY2 + this.separation;
            for (int col = 0; col < state.getCols(); col++) {
                if (state.belongsToWinningLine(fila, col)) {
                    canvas.drawOval(new RectF((float) ((int) tempPosX3), (float) ((int) tempPosY3), (float) ((int) (this.pieceSize + tempPosX3)), (float) ((int) (this.pieceSize + tempPosY3))), this.winningMarkerPaint);
                }
                tempPosX3 += this.pieceSize + this.separation;
            }
            tempPosX2 = (((float) this.containerWidth) - this.boardWidth) / 2.0f;
            tempPosY2 = tempPosY3 + this.pieceSize;
        }
    }

    public void fakeSelectorDisc(int column) {
        this.aboutToDropColumn = column;
    }

    private void drawSelectorDisc(Canvas canvas, int column, float offset, GameState state) {
        Bitmap selectorDisc;
        if (column >= 0) {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[state.getTurn().ordinal()]) {
                case 1:
                    selectorDisc = this.player1Disc;
                    break;
                case 2:
                    selectorDisc = this.player2Disc;
                    break;
                default:
                    return;
            }
            float cornerX = (float) ((int) (this.separation + offset + (((float) column) * (this.pieceSize + this.separation))));
            float cornerY = (float) (((int) this.pieceSize) / 2);
            if (!state.isColumnAvailable(column)) {
                cornerY = (float) (((int) this.pieceSize) / 6);
            }
            if (!selectorDisc.isRecycled()) {
                canvas.drawBitmap(selectorDisc, (float) ((int) (cornerX - this.pieceBorderSize)), (float) ((int) (cornerY - this.pieceBorderSize)), (Paint) null);
            }
        }
    }

    private void drawDroppingPieceFrame(Canvas canvas, int fila, int col, float finalPosX, float finalPosY) {
        if (this.dropStartTimeMillis == 0) {
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$GameState$Players()[this.animatingPlayer.ordinal()]) {
                case 1:
                    this.droppingDisc = this.player1Disc;
                    break;
                case 2:
                    this.droppingDisc = this.player2Disc;
                    break;
            }
            this.currentPosY = this.pieceSize / 2.0f;
            this.dropStartTimeMillis = System.currentTimeMillis();
        }
        this.dropCurrentTimeMillis = System.currentTimeMillis();
        this.currentPosY += (((float) (this.dropCurrentTimeMillis - this.dropStartTimeMillis)) * this.pieceSize) / 150.0f;
        if (this.currentPosY > finalPosY || this.currentPosY > this.boardHeight) {
            this.currentPosY = finalPosY;
            this.viewState = States.STATIC;
            this.dropStartTimeMillis = 0;
        }
        if (!this.droppingDisc.isRecycled()) {
            canvas.drawBitmap(this.droppingDisc, (float) ((int) (finalPosX - this.pieceBorderSize)), (float) ((int) (this.currentPosY - this.pieceBorderSize)), (Paint) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        computeSizeAttributes(w, h);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int availableHeight = View.MeasureSpec.getSize(heightMeasureSpec);
        int availableWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        float widthToHeightRatio = ((float) this.gameState.getCols()) / (((float) this.gameState.getRows()) + EXTRA_ROWS_FOR_SELECTOR);
        float optimalHeight = (float) availableHeight;
        float optimalWidth = ((float) availableHeight) * widthToHeightRatio;
        if (optimalWidth > ((float) availableWidth)) {
            optimalHeight = ((float) availableWidth) / widthToHeightRatio;
            optimalWidth = (float) availableWidth;
        }
        if (optimalHeight > ((float) availableHeight)) {
            optimalHeight = (float) availableHeight;
        }
        if (optimalWidth > ((float) availableWidth)) {
            optimalWidth = (float) availableWidth;
        }
        setMeasuredDimension((int) optimalWidth, (int) optimalHeight);
    }

    public void recycleBitmaps() {
        this.boardRecycled = true;
        if (this.player1Disc != null) {
            this.player1Disc.recycle();
        }
        if (this.player2Disc != null) {
            this.player2Disc.recycle();
        }
        if (this.boardBitmap != null) {
            this.boardBitmap.recycle();
        }
        if (this.boardDarkerBitmap != null) {
            this.boardDarkerBitmap.recycle();
        }
        this.boardBitmap = null;
        this.boardDarkerBitmap = null;
    }

    public void onPausing() {
        this.running = false;
    }

    public void onResuming() {
        this.running = true;
    }
}
