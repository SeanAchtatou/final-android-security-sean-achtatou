package com.mocelet.fourinrow;

public class RiddleCategoryInfo {
    String category;
    int categoryId;
    String description;
    boolean isSeparator;
    int riddleCount;
    int riddlesDone;

    public RiddleCategoryInfo(int categoryId2, String category2, String description2, int riddleCount2, int riddlesDone2, boolean isSeparator2) {
        this.categoryId = categoryId2;
        this.category = category2;
        this.description = description2;
        this.riddleCount = riddleCount2;
        this.riddlesDone = riddlesDone2;
        this.isSeparator = isSeparator2;
    }
}
