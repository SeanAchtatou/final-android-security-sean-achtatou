package com.mocelet.fourinrow;

import android.content.SharedPreferences;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class RiddleManager {
    private static final String IN_RIDDLE_SEPARATOR = ",";
    private static final String PREF_NAME = "riddle_manager";
    public static final String PREF_NAME_SOLVED_COUNT = "riddle_manager_count";
    public static final String PREF_NAME_SOLVED_STATS = "riddle_manager_stats";
    private static final String RIDDLE_SEPARATOR = ";";
    private static final String[] SECTIONS = {"A", "B", "C", "D", "E", "F"};
    private Hashtable<String, RiddleStat> riddleStats = new Hashtable<>();
    private int solvedCount;
    private String solvedStats;

    public RiddleStat getRiddleStat(String riddleId) {
        return this.riddleStats.get(riddleId);
    }

    public void put(RiddleStat rs) {
        this.riddleStats.put(rs.id, rs);
    }

    public void loadFromPrefs(SharedPreferences prefs) {
        loadFromString(prefs.getString(PREF_NAME, ""));
    }

    private boolean intToBool(int i) {
        return i != 0;
    }

    private int boolToInt(boolean b) {
        return b ? 1 : 0;
    }

    public int getRiddlesDoneCount(String categoryStartingLetter) {
        int count = 0;
        for (String startsWith : this.riddleStats.keySet()) {
            if (startsWith.startsWith(categoryStartingLetter)) {
                count++;
            }
        }
        return count;
    }

    public void loadFromString(String s) {
        this.riddleStats.clear();
        StringTokenizer st = new StringTokenizer(s, RIDDLE_SEPARATOR);
        while (st.hasMoreTokens()) {
            try {
                StringTokenizer stRiddle = new StringTokenizer(st.nextToken(), IN_RIDDLE_SEPARATOR);
                RiddleStat r = new RiddleStat(stRiddle.nextToken(), intToBool(Integer.parseInt(stRiddle.nextToken())), Long.parseLong(stRiddle.nextToken()));
                this.riddleStats.put(r.id, r);
            } catch (Exception e) {
            }
        }
    }

    public void savePrefs(SharedPreferences prefs) {
        updateCompletedStats();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_NAME, getStringForPrefs());
        editor.putInt(PREF_NAME_SOLVED_COUNT, this.solvedCount);
        editor.putString(PREF_NAME_SOLVED_STATS, this.solvedStats);
        editor.commit();
    }

    private void updateCompletedStats() {
        int[] sectionCount = new int[SECTIONS.length];
        int globalCount = 0;
        Enumeration<RiddleStat> e = this.riddleStats.elements();
        while (e.hasMoreElements()) {
            RiddleStat r = e.nextElement();
            if (r.completed) {
                globalCount++;
                for (int i = 0; i < SECTIONS.length; i++) {
                    if (r.id.startsWith(SECTIONS[i])) {
                        sectionCount[i] = sectionCount[i] + 1;
                    }
                }
            }
        }
        String countString = new StringBuilder().append(globalCount).toString();
        for (int i2 = 0; i2 < sectionCount.length; i2++) {
            countString = String.valueOf(countString) + "." + SECTIONS[i2] + sectionCount[i2];
        }
        this.solvedCount = globalCount;
        this.solvedStats = countString;
    }

    public String getStringForPrefs() {
        StringBuffer sb = new StringBuffer("");
        Enumeration<RiddleStat> e = this.riddleStats.elements();
        while (e.hasMoreElements()) {
            RiddleStat r = e.nextElement();
            sb.append(String.valueOf(r.id) + IN_RIDDLE_SEPARATOR + boolToInt(r.completed) + IN_RIDDLE_SEPARATOR + "0" + RIDDLE_SEPARATOR);
        }
        return sb.toString();
    }

    public void debugClearRiddleStats() {
        this.riddleStats.clear();
    }
}
