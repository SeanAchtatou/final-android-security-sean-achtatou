package com.mocelet.fourinrow;

import android.app.Activity;
import android.os.Build;
import java.lang.reflect.InvocationTargetException;

public class HoneyWrapper {
    public static void invalidateOptionsMenu(Activity a) {
        try {
            a.getClass().getMethod("invalidateOptionsMenu", new Class[0]).invoke(a, new Object[0]);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
        }
    }

    public static boolean isHoneycomb() {
        return Build.VERSION.SDK_INT >= 11;
    }
}
