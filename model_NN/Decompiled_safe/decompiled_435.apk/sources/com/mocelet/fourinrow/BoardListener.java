package com.mocelet.fourinrow;

public interface BoardListener {
    void onColumnPlayed(int i);
}
