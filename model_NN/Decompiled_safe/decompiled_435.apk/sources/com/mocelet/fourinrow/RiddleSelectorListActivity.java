package com.mocelet.fourinrow;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class RiddleSelectorListActivity extends Activity {
    private AdView adView;
    private ArrayAdapter listAdapter;
    private RiddleInfo[] riddles;
    private boolean showingAds;
    private boolean solidBlackBackground;
    private int theme;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        try {
            this.theme = Integer.parseInt(prefs.getString("skin", "10"));
        } catch (NumberFormatException e) {
            this.theme = 20;
        }
        boolean switchColors = prefs.getBoolean("switch_colors", false);
        this.solidBlackBackground = prefs.getBoolean("black_background", false);
        String categoryId = getIntent().getStringExtra("com.mocelet.games.fourinrow.RiddleCategoryIdTag");
        String categoryName = getIntent().getStringExtra("com.mocelet.games.fourinrow.RiddleCategoryNameTag");
        this.riddles = RiddlesDepot.getRiddles(categoryId);
        setTitle(categoryName);
        setContentView((int) R.layout.riddle_grid_layout);
        GridView lv = (GridView) findViewById(R.id.riddle_grid_view);
        this.listAdapter = new RiddleSelectorListAdapter(this, this.riddles, getString(R.string.riddles_riddle_prefix), this.theme, switchColors);
        lv.setAdapter((ListAdapter) this.listAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                RiddleInfo riddle = (RiddleInfo) view.getTag();
                Intent intent = new Intent(RiddleSelectorListActivity.this, InGameVariationActivity.class);
                intent.putExtra("com.mocelet.games.fourinrow.RiddleTypeTag", 3);
                intent.putExtra("com.mocelet.games.fourinrow.RiddleIdTag", riddle.id);
                intent.putExtra("com.mocelet.games.fourinrow.RiddleRowsTag", riddle.rows);
                intent.putExtra("com.mocelet.games.fourinrow.RiddleColsTag", riddle.cols);
                intent.putExtra("com.mocelet.games.fourinrow.RiddleFirstPlayerTag", riddle.firstPlayer);
                intent.putExtra("com.mocelet.games.fourinrow.RiddleInitialGameStateTag", riddle.initialMoves);
                intent.putExtra("com.mocelet.games.fourinrow.RiddleMovesLimitTag", riddle.movesLimit);
                RiddleSelectorListActivity.this.startActivity(intent);
            }
        });
        this.adView = (AdView) findViewById(R.id.adView);
        showAds();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        RiddleManager rm = new RiddleManager();
        rm.loadFromPrefs(prefs);
        for (RiddleInfo riddle : this.riddles) {
            riddle.stat = rm.getRiddleStat(riddle.id);
        }
        this.listAdapter.notifyDataSetChanged();
        setTiledBackground();
    }

    private void setTiledBackground() {
        if (this.solidBlackBackground) {
            getWindow().setBackgroundDrawableResource(R.drawable.black_background);
            return;
        }
        BitmapDrawable tiledBg = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.tile_black_stripes));
        tiledBg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        getWindow().setBackgroundDrawable(tiledBg);
    }

    private void hideAds() {
        if (this.adView != null) {
            this.adView.setVisibility(8);
            this.showingAds = false;
        }
    }

    private void showAds() {
        if (!this.showingAds && this.adView != null) {
            AdRequest request = new AdRequest();
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            this.adView.setVisibility(0);
            this.adView.loadAd(request);
            this.showingAds = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.adView != null) {
            this.adView.destroy();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent intent = new Intent(this, CuatroEnLinea.class);
                intent.addFlags(67108864);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
