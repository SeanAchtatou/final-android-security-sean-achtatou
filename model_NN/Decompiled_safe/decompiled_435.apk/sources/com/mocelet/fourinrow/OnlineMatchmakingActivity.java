package com.mocelet.fourinrow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.mocelet.fourinrow.OnlineActivity;
import com.mocelet.fourinrow.online.Network;
import com.mocelet.fourinrow.online.OnlineService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class OnlineMatchmakingActivity extends OnlineActivity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineMatchmakingActivity$States = null;
    private static final String ME_FIRST = "me_first";
    private static final String REMATCH_SUPPORT = "rematch_support";
    private static final int SOUND_NEW_MATCH = 1;
    private static final int SWITCH_STATE_SEARCHING_MESSAGE = 0;
    private static final int WAITING_TIME_FOR_AUTOINVITE_MS = 20000;
    private static final int WAITING_TIME_FOR_INVITE_MS = 1000;
    private static final int WAITING_TIME_FOR_REPLY_MS = 2000;
    private static final int WAITING_TIME_FOR_RETRY_MS = 1000;
    private static final String YOU_FIRST = "you_first";
    private AdView adView;
    private String autoInvitePlayerId;
    private TextView codeText;
    private boolean elementsSet;
    private Handler handler;
    private boolean hideAlias;
    private TextView infoText;
    private int initialDelay;
    private String initialText;
    private boolean inviteSender;
    private boolean meFirstTurn;
    private TextView newsText;
    private String newsTextFull;
    private String newsTextHeadline;
    private TextView nickText;
    private View progressBox;
    private TextView progressInfoTextView;
    private boolean rematchSupport;
    private boolean showingAds;
    private boolean soundEnabled;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;
    private States state;
    private boolean switchColors;

    private enum States {
        INIT,
        CONNECTED,
        SEARCHING,
        AUTO_INVITING,
        FOUND,
        PLAY
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$mocelet$fourinrow$OnlineMatchmakingActivity$States() {
        int[] iArr = $SWITCH_TABLE$com$mocelet$fourinrow$OnlineMatchmakingActivity$States;
        if (iArr == null) {
            iArr = new int[States.values().length];
            try {
                iArr[States.AUTO_INVITING.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[States.CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[States.FOUND.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[States.INIT.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[States.PLAY.ordinal()] = 6;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[States.SEARCHING.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$com$mocelet$fourinrow$OnlineMatchmakingActivity$States = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStartMode(OnlineActivity.StartMode.CONNECT_ON_START);
        setVolumeControlStream(3);
        Intent intent = getIntent();
        this.initialText = intent.getStringExtra("initial_message");
        if (this.initialText == null) {
            this.initialText = getString(R.string.online_matchmaking_searching);
        }
        this.initialDelay = intent.getIntExtra("initial_delay", 0);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.switchColors = prefs.getBoolean("switch_colors", false);
        this.soundEnabled = prefs.getBoolean("sound_enabled", true);
        this.hideAlias = prefs.getBoolean("online_hide_alias", false);
        switchState(States.INIT);
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 0) {
                    OnlineMatchmakingActivity.this.switchState(States.SEARCHING);
                }
            }
        };
        initSounds();
        setupViewElements();
        updateNewsText();
    }

    private void setupViewElements() {
        Resources resources = getResources();
        setContentView((int) R.layout.matchmaking);
        this.adView = (AdView) findViewById(R.id.adView);
        this.progressInfoTextView = (TextView) findViewById(R.id.progressInfoTextView);
        this.progressBox = findViewById(R.id.progressBox);
        this.infoText = (TextView) findViewById(R.id.conn_info);
        this.nickText = (TextView) findViewById(R.id.nick_info);
        this.codeText = (TextView) findViewById(R.id.code_info);
        this.newsText = (TextView) findViewById(R.id.news_info);
        this.elementsSet = true;
    }

    private void updateNewsText() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.newsTextFull = prefs.getString("online_latest_news_full", "");
        this.newsTextHeadline = prefs.getString("online_latest_news_headline", "");
        if (this.newsTextHeadline == null || this.newsTextHeadline.equals("")) {
            if (this.newsText != null) {
                this.newsText.setText("");
                this.newsText.setSelected(true);
                this.newsText.setVisibility(8);
            }
        } else if (this.newsText != null) {
            this.newsText.setText(this.newsTextHeadline);
            this.newsText.setSelected(true);
            this.newsText.setVisibility(0);
        }
    }

    public void onNews(View v) {
        if (this.newsTextFull == null) {
            Toast.makeText(this, this.newsTextHeadline, 1).show();
        } else {
            Toast.makeText(this, this.newsTextFull, 1).show();
        }
    }

    private void updateElements() {
        if (this.elementsSet) {
            int connCount = getConnectedCount();
            if (connCount < 1) {
                connCount = 1;
            }
            this.infoText.setText(getResources().getQuantityString(R.plurals.online_matchmaking_quantity_info, connCount, Integer.valueOf(connCount)));
            this.nickText.setText(getNickFromId(getPublicId()));
            this.codeText.setText(getCodeFromId(getPublicId()));
            this.progressInfoTextView.setText(String.valueOf(this.initialText) + "...");
            switch ($SWITCH_TABLE$com$mocelet$fourinrow$OnlineMatchmakingActivity$States()[this.state.ordinal()]) {
                case 1:
                    this.infoText.setText((int) R.string.online_you_are_not_logged);
                    this.nickText.setVisibility(4);
                    this.codeText.setVisibility(4);
                    this.progressBox.setVisibility(4);
                    return;
                case 2:
                    this.nickText.setVisibility(0);
                    this.codeText.setVisibility(0);
                    this.progressBox.setVisibility(0);
                    return;
                case 3:
                case 4:
                    this.nickText.setVisibility(0);
                    this.codeText.setVisibility(0);
                    this.progressBox.setVisibility(0);
                    this.progressInfoTextView.setText(String.valueOf(getString(R.string.online_matchmaking_searching)) + "...");
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void switchState(States newState) {
        States previousState = this.state;
        this.state = newState;
        switch ($SWITCH_TABLE$com$mocelet$fourinrow$OnlineMatchmakingActivity$States()[newState.ordinal()]) {
            case 1:
                hideAds();
                stopTimers();
                return;
            case 2:
                showAds();
                startMatchmakingTimer(this.initialDelay);
                updateElements();
                return;
            case 3:
                showAds();
                refreshConnectedCount();
                this.autoInvitePlayerId = "";
                startMatchmakingTimer(WAITING_TIME_FOR_AUTOINVITE_MS);
                onlineSendCommand(Network.MATCHMAKING_START_METHOD);
                updateElements();
                return;
            case 4:
            default:
                return;
            case 5:
                if (previousState == States.AUTO_INVITING) {
                    playSound(1);
                }
                Intent intent = new Intent(this, OnlineMatchPreviewActivity.class);
                intent.putExtra("local_id", getPublicId());
                intent.putExtra("remote_id", this.autoInvitePlayerId);
                intent.putExtra(ME_FIRST, this.meFirstTurn);
                intent.putExtra(REMATCH_SUPPORT, this.rematchSupport);
                startActivity(intent);
                finish();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        updateElements();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        stopTimers();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.adView != null) {
            this.adView.destroy();
        }
    }

    private void hideAds() {
        if (this.adView != null) {
            this.adView.setVisibility(8);
            this.showingAds = false;
        }
    }

    private void showAds() {
        if (!this.showingAds && this.adView != null) {
            AdRequest request = new AdRequest();
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            this.adView.setVisibility(0);
            this.adView.loadAd(request);
            this.showingAds = true;
        }
    }

    private String getCodeFromId(String id) {
        int index = id.lastIndexOf(35);
        if (index > 0) {
            return id.substring(index);
        }
        return "";
    }

    private String getNickFromId(String id) {
        int index = id.lastIndexOf(35);
        if (index > 0) {
            return id.substring(0, index);
        }
        return id;
    }

    private void initSounds() {
        this.soundPool = new SoundPool(4, 3, 100);
        this.soundPoolMap = new HashMap<>();
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(this, R.raw.soft_notification, 1)));
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public void playSound(int sound) {
        if (this.soundEnabled) {
            AudioManager mgr = (AudioManager) getSystemService("audio");
            float volume = ((float) mgr.getStreamVolume(3)) / ((float) mgr.getStreamMaxVolume(3));
            this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), volume, volume, 1, 0, 1.0f);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateElements();
    }

    public void onOnlineConnected() {
        super.onOnlineConnected();
        switchState(States.CONNECTED);
        updateElements();
        onlineSendCommand(Network.INFO_METHOD, new String[]{"NEWS"});
    }

    public void onOnlineDisconnected(OnlineService.DisconnectionReasons reason) {
        super.onOnlineDisconnected(reason);
        switchState(States.INIT);
        updateElements();
    }

    public void onOnlineStatsUpdate() {
        updateElements();
    }

    public void onOnlineRequestReceived(Network.GenericRequest request) {
        super.onOnlineRequestReceived(request);
        if (this.state == States.SEARCHING && request.method.equals(Network.AUTO_INVITE_METHOD) && request.attributes.length > 0) {
            stopTimers();
            this.inviteSender = request.attributes[0].equals("client");
            this.autoInvitePlayerId = request.from;
            if (this.inviteSender) {
                this.meFirstTurn = new Random().nextBoolean();
                onlineSendMessageTo(Network.INVITE_METHOD, new String[]{this.meFirstTurn ? ME_FIRST : YOU_FIRST, REMATCH_SUPPORT}, this.autoInvitePlayerId);
                startMatchmakingTimer(WAITING_TIME_FOR_REPLY_MS);
            } else {
                startMatchmakingTimer(1000);
            }
            switchState(States.AUTO_INVITING);
        } else if (this.state != States.AUTO_INVITING || this.inviteSender || !request.method.equals(Network.INVITE_METHOD) || !request.from.equals(this.autoInvitePlayerId)) {
            onlineSendResponse(request, Network.BUSY_RESPONSE, new String[0]);
        } else {
            stopTimers();
            List attributes = Arrays.asList(request.attributes);
            this.meFirstTurn = attributes.contains(YOU_FIRST);
            this.rematchSupport = attributes.contains(REMATCH_SUPPORT);
            onlineSendResponse(request, Network.OK_RESPONSE, new String[]{REMATCH_SUPPORT});
            switchState(States.FOUND);
        }
    }

    public void onOnlineResponseReceived(Network.GenericResponse response) {
        super.onOnlineResponseReceived(response);
        if (checkNewsUpdate(response)) {
            updateNewsText();
        } else if (this.state == States.AUTO_INVITING && this.inviteSender && response.method.equals(Network.INVITE_METHOD) && response.to.equals(this.autoInvitePlayerId) && response.code == 200) {
            stopTimers();
            this.rematchSupport = false;
            if (response.attributes != null && response.attributes.length > 0) {
                this.rematchSupport = Arrays.asList(response.attributes).contains(REMATCH_SUPPORT);
            }
            switchState(States.FOUND);
        } else if ((this.state == States.AUTO_INVITING && this.inviteSender && response.method.equals(Network.INVITE_METHOD) && response.to.equals(this.autoInvitePlayerId) && response.code == 404) || response.code == 486) {
            stopTimers();
            startMatchmakingTimer(1000);
        }
    }

    private void startMatchmakingTimer(int time) {
        this.handler.sendMessageDelayed(this.handler.obtainMessage(0), (long) time);
    }

    private void stopTimers() {
        if (this.handler != null) {
            this.handler.removeMessages(0);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent intent = new Intent(this, CuatroEnLinea.class);
                intent.addFlags(67108864);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
