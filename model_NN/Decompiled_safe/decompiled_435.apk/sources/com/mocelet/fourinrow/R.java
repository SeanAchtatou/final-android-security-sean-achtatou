package com.mocelet.fourinrow;

public final class R {

    public static final class anim {
        public static final int winner_button = 2130968576;
    }

    public static final class array {
        public static final int boardMaterials = 2131165184;
        public static final int boardMaterialsValues = 2131165185;
        public static final int boardSizes = 2131165186;
        public static final int boardSizesValues = 2131165187;
        public static final int intelligentLevels = 2131165188;
        public static final int intelligentLevelsValues = 2131165189;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int app_bg = 2131230731;
        public static final int board = 2131230726;
        public static final int board_bg = 2131230730;
        public static final int board_darker = 2131230728;
        public static final int board_middle = 2131230727;
        public static final int empty_piece = 2131230725;
        public static final int greenEnd = 2131230741;
        public static final int greenMid = 2131230740;
        public static final int greenStart = 2131230739;
        public static final int match_preview_code = 2131230733;
        public static final int match_preview_name = 2131230732;
        public static final int old_board = 2131230738;
        public static final int old_board_bg = 2131230737;
        public static final int old_board_darker = 2131230736;
        public static final int old_red_piece = 2131230735;
        public static final int old_white_piece = 2131230734;
        public static final int red_piece = 2131230722;
        public static final int red_piece_end = 2131230724;
        public static final int white_piece = 2131230721;
        public static final int white_piece_end = 2131230723;
        public static final int winning_marker = 2131230720;
        public static final int wooden_board = 2131230729;
    }

    public static final class drawable {
        public static final int black_background = 2130837504;
        public static final int black_button = 2130837505;
        public static final int blue_button = 2130837506;
        public static final int board = 2130837507;
        public static final int board_darker = 2130837508;
        public static final int board_middle = 2130837509;
        public static final int dark_blue_background = 2130837510;
        public static final int glass_board = 2130837511;
        public static final int glass_red_disc = 2130837512;
        public static final int glass_white_disc = 2130837513;
        public static final int grayscale_board = 2130837514;
        public static final int grayscale_board_darker = 2130837515;
        public static final int grayscale_board_middle = 2130837516;
        public static final int grayscale_red_disc = 2130837517;
        public static final int grayscale_white_disc = 2130837518;
        public static final int green_button = 2130837519;
        public static final int green_progress = 2130837520;
        public static final int green_progress_translucent_background = 2130837521;
        public static final int hole_emboss = 2130837522;
        public static final int ic_menu_equalizer = 2130837523;
        public static final int ic_menu_info_details = 2130837524;
        public static final int ic_menu_stop = 2130837525;
        public static final int icon = 2130837526;
        public static final int metal_board = 2130837527;
        public static final int metal_red_disc = 2130837528;
        public static final int metal_white_disc = 2130837529;
        public static final int mini_board = 2130837530;
        public static final int mini_glass_board = 2130837531;
        public static final int mini_glass_red_disc = 2130837532;
        public static final int mini_glass_white_disc = 2130837533;
        public static final int mini_metal_board = 2130837534;
        public static final int mini_metal_red_disc = 2130837535;
        public static final int mini_metal_white_disc = 2130837536;
        public static final int mini_wood_board = 2130837537;
        public static final int mini_wood_red_disc = 2130837538;
        public static final int mini_wood_white_disc = 2130837539;
        public static final int purple_button = 2130837540;
        public static final int quitter_background = 2130837541;
        public static final int red_button = 2130837542;
        public static final int red_piece = 2130837543;
        public static final int restart = 2130837544;
        public static final int riddle_solved_background = 2130837545;
        public static final int tile_black_stripes = 2130837546;
        public static final int translucent_black_button = 2130837547;
        public static final int translucent_blue_button = 2130837548;
        public static final int translucent_blue_category_separator = 2130837549;
        public static final int translucent_green_button = 2130837550;
        public static final int translucent_yellow_button = 2130837551;
        public static final int undo = 2130837552;
        public static final int white_piece = 2130837553;
        public static final int winner_button = 2130837554;
        public static final int wood_board = 2130837555;
        public static final int wood_red_disc = 2130837556;
        public static final int wood_white_disc = 2130837557;
        public static final int yellow_button = 2130837558;
    }

    public static final class id {
        public static final int Button1P = 2131558418;
        public static final int Button2P = 2131558419;
        public static final int LinearLayout1 = 2131558411;
        public static final int abandonButton = 2131558451;
        public static final int about = 2131558468;
        public static final int aboutIconView = 2131558402;
        public static final int aboutTextView = 2131558403;
        public static final int adView = 2131558445;
        public static final int ad_container = 2131558453;
        public static final int boardView = 2131558407;
        public static final int bottomBox = 2131558455;
        public static final int code_info = 2131558439;
        public static final int conn_info = 2131558443;
        public static final int containerP1 = 2131558424;
        public static final int containerP2 = 2131558428;
        public static final int contenedor = 2131558423;
        public static final int description = 2131558462;
        public static final int frameLayout1 = 2131558405;
        public static final int frontBoardView = 2131558414;
        public static final int hintsLabel = 2131558413;
        public static final int info_box = 2131558436;
        public static final int info_text_area = 2131558406;
        public static final int keepPlayingButton = 2131558452;
        public static final int label = 2131558461;
        public static final int linearLayout1 = 2131558404;
        public static final int linearLayout2 = 2131558401;
        public static final int linearLayout3 = 2131558400;
        public static final int new_game = 2131558469;
        public static final int news_info = 2131558444;
        public static final int nick_box = 2131558437;
        public static final int nick_info = 2131558438;
        public static final int onlineButton = 2131558422;
        public static final int options = 2131558467;
        public static final int play_again_button = 2131558410;
        public static final int player1_code = 2131558427;
        public static final int player1_name = 2131558426;
        public static final int player1_piece = 2131558425;
        public static final int player2_code = 2131558431;
        public static final int player2_name = 2131558430;
        public static final int player2_piece = 2131558429;
        public static final int progressBar1 = 2131558441;
        public static final int progressBox = 2131558440;
        public static final int progressInfoTextView = 2131558442;
        public static final int progress_bar = 2131558463;
        public static final int progress_text = 2131558460;
        public static final int quitterNoticeText = 2131558450;
        public static final int quittersNoticeContainer = 2131558449;
        public static final int relativeLayout1 = 2131558446;
        public static final int relativeLayout2 = 2131558447;
        public static final int rematch_button = 2131558448;
        public static final int riddleBoard = 2131558466;
        public static final int riddleButtonLayout = 2131558417;
        public static final int riddleInfoLabel = 2131558464;
        public static final int riddleInfoText = 2131558456;
        public static final int riddleLabel = 2131558465;
        public static final int riddle_grid_view = 2131558454;
        public static final int riddle_list_view = 2131558459;
        public static final int riddlesButton = 2131558420;
        public static final int riddlesProgress = 2131558421;
        public static final int scoreBoard = 2131558408;
        public static final int scoreTextView = 2131558409;
        public static final int search_again_button = 2131558435;
        public static final int starting_counter_text = 2131558433;
        public static final int starting_info_text = 2131558434;
        public static final int success_button = 2131558457;
        public static final int textView3 = 2131558416;
        public static final int textView4 = 2131558415;
        public static final int titleLabel = 2131558412;
        public static final int try_again_button = 2131558458;
        public static final int undo = 2131558470;
        public static final int vs_text = 2131558432;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int ingame = 2130903041;
        public static final int main = 2130903042;
        public static final int match_preview = 2130903043;
        public static final int matchmaking = 2130903044;
        public static final int online_game = 2130903045;
        public static final int riddle_grid_layout = 2130903046;
        public static final int riddle_ingame = 2130903047;
        public static final int riddle_list_layout = 2130903048;
        public static final int riddle_list_row = 2130903049;
        public static final int riddle_selector_list_row = 2130903050;
    }

    public static final class menu {
        public static final int frontmenu = 2131492864;
        public static final int menu = 2131492865;
    }

    public static final class plurals {
        public static final int online_matchmaking_quantity_info = 2131361792;
    }

    public static final class raw {
        public static final int click = 2131099648;
        public static final int lose = 2131099649;
        public static final int soft_notification = 2131099650;
        public static final int strong_notification = 2131099651;
        public static final int win = 2131099652;
        public static final int wood_click = 2131099653;
    }

    public static final class string {
        public static final int about = 2131296269;
        public static final int about_text = 2131296273;
        public static final int actionBarHint = 2131296259;
        public static final int actionBarTitle = 2131296258;
        public static final int admob_unit_id = 2131296263;
        public static final int app_name = 2131296256;
        public static final int classic_6x7 = 2131296341;
        public static final int desc = 2131296262;
        public static final int info_column_full = 2131296288;
        public static final int info_has_won = 2131296278;
        public static final int info_opponent_turn = 2131296281;
        public static final int info_opponent_turn_humans = 2131296282;
        public static final int info_someone_wins = 2131296285;
        public static final int info_you_lose = 2131296284;
        public static final int info_you_tie = 2131296287;
        public static final int info_you_win = 2131296283;
        public static final int info_you_win_someone = 2131296286;
        public static final int info_your_turn = 2131296279;
        public static final int info_your_turn_humans = 2131296280;
        public static final int menu_new_game = 2131296339;
        public static final int mocelet_website = 2131296261;
        public static final int not_implemented = 2131296272;
        public static final int online_connected = 2131296346;
        public static final int online_connecting = 2131296343;
        public static final int online_connection_lost = 2131296350;
        public static final int online_hide_alias_name = 2131296318;
        public static final int online_inactivity = 2131296356;
        public static final int online_maintenance = 2131296342;
        public static final int online_matchmaking_rejected_match = 2131296372;
        public static final int online_matchmaking_rejecting_match = 2131296373;
        public static final int online_matchmaking_searching = 2131296360;
        public static final int online_matchmaking_starting = 2131296370;
        public static final int online_matchmaking_starting_match = 2131296371;
        public static final int online_matchmaking_starting_text = 2131296369;
        public static final int online_matchmaking_waiting_reply = 2131296359;
        public static final int online_no_connection = 2131296349;
        public static final int online_no_internet = 2131296352;
        public static final int online_no_registration = 2131296347;
        public static final int online_opponent_lost = 2131296357;
        public static final int online_opponent_turn_info = 2131296361;
        public static final int online_playing_disconnected_desc = 2131296367;
        public static final int online_playing_failed = 2131296363;
        public static final int online_playing_failed_desc = 2131296364;
        public static final int online_playing_pinging = 2131296362;
        public static final int online_playing_quit_desc = 2131296365;
        public static final int online_playing_rematch_accept = 2131296376;
        public static final int online_playing_rematch_button = 2131296375;
        public static final int online_playing_rematch_rejected = 2131296378;
        public static final int online_playing_rematch_rejected_toast = 2131296379;
        public static final int online_playing_rematch_waiting = 2131296377;
        public static final int online_playing_search_button = 2131296374;
        public static final int online_playing_tired_desc = 2131296366;
        public static final int online_quitter_keep = 2131296383;
        public static final int online_quitter_notice_text = 2131296381;
        public static final int online_quitter_notice_title = 2131296380;
        public static final int online_quitter_quit = 2131296382;
        public static final int online_reconnecting = 2131296345;
        public static final int online_registering = 2131296344;
        public static final int online_search_again_button = 2131296368;
        public static final int online_server_error = 2131296348;
        public static final int online_too_many_users = 2131296351;
        public static final int online_update_needed = 2131296354;
        public static final int online_update_needed_link = 2131296355;
        public static final int online_update_needed_title = 2131296353;
        public static final int online_you_are_not_logged = 2131296358;
        public static final int options = 2131296270;
        public static final int options_ai_category = 2131296289;
        public static final int options_animation = 2131296325;
        public static final int options_animation_desc = 2131296326;
        public static final int options_aspect_category = 2131296322;
        public static final int options_black_background = 2131296333;
        public static final int options_black_background_desc = 2131296334;
        public static final int options_difficulty = 2131296290;
        public static final int options_difficulty_list = 2131296298;
        public static final int options_difficulty_list_desc = 2131296299;
        public static final int options_easy = 2131296292;
        public static final int options_extreme = 2131296295;
        public static final int options_exxtreme = 2131296296;
        public static final int options_glass = 2131296305;
        public static final int options_grayscale = 2131296306;
        public static final int options_hard = 2131296294;
        public static final int options_machine_first = 2131296319;
        public static final int options_machine_first_desc = 2131296320;
        public static final int options_medium = 2131296293;
        public static final int options_metal = 2131296303;
        public static final int options_more_difficult = 2131296307;
        public static final int options_more_difficult_desc = 2131296308;
        public static final int options_online_alias = 2131296312;
        public static final int options_online_alias_default = 2131296314;
        public static final int options_online_alias_desc = 2131296313;
        public static final int options_online_enabled = 2131296309;
        public static final int options_online_enabled_off = 2131296311;
        public static final int options_online_enabled_on = 2131296310;
        public static final int options_online_hide_alias = 2131296315;
        public static final int options_online_hide_alias_off = 2131296317;
        public static final int options_online_hide_alias_on = 2131296316;
        public static final int options_other_category = 2131296321;
        public static final int options_perfect = 2131296297;
        public static final int options_plastic = 2131296302;
        public static final int options_show_last_column = 2131296330;
        public static final int options_show_last_column_off = 2131296332;
        public static final int options_show_last_column_on = 2131296331;
        public static final int options_size = 2131296337;
        public static final int options_size_desc = 2131296338;
        public static final int options_skin_list = 2131296300;
        public static final int options_skin_list_desc = 2131296301;
        public static final int options_sound = 2131296323;
        public static final int options_sound_desc = 2131296324;
        public static final int options_static_menu = 2131296335;
        public static final int options_static_menu_desc = 2131296336;
        public static final int options_switch_colors = 2131296327;
        public static final int options_switch_colors_off = 2131296329;
        public static final int options_switch_colors_on = 2131296328;
        public static final int options_very_easy = 2131296291;
        public static final int options_wood = 2131296304;
        public static final int play = 2131296264;
        public static final int play_again = 2131296271;
        public static final int play_against_human = 2131296266;
        public static final int play_against_machine = 2131296265;
        public static final int play_online = 2131296267;
        public static final int player1 = 2131296274;
        public static final int player2 = 2131296275;
        public static final int press_menu_for_options = 2131296340;
        public static final int riddle_variation_fill_line_score = 2131296409;
        public static final int riddles_button = 2131296384;
        public static final int riddles_category_activity_title = 2131296394;
        public static final int riddles_category_game_variations = 2131296395;
        public static final int riddles_category_game_variations_desc = 2131296396;
        public static final int riddles_category_riddles = 2131296397;
        public static final int riddles_category_riddles_desc = 2131296398;
        public static final int riddles_fail_info = 2131296389;
        public static final int riddles_menu_restart = 2131296387;
        public static final int riddles_riddle_claustrophobia = 2131296423;
        public static final int riddles_riddle_claustrophobia_desc = 2131296424;
        public static final int riddles_riddle_hercules = 2131296425;
        public static final int riddles_riddle_hercules_desc = 2131296426;
        public static final int riddles_riddle_info_not_solved = 2131296391;
        public static final int riddles_riddle_info_solved = 2131296392;
        public static final int riddles_riddle_info_solved_in = 2131296393;
        public static final int riddles_riddle_killer_black_swan = 2131296419;
        public static final int riddles_riddle_killer_black_swan_desc = 2131296420;
        public static final int riddles_riddle_killer_dance = 2131296415;
        public static final int riddles_riddle_killer_dance_desc = 2131296416;
        public static final int riddles_riddle_killer_move = 2131296413;
        public static final int riddles_riddle_killer_move_desc = 2131296414;
        public static final int riddles_riddle_killer_tango = 2131296421;
        public static final int riddles_riddle_killer_tango_desc = 2131296422;
        public static final int riddles_riddle_killer_vals = 2131296417;
        public static final int riddles_riddle_killer_vals_desc = 2131296418;
        public static final int riddles_riddle_limit_move_info = 2131296410;
        public static final int riddles_riddle_limit_move_red = 2131296412;
        public static final int riddles_riddle_limit_move_white = 2131296411;
        public static final int riddles_riddle_prefix = 2131296390;
        public static final int riddles_success_button = 2131296386;
        public static final int riddles_success_info = 2131296388;
        public static final int riddles_try_again_button = 2131296385;
        public static final int riddles_variation_dontconnectfour = 2131296399;
        public static final int riddles_variation_dontconnectfour_2p = 2131296405;
        public static final int riddles_variation_dontconnectfour_2p_desc = 2131296406;
        public static final int riddles_variation_dontconnectfour_desc = 2131296400;
        public static final int riddles_variation_dontconnectfour_info = 2131296401;
        public static final int riddles_variation_fill_all = 2131296402;
        public static final int riddles_variation_fill_all_2p = 2131296407;
        public static final int riddles_variation_fill_all_2p_desc = 2131296408;
        public static final int riddles_variation_fill_all_desc = 2131296403;
        public static final int riddles_variation_fill_all_info = 2131296404;
        public static final int title = 2131296257;
        public static final int toast_undo_nothing_to_undo = 2131296277;
        public static final int toast_undo_start_not_available = 2131296276;
        public static final int undo = 2131296268;
        public static final int version = 2131296260;
    }

    public static final class style {
        public static final int ButtonText = 2131427331;
        public static final int ButtonTextSmaller = 2131427332;
        public static final int FourInARowTheme = 2131427328;
        public static final int FourInARowThemeWithTitle = 2131427329;
        public static final int ScoreBoardText = 2131427330;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }
}
