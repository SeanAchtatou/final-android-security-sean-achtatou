package com.mocelet.fourinrow;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.mocelet.fourinrow.GameState;
import com.mocelet.fourinrow.online.Network;
import java.util.Random;

public class CuatroEnLinea extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {
    /* access modifiers changed from: private */
    public static int MOVE_SIMULATION_DELAY = Network.OK_RESPONSE;
    /* access modifiers changed from: private */
    public static int NEW_SIMULATION_DELAY = 5000;
    private static final String TAG = "CuatroEnLinea";
    private boolean animationEnabled;
    /* access modifiers changed from: private */
    public GameState gameState;
    private boolean onlineEnabled = true;
    /* access modifiers changed from: private */
    public boolean running;
    private boolean solidBlackBackground;
    private boolean staticMenu;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.registerOnSharedPreferenceChangeListener(this);
        onSharedPreferenceChanged(prefs, "");
        updateElements();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        updateRiddlesProgressBar();
        setTiledBackground();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.running = false;
        View board = findViewById(R.id.frontBoardView);
        if (board != null) {
            ((BoardView) board).onPausing();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.running = true;
        View board = findViewById(R.id.frontBoardView);
        if (board != null) {
            ((BoardView) board).onResuming();
        }
        runBackgroundGameSimulation();
    }

    public void onRiddles(View v) {
        startActivity(new Intent(this, RiddleCategoryListActivity.class));
    }

    public void onPlay(View v) {
        Intent intent = new Intent(this, InGameActivity.class);
        intent.putExtra("com.mocelet.games.fourinrow.PlayTag", (String) v.getTag());
        startActivity(intent);
    }

    public void onGoOnline(View v) {
        try {
            if (((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo().isConnectedOrConnecting()) {
                startActivity(new Intent(this, OnlineMatchmakingActivity.class));
            } else {
                Toast.makeText(this, getString(R.string.online_no_internet), 0).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.online_no_internet), 0).show();
        }
    }

    public void onOptions(View v) {
        startActivity(new Intent(this, GamePreferenceActivity.class));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.frontmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Intent homeIntent = new Intent(this, CuatroEnLinea.class);
                homeIntent.addFlags(67108864);
                startActivity(homeIntent);
                return true;
            case R.id.options /*2131558467*/:
                startActivity(new Intent(this, GamePreferenceActivity.class));
                return true;
            case R.id.about /*2131558468*/:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle((int) R.string.about);
                alertDialog.setView(((LayoutInflater) getApplicationContext().getSystemService("layout_inflater")).inflate((int) R.layout.about, (ViewGroup) null));
                alertDialog.setCanceledOnTouchOutside(true);
                alertDialog.setIcon((Drawable) null);
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateElements() {
        setContentView((int) R.layout.main);
        updateRiddlesProgressBar();
        BoardView board = (BoardView) findViewById(R.id.frontBoardView);
        board.setVisibility(4);
        board.setTouchable(false);
        board.setSoundEnabled(false);
        Random r = new Random();
        this.gameState = new GameState(6, 7);
        if (r.nextBoolean() && getResources().getConfiguration().orientation == 2) {
            this.gameState = new GameState(7, 9);
        }
        this.gameState.startGame();
        board.setGameState(this.gameState);
        if (!this.animationEnabled || this.staticMenu) {
            int playColumn = 0;
            while (this.gameState.getState() == GameState.States.PLAYING) {
                playColumn = r.nextInt(this.gameState.getCols());
                if (this.gameState.isColumnAvailable(playColumn)) {
                    this.gameState.put(playColumn);
                }
            }
            this.gameState.undo(1);
            board.fakeSelectorDisc(playColumn);
        }
        int randomTheme = r.nextInt(4);
        if (randomTheme == 0) {
            board.setTheme(20);
        } else if (randomTheme == 1) {
            board.setTheme(10);
        } else if (randomTheme == 2) {
            board.setTheme(30);
        } else {
            board.setTheme(10);
        }
        board.setVisibility(0);
        updateHoneycombElements();
    }

    private void updateHoneycombElements() {
        if (HoneyWrapper.isHoneycomb()) {
            ((TextView) findViewById(R.id.titleLabel)).setVisibility(4);
            setTitle(getString(R.string.actionBarTitle));
            ((TextView) findViewById(R.id.hintsLabel)).setVisibility(4);
            HoneyWrapper.invalidateOptionsMenu(this);
        }
    }

    private void updateRiddlesProgressBar() {
        int solvedCount = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt(RiddleManager.PREF_NAME_SOLVED_COUNT, 0);
        int globalCount = RiddlesDepot.getRiddleGlobalCount();
        if (solvedCount > globalCount) {
            solvedCount = globalCount;
        }
        View v = findViewById(R.id.riddlesProgress);
        if (v != null) {
            ProgressBar bar = (ProgressBar) v;
            if (solvedCount > 0) {
                bar.setMax(globalCount);
                bar.setProgress(solvedCount);
                bar.setVisibility(0);
                return;
            }
            bar.setVisibility(8);
        }
    }

    private void setTiledBackground() {
        if (this.solidBlackBackground) {
            getWindow().setBackgroundDrawableResource(R.drawable.black_background);
            return;
        }
        BitmapDrawable tiledBg = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.tile_black_stripes));
        tiledBg.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        getWindow().setBackgroundDrawable(tiledBg);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        this.solidBlackBackground = sharedPreferences.getBoolean("black_background", false);
        this.animationEnabled = sharedPreferences.getBoolean("animation_enabled", true);
        this.staticMenu = sharedPreferences.getBoolean("static_menu", false);
        updateElements();
    }

    private void runBackgroundGameSimulation() {
        if (this.animationEnabled && !this.staticMenu) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    GameAI ai;
                    BoardView boardView = null;
                    while (CuatroEnLinea.this.running && boardView == null) {
                        View board = CuatroEnLinea.this.findViewById(R.id.frontBoardView);
                        if (board == null) {
                            CuatroEnLinea.this.sleep(15);
                        } else {
                            boardView = (BoardView) board;
                        }
                    }
                    Random r = new Random();
                    if (boardView != null) {
                        boardView.setLastColumnPlayedMarker(true);
                    }
                    boolean simulationLoop = true;
                    while (CuatroEnLinea.this.running && simulationLoop) {
                        CuatroEnLinea.this.gameState.restoreGame(CuatroEnLinea.this.gameState.getRows(), CuatroEnLinea.this.gameState.getCols(), "", 1);
                        CuatroEnLinea.this.gameState.startGame();
                        while (CuatroEnLinea.this.running && CuatroEnLinea.this.gameState.getState() == GameState.States.PLAYING) {
                            if (r.nextBoolean()) {
                                ai = new GameAI(2, CuatroEnLinea.this.gameState);
                            } else {
                                ai = new GameAI(3, CuatroEnLinea.this.gameState);
                            }
                            long startThinking = System.currentTimeMillis();
                            int playColumn = ai.getBestMove();
                            long delay = System.currentTimeMillis() - startThinking;
                            if (delay < ((long) CuatroEnLinea.MOVE_SIMULATION_DELAY)) {
                                CuatroEnLinea.this.sleep(((long) CuatroEnLinea.MOVE_SIMULATION_DELAY) - delay);
                            }
                            if (boardView != null) {
                                boardView.doDropPieceAnimation(playColumn);
                            }
                            CuatroEnLinea.this.gameState.put(playColumn);
                        }
                        if (CuatroEnLinea.this.running) {
                            boardView.postInvalidate();
                        }
                        int ellapsedTime = 0;
                        while (CuatroEnLinea.this.running && ellapsedTime < CuatroEnLinea.NEW_SIMULATION_DELAY) {
                            CuatroEnLinea.this.sleep(15);
                            ellapsedTime += 15;
                        }
                        if (!CuatroEnLinea.this.running || ellapsedTime < CuatroEnLinea.NEW_SIMULATION_DELAY) {
                            simulationLoop = false;
                        } else {
                            simulationLoop = true;
                        }
                    }
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
        }
    }
}
