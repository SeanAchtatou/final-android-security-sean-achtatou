package org.objectweb.asm;

public class Label {
    int a;
    int b;
    int c;
    private int d;
    private int[] e;
    int f;
    int g;
    Frame h;
    Label i;
    public Object info;
    Edge j;
    Label k;

    private void a(int i2, int i3) {
        if (this.e == null) {
            this.e = new int[6];
        }
        if (this.d >= this.e.length) {
            int[] iArr = new int[(this.e.length + 6)];
            System.arraycopy(this.e, 0, iArr, 0, this.e.length);
            this.e = iArr;
        }
        int[] iArr2 = this.e;
        int i4 = this.d;
        this.d = i4 + 1;
        iArr2[i4] = i2;
        int[] iArr3 = this.e;
        int i5 = this.d;
        this.d = i5 + 1;
        iArr3[i5] = i3;
    }

    /* access modifiers changed from: package-private */
    public Label a() {
        return this.h == null ? this : this.h.b;
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, int i2) {
        if ((this.a & Opcodes.ACC_ABSTRACT) == 0) {
            this.a |= Opcodes.ACC_ABSTRACT;
            this.e = new int[(((i2 - 1) / 32) + 1)];
        }
        int[] iArr = this.e;
        int i3 = (int) (j2 >>> 32);
        iArr[i3] = iArr[i3] | ((int) j2);
    }

    /* access modifiers changed from: package-private */
    public void a(MethodWriter methodWriter, ByteVector byteVector, int i2, boolean z) {
        if ((this.a & 2) == 0) {
            if (z) {
                a(-1 - i2, byteVector.b);
                byteVector.putInt(-1);
                return;
            }
            a(i2, byteVector.b);
            byteVector.putShort(-1);
        } else if (z) {
            byteVector.putInt(this.c - i2);
        } else {
            byteVector.putShort(this.c - i2);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(long j2) {
        if ((this.a & Opcodes.ACC_ABSTRACT) != 0) {
            return (this.e[(int) (j2 >>> 32)] & ((int) j2)) != 0;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Label label) {
        if ((this.a & Opcodes.ACC_ABSTRACT) == 0 || (label.a & Opcodes.ACC_ABSTRACT) == 0) {
            return false;
        }
        for (int i2 = 0; i2 < this.e.length; i2++) {
            if ((this.e[i2] & label.e[i2]) != 0) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(MethodWriter methodWriter, int i2, byte[] bArr) {
        boolean z = false;
        this.a |= 2;
        this.c = i2;
        int i3 = 0;
        while (i3 < this.d) {
            int i4 = i3 + 1;
            int i5 = this.e[i3];
            int i6 = i4 + 1;
            int i7 = this.e[i4];
            if (i5 >= 0) {
                int i8 = i2 - i5;
                if (i8 < -32768 || i8 > 32767) {
                    byte b2 = bArr[i7 - 1] & 255;
                    if (b2 <= 168) {
                        bArr[i7 - 1] = (byte) (b2 + 49);
                    } else {
                        bArr[i7 - 1] = (byte) (b2 + 20);
                    }
                    z = true;
                }
                bArr[i7] = (byte) (i8 >>> 8);
                bArr[i7 + 1] = (byte) i8;
            } else {
                int i9 = i5 + i2 + 1;
                int i10 = i7 + 1;
                bArr[i7] = (byte) (i9 >>> 24);
                int i11 = i10 + 1;
                bArr[i10] = (byte) (i9 >>> 16);
                bArr[i11] = (byte) (i9 >>> 8);
                bArr[i11 + 1] = (byte) i9;
            }
            z = z;
            i3 = i6;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void b(Label label, long j2, int i2) {
        Label label2 = this;
        while (label2 != null) {
            Label label3 = label2.k;
            label2.k = null;
            if (label != null) {
                if ((label2.a & Opcodes.ACC_STRICT) != 0) {
                    label2 = label3;
                } else {
                    label2.a |= Opcodes.ACC_STRICT;
                    if ((label2.a & Opcodes.ACC_NATIVE) != 0 && !label2.a(label)) {
                        Edge edge = new Edge();
                        edge.a = label2.f;
                        edge.b = label.j.b;
                        edge.c = label2.j;
                        label2.j = edge;
                    }
                }
            } else if (label2.a(j2)) {
                label2 = label3;
            } else {
                label2.a(j2, i2);
            }
            Label label4 = label3;
            for (Edge edge2 = label2.j; edge2 != null; edge2 = edge2.c) {
                if (((label2.a & 128) == 0 || edge2 != label2.j.c) && edge2.b.k == null) {
                    edge2.b.k = label4;
                    label4 = edge2.b;
                }
            }
            label2 = label4;
        }
    }

    public int getOffset() {
        if ((this.a & 2) != 0) {
            return this.c;
        }
        throw new IllegalStateException("Label offset position has not been resolved yet");
    }

    public String toString() {
        return new StringBuffer().append("L").append(System.identityHashCode(this)).toString();
    }
}
