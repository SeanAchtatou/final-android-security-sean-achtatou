package org.objectweb.asm;

public class ByteVector {
    byte[] a;
    int b;

    public ByteVector() {
        this.a = new byte[64];
    }

    public ByteVector(int i) {
        this.a = new byte[i];
    }

    private void a(int i) {
        int length = this.a.length * 2;
        int i2 = this.b + i;
        if (length <= i2) {
            length = i2;
        }
        byte[] bArr = new byte[length];
        System.arraycopy(this.a, 0, bArr, 0, this.b);
        this.a = bArr;
    }

    /* access modifiers changed from: package-private */
    public ByteVector a(int i, int i2) {
        int i3 = this.b;
        if (i3 + 2 > this.a.length) {
            a(2);
        }
        byte[] bArr = this.a;
        int i4 = i3 + 1;
        bArr[i3] = (byte) i;
        bArr[i4] = (byte) i2;
        this.b = i4 + 1;
        return this;
    }

    /* access modifiers changed from: package-private */
    public ByteVector b(int i, int i2) {
        int i3 = this.b;
        if (i3 + 3 > this.a.length) {
            a(3);
        }
        byte[] bArr = this.a;
        int i4 = i3 + 1;
        bArr[i3] = (byte) i;
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i2 >>> 8);
        bArr[i5] = (byte) i2;
        this.b = i5 + 1;
        return this;
    }

    public ByteVector putByte(int i) {
        int i2 = this.b;
        if (i2 + 1 > this.a.length) {
            a(1);
        }
        this.a[i2] = (byte) i;
        this.b = i2 + 1;
        return this;
    }

    public ByteVector putByteArray(byte[] bArr, int i, int i2) {
        if (this.b + i2 > this.a.length) {
            a(i2);
        }
        if (bArr != null) {
            System.arraycopy(bArr, i, this.a, this.b, i2);
        }
        this.b += i2;
        return this;
    }

    public ByteVector putInt(int i) {
        int i2 = this.b;
        if (i2 + 4 > this.a.length) {
            a(4);
        }
        byte[] bArr = this.a;
        int i3 = i2 + 1;
        bArr[i2] = (byte) (i >>> 24);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i >>> 16);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i >>> 8);
        bArr[i5] = (byte) i;
        this.b = i5 + 1;
        return this;
    }

    public ByteVector putLong(long j) {
        int i = this.b;
        if (i + 8 > this.a.length) {
            a(8);
        }
        byte[] bArr = this.a;
        int i2 = (int) (j >>> 32);
        int i3 = i + 1;
        bArr[i] = (byte) (i2 >>> 24);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i2 >>> 16);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i2 >>> 8);
        int i6 = i5 + 1;
        bArr[i5] = (byte) i2;
        int i7 = (int) j;
        int i8 = i6 + 1;
        bArr[i6] = (byte) (i7 >>> 24);
        int i9 = i8 + 1;
        bArr[i8] = (byte) (i7 >>> 16);
        int i10 = i9 + 1;
        bArr[i9] = (byte) (i7 >>> 8);
        bArr[i10] = (byte) i7;
        this.b = i10 + 1;
        return this;
    }

    public ByteVector putShort(int i) {
        int i2 = this.b;
        if (i2 + 2 > this.a.length) {
            a(2);
        }
        byte[] bArr = this.a;
        int i3 = i2 + 1;
        bArr[i2] = (byte) (i >>> 8);
        bArr[i3] = (byte) i;
        this.b = i3 + 1;
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003f, code lost:
        if (r4 >= r0) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        r6 = r12.charAt(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0045, code lost:
        if (r6 < 1) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0047, code lost:
        if (r6 > 127) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0049, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004b, code lost:
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004e, code lost:
        if (r6 <= 2047) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0050, code lost:
        r5 = r5 + 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0053, code lost:
        r5 = r5 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0056, code lost:
        r2[r11.b] = (byte) (r5 >>> 8);
        r2[r11.b + 1] = (byte) r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006a, code lost:
        if (((r11.b + 2) + r5) <= r2.length) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006c, code lost:
        r11.b = r3;
        a(r5 + 2);
        r2 = r11.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0075, code lost:
        if (r1 >= r0) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0077, code lost:
        r4 = r12.charAt(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x007b, code lost:
        if (r4 < 1) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007d, code lost:
        if (r4 > 127) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007f, code lost:
        r2[r3] = (byte) r4;
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0085, code lost:
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0088, code lost:
        if (r4 <= 2047) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008a, code lost:
        r5 = r3 + 1;
        r2[r3] = (byte) (((r4 >> 12) & 15) | 224);
        r3 = r5 + 1;
        r2[r5] = (byte) (((r4 >> 6) & 63) | 128);
        r2[r3] = (byte) ((r4 & '?') | 128);
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00ab, code lost:
        r5 = r3 + 1;
        r2[r3] = (byte) (((r4 >> 6) & 31) | org.objectweb.asm.Opcodes.CHECKCAST);
        r3 = r5 + 1;
        r2[r5] = (byte) ((r4 & '?') | 128);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c0, code lost:
        r0 = r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.objectweb.asm.ByteVector putUTF8(java.lang.String r12) {
        /*
            r11 = this;
            r9 = 2047(0x7ff, float:2.868E-42)
            r8 = 127(0x7f, float:1.78E-43)
            r7 = 1
            int r0 = r12.length()
            int r1 = r11.b
            int r2 = r1 + 2
            int r2 = r2 + r0
            byte[] r3 = r11.a
            int r3 = r3.length
            if (r2 <= r3) goto L_0x0018
            int r2 = r0 + 2
            r11.a(r2)
        L_0x0018:
            byte[] r2 = r11.a
            int r3 = r1 + 1
            int r4 = r0 >>> 8
            byte r4 = (byte) r4
            r2[r1] = r4
            int r1 = r3 + 1
            byte r4 = (byte) r0
            r2[r3] = r4
            r3 = 0
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x002a:
            if (r1 >= r0) goto L_0x00c4
            char r4 = r12.charAt(r1)
            if (r4 < r7) goto L_0x003d
            if (r4 > r8) goto L_0x003d
            int r5 = r3 + 1
            byte r4 = (byte) r4
            r2[r3] = r4
            int r1 = r1 + 1
            r3 = r5
            goto L_0x002a
        L_0x003d:
            r4 = r1
            r5 = r1
        L_0x003f:
            if (r4 >= r0) goto L_0x0056
            char r6 = r12.charAt(r4)
            if (r6 < r7) goto L_0x004e
            if (r6 > r8) goto L_0x004e
            int r5 = r5 + 1
        L_0x004b:
            int r4 = r4 + 1
            goto L_0x003f
        L_0x004e:
            if (r6 <= r9) goto L_0x0053
            int r5 = r5 + 3
            goto L_0x004b
        L_0x0053:
            int r5 = r5 + 2
            goto L_0x004b
        L_0x0056:
            int r4 = r11.b
            int r6 = r5 >>> 8
            byte r6 = (byte) r6
            r2[r4] = r6
            int r4 = r11.b
            int r4 = r4 + 1
            byte r6 = (byte) r5
            r2[r4] = r6
            int r4 = r11.b
            int r4 = r4 + 2
            int r4 = r4 + r5
            int r6 = r2.length
            if (r4 <= r6) goto L_0x0075
            r11.b = r3
            int r2 = r5 + 2
            r11.a(r2)
            byte[] r2 = r11.a
        L_0x0075:
            if (r1 >= r0) goto L_0x00c0
            char r4 = r12.charAt(r1)
            if (r4 < r7) goto L_0x0088
            if (r4 > r8) goto L_0x0088
            int r5 = r3 + 1
            byte r4 = (byte) r4
            r2[r3] = r4
            r3 = r5
        L_0x0085:
            int r1 = r1 + 1
            goto L_0x0075
        L_0x0088:
            if (r4 <= r9) goto L_0x00ab
            int r5 = r3 + 1
            int r6 = r4 >> 12
            r6 = r6 & 15
            r6 = r6 | 224(0xe0, float:3.14E-43)
            byte r6 = (byte) r6
            r2[r3] = r6
            int r3 = r5 + 1
            int r6 = r4 >> 6
            r6 = r6 & 63
            r6 = r6 | 128(0x80, float:1.794E-43)
            byte r6 = (byte) r6
            r2[r5] = r6
            int r5 = r3 + 1
            r4 = r4 & 63
            r4 = r4 | 128(0x80, float:1.794E-43)
            byte r4 = (byte) r4
            r2[r3] = r4
            r3 = r5
            goto L_0x0085
        L_0x00ab:
            int r5 = r3 + 1
            int r6 = r4 >> 6
            r6 = r6 & 31
            r6 = r6 | 192(0xc0, float:2.69E-43)
            byte r6 = (byte) r6
            r2[r3] = r6
            int r3 = r5 + 1
            r4 = r4 & 63
            r4 = r4 | 128(0x80, float:1.794E-43)
            byte r4 = (byte) r4
            r2[r5] = r4
            goto L_0x0085
        L_0x00c0:
            r0 = r3
        L_0x00c1:
            r11.b = r0
            return r11
        L_0x00c4:
            r0 = r3
            goto L_0x00c1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.objectweb.asm.ByteVector.putUTF8(java.lang.String):org.objectweb.asm.ByteVector");
    }
}
