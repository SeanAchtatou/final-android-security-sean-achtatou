package org.objectweb.asm;

import java.io.IOException;
import java.io.InputStream;

public class ClassReader {
    public static final int EXPAND_FRAMES = 8;
    public static final int SKIP_CODE = 1;
    public static final int SKIP_DEBUG = 2;
    public static final int SKIP_FRAMES = 4;
    private final int[] a;
    public final byte[] b;
    private final String[] c;
    private final int d;
    public final int header;

    public ClassReader(InputStream inputStream) throws IOException {
        this(a(inputStream));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public ClassReader(String str) throws IOException {
        this(ClassLoader.getSystemResourceAsStream(new StringBuffer().append(str.replace('.', '/')).append(".class").toString()));
    }

    public ClassReader(byte[] bArr) {
        this(bArr, 0, bArr.length);
    }

    public ClassReader(byte[] bArr, int i, int i2) {
        int readUnsignedShort;
        int i3;
        int i4;
        this.b = bArr;
        this.a = new int[readUnsignedShort(i + 8)];
        int length = this.a.length;
        this.c = new String[length];
        int i5 = i + 10;
        int i6 = 0;
        int i7 = 1;
        while (i7 < length) {
            this.a[i7] = i5 + 1;
            switch (bArr[i5]) {
                case 1:
                    readUnsignedShort = readUnsignedShort(i5 + 1) + 3;
                    if (readUnsignedShort <= i6) {
                        int i8 = readUnsignedShort;
                        readUnsignedShort = i6;
                        i3 = i7;
                        i4 = i8;
                        break;
                    } else {
                        i3 = i7;
                        i4 = readUnsignedShort;
                        break;
                    }
                case 2:
                case 7:
                case 8:
                default:
                    readUnsignedShort = i6;
                    i3 = i7;
                    i4 = 3;
                    break;
                case 3:
                case 4:
                case 9:
                case 10:
                case 11:
                case 12:
                    readUnsignedShort = i6;
                    i3 = i7;
                    i4 = 5;
                    break;
                case 5:
                case 6:
                    readUnsignedShort = i6;
                    i3 = i7 + 1;
                    i4 = 9;
                    break;
            }
            int i9 = i4 + i5;
            int i10 = i3 + 1;
            i6 = readUnsignedShort;
            int i11 = i9;
            i7 = i10;
            i5 = i11;
        }
        this.d = i6;
        this.header = i5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, ?[OBJECT, ARRAY]]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    private int a(int i, char[] cArr, String str, AnnotationVisitor annotationVisitor) {
        if (annotationVisitor == null) {
            switch (this.b[i] & 255) {
                case 64:
                    return a(i + 3, cArr, true, (AnnotationVisitor) null);
                case Opcodes.DUP_X2 /*91*/:
                    return a(i + 1, cArr, false, (AnnotationVisitor) null);
                case Opcodes.LSUB /*101*/:
                    return i + 5;
                default:
                    return i + 3;
            }
        } else {
            int i2 = i + 1;
            switch (this.b[i] & 255) {
                case 64:
                    return a(i2 + 2, cArr, true, annotationVisitor.visitAnnotation(str, readUTF8(i2, cArr)));
                case 66:
                    annotationVisitor.visit(str, new Byte((byte) readInt(this.a[readUnsignedShort(i2)])));
                    return i2 + 2;
                case 67:
                    annotationVisitor.visit(str, new Character((char) readInt(this.a[readUnsignedShort(i2)])));
                    return i2 + 2;
                case 68:
                case 70:
                case 73:
                case 74:
                    annotationVisitor.visit(str, readConst(readUnsignedShort(i2), cArr));
                    return i2 + 2;
                case Opcodes.AASTORE /*83*/:
                    annotationVisitor.visit(str, new Short((short) readInt(this.a[readUnsignedShort(i2)])));
                    return i2 + 2;
                case Opcodes.DUP_X1 /*90*/:
                    annotationVisitor.visit(str, readInt(this.a[readUnsignedShort(i2)]) == 0 ? Boolean.FALSE : Boolean.TRUE);
                    return i2 + 2;
                case Opcodes.DUP_X2 /*91*/:
                    int readUnsignedShort = readUnsignedShort(i2);
                    int i3 = i2 + 2;
                    if (readUnsignedShort == 0) {
                        return a(i3 - 2, cArr, false, annotationVisitor.visitArray(str));
                    }
                    int i4 = i3 + 1;
                    switch (this.b[i3] & 255) {
                        case 66:
                            byte[] bArr = new byte[readUnsignedShort];
                            for (int i5 = 0; i5 < readUnsignedShort; i5++) {
                                bArr[i5] = (byte) readInt(this.a[readUnsignedShort(i4)]);
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, bArr);
                            return i4 - 1;
                        case 67:
                            char[] cArr2 = new char[readUnsignedShort];
                            for (int i6 = 0; i6 < readUnsignedShort; i6++) {
                                cArr2[i6] = (char) readInt(this.a[readUnsignedShort(i4)]);
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, cArr2);
                            return i4 - 1;
                        case 68:
                            double[] dArr = new double[readUnsignedShort];
                            for (int i7 = 0; i7 < readUnsignedShort; i7++) {
                                dArr[i7] = Double.longBitsToDouble(readLong(this.a[readUnsignedShort(i4)]));
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, dArr);
                            return i4 - 1;
                        case 70:
                            float[] fArr = new float[readUnsignedShort];
                            for (int i8 = 0; i8 < readUnsignedShort; i8++) {
                                fArr[i8] = Float.intBitsToFloat(readInt(this.a[readUnsignedShort(i4)]));
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, fArr);
                            return i4 - 1;
                        case 73:
                            int[] iArr = new int[readUnsignedShort];
                            for (int i9 = 0; i9 < readUnsignedShort; i9++) {
                                iArr[i9] = readInt(this.a[readUnsignedShort(i4)]);
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, iArr);
                            return i4 - 1;
                        case 74:
                            long[] jArr = new long[readUnsignedShort];
                            for (int i10 = 0; i10 < readUnsignedShort; i10++) {
                                jArr[i10] = readLong(this.a[readUnsignedShort(i4)]);
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, jArr);
                            return i4 - 1;
                        case Opcodes.AASTORE /*83*/:
                            short[] sArr = new short[readUnsignedShort];
                            for (int i11 = 0; i11 < readUnsignedShort; i11++) {
                                sArr[i11] = (short) readInt(this.a[readUnsignedShort(i4)]);
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, sArr);
                            return i4 - 1;
                        case Opcodes.DUP_X1 /*90*/:
                            boolean[] zArr = new boolean[readUnsignedShort];
                            for (int i12 = 0; i12 < readUnsignedShort; i12++) {
                                zArr[i12] = readInt(this.a[readUnsignedShort(i4)]) != 0;
                                i4 += 3;
                            }
                            annotationVisitor.visit(str, zArr);
                            return i4 - 1;
                        default:
                            return a(i4 - 3, cArr, false, annotationVisitor.visitArray(str));
                    }
                case Opcodes.DADD /*99*/:
                    annotationVisitor.visit(str, Type.getType(readUTF8(i2, cArr)));
                    return i2 + 2;
                case Opcodes.LSUB /*101*/:
                    annotationVisitor.visitEnum(str, readUTF8(i2, cArr), readUTF8(i2 + 2, cArr));
                    return i2 + 4;
                case Opcodes.DREM /*115*/:
                    annotationVisitor.visit(str, readUTF8(i2, cArr));
                    return i2 + 2;
                default:
                    return i2;
            }
        }
    }

    private int a(int i, char[] cArr, boolean z, AnnotationVisitor annotationVisitor) {
        int readUnsignedShort = readUnsignedShort(i);
        int i2 = i + 2;
        if (z) {
            while (readUnsignedShort > 0) {
                i2 = a(i2 + 2, cArr, readUTF8(i2, cArr), annotationVisitor);
                readUnsignedShort--;
            }
        } else {
            while (readUnsignedShort > 0) {
                i2 = a(i2, cArr, (String) null, annotationVisitor);
                readUnsignedShort--;
            }
        }
        int i3 = i2;
        if (annotationVisitor != null) {
            annotationVisitor.visitEnd();
        }
        return i3;
    }

    private int a(Object[] objArr, int i, int i2, char[] cArr, Label[] labelArr) {
        int i3 = i2 + 1;
        switch (this.b[i2] & 255) {
            case 0:
                objArr[i] = Opcodes.TOP;
                return i3;
            case 1:
                objArr[i] = Opcodes.INTEGER;
                return i3;
            case 2:
                objArr[i] = Opcodes.FLOAT;
                return i3;
            case 3:
                objArr[i] = Opcodes.DOUBLE;
                return i3;
            case 4:
                objArr[i] = Opcodes.LONG;
                return i3;
            case 5:
                objArr[i] = Opcodes.NULL;
                return i3;
            case 6:
                objArr[i] = Opcodes.UNINITIALIZED_THIS;
                return i3;
            case 7:
                objArr[i] = readClass(i3, cArr);
                return i3 + 2;
            default:
                objArr[i] = readLabel(readUnsignedShort(i3), labelArr);
                return i3 + 2;
        }
    }

    private String a(int i, int i2, char[] cArr) {
        int i3 = i + i2;
        byte[] bArr = this.b;
        char c2 = 0;
        char c3 = 0;
        int i4 = 0;
        int i5 = i;
        while (i5 < i3) {
            int i6 = i5 + 1;
            byte b2 = bArr[i5];
            switch (c3) {
                case 0:
                    byte b3 = b2 & 255;
                    if (b3 >= 128) {
                        if (b3 < 224 && b3 > 191) {
                            c2 = (char) (b3 & 31);
                            c3 = 1;
                            break;
                        } else {
                            c2 = (char) (b3 & 15);
                            c3 = 2;
                            break;
                        }
                    } else {
                        cArr[i4] = (char) b3;
                        i4++;
                        break;
                    }
                case 1:
                    cArr[i4] = (char) ((b2 & 63) | (c2 << 6));
                    i4++;
                    c3 = 0;
                    break;
                case 2:
                    c2 = (char) ((c2 << 6) | (b2 & 63));
                    c3 = 1;
                    break;
            }
            i5 = i6;
        }
        return new String(cArr, 0, i4);
    }

    private Attribute a(Attribute[] attributeArr, String str, int i, int i2, char[] cArr, int i3, Label[] labelArr) {
        for (int i4 = 0; i4 < attributeArr.length; i4++) {
            if (attributeArr[i4].type.equals(str)) {
                return attributeArr[i4].read(this, i, i2, cArr, i3, labelArr);
            }
        }
        return new Attribute(str).read(this, i, i2, null, -1, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    private void a(int i, String str, char[] cArr, boolean z, MethodVisitor methodVisitor) {
        int i2 = i + 1;
        byte b2 = this.b[i] & 255;
        int length = Type.getArgumentTypes(str).length - b2;
        int i3 = 0;
        while (i3 < length) {
            AnnotationVisitor visitParameterAnnotation = methodVisitor.visitParameterAnnotation(i3, "Ljava/lang/Synthetic;", false);
            if (visitParameterAnnotation != null) {
                visitParameterAnnotation.visitEnd();
            }
            i3++;
        }
        int i4 = i3;
        int i5 = i2;
        int i6 = i4;
        while (i6 < b2 + length) {
            int i7 = i5 + 2;
            for (int readUnsignedShort = readUnsignedShort(i5); readUnsignedShort > 0; readUnsignedShort--) {
                i7 = a(i7 + 2, cArr, true, methodVisitor.visitParameterAnnotation(i6, readUTF8(i7, cArr), z));
            }
            i6++;
            i5 = i7;
        }
    }

    private static byte[] a(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            throw new IOException("Class not found");
        }
        byte[] bArr = new byte[inputStream.available()];
        int i = 0;
        while (true) {
            int read = inputStream.read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
                if (i == bArr.length) {
                    int read2 = inputStream.read();
                    if (read2 < 0) {
                        return bArr;
                    }
                    byte[] bArr2 = new byte[(bArr.length + 1000)];
                    System.arraycopy(bArr, 0, bArr2, 0, i);
                    bArr2[i] = (byte) read2;
                    i++;
                    bArr = bArr2;
                }
            } else if (i >= bArr.length) {
                return bArr;
            } else {
                byte[] bArr3 = new byte[i];
                System.arraycopy(bArr, 0, bArr3, 0, i);
                return bArr3;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ClassWriter classWriter) {
        char[] cArr = new char[this.d];
        int length = this.a.length;
        Item[] itemArr = new Item[length];
        int i = 1;
        while (i < length) {
            int i2 = this.a[i];
            byte b2 = this.b[i2 - 1];
            Item item = new Item(i);
            switch (b2) {
                case 1:
                    String str = this.c[i];
                    if (str == null) {
                        int i3 = this.a[i];
                        String[] strArr = this.c;
                        str = a(i3 + 2, readUnsignedShort(i3), cArr);
                        strArr[i] = str;
                    }
                    item.a(b2, str, null, null);
                    break;
                case 2:
                case 7:
                case 8:
                default:
                    item.a(b2, readUTF8(i2, cArr), null, null);
                    break;
                case 3:
                    item.a(readInt(i2));
                    break;
                case 4:
                    item.a(Float.intBitsToFloat(readInt(i2)));
                    break;
                case 5:
                    item.a(readLong(i2));
                    i++;
                    break;
                case 6:
                    item.a(Double.longBitsToDouble(readLong(i2)));
                    i++;
                    break;
                case 9:
                case 10:
                case 11:
                    int i4 = this.a[readUnsignedShort(i2 + 2)];
                    item.a(b2, readClass(i2, cArr), readUTF8(i4, cArr), readUTF8(i4 + 2, cArr));
                    break;
                case 12:
                    item.a(b2, readUTF8(i2, cArr), readUTF8(i2 + 2, cArr), null);
                    break;
            }
            int length2 = item.j % itemArr.length;
            item.k = itemArr[length2];
            itemArr[length2] = item;
            i++;
        }
        int i5 = this.a[1] - 1;
        classWriter.d.putByteArray(this.b, i5, this.header - i5);
        classWriter.e = itemArr;
        classWriter.f = (int) (0.75d * ((double) length));
        classWriter.c = length;
    }

    public void accept(ClassVisitor classVisitor, int i) {
        accept(classVisitor, new Attribute[0], i);
    }

    /* JADX WARN: Type inference failed for: r23v7, types: [int] */
    /* JADX WARN: Type inference failed for: r7v60, types: [int] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, java.lang.String, char[], boolean, org.objectweb.asm.MethodVisitor):void
     arg types: [int, java.lang.String, char[], int, org.objectweb.asm.MethodVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(java.lang.Object[], int, int, char[], org.objectweb.asm.Label[]):int
      org.objectweb.asm.ClassReader.a(int, java.lang.String, char[], boolean, org.objectweb.asm.MethodVisitor):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void accept(org.objectweb.asm.ClassVisitor r49, org.objectweb.asm.Attribute[] r50, int r51) {
        /*
            r48 = this;
            r0 = r48
            byte[] r0 = r0.b
            r28 = r0
            r0 = r48
            int r0 = r0.d
            r5 = r0
            char[] r10 = new char[r5]
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r48
            int r0 = r0.header
            r8 = r0
            r0 = r48
            r1 = r8
            int r9 = r0.readUnsignedShort(r1)
            int r11 = r8 + 2
            r0 = r48
            r1 = r11
            r2 = r10
            java.lang.String r14 = r0.readClass(r1, r2)
            r0 = r48
            int[] r0 = r0.a
            r11 = r0
            int r12 = r8 + 4
            r0 = r48
            r1 = r12
            int r12 = r0.readUnsignedShort(r1)
            r11 = r11[r12]
            if (r11 != 0) goto L_0x0068
            r11 = 0
            r16 = r11
        L_0x003b:
            int r11 = r8 + 6
            r0 = r48
            r1 = r11
            int r11 = r0.readUnsignedShort(r1)
            r0 = r11
            java.lang.String[] r0 = new java.lang.String[r0]
            r17 = r0
            r11 = 0
            int r8 = r8 + 8
            r12 = 0
            r18 = r8
            r8 = r12
        L_0x0050:
            r0 = r17
            int r0 = r0.length
            r12 = r0
            if (r8 >= r12) goto L_0x0073
            r0 = r48
            r1 = r18
            r2 = r10
            java.lang.String r12 = r0.readClass(r1, r2)
            r17[r8] = r12
            int r12 = r18 + 2
            int r8 = r8 + 1
            r18 = r12
            goto L_0x0050
        L_0x0068:
            r0 = r48
            r1 = r11
            r2 = r10
            java.lang.String r11 = r0.readUTF8(r1, r2)
            r16 = r11
            goto L_0x003b
        L_0x0073:
            r8 = r51 & 1
            if (r8 == 0) goto L_0x00b5
            r8 = 1
            r29 = r8
        L_0x007a:
            r8 = r51 & 2
            if (r8 == 0) goto L_0x00b9
            r8 = 1
            r30 = r8
        L_0x0081:
            r8 = r51 & 8
            if (r8 == 0) goto L_0x00bd
            r8 = 1
            r31 = r8
        L_0x0088:
            r0 = r48
            r1 = r18
            int r8 = r0.readUnsignedShort(r1)
            int r12 = r18 + 2
        L_0x0092:
            if (r8 <= 0) goto L_0x00c5
            int r13 = r12 + 6
            r0 = r48
            r1 = r13
            int r13 = r0.readUnsignedShort(r1)
            int r12 = r12 + 8
            r46 = r13
            r13 = r12
            r12 = r46
        L_0x00a4:
            if (r12 <= 0) goto L_0x00c1
            int r15 = r13 + 2
            r0 = r48
            r1 = r15
            int r15 = r0.readInt(r1)
            int r15 = r15 + 6
            int r13 = r13 + r15
            int r12 = r12 + -1
            goto L_0x00a4
        L_0x00b5:
            r8 = 0
            r29 = r8
            goto L_0x007a
        L_0x00b9:
            r8 = 0
            r30 = r8
            goto L_0x0081
        L_0x00bd:
            r8 = 0
            r31 = r8
            goto L_0x0088
        L_0x00c1:
            int r8 = r8 + -1
            r12 = r13
            goto L_0x0092
        L_0x00c5:
            r0 = r48
            r1 = r12
            int r8 = r0.readUnsignedShort(r1)
            int r12 = r12 + 2
        L_0x00ce:
            if (r8 <= 0) goto L_0x00f5
            int r13 = r12 + 6
            r0 = r48
            r1 = r13
            int r13 = r0.readUnsignedShort(r1)
            int r12 = r12 + 8
            r46 = r13
            r13 = r12
            r12 = r46
        L_0x00e0:
            if (r12 <= 0) goto L_0x00f1
            int r15 = r13 + 2
            r0 = r48
            r1 = r15
            int r15 = r0.readInt(r1)
            int r15 = r15 + 6
            int r13 = r13 + r15
            int r12 = r12 + -1
            goto L_0x00e0
        L_0x00f1:
            int r8 = r8 + -1
            r12 = r13
            goto L_0x00ce
        L_0x00f5:
            r8 = 0
            r13 = 0
            r15 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r0 = r48
            r1 = r12
            int r22 = r0.readUnsignedShort(r1)
            int r12 = r12 + 2
            r23 = r13
            r24 = r22
            r25 = r11
            r26 = r12
            r27 = r7
            r32 = r6
            r33 = r5
            r13 = r9
            r22 = r15
            r15 = r8
            r46 = r19
            r19 = r21
            r21 = r46
        L_0x011f:
            if (r24 <= 0) goto L_0x02f1
            r0 = r48
            r1 = r26
            r2 = r10
            java.lang.String r7 = r0.readUTF8(r1, r2)
            java.lang.String r5 = "SourceFile"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0176
            int r5 = r26 + 6
            r0 = r48
            r1 = r5
            r2 = r10
            java.lang.String r5 = r0.readUTF8(r1, r2)
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r5
            r11 = r15
            r12 = r25
            r5 = r19
            r15 = r27
            r20 = r33
            r19 = r32
        L_0x014e:
            int r21 = r26 + 2
            r0 = r48
            r1 = r21
            int r21 = r0.readInt(r1)
            int r21 = r21 + 6
            int r21 = r21 + r26
            int r22 = r24 + -1
            r23 = r9
            r24 = r22
            r25 = r12
            r26 = r21
            r27 = r15
            r32 = r19
            r33 = r20
            r21 = r7
            r22 = r8
            r19 = r5
            r15 = r11
            r20 = r6
            goto L_0x011f
        L_0x0176:
            java.lang.String r5 = "InnerClasses"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0193
            int r5 = r26 + 6
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r5
            r5 = r19
            r15 = r27
            r20 = r33
            r19 = r32
            goto L_0x014e
        L_0x0193:
            java.lang.String r5 = "EnclosingMethod"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x01e5
            int r5 = r26 + 6
            r0 = r48
            r1 = r5
            r2 = r10
            java.lang.String r5 = r0.readClass(r1, r2)
            int r6 = r26 + 8
            r0 = r48
            r1 = r6
            int r6 = r0.readUnsignedShort(r1)
            if (r6 == 0) goto L_0x124b
            r0 = r48
            int[] r0 = r0.a
            r7 = r0
            r7 = r7[r6]
            r0 = r48
            r1 = r7
            r2 = r10
            java.lang.String r7 = r0.readUTF8(r1, r2)
            r0 = r48
            int[] r0 = r0.a
            r8 = r0
            r6 = r8[r6]
            int r6 = r6 + 2
            r0 = r48
            r1 = r6
            r2 = r10
            java.lang.String r6 = r0.readUTF8(r1, r2)
        L_0x01d0:
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r25
            r19 = r32
            r20 = r33
            r15 = r27
            r46 = r6
            r6 = r7
            r7 = r5
            r5 = r46
            goto L_0x014e
        L_0x01e5:
            java.lang.String r5 = "Signature"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x020c
            int r5 = r26 + 6
            r0 = r48
            r1 = r5
            r2 = r10
            java.lang.String r5 = r0.readUTF8(r1, r2)
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r5
            r12 = r25
            r15 = r27
            r5 = r19
            r20 = r33
            r19 = r32
            goto L_0x014e
        L_0x020c:
            java.lang.String r5 = "RuntimeVisibleAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x022b
            int r5 = r26 + 6
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r25
            r15 = r27
            r20 = r5
            r5 = r19
            r19 = r32
            goto L_0x014e
        L_0x022b:
            java.lang.String r5 = "Deprecated"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x024c
            r5 = 131072(0x20000, float:1.83671E-40)
            r5 = r5 | r13
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r25
            r13 = r5
            r5 = r19
            r15 = r27
            r20 = r33
            r19 = r32
            goto L_0x014e
        L_0x024c:
            java.lang.String r5 = "Synthetic"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x026e
            r5 = 266240(0x41000, float:3.73082E-40)
            r5 = r5 | r13
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r25
            r13 = r5
            r5 = r19
            r15 = r27
            r20 = r33
            r19 = r32
            goto L_0x014e
        L_0x026e:
            java.lang.String r5 = "SourceDebugExtension"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x02a0
            int r5 = r26 + 2
            r0 = r48
            r1 = r5
            int r5 = r0.readInt(r1)
            int r6 = r26 + 6
            char[] r7 = new char[r5]
            r0 = r48
            r1 = r6
            r2 = r5
            r3 = r7
            java.lang.String r5 = r0.a(r1, r2, r3)
            r6 = r20
            r7 = r21
            r8 = r5
            r9 = r23
            r11 = r15
            r12 = r25
            r5 = r19
            r15 = r27
            r20 = r33
            r19 = r32
            goto L_0x014e
        L_0x02a0:
            java.lang.String r5 = "RuntimeInvisibleAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x02c1
            int r5 = r26 + 6
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r25
            r15 = r27
            r20 = r33
            r46 = r5
            r5 = r19
            r19 = r46
            goto L_0x014e
        L_0x02c1:
            int r8 = r26 + 6
            int r5 = r26 + 2
            r0 = r48
            r1 = r5
            int r9 = r0.readInt(r1)
            r11 = -1
            r12 = 0
            r5 = r48
            r6 = r50
            org.objectweb.asm.Attribute r5 = r5.a(r6, r7, r8, r9, r10, r11, r12)
            if (r5 == 0) goto L_0x1236
            r0 = r27
            r1 = r5
            r1.a = r0
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r25
            r15 = r5
            r20 = r33
            r5 = r19
            r19 = r32
            goto L_0x014e
        L_0x02f1:
            r5 = 4
            r0 = r48
            r1 = r5
            int r12 = r0.readInt(r1)
            r11 = r49
            r11.visit(r12, r13, r14, r15, r16, r17)
            if (r30 != 0) goto L_0x030d
            if (r23 != 0) goto L_0x0304
            if (r22 == 0) goto L_0x030d
        L_0x0304:
            r0 = r49
            r1 = r23
            r2 = r22
            r0.visitSource(r1, r2)
        L_0x030d:
            if (r21 == 0) goto L_0x031a
            r0 = r49
            r1 = r21
            r2 = r20
            r3 = r19
            r0.visitOuterClass(r1, r2, r3)
        L_0x031a:
            r5 = 1
        L_0x031b:
            if (r5 < 0) goto L_0x1232
            if (r5 != 0) goto L_0x0356
            r6 = r32
        L_0x0321:
            if (r6 == 0) goto L_0x035b
            r0 = r48
            r1 = r6
            int r7 = r0.readUnsignedShort(r1)
            int r6 = r6 + 2
            r46 = r7
            r7 = r6
            r6 = r46
        L_0x0331:
            if (r6 <= 0) goto L_0x035b
            int r8 = r7 + 2
            r9 = 1
            r0 = r48
            r1 = r7
            r2 = r10
            java.lang.String r7 = r0.readUTF8(r1, r2)
            if (r5 == 0) goto L_0x0359
            r11 = 1
        L_0x0341:
            r0 = r49
            r1 = r7
            r2 = r11
            org.objectweb.asm.AnnotationVisitor r7 = r0.visitAnnotation(r1, r2)
            r0 = r48
            r1 = r8
            r2 = r10
            r3 = r9
            r4 = r7
            int r7 = r0.a(r1, r2, r3, r4)
            int r6 = r6 + -1
            goto L_0x0331
        L_0x0356:
            r6 = r33
            goto L_0x0321
        L_0x0359:
            r11 = 0
            goto L_0x0341
        L_0x035b:
            int r5 = r5 + -1
            goto L_0x031b
        L_0x035e:
            if (r5 == 0) goto L_0x036d
            org.objectweb.asm.Attribute r6 = r5.a
            r7 = 0
            r5.a = r7
            r0 = r49
            r1 = r5
            r0.visitAttribute(r1)
            r5 = r6
            goto L_0x035e
        L_0x036d:
            if (r25 == 0) goto L_0x03d3
            r0 = r48
            r1 = r25
            int r5 = r0.readUnsignedShort(r1)
            int r6 = r25 + 2
        L_0x0379:
            if (r5 <= 0) goto L_0x03d3
            r0 = r48
            r1 = r6
            int r7 = r0.readUnsignedShort(r1)
            if (r7 != 0) goto L_0x03b4
            r7 = 0
        L_0x0385:
            int r8 = r6 + 2
            r0 = r48
            r1 = r8
            int r8 = r0.readUnsignedShort(r1)
            if (r8 != 0) goto L_0x03bd
            r8 = 0
        L_0x0391:
            int r9 = r6 + 4
            r0 = r48
            r1 = r9
            int r9 = r0.readUnsignedShort(r1)
            if (r9 != 0) goto L_0x03c8
            r9 = 0
        L_0x039d:
            int r11 = r6 + 6
            r0 = r48
            r1 = r11
            int r11 = r0.readUnsignedShort(r1)
            r0 = r49
            r1 = r7
            r2 = r8
            r3 = r9
            r4 = r11
            r0.visitInnerClass(r1, r2, r3, r4)
            int r6 = r6 + 8
            int r5 = r5 + -1
            goto L_0x0379
        L_0x03b4:
            r0 = r48
            r1 = r6
            r2 = r10
            java.lang.String r7 = r0.readClass(r1, r2)
            goto L_0x0385
        L_0x03bd:
            int r8 = r6 + 2
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.String r8 = r0.readClass(r1, r2)
            goto L_0x0391
        L_0x03c8:
            int r9 = r6 + 4
            r0 = r48
            r1 = r9
            r2 = r10
            java.lang.String r9 = r0.readUTF8(r1, r2)
            goto L_0x039d
        L_0x03d3:
            r0 = r48
            r1 = r18
            int r5 = r0.readUnsignedShort(r1)
            int r6 = r18 + 2
            r17 = r5
            r5 = r6
        L_0x03e0:
            if (r17 <= 0) goto L_0x0572
            r0 = r48
            r1 = r5
            int r6 = r0.readUnsignedShort(r1)
            int r7 = r5 + 2
            r0 = r48
            r1 = r7
            r2 = r10
            java.lang.String r13 = r0.readUTF8(r1, r2)
            int r7 = r5 + 4
            r0 = r48
            r1 = r7
            r2 = r10
            java.lang.String r14 = r0.readUTF8(r1, r2)
            r7 = 0
            r8 = 0
            r9 = 0
            r11 = 0
            r12 = 0
            int r15 = r5 + 6
            r0 = r48
            r1 = r15
            int r15 = r0.readUnsignedShort(r1)
            int r5 = r5 + 8
            r16 = r7
            r18 = r15
            r19 = r6
            r20 = r5
            r21 = r12
            r22 = r11
            r23 = r9
            r15 = r8
        L_0x041c:
            if (r18 <= 0) goto L_0x0500
            r0 = r48
            r1 = r20
            r2 = r10
            java.lang.String r7 = r0.readUTF8(r1, r2)
            java.lang.String r5 = "ConstantValue"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0460
            int r5 = r20 + 6
            r0 = r48
            r1 = r5
            int r5 = r0.readUnsignedShort(r1)
            r6 = r15
            r7 = r19
            r8 = r21
            r9 = r22
            r11 = r23
        L_0x0441:
            int r12 = r20 + 2
            r0 = r48
            r1 = r12
            int r12 = r0.readInt(r1)
            int r12 = r12 + 6
            int r12 = r12 + r20
            int r15 = r18 + -1
            r16 = r5
            r18 = r15
            r19 = r7
            r20 = r12
            r21 = r8
            r22 = r9
            r23 = r11
            r15 = r6
            goto L_0x041c
        L_0x0460:
            java.lang.String r5 = "Signature"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x047e
            int r5 = r20 + 6
            r0 = r48
            r1 = r5
            r2 = r10
            java.lang.String r5 = r0.readUTF8(r1, r2)
            r6 = r5
            r7 = r19
            r8 = r21
            r9 = r22
            r11 = r23
            r5 = r16
            goto L_0x0441
        L_0x047e:
            java.lang.String r5 = "Deprecated"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0495
            r5 = 131072(0x20000, float:1.83671E-40)
            r5 = r5 | r19
            r6 = r15
            r7 = r5
            r8 = r21
            r9 = r22
            r11 = r23
            r5 = r16
            goto L_0x0441
        L_0x0495:
            java.lang.String r5 = "Synthetic"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x04ad
            r5 = 266240(0x41000, float:3.73082E-40)
            r5 = r5 | r19
            r6 = r15
            r7 = r5
            r8 = r21
            r9 = r22
            r11 = r23
            r5 = r16
            goto L_0x0441
        L_0x04ad:
            java.lang.String r5 = "RuntimeVisibleAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x04c2
            int r5 = r20 + 6
            r6 = r15
            r7 = r19
            r8 = r21
            r9 = r22
            r11 = r5
            r5 = r16
            goto L_0x0441
        L_0x04c2:
            java.lang.String r5 = "RuntimeInvisibleAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x04d8
            int r5 = r20 + 6
            r6 = r15
            r7 = r19
            r8 = r21
            r9 = r5
            r11 = r23
            r5 = r16
            goto L_0x0441
        L_0x04d8:
            int r8 = r20 + 6
            int r5 = r20 + 2
            r0 = r48
            r1 = r5
            int r9 = r0.readInt(r1)
            r11 = -1
            r12 = 0
            r5 = r48
            r6 = r50
            org.objectweb.asm.Attribute r5 = r5.a(r6, r7, r8, r9, r10, r11, r12)
            if (r5 == 0) goto L_0x1225
            r0 = r21
            r1 = r5
            r1.a = r0
            r6 = r15
            r7 = r19
            r8 = r5
            r9 = r22
            r11 = r23
            r5 = r16
            goto L_0x0441
        L_0x0500:
            if (r16 != 0) goto L_0x0547
            r5 = 0
            r16 = r5
        L_0x0505:
            r11 = r49
            r12 = r19
            org.objectweb.asm.FieldVisitor r5 = r11.visitField(r12, r13, r14, r15, r16)
            if (r5 == 0) goto L_0x056a
            r6 = 1
        L_0x0510:
            if (r6 < 0) goto L_0x1221
            if (r6 != 0) goto L_0x0553
            r7 = r22
        L_0x0516:
            if (r7 == 0) goto L_0x0558
            r0 = r48
            r1 = r7
            int r8 = r0.readUnsignedShort(r1)
            int r7 = r7 + 2
            r46 = r8
            r8 = r7
            r7 = r46
        L_0x0526:
            if (r7 <= 0) goto L_0x0558
            int r9 = r8 + 2
            r11 = 1
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.String r8 = r0.readUTF8(r1, r2)
            if (r6 == 0) goto L_0x0556
            r12 = 1
        L_0x0536:
            org.objectweb.asm.AnnotationVisitor r8 = r5.visitAnnotation(r8, r12)
            r0 = r48
            r1 = r9
            r2 = r10
            r3 = r11
            r4 = r8
            int r8 = r0.a(r1, r2, r3, r4)
            int r7 = r7 + -1
            goto L_0x0526
        L_0x0547:
            r0 = r48
            r1 = r16
            r2 = r10
            java.lang.Object r5 = r0.readConst(r1, r2)
            r16 = r5
            goto L_0x0505
        L_0x0553:
            r7 = r23
            goto L_0x0516
        L_0x0556:
            r12 = 0
            goto L_0x0536
        L_0x0558:
            int r6 = r6 + -1
            goto L_0x0510
        L_0x055b:
            if (r6 == 0) goto L_0x0567
            org.objectweb.asm.Attribute r7 = r6.a
            r8 = 0
            r6.a = r8
            r5.visitAttribute(r6)
            r6 = r7
            goto L_0x055b
        L_0x0567:
            r5.visitEnd()
        L_0x056a:
            int r5 = r17 + -1
            r17 = r5
            r5 = r20
            goto L_0x03e0
        L_0x0572:
            r0 = r48
            r1 = r5
            int r6 = r0.readUnsignedShort(r1)
            int r5 = r5 + 2
            r32 = r6
        L_0x057d:
            if (r32 <= 0) goto L_0x11b1
            int r18 = r5 + 6
            r0 = r48
            r1 = r5
            int r6 = r0.readUnsignedShort(r1)
            int r7 = r5 + 2
            r0 = r48
            r1 = r7
            r2 = r10
            java.lang.String r13 = r0.readUTF8(r1, r2)
            int r7 = r5 + 4
            r0 = r48
            r1 = r7
            r2 = r10
            java.lang.String r14 = r0.readUTF8(r1, r2)
            r7 = 0
            r8 = 0
            r9 = 0
            r11 = 0
            r12 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r19 = 0
            int r20 = r5 + 6
            r0 = r48
            r1 = r20
            int r20 = r0.readUnsignedShort(r1)
            int r5 = r5 + 8
            r21 = r11
            r22 = r17
            r23 = r6
            r24 = r16
            r25 = r9
            r26 = r8
            r16 = r20
            r17 = r19
            r20 = r12
            r19 = r15
            r15 = r7
        L_0x05c9:
            if (r16 <= 0) goto L_0x074b
            r0 = r48
            r1 = r5
            r2 = r10
            java.lang.String r7 = r0.readUTF8(r1, r2)
            int r6 = r5 + 2
            r0 = r48
            r1 = r6
            int r9 = r0.readInt(r1)
            int r8 = r5 + 6
            java.lang.String r5 = "Code"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0614
            if (r29 != 0) goto L_0x120c
            r5 = r19
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r15 = r8
            r17 = r23
            r19 = r24
            r20 = r25
            r21 = r26
        L_0x05fa:
            int r8 = r8 + r9
            int r9 = r16 + -1
            r16 = r9
            r22 = r15
            r23 = r17
            r24 = r19
            r25 = r20
            r26 = r21
            r20 = r6
            r21 = r7
            r15 = r11
            r19 = r5
            r17 = r12
            r5 = r8
            goto L_0x05c9
        L_0x0614:
            java.lang.String r5 = "Exceptions"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x062f
            r5 = r19
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r8
            r17 = r23
            r15 = r22
            r19 = r24
            r20 = r25
            r21 = r26
            goto L_0x05fa
        L_0x062f:
            java.lang.String r5 = "Signature"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0653
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.String r5 = r0.readUTF8(r1, r2)
            r6 = r20
            r7 = r21
            r11 = r5
            r12 = r17
            r15 = r22
            r5 = r19
            r17 = r23
            r20 = r25
            r21 = r26
            r19 = r24
            goto L_0x05fa
        L_0x0653:
            java.lang.String r5 = "Deprecated"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0673
            r5 = 131072(0x20000, float:1.83671E-40)
            r5 = r5 | r23
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r15 = r22
            r17 = r5
            r20 = r25
            r21 = r26
            r5 = r19
            r19 = r24
            goto L_0x05fa
        L_0x0673:
            java.lang.String r5 = "RuntimeVisibleAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0690
            r5 = r19
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r15 = r22
            r17 = r23
            r19 = r24
            r20 = r25
            r21 = r8
            goto L_0x05fa
        L_0x0690:
            java.lang.String r5 = "AnnotationDefault"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x06ac
            r5 = r19
            r6 = r20
            r7 = r8
            r11 = r15
            r12 = r17
            r21 = r26
            r17 = r23
            r19 = r24
            r20 = r25
            r15 = r22
            goto L_0x05fa
        L_0x06ac:
            java.lang.String r5 = "Synthetic"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x06ce
            r5 = 266240(0x41000, float:3.73082E-40)
            r5 = r5 | r23
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r15 = r22
            r17 = r5
            r20 = r25
            r21 = r26
            r5 = r19
            r19 = r24
            goto L_0x05fa
        L_0x06ce:
            java.lang.String r5 = "RuntimeInvisibleAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x06eb
            r5 = r19
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r15 = r22
            r17 = r23
            r19 = r24
            r20 = r8
            r21 = r26
            goto L_0x05fa
        L_0x06eb:
            java.lang.String r5 = "RuntimeVisibleParameterAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0707
            r5 = r19
            r6 = r8
            r7 = r21
            r11 = r15
            r12 = r17
            r20 = r25
            r17 = r23
            r19 = r24
            r15 = r22
            r21 = r26
            goto L_0x05fa
        L_0x0707:
            java.lang.String r5 = "RuntimeInvisibleParameterAnnotations"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0723
            r5 = r8
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r19 = r24
            r17 = r23
            r15 = r22
            r20 = r25
            r21 = r26
            goto L_0x05fa
        L_0x0723:
            r11 = -1
            r12 = 0
            r5 = r48
            r6 = r50
            org.objectweb.asm.Attribute r5 = r5.a(r6, r7, r8, r9, r10, r11, r12)
            if (r5 == 0) goto L_0x120c
            r0 = r24
            r1 = r5
            r1.a = r0
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r15 = r22
            r17 = r23
            r20 = r25
            r21 = r26
            r46 = r19
            r19 = r5
            r5 = r46
            goto L_0x05fa
        L_0x074b:
            if (r17 != 0) goto L_0x0798
            r6 = 0
            r16 = r6
            r6 = r17
        L_0x0752:
            r11 = r49
            r12 = r23
            org.objectweb.asm.MethodVisitor r12 = r11.visitMethod(r12, r13, r14, r15, r16)
            if (r12 == 0) goto L_0x0868
            boolean r7 = r12 instanceof org.objectweb.asm.MethodWriter
            if (r7 == 0) goto L_0x07ee
            r0 = r12
            org.objectweb.asm.MethodWriter r0 = (org.objectweb.asm.MethodWriter) r0
            r17 = r0
            r0 = r17
            org.objectweb.asm.ClassWriter r0 = r0.b
            r7 = r0
            org.objectweb.asm.ClassReader r7 = r7.J
            r0 = r7
            r1 = r48
            if (r0 != r1) goto L_0x07ee
            r0 = r17
            java.lang.String r0 = r0.g
            r7 = r0
            if (r15 != r7) goto L_0x07ee
            r7 = 0
            if (r16 != 0) goto L_0x07be
            r0 = r17
            int r0 = r0.j
            r6 = r0
            if (r6 != 0) goto L_0x07bc
            r6 = 1
        L_0x0783:
            if (r6 == 0) goto L_0x07ee
            r0 = r18
            r1 = r17
            r1.h = r0
            int r6 = r5 - r18
            r0 = r6
            r1 = r17
            r1.i = r0
        L_0x0792:
            int r6 = r32 + -1
            r32 = r6
            goto L_0x057d
        L_0x0798:
            r0 = r48
            r1 = r17
            int r6 = r0.readUnsignedShort(r1)
            java.lang.String[] r6 = new java.lang.String[r6]
            int r7 = r17 + 2
            r8 = 0
            r46 = r8
            r8 = r7
            r7 = r46
        L_0x07aa:
            int r9 = r6.length
            if (r7 >= r9) goto L_0x1207
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.String r9 = r0.readClass(r1, r2)
            r6[r7] = r9
            int r8 = r8 + 2
            int r7 = r7 + 1
            goto L_0x07aa
        L_0x07bc:
            r6 = 0
            goto L_0x0783
        L_0x07be:
            r0 = r16
            int r0 = r0.length
            r8 = r0
            r0 = r17
            int r0 = r0.j
            r9 = r0
            if (r8 != r9) goto L_0x1204
            r7 = 1
            r0 = r16
            int r0 = r0.length
            r8 = r0
            r9 = 1
            int r8 = r8 - r9
            r46 = r8
            r8 = r6
            r6 = r46
        L_0x07d5:
            if (r6 < 0) goto L_0x1204
            int r8 = r8 + -2
            r0 = r17
            int[] r0 = r0.k
            r9 = r0
            r9 = r9[r6]
            r0 = r48
            r1 = r8
            int r11 = r0.readUnsignedShort(r1)
            if (r9 == r11) goto L_0x07eb
            r6 = 0
            goto L_0x0783
        L_0x07eb:
            int r6 = r6 + -1
            goto L_0x07d5
        L_0x07ee:
            if (r21 == 0) goto L_0x0804
            org.objectweb.asm.AnnotationVisitor r6 = r12.visitAnnotationDefault()
            r7 = 0
            r0 = r48
            r1 = r21
            r2 = r10
            r3 = r7
            r4 = r6
            r0.a(r1, r2, r3, r4)
            if (r6 == 0) goto L_0x0804
            r6.visitEnd()
        L_0x0804:
            r6 = 1
        L_0x0805:
            if (r6 < 0) goto L_0x0844
            if (r6 != 0) goto L_0x083c
            r7 = r25
        L_0x080b:
            if (r7 == 0) goto L_0x0841
            r0 = r48
            r1 = r7
            int r8 = r0.readUnsignedShort(r1)
            int r7 = r7 + 2
            r46 = r8
            r8 = r7
            r7 = r46
        L_0x081b:
            if (r7 <= 0) goto L_0x0841
            int r9 = r8 + 2
            r11 = 1
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.String r8 = r0.readUTF8(r1, r2)
            if (r6 == 0) goto L_0x083f
            r15 = 1
        L_0x082b:
            org.objectweb.asm.AnnotationVisitor r8 = r12.visitAnnotation(r8, r15)
            r0 = r48
            r1 = r9
            r2 = r10
            r3 = r11
            r4 = r8
            int r8 = r0.a(r1, r2, r3, r4)
            int r7 = r7 + -1
            goto L_0x081b
        L_0x083c:
            r7 = r26
            goto L_0x080b
        L_0x083f:
            r15 = 0
            goto L_0x082b
        L_0x0841:
            int r6 = r6 + -1
            goto L_0x0805
        L_0x0844:
            if (r20 == 0) goto L_0x084f
            r11 = 1
            r7 = r48
            r8 = r20
            r9 = r14
            r7.a(r8, r9, r10, r11, r12)
        L_0x084f:
            if (r19 == 0) goto L_0x085a
            r11 = 0
            r7 = r48
            r8 = r19
            r9 = r14
            r7.a(r8, r9, r10, r11, r12)
        L_0x085a:
            r6 = r24
        L_0x085c:
            if (r6 == 0) goto L_0x0868
            org.objectweb.asm.Attribute r7 = r6.a
            r8 = 0
            r6.a = r8
            r12.visitAttribute(r6)
            r6 = r7
            goto L_0x085c
        L_0x0868:
            if (r12 == 0) goto L_0x11aa
            if (r22 == 0) goto L_0x11aa
            r0 = r48
            r1 = r22
            int r33 = r0.readUnsignedShort(r1)
            int r6 = r22 + 2
            r0 = r48
            r1 = r6
            int r34 = r0.readUnsignedShort(r1)
            int r6 = r22 + 4
            r0 = r48
            r1 = r6
            int r6 = r0.readInt(r1)
            int r35 = r22 + 8
            int r36 = r35 + r6
            r12.visitCode()
            int r7 = r6 + 2
            r0 = r7
            org.objectweb.asm.Label[] r0 = new org.objectweb.asm.Label[r0]
            r21 = r0
            int r7 = r6 + 1
            r0 = r48
            r1 = r7
            r2 = r21
            r0.readLabel(r1, r2)
            r7 = r35
        L_0x08a0:
            r0 = r7
            r1 = r36
            if (r0 >= r1) goto L_0x0984
            int r8 = r7 - r35
            byte r9 = r28[r7]
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte[] r11 = org.objectweb.asm.ClassWriter.a
            byte r9 = r11[r9]
            switch(r9) {
                case 0: goto L_0x08b5;
                case 1: goto L_0x0978;
                case 2: goto L_0x097c;
                case 3: goto L_0x0978;
                case 4: goto L_0x08b5;
                case 5: goto L_0x097c;
                case 6: goto L_0x097c;
                case 7: goto L_0x0980;
                case 8: goto L_0x08b8;
                case 9: goto L_0x08cd;
                case 10: goto L_0x0978;
                case 11: goto L_0x097c;
                case 12: goto L_0x097c;
                case 13: goto L_0x08f2;
                case 14: goto L_0x093a;
                case 15: goto L_0x08b2;
                case 16: goto L_0x08e2;
                default: goto L_0x08b2;
            }
        L_0x08b2:
            int r7 = r7 + 4
            goto L_0x08a0
        L_0x08b5:
            int r7 = r7 + 1
            goto L_0x08a0
        L_0x08b8:
            int r9 = r7 + 1
            r0 = r48
            r1 = r9
            short r9 = r0.readShort(r1)
            int r8 = r8 + r9
            r0 = r48
            r1 = r8
            r2 = r21
            r0.readLabel(r1, r2)
            int r7 = r7 + 3
            goto L_0x08a0
        L_0x08cd:
            int r9 = r7 + 1
            r0 = r48
            r1 = r9
            int r9 = r0.readInt(r1)
            int r8 = r8 + r9
            r0 = r48
            r1 = r8
            r2 = r21
            r0.readLabel(r1, r2)
            int r7 = r7 + 5
            goto L_0x08a0
        L_0x08e2:
            int r8 = r7 + 1
            byte r8 = r28[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            r9 = 132(0x84, float:1.85E-43)
            if (r8 != r9) goto L_0x08ef
            int r7 = r7 + 6
            goto L_0x08a0
        L_0x08ef:
            int r7 = r7 + 4
            goto L_0x08a0
        L_0x08f2:
            int r7 = r7 + 4
            r9 = r8 & 3
            int r7 = r7 - r9
            r0 = r48
            r1 = r7
            int r9 = r0.readInt(r1)
            int r9 = r9 + r8
            r0 = r48
            r1 = r9
            r2 = r21
            r0.readLabel(r1, r2)
            int r9 = r7 + 8
            r0 = r48
            r1 = r9
            int r9 = r0.readInt(r1)
            int r11 = r7 + 4
            r0 = r48
            r1 = r11
            int r11 = r0.readInt(r1)
            int r9 = r9 - r11
            int r9 = r9 + 1
            int r7 = r7 + 12
            r46 = r9
            r9 = r7
            r7 = r46
        L_0x0923:
            if (r7 <= 0) goto L_0x1201
            r0 = r48
            r1 = r9
            int r11 = r0.readInt(r1)
            int r11 = r11 + r8
            r0 = r48
            r1 = r11
            r2 = r21
            r0.readLabel(r1, r2)
            int r9 = r9 + 4
            int r7 = r7 + -1
            goto L_0x0923
        L_0x093a:
            int r7 = r7 + 4
            r9 = r8 & 3
            int r7 = r7 - r9
            r0 = r48
            r1 = r7
            int r9 = r0.readInt(r1)
            int r9 = r9 + r8
            r0 = r48
            r1 = r9
            r2 = r21
            r0.readLabel(r1, r2)
            int r9 = r7 + 4
            r0 = r48
            r1 = r9
            int r9 = r0.readInt(r1)
            int r7 = r7 + 8
            r46 = r9
            r9 = r7
            r7 = r46
        L_0x095f:
            if (r7 <= 0) goto L_0x1201
            int r11 = r9 + 4
            r0 = r48
            r1 = r11
            int r11 = r0.readInt(r1)
            int r11 = r11 + r8
            r0 = r48
            r1 = r11
            r2 = r21
            r0.readLabel(r1, r2)
            int r9 = r9 + 8
            int r7 = r7 + -1
            goto L_0x095f
        L_0x0978:
            int r7 = r7 + 2
            goto L_0x08a0
        L_0x097c:
            int r7 = r7 + 3
            goto L_0x08a0
        L_0x0980:
            int r7 = r7 + 5
            goto L_0x08a0
        L_0x0984:
            r0 = r48
            r1 = r7
            int r8 = r0.readUnsignedShort(r1)
            int r7 = r7 + 2
            r46 = r8
            r8 = r7
            r7 = r46
        L_0x0992:
            if (r7 <= 0) goto L_0x09ff
            r0 = r48
            r1 = r8
            int r9 = r0.readUnsignedShort(r1)
            r0 = r48
            r1 = r9
            r2 = r21
            org.objectweb.asm.Label r9 = r0.readLabel(r1, r2)
            int r11 = r8 + 2
            r0 = r48
            r1 = r11
            int r11 = r0.readUnsignedShort(r1)
            r0 = r48
            r1 = r11
            r2 = r21
            org.objectweb.asm.Label r11 = r0.readLabel(r1, r2)
            int r15 = r8 + 4
            r0 = r48
            r1 = r15
            int r15 = r0.readUnsignedShort(r1)
            r0 = r48
            r1 = r15
            r2 = r21
            org.objectweb.asm.Label r15 = r0.readLabel(r1, r2)
            int r16 = r8 + 6
            r0 = r48
            r1 = r16
            int r16 = r0.readUnsignedShort(r1)
            if (r16 != 0) goto L_0x09e4
            r16 = 0
            r0 = r12
            r1 = r9
            r2 = r11
            r3 = r15
            r4 = r16
            r0.visitTryCatchBlock(r1, r2, r3, r4)
        L_0x09df:
            int r8 = r8 + 8
            int r7 = r7 + -1
            goto L_0x0992
        L_0x09e4:
            r0 = r48
            int[] r0 = r0.a
            r17 = r0
            r16 = r17[r16]
            r0 = r48
            r1 = r16
            r2 = r10
            java.lang.String r16 = r0.readUTF8(r1, r2)
            r0 = r12
            r1 = r9
            r2 = r11
            r3 = r15
            r4 = r16
            r0.visitTryCatchBlock(r1, r2, r3, r4)
            goto L_0x09df
        L_0x09ff:
            r7 = 0
            r9 = 0
            r11 = 0
            r15 = 0
            r16 = 0
            r22 = 0
            r24 = 0
            r25 = 0
            r26 = 0
            r27 = 0
            r37 = 0
            r38 = 0
            r17 = 1
            r18 = 0
            r0 = r48
            r1 = r8
            int r19 = r0.readUnsignedShort(r1)
            int r8 = r8 + 2
            r39 = r17
            r40 = r9
            r41 = r7
            r42 = r8
            r43 = r18
            r8 = r15
            r9 = r11
            r7 = r16
            r11 = r19
        L_0x0a30:
            if (r11 <= 0) goto L_0x0be1
            r0 = r48
            r1 = r42
            r2 = r10
            java.lang.String r44 = r0.readUTF8(r1, r2)
            java.lang.String r15 = "LocalVariableTable"
            r0 = r15
            r1 = r44
            boolean r15 = r0.equals(r1)
            if (r15 == 0) goto L_0x0aa9
            if (r30 != 0) goto L_0x11d6
            int r15 = r42 + 6
            int r16 = r42 + 6
            r0 = r48
            r1 = r16
            int r16 = r0.readUnsignedShort(r1)
            int r17 = r42 + 8
        L_0x0a56:
            if (r16 <= 0) goto L_0x11e3
            r0 = r48
            r1 = r17
            int r18 = r0.readUnsignedShort(r1)
            r19 = r21[r18]
            if (r19 != 0) goto L_0x0a7c
            r0 = r48
            r1 = r18
            r2 = r21
            org.objectweb.asm.Label r19 = r0.readLabel(r1, r2)
            r0 = r19
            int r0 = r0.a
            r20 = r0
            r20 = r20 | 1
            r0 = r20
            r1 = r19
            r1.a = r0
        L_0x0a7c:
            int r19 = r17 + 2
            r0 = r48
            r1 = r19
            int r19 = r0.readUnsignedShort(r1)
            int r18 = r18 + r19
            r19 = r21[r18]
            if (r19 != 0) goto L_0x0aa4
            r0 = r48
            r1 = r18
            r2 = r21
            org.objectweb.asm.Label r18 = r0.readLabel(r1, r2)
            r0 = r18
            int r0 = r0.a
            r19 = r0
            r19 = r19 | 1
            r0 = r19
            r1 = r18
            r1.a = r0
        L_0x0aa4:
            int r17 = r17 + 10
            int r16 = r16 + -1
            goto L_0x0a56
        L_0x0aa9:
            java.lang.String r15 = "LocalVariableTypeTable"
            r0 = r15
            r1 = r44
            boolean r15 = r0.equals(r1)
            if (r15 == 0) goto L_0x0ae0
            int r15 = r42 + 6
            r16 = r15
            r17 = r41
            r18 = r43
            r15 = r9
            r9 = r8
            r8 = r7
            r7 = r39
        L_0x0ac1:
            int r19 = r42 + 2
            r0 = r48
            r1 = r19
            int r19 = r0.readInt(r1)
            int r19 = r19 + 6
            int r19 = r19 + r42
            int r11 = r11 + -1
            r39 = r7
            r40 = r16
            r41 = r17
            r42 = r19
            r43 = r18
            r7 = r8
            r8 = r9
            r9 = r15
            goto L_0x0a30
        L_0x0ae0:
            java.lang.String r15 = "LineNumberTable"
            r0 = r15
            r1 = r44
            boolean r15 = r0.equals(r1)
            if (r15 == 0) goto L_0x0b35
            if (r30 != 0) goto L_0x11d6
            int r15 = r42 + 6
            r0 = r48
            r1 = r15
            int r15 = r0.readUnsignedShort(r1)
            int r16 = r42 + 8
        L_0x0af8:
            if (r15 <= 0) goto L_0x11d6
            r0 = r48
            r1 = r16
            int r17 = r0.readUnsignedShort(r1)
            r18 = r21[r17]
            if (r18 != 0) goto L_0x0b1e
            r0 = r48
            r1 = r17
            r2 = r21
            org.objectweb.asm.Label r18 = r0.readLabel(r1, r2)
            r0 = r18
            int r0 = r0.a
            r19 = r0
            r19 = r19 | 1
            r0 = r19
            r1 = r18
            r1.a = r0
        L_0x0b1e:
            r17 = r21[r17]
            int r18 = r16 + 2
            r0 = r48
            r1 = r18
            int r18 = r0.readUnsignedShort(r1)
            r0 = r18
            r1 = r17
            r1.b = r0
            int r16 = r16 + 4
            int r15 = r15 + -1
            goto L_0x0af8
        L_0x0b35:
            java.lang.String r15 = "StackMapTable"
            r0 = r15
            r1 = r44
            boolean r15 = r0.equals(r1)
            if (r15 == 0) goto L_0x0b68
            r15 = r51 & 4
            if (r15 != 0) goto L_0x11d6
            int r7 = r42 + 8
            int r8 = r42 + 2
            r0 = r48
            r1 = r8
            int r8 = r0.readInt(r1)
            int r9 = r42 + 6
            r0 = r48
            r1 = r9
            int r9 = r0.readUnsignedShort(r1)
            r15 = r7
            r16 = r40
            r17 = r41
            r18 = r43
            r7 = r39
            r46 = r8
            r8 = r9
            r9 = r46
            goto L_0x0ac1
        L_0x0b68:
            java.lang.String r15 = "StackMap"
            r0 = r15
            r1 = r44
            boolean r15 = r0.equals(r1)
            if (r15 == 0) goto L_0x0b9e
            r15 = r51 & 4
            if (r15 != 0) goto L_0x11d6
            int r7 = r42 + 8
            int r8 = r42 + 2
            r0 = r48
            r1 = r8
            int r8 = r0.readInt(r1)
            int r9 = r42 + 6
            r0 = r48
            r1 = r9
            int r9 = r0.readUnsignedShort(r1)
            r15 = 0
            r16 = r40
            r17 = r41
            r18 = r43
            r46 = r7
            r7 = r15
            r15 = r46
            r47 = r9
            r9 = r8
            r8 = r47
            goto L_0x0ac1
        L_0x0b9e:
            r15 = 0
            r45 = r43
            r43 = r15
        L_0x0ba3:
            r0 = r50
            int r0 = r0.length
            r15 = r0
            r0 = r43
            r1 = r15
            if (r0 >= r1) goto L_0x11f4
            r15 = r50[r43]
            java.lang.String r15 = r15.type
            r0 = r15
            r1 = r44
            boolean r15 = r0.equals(r1)
            if (r15 == 0) goto L_0x11f0
            r15 = r50[r43]
            int r17 = r42 + 6
            int r16 = r42 + 2
            r0 = r48
            r1 = r16
            int r18 = r0.readInt(r1)
            r16 = 8
            int r20 = r35 - r16
            r16 = r48
            r19 = r10
            org.objectweb.asm.Attribute r15 = r15.read(r16, r17, r18, r19, r20, r21)
            if (r15 == 0) goto L_0x11f0
            r0 = r45
            r1 = r15
            r1.a = r0
        L_0x0bda:
            int r16 = r43 + 1
            r43 = r16
            r45 = r15
            goto L_0x0ba3
        L_0x0be1:
            if (r9 == 0) goto L_0x11cc
            r0 = r34
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r11 = r0
            r0 = r33
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r15 = r0
            if (r31 == 0) goto L_0x11c8
            r16 = 0
            r17 = r23 & 8
            if (r17 != 0) goto L_0x11c4
            java.lang.String r17 = "<init>"
            r0 = r17
            r1 = r13
            boolean r13 = r0.equals(r1)
            if (r13 == 0) goto L_0x0c5c
            int r13 = r16 + 1
            java.lang.Integer r17 = org.objectweb.asm.Opcodes.UNINITIALIZED_THIS
            r11[r16] = r17
        L_0x0c06:
            r16 = 1
        L_0x0c08:
            int r17 = r16 + 1
            r0 = r14
            r1 = r16
            char r18 = r0.charAt(r1)
            switch(r18) {
                case 66: goto L_0x0c72;
                case 67: goto L_0x0c72;
                case 68: goto L_0x0c94;
                case 70: goto L_0x0c7d;
                case 73: goto L_0x0c72;
                case 74: goto L_0x0c88;
                case 76: goto L_0x0cea;
                case 83: goto L_0x0c72;
                case 90: goto L_0x0c72;
                case 91: goto L_0x0ca0;
                default: goto L_0x0c14;
            }
        L_0x0c14:
            r14 = -1
            r16 = r9
        L_0x0c17:
            int r17 = r9 + r8
            r18 = 2
            int r17 = r17 - r18
            r0 = r16
            r1 = r17
            if (r0 >= r1) goto L_0x0d13
            byte r17 = r28[r16]
            r18 = 8
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0c59
            int r17 = r16 + 1
            r0 = r48
            r1 = r17
            int r17 = r0.readUnsignedShort(r1)
            if (r17 < 0) goto L_0x0c59
            r0 = r17
            r1 = r6
            if (r0 >= r1) goto L_0x0c59
            int r18 = r35 + r17
            byte r18 = r28[r18]
            r0 = r18
            r0 = r0 & 255(0xff, float:3.57E-43)
            r18 = r0
            r19 = 187(0xbb, float:2.62E-43)
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x0c59
            r0 = r48
            r1 = r17
            r2 = r21
            r0.readLabel(r1, r2)
        L_0x0c59:
            int r16 = r16 + 1
            goto L_0x0c17
        L_0x0c5c:
            int r13 = r16 + 1
            r0 = r48
            int r0 = r0.header
            r17 = r0
            int r17 = r17 + 2
            r0 = r48
            r1 = r17
            r2 = r10
            java.lang.String r17 = r0.readClass(r1, r2)
            r11[r16] = r17
            goto L_0x0c06
        L_0x0c72:
            int r16 = r13 + 1
            java.lang.Integer r18 = org.objectweb.asm.Opcodes.INTEGER
            r11[r13] = r18
            r13 = r16
            r16 = r17
            goto L_0x0c08
        L_0x0c7d:
            int r16 = r13 + 1
            java.lang.Integer r18 = org.objectweb.asm.Opcodes.FLOAT
            r11[r13] = r18
            r13 = r16
            r16 = r17
            goto L_0x0c08
        L_0x0c88:
            int r16 = r13 + 1
            java.lang.Integer r18 = org.objectweb.asm.Opcodes.LONG
            r11[r13] = r18
            r13 = r16
            r16 = r17
            goto L_0x0c08
        L_0x0c94:
            int r16 = r13 + 1
            java.lang.Integer r18 = org.objectweb.asm.Opcodes.DOUBLE
            r11[r13] = r18
            r13 = r16
            r16 = r17
            goto L_0x0c08
        L_0x0ca0:
            r0 = r14
            r1 = r17
            char r18 = r0.charAt(r1)
            r19 = 91
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x0cb2
            int r17 = r17 + 1
            goto L_0x0ca0
        L_0x0cb2:
            r0 = r14
            r1 = r17
            char r18 = r0.charAt(r1)
            r19 = 76
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x0cd5
            int r17 = r17 + 1
        L_0x0cc3:
            r0 = r14
            r1 = r17
            char r18 = r0.charAt(r1)
            r19 = 59
            r0 = r18
            r1 = r19
            if (r0 == r1) goto L_0x0cd5
            int r17 = r17 + 1
            goto L_0x0cc3
        L_0x0cd5:
            int r18 = r13 + 1
            int r17 = r17 + 1
            r0 = r14
            r1 = r16
            r2 = r17
            java.lang.String r16 = r0.substring(r1, r2)
            r11[r13] = r16
            r13 = r18
            r16 = r17
            goto L_0x0c08
        L_0x0cea:
            r0 = r14
            r1 = r17
            char r18 = r0.charAt(r1)
            r19 = 59
            r0 = r18
            r1 = r19
            if (r0 == r1) goto L_0x0cfc
            int r17 = r17 + 1
            goto L_0x0cea
        L_0x0cfc:
            int r18 = r13 + 1
            int r16 = r16 + 1
            int r19 = r17 + 1
            r0 = r14
            r1 = r16
            r2 = r17
            java.lang.String r16 = r0.substring(r1, r2)
            r11[r13] = r16
            r13 = r18
            r16 = r19
            goto L_0x0c08
        L_0x0d13:
            r17 = r15
            r6 = r11
            r8 = r13
            r11 = r14
        L_0x0d18:
            r13 = r22
            r14 = r7
            r15 = r9
            r37 = r35
            r9 = r8
            r7 = r27
            r8 = r26
        L_0x0d23:
            r0 = r37
            r1 = r36
            if (r0 >= r1) goto L_0x10ea
            int r38 = r37 - r35
            r16 = r21[r38]
            if (r16 == 0) goto L_0x0d4d
            r0 = r12
            r1 = r16
            r0.visitLabel(r1)
            if (r30 != 0) goto L_0x0d4d
            r0 = r16
            int r0 = r0.b
            r18 = r0
            if (r18 <= 0) goto L_0x0d4d
            r0 = r16
            int r0 = r0.b
            r18 = r0
            r0 = r12
            r1 = r18
            r2 = r16
            r0.visitLineNumber(r1, r2)
        L_0x0d4d:
            r16 = r7
            r24 = r8
            r23 = r13
            r42 = r14
            r14 = r9
            r7 = r15
            r15 = r6
            r6 = r11
        L_0x0d59:
            if (r15 == 0) goto L_0x0ea5
            r0 = r6
            r1 = r38
            if (r0 == r1) goto L_0x0d63
            r8 = -1
            if (r6 != r8) goto L_0x0ea5
        L_0x0d63:
            if (r39 == 0) goto L_0x0d67
            if (r31 == 0) goto L_0x0da1
        L_0x0d67:
            r13 = -1
            r12.visitFrame(r13, r14, r15, r16, r17)
        L_0x0d6b:
            if (r42 <= 0) goto L_0x0ea1
            if (r39 == 0) goto L_0x0db0
            int r8 = r7 + 1
            byte r7 = r28[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r13 = r7
            r22 = r6
            r19 = r8
        L_0x0d7a:
            r6 = 0
            r7 = 64
            if (r13 >= r7) goto L_0x0db9
            r7 = 3
            r8 = 0
            r9 = r14
            r11 = r7
            r7 = r8
            r8 = r6
            r6 = r13
            r13 = r19
        L_0x0d88:
            int r6 = r6 + 1
            int r6 = r6 + r22
            r0 = r48
            r1 = r6
            r2 = r21
            r0.readLabel(r1, r2)
            int r14 = r42 + -1
            r16 = r7
            r24 = r8
            r23 = r11
            r42 = r14
            r14 = r9
            r7 = r13
            goto L_0x0d59
        L_0x0da1:
            r8 = -1
            if (r6 == r8) goto L_0x0d6b
            r22 = r12
            r25 = r15
            r26 = r16
            r27 = r17
            r22.visitFrame(r23, r24, r25, r26, r27)
            goto L_0x0d6b
        L_0x0db0:
            r6 = 255(0xff, float:3.57E-43)
            r8 = -1
            r13 = r6
            r22 = r8
            r19 = r7
            goto L_0x0d7a
        L_0x0db9:
            r7 = 128(0x80, float:1.794E-43)
            if (r13 >= r7) goto L_0x0dd4
            r7 = 64
            int r7 = r13 - r7
            r18 = 0
            r16 = r48
            r20 = r10
            int r8 = r16.a(r17, r18, r19, r20, r21)
            r9 = 4
            r11 = 1
            r13 = r8
            r8 = r6
            r6 = r7
            r7 = r11
            r11 = r9
            r9 = r14
            goto L_0x0d88
        L_0x0dd4:
            r0 = r48
            r1 = r19
            int r23 = r0.readUnsignedShort(r1)
            int r19 = r19 + 2
            r7 = 247(0xf7, float:3.46E-43)
            if (r13 != r7) goto L_0x0df6
            r18 = 0
            r16 = r48
            r20 = r10
            int r7 = r16.a(r17, r18, r19, r20, r21)
            r8 = 4
            r9 = 1
            r11 = r8
            r13 = r7
            r8 = r6
            r7 = r9
            r6 = r23
            r9 = r14
            goto L_0x0d88
        L_0x0df6:
            r7 = 248(0xf8, float:3.48E-43)
            if (r13 < r7) goto L_0x0e12
            r7 = 251(0xfb, float:3.52E-43)
            if (r13 >= r7) goto L_0x0e12
            r6 = 2
            r7 = 251(0xfb, float:3.52E-43)
            int r7 = r7 - r13
            int r8 = r14 - r7
            r9 = 0
            r11 = r6
            r13 = r19
            r6 = r23
            r46 = r8
            r8 = r7
            r7 = r9
            r9 = r46
            goto L_0x0d88
        L_0x0e12:
            r7 = 251(0xfb, float:3.52E-43)
            if (r13 != r7) goto L_0x0e22
            r7 = 3
            r8 = 0
            r9 = r14
            r11 = r7
            r13 = r19
            r7 = r8
            r8 = r6
            r6 = r23
            goto L_0x0d88
        L_0x0e22:
            r6 = 255(0xff, float:3.57E-43)
            if (r13 >= r6) goto L_0x0e5a
            if (r31 == 0) goto L_0x0e47
            r6 = r14
        L_0x0e29:
            r7 = 251(0xfb, float:3.52E-43)
            int r7 = r13 - r7
            r9 = r19
            r16 = r7
            r8 = r6
        L_0x0e32:
            if (r16 <= 0) goto L_0x0e49
            int r18 = r8 + 1
            r6 = r48
            r7 = r15
            r11 = r21
            int r6 = r6.a(r7, r8, r9, r10, r11)
            int r7 = r16 + -1
            r9 = r6
            r16 = r7
            r8 = r18
            goto L_0x0e32
        L_0x0e47:
            r6 = 0
            goto L_0x0e29
        L_0x0e49:
            r6 = 1
            r7 = 251(0xfb, float:3.52E-43)
            int r7 = r13 - r7
            int r8 = r14 + r7
            r11 = 0
            r13 = r9
            r9 = r8
            r8 = r7
            r7 = r11
            r11 = r6
            r6 = r23
            goto L_0x0d88
        L_0x0e5a:
            r13 = 0
            r0 = r48
            r1 = r19
            int r14 = r0.readUnsignedShort(r1)
            int r6 = r19 + 2
            r7 = 0
            r16 = r14
            r9 = r6
            r8 = r7
        L_0x0e6a:
            if (r16 <= 0) goto L_0x0e7f
            int r18 = r8 + 1
            r6 = r48
            r7 = r15
            r11 = r21
            int r6 = r6.a(r7, r8, r9, r10, r11)
            int r7 = r16 + -1
            r16 = r7
            r9 = r6
            r8 = r18
            goto L_0x0e6a
        L_0x0e7f:
            r0 = r48
            r1 = r9
            int r6 = r0.readUnsignedShort(r1)
            int r7 = r9 + 2
            r8 = 0
            r19 = r7
            r18 = r8
            r7 = r6
        L_0x0e8e:
            if (r7 <= 0) goto L_0x11ba
            int r8 = r18 + 1
            r16 = r48
            r20 = r10
            int r9 = r16.a(r17, r18, r19, r20, r21)
            int r7 = r7 + -1
            r19 = r9
            r18 = r8
            goto L_0x0e8e
        L_0x0ea1:
            r8 = 0
            r15 = r8
            goto L_0x0d59
        L_0x0ea5:
            byte r8 = r28[r37]
            r8 = r8 & 255(0xff, float:3.57E-43)
            byte[] r9 = org.objectweb.asm.ClassWriter.a
            byte r9 = r9[r8]
            switch(r9) {
                case 0: goto L_0x0ed5;
                case 1: goto L_0x1017;
                case 2: goto L_0x1022;
                case 3: goto L_0x100a;
                case 4: goto L_0x0edb;
                case 5: goto L_0x10c8;
                case 6: goto L_0x105f;
                case 7: goto L_0x105f;
                case 8: goto L_0x0ef9;
                case 9: goto L_0x0f0c;
                case 10: goto L_0x1032;
                case 11: goto L_0x1047;
                case 12: goto L_0x10d9;
                case 13: goto L_0x0f54;
                case 14: goto L_0x0fb4;
                case 15: goto L_0x0eb0;
                case 16: goto L_0x0f22;
                default: goto L_0x0eb0;
            }
        L_0x0eb0:
            int r8 = r37 + 1
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.String r8 = r0.readClass(r1, r2)
            int r9 = r37 + 3
            byte r9 = r28[r9]
            r9 = r9 & 255(0xff, float:3.57E-43)
            r12.visitMultiANewArrayInsn(r8, r9)
            int r8 = r37 + 4
        L_0x0ec5:
            r9 = r14
            r11 = r6
            r13 = r23
            r37 = r8
            r8 = r24
            r14 = r42
            r6 = r15
            r15 = r7
            r7 = r16
            goto L_0x0d23
        L_0x0ed5:
            r12.visitInsn(r8)
            int r8 = r37 + 1
            goto L_0x0ec5
        L_0x0edb:
            r9 = 54
            if (r8 <= r9) goto L_0x0eed
            int r8 = r8 + -59
            int r9 = r8 >> 2
            int r9 = r9 + 54
            r8 = r8 & 3
            r12.visitVarInsn(r9, r8)
        L_0x0eea:
            int r8 = r37 + 1
            goto L_0x0ec5
        L_0x0eed:
            int r8 = r8 + -26
            int r9 = r8 >> 2
            int r9 = r9 + 21
            r8 = r8 & 3
            r12.visitVarInsn(r9, r8)
            goto L_0x0eea
        L_0x0ef9:
            int r9 = r37 + 1
            r0 = r48
            r1 = r9
            short r9 = r0.readShort(r1)
            int r9 = r9 + r38
            r9 = r21[r9]
            r12.visitJumpInsn(r8, r9)
            int r8 = r37 + 3
            goto L_0x0ec5
        L_0x0f0c:
            r9 = 33
            int r8 = r8 - r9
            int r9 = r37 + 1
            r0 = r48
            r1 = r9
            int r9 = r0.readInt(r1)
            int r9 = r9 + r38
            r9 = r21[r9]
            r12.visitJumpInsn(r8, r9)
            int r8 = r37 + 5
            goto L_0x0ec5
        L_0x0f22:
            int r8 = r37 + 1
            byte r8 = r28[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            r9 = 132(0x84, float:1.85E-43)
            if (r8 != r9) goto L_0x0f44
            int r8 = r37 + 2
            r0 = r48
            r1 = r8
            int r8 = r0.readUnsignedShort(r1)
            int r9 = r37 + 4
            r0 = r48
            r1 = r9
            short r9 = r0.readShort(r1)
            r12.visitIincInsn(r8, r9)
            int r8 = r37 + 6
            goto L_0x0ec5
        L_0x0f44:
            int r9 = r37 + 2
            r0 = r48
            r1 = r9
            int r9 = r0.readUnsignedShort(r1)
            r12.visitVarInsn(r8, r9)
            int r8 = r37 + 4
            goto L_0x0ec5
        L_0x0f54:
            int r8 = r37 + 4
            r9 = r38 & 3
            int r8 = r8 - r9
            r0 = r48
            r1 = r8
            int r9 = r0.readInt(r1)
            int r9 = r9 + r38
            int r11 = r8 + 4
            r0 = r48
            r1 = r11
            int r11 = r0.readInt(r1)
            int r13 = r8 + 8
            r0 = r48
            r1 = r13
            int r13 = r0.readInt(r1)
            int r8 = r8 + 12
            int r18 = r13 - r11
            int r18 = r18 + 1
            r0 = r18
            org.objectweb.asm.Label[] r0 = new org.objectweb.asm.Label[r0]
            r18 = r0
            r19 = 0
            r46 = r19
            r19 = r8
            r8 = r46
        L_0x0f88:
            r0 = r18
            int r0 = r0.length
            r20 = r0
            r0 = r8
            r1 = r20
            if (r0 >= r1) goto L_0x0fa5
            r0 = r48
            r1 = r19
            int r20 = r0.readInt(r1)
            int r20 = r20 + r38
            r20 = r21[r20]
            r18[r8] = r20
            int r19 = r19 + 4
            int r8 = r8 + 1
            goto L_0x0f88
        L_0x0fa5:
            r8 = r21[r9]
            r0 = r12
            r1 = r11
            r2 = r13
            r3 = r8
            r4 = r18
            r0.visitTableSwitchInsn(r1, r2, r3, r4)
            r8 = r19
            goto L_0x0ec5
        L_0x0fb4:
            int r8 = r37 + 4
            r9 = r38 & 3
            int r8 = r8 - r9
            r0 = r48
            r1 = r8
            int r9 = r0.readInt(r1)
            int r9 = r9 + r38
            int r11 = r8 + 4
            r0 = r48
            r1 = r11
            int r11 = r0.readInt(r1)
            int r8 = r8 + 8
            int[] r13 = new int[r11]
            org.objectweb.asm.Label[] r11 = new org.objectweb.asm.Label[r11]
            r18 = 0
            r46 = r18
            r18 = r8
            r8 = r46
        L_0x0fd9:
            r0 = r13
            int r0 = r0.length
            r19 = r0
            r0 = r8
            r1 = r19
            if (r0 >= r1) goto L_0x1001
            r0 = r48
            r1 = r18
            int r19 = r0.readInt(r1)
            r13[r8] = r19
            int r19 = r18 + 4
            r0 = r48
            r1 = r19
            int r19 = r0.readInt(r1)
            int r19 = r19 + r38
            r19 = r21[r19]
            r11[r8] = r19
            int r18 = r18 + 8
            int r8 = r8 + 1
            goto L_0x0fd9
        L_0x1001:
            r8 = r21[r9]
            r12.visitLookupSwitchInsn(r8, r13, r11)
            r8 = r18
            goto L_0x0ec5
        L_0x100a:
            int r9 = r37 + 1
            byte r9 = r28[r9]
            r9 = r9 & 255(0xff, float:3.57E-43)
            r12.visitVarInsn(r8, r9)
            int r8 = r37 + 2
            goto L_0x0ec5
        L_0x1017:
            int r9 = r37 + 1
            byte r9 = r28[r9]
            r12.visitIntInsn(r8, r9)
            int r8 = r37 + 2
            goto L_0x0ec5
        L_0x1022:
            int r9 = r37 + 1
            r0 = r48
            r1 = r9
            short r9 = r0.readShort(r1)
            r12.visitIntInsn(r8, r9)
            int r8 = r37 + 3
            goto L_0x0ec5
        L_0x1032:
            int r8 = r37 + 1
            byte r8 = r28[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.Object r8 = r0.readConst(r1, r2)
            r12.visitLdcInsn(r8)
            int r8 = r37 + 2
            goto L_0x0ec5
        L_0x1047:
            int r8 = r37 + 1
            r0 = r48
            r1 = r8
            int r8 = r0.readUnsignedShort(r1)
            r0 = r48
            r1 = r8
            r2 = r10
            java.lang.Object r8 = r0.readConst(r1, r2)
            r12.visitLdcInsn(r8)
            int r8 = r37 + 3
            goto L_0x0ec5
        L_0x105f:
            r0 = r48
            int[] r0 = r0.a
            r9 = r0
            int r11 = r37 + 1
            r0 = r48
            r1 = r11
            int r11 = r0.readUnsignedShort(r1)
            r9 = r9[r11]
            r11 = 186(0xba, float:2.6E-43)
            if (r8 != r11) goto L_0x10a2
            java.lang.String r11 = "java/lang/dyn/Dynamic"
            r46 = r11
            r11 = r9
            r9 = r46
        L_0x107a:
            r0 = r48
            r1 = r11
            r2 = r10
            java.lang.String r13 = r0.readUTF8(r1, r2)
            int r11 = r11 + 2
            r0 = r48
            r1 = r11
            r2 = r10
            java.lang.String r11 = r0.readUTF8(r1, r2)
            r18 = 182(0xb6, float:2.55E-43)
            r0 = r8
            r1 = r18
            if (r0 >= r1) goto L_0x10c0
            r12.visitFieldInsn(r8, r9, r13, r11)
        L_0x1096:
            r9 = 185(0xb9, float:2.59E-43)
            if (r8 == r9) goto L_0x109e
            r9 = 186(0xba, float:2.6E-43)
            if (r8 != r9) goto L_0x10c4
        L_0x109e:
            int r8 = r37 + 5
            goto L_0x0ec5
        L_0x10a2:
            r0 = r48
            r1 = r9
            r2 = r10
            java.lang.String r11 = r0.readClass(r1, r2)
            r0 = r48
            int[] r0 = r0.a
            r13 = r0
            int r9 = r9 + 2
            r0 = r48
            r1 = r9
            int r9 = r0.readUnsignedShort(r1)
            r9 = r13[r9]
            r46 = r11
            r11 = r9
            r9 = r46
            goto L_0x107a
        L_0x10c0:
            r12.visitMethodInsn(r8, r9, r13, r11)
            goto L_0x1096
        L_0x10c4:
            int r8 = r37 + 3
            goto L_0x0ec5
        L_0x10c8:
            int r9 = r37 + 1
            r0 = r48
            r1 = r9
            r2 = r10
            java.lang.String r9 = r0.readClass(r1, r2)
            r12.visitTypeInsn(r8, r9)
            int r8 = r37 + 3
            goto L_0x0ec5
        L_0x10d9:
            int r8 = r37 + 1
            byte r8 = r28[r8]
            r8 = r8 & 255(0xff, float:3.57E-43)
            int r9 = r37 + 2
            byte r9 = r28[r9]
            r12.visitIincInsn(r8, r9)
            int r8 = r37 + 3
            goto L_0x0ec5
        L_0x10ea:
            int r6 = r36 - r35
            r6 = r21[r6]
            if (r6 == 0) goto L_0x10f3
            r12.visitLabel(r6)
        L_0x10f3:
            if (r30 != 0) goto L_0x11b7
            if (r41 == 0) goto L_0x11b7
            r6 = 0
            if (r40 == 0) goto L_0x112c
            r0 = r48
            r1 = r40
            int r6 = r0.readUnsignedShort(r1)
            int r6 = r6 * 3
            int r7 = r40 + 2
            int[] r8 = new int[r6]
        L_0x1108:
            if (r6 <= 0) goto L_0x112b
            int r6 = r6 + -1
            int r9 = r7 + 6
            r8[r6] = r9
            int r6 = r6 + -1
            int r9 = r7 + 8
            r0 = r48
            r1 = r9
            int r9 = r0.readUnsignedShort(r1)
            r8[r6] = r9
            int r6 = r6 + -1
            r0 = r48
            r1 = r7
            int r9 = r0.readUnsignedShort(r1)
            r8[r6] = r9
            int r7 = r7 + 10
            goto L_0x1108
        L_0x112b:
            r6 = r8
        L_0x112c:
            r0 = r48
            r1 = r41
            int r7 = r0.readUnsignedShort(r1)
            int r8 = r41 + 2
        L_0x1136:
            if (r7 <= 0) goto L_0x11b7
            r0 = r48
            r1 = r8
            int r9 = r0.readUnsignedShort(r1)
            int r11 = r8 + 2
            r0 = r48
            r1 = r11
            int r11 = r0.readUnsignedShort(r1)
            int r13 = r8 + 8
            r0 = r48
            r1 = r13
            int r18 = r0.readUnsignedShort(r1)
            r13 = 0
            if (r6 == 0) goto L_0x11b5
            r14 = 0
        L_0x1155:
            int r15 = r6.length
            if (r14 >= r15) goto L_0x11b5
            r15 = r6[r14]
            if (r15 != r9) goto L_0x1193
            int r15 = r14 + 1
            r15 = r6[r15]
            r0 = r15
            r1 = r18
            if (r0 != r1) goto L_0x1193
            int r13 = r14 + 2
            r13 = r6[r13]
            r0 = r48
            r1 = r13
            r2 = r10
            java.lang.String r13 = r0.readUTF8(r1, r2)
            r15 = r13
        L_0x1172:
            int r13 = r8 + 4
            r0 = r48
            r1 = r13
            r2 = r10
            java.lang.String r13 = r0.readUTF8(r1, r2)
            int r14 = r8 + 6
            r0 = r48
            r1 = r14
            r2 = r10
            java.lang.String r14 = r0.readUTF8(r1, r2)
            r16 = r21[r9]
            int r9 = r9 + r11
            r17 = r21[r9]
            r12.visitLocalVariable(r13, r14, r15, r16, r17, r18)
            int r8 = r8 + 10
            int r7 = r7 + -1
            goto L_0x1136
        L_0x1193:
            int r14 = r14 + 3
            goto L_0x1155
        L_0x1196:
            if (r6 == 0) goto L_0x11a2
            org.objectweb.asm.Attribute r7 = r6.a
            r8 = 0
            r6.a = r8
            r12.visitAttribute(r6)
            r6 = r7
            goto L_0x1196
        L_0x11a2:
            r0 = r12
            r1 = r33
            r2 = r34
            r0.visitMaxs(r1, r2)
        L_0x11aa:
            if (r12 == 0) goto L_0x0792
            r12.visitEnd()
            goto L_0x0792
        L_0x11b1:
            r49.visitEnd()
            return
        L_0x11b5:
            r15 = r13
            goto L_0x1172
        L_0x11b7:
            r6 = r43
            goto L_0x1196
        L_0x11ba:
            r7 = r6
            r8 = r14
            r9 = r14
            r11 = r13
            r6 = r23
            r13 = r19
            goto L_0x0d88
        L_0x11c4:
            r13 = r16
            goto L_0x0c06
        L_0x11c8:
            r13 = r25
            goto L_0x0c14
        L_0x11cc:
            r17 = r38
            r6 = r37
            r8 = r25
            r11 = r24
            goto L_0x0d18
        L_0x11d6:
            r15 = r9
            r16 = r40
            r17 = r41
            r18 = r43
            r9 = r8
            r8 = r7
            r7 = r39
            goto L_0x0ac1
        L_0x11e3:
            r16 = r40
            r17 = r15
            r18 = r43
            r15 = r9
            r9 = r8
            r8 = r7
            r7 = r39
            goto L_0x0ac1
        L_0x11f0:
            r15 = r45
            goto L_0x0bda
        L_0x11f4:
            r15 = r9
            r16 = r40
            r17 = r41
            r18 = r45
            r9 = r8
            r8 = r7
            r7 = r39
            goto L_0x0ac1
        L_0x1201:
            r7 = r9
            goto L_0x08a0
        L_0x1204:
            r6 = r7
            goto L_0x0783
        L_0x1207:
            r16 = r6
            r6 = r8
            goto L_0x0752
        L_0x120c:
            r5 = r19
            r6 = r20
            r7 = r21
            r11 = r15
            r12 = r17
            r15 = r22
            r17 = r23
            r19 = r24
            r20 = r25
            r21 = r26
            goto L_0x05fa
        L_0x1221:
            r6 = r21
            goto L_0x055b
        L_0x1225:
            r5 = r16
            r6 = r15
            r7 = r19
            r8 = r21
            r9 = r22
            r11 = r23
            goto L_0x0441
        L_0x1232:
            r5 = r27
            goto L_0x035e
        L_0x1236:
            r5 = r19
            r6 = r20
            r7 = r21
            r8 = r22
            r9 = r23
            r11 = r15
            r12 = r25
            r15 = r27
            r19 = r32
            r20 = r33
            goto L_0x014e
        L_0x124b:
            r6 = r19
            r7 = r20
            goto L_0x01d0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.objectweb.asm.ClassReader.accept(org.objectweb.asm.ClassVisitor, org.objectweb.asm.Attribute[], int):void");
    }

    public int getAccess() {
        return readUnsignedShort(this.header);
    }

    public String getClassName() {
        return readClass(this.header + 2, new char[this.d]);
    }

    public String[] getInterfaces() {
        int i = this.header + 6;
        int readUnsignedShort = readUnsignedShort(i);
        String[] strArr = new String[readUnsignedShort];
        if (readUnsignedShort > 0) {
            char[] cArr = new char[this.d];
            int i2 = i;
            for (int i3 = 0; i3 < readUnsignedShort; i3++) {
                i2 += 2;
                strArr[i3] = readClass(i2, cArr);
            }
        }
        return strArr;
    }

    public int getItem(int i) {
        return this.a[i];
    }

    public String getSuperName() {
        int i = this.a[readUnsignedShort(this.header + 4)];
        if (i == 0) {
            return null;
        }
        return readUTF8(i, new char[this.d]);
    }

    public int readByte(int i) {
        return this.b[i] & 255;
    }

    public String readClass(int i, char[] cArr) {
        return readUTF8(this.a[readUnsignedShort(i)], cArr);
    }

    public Object readConst(int i, char[] cArr) {
        int i2 = this.a[i];
        switch (this.b[i2 - 1]) {
            case 3:
                return new Integer(readInt(i2));
            case 4:
                return new Float(Float.intBitsToFloat(readInt(i2)));
            case 5:
                return new Long(readLong(i2));
            case 6:
                return new Double(Double.longBitsToDouble(readLong(i2)));
            case 7:
                return Type.getObjectType(readUTF8(i2, cArr));
            default:
                return readUTF8(i2, cArr);
        }
    }

    public int readInt(int i) {
        byte[] bArr = this.b;
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    /* access modifiers changed from: protected */
    public Label readLabel(int i, Label[] labelArr) {
        if (labelArr[i] == null) {
            labelArr[i] = new Label();
        }
        return labelArr[i];
    }

    public long readLong(int i) {
        return (((long) readInt(i)) << 32) | (((long) readInt(i + 4)) & 4294967295L);
    }

    public short readShort(int i) {
        byte[] bArr = this.b;
        return (short) ((bArr[i + 1] & 255) | ((bArr[i] & 255) << 8));
    }

    public String readUTF8(int i, char[] cArr) {
        int readUnsignedShort = readUnsignedShort(i);
        String str = this.c[readUnsignedShort];
        if (str != null) {
            return str;
        }
        int i2 = this.a[readUnsignedShort];
        String[] strArr = this.c;
        String a2 = a(i2 + 2, readUnsignedShort(i2), cArr);
        strArr[readUnsignedShort] = a2;
        return a2;
    }

    public int readUnsignedShort(int i) {
        byte[] bArr = this.b;
        return (bArr[i + 1] & 255) | ((bArr[i] & 255) << 8);
    }
}
