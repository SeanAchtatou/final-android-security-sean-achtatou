package com.gmail.ocogamas.super_exciting_confrontation.game;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import com.gmail.ocogamas.super_exciting_confrontation.R;
import com.gmail.ocogamas.super_exciting_confrontation.constant.AdvertisementConstant;
import com.gmail.ocogamas.super_exciting_confrontation.constant.Constant;
import com.gmail.ocogamas.super_exciting_confrontation.constant.GameConstant;
import java.lang.reflect.Array;
import java.util.Random;
import jp.co.nobot.libAdMaker.libAdMaker;

public class BoardGameActivity extends Activity {
    private static final String log = "[X] BoardGameActivity [X]";
    private libAdMaker AdMaker;
    /* access modifiers changed from: private */
    public int alpha;
    ImageView[][] mBallImageView = ((ImageView[][]) Array.newInstance(ImageView.class, 8, 8));
    int mSound00;
    int mSound01;
    int mSound02;
    int mSound03;
    int mSound04;
    SoundPool mSoundPool;
    float mSoundVolume;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameConstant.mTurn = 1;
        GameConstant.mEnemyTurn = 0;
        this.mSoundPool = new SoundPool(4, 3, 0);
        this.mSound00 = this.mSoundPool.load(this, R.raw.sound00, 1);
        this.mSound01 = this.mSoundPool.load(this, R.raw.sound01, 1);
        this.mSound02 = this.mSoundPool.load(this, R.raw.sound02, 1);
        this.mSound03 = this.mSoundPool.load(this, R.raw.sound03, 1);
        this.mSound04 = this.mSoundPool.load(this, R.raw.sound04, 1);
        requestWindowFeature(1);
        setContentView(R.layout.activity_board_game);
        SeekBar seekbar = (SeekBar) findViewById(R.id.seekbar);
        seekbar.setMax(100);
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.SP, 0);
        seekbar.setProgress(sharedPreferences.getInt(Constant.SP_KEY_VOLUME, 70));
        this.mSoundVolume = ((float) seekbar.getProgress()) / 100.0f;
        final SharedPreferences sharedPreferences2 = sharedPreferences;
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d(BoardGameActivity.log, "volume = " + BoardGameActivity.this.mSoundVolume);
                BoardGameActivity.this.mSoundPool.play(BoardGameActivity.this.mSound04, BoardGameActivity.this.mSoundVolume, BoardGameActivity.this.mSoundVolume, 1, 0, 1.0f);
                SharedPreferences.Editor e = sharedPreferences2.edit();
                e.putInt(Constant.SP_KEY_VOLUME, seekBar.getProgress());
                e.commit();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                BoardGameActivity.this.mSoundVolume = ((float) progress) / 100.0f;
            }
        });
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker = (libAdMaker) findViewById(R.id.admakerview);
            this.AdMaker.siteId = AdvertisementConstant.SITE_ID;
            this.AdMaker.zoneId = AdvertisementConstant.ZONE_ID;
            this.AdMaker.setUrl(AdvertisementConstant.AD_URL);
            this.AdMaker.start();
        }
        LinearLayout rootLayout = (LinearLayout) findViewById(R.id.root_layout);
        Random random = new Random();
        int randomInteger = random.nextInt(8);
        int backgroundResource = 0;
        Log.d(log, "randomInteger = " + randomInteger);
        if (randomInteger == 0) {
            backgroundResource = R.drawable.bg00;
        } else if (randomInteger == 1) {
            backgroundResource = R.drawable.bg01;
        } else if (randomInteger == 2) {
            backgroundResource = R.drawable.bg02;
        } else if (randomInteger == 3) {
            backgroundResource = R.drawable.bg03;
        } else if (randomInteger == 4) {
            backgroundResource = R.drawable.bg04;
        } else if (randomInteger == 5) {
            backgroundResource = R.drawable.bg05;
        } else if (randomInteger == 6) {
            backgroundResource = R.drawable.bg06;
        } else if (randomInteger == 7) {
            backgroundResource = R.drawable.bg07;
        }
        int randomInteger2 = random.nextInt(8);
        int randomAlpha = (random.nextInt(3) + 1) * 16;
        LinearLayout coverLayout = (LinearLayout) findViewById(R.id.cover_layout);
        Log.d(log, "randomInteger（背景） = " + randomInteger2 + ", alpha = " + randomAlpha);
        setBackGroundColor(coverLayout, randomInteger2, randomAlpha);
        rootLayout.setBackgroundResource(backgroundResource);
        this.alpha = randomAlpha;
        final LinearLayout linearLayout = coverLayout;
        final int i = randomInteger2;
        ((ImageView) findViewById(R.id.change_light_alpha_image_view)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BoardGameActivity.this.alpha == 16) {
                    BoardGameActivity.this.alpha = 32;
                } else if (BoardGameActivity.this.alpha == 32) {
                    BoardGameActivity.this.alpha = 48;
                } else {
                    BoardGameActivity.this.alpha = 16;
                }
                BoardGameActivity.this.setBackGroundColor(linearLayout, i, BoardGameActivity.this.alpha);
            }
        });
        LinearLayout[] columnLinearLayout = {(LinearLayout) findViewById(R.id.column_1_linear_layout), (LinearLayout) findViewById(R.id.column_2_linear_layout), (LinearLayout) findViewById(R.id.column_3_linear_layout), (LinearLayout) findViewById(R.id.column_4_linear_layout), (LinearLayout) findViewById(R.id.column_5_linear_layout), (LinearLayout) findViewById(R.id.column_6_linear_layout), (LinearLayout) findViewById(R.id.column_7_linear_layout), (LinearLayout) findViewById(R.id.column_8_linear_layout)};
        Log.d(log, new StringBuilder().append(getWindow().getContext().getWallpaperDesiredMinimumHeight()).toString());
        int cellHeight = (int) ((((double) getWindow().getContext().getWallpaperDesiredMinimumHeight()) * 0.58d) / 8.0d);
        Log.d(log, new StringBuilder().append(cellHeight).toString());
        ButtonOnClickListener buttonOnClickListener = new ButtonOnClickListener(this, this.mBallImageView);
        for (int y = 0; y < 8; y++) {
            columnLinearLayout[y].setOrientation(1);
            columnLinearLayout[y].setGravity(17);
            for (int x = 0; x < 8; x++) {
                this.mBallImageView[x][y] = new CustomImageView(this, x, y);
                this.mBallImageView[x][y].setTag(0);
                this.mBallImageView[x][y].setLayoutParams(new ViewGroup.LayoutParams(cellHeight, cellHeight));
                this.mBallImageView[x][y].setPadding(6, 6, 6, 6);
                this.mBallImageView[x][y].setImageResource(R.drawable.ball_white_motion);
                this.mBallImageView[x][y].setOnClickListener(buttonOnClickListener);
                columnLinearLayout[y].addView(this.mBallImageView[x][y]);
            }
        }
        int blackBallCount = 0;
        int blackNumber = Integer.parseInt(GameConstant.mBlackNumber);
        while (blackBallCount < blackNumber) {
            int randomX = random.nextInt(8);
            int randomY = random.nextInt(8);
            if (this.mBallImageView[randomX][randomY].getTag().equals(0)) {
                this.mBallImageView[randomX][randomY].setTag(999);
                this.mBallImageView[randomX][randomY].setImageResource(R.drawable.ball_black);
                blackBallCount++;
            }
        }
    }

    /* access modifiers changed from: private */
    public void setBackGroundColor(LinearLayout coverLayout, int randomInteger, int alpha2) {
        if (randomInteger == 0) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 0, 0, 255));
            Log.d(log, "backgroundColor = 青");
        } else if (randomInteger == 1) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 0, 255, 0));
            Log.d(log, "backgroundColor = 緑");
        } else if (randomInteger == 2) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 255, 0, 0));
            Log.d(log, "backgroundColor = 赤");
        } else if (randomInteger == 3) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 255, 255, 0));
            Log.d(log, "backgroundColor = 黄");
        } else if (randomInteger == 4) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 255, 0, 255));
            Log.d(log, "backgroundColor = 紫");
        } else if (randomInteger == 5) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 0, 255, 255));
            Log.d(log, "backgroundColor = シアン");
        } else if (randomInteger == 6) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 255, 255, 255));
            Log.d(log, "backgroundColor = 白");
        } else if (randomInteger == 7) {
            coverLayout.setBackgroundColor(Color.argb(alpha2, 0, 0, 0));
            Log.d(log, "backgroundColor = 黒");
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker.destroy();
            this.AdMaker = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker.stop();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker.start();
        }
    }
}
