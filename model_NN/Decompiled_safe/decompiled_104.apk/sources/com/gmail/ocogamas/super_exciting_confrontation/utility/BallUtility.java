package com.gmail.ocogamas.super_exciting_confrontation.utility;

import com.gmail.ocogamas.super_exciting_confrontation.R;
import com.gmail.ocogamas.super_exciting_confrontation.constant.Constant;

public class BallUtility {
    public static int getBallColorImageResource(String color) {
        if (color.equals(Constant.SP_VALUE_BLUE)) {
            return R.drawable.ball_blue;
        }
        if (color.equals(Constant.SP_VALUE_PINK)) {
            return R.drawable.ball_pink;
        }
        if (color.equals(Constant.SP_VALUE_GREEN)) {
            return R.drawable.ball_green;
        }
        if (color.equals(Constant.SP_VALUE_YELLOW)) {
            return R.drawable.ball_yellow;
        }
        return R.drawable.ball_black;
    }

    public static int getBallLightColorImageResource(String color) {
        if (color.equals(Constant.SP_VALUE_BLUE)) {
            return R.drawable.ball_blue_light;
        }
        if (color.equals(Constant.SP_VALUE_PINK)) {
            return R.drawable.ball_pink_light;
        }
        if (color.equals(Constant.SP_VALUE_GREEN)) {
            return R.drawable.ball_green_light;
        }
        if (color.equals(Constant.SP_VALUE_YELLOW)) {
            return R.drawable.ball_yellow_light;
        }
        return R.drawable.ball_black;
    }
}
