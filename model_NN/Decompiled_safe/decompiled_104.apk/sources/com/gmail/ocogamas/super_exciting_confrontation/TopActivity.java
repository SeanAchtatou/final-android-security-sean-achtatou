package com.gmail.ocogamas.super_exciting_confrontation;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.gmail.ocogamas.super_exciting_confrontation.constant.AdvertisementConstant;
import com.gmail.ocogamas.super_exciting_confrontation.constant.Constant;
import com.gmail.ocogamas.super_exciting_confrontation.constant.GameConstant;
import com.gmail.ocogamas.super_exciting_confrontation.game.BoardGameActivity;
import com.gmail.ocogamas.super_exciting_confrontation.utility.Utility;
import jp.co.nobot.libAdMaker.libAdMaker;

public class TopActivity extends Activity {
    private static Boolean isDataSubmitted = false;
    private static final String log = "[X] TopActivity [X]";
    private static int mCount = 0;
    private libAdMaker AdMaker;
    private Button m1pBlueButton;
    private Button m1pGreenButton;
    private Button m1pPinkButton;
    private Button m1pYellowButton;
    /* access modifiers changed from: private */
    public Button m2pBlueButton;
    /* access modifiers changed from: private */
    public Button m2pGreenButton;
    /* access modifiers changed from: private */
    public Button m2pPinkButton;
    /* access modifiers changed from: private */
    public Button m2pYellowButton;
    /* access modifiers changed from: private */
    public SharedPreferences mSharedPreferences;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_top);
        this.mSharedPreferences = getSharedPreferences(Constant.SP, 0);
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker = (libAdMaker) findViewById(R.id.admakerview);
            this.AdMaker.siteId = AdvertisementConstant.SITE_ID;
            this.AdMaker.zoneId = AdvertisementConstant.ZONE_ID;
            this.AdMaker.setUrl(AdvertisementConstant.AD_URL);
            this.AdMaker.start();
        }
        this.m1pBlueButton = (Button) findViewById(R.id.top_1p_color_blue_button);
        this.m1pPinkButton = (Button) findViewById(R.id.top_1p_color_pink_button);
        this.m1pGreenButton = (Button) findViewById(R.id.top_1p_color_green_button);
        this.m1pYellowButton = (Button) findViewById(R.id.top_1p_color_yellow_button);
        this.m2pBlueButton = (Button) findViewById(R.id.top_2p_color_blue_button);
        this.m2pPinkButton = (Button) findViewById(R.id.top_2p_color_pink_button);
        this.m2pGreenButton = (Button) findViewById(R.id.top_2p_color_green_button);
        this.m2pYellowButton = (Button) findViewById(R.id.top_2p_color_yellow_button);
        GameConstant.m1pColor = this.mSharedPreferences.getString(Constant.SP_KEY_1P_COLOR, Constant.SP_VALUE_BLUE);
        GameConstant.m2pColor = this.mSharedPreferences.getString(Constant.SP_KEY_2P_COLOR, Constant.SP_VALUE_PINK);
        Utility.setupColorButtons(Constant.SP_KEY_1P_COLOR, GameConstant.m1pColor, this.m1pBlueButton, this.m1pPinkButton, this.m1pGreenButton, this.m1pYellowButton);
        Utility.setupColorButtons(Constant.SP_KEY_2P_COLOR, GameConstant.m2pColor, this.m2pBlueButton, this.m2pPinkButton, this.m2pGreenButton, this.m2pYellowButton);
        GameConstant.mWinCount = this.mSharedPreferences.getInt(Constant.SP_KEY_WIN_COUNT, 0);
        GameConstant.mLoseCount = this.mSharedPreferences.getInt(Constant.SP_KEY_LOSE_COUNT, 0);
        TextView countTextView = (TextView) findViewById(R.id.top_count_text_view);
        if (mCount == 0) {
            mCount = Integer.parseInt(this.mSharedPreferences.getString(Constant.SP_KEY_COUNT_STRING, Constant.SP_VALUE_BLACK_NUMBER_0));
            mCount++;
            SharedPreferences.Editor editor = this.mSharedPreferences.edit();
            editor.putString(Constant.SP_KEY_COUNT_STRING, String.valueOf(mCount));
            editor.commit();
        }
        countTextView.setText(String.valueOf(mCount));
        final Button playMode1PlayerButton = (Button) findViewById(R.id.top_1p_mode_button);
        final Button playMode2PlayerButton = (Button) findViewById(R.id.top_2p_mode_button);
        String playMode = this.mSharedPreferences.getString(Constant.SP_KEY_PLAY_MODE, Constant.SP_VALUE_PLAY_MODE_1P);
        if (playMode.equals(Constant.SP_VALUE_PLAY_MODE_1P)) {
            GameConstant.mPlayMode = Constant.SP_VALUE_PLAY_MODE_1P;
            Utility.buttonOn(playMode1PlayerButton);
            Utility.buttonOff(playMode2PlayerButton);
            Utility.setTextButton("COM", this.m2pBlueButton, this.m2pPinkButton, this.m2pGreenButton, this.m2pYellowButton);
        } else {
            GameConstant.mPlayMode = Constant.SP_VALUE_PLAY_MODE_2P;
            Utility.buttonOn(playMode2PlayerButton);
            Utility.buttonOff(playMode1PlayerButton);
            Utility.setTextButton(Constant.SP_VALUE_PLAY_MODE_2P, this.m2pBlueButton, this.m2pPinkButton, this.m2pGreenButton, this.m2pYellowButton);
        }
        if (!isDataSubmitted.booleanValue()) {
            Logger.postData(this, mCount, playMode);
            isDataSubmitted = true;
        }
        playMode1PlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GameConstant.mPlayMode = Constant.SP_VALUE_PLAY_MODE_1P;
                Utility.buttonOn(playMode1PlayerButton);
                Utility.buttonOff(playMode2PlayerButton);
                SharedPreferences.Editor editor = TopActivity.this.mSharedPreferences.edit();
                editor.putString(Constant.SP_KEY_PLAY_MODE, Constant.SP_VALUE_PLAY_MODE_1P);
                editor.commit();
                Utility.setTextButton("COM", TopActivity.this.m2pBlueButton, TopActivity.this.m2pPinkButton, TopActivity.this.m2pGreenButton, TopActivity.this.m2pYellowButton);
            }
        });
        playMode2PlayerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GameConstant.mPlayMode = Constant.SP_VALUE_PLAY_MODE_2P;
                Utility.buttonOn(playMode2PlayerButton);
                Utility.buttonOff(playMode1PlayerButton);
                SharedPreferences.Editor editor = TopActivity.this.mSharedPreferences.edit();
                editor.putString(Constant.SP_KEY_PLAY_MODE, Constant.SP_VALUE_PLAY_MODE_2P);
                editor.commit();
                Utility.setTextButton(Constant.SP_VALUE_PLAY_MODE_2P, TopActivity.this.m2pBlueButton, TopActivity.this.m2pPinkButton, TopActivity.this.m2pGreenButton, TopActivity.this.m2pYellowButton);
            }
        });
        GameConstant.mBlackNumber = this.mSharedPreferences.getString(Constant.SP_KEY_BLACK_NUMBER, Constant.SP_VALUE_BLACK_NUMBER_0);
        Log.d(log, "onCreate   blackNumber = " + GameConstant.mBlackNumber);
        Utility.setupBlackButtons(GameConstant.mBlackNumber, (Button) findViewById(R.id.top_black0_button), (Button) findViewById(R.id.top_black1_button), (Button) findViewById(R.id.top_black2_button), (Button) findViewById(R.id.top_black3_button), (Button) findViewById(R.id.top_black4_button), (Button) findViewById(R.id.top_black5_button));
        ((Button) findViewById(R.id.top_start_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopActivity.this.startActivity(new Intent(TopActivity.this, BoardGameActivity.class));
            }
        });
        ((Button) findViewById(R.id.top_more_app_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/search?q=kondo.masato&so=1&c=apps")));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker.destroy();
            this.AdMaker = null;
        }
        mCount = 0;
        isDataSubmitted = false;
        SharedPreferences.Editor editor = this.mSharedPreferences.edit();
        editor.putString(Constant.SP_KEY_BLACK_NUMBER, GameConstant.mBlackNumber);
        editor.putString(Constant.SP_KEY_1P_COLOR, GameConstant.m1pColor);
        editor.putString(Constant.SP_KEY_2P_COLOR, GameConstant.m2pColor);
        editor.commit();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker.stop();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (AdvertisementConstant.IS_ADVERTISEMENT.booleanValue()) {
            this.AdMaker.start();
        }
    }
}
