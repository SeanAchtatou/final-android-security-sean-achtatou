package com.gmail.ocogamas.super_exciting_confrontation.constant;

public class AdvertisementConstant {
    public static final String AD_URL = "http://images.ad-maker.info/apps/u87vn35xpx8r.html";
    public static final Boolean IS_ADVERTISEMENT = true;
    public static final String SITE_ID = "1742";
    public static final String ZONE_ID = "5398";
}
