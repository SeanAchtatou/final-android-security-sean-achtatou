package com.gmail.ocogamas.super_exciting_confrontation;

import android.content.Context;
import android.net.ConnectivityManager;

public class Logger {
    public static void postData(Context context, int count, String playMode) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            new PostDataTask(context, count, playMode).execute(0);
        }
    }
}
