package com.gmail.ocogamas.super_exciting_confrontation.game;

import android.content.Context;
import android.widget.ImageView;

public class CustomImageView extends ImageView {
    public int x;
    public int y;

    public CustomImageView(Context context, int x2, int y2) {
        super(context);
        this.x = x2;
        this.y = y2;
    }
}
