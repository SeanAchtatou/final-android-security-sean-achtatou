package com.gmail.ocogamas.super_exciting_confrontation.game;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.gmail.ocogamas.super_exciting_confrontation.R;
import com.gmail.ocogamas.super_exciting_confrontation.constant.Constant;
import com.gmail.ocogamas.super_exciting_confrontation.constant.GameConstant;
import com.gmail.ocogamas.super_exciting_confrontation.utility.BallUtility;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

public class ButtonOnClickListener implements View.OnClickListener {
    /* access modifiers changed from: private */
    public static String log = "[X] ButtonOnClickListener [X]";
    Context mContext;
    ImageView[][] mImageView;
    int mX;
    int mY;

    public ButtonOnClickListener(Context context, ImageView[][] imageView) {
        this.mContext = context;
        this.mImageView = imageView;
    }

    public void onClick(View view) {
        int enemyIndex;
        if (!view.getTag().equals(0)) {
            return;
        }
        if (GameConstant.mPlayMode == Constant.SP_VALUE_PLAY_MODE_2P || ((GameConstant.mPlayMode == Constant.SP_VALUE_PLAY_MODE_1P && GameConstant.mTurn == 1) || (GameConstant.mPlayMode == Constant.SP_VALUE_PLAY_MODE_1P && GameConstant.mEnemyTurn == 1))) {
            this.mX = ((CustomImageView) view).x;
            this.mY = ((CustomImageView) view).y;
            playSound();
            if (GameConstant.mTurn == 1) {
                view.setTag(2);
                ((ImageView) view).setImageResource(BallUtility.getBallColorImageResource(GameConstant.m1pColor));
                new ButtonScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, view, 1100);
            } else {
                view.setTag(-2);
                ((ImageView) view).setImageResource(BallUtility.getBallColorImageResource(GameConstant.m2pColor));
                new ButtonScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, view, 1100);
            }
            Log.d(log, "onClick   x:y = " + this.mX + ":" + this.mY);
            changeAround();
            BoardGameActivity activity = (BoardGameActivity) this.mContext;
            TextView score1pTextView = (TextView) activity.findViewById(R.id.score_1p_text_view);
            TextView score2pTextView = (TextView) activity.findViewById(R.id.score_2p_text_view);
            int score1p = calculateScore(1);
            int score2p = calculateScore(-1);
            score1pTextView.setText("1P:" + score1p);
            score2pTextView.setText("2P:" + score2p);
            GameConstant.mTurn *= -1;
            TextView turnTextView = (TextView) activity.findViewById(R.id.player_turn_text_view);
            ScaleAnimation animation = new ScaleAnimation(1.6f, 1.0f, 1.6f, 1.0f, (float) (turnTextView.getWidth() / 2), (float) (turnTextView.getHeight() / 2));
            animation.setDuration(600);
            turnTextView.startAnimation(animation);
            if (GameConstant.mTurn == 1) {
                turnTextView.setText(Constant.SP_VALUE_PLAY_MODE_1P);
                turnTextView.setTextColor(Color.rgb(0, 255, 160));
            } else {
                turnTextView.setText(Constant.SP_VALUE_PLAY_MODE_2P);
                turnTextView.setTextColor(Color.rgb(255, 0, 64));
            }
            if (checkEndGame()) {
                SharedPreferences sharedPreferences = this.mContext.getSharedPreferences(Constant.SP, 0);
                SharedPreferences.Editor e = sharedPreferences.edit();
                GameConstant.mWinCount = sharedPreferences.getInt(Constant.SP_KEY_WIN_COUNT, 0);
                GameConstant.mLoseCount = sharedPreferences.getInt(Constant.SP_KEY_LOSE_COUNT, 0);
                float soundVolume = activity.mSoundVolume;
                activity.mSoundPool.play(activity.mSound04, soundVolume, soundVolume, 1, 0, 1.0f);
                if (score1p > score2p) {
                    score1pTextView.append(" WIN!");
                    if (GameConstant.mPlayMode.equals(Constant.SP_VALUE_PLAY_MODE_1P)) {
                        GameConstant.mWinCount++;
                        e.putInt(Constant.SP_KEY_WIN_COUNT, GameConstant.mWinCount);
                        e.commit();
                    }
                } else if (score1p < score2p) {
                    score2pTextView.append(" WIN!");
                    if (GameConstant.mPlayMode.equals(Constant.SP_VALUE_PLAY_MODE_1P)) {
                        GameConstant.mLoseCount++;
                        e.putInt(Constant.SP_KEY_LOSE_COUNT, GameConstant.mLoseCount);
                        e.commit();
                    }
                } else {
                    score1pTextView.append(" DRAW!");
                    score2pTextView.append(" DRAW!");
                }
            }
            if (GameConstant.mPlayMode == Constant.SP_VALUE_PLAY_MODE_1P && GameConstant.mEnemyTurn == 0) {
                ArrayList<EnemyPositionEntity> enemyPositionEntityList = new ArrayList<>();
                for (int x = 0; x < 8; x++) {
                    for (int y = 0; y < 8; y++) {
                        if (Integer.parseInt(this.mImageView[x][y].getTag().toString()) == 0) {
                            enemyPositionEntityList.add(new EnemyPositionEntity(x, y));
                        }
                    }
                }
                Iterator it = enemyPositionEntityList.iterator();
                while (it.hasNext()) {
                    setEnemyPositionValue((EnemyPositionEntity) it.next());
                }
                Object[] array = enemyPositionEntityList.toArray();
                Arrays.sort(array);
                int length = array.length;
                for (int i = 0; i < length; i++) {
                    Log.d(log, "object.value = " + ((EnemyPositionEntity) array[i]).value);
                }
                Random random = new Random();
                Random random2 = new Random();
                Log.d(log, " size " + enemyPositionEntityList.size());
                if (enemyPositionEntityList.size() != 0) {
                    if (enemyPositionEntityList.size() > 32) {
                        if (random2.nextInt(100) < 20) {
                            enemyIndex = random.nextInt(7);
                        } else if (random2.nextInt(100) < 30) {
                            enemyIndex = random.nextInt(10);
                        } else if (random2.nextInt(100) < 20) {
                            enemyIndex = random.nextInt(4);
                        } else if (random2.nextInt(100) < 50) {
                            enemyIndex = random.nextInt(15);
                        } else if (random2.nextInt(100) < 30) {
                            enemyIndex = random.nextInt(6);
                        } else if (random2.nextInt(100) < 50) {
                            enemyIndex = random.nextInt(18);
                        } else if (random2.nextInt(100) < 95) {
                            enemyIndex = random.nextInt(30);
                        } else {
                            enemyIndex = random.nextInt(2);
                        }
                    } else if (enemyPositionEntityList.size() > 4) {
                        if (random2.nextInt(100) < 35) {
                            enemyIndex = random.nextInt(2);
                        } else if (random2.nextInt(100) < 45) {
                            enemyIndex = 0;
                        } else if (random2.nextInt(100) < 55) {
                            enemyIndex = random.nextInt(3);
                        } else if (random2.nextInt(100) < 70) {
                            enemyIndex = 1;
                        } else {
                            enemyIndex = random.nextInt(5);
                        }
                    } else if (enemyPositionEntityList.size() <= 2) {
                        enemyIndex = 0;
                    } else if (random2.nextInt(100) < 50) {
                        enemyIndex = 0;
                    } else if (random2.nextInt(100) < 70) {
                        enemyIndex = random.nextInt(2);
                    } else {
                        enemyIndex = random.nextInt(3);
                    }
                    EnemyPositionEntity enemyPositionEntity = (EnemyPositionEntity) array[enemyIndex];
                    Log.d(log, "enemy selected = " + enemyPositionEntity.value);
                    new EnemyTask(enemyPositionEntity.x, enemyPositionEntity.y).execute(0);
                }
            }
        }
    }

    public void setEnemyPositionValue(EnemyPositionEntity entity) {
        int preX = entity.x - 1;
        int preY = entity.y - 1;
        int postX = entity.x + 1;
        int postY = entity.y + 1;
        entity.value = 0;
        entity.value += getEnemyScore(preX, preY);
        entity.value += getEnemyScore(preX, entity.y);
        entity.value += getEnemyScore(preX, postY);
        entity.value += getEnemyScore(entity.x, preY);
        entity.value += getEnemyScore(entity.x, postY);
        entity.value += getEnemyScore(postX, preY);
        entity.value += getEnemyScore(postX, entity.y);
        entity.value += getEnemyScore(postX, postY);
    }

    public int getEnemyScore(int x, int y) {
        if (x >= 0 && y >= 0 && x <= 7 && y <= 7) {
            if (Integer.parseInt(this.mImageView[x][y].getTag().toString()) == (-GameConstant.mTurn)) {
                return 10;
            }
            if (Integer.parseInt(this.mImageView[x][y].getTag().toString()) == 0) {
                return 12;
            }
            if (Integer.parseInt(this.mImageView[x][y].getTag().toString()) == GameConstant.mTurn) {
                return 1;
            }
        }
        return 0;
    }

    private boolean checkEndGame() {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (Integer.parseInt(this.mImageView[x][y].getTag().toString()) == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void changeAround() {
        int preX = this.mX - 1;
        int preY = this.mY - 1;
        int postX = this.mX + 1;
        int postY = this.mY + 1;
        change(preX, preY);
        change(preX, this.mY);
        change(preX, postY);
        change(this.mX, preY);
        change(this.mX, postY);
        change(postX, preY);
        change(postX, this.mY);
        change(postX, postY);
    }

    public void change(int x, int y) {
        if (x >= 0 && y >= 0 && x <= 7 && y <= 7) {
            ImageView imageView = this.mImageView[x][y];
            Object tag = imageView.getTag();
            if (!tag.equals(2) && !tag.equals(-2) && !tag.equals(999)) {
                if (Integer.parseInt(tag.toString()) * -1 == GameConstant.mTurn) {
                    imageView.setTag(0);
                    imageView.setImageResource(R.drawable.ball_white_motion);
                    new ButtonScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, imageView, 400);
                    return;
                }
                Object beforeTag = imageView.getTag();
                imageView.setTag(Integer.valueOf(GameConstant.mTurn));
                if (beforeTag.equals(imageView.getTag())) {
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.2f, 1.0f, 1.2f, 1.0f, (float) (imageView.getWidth() / 2), (float) (imageView.getHeight() / 2));
                    scaleAnimation.setDuration(800);
                    imageView.startAnimation(scaleAnimation);
                } else {
                    new ButtonScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, imageView, 700);
                }
                if (GameConstant.mTurn == 1) {
                    imageView.setImageResource(BallUtility.getBallLightColorImageResource(GameConstant.m1pColor));
                } else {
                    imageView.setImageResource(BallUtility.getBallLightColorImageResource(GameConstant.m2pColor));
                }
            }
        }
    }

    public int calculateScore(int turn) {
        int point = 0;
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (Integer.parseInt(this.mImageView[x][y].getTag().toString()) == turn) {
                    point++;
                }
            }
        }
        return point;
    }

    class EnemyTask extends AsyncTask<Object, Object, Object> {
        private int enemyX;
        private int enemyY;

        public EnemyTask(int x, int y) {
            this.enemyX = x;
            this.enemyY = y;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
            Log.d(ButtonOnClickListener.log, "onPostExecute");
            GameConstant.mEnemyTurn = 1;
            ButtonOnClickListener.this.onClick(((BoardGameActivity) ButtonOnClickListener.this.mContext).mBallImageView[this.enemyX][this.enemyY]);
            GameConstant.mEnemyTurn = 0;
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... params) {
            try {
                Thread.sleep(1100);
                return null;
            } catch (InterruptedException e) {
                return null;
            }
        }
    }

    class EnemyPositionEntity implements Comparable<EnemyPositionEntity> {
        public int value = 0;
        public int x;
        public int y;

        public EnemyPositionEntity(int x2, int y2) {
            this.x = x2;
            this.y = y2;
        }

        public int compareTo(EnemyPositionEntity another) {
            Log.d(ButtonOnClickListener.log, "compareTo " + another.value);
            return another.value - this.value;
        }
    }

    public void playSound() {
        BoardGameActivity activity = (BoardGameActivity) this.mContext;
        ArrayList<EnemyPositionEntity> enemyPositionEntityList = new ArrayList<>();
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (Integer.parseInt(this.mImageView[x][y].getTag().toString()) == 0) {
                    enemyPositionEntityList.add(new EnemyPositionEntity(x, y));
                }
            }
        }
        EnemyPositionEntity entity = new EnemyPositionEntity(this.mX, this.mY);
        setEnemyPositionValue(entity);
        float soundVolume = activity.mSoundVolume;
        if (entity.value >= 80) {
            activity.mSoundPool.play(activity.mSound01, soundVolume, soundVolume, 1, 0, 1.0f);
        } else if (entity.value >= 35) {
            activity.mSoundPool.play(activity.mSound02, soundVolume, soundVolume, 1, 0, 1.0f);
        } else if (entity.value >= 10) {
            activity.mSoundPool.play(activity.mSound03, soundVolume, soundVolume, 1, 0, 1.0f);
        } else {
            activity.mSoundPool.play(activity.mSound00, soundVolume, soundVolume, 1, 0, 1.0f);
        }
    }
}
