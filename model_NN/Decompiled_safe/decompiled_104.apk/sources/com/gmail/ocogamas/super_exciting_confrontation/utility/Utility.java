package com.gmail.ocogamas.super_exciting_confrontation.utility;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import com.gmail.ocogamas.super_exciting_confrontation.constant.Constant;
import com.gmail.ocogamas.super_exciting_confrontation.constant.GameConstant;

public class Utility {
    public static void buttonOn(Button button) {
        button.setTextColor(Color.rgb(0, 100, 180));
    }

    public static void buttonOff(Button button) {
        button.setTextColor(Color.rgb(236, 236, 236));
    }

    public static void resetColorButtons(Button b1, Button b2, Button b3, Button b4) {
        buttonOff(b1);
        buttonOff(b2);
        buttonOff(b3);
        buttonOff(b4);
    }

    public static void setPlayerColor(String player, String color) {
        if (player.equals(Constant.SP_KEY_1P_COLOR)) {
            GameConstant.m1pColor = color;
        } else {
            GameConstant.m2pColor = color;
        }
    }

    public static void setupColorButtons(String player, String color, Button blueButton, Button pinkButton, Button greenButton, Button yellowButton) {
        resetColorButtons(blueButton, pinkButton, greenButton, yellowButton);
        if (color.equals(Constant.SP_VALUE_BLUE)) {
            buttonOn(blueButton);
        } else if (color.equals(Constant.SP_VALUE_PINK)) {
            buttonOn(pinkButton);
        } else if (color.equals(Constant.SP_VALUE_GREEN)) {
            buttonOn(greenButton);
        } else if (color.equals(Constant.SP_VALUE_YELLOW)) {
            buttonOn(yellowButton);
        }
        final Button button = blueButton;
        final Button button2 = pinkButton;
        final Button button3 = greenButton;
        final Button button4 = yellowButton;
        final String str = player;
        blueButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetColorButtons(button, button2, button3, button4);
                Utility.buttonOn(button);
                Utility.setPlayerColor(str, Constant.SP_VALUE_BLUE);
            }
        });
        final Button button5 = blueButton;
        final Button button6 = pinkButton;
        final Button button7 = greenButton;
        final Button button8 = yellowButton;
        final String str2 = player;
        pinkButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetColorButtons(button5, button6, button7, button8);
                Utility.buttonOn(button6);
                Utility.setPlayerColor(str2, Constant.SP_VALUE_PINK);
            }
        });
        final Button button9 = blueButton;
        final Button button10 = pinkButton;
        final Button button11 = greenButton;
        final Button button12 = yellowButton;
        final String str3 = player;
        greenButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetColorButtons(button9, button10, button11, button12);
                Utility.buttonOn(button11);
                Utility.setPlayerColor(str3, Constant.SP_VALUE_GREEN);
            }
        });
        final Button button13 = blueButton;
        final Button button14 = pinkButton;
        final Button button15 = greenButton;
        final Button button16 = yellowButton;
        final String str4 = player;
        yellowButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetColorButtons(button13, button14, button15, button16);
                Utility.buttonOn(button16);
                Utility.setPlayerColor(str4, Constant.SP_VALUE_YELLOW);
            }
        });
    }

    public static void resetBlackButton(Button black0Button, Button black1Button, Button black2Button, Button black3Button, Button black4Button, Button black5Button) {
        buttonOff(black0Button);
        buttonOff(black1Button);
        buttonOff(black2Button);
        buttonOff(black3Button);
        buttonOff(black4Button);
        buttonOff(black5Button);
    }

    public static void setupBlackButtons(String blackNumber, Button black0Button, Button black1Button, Button black2Button, Button black3Button, Button black4Button, Button black5Button) {
        buttonOff(black0Button);
        buttonOff(black1Button);
        buttonOff(black2Button);
        buttonOff(black3Button);
        buttonOff(black4Button);
        buttonOff(black5Button);
        if (blackNumber.equals(Constant.SP_VALUE_BLACK_NUMBER_0)) {
            buttonOn(black0Button);
        } else if (blackNumber.equals(Constant.SP_VALUE_BLACK_NUMBER_1)) {
            buttonOn(black1Button);
        } else if (blackNumber.equals(Constant.SP_VALUE_BLACK_NUMBER_2)) {
            buttonOn(black2Button);
        } else if (blackNumber.equals(Constant.SP_VALUE_BLACK_NUMBER_3)) {
            buttonOn(black3Button);
        } else if (blackNumber.equals(Constant.SP_VALUE_BLACK_NUMBER_4)) {
            buttonOn(black4Button);
        } else if (blackNumber.equals(Constant.SP_VALUE_BLACK_NUMBER_5)) {
            buttonOn(black5Button);
        }
        final Button button = black0Button;
        final Button button2 = black1Button;
        final Button button3 = black2Button;
        final Button button4 = black3Button;
        final Button button5 = black4Button;
        final Button button6 = black5Button;
        black0Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Utility.resetBlackButton(button, button2, button3, button4, button5, button6);
                Utility.buttonOn(button);
                GameConstant.mBlackNumber = Constant.SP_VALUE_BLACK_NUMBER_0;
            }
        });
        final Button button7 = black0Button;
        final Button button8 = black1Button;
        final Button button9 = black2Button;
        final Button button10 = black3Button;
        final Button button11 = black4Button;
        final Button button12 = black5Button;
        black1Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetBlackButton(button7, button8, button9, button10, button11, button12);
                Utility.buttonOn(button8);
                GameConstant.mBlackNumber = Constant.SP_VALUE_BLACK_NUMBER_1;
            }
        });
        final Button button13 = black0Button;
        final Button button14 = black1Button;
        final Button button15 = black2Button;
        final Button button16 = black3Button;
        final Button button17 = black4Button;
        final Button button18 = black5Button;
        black2Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetBlackButton(button13, button14, button15, button16, button17, button18);
                Utility.buttonOn(button15);
                GameConstant.mBlackNumber = Constant.SP_VALUE_BLACK_NUMBER_2;
            }
        });
        final Button button19 = black0Button;
        final Button button20 = black1Button;
        final Button button21 = black2Button;
        final Button button22 = black3Button;
        final Button button23 = black4Button;
        final Button button24 = black5Button;
        black3Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetBlackButton(button19, button20, button21, button22, button23, button24);
                Utility.buttonOn(button22);
                GameConstant.mBlackNumber = Constant.SP_VALUE_BLACK_NUMBER_3;
            }
        });
        final Button button25 = black0Button;
        final Button button26 = black1Button;
        final Button button27 = black2Button;
        final Button button28 = black3Button;
        final Button button29 = black4Button;
        final Button button30 = black5Button;
        black4Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetBlackButton(button25, button26, button27, button28, button29, button30);
                Utility.buttonOn(button29);
                GameConstant.mBlackNumber = Constant.SP_VALUE_BLACK_NUMBER_4;
            }
        });
        final Button button31 = black0Button;
        final Button button32 = black1Button;
        final Button button33 = black2Button;
        final Button button34 = black3Button;
        final Button button35 = black4Button;
        final Button button36 = black5Button;
        black5Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.resetBlackButton(button31, button32, button33, button34, button35, button36);
                Utility.buttonOn(button36);
                GameConstant.mBlackNumber = Constant.SP_VALUE_BLACK_NUMBER_5;
            }
        });
    }

    public static void setTextButton(String text, Button b1, Button b2, Button b3, Button b4) {
        b1.setText(text);
        b2.setText(text);
        b3.setText(text);
        b4.setText(text);
    }
}
