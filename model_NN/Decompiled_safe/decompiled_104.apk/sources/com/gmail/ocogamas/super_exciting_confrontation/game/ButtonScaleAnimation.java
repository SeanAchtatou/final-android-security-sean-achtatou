package com.gmail.ocogamas.super_exciting_confrontation.game;

import android.view.View;
import android.view.animation.ScaleAnimation;

public class ButtonScaleAnimation extends ScaleAnimation {
    public ButtonScaleAnimation(float fromX, float toX, float fromY, float toY, View view, int duration) {
        super(fromX, toX, fromY, toY, (float) (view.getWidth() / 2), (float) (view.getHeight() / 2));
        setDuration((long) duration);
        view.startAnimation(this);
    }
}
