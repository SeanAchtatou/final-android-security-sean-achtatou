package com.gmail.ocogamas.super_exciting_confrontation.constant;

public class Constant {
    public static final int ENEMY_TURN_OFF = 0;
    public static final int ENEMY_TURN_ON = 1;
    public static final String SP = "sharedPreferences";
    public static final String SP_KEY_1P_COLOR = "1pColor";
    public static final String SP_KEY_2P_COLOR = "2pColor";
    public static final String SP_KEY_BLACK_NUMBER = "blackNumber";
    public static final String SP_KEY_COUNT_STRING = "countString";
    public static final String SP_KEY_LOSE_COUNT = "loseCount";
    public static final String SP_KEY_PLAY_MODE = "playMode";
    public static final String SP_KEY_VOLUME = "volume";
    public static final String SP_KEY_WIN_COUNT = "winCount";
    public static final String SP_VALUE_BLACK_NUMBER_0 = "0";
    public static final String SP_VALUE_BLACK_NUMBER_1 = "1";
    public static final String SP_VALUE_BLACK_NUMBER_2 = "2";
    public static final String SP_VALUE_BLACK_NUMBER_3 = "3";
    public static final String SP_VALUE_BLACK_NUMBER_4 = "4";
    public static final String SP_VALUE_BLACK_NUMBER_5 = "5";
    public static final String SP_VALUE_BLUE = "blue";
    public static final String SP_VALUE_GREEN = "green";
    public static final String SP_VALUE_PINK = "pink";
    public static final String SP_VALUE_PLAY_MODE_1P = "1P";
    public static final String SP_VALUE_PLAY_MODE_2P = "2P";
    public static final String SP_VALUE_YELLOW = "yellow";
    public static final int TURN_1P = 1;
    public static final int TURN_2P = -1;
}
