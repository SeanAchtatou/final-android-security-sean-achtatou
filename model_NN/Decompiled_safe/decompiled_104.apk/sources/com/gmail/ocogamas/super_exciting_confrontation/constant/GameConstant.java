package com.gmail.ocogamas.super_exciting_confrontation.constant;

public class GameConstant {
    public static String m1pColor;
    public static String m2pColor;
    public static String mBlackNumber;
    public static int mEnemyTurn;
    public static int mLoseCount;
    public static String mPlayMode;
    public static int mTurn;
    public static int mWinCount;
}
