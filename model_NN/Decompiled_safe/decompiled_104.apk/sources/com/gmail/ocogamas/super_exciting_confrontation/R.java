package com.gmail.ocogamas.super_exciting_confrontation;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ball_black = 2130837504;
        public static final int ball_blue = 2130837505;
        public static final int ball_blue_light = 2130837506;
        public static final int ball_blue_motion = 2130837507;
        public static final int ball_gray = 2130837508;
        public static final int ball_gray_motion = 2130837509;
        public static final int ball_green = 2130837510;
        public static final int ball_green_light = 2130837511;
        public static final int ball_green_motion = 2130837512;
        public static final int ball_pink = 2130837513;
        public static final int ball_pink_light = 2130837514;
        public static final int ball_pink_motion = 2130837515;
        public static final int ball_white = 2130837516;
        public static final int ball_white_motion = 2130837517;
        public static final int ball_yellow = 2130837518;
        public static final int ball_yellow_light = 2130837519;
        public static final int ball_yellow_motion = 2130837520;
        public static final int bg00 = 2130837521;
        public static final int bg01 = 2130837522;
        public static final int bg02 = 2130837523;
        public static final int bg03 = 2130837524;
        public static final int bg04 = 2130837525;
        public static final int bg05 = 2130837526;
        public static final int bg06 = 2130837527;
        public static final int bg07 = 2130837528;
        public static final int icon = 2130837529;
    }

    public static final class id {
        public static final int admakerview = 2131165200;
        public static final int board_linear_layout = 2131165188;
        public static final int change_light_alpha_image_view = 2131165186;
        public static final int column_1_linear_layout = 2131165189;
        public static final int column_2_linear_layout = 2131165190;
        public static final int column_3_linear_layout = 2131165191;
        public static final int column_4_linear_layout = 2131165192;
        public static final int column_5_linear_layout = 2131165193;
        public static final int column_6_linear_layout = 2131165194;
        public static final int column_7_linear_layout = 2131165195;
        public static final int column_8_linear_layout = 2131165196;
        public static final int cover_layout = 2131165185;
        public static final int player_turn_text_view = 2131165187;
        public static final int root_layout = 2131165184;
        public static final int score_1p_text_view = 2131165197;
        public static final int score_2p_text_view = 2131165198;
        public static final int seekbar = 2131165199;
        public static final int top_1p_color_blue_button = 2131165210;
        public static final int top_1p_color_green_button = 2131165212;
        public static final int top_1p_color_pink_button = 2131165211;
        public static final int top_1p_color_yellow_button = 2131165213;
        public static final int top_1p_mode_button = 2131165202;
        public static final int top_2p_color_blue_button = 2131165214;
        public static final int top_2p_color_green_button = 2131165216;
        public static final int top_2p_color_pink_button = 2131165215;
        public static final int top_2p_color_yellow_button = 2131165217;
        public static final int top_2p_mode_button = 2131165203;
        public static final int top_black0_button = 2131165204;
        public static final int top_black1_button = 2131165205;
        public static final int top_black2_button = 2131165206;
        public static final int top_black3_button = 2131165207;
        public static final int top_black4_button = 2131165208;
        public static final int top_black5_button = 2131165209;
        public static final int top_count_text_view = 2131165201;
        public static final int top_more_app_button = 2131165219;
        public static final int top_start_button = 2131165218;
    }

    public static final class layout {
        public static final int activity_board_game = 2130903040;
        public static final int activity_top = 2130903041;
    }

    public static final class raw {
        public static final int sound00 = 2130968576;
        public static final int sound01 = 2130968577;
        public static final int sound02 = 2130968578;
        public static final int sound03 = 2130968579;
        public static final int sound04 = 2130968580;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }

    public static final class style {
        public static final int Button = 2131099648;
        public static final int ColorButton = 2131099649;
    }
}
