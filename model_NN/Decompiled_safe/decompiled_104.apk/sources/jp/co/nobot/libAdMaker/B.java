package jp.co.nobot.libAdMaker;

import com.gmail.ocogamas.super_exciting_confrontation.constant.Constant;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class B {
    public static String a(String str) throws NoSuchAlgorithmException {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("String to encript cannot be null or zero length");
        }
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.update(str.getBytes());
        return b(md.digest());
    }

    public static String a(byte[] bytes) throws NoSuchAlgorithmException {
        if (bytes != null && bytes.length != 0) {
            return b(MessageDigest.getInstance("SHA1").digest(bytes));
        }
        throw new IllegalArgumentException("bytes to encript cannot be null or zero length");
    }

    public static String a(InputStream in) throws NoSuchAlgorithmException, IOException {
        if (in == null || in.available() == 0) {
            throw new IllegalArgumentException("InputStream can't be null or zero length.");
        }
        MessageDigest digest = MessageDigest.getInstance("SHA1");
        try {
            byte[] buff = new byte[4096];
            while (true) {
                int len = in.read(buff, 0, buff.length);
                if (len < 0) {
                    break;
                }
                digest.update(buff, 0, len);
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            return b(digest.digest());
        } catch (IOException e2) {
            throw e2;
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
    }

    private static String b(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : hash) {
            String plainText = Integer.toHexString(b & 255);
            if (plainText.length() < 2) {
                plainText = Constant.SP_VALUE_BLACK_NUMBER_0 + plainText;
            }
            hexString.append(plainText);
        }
        return new String(hexString);
    }
}
