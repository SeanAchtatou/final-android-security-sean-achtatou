package net.rbgrn.android.glwallpaperservice;

import javax.microedition.khronos.opengles.GL;

/* compiled from: GLWallpaperService */
interface GLWrapper {
    GL wrap(GL gl);
}
