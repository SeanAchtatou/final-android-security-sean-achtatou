package com.oslwp.android3d;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import min3d.Shared;
import min3d.core.Scene;
import min3d.objectPrimitives.Box;
import min3d.objectPrimitives.Rectangle;
import min3d.vos.CameraVo;
import min3d.vos.Color4;
import min3d.vos.Light;
import min3d.vos.TextureVo;

public class MyScene {
    public static final String LOG_TAG = "Min3D";
    private static final int MAX_NUM = 10;
    public static float camz = 10.0f;
    public static boolean isPreview;
    private Rectangle bg;
    /* access modifiers changed from: private */
    public float cammove = 0.0f;
    /* access modifiers changed from: private */
    public float camnextloc = 0.0f;
    private int currentID;
    private Rectangle down;
    /* access modifiers changed from: private */
    public FlyBox[] flybox = new FlyBox[MAX_NUM];
    public MySceneGestureListener gesture;
    private Rectangle left;
    private Vector<String> otextures;
    /* access modifiers changed from: private */
    public Hashtable<String, TextureVo> otexturesvo;
    private Rectangle right;
    /* access modifiers changed from: private */
    public Scene scene;
    private Rectangle up;
    private Vector<String> wtextures;
    private float xLastOffset = 0.5f;
    private float yLastOffset;

    public MyScene(Scene scene2) {
        this.scene = scene2;
        this.gesture = new MySceneGestureListener(this, null);
    }

    public void initScene() {
        boolean z;
        loadTextures();
        Light light = new Light();
        light.position.setAll(0.0f, 0.0f, 10.0f);
        this.scene.lights().add(light);
        this.scene.camera().position.setAll(0.0f, 0.0f, camz);
        if (isPreview || Settings.canscroll != 1) {
            this.xLastOffset = 0.5f;
        }
        this.scene.camera().target.setAll((this.xLastOffset * ((float) (Settings.wideangle * 2))) - ((float) Settings.wideangle), 0.0f, (float) (Settings.tunneldepth * -1));
        if (Settings.objnum > MAX_NUM) {
            Settings.objnum = MAX_NUM;
        }
        for (int i = 0; i < Settings.objnum && Settings.hascube == 1; i++) {
            this.flybox[i] = new FlyBox();
            this.flybox[i].init(false);
        }
        Color4 planeColor = new Color4(255, 255, 255, 255);
        this.right = new Rectangle((float) Settings.tunneldepth, (float) Settings.tunnelwide, 2, 2, planeColor);
        this.right.rotation().y = 90.0f;
        this.right.position().x = (float) (Settings.tunnelwide / 2);
        this.right.position().z = (float) (-(Settings.tunneldepth / 2));
        this.right.lightingEnabled(false);
        setWallTexture(this.right, getWallTexture("right"), false, false);
        this.scene.addChild(this.right);
        this.left = new Rectangle((float) Settings.tunneldepth, (float) Settings.tunnelwide, 2, 2, planeColor);
        this.left.rotation().y = -90.0f;
        this.left.position().x = (float) (-(Settings.tunnelwide / 2));
        this.left.position().z = (float) (-(Settings.tunneldepth / 2));
        this.left.lightingEnabled(false);
        setWallTexture(this.left, getWallTexture("left"), false, false);
        this.scene.addChild(this.left);
        this.up = new Rectangle((float) Settings.tunneldepth, (float) Settings.tunnelwide, 2, 2, planeColor);
        this.up.rotation().x = -90.0f;
        this.up.rotation().z = 90.0f;
        this.up.position().y = (float) (Settings.tunnelwide / 2);
        this.up.position().z = (float) (-(Settings.tunneldepth / 2));
        this.up.lightingEnabled(false);
        setWallTexture(this.up, getWallTexture("up"), false, false);
        this.scene.addChild(this.up);
        this.down = new Rectangle((float) Settings.tunneldepth, (float) Settings.tunnelwide, 2, 2, planeColor);
        this.down.rotation().x = 90.0f;
        this.down.rotation().z = 90.0f;
        this.down.position().y = (float) (-(Settings.tunnelwide / 2));
        this.down.position().z = (float) (-(Settings.tunneldepth / 2));
        this.down.lightingEnabled(false);
        setWallTexture(this.down, getWallTexture("down"), false, false);
        this.scene.addChild(this.down);
        if (this.wtextures.contains(getWallTexture("bg"))) {
            this.bg = new Rectangle((float) Settings.tunnelwide, (float) Settings.tunnelwide, 2, 2, planeColor);
            this.bg.position().z = (float) (-Settings.tunneldepth);
            this.bg.lightingEnabled(false);
            this.bg.rotation().y = 180.0f;
            setWallTexture(this.bg, getWallTexture("bg"), false, false);
            this.scene.addChild(this.bg);
        }
        Color4 c = new Color4();
        c.setAll((long) Settings.bgcolor);
        this.scene.fogColor(c);
        this.scene.fogNear((float) Settings.fognear);
        this.scene.fogFar((float) Settings.fogfar);
        Scene scene2 = this.scene;
        if (Settings.fogon == 1) {
            z = true;
        } else {
            z = false;
        }
        scene2.fogEnabled(z);
        this.scene.backgroundColor().setAll((long) Settings.bgcolor);
        String s = Shared.context().getPackageName();
        int cc = 0;
        for (int i2 = 1; i2 < s.length(); i2 += 2) {
            cc += s.charAt(i2);
        }
        if (cc != Settings.getInt()) {
            this.scene.reset();
        }
    }

    public void updateScene() {
        int i = 0;
        while (i < Settings.objnum && Settings.hascube == 1) {
            try {
                this.flybox[i].fly();
                if (this.flybox[i].box.position().z > this.scene.camera().position.z) {
                    this.flybox[i].init(true);
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        if (this.cammove != 0.0f) {
            float pos = this.scene.camera().position.z;
            if (this.cammove < 0.0f) {
                if (pos <= this.camnextloc) {
                    this.scene.camera().position.z = this.camnextloc;
                    this.cammove = 0.0f;
                } else {
                    this.scene.camera().position.z += this.cammove;
                }
            } else if (pos >= this.camnextloc) {
                this.scene.camera().position.z = this.camnextloc;
                this.cammove = 0.0f;
            } else {
                this.scene.camera().position.z += this.cammove;
            }
            camz = this.scene.camera().position.z;
        }
    }

    public void updateOnOffsetsChanged(float x, float y) {
        CameraVo cam;
        try {
            if (this.scene != null && Settings.canscroll == 1 && (cam = this.scene.camera()) != null) {
                cam.target.setAll((((float) (Settings.wideangle * 2)) * x) - ((float) Settings.wideangle), 0.0f, (float) (Settings.tunneldepth * -1));
                this.xLastOffset = x;
                this.yLastOffset = y;
            }
        } catch (Exception e) {
        }
    }

    public void initTextures() {
        try {
            AssetManager asset = Shared.context().getAssets();
            this.wtextures = new Vector<>();
            this.otextures = new Vector<>();
            String[] images = asset.list("wall");
            int length = images.length;
            for (int i = 0; i < length; i++) {
                this.wtextures.add("wall/" + images[i]);
            }
            String[] images2 = asset.list("textures");
            int length2 = images2.length;
            for (int i2 = 0; i2 < length2; i2++) {
                this.otextures.add("textures/" + images2[i2]);
            }
        } catch (Exception e) {
        }
    }

    public void loadTextures() {
        initTextures();
        unloadTextures();
        int sample = 1;
        while (true) {
            try {
                loadTextures(this.wtextures, sample);
                loadTextures(this.otextures, sample);
                break;
            } catch (OutOfMemoryError e) {
                unloadTextures();
                sample *= 2;
            }
        }
        this.otexturesvo = new Hashtable<>();
        Iterator<String> it = this.otextures.iterator();
        while (it.hasNext()) {
            String t = it.next();
            TextureVo tv = new TextureVo(t);
            tv.repeatV = false;
            tv.repeatU = false;
            this.otexturesvo.put(t, tv);
        }
        this.currentID = 0;
    }

    public void loadTextures(Vector<String> tlist, int sample) {
        InputStream is = null;
        try {
            AssetManager asset = Shared.context().getAssets();
            Iterator<String> it = tlist.iterator();
            while (it.hasNext()) {
                String texture = it.next();
                if (!Shared.textureManager().contains(texture)) {
                    is = asset.open(texture);
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inSampleSize = sample;
                    opts.inScaled = false;
                    opts.inDither = false;
                    Bitmap bmp = BitmapFactory.decodeStream(is, null, opts);
                    Shared.textureManager().addTextureId(bmp, texture, false);
                    is.close();
                    bmp.recycle();
                }
            }
            try {
                is.close();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            Log.d("Min3D", "failed to load texures: " + e2.toString());
            try {
                is.close();
            } catch (Exception e3) {
            }
        } catch (Throwable th) {
            try {
                is.close();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    public void unloadTextures() {
        for (String id : Shared.textureManager().getTextureIds()) {
            Shared.textureManager().deleteTexture(id);
        }
    }

    public String getNextTexture() {
        Vector<String> vector = this.otextures;
        int i = this.currentID;
        this.currentID = i + 1;
        String id = vector.get(i);
        if (this.currentID >= this.otextures.size()) {
            this.currentID = 0;
        }
        return id;
    }

    public String getWallTexture(String side) {
        String t = "wall/" + side + ".jpg";
        if (this.wtextures.contains(t)) {
            return t;
        }
        return "wall/wall.jpg";
    }

    public void setWallTexture(Rectangle rect, String tname, boolean u, boolean v) {
        TextureVo tv = new TextureVo(tname);
        tv.repeatU = u;
        tv.repeatV = v;
        rect.textures().add(tv);
    }

    private class FlyBox {
        public Box box = new Box(this.size, this.size, this.size);
        public float rx;
        public float ry;
        public float rz;
        public float size = getRandom(Settings.objminsize, Settings.objmaxsize);
        public float speed;

        public FlyBox() {
            this.box.vertexColorsEnabled(false);
            TextureVo tv = new TextureVo(MyScene.this.getNextTexture());
            tv.repeatV = false;
            tv.repeatU = false;
            this.box.textures().add(tv);
            MyScene.this.scene.addChild(this.box);
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public void init(boolean repeat) {
            this.speed = getRandom(Settings.objminspeed, Settings.objmaxspeed);
            this.rx = getRandom(Settings.objminrotate, Settings.objmaxrotate);
            this.ry = getRandom(Settings.objminrotate, Settings.objmaxrotate);
            this.rz = getRandom(Settings.objminrotate, Settings.objmaxrotate);
            float max = (((float) Settings.tunnelwide) - (this.size * 2.0f)) / 2.0f;
            float min = -max;
            this.box.position().x = getRandom(min, max);
            this.box.position().y = getRandom(min, max);
            if (repeat) {
                this.box.position().z = (float) (-Settings.tunneldepth);
                this.box.textures().addReplace((TextureVo) MyScene.this.otexturesvo.get(MyScene.this.getNextTexture()));
                return;
            }
            this.box.position().z = getRandom((float) (-Settings.tunneldepth), 0.0f);
        }

        public void fly() {
            this.box.rotation().x += this.rx;
            this.box.rotation().y += this.ry;
            this.box.rotation().z += this.rz;
            this.box.position().z += this.speed;
        }

        public float getRandom(float min, float max) {
            return (((float) Math.random()) * (max - min)) + min;
        }
    }

    private class MySceneGestureListener extends GestureDetector.SimpleOnGestureListener {
        private MySceneGestureListener() {
        }

        /* synthetic */ MySceneGestureListener(MyScene myScene, MySceneGestureListener mySceneGestureListener) {
            this();
        }

        public boolean onDoubleTap(MotionEvent ev) {
            return true;
        }

        public boolean onSingleTapUp(MotionEvent ev) {
            return false;
        }

        public void onShowPress(MotionEvent ev) {
        }

        public void onLongPress(MotionEvent ev) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return true;
        }

        public boolean onDown(MotionEvent ev) {
            return false;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(velocityX) > Math.abs(velocityY)) {
                    if (Settings.spincube == 1) {
                        float forceX = velocityX * 0.002f;
                        for (int i = 0; i < Settings.objnum && Settings.hascube == 1; i++) {
                            MyScene.this.flybox[i].ry += forceX;
                        }
                    }
                } else if (Math.abs(velocityY) > 1000.0f && MyScene.this.cammove == 0.0f && MyScene.this.scene.camera() != null) {
                    float pos = MyScene.this.scene.camera().position.z;
                    if (velocityY > 0.0f) {
                        if (pos > ((float) (-(Settings.tunneldepth / 2)))) {
                            MyScene.this.camnextloc = (float) (-(Settings.tunneldepth / 2));
                            MyScene.this.cammove = -1.0f;
                        }
                    } else if (pos < 0.0f) {
                        MyScene.this.camnextloc = 10.0f;
                        MyScene.this.cammove = 1.0f;
                    }
                }
            } catch (Exception e) {
            }
            return true;
        }
    }
}
