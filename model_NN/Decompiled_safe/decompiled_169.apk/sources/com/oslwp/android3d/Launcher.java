package com.oslwp.android3d;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class Launcher extends Activity {
    private AdView adView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.launcher);
        ((Button) findViewById(R.id.options)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Launcher.this.startActivity(new Intent(Launcher.this, Settings.class));
            }
        });
        ((Button) findViewById(R.id.use)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Toast.makeText(Launcher.this, String.valueOf(Launcher.this.getString(R.string.useprompt)) + " " + Launcher.this.getString(R.string.app_name), 2).show();
                    Launcher.this.startActivity(new Intent("android.service.wallpaper.LIVE_WALLPAPER_CHOOSER"));
                } catch (Exception e) {
                    Launcher.this.promptNoLwpPicker();
                }
            }
        });
        ((Button) findViewById(R.id.more)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Launcher.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://android.ownskin.com/lwp_phone_moredownload.jsp?t=flybox&fr=" + Launcher.this.getPackageName())));
            }
        });
        ((TextView) findViewById(R.id.intro)).setText(Html.fromHtml(getString(R.string.intro)));
        this.adView = (AdView) findViewById(R.id.adView);
        this.adView.loadAd(new AdRequest());
    }

    /* access modifiers changed from: private */
    public void promptNoLwpPicker() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setCancelable(false).create();
        alertDialog.setMessage(getString(R.string.nolwpsupport));
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setIcon((int) R.drawable.icon);
        alertDialog.setButton(getString(17039370), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }
}
