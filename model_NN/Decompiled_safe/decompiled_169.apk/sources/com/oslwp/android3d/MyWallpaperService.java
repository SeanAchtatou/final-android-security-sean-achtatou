package com.oslwp.android3d;

import android.content.SharedPreferences;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import min3d.Shared;
import min3d.core.Renderer;
import min3d.core.Scene;
import min3d.interfaces.ISceneController;
import net.rbgrn.android.glwallpaperservice.GLWallpaperService;

public class MyWallpaperService extends GLWallpaperService implements ISceneController {
    public static boolean isPreview = false;
    public static final String preferenceName = "FLYBOX";
    /* access modifiers changed from: private */
    public GestureDetector gesture;
    private Handler initSceneHandler;
    private final Runnable initSceneRunnable = new Runnable() {
        public void run() {
            MyWallpaperService.this.onInitScene();
        }
    };
    /* access modifiers changed from: private */
    public MyScene myScene;
    /* access modifiers changed from: private */
    public SharedPreferences prefs;
    /* access modifiers changed from: private */
    public MyRenderer renderer;
    private Scene scene;
    private Handler updateSceneHandler;
    private final Runnable updateSceneRunnable = new Runnable() {
        public void run() {
            MyWallpaperService.this.onUpdateScene();
        }
    };

    public MyWallpaperService() {
        min3dSetup();
    }

    public WallpaperService.Engine onCreateEngine() {
        return new MyEngine();
    }

    public void onDestory() {
        super.onDestroy();
    }

    private class MyEngine extends GLWallpaperService.GLEngine implements SharedPreferences.OnSharedPreferenceChangeListener {
        public MyEngine() {
            super();
        }

        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);
            Settings.loadDefault();
            MyWallpaperService.this.prefs = MyWallpaperService.this.getSharedPreferences(MyWallpaperService.preferenceName, 0);
            MyWallpaperService.this.prefs.registerOnSharedPreferenceChangeListener(this);
            Settings.load(MyWallpaperService.this.prefs);
            setRenderer(MyWallpaperService.this.renderer);
            setTouchEventsEnabled(true);
            MyWallpaperService.this.gesture = new GestureDetector(MyWallpaperService.this, MyWallpaperService.this.myScene.gesture);
            MyScene.isPreview = isPreview();
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
        }

        public void onDestroy() {
            super.onDestroy();
        }

        public void onTouchEvent(MotionEvent event) {
            MyWallpaperService.this.gesture.onTouchEvent(event);
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
            MyWallpaperService.this.myScene.updateOnOffsetsChanged(xOffset, yOffset);
        }

        public void onSharedPreferenceChanged(SharedPreferences p, String key) {
            Settings.load(p);
        }
    }

    private class MyRenderer extends Renderer {
        public MyRenderer(Scene $scene) {
            super($scene);
        }

        public void onSurfaceCreated(GL10 $gl, EGLConfig eglConfig) {
            super.onSurfaceCreated($gl, eglConfig);
        }

        public void onSurfaceChanged(GL10 gl, int w, int h) {
            super.onSurfaceChanged(gl, w, h);
        }

        public void onDrawFrame(GL10 gl) {
            try {
                super.onDrawFrame(gl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void min3dSetup() {
        this.initSceneHandler = new Handler();
        this.updateSceneHandler = new Handler();
        Shared.context(this);
        this.scene = new Scene(this);
        this.renderer = new MyRenderer(this.scene);
        Shared.renderer(this.renderer);
        this.myScene = new MyScene(this.scene);
    }

    /* access modifiers changed from: private */
    public void onInitScene() {
    }

    /* access modifiers changed from: private */
    public void onUpdateScene() {
    }

    public Handler getInitSceneHandler() {
        return this.initSceneHandler;
    }

    public Runnable getInitSceneRunnable() {
        return this.initSceneRunnable;
    }

    public Handler getUpdateSceneHandler() {
        return this.updateSceneHandler;
    }

    public Runnable getUpdateSceneRunnable() {
        return this.updateSceneRunnable;
    }

    public void initScene() {
        this.myScene.initScene();
    }

    public void updateScene() {
        this.myScene.updateScene();
    }
}
