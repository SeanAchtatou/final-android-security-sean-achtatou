package min3d.vos;

public enum ShadeModel {
    SMOOTH(7425),
    FLAT(7424);
    
    private final int _glConstant;

    private ShadeModel(int $glConstant) {
        this._glConstant = $glConstant;
    }

    public int glConstant() {
        return this._glConstant;
    }
}
