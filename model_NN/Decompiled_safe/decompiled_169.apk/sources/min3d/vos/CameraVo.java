package min3d.vos;

public class CameraVo {
    public FrustumManaged frustum = new FrustumManaged(null);
    public Number3d position = new Number3d(0.0f, 0.0f, 5.0f);
    public Number3d target = new Number3d(0.0f, 0.0f, 0.0f);
    public Number3d upAxis = new Number3d(0.0f, 1.0f, 0.0f);
}
