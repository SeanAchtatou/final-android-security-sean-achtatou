package min3d.interfaces;

import android.os.Handler;

public interface ISceneController {
    Handler getInitSceneHandler();

    Runnable getInitSceneRunnable();

    Handler getUpdateSceneHandler();

    Runnable getUpdateSceneRunnable();

    void initScene();

    void updateScene();
}
