package au.com.xandar.jumblee.overview;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import au.com.xandar.android.os.DialogTaskManager;
import au.com.xandar.android.os.ExtendedRunnable;
import au.com.xandar.android.os.SimpleAsyncTask;
import au.com.xandar.android.pm.PackageManagerUtility;
import au.com.xandar.jumblee.CurrentJumble;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.db.Score;
import au.com.xandar.jumblee.dialog.AboutDialogFactory;
import au.com.xandar.jumblee.dialog.CompletedGameDialogEnabler;
import au.com.xandar.jumblee.dialog.DialogId;
import au.com.xandar.jumblee.game.GameActivity;
import au.com.xandar.jumblee.game.JumbleFactory;
import au.com.xandar.jumblee.prefs.PreferencesActivity;
import au.com.xandar.jumblee.score.ScoresActivity;
import au.com.xandar.jumblee.scoreloop.GlobalProfileActivity;
import au.com.xandar.jumblee.scoreloop.HighScoresActivity;
import au.com.xandar.jumblee.scoreloop.ScoreloopPosterService;
import java.util.Calendar;

public final class OverviewActivity extends JumbleeActivity {
    private static final String SAVED_STATE_COMPLETED_GAME_SCORE_ID = "completedGame.scoreId";
    /* access modifiers changed from: private */
    public final CompletedGameDialogEnabler completedGameDialogEnabler = new CompletedGameDialogEnabler(this);
    private TextView currentGameDetails;
    private Calendar dateStarted;
    /* access modifiers changed from: private */
    public Button resumeGameButton;
    /* access modifiers changed from: private */
    public Button startNewGameButton;
    /* access modifiers changed from: private */
    public DialogTaskManager taskManager;

    private class StartGameRunnable implements ExtendedRunnable {
        CurrentJumble newJumble;

        private StartGameRunnable() {
        }

        public void run() {
            this.newJumble = new JumbleFactory(OverviewActivity.this, OverviewActivity.this.getJumbleeDB()).getNewJumble();
            OverviewActivity.this.getJumbleeApp().setCurrentJumble(this.newJumble);
        }

        public void postExecute() {
            OverviewActivity.this.getAnalytics().sendStartGame();
            if (this.newJumble.isCurrentJumble()) {
                Intent intent = new Intent(OverviewActivity.this, GameActivity.class);
                intent.putExtra(GameActivity.KICKOFF_TYPE, GameActivity.KICKOFF_START_GAME);
                OverviewActivity.this.startActivity(intent);
                return;
            }
            String unused = OverviewActivity.this.getGameDetails(this.newJumble);
            OverviewActivity.this.showDialog(70);
            OverviewActivity.this.startNewGameButton.setEnabled(true);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.overview_screen);
        this.taskManager = (DialogTaskManager) getLastNonConfigurationInstance();
        if (this.taskManager == null) {
            this.taskManager = new DialogTaskManager(this, getExecutor());
        } else {
            this.taskManager.setActivity(this);
        }
        this.dateStarted = Calendar.getInstance();
        this.startNewGameButton = (Button) findViewById(R.id.buttonNewGame);
        this.resumeGameButton = (Button) findViewById(R.id.buttonResume);
        this.currentGameDetails = (TextView) findViewById(R.id.currentGamesDetails);
        this.resumeGameButton.setEnabled(false);
        this.startNewGameButton.setEnabled(false);
        this.startNewGameButton.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void
             arg types: [int, au.com.xandar.jumblee.overview.OverviewActivity$StartGameRunnable]
             candidates:
              au.com.xandar.android.os.DialogTaskManager.executeTask(int, java.lang.Runnable):void
              au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void */
            public void onClick(View view) {
                if (!OverviewActivity.this.showPiracy()) {
                    OverviewActivity.this.startNewGameButton.setEnabled(false);
                    final CurrentJumble currentJumble = OverviewActivity.this.getJumbleeApp().getCurrentJumble();
                    OverviewActivity.this.getAnalytics().sendGameOver(currentJumble);
                    if (currentJumble.isCurrentJumble()) {
                        OverviewActivity.this.showDialog(16);
                        new SimpleAsyncTask() {
                            Score score;

                            /* access modifiers changed from: protected */
                            public void doInBackground() {
                                this.score = OverviewActivity.this.getJumbleeDB().completeGame(currentJumble);
                            }

                            /* access modifiers changed from: protected */
                            public void onPostExecute() {
                                OverviewActivity.this.startService(new Intent(OverviewActivity.this, ScoreloopPosterService.class));
                                OverviewActivity.this.completedGameDialogEnabler.setScoreId(Long.valueOf(this.score.getId()));
                            }
                        }.execute(OverviewActivity.this.getExecutor());
                        return;
                    }
                    OverviewActivity.this.taskManager.executeTask(11, (ExtendedRunnable) new StartGameRunnable());
                }
            }
        });
        this.resumeGameButton.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void
             arg types: [int, au.com.xandar.jumblee.overview.OverviewActivity$2$1]
             candidates:
              au.com.xandar.android.os.DialogTaskManager.executeTask(int, java.lang.Runnable):void
              au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void */
            public void onClick(View view) {
                if (!OverviewActivity.this.showPiracy()) {
                    OverviewActivity.this.resumeGameButton.setEnabled(false);
                    OverviewActivity.this.taskManager.executeTask(15, (ExtendedRunnable) new ExtendedRunnable() {
                        public void run() {
                        }

                        public void postExecute() {
                            Intent intent = new Intent(OverviewActivity.this, GameActivity.class);
                            intent.putExtra(GameActivity.KICKOFF_TYPE, GameActivity.KICKOFF_RESUME_GAME);
                            OverviewActivity.this.startActivity(intent);
                        }
                    });
                }
            }
        });
        ((Button) findViewById(R.id.buttonScores)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!OverviewActivity.this.showPiracy()) {
                    OverviewActivity.this.startActivity(new Intent(OverviewActivity.this, ScoresActivity.class));
                }
            }
        });
        ((Button) findViewById(R.id.buttonGlobalProfile)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!OverviewActivity.this.showPiracy()) {
                    OverviewActivity.this.startActivity(new Intent(OverviewActivity.this, GlobalProfileActivity.class));
                    OverviewActivity.this.getApplicationPreferences().setHasShownGlobalProfile(true);
                }
            }
        });
        ((Button) findViewById(R.id.buttonGlobalScores)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!OverviewActivity.this.showPiracy()) {
                    if (OverviewActivity.this.getApplicationPreferences().getHasShownGlobalProfile()) {
                        Intent intent = new Intent(OverviewActivity.this, HighScoresActivity.class);
                        intent.setFlags(67108864);
                        OverviewActivity.this.startActivity(intent);
                        return;
                    }
                    OverviewActivity.this.startActivity(new Intent(OverviewActivity.this, GlobalProfileActivity.class));
                    OverviewActivity.this.getApplicationPreferences().setHasShownGlobalProfile(true);
                }
            }
        });
        ((Button) findViewById(R.id.buttonHowToPlay)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OverviewActivity.this.showDialog(30);
            }
        });
    }

    public Object onRetainNonConfigurationInstance() {
        return this.taskManager;
    }

    public void onRestart() {
        super.onRestart();
    }

    public void onStart() {
        super.onStart();
        getAnalytics().startTracking();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        populateViews();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getAnalytics().stopTracking();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.taskManager.clearActivity();
        super.onDestroy();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (new PackageManagerUtility(this).isMarketAvailable()) {
            menu.findItem(R.id.menu_rateMe).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_preferences:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            case R.id.menu_howToPlay:
                showDialog(30);
                break;
            case R.id.menu_rateMe:
                getApplicationPreferences().setApplicationRated(true);
                startActivity(AppConstants.MARKET_INTENT);
                break;
            case R.id.menu_licence:
                showDialog(22);
                break;
            case R.id.menu_about:
                showDialog(90);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch (id) {
            case 11:
                dialog = getDialogFactory().createUnkillableProgressDialog(R.string.progress_startingNewGame);
                break;
            case 15:
                dialog = getDialogFactory().createUnkillableProgressDialog(R.string.progress_resumingGame);
                break;
            case 16:
                dialog = getDialogFactory().createCompletedGameDialog();
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void
                     arg types: [int, au.com.xandar.jumblee.overview.OverviewActivity$StartGameRunnable]
                     candidates:
                      au.com.xandar.android.os.DialogTaskManager.executeTask(int, java.lang.Runnable):void
                      au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void */
                    public void onCancel(DialogInterface dialogInterface) {
                        OverviewActivity.this.completedGameDialogEnabler.clear();
                        OverviewActivity.this.taskManager.executeTask(11, (ExtendedRunnable) new StartGameRunnable());
                    }
                });
                break;
            case DialogId.EULA_DIALOG:
                dialog = getDialogFactory().createEula();
                break;
            case 30:
                dialog = getDialogFactory().createHowToPlayDialog(getApplicationPreferences());
                break;
            case DialogId.PIRACY_DIALOG:
                dialog = getDialogFactory().createPiracyDialog(getApplicationPreferences());
                break;
            case DialogId.NO_AVAILABLE_JUMBLES_DIALOG:
                dialog = getDialogFactory().createNoAvailableJumblesDialog();
                break;
            case DialogId.ABOUT_DIALOG:
                dialog = new AboutDialogFactory(this).create(getApplicationPreferences());
                break;
            default:
                Log.e(AppConstants.TAG, "Unknown Dialog ID : " + id);
                return super.onCreateDialog(id);
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case 16:
                getDialogFactory().populateCompletedGameDialog(dialog, getApplicationPreferences(), getJumbleeApp().getCurrentJumble(), true);
                this.completedGameDialogEnabler.setDialog(dialog);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        if (this.completedGameDialogEnabler.getScoreId() != null) {
            outState.putLong(SAVED_STATE_COMPLETED_GAME_SCORE_ID, this.completedGameDialogEnabler.getScoreId().longValue());
        }
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(SAVED_STATE_COMPLETED_GAME_SCORE_ID)) {
            this.completedGameDialogEnabler.setScoreId(Long.valueOf(savedInstanceState.getLong(SAVED_STATE_COMPLETED_GAME_SCORE_ID)));
        }
    }

    /* access modifiers changed from: private */
    public String getGameDetails(CurrentJumble jumble) {
        if (!jumble.isCurrentJumble()) {
            return "";
        }
        this.dateStarted.setTimeInMillis(jumble.getDateStartedAsMillis());
        return getString(R.string.overview_CurrentGameText, new Object[]{Integer.valueOf(jumble.getNrFoundWords()), Integer.valueOf(jumble.getNrPossibleWords()), Float.valueOf(((float) jumble.getMillisRemaining()) / 1000.0f), this.dateStarted});
    }

    private void populateViews() {
        CurrentJumble currentJumble = getJumbleeApp().getCurrentJumble();
        if (currentJumble == null) {
            this.resumeGameButton.setEnabled(false);
            this.currentGameDetails.setText("");
        } else {
            this.resumeGameButton.setEnabled(currentJumble.isCurrentJumble());
            this.currentGameDetails.setText(getGameDetails(currentJumble));
        }
        this.startNewGameButton.setEnabled(true);
    }

    /* access modifiers changed from: private */
    public boolean showPiracy() {
        if (!getJumbleeApp().isTimeToNotifyPirate()) {
            return false;
        }
        showDialog(50);
        return true;
    }
}
