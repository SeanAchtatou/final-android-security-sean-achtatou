package au.com.xandar.jumblee.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import au.com.xandar.android.pm.PackageManagerUtility;
import au.com.xandar.jumblee.CurrentJumble;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.common.EncouragementWordSelector;
import au.com.xandar.jumblee.game.Game;
import au.com.xandar.jumblee.game.IllegalStateTracker;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public final class JumbleeDialogFactory {
    /* access modifiers changed from: private */
    public final Activity activity;
    private final ProgressDialogFactory progressDialogFactory = new ProgressDialogFactory();

    public JumbleeDialogFactory(Activity activity2) {
        this.activity = activity2;
    }

    public Dialog createNoAvailableJumblesDialog() {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.no_available_jumbles_dialog_title);
        dialogFactory.setMessage((int) R.string.no_available_jumbles_dialog_message);
        dialogFactory.setPositiveButton(R.string.okButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public Dialog createCompletedGameDialog() {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.progress_completingGame_title);
        dialogFactory.setPositiveButton(R.string.progress_completingGame_positiveButton, null);
        dialogFactory.setNegativeButton(R.string.progress_completingGame_negativeButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        dialogFactory.setNeutralButton(R.string.progress_completingGame_neutralButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public void populateCompletedGameDialog(Dialog dialog, ApplicationPreferences prefs, CurrentJumble jumble, boolean startingNewGame) {
        StringBuilder dialogMessage = new StringBuilder();
        Button positiveButton = (Button) dialog.findViewById(R.id.positiveButton);
        Button neutralButton = (Button) dialog.findViewById(R.id.neutralButton);
        Button negativeButton = (Button) dialog.findViewById(R.id.negativeButton);
        positiveButton.setEnabled(false);
        neutralButton.setEnabled(false);
        negativeButton.setEnabled(false);
        positiveButton.setVisibility(8);
        neutralButton.setVisibility(8);
        negativeButton.setVisibility(8);
        if (jumble.getNrFoundWords() == 0) {
            if (startingNewGame) {
                dialogMessage.append(this.activity.getString(R.string.progress_completingGame_message_noWordsFound_startingNewGame));
            } else {
                dialogMessage.append(this.activity.getString(R.string.progress_completingGame_message_noWordsFound));
            }
            neutralButton.setVisibility(0);
        } else {
            String encouragementWord = new EncouragementWordSelector(this.activity, R.array.progress_completingGame_encouragementWords).getEncouragementWord(jumble.getScore());
            dialogMessage.append(this.activity.getString(R.string.progress_completingGame_message, new Object[]{Double.valueOf(jumble.getScore()), Integer.valueOf(jumble.getNrFoundWords()), Integer.valueOf(jumble.getNrPossibleWords()), Double.valueOf(jumble.getPercentageFound()), Double.valueOf(jumble.getWordsPerMinute()), encouragementWord}));
            if (prefs.getFacebook_postScores()) {
                dialogMessage.append(this.activity.getString(R.string.progress_completingGame_message_askPostToFB));
                positiveButton.setVisibility(0);
                negativeButton.setVisibility(0);
            } else {
                neutralButton.setVisibility(0);
            }
        }
        ((TextView) dialog.findViewById(R.id.message)).setText(dialogMessage.toString());
    }

    public void enableCompletedGameDialogButtons(Dialog dialog, View.OnClickListener postToFacebookHandler) {
        Button postToFBButton = (Button) dialog.findViewById(R.id.positiveButton);
        if (postToFBButton.getVisibility() != 8) {
            postToFBButton.setEnabled(true);
            postToFBButton.setOnClickListener(postToFacebookHandler);
        }
        Button neutralButton = (Button) dialog.findViewById(R.id.neutralButton);
        if (neutralButton.getVisibility() != 8) {
            neutralButton.setEnabled(true);
        }
        Button negativeButton = (Button) dialog.findViewById(R.id.negativeButton);
        if (negativeButton.getVisibility() != 8) {
            negativeButton.setEnabled(true);
        }
    }

    public Dialog createConfirmEndGameDialog() {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.confirmEndGame_title);
        dialogFactory.setMessage((int) R.string.confirmEndGame_message);
        dialogFactory.setPositiveButton(R.string.confirmEndGame_positiveButton, null);
        dialogFactory.setNegativeButton(R.string.confirmEndGame_negativeButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public void populateConfirmEndGameDialog(final Dialog dialog, final Game game, final IllegalStateTracker illegalStateTracker) {
        ((Button) dialog.findViewById(R.id.positiveButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                illegalStateTracker.completeGame();
                game.finishGame();
                dialog.cancel();
            }
        });
    }

    public Dialog createUnkillableProgressDialog(int messageResourceId) {
        return this.progressDialogFactory.create(this.activity, messageResourceId, false);
    }

    public Dialog createKillableProgressDialog(int messageResourceId) {
        return this.progressDialogFactory.create(this.activity, messageResourceId, true);
    }

    public Dialog createKillableErrorDialog(int messageResourceId) {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setMessage(messageResourceId);
        dialogFactory.setMessageGravity(3);
        dialogFactory.setCancelable(true);
        return dialogFactory.create();
    }

    public Dialog createHowToPlayDialog(final ApplicationPreferences prefs) {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.how_to_play_dialog_title);
        dialogFactory.setMessage((int) R.string.how_to_play_dialog_message);
        dialogFactory.setMessageGravity(3);
        dialogFactory.setPositiveButton(R.string.okButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                prefs.setShowHowToPlayOnStartup(false);
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public Dialog createEulaWithRejectButton(final ApplicationPreferences prefs) {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.eula_dialog_title);
        dialogFactory.setMessage((int) R.string.eula_dialog_message);
        dialogFactory.setPositiveButton(R.string.acceptButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                prefs.setShowEULAOnStartup(false);
                dialog.cancel();
            }
        });
        dialogFactory.setNegativeButton(R.string.rejectButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                JumbleeDialogFactory.this.activity.finish();
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public Dialog createEula() {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.eula_dialog_title);
        dialogFactory.setMessage((int) R.string.eula_dialog_message);
        dialogFactory.setPositiveButton(R.string.acceptButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public Dialog createUpdateDialog(final ApplicationPreferences preferences) {
        PackageManagerUtility packageManagerUtility = new PackageManagerUtility(this.activity);
        PackageInfo packageInfo = packageManagerUtility.getAllPackageInfo();
        String message = String.format(this.activity.getString(R.string.update_to_new_version_message), packageInfo.versionName, Integer.valueOf(packageInfo.versionCode), preferences.getLatestVersionName(), Integer.valueOf(preferences.getLatestVersionCode()), preferences.getLatestVersionDatePublished());
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.update_to_new_version_title);
        dialogFactory.setMessage(message);
        if (packageManagerUtility.isMarketAvailable()) {
            dialogFactory.setNeutralButton(R.string.update_to_new_version_neutral_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    JumbleeDialogFactory.this.activity.startActivity(AppConstants.MARKET_INTENT);
                    JumbleeDialogFactory.this.activity.finish();
                    dialog.cancel();
                }
            });
        } else {
            dialogFactory.setPositiveButton(R.string.update_to_new_version_positive_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://jumblee.xandar.com.au/Jumblee-" + preferences.getLatestVersionName() + "-" + preferences.getLatestVersionCode() + "-aligned.apk"));
                    intent.setFlags(524288);
                    JumbleeDialogFactory.this.activity.startActivity(intent);
                    JumbleeDialogFactory.this.activity.finish();
                    dialog.cancel();
                }
            });
        }
        dialogFactory.setNegativeButton(R.string.update_to_new_version_negative_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public Dialog createPiracyDialog(final ApplicationPreferences preferences) {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.piracy_dialog_title);
        dialogFactory.setMessage((int) R.string.piracy_dialog_message);
        if (new PackageManagerUtility(this.activity).isMarketAvailable()) {
            dialogFactory.setNeutralButton(R.string.piracy_dialog_neutral_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    JumbleeDialogFactory.this.activity.startActivity(AppConstants.MARKET_INTENT);
                    JumbleeDialogFactory.this.activity.finish();
                    dialog.cancel();
                }
            });
        } else {
            dialogFactory.setPositiveButton(R.string.piracy_dialog_positive_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://jumblee.xandar.com.au/Jumblee-" + preferences.getLatestVersionName() + "-" + preferences.getLatestVersionCode() + "-aligned.apk"));
                    intent.setFlags(524288);
                    JumbleeDialogFactory.this.activity.startActivity(intent);
                    JumbleeDialogFactory.this.activity.finish();
                    dialog.cancel();
                }
            });
        }
        dialogFactory.setNegativeButton(R.string.piracy_dialog_negative_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                JumbleeDialogFactory.this.activity.finish();
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }

    public Dialog createRateMeDialog(final ApplicationPreferences preferences) {
        DialogFactory dialogFactory = new DialogFactory(this.activity);
        dialogFactory.setTitle(R.string.rate_me_dialog_title);
        dialogFactory.setMessage((int) R.string.rate_me_dialog_message);
        dialogFactory.setMessageGravity(3);
        dialogFactory.setPositiveButton(R.string.rate_me_dialog_positive_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                preferences.setApplicationRated(true);
                JumbleeDialogFactory.this.activity.startActivity(AppConstants.MARKET_INTENT);
                dialog.cancel();
            }
        });
        dialogFactory.setNegativeButton(R.string.rate_me_dialog_negative_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                preferences.setDateRateMeNextRequested(System.currentTimeMillis() + AppConstants.NR_MILLIS_BEFORE_REQUESTING_NEXT_RATE_ME);
                dialog.cancel();
            }
        });
        return dialogFactory.create();
    }
}
