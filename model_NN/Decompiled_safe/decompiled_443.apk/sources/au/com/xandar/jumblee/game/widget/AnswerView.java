package au.com.xandar.jumblee.game.widget;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.TextView;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.game.Game;
import java.util.List;

public final class AnswerView extends TextView {
    private final TextViewStateRestorer restorer;

    public AnswerView(Context context) {
        this(context, null);
    }

    public AnswerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnswerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.restorer = new TextViewStateRestorer();
        setSingleLine(false);
    }

    public void populateText(Game game, boolean includeUndiscoveredWords) {
        String text;
        if (includeUndiscoveredWords) {
            text = getContext().getString(R.string.answerView_GameText_IncludingUndiscovered, Integer.valueOf(game.getNrFoundWords()), Integer.valueOf(game.getNrPossibleWords()), getWordsAsString(game.getFoundWords()), getWordsAsString(game.getUnfoundWords()));
        } else {
            text = getContext().getString(R.string.answerView_GameText, Integer.valueOf(game.getNrFoundWords()), Integer.valueOf(game.getNrPossibleWords()), getWordsAsString(game.getFoundWords()));
        }
        setText(text);
    }

    public final Parcelable onSaveInstanceState() {
        return this.restorer.saveInstanceState(this, super.onSaveInstanceState());
    }

    public final void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(this.restorer.restoreInstanceState(this, state));
    }

    private String getWordsAsString(List<String> words) {
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            if (builder.length() > 0) {
                builder.append(", ");
            }
            builder.append(word);
        }
        return builder.toString();
    }
}
