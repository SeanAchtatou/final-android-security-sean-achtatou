package au.com.xandar.jumblee.game.widget;

import android.content.Context;
import android.util.AttributeSet;

public final class LetterLatticeButton extends RestorableButton {
    private String letter;

    public LetterLatticeButton(Context context) {
        this(context, null);
    }

    public LetterLatticeButton(Context context, AttributeSet attrs) {
        this(context, attrs, 16842824);
    }

    public LetterLatticeButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setEnabled(false);
    }

    public void setLetter(String letter2) {
        this.letter = letter2.toLowerCase();
        setText(letter2.toUpperCase());
    }

    public String getLetter() {
        if (this.letter == null) {
            this.letter = getText().toString().toLowerCase();
        }
        return this.letter;
    }
}
