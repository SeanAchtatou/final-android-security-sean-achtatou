package au.com.xandar.jumblee.facebook;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import au.com.xandar.android.facebook.Facebook;
import au.com.xandar.android.facebook.FacebookAuthenticationException;
import au.com.xandar.android.facebook.FacebookException;
import au.com.xandar.android.facebook.SessionStore;
import au.com.xandar.android.facebook.Util;
import au.com.xandar.android.net.ConnectionChecker;
import au.com.xandar.jumblee.JumbleeApplication;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.common.EncouragementWordSelector;
import au.com.xandar.jumblee.db.JumbleeDB;
import au.com.xandar.jumblee.db.Score;
import au.com.xandar.jumblee.net.IntentExecutable;
import java.io.IOException;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;

public final class FacebookPoster implements IntentExecutable {
    public static final String EXECUTABLE_NAME_VALUE = "facebookPoster";
    private static final Random RANDOM = new Random();

    public void execute(Application application, Intent intent) {
        JumbleeApplication jumbleeApplication = (JumbleeApplication) application;
        JumbleeDB jumbleeDB = jumbleeApplication.getJumbleeDB();
        Facebook facebook = jumbleeApplication.getFacebook();
        ConnectionChecker connectionChecker = new ConnectionChecker(jumbleeApplication);
        Score[] arr$ = jumbleeDB.getScoresToBePostedToFacebook();
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            Score score = arr$[i$];
            if (connectionChecker.hasConnection() && facebook.isSessionValid()) {
                try {
                    postComment(jumbleeApplication, facebook, score);
                    jumbleeDB.markScoreAsPostedToFacebook(score.getId());
                } catch (PostException e) {
                } catch (FacebookAuthenticationException e2) {
                    facebook.setAccessToken(null);
                    SessionStore.save(facebook, jumbleeApplication);
                }
                i$++;
            } else {
                return;
            }
        }
    }

    private boolean postComment(Context context, Facebook facebook, Score score) throws PostException, FacebookAuthenticationException {
        String message;
        String caption = context.getString(R.string.facebook_posting_caption);
        if (score.getNrFoundWords() == 0) {
            message = context.getString(R.string.facebook_posting_message_utterFailure);
        } else {
            message = context.getString(R.string.facebook_posting_message, Double.valueOf(score.getScore()), Integer.valueOf(score.getNrFoundWords()), Integer.valueOf(score.getNrPossibleWords()), Double.valueOf(score.getPercentageFound()), Double.valueOf(score.getWordsPerMinute()), new EncouragementWordSelector(context, R.array.facebook_posting_encouragementWords).getEncouragementWord(score.getScore()));
        }
        String[] descriptions = context.getResources().getStringArray(R.array.facebook_posting_descriptions);
        String description = descriptions[RANDOM.nextInt(descriptions.length)];
        Bundle params = new Bundle();
        params.putString("format", "json");
        params.putString(Facebook.TOKEN, facebook.getAccessToken());
        params.putString("attribution", AppConstants.FACEBOOK_APP_ID_FOR_JUMBLEE);
        params.putString("message", message);
        params.putString("picture", "http://jumblee.xandar.com.au/icon_72_facebook_post.png");
        params.putString("link", AppConstants.MARKET_URL);
        params.putString("name", "Jumblee");
        params.putString("caption", caption);
        params.putString("description", description);
        try {
            JSONObject json = Util.parseJson(Util.openUrl("https://graph.facebook.com/me/feed", "POST", params));
            String postID = null;
            if (json.has("id")) {
                postID = json.get("id").toString();
            }
            if (postID != null) {
                return true;
            }
            return false;
        } catch (IOException e) {
            throw new PostException("Non-critical error posting to Facebook. Will attempt to post again.", e);
        } catch (FacebookException e2) {
            throw new PostException("Non-critical error posting to Facebook. Will attempt to post again.", e2);
        } catch (JSONException e3) {
            throw new PostException("Non-critical error posting to Facebook. Will attempt to post again.", e3);
        }
    }
}
