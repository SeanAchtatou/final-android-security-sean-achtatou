package au.com.xandar.jumblee.scoreloop.image;

public class LocalImageStorage {
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028 A[SYNTHETIC, Splitter:B:12:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038 A[SYNTHETIC, Splitter:B:21:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File putStream(java.io.InputStream r9) {
        /*
            r4 = 0
            java.lang.String r6 = "prefix"
            java.lang.String r7 = "suffix"
            java.io.File r8 = android.os.Environment.getDataDirectory()     // Catch:{ Exception -> 0x0045, all -> 0x0035 }
            java.io.File r2 = java.io.File.createTempFile(r6, r7, r8)     // Catch:{ Exception -> 0x0045, all -> 0x0035 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0045, all -> 0x0035 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0045, all -> 0x0035 }
            r1 = 1024(0x400, float:1.435E-42)
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r6]     // Catch:{ Exception -> 0x0024, all -> 0x0042 }
        L_0x0018:
            int r3 = r9.read(r0)     // Catch:{ Exception -> 0x0024, all -> 0x0042 }
            r6 = -1
            if (r3 == r6) goto L_0x002d
            r6 = 0
            r5.write(r0, r6, r3)     // Catch:{ Exception -> 0x0024, all -> 0x0042 }
            goto L_0x0018
        L_0x0024:
            r6 = move-exception
            r4 = r5
        L_0x0026:
            if (r4 == 0) goto L_0x002b
            r4.close()     // Catch:{ IOException -> 0x003e }
        L_0x002b:
            r6 = 0
        L_0x002c:
            return r6
        L_0x002d:
            if (r5 == 0) goto L_0x0032
            r5.close()     // Catch:{ IOException -> 0x003c }
        L_0x0032:
            r4 = r5
            r6 = r2
            goto L_0x002c
        L_0x0035:
            r6 = move-exception
        L_0x0036:
            if (r4 == 0) goto L_0x003b
            r4.close()     // Catch:{ IOException -> 0x0040 }
        L_0x003b:
            throw r6
        L_0x003c:
            r6 = move-exception
            goto L_0x0032
        L_0x003e:
            r6 = move-exception
            goto L_0x002b
        L_0x0040:
            r7 = move-exception
            goto L_0x003b
        L_0x0042:
            r6 = move-exception
            r4 = r5
            goto L_0x0036
        L_0x0045:
            r6 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: au.com.xandar.jumblee.scoreloop.image.LocalImageStorage.putStream(java.io.InputStream):java.io.File");
    }
}
