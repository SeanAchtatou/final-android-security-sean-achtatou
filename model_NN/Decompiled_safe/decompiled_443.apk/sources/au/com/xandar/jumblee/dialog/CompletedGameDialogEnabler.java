package au.com.xandar.jumblee.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.facebook.FacebookPoster;
import au.com.xandar.jumblee.net.GeneralIntentService;

public final class CompletedGameDialogEnabler {
    /* access modifiers changed from: private */
    public final JumbleeActivity activity;
    /* access modifiers changed from: private */
    public Dialog dialog;
    /* access modifiers changed from: private */
    public Long scoreId;

    public CompletedGameDialogEnabler(JumbleeActivity activity2) {
        this.activity = activity2;
    }

    public synchronized void clear() {
        this.scoreId = null;
    }

    public Long getScoreId() {
        return this.scoreId;
    }

    public synchronized void setScoreId(Long scoreId2) {
        this.scoreId = scoreId2;
        if (this.dialog != null) {
            this.activity.getDialogFactory().enableCompletedGameDialogButtons(this.dialog, getPostToFacebookHandler());
        }
    }

    public synchronized void setDialog(Dialog dialog2) {
        this.dialog = dialog2;
        if (this.scoreId != null) {
            this.activity.getDialogFactory().enableCompletedGameDialogButtons(dialog2, getPostToFacebookHandler());
        }
    }

    private View.OnClickListener getPostToFacebookHandler() {
        return new View.OnClickListener() {
            public void onClick(View view) {
                CompletedGameDialogEnabler.this.activity.getJumbleeDB().updateScoreToPostToFacebook(CompletedGameDialogEnabler.this.scoreId.longValue());
                Intent intent = new Intent(CompletedGameDialogEnabler.this.activity, GeneralIntentService.class);
                intent.putExtra(GeneralIntentService.EXECUTABLE_NAME_KEY, FacebookPoster.EXECUTABLE_NAME_VALUE);
                CompletedGameDialogEnabler.this.activity.startService(intent);
                CompletedGameDialogEnabler.this.activity.getAnalytics().sendPostToFacebook();
                CompletedGameDialogEnabler.this.dialog.cancel();
            }
        };
    }
}
