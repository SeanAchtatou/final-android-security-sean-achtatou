package au.com.xandar.jumblee.scoreloop.image;

import android.content.Context;
import android.net.ConnectivityManager;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public final class ImageDownloadGate {
    private final ConnectivityManager connManager;
    private final ApplicationPreferences prefs;

    public ImageDownloadGate(Context context, ApplicationPreferences prefs2) {
        this.prefs = prefs2;
        this.connManager = (ConnectivityManager) context.getSystemService("connectivity");
    }

    public boolean shouldDownload() {
        int pref = this.prefs.getDownloadGlobalScoreImages();
        if (pref == ApplicationPreferences.DownloadScoreloopImages.ALWAYS.ordinal()) {
            return true;
        }
        if (pref == ApplicationPreferences.DownloadScoreloopImages.NEVER.ordinal()) {
            return false;
        }
        return this.connManager.getNetworkInfo(1).isConnected();
    }
}
