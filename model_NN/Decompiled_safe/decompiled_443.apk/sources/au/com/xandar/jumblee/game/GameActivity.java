package au.com.xandar.jumblee.game;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import au.com.xandar.android.os.DialogTaskManager;
import au.com.xandar.android.os.ExtendedRunnable;
import au.com.xandar.android.os.SimpleAsyncTask;
import au.com.xandar.android.pm.PackageManagerUtility;
import au.com.xandar.jumblee.CurrentJumble;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.common.JumbleeHandledException;
import au.com.xandar.jumblee.db.Score;
import au.com.xandar.jumblee.dialog.AboutDialogFactory;
import au.com.xandar.jumblee.dialog.CompletedGameDialogEnabler;
import au.com.xandar.jumblee.dialog.DialogId;
import au.com.xandar.jumblee.game.widget.AnswerView;
import au.com.xandar.jumblee.game.widget.CurrentWordView;
import au.com.xandar.jumblee.game.widget.GameClockView;
import au.com.xandar.jumblee.game.widget.GameScoreView;
import au.com.xandar.jumblee.game.widget.LetterLattice;
import au.com.xandar.jumblee.game.widget.LetterLatticeButton;
import au.com.xandar.jumblee.prefs.PreferencesActivity;
import au.com.xandar.jumblee.scoreloop.ScoreloopPosterService;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import org.acra.ACRA;

public final class GameActivity extends JumbleeActivity {
    public static final String KICKOFF_RESUME_GAME = "kickoff.resumeGame";
    public static final String KICKOFF_START_GAME = "kickoff.StartGame";
    public static final String KICKOFF_TYPE = "kickoff";
    private static final String SAVED_STATE_COMPLETED_GAME_SCORE_ID = "completedGame.scoreId";
    private static final String SAVED_STATE_CURRENT_WORD = "game.currentWord";
    private static final String SAVED_STATE_ILLEGAL_STATE_TRACKER = "game.illegalStateTracker";
    private static final String SAVED_STATE_SPECIAL_LETTER_SELECTED = "game.specialLetterSelected";
    private AdView adView;
    /* access modifiers changed from: private */
    public AnswerToastDisplayer answerToastDisplayer;
    private AnswerView answerView;
    /* access modifiers changed from: private */
    public Button checkWordButton;
    /* access modifiers changed from: private */
    public Button clearWordButton;
    /* access modifiers changed from: private */
    public GameClockView clockView;
    /* access modifiers changed from: private */
    public final CompletedGameDialogEnabler completedGameDialogEnabler = new CompletedGameDialogEnabler(this);
    /* access modifiers changed from: private */
    public CurrentWordView currentWordView;
    /* access modifiers changed from: private */
    public Button finishGameButton;
    /* access modifiers changed from: private */
    public Game game;
    private GameListener gameListener = new GameListener() {
        public void onGameEvent(Game game, int gameEvent, Object eventData) {
            switch (gameEvent) {
                case 2:
                    GameActivity.this.updateResultViews();
                    GameActivity.this.resetCoreViews(true);
                    GameActivity.this.setCoreViewState(true);
                    break;
                case 3:
                    GameActivity.this.updateResultViews();
                    GameActivity.this.resetCoreViews(false);
                    GameActivity.this.setCoreViewState(false);
                    break;
                case 4:
                    GameActivity.this.currentWordView.populateText(game);
                    GameActivity.this.clearWordButton.setEnabled(true);
                    GameActivity.this.checkWordButton.setEnabled(game.doesWordHavePotential());
                    break;
                case 5:
                case 6:
                    GameActivity.this.updateResultViews();
                case 7:
                case 8:
                case 9:
                    GameActivity.this.resetCoreViews(true);
                    break;
                case 10:
                    GameActivity.this.uiHandler.sendMessage(GameActivity.this.uiHandler.obtainMessage(gameEvent, eventData));
                    break;
                case 11:
                    GameActivity.this.illegalStateTracker.gameTimedOut();
                    GameActivity.this.uiHandler.sendMessage(GameActivity.this.uiHandler.obtainMessage(gameEvent, eventData));
                    break;
            }
            switch (gameEvent) {
                case 3:
                    GameActivity.this.getAnalytics().sendGameOver(game.getJumble());
                    GameActivity.this.soundManager.playGameOver();
                    final CurrentJumble jumble = game.getJumble();
                    GameActivity.this.showDialog(16);
                    new SimpleAsyncTask() {
                        Score score;

                        /* access modifiers changed from: protected */
                        public void doInBackground() {
                            this.score = GameActivity.this.getJumbleeDB().completeGame(jumble);
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute() {
                            GameActivity.this.startService(new Intent(GameActivity.this, ScoreloopPosterService.class));
                            GameActivity.this.completedGameDialogEnabler.setScoreId(Long.valueOf(this.score.getId()));
                        }
                    }.execute(GameActivity.this.getExecutor());
                    return;
                case 4:
                default:
                    return;
                case 5:
                    GameActivity.this.answerToastDisplayer.display(gameEvent, eventData);
                    GameActivity.this.soundManager.playCheckWordSuccess();
                    return;
                case 6:
                    GameActivity.this.answerToastDisplayer.display(gameEvent, eventData);
                    GameActivity.this.soundManager.playCheckWordSuccessNineLetter();
                    return;
                case 7:
                    GameActivity.this.getAnalytics().sendWordRejected((String) eventData);
                    GameActivity.this.answerToastDisplayer.display(gameEvent, eventData);
                    GameActivity.this.soundManager.playCheckWordFailure();
                    return;
                case 8:
                    GameActivity.this.answerToastDisplayer.display(gameEvent, eventData);
                    GameActivity.this.soundManager.playCheckWordFailureAlreadyFound();
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public IllegalStateTracker illegalStateTracker;
    /* access modifiers changed from: private */
    public JumbleFactory jumbleFactory;
    private LetterLattice letterLattice;
    /* access modifiers changed from: private */
    public GameScoreView scoreView;
    /* access modifiers changed from: private */
    public SoundManager soundManager;
    /* access modifiers changed from: private */
    public Button startGameButton;
    /* access modifiers changed from: private */
    public DialogTaskManager taskManager;
    /* access modifiers changed from: private */
    public Handler uiHandler = new Handler() {
        public void handleMessage(Message message) {
            int gameEvent = message.what;
            Object eventData = message.obj;
            switch (gameEvent) {
                case 10:
                    GameActivity.this.clockView.setTimeRemaining(((Integer) eventData).intValue());
                    GameActivity.this.scoreView.setScore(GameActivity.this.game);
                    return;
                case 11:
                    GameActivity.this.clockView.setTimeRemaining(0);
                    GameActivity.this.scoreView.setScore(GameActivity.this.game);
                    GameActivity.this.game.finishGame();
                    return;
                default:
                    return;
            }
        }
    };

    private static class LastNonConfigInstance {
        /* access modifiers changed from: private */
        public final Game game;
        /* access modifiers changed from: private */
        public final IllegalStateTracker illegalStateTracker;
        /* access modifiers changed from: private */
        public final SoundManager soundManager;
        /* access modifiers changed from: private */
        public final DialogTaskManager taskManager;

        private LastNonConfigInstance(Game game2, SoundManager soundManager2, DialogTaskManager taskManager2, IllegalStateTracker illegalStateTracker2) {
            this.game = game2;
            this.soundManager = soundManager2;
            this.taskManager = taskManager2;
            this.illegalStateTracker = illegalStateTracker2;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game_screen);
        final PreferencedVibrator vibrator = new PreferencedVibrator((Vibrator) getSystemService("vibrator"), getApplicationPreferences());
        this.jumbleFactory = new JumbleFactory(this, getJumbleeDB());
        this.answerView = (AnswerView) findViewById(R.id.answerView);
        this.letterLattice = (LetterLattice) findViewById(R.id.letterLattice);
        this.startGameButton = (Button) findViewById(R.id.startGameButton);
        this.finishGameButton = (Button) findViewById(R.id.finishGameButton);
        this.checkWordButton = (Button) findViewById(R.id.checkWordButton);
        this.clearWordButton = (Button) findViewById(R.id.clearWordButton);
        this.currentWordView = (CurrentWordView) findViewById(R.id.currentWordView);
        this.clockView = (GameClockView) findViewById(R.id.clockView);
        this.scoreView = (GameScoreView) findViewById(R.id.scoreView);
        this.answerToastDisplayer = new AnswerToastDisplayer(this, getApplicationPreferences());
        this.startGameButton.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void
             arg types: [int, au.com.xandar.jumblee.game.GameActivity$3$1]
             candidates:
              au.com.xandar.android.os.DialogTaskManager.executeTask(int, java.lang.Runnable):void
              au.com.xandar.android.os.DialogTaskManager.executeTask(int, au.com.xandar.android.os.ExtendedRunnable):void */
            public void onClick(View view) {
                vibrator.vibrate(20);
                GameActivity.this.startGameButton.setEnabled(false);
                GameActivity.this.illegalStateTracker.startGame();
                GameActivity.this.taskManager.executeTask(11, (ExtendedRunnable) new ExtendedRunnable() {
                    CurrentJumble newJumble;

                    public void run() {
                        this.newJumble = GameActivity.this.jumbleFactory.getNewJumble();
                        if (this.newJumble.isCurrentJumble()) {
                            GameActivity.this.getJumbleeApp().setCurrentJumble(this.newJumble);
                        }
                    }

                    public void postExecute() {
                        GameActivity.this.getAnalytics().sendStartGame();
                        if (this.newJumble.isCurrentJumble()) {
                            GameActivity.this.game.setJumble(this.newJumble, "", false);
                            GameActivity.this.game.startGame();
                            return;
                        }
                        GameActivity.this.showDialog(70);
                        GameActivity.this.startGameButton.setEnabled(true);
                    }
                });
            }
        });
        this.finishGameButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(20);
                GameActivity.this.finishGameButton.setEnabled(false);
                if (GameActivity.this.getApplicationPreferences().getShowConfirmationDialogOnEndGame()) {
                    GameActivity.this.game.pauseGame();
                    GameActivity.this.showDialog(17);
                    return;
                }
                GameActivity.this.illegalStateTracker.completeGame();
                GameActivity.this.game.finishGame();
            }
        });
        this.checkWordButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (GameActivity.this.checkWordButton.isEnabled()) {
                    GameActivity.this.checkWordButton.setEnabled(false);
                    vibrator.vibrate(20);
                    GameActivity.this.illegalStateTracker.checkWord();
                    GameActivity.this.game.checkWord(GameActivity.this.illegalStateTracker);
                }
            }
        });
        this.clearWordButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                vibrator.vibrate(20);
                GameActivity.this.illegalStateTracker.clearWord();
                GameActivity.this.game.clearWord();
            }
        });
        this.letterLattice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean specialLetter;
                if (view instanceof Button) {
                    vibrator.vibrate(20);
                    view.setEnabled(false);
                    String letter = ((LetterLatticeButton) view).getLetter();
                    if (view.getId() == R.id.middleMiddle) {
                        specialLetter = true;
                    } else {
                        specialLetter = false;
                    }
                    GameActivity.this.illegalStateTracker.addLetter(specialLetter);
                    GameActivity.this.game.selectLetter(letter, specialLetter);
                }
            }
        });
        LastNonConfigInstance configInstance = (LastNonConfigInstance) getLastNonConfigurationInstance();
        if (configInstance == null) {
            configureFreshActivity(savedInstanceState);
        } else {
            this.game = configInstance.game;
            this.game.setGameListener(this.gameListener);
            this.soundManager = configInstance.soundManager;
            this.taskManager = configInstance.taskManager;
            this.illegalStateTracker = configInstance.illegalStateTracker;
            this.illegalStateTracker.rotated();
            this.taskManager.setActivity(this);
        }
        configureAdView((ViewGroup) findViewById(R.id.adBanner));
    }

    public Object onRetainNonConfigurationInstance() {
        this.game.removeListeners();
        return new LastNonConfigInstance(this.game, this.soundManager, this.taskManager, this.illegalStateTracker);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (new PackageManagerUtility(this).isMarketAvailable()) {
            menu.findItem(R.id.menu_rateMe).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_preferences:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            case R.id.menu_howToPlay:
                this.game.pauseGame();
                showDialog(30);
                break;
            case R.id.menu_rateMe:
                startActivity(AppConstants.MARKET_INTENT);
                break;
            case R.id.menu_licence:
                this.game.pauseGame();
                showDialog(22);
                break;
            case R.id.menu_about:
                this.game.pauseGame();
                showDialog(90);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public Context getApplicationContext() {
        return new ContextWrapper(super.getApplicationContext()) {
            public void startActivity(Intent intent) {
                try {
                    super.startActivity(intent);
                } catch (AndroidRuntimeException e) {
                    ACRA.getErrorReporter().handleSilentException(new JumbleeHandledException("Application could not start Activity with Intent:" + ("Intent{action=" + intent.getAction() + " uri=@" + intent.getData() + " flags=" + intent.getFlags() + "}"), e));
                }
            }
        };
    }

    public void startActivity(Intent intent) {
        try {
            super.startActivity(intent);
        } catch (AndroidRuntimeException e) {
            ACRA.getErrorReporter().handleSilentException(new JumbleeHandledException("GameActivity could not start Activity with Intent:" + ("Intent{action=" + intent.getAction() + " uri=@" + intent.getData() + " flags=" + intent.getFlags() + "}"), e));
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        getAnalytics().startTracking();
        startLoadingAds();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.game.resumeGame();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.game.pauseGame();
        boolean noConfigChanges = getChangingConfigurations() == 0;
        if (isFinishing() || noConfigChanges) {
            final CurrentJumble jumbleToUpdate = this.game.getJumble();
            this.taskManager.executeTask(13, new Runnable() {
                public void run() {
                    if (jumbleToUpdate.isCurrentJumble()) {
                        GameActivity.this.getJumbleeDB().updateJumble(jumbleToUpdate);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getAnalytics().stopTracking();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.taskManager.clearActivity();
        if (this.adView != null) {
            this.adView.stopLoading();
            this.adView.destroy();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch (id) {
            case 11:
                dialog = getDialogFactory().createUnkillableProgressDialog(R.string.progress_startingNewGame);
                break;
            case 13:
                dialog = getDialogFactory().createUnkillableProgressDialog(R.string.progress_savingGame);
                break;
            case 16:
                dialog = getDialogFactory().createCompletedGameDialog();
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        GameActivity.this.completedGameDialogEnabler.clear();
                    }
                });
                break;
            case DialogId.CONFIRM_END_GAME_DIALOG:
                dialog = getDialogFactory().createConfirmEndGameDialog();
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        GameActivity.this.finishGameButton.setEnabled(true);
                        GameActivity.this.game.resumeGame();
                    }
                });
                break;
            case DialogId.EULA_DIALOG:
                dialog = getDialogFactory().createEula();
                break;
            case 30:
                dialog = getDialogFactory().createHowToPlayDialog(getApplicationPreferences());
                break;
            case DialogId.NO_AVAILABLE_JUMBLES_DIALOG:
                dialog = getDialogFactory().createNoAvailableJumblesDialog();
                break;
            case DialogId.ABOUT_DIALOG:
                dialog = new AboutDialogFactory(this).create(getApplicationPreferences());
                break;
            default:
                Log.e(AppConstants.TAG, "Unknown Dialog ID : " + id);
                return super.onCreateDialog(id);
        }
        switch (id) {
            case DialogId.EULA_DIALOG:
            case 30:
            case DialogId.ABOUT_DIALOG:
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        GameActivity.this.game.resumeGame();
                    }
                });
                break;
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case 16:
                getDialogFactory().populateCompletedGameDialog(dialog, getApplicationPreferences(), this.game.getJumble(), false);
                this.completedGameDialogEnabler.setDialog(dialog);
                return;
            case DialogId.CONFIRM_END_GAME_DIALOG:
                getDialogFactory().populateConfirmEndGameDialog(dialog, this.game, this.illegalStateTracker);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(SAVED_STATE_CURRENT_WORD, this.game.getCurrentWord());
        outState.putBoolean(SAVED_STATE_SPECIAL_LETTER_SELECTED, this.game.isSpecialLetterSelected());
        outState.putStringArrayList(SAVED_STATE_ILLEGAL_STATE_TRACKER, this.illegalStateTracker.getEvents());
        if (this.completedGameDialogEnabler.getScoreId() != null) {
            outState.putLong(SAVED_STATE_COMPLETED_GAME_SCORE_ID, this.completedGameDialogEnabler.getScoreId().longValue());
        }
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(SAVED_STATE_COMPLETED_GAME_SCORE_ID)) {
            this.completedGameDialogEnabler.setScoreId(Long.valueOf(savedInstanceState.getLong(SAVED_STATE_COMPLETED_GAME_SCORE_ID)));
        }
    }

    private void configureFreshActivity(Bundle savedInstanceState) {
        this.soundManager = new SoundManager();
        this.taskManager = new DialogTaskManager(this, getExecutor());
        getExecutor().execute(new Runnable() {
            public void run() {
                GameActivity.this.soundManager.initSounds(GameActivity.this.getApplicationContext(), GameActivity.this.getApplicationPreferences());
            }
        });
        this.game = new Game();
        this.game.setGameListener(this.gameListener);
        CurrentJumble currentJumble = getJumbleeApp().getCurrentJumble();
        if (savedInstanceState == null) {
            this.illegalStateTracker = new IllegalStateTracker();
            String kickOffType = getIntent().getStringExtra(KICKOFF_TYPE);
            if (KICKOFF_START_GAME.equals(kickOffType)) {
                this.illegalStateTracker.startGame();
            } else if (KICKOFF_RESUME_GAME.equals(kickOffType)) {
                this.illegalStateTracker.resumeGame();
            } else {
                this.illegalStateTracker.unknownStart();
            }
            this.game.setJumble(currentJumble, "", false);
        } else {
            this.illegalStateTracker = new IllegalStateTracker(savedInstanceState.getStringArrayList(SAVED_STATE_ILLEGAL_STATE_TRACKER));
            this.illegalStateTracker.recoveredFromStorage();
            this.game.setJumble(currentJumble, savedInstanceState.getString(SAVED_STATE_CURRENT_WORD), savedInstanceState.getBoolean(SAVED_STATE_SPECIAL_LETTER_SELECTED));
        }
        this.game.startGame();
        this.answerView.populateText(this.game, this.game.isGameOver());
        this.currentWordView.populateText(this.game);
        this.letterLattice.populateLattice(this.game);
    }

    private void configureAdView(ViewGroup adLayoutContainer) {
        this.adView = new AdView(this, AdSize.BANNER, AppConstants.ADMOB_APPLICATION_ID) {
            public void stopLoading() {
                try {
                    if (isRefreshing()) {
                        super.stopLoading();
                    }
                } catch (NullPointerException e) {
                    ACRA.getErrorReporter().handleSilentException(new JumbleeHandledException("AdView cannot #stopLoading", e));
                }
            }
        };
        adLayoutContainer.addView(this.adView);
        this.adView.setAdListener(new AdListener() {
            public void onReceiveAd(Ad ad) {
                GameActivity.this.getAnalytics().sendAdRequested();
                GameActivity.this.getAnalytics().sendAdImpressionReceived();
            }

            public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
                GameActivity.this.getAnalytics().sendAdRequested();
            }

            public void onPresentScreen(Ad ad) {
                GameActivity.this.getAnalytics().sendAdClicked();
            }

            public void onDismissScreen(Ad ad) {
            }

            public void onLeaveApplication(Ad ad) {
            }
        });
    }

    private void startLoadingAds() {
        AdRequest adRequest = new AdRequest();
        adRequest.setKeywords(AppConstants.AD_KEY_WORDS);
        this.adView.loadAd(adRequest);
    }

    /* access modifiers changed from: private */
    public void setCoreViewState(boolean startGame) {
        boolean z;
        int i;
        int i2;
        if (startGame) {
            this.letterLattice.populateLattice(this.game);
        }
        Button button = this.startGameButton;
        if (!startGame) {
            z = true;
        } else {
            z = false;
        }
        button.setEnabled(z);
        Button button2 = this.startGameButton;
        if (startGame) {
            i = 4;
        } else {
            i = 0;
        }
        button2.setVisibility(i);
        this.finishGameButton.setEnabled(startGame);
        Button button3 = this.finishGameButton;
        if (startGame) {
            i2 = 0;
        } else {
            i2 = 4;
        }
        button3.setVisibility(i2);
    }

    /* access modifiers changed from: private */
    public void resetCoreViews(boolean enableLetterButtons) {
        this.currentWordView.populateText(this.game);
        this.checkWordButton.setEnabled(false);
        this.clearWordButton.setEnabled(false);
        this.letterLattice.enableLetters(enableLetterButtons);
    }

    /* access modifiers changed from: private */
    public void updateResultViews() {
        this.answerView.populateText(this.game, this.game.isGameOver());
        this.scoreView.setScore(this.game);
    }
}
