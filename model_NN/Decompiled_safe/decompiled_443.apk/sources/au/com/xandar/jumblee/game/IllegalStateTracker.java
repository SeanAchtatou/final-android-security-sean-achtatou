package au.com.xandar.jumblee.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class IllegalStateTracker {
    private static final int MAX_SIZE = 20;
    private final LinkedList<String> events = new LinkedList<>();

    public IllegalStateTracker() {
    }

    public IllegalStateTracker(List<String> initialEvents) {
        this.events.addAll(initialEvents);
    }

    public void startGame() {
        addEvent("userStartedGame");
    }

    public void resumeGame() {
        addEvent("userResumedGame");
    }

    public void unknownStart() {
        addEvent("unknownStart");
    }

    public void completeGame() {
        addEvent("userCompletedGame");
    }

    public void gameTimedOut() {
        addEvent("gameTimeOut");
    }

    public void checkWord() {
        addEvent("userCheckedWord");
    }

    public void clearWord() {
        addEvent("userClearedWord");
    }

    public void addLetter(boolean centralLetter) {
        addEvent("addLetter-" + (centralLetter ? "special" : "std"));
    }

    public void rotated() {
        addEvent("rotated");
    }

    public void recoveredFromStorage() {
        addEvent("recoveredFromStorage");
    }

    public synchronized String getEventsString() {
        StringBuilder sb;
        sb = new StringBuilder();
        Iterator i$ = this.events.iterator();
        while (i$.hasNext()) {
            sb.append(i$.next());
            sb.append(',');
        }
        return sb.toString();
    }

    public synchronized ArrayList<String> getEvents() {
        ArrayList<String> copy;
        copy = new ArrayList<>();
        copy.addAll(this.events);
        return copy;
    }

    private synchronized void addEvent(String event) {
        if (this.events.size() > MAX_SIZE) {
            this.events.removeFirst();
        }
        this.events.add(System.currentTimeMillis() + "-" + event);
    }
}
