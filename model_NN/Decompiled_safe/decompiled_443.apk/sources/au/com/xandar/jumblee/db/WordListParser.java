package au.com.xandar.jumblee.db;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

final class WordListParser {
    WordListParser() {
    }

    public List<String> getListOfString(String dbFieldValue) {
        List<String> words = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(dbFieldValue, ",");
        while (tokenizer.hasMoreElements()) {
            words.add(tokenizer.nextToken());
        }
        return words;
    }

    public String getCommaSeparatedString(List<String> words) {
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            builder.append(word);
            builder.append(",");
        }
        return builder.toString();
    }
}
