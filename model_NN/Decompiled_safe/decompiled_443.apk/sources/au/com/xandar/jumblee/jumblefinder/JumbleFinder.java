package au.com.xandar.jumblee.jumblefinder;

import java.io.InputStream;

public final class JumbleFinder {
    public void findWordsForJumbles(InputStream dictionaryResource, MutableJumble jumble) {
        StringBuilder word = new StringBuilder();
        int count = 0;
        DictionaryReader reader = new DictionaryReader(0, dictionaryResource);
        while (reader.readWord(word)) {
            if (count % 10000 == 0) {
            }
            count++;
            if (jumble.couldMakeWord(word)) {
                jumble.addPossibleWord(word);
            }
        }
        reader.close();
    }
}
