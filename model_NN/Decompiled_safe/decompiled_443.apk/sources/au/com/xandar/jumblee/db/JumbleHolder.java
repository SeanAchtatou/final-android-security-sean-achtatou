package au.com.xandar.jumblee.db;

import au.com.xandar.jumblee.CurrentJumble;

public final class JumbleHolder {
    private final CurrentJumble jumble;
    private final int nrJumbles;

    public JumbleHolder(int nrJumbles2, CurrentJumble jumble2) {
        this.nrJumbles = nrJumbles2;
        this.jumble = jumble2;
    }

    public int getNrJumbles() {
        return this.nrJumbles;
    }

    public CurrentJumble getJumble() {
        return this.jumble;
    }

    public String toString() {
        return "JumbleHolder{nrJumbles=" + this.nrJumbles + ", jumble=" + this.jumble + '}';
    }
}
