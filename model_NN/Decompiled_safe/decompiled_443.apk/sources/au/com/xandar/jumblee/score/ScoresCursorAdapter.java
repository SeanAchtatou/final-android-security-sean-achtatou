package au.com.xandar.jumblee.score;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.db.ScoresCursor;

public final class ScoresCursorAdapter extends CursorAdapter {
    private final LayoutInflater inflater;

    private static final class ScoreInfo {
        /* access modifiers changed from: private */
        public TextView date;
        /* access modifiers changed from: private */
        public TextView nrFound;
        /* access modifiers changed from: private */
        public TextView percentageFound;
        /* access modifiers changed from: private */
        public TextView rate;
        /* access modifiers changed from: private */
        public TextView score;
        /* access modifiers changed from: private */
        public TextView time;

        private ScoreInfo() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.app.Activity, au.com.xandar.jumblee.db.ScoresCursor, int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    public ScoresCursorAdapter(Activity context, ScoresCursor cursor) {
        super((Context) context, (Cursor) cursor, true);
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = this.inflater.inflate((int) R.layout.score_row, parent, false);
        ScoreInfo info = new ScoreInfo();
        TextView unused = info.time = (TextView) view.findViewById(R.id.score_time);
        TextView unused2 = info.date = (TextView) view.findViewById(R.id.score_date);
        TextView unused3 = info.nrFound = (TextView) view.findViewById(R.id.score_nrFound);
        TextView unused4 = info.percentageFound = (TextView) view.findViewById(R.id.score_percentageFound);
        TextView unused5 = info.rate = (TextView) view.findViewById(R.id.score_rate);
        TextView unused6 = info.score = (TextView) view.findViewById(R.id.score_score);
        view.setTag(info);
        return view;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ScoresCursor scoresCursor = (ScoresCursor) cursor;
        ScoreInfo info = (ScoreInfo) view.getTag();
        info.time.setText(scoresCursor.getTimeCompletedAsString());
        info.date.setText(scoresCursor.getDateCompletedAsString());
        info.nrFound.setText(scoresCursor.getNrFoundAsString());
        info.percentageFound.setText(scoresCursor.getPercentageFoundAsString());
        info.rate.setText(scoresCursor.getWordsPerMinuteAsString());
        info.score.setText(scoresCursor.getScoreAsString());
    }
}
