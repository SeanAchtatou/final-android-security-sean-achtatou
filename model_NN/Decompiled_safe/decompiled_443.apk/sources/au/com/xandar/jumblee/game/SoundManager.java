package au.com.xandar.jumblee.game;

import android.content.Context;
import android.media.SoundPool;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public final class SoundManager {
    private static final int MAX_SIMULTANEOUS_SOUNDS = 3;
    private static final float NORMAL_RATE = 1.0f;
    private static final int NO_LOOPS = 0;
    private static final int PRIORITY = 1;
    private int checkWordFailureAlreadyFoundSound;
    private int checkWordFailureSound;
    private int checkWordSuccessNineLetterSound;
    private int checkWordSuccessSound;
    private int gameOverSound;
    private boolean initialised = false;
    private ApplicationPreferences prefs;
    private SoundPool soundPool;

    public void initSounds(Context ctx, ApplicationPreferences applicationPreferences) {
        if (!this.initialised) {
            this.soundPool = new SoundPool(3, 3, 0);
            this.prefs = applicationPreferences;
            this.checkWordSuccessSound = this.soundPool.load(ctx, R.raw.checkword_success, 1);
            this.checkWordSuccessNineLetterSound = this.soundPool.load(ctx, R.raw.check_word_success_nine_letter, 1);
            this.checkWordFailureSound = this.soundPool.load(ctx, R.raw.checkword_failure, 1);
            this.checkWordFailureAlreadyFoundSound = this.soundPool.load(ctx, R.raw.checkword_rejected_already_found, 1);
            this.gameOverSound = this.soundPool.load(ctx, R.raw.game_over, 1);
            this.initialised = true;
        }
    }

    public void releaseSounds() {
        if (this.initialised) {
            this.soundPool.release();
            this.soundPool = null;
        }
    }

    private void playSound(int resourceId) {
        if (this.initialised && this.prefs.getSoundEffectOn()) {
            float volume = this.prefs.getSoundEffectVolume();
            this.soundPool.play(resourceId, volume, volume, 1, 0, NORMAL_RATE);
        }
    }

    public void playCheckWordSuccess() {
        playSound(this.checkWordSuccessSound);
    }

    public void playCheckWordSuccessNineLetter() {
        playSound(this.checkWordSuccessNineLetterSound);
    }

    public void playCheckWordFailure() {
        playSound(this.checkWordFailureSound);
    }

    public void playCheckWordFailureAlreadyFound() {
        playSound(this.checkWordFailureAlreadyFoundSound);
    }

    public void playGameOver() {
        playSound(this.gameOverSound);
    }
}
