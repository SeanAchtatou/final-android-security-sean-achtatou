package au.com.xandar.jumblee.scoreloop.image;

import android.graphics.drawable.Drawable;
import android.util.Log;
import au.com.xandar.java.collection.LruCache;
import au.com.xandar.java.io.ByteArrayReader;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.db.JumbleeDB;
import au.com.xandar.jumblee.db.ScoreloopImage;
import com.scoreloop.client.android.core.model.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ImageDatabaseCacher {
    private final ImageDownloadGate downloadGate;
    private final ExecutorService imageDownloadExecutor = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public final ImageDatabaseInserter imageInserter;
    /* access modifiers changed from: private */
    public final ImageDatabaseUpdater imageUpdater;

    private static final class ImageDatabaseInserter {
        private final LruCache<String, Drawable> inMemoryCache;
        private final JumbleeDB jumbleeDB;

        public ImageDatabaseInserter(LruCache<String, Drawable> inMemoryCache2, JumbleeDB jumbleeDB2) {
            this.inMemoryCache = inMemoryCache2;
            this.jumbleeDB = jumbleeDB2;
        }

        public Drawable getImage(User user) {
            String userId = user.getIdentifier();
            String userName = user.getLogin();
            String scoreloopImageUrl = ImageUrlHelper.getImageUrl(user);
            if (userId == null) {
                Log.w(AppConstants.TAG, "ImageDatabaseInserter - no userId, but asked to retrieve image url=" + scoreloopImageUrl);
                return null;
            }
            try {
                byte[] imageBytes = new ByteArrayReader().readBytesFromUrl(scoreloopImageUrl);
                ScoreloopImage newScoreloopImage = new ScoreloopImage();
                newScoreloopImage.setScoreloopUserId(userId);
                newScoreloopImage.setDateLastReadAsMillis(System.currentTimeMillis());
                newScoreloopImage.setImageUri(scoreloopImageUrl);
                newScoreloopImage.setImageBytes(imageBytes);
                this.jumbleeDB.insertScoreloopImage(newScoreloopImage);
                Drawable drawable = Drawable.createFromStream(new ByteArrayInputStream(imageBytes), scoreloopImageUrl);
                if (this.inMemoryCache != null) {
                    this.inMemoryCache.put(userId, drawable);
                }
                this.jumbleeDB.trimScoreloopImageCache(100);
                return drawable;
            } catch (IOException e) {
                Log.w(AppConstants.TAG, "ImageDatabaseInserter - Could not retrieve image for " + userName, e);
                return null;
            }
        }
    }

    private static final class ImageDatabaseUpdater {
        private final LruCache<String, Drawable> inMemoryCache;
        private final JumbleeDB jumbleeDB;

        public ImageDatabaseUpdater(LruCache<String, Drawable> inMemoryCache2, JumbleeDB jumbleeDB2) {
            this.inMemoryCache = inMemoryCache2;
            this.jumbleeDB = jumbleeDB2;
        }

        public Drawable getImage(User user) {
            String scoreloopUserId = user.getIdentifier();
            String userName = user.getLogin();
            String scoreloopImageUrl = ImageUrlHelper.getImageUrl(user);
            try {
                byte[] imageBytes = new ByteArrayReader().readBytesFromUrl(scoreloopImageUrl);
                ScoreloopImage scoreloopImage = new ScoreloopImage();
                scoreloopImage.setScoreloopUserId(scoreloopUserId);
                scoreloopImage.setImageUri(scoreloopImageUrl);
                scoreloopImage.setImageBytes(imageBytes);
                this.jumbleeDB.updateScoreloopImage(scoreloopImage);
                Drawable drawable = Drawable.createFromStream(new ByteArrayInputStream(imageBytes), scoreloopImageUrl);
                if (this.inMemoryCache != null) {
                    this.inMemoryCache.put(scoreloopUserId, drawable);
                }
                return drawable;
            } catch (IOException e) {
                Log.w(AppConstants.TAG, "ImageDatabaseUpdater - Could not retrieve image for " + userName, e);
                return null;
            }
        }
    }

    public ImageDatabaseCacher(ImageDownloadGate downloadGate2, JumbleeDB jumbleeDB, LruCache<String, Drawable> inMemoryCache) {
        this.downloadGate = downloadGate2;
        this.imageInserter = new ImageDatabaseInserter(inMemoryCache, jumbleeDB);
        this.imageUpdater = new ImageDatabaseUpdater(inMemoryCache, jumbleeDB);
    }

    public void shutdown() {
        this.imageDownloadExecutor.shutdownNow();
    }

    public void fillDatabaseCacheFromScoreloop(final User user, ScoreloopImage cachedImage) {
        String imageUrl = ImageUrlHelper.getImageUrl(user);
        if (imageUrl == null || !this.downloadGate.shouldDownload()) {
            return;
        }
        if (cachedImage == null) {
            this.imageDownloadExecutor.execute(new Runnable() {
                public void run() {
                    ImageDatabaseCacher.this.imageInserter.getImage(user);
                }
            });
        } else if (!imageUrl.equals(cachedImage.getImageUri())) {
            this.imageDownloadExecutor.execute(new Runnable() {
                public void run() {
                    ImageDatabaseCacher.this.imageUpdater.getImage(user);
                }
            });
        }
    }

    public Drawable getMostRecentImageFromScoreloop(User user, ScoreloopImage cachedImage) {
        String imageUrl = ImageUrlHelper.getImageUrl(user);
        if (imageUrl == null) {
            return null;
        }
        if (cachedImage == null) {
            return this.imageInserter.getImage(user);
        }
        if (!imageUrl.equals(cachedImage.getImageUri())) {
            return this.imageUpdater.getImage(user);
        }
        return cachedImage.getDrawable();
    }
}
