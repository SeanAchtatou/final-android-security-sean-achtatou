package au.com.xandar.jumblee.prefs;

public final class LatestVersion {
    private String datePublished;
    private int versionCode;
    private String versionName;

    public int getVersionCode() {
        return this.versionCode;
    }

    public void setVersionCode(int versionCode2) {
        this.versionCode = versionCode2;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public String getDatePublished() {
        return this.datePublished;
    }

    public void setDatePublished(String datePublished2) {
        this.datePublished = datePublished2;
    }
}
