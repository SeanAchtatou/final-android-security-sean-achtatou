package au.com.xandar.jumblee.game;

public interface GameListener {
    void onGameEvent(Game game, int i, Object obj);
}
