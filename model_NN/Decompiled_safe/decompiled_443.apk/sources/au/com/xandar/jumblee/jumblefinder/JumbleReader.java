package au.com.xandar.jumblee.jumblefinder;

import android.content.Context;
import android.util.Log;
import au.com.xandar.jumblee.Jumble;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

final class JumbleReader {
    private final Context context;
    private int nrJumblesToRead;
    private final ApplicationPreferences preferences;
    private final Random random = new Random();

    JumbleReader(Context context2, ApplicationPreferences preferences2, int nrJumblesToRead2) {
        this.context = context2;
        this.preferences = preferences2;
        this.nrJumblesToRead = nrJumblesToRead2;
    }

    public List<Jumble> getJumbles() {
        List<Jumble> jumbles = new ArrayList<>();
        int firstDictionaryWord = this.preferences.getNextJumbleToConsider();
        DictionaryReader reader = new DictionaryReader(firstDictionaryWord, this.context.getResources().openRawResource(R.raw.dictionary_9_letter_words));
        StringBuilder word = new StringBuilder();
        while (true) {
            if (this.nrJumblesToRead <= 0) {
                break;
            }
            if (!reader.readWord(word)) {
                reader.close();
                firstDictionaryWord = 0;
                reader = new DictionaryReader(0, this.context.getResources().openRawResource(R.raw.dictionary_9_letter_words));
                if (!reader.readWord(word)) {
                    Log.w(AppConstants.TAG, "At 9 letter dictionary EOF - Reset dictionary but could not read another Jumble");
                    break;
                }
            }
            String jumbleWord = word.toString();
            int specialLetterOffset = this.random.nextInt(9);
            jumbles.add(new MutableJumble(jumbleWord, jumbleWord.substring(specialLetterOffset, specialLetterOffset + 1)));
            this.nrJumblesToRead--;
            firstDictionaryWord++;
        }
        this.preferences.setNextJumbleToConsider(firstDictionaryWord);
        reader.close();
        return jumbles;
    }
}
