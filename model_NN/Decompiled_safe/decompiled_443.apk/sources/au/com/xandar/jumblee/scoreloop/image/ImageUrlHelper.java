package au.com.xandar.jumblee.scoreloop.image;

import com.scoreloop.client.android.core.model.User;

public final class ImageUrlHelper {
    public static String getImageUrl(User user) {
        String url = user.getImageUrl();
        if (url == null || "".equals(url)) {
            return null;
        }
        int lastIndex = url.lastIndexOf("?");
        return lastIndex == -1 ? url : url.substring(0, lastIndex);
    }
}
