package au.com.xandar.jumblee.game.widget;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.Button;

public class RestorableButton extends Button {
    private final TextViewStateRestorer restorer;

    public RestorableButton(Context context) {
        this(context, null);
    }

    public RestorableButton(Context context, AttributeSet attrs) {
        this(context, attrs, 16842824);
    }

    public RestorableButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.restorer = new TextViewStateRestorer();
    }

    public final Parcelable onSaveInstanceState() {
        return this.restorer.saveInstanceState(this, super.onSaveInstanceState());
    }

    public void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(this.restorer.restoreInstanceState(this, state));
    }
}
