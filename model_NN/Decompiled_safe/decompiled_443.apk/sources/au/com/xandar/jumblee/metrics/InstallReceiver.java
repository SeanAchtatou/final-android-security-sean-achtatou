package au.com.xandar.jumblee.metrics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import au.com.xandar.android.net.QueryParser;
import au.com.xandar.jumblee.common.JumbleeHandledException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import org.acra.ACRA;

public final class InstallReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            new com.google.ads.InstallReceiver().onReceive(context, intent);
            String referrer = intent.getStringExtra("referrer");
            QueryParser parser = new QueryParser();
            Map<String, String> params = new HashMap<>();
            try {
                params.putAll(parser.getQueryMap(referrer));
            } catch (IllegalArgumentException e) {
                IllegalArgumentException illegalArgumentException = e;
                params.put("decodedReferrer", getDecodedReferrer(referrer));
            }
            FlurryAnalytics analytics = new FlurryAnalytics(context);
            analytics.startTracking();
            analytics.sendInstalled(params);
            analytics.stopTracking();
        } catch (RuntimeException e2) {
            RuntimeException e3 = e2;
            ACRA.getErrorReporter().handleSilentException(new JumbleeHandledException(e3.getMessage(), e3));
        }
    }

    private String getDecodedReferrer(String referrer) {
        if (referrer == null) {
            return "";
        }
        try {
            return URLDecoder.decode(referrer, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return e.getMessage();
        }
    }
}
