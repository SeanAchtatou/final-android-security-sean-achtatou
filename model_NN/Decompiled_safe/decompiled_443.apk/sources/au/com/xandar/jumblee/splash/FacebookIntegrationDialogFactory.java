package au.com.xandar.jumblee.splash;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import au.com.xandar.android.facebook.Facebook;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.facebook.FacebookPreferencesHandler;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

final class FacebookIntegrationDialogFactory {
    /* access modifiers changed from: private */
    public final Activity context;
    private final Facebook facebook;
    private final ApplicationPreferences preferences;

    public FacebookIntegrationDialogFactory(Activity context2, Facebook facebook2, ApplicationPreferences preferences2) {
        this.context = context2;
        this.facebook = facebook2;
        this.preferences = preferences2;
    }

    public Dialog create() {
        View layout = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_facebook_config, (ViewGroup) null);
        final Dialog dialog = new Dialog(this.context, R.style.DialogTheme);
        dialog.addContentView(layout, new ViewGroup.LayoutParams(-1, -2));
        dialog.setCancelable(false);
        final Button positiveButton = (Button) layout.findViewById(R.id.positiveButton);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        new FacebookPreferencesHandler(this.context, this.facebook, this.preferences, layout, new Runnable() {
            public void run() {
                positiveButton.setEnabled(false);
            }
        }, new Runnable() {
            public void run() {
                positiveButton.setEnabled(true);
                FacebookIntegrationDialogFactory.this.context.removeDialog(100);
            }
        }).populateViews();
        return dialog;
    }
}
