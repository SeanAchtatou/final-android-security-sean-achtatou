package au.com.xandar.jumblee.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import au.com.xandar.jumblee.R;

final class ProgressDialogFactory {
    ProgressDialogFactory() {
    }

    public Dialog create(Context context, int messageResourceId, boolean cancelable) {
        Dialog dialog = new Dialog(context, R.style.DialogTheme);
        View layout = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_progress, (ViewGroup) null);
        ((TextView) layout.findViewById(R.id.progress_text)).setText(context.getString(messageResourceId));
        dialog.addContentView(layout, new ViewGroup.LayoutParams(-1, -2));
        dialog.setCancelable(cancelable);
        return dialog;
    }
}
