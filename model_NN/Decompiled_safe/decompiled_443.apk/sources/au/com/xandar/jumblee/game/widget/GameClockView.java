package au.com.xandar.jumblee.game.widget;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.TextView;

public final class GameClockView extends TextView {
    private final SimpleCharSequence buffer;
    private final TextViewStateRestorer restorer;

    public GameClockView(Context context) {
        this(context, null);
    }

    public GameClockView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GameClockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.restorer = new TextViewStateRestorer();
        this.buffer = new SimpleCharSequence(20);
        setSingleLine(false);
    }

    public final Parcelable onSaveInstanceState() {
        return this.restorer.saveInstanceState(this, super.onSaveInstanceState());
    }

    public final void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(this.restorer.restoreInstanceState(this, state));
    }

    public void setTimeRemaining(int secondsRemaining) {
        this.buffer.clear();
        this.buffer.appendSeconds(secondsRemaining);
        setText(this.buffer.getChars(), 0, this.buffer.length());
    }
}
