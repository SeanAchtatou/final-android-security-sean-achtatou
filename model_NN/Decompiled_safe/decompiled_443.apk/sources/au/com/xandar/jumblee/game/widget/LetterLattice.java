package au.com.xandar.jumblee.game.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.game.Game;

public final class LetterLattice extends TableLayout {
    private final LetterLatticeButton centreButton;
    private final LetterLatticeButton[] standardButtons;

    public LetterLattice(Context context) {
        this(context, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, au.com.xandar.jumblee.game.widget.LetterLattice, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public LetterLattice(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.standardButtons = new LetterLatticeButton[8];
        ((Activity) context).getLayoutInflater().inflate((int) R.layout.letter_lattice, (ViewGroup) this, true);
        this.standardButtons[0] = (LetterLatticeButton) findViewById(R.id.topLeft);
        this.standardButtons[1] = (LetterLatticeButton) findViewById(R.id.topMiddle);
        this.standardButtons[2] = (LetterLatticeButton) findViewById(R.id.topRight);
        this.standardButtons[3] = (LetterLatticeButton) findViewById(R.id.middleLeft);
        this.centreButton = (LetterLatticeButton) findViewById(R.id.middleMiddle);
        this.standardButtons[4] = (LetterLatticeButton) findViewById(R.id.middleRight);
        this.standardButtons[5] = (LetterLatticeButton) findViewById(R.id.bottomLeft);
        this.standardButtons[6] = (LetterLatticeButton) findViewById(R.id.bottomMiddle);
        this.standardButtons[7] = (LetterLatticeButton) findViewById(R.id.bottomRight);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.centreButton.setOnClickListener(listener);
        for (Button button : this.standardButtons) {
            button.setOnClickListener(listener);
        }
    }

    public void populateLattice(Game game) {
        this.centreButton.setLetter(game.getSpecialLetter());
        int i = 0;
        for (String letter : game.getStandardLetters()) {
            this.standardButtons[i].setLetter(letter);
            i++;
        }
    }

    public void enableLetters(boolean enable) {
        this.centreButton.setEnabled(enable);
        for (Button button : this.standardButtons) {
            button.setEnabled(enable);
        }
    }
}
