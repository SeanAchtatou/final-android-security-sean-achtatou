package au.com.xandar.jumblee.splash;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.WebView;
import au.com.xandar.android.facebook.Facebook;
import au.com.xandar.android.os.Compatibility;
import au.com.xandar.android.os.SimpleAsyncTask;
import au.com.xandar.android.os.StrictModeThreadPolicy;
import au.com.xandar.android.pm.PackageManagerUtility;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.JumbleeApplication;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.dialog.DialogId;
import au.com.xandar.jumblee.dialog.JumbleeDialogFactory;
import au.com.xandar.jumblee.facebook.FacebookPoster;
import au.com.xandar.jumblee.jumblefinder.JumbleFinderService;
import au.com.xandar.jumblee.metrics.VersionFetcher;
import au.com.xandar.jumblee.net.GeneralIntentService;
import au.com.xandar.jumblee.overview.OverviewActivity;
import au.com.xandar.jumblee.scoreloop.ScoreloopPosterService;

public final class SplashActivity extends JumbleeActivity {
    private Handler appInitialiser = new Handler() {
        public void handleMessage(Message msg) {
            SplashActivity.this.initialiseApp();
        }
    };
    private int dialogNr;
    /* access modifiers changed from: private */
    public long startTime;
    private boolean started;

    private static final class ConfigInfo {
        /* access modifiers changed from: private */
        public int dialogNr;
        /* access modifiers changed from: private */
        public boolean started;

        private ConfigInfo() {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.splash_screen);
        ConfigInfo configInfo = (ConfigInfo) getLastNonConfigurationInstance();
        if (configInfo != null) {
            this.dialogNr = configInfo.dialogNr;
            this.started = configInfo.started;
            return;
        }
        this.dialogNr = 1;
        this.started = false;
    }

    public Object onRetainNonConfigurationInstance() {
        ConfigInfo configInfo = new ConfigInfo();
        int unused = configInfo.dialogNr = this.dialogNr;
        boolean unused2 = configInfo.started = this.started;
        return configInfo;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.startTime = System.currentTimeMillis();
        getAnalytics().startTracking();
        if (!this.started) {
            this.appInitialiser.sendEmptyMessage(0);
            this.started = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getAnalytics().stopTracking();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog;
        JumbleeDialogFactory dialogFactory = new JumbleeDialogFactory(this);
        switch (id) {
            case DialogId.EULA_DIALOG_WITH_REJECT_BUTTON:
                dialog = dialogFactory.createEulaWithRejectButton(getApplicationPreferences());
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        if (!SplashActivity.this.isFinishing()) {
                            SplashActivity.this.showNextDialog();
                        }
                    }
                });
                break;
            case 30:
                dialog = dialogFactory.createHowToPlayDialog(getApplicationPreferences());
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        SplashActivity.this.showNextDialog();
                    }
                });
                break;
            case 40:
                dialog = dialogFactory.createUpdateDialog(getApplicationPreferences());
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        if (!SplashActivity.this.isFinishing()) {
                            SplashActivity.this.showNextDialog();
                        }
                    }
                });
                break;
            case DialogId.PIRACY_DIALOG:
                dialog = dialogFactory.createPiracyDialog(getApplicationPreferences());
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                    }
                });
                break;
            case DialogId.RATE_ME_DIALOG:
                dialog = dialogFactory.createRateMeDialog(getApplicationPreferences());
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        SplashActivity.this.showNextDialog();
                    }
                });
                break;
            case DialogId.FACEBOOK_INTEGRATION_DIALOG:
                dialog = new FacebookIntegrationDialogFactory(this, getFacebook(), getApplicationPreferences()).create();
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        SplashActivity.this.getApplicationPreferences().setFacebook_haveAskedAboutIntegration(true);
                        SplashActivity.this.showNextDialog();
                    }
                });
                break;
            case DialogId.FACEBOOK_AUTHENTICATION_DIALOG:
                dialog = getFacebook().createAuthenticationDialog(this);
                break;
            case 101:
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.requestWindowFeature(1);
                progressDialog.setMessage("Loading...");
                dialog = progressDialog;
                break;
            default:
                Log.e(AppConstants.TAG, "Unknown Dialog ID : " + id);
                return super.onCreateDialog(id);
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case DialogId.FACEBOOK_AUTHENTICATION_DIALOG:
                getFacebook().prepareAuthenticationDialog(dialog);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 32665) {
            getFacebook().authorizeCallback(this, requestCode, resultCode, data);
        }
    }

    /* access modifiers changed from: private */
    public void initialiseApp() {
        new SimpleAsyncTask() {
            WebView webCache;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                StrictModeThreadPolicy strictModeThreadPolicy = Compatibility.getHelper().allowThreadDiskWrites();
                this.webCache = new WebView(SplashActivity.this);
                Compatibility.getHelper().setStrictModeThreadPolicy(strictModeThreadPolicy);
            }

            /* access modifiers changed from: protected */
            public void doInBackground() {
                SplashActivity.this.getJumbleeApp().prefetchCurrentJumble();
                this.webCache.clearCache(true);
                Intent checkVersionIntent = new Intent(SplashActivity.this, GeneralIntentService.class);
                checkVersionIntent.putExtra(GeneralIntentService.EXECUTABLE_NAME_KEY, VersionFetcher.EXECUTABLE_NAME_VALUE);
                SplashActivity.this.startService(checkVersionIntent);
                SplashActivity.this.getApplicationPreferences().updateApplicationStarted();
                Intent facebookPosterIntent = new Intent(SplashActivity.this, GeneralIntentService.class);
                facebookPosterIntent.putExtra(GeneralIntentService.EXECUTABLE_NAME_KEY, FacebookPoster.EXECUTABLE_NAME_VALUE);
                SplashActivity.this.startService(facebookPosterIntent);
                SplashActivity.this.startService(new Intent(SplashActivity.this, JumbleFinderService.class));
            }

            /* access modifiers changed from: protected */
            public void onPostExecute() {
                SplashActivity.this.startService(new Intent(SplashActivity.this, ScoreloopPosterService.class));
                SplashActivity.this.showNextDialog();
            }
        }.execute(getExecutor());
    }

    /* access modifiers changed from: private */
    public void showNextDialog() {
        if (!isFinishing()) {
            int i = this.dialogNr;
            this.dialogNr = i + 1;
            switch (i) {
                case 1:
                    showPiracy();
                    return;
                case 2:
                    showEula();
                    return;
                case 3:
                    showHowToPlay();
                    return;
                case 4:
                    showUpdateVersion();
                    return;
                case 5:
                    showRateMe();
                    return;
                case 6:
                    showFacebookIntegration();
                    return;
                default:
                    showOverview();
                    return;
            }
        }
    }

    private void showPiracy() {
        if (getJumbleeApp().isPirated()) {
            getAnalytics().sendPirateName(getPackageName());
            if (getJumbleeApp().isTimeToNotifyPirate()) {
                showDialog(50);
                return;
            }
            return;
        }
        showNextDialog();
    }

    private void showEula() {
        if (getApplicationPreferences().getShowEULAOnStartup()) {
            showDialog(21);
        } else {
            showNextDialog();
        }
    }

    private void showHowToPlay() {
        if (getApplicationPreferences().getShowHowToPlayOnStartup()) {
            showDialog(30);
        } else {
            showNextDialog();
        }
    }

    private void showFacebookIntegration() {
        boolean startedEnoughTimesToPromptForFacebook;
        boolean shouldAskAboutFacebook;
        boolean authenticationExpired;
        if (getApplicationPreferences().getNrTimesStarted() > 16) {
            startedEnoughTimesToPromptForFacebook = true;
        } else {
            startedEnoughTimesToPromptForFacebook = false;
        }
        if (!startedEnoughTimesToPromptForFacebook || getApplicationPreferences().getFacebook_haveAskedAboutIntegration()) {
            shouldAskAboutFacebook = false;
        } else {
            shouldAskAboutFacebook = true;
        }
        if (!getApplicationPreferences().getFacebook_postScores() || getFacebook().isSessionValid()) {
            authenticationExpired = false;
        } else {
            authenticationExpired = true;
        }
        if (shouldAskAboutFacebook || authenticationExpired) {
            showDialog(80);
        } else {
            showNextDialog();
        }
    }

    private void showRateMe() {
        boolean startedEnoughTimesToRate;
        boolean timeForNextRequest;
        boolean requiresRating;
        boolean isMarketAvailable = new PackageManagerUtility(this).isMarketAvailable();
        if (getApplicationPreferences().getNrTimesStarted() > 8) {
            startedEnoughTimesToRate = true;
        } else {
            startedEnoughTimesToRate = false;
        }
        if (getApplicationPreferences().getDateRateMeNextRequested() < System.currentTimeMillis()) {
            timeForNextRequest = true;
        } else {
            timeForNextRequest = false;
        }
        if (!startedEnoughTimesToRate || !timeForNextRequest || getApplicationPreferences().getApplicationRated()) {
            requiresRating = false;
        } else {
            requiresRating = true;
        }
        if (!requiresRating || !isMarketAvailable) {
            showNextDialog();
        } else {
            showDialog(60);
        }
    }

    private void showUpdateVersion() {
        if (new PackageManagerUtility(this).getAllPackageInfo().versionCode < getApplicationPreferences().getLatestVersionCode()) {
            showDialog(40);
        } else {
            showNextDialog();
        }
    }

    private void showOverview() {
        new SimpleAsyncTask() {
            /* access modifiers changed from: protected */
            public void doInBackground() {
                while (System.currentTimeMillis() < SplashActivity.this.startTime + 1000) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute() {
                SplashActivity.this.startActivity(new Intent(SplashActivity.this, OverviewActivity.class));
            }
        }.execute(getExecutor());
    }

    private Facebook getFacebook() {
        return ((JumbleeApplication) getApplication()).getFacebook();
    }
}
