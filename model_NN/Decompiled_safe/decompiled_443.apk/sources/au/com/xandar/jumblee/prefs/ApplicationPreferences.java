package au.com.xandar.jumblee.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import au.com.xandar.jumblee.common.AppConstants;
import java.util.Random;

public final class ApplicationPreferences {
    private static final String APPLICATION_HAS_BEEN_RATED = "applicationHasBeenRated";
    private static final String DATE_FIRST_STARTED = "dateFirstStarted";
    private static final String DATE_NEXT_ONCE_PER_DAY_CHECK = "dateNextOncePerDayCheck";
    private static final String DATE_RATE_ME_NEXT_REQUESTED = "dateRateMeNextRequested";
    private static final String DOWNLOAD_GLOBAL_SCORE_IMAGES = "downloadGlobalScoreImages";
    private static final String FACEBOOK_HAVE_ASKED_ABOUT_INTEGRATION = "haveAskedAboutFBIntegration";
    private static final String FACEBOOK_POST_SCORES = "postScoresToFB";
    private static final String HAPTIC_FEED_BACK = "hapticFeedBack";
    private static final String HAS_SHOWN_GLOBAL_PROFILE = "hasShownGlobalProfile";
    private static final String HIDE_WORD_ACCEPTED_MESSAGE = "hideWordAcceptedMessage";
    private static final String HIDE_WORD_ALREADY_FOUND_MESSAGE = "hideWordAlreadyFoundMessage";
    private static final String HIDE_WORD_REJECTED_MESSAGE = "hideWordRejectedMessage";
    private static final String LATEST_VERSION_CODE = "latestVersionCode";
    private static final String LATEST_VERSION_DATE_PUBLISHED = "latestVersionDatePublished";
    private static final String LATEST_VERSION_NAME = "latestVersionName";
    private static final String NEXT_JUMBLE_TO_CONSIDER = "nextJumbleToConsider";
    private static final String NR_TIMES_STARTED = "nrTimesStarted";
    private static final String SHOW_CONFIRMATION_DIALOG_ON_END_GAME = "showConfirmationDialogOnEndGame";
    private static final String SHOW_EULA_ON_STARTUP = "showEULAOnStartup";
    private static final String SHOW_HOW_TO_PLAY_ON_STARTUP = "showHowToPlayOnStartup";
    private static final String SOUND_EFFECT_ON = "soundEffectOn";
    private static final String SOUND_EFFECT_VOLUME = "soundEffectVolume";
    private final SharedPreferences preferences;

    public enum DownloadScoreloopImages {
        ALWAYS,
        ONLY_WITH_WIFI_CONNECTION,
        NEVER
    }

    public ApplicationPreferences(Context context) {
        this.preferences = context.getSharedPreferences(context.getPackageName() + "_preferences", 0);
    }

    public int getNextJumbleToConsider() {
        return this.preferences.getInt(NEXT_JUMBLE_TO_CONSIDER, new Random().nextInt(AppConstants.TOTAL_NR_OF_NINE_LETTER_JUMBLES));
    }

    public void setNextJumbleToConsider(int nextJumbleToConsider) {
        postInteger(NEXT_JUMBLE_TO_CONSIDER, nextJumbleToConsider);
    }

    public boolean getHapticFeedBack() {
        return this.preferences.getBoolean(HAPTIC_FEED_BACK, true);
    }

    public void setHapticFeedBack(boolean value) {
        postBoolean(HAPTIC_FEED_BACK, value);
    }

    public boolean getSoundEffectOn() {
        return this.preferences.getBoolean(SOUND_EFFECT_ON, true);
    }

    public void setSoundEffectOn(boolean value) {
        postBoolean(SOUND_EFFECT_ON, value);
    }

    public float getSoundEffectVolume() {
        return this.preferences.getFloat(SOUND_EFFECT_VOLUME, 0.5f);
    }

    public void setSoundEffectVolume(float value) {
        postFloat(SOUND_EFFECT_VOLUME, value);
    }

    public int getLatestVersionCode() {
        return this.preferences.getInt(LATEST_VERSION_CODE, 0);
    }

    public String getLatestVersionName() {
        return this.preferences.getString(LATEST_VERSION_NAME, "1.0.0");
    }

    public String getLatestVersionDatePublished() {
        return this.preferences.getString(LATEST_VERSION_DATE_PUBLISHED, "5-NOV-2010");
    }

    public void setLatestVersion(LatestVersion latestVersion) {
        SharedPreferences.Editor editor = this.preferences.edit();
        if (getLatestVersionCode() != latestVersion.getVersionCode()) {
            editor.putInt(LATEST_VERSION_CODE, latestVersion.getVersionCode());
        }
        if (!getLatestVersionName().equals(latestVersion.getVersionName())) {
            editor.putString(LATEST_VERSION_NAME, latestVersion.getVersionName());
        }
        if (!getLatestVersionDatePublished().equals(latestVersion.getDatePublished())) {
            editor.putString(LATEST_VERSION_DATE_PUBLISHED, latestVersion.getDatePublished());
        }
        editor.commit();
    }

    public int getNrTimesStarted() {
        return this.preferences.getInt(NR_TIMES_STARTED, 0);
    }

    public long getDateFirstStarted() {
        return this.preferences.getLong(DATE_FIRST_STARTED, 0);
    }

    public long getNextOncePerDayCheck() {
        return this.preferences.getLong(DATE_NEXT_ONCE_PER_DAY_CHECK, 0);
    }

    public void setNextOncePerDayCheck(long dateInMillis) {
        postLong(DATE_NEXT_ONCE_PER_DAY_CHECK, dateInMillis);
    }

    public long getDateRateMeNextRequested() {
        return this.preferences.getLong(DATE_RATE_ME_NEXT_REQUESTED, 0);
    }

    public void setDateRateMeNextRequested(long dateInMillis) {
        postLong(DATE_RATE_ME_NEXT_REQUESTED, dateInMillis);
    }

    public void updateApplicationStarted() {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putInt(NR_TIMES_STARTED, getNrTimesStarted() + 1);
        if (getDateFirstStarted() == 0) {
            editor.putLong(DATE_FIRST_STARTED, System.currentTimeMillis());
        }
        if (getDateRateMeNextRequested() == 0) {
            editor.putLong(DATE_RATE_ME_NEXT_REQUESTED, System.currentTimeMillis());
        }
        editor.commit();
    }

    public boolean getShowHowToPlayOnStartup() {
        return this.preferences.getBoolean(SHOW_HOW_TO_PLAY_ON_STARTUP, true);
    }

    public void setShowHowToPlayOnStartup(boolean value) {
        postBoolean(SHOW_HOW_TO_PLAY_ON_STARTUP, value);
    }

    public boolean getShowEULAOnStartup() {
        return this.preferences.getBoolean(SHOW_EULA_ON_STARTUP, true);
    }

    public void setShowEULAOnStartup(boolean value) {
        postBoolean(SHOW_EULA_ON_STARTUP, value);
    }

    public boolean getApplicationRated() {
        return this.preferences.getBoolean(APPLICATION_HAS_BEEN_RATED, false);
    }

    public void setApplicationRated(boolean value) {
        postBoolean(APPLICATION_HAS_BEEN_RATED, value);
    }

    public boolean getFacebook_haveAskedAboutIntegration() {
        return this.preferences.getBoolean(FACEBOOK_HAVE_ASKED_ABOUT_INTEGRATION, false);
    }

    public void setFacebook_haveAskedAboutIntegration(boolean value) {
        postBoolean(FACEBOOK_HAVE_ASKED_ABOUT_INTEGRATION, value);
    }

    public boolean getFacebook_postScores() {
        return this.preferences.getBoolean(FACEBOOK_POST_SCORES, false);
    }

    public void setFacebook_postScores(boolean value) {
        postBoolean(FACEBOOK_POST_SCORES, value);
    }

    public boolean getHideWordAcceptedMessage() {
        return this.preferences.getBoolean(HIDE_WORD_ACCEPTED_MESSAGE, false);
    }

    public void setHideWordAcceptedMessage(boolean value) {
        postBoolean(HIDE_WORD_ACCEPTED_MESSAGE, value);
    }

    public boolean getHideWordRejectedMessage() {
        return this.preferences.getBoolean(HIDE_WORD_REJECTED_MESSAGE, false);
    }

    public void setHideWordRejectedMessage(boolean value) {
        postBoolean(HIDE_WORD_REJECTED_MESSAGE, value);
    }

    public boolean getHideWordAlreadyFoundMessage() {
        return this.preferences.getBoolean(HIDE_WORD_ALREADY_FOUND_MESSAGE, false);
    }

    public void setHideWordAlreadyFoundMessage(boolean value) {
        postBoolean(HIDE_WORD_ALREADY_FOUND_MESSAGE, value);
    }

    public boolean getShowConfirmationDialogOnEndGame() {
        return this.preferences.getBoolean(SHOW_CONFIRMATION_DIALOG_ON_END_GAME, false);
    }

    public void setShowConfirmationDialogOnEndGame(boolean value) {
        postBoolean(SHOW_CONFIRMATION_DIALOG_ON_END_GAME, value);
    }

    public boolean getHasShownGlobalProfile() {
        return this.preferences.getBoolean(HAS_SHOWN_GLOBAL_PROFILE, false);
    }

    public void setHasShownGlobalProfile(boolean value) {
        postBoolean(HAS_SHOWN_GLOBAL_PROFILE, value);
    }

    public int getDownloadGlobalScoreImages() {
        return this.preferences.getInt(DOWNLOAD_GLOBAL_SCORE_IMAGES, DownloadScoreloopImages.ONLY_WITH_WIFI_CONNECTION.ordinal());
    }

    public void setDownloadGlobalScoreImages(int value) {
        postInteger(DOWNLOAD_GLOBAL_SCORE_IMAGES, value);
    }

    private void postInteger(String key, int value) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private void postBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void postLong(String key, long value) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    private void postFloat(String key, float value) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }
}
