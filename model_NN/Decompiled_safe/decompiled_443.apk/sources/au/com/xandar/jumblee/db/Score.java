package au.com.xandar.jumblee.db;

public final class Score {
    private long id;
    private int nrFoundWords;
    private int nrPossibleWords;
    private double percentageFound;
    private double score;
    private double wordsPerMinute;

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public void setScore(double score2) {
        this.score = score2;
    }

    public double getScore() {
        return this.score;
    }

    public double getPercentageFound() {
        return this.percentageFound;
    }

    public void setPercentageFound(double percentageFound2) {
        this.percentageFound = percentageFound2;
    }

    public int getNrFoundWords() {
        return this.nrFoundWords;
    }

    public void setNrFoundWords(int nrFoundWords2) {
        this.nrFoundWords = nrFoundWords2;
    }

    public int getNrPossibleWords() {
        return this.nrPossibleWords;
    }

    public void setNrPossibleWords(int nrPossibleWords2) {
        this.nrPossibleWords = nrPossibleWords2;
    }

    public double getWordsPerMinute() {
        return this.wordsPerMinute;
    }

    public void setWordsPerMinute(double wordsPerMinute2) {
        this.wordsPerMinute = wordsPerMinute2;
    }
}
