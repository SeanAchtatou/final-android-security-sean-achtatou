package au.com.xandar.jumblee.game.widget;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.TextView;
import au.com.xandar.jumblee.game.Game;

public final class GameScoreView extends TextView {
    private final SimpleCharSequence buffer;
    private final TextViewStateRestorer restorer;

    public GameScoreView(Context context) {
        this(context, null);
    }

    public GameScoreView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GameScoreView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.restorer = new TextViewStateRestorer();
        this.buffer = new SimpleCharSequence(20);
        setSingleLine(false);
    }

    public void setScore(Game game) {
        double score = game.getJumble().getScore();
        this.buffer.clear();
        if (score < 10.0d) {
            this.buffer.appendNumberToTwoDecimalPlaces(score);
        } else if (score < 100.0d) {
            this.buffer.appendNumberToOneDecimalPlace(score);
        } else {
            this.buffer.appendInteger(score);
        }
        setText(this.buffer.getChars(), 0, this.buffer.length());
    }

    public final Parcelable onSaveInstanceState() {
        return this.restorer.saveInstanceState(this, super.onSaveInstanceState());
    }

    public final void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(this.restorer.restoreInstanceState(this, state));
    }
}
