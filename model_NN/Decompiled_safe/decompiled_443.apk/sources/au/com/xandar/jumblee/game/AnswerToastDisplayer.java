package au.com.xandar.jumblee.game;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

final class AnswerToastDisplayer {
    private static final int PORTRAIT = 0;
    private final Context context;
    private final LayoutInflater inflater;
    private Toast onAnswerToast;
    private final ApplicationPreferences preferences;
    private final Resources resources;

    AnswerToastDisplayer(Activity context2, ApplicationPreferences preferences2) {
        this.context = context2;
        this.preferences = preferences2;
        this.resources = context2.getResources();
        this.inflater = context2.getLayoutInflater();
    }

    public void display(int gameEvent, Object eventData) {
        String msg;
        int imageResourceId;
        int backgroundResourceId;
        int gravity;
        int yOffset;
        boolean textColourYellow = true;
        switch (gameEvent) {
            case 5:
                if (!this.preferences.getHideWordAcceptedMessage()) {
                    msg = this.resources.getString(R.string.word_accepted);
                    imageResourceId = R.drawable.check_response_word_accepted;
                    backgroundResourceId = R.drawable.background__check_word_response__accepted;
                    break;
                } else {
                    return;
                }
            case 6:
                if (!this.preferences.getHideWordAcceptedMessage()) {
                    msg = this.resources.getString(R.string.word_accepted_nine_letter);
                    imageResourceId = R.drawable.check_response_word_accepted_nine_letter;
                    backgroundResourceId = R.drawable.background__check_word_response__accepted_nine_letter;
                    textColourYellow = false;
                    break;
                } else {
                    return;
                }
            case 7:
                if (!this.preferences.getHideWordRejectedMessage()) {
                    msg = String.format(this.resources.getString(R.string.word_rejected), (String) eventData);
                    imageResourceId = R.drawable.check_response_word_rejected;
                    backgroundResourceId = R.drawable.background__check_word_response__rejected;
                    break;
                } else {
                    return;
                }
            case 8:
                if (!this.preferences.getHideWordAlreadyFoundMessage()) {
                    msg = String.format(this.resources.getString(R.string.word_rejected_already_found), (String) eventData);
                    imageResourceId = R.drawable.check_response_word_rejected_already_found;
                    backgroundResourceId = R.drawable.background__check_word_response__rejected_already_found;
                    break;
                } else {
                    return;
                }
            default:
                return;
        }
        if (this.onAnswerToast == null) {
            if (((WindowManager) this.context.getSystemService("window")).getDefaultDisplay().getOrientation() == 0) {
                yOffset = 25;
                gravity = 49;
            } else {
                gravity = 17;
                yOffset = 0;
            }
            View view = this.inflater.inflate((int) R.layout.check_word_response_screen, (ViewGroup) null);
            this.onAnswerToast = new Toast(this.context);
            this.onAnswerToast.setView(view);
            this.onAnswerToast.setGravity(gravity, 0, yOffset);
            this.onAnswerToast.setDuration(0);
        }
        View view2 = this.onAnswerToast.getView();
        view2.setBackgroundResource(backgroundResourceId);
        TextView textView = (TextView) view2.findViewById(R.id.check_word_response_text);
        textView.setTextColor(this.context.getResources().getColor(textColourYellow ? R.color.jumblee_yellow : R.color.black));
        textView.setText(msg);
        ((ImageView) view2.findViewById(R.id.check_word_response_image)).setImageResource(imageResourceId);
        this.onAnswerToast.show();
    }
}
