package au.com.xandar.jumblee.scoreloop;

public final class ScoreloopDialog {
    public static final int DIALOG_ERROR_EMAIL_ALREADY_TAKEN = 8;
    public static final int DIALOG_ERROR_INVALID_EMAIL_FORMAT = 10;
    public static final int DIALOG_ERROR_NAME_ALREADY_TAKEN = 11;
    public static final int DIALOG_ERROR_NETWORK = 3;
    public static final int DIALOG_ERROR_NOT_ON_HIGHSCORE_LIST = 1;
    public static final int DIALOG_ERROR_REQUEST_CANCELLED = 2;
    public static final int DIALOG_PROGRESS_LOADING_SCORES = 15;
    public static final int DIALOG_PROGRESS_LOADING_USER = 13;
    public static final int DIALOG_PROGRESS_UPDATING_USER = 14;
}
