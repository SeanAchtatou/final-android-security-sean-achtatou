package au.com.xandar.jumblee.jumblefinder;

import android.util.Log;
import au.com.xandar.jumblee.common.AppConstants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

final class DictionaryReader {
    private static final int CARRIAGE_RETURN = 13;
    private static final int EOF = -1;
    private static final int LINE_FEED = 10;
    private boolean atEOF = false;
    private int nrLinesRead = 0;
    private int nrLinesToDiscard;
    private final BufferedReader reader;

    DictionaryReader(int nrLinesToDiscard2, InputStream stream) {
        this.nrLinesToDiscard = nrLinesToDiscard2;
        this.reader = new BufferedReader(new InputStreamReader(stream), AppConstants.HTTP_CONNECTION_SIZE_BYTES);
    }

    public void close() {
        try {
            this.reader.close();
        } catch (IOException e) {
            Log.e(AppConstants.TAG, "Could not close Dictionary");
        }
    }

    public boolean readWord(StringBuilder word) {
        while (this.nrLinesToDiscard > 0) {
            try {
                if (!readLine(word)) {
                    return false;
                }
            } catch (IOException e) {
                Log.e(AppConstants.TAG, "Failed to read (and discard) line " + this.nrLinesRead);
            }
        }
        try {
            return readLine(word);
        } catch (IOException e2) {
            Log.e(AppConstants.TAG, "Failed to read line " + this.nrLinesRead);
            return false;
        }
    }

    public int getNrLinesRead() {
        return this.nrLinesRead;
    }

    private boolean readLine(StringBuilder word) throws IOException {
        if (this.atEOF) {
            return false;
        }
        word.setLength(0);
        this.nrLinesRead++;
        this.nrLinesToDiscard--;
        while (true) {
            int chr = this.reader.read();
            if (chr == EOF) {
                this.atEOF = true;
                return word.length() > 0;
            } else if (chr != 13) {
                if (chr == 10) {
                    return true;
                }
                word.append((char) chr);
            }
        }
    }
}
