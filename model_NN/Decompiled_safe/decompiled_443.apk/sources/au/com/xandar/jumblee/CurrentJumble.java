package au.com.xandar.jumblee;

import au.com.xandar.jumblee.common.AppConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class CurrentJumble implements Serializable {
    public static final CurrentJumble NO_CURRENT_JUMBLE = new CurrentJumble("", "", Collections.emptyList());
    private boolean currentJumble;
    private long dateStartedAsMillis;
    private List<String> foundWords = Collections.emptyList();
    private Integer id;
    private long millisRemaining;
    private List<String> possibleWords = Collections.emptyList();
    private final String specialLetter;
    private List<String> standardLetters;
    private final String word;

    public CurrentJumble(String word2, String specialLetter2, List<String> possibleWords2) {
        this.word = word2;
        this.specialLetter = specialLetter2;
        this.possibleWords = possibleWords2;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id2) {
        this.id = id2;
    }

    public String getWord() {
        return this.word;
    }

    public String getSpecialLetter() {
        return this.specialLetter;
    }

    public long getDateStartedAsMillis() {
        return this.dateStartedAsMillis;
    }

    public void setDateStartedAsMillis(long dateStartedAsMillis2) {
        this.dateStartedAsMillis = dateStartedAsMillis2;
    }

    public long getMillisRemaining() {
        return this.millisRemaining;
    }

    public void setMillisRemaining(long millisRemaining2) {
        this.millisRemaining = millisRemaining2;
    }

    public boolean isCurrentJumble() {
        return this.currentJumble;
    }

    public void setCurrentJumble(boolean currentJumble2) {
        this.currentJumble = currentJumble2;
    }

    public long getMillisTaken() {
        return (60000 + (((long) getNrFoundWords()) * AppConstants.ACCEPTED_WORD_BONUS_MILLIS)) - this.millisRemaining;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public double getWordsPerMinute() {
        return ((double) (((float) getNrFoundWords()) * 1.0f)) / ((double) Math.max((((float) getMillisTaken()) * 1.0f) / 60000.0f, 1.0f));
    }

    public double getScore() {
        return getPercentageFound() * Math.sqrt(getWordsPerMinute()) * 2.0d;
    }

    public List<String> getStandardLetters() {
        if (this.standardLetters == null) {
            this.standardLetters = new ArrayList();
            String otherChars = this.word.replaceFirst(this.specialLetter, "");
            for (int i = 0; i < otherChars.length(); i++) {
                this.standardLetters.add(otherChars.substring(i, i + 1));
            }
            Collections.shuffle(this.standardLetters);
        }
        return this.standardLetters;
    }

    public List<String> getPossibleWords() {
        return this.possibleWords;
    }

    public void setPossibleWords(List<String> possibleWords2) {
        this.possibleWords = possibleWords2;
    }

    public int getNrPossibleWords() {
        return this.possibleWords.size();
    }

    public List<String> getFoundWords() {
        return this.foundWords;
    }

    public void setFoundWords(List<String> foundWords2) {
        this.foundWords = foundWords2;
    }

    public int getNrFoundWords() {
        return this.foundWords.size();
    }

    public double getPercentageFound() {
        return 100.0d * getPercentageFoundAsDecimal();
    }

    private double getPercentageFoundAsDecimal() {
        return (double) ((1.0f * ((float) getNrFoundWords())) / ((float) getNrPossibleWords()));
    }

    public String toString() {
        return "CurrentJumble{\n  specialLetter='" + this.specialLetter + "'\n" + "  word='" + this.word + "'\n" + "  foundWords=" + this.foundWords + "\n" + "  nrPossibleWords=" + this.possibleWords.size() + "\n" + "  possibleWords=" + this.possibleWords + "\n}";
    }
}
