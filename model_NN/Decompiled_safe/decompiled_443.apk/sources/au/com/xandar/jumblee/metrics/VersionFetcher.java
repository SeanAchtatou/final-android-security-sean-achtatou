package au.com.xandar.jumblee.metrics;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import au.com.xandar.android.net.ConnectionChecker;
import au.com.xandar.jumblee.JumbleeApplication;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.common.JumbleeHandledException;
import au.com.xandar.jumblee.net.IntentExecutable;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;
import au.com.xandar.jumblee.prefs.LatestVersion;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.acra.ACRA;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class VersionFetcher implements IntentExecutable {
    public static final String EXECUTABLE_NAME_VALUE = "versionFetcher";
    public static final String ONLY_CHECK_ONCE_A_DAY = "onlyCheckOncePerDay";

    private static class LatestVersionResponseProcessor implements ResponseHandler<LatestVersion> {
        private static final String DATE_PUBLISHED = "datePublished";
        private static final String VERSION_CODE = "versionCode";
        private static final String VERSION_NAME = "versionName";

        private LatestVersionResponseProcessor() {
        }

        /* JADX INFO: finally extract failed */
        public LatestVersion handleResponse(HttpResponse response) throws IOException {
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() != 200) {
                throw new IOException(statusLine.getStatusCode() + " : " + statusLine.getReasonPhrase());
            }
            Map<String, String> values = new HashMap<>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            while (true) {
                try {
                    String line = reader.readLine();
                    if (line == null) {
                        reader.close();
                        String versionCode = (String) values.get(VERSION_CODE);
                        String versionName = (String) values.get(VERSION_NAME);
                        String datePublished = (String) values.get(DATE_PUBLISHED);
                        if (versionCode == null) {
                            throw new IOException("Response missing versionCode");
                        } else if (versionName == null) {
                            throw new IOException("Response missing versionName");
                        } else if (datePublished == null) {
                            throw new IOException("Response missing DatePublished");
                        } else {
                            LatestVersion latestVersion = new LatestVersion();
                            latestVersion.setVersionCode(Integer.valueOf(versionCode).intValue());
                            latestVersion.setVersionName(versionName);
                            latestVersion.setDatePublished(datePublished);
                            return latestVersion;
                        }
                    } else {
                        int delimiter = line.indexOf(61);
                        if (delimiter == -1) {
                            throw new IOException("Could not find delimiter for line '" + line + "'");
                        }
                        values.put(line.substring(0, delimiter), line.substring(delimiter + 1));
                    }
                } catch (Throwable th) {
                    reader.close();
                    throw th;
                }
            }
        }
    }

    public void execute(Application application, Intent intent) {
        JumbleeApplication jumbleeApplication = (JumbleeApplication) application;
        ApplicationPreferences prefs = jumbleeApplication.getApplicationPreferences();
        if (intent.hasExtra(ONLY_CHECK_ONCE_A_DAY) && intent.getBooleanExtra(ONLY_CHECK_ONCE_A_DAY, false)) {
            if (!(prefs.getNextOncePerDayCheck() < System.currentTimeMillis())) {
                return;
            }
        }
        if (new ConnectionChecker(jumbleeApplication).hasConnection()) {
            try {
                prefs.setLatestVersion(retrieveCurrentVersionFromWebsite());
                prefs.setNextOncePerDayCheck(System.currentTimeMillis() + AppConstants.NR_MILLIS_IN_A_DAY);
            } catch (IllegalArgumentException e) {
                IllegalArgumentException e2 = e;
                ACRA.getErrorReporter().handleSilentException(new JumbleeHandledException(e2.getMessage(), e2));
            } catch (IOException e3) {
                Log.e(AppConstants.TAG, "Not updating versionCode - failed to retrieve response from : http://jumblee.xandar.com.au/latestVersion.txt", e3);
            }
        }
    }

    private LatestVersion retrieveCurrentVersionFromWebsite() throws IOException {
        HttpParams httpConnectionParams = new BasicHttpParams();
        HttpConnectionParams.setSocketBufferSize(httpConnectionParams, AppConstants.HTTP_CONNECTION_SIZE_BYTES);
        return (LatestVersion) new DefaultHttpClient(httpConnectionParams).execute(new HttpGet(AppConstants.LATEST_VERSION_URL), new LatestVersionResponseProcessor());
    }
}
