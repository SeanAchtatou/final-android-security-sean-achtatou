package au.com.xandar.jumblee;

public interface Jumble {
    int getNrPossibleWords();

    String getPossibleWordsAsCommaSeparatedString();

    String getSpecialLetter();

    String getWord();
}
