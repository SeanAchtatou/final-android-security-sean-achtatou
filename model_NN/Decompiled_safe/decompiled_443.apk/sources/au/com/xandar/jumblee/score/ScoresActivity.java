package au.com.xandar.jumblee.score;

import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.db.ScoresCursor;

public final class ScoresActivity extends JumbleeActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.scores_screen);
        ScoresCursor scoresCursor = getJumbleeDB().getScoresCursor();
        startManagingCursor(scoresCursor);
        ((ListView) findViewById(R.id.scoreGrid)).setAdapter((ListAdapter) new ScoresCursorAdapter(this, scoresCursor));
    }
}
