package au.com.xandar.jumblee.game;

import android.os.Vibrator;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public class PreferencedVibrator {
    private final ApplicationPreferences prefs;
    private final Vibrator vibrator;

    public PreferencedVibrator(Vibrator vibrator2, ApplicationPreferences prefs2) {
        this.vibrator = vibrator2;
        this.prefs = prefs2;
    }

    public void vibrate(long milliseconds) {
        if (this.prefs.getHapticFeedBack()) {
            this.vibrator.vibrate(milliseconds);
        }
    }

    public void vibrate(long[] pattern, int repeat) {
        if (this.prefs.getHapticFeedBack()) {
            this.vibrator.vibrate(pattern, repeat);
        }
    }

    public void cancel() {
        this.vibrator.cancel();
    }
}
