package au.com.xandar.jumblee.facebook;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import au.com.xandar.android.facebook.AuthenticationListener;
import au.com.xandar.android.facebook.DialogError;
import au.com.xandar.android.facebook.Facebook;
import au.com.xandar.android.facebook.FacebookException;
import au.com.xandar.android.facebook.SessionStore;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public final class FacebookPreferencesHandler {
    /* access modifiers changed from: private */
    public final CheckBox authenticatedCheckBox;
    /* access modifiers changed from: private */
    public final Activity context;
    /* access modifiers changed from: private */
    public final Facebook facebook;
    private final Runnable onAuthenticationFinish;
    private final Runnable onAuthenticationStart;
    /* access modifiers changed from: private */
    public final CheckBox postScoresCheckBox;
    private final TextView postScoresTextView;
    /* access modifiers changed from: private */
    public final ApplicationPreferences preferences;

    public FacebookPreferencesHandler(Activity context2, Facebook facebook2, ApplicationPreferences preferences2, View layout, Runnable onAuthenticationStart2, Runnable onAuthenticationFinish2) {
        this.context = context2;
        this.facebook = facebook2;
        this.preferences = preferences2;
        this.onAuthenticationStart = onAuthenticationStart2;
        this.onAuthenticationFinish = onAuthenticationFinish2;
        this.authenticatedCheckBox = (CheckBox) layout.findViewById(R.id.facebook_authenticated);
        this.postScoresCheckBox = (CheckBox) layout.findViewById(R.id.facebook_postScores);
        this.postScoresTextView = (TextView) layout.findViewById(R.id.facebook_postScoresText);
        this.authenticatedCheckBox.setOnClickListener(getAuthenticatedCheckBoxHandler());
        this.postScoresCheckBox.setOnClickListener(getPostScoreCheckBoxHandler());
        facebook2.setAuthenticationListener(new FacebookAuthenticationListener());
    }

    public void populateViews() {
        this.authenticatedCheckBox.setChecked(this.facebook.isSessionValid());
        this.postScoresCheckBox.setChecked(this.preferences.getFacebook_postScores());
        enableViews();
    }

    /* access modifiers changed from: private */
    public void enableViews() {
        boolean postScoresEnabled = this.facebook.isSessionValid() || this.postScoresCheckBox.isChecked();
        this.postScoresCheckBox.setEnabled(postScoresEnabled);
        this.postScoresTextView.setEnabled(postScoresEnabled);
    }

    private View.OnClickListener getAuthenticatedCheckBoxHandler() {
        return new View.OnClickListener() {
            public void onClick(View view) {
                if (FacebookPreferencesHandler.this.authenticatedCheckBox.isChecked()) {
                    FacebookPreferencesHandler.this.authenticatedCheckBox.setChecked(!FacebookPreferencesHandler.this.authenticatedCheckBox.isChecked());
                    FacebookPreferencesHandler.this.notifyListenerOfAuthenticationStart();
                    FacebookPreferencesHandler.this.facebook.authorize(FacebookPreferencesHandler.this.context);
                    return;
                }
                FacebookPreferencesHandler.this.facebook.clearSession();
                SessionStore.save(FacebookPreferencesHandler.this.facebook, FacebookPreferencesHandler.this.context);
                FacebookPreferencesHandler.this.enableViews();
            }
        };
    }

    private View.OnClickListener getPostScoreCheckBoxHandler() {
        return new View.OnClickListener() {
            public void onClick(View view) {
                FacebookPreferencesHandler.this.preferences.setFacebook_postScores(FacebookPreferencesHandler.this.postScoresCheckBox.isChecked());
                FacebookPreferencesHandler.this.preferences.setFacebook_haveAskedAboutIntegration(true);
                FacebookPreferencesHandler.this.enableViews();
            }
        };
    }

    /* access modifiers changed from: private */
    public void notifyListenerOfAuthenticationStart() {
        if (this.onAuthenticationStart != null) {
            this.onAuthenticationStart.run();
        }
    }

    /* access modifiers changed from: private */
    public void notifyListenerOfAuthenticationFinish() {
        if (this.onAuthenticationFinish != null) {
            this.onAuthenticationFinish.run();
        }
        this.context.removeDialog(100);
    }

    private final class FacebookAuthenticationListener implements AuthenticationListener {
        private FacebookAuthenticationListener() {
        }

        public void onComplete(Bundle values) {
            SessionStore.save(FacebookPreferencesHandler.this.facebook, FacebookPreferencesHandler.this.context);
            FacebookPreferencesHandler.this.authenticatedCheckBox.setChecked(FacebookPreferencesHandler.this.facebook.isSessionValid());
            if (FacebookPreferencesHandler.this.facebook.isSessionValid()) {
                FacebookPreferencesHandler.this.postScoresCheckBox.setChecked(true);
                FacebookPreferencesHandler.this.preferences.setFacebook_postScores(true);
            }
            FacebookPreferencesHandler.this.enableViews();
            FacebookPreferencesHandler.this.notifyListenerOfAuthenticationFinish();
        }

        public void onFacebookError(FacebookException error) {
            FacebookPreferencesHandler.this.notifyListenerOfAuthenticationFinish();
        }

        public void onError(DialogError e) {
            FacebookPreferencesHandler.this.notifyListenerOfAuthenticationFinish();
            Toast toast = Toast.makeText(FacebookPreferencesHandler.this.context, e.getMessage(), 1);
            toast.setGravity(17, 0, 0);
            toast.show();
        }

        public void onCancel() {
            FacebookPreferencesHandler.this.notifyListenerOfAuthenticationFinish();
        }
    }
}
