package au.com.xandar.jumblee.jumblefinder;

import au.com.xandar.jumblee.Jumble;
import java.util.ArrayList;
import java.util.List;

public final class MutableJumble implements Jumble {
    private final String jumbleWord;
    private final StringBuilder nineLetterWord = new StringBuilder(15);
    private final List<String> possibleWords = new ArrayList(200);
    private final StringBuilder possibleWordsBuilder = new StringBuilder(2048);
    private final String specialLetter;

    public MutableJumble(String jumbleWord2, String specialLetter2) {
        this.jumbleWord = jumbleWord2;
        this.specialLetter = specialLetter2;
    }

    public String getWord() {
        return this.jumbleWord;
    }

    public String getSpecialLetter() {
        return this.specialLetter;
    }

    public boolean couldMakeWord(CharSequence word) {
        if (findLetter(word, this.specialLetter.charAt(0)) == -1) {
            return false;
        }
        this.nineLetterWord.setLength(0);
        this.nineLetterWord.append(this.jumbleWord);
        for (int i = 0; i < word.length(); i++) {
            int offsetInNineLetterWord = findLetter(this.nineLetterWord, word.charAt(i));
            if (offsetInNineLetterWord == -1) {
                return false;
            }
            this.nineLetterWord.deleteCharAt(offsetInNineLetterWord);
        }
        return true;
    }

    private int findLetter(CharSequence word, char letter) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == letter) {
                return i;
            }
        }
        return -1;
    }

    public void addPossibleWord(CharSequence word) {
        if (this.possibleWordsBuilder.length() > 0) {
            this.possibleWordsBuilder.append(',');
        }
        String wordAsString = word.toString();
        this.possibleWordsBuilder.append(wordAsString);
        this.possibleWords.add(wordAsString);
    }

    public List<String> getPossibleWords() {
        return this.possibleWords;
    }

    public String getPossibleWordsAsCommaSeparatedString() {
        return this.possibleWordsBuilder.toString();
    }

    public int getNrPossibleWords() {
        return this.possibleWords.size();
    }

    public String toString() {
        return "MutableJumble{word='" + this.jumbleWord + "' specialLetter='" + this.specialLetter + "'}";
    }
}
