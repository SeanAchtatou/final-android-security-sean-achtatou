package au.com.xandar.jumblee.game;

public final class GameEvent {
    public static final int CLOCK_FINISHED = 11;
    public static final int CLOCK_TICK = 10;
    public static final int GAME_OVER = 3;
    public static final int GAME_START = 2;
    public static final int LETTER_SELECTED = 4;
    public static final int WORD_ACCEPTED = 5;
    public static final int WORD_ACCEPTED_NINE_LETTER = 6;
    public static final int WORD_CLEARED = 9;
    public static final int WORD_REJECTED = 7;
    public static final int WORD_REJECTED_ALREADY_FOUND = 8;
}
