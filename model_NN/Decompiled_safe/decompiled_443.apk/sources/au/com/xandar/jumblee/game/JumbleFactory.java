package au.com.xandar.jumblee.game;

import android.content.Context;
import android.content.Intent;
import au.com.xandar.jumblee.CurrentJumble;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.db.JumbleHolder;
import au.com.xandar.jumblee.db.JumbleeDB;
import au.com.xandar.jumblee.jumblefinder.JumbleFinder;
import au.com.xandar.jumblee.jumblefinder.JumbleFinderService;
import au.com.xandar.jumblee.jumblefinder.MutableJumble;

public final class JumbleFactory {
    private final Context context;
    private final JumbleeDB jumbleeDB;

    public JumbleFactory(Context context2, JumbleeDB jumbleeDB2) {
        this.context = context2;
        this.jumbleeDB = jumbleeDB2;
    }

    public CurrentJumble getNewJumble() {
        JumbleHolder holder = this.jumbleeDB.getNewJumble();
        if (holder.getJumble() != null) {
            CurrentJumble jumble = holder.getJumble();
            MutableJumble mutableJumble = new MutableJumble(jumble.getWord(), jumble.getSpecialLetter());
            new JumbleFinder().findWordsForJumbles(this.context.getResources().openRawResource(R.raw.dictionary_4to9_letter_words), mutableJumble);
            this.jumbleeDB.updateJumbleWords(jumble.getId(), mutableJumble);
            jumble.setPossibleWords(mutableJumble.getPossibleWords());
        }
        if (holder.getNrJumbles() < 25) {
            this.context.startService(new Intent(this.context, JumbleFinderService.class));
        }
        return holder.getJumble();
    }
}
