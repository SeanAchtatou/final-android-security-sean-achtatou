package au.com.xandar.jumblee.scoreloop;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import au.com.xandar.jumblee.JumbleeApplication;
import au.com.xandar.jumblee.db.AbbreviatedScore;
import au.com.xandar.jumblee.db.JumbleeDB;
import java.util.List;

public final class ScoreloopPosterService extends Service {
    private static final int STICKY_SERVICE = 1;
    private ScoreSubmitter scoreSubmitter;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        handleCommand();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand();
        return 1;
    }

    private synchronized void handleCommand() {
        List<AbbreviatedScore> scoresToBePostedToScoreloop = getJumbleeDB().getScoresToBePostedToScoreloop();
        if (scoresToBePostedToScoreloop.isEmpty()) {
            shutdownService();
        } else {
            getScoreSubmitter().submitScores(scoresToBePostedToScoreloop);
        }
    }

    private JumbleeDB getJumbleeDB() {
        return ((JumbleeApplication) getApplication()).getJumbleeDB();
    }

    private ScoreSubmitter getScoreSubmitter() {
        if (this.scoreSubmitter == null) {
            this.scoreSubmitter = new ScoreSubmitter(((JumbleeApplication) getApplication()).getScoreloopSession(), getJumbleeDB(), new Runnable() {
                public void run() {
                    ScoreloopPosterService.this.shutdownService();
                }
            });
        }
        return this.scoreSubmitter;
    }

    /* access modifiers changed from: private */
    public void shutdownService() {
        stopSelf();
    }
}
