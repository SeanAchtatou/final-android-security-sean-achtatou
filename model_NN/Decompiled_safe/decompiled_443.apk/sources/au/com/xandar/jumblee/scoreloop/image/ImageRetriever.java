package au.com.xandar.jumblee.scoreloop.image;

import android.graphics.drawable.Drawable;
import au.com.xandar.android.os.NewThreadExecutor;
import au.com.xandar.jumblee.db.JumbleeDB;
import au.com.xandar.jumblee.db.ScoreloopImage;
import com.scoreloop.client.android.core.model.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;

public final class ImageRetriever {
    private final Executor executor = new NewThreadExecutor();
    /* access modifiers changed from: private */
    public final JumbleeDB jumbleeDB;

    public ImageRetriever(JumbleeDB jumbleeDB2) {
        this.jumbleeDB = jumbleeDB2;
    }

    public ScoreloopImage getScoreloopImage(User user) {
        String userId = user.getIdentifier();
        String login = user.getLogin();
        String scoreloopImageUrl = ImageUrlHelper.getImageUrl(user);
        if (scoreloopImageUrl == null) {
            return null;
        }
        final ScoreloopImage cachedImage = this.jumbleeDB.getScoreloopImage(userId);
        if (cachedImage == null) {
            return null;
        }
        this.executor.execute(new Runnable() {
            public void run() {
                ImageRetriever.this.jumbleeDB.updateScoreloopImageLastRead(Long.valueOf(cachedImage.getId()));
            }
        });
        byte[] image = cachedImage.getImageBytes();
        if (image.length == 0) {
            return cachedImage;
        }
        InputStream stream = new ByteArrayInputStream(image);
        cachedImage.setDrawable(Drawable.createFromStream(stream, scoreloopImageUrl));
        try {
            stream.close();
        } catch (IOException e) {
        }
        return cachedImage;
    }
}
