package au.com.xandar.jumblee.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import au.com.xandar.jumblee.R;

final class DialogFactory {
    private boolean cancelable;
    private final Context context;
    private CharSequence message;
    private Integer messageGravity;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener negativeButtonClickListener;
    private CharSequence negativeButtonText;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener neutralButtonClickListener;
    private CharSequence neutralButtonText;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener positiveButtonClickListener;
    private CharSequence positiveButtonText;
    private CharSequence title;

    public DialogFactory(Context context2) {
        this.context = context2;
    }

    public void setTitle(int title2) {
        this.title = this.context.getText(title2);
    }

    public void setMessage(int message2) {
        this.message = this.context.getText(message2);
    }

    public void setMessage(CharSequence message2) {
        this.message = message2;
    }

    public void setMessageGravity(Integer messageGravity2) {
        this.messageGravity = messageGravity2;
    }

    public void setPositiveButton(int buttonText, DialogInterface.OnClickListener listener) {
        this.positiveButtonText = this.context.getText(buttonText);
        this.positiveButtonClickListener = listener;
    }

    public void setNeutralButton(int buttonText, DialogInterface.OnClickListener listener) {
        this.neutralButtonText = this.context.getText(buttonText);
        this.neutralButtonClickListener = listener;
    }

    public void setNegativeButton(int buttonText, DialogInterface.OnClickListener listener) {
        this.negativeButtonText = this.context.getText(buttonText);
        this.negativeButtonClickListener = listener;
    }

    public void setCancelable(boolean cancelable2) {
        this.cancelable = cancelable2;
    }

    public Dialog create() {
        final Dialog dialog = new Dialog(this.context, R.style.DialogTheme);
        View layout = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.dialog_generic_3button, (ViewGroup) null);
        TextView titleView = (TextView) layout.findViewById(R.id.title);
        if (this.title == null) {
            titleView.setVisibility(8);
        } else {
            titleView.setText(this.title);
        }
        Button positiveButton = (Button) layout.findViewById(R.id.positiveButton);
        if (this.positiveButtonText != null) {
            positiveButton.setText(this.positiveButtonText);
            if (this.positiveButtonClickListener != null) {
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        DialogFactory.this.positiveButtonClickListener.onClick(dialog, -1);
                    }
                });
            }
        } else {
            positiveButton.setVisibility(8);
        }
        Button neutralButton = (Button) layout.findViewById(R.id.neutralButton);
        if (this.neutralButtonText != null) {
            neutralButton.setText(this.neutralButtonText);
            if (this.neutralButtonClickListener != null) {
                neutralButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        DialogFactory.this.neutralButtonClickListener.onClick(dialog, -3);
                    }
                });
            }
        } else {
            neutralButton.setVisibility(8);
        }
        Button negativeButton = (Button) layout.findViewById(R.id.negativeButton);
        if (this.negativeButtonText != null) {
            negativeButton.setText(this.negativeButtonText);
            if (this.negativeButtonClickListener != null) {
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        DialogFactory.this.negativeButtonClickListener.onClick(dialog, -2);
                    }
                });
            }
        } else {
            negativeButton.setVisibility(8);
        }
        if (this.message != null) {
            TextView messageView = (TextView) layout.findViewById(R.id.message);
            messageView.setText(this.message);
            if (this.messageGravity != null) {
                messageView.setGravity(this.messageGravity.intValue());
            }
        }
        dialog.addContentView(layout, new ViewGroup.LayoutParams(-1, -2));
        dialog.setCancelable(this.cancelable);
        dialog.setCanceledOnTouchOutside(this.cancelable);
        return dialog;
    }
}
