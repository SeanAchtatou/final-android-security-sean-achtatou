package au.com.xandar.jumblee.game.widget;

import android.os.Bundle;
import android.os.Parcelable;
import android.widget.TextView;

final class TextViewStateRestorer {
    private static final String ENABLED = "enabled";
    private static final String SUPERSTATE = "superState";
    private static final String TEXT = "text";
    private static final String VISIBILITY = "visibility";

    TextViewStateRestorer() {
    }

    public final Parcelable saveInstanceState(TextView view, Parcelable superState) {
        Bundle viewState = new Bundle();
        viewState.putInt(VISIBILITY, view.getVisibility());
        viewState.putBoolean(ENABLED, view.isEnabled());
        viewState.putString(TEXT, view.getText().toString());
        viewState.putParcelable(SUPERSTATE, superState);
        return viewState;
    }

    public final Parcelable restoreInstanceState(TextView view, Parcelable state) {
        Bundle viewState = (Bundle) state;
        view.setVisibility(viewState.getInt(VISIBILITY));
        view.setEnabled(viewState.getBoolean(ENABLED));
        view.setText(viewState.getString(TEXT));
        return viewState.getParcelable(SUPERSTATE);
    }
}
