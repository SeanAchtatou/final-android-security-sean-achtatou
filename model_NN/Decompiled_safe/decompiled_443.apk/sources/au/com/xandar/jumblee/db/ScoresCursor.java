package au.com.xandar.jumblee.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import au.com.xandar.android.database.DelegateCursor;
import au.com.xandar.jumblee.db.JumbleeDBOpenHelper;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class ScoresCursor extends DelegateCursor {
    private static final int FLOAT_FIELD = 2;
    private static final int INTEGER_FIELD = 1;
    private final Calendar dateCompleted = Calendar.getInstance();
    private final Format dateFormat = new SimpleDateFormat("d-MMM-yy");
    private final Format percentageFormat = new DecimalFormat("##0'%'");
    private final Format rateFormat = new DecimalFormat("##0.00");
    private final Format scoreFormat = new DecimalFormat("###0.00");
    private final Format timeFormat = new SimpleDateFormat("h:mmaa");

    private ScoresCursor(Cursor cursor) {
        super(cursor);
    }

    public Date getDateCompleted() {
        this.dateCompleted.setTimeInMillis(getLong(getColumnIndex("date_completed")));
        return this.dateCompleted.getTime();
    }

    public String getTimeCompletedAsString() {
        return this.timeFormat.format(getDateCompleted());
    }

    public String getDateCompletedAsString() {
        return this.dateFormat.format(getDateCompleted());
    }

    public int getNrFound() {
        return getInt(getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.NR_FOUND_WORDS));
    }

    public String getNrFoundAsString() {
        return "" + getNrFound();
    }

    public float getPercentageFound() {
        return getFloat(getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.PERCENTAGE_WORDS_FOUND));
    }

    public String getPercentageFoundAsString() {
        return this.percentageFormat.format(Float.valueOf(getPercentageFound()));
    }

    public float getWordsPerMinute() {
        return getFloat(getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.WORDS_PER_MINUTE));
    }

    public String getWordsPerMinuteAsString() {
        return this.rateFormat.format(Float.valueOf(getWordsPerMinute()));
    }

    public float getScore() {
        return getFloat(getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.SCORE));
    }

    public String getScoreAsString() {
        return this.scoreFormat.format(Float.valueOf(getScore()));
    }

    public int getType(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 1:
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 2;
            case 5:
                return 2;
            default:
                throw new IllegalArgumentException("Invalid column: " + columnIndex + " - cursor only has 6 columns");
        }
    }

    public static ScoresCursor getScores(SQLiteDatabase db) {
        return new ScoresCursor(db.query(JumbleeDBOpenHelper.ScoreTable.NAME, new String[]{"_id", "date_completed", JumbleeDBOpenHelper.ScoreDBFields.NR_FOUND_WORDS, JumbleeDBOpenHelper.ScoreDBFields.PERCENTAGE_WORDS_FOUND, JumbleeDBOpenHelper.ScoreDBFields.WORDS_PER_MINUTE, JumbleeDBOpenHelper.ScoreDBFields.SCORE}, null, null, null, null, "date_completed desc"));
    }
}
