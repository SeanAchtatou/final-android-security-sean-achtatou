package au.com.xandar.jumblee.prefs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import au.com.xandar.android.facebook.Facebook;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.JumbleeApplication;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.dialog.DialogId;
import au.com.xandar.jumblee.facebook.FacebookPreferencesHandler;

public final class PreferencesActivity extends JumbleeActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.preferences);
        setupSeekBars();
        setupHandlers();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        getAnalytics().startTracking();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getAnalytics().stopTracking();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 32665) {
            getFacebook().authorizeCallback(this, requestCode, resultCode, data);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch (id) {
            case DialogId.FACEBOOK_AUTHENTICATION_DIALOG:
                dialog = getFacebook().createAuthenticationDialog(this);
                break;
            case 101:
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.requestWindowFeature(1);
                progressDialog.setMessage("Loading...");
                dialog = progressDialog;
                break;
            default:
                Log.e(AppConstants.TAG, "Unknown Dialog ID : " + id);
                return super.onCreateDialog(id);
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case DialogId.FACEBOOK_AUTHENTICATION_DIALOG:
                getFacebook().prepareAuthenticationDialog(dialog);
                return;
            default:
                return;
        }
    }

    private void setupHandlers() {
        CheckBox hapticCheckBox = (CheckBox) findViewById(R.id.checkbox_hapticFeedback);
        hapticCheckBox.setChecked(getApplicationPreferences().getHapticFeedBack());
        hapticCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                PreferencesActivity.this.getApplicationPreferences().setHapticFeedBack(value);
            }
        });
        CheckBox soundCheckBox = (CheckBox) findViewById(R.id.checkbox_sound);
        soundCheckBox.setChecked(getApplicationPreferences().getSoundEffectOn());
        soundCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                PreferencesActivity.this.getApplicationPreferences().setSoundEffectOn(value);
            }
        });
        CheckBox confirmationDialogOnEndGame = (CheckBox) findViewById(R.id.checkbox_confirmationDialogOnEndGame);
        confirmationDialogOnEndGame.setChecked(getApplicationPreferences().getShowConfirmationDialogOnEndGame());
        confirmationDialogOnEndGame.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                PreferencesActivity.this.getApplicationPreferences().setShowConfirmationDialogOnEndGame(value);
            }
        });
        CheckBox hideWordAcceptedCheckBox = (CheckBox) findViewById(R.id.checkbox_hideWordAccepted);
        hideWordAcceptedCheckBox.setChecked(getApplicationPreferences().getHideWordAcceptedMessage());
        hideWordAcceptedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                PreferencesActivity.this.getApplicationPreferences().setHideWordAcceptedMessage(value);
            }
        });
        CheckBox hideWordRejectedCheckBox = (CheckBox) findViewById(R.id.checkbox_hideWordRejected);
        hideWordRejectedCheckBox.setChecked(getApplicationPreferences().getHideWordRejectedMessage());
        hideWordRejectedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                PreferencesActivity.this.getApplicationPreferences().setHideWordRejectedMessage(value);
            }
        });
        CheckBox hideWordAlreadyFoundCheckBox = (CheckBox) findViewById(R.id.checkbox_hideWordAlreadyFound);
        hideWordAlreadyFoundCheckBox.setChecked(getApplicationPreferences().getHideWordAlreadyFoundMessage());
        hideWordAlreadyFoundCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                PreferencesActivity.this.getApplicationPreferences().setHideWordAlreadyFoundMessage(value);
            }
        });
        ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(this, R.array.download_score_image_options, 17367048);
        adapter.setDropDownViewResource(17367049);
        Spinner downloadScoreImagesSpinner = (Spinner) findViewById(R.id.download_score_images_spinner);
        downloadScoreImagesSpinner.setAdapter((SpinnerAdapter) adapter);
        downloadScoreImagesSpinner.setSelection(getApplicationPreferences().getDownloadGlobalScoreImages());
        downloadScoreImagesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                PreferencesActivity.this.getApplicationPreferences().setDownloadGlobalScoreImages(position);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        new FacebookPreferencesHandler(this, getFacebook(), getApplicationPreferences(), findViewById(16908290), null, null).populateViews();
    }

    private void setupSeekBars() {
        final int startingVolume = (int) (getApplicationPreferences().getSoundEffectVolume() * 1000.0f);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekbar_sound);
        seekBar.setMax(1000);
        seekBar.setProgress(startingVolume);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private int barProgress = startingVolume;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                this.barProgress = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                PreferencesActivity.this.getApplicationPreferences().setSoundEffectVolume(((float) this.barProgress) / 1000.0f);
            }
        });
    }

    private Facebook getFacebook() {
        return ((JumbleeApplication) getApplication()).getFacebook();
    }
}
