package au.com.xandar.jumblee.scoreloop;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import au.com.xandar.android.os.SimpleAsyncTask;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.scoreloop.image.Base64;
import au.com.xandar.jumblee.scoreloop.image.ImageDatabaseCacher;
import au.com.xandar.jumblee.scoreloop.image.ImageDownloadGate;
import au.com.xandar.jumblee.scoreloop.image.ImageHelper;
import au.com.xandar.jumblee.scoreloop.image.ImageRetriever;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.ImageSource;
import com.scoreloop.client.android.core.model.User;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public final class GlobalProfileActivity extends BaseActivity {
    private static final int ACTIVITY_REQUEST_CODE_CHOOSE_IMAGE = 10;
    private static final int IMAGE_SIZE = 144;
    private Button changePictureButton;
    /* access modifiers changed from: private */
    public ImageView currentImageView;
    /* access modifiers changed from: private */
    public Drawable currentUserImage;
    /* access modifiers changed from: private */
    public boolean dataDirty = false;
    private View discardChangesGroup;
    /* access modifiers changed from: private */
    public EditText emailView;
    /* access modifiers changed from: private */
    public ImageDatabaseCacher imageDatabaseCacher;
    /* access modifiers changed from: private */
    public int lastProgressDialogDisplayed;
    /* access modifiers changed from: private */
    public EditText loginView;
    /* access modifiers changed from: private */
    public Bitmap newImageBitmap;
    /* access modifiers changed from: private */
    public Uri newImageUri;
    /* access modifiers changed from: private */
    public ImageView newImageView;
    private View newProfileImageGroup;
    private View showScoresGroup;
    private TextWatcher textChangedListener;
    private View updateProfileGroup;
    /* access modifiers changed from: private */
    public UserController userController;

    private class UserUpdateObserver implements RequestControllerObserver {
        private UserUpdateObserver() {
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            if (!GlobalProfileActivity.this.isFinishing()) {
                final User user = GlobalProfileActivity.this.getScoreloopSession().getUser();
                new SimpleAsyncTask() {
                    /* access modifiers changed from: protected */
                    public void doInBackground() {
                        Drawable updatedImage = GlobalProfileActivity.this.imageDatabaseCacher.getMostRecentImageFromScoreloop(user, new ImageRetriever(GlobalProfileActivity.this.getJumbleeDB()).getScoreloopImage(user));
                        if (updatedImage != null) {
                            Drawable unused = GlobalProfileActivity.this.currentUserImage = updatedImage;
                        } else {
                            Drawable unused2 = GlobalProfileActivity.this.currentUserImage = GlobalProfileActivity.this.getResources().getDrawable(R.drawable.sl_icon_user);
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute() {
                        GlobalProfileActivity.this.currentImageView.setImageDrawable(GlobalProfileActivity.this.currentUserImage);
                    }
                }.execute();
                GlobalProfileActivity.this.dismissDialogSafely(GlobalProfileActivity.this.lastProgressDialogDisplayed);
                boolean unused = GlobalProfileActivity.this.dataDirty = false;
                GlobalProfileActivity.this.updateViews();
                GlobalProfileActivity.this.onDataChanged();
            }
        }

        public void requestControllerDidFail(RequestController requestController, Exception e) {
            if (!GlobalProfileActivity.this.isFinishing()) {
                GlobalProfileActivity.this.dismissDialogSafely(GlobalProfileActivity.this.lastProgressDialogDisplayed);
                displayErrorDialog(e);
                GlobalProfileActivity.this.updateViews();
            }
        }

        private void displayErrorDialog(Exception e) {
            if (e instanceof RequestCancelledException) {
                GlobalProfileActivity.this.showDialog(2);
            } else if (!(e instanceof RequestControllerException)) {
                GlobalProfileActivity.this.showDialog(3);
            } else {
                RequestControllerException ctrlException = (RequestControllerException) e;
                if (ctrlException.hasDetail(16)) {
                    GlobalProfileActivity.this.showDialog(8);
                } else if (ctrlException.hasDetail(8)) {
                    GlobalProfileActivity.this.showDialog(10);
                } else if (ctrlException.hasDetail(1) || ctrlException.hasDetail(4) || ctrlException.hasDetail(2)) {
                    GlobalProfileActivity.this.showDialog(11);
                } else {
                    GlobalProfileActivity.this.showDialog(3);
                }
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.scoreloop_profile);
        this.imageDatabaseCacher = new ImageDatabaseCacher(new ImageDownloadGate(this, getApplicationPreferences()), getJumbleeDB(), getScoreloopImageCache());
        this.userController = new UserController(getScoreloopSession(), new UserUpdateObserver());
        this.loginView = (EditText) findViewById(R.id.name);
        this.emailView = (EditText) findViewById(R.id.email);
        this.newProfileImageGroup = findViewById(R.id.profile_newProfileImage);
        this.currentImageView = (ImageView) findViewById(R.id.profile_image);
        this.newImageView = (ImageView) findViewById(R.id.profile_newProfile_image);
        this.updateProfileGroup = findViewById(R.id.globalProfile_updateProfileBlock);
        this.discardChangesGroup = findViewById(R.id.globalProfile_discardChangesBlock);
        this.showScoresGroup = findViewById(R.id.globalProfile_showScoresBlock);
        this.changePictureButton = (Button) findViewById(R.id.change_picture);
        this.changePictureButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction("android.intent.action.GET_CONTENT");
                GlobalProfileActivity.this.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 10);
            }
        });
        ((Button) findViewById(R.id.rotate_picture)).setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
             arg types: [android.graphics.Bitmap, int, int, ?, int, android.graphics.Matrix, int]
             candidates:
              ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
              ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
            public void onClick(View view) {
                Matrix matrix = new Matrix();
                matrix.postRotate(90.0f);
                Bitmap unused = GlobalProfileActivity.this.newImageBitmap = Bitmap.createBitmap(GlobalProfileActivity.this.newImageBitmap, 0, 0, (int) GlobalProfileActivity.IMAGE_SIZE, GlobalProfileActivity.IMAGE_SIZE, matrix, true);
                GlobalProfileActivity.this.newImageView.setImageBitmap(GlobalProfileActivity.this.newImageBitmap);
            }
        });
        ((Button) findViewById(R.id.discard_changes_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean unused = GlobalProfileActivity.this.dataDirty = false;
                GlobalProfileActivity.this.newImageView.setImageDrawable(null);
                Uri unused2 = GlobalProfileActivity.this.newImageUri = null;
                GlobalProfileActivity.this.updateViews();
                GlobalProfileActivity.this.onDataChanged();
            }
        });
        ((Button) findViewById(R.id.update_profile_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String login = GlobalProfileActivity.this.loginView.getText().toString().trim();
                String email = GlobalProfileActivity.this.emailView.getText().toString().trim();
                GlobalProfileActivity.this.loginView.setText(login);
                GlobalProfileActivity.this.emailView.setText(email);
                User user = GlobalProfileActivity.this.getScoreloopSession().getUser();
                user.setLogin(login);
                user.setEmailAddress(email);
                if (GlobalProfileActivity.this.newImageUri != null) {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    GlobalProfileActivity.this.newImageBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                    byte[] imageBytes = out.toByteArray();
                    user.setImageSource(ImageSource.IMAGE_SOURCE_SCORELOOP);
                    user.setImageMimeType("image/png");
                    user.setImageData(Base64.encodeBytes(imageBytes));
                    Drawable unused = GlobalProfileActivity.this.currentUserImage = Drawable.createFromStream(new ByteArrayInputStream(imageBytes), GlobalProfileActivity.this.newImageUri.toString());
                    GlobalProfileActivity.this.currentImageView.setImageDrawable(GlobalProfileActivity.this.currentUserImage);
                    Uri unused2 = GlobalProfileActivity.this.newImageUri = null;
                    Bitmap unused3 = GlobalProfileActivity.this.newImageBitmap = null;
                }
                int unused4 = GlobalProfileActivity.this.lastProgressDialogDisplayed = 14;
                GlobalProfileActivity.this.showDialog(14);
                GlobalProfileActivity.this.userController.submitUser();
            }
        });
        ((Button) findViewById(R.id.global_scores_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(GlobalProfileActivity.this, HighScoresActivity.class);
                intent.setFlags(67108864);
                GlobalProfileActivity.this.startActivity(intent);
            }
        });
        this.textChangedListener = new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!GlobalProfileActivity.this.isDataDirty()) {
                    boolean unused = GlobalProfileActivity.this.dataDirty = true;
                    GlobalProfileActivity.this.onDataChanged();
                }
            }

            public void afterTextChanged(Editable editable) {
            }
        };
        this.currentUserImage = getResources().getDrawable(R.drawable.sl_icon_user);
        updateViews();
        this.lastProgressDialogDisplayed = 13;
        showDialog(13);
        this.userController.loadUser();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.imageDatabaseCacher.shutdown();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        getAnalytics().startTracking();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getAnalytics().stopTracking();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == 10) {
            Uri selectedImageUri = data.getData();
            this.newImageBitmap = ImageHelper.createThumbnail(selectedImageUri, getContentResolver(), IMAGE_SIZE, ImageHelper.getExifOrientation(selectedImageUri, getContentResolver()));
            this.newImageView.setImageBitmap(this.newImageBitmap);
            this.newImageUri = selectedImageUri;
            this.dataDirty = true;
            onDataChanged();
        }
    }

    /* access modifiers changed from: private */
    public void updateViews() {
        User user = getScoreloopSession().getUser();
        if (user.isAuthenticated()) {
            this.loginView.setEnabled(true);
            this.emailView.setEnabled(true);
            this.changePictureButton.setEnabled(true);
            this.loginView.removeTextChangedListener(this.textChangedListener);
            this.loginView.setText(user.getLogin());
            this.loginView.addTextChangedListener(this.textChangedListener);
            this.emailView.removeTextChangedListener(this.textChangedListener);
            this.emailView.setText(user.getEmailAddress());
            this.emailView.addTextChangedListener(this.textChangedListener);
            this.currentImageView.setImageDrawable(this.currentUserImage);
            return;
        }
        this.loginView.setEnabled(false);
        this.emailView.setEnabled(false);
        this.changePictureButton.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void onDataChanged() {
        int i;
        int i2;
        int i3;
        int i4;
        boolean dirty = isDataDirty();
        View view = this.discardChangesGroup;
        if (dirty) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.updateProfileGroup;
        if (dirty) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        view2.setVisibility(i2);
        View view3 = this.showScoresGroup;
        if (dirty) {
            i3 = 8;
        } else {
            i3 = 0;
        }
        view3.setVisibility(i3);
        View view4 = this.newProfileImageGroup;
        if (this.newImageUri == null) {
            i4 = 8;
        } else {
            i4 = 0;
        }
        view4.setVisibility(i4);
    }

    /* access modifiers changed from: private */
    public boolean isDataDirty() {
        return this.dataDirty;
    }
}
