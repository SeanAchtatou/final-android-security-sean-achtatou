package au.com.xandar.jumblee.db;

import au.com.xandar.jumblee.Jumble;

public final class ImmutableJumble implements Jumble {
    private final int nrPossibleWords;
    private final String possibleWords;
    private final String specialLetter;
    private final String word;

    public ImmutableJumble(String word2, String specialLetter2, String possibleWords2, int nrPossibleWords2) {
        this.word = word2;
        this.specialLetter = specialLetter2;
        this.possibleWords = possibleWords2;
        this.nrPossibleWords = nrPossibleWords2;
    }

    public String getWord() {
        return this.word;
    }

    public String getSpecialLetter() {
        return this.specialLetter;
    }

    public String getPossibleWordsAsCommaSeparatedString() {
        return this.possibleWords;
    }

    public int getNrPossibleWords() {
        return this.nrPossibleWords;
    }

    public String toString() {
        return "ImmutableJumble{\n  word='" + this.word + "'\n" + "  specialLetter='" + this.specialLetter + "'\n" + "  nrPossibleWords=" + this.nrPossibleWords + "\n" + "  possibleWords=" + this.possibleWords + "\n}";
    }
}
