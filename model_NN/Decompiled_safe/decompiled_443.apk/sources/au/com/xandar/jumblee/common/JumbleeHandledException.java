package au.com.xandar.jumblee.common;

public final class JumbleeHandledException extends Exception {
    public JumbleeHandledException(String msg, Throwable th) {
        super(msg, th);
    }
}
