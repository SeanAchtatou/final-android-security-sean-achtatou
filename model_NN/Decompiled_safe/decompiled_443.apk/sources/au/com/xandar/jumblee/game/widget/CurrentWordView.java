package au.com.xandar.jumblee.game.widget;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.TextView;
import au.com.xandar.jumblee.game.Game;

public final class CurrentWordView extends TextView {
    private final TextViewStateRestorer restorer;

    public CurrentWordView(Context context) {
        this(context, null);
    }

    public CurrentWordView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CurrentWordView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.restorer = new TextViewStateRestorer();
        setSingleLine(false);
    }

    public void populateText(Game game) {
        setText(game.getCurrentWord());
    }

    public final Parcelable onSaveInstanceState() {
        return this.restorer.saveInstanceState(this, super.onSaveInstanceState());
    }

    public final void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(this.restorer.restoreInstanceState(this, state));
    }
}
