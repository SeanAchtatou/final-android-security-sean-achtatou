package au.com.xandar.jumblee.facebook;

public final class PostException extends Exception {
    public PostException(String message, Throwable th) {
        super(message, th);
    }
}
