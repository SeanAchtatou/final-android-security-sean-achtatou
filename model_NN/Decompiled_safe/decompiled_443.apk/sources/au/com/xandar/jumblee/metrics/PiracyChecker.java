package au.com.xandar.jumblee.metrics;

import android.app.Application;
import au.com.xandar.android.pm.PackageManagerUtility;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public final class PiracyChecker {
    private final String expectedSignature;
    private final PackageManagerUtility packageManagerUtility;

    public PiracyChecker(Application ctx, String expectedSignature2) {
        this.expectedSignature = expectedSignature2;
        this.packageManagerUtility = new PackageManagerUtility(ctx);
    }

    public boolean isPirated() {
        return !this.expectedSignature.equals(this.packageManagerUtility.getPackageSignature().toCharsString());
    }

    public boolean isTimeToNotifyPirate(ApplicationPreferences prefs) {
        boolean timeToReportPiracyToUser;
        if (System.currentTimeMillis() > prefs.getDateFirstStarted() + AppConstants.NR_MILLIS_BEFORE_REPORT_PIRACY_TO_USER) {
            timeToReportPiracyToUser = true;
        } else {
            timeToReportPiracyToUser = false;
        }
        if (!timeToReportPiracyToUser || !isPirated()) {
            return false;
        }
        return true;
    }
}
