package au.com.xandar.jumblee.game;

import au.com.xandar.android.time.FlexibleCountDownTimer;
import au.com.xandar.android.time.TimerListener;
import au.com.xandar.jumblee.CurrentJumble;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.common.JumbleeHandledException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.acra.ACRA;

public final class Game {
    private static final int ALMOST_ANOTHER_SECOND = 999;
    /* access modifiers changed from: private */
    public boolean clockNotificationRequired;
    /* access modifiers changed from: private */
    public CurrentJumble currentJumble;
    private final StringBuilder currentWord = new StringBuilder();
    private boolean gameOver = true;
    /* access modifiers changed from: private */
    public FlexibleCountDownTimer gameTimer = new FlexibleCountDownTimer(100);
    private GameListener listener;
    /* access modifiers changed from: private */
    public int secondsRemaining;
    private boolean specialLetterSelected;
    private final List<String> unfoundWords = new ArrayList();

    public Game() {
        this.gameTimer.addTimerListener(new TimerListener() {
            public void onTick(long millisUntilFinished) {
                Game.this.currentJumble.setMillisRemaining(millisUntilFinished);
                int newSecondsRemaining = ((int) (999 + millisUntilFinished)) / 1000;
                boolean unused = Game.this.clockNotificationRequired = Game.this.clockNotificationRequired || newSecondsRemaining < Game.this.secondsRemaining;
                int unused2 = Game.this.secondsRemaining = newSecondsRemaining;
                if (Game.this.clockNotificationRequired) {
                    boolean unused3 = Game.this.clockNotificationRequired = false;
                    Game.this.notifyListeners(10, Integer.valueOf(Game.this.secondsRemaining));
                }
            }

            public void onFinish() {
                Game.this.gameTimer.stop();
                Game.this.currentJumble.setMillisRemaining(0);
                Game.this.notifyListeners(11, null);
            }
        });
    }

    public void setJumble(CurrentJumble jumble, String currentWord2, boolean specialLetterSelected2) {
        this.currentJumble = jumble;
        this.currentWord.setLength(0);
        this.currentWord.append(currentWord2);
        this.specialLetterSelected = specialLetterSelected2;
        this.unfoundWords.clear();
        this.unfoundWords.addAll(this.currentJumble.getPossibleWords());
        this.unfoundWords.removeAll(this.currentJumble.getFoundWords());
        this.gameOver = !this.currentJumble.isCurrentJumble();
        this.gameTimer.setMillisRemaining(this.currentJumble.getMillisRemaining());
        this.secondsRemaining = ((int) this.currentJumble.getMillisRemaining()) / 1000;
    }

    public synchronized CurrentJumble getJumble() {
        return this.currentJumble;
    }

    public String getSpecialLetter() {
        return this.currentJumble.getSpecialLetter();
    }

    public boolean isSpecialLetterSelected() {
        return this.specialLetterSelected;
    }

    public List<String> getStandardLetters() {
        return this.currentJumble.getStandardLetters();
    }

    public String getCurrentWord() {
        return this.currentWord.toString();
    }

    public int getNrPossibleWords() {
        return this.currentJumble.getPossibleWords().size();
    }

    public int getNrFoundWords() {
        return this.currentJumble.getFoundWords().size();
    }

    public List<String> getFoundWords() {
        return this.currentJumble.getFoundWords();
    }

    public List<String> getUnfoundWords() {
        return this.unfoundWords;
    }

    public boolean isGameOver() {
        return this.gameOver;
    }

    public synchronized boolean doesWordHavePotential() {
        return this.specialLetterSelected && this.currentWord.length() >= 4;
    }

    public synchronized void startGame() {
        if (!this.gameOver) {
            this.clockNotificationRequired = true;
            this.gameTimer.start();
            notifyListeners(2, null);
        }
    }

    public synchronized void finishGame() {
        if (!this.gameOver) {
            this.gameTimer.stop();
            this.gameOver = true;
            clearCurrentWord();
            this.currentJumble.setCurrentJumble(false);
            notifyListeners(3, null);
        }
    }

    public synchronized void pauseGame() {
        if (!this.gameOver) {
            this.gameTimer.stop();
        }
    }

    public synchronized void resumeGame() {
        if (!this.gameOver) {
            this.clockNotificationRequired = true;
            this.gameTimer.start();
        }
    }

    public synchronized void selectLetter(String letter, boolean specialLetter) {
        if (!this.gameOver) {
            this.currentWord.append(letter);
            if (specialLetter) {
                this.specialLetterSelected = true;
            }
            notifyListeners(4, null);
        }
    }

    public synchronized void checkWord(IllegalStateTracker illegalStateTracker) {
        int event;
        if (!this.gameOver) {
            if (!this.specialLetterSelected) {
                Exception stateFailure = new IllegalStateException("Non-critical attempt to check the word when the special letter has not been selected. Events: @" + illegalStateTracker.getEventsString());
                ACRA.getErrorReporter().handleSilentException(new JumbleeHandledException(stateFailure.getMessage(), stateFailure));
            } else {
                String word = this.currentWord.toString();
                if (this.unfoundWords.remove(word)) {
                    event = word.length() == 9 ? 6 : 5;
                    this.gameTimer.incrementTimeRemaining(AppConstants.ACCEPTED_WORD_BONUS_MILLIS);
                    this.clockNotificationRequired = true;
                    this.currentJumble.getFoundWords().add(word);
                    Collections.sort(this.currentJumble.getFoundWords());
                } else if (this.currentJumble.getFoundWords().contains(word)) {
                    event = 8;
                } else {
                    event = 7;
                }
                clearCurrentWord();
                notifyListeners(event, word);
            }
        }
    }

    public synchronized void clearWord() {
        if (!this.gameOver) {
            clearCurrentWord();
            notifyListeners(9, null);
        }
    }

    public void setGameListener(GameListener listener2) {
        this.listener = listener2;
    }

    public void removeListeners() {
        this.listener = null;
    }

    /* access modifiers changed from: private */
    public void notifyListeners(int gameEvent, Object eventData) {
        if (this.listener != null) {
            this.listener.onGameEvent(this, gameEvent, eventData);
        }
    }

    private void clearCurrentWord() {
        this.currentWord.setLength(0);
        this.specialLetterSelected = false;
    }
}
