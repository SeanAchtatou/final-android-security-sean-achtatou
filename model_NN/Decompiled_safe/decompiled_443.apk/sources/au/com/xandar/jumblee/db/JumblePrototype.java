package au.com.xandar.jumblee.db;

import au.com.xandar.jumblee.CurrentJumble;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class JumblePrototype implements Serializable {
    private static final WordListParser WORD_LIST_PARSER = new WordListParser();
    private Integer currentJumble;
    private long dateStartedAsMillis;
    private String foundWordsText;
    private Integer id;
    private long millisRemaining;
    private String possibleWordsText;
    private String specialLetter;
    private String word;

    public CurrentJumble toJumble() {
        List<String> possibleWords = this.possibleWordsText == null ? Collections.emptyList() : WORD_LIST_PARSER.getListOfString(this.possibleWordsText);
        List<String> foundWords = this.foundWordsText == null ? new ArrayList<>() : WORD_LIST_PARSER.getListOfString(this.foundWordsText);
        CurrentJumble jumble = new CurrentJumble(this.word, this.specialLetter, possibleWords);
        jumble.setId(this.id);
        jumble.setFoundWords(foundWords);
        jumble.setDateStartedAsMillis(this.dateStartedAsMillis);
        jumble.setMillisRemaining(this.millisRemaining);
        jumble.setCurrentJumble(this.currentJumble != null);
        return jumble;
    }

    public void setId(Integer id2) {
        this.id = id2;
    }

    public void setWord(String word2) {
        this.word = word2;
    }

    public void setSpecialLetter(String specialLetter2) {
        this.specialLetter = specialLetter2;
    }

    public void setDateStartedAsMillis(long dateStartedAsMillis2) {
        this.dateStartedAsMillis = dateStartedAsMillis2;
    }

    public void setMillisRemaining(long millisRemaining2) {
        this.millisRemaining = millisRemaining2;
    }

    public void setPossibleWordsText(String text) {
        this.possibleWordsText = text;
    }

    public void setFoundWordsText(String foundWords) {
        this.foundWordsText = foundWords;
    }

    public void setCurrentJumble(Integer currentJumble2) {
        this.currentJumble = currentJumble2;
    }

    public String toString() {
        return "CurrentJumble{\n  specialLetter='" + this.specialLetter + "'\n" + "  word='" + this.word + "'\n" + "  foundWords=" + this.foundWordsText + "\n" + "  possibleWords=" + this.possibleWordsText + "\n}";
    }
}
