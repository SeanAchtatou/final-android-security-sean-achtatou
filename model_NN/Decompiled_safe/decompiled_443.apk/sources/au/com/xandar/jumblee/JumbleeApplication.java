package au.com.xandar.jumblee;

import android.app.Application;
import android.graphics.drawable.Drawable;
import au.com.xandar.android.PlatformConstants;
import au.com.xandar.android.facebook.Facebook;
import au.com.xandar.android.facebook.SessionStore;
import au.com.xandar.android.os.NewThreadExecutor;
import au.com.xandar.java.collection.LruCache;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.db.JumbleeDB;
import au.com.xandar.jumblee.db.JumbleeDBOpenHelper;
import au.com.xandar.jumblee.facebook.FacebookPoster;
import au.com.xandar.jumblee.metrics.PiracyChecker;
import au.com.xandar.jumblee.metrics.VersionFetcher;
import au.com.xandar.jumblee.net.IntentExecutable;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Session;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(customReportContent = {ReportField.DISPLAY, ReportField.SETTINGS_SECURE, ReportField.STACK_TRACE, ReportField.CUSTOM_DATA, ReportField.USER_CRASH_DATE, ReportField.PHONE_MODEL, ReportField.APP_VERSION_NAME, ReportField.APP_VERSION_CODE, ReportField.PACKAGE_NAME, ReportField.ANDROID_VERSION}, formKey = "", formUri = "http://www.bugsense.com/api/acra?api_key=6fa63c20", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text, socketTimeout = 5000)
public final class JumbleeApplication extends Application {
    private CurrentJumble currentJumble;
    private Executor executor;
    private Facebook facebook;
    private JumbleeDB jumbleeDB;
    private Map<String, IntentExecutable> netExecutables;
    private PiracyChecker piracyChecker;
    private ApplicationPreferences prefs;
    private Client scoreloopClient;
    private LruCache<String, Drawable> scoreloopImageCache;

    public void onCreate() {
        PlatformConstants.DEV_LOGGING = false;
        super.onCreate();
        ACRA.init(this);
    }

    public void onTerminate() {
        super.onTerminate();
    }

    public synchronized ApplicationPreferences getApplicationPreferences() {
        if (this.prefs == null) {
            this.prefs = new ApplicationPreferences(this);
        }
        return this.prefs;
    }

    public void prefetchCurrentJumble() {
        this.currentJumble = getJumbleeDB().getCurrentJumble();
    }

    public CurrentJumble getCurrentJumble() {
        if (this.currentJumble == null) {
            this.currentJumble = getJumbleeDB().getCurrentJumble();
        }
        return this.currentJumble;
    }

    public void setCurrentJumble(CurrentJumble jumble) {
        this.currentJumble = jumble;
    }

    public synchronized JumbleeDB getJumbleeDB() {
        if (this.jumbleeDB == null) {
            JumbleeDBOpenHelper dbOpenHelper = new JumbleeDBOpenHelper(this);
            dbOpenHelper.getWritableDatabase();
            this.jumbleeDB = new JumbleeDB(dbOpenHelper.getWritableDatabase());
        }
        return this.jumbleeDB;
    }

    public Session getScoreloopSession() {
        if (this.scoreloopClient == null) {
            this.scoreloopClient = new Client(this, AppConstants.SCORELOOP_GAME_SECRET, null);
        }
        return this.scoreloopClient.getSession();
    }

    public boolean isTimeToNotifyPirate() {
        return getPiracyChecker().isTimeToNotifyPirate(getApplicationPreferences());
    }

    public boolean isPirated() {
        return getPiracyChecker().isPirated();
    }

    private PiracyChecker getPiracyChecker() {
        if (this.piracyChecker == null) {
            this.piracyChecker = new PiracyChecker(this, AppConstants.APPLICATION_SIGNATURE);
        }
        return this.piracyChecker;
    }

    public Executor getExecutor() {
        if (this.executor == null) {
            this.executor = new NewThreadExecutor();
        }
        return this.executor;
    }

    public synchronized Facebook getFacebook() {
        if (this.facebook == null) {
            this.facebook = new Facebook(AppConstants.FACEBOOK_APP_ID_FOR_JUMBLEE, R.drawable.icon_facebook, 100, 101, AppConstants.FACEBOOK_PERMISSIONS, AppConstants.FACEBOOK_AUTHENTICATION_ACTIVITY_CODE);
            if (SessionStore.restore(this.facebook, this)) {
            }
        }
        return this.facebook;
    }

    public synchronized IntentExecutable getNetExecutable(String executableName) {
        if (this.netExecutables == null) {
            this.netExecutables = new HashMap();
            this.netExecutables.put(FacebookPoster.EXECUTABLE_NAME_VALUE, new FacebookPoster());
            this.netExecutables.put(VersionFetcher.EXECUTABLE_NAME_VALUE, new VersionFetcher());
        }
        return this.netExecutables.get(executableName);
    }

    public LruCache<String, Drawable> getScoreloopImageCache() {
        return this.scoreloopImageCache;
    }

    public void setScoreloopImageCache(LruCache<String, Drawable> scoreloopImageCache2) {
        this.scoreloopImageCache = scoreloopImageCache2;
    }
}
