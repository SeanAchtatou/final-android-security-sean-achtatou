package au.com.xandar.jumblee.net;

import android.app.Application;
import android.content.Intent;

public interface IntentExecutable {
    void execute(Application application, Intent intent);
}
