package au.com.xandar.jumblee.db;

public final class AbbreviatedScore implements Comparable<AbbreviatedScore> {
    private final long id;
    private final int scoreType;
    private final double value;

    public AbbreviatedScore(long id2, int scoreType2, double value2) {
        this.id = id2;
        this.scoreType = scoreType2;
        this.value = value2;
    }

    public long getId() {
        return this.id;
    }

    public int getScoreType() {
        return this.scoreType;
    }

    public double getValue() {
        return this.value;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AbbreviatedScore that = (AbbreviatedScore) o;
        if (this.id != that.id) {
            return false;
        }
        return this.scoreType == that.scoreType;
    }

    public int hashCode() {
        return (((int) this.id) * 31) + this.scoreType;
    }

    public int compareTo(AbbreviatedScore other) {
        if (this == other) {
            return 0;
        }
        if (this.id != other.id) {
            return (int) (this.id - other.id);
        }
        if (this.scoreType != other.scoreType) {
            return this.scoreType - other.scoreType;
        }
        return 0;
    }

    public String toString() {
        return "AbbreviatedScore{id=" + this.id + ", scoreType=" + this.scoreType + ", value=" + this.value + '}';
    }
}
