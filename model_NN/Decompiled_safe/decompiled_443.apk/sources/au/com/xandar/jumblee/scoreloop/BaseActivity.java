package au.com.xandar.jumblee.scoreloop;

import android.app.Dialog;
import au.com.xandar.android.PlatformConstants;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.dialog.JumbleeDialogFactory;

abstract class BaseActivity extends JumbleeActivity {
    private JumbleeDialogFactory dialogFactory = new JumbleeDialogFactory(this);

    BaseActivity() {
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return createErrorDialog(R.string.error_message_not_on_highscore_list);
            case 2:
                return createErrorDialog(R.string.error_message_request_cancelled);
            case 3:
                return createErrorDialog(R.string.error_message_network);
            case 4:
            case 5:
            case 6:
            case 7:
            case 9:
            case PlatformConstants.HONEYCOMB_3_1:
            default:
                return null;
            case 8:
                return createErrorDialog(R.string.error_message_email_already_taken);
            case 10:
                return createErrorDialog(R.string.error_message_invalid_email_format);
            case 11:
                return createErrorDialog(R.string.error_message_name_already_taken);
            case 13:
                return createKillableProgressDialog(R.string.progress_message_loading_user);
            case ScoreloopDialog.DIALOG_PROGRESS_UPDATING_USER /*14*/:
                return createUnkillableProgressDialog(R.string.progress_message_updating_user);
            case 15:
                return createKillableProgressDialog(R.string.progress_message_loading_scores);
        }
    }

    private Dialog createErrorDialog(int resId) {
        return this.dialogFactory.createKillableErrorDialog(resId);
    }

    private Dialog createKillableProgressDialog(int resId) {
        return this.dialogFactory.createKillableProgressDialog(resId);
    }

    private Dialog createUnkillableProgressDialog(int resId) {
        return this.dialogFactory.createUnkillableProgressDialog(resId);
    }
}
