package au.com.xandar.jumblee.scoreloop;

import au.com.xandar.jumblee.db.AbbreviatedScore;
import au.com.xandar.jumblee.db.JumbleeDB;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

final class ScoreSubmitter {
    /* access modifiers changed from: private */
    public AbbreviatedScore scoreBeingSubmitted;
    private final ScoreController scoreController;
    /* access modifiers changed from: private */
    public final Set<AbbreviatedScore> scoresToPost = new TreeSet();

    public ScoreSubmitter(Session session, final JumbleeDB jumbleeDB, final Runnable onFinished) {
        this.scoreController = new ScoreController(session, new RequestControllerObserver() {
            public void requestControllerDidReceiveResponse(RequestController requestController) {
                jumbleeDB.markScoreAsPostedToScoreloop(ScoreSubmitter.this.scoreBeingSubmitted.getId(), ScoreSubmitter.this.scoreBeingSubmitted.getScoreType());
                synchronized (ScoreSubmitter.this) {
                    if (!ScoreSubmitter.this.scoresToPost.isEmpty()) {
                        ScoreSubmitter.this.submitNextScore();
                    } else {
                        AbbreviatedScore unused = ScoreSubmitter.this.scoreBeingSubmitted = null;
                        onFinished.run();
                    }
                }
            }

            public void requestControllerDidFail(RequestController requestController, Exception e) {
                synchronized (ScoreSubmitter.this) {
                    if (!ScoreSubmitter.this.scoresToPost.isEmpty()) {
                        ScoreSubmitter.this.submitNextScore();
                    } else {
                        AbbreviatedScore unused = ScoreSubmitter.this.scoreBeingSubmitted = null;
                        onFinished.run();
                    }
                }
            }
        });
    }

    public synchronized void submitScore(AbbreviatedScore score) {
        this.scoresToPost.add(score);
        if (this.scoreBeingSubmitted == null) {
            submitNextScore();
        }
    }

    public synchronized void submitScores(Collection<AbbreviatedScore> scores) {
        this.scoresToPost.addAll(scores);
        if (!this.scoresToPost.isEmpty() && this.scoreBeingSubmitted == null) {
            submitNextScore();
        }
    }

    /* access modifiers changed from: private */
    public void submitNextScore() {
        this.scoreBeingSubmitted = this.scoresToPost.iterator().next();
        this.scoresToPost.remove(this.scoreBeingSubmitted);
        this.scoreController.submitScore(getScoreloopScore(this.scoreBeingSubmitted));
    }

    private Score getScoreloopScore(AbbreviatedScore score) {
        Score scoreloopScore = new Score(Double.valueOf(score.getValue()), null);
        scoreloopScore.setMode(Integer.valueOf(score.getScoreType()));
        return scoreloopScore;
    }
}
