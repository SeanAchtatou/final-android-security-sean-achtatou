package au.com.xandar.jumblee.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import au.com.xandar.jumblee.common.AppConstants;

public class TestDialog extends Dialog {
    public TestDialog(Context context) {
        super(context);
        Log.v(AppConstants.TAG, this + "#constuctor1", new Exception("Just for the stacktrace"));
    }

    public TestDialog(Context context, int theme) {
        super(context, theme);
        Log.v(AppConstants.TAG, this + "#constuctor2", new Exception("Just for the stacktrace"));
    }

    public TestDialog(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        Log.v(AppConstants.TAG, this + "#constuctor3 cancelable=" + cancelable + " cancelListener=" + cancelListener, new Exception("Just for the stacktrace"));
    }

    public boolean isShowing() {
        boolean isShowing = super.isShowing();
        Log.v(AppConstants.TAG, this + "#isShowing=" + isShowing, new Exception("Just for the stacktrace"));
        return isShowing;
    }

    public void show() {
        Log.v(AppConstants.TAG, this + "#show");
        super.show();
    }

    public void hide() {
        Log.v(AppConstants.TAG, this + "#hide");
        super.hide();
    }

    public void dismiss() {
        Log.v(AppConstants.TAG, this + "#dismiss");
        super.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Log.v(AppConstants.TAG, this + "#onCreate");
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Log.v(AppConstants.TAG, this + "#onStart");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.v(AppConstants.TAG, this + "#onStop");
        super.onStop();
    }

    public Bundle onSaveInstanceState() {
        Log.v(AppConstants.TAG, this + "#onSaveInstanceState");
        return super.onSaveInstanceState();
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.v(AppConstants.TAG, this + "#onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

    public Window getWindow() {
        Log.v(AppConstants.TAG, this + "#getWindow");
        return super.getWindow();
    }

    public View getCurrentFocus() {
        Log.v(AppConstants.TAG, this + "#getCurrentFocus");
        return super.getCurrentFocus();
    }

    public View findViewById(int id) {
        Log.v(AppConstants.TAG, this + "#findViewById");
        return super.findViewById(id);
    }

    public void setContentView(int layoutResID) {
        Log.v(AppConstants.TAG, this + "#setContentView");
        super.setContentView(layoutResID);
    }

    public void setContentView(View view) {
        Log.v(AppConstants.TAG, this + "#setContentView");
        super.setContentView(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams params) {
        Log.v(AppConstants.TAG, this + "#setContentView");
        super.setContentView(view, params);
    }

    public void addContentView(View view, ViewGroup.LayoutParams params) {
        Log.v(AppConstants.TAG, this + "#addContentView");
        super.addContentView(view, params);
    }

    public void setTitle(CharSequence title) {
        Log.v(AppConstants.TAG, this + "#setTitle");
        super.setTitle(title);
    }

    public void setTitle(int titleId) {
        Log.v(AppConstants.TAG, this + "#setTitle");
        super.setTitle(titleId);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.v(AppConstants.TAG, this + "#onKeyDown");
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.v(AppConstants.TAG, this + "#onKeyUp");
        return super.onKeyUp(keyCode, event);
    }

    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        Log.v(AppConstants.TAG, this + "#onKeyMultiple");
        return super.onKeyMultiple(keyCode, repeatCount, event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        Log.v(AppConstants.TAG, this + "#onTouchEvent");
        return super.onTouchEvent(event);
    }

    public boolean onTrackballEvent(MotionEvent event) {
        Log.v(AppConstants.TAG, this + "#onTrackballEvent");
        return super.onTrackballEvent(event);
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams params) {
        Log.v(AppConstants.TAG, this + "#onWindowAttributesChanged params=" + params);
        super.onWindowAttributesChanged(params);
    }

    public void onContentChanged() {
        Log.v(AppConstants.TAG, this + "#onContentChanged");
        super.onContentChanged();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        Log.v(AppConstants.TAG, this + "#onWindowFocusChanged");
        super.onWindowFocusChanged(hasFocus);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        Log.v(AppConstants.TAG, this + "#dispatchKeyEvent");
        return super.dispatchKeyEvent(event);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.v(AppConstants.TAG, this + "#dispatchTouchEvent");
        return super.dispatchTouchEvent(ev);
    }

    public boolean dispatchTrackballEvent(MotionEvent ev) {
        Log.v(AppConstants.TAG, this + "#dispatchTrackballEvent");
        return super.dispatchTrackballEvent(ev);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
        Log.v(AppConstants.TAG, this + "#dispatchPopulateAccessibilityEvent");
        return super.dispatchPopulateAccessibilityEvent(event);
    }

    public View onCreatePanelView(int featureId) {
        Log.v(AppConstants.TAG, this + "#onCreatePanelView");
        return super.onCreatePanelView(featureId);
    }

    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        Log.v(AppConstants.TAG, this + "#onCreatePanelMenu");
        return super.onCreatePanelMenu(featureId, menu);
    }

    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        Log.v(AppConstants.TAG, this + "#onPreparePanel");
        return super.onPreparePanel(featureId, view, menu);
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        Log.v(AppConstants.TAG, this + "#onMenuOpened");
        return super.onMenuOpened(featureId, menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        Log.v(AppConstants.TAG, this + "#onMenuItemSelected");
        return super.onMenuItemSelected(featureId, item);
    }

    public void onPanelClosed(int featureId, Menu menu) {
        Log.v(AppConstants.TAG, this + "#onPanelClosed");
        super.onPanelClosed(featureId, menu);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Log.v(AppConstants.TAG, this + "#onCreateOptionsMenu");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.v(AppConstants.TAG, this + "#onPrepareOptionsMenu");
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v(AppConstants.TAG, this + "#onOptionsItemSelected");
        return super.onOptionsItemSelected(item);
    }

    public void onOptionsMenuClosed(Menu menu) {
        Log.v(AppConstants.TAG, this + "#onOptionsMenuClosed");
        super.onOptionsMenuClosed(menu);
    }

    public void openOptionsMenu() {
        Log.v(AppConstants.TAG, this + "#openOptionsMenu");
        super.openOptionsMenu();
    }

    public void closeOptionsMenu() {
        Log.v(AppConstants.TAG, this + "#closeOptionsMenu");
        super.closeOptionsMenu();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.v(AppConstants.TAG, this + "#onCreateContextMenu");
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    public void registerForContextMenu(View view) {
        Log.v(AppConstants.TAG, this + "#registerForContextMenu");
        super.registerForContextMenu(view);
    }

    public void unregisterForContextMenu(View view) {
        Log.v(AppConstants.TAG, this + "#unregisterForContextMenu");
        super.unregisterForContextMenu(view);
    }

    public void openContextMenu(View view) {
        Log.v(AppConstants.TAG, this + "#openContextMenu");
        super.openContextMenu(view);
    }

    public boolean onContextItemSelected(MenuItem item) {
        Log.v(AppConstants.TAG, this + "#onContextItemSelected");
        return super.onContextItemSelected(item);
    }

    public void onContextMenuClosed(Menu menu) {
        Log.v(AppConstants.TAG, this + "#onContextMenuClosed");
        super.onContextMenuClosed(menu);
    }

    public boolean onSearchRequested() {
        Log.v(AppConstants.TAG, this + "#onSearchRequested");
        return super.onSearchRequested();
    }

    public void takeKeyEvents(boolean get) {
        Log.v(AppConstants.TAG, this + "#takeKeyEvents");
        super.takeKeyEvents(get);
    }

    public LayoutInflater getLayoutInflater() {
        Log.v(AppConstants.TAG, this + "#getLayoutInflater");
        return super.getLayoutInflater();
    }

    public void setCancelable(boolean flag) {
        Log.v(AppConstants.TAG, this + "#setCancelable");
        super.setCancelable(flag);
    }

    public void setCanceledOnTouchOutside(boolean cancel) {
        Log.v(AppConstants.TAG, this + "#setCanceledOnTouchOutside");
        super.setCanceledOnTouchOutside(cancel);
    }

    public void cancel() {
        Log.v(AppConstants.TAG, this + "#cancel");
        super.cancel();
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener listener) {
        Log.v(AppConstants.TAG, this + "#setOnCancelListener");
        super.setOnCancelListener(listener);
    }

    public void setCancelMessage(Message msg) {
        Log.v(AppConstants.TAG, this + "#setCancelMessage");
        super.setCancelMessage(msg);
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        Log.v(AppConstants.TAG, this + "#setOnDismissListener");
        super.setOnDismissListener(listener);
    }

    public void setDismissMessage(Message msg) {
        Log.v(AppConstants.TAG, this + "#setDismissMessage");
        super.setDismissMessage(msg);
    }

    public void setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
        Log.v(AppConstants.TAG, this + "#setOnKeyListener");
        super.setOnKeyListener(onKeyListener);
    }
}
