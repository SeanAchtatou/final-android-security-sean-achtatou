package au.com.xandar.jumblee.common;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

public final class LocationFinder {
    private final Context context;

    public LocationFinder(Context context2) {
        this.context = context2;
    }

    public Location getLocation() {
        Location lastLocation = null;
        long lastLocationTime = 0;
        LocationManager manager = (LocationManager) this.context.getSystemService("location");
        for (String provider : manager.getProviders(false)) {
            Location location = manager.getLastKnownLocation(provider);
            if (location != null && location.getTime() > lastLocationTime) {
                lastLocationTime = location.getTime();
                lastLocation = location;
            }
        }
        return lastLocation;
    }
}
