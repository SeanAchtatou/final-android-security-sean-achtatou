package au.com.xandar.jumblee;

public final class R {

    public static final class array {
        public static final int download_score_image_options = 2131230721;
        public static final int facebook_posting_descriptions = 2131230724;
        public static final int facebook_posting_encouragementWords = 2131230723;
        public static final int game_modes = 2131230720;
        public static final int progress_completingGame_encouragementWords = 2131230722;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131099649;
        public static final int checkWordResponse_accepted = 2131099667;
        public static final int checkWordResponse_accepted_nine_letter = 2131099668;
        public static final int checkWordResponse_rejected = 2131099669;
        public static final int checkWordResponse_rejectedAlreadyFound = 2131099670;
        public static final int jumblee_amber = 2131099664;
        public static final int jumblee_amber_75_percent_solid = 2131099665;
        public static final int jumblee_blue = 2131099671;
        public static final int jumblee_purple = 2131099653;
        public static final int jumblee_purple_00_percent_solid = 2131099657;
        public static final int jumblee_purple_25_percent_solid = 2131099656;
        public static final int jumblee_purple_50_percent_solid = 2131099655;
        public static final int jumblee_purple_75_percent_solid = 2131099654;
        public static final int jumblee_purple_light2 = 2131099658;
        public static final int jumblee_purple_light2_75_percent_solid = 2131099659;
        public static final int jumblee_purple_light3 = 2131099660;
        public static final int jumblee_purple_light3_75_percent_solid = 2131099661;
        public static final int jumblee_red = 2131099662;
        public static final int jumblee_red_75_percent_solid = 2131099663;
        public static final int jumblee_yellow = 2131099651;
        public static final int jumblee_yellow_75_percent_solid = 2131099652;
        public static final int primary_text_links = 2131099673;
        public static final int primary_text_purple = 2131099674;
        public static final int primary_text_purple_light3 = 2131099675;
        public static final int primary_text_yellow = 2131099676;
        public static final int scoreloop_blue = 2131099672;
        public static final int text_light_disabled = 2131099666;
        public static final int transparent = 2131099648;
        public static final int white = 2131099650;
    }

    public static final class dimen {
        public static final int background_checkWordResponse_border_width = 2131165213;
        public static final int background_checkWordResponse_corner_radius = 2131165212;
        public static final int background_checkWordResponse_padding = 2131165211;
        public static final int layout_height_adBanner = 2131165215;
        public static final int layout_height_gameScreen_textViews = 2131165193;
        public static final int layout_height_globalProfile_button_block = 2131165227;
        public static final int layout_height_globalProfile_scoreloop_icon = 2131165229;
        public static final int layout_height_globalScoresButton = 2131165232;
        public static final int layout_height_overviewButton = 2131165209;
        public static final int layout_marginLeft_about_links = 2131165235;
        public static final int layout_marginTop_about_button = 2131165236;
        public static final int layout_marginTop_about_links = 2131165234;
        public static final int layout_margin_answerView = 2131165197;
        public static final int layout_margin_bottom_scoreView = 2131165206;
        public static final int layout_margin_currentGameDetails = 2131165207;
        public static final int layout_margin_currentWordView = 2131165202;
        public static final int layout_margin_integrationTable = 2131165217;
        public static final int layout_margin_letterLattice = 2131165200;
        public static final int layout_margin_overviewButton = 2131165210;
        public static final int layout_margin_top_adBanner = 2131165216;
        public static final int layout_size_latticeLetterButton = 2131165189;
        public static final int layout_size_wordButton = 2131165190;
        public static final int layout_width_clockView = 2131165204;
        public static final int layout_width_currentWordView = 2131165201;
        public static final int layout_width_globalProfile_email = 2131165231;
        public static final int layout_width_globalProfile_image = 2131165226;
        public static final int layout_width_globalProfile_login = 2131165230;
        public static final int layout_width_globalProfile_profileButton = 2131165233;
        public static final int layout_width_globalProfile_scoreloop_icon = 2131165228;
        public static final int layout_width_globalScores_direction_button = 2131165223;
        public static final int layout_width_globalScores_image = 2131165221;
        public static final int layout_width_globalScores_info = 2131165222;
        public static final int layout_width_globalScores_rank = 2131165220;
        public static final int layout_width_overviewButton = 2131165208;
        public static final int layout_width_scoreView = 2131165205;
        public static final int layout_width_score_row_date = 2131165218;
        public static final int layout_width_score_row_time = 2131165219;
        public static final int marginBottom_answerView = 2131165199;
        public static final int paddingRight_answerView = 2131165198;
        public static final int padding_currentWordView = 2131165203;
        public static final int padding_globalScores = 2131165224;
        public static final int padding_globalScores_info = 2131165225;
        public static final int padding_left_wordButton = 2131165191;
        public static final int padding_right_wordButton = 2131165192;
        public static final int textsize_large = 2131165185;
        public static final int textsize_latticeLetter = 2131165184;
        public static final int textsize_medium = 2131165186;
        public static final int textsize_mid = 2131165187;
        public static final int textsize_small = 2131165188;
        public static final int textview_border_width = 2131165196;
        public static final int textview_corner_radius = 2131165195;
        public static final int textview_padding = 2131165194;
        public static final int width_preferenceDivider = 2131165214;
    }

    public static final class drawable {
        public static final int background__check_word_response__accepted = 2130837504;
        public static final int background__check_word_response__accepted_nine_letter = 2130837505;
        public static final int background__check_word_response__rejected = 2130837506;
        public static final int background__check_word_response__rejected_already_found = 2130837507;
        public static final int background_dialog = 2130837508;
        public static final int background_window_title = 2130837509;
        public static final int block_purple_1 = 2130837510;
        public static final int block_purple_2 = 2130837511;
        public static final int block_purple_3 = 2130837512;
        public static final int block_yellow = 2130837513;
        public static final int btn_disabled = 2130837514;
        public static final int btn_disabled_focused = 2130837515;
        public static final int btn_enabled_focused_purple = 2130837516;
        public static final int btn_enabled_focused_yellow = 2130837517;
        public static final int btn_enabled_purple = 2130837518;
        public static final int btn_enabled_yellow = 2130837519;
        public static final int btn_pressed = 2130837520;
        public static final int button_about_links = 2130837521;
        public static final int button_letter = 2130837522;
        public static final int button_standard = 2130837523;
        public static final int check_response_word_accepted = 2130837524;
        public static final int check_response_word_accepted_nine_letter = 2130837525;
        public static final int check_response_word_rejected = 2130837526;
        public static final int check_response_word_rejected_already_found = 2130837527;
        public static final int gradient_lightpurple_darkpurple_off = 2130837528;
        public static final int gradient_off_darkpurple_off = 2130837529;
        public static final int ic_menu_help = 2130837530;
        public static final int ic_menu_info_details = 2130837531;
        public static final int ic_menu_licence = 2130837532;
        public static final int ic_menu_preferences = 2130837533;
        public static final int ic_menu_rating = 2130837534;
        public static final int icon_144_game_splash = 2130837535;
        public static final int icon_16 = 2130837536;
        public static final int icon_email = 2130837537;
        public static final int icon_facebook = 2130837538;
        public static final int icon_game_launcher = 2130837539;
        public static final int icon_window_title = 2130837540;
        public static final int sl_icon_badge = 2130837541;
        public static final int sl_icon_user = 2130837542;
        public static final int sl_icon_user144 = 2130837543;
        public static final int view_border = 2130837544;
    }

    public static final class id {
        public static final int aboutContactUs = 2131492873;
        public static final int aboutContactUsButton = 2131492874;
        public static final int aboutFacebook = 2131492871;
        public static final int aboutFacebookButton = 2131492872;
        public static final int aboutJumbleeHome = 2131492869;
        public static final int aboutJumbleeHomeButton = 2131492870;
        public static final int adBanner = 2131492893;
        public static final int answerView = 2131492886;
        public static final int bottomLeft = 2131492901;
        public static final int bottomMiddle = 2131492902;
        public static final int bottomRight = 2131492903;
        public static final int btn_load_next = 2131492932;
        public static final int btn_load_prev = 2131492930;
        public static final int btn_my_profile = 2131492933;
        public static final int btn_show_me = 2131492931;
        public static final int buttonGlobalProfile = 2131492910;
        public static final int buttonGlobalScores = 2131492911;
        public static final int buttonHowToPlay = 2131492908;
        public static final int buttonNewGame = 2131492907;
        public static final int buttonResume = 2131492905;
        public static final int buttonScores = 2131492909;
        public static final int centerContainer = 2131492884;
        public static final int change_picture = 2131492941;
        public static final int checkWordButton = 2131492887;
        public static final int checkWordPreferences = 2131492916;
        public static final int check_word_response_image = 2131492864;
        public static final int check_word_response_text = 2131492865;
        public static final int checkbox_confirmationDialogOnEndGame = 2131492915;
        public static final int checkbox_hapticFeedback = 2131492912;
        public static final int checkbox_hideWordAccepted = 2131492917;
        public static final int checkbox_hideWordAlreadyFound = 2131492919;
        public static final int checkbox_hideWordRejected = 2131492918;
        public static final int checkbox_sound = 2131492914;
        public static final int clearWordButton = 2131492888;
        public static final int clockView = 2131492890;
        public static final int currentGamesDetails = 2131492906;
        public static final int currentWordView = 2131492892;
        public static final int discard_changes_button = 2131492948;
        public static final int download_score_images_spinner = 2131492920;
        public static final int email = 2131492946;
        public static final int facebookPreferences = 2131492921;
        public static final int facebook_authenticated = 2131492879;
        public static final int facebook_authenticatedText = 2131492878;
        public static final int facebook_postScores = 2131492881;
        public static final int facebook_postScoresText = 2131492880;
        public static final int filler = 2131492934;
        public static final int finishGameButton = 2131492891;
        public static final int game_mode = 2131492928;
        public static final int globalProfile_discardChangesBlock = 2131492947;
        public static final int globalProfile_showScoresBlock = 2131492951;
        public static final int globalProfile_updateProfileBlock = 2131492949;
        public static final int global_scores_button = 2131492952;
        public static final int letterLattice = 2131492885;
        public static final int list_view = 2131492935;
        public static final int menu_about = 2131492962;
        public static final int menu_howToPlay = 2131492959;
        public static final int menu_licence = 2131492961;
        public static final int menu_preferences = 2131492958;
        public static final int menu_rateMe = 2131492960;
        public static final int message = 2131492867;
        public static final int middleLeft = 2131492898;
        public static final int middleMiddle = 2131492899;
        public static final int middleRight = 2131492900;
        public static final int name = 2131492945;
        public static final int negativeButton = 2131492877;
        public static final int neutralButton = 2131492876;
        public static final int player_image = 2131492937;
        public static final int player_name = 2131492938;
        public static final int positiveButton = 2131492875;
        public static final int profile_image = 2131492940;
        public static final int profile_newProfileImage = 2131492942;
        public static final int profile_newProfile_image = 2131492943;
        public static final int progress_bar = 2131492882;
        public static final int progress_text = 2131492883;
        public static final int resumeGameContainer = 2131492904;
        public static final int rotate_picture = 2131492944;
        public static final int scoreGrid = 2131492954;
        public static final int scoreHeader = 2131492953;
        public static final int scoreView = 2131492894;
        public static final int score_date = 2131492922;
        public static final int score_info = 2131492939;
        public static final int score_nrFound = 2131492924;
        public static final int score_percentageFound = 2131492925;
        public static final int score_rank = 2131492936;
        public static final int score_rate = 2131492926;
        public static final int score_score = 2131492927;
        public static final int score_time = 2131492923;
        public static final int search_list_spinner = 2131492929;
        public static final int seekbar_sound = 2131492913;
        public static final int splash_icon = 2131492955;
        public static final int splash_progress = 2131492957;
        public static final int splash_text = 2131492956;
        public static final int startGameButton = 2131492889;
        public static final int title = 2131492866;
        public static final int topLeft = 2131492895;
        public static final int topMiddle = 2131492896;
        public static final int topRight = 2131492897;
        public static final int updateMessage = 2131492868;
        public static final int update_profile_button = 2131492950;
    }

    public static final class integer {
        public static final int layout_weight_score_nrFound = 2131034113;
        public static final int layout_weight_score_percentageFound = 2131034114;
        public static final int layout_weight_score_rate = 2131034115;
        public static final int layout_weight_score_score = 2131034116;
        public static final int layout_weight_score_timeAndDate = 2131034112;
    }

    public static final class layout {
        public static final int check_word_response_screen = 2130903040;
        public static final int dialog_about = 2130903041;
        public static final int dialog_facebook_config = 2130903042;
        public static final int dialog_generic_3button = 2130903043;
        public static final int dialog_progress = 2130903044;
        public static final int game_screen = 2130903045;
        public static final int letter_lattice = 2130903046;
        public static final int overview_screen = 2130903047;
        public static final int preferences = 2130903048;
        public static final int score_row = 2130903049;
        public static final int scoreloop_highscores = 2130903050;
        public static final int scoreloop_list_item_score = 2130903051;
        public static final int scoreloop_profile = 2130903052;
        public static final int scores_screen = 2130903053;
        public static final int splash_screen = 2130903054;
    }

    public static final class menu {
        public static final int main = 2131427328;
    }

    public static final class raw {
        public static final int check_word_success_nine_letter = 2130968576;
        public static final int checkword_failure = 2130968577;
        public static final int checkword_rejected_already_found = 2130968578;
        public static final int checkword_success = 2130968579;
        public static final int dictionary_4to9_letter_words = 2130968580;
        public static final int dictionary_9_letter_words = 2130968581;
        public static final int game_over = 2130968582;
        public static final int ssl_keystore = 2130968583;
    }

    public static final class string {
        public static final int about_contactUs = 2131296318;
        public static final int about_dialog_closeButton = 2131296374;
        public static final int about_dialog_contactUs_body = 2131296376;
        public static final int about_dialog_contactUs_subject = 2131296375;
        public static final int about_dialog_message = 2131296371;
        public static final int about_dialog_title = 2131296370;
        public static final int about_dialog_updateButton = 2131296373;
        public static final int about_dialog_updateMessage = 2131296372;
        public static final int about_facebook = 2131296317;
        public static final int about_jumbleeHome = 2131296316;
        public static final int acceptButton = 2131296291;
        public static final int answerView_GameText = 2131296332;
        public static final int answerView_GameText_IncludingUndiscovered = 2131296333;
        public static final int app_name = 2131296277;
        public static final int btn_change_picture = 2131296261;
        public static final int btn_discard_changes = 2131296264;
        public static final int btn_global_scores = 2131296260;
        public static final int btn_load_next = 2131296257;
        public static final int btn_load_prev = 2131296256;
        public static final int btn_my_profile = 2131296259;
        public static final int btn_rotate_picture = 2131296262;
        public static final int btn_show_me = 2131296258;
        public static final int btn_update_profile = 2131296263;
        public static final int checkButton = 2131296288;
        public static final int clearButton = 2131296289;
        public static final int confirmEndGame_message = 2131296347;
        public static final int confirmEndGame_negativeButton = 2131296349;
        public static final int confirmEndGame_positiveButton = 2131296348;
        public static final int confirmEndGame_title = 2131296346;
        public static final int couldNotCopyDatabase = 2131296301;
        public static final int crash_toast_text = 2131296302;
        public static final int currentWordView_defaultText = 2131296293;
        public static final int download_score_images_title = 2131296377;
        public static final int encouragementWord_utterFailure = 2131296385;
        public static final int error_message_email_already_taken = 2131296272;
        public static final int error_message_invalid_email_format = 2131296273;
        public static final int error_message_name_already_taken = 2131296271;
        public static final int error_message_network = 2131296270;
        public static final int error_message_not_on_highscore_list = 2131296269;
        public static final int error_message_request_cancelled = 2131296268;
        public static final int eula_dialog_message = 2131296367;
        public static final int eula_dialog_title = 2131296366;
        public static final int facebook_integration_dialog_askBeforePostingCheckbox = 2131296383;
        public static final int facebook_integration_dialog_authenticatedCheckbox = 2131296380;
        public static final int facebook_integration_dialog_message = 2131296379;
        public static final int facebook_integration_dialog_minimumScoreToPostPicker = 2131296382;
        public static final int facebook_integration_dialog_positiveButton = 2131296384;
        public static final int facebook_integration_dialog_postScoresCheckbox = 2131296381;
        public static final int facebook_integration_dialog_title = 2131296378;
        public static final int facebook_posting_caption = 2131296386;
        public static final int facebook_posting_message = 2131296387;
        public static final int facebook_posting_message_utterFailure = 2131296388;
        public static final int finishButton = 2131296287;
        public static final int gameClockView_defaultText = 2131296294;
        public static final int gameScoreView_defaultText = 2131296295;
        public static final int globalProfile_label = 2131296330;
        public static final int globalScores_label = 2131296329;
        public static final int how_to_play_dialog_message = 2131296365;
        public static final int how_to_play_dialog_title = 2131296364;
        public static final int menu_about = 2131296300;
        public static final int menu_howToPlay = 2131296297;
        public static final int menu_licence = 2131296299;
        public static final int menu_preferences = 2131296296;
        public static final int menu_rateMe = 2131296298;
        public static final int no_available_jumbles_dialog_message = 2131296369;
        public static final int no_available_jumbles_dialog_title = 2131296368;
        public static final int okButton = 2131296290;
        public static final int overview_CurrentGameText = 2131296331;
        public static final int overview_EulaButton = 2131296306;
        public static final int overview_GlobalProfileButton = 2131296308;
        public static final int overview_GlobalScoresButton = 2131296309;
        public static final int overview_NewGameButton = 2131296304;
        public static final int overview_ResumeButton = 2131296303;
        public static final int overview_RulesButton = 2131296305;
        public static final int overview_ScoresButton = 2131296307;
        public static final int piracy_dialog_message = 2131296363;
        public static final int piracy_dialog_negative_button = 2131296361;
        public static final int piracy_dialog_neutral_button = 2131296360;
        public static final int piracy_dialog_positive_button = 2131296359;
        public static final int piracy_dialog_title = 2131296362;
        public static final int preferences_hapticFeedBack_description = 2131296321;
        public static final int preferences_hapticFeedBack_title = 2131296320;
        public static final int preferences_hideCheckWordToasts_title = 2131296325;
        public static final int preferences_hideWordAccepted_description = 2131296326;
        public static final int preferences_hideWordAlreadyFound_description = 2131296328;
        public static final int preferences_hideWordRejected_description = 2131296327;
        public static final int preferences_label = 2131296319;
        public static final int preferences_showConfirmationDialogOnEndGame_message = 2131296324;
        public static final int preferences_showConfirmationDialogOnEndGame_title = 2131296323;
        public static final int preferences_soundEffect_title = 2131296322;
        public static final int profile_email_label = 2131296266;
        public static final int profile_name_label = 2131296265;
        public static final int profile_newProfileImage = 2131296267;
        public static final int progress_completingGame_message = 2131296340;
        public static final int progress_completingGame_message_askPostToFB = 2131296341;
        public static final int progress_completingGame_message_noWordsFound = 2131296339;
        public static final int progress_completingGame_message_noWordsFound_startingNewGame = 2131296338;
        public static final int progress_completingGame_negativeButton = 2131296344;
        public static final int progress_completingGame_neutralButton = 2131296343;
        public static final int progress_completingGame_positiveButton = 2131296342;
        public static final int progress_completingGame_title = 2131296337;
        public static final int progress_initialiseApplication = 2131296334;
        public static final int progress_message_loading_scores = 2131296276;
        public static final int progress_message_loading_user = 2131296274;
        public static final int progress_message_updating_user = 2131296275;
        public static final int progress_resumingGame = 2131296336;
        public static final int progress_savingGame = 2131296345;
        public static final int progress_startingNewGame = 2131296335;
        public static final int rate_me_dialog_message = 2131296358;
        public static final int rate_me_dialog_negative_button = 2131296356;
        public static final int rate_me_dialog_positive_button = 2131296355;
        public static final int rate_me_dialog_title = 2131296357;
        public static final int rejectButton = 2131296292;
        public static final int scores_completed = 2131296311;
        public static final int scores_label = 2131296310;
        public static final int scores_nrFound = 2131296312;
        public static final int scores_percentageFound = 2131296313;
        public static final int scores_rate = 2131296314;
        public static final int scores_score = 2131296315;
        public static final int service_generalIntent = 2131296280;
        public static final int service_jumbleFinder = 2131296278;
        public static final int service_scoreloopPoster = 2131296279;
        public static final int splash_text = 2131296281;
        public static final int startButton = 2131296286;
        public static final int update_to_new_version_message = 2131296354;
        public static final int update_to_new_version_negative_button = 2131296352;
        public static final int update_to_new_version_neutral_button = 2131296351;
        public static final int update_to_new_version_positive_button = 2131296350;
        public static final int update_to_new_version_title = 2131296353;
        public static final int word_accepted = 2131296282;
        public static final int word_accepted_nine_letter = 2131296283;
        public static final int word_rejected = 2131296284;
        public static final int word_rejected_already_found = 2131296285;
        /* added by JADX */

        /* renamed from: service.jumbleFinder  reason: not valid java name */
        public static final int f0servicejumbleFinder = 2131296278;
        /* added by JADX */

        /* renamed from: service.scoreloopPoster  reason: not valid java name */
        public static final int f1servicescoreloopPoster = 2131296279;
        /* added by JADX */

        /* renamed from: service.generalIntent  reason: not valid java name */
        public static final int f2servicegeneralIntent = 2131296280;
    }

    public static final class style {
        public static final int CustomWindowTitle = 2131361835;
        public static final int CustomWindowTitleBackground = 2131361834;
        public static final int CustomWindowTitleText = 2131361833;
        public static final int DialogButton = 2131361823;
        public static final int DialogMessage = 2131361822;
        public static final int DialogTheme = 2131361820;
        public static final int DialogTitle = 2131361821;
        public static final int JumbleeTheme = 2131361836;
        public static final int JumbleeTheme_GameScreen = 2131361837;
        public static final int ProgressDialogMessage = 2131361824;
        public static final int about_layout = 2131361829;
        public static final int about_link_image = 2131361831;
        public static final int about_link_text = 2131361832;
        public static final int about_text = 2131361830;
        public static final int answerScrollView = 2131361799;
        public static final int answerView = 2131361800;
        public static final int checkWordResponseText = 2131361809;
        public static final int clockView = 2131361803;
        public static final int currentWordView = 2131361801;
        public static final int globalProfileEditText = 2131361819;
        public static final int globalScoresButton = 2131361818;
        public static final int globalScoresButtonBlock = 2131361817;
        public static final int integrationCheckbox = 2131361828;
        public static final int integrationRow = 2131361826;
        public static final int integrationTable = 2131361825;
        public static final int integrationTextView = 2131361827;
        public static final int latticeButton = 2131361795;
        public static final int latticeButton_special = 2131361797;
        public static final int latticeButton_standard = 2131361796;
        public static final int overviewButton = 2131361805;
        public static final int overviewGameDetails = 2131361806;
        public static final int preferenceDivider = 2131361812;
        public static final int preferenceHeading = 2131361811;
        public static final int preferenceHeadingContainer = 2131361810;
        public static final int preferenceLeftColumn = 2131361815;
        public static final int preferenceRightColumn = 2131361816;
        public static final int preferenceTable = 2131361813;
        public static final int preferenceTableRow = 2131361814;
        public static final int scoreDetails = 2131361807;
        public static final int scoreDetails_header = 2131361808;
        public static final int scoreView = 2131361804;
        public static final int splashText = 2131361794;
        public static final int standardActivity = 2131361792;
        public static final int standardButton = 2131361798;
        public static final int text = 2131361793;
        public static final int wordButton = 2131361802;
    }
}
