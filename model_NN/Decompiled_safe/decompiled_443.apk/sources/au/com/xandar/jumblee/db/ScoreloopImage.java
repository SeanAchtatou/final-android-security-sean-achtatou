package au.com.xandar.jumblee.db;

import android.graphics.drawable.Drawable;

public final class ScoreloopImage {
    private long dateLastReadAsMillis;
    private Drawable drawable;
    private long id;
    private byte[] imageBytes = new byte[0];
    private String imageUri;
    private String scoreloopUserId;

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getScoreloopUserId() {
        return this.scoreloopUserId;
    }

    public void setScoreloopUserId(String scoreloopUserId2) {
        this.scoreloopUserId = scoreloopUserId2;
    }

    public long getDateLastReadAsMillis() {
        return this.dateLastReadAsMillis;
    }

    public void setDateLastReadAsMillis(long dateLastReadAsMillis2) {
        this.dateLastReadAsMillis = dateLastReadAsMillis2;
    }

    public String getImageUri() {
        return this.imageUri;
    }

    public void setImageUri(String imageUri2) {
        this.imageUri = imageUri2;
    }

    public byte[] getImageBytes() {
        return this.imageBytes;
    }

    public void setImageBytes(byte[] image) {
        this.imageBytes = image;
    }

    public Drawable getDrawable() {
        return this.drawable;
    }

    public void setDrawable(Drawable drawable2) {
        this.drawable = drawable2;
    }
}
