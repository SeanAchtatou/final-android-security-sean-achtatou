package au.com.xandar.jumblee.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import au.com.xandar.android.pm.PackageManagerUtility;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public final class AboutDialogFactory {
    /* access modifiers changed from: private */
    public final Activity context;

    public AboutDialogFactory(Activity context2) {
        this.context = context2;
    }

    public Dialog create(ApplicationPreferences preferences) {
        View layout = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.dialog_about, (ViewGroup) null);
        Dialog dialog = new Dialog(this.context, R.style.DialogTheme);
        dialog.addContentView(layout, new ViewGroup.LayoutParams(-1, -2));
        dialog.setCancelable(false);
        PackageManagerUtility packageManagerUtility = new PackageManagerUtility(this.context);
        PackageInfo packageInfo = packageManagerUtility.getAllPackageInfo();
        ((TextView) layout.findViewById(R.id.message)).setText(String.format(this.context.getString(R.string.about_dialog_message), packageInfo.versionName, Integer.valueOf(packageInfo.versionCode)));
        layout.findViewById(R.id.aboutJumbleeHomeButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(AppConstants.WEB_SITE));
                intent.setFlags(524288);
                AboutDialogFactory.this.context.startActivity(intent);
            }
        });
        layout.findViewById(R.id.aboutFacebookButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(AppConstants.FACEBOOK_APP_SITE));
                intent.setFlags(524288);
                AboutDialogFactory.this.context.startActivity(intent);
            }
        });
        final PackageInfo packageInfo2 = packageInfo;
        layout.findViewById(R.id.aboutContactUsButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setFlags(524288);
                intent.putExtra("android.intent.extra.EMAIL", new String[]{AppConstants.SUPPORT_EMAIL_ADDRESS});
                intent.putExtra("android.intent.extra.SUBJECT", AboutDialogFactory.this.context.getString(R.string.about_dialog_contactUs_subject));
                intent.setType("message/rfc822");
                intent.putExtra("android.intent.extra.TEXT", AboutDialogFactory.this.context.getString(R.string.about_dialog_contactUs_body, new Object[]{packageInfo2.versionName, Integer.valueOf(packageInfo2.versionCode), Build.PRODUCT, Build.MODEL, Build.VERSION.RELEASE}));
                AboutDialogFactory.this.context.startActivity(intent);
            }
        });
        if (preferences.getLatestVersionCode() > packageInfo.versionCode) {
            ((TextView) layout.findViewById(R.id.updateMessage)).setText(String.format(this.context.getString(R.string.about_dialog_updateMessage), preferences.getLatestVersionName(), Integer.valueOf(preferences.getLatestVersionCode()), preferences.getLatestVersionDatePublished()));
            if (packageManagerUtility.isMarketAvailable()) {
                Button positiveButton = (Button) layout.findViewById(R.id.positiveButton);
                positiveButton.setVisibility(0);
                final Dialog dialog2 = dialog;
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        AboutDialogFactory.this.context.startActivity(AppConstants.MARKET_INTENT);
                        AboutDialogFactory.this.context.finish();
                        dialog2.cancel();
                    }
                });
            } else {
                Button neutralButton = (Button) layout.findViewById(R.id.neutralButton);
                neutralButton.setVisibility(0);
                final ApplicationPreferences applicationPreferences = preferences;
                final Dialog dialog3 = dialog;
                neutralButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://jumblee.xandar.com.au/Jumblee-" + applicationPreferences.getLatestVersionName() + "-" + applicationPreferences.getLatestVersionCode() + "-aligned.apk"));
                        intent.setFlags(524288);
                        AboutDialogFactory.this.context.startActivity(intent);
                        AboutDialogFactory.this.context.finish();
                        dialog3.cancel();
                    }
                });
            }
        }
        final Dialog dialog4 = dialog;
        ((Button) layout.findViewById(R.id.negativeButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog4.cancel();
            }
        });
        return dialog;
    }
}
