package au.com.xandar.jumblee.metrics;

import au.com.xandar.jumblee.CurrentJumble;

public interface Analytics {
    void sendAdClicked();

    void sendAdImpressionReceived();

    void sendAdRequested();

    void sendGameOver(CurrentJumble currentJumble);

    void sendPirateName(String str);

    void sendPostToFacebook();

    void sendStartGame();

    void sendWordRejected(String str);

    void startTracking();

    void stopTracking();
}
