package au.com.xandar.jumblee.scoreloop;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import au.com.xandar.android.os.SimpleAsyncTask;
import au.com.xandar.java.collection.CacheListener;
import au.com.xandar.java.collection.LruCache;
import au.com.xandar.jumblee.R;
import au.com.xandar.jumblee.db.ScoreloopImage;
import au.com.xandar.jumblee.scoreloop.image.ImageDatabaseCacher;
import au.com.xandar.jumblee.scoreloop.image.ImageDownloadGate;
import au.com.xandar.jumblee.scoreloop.image.ImageRetriever;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public final class HighScoresActivity extends BaseActivity {
    private static final List<String> ACCEPTABLE_LEADERBOARDS = Arrays.asList(LEADERBOARD_GLOBAL, LEADERBOARD_GLOBAL_WEEKLY, LEADERBOARD_COUNTRY, LEADERBOARD_COUNTRY_WEEKLY);
    private static final int DEFAULT_SEARCH_LISTS_SELECTION = 0;
    private static final int FIXED_OFFSET = 3;
    private static final String LEADERBOARD_COUNTRY = "428a66d4-e6ca-4ff0-b7ea-f482ba4541a3";
    private static final String LEADERBOARD_COUNTRY_WEEKLY = "428a66d4-e6ca-4ff0-b7ea-f482ba4541a9";
    private static final String LEADERBOARD_GLOBAL = "428a66d4-e6ca-4ff0-b7ea-f482ba4541a1";
    private static final String LEADERBOARD_GLOBAL_WEEKLY = "428a66d4-e6ca-4ff0-b7ea-f482ba4541a6";
    /* access modifiers changed from: private */
    public static final Format[] SCORE_FORMATS = {new DecimalFormat("#,##0.00"), new DecimalFormat("##0.00'%'"), new DecimalFormat("#0.00")};
    /* access modifiers changed from: private */
    public CurrentOperationType currentOperation = CurrentOperationType.awaitingStartup;
    /* access modifiers changed from: private */
    public Spinner gameModeSpinner;
    /* access modifiers changed from: private */
    public ListView highScoresListView;
    /* access modifiers changed from: private */
    public LruCache<String, Drawable> imageCache;
    /* access modifiers changed from: private */
    public ImageDatabaseCacher imageDatabaseCacher;
    /* access modifiers changed from: private */
    public Button nextRangeButton;
    /* access modifiers changed from: private */
    public Button prevRangeButton;
    /* access modifiers changed from: private */
    public ScoresController scoresController;
    private Spinner searchListSpinner;

    enum CurrentOperationType {
        me,
        none,
        other,
        awaitingStartup
    }

    private static class ScoreViewHolder {
        ImageView imageView;
        TextView playerName;
        TextView scoreInfo;
        TextView scoreRank;
        String scoreloopUserId;

        ScoreViewHolder(View view) {
            this.scoreRank = (TextView) view.findViewById(R.id.score_rank);
            this.playerName = (TextView) view.findViewById(R.id.player_name);
            this.scoreInfo = (TextView) view.findViewById(R.id.score_info);
            this.imageView = (ImageView) view.findViewById(R.id.player_image);
        }
    }

    private class ScoresAdapter extends ArrayAdapter<Score> {
        private final Format scoreFormat;

        public ScoresAdapter(Context context, int resource, final List<Score> objects, Format scoreFormat2) {
            super(context, resource, objects);
            this.scoreFormat = scoreFormat2;
            new Thread(new Runnable(HighScoresActivity.this) {
                public void run() {
                    ImageRetriever retriever = new ImageRetriever(HighScoresActivity.this.getJumbleeDB());
                    for (Score score : objects) {
                        ScoreloopImage cachedImage = retriever.getScoreloopImage(score.getUser());
                        if (!(cachedImage == null || cachedImage.getDrawable() == null)) {
                            HighScoresActivity.this.imageCache.put(cachedImage.getScoreloopUserId(), cachedImage.getDrawable());
                        }
                        HighScoresActivity.this.imageDatabaseCacher.fillDatabaseCacheFromScoreloop(score.getUser(), cachedImage);
                    }
                }
            }).start();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = HighScoresActivity.this.getLayoutInflater().inflate((int) R.layout.scoreloop_list_item_score, (ViewGroup) null);
                final ScoreViewHolder holder = new ScoreViewHolder(convertView);
                convertView.setTag(holder);
                HighScoresActivity.this.imageCache.addListener(new CacheListener<String, Drawable>() {
                    public void added(String key, final Drawable cachedImage) {
                        if (key.equals(holder.scoreloopUserId)) {
                            new SimpleAsyncTask() {
                                /* access modifiers changed from: protected */
                                public void doInBackground() {
                                }

                                /* access modifiers changed from: protected */
                                public void onPostExecute() {
                                    holder.imageView.setImageDrawable(cachedImage);
                                }
                            }.execute();
                        }
                    }
                });
            }
            ScoreViewHolder holder2 = (ScoreViewHolder) convertView.getTag();
            Score score = (Score) getItem(position);
            String scoreText = this.scoreFormat.format(score.getResult());
            holder2.scoreloopUserId = score.getUser().getIdentifier();
            holder2.scoreRank.setText("" + score.getRank(), (TextView.BufferType) null);
            holder2.playerName.setText(score.getUser().getLogin(), (TextView.BufferType) null);
            holder2.scoreInfo.setText(scoreText);
            Drawable cachedImage = (Drawable) HighScoresActivity.this.imageCache.get(score.getUser().getIdentifier());
            if (cachedImage != null) {
                holder2.imageView.setImageDrawable(cachedImage);
            } else {
                holder2.imageView.setImageResource(R.drawable.sl_icon_user);
            }
            convertView.setBackgroundColor(score.getUser().equals(HighScoresActivity.this.getScoreloopSession().getUser()) ? HighScoresActivity.this.getResources().getColor(R.color.scoreloop_blue) : 0);
            return convertView;
        }
    }

    private class ScoresControllerObserver implements RequestControllerObserver {
        private ScoresControllerObserver() {
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            if (!HighScoresActivity.this.isFinishing()) {
                HighScoresActivity.this.dismissDialogSafely(15);
                List<Score> scores = HighScoresActivity.this.scoresController.getScores();
                HighScoresActivity.this.highScoresListView.setAdapter((ListAdapter) new ScoresAdapter(HighScoresActivity.this, R.layout.scoreloop_highscores, scores, HighScoresActivity.SCORE_FORMATS[HighScoresActivity.this.gameModeSpinner.getSelectedItemPosition()]));
                HighScoresActivity.this.prevRangeButton.setEnabled(HighScoresActivity.this.scoresController.hasPreviousRange());
                HighScoresActivity.this.nextRangeButton.setEnabled(HighScoresActivity.this.scoresController.hasNextRange());
                if (HighScoresActivity.this.currentOperation == CurrentOperationType.me) {
                    boolean loginFound = false;
                    int idx = 0;
                    Iterator i$ = scores.iterator();
                    while (true) {
                        if (!i$.hasNext()) {
                            break;
                        } else if (i$.next().getUser().equals(HighScoresActivity.this.getScoreloopSession().getUser())) {
                            loginFound = true;
                            break;
                        } else {
                            idx++;
                        }
                    }
                    if (loginFound) {
                        HighScoresActivity.this.highScoresListView.setSelection(idx < 3 ? 0 : idx - 3);
                    } else {
                        HighScoresActivity.this.showDialog(1);
                    }
                }
                CurrentOperationType unused = HighScoresActivity.this.currentOperation = CurrentOperationType.none;
            }
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            if (!HighScoresActivity.this.isFinishing()) {
                HighScoresActivity.this.dismissDialogSafely(15);
                CurrentOperationType unused = HighScoresActivity.this.currentOperation = CurrentOperationType.none;
                if (exception instanceof RequestCancelledException) {
                    HighScoresActivity.this.showDialog(2);
                } else {
                    HighScoresActivity.this.showDialog(3);
                }
            }
        }
    }

    private class UserLoadObserver implements RequestControllerObserver {
        private UserLoadObserver() {
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            if (!HighScoresActivity.this.isFinishing()) {
                HighScoresActivity.this.dismissDialogSafely(13);
                HighScoresActivity.this.configureSearchListsSpinner();
            }
        }

        public void requestControllerDidFail(RequestController requestController, Exception e) {
            if (!HighScoresActivity.this.isFinishing()) {
                HighScoresActivity.this.dismissDialogSafely(13);
                if (e instanceof RequestCancelledException) {
                    HighScoresActivity.this.showDialog(2);
                } else {
                    HighScoresActivity.this.showDialog(3);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.scoreloop_highscores);
        setScoreloopImageCache(new LruCache(25));
        this.imageCache = getScoreloopImageCache();
        this.imageDatabaseCacher = new ImageDatabaseCacher(new ImageDownloadGate(this, getApplicationPreferences()), getJumbleeDB(), this.imageCache);
        this.scoresController = new ScoresController(getScoreloopSession(), new ScoresControllerObserver());
        ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(this, R.array.game_modes, 17367048);
        adapter.setDropDownViewResource(17367049);
        this.gameModeSpinner = (Spinner) findViewById(R.id.game_mode);
        this.gameModeSpinner.setAdapter((SpinnerAdapter) adapter);
        this.gameModeSpinner.setSelection(0);
        this.gameModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int gameMode, long id) {
                HighScoresActivity.this.scoresController.setMode(Integer.valueOf(gameMode));
                HighScoresActivity.this.loadScoresFromStart();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.searchListSpinner = (Spinner) findViewById(R.id.search_list_spinner);
        this.searchListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapter, View view, int position, long id) {
                HighScoresActivity.this.scoresController.setSearchList((SearchList) adapter.getItemAtPosition(position));
                HighScoresActivity.this.loadScoresFromStart();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.searchListSpinner.setEnabled(false);
        this.prevRangeButton = (Button) findViewById(R.id.btn_load_prev);
        this.prevRangeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighScoresActivity.this.showDialog(15);
                HighScoresActivity.this.loadPreviousScoreRange();
            }
        });
        this.nextRangeButton = (Button) findViewById(R.id.btn_load_next);
        this.nextRangeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighScoresActivity.this.showDialog(15);
                HighScoresActivity.this.loadNextScoreRange();
            }
        });
        ((Button) findViewById(R.id.btn_my_profile)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighScoresActivity.this.startActivity(new Intent(HighScoresActivity.this, GlobalProfileActivity.class));
            }
        });
        ((Button) findViewById(R.id.btn_show_me)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighScoresActivity.this.loadScoreRangeForUser();
            }
        });
        this.highScoresListView = (ListView) findViewById(R.id.list_view);
        if (!getScoreloopSession().isAuthenticated()) {
            UserController userController = new UserController(getScoreloopSession(), new UserLoadObserver());
            showDialog(13);
            userController.loadUser();
            return;
        }
        configureSearchListsSpinner();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.imageDatabaseCacher.shutdown();
        setScoreloopImageCache(null);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        getAnalytics().startTracking();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getAnalytics().stopTracking();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private boolean readyToLoad() {
        boolean gameModeSelected;
        boolean searchListSelected;
        if (this.gameModeSpinner.getSelectedItem() != null) {
            gameModeSelected = true;
        } else {
            gameModeSelected = false;
        }
        if (this.searchListSpinner.getSelectedItem() != null) {
            searchListSelected = true;
        } else {
            searchListSelected = false;
        }
        if (!gameModeSelected || !searchListSelected || this.currentOperation != CurrentOperationType.none) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void loadScoresFromStart() {
        if (readyToLoad()) {
            showDialog(15);
            this.currentOperation = CurrentOperationType.other;
            this.scoresController.loadRangeAtRank(1);
        }
    }

    /* access modifiers changed from: private */
    public void loadNextScoreRange() {
        if (readyToLoad()) {
            showDialog(15);
            this.currentOperation = CurrentOperationType.other;
            this.scoresController.loadNextRange();
        }
    }

    /* access modifiers changed from: private */
    public void loadPreviousScoreRange() {
        if (readyToLoad()) {
            showDialog(15);
            this.currentOperation = CurrentOperationType.other;
            this.scoresController.loadPreviousRange();
        }
    }

    /* access modifiers changed from: private */
    public void loadScoreRangeForUser() {
        if (readyToLoad()) {
            showDialog(15);
            this.currentOperation = CurrentOperationType.me;
            this.scoresController.loadRangeForUser(getScoreloopSession().getUser());
        }
    }

    /* access modifiers changed from: private */
    public void configureSearchListsSpinner() {
        ArrayAdapter<SearchList> adapter = new ArrayAdapter<>(this, 17367048, getMySearchLists());
        adapter.setDropDownViewResource(17367049);
        this.searchListSpinner.setAdapter((SpinnerAdapter) adapter);
        this.searchListSpinner.setEnabled(true);
        this.currentOperation = CurrentOperationType.none;
        this.searchListSpinner.setSelection(0);
    }

    private List<SearchList> getMySearchLists() {
        List<SearchList> mySearchLists = new ArrayList<>();
        for (SearchList searchList : getScoreloopSession().getScoreSearchLists()) {
            if (ACCEPTABLE_LEADERBOARDS.contains(searchList.getIdentifier())) {
                mySearchLists.add(searchList);
            }
        }
        Collections.sort(mySearchLists, new Comparator<SearchList>() {
            public int compare(SearchList o1, SearchList o2) {
                return -o1.getIdentifier().compareTo(o2.getIdentifier());
            }
        });
        return mySearchLists;
    }
}
