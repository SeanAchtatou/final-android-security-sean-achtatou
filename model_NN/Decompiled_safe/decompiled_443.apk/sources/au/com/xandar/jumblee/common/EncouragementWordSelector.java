package au.com.xandar.jumblee.common;

import android.content.Context;
import au.com.xandar.jumblee.R;

public class EncouragementWordSelector {
    private final Context context;
    private final int encouragementArrayResourceId;

    public EncouragementWordSelector(Context context2, int encouragementArrayResourceId2) {
        this.context = context2;
        this.encouragementArrayResourceId = encouragementArrayResourceId2;
    }

    public String getEncouragementWord(double score) {
        if (score == 0.0d) {
            return this.context.getString(R.string.encouragementWord_utterFailure);
        }
        String[] encouragementWords = this.context.getResources().getStringArray(this.encouragementArrayResourceId);
        int offset = ((int) score) / 40;
        return encouragementWords[offset < encouragementWords.length ? offset : encouragementWords.length - 1];
    }
}
