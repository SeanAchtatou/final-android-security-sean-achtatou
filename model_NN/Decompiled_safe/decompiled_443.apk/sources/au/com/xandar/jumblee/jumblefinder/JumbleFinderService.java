package au.com.xandar.jumblee.jumblefinder;

import android.app.IntentService;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import au.com.xandar.jumblee.JumbleeApplication;
import au.com.xandar.jumblee.db.JumbleeDB;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;

public final class JumbleFinderService extends IntentService {
    public JumbleFinderService() {
        super("JumbleFinderService");
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        findNewBatchOfJumbles();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void findNewBatchOfJumbles() {
        if (getJumbleeDB().getNrUnstartedJumbles() < 25) {
            getJumbleeDB().insertNewJumbles(new JumbleReader(this, getApplicationPreferences(), 30).getJumbles());
        }
    }

    private JumbleeDB getJumbleeDB() {
        return getJumbleeApplication().getJumbleeDB();
    }

    private ApplicationPreferences getApplicationPreferences() {
        return getJumbleeApplication().getApplicationPreferences();
    }

    private JumbleeApplication getJumbleeApplication() {
        return (JumbleeApplication) getApplication();
    }
}
