package au.com.xandar.jumblee.net;

import android.app.IntentService;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.util.Log;
import au.com.xandar.jumblee.JumbleeApplication;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.common.JumbleeHandledException;
import org.acra.ACRA;

public final class GeneralIntentService extends IntentService {
    public static final String EXECUTABLE_NAME_KEY = "executableName";

    public GeneralIntentService() {
        super("GeneralIntentService");
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        try {
            String executableName = intent.getStringExtra(EXECUTABLE_NAME_KEY);
            IntentExecutable executable = getJumbleeApplication().getNetExecutable(executableName);
            if (executable == null) {
                Log.w(AppConstants.TAG, "GeneralIntentService - Unknown netExecutable : " + executableName + " - quitting");
            } else {
                executable.execute(getJumbleeApplication(), intent);
            }
        } catch (RuntimeException e) {
            RuntimeException e2 = e;
            ACRA.getErrorReporter().handleSilentException(new JumbleeHandledException(e2.getMessage(), e2));
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private JumbleeApplication getJumbleeApplication() {
        return (JumbleeApplication) getApplication();
    }
}
