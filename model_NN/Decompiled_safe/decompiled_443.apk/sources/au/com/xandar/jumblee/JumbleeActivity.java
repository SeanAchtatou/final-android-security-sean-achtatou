package au.com.xandar.jumblee;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import au.com.xandar.android.PlatformConstants;
import au.com.xandar.android.app.XandarActivity;
import au.com.xandar.java.collection.LruCache;
import au.com.xandar.jumblee.db.JumbleeDB;
import au.com.xandar.jumblee.dialog.JumbleeDialogFactory;
import au.com.xandar.jumblee.metrics.Analytics;
import au.com.xandar.jumblee.metrics.FlurryAnalytics;
import au.com.xandar.jumblee.prefs.ApplicationPreferences;
import com.scoreloop.client.android.core.model.Session;
import java.util.concurrent.Executor;

public abstract class JumbleeActivity extends Activity implements XandarActivity {
    private final Analytics analytics = new FlurryAnalytics(this);
    private JumbleeDialogFactory dialogFactory;

    public JumbleeApplication getJumbleeApp() {
        return (JumbleeApplication) getApplication();
    }

    public ApplicationPreferences getApplicationPreferences() {
        return ((JumbleeApplication) getApplication()).getApplicationPreferences();
    }

    public JumbleeDB getJumbleeDB() {
        return ((JumbleeApplication) getApplication()).getJumbleeDB();
    }

    public Executor getExecutor() {
        return ((JumbleeApplication) getApplication()).getExecutor();
    }

    public JumbleeDialogFactory getDialogFactory() {
        if (this.dialogFactory == null) {
            this.dialogFactory = new JumbleeDialogFactory(this);
        }
        return this.dialogFactory;
    }

    public Analytics getAnalytics() {
        return this.analytics;
    }

    public Session getScoreloopSession() {
        return getJumbleeApp().getScoreloopSession();
    }

    public LruCache<String, Drawable> getScoreloopImageCache() {
        return getJumbleeApp().getScoreloopImageCache();
    }

    public void setScoreloopImageCache(LruCache<String, Drawable> cache) {
        getJumbleeApp().setScoreloopImageCache(cache);
    }

    public final void dismissDialogSafely(int dialogId) {
        try {
            dismissDialog(dialogId);
        } catch (IllegalArgumentException e) {
            Log.i(PlatformConstants.TAG, "Tried to dismiss dialog " + dialogId + " but its not there - nothing to dismiss so ignoring");
        }
    }
}
