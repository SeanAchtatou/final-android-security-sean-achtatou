package au.com.xandar.jumblee.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import au.com.xandar.jumblee.CurrentJumble;
import au.com.xandar.jumblee.Jumble;
import au.com.xandar.jumblee.db.JumbleeDBOpenHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

public final class JumbleeDB {
    private static final String[] ALL_JUMBLE_COLUMNS = {"_id", JumbleeDBOpenHelper.JumbleDBFields.WORD, JumbleeDBOpenHelper.JumbleDBFields.SPECIAL_LETTER, JumbleeDBOpenHelper.JumbleDBFields.POSSIBLE_WORDS, JumbleeDBOpenHelper.JumbleDBFields.FOUND_WORDS, JumbleeDBOpenHelper.JumbleDBFields.DATE_STARTED, "millis_remaining", JumbleeDBOpenHelper.JumbleDBFields.CURRENT_JUMBLE};
    private static final String[] ALL_SCORELOOP_IMAGE_CACHE_COLUMNS = {"_id", JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.SCORELOOP_USER_ID, JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE_URI, JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.DATE_LAST_READ, JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE};
    private static final int BOOLEAN_FALSE = 0;
    private static final int BOOLEAN_TRUE = 1;
    private static final String INSERT_STATEMENT = "INSERT INTO jumblee (word, special_letter, jumble_order) VALUES (?, ?, ?)";
    private static final String[] IS_TRUE_VALUES = {"1"};
    private static final int LARGE_SPREAD = 65536;
    private static final String UNSTARTED_JUMBLE_COUNT_QUERY = "select count(*) as nrJumbles from jumblee";
    private static final String WHERE_CLAUSE_CURRENT_JUMBLE = "current_jumble = ?";
    private final SQLiteDatabase db;
    private final Random random = new Random();
    private final WordListParser wordListParser = new WordListParser();

    public JumbleeDB(SQLiteDatabase db2) {
        this.db = db2;
    }

    public void close() {
        this.db.close();
    }

    public ScoresCursor getScoresCursor() {
        return ScoresCursor.getScores(this.db);
    }

    public Score[] getScoresToBePostedToFacebook() {
        Cursor cursor = this.db.query(JumbleeDBOpenHelper.ScoreTable.NAME, new String[]{"_id", JumbleeDBOpenHelper.ScoreDBFields.NR_FOUND_WORDS, "nr_possible_words", JumbleeDBOpenHelper.ScoreDBFields.WORDS_PER_MINUTE, JumbleeDBOpenHelper.ScoreDBFields.PERCENTAGE_WORDS_FOUND, JumbleeDBOpenHelper.ScoreDBFields.SCORE}, "post_score_to_facebook = 1", null, null, null, null);
        Score[] scoresToPost = new Score[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            Score score = new Score();
            score.setId((long) cursor.getInt(cursor.getColumnIndex("_id")));
            score.setNrFoundWords(cursor.getInt(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.NR_FOUND_WORDS)));
            score.setNrPossibleWords(cursor.getInt(cursor.getColumnIndex("nr_possible_words")));
            score.setWordsPerMinute((double) cursor.getFloat(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.WORDS_PER_MINUTE)));
            score.setPercentageFound((double) cursor.getFloat(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.PERCENTAGE_WORDS_FOUND)));
            score.setScore((double) cursor.getFloat(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreDBFields.SCORE)));
            scoresToPost[i] = score;
            i++;
        }
        cursor.close();
        return scoresToPost;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void markScoreAsPostedToFacebook(long id) {
        ContentValues values = new ContentValues();
        values.put(JumbleeDBOpenHelper.ScoreDBFields.DATE_SCORE_POSTED_TO_FACEBOOK, Long.valueOf(new Date().getTime()));
        values.put(JumbleeDBOpenHelper.ScoreDBFields.POST_SCORE_TO_FACEBOOK, (Integer) 0);
        this.db.update(JumbleeDBOpenHelper.ScoreTable.NAME, values, "_id = ?", new String[]{String.valueOf(id)});
    }

    public List<AbbreviatedScore> getScoresToBePostedToScoreloop() {
        List<AbbreviatedScore> scores = new ArrayList<>();
        scores.addAll(getScoresToBePostedForScoreType(0));
        scores.addAll(getScoresToBePostedForScoreType(1));
        scores.addAll(getScoresToBePostedForScoreType(2));
        return scores;
    }

    private List<AbbreviatedScore> getScoresToBePostedForScoreType(int scoreType) {
        String scoreField = getScoreloopField(scoreType);
        String scoreFieldMarker = getScoreloopFieldMarker(scoreType);
        Cursor cursor = this.db.query(JumbleeDBOpenHelper.ScoreTable.NAME, new String[]{"_id", scoreField}, scoreFieldMarker + " = " + 1, null, null, null, null);
        List<AbbreviatedScore> scores = new ArrayList<>();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            scores.add(new AbbreviatedScore((long) id, scoreType, cursor.getDouble(cursor.getColumnIndex(scoreField))));
        }
        cursor.close();
        return scores;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void markScoreAsPostedToScoreloop(long id, int scoreType) {
        ContentValues values = new ContentValues();
        values.put(getScoreloopFieldMarker(scoreType), (Integer) 0);
        this.db.update(JumbleeDBOpenHelper.ScoreTable.NAME, values, "_id = ?", new String[]{String.valueOf(id)});
    }

    private String getScoreloopField(int scoreType) {
        switch (scoreType) {
            case 0:
                return JumbleeDBOpenHelper.ScoreDBFields.SCORE;
            case 1:
                return JumbleeDBOpenHelper.ScoreDBFields.PERCENTAGE_WORDS_FOUND;
            case 2:
                return JumbleeDBOpenHelper.ScoreDBFields.WORDS_PER_MINUTE;
            default:
                throw new IllegalArgumentException("Invalid Scoreloop mode : " + scoreType);
        }
    }

    private String getScoreloopFieldMarker(int scoreType) {
        switch (scoreType) {
            case 0:
                return JumbleeDBOpenHelper.ScoreDBFields.POST_GLOBAL_SCORE;
            case 1:
                return JumbleeDBOpenHelper.ScoreDBFields.POST_GLOBAL_PERCENTAGE;
            case 2:
                return JumbleeDBOpenHelper.ScoreDBFields.POST_GLOBAL_WORDS_PER_MINUTE;
            default:
                throw new IllegalArgumentException("Invalid Scoreloop mode : " + scoreType);
        }
    }

    public void insertNewJumbles(Collection<Jumble> jumbles) {
        this.db.beginTransaction();
        SQLiteStatement insertStatement = this.db.compileStatement(INSERT_STATEMENT);
        for (Jumble jumble : jumbles) {
            insertStatement.bindString(1, jumble.getWord());
            insertStatement.bindString(2, jumble.getSpecialLetter());
            insertStatement.bindLong(3, (long) this.random.nextInt(LARGE_SPREAD));
            insertStatement.execute();
        }
        insertStatement.close();
        this.db.setTransactionSuccessful();
        this.db.endTransaction();
    }

    public CurrentJumble getCurrentJumble() {
        CurrentJumble currentJumble;
        Cursor cursor = this.db.query(JumbleeDBOpenHelper.JumbleeTable.NAME, ALL_JUMBLE_COLUMNS, WHERE_CLAUSE_CURRENT_JUMBLE, IS_TRUE_VALUES, null, null, null);
        if (cursor.moveToFirst()) {
            currentJumble = getPrototype(cursor).toJumble();
        } else {
            currentJumble = CurrentJumble.NO_CURRENT_JUMBLE;
        }
        cursor.close();
        return currentJumble;
    }

    public synchronized JumbleHolder getNewJumble() {
        JumbleHolder jumbleHolder;
        Cursor cursor = this.db.query(JumbleeDBOpenHelper.JumbleeTable.NAME, ALL_JUMBLE_COLUMNS, null, null, null, null, JumbleeDBOpenHelper.JumbleDBFields.JUMBLE_ORDER);
        int nrJumbles = cursor.getCount();
        if (nrJumbles == 0) {
            cursor.close();
            jumbleHolder = new JumbleHolder(0, CurrentJumble.NO_CURRENT_JUMBLE);
        } else {
            cursor.moveToFirst();
            JumblePrototype prototype = getPrototype(cursor);
            cursor.close();
            prototype.setDateStartedAsMillis(new Date().getTime());
            prototype.setMillisRemaining(60000);
            prototype.setCurrentJumble(1);
            jumbleHolder = new JumbleHolder(nrJumbles, prototype.toJumble());
        }
        return jumbleHolder;
    }

    public void updateJumbleWords(Integer id, Jumble jumble) {
        ContentValues values = new ContentValues();
        values.put(JumbleeDBOpenHelper.JumbleDBFields.POSSIBLE_WORDS, jumble.getPossibleWordsAsCommaSeparatedString());
        values.put("nr_possible_words", Integer.valueOf(jumble.getNrPossibleWords()));
        this.db.update(JumbleeDBOpenHelper.JumbleeTable.NAME, values, "_id = ?", new String[]{id.toString()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized void updateJumble(CurrentJumble jumble) {
        ContentValues values = new ContentValues();
        long now = new Date().getTime();
        values.put(JumbleeDBOpenHelper.JumbleDBFields.FOUND_WORDS, this.wordListParser.getCommaSeparatedString(jumble.getFoundWords()));
        values.put(JumbleeDBOpenHelper.JumbleDBFields.DATE_LAST_PLAYED, Long.valueOf(now));
        values.put("millis_remaining", Long.valueOf(jumble.getMillisRemaining()));
        values.put(JumbleeDBOpenHelper.JumbleDBFields.DATE_STARTED, Long.valueOf(jumble.getDateStartedAsMillis()));
        if (jumble.isCurrentJumble()) {
            values.put(JumbleeDBOpenHelper.JumbleDBFields.CURRENT_JUMBLE, (Integer) 1);
        } else {
            values.putNull(JumbleeDBOpenHelper.JumbleDBFields.CURRENT_JUMBLE);
        }
        this.db.update(JumbleeDBOpenHelper.JumbleeTable.NAME, values, "_id = ?", new String[]{jumble.getId().toString()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized void updateScoreToPostToFacebook(long id) {
        ContentValues values = new ContentValues();
        values.put(JumbleeDBOpenHelper.ScoreDBFields.POST_SCORE_TO_FACEBOOK, (Integer) 1);
        this.db.update(JumbleeDBOpenHelper.ScoreTable.NAME, values, "_id = ?", new String[]{Long.toString(id)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized Score completeGame(CurrentJumble jumble) {
        Score score;
        long now = new Date().getTime();
        this.db.beginTransaction();
        try {
            this.db.delete(JumbleeDBOpenHelper.JumbleeTable.NAME, "_id = ?", new String[]{jumble.getId().toString()});
            ContentValues values = new ContentValues();
            values.put("date_completed", Long.valueOf(now));
            values.put(JumbleeDBOpenHelper.ScoreDBFields.NR_FOUND_WORDS, Integer.valueOf(jumble.getNrFoundWords()));
            values.put("nr_possible_words", Integer.valueOf(jumble.getNrPossibleWords()));
            values.put(JumbleeDBOpenHelper.ScoreDBFields.MILLIS_TAKEN, Long.valueOf(jumble.getMillisTaken()));
            values.put("millis_remaining", Long.valueOf(jumble.getMillisRemaining()));
            values.put(JumbleeDBOpenHelper.ScoreDBFields.WORDS_PER_MINUTE, Double.valueOf(jumble.getWordsPerMinute()));
            values.put(JumbleeDBOpenHelper.ScoreDBFields.PERCENTAGE_WORDS_FOUND, Double.valueOf(jumble.getPercentageFound()));
            values.put(JumbleeDBOpenHelper.ScoreDBFields.SCORE, Double.valueOf(jumble.getScore()));
            values.put(JumbleeDBOpenHelper.ScoreDBFields.POST_SCORE_TO_FACEBOOK, (Integer) 0);
            values.put(JumbleeDBOpenHelper.ScoreDBFields.POST_GLOBAL_SCORE, (Integer) 1);
            values.put(JumbleeDBOpenHelper.ScoreDBFields.POST_GLOBAL_PERCENTAGE, (Integer) 1);
            values.put(JumbleeDBOpenHelper.ScoreDBFields.POST_GLOBAL_WORDS_PER_MINUTE, (Integer) 1);
            long scoreId = this.db.insert(JumbleeDBOpenHelper.ScoreTable.NAME, "date_completed", values);
            this.db.setTransactionSuccessful();
            this.db.endTransaction();
            score = new Score();
            score.setId(scoreId);
            score.setNrFoundWords(jumble.getNrFoundWords());
            score.setNrPossibleWords(jumble.getNrPossibleWords());
            score.setWordsPerMinute(jumble.getWordsPerMinute());
            score.setPercentageFound(jumble.getPercentageFound());
            score.setScore(jumble.getScore());
        } catch (Throwable th) {
            this.db.endTransaction();
            throw th;
        }
        return score;
    }

    public synchronized int getNrUnstartedJumbles() {
        int nrJumbles;
        Cursor cursor = this.db.rawQuery(UNSTARTED_JUMBLE_COUNT_QUERY, null);
        cursor.moveToFirst();
        nrJumbles = cursor.getInt(0);
        cursor.close();
        return nrJumbles;
    }

    public ScoreloopImage getScoreloopImage(String scoreloopId) {
        Cursor cursor = this.db.query(JumbleeDBOpenHelper.ScoreloopImageCacheTable.NAME, ALL_SCORELOOP_IMAGE_CACHE_COLUMNS, "scoreloop_id = ?", new String[]{scoreloopId}, null, null, null);
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }
        cursor.moveToFirst();
        ScoreloopImage scoreloopImage = new ScoreloopImage();
        scoreloopImage.setId((long) cursor.getInt(cursor.getColumnIndex("_id")));
        scoreloopImage.setScoreloopUserId(cursor.getString(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.SCORELOOP_USER_ID)));
        scoreloopImage.setImageUri(cursor.getString(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE_URI)));
        scoreloopImage.setDateLastReadAsMillis((long) cursor.getInt(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.DATE_LAST_READ)));
        scoreloopImage.setImageBytes(cursor.getBlob(cursor.getColumnIndex(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE)));
        cursor.close();
        return scoreloopImage;
    }

    public void insertScoreloopImage(ScoreloopImage scoreloopImage) {
        ContentValues values = new ContentValues();
        values.put(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.SCORELOOP_USER_ID, scoreloopImage.getScoreloopUserId());
        values.put(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.DATE_LAST_READ, Long.valueOf(scoreloopImage.getDateLastReadAsMillis()));
        values.put(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE_URI, scoreloopImage.getImageUri());
        values.put(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE, scoreloopImage.getImageBytes());
        this.db.insert(JumbleeDBOpenHelper.ScoreloopImageCacheTable.NAME, JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.SCORELOOP_USER_ID, values);
    }

    public void updateScoreloopImage(ScoreloopImage scoreloopImage) {
        ContentValues values = new ContentValues();
        values.put(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE_URI, scoreloopImage.getImageUri());
        values.put(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.IMAGE, scoreloopImage.getImageBytes());
        this.db.update(JumbleeDBOpenHelper.ScoreloopImageCacheTable.NAME, values, "scoreloop_id = ?", new String[]{scoreloopImage.getScoreloopUserId()});
    }

    public void updateScoreloopImageLastRead(Long id) {
        ContentValues values = new ContentValues();
        values.put(JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.DATE_LAST_READ, Long.valueOf(System.currentTimeMillis()));
        this.db.update(JumbleeDBOpenHelper.ScoreloopImageCacheTable.NAME, values, "_id = ?", new String[]{id.toString()});
    }

    public synchronized void trimScoreloopImageCache(int nrImagesToKeep) {
        Cursor cursor = this.db.query(JumbleeDBOpenHelper.ScoreloopImageCacheTable.NAME, new String[]{"_id"}, null, null, null, null, JumbleeDBOpenHelper.ScoreloopImageCacheDBFields.DATE_LAST_READ);
        int nrCachedImages = cursor.getCount();
        cursor.moveToFirst();
        Collection<Long> idsToDelete = new ArrayList<>();
        for (int i = nrImagesToKeep; i < nrCachedImages; i++) {
            idsToDelete.add(Long.valueOf(cursor.getLong(cursor.getColumnIndex("_id"))));
        }
        cursor.close();
        for (Long id : idsToDelete) {
            this.db.delete(JumbleeDBOpenHelper.ScoreloopImageCacheTable.NAME, "_id = ?", new String[]{id.toString()});
        }
    }

    private JumblePrototype getPrototype(Cursor cursor) {
        JumblePrototype proto = new JumblePrototype();
        proto.setId(Integer.valueOf(cursor.getInt(cursor.getColumnIndex("_id"))));
        proto.setWord(cursor.getString(cursor.getColumnIndex(JumbleeDBOpenHelper.JumbleDBFields.WORD)));
        proto.setSpecialLetter(cursor.getString(cursor.getColumnIndex(JumbleeDBOpenHelper.JumbleDBFields.SPECIAL_LETTER)));
        proto.setPossibleWordsText(cursor.getString(cursor.getColumnIndex(JumbleeDBOpenHelper.JumbleDBFields.POSSIBLE_WORDS)));
        proto.setFoundWordsText(cursor.getString(cursor.getColumnIndex(JumbleeDBOpenHelper.JumbleDBFields.FOUND_WORDS)));
        proto.setDateStartedAsMillis(cursor.getLong(cursor.getColumnIndex(JumbleeDBOpenHelper.JumbleDBFields.DATE_STARTED)));
        proto.setMillisRemaining(cursor.getLong(cursor.getColumnIndex("millis_remaining")));
        proto.setCurrentJumble(Integer.valueOf(cursor.getInt(cursor.getColumnIndex(JumbleeDBOpenHelper.JumbleDBFields.CURRENT_JUMBLE))));
        return proto;
    }
}
