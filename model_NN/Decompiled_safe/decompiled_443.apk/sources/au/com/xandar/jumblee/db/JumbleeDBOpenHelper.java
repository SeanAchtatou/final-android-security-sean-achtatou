package au.com.xandar.jumblee.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import au.com.xandar.jumblee.Jumble;
import au.com.xandar.jumblee.jumblefinder.MutableJumble;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public final class JumbleeDBOpenHelper extends SQLiteOpenHelper {
    private static final int CURRENT_DB_VERSION = 17;
    private static final int DB_VERSION_AT_WHICH_WE_ADDED_GLOBAL_SCORES = 16;
    private static final int DB_VERSION_AT_WHICH_WE_ADDED_SCORELOOP_IMAGE_CACHING = 17;
    private static final int DB_VERSION_AT_WHICH_WE_CHANGED_SCORE_TO_BE_NON_LINEAR_FOR_WORDS_PER_MINUTE = 15;
    private static final int DB_VERSION_AT_WHICH_WE_INTRODUCED_POST_TO_FACEBOOK = 14;

    public static class JumbleDBFields {
        public static final String CURRENT_JUMBLE = "current_jumble";
        public static final String DATE_COMPLETED = "date_completed";
        public static final String DATE_LAST_PLAYED = "date_last_played";
        public static final String DATE_STARTED = "date_started";
        public static final String FOUND_WORDS = "found_words";
        public static final String ID = "_id";
        public static final String JUMBLE_ORDER = "jumble_order";
        public static final String MILLIS_REMAINING = "millis_remaining";
        public static final String NR_POSSIBLE_WORDS = "nr_possible_words";
        public static final String POSSIBLE_WORDS = "possible_words";
        public static final String SPECIAL_LETTER = "special_letter";
        public static final String WORD = "word";
    }

    public static class JumbleeTable {
        private static final String INDEX_CURRENT = "jumblee_current";
        private static final String INDEX_JUMBLE_ORDER = "jumble_order";
        public static final String NAME = "jumblee";
    }

    public static class ScoreDBFields {
        public static final String DATE_COMPLETED = "date_completed";
        public static final String DATE_SCORE_POSTED_TO_FACEBOOK = "date_score_posted_to_facebook";
        public static final String ID = "_id";
        public static final String MILLIS_REMAINING = "millis_remaining";
        public static final String MILLIS_TAKEN = "millis_taken";
        public static final String NR_FOUND_WORDS = "nr_found_words";
        public static final String NR_POSSIBLE_WORDS = "nr_possible_words";
        public static final String PERCENTAGE_WORDS_FOUND = "percentage_words_found";
        public static final String POST_GLOBAL_PERCENTAGE = "post_global_percentage";
        public static final String POST_GLOBAL_SCORE = "post_global_score";
        public static final String POST_GLOBAL_WORDS_PER_MINUTE = "post_global_words_per_minute";
        public static final String POST_SCORE_TO_FACEBOOK = "post_score_to_facebook";
        public static final String SCORE = "score";
        public static final String WORDS_PER_MINUTE = "words_per_minute";
    }

    public static class ScoreTable {
        private static final String INDEX_DATE_COMPLETED = "gamescore_completed";
        private static final String INDEX_SCORE = "gamescore_score";
        public static final String NAME = "gamescore";
    }

    public static class ScoreloopImageCacheDBFields {
        public static final String DATE_LAST_READ = "date_last_read";
        public static final String ID = "_id";
        public static final String IMAGE = "image";
        public static final String IMAGE_URI = "image_uri";
        public static final String SCORELOOP_USER_ID = "scoreloop_id";
    }

    public static class ScoreloopImageCacheTable {
        public static final String NAME = "scoreloopImageCache";
    }

    public JumbleeDBOpenHelper(Context context) {
        super(context, "jumblee.db", (SQLiteDatabase.CursorFactory) null, 17);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE jumblee(_id INTEGER PRIMARY KEY AUTOINCREMENT, word TEXT, special_letter TEXT, nr_possible_words INTEGER, possible_words TEXT, found_words TEXT, date_started INTEGER, date_last_played INTEGER, date_completed INTEGER, millis_remaining INTEGER DEFAULT 0,current_jumble INTEGER, jumble_order INTEGER)");
        db.execSQL("CREATE INDEX jumblee_current on jumblee (current_jumble)");
        db.execSQL("CREATE INDEX jumble_order on jumblee (jumble_order)");
        new JumbleeDB(db).insertNewJumbles(getStartingJumbles());
        db.execSQL("CREATE TABLE gamescore(_id INTEGER PRIMARY KEY AUTOINCREMENT, date_completed INTEGER, nr_found_words INTEGER, nr_possible_words INTEGER, millis_taken INTEGER, millis_remaining INTEGER, words_per_minute REAL, percentage_words_found REAL, score REAL,post_score_to_facebook INTEGER, date_score_posted_to_facebook INTEGER, post_global_score INTEGER, post_global_percentage INTEGER, post_global_words_per_minute INTEGER)");
        db.execSQL("CREATE INDEX gamescore_completed on gamescore (date_completed)");
        db.execSQL("CREATE INDEX gamescore_score on gamescore (score)");
        db.execSQL("CREATE TABLE scoreloopImageCache(_id INTEGER PRIMARY KEY AUTOINCREMENT, scoreloop_id INTEGER, date_last_read INTEGER, image_uri TEXT, image BLOB)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 14) {
            db.execSQL("ALTER TABLE gamescore ADD COLUMN post_score_to_facebook INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE gamescore ADD COLUMN date_score_posted_to_facebook INTEGER");
        }
        if (oldVersion < 15) {
            updateScoresUsingNonLinearWordPerMinuteCalculation(db);
        }
        if (oldVersion < 16) {
            db.execSQL("ALTER TABLE gamescore ADD COLUMN post_global_score INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE gamescore ADD COLUMN post_global_percentage INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE gamescore ADD COLUMN post_global_words_per_minute INTEGER DEFAULT 0");
        }
        if (oldVersion < 17) {
            db.execSQL("CREATE TABLE scoreloopImageCache(_id INTEGER PRIMARY KEY AUTOINCREMENT, scoreloop_id INTEGER, date_last_read INTEGER, image_uri TEXT, image BLOB)");
        }
    }

    private Collection<Jumble> getStartingJumbles() {
        Random random = new Random();
        Collection<Jumble> startingJumbles = new ArrayList<>();
        for (String word : new String[]{"uncovered", "motivator", "dislocate", "waterline", "obstinate", "causative"}) {
            int specialLetterOffset = random.nextInt(9);
            startingJumbles.add(new MutableJumble(word, word.substring(specialLetterOffset, specialLetterOffset + 1)));
        }
        return startingJumbles;
    }

    private void updateScoresUsingNonLinearWordPerMinuteCalculation(SQLiteDatabase db) {
        SQLiteStatement updateStatement = db.compileStatement("UPDATE gamescore SET score = ? WHERE _id = ?");
        Cursor cursor = db.query(ScoreTable.NAME, new String[]{"_id", ScoreDBFields.WORDS_PER_MINUTE, ScoreDBFields.PERCENTAGE_WORDS_FOUND, ScoreDBFields.SCORE}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            long scoreId = cursor.getLong(cursor.getColumnIndex("_id"));
            double percentageFound = (double) cursor.getFloat(cursor.getColumnIndex(ScoreDBFields.PERCENTAGE_WORDS_FOUND));
            double d = (double) cursor.getFloat(cursor.getColumnIndex(ScoreDBFields.SCORE));
            updateStatement.bindLong(2, scoreId);
            updateStatement.bindDouble(1, Math.sqrt((double) cursor.getFloat(cursor.getColumnIndex(ScoreDBFields.WORDS_PER_MINUTE))) * percentageFound * 2.0d);
            updateStatement.execute();
        }
        cursor.close();
    }
}
