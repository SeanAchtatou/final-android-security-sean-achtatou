package au.com.xandar.jumblee.dialog;

public final class DialogId {
    public static final int ABOUT_DIALOG = 90;
    public static final int COMPLETE_GAME_DIALOG = 16;
    public static final int CONFIRM_END_GAME_DIALOG = 17;
    public static final int EULA_DIALOG = 22;
    public static final int EULA_DIALOG_WITH_REJECT_BUTTON = 21;
    public static final int FACEBOOK_AUTHENTICATION_DIALOG = 100;
    public static final int FACEBOOK_AUTH_PROGRESS_DIALOG = 101;
    public static final int FACEBOOK_INTEGRATION_DIALOG = 80;
    public static final int HOW_TO_PLAY_DIALOG = 30;
    public static final int NO_AVAILABLE_JUMBLES_DIALOG = 70;
    public static final int PIRACY_DIALOG = 50;
    public static final int PROGRESS_DIALOG_RESUMING_GAME = 15;
    public static final int PROGRESS_DIALOG_SAVING_GAME = 13;
    public static final int PROGRESS_DIALOG_STARTING_NEW_GAME = 11;
    public static final int RATE_ME_DIALOG = 60;
    public static final int UPDATE_VERSION_DIALOG = 40;
}
