package au.com.xandar.jumblee.game.widget;

final class SimpleCharSequence implements CharSequence {
    private char[] buffer;
    private final StringBuilder sb;

    SimpleCharSequence(int length) {
        this.buffer = new char[length];
        this.sb = new StringBuilder(length);
    }

    public int length() {
        return this.sb.length();
    }

    public char charAt(int index) {
        return this.sb.charAt(index);
    }

    public CharSequence subSequence(int start, int end) {
        return this.sb.subSequence(start, end);
    }

    public void clear() {
        this.sb.setLength(0);
    }

    public char[] getChars() {
        this.sb.getChars(0, this.sb.length(), this.buffer, 0);
        return this.buffer;
    }

    public void appendSeconds(int seconds) {
        this.sb.append(seconds);
        this.sb.append('s');
    }

    public void appendInteger(double value) {
        this.sb.append((int) value);
    }

    public void appendNumberToOneDecimalPlace(double value) {
        appendInteger(value);
        appendDot();
        appendInteger((value - Math.floor(value)) * 10.0d);
    }

    public void appendNumberToTwoDecimalPlaces(double value) {
        appendInteger(value);
        appendDot();
        double decimalPortion = (value - Math.floor(value)) * 100.0d;
        if (decimalPortion < 10.0d) {
            appendInteger(0.0d);
        }
        appendInteger(decimalPortion);
    }

    private void appendDot() {
        this.sb.append(".");
    }
}
