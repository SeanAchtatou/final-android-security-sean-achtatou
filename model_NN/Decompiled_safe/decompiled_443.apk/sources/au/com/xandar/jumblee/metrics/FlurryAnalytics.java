package au.com.xandar.jumblee.metrics;

import android.content.Context;
import au.com.xandar.android.pm.PackageManagerUtility;
import au.com.xandar.jumblee.CurrentJumble;
import au.com.xandar.jumblee.common.AppConstants;
import au.com.xandar.jumblee.db.JumbleeDBOpenHelper;
import com.flurry.android.FlurryAgent;
import java.util.HashMap;
import java.util.Map;

public final class FlurryAnalytics implements Analytics {
    private static final String[] PERCENT_FOUND_GROUPS = {"01-10%", "10-20%", "20-30%", "30-40%", "40-50%", "50-60%", "60-70%", "70-80%", "80-90%", "90-99%", "100%"};
    private static final String PERCENT_FOUND_ZERO = "0%";
    private static final String[] SCORE_GROUPS = {"0-50", "50-100", "100-150", "150-200", "200-250", "250-300", "300-350", "350-400", "400-450", "450-500", "500-550", "550-600", "600+"};
    private static final String[] WORDS_PER_MINUTE_GROUPS = {"0-2", "2-4", "4-6", "6-8", "8-10", "10-12", "12-14", "14-16", "16-18", "18-20", "20+"};
    private final Context context;
    private final PackageManagerUtility packageManagerUtility;

    private static class EventType {
        private static final String AD_CLICKED = "adClicked";
        private static final String AD_IMPRESSION = "adImpressionReceived";
        private static final String AD_REQUESTED = "adRequested";
        private static final String GAME_OVER = "gameOver";
        private static final String GAME_PIRATED = "gamePirated";
        private static final String INSTALLED = "installed";
        private static final String POST_TO_FACEBOOK = "postToFacebook";
        private static final String START_GAME = "startGame";
        private static final String WORD_REJECTED = "wordRejected";

        private EventType() {
        }
    }

    private static class StartGameParams {
        private static final String AD_BLOCKER = "adBlocker";
        private static final String HAS_MARKET = "hasMarket";

        private StartGameParams() {
        }
    }

    private static class GameOverParams {
        private static final String PERCENT_FOUND = "percentFound";
        private static final String SCORE = "score";
        private static final String WORDS_PER_MINUTE = "wordsPerMinute";

        private GameOverParams() {
        }
    }

    private static class WordRejectedParams {
        private static final String WORD = "word";

        private WordRejectedParams() {
        }
    }

    private static class GamePiratedParams {
        private static final String PIRATE_PACKAGE_NAME = "piratePackageName";

        private GamePiratedParams() {
        }
    }

    public FlurryAnalytics(Context context2) {
        this.context = context2;
        this.packageManagerUtility = new PackageManagerUtility(context2);
    }

    public void startTracking() {
        FlurryAgent.onStartSession(this.context, AppConstants.FLURRY_APP_KEY_FOR_JUMBLEE);
    }

    public void stopTracking() {
        FlurryAgent.onEndSession(this.context);
    }

    public void sendInstalled(Map<String, String> params) {
        FlurryAgent.onEvent("installed", params);
    }

    public void sendStartGame() {
        boolean isMarketAvailable = this.packageManagerUtility.isMarketAvailable();
        String adBlockerPackage = this.packageManagerUtility.getAdBlockerPackage();
        Map<String, String> params = new HashMap<>();
        params.put("hasMarket", isMarketAvailable ? "market" : "no-market");
        params.put("adBlocker", adBlockerPackage);
        FlurryAgent.onEvent("startGame", params);
    }

    private String getPercentFoundGroup(CurrentJumble jumble) {
        if (jumble.getPercentageFound() == 0.0d) {
            return PERCENT_FOUND_ZERO;
        }
        int index = ((int) jumble.getPercentageFound()) / 10;
        return PERCENT_FOUND_GROUPS[index < PERCENT_FOUND_GROUPS.length ? index : PERCENT_FOUND_GROUPS.length - 1];
    }

    private String getScoreGroup(CurrentJumble jumble) {
        int index = ((int) jumble.getScore()) / 50;
        return SCORE_GROUPS[index < SCORE_GROUPS.length ? index : SCORE_GROUPS.length - 1];
    }

    private String getWordsPerMinuteGroup(CurrentJumble jumble) {
        int index = ((int) jumble.getWordsPerMinute()) / 2;
        return WORDS_PER_MINUTE_GROUPS[index < WORDS_PER_MINUTE_GROUPS.length ? index : WORDS_PER_MINUTE_GROUPS.length - 1];
    }

    public void sendGameOver(CurrentJumble jumble) {
        Map<String, String> params = new HashMap<>();
        params.put(JumbleeDBOpenHelper.ScoreDBFields.SCORE, getScoreGroup(jumble));
        params.put("percentFound", getPercentFoundGroup(jumble));
        params.put("wordsPerMinute", getWordsPerMinuteGroup(jumble));
        FlurryAgent.onEvent("gameOver", params);
    }

    public void sendPostToFacebook() {
        FlurryAgent.onEvent("postToFacebook");
    }

    public void sendWordRejected(String rejectedWord) {
        Map<String, String> params = new HashMap<>();
        params.put(JumbleeDBOpenHelper.JumbleDBFields.WORD, rejectedWord);
        FlurryAgent.onEvent("wordRejected", params);
    }

    public void sendAdRequested() {
        FlurryAgent.onEvent("adRequested");
    }

    public void sendAdImpressionReceived() {
        FlurryAgent.onEvent("adImpressionReceived");
    }

    public void sendAdClicked() {
        FlurryAgent.onEvent("adClicked");
    }

    public void sendPirateName(String piratePackageName) {
        Map<String, String> params = new HashMap<>();
        params.put("piratePackageName", piratePackageName);
        FlurryAgent.onEvent("gamePirated", params);
    }
}
