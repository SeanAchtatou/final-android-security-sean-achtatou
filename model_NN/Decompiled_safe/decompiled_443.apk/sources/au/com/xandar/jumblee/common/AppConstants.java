package au.com.xandar.jumblee.common;

import android.content.Intent;
import android.net.Uri;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public final class AppConstants {
    public static final long ACCEPTED_WORD_BONUS_MILLIS = 20000;
    public static final int ADDITIONAL_JUMBLES_TO_FIND = 30;
    public static final String ADMOB_APPLICATION_ID = "a14ccc2511575b1";
    public static final Set<String> AD_KEY_WORDS = new HashSet(Arrays.asList("word puzzle", "word game"));
    public static final String APPLICATION_SIGNATURE = "30820267308201d0a00302010202044d368120300d06092a864886f70d01010505003078310b3009060355040613024155310c300a06035504081303514c443111300f060355040713084272697362616e6531173015060355040a130e58616e64617220507479204c746431143012060355040b130b446576656c6f706d656e74311930170603550403131057696c6c69616d204665726775736f6e301e170d3131303131393036313335325a170d3336303131333036313335325a3078310b3009060355040613024155310c300a06035504081303514c443111300f060355040713084272697362616e6531173015060355040a130e58616e64617220507479204c746431143012060355040b130b446576656c6f706d656e74311930170603550403131057696c6c69616d204665726775736f6e30819f300d06092a864886f70d010101050003818d0030818902818100a27aca219ad40ded94cd8446dfa872ae0753547d636af5fffaa3ec57b5590f0ccbebf11f5a4bb8864bc8dd956c964c347afc3a8b29a17d9572a3361311bcad1e23479dcf843e7bd759b9f83056dbf602f34ca62df735f55bf8010a711d3ba183631ab0d654e1ed77f9c72960c89e8e69ceba757e41c7910324c413048ddd45970203010001300d06092a864886f70d01010505000381810082784ca7b8c80e31d38bfea199b73432c065a34784d24198e2a6ed2e1e658ac797cbf388b0a498e9763b313c09819a35e1f060003cb024bcf17cfd2de0dbde40c0f3fb4bc83049da49c600187467332c9f95fd308ff836234cc480e92574fff97973ad2240004d90c874ff90482141e697721805b8804bc8bd2b807b2a6ea097";
    public static final long BUTTON_VIBRATE_MILLIS = 20;
    public static final boolean DEV_LOGGING = false;
    public static final boolean DEV_MODE = false;
    public static final int ENCOURAGEMENT_WORD_SCORE_STEP = 40;
    public static final String EXTERNAL_RESOURCE_SITE = "http://jumblee.xandar.com.au";
    public static final String FACEBOOK_APP_ID_FOR_JUMBLEE = "222637494420480";
    public static final String FACEBOOK_APP_SITE = "https://www.facebook.com/apps/application.php?id=222637494420480";
    public static final int FACEBOOK_AUTHENTICATION_ACTIVITY_CODE = 32665;
    public static final String[] FACEBOOK_PERMISSIONS = {"publish_stream, offline_access"};
    public static final String FLURRY_APP_KEY_FOR_JUMBLEE = "EPZYFNJNUBT73RKBYAIA";
    public static final int HTTP_CONNECTION_SIZE_BYTES = 8192;
    public static final long INITIAL_GAME_LENGTH_MILLIS = 60000;
    public static final String LATEST_VERSION_URL = "http://jumblee.xandar.com.au/latestVersion.txt";
    public static final Intent MARKET_INTENT = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=au.com.xandar.jumblee"));
    public static final String MARKET_URL = "https://market.android.com/details?id=au.com.xandar.jumblee";
    public static final int MINIMUM_UNSTARTED_JUMBLES_ALLOWED = 25;
    public static final int NR_GAMES_BEFORE_FACEBOOK_IS_SUGGESTED = 16;
    public static final int NR_GAMES_BEFORE_RATE_ME_IS_REQUESTED = 8;
    public static final long NR_MILLIS_BEFORE_REPORT_PIRACY_TO_USER = 864000000;
    public static final long NR_MILLIS_BEFORE_REQUESTING_NEXT_RATE_ME = 1209600000;
    public static final long NR_MILLIS_IN_A_DAY = 86400000;
    public static final long NR_MILLIS_IN_A_MINUTE = 60000;
    public static final long NR_MILLIS_IN_A_SECOND = 1000;
    public static final String SCORELOOP_GAME_SECRET = "nsDSQOS6CN/14Y8vx7voikEROeo2m2p7/b3+06QFEsrNDUw1uCM2jg==";
    public static final double SCORE_MULTIPLIER = 2.0d;
    public static final String SUPPORT_EMAIL_ADDRESS = "support@jumblee.org";
    public static final String TAG = "xandar.Jumblee";
    public static final int TOTAL_NR_OF_NINE_LETTER_JUMBLES = 11600;
    public static final String WEB_SITE = "http://www.jumblee.org";

    static {
        MARKET_INTENT.setFlags(524288);
    }
}
