package au.com.xandar.android.time;

public interface TimerListener {
    void onFinish();

    void onTick(long j);
}
