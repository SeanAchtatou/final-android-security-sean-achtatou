package au.com.xandar.android.os;

import android.util.Log;
import au.com.xandar.android.PlatformConstants;

final class SimpleDialogTask extends SimpleAsyncTask implements DialogTask {
    private boolean completed;
    private final int dialogId;
    private final DialogTaskManager manager;
    private final Runnable runnable;

    public SimpleDialogTask(DialogTaskManager manager2, int dialogId2, Runnable runnable2) {
        this.manager = manager2;
        this.dialogId = dialogId2;
        this.runnable = runnable2;
    }

    public int getDialogId() {
        return this.dialogId;
    }

    public boolean isCompleted() {
        return this.completed;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTask#onPreExecute showing dialogId=" + this.dialogId);
        }
        this.manager.preExecute(this);
    }

    /* access modifiers changed from: protected */
    public void doInBackground() {
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTask#execute-start dialogId=" + this.dialogId);
        }
        this.runnable.run();
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTask#execute-finish dialogId=" + this.dialogId);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute() {
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTask#onPostExecute start dialogId=" + this.dialogId);
        }
        if (this.runnable instanceof ExtendedRunnable) {
            ((ExtendedRunnable) this.runnable).postExecute();
        }
        synchronized (this.manager) {
            this.completed = true;
            this.manager.postExecute(this);
        }
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTask#onPostExecute finish dialogId=" + this.dialogId);
        }
    }
}
