package au.com.xandar.android.facebook.objects;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class JSONSingleObjectDecode {
    public Location getLocation(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        String id = "";
        if (!json.isNull("id")) {
            id = json.getString("id");
        }
        String name = "";
        if (!json.isNull("name")) {
            name = json.getString("name");
        }
        return new Location(id, name);
    }

    public User getUser(JSONObject json) throws JSONException {
        String id = "";
        if (!json.isNull("id")) {
            id = json.get("id").toString();
        }
        String name = "";
        if (!json.isNull("name")) {
            name = json.get("name").toString();
        }
        String first_name = "";
        if (!json.isNull("first_name")) {
            first_name = json.get("first_name").toString();
        }
        String last_name = "";
        if (!json.isNull("last_name")) {
            last_name = json.get("last_name").toString();
        }
        String gender = "";
        if (!json.isNull("gender")) {
            gender = json.get("gender").toString();
        }
        String locale = "";
        if (!json.isNull("locale")) {
            locale = json.get("locale").toString();
        }
        String timezone = "";
        if (!json.isNull("timezone")) {
            timezone = json.get("timezone").toString();
        }
        String birthday = "";
        if (!json.isNull("birthday")) {
            birthday = json.get("birthday").toString();
        }
        String email = "";
        if (!json.isNull("email")) {
            email = json.get("email").toString();
        }
        Location location = new Location();
        if (!json.isNull("location")) {
            location = getLocation(json.getString("location"));
        }
        List<String> interested_in = new ArrayList<>();
        if (!json.isNull("interested_in")) {
            JSONArray jSONArray = new JSONArray(json.getString("interested_in"));
            for (int i = 0; i < jSONArray.length(); i++) {
                interested_in.add(jSONArray.getString(i));
            }
        }
        return new User(id, name, first_name, last_name, gender, locale, timezone, birthday, email, interested_in, location);
    }
}
