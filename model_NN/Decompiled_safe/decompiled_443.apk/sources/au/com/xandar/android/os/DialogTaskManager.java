package au.com.xandar.android.os;

import android.app.Activity;
import android.util.Log;
import au.com.xandar.android.PlatformConstants;
import au.com.xandar.android.app.XandarActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public final class DialogTaskManager {
    private Activity activity;
    private final Executor singleThreadedExecutor;
    private final List<DialogTask> tasks = new ArrayList();

    public DialogTaskManager(Activity activity2, Executor executor) {
        this.activity = activity2;
        this.singleThreadedExecutor = executor;
    }

    public synchronized void setActivity(Activity newActivity) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTaskManager#setActivity currentActivity=" + this.activity + " newActivity=" + newActivity);
        }
        List<DialogTask> tasksToRemove = new ArrayList<>();
        for (DialogTask task : this.tasks) {
            if (task.isCompleted() && newActivity != null) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.v(PlatformConstants.TAG, "DialogTaskManager#setActivity dismissing dialogId=" + task.getDialogId() + " newActivity=" + newActivity);
                }
                ((XandarActivity) newActivity).dismissDialogSafely(task.getDialogId());
                tasksToRemove.add(task);
            }
        }
        this.tasks.removeAll(tasksToRemove);
        this.activity = newActivity;
    }

    public synchronized void clearActivity() {
        this.activity = null;
    }

    public void executeTask(int dialogId, Runnable runnable) {
        new SimpleDialogTask(this, dialogId, runnable).execute(this.singleThreadedExecutor);
    }

    public void executeTask(int dialogId, ExtendedRunnable runnable) {
        new SimpleDialogTask(this, dialogId, runnable).execute(this.singleThreadedExecutor);
    }

    /* access modifiers changed from: package-private */
    public synchronized void preExecute(DialogTask task) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTaskManager#preExecute dialogId=" + task.getDialogId() + " activity=" + this.activity);
        }
        this.tasks.add(task);
        if (this.activity != null) {
            showDialog(this.activity, task.getDialogId());
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void postExecute(DialogTask task) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "DialogTaskManager#postExecute dialogId=" + task.getDialogId() + " activity=" + this.activity);
        }
        if (this.activity != null) {
            ((XandarActivity) this.activity).dismissDialogSafely(task.getDialogId());
            this.tasks.remove(task);
        }
    }

    private void showDialog(Activity dialogActivity, int dialogId) {
        dialogActivity.showDialog(dialogId);
    }
}
