package au.com.xandar.android.facebook;

public class FacebookException extends Exception {
    private static final long serialVersionUID = 1;
    private final int mErrorCode;
    private final String mErrorType;

    public FacebookException(String message) {
        this(message, "", 0);
    }

    public FacebookException(String message, String type, int code) {
        super(message);
        this.mErrorType = type;
        this.mErrorCode = code;
    }

    public int getErrorCode() {
        return this.mErrorCode;
    }

    public String getErrorType() {
        return this.mErrorType;
    }
}
