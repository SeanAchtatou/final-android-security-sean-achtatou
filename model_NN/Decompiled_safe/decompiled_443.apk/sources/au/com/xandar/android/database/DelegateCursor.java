package au.com.xandar.android.database;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

public abstract class DelegateCursor implements Cursor {
    private final Cursor delegate;

    public DelegateCursor(Cursor delegate2) {
        this.delegate = delegate2;
    }

    public int getCount() {
        return this.delegate.getCount();
    }

    public int getPosition() {
        return this.delegate.getPosition();
    }

    public boolean move(int i) {
        return this.delegate.move(i);
    }

    public boolean moveToPosition(int i) {
        return this.delegate.moveToPosition(i);
    }

    public boolean moveToFirst() {
        return this.delegate.moveToFirst();
    }

    public boolean moveToLast() {
        return this.delegate.moveToLast();
    }

    public boolean moveToNext() {
        return this.delegate.moveToNext();
    }

    public boolean moveToPrevious() {
        return this.delegate.moveToPrevious();
    }

    public boolean isFirst() {
        return this.delegate.isFirst();
    }

    public boolean isLast() {
        return this.delegate.isLast();
    }

    public boolean isBeforeFirst() {
        return this.delegate.isBeforeFirst();
    }

    public boolean isAfterLast() {
        return this.delegate.isAfterLast();
    }

    public int getColumnIndex(String s) {
        return this.delegate.getColumnIndex(s);
    }

    public int getColumnIndexOrThrow(String s) throws IllegalArgumentException {
        return this.delegate.getColumnIndexOrThrow(s);
    }

    public String getColumnName(int i) {
        return this.delegate.getColumnName(i);
    }

    public String[] getColumnNames() {
        return this.delegate.getColumnNames();
    }

    public int getColumnCount() {
        return this.delegate.getColumnCount();
    }

    public byte[] getBlob(int i) {
        return this.delegate.getBlob(i);
    }

    public String getString(int i) {
        return this.delegate.getString(i);
    }

    public void copyStringToBuffer(int i, CharArrayBuffer charArrayBuffer) {
        this.delegate.copyStringToBuffer(i, charArrayBuffer);
    }

    public short getShort(int i) {
        return this.delegate.getShort(i);
    }

    public int getInt(int i) {
        return this.delegate.getInt(i);
    }

    public long getLong(int i) {
        return this.delegate.getLong(i);
    }

    public float getFloat(int i) {
        return this.delegate.getFloat(i);
    }

    public double getDouble(int i) {
        return this.delegate.getDouble(i);
    }

    public boolean isNull(int i) {
        return this.delegate.isNull(i);
    }

    public void deactivate() {
        this.delegate.deactivate();
    }

    public boolean requery() {
        return this.delegate.requery();
    }

    public void close() {
        this.delegate.close();
    }

    public boolean isClosed() {
        return this.delegate.isClosed();
    }

    public void registerContentObserver(ContentObserver contentObserver) {
        this.delegate.registerContentObserver(contentObserver);
    }

    public void unregisterContentObserver(ContentObserver contentObserver) {
        this.delegate.unregisterContentObserver(contentObserver);
    }

    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        this.delegate.registerDataSetObserver(dataSetObserver);
    }

    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        this.delegate.unregisterDataSetObserver(dataSetObserver);
    }

    public void setNotificationUri(ContentResolver contentResolver, Uri uri) {
        this.delegate.setNotificationUri(contentResolver, uri);
    }

    public boolean getWantsAllOnMoveCalls() {
        return this.delegate.getWantsAllOnMoveCalls();
    }

    public Bundle getExtras() {
        return this.delegate.getExtras();
    }

    public Bundle respond(Bundle bundle) {
        return this.delegate.respond(bundle);
    }
}
