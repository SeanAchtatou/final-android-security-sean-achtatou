package au.com.xandar.android.os;

import java.util.concurrent.Executor;

interface DialogTask {
    void execute();

    void execute(Executor executor);

    int getDialogId();

    boolean isCompleted();
}
