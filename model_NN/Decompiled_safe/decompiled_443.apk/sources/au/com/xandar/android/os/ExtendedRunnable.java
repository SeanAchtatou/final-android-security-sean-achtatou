package au.com.xandar.android.os;

public interface ExtendedRunnable extends Runnable {
    void postExecute();
}
