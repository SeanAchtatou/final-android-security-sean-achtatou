package au.com.xandar.android.facebook.objects;

import java.util.List;

public class User {
    private final String birthday;
    private final String email;
    private final String first_name;
    private final String gender;
    private final String id;
    private final List<String> interested_in;
    private final String last_name;
    private final String locale;
    private final Location location;
    private final String name;
    private final String timezone;

    public User(String id2, String name2, String first_name2, String last_name2, String gender2, String locale2, String timezone2, String birthday2, String email2, List<String> interested_in2, Location location2) {
        this.id = id2;
        this.name = name2;
        this.first_name = first_name2;
        this.last_name = last_name2;
        this.gender = gender2;
        this.locale = locale2;
        this.timezone = timezone2;
        this.birthday = birthday2;
        this.email = email2;
        this.interested_in = interested_in2;
        this.location = location2;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public String getGender() {
        return this.gender;
    }

    public String getLocale() {
        return this.locale;
    }

    public String getTimezone() {
        return this.timezone;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public String getEmail() {
        return this.email;
    }

    public List<String> getInterested_in() {
        return this.interested_in;
    }

    public Location getLocation() {
        return this.location;
    }

    public String toString() {
        return "User{id='" + this.id + '\'' + ", name='" + this.name + '\'' + ", first_name='" + this.first_name + '\'' + ", last_name='" + this.last_name + '\'' + ", gender='" + this.gender + '\'' + ", locale='" + this.locale + '\'' + ", timezone='" + this.timezone + '\'' + ", birthday='" + this.birthday + '\'' + ", email='" + this.email + '\'' + ", interested_in='" + this.interested_in + "', location=" + this.location + '}';
    }
}
