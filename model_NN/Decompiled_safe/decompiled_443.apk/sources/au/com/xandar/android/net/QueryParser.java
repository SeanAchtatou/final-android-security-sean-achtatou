package au.com.xandar.android.net;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import org.acra.ACRA;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public final class QueryParser {
    public Map<String, String> getQueryMap(String query) {
        Map<String, String> queryParams = new HashMap<>();
        if (query != null) {
            try {
                try {
                    for (NameValuePair param : URLEncodedUtils.parse(new URI("http", "jumblee.xandar.com.au", "", URLDecoder.decode(query, "UTF-8"), ""), "UTF-8")) {
                        if (param.getValue() != null) {
                            queryParams.put(param.getName(), param.getValue());
                        }
                    }
                } catch (URISyntaxException e) {
                    ACRA.getErrorReporter().handleSilentException(e);
                }
            } catch (UnsupportedEncodingException e2) {
                ACRA.getErrorReporter().handleSilentException(e2);
            }
        }
        return queryParams;
    }
}
