package au.com.xandar.android.os;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import au.com.xandar.android.PlatformConstants;
import java.util.concurrent.Executor;

public abstract class SimpleAsyncTask {
    /* access modifiers changed from: private */
    public final Handler handler = new Handler(Looper.getMainLooper());

    /* access modifiers changed from: protected */
    public abstract void doInBackground();

    /* access modifiers changed from: protected */
    public abstract void onPostExecute();

    public final void execute() {
        onPreExecute();
        new Thread(getRunnableToExecute()).start();
    }

    public final void execute(Executor executor) {
        onPreExecute();
        executor.execute(getRunnableToExecute());
    }

    private Runnable getRunnableToExecute() {
        return new Runnable() {
            public void run() {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.v(PlatformConstants.TAG, SimpleAsyncTask.this + "#doInBackground started");
                }
                SimpleAsyncTask.this.doInBackground();
                if (PlatformConstants.DEV_LOGGING) {
                    Log.v(PlatformConstants.TAG, SimpleAsyncTask.this + "#doInBackground finished");
                }
                SimpleAsyncTask.this.handler.post(new Runnable() {
                    public void run() {
                        if (PlatformConstants.DEV_LOGGING) {
                            Log.v(PlatformConstants.TAG, SimpleAsyncTask.this + "#postExecute started");
                        }
                        SimpleAsyncTask.this.onPostExecute();
                        if (PlatformConstants.DEV_LOGGING) {
                            Log.v(PlatformConstants.TAG, SimpleAsyncTask.this + "#postExecute finished");
                        }
                    }
                });
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }
}
