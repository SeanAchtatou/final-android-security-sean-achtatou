package au.com.xandar.android.facebook;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import au.com.xandar.android.PlatformConstants;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public final class Util {
    private static final String OAUTH_EXCEPTION = "OAuthException";

    public static String encodePostBody(Bundle parameters, String boundary) {
        if (parameters == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String key : parameters.keySet()) {
            if (!isKeyValueAByteArray(parameters, key)) {
                sb.append("Content-Disposition: form-data; name=\"");
                sb.append(key);
                sb.append("\"\r\n\r\n");
                sb.append(parameters.getString(key));
                sb.append("\r\n--");
                sb.append(boundary);
                sb.append("\r\n");
            }
        }
        return sb.toString();
    }

    public static String encodeUrl(Bundle parameters) {
        if (parameters == null) {
            return "";
        }
        boolean first = true;
        StringBuilder sb = new StringBuilder();
        for (String key : parameters.keySet()) {
            if (first) {
                first = false;
            } else {
                sb.append("&");
            }
            String value = parameters.getString(key);
            if (PlatformConstants.DEV_LOGGING) {
                Log.v(FacebookConstants.TAG, "encoding key='" + key + "' value='" + value + "'");
            }
            sb.append(URLEncoder.encode(key));
            sb.append("=");
            sb.append(URLEncoder.encode(value));
        }
        return sb.toString();
    }

    public static Bundle decodeUrl(String s) {
        Bundle params = new Bundle();
        if (s != null) {
            for (String parameter : s.split("&")) {
                String[] v = parameter.split("=");
                params.putString(URLDecoder.decode(v[0]), URLDecoder.decode(v[1]));
            }
        }
        return params;
    }

    public static Bundle parseUrl(String url) {
        try {
            URL u = new URL(url.replace("fbconnect", "http"));
            Bundle b = decodeUrl(u.getQuery());
            b.putAll(decodeUrl(u.getRef()));
            return b;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }

    /* JADX INFO: Multiple debug info for r6v3 java.net.HttpURLConnection: [D('url' java.lang.String), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r6v6 java.lang.String: [D('conn' java.net.HttpURLConnection), D('response' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v7 java.lang.String: [D('conn' java.net.HttpURLConnection), D('response' java.lang.String)] */
    public static String openUrl(String url, String method, Bundle params) throws IOException {
        if (method.equals("GET")) {
            url = url + "?" + encodeUrl(params);
        }
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, method + " URL: " + url);
        }
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setRequestProperty("User-Agent", System.getProperties().getProperty("http.agent") + " FacebookAndroidSDK");
        if (!method.equals("GET")) {
            Bundle dataparams = new Bundle();
            for (String key : params.keySet()) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.v(FacebookConstants.TAG, "key='" + key + "' value='" + params.get(key) + "'");
                }
                if (isKeyValueAByteArray(params, key)) {
                    dataparams.putByteArray(key, params.getByteArray(key));
                }
            }
            if (!params.containsKey("method")) {
                params.putString("method", method);
            }
            if (params.containsKey(Facebook.TOKEN)) {
                params.putString(Facebook.TOKEN, URLDecoder.decode(params.getString(Facebook.TOKEN)));
            }
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.connect();
            OutputStream os = new BufferedOutputStream(conn.getOutputStream());
            os.write("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
            os.write(encodePostBody(params, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f").getBytes());
            os.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
            if (!dataparams.isEmpty()) {
                for (String key2 : dataparams.keySet()) {
                    os.write(("Content-Disposition: form-data; filename=\"" + key2 + "\"" + "\r\n").getBytes());
                    os.write("Content-Type: content/unknown\r\n\r\n".getBytes());
                    os.write(dataparams.getByteArray(key2));
                    os.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
                }
            }
            os.flush();
        }
        try {
            return read(conn.getInputStream());
        } catch (FileNotFoundException e) {
            return read(conn.getErrorStream());
        }
    }

    public static boolean isKeyValueAByteArray(Bundle bundle, String key) {
        return bundle.get(key) instanceof byte[];
    }

    private static String read(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }

    public static void clearCookies(Context context) {
        CookieSyncManager createInstance = CookieSyncManager.createInstance(context);
        CookieManager.getInstance().removeAllCookie();
    }

    public static JSONObject parseJson(String response) throws JSONException, FacebookException, FacebookAuthenticationException {
        if (response.equals("false")) {
            throw new FacebookException("request failed");
        }
        if (response.equals("true")) {
            response = "{value : true}";
        }
        JSONObject json = new JSONObject(response);
        if (json.has("error")) {
            JSONObject error = json.getJSONObject("error");
            String errorMessage = error.getString("message");
            String errorType = error.getString("type");
            if (OAUTH_EXCEPTION.equals(errorType)) {
                throw new FacebookAuthenticationException(errorMessage);
            }
            throw new FacebookException(errorMessage, errorType, 0);
        } else if (json.has("error_code") && json.has("error_msg")) {
            throw new FacebookException(json.getString("error_msg"), "", Integer.parseInt(json.getString("error_code")));
        } else if (json.has("error_code")) {
            throw new FacebookException("request failed", "", Integer.parseInt(json.getString("error_code")));
        } else if (json.has("error_msg")) {
            throw new FacebookException(json.getString("error_msg"));
        } else if (!json.has("error_reason")) {
            return json;
        } else {
            throw new FacebookException(json.getString("error_reason"));
        }
    }

    public static void showAlert(Context context, String title, String text) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(text);
        alertBuilder.create().show();
    }
}
