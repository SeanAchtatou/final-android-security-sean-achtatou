package au.com.xandar.android.os;

import android.os.Build;
import android.util.Log;
import au.com.xandar.android.PlatformConstants;
import au.com.xandar.java.lang.ReflectionException;
import au.com.xandar.java.lang.ReflectionHelper;

public final class Compatibility {
    private static CompatibilityHelper helper;

    public static int getAPILevel() {
        try {
            return Build.VERSION.class.getField("SDK_INT").getInt(null);
        } catch (SecurityException e) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (NoSuchFieldException e2) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalArgumentException e3) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalAccessException e4) {
            return Integer.parseInt(Build.VERSION.SDK);
        }
    }

    public static CompatibilityHelper getHelper() {
        if (helper != null) {
            return helper;
        }
        int apiLevel = getAPILevel();
        if (apiLevel >= 9) {
            try {
                helper = (CompatibilityHelper) ReflectionHelper.createClassInstance("au.com.xandar.android.os.CompatibilityHelperApiLevel9");
            } catch (ReflectionException e) {
                Log.e(PlatformConstants.TAG, "Could not create CompatibilityHelper for ApiLevel=" + apiLevel);
                helper = new CompatibilityHelperBasic();
            }
        } else {
            helper = new CompatibilityHelperBasic();
        }
        return helper;
    }
}
