package au.com.xandar.android.time;

import android.util.Log;
import au.com.xandar.android.PlatformConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public final class FlexibleCountDownTimer {
    private final long countDownInterval;
    private TimerTask currentTimerTask;
    /* access modifiers changed from: private */
    public final List<TimerListener> listeners = new ArrayList();
    /* access modifiers changed from: private */
    public long millisRemaining;
    private final Timer timer = new Timer("FlexibleCountDownTimer Thread");

    static /* synthetic */ long access$022(FlexibleCountDownTimer x0, long x1) {
        long j = x0.millisRemaining - x1;
        x0.millisRemaining = j;
        return j;
    }

    private final class MyTimerTask extends TimerTask {
        private long lastTimeExecuted;

        private MyTimerTask() {
        }

        public void run() {
            if (this.lastTimeExecuted != 0) {
                long millisSinceLastTime = scheduledExecutionTime() - this.lastTimeExecuted;
                synchronized (FlexibleCountDownTimer.this) {
                    FlexibleCountDownTimer.access$022(FlexibleCountDownTimer.this, millisSinceLastTime);
                }
            }
            this.lastTimeExecuted = scheduledExecutionTime();
            long nrMillisRemaining = FlexibleCountDownTimer.this.millisRemaining;
            boolean moreTimeRemains = nrMillisRemaining > 0;
            for (int i = 0; i < FlexibleCountDownTimer.this.listeners.size(); i++) {
                TimerListener listener = (TimerListener) FlexibleCountDownTimer.this.listeners.get(i);
                if (moreTimeRemains) {
                    listener.onTick(nrMillisRemaining);
                } else {
                    listener.onFinish();
                }
            }
        }
    }

    public FlexibleCountDownTimer(long countDownInterval2) {
        this.countDownInterval = countDownInterval2;
    }

    public void start() {
        startTimer();
    }

    public void stop() {
        stopTimer();
    }

    public void setMillisRemaining(long millis) {
        boolean wasRunning = stopTimer();
        synchronized (this) {
            this.millisRemaining = millis;
        }
        if (wasRunning) {
            start();
        }
    }

    public void incrementTimeRemaining(long millis) {
        synchronized (this) {
            this.millisRemaining += millis;
        }
    }

    public void addTimerListener(TimerListener listener) {
        this.listeners.add(listener);
    }

    public void removeTimerListeners() {
        this.listeners.clear();
    }

    private void startTimer() {
        if (this.currentTimerTask == null) {
            this.currentTimerTask = new MyTimerTask();
            this.timer.scheduleAtFixedRate(this.currentTimerTask, 0, this.countDownInterval);
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(PlatformConstants.TAG, "FCCT#startTimer - finished millisRemaining=" + this.millisRemaining);
            }
        } else if (PlatformConstants.DEV_LOGGING) {
            Log.d(PlatformConstants.TAG, "FCCT#startTimer - not starting as one already exists");
        }
    }

    private boolean stopTimer() {
        if (this.currentTimerTask == null) {
            return false;
        }
        this.currentTimerTask.cancel();
        this.currentTimerTask = null;
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(PlatformConstants.TAG, "FCCT#stopTimer - finished millisRemaining=" + this.millisRemaining);
        }
        return true;
    }
}
