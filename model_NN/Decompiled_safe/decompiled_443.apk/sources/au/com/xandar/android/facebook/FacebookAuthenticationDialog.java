package au.com.xandar.android.facebook;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslCertificate;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import au.com.xandar.android.PlatformConstants;
import au.com.xandar.jumblee.JumbleeActivity;
import au.com.xandar.jumblee.R;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public class FacebookAuthenticationDialog extends Dialog {
    private static final float[] DIMENSIONS_DIFF_LANDSCAPE = {20.0f, 60.0f};
    private static final float[] DIMENSIONS_DIFF_PORTRAIT = {40.0f, 60.0f};
    private static final String DISPLAY_STRING = "touch";
    private static final int FB_BLUE = -9599820;
    private static final FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(-1, -1);
    private static final int MARGIN = 4;
    private static final int PADDING = 2;
    private final int authenticationProgressDialogId;
    private final int facebookIconResourceId;
    private LinearLayout mContent;
    /* access modifiers changed from: private */
    public final AuthenticationListener mListener;
    /* access modifiers changed from: private */
    public TextView mTitle;
    /* access modifiers changed from: private */
    public WebView mWebView;

    public FacebookAuthenticationDialog(Context context, AuthenticationListener listener, int facebookIconResId, int authenticationProgressDialogId2) {
        super(context);
        this.mListener = listener;
        this.facebookIconResourceId = facebookIconResId;
        this.authenticationProgressDialogId = authenticationProgressDialogId2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContent = new LinearLayout(getContext());
        this.mContent.setOrientation(1);
        setUpTitle();
        setUpWebView();
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "FacebookAuthenticationDialog was cancelled by user");
                }
                FacebookAuthenticationDialog.this.mListener.onCancel();
            }
        });
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        float scale = getContext().getResources().getDisplayMetrics().density;
        float[] dimensions = getContext().getResources().getConfiguration().orientation == 2 ? DIMENSIONS_DIFF_LANDSCAPE : DIMENSIONS_DIFF_PORTRAIT;
        addContentView(this.mContent, new LinearLayout.LayoutParams(display.getWidth() - ((int) ((dimensions[0] * scale) + 0.5f)), display.getHeight() - ((int) ((dimensions[1] * scale) + 0.5f))));
    }

    public void loadURL(String url) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "#loadURL " + url);
        }
        this.mWebView.loadUrl(url);
    }

    private void setUpTitle() {
        requestWindowFeature(1);
        Drawable icon = getContext().getResources().getDrawable(this.facebookIconResourceId);
        this.mTitle = new TextView(getContext());
        this.mTitle.setText("Facebook");
        this.mTitle.setTextColor(-1);
        this.mTitle.setTypeface(Typeface.DEFAULT_BOLD);
        this.mTitle.setBackgroundColor(FB_BLUE);
        this.mTitle.setPadding(6, 4, 4, 4);
        this.mTitle.setCompoundDrawablePadding(6);
        this.mTitle.setCompoundDrawablesWithIntrinsicBounds(icon, (Drawable) null, (Drawable) null, (Drawable) null);
        this.mContent.addView(this.mTitle);
    }

    private void setUpWebView() {
        try {
            SslCertificate certificate = getCertificate();
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "Found SslCertificate : " + certificate);
            }
            this.mWebView = new WebView(getContext());
            this.mWebView.setVerticalScrollBarEnabled(false);
            this.mWebView.setHorizontalScrollBarEnabled(false);
            this.mWebView.setWebViewClient(new FbWebViewClient());
            this.mWebView.setCertificate(certificate);
            this.mWebView.getSettings().setJavaScriptEnabled(true);
            this.mWebView.setLayoutParams(FILL);
            this.mContent.addView(this.mWebView);
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* JADX INFO: finally extract failed */
    private SslCertificate getCertificate() throws GeneralSecurityException, IOException {
        KeyStore keyStore = KeyStore.getInstance("BKS");
        InputStream inputStream = getContext().getResources().openRawResource(R.raw.ssl_keystore);
        try {
            keyStore.load(inputStream, "mystorepass".toCharArray());
            inputStream.close();
            Certificate certificate = keyStore.getCertificate("facebook");
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "Found certificate : " + certificate);
            }
            return new SslCertificate((X509Certificate) certificate);
        } catch (Throwable th) {
            inputStream.close();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void dismissAuthenticationDialog() {
        if (isDialogVisibleAndAcceptingInput()) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.v(PlatformConstants.TAG, "#dismissAuthenticationDialog isVisibleAndAcceptingInput");
            }
            dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void showAuthenticationProgressDialog() {
        if (isDialogVisibleAndAcceptingInput()) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.v(PlatformConstants.TAG, "#showAuthenticationProgressDialog isVisibleAndAcceptingInput");
            }
            getOwnerActivity().showDialog(this.authenticationProgressDialogId);
        }
    }

    /* access modifiers changed from: private */
    public boolean dismissAuthenticationProgressDialog() {
        if (getOwnerActivity().isFinishing()) {
            return false;
        }
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "#dismissAuthenticationProgressDialog isNotFinishing");
        }
        ((JumbleeActivity) getOwnerActivity()).dismissDialogSafely(this.authenticationProgressDialogId);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean isDialogVisibleAndAcceptingInput() {
        if (!isShowing()) {
            return false;
        }
        if (getOwnerActivity().isFinishing()) {
            return false;
        }
        return true;
    }

    private class FbWebViewClient extends WebViewClient {
        private FbWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#shouldOverrideUrlLoading URL: " + url);
            }
            if (url.startsWith(Facebook.REDIRECT_URI)) {
                Bundle values = Util.parseUrl(url);
                String error = values.getString("error");
                if (error == null) {
                    error = values.getString("error_type");
                }
                if (error == null) {
                    FacebookAuthenticationDialog.this.mListener.onComplete(values);
                } else if (error.equals("access_denied") || error.equals("OAuthAccessDeniedException")) {
                    FacebookAuthenticationDialog.this.mListener.onCancel();
                } else {
                    FacebookAuthenticationDialog.this.mListener.onFacebookError(new FacebookException(error));
                }
                FacebookAuthenticationDialog.this.dismissAuthenticationDialog();
                return true;
            } else if (url.startsWith(Facebook.CANCEL_URI)) {
                FacebookAuthenticationDialog.this.mListener.onCancel();
                FacebookAuthenticationDialog.this.dismissAuthenticationDialog();
                return true;
            } else if (url.contains(FacebookAuthenticationDialog.DISPLAY_STRING)) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "#shouldOverrideUrlLoading URL contains DISPLAY_STRING");
                }
                return false;
            } else {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "#shouldOverrideUrlLoading launch in full browser. Url: " + url);
                }
                FacebookAuthenticationDialog.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                return true;
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#onReceivedError URL: " + failingUrl + " errorCode=" + errorCode + " description=" + description);
            }
            super.onReceivedError(view, errorCode, description, failingUrl);
            FacebookAuthenticationDialog.this.mListener.onError(new DialogError(description, errorCode, failingUrl));
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#onReceivedError notified listener of the error");
            }
            boolean unused = FacebookAuthenticationDialog.this.dismissAuthenticationProgressDialog();
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#onReceivedError dismissed AuthProgressDialog");
            }
            FacebookAuthenticationDialog.this.dismissAuthenticationDialog();
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#onReceivedError dismissed AuthenticationDialog");
            }
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#onPageStarted start URL: " + url);
            }
            super.onPageStarted(view, url, favicon);
            FacebookAuthenticationDialog.this.showAuthenticationProgressDialog();
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#onPageStarted finish");
            }
        }

        public void onPageFinished(WebView view, String url) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#onPageFinished start URL: " + url);
            }
            super.onPageFinished(view, url);
            if (FacebookAuthenticationDialog.this.isDialogVisibleAndAcceptingInput()) {
                String title = FacebookAuthenticationDialog.this.mWebView.getTitle();
                if (title != null && title.length() > 0) {
                    FacebookAuthenticationDialog.this.mTitle.setText(title);
                }
                boolean unused = FacebookAuthenticationDialog.this.dismissAuthenticationProgressDialog();
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "#onPageFinished finish");
                }
            }
        }
    }
}
