package au.com.xandar.android.facebook;

import android.util.Log;
import au.com.xandar.android.facebook.AsyncFacebookRunner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public abstract class BaseRequestListener implements AsyncFacebookRunner.RequestListener {
    public void onFacebookError(FacebookException e, Object state) {
        Log.e(FacebookConstants.TAG, "Request failed. State=" + state, e);
    }

    public void onFileNotFoundException(FileNotFoundException e, Object state) {
        Log.e(FacebookConstants.TAG, "Request failed. State=" + state, e);
    }

    public void onIOException(IOException e, Object state) {
        Log.e(FacebookConstants.TAG, "Request failed. State=" + state, e);
    }

    public void onMalformedURLException(MalformedURLException e, Object state) {
        Log.e(FacebookConstants.TAG, "Bad Request URL. State=" + state, e);
    }
}
