package au.com.xandar.android.pm;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.net.Uri;
import android.util.Log;
import au.com.xandar.android.PlatformConstants;
import java.util.List;

public final class PackageManagerUtility {
    private static final String[] AD_BLOCKER_PACKAGES = {"com.bigtincan.android.adfree", "tw.nicky.AdBlockT", "tw.nicky.AdBlock", "com.droidlab.adblocker", "net.xdevelop.adblocker_t", "net.xdevelop.adblocker", "soapbox.sym3try.andguard", "com.dezhi.adterminator", "com.canvs2321.absolute.system", "com.jrummy.roottools", "com.jrummy.liberty.toolboxpro", "com.jrummy.liberty.toolbox", "de.ub0r.android.adBlock"};
    private static final int ALL_INFO = 0;
    private static final Intent BASE_MARKET_INTENT = new Intent("android.intent.action.VIEW", Uri.parse("market://details"));
    private static final String NO_AD_BLOCKERS_INSTALLED = "NONE";
    private final Context context;

    public PackageManagerUtility(Context context2) {
        this.context = context2;
    }

    public PackageInfo getAllPackageInfo() {
        return getPackageInfo(0);
    }

    public Signature getPackageSignature() {
        return getPackageInfo(64).signatures[0];
    }

    public boolean isMarketAvailable() {
        List<ResolveInfo> activities = this.context.getPackageManager().queryIntentActivities(BASE_MARKET_INTENT, 65536);
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "isMarketAvailable Activities=" + activities);
        }
        return !activities.isEmpty();
    }

    public String getAdBlockerPackage() {
        String foundPackage = null;
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "PackageManagerUtility#getAdBlockerPackage - start");
        }
        String[] arr$ = AD_BLOCKER_PACKAGES;
        int len$ = arr$.length;
        int i$ = 0;
        while (true) {
            if (i$ >= len$) {
                break;
            }
            String packageName = arr$[i$];
            if (applicationIsInstalled(packageName)) {
                foundPackage = packageName;
                if (PlatformConstants.DEV_LOGGING) {
                    Log.v(PlatformConstants.TAG, "PackageManagerUtility#getAdBlockerPackage - found AdBlocker : " + foundPackage);
                }
            } else {
                i$++;
            }
        }
        if (PlatformConstants.DEV_LOGGING) {
            Log.v(PlatformConstants.TAG, "PackageManagerUtility#getAdBlockerPackage - finish");
        }
        return foundPackage == null ? NO_AD_BLOCKERS_INSTALLED : foundPackage;
    }

    private PackageInfo getPackageInfo(int flags) {
        String packageName = this.context.getPackageName();
        try {
            return this.context.getPackageManager().getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
            throw new IllegalStateException("Could not find the package for " + packageName + "!!", e);
        }
    }

    private boolean applicationIsInstalled(String packageName) {
        try {
            this.context.getPackageManager().getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
