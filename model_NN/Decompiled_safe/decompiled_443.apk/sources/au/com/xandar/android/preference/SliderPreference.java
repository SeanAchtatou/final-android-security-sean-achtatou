package au.com.xandar.android.preference;

import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import au.com.xandar.jumblee.common.AppConstants;

public final class SliderPreference extends DialogPreference {
    private static final String ANDROID_NAMESPACE = "http://schemas.android.com/apk/res/android";
    private Context mContext;
    private int mDefault;
    private String mDialogMessage;
    private int mMax;
    private SeekBar mSeekBar;
    /* access modifiers changed from: private */
    public String mSuffix;
    /* access modifiers changed from: private */
    public int mValue = 0;
    /* access modifiers changed from: private */
    public TextView mValueText;

    public SliderPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.mDialogMessage = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "dialogMessage");
        this.mSuffix = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "text");
        this.mDefault = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/android", "defaultValue", 0);
        this.mMax = attrs.getAttributeIntValue("http://schemas.android.com/apk/res/android", "max", 100);
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        LinearLayout layout = new LinearLayout(this.mContext);
        layout.setOrientation(1);
        layout.setPadding(6, 6, 6, 6);
        TextView mSplashText = new TextView(this.mContext);
        if (this.mDialogMessage != null) {
            mSplashText.setText(this.mDialogMessage);
        }
        layout.addView(mSplashText);
        this.mValueText = new TextView(this.mContext);
        this.mValueText.setGravity(1);
        this.mValueText.setTextSize(32.0f);
        layout.addView(this.mValueText, new LinearLayout.LayoutParams(-1, -2));
        this.mSeekBar = new SeekBar(this.mContext);
        this.mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seek, int value, boolean fromTouch) {
                String t = String.valueOf(value);
                SliderPreference.this.mValueText.setText(SliderPreference.this.mSuffix == null ? t : t.concat(SliderPreference.this.mSuffix));
                int unused = SliderPreference.this.mValue = value;
                boolean unused2 = SliderPreference.this.callChangeListener(Integer.valueOf(value));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        layout.addView(this.mSeekBar, new LinearLayout.LayoutParams(-1, -2));
        if (shouldPersist()) {
            this.mValue = getPersistedInt(this.mDefault);
        }
        this.mSeekBar.setMax(this.mMax);
        this.mSeekBar.setProgress(this.mValue);
        return layout;
    }

    /* access modifiers changed from: protected */
    public void onBindDialogView(View v) {
        super.onBindDialogView(v);
        this.mSeekBar.setMax(this.mMax);
        this.mSeekBar.setProgress(this.mValue);
    }

    public void onClick(DialogInterface dialog, int which) {
        super.onClick(dialog, which);
        switch (which) {
            case -1:
                if (shouldPersist()) {
                    persistInt(this.mValue);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean restore, Object defaultValue) {
        super.onSetInitialValue(restore, defaultValue);
        if (restore) {
            this.mValue = shouldPersist() ? getPersistedInt(this.mDefault) : 0;
        } else {
            this.mValue = ((Integer) defaultValue).intValue();
        }
    }

    public void setDefaultValue(Object defaultValue) {
        super.setDefaultValue(defaultValue);
        if (defaultValue instanceof Integer) {
            this.mDefault = ((Integer) defaultValue).intValue();
        } else {
            Log.e(AppConstants.TAG, "SliderPreference expected Integer as default : found " + defaultValue.getClass());
        }
    }

    public void setMax(int max) {
        this.mMax = max;
    }

    public int getMax() {
        return this.mMax;
    }

    public void setProgress(int progress) {
        this.mValue = progress;
        if (this.mSeekBar != null) {
            this.mSeekBar.setProgress(progress);
        }
    }

    public int getProgress() {
        return this.mValue;
    }
}
