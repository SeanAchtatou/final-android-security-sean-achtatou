package au.com.xandar.android.facebook;

public final class FacebookAuthenticationException extends Exception {
    public FacebookAuthenticationException(String message) {
        super(message);
    }
}
