package au.com.xandar.android.os;

import android.os.StrictMode;
import android.util.Log;
import au.com.xandar.android.PlatformConstants;

public final class CompatibilityHelperApiLevel9 implements CompatibilityHelper {

    private static final class ThreadPolicy implements StrictModeThreadPolicy {
        private StrictMode.ThreadPolicy policy;

        private ThreadPolicy(StrictMode.ThreadPolicy policy2) {
            this.policy = policy2;
        }

        public void applyPolicy() {
            StrictMode.setThreadPolicy(this.policy);
        }
    }

    public void enableStrictMode() {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(PlatformConstants.TAG, "CompatibilityHelperApiLevel9#enableStrictMode");
        }
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyLog().penaltyDialog().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().penaltyDeath().build());
    }

    public StrictModeThreadPolicy allowThreadDiskWrites() {
        return new ThreadPolicy(StrictMode.allowThreadDiskWrites());
    }

    public void setStrictModeThreadPolicy(StrictModeThreadPolicy policy) {
        if (!(policy instanceof ThreadPolicy)) {
            Log.e(PlatformConstants.TAG, "CompatibilityHelperApiLevel9#setStrictModeThreadPolicy expected instance of CompatibilityHelperApiLevel9#ThreadPolicy but received " + policy.getClass() + " ignoring policy");
        } else {
            ((ThreadPolicy) policy).applyPolicy();
        }
    }
}
