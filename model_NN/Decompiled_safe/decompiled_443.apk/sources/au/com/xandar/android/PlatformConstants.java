package au.com.xandar.android;

import java.lang.reflect.Field;

public class PlatformConstants {
    public static final String ANDROID_XMLNS = "http://schemas.android.com/apk/res/android";
    public static final int CUPCAKE_1_5 = 3;
    public static boolean DEV_LOGGING = false;
    public static final int DONUT_1_6 = 4;
    public static final int ECLAIR_2_0 = 5;
    public static final int ECLAIR_2_0_1 = 6;
    public static final int ECLAIR_2_1 = 7;
    public static final int FROYO_2_2 = 8;
    public static final int GINGERBREAD_2_3 = 9;
    public static final int GINGERBREAD_2_3_3 = 10;
    public static final int HONEYCOMB_3_0 = 11;
    public static final int HONEYCOMB_3_1 = 12;
    public static final String TAG = "xandar.android";

    public static int getPlatformVersion() {
        try {
            Field verField = Class.forName("android.os.Build$VERSION").getField("SDK_INT");
            return verField.getInt(verField);
        } catch (Exception e) {
            return 3;
        }
    }
}
