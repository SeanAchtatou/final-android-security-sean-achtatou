package au.com.xandar.android.facebook;

import android.os.Bundle;

public interface AuthenticationListener {
    void onCancel();

    void onComplete(Bundle bundle);

    void onError(DialogError dialogError);

    void onFacebookError(FacebookException facebookException);
}
