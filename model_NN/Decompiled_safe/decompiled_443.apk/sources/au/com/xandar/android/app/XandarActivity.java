package au.com.xandar.android.app;

public interface XandarActivity {
    void dismissDialogSafely(int i);
}
