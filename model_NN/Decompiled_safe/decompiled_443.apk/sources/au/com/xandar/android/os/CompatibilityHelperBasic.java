package au.com.xandar.android.os;

import android.util.Log;
import au.com.xandar.android.PlatformConstants;

public final class CompatibilityHelperBasic implements CompatibilityHelper {
    private final StrictModeThreadPolicy threadPolicy = new StrictModeThreadPolicy() {
    };

    public void enableStrictMode() {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(PlatformConstants.TAG, "CompatibilityHelperBasic#enableStrictMode - no-op");
        }
    }

    public StrictModeThreadPolicy allowThreadDiskWrites() {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(PlatformConstants.TAG, "CompatibilityHelperBasic#allowThreadDiskWrites - no-op");
        }
        return this.threadPolicy;
    }

    public void setStrictModeThreadPolicy(StrictModeThreadPolicy policy) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(PlatformConstants.TAG, "CompatibilityHelperBasic#setStrictModeThreadPolicy - no-op");
        }
    }
}
