package au.com.xandar.android.facebook;

public final class DialogError extends Throwable {
    private static final long serialVersionUID = 1;
    private final int mErrorCode;
    private final String mFailingUrl;

    public DialogError(String message, int errorCode, String failingUrl) {
        super(message);
        this.mErrorCode = errorCode;
        this.mFailingUrl = failingUrl;
    }

    public int getErrorCode() {
        return this.mErrorCode;
    }

    public String getFailingUrl() {
        return this.mFailingUrl;
    }
}
