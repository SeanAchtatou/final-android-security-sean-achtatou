package au.com.xandar.android.facebook;

import android.os.Bundle;
import au.com.xandar.android.facebook.objects.JSONSingleObjectDecode;
import au.com.xandar.android.facebook.objects.User;
import java.io.IOException;
import org.json.JSONException;

public final class GraphApi {
    private Facebook facebook;

    public GraphApi(Facebook facebook2) {
        this.facebook = facebook2;
    }

    public User getMyAccountInfo() throws JSONException, IOException, FacebookException, FacebookAuthenticationException {
        return getUserCall(null);
    }

    public User getUserInfo(String friendID) throws JSONException, IOException, FacebookException, FacebookAuthenticationException {
        return getUserCall(friendID);
    }

    private User getUserCall(String friendID) throws JSONException, IOException, FacebookException, FacebookAuthenticationException {
        Bundle params = new Bundle();
        params.putString("format", "json");
        params.putString(Facebook.TOKEN, this.facebook.getAccessToken());
        if (friendID == null) {
            friendID = "me";
        }
        return new JSONSingleObjectDecode().getUser(Util.parseJson(Util.openUrl("https://graph.facebook.com/" + friendID, "GET", params)));
    }
}
