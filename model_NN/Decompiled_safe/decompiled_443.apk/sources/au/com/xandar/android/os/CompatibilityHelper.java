package au.com.xandar.android.os;

public interface CompatibilityHelper {
    StrictModeThreadPolicy allowThreadDiskWrites();

    void enableStrictMode();

    void setStrictModeThreadPolicy(StrictModeThreadPolicy strictModeThreadPolicy);
}
