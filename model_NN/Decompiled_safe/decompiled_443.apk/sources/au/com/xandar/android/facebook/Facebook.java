package au.com.xandar.android.facebook;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieSyncManager;
import au.com.xandar.android.PlatformConstants;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;

public final class Facebook {
    public static final String CANCEL_URI = "fbconnect://cancel";
    private static final String DIALOG_BASE_URL = "https://m.facebook.com/dialog/";
    public static final String EXPIRES = "expires_in";
    public static final String FB_APP_SIGNATURE = "30820268308201d102044a9c4610300d06092a864886f70d0101040500307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e3020170d3039303833313231353231365a180f32303530303932353231353231365a307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e30819f300d06092a864886f70d010101050003818d0030818902818100c207d51df8eb8c97d93ba0c8c1002c928fab00dc1b42fca5e66e99cc3023ed2d214d822bc59e8e35ddcf5f44c7ae8ade50d7e0c434f500e6c131f4a2834f987fc46406115de2018ebbb0d5a3c261bd97581ccfef76afc7135a6d59e8855ecd7eacc8f8737e794c60a761c536b72b11fac8e603f5da1a2d54aa103b8a13c0dbc10203010001300d06092a864886f70d0101040500038181005ee9be8bcbb250648d3b741290a82a1c9dc2e76a0af2f2228f1d9f9c4007529c446a70175c5a900d5141812866db46be6559e2141616483998211f4a673149fb2232a10d247663b26a9031e15f84bc1c74d141ff98a02d76f85b2c8ab2571b6469b232d8e768a7f7ca04f7abe4a775615916c07940656b58717457b42bd928a2";
    private static final String GRAPH_BASE_URL = "https://graph.facebook.com/";
    private static final String LOGIN = "oauth";
    public static final String REDIRECT_URI = "fbconnect://success";
    private static final String RESTSERVER_URL = "https://api.facebook.com/restserver.php";
    public static final String SINGLE_SIGN_ON_DISABLED = "service_disabled";
    public static final String TOKEN = "access_token";
    private final int authenticationActivityCode;
    private final int authenticationDialogId;
    private final String[] authenticationPermissions;
    private final int authenticationProgressDialogId;
    /* access modifiers changed from: private */
    public AuthenticationListener externalAuthenticationListener;
    private final int facebookIconResourceId;
    private long mAccessExpires = 0;
    private String mAccessToken = null;
    private final String mAppId;

    public Facebook(String appId, int facebookIconResId, int authenticationDialogId2, int authenticationProgressDialogId2, String[] authenticationPermissions2, int activityCode) {
        if (appId == null) {
            throw new IllegalArgumentException("You must specify your application ID when instantiating a Facebook object. See README for details.");
        }
        this.mAppId = appId;
        this.facebookIconResourceId = facebookIconResId;
        this.authenticationDialogId = authenticationDialogId2;
        this.authenticationProgressDialogId = authenticationProgressDialogId2;
        this.authenticationPermissions = authenticationPermissions2;
        this.authenticationActivityCode = activityCode;
    }

    public void setAuthenticationListener(AuthenticationListener listener) {
        this.externalAuthenticationListener = listener;
    }

    public void authorize(Activity activity) {
        if (this.externalAuthenticationListener == null) {
            throw new IllegalStateException("Cannot authorize without first setting an AuthenticationListener");
        } else if (this.authenticationPermissions == null) {
            throw new IllegalStateException("Cannot authorize without first setting the AuthenticationPermissions");
        } else {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#authorize start. Permissions=" + Arrays.asList(this.authenticationPermissions) + " activityCode=" + this.authenticationActivityCode);
            }
            boolean singleSignOnStarted = false;
            if (this.authenticationActivityCode >= 0) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "#authorize attempting single sign-on using Facebook app");
                }
                singleSignOnStarted = startSingleSignOn(activity, this.mAppId);
            }
            if (!singleSignOnStarted) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "#authorize attempt authentication using dialog");
                }
                activity.showDialog(this.authenticationDialogId);
            }
        }
    }

    private boolean startSingleSignOn(Activity activity, String applicationId) {
        boolean didSucceed = true;
        Intent intent = new Intent();
        intent.setClassName("com.facebook.katana", "com.facebook.katana.ProxyAuth");
        intent.putExtra("client_id", applicationId);
        if (this.authenticationPermissions.length > 0) {
            intent.putExtra("scope", TextUtils.join(",", this.authenticationPermissions));
        }
        boolean validateSignatureForIntent = validateAppSignatureForIntent(activity, intent);
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "#startingSingleSignOn validateSignature=" + validateSignatureForIntent);
        }
        if (!validateSignatureForIntent) {
            return false;
        }
        try {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#startingSingleSignOn start SingleSignOn Activity. Intent==" + intent);
            }
            activity.startActivityForResult(intent, this.authenticationActivityCode);
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#startingSingleSignOn finished SingleSignOn Activity.");
            }
        } catch (ActivityNotFoundException e) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#startingSingleSignOn - no Facebook app");
            }
            didSucceed = false;
        }
        return didSucceed;
    }

    private boolean validateAppSignatureForIntent(Activity activity, Intent intent) {
        ResolveInfo resolveInfo = activity.getPackageManager().resolveActivity(intent, 0);
        if (resolveInfo == null) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#validateAppSignatureForIntent NoInfo for Intent=" + intent);
            }
            return false;
        }
        String packageName = resolveInfo.activityInfo.packageName;
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "#validateAppSignatureForIntent packageName=" + packageName);
        }
        try {
            PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(packageName, 64);
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "#validateAppSignatureForIntent packageInfo=" + packageInfo);
            }
            for (Signature signature : packageInfo.signatures) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "#validateAppSignatureForIntent sig=" + signature.toCharsString());
                }
                if (signature.toCharsString().equals(FB_APP_SIGNATURE)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public Dialog createAuthenticationDialog(Activity activity) {
        if (activity.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            throw new IllegalStateException("Application requires permission to access the Internet");
        }
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "#authorizeUsingDialog create CookieSyncManager");
        }
        CookieSyncManager.createInstance(activity);
        return new FacebookAuthenticationDialog(activity, new AuthenticationListener() {
            public void onComplete(Bundle values) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "FacebookDialogListener#onComplete bundle=" + values);
                }
                CookieSyncManager.getInstance().sync();
                Facebook.this.setAccessToken(values.getString(Facebook.TOKEN));
                Facebook.this.setAccessExpiresIn(values.getString(Facebook.EXPIRES));
                if (Facebook.this.isSessionValid()) {
                    if (PlatformConstants.DEV_LOGGING) {
                        Log.d(FacebookConstants.TAG, "Login Success! access_token=" + Facebook.this.getAccessToken() + " expires=" + DateFormat.getDateTimeInstance().format(new Date(Facebook.this.getAccessExpires())));
                    }
                    Facebook.this.externalAuthenticationListener.onComplete(values);
                    return;
                }
                Facebook.this.externalAuthenticationListener.onFacebookError(new FacebookException("Failed to receive access token."));
            }

            public void onError(DialogError error) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "Login failed: " + error);
                }
                Facebook.this.externalAuthenticationListener.onError(error);
            }

            public void onFacebookError(FacebookException error) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "Login failed: " + error);
                }
                Facebook.this.externalAuthenticationListener.onFacebookError(error);
            }

            public void onCancel() {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "Login canceled");
                }
                Facebook.this.externalAuthenticationListener.onCancel();
            }
        }, this.facebookIconResourceId, this.authenticationProgressDialogId);
    }

    public void prepareAuthenticationDialog(Dialog dialog) {
        if (!(dialog instanceof FacebookAuthenticationDialog)) {
            throw new IllegalArgumentException("Can only prepare FacebookAuthenticationDialogs");
        }
        Bundle parameters = new Bundle();
        if (this.authenticationPermissions.length > 0) {
            parameters.putString("scope", TextUtils.join(",", this.authenticationPermissions));
        }
        parameters.putString("display", "touch");
        parameters.putString("redirect_uri", REDIRECT_URI);
        parameters.putString("type", "user_agent");
        parameters.putString("client_id", this.mAppId);
        if (isSessionValid()) {
            parameters.putString(TOKEN, getAccessToken());
        }
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "#dialog show auth Dialog. Params=" + parameters);
        }
        ((FacebookAuthenticationDialog) dialog).loadURL("https://m.facebook.com/dialog/oauth?" + Util.encodeUrl(parameters));
    }

    public void authorizeCallback(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode != this.authenticationActivityCode) {
            return;
        }
        if (resultCode == -1) {
            String error = data.getStringExtra("error");
            if (error == null) {
                error = data.getStringExtra("error_type");
            }
            if (error == null) {
                setAccessToken(data.getStringExtra(TOKEN));
                setAccessExpiresIn(data.getStringExtra(EXPIRES));
                if (isSessionValid()) {
                    if (PlatformConstants.DEV_LOGGING) {
                        Log.d(FacebookConstants.TAG, "Login Success! access_token=" + getAccessToken() + " expires=" + DateFormat.getDateTimeInstance().format(new Date(getAccessExpires())));
                    }
                    this.externalAuthenticationListener.onComplete(data.getExtras());
                    return;
                }
                this.externalAuthenticationListener.onFacebookError(new FacebookException("Failed to receive access token."));
            } else if (error.equals(SINGLE_SIGN_ON_DISABLED) || error.equals("AndroidAuthKillSwitchException")) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "Hosted auth currently disabled. Retrying dialog auth...");
                }
                activity.showDialog(this.authenticationDialogId);
            } else if (error.equals("access_denied") || error.equals("OAuthAccessDeniedException")) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "Login canceled by user.");
                }
                this.externalAuthenticationListener.onCancel();
            } else {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "Login failed: " + error);
                }
                this.externalAuthenticationListener.onFacebookError(new FacebookException(error));
            }
        } else if (resultCode != 0) {
        } else {
            if (data != null) {
                if (PlatformConstants.DEV_LOGGING) {
                    Log.d(FacebookConstants.TAG, "Login failed: " + data.getStringExtra("error"));
                }
                this.externalAuthenticationListener.onError(new DialogError(data.getStringExtra("error"), data.getIntExtra("error_code", -1), data.getStringExtra("failing_url")));
                return;
            }
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "Login canceled by user.");
            }
            this.externalAuthenticationListener.onCancel();
        }
    }

    public String logout(Context context) throws IOException {
        Util.clearCookies(context);
        Bundle b = new Bundle();
        b.putString("method", "auth.expireSession");
        String response = request(b);
        clearSession();
        return response;
    }

    public String request(Bundle parameters) throws IOException {
        if (parameters.containsKey("method")) {
            return request(null, parameters, "GET");
        }
        throw new IllegalArgumentException("API method must be specified. (parameters must contain key \"method\" and value). See http://developers.facebook.com/docs/reference/rest/");
    }

    public String request(String graphPath, Bundle params, String httpMethod) throws IOException {
        params.putString("format", "json");
        if (isSessionValid()) {
            params.putString(TOKEN, getAccessToken());
        }
        return Util.openUrl(graphPath != null ? GRAPH_BASE_URL + graphPath : RESTSERVER_URL, httpMethod, params);
    }

    public boolean isSessionValid() {
        if (getAccessToken() == null) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "invalidSession - no AccessToken");
            }
            return false;
        } else if (getAccessExpires() == 0) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "validSession - never expires");
            }
            return true;
        } else if (System.currentTimeMillis() < getAccessExpires()) {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "validSession - doesn't expire until " + DateFormat.getDateTimeInstance().format(new Date(getAccessExpires())));
            }
            return true;
        } else {
            if (PlatformConstants.DEV_LOGGING) {
                Log.d(FacebookConstants.TAG, "invalidSession - token expired at " + DateFormat.getDateTimeInstance().format(new Date(getAccessExpires())));
            }
            return false;
        }
    }

    public void clearSession() {
        this.mAccessToken = null;
        this.mAccessExpires = 0;
    }

    public String getAccessToken() {
        return this.mAccessToken;
    }

    public long getAccessExpires() {
        return this.mAccessExpires;
    }

    public void setAccessToken(String token) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "Setting AccessToken=" + token);
        }
        this.mAccessToken = token;
    }

    public void setAccessExpires(long time) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "Setting AccessExpires=" + DateFormat.getDateTimeInstance().format(new Date(time)));
        }
        this.mAccessExpires = time;
    }

    public void setAccessExpiresIn(String expiresIn) {
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(FacebookConstants.TAG, "Setting AccessExpires in " + expiresIn + " seconds");
        }
        if (expiresIn != null) {
            if (expiresIn.equals("0")) {
                setAccessExpires(0);
                return;
            }
            setAccessExpires(System.currentTimeMillis() + ((long) (Integer.parseInt(expiresIn) * 1000)));
        }
    }
}
