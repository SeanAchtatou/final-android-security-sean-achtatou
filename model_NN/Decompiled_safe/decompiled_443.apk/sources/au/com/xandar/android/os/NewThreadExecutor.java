package au.com.xandar.android.os;

import java.util.concurrent.Executor;

public final class NewThreadExecutor implements Executor {
    public void execute(Runnable runnable) {
        new Thread(runnable).start();
    }
}
