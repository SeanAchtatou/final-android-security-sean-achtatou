package au.com.xandar.android.os;

import java.util.concurrent.Executor;

public final class CurrentThreadExecutor implements Executor {
    public void execute(Runnable runnable) {
        runnable.run();
    }
}
