package au.com.xandar.android.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import au.com.xandar.android.PlatformConstants;

public final class ConnectionChecker {
    private final Context context;

    public ConnectionChecker(Context context2) {
        this.context = context2;
    }

    public boolean hasConnection() {
        NetworkInfo activeNetwork = ((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (PlatformConstants.DEV_LOGGING) {
            Log.d(PlatformConstants.TAG, "ConnectionChecker activeNetwork=" + activeNetwork + " state=" + (activeNetwork == null ? "" : activeNetwork.getState()));
        }
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
