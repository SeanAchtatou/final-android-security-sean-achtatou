package au.com.xandar.android.facebook.objects;

public class Location {
    private String id;
    private String name;

    public Location() {
    }

    public Location(String id2, String name2) {
        this.id = id2;
        this.name = name2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }
}
