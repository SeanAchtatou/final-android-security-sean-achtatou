package au.com.xandar.java.collection;

public interface CacheListener<K, V> {
    void added(K k, V v);
}
