package au.com.xandar.java.lang;

public final class ReflectionHelper {
    public static Object createClassInstance(String name) throws ReflectionException {
        try {
            return Class.forName(name).newInstance();
        } catch (InstantiationException e) {
            throw new ReflectionException("Could not create instance of " + name, e);
        } catch (IllegalAccessException e2) {
            throw new ReflectionException("Could not create instance of " + name, e2);
        } catch (ClassNotFoundException e3) {
            throw new ReflectionException("Could not create instance of " + name, e3);
        }
    }
}
