package au.com.xandar.java.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public final class ByteArrayReader {
    private static final int BLOCK_SIZE = 4096;

    /* JADX INFO: finally extract failed */
    public byte[] readBytesFromUrl(String url) throws IOException {
        if (url == null) {
            return new byte[0];
        }
        InputStream inputStream = (InputStream) new URL(url).getContent();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream(BLOCK_SIZE);
            byte[] buff = new byte[BLOCK_SIZE];
            while (true) {
                int len = inputStream.read(buff, 0, BLOCK_SIZE);
                if (len < 0) {
                    byte[] result = out.toByteArray();
                    inputStream.close();
                    return result;
                }
                out.write(buff, 0, len);
            }
        } catch (Throwable th) {
            inputStream.close();
            throw th;
        }
    }
}
