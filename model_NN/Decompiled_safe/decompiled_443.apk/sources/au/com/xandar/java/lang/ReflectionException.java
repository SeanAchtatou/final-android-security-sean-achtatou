package au.com.xandar.java.lang;

public class ReflectionException extends Exception {
    public ReflectionException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
