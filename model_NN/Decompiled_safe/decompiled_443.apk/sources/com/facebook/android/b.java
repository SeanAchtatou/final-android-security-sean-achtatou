package com.facebook.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieSyncManager;
import au.com.xandar.android.facebook.Facebook;

public class b {
    protected static String a = "https://www.facebook.com/dialog/oauth";
    protected static String b = "https://www.facebook.com/connect/uiserver.php";
    protected static String c = "https://graph.facebook.com/";
    protected static String d = "https://api.facebook.com/restserver.php";
    private String e = null;
    private long f = 0;
    private String g;
    private Activity h;
    private String[] i;
    private int j;
    /* access modifiers changed from: private */
    public a k;

    public interface a {
        void a();

        void a(Bundle bundle);

        void a(a aVar);

        void a(c cVar);
    }

    public b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("You must specify your application ID when instantiating a Facebook object. See README for details.");
        }
        this.g = str;
    }

    private void a(Activity activity, String str, String[] strArr) {
        Bundle bundle = new Bundle();
        bundle.putString("client_id", str);
        if (strArr.length > 0) {
            bundle.putString("scope", TextUtils.join(",", strArr));
        }
        CookieSyncManager.createInstance(activity);
        a(activity, "login", bundle, new a() {
            public void a() {
                Log.d("Facebook-authorize", "Login canceled");
                b.this.k.a();
            }

            public void a(Bundle bundle) {
                CookieSyncManager.getInstance().sync();
                b.this.a(bundle.getString(Facebook.TOKEN));
                b.this.b(bundle.getString(Facebook.EXPIRES));
                if (b.this.a()) {
                    Log.d("Facebook-authorize", "Login Success! access_token=" + b.this.b() + " expires=" + b.this.c());
                    b.this.k.a(bundle);
                    return;
                }
                b.this.k.a(new c("Failed to receive access token."));
            }

            public void a(a aVar) {
                Log.d("Facebook-authorize", "Login failed: " + aVar);
                b.this.k.a(aVar);
            }

            public void a(c cVar) {
                Log.d("Facebook-authorize", "Login failed: " + cVar);
                b.this.k.a(cVar);
            }
        });
    }

    private boolean a(Activity activity, Intent intent) {
        ResolveInfo resolveActivity = activity.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return false;
        }
        try {
            for (Signature charsString : activity.getPackageManager().getPackageInfo(resolveActivity.activityInfo.packageName, 64).signatures) {
                if (charsString.toCharsString().equals(Facebook.FB_APP_SIGNATURE)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    private boolean a(Activity activity, String str, String[] strArr, int i2) {
        Intent intent = new Intent();
        intent.setClassName("com.facebook.katana", "com.facebook.katana.ProxyAuth");
        intent.putExtra("client_id", str);
        if (strArr.length > 0) {
            intent.putExtra("scope", TextUtils.join(",", strArr));
        }
        if (!a(activity, intent)) {
            return false;
        }
        this.h = activity;
        this.i = strArr;
        this.j = i2;
        try {
            activity.startActivityForResult(intent, i2);
            return true;
        } catch (ActivityNotFoundException e2) {
            return false;
        }
    }

    public void a(long j2) {
        this.f = j2;
    }

    public void a(Activity activity, String[] strArr, int i2, a aVar) {
        boolean z = false;
        this.k = aVar;
        if (i2 >= 0) {
            z = a(activity, this.g, strArr, i2);
        }
        if (!z) {
            a(activity, this.g, strArr);
        }
    }

    public void a(Context context, String str, Bundle bundle, final a aVar) {
        String str2;
        if (str.equals("login")) {
            str2 = a;
            bundle.putString("type", "user_agent");
            bundle.putString("redirect_uri", Facebook.REDIRECT_URI);
        } else {
            str2 = b;
            bundle.putString("method", str);
            bundle.putString("next", Facebook.REDIRECT_URI);
        }
        bundle.putString("display", "touch");
        if (a()) {
            bundle.putString(Facebook.TOKEN, b());
        }
        String str3 = str2 + "?" + e.a(bundle);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            e.a(context, "Error", "Application requires permission to access the Internet");
            return;
        }
        d dVar = new d(context, str3, aVar);
        dVar.setCancelable(true);
        dVar.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                aVar.a();
            }
        });
        dVar.show();
    }

    public void a(String str) {
        this.e = str;
    }

    public boolean a() {
        return b() != null && (c() == 0 || System.currentTimeMillis() < c());
    }

    public String b() {
        return this.e;
    }

    public void b(String str) {
        if (str != null && !str.equals("0")) {
            a(System.currentTimeMillis() + ((long) (Integer.parseInt(str) * 1000)));
        }
    }

    public long c() {
        return this.f;
    }
}
