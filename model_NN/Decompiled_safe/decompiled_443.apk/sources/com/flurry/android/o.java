package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import com.google.ads.AdActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

final class o implements View.OnClickListener {
    static String a = "FlurryAgent";
    static String b = "";
    private static volatile String c = "market://";
    private static volatile String d = "market://details?id=";
    private static volatile String e = "https://market.android.com/details?id=";
    private static String f = "com.flurry.android.ACTION_CATALOG";
    private static int g = 5000;
    private static volatile long z = 0;
    private String h;
    private String i;
    private String j;
    private long k;
    private long l;
    private long m;
    private v n = new v();
    private boolean o = true;
    private volatile boolean p;
    private String q;
    private Map r = new HashMap();
    private Handler s;
    private boolean t;
    private transient Map u = new HashMap();
    private ae v;
    private List w = new ArrayList();
    private Map x = new HashMap();
    /* access modifiers changed from: private */
    public AppCircleCallback y;

    static /* synthetic */ void a(o oVar, Context context, String str) {
        if (str.startsWith(d)) {
            String substring = str.substring(d.length());
            if (oVar.o) {
                try {
                    ag.a(a, "Launching Android Market for app " + substring);
                    context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(str)));
                } catch (Exception e2) {
                    ag.c(a, "Cannot launch Marketplace url " + str, e2);
                }
            } else {
                ag.a(a, "Launching Android Market website for app " + substring);
                context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(e + substring)));
            }
        } else {
            ag.d(a, "Unexpected android market url scheme: " + str);
        }
    }

    static {
        new Random(System.currentTimeMillis());
    }

    public final synchronized void a(Context context, a aVar) {
        if (!this.p) {
            this.h = aVar.e;
            this.i = aVar.f;
            this.j = aVar.a;
            this.k = aVar.b;
            this.l = aVar.c;
            this.m = aVar.d;
            this.s = aVar.g;
            this.v = new ae(this.s, g);
            context.getResources().getDisplayMetrics();
            this.u.clear();
            this.x.clear();
            this.n.a(context, this, aVar);
            this.r.clear();
            PackageManager packageManager = context.getPackageManager();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(d + context.getPackageName()));
            this.o = packageManager.queryIntentActivities(intent, 65536).size() > 0;
            this.p = true;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.q = str;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b() {
        if (p()) {
            this.n.d();
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void c() {
        if (p()) {
            this.n.e();
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        if (p()) {
            this.n.a(map, map2, map3, map4, map5, map6);
            Log.i("FlurryAgent", this.n.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized long d() {
        long c2;
        if (!p()) {
            c2 = 0;
        } else {
            c2 = this.n.c();
        }
        return c2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set e() {
        Set a2;
        if (!p()) {
            a2 = Collections.emptySet();
        } else {
            a2 = this.n.a();
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized AdImage a(long j2) {
        AdImage b2;
        if (!p()) {
            b2 = null;
        } else {
            b2 = this.n.b(j2);
        }
        return b2;
    }

    private synchronized AdImage n() {
        AdImage a2;
        if (!p()) {
            a2 = null;
        } else {
            a2 = this.n.a((short) 1);
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized List f() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public final synchronized af b(long j2) {
        return (af) this.u.get(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void g() {
        this.u.clear();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, String str) {
        if (p()) {
            try {
                List a2 = a(Arrays.asList(str), (Long) null);
                if (a2 == null || a2.isEmpty()) {
                    Intent intent = new Intent(o());
                    intent.addCategory("android.intent.category.DEFAULT");
                    context.startActivity(intent);
                } else {
                    af afVar = new af(str, (byte) 2, SystemClock.elapsedRealtime() - this.m);
                    afVar.b = (q) a2.get(0);
                    a(afVar);
                    b(context, afVar, this.h + a(afVar, Long.valueOf(System.currentTimeMillis())));
                }
            } catch (Exception e2) {
                ag.d(a, "Failed to launch promotional canvas for hook: " + str, e2);
            }
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public final void a(AppCircleCallback appCircleCallback) {
        this.y = appCircleCallback;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z2) {
        this.t = z2;
    }

    /* access modifiers changed from: package-private */
    public final boolean h() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public final String i() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, af afVar, String str) {
        if (p()) {
            this.s.post(new ai(this, str, context, afVar));
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        com.flurry.android.ag.c(com.flurry.android.o.a, "Unknown host: " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
        if (r7.y != null) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0073, code lost:
        e("Unknown host: " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00ab, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ac, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052 A[ExcHandler: UnknownHostException (r0v3 'e' java.net.UnknownHostException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String d(java.lang.String r8) {
        /*
            r7 = this;
            r5 = 0
            java.lang.String r0 = com.flurry.android.o.c     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            boolean r0 = r8.startsWith(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            if (r0 != 0) goto L_0x0050
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r0.<init>(r8)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r1.<init>()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.HttpResponse r0 = r1.execute(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            int r1 = r1.getStatusCode()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0038
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r0 = org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r1 = com.flurry.android.o.c     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
            boolean r1 = r0.startsWith(r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
            if (r1 != 0) goto L_0x0037
            java.lang.String r0 = r7.d(r0)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x00ab }
        L_0x0037:
            return r0
        L_0x0038:
            java.lang.String r0 = com.flurry.android.o.a     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            r2.<init>()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r3 = "Cannot process with responseCode "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            java.lang.String r1 = r1.toString()     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
            com.flurry.android.ag.c(r0, r1)     // Catch:{ UnknownHostException -> 0x0052, Exception -> 0x008f }
        L_0x0050:
            r0 = r8
            goto L_0x0037
        L_0x0052:
            r0 = move-exception
            java.lang.String r1 = com.flurry.android.o.a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unknown host: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.flurry.android.ag.c(r1, r2)
            com.flurry.android.AppCircleCallback r1 = r7.y
            if (r1 == 0) goto L_0x008d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown host: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r7.e(r0)
        L_0x008d:
            r0 = r5
            goto L_0x0037
        L_0x008f:
            r0 = move-exception
            r1 = r8
        L_0x0091:
            java.lang.String r2 = com.flurry.android.o.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed on url: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            com.flurry.android.ag.c(r2, r1, r0)
            r0 = r5
            goto L_0x0037
        L_0x00ab:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.o.d(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        a(new ab(this, str));
    }

    /* access modifiers changed from: package-private */
    public final synchronized Offer b(String str) {
        Offer offer;
        if (!p()) {
            offer = null;
        } else {
            List a2 = a(Arrays.asList(str), (Long) null);
            if (a2 == null || a2.isEmpty()) {
                offer = null;
            } else {
                offer = a(str, (q) a2.get(0));
                ag.a(a, "Impression for offer with ID " + offer.getId());
            }
        }
        return offer;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, long j2) {
        if (p()) {
            OfferInSdk offerInSdk = (OfferInSdk) this.x.get(Long.valueOf(j2));
            if (offerInSdk == null) {
                ag.b(a, "Cannot find offer " + j2);
            } else {
                af afVar = offerInSdk.b;
                afVar.a(new h((byte) 4, j()));
                ag.a(a, "Offer " + offerInSdk.a + " accepted. Sent with cookies: " + this.r);
                a(context, afVar, FlurryAgent.c() + a(afVar, Long.valueOf(afVar.a())));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized List c(String str) {
        List list;
        if (!p()) {
            list = Collections.emptyList();
        } else if (!this.n.b()) {
            list = Collections.emptyList();
        } else {
            q[] a2 = this.n.a(str);
            ArrayList arrayList = new ArrayList();
            if (a2 != null && a2.length > 0) {
                for (q a3 : a2) {
                    arrayList.add(a(str, a3));
                }
            }
            ag.a(a, "Impressions for " + arrayList.size() + " offers.");
            list = arrayList;
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(List list) {
        if (p()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.x.remove((Long) it.next());
            }
        }
    }

    private Offer a(String str, q qVar) {
        af afVar = new af(str, (byte) 3, j());
        a(afVar);
        afVar.a(new h((byte) 2, j()));
        afVar.b = qVar;
        aj a2 = this.n.a(qVar.a);
        String str2 = a2 == null ? "" : a2.a;
        int i2 = a2 == null ? 0 : a2.c;
        long j2 = z + 1;
        z = j2;
        OfferInSdk offerInSdk = new OfferInSdk(j2, afVar, qVar.h, qVar.d, str2, i2);
        this.x.put(Long.valueOf(offerInSdk.a), offerInSdk);
        return new Offer(offerInSdk.a, offerInSdk.f, offerInSdk.c, offerInSdk.d, offerInSdk.e);
    }

    /* access modifiers changed from: package-private */
    public final synchronized List a(Context context, List list, Long l2, int i2, boolean z2) {
        List emptyList;
        if (!p()) {
            emptyList = Collections.emptyList();
        } else if (!this.n.b() || list == null) {
            emptyList = Collections.emptyList();
        } else {
            List a2 = a(list, l2);
            int min = Math.min(list.size(), a2.size());
            ArrayList arrayList = new ArrayList();
            for (int i3 = 0; i3 < min; i3++) {
                String str = (String) list.get(i3);
                e b2 = this.n.b(str);
                if (b2 != null) {
                    af afVar = new af((String) list.get(i3), (byte) 1, j());
                    a(afVar);
                    if (i3 < a2.size()) {
                        afVar.b = (q) a2.get(i3);
                        afVar.a(new h((byte) 2, j()));
                        u uVar = new u(context, this, afVar, b2, i2, z2);
                        uVar.a(a(afVar, (Long) null));
                        arrayList.add(uVar);
                    }
                } else {
                    ag.d(a, "Cannot find hook: " + str);
                }
            }
            emptyList = arrayList;
        }
        return emptyList;
    }

    /* access modifiers changed from: package-private */
    public final synchronized View a(Context context, String str, int i2) {
        ac acVar;
        if (!p()) {
            acVar = null;
        } else {
            acVar = new ac(this, context, str, i2);
            this.v.a(acVar);
        }
        return acVar;
    }

    private void a(af afVar) {
        if (this.w.size() < 32767) {
            this.w.add(afVar);
            this.u.put(Long.valueOf(afVar.a()), afVar);
        }
    }

    private List a(List list, Long l2) {
        if (list == null || list.isEmpty() || !this.n.b()) {
            return Collections.emptyList();
        }
        q[] a2 = this.n.a((String) list.get(0));
        if (a2 == null || a2.length <= 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(Arrays.asList(a2));
        Collections.shuffle(arrayList);
        if (l2 != null) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((q) it.next()).a == l2.longValue()) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return arrayList.subList(0, Math.min(arrayList.size(), list.size()));
    }

    /* access modifiers changed from: package-private */
    public final long j() {
        return SystemClock.elapsedRealtime() - this.m;
    }

    public final synchronized void onClick(View view) {
        u uVar = (u) view;
        af a2 = uVar.a();
        a2.a(new h((byte) 4, j()));
        if (this.t) {
            b(view.getContext(), a2, uVar.b(this.h));
        } else {
            a(view.getContext(), a2, uVar.b(this.i));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.r.put(str, str2);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void k() {
        this.r.clear();
    }

    private void b(Context context, af afVar, String str) {
        Intent intent = new Intent(o());
        intent.addCategory("android.intent.category.DEFAULT");
        intent.putExtra(AdActivity.URL_PARAM, str);
        if (afVar != null) {
            intent.putExtra(AdActivity.ORIENTATION_PARAM, afVar.a());
        }
        context.startActivity(intent);
    }

    private static String o() {
        return FlurryAgent.a != null ? FlurryAgent.a : f;
    }

    private String a(af afVar, Long l2) {
        q qVar = afVar.b;
        StringBuilder append = new StringBuilder().append("?apik=").append(this.j).append("&cid=").append(qVar.e).append("&adid=").append(qVar.a).append("&pid=").append(this.q).append("&iid=").append(this.k).append("&sid=").append(this.l).append("&its=").append(afVar.a()).append("&hid=").append(g.a(afVar.a)).append("&ac=").append(a(qVar.g));
        if (this.r != null && !this.r.isEmpty()) {
            for (Map.Entry entry : this.r.entrySet()) {
                append.append("&").append("c_" + g.a((String) entry.getKey())).append("=").append(g.a((String) entry.getValue()));
            }
        }
        append.append("&ats=");
        if (l2 != null) {
            append.append(l2);
        }
        return append.toString();
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            int i3 = (bArr[i2] >> 4) & 15;
            if (i3 < 10) {
                sb.append((char) (i3 + 48));
            } else {
                sb.append((char) ((i3 + 65) - 10));
            }
            byte b2 = bArr[i2] & 15;
            if (b2 < 10) {
                sb.append((char) (b2 + 48));
            } else {
                sb.append((char) ((b2 + 65) - 10));
            }
        }
        return sb.toString();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[adLogs=").append(this.w).append("]");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final synchronized AdImage l() {
        AdImage n2;
        if (!p()) {
            n2 = null;
        } else {
            n2 = n();
        }
        return n2;
    }

    private static void a(Runnable runnable) {
        new Handler().post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(int i2) {
        if (this.y != null) {
            a(new aa(this, i2));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean m() {
        boolean b2;
        if (!p()) {
            b2 = false;
        } else {
            b2 = this.n.b();
        }
        return b2;
    }

    private boolean p() {
        if (!this.p) {
            ag.d(a, "AppCircle is not initialized");
        }
        if (this.q == null) {
            ag.d(a, "Cannot identify UDID.");
        }
        return this.p;
    }
}
