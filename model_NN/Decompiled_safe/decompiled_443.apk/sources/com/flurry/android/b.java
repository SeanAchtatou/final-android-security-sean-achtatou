package com.flurry.android;

import android.content.Context;

final class b implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ FlurryAgent b;
    private /* synthetic */ boolean c;

    b(FlurryAgent flurryAgent, boolean z, Context context) {
        this.b = flurryAgent;
        this.c = z;
        this.a = context;
    }

    public final void run() {
        this.b.i();
        this.b.k();
        if (!this.c) {
            this.b.o.postDelayed(new t(this), FlurryAgent.h);
        }
        if (FlurryAgent.m) {
            this.b.W.c();
        }
    }
}
