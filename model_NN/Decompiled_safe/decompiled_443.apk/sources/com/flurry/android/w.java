package com.flurry.android;

final class w implements Runnable {
    private /* synthetic */ String a;
    private /* synthetic */ ai b;

    w(ai aiVar, String str) {
        this.b = aiVar;
        this.a = str;
    }

    public final void run() {
        if (this.a != null) {
            o.a(this.b.d, this.b.b, this.a);
            this.b.c.a(new h((byte) 8, this.b.d.j()));
            return;
        }
        String str = "Unable to launch in app market: " + this.b.a;
        ag.d(o.a, str);
        this.b.d.e(str);
    }
}
