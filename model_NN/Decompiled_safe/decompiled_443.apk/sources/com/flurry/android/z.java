package com.flurry.android;

import android.view.View;
import android.widget.TextView;

final class z implements View.OnFocusChangeListener {
    private /* synthetic */ TextView a;
    private /* synthetic */ y b;

    z(y yVar, TextView textView) {
        this.b = yVar;
        this.a = textView;
    }

    public final void onFocusChange(View view, boolean z) {
        this.a.setText(z ? this.b.b : this.b.a);
    }
}
