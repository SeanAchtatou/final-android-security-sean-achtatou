package com.flurry.android;

import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

final class l implements Runnable {
    private /* synthetic */ Map a;
    private /* synthetic */ InstallReceiver b;

    l(InstallReceiver installReceiver, Map map) {
        this.b = installReceiver;
        this.a = map;
    }

    public final void run() {
        DataOutputStream dataOutputStream;
        try {
            File parentFile = this.b.b.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.b.b));
                try {
                    boolean z = true;
                    for (Map.Entry entry : this.a.entrySet()) {
                        if (z) {
                            z = false;
                        } else {
                            dataOutputStream.writeUTF("&");
                        }
                        dataOutputStream.writeUTF((String) entry.getKey());
                        dataOutputStream.writeUTF("=");
                        dataOutputStream.writeUTF((String) entry.getValue());
                    }
                    dataOutputStream.writeShort(0);
                    g.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                }
            } else {
                ag.b("InstallReceiver", "Unable to create persistent dir: " + parentFile);
                g.a((Closeable) null);
            }
        } catch (Throwable th2) {
            th = th2;
            dataOutputStream = null;
            g.a(dataOutputStream);
            throw th;
        }
    }
}
