package com.flurry.android;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Arrays;
import java.util.List;

final class ac extends RelativeLayout {
    private o a;
    private Context b;
    private String c;
    private int d;
    private boolean e;
    private boolean f;

    ac(o oVar, Context context, String str, int i) {
        super(context);
        this.a = oVar;
        this.b = context;
        this.c = str;
        this.d = i;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (!this.e) {
            u c2 = c();
            if (c2 != null) {
                removeAllViews();
                addView(c2, b());
                c2.a().a(new h((byte) 3, this.a.j()));
                this.e = true;
            } else if (!this.f) {
                TextView textView = new TextView(this.b);
                textView.setText(o.b);
                textView.setTextSize(1, 20.0f);
                addView(textView, b());
            }
            this.f = true;
        }
    }

    private static RelativeLayout.LayoutParams b() {
        return new RelativeLayout.LayoutParams(-1, -1);
    }

    private synchronized u c() {
        u uVar;
        List a2 = this.a.a(this.b, Arrays.asList(this.c), null, this.d, false);
        if (!a2.isEmpty()) {
            uVar = (u) a2.get(0);
            uVar.setOnClickListener(this.a);
        } else {
            uVar = null;
        }
        return uVar;
    }
}
