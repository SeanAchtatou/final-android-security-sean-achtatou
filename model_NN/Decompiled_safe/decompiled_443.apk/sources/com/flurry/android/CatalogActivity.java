package com.flurry.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.google.ads.AdActivity;
import java.util.ArrayList;
import java.util.List;

public class CatalogActivity extends Activity implements View.OnClickListener {
    private static volatile String a = "<html><body><table height='100%' width='100%' border='0'><tr><td style='vertical-align:middle;text-align:center'>No recommendations available<p><button type='input' onClick='activity.finish()'>Back</button></td></tr></table></body></html>";
    private WebView b;
    private r c;
    private List d = new ArrayList();
    /* access modifiers changed from: private */
    public o e;
    /* access modifiers changed from: private */
    public af f;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Long valueOf;
        setTheme(16973839);
        super.onCreate(bundle);
        this.e = FlurryAgent.b();
        Intent intent = getIntent();
        if (!(intent.getExtras() == null || (valueOf = Long.valueOf(intent.getExtras().getLong(AdActivity.ORIENTATION_PARAM))) == null)) {
            this.f = this.e.b(valueOf.longValue());
        }
        y yVar = new y(this, this);
        yVar.setId(1);
        yVar.setBackgroundColor(-16777216);
        this.b = new WebView(this);
        this.b.setId(2);
        this.b.setScrollBarStyle(0);
        this.b.setBackgroundColor(-1);
        if (this.f != null) {
            this.b.setWebViewClient(new f(this));
        }
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.addJavascriptInterface(this, "activity");
        this.c = new r(this, this);
        this.c.setId(3);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10, yVar.getId());
        relativeLayout.addView(yVar, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(3, yVar.getId());
        layoutParams2.addRule(2, this.c.getId());
        relativeLayout.addView(this.b, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(12, yVar.getId());
        relativeLayout.addView(this.c, layoutParams3);
        Bundle extras = getIntent().getExtras();
        String string = extras == null ? null : extras.getString(AdActivity.URL_PARAM);
        if (string == null) {
            this.b.loadDataWithBaseURL(null, a, "text/html", "utf-8", null);
        } else {
            this.b.loadUrl(string);
        }
        setContentView(relativeLayout);
    }

    public void finish() {
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.e.g();
        super.onDestroy();
    }

    public void onClick(View view) {
        if (view instanceof u) {
            s sVar = new s();
            sVar.a = this.f;
            sVar.b = this.b.getUrl();
            sVar.c = new ArrayList(this.c.b());
            this.d.add(sVar);
            if (this.d.size() > 5) {
                this.d.remove(0);
            }
            s sVar2 = new s();
            u uVar = (u) view;
            String b2 = uVar.b(this.e.i());
            this.f = uVar.a();
            sVar2.a = uVar.a();
            sVar2.a.a(new h((byte) 4, this.e.j()));
            sVar2.b = b2;
            sVar2.c = this.c.a(view.getContext());
            a(sVar2);
        } else if (view.getId() == 10000) {
            finish();
        } else if (view.getId() == 10002) {
            this.c.a();
        } else if (this.d.isEmpty()) {
            finish();
        } else {
            a((s) this.d.remove(this.d.size() - 1));
        }
    }

    private void a(s sVar) {
        try {
            this.b.loadUrl(sVar.b);
            this.c.a(sVar.c);
        } catch (Exception e2) {
            ag.a("FlurryAgent", "Error loading url: " + sVar.b);
        }
    }
}
