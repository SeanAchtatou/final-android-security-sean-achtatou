package com.flurry.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.View;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public final class FlurryAgent implements LocationListener {
    static String a;
    private static volatile String b = null;
    private static volatile String c = null;
    private static volatile String d = "http://ad.flurry.com/getCanvas.do";
    private static volatile String e = null;
    private static volatile String f = "http://ad.flurry.com/getAndroidApp.do";
    /* access modifiers changed from: private */
    public static final FlurryAgent g = new FlurryAgent();
    /* access modifiers changed from: private */
    public static long h = 10000;
    private static boolean i = true;
    private static boolean j = false;
    private static boolean k = true;
    private static volatile String kInsecureReportUrl = "http://data.flurry.com/aap.do";
    private static volatile String kSecureReportUrl = "https://data.flurry.com/aap.do";
    private static Criteria l = null;
    /* access modifiers changed from: private */
    public static boolean m = false;
    private static AppCircle n = new AppCircle();
    private LocationManager A;
    private String B;
    private boolean C;
    private long D;
    private List E = new ArrayList();
    private long F;
    private long G;
    private long H;
    private String I = "";
    private String J = "";
    private byte K = -1;
    private String L;
    private byte M = -1;
    private Long N;
    private int O;
    private Location P;
    private Map Q;
    private List R;
    private boolean S;
    private int T;
    private List U;
    private int V;
    /* access modifiers changed from: private */
    public o W = new o();
    /* access modifiers changed from: private */
    public final Handler o;
    private File p;
    private File q = null;
    private volatile boolean r = false;
    /* access modifiers changed from: private */
    public volatile boolean s = false;
    private long t;
    private Map u = new WeakHashMap();
    private String v;
    private String w;
    private String x;
    private boolean y = true;
    private List z;

    static /* synthetic */ void a(FlurryAgent flurryAgent, Context context, boolean z2) {
        Location location = null;
        if (z2) {
            try {
                location = flurryAgent.d(context);
            } catch (Throwable th) {
                ag.b("FlurryAgent", "", th);
                return;
            }
        }
        synchronized (flurryAgent) {
            flurryAgent.P = location;
        }
        if (m) {
            flurryAgent.W.b();
        }
        flurryAgent.c(true);
    }

    static /* synthetic */ void b(FlurryAgent flurryAgent, Context context) {
        boolean z2;
        try {
            synchronized (flurryAgent) {
                z2 = !flurryAgent.r && SystemClock.elapsedRealtime() - flurryAgent.t > h && flurryAgent.E.size() > 0;
            }
            if (z2) {
                flurryAgent.c(false);
            }
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    public class FlurryDefaultExceptionHandler implements Thread.UncaughtExceptionHandler {
        private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

        FlurryDefaultExceptionHandler() {
        }

        public void uncaughtException(Thread thread, Throwable th) {
            try {
                FlurryAgent.g.a(th);
            } catch (Throwable th2) {
                ag.b("FlurryAgent", "", th2);
            }
            if (this.a != null) {
                this.a.uncaughtException(thread, th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, long):void
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String):void
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context):void
      com.flurry.android.FlurryAgent.a(byte[], java.lang.String):boolean
      com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Throwable th) {
        th.printStackTrace();
        String str = "";
        StackTraceElement[] stackTrace = th.getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            StackTraceElement stackTraceElement = stackTrace[0];
            StringBuilder sb = new StringBuilder();
            sb.append(stackTraceElement.getClassName()).append(".").append(stackTraceElement.getMethodName()).append(":").append(stackTraceElement.getLineNumber());
            if (th.getMessage() != null) {
                sb.append(" (" + th.getMessage() + ")");
            }
            str = sb.toString();
        } else if (th.getMessage() != null) {
            str = th.getMessage();
        }
        onError("uncaught", str, th.getClass().toString());
        this.u.clear();
        a((Context) null, true);
    }

    private FlurryAgent() {
        HandlerThread handlerThread = new HandlerThread("FlurryAgent");
        handlerThread.start();
        this.o = new Handler(handlerThread.getLooper());
    }

    public static void setCatalogIntentName(String str) {
        a = str;
    }

    public static AppCircle getAppCircle() {
        return n;
    }

    static View a(Context context, String str, int i2) {
        if (!m) {
            return null;
        }
        try {
            return g.W.a(context, str, i2);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
            return null;
        }
    }

    static void a(Context context, String str) {
        if (m) {
            g.W.a(context, str);
        }
    }

    static Offer a(String str) {
        if (!m) {
            return null;
        }
        return g.W.b(str);
    }

    static List b(String str) {
        if (!m) {
            return null;
        }
        return g.W.c(str);
    }

    static void a(Context context, long j2) {
        if (!m) {
            ag.d("FlurryAgent", "Cannot accept Offer. AppCircle is not enabled");
        }
        g.W.a(context, j2);
    }

    static void a(List list) {
        if (m) {
            g.W.a(list);
        }
    }

    static void a(boolean z2) {
        if (m) {
            g.W.a(z2);
        }
    }

    static boolean a() {
        return g.W.h();
    }

    public static void enableAppCircle() {
        m = true;
    }

    public static void setDefaultNoAdsMessage(String str) {
        if (m) {
            o.b = str == null ? "" : str;
        }
    }

    static void a(AppCircleCallback appCircleCallback) {
        g.W.a(appCircleCallback);
    }

    public static void addUserCookie(String str, String str2) {
        if (m) {
            g.W.a(str, str2);
        }
    }

    public static void clearUserCookies() {
        if (m) {
            g.W.k();
        }
    }

    public static void onStartSession(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("Null context");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key not specified");
        } else {
            try {
                g.b(context, str);
            } catch (Throwable th) {
                ag.b("FlurryAgent", "", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, long):void
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String):void
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context):void
      com.flurry.android.FlurryAgent.a(byte[], java.lang.String):boolean
      com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void */
    public static void onEndSession(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        try {
            g.a(context, false);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    public static void onPageView() {
        try {
            g.j();
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void logEvent(String str) {
        try {
            g.a(str, (Map) null, false);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void logEvent(String str, Map map) {
        try {
            g.a(str, map, false);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void logEvent(String str, boolean z2) {
        try {
            g.a(str, (Map) null, z2);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void logEvent(String str, Map map, boolean z2) {
        try {
            g.a(str, map, z2);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void endTimedEvent(String str) {
        try {
            g.c(str);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "Failed to signify the end of event: " + str, th);
        }
    }

    public static void onError(String str, String str2, String str3) {
        try {
            g.a(str, str2, str3);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    public static void setReportUrl(String str) {
        b = str;
    }

    public static void setCanvasUrl(String str) {
        c = str;
    }

    public static void setGetAppUrl(String str) {
        e = str;
    }

    public static void setVersionName(String str) {
        synchronized (g) {
            g.x = str;
        }
    }

    public static void setReportLocation(boolean z2) {
        synchronized (g) {
            g.y = z2;
        }
    }

    public static void setLocationCriteria(Criteria criteria) {
        synchronized (g) {
            l = criteria;
        }
    }

    public static void setAge(int i2) {
        if (i2 > 0 && i2 < 110) {
            Date date = new Date(new Date(System.currentTimeMillis() - (((long) i2) * 31449600000L)).getYear(), 1, 1);
            g.N = Long.valueOf(date.getTime());
        }
    }

    public static void setGender(byte b2) {
        switch (b2) {
            case 0:
            case 1:
                g.M = b2;
                return;
            default:
                g.M = -1;
                return;
        }
    }

    public static int getAgentVersion() {
        return 115;
    }

    public static boolean getForbidPlaintextFallback() {
        return false;
    }

    public static void setLogEnabled(boolean z2) {
        synchronized (g) {
            if (z2) {
                ag.b();
            } else {
                ag.a();
            }
        }
    }

    public static void setLogLevel(int i2) {
        synchronized (g) {
            ag.a(i2);
        }
    }

    public static void setContinueSessionMillis(long j2) {
        synchronized (g) {
            h = j2;
        }
    }

    public static void setLogEvents(boolean z2) {
        synchronized (g) {
            i = z2;
        }
    }

    public static void setUserId(String str) {
        synchronized (g) {
            g.L = g.a(str, 255);
        }
    }

    public static void setCaptureUncaughtExceptions(boolean z2) {
        synchronized (g) {
            if (g.r) {
                ag.b("FlurryAgent", "Cannot setCaptureUncaughtExceptions after onSessionStart");
            } else {
                k = z2;
            }
        }
    }

    protected static boolean isCaptureUncaughtExceptions() {
        return k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void onEvent(String str) {
        try {
            g.a(str, (Map) null, false);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void onEvent(String str, Map map) {
        try {
            g.a(str, map, false);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    static o b() {
        return g.W;
    }

    private synchronized void b(Context context, String str) {
        if (this.v != null && !this.v.equals(str)) {
            ag.b("FlurryAgent", "onStartSession called with different api keys: " + this.v + " and " + str);
        }
        if (((Context) this.u.put(context, context)) != null) {
            ag.d("FlurryAgent", "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context");
        }
        if (!this.r) {
            ag.a("FlurryAgent", "Initializing Flurry session");
            this.v = str;
            this.q = context.getFileStreamPath(".flurryagent." + Integer.toString(this.v.hashCode(), 16));
            this.p = context.getFileStreamPath(".flurryb.");
            if (k) {
                Thread.setDefaultUncaughtExceptionHandler(new FlurryDefaultExceptionHandler());
            }
            Context applicationContext = context.getApplicationContext();
            if (this.x == null) {
                this.x = c(applicationContext);
            }
            String packageName = applicationContext.getPackageName();
            if (this.w != null && !this.w.equals(packageName)) {
                ag.b("FlurryAgent", "onStartSession called from different application packages: " + this.w + " and " + packageName);
            }
            this.w = packageName;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime - this.t > h) {
                ag.a("FlurryAgent", "New session");
                this.F = System.currentTimeMillis();
                this.G = elapsedRealtime;
                this.H = -1;
                this.L = "";
                this.O = 0;
                this.P = null;
                this.J = TimeZone.getDefault().getID();
                this.I = Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
                this.Q = new HashMap();
                this.R = new ArrayList();
                this.S = true;
                this.U = new ArrayList();
                this.T = 0;
                this.V = 0;
                if (m && !this.W.a()) {
                    ag.a("FlurryAgent", "Initializing AppCircle");
                    a aVar = new a();
                    aVar.a = this.v;
                    aVar.b = this.D;
                    aVar.c = this.F;
                    aVar.d = this.G;
                    aVar.e = c != null ? c : d;
                    aVar.f = c();
                    aVar.g = this.o;
                    this.W.a(context, aVar);
                    ag.a("FlurryAgent", "AppCircle initialized");
                }
                a(new d(this, applicationContext, this.y));
            } else {
                ag.a("FlurryAgent", "Continuing previous session");
                if (!this.E.isEmpty()) {
                    this.E.remove(this.E.size() - 1);
                }
            }
            this.r = true;
        }
    }

    private synchronized void a(Context context, boolean z2) {
        if (context != null) {
            if (((Context) this.u.remove(context)) == null) {
                ag.d("FlurryAgent", "onEndSession called without context from corresponding onStartSession");
            }
        }
        if (this.r && this.u.isEmpty()) {
            ag.a("FlurryAgent", "Ending session");
            l();
            Context applicationContext = context == null ? null : context.getApplicationContext();
            if (context != null) {
                String packageName = applicationContext.getPackageName();
                if (!this.w.equals(packageName)) {
                    ag.b("FlurryAgent", "onEndSession called from different application package, expected: " + this.w + " actual: " + packageName);
                }
            }
            this.r = false;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.t = elapsedRealtime;
            this.H = elapsedRealtime - this.G;
            if (this.B == null) {
                ag.b("FlurryAgent", "Not creating report because of bad Android ID or generated ID is null");
            }
            a(new b(this, z2, applicationContext));
        }
    }

    /* access modifiers changed from: private */
    public synchronized void i() {
        DataOutputStream dataOutputStream;
        IOException e2;
        Throwable th;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(1);
                dataOutputStream2.writeUTF(this.x);
                dataOutputStream2.writeLong(this.F);
                dataOutputStream2.writeLong(this.H);
                dataOutputStream2.writeLong(0);
                dataOutputStream2.writeUTF(this.I);
                dataOutputStream2.writeUTF(this.J);
                dataOutputStream2.writeByte(this.K);
                dataOutputStream2.writeUTF(this.L);
                if (this.P == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeDouble(this.P.getLatitude());
                    dataOutputStream2.writeDouble(this.P.getLongitude());
                    dataOutputStream2.writeFloat(this.P.getAccuracy());
                }
                dataOutputStream2.writeInt(this.V);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(this.M);
                if (this.N == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeLong(this.N.longValue());
                }
                dataOutputStream2.writeShort(this.Q.size());
                for (Map.Entry entry : this.Q.entrySet()) {
                    dataOutputStream2.writeUTF((String) entry.getKey());
                    dataOutputStream2.writeInt(((i) entry.getValue()).a);
                }
                dataOutputStream2.writeShort(this.R.size());
                for (m b2 : this.R) {
                    dataOutputStream2.write(b2.b());
                }
                dataOutputStream2.writeBoolean(this.S);
                dataOutputStream2.writeInt(this.O);
                dataOutputStream2.writeShort(this.U.size());
                for (x xVar : this.U) {
                    dataOutputStream2.writeLong(xVar.a);
                    dataOutputStream2.writeUTF(xVar.b);
                    dataOutputStream2.writeUTF(xVar.c);
                    dataOutputStream2.writeUTF(xVar.d);
                }
                if (m) {
                    List<af> f2 = this.W.f();
                    dataOutputStream2.writeShort(f2.size());
                    for (af a2 : f2) {
                        a2.a(dataOutputStream2);
                    }
                } else {
                    dataOutputStream2.writeShort(0);
                }
                this.E.add(byteArrayOutputStream.toByteArray());
                g.a(dataOutputStream2);
            } catch (IOException e3) {
                e2 = e3;
                dataOutputStream = dataOutputStream2;
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream = dataOutputStream2;
                g.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            dataOutputStream = null;
            e2 = iOException;
            try {
                ag.b("FlurryAgent", "", e2);
                g.a(dataOutputStream);
            } catch (Throwable th3) {
                th = th3;
                g.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            dataOutputStream = null;
            th = th5;
            g.a(dataOutputStream);
            throw th;
        }
    }

    private void a(Runnable runnable) {
        this.o.post(runnable);
    }

    private synchronized void j() {
        this.V++;
    }

    private synchronized void a(String str, Map map, boolean z2) {
        Map map2;
        if (this.R == null) {
            ag.b("FlurryAgent", "onEvent called before onStartSession.  Event: " + str);
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.G;
            String a2 = g.a(str, 255);
            if (a2.length() != 0) {
                i iVar = (i) this.Q.get(a2);
                if (iVar != null) {
                    iVar.a++;
                } else if (this.Q.size() < 100) {
                    i iVar2 = new i();
                    iVar2.a = 1;
                    this.Q.put(a2, iVar2);
                } else if (ag.a("FlurryAgent")) {
                    ag.a("FlurryAgent", "MaxEventIds exceeded: " + a2);
                }
                if (!i || this.R.size() >= 100 || this.T >= 8000) {
                    this.S = false;
                } else {
                    if (map == null) {
                        map2 = Collections.emptyMap();
                    } else {
                        map2 = map;
                    }
                    if (map2.size() <= 10) {
                        m mVar = new m(a2, map2, elapsedRealtime, z2);
                        if (mVar.b().length + this.T <= 8000) {
                            this.R.add(mVar);
                            this.T = mVar.b().length + this.T;
                        } else {
                            this.T = 8000;
                            this.S = false;
                        }
                    } else if (ag.a("FlurryAgent")) {
                        ag.a("FlurryAgent", "MaxEventParams exceeded: " + map2.size());
                    }
                }
            }
        }
    }

    private synchronized void c(String str) {
        Iterator it = this.R.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            m mVar = (m) it.next();
            if (mVar.a(str)) {
                mVar.a();
                break;
            }
        }
    }

    private synchronized void a(String str, String str2, String str3) {
        if (this.U == null) {
            ag.b("FlurryAgent", "onError called before onStartSession.  Error: " + str);
        } else {
            this.O++;
            if (this.U.size() < 10) {
                x xVar = new x();
                xVar.a = System.currentTimeMillis();
                xVar.b = g.a(str, 255);
                xVar.c = g.a(str2, 512);
                xVar.d = g.a(str3, 255);
                this.U.add(xVar);
            }
        }
    }

    private synchronized byte[] b(boolean z2) {
        DataOutputStream dataOutputStream;
        byte[] bArr;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(15);
                if (!m || !z2) {
                    dataOutputStream2.writeShort(0);
                } else {
                    dataOutputStream2.writeShort(1);
                }
                if (m) {
                    dataOutputStream2.writeLong(this.W.d());
                    Set<Long> e2 = this.W.e();
                    dataOutputStream2.writeShort(e2.size());
                    for (Long longValue : e2) {
                        long longValue2 = longValue.longValue();
                        dataOutputStream2.writeByte(1);
                        dataOutputStream2.writeLong(longValue2);
                    }
                } else {
                    dataOutputStream2.writeLong(0);
                    dataOutputStream2.writeShort(0);
                }
                dataOutputStream2.writeShort(3);
                dataOutputStream2.writeShort(115);
                dataOutputStream2.writeLong(System.currentTimeMillis());
                dataOutputStream2.writeUTF(this.v);
                dataOutputStream2.writeUTF(this.x);
                dataOutputStream2.writeShort(0);
                dataOutputStream2.writeUTF(this.B);
                dataOutputStream2.writeLong(this.D);
                dataOutputStream2.writeLong(this.F);
                dataOutputStream2.writeShort(6);
                dataOutputStream2.writeUTF("device.model");
                dataOutputStream2.writeUTF(Build.MODEL);
                dataOutputStream2.writeUTF("build.brand");
                dataOutputStream2.writeUTF(Build.BRAND);
                dataOutputStream2.writeUTF("build.id");
                dataOutputStream2.writeUTF(Build.ID);
                dataOutputStream2.writeUTF("version.release");
                dataOutputStream2.writeUTF(Build.VERSION.RELEASE);
                dataOutputStream2.writeUTF("build.device");
                dataOutputStream2.writeUTF(Build.DEVICE);
                dataOutputStream2.writeUTF("build.product");
                dataOutputStream2.writeUTF(Build.PRODUCT);
                int size = this.E.size();
                dataOutputStream2.writeShort(size);
                for (int i2 = 0; i2 < size; i2++) {
                    dataOutputStream2.write((byte[]) this.E.get(i2));
                }
                this.z = new ArrayList(this.E);
                dataOutputStream2.close();
                bArr = byteArrayOutputStream.toByteArray();
                g.a(dataOutputStream2);
            } catch (Throwable th) {
                th = th;
                dataOutputStream = dataOutputStream2;
                g.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            dataOutputStream = null;
            g.a(dataOutputStream);
            throw th;
        }
        return bArr;
    }

    static String c() {
        return e != null ? e : f;
    }

    static boolean d() {
        if (!m) {
            return false;
        }
        return g.W.m();
    }

    private boolean a(byte[] bArr) {
        boolean z2;
        String str = b != null ? b : kInsecureReportUrl;
        if (str == null) {
            return false;
        }
        try {
            z2 = a(bArr, str);
        } catch (Exception e2) {
            ag.a("FlurryAgent", "Sending report exception: " + e2.getMessage());
            z2 = false;
        }
        if (z2 || b != null) {
        }
        return z2;
    }

    /* JADX INFO: finally extract failed */
    private boolean a(byte[] bArr, String str) {
        boolean z2;
        if ("local".equals(str)) {
            return true;
        }
        ag.a("FlurryAgent", "Sending report to: " + str);
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
        byteArrayEntity.setContentType("application/octet-stream");
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(byteArrayEntity);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
        httpPost.getParams().setBooleanParameter("http.protocol.expect-continue", false);
        HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(httpPost);
        int statusCode = execute.getStatusLine().getStatusCode();
        synchronized (this) {
            if (statusCode == 200) {
                ag.a("FlurryAgent", "Report successful");
                this.C = true;
                this.E.removeAll(this.z);
                HttpEntity entity = execute.getEntity();
                ag.a("FlurryAgent", "Processing report response");
                if (entity == null || entity.getContentLength() == 0) {
                    z2 = true;
                } else {
                    try {
                        a(new DataInputStream(entity.getContent()));
                        entity.consumeContent();
                        z2 = true;
                    } catch (Throwable th) {
                        entity.consumeContent();
                        throw th;
                    }
                }
            } else {
                ag.a("FlurryAgent", "Report failed. HTTP response: " + statusCode);
                z2 = false;
            }
            this.z = null;
        }
        return z2;
    }

    /* JADX WARN: Type inference failed for: r0v12, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.DataInputStream r15) {
        /*
            r14 = this;
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
        L_0x001e:
            int r7 = r15.readUnsignedShort()
            int r0 = r15.readInt()
            switch(r7) {
                case 258: goto L_0x005f;
                case 259: goto L_0x0063;
                case 260: goto L_0x0029;
                case 261: goto L_0x0029;
                case 262: goto L_0x0082;
                case 263: goto L_0x00b4;
                case 264: goto L_0x0044;
                case 265: goto L_0x0029;
                case 266: goto L_0x00cd;
                case 267: goto L_0x0029;
                case 268: goto L_0x0104;
                case 269: goto L_0x014b;
                case 270: goto L_0x00c8;
                case 271: goto L_0x00e5;
                case 272: goto L_0x0121;
                case 273: goto L_0x0150;
                default: goto L_0x0029;
            }
        L_0x0029:
            java.lang.String r8 = "FlurryAgent"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Unknown chunkType: "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r9 = r9.toString()
            com.flurry.android.ag.a(r8, r9)
            r15.skipBytes(r0)
        L_0x0044:
            r0 = 264(0x108, float:3.7E-43)
            if (r7 != r0) goto L_0x001e
            boolean r0 = com.flurry.android.FlurryAgent.m
            if (r0 == 0) goto L_0x005e
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x0059
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r7 = "No ads from server"
            com.flurry.android.ag.a(r0, r7)
        L_0x0059:
            com.flurry.android.o r0 = r14.W
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x005e:
            return
        L_0x005f:
            r15.readInt()
            goto L_0x0044
        L_0x0063:
            byte r0 = r15.readByte()
            int r8 = r15.readUnsignedShort()
            com.flurry.android.q[] r9 = new com.flurry.android.q[r8]
            r10 = 0
        L_0x006e:
            if (r10 >= r8) goto L_0x007a
            com.flurry.android.q r11 = new com.flurry.android.q
            r11.<init>(r15)
            r9[r10] = r11
            int r10 = r10 + 1
            goto L_0x006e
        L_0x007a:
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            r1.put(r0, r9)
            goto L_0x0044
        L_0x0082:
            int r0 = r15.readUnsignedShort()
            r8 = 0
        L_0x0087:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.AdImage r9 = new com.flurry.android.AdImage
            r9.<init>(r15)
            long r10 = r9.a
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            r4.put(r10, r9)
            java.lang.String r10 = "FlurryAgent"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Parsed image: "
            java.lang.StringBuilder r11 = r11.append(r12)
            long r12 = r9.a
            java.lang.StringBuilder r9 = r11.append(r12)
            java.lang.String r9 = r9.toString()
            com.flurry.android.ag.a(r10, r9)
            int r8 = r8 + 1
            goto L_0x0087
        L_0x00b4:
            int r0 = r15.readInt()
            r8 = 0
        L_0x00b9:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.e r9 = new com.flurry.android.e
            r9.<init>(r15)
            java.lang.String r10 = r9.a
            r2.put(r10, r9)
            int r8 = r8 + 1
            goto L_0x00b9
        L_0x00c8:
            r15.skipBytes(r0)
            goto L_0x0044
        L_0x00cd:
            byte r0 = r15.readByte()
            r8 = 0
        L_0x00d2:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.c r9 = new com.flurry.android.c
            r9.<init>(r15)
            byte r10 = r9.a
            java.lang.Byte r10 = java.lang.Byte.valueOf(r10)
            r3.put(r10, r9)
            int r8 = r8 + 1
            goto L_0x00d2
        L_0x00e5:
            byte r8 = r15.readByte()
            r0 = 0
            r9 = r0
        L_0x00eb:
            if (r9 >= r8) goto L_0x0044
            byte r0 = r15.readByte()
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            java.lang.Object r0 = r3.get(r0)
            com.flurry.android.c r0 = (com.flurry.android.c) r0
            if (r0 == 0) goto L_0x0100
            r0.a(r15)
        L_0x0100:
            int r0 = r9 + 1
            r9 = r0
            goto L_0x00eb
        L_0x0104:
            int r0 = r15.readInt()
            r8 = 0
        L_0x0109:
            if (r8 >= r0) goto L_0x0044
            long r9 = r15.readLong()
            short r11 = r15.readShort()
            java.lang.Short r11 = java.lang.Short.valueOf(r11)
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            r6.put(r11, r9)
            int r8 = r8 + 1
            goto L_0x0109
        L_0x0121:
            long r8 = r15.readLong()
            java.lang.Long r0 = java.lang.Long.valueOf(r8)
            java.lang.Object r0 = r5.get(r0)
            com.flurry.android.aj r0 = (com.flurry.android.aj) r0
            if (r0 != 0) goto L_0x0136
            com.flurry.android.aj r0 = new com.flurry.android.aj
            r0.<init>()
        L_0x0136:
            java.lang.String r10 = r15.readUTF()
            r0.a = r10
            int r10 = r15.readInt()
            r0.c = r10
            java.lang.Long r8 = java.lang.Long.valueOf(r8)
            r5.put(r8, r0)
            goto L_0x0044
        L_0x014b:
            r15.skipBytes(r0)
            goto L_0x0044
        L_0x0150:
            r15.skipBytes(r0)
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.FlurryAgent.a(java.io.DataInputStream):void");
    }

    private void c(boolean z2) {
        try {
            ag.a("FlurryAgent", "generating report");
            byte[] b2 = b(z2);
            if (b2 == null) {
                ag.a("FlurryAgent", "Error generating report");
            } else if (a(b2)) {
                ag.a("FlurryAgent", "Done sending " + (this.r ? "initial " : "") + "agent report");
                k();
            }
        } catch (IOException e2) {
            ag.a("FlurryAgent", "", e2);
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065 A[Catch:{ Throwable -> 0x0120 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0078 A[Catch:{ Throwable -> 0x0120 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0086 A[Catch:{ Throwable -> 0x0120 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:49:0x0113=Splitter:B:49:0x0113, B:20:0x0074=Splitter:B:20:0x0074, B:12:0x005e=Splitter:B:12:0x005e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(android.content.Context r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            java.lang.String r0 = r9.b(r10)     // Catch:{ all -> 0x010c }
            r9.B = r0     // Catch:{ all -> 0x010c }
            java.io.File r0 = r9.q     // Catch:{ all -> 0x010c }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x010c }
            if (r0 == 0) goto L_0x012a
            java.lang.String r0 = "FlurryAgent"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            r1.<init>()     // Catch:{ all -> 0x010c }
            java.lang.String r2 = "loading persistent data: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x010c }
            java.io.File r2 = r9.q     // Catch:{ all -> 0x010c }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ all -> 0x010c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x010c }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x010c }
            com.flurry.android.ag.c(r0, r1)     // Catch:{ all -> 0x010c }
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0138, all -> 0x010f }
            java.io.File r2 = r9.q     // Catch:{ Throwable -> 0x0138, all -> 0x010f }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0138, all -> 0x010f }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0138, all -> 0x010f }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0138, all -> 0x010f }
            int r0 = r2.readUnsignedShort()     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            java.lang.String r1 = "FlurryAgent"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            r3.<init>()     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            java.lang.String r4 = "Magic: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            com.flurry.android.ag.c(r1, r3)     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            r1 = 46586(0xb5fa, float:6.5281E-41)
            if (r0 != r1) goto L_0x00f5
            r9.b(r2)     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
        L_0x005e:
            com.flurry.android.g.a(r2)     // Catch:{ all -> 0x010c }
        L_0x0061:
            boolean r0 = r9.s     // Catch:{ Throwable -> 0x0120 }
            if (r0 != 0) goto L_0x0074
            java.io.File r0 = r9.q     // Catch:{ Throwable -> 0x0120 }
            boolean r0 = r0.delete()     // Catch:{ Throwable -> 0x0120 }
            if (r0 == 0) goto L_0x0117
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Deleted persistence file"
            com.flurry.android.ag.a(r0, r1)     // Catch:{ Throwable -> 0x0120 }
        L_0x0074:
            boolean r0 = r9.s     // Catch:{ all -> 0x010c }
            if (r0 != 0) goto L_0x0082
            r0 = 0
            r9.C = r0     // Catch:{ all -> 0x010c }
            long r0 = r9.F     // Catch:{ all -> 0x010c }
            r9.D = r0     // Catch:{ all -> 0x010c }
            r0 = 1
            r9.s = r0     // Catch:{ all -> 0x010c }
        L_0x0082:
            java.lang.String r0 = r9.B     // Catch:{ all -> 0x010c }
            if (r0 != 0) goto L_0x00d5
            double r0 = java.lang.Math.random()     // Catch:{ all -> 0x010c }
            long r0 = java.lang.Double.doubleToLongBits(r0)     // Catch:{ all -> 0x010c }
            r2 = 37
            long r4 = java.lang.System.nanoTime()     // Catch:{ all -> 0x010c }
            java.lang.String r6 = r9.v     // Catch:{ all -> 0x010c }
            int r6 = r6.hashCode()     // Catch:{ all -> 0x010c }
            int r6 = r6 * 37
            long r6 = (long) r6     // Catch:{ all -> 0x010c }
            long r4 = r4 + r6
            long r2 = r2 * r4
            long r0 = r0 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            r2.<init>()     // Catch:{ all -> 0x010c }
            java.lang.String r3 = "ID"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x010c }
            r3 = 16
            java.lang.String r0 = java.lang.Long.toString(r0, r3)     // Catch:{ all -> 0x010c }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x010c }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x010c }
            r9.B = r0     // Catch:{ all -> 0x010c }
            java.lang.String r0 = "FlurryAgent"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            r1.<init>()     // Catch:{ all -> 0x010c }
            java.lang.String r2 = "Generated phoneId: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x010c }
            java.lang.String r2 = r9.B     // Catch:{ all -> 0x010c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x010c }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x010c }
            com.flurry.android.ag.c(r0, r1)     // Catch:{ all -> 0x010c }
        L_0x00d5:
            com.flurry.android.o r0 = r9.W     // Catch:{ all -> 0x010c }
            java.lang.String r1 = r9.B     // Catch:{ all -> 0x010c }
            r0.a(r1)     // Catch:{ all -> 0x010c }
            java.lang.String r0 = r9.B     // Catch:{ all -> 0x010c }
            java.lang.String r1 = "AND"
            boolean r0 = r0.startsWith(r1)     // Catch:{ all -> 0x010c }
            if (r0 != 0) goto L_0x00f3
            java.io.File r0 = r9.p     // Catch:{ all -> 0x010c }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x010c }
            if (r0 != 0) goto L_0x00f3
            java.lang.String r0 = r9.B     // Catch:{ all -> 0x010c }
            r9.c(r10, r0)     // Catch:{ all -> 0x010c }
        L_0x00f3:
            monitor-exit(r9)
            return
        L_0x00f5:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Unexpected file type"
            com.flurry.android.ag.a(r0, r1)     // Catch:{ Throwable -> 0x00fe, all -> 0x0133 }
            goto L_0x005e
        L_0x00fe:
            r0 = move-exception
            r1 = r2
        L_0x0100:
            java.lang.String r2 = "FlurryAgent"
            java.lang.String r3 = "Error when loading persistent file"
            com.flurry.android.ag.b(r2, r3, r0)     // Catch:{ all -> 0x0136 }
            com.flurry.android.g.a(r1)     // Catch:{ all -> 0x010c }
            goto L_0x0061
        L_0x010c:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x010f:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0113:
            com.flurry.android.g.a(r1)     // Catch:{ all -> 0x010c }
            throw r0     // Catch:{ all -> 0x010c }
        L_0x0117:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Cannot delete persistence file"
            com.flurry.android.ag.b(r0, r1)     // Catch:{ Throwable -> 0x0120 }
            goto L_0x0074
        L_0x0120:
            r0 = move-exception
            java.lang.String r1 = "FlurryAgent"
            java.lang.String r2 = ""
            com.flurry.android.ag.b(r1, r2, r0)     // Catch:{ all -> 0x010c }
            goto L_0x0074
        L_0x012a:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Agent cache file doesn't exist."
            com.flurry.android.ag.c(r0, r1)     // Catch:{ all -> 0x010c }
            goto L_0x0074
        L_0x0133:
            r0 = move-exception
            r1 = r2
            goto L_0x0113
        L_0x0136:
            r0 = move-exception
            goto L_0x0113
        L_0x0138:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0100
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.FlurryAgent.a(android.content.Context):void");
    }

    private synchronized void b(DataInputStream dataInputStream) {
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        ag.a("FlurryAgent", "File version: " + readUnsignedShort);
        if (readUnsignedShort > 2) {
            ag.b("FlurryAgent", "Unknown agent file version: " + readUnsignedShort);
            throw new IOException("Unknown agent file version: " + readUnsignedShort);
        } else if (readUnsignedShort >= 2) {
            String readUTF = dataInputStream.readUTF();
            ag.a("FlurryAgent", "Loading API key: " + this.v);
            if (readUTF.equals(this.v)) {
                String readUTF2 = dataInputStream.readUTF();
                if (this.B == null) {
                    ag.a("FlurryAgent", "Loading phoneId: " + readUTF2);
                }
                this.B = readUTF2;
                this.C = dataInputStream.readBoolean();
                this.D = dataInputStream.readLong();
                ag.a("FlurryAgent", "Loading session reports");
                int i2 = 0;
                while (true) {
                    int readUnsignedShort2 = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort2 == 0) {
                        break;
                    }
                    byte[] bArr = new byte[readUnsignedShort2];
                    dataInputStream.readFully(bArr);
                    this.E.add(0, bArr);
                    i2++;
                    ag.a("FlurryAgent", "Session report added: " + i2);
                }
                ag.a("FlurryAgent", "Persistent file loaded");
                this.s = true;
            } else {
                ag.a("FlurryAgent", "Api keys do not match, old: " + readUTF + ", new: " + this.v);
            }
        } else {
            ag.d("FlurryAgent", "Deleting old file version: " + readUnsignedShort);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void k() {
        DataOutputStream dataOutputStream;
        try {
            if (!a(this.q)) {
                g.a((Closeable) null);
            } else {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.q));
                try {
                    dataOutputStream.writeShort(46586);
                    dataOutputStream.writeShort(2);
                    dataOutputStream.writeUTF(this.v);
                    dataOutputStream.writeUTF(this.B);
                    dataOutputStream.writeBoolean(this.C);
                    dataOutputStream.writeLong(this.D);
                    int size = this.E.size() - 1;
                    while (true) {
                        if (size < 0) {
                            break;
                        }
                        byte[] bArr = (byte[]) this.E.get(size);
                        int length = bArr.length;
                        if (length + 2 + dataOutputStream.size() > 50000) {
                            ag.a("FlurryAgent", "discarded sessions: " + size);
                            break;
                        }
                        dataOutputStream.writeShort(length);
                        dataOutputStream.write(bArr);
                        size--;
                    }
                    dataOutputStream.writeShort(0);
                    g.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                    try {
                        ag.b("FlurryAgent", "", th);
                        g.a(dataOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        g.a(dataOutputStream);
                        throw th;
                    }
                }
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            g.a(dataOutputStream);
            throw th;
        }
    }

    private static boolean a(File file) {
        File parentFile = file.getParentFile();
        if (parentFile.mkdirs() || parentFile.exists()) {
            return true;
        }
        ag.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
        return false;
    }

    private synchronized void c(Context context, String str) {
        DataOutputStream dataOutputStream;
        Throwable th;
        Throwable th2;
        this.p = context.getFileStreamPath(".flurryb.");
        if (a(this.p)) {
            try {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(this.p));
                try {
                    dataOutputStream2.writeInt(1);
                    dataOutputStream2.writeUTF(str);
                    g.a(dataOutputStream2);
                } catch (Throwable th3) {
                    th = th3;
                    dataOutputStream = dataOutputStream2;
                    g.a(dataOutputStream);
                    throw th;
                }
            } catch (Throwable th4) {
                Throwable th5 = th4;
                dataOutputStream = null;
                th = th5;
                g.a(dataOutputStream);
                throw th;
            }
        }
    }

    private String b(Context context) {
        DataInputStream dataInputStream;
        if (this.B != null) {
            return this.B;
        }
        String string = Settings.System.getString(context.getContentResolver(), "android_id");
        if (string != null && string.length() > 0 && !string.equals("null") && !string.equals("9774d56d682e549c")) {
            return "AND" + string;
        }
        File fileStreamPath = context.getFileStreamPath(".flurryb.");
        if (!fileStreamPath.exists()) {
            return null;
        }
        try {
            DataInputStream dataInputStream2 = new DataInputStream(new FileInputStream(fileStreamPath));
            try {
                dataInputStream2.readInt();
                String readUTF = dataInputStream2.readUTF();
                g.a(dataInputStream2);
                return readUTF;
            } catch (Throwable th) {
                Throwable th2 = th;
                dataInputStream = dataInputStream2;
                th = th2;
                g.a(dataInputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            dataInputStream = null;
            g.a(dataInputStream);
            throw th;
        }
    }

    private static String c(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return "Unknown";
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
    }

    private Location d(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            synchronized (this) {
                if (this.A == null) {
                    this.A = locationManager;
                } else {
                    locationManager = this.A;
                }
            }
            Criteria criteria = l;
            if (criteria == null) {
                criteria = new Criteria();
            }
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider != null) {
                locationManager.requestLocationUpdates(bestProvider, 0, 0.0f, this, Looper.getMainLooper());
                return locationManager.getLastKnownLocation(bestProvider);
            }
        }
        return null;
    }

    private synchronized void l() {
        if (this.A != null) {
            this.A.removeUpdates(this);
        }
    }

    static String e() {
        return g.v;
    }

    private synchronized String m() {
        return this.B;
    }

    public static String getPhoneId() {
        return g.m();
    }

    public final synchronized void onLocationChanged(Location location) {
        try {
            this.P = location;
            l();
        } catch (Throwable th) {
            ag.b("FlurryAgent", "", th);
        }
        return;
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i2, Bundle bundle) {
    }
}
