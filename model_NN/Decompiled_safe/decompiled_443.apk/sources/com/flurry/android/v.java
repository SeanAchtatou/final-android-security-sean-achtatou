package com.flurry.android;

import android.content.Context;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class v {
    private Context a;
    private o b;
    private a c;
    private volatile long d;
    private ad e = new ad(1000);
    private ad f = new ad(1000);
    private Map g = new HashMap();
    private Map h = new HashMap();
    private Map i = new HashMap();
    private Map j = new HashMap();
    private volatile boolean k;

    v() {
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context, o oVar, a aVar) {
        this.a = context;
        this.b = oVar;
        this.c = aVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized q[] a(String str) {
        q[] qVarArr;
        qVarArr = (q[]) this.g.get(str);
        if (qVarArr == null) {
            qVarArr = (q[]) this.g.get("");
        }
        return qVarArr;
    }

    /* access modifiers changed from: package-private */
    public final synchronized aj a(long j2) {
        return (aj) this.f.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set a() {
        return this.e.c();
    }

    /* access modifiers changed from: package-private */
    public final synchronized AdImage b(long j2) {
        return (AdImage) this.e.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized AdImage a(short s) {
        Long l;
        l = (Long) this.j.get((short) 1);
        return l == null ? null : b(l.longValue());
    }

    /* access modifiers changed from: package-private */
    public final synchronized e b(String str) {
        e eVar;
        eVar = (e) this.h.get(str);
        if (eVar == null) {
            eVar = (e) this.h.get("");
        }
        return eVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.k;
    }

    private synchronized c a(byte b2) {
        return (c) this.i.get(Byte.valueOf(b2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        this.d = System.currentTimeMillis();
        for (Map.Entry entry : map4.entrySet()) {
            if (entry.getValue() != null) {
                this.e.a(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry entry2 : map5.entrySet()) {
            if (entry2.getValue() != null) {
                this.f.a(entry2.getKey(), entry2.getValue());
            }
        }
        if (map2 != null && !map2.isEmpty()) {
            this.h = map2;
        }
        if (map3 != null && !map3.isEmpty()) {
            this.i = map3;
        }
        if (map6 != null && !map6.isEmpty()) {
            this.j = map6;
        }
        this.g = new HashMap();
        for (Map.Entry entry3 : map2.entrySet()) {
            e eVar = (e) entry3.getValue();
            q[] qVarArr = (q[]) map.get(Byte.valueOf(eVar.b));
            if (qVarArr != null) {
                this.g.put(entry3.getKey(), qVarArr);
            }
            c cVar = (c) map3.get(Byte.valueOf(eVar.c));
            if (cVar != null) {
                eVar.d = cVar;
            }
        }
        f();
        a((int) CallbackEvent.ADS_UPDATED);
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x004a=Splitter:B:28:0x004a, B:11:0x002d=Splitter:B:11:0x002d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void d() {
        /*
            r6 = this;
            monitor-enter(r6)
            android.content.Context r0 = r6.a     // Catch:{ all -> 0x0046 }
            java.lang.String r1 = r6.g()     // Catch:{ all -> 0x0046 }
            java.io.File r0 = r0.getFileStreamPath(r1)     // Catch:{ all -> 0x0046 }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x0046 }
            if (r1 == 0) goto L_0x004e
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x0071, all -> 0x0049 }
            int r1 = r3.readUnsignedShort()     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
            r2 = 46587(0xb5fb, float:6.5282E-41)
            if (r1 != r2) goto L_0x0032
            r6.a(r3)     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
            r1 = 201(0xc9, float:2.82E-43)
            r6.a(r1)     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
        L_0x002d:
            com.flurry.android.g.a(r3)     // Catch:{ all -> 0x0046 }
        L_0x0030:
            monitor-exit(r6)
            return
        L_0x0032:
            a(r0)     // Catch:{ Throwable -> 0x0036, all -> 0x006b }
            goto L_0x002d
        L_0x0036:
            r1 = move-exception
            r2 = r3
        L_0x0038:
            java.lang.String r3 = "FlurryAgent"
            java.lang.String r4 = "Discarding cache"
            com.flurry.android.ag.a(r3, r4, r1)     // Catch:{ all -> 0x006e }
            a(r0)     // Catch:{ all -> 0x006e }
            com.flurry.android.g.a(r2)     // Catch:{ all -> 0x0046 }
            goto L_0x0030
        L_0x0046:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            com.flurry.android.g.a(r1)     // Catch:{ all -> 0x0046 }
            throw r0     // Catch:{ all -> 0x0046 }
        L_0x004e:
            java.lang.String r1 = "FlurryAgent"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0046 }
            r2.<init>()     // Catch:{ all -> 0x0046 }
            java.lang.String r3 = "cache file does not exist, path="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0046 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0046 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0046 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0046 }
            com.flurry.android.ag.c(r1, r0)     // Catch:{ all -> 0x0046 }
            goto L_0x0030
        L_0x006b:
            r0 = move-exception
            r1 = r3
            goto L_0x004a
        L_0x006e:
            r0 = move-exception
            r1 = r2
            goto L_0x004a
        L_0x0071:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.v.d():void");
    }

    private static void a(File file) {
        if (!file.delete()) {
            ag.b("FlurryAgent", "Cannot delete cached ads");
        }
    }

    private void f() {
        Iterator it = this.i.values().iterator();
        while (it.hasNext()) {
            it.next();
        }
        for (q[] qVarArr : this.g.values()) {
            if (qVarArr != null) {
                for (q qVar : qVarArr) {
                    qVar.h = b(qVar.f.longValue());
                    if (qVar.h == null) {
                        ag.b("FlurryAgent", "Ad " + qVar.d + " has no image");
                    }
                    if (a(qVar.a) == null) {
                        ag.b("FlurryAgent", "Ad " + qVar.d + " has no pricing");
                    }
                }
            }
        }
        for (e eVar : this.h.values()) {
            eVar.d = a(eVar.c);
            if (eVar.d == null) {
                ag.d("FlurryAgent", "No ad theme found for " + ((int) eVar.c));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void e() {
        DataOutputStream dataOutputStream;
        try {
            File fileStreamPath = this.a.getFileStreamPath(g());
            File parentFile = fileStreamPath.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(fileStreamPath));
                try {
                    dataOutputStream2.writeShort(46587);
                    a(dataOutputStream2);
                    g.a(dataOutputStream2);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    dataOutputStream = dataOutputStream2;
                    th = th2;
                    g.a(dataOutputStream);
                    throw th;
                }
            } else {
                ag.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                g.a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            g.a(dataOutputStream);
            throw th;
        }
    }

    private void a(DataInputStream dataInputStream) {
        ag.a("FlurryAgent", "Reading cache");
        if (dataInputStream.readUnsignedShort() == 2) {
            this.d = dataInputStream.readLong();
            int readUnsignedShort = dataInputStream.readUnsignedShort();
            this.e = new ad(1000);
            for (int i2 = 0; i2 < readUnsignedShort; i2++) {
                long readLong = dataInputStream.readLong();
                AdImage adImage = new AdImage();
                adImage.a(dataInputStream);
                this.e.a(Long.valueOf(readLong), adImage);
            }
            int readUnsignedShort2 = dataInputStream.readUnsignedShort();
            this.f = new ad(1000);
            for (int i3 = 0; i3 < readUnsignedShort2; i3++) {
                long readLong2 = dataInputStream.readLong();
                aj ajVar = new aj();
                if (dataInputStream.readBoolean()) {
                    ajVar.a = dataInputStream.readUTF();
                }
                if (dataInputStream.readBoolean()) {
                    ajVar.b = dataInputStream.readUTF();
                }
                ajVar.c = dataInputStream.readInt();
                this.f.a(Long.valueOf(readLong2), ajVar);
            }
            int readUnsignedShort3 = dataInputStream.readUnsignedShort();
            this.h = new HashMap(readUnsignedShort3);
            for (int i4 = 0; i4 < readUnsignedShort3; i4++) {
                this.h.put(dataInputStream.readUTF(), new e(dataInputStream));
            }
            int readUnsignedShort4 = dataInputStream.readUnsignedShort();
            this.g = new HashMap(readUnsignedShort4);
            for (int i5 = 0; i5 < readUnsignedShort4; i5++) {
                String readUTF = dataInputStream.readUTF();
                int readUnsignedShort5 = dataInputStream.readUnsignedShort();
                q[] qVarArr = new q[readUnsignedShort5];
                for (int i6 = 0; i6 < readUnsignedShort5; i6++) {
                    q qVar = new q();
                    qVar.a(dataInputStream);
                    qVarArr[i6] = qVar;
                }
                this.g.put(readUTF, qVarArr);
            }
            int readUnsignedShort6 = dataInputStream.readUnsignedShort();
            this.i = new HashMap();
            for (int i7 = 0; i7 < readUnsignedShort6; i7++) {
                byte readByte = dataInputStream.readByte();
                c cVar = new c();
                cVar.b(dataInputStream);
                this.i.put(Byte.valueOf(readByte), cVar);
            }
            int readUnsignedShort7 = dataInputStream.readUnsignedShort();
            this.j = new HashMap(readUnsignedShort7);
            for (int i8 = 0; i8 < readUnsignedShort7; i8++) {
                this.j.put(Short.valueOf(dataInputStream.readShort()), Long.valueOf(dataInputStream.readLong()));
            }
            f();
            ag.a("FlurryAgent", "Cache read, num images: " + this.e.a());
        }
    }

    private void a(DataOutputStream dataOutputStream) {
        boolean z;
        dataOutputStream.writeShort(2);
        dataOutputStream.writeLong(this.d);
        List<Map.Entry> b2 = this.e.b();
        Collections.reverse(b2);
        dataOutputStream.writeShort(b2.size());
        int i2 = 0;
        for (Map.Entry entry : b2) {
            if (i2 < 1000) {
                dataOutputStream.writeLong(((Long) entry.getKey()).longValue());
                AdImage adImage = (AdImage) entry.getValue();
                dataOutputStream.writeLong(adImage.a);
                dataOutputStream.writeInt(adImage.b);
                dataOutputStream.writeInt(adImage.c);
                dataOutputStream.writeUTF(adImage.d);
                dataOutputStream.writeInt(adImage.e.length);
                dataOutputStream.write(adImage.e);
                i2++;
            } else {
                return;
            }
        }
        List<Map.Entry> b3 = this.f.b();
        Collections.reverse(b3);
        dataOutputStream.writeShort(b3.size());
        int i3 = 0;
        for (Map.Entry entry2 : b3) {
            if (i3 >= 1000) {
                break;
            }
            dataOutputStream.writeLong(((Long) entry2.getKey()).longValue());
            aj ajVar = (aj) entry2.getValue();
            if (ajVar.a != null) {
                z = true;
            } else {
                z = false;
            }
            dataOutputStream.writeBoolean(z);
            if (z) {
                dataOutputStream.writeUTF(ajVar.a);
            }
            boolean z2 = ajVar.b != null;
            dataOutputStream.writeBoolean(z2);
            if (z2) {
                dataOutputStream.writeUTF(ajVar.b);
            }
            dataOutputStream.writeInt(ajVar.c);
            i3++;
        }
        dataOutputStream.writeShort(this.h.size());
        for (Map.Entry entry3 : this.h.entrySet()) {
            dataOutputStream.writeUTF((String) entry3.getKey());
            e eVar = (e) entry3.getValue();
            dataOutputStream.writeUTF(eVar.a);
            dataOutputStream.writeByte(eVar.b);
            dataOutputStream.writeByte(eVar.c);
        }
        dataOutputStream.writeShort(this.g.size());
        for (Map.Entry entry4 : this.g.entrySet()) {
            dataOutputStream.writeUTF((String) entry4.getKey());
            q[] qVarArr = (q[]) entry4.getValue();
            int length = qVarArr == null ? 0 : qVarArr.length;
            dataOutputStream.writeShort(length);
            for (int i4 = 0; i4 < length; i4++) {
                q qVar = qVarArr[i4];
                dataOutputStream.writeLong(qVar.a);
                dataOutputStream.writeLong(qVar.b);
                dataOutputStream.writeUTF(qVar.d);
                dataOutputStream.writeUTF(qVar.c);
                dataOutputStream.writeLong(qVar.e);
                dataOutputStream.writeLong(qVar.f.longValue());
                dataOutputStream.writeByte(qVar.g.length);
                dataOutputStream.write(qVar.g);
            }
        }
        dataOutputStream.writeShort(this.i.size());
        for (Map.Entry entry5 : this.i.entrySet()) {
            dataOutputStream.writeByte(((Byte) entry5.getKey()).byteValue());
            ((c) entry5.getValue()).a(dataOutputStream);
        }
        dataOutputStream.writeShort(this.j.size());
        for (Map.Entry entry6 : this.j.entrySet()) {
            dataOutputStream.writeShort(((Short) entry6.getKey()).shortValue());
            dataOutputStream.writeLong(((Long) entry6.getValue()).longValue());
        }
    }

    private String g() {
        return ".flurryappcircle." + Integer.toString(this.c.a.hashCode(), 16);
    }

    private void a(int i2) {
        this.k = !this.g.isEmpty();
        if (this.k) {
            this.b.a(i2);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("adImages (" + this.e.b().size() + "),\n");
        sb.append("adBlock (" + this.g.size() + "):").append(",\n");
        for (Map.Entry entry : this.g.entrySet()) {
            sb.append("\t" + ((String) entry.getKey()) + ": " + Arrays.toString((Object[]) entry.getValue()));
        }
        sb.append("adHooks (" + this.h.size() + "):" + this.h).append(",\n");
        sb.append("adThemes (" + this.i.size() + "):" + this.i).append(",\n");
        sb.append("auxMap (" + this.j.size() + "):" + this.j).append(",\n");
        sb.append("}");
        return sb.toString();
    }
}
