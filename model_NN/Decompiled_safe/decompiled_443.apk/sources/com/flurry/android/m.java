package com.flurry.android;

import android.os.SystemClock;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

final class m {
    private String a;
    private Map b;
    private long c;
    private boolean d;
    private long e;
    private byte[] f;

    public m(String str, Map map, long j, boolean z) {
        this.a = str;
        this.b = map;
        this.c = j;
        this.d = z;
    }

    public final boolean a(String str) {
        return this.d && this.e == 0 && this.a.equals(str);
    }

    public final void a() {
        this.e = SystemClock.elapsedRealtime() - this.c;
    }

    public final byte[] b() {
        DataOutputStream dataOutputStream;
        Throwable th;
        if (this.f == null) {
            DataOutputStream dataOutputStream2 = null;
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DataOutputStream dataOutputStream3 = new DataOutputStream(byteArrayOutputStream);
                try {
                    dataOutputStream3.writeUTF(this.a);
                    if (this.b == null) {
                        dataOutputStream3.writeShort(0);
                    } else {
                        dataOutputStream3.writeShort(this.b.size());
                        for (Map.Entry entry : this.b.entrySet()) {
                            dataOutputStream3.writeUTF(g.a((String) entry.getKey(), 255));
                            dataOutputStream3.writeUTF(g.a((String) entry.getValue(), 255));
                        }
                    }
                    dataOutputStream3.writeLong(this.c);
                    dataOutputStream3.writeLong(this.e);
                    dataOutputStream3.flush();
                    this.f = byteArrayOutputStream.toByteArray();
                    g.a(dataOutputStream3);
                } catch (IOException e2) {
                    dataOutputStream2 = dataOutputStream3;
                    try {
                        this.f = new byte[0];
                        g.a(dataOutputStream2);
                        return this.f;
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        dataOutputStream = dataOutputStream2;
                        th = th3;
                        g.a(dataOutputStream);
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    dataOutputStream = dataOutputStream3;
                    g.a(dataOutputStream);
                    throw th;
                }
            } catch (IOException e3) {
                this.f = new byte[0];
                g.a(dataOutputStream2);
                return this.f;
            } catch (Throwable th5) {
                Throwable th6 = th5;
                dataOutputStream = null;
                th = th6;
                g.a(dataOutputStream);
                throw th;
            }
        }
        return this.f;
    }
}
