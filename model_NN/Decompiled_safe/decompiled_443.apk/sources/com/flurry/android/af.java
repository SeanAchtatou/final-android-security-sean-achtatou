package com.flurry.android;

import java.io.DataOutput;
import java.util.ArrayList;
import java.util.List;

final class af {
    final String a;
    q b;
    long c;
    List d = new ArrayList();
    private byte e;

    af(String str, byte b2, long j) {
        this.a = str;
        this.e = b2;
        this.d.add(new h((byte) 1, j));
    }

    /* access modifiers changed from: package-private */
    public final void a(h hVar) {
        this.d.add(hVar);
    }

    /* access modifiers changed from: package-private */
    public final long a() {
        return ((h) this.d.get(0)).b;
    }

    /* access modifiers changed from: package-private */
    public final void a(DataOutput dataOutput) {
        dataOutput.writeUTF(this.a);
        dataOutput.writeByte(this.e);
        if (this.b == null) {
            dataOutput.writeLong(0);
            dataOutput.writeLong(0);
            dataOutput.writeByte(0);
        } else {
            dataOutput.writeLong(this.b.a);
            dataOutput.writeLong(this.b.e);
            byte[] bArr = this.b.g;
            dataOutput.writeByte(bArr.length);
            dataOutput.write(bArr);
        }
        dataOutput.writeShort(this.d.size());
        for (h hVar : this.d) {
            dataOutput.writeByte(hVar.a);
            dataOutput.writeLong(hVar.b);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{hook: " + this.a + ", ad: " + this.b.d + ", transitions: [");
        for (h append : this.d) {
            sb.append(append);
            sb.append(",");
        }
        sb.append("]}");
        return sb.toString();
    }
}
