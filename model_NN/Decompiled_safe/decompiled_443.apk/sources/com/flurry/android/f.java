package com.flurry.android;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class f extends WebViewClient {
    private /* synthetic */ CatalogActivity a;

    f(CatalogActivity catalogActivity) {
        this.a = catalogActivity;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str == null) {
            return false;
        }
        if (this.a.f != null) {
            this.a.f.a(new h((byte) 6, this.a.e.j()));
        }
        this.a.e.a(webView.getContext(), this.a.f, str);
        return true;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        ag.c("FlurryAgent", "Failed to load url: " + str2);
        webView.loadData("Cannot find Android Market information. <p>Please check your network", "text/html", "UTF-8");
    }

    public final void onPageFinished(WebView webView, String str) {
        try {
            af a2 = this.a.f;
            h hVar = new h((byte) 5, this.a.e.j());
            long j = this.a.f.c;
            a2.d.add(hVar);
            a2.c = j;
        } catch (Exception e) {
        }
    }
}
