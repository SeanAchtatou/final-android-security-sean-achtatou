package com.scoreloop.client.android.core.model;

import android.os.Build;
import com.flurry.android.CallbackEvent;
import org.json.JSONException;
import org.json.JSONObject;

public class Device extends BaseEntity {
    public static String a = "device";
    private String c;
    private State d = State.UNKNOWN;

    public enum State {
        CREATED(CallbackEvent.ADS_LOADED_FROM_CACHE),
        FREED(1),
        UNKNOWN(0),
        VERIFIED(200);
        
        private final int a;

        private State(int i) {
            this.a = i;
        }

        public int intValue() {
            return this.a;
        }
    }

    public String a() {
        return a;
    }

    public void a(State state) {
        this.d = state;
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
    }

    public void b(String str) {
        this.b = str;
    }

    public String c() {
        return Build.MODEL;
    }

    public JSONObject d() throws JSONException {
        JSONObject d2 = super.d();
        d2.put("uuid", h());
        d2.put("name", e());
        d2.put("system_name", f());
        d2.put("system_version", g());
        d2.put("model", c());
        return d2;
    }

    public String e() {
        return null;
    }

    public String f() {
        return "Android";
    }

    public String g() {
        return Build.VERSION.RELEASE;
    }

    public String h() {
        return this.c;
    }
}
