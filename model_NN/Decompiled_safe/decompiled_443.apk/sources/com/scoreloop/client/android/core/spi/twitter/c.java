package com.scoreloop.client.android.core.spi.twitter;

import android.app.Activity;
import android.content.DialogInterface;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.spi.AuthViewController;
import com.scoreloop.client.android.core.util.Logger;
import com.scoreloop.client.android.core.util.OAuthBuilder;
import java.net.URL;
import java.util.HashMap;

class c extends AuthViewController {
    a a;
    private String b;

    c(Session session, AuthViewController.Observer observer) {
        super(session, observer);
    }

    public void a(Activity activity) {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        HashMap hashMap = new HashMap();
        hashMap.put("oauth_token", this.b);
        URL a2 = oAuthBuilder.a("http://twitter.com/oauth/authorize", hashMap);
        this.a = new a(activity, 16973841, this);
        this.a.b();
        this.a.setCancelable(true);
        this.a.setCanceledOnTouchOutside(true);
        this.a.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                Logger.a("twitter auth view controller", "dialog dismissed");
            }
        });
        this.a.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                Logger.a("twitter auth view controller", "dialog cancelled");
            }
        });
        this.a.a(a2.toString());
        this.a.show();
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.b;
    }
}
