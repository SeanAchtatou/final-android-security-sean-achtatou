package com.scoreloop.client.android.core.model;

import java.util.Arrays;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageReceiver {
    private final MessageReceiverInterface a;
    private final String[] b;

    public MessageReceiver(MessageReceiverInterface messageReceiverInterface, String[] strArr) {
        this.a = messageReceiverInterface;
        this.b = strArr;
    }

    public MessageReceiverInterface a() {
        return this.a;
    }

    public JSONObject b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("receiver_type", a().a());
        if (this.b.length > 0) {
            jSONObject.put("users", new JSONArray((Collection) Arrays.asList(this.b)));
        }
        return jSONObject;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof MessageReceiver)) {
            return super.equals(obj);
        }
        MessageReceiver messageReceiver = (MessageReceiver) obj;
        if (messageReceiver == this) {
            return true;
        }
        return a().equals(messageReceiver.a());
    }

    public int hashCode() {
        return a().hashCode();
    }
}
