package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.PublishedFor__2_2_0;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.AchievementsStore;
import com.scoreloop.client.android.core.model.Award;
import com.scoreloop.client.android.core.model.AwardList;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.persistence.DbAchievementsStore;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AchievementsController extends RequestController {
    private static String c = null;
    private List<Achievement> d;
    private AchievementsStore e;
    private AwardList f;
    private boolean g;
    private User h;

    private static final class a extends Request {
        private final AwardList a;
        private final Game b;
        private final User c;

        public a(RequestCompletionCallback requestCompletionCallback, User user, Game game, AwardList awardList) {
            super(requestCompletionCallback);
            this.c = user;
            this.b = game;
            this.a = awardList;
        }

        public String a() {
            return String.format("/service/games/%s/achievements", this.b.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("user_id", this.c.getIdentifier());
                jSONObject.put("achievable_list_id", this.a != null ? this.a.a() : null);
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid achievement request", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    @PublishedFor__1_1_0
    public AchievementsController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_1_0
    public AchievementsController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.d = Collections.emptyList();
        this.e = null;
        this.f = null;
        this.g = true;
        this.h = null;
        this.h = i();
    }

    private Achievement a(String str, String str2) {
        b();
        Award awardWithIdentifier = getAwardList().getAwardWithIdentifier(str);
        for (Achievement next : getAchievements()) {
            boolean equals = str2 == null ? next.getIdentifier() == null : str2.equals(next.getIdentifier());
            if (awardWithIdentifier.equals(next.getAward()) && equals) {
                return next;
            }
        }
        return null;
    }

    private void a(List<Achievement> list, List<Award> list2) {
        HashSet hashSet = new HashSet();
        for (Achievement award : list) {
            hashSet.add(award.getAward());
        }
        for (Award next : list2) {
            if (!hashSet.contains(next)) {
                list.add(new Achievement(next));
            }
        }
    }

    private void b() {
        if (getAwardList() == null) {
            throw new IllegalStateException("no SLAwards.bundle found in the assets");
        }
    }

    private void b(List<Achievement> list) throws JSONException {
        Achievement achievement;
        for (Achievement next : list) {
            Award award = next.getAward();
            String identifier = award.getIdentifier();
            Achievement a2 = a(identifier, next.getIdentifier());
            if (a2 != null || (achievement = a(identifier, (String) null)) == null || achievement.needsSubmit()) {
                achievement = a2;
            }
            if (achievement == null) {
                achievement = new Achievement(award, null, a());
            }
            achievement.a(next, false);
        }
        a().b();
    }

    private void c() {
        ArrayList arrayList = new ArrayList();
        AwardList awardList = getAwardList();
        AchievementsStore a2 = a();
        for (Award next : awardList.getAwards()) {
            try {
                List<Achievement> a3 = a2.a(next);
                if (a3.size() > 0) {
                    arrayList.addAll(a3);
                } else {
                    arrayList.add(new Achievement(next, null, a2));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                Logger.b("can't create achievement from local store for award: " + next);
            }
        }
        a(arrayList);
    }

    private void k() {
        a aVar = new a(g(), getUser(), getGame(), getAwardList());
        aVar.a(120000);
        b(aVar);
    }

    /* access modifiers changed from: package-private */
    public AchievementsStore a() {
        if (this.e == null) {
            this.e = new DbAchievementsStore(h().d(), getAwardList(), getGame().getIdentifier());
        }
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void a(List<Achievement> list) {
        this.d = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        try {
            if (response.f() != 200) {
                throw new IllegalStateException("invalid response status: " + response.f());
            }
            JSONArray jSONArray = response.e().getJSONArray("achievements");
            AwardList awardList = getAwardList();
            List<Award> awards = awardList.getAwards();
            ArrayList arrayList = new ArrayList();
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                Achievement achievement = new Achievement(awardList, jSONArray.getJSONObject(i).getJSONObject(Achievement.a));
                if (awards.contains(achievement.getAward())) {
                    arrayList.add(achievement);
                }
            }
            if (h().isOwnedByUser(getUser())) {
                c();
                b(arrayList);
                return true;
            }
            a(arrayList, awards);
            a(arrayList);
            return true;
        } catch (Exception e2) {
            throw new IllegalStateException(e2);
        }
    }

    @PublishedFor__1_1_0
    public int countAchievedAwards() {
        HashSet hashSet = new HashSet();
        for (Achievement next : getAchievements()) {
            if (next.isAchieved()) {
                hashSet.add(next.getAward());
            }
        }
        return hashSet.size();
    }

    @PublishedFor__1_1_0
    public Achievement getAchievementForAwardIdentifier(String str) {
        b();
        Award awardWithIdentifier = getAwardList().getAwardWithIdentifier(str);
        for (Achievement next : getAchievements()) {
            if (awardWithIdentifier.equals(next.getAward())) {
                return next;
            }
        }
        return null;
    }

    @PublishedFor__1_1_0
    public List<Achievement> getAchievements() {
        return this.d;
    }

    @PublishedFor__1_1_0
    public AwardList getAwardList() {
        if (this.f != null) {
            return this.f;
        }
        if (this.f == null && c == null) {
            this.f = getGame().e();
        }
        if (this.f == null) {
            this.f = AwardList.a(h().d(), getGame().getIdentifier(), c);
            if (c == null) {
                getGame().a(this.f);
            }
        }
        return this.f;
    }

    @PublishedFor__2_2_0
    public boolean getForceInitialSync() {
        return this.g;
    }

    @PublishedFor__1_1_0
    public User getUser() {
        return this.h;
    }

    @PublishedFor__2_2_0
    public boolean hadInitialSync() {
        return a().a();
    }

    @PublishedFor__1_1_0
    public void loadAchievements() {
        b();
        a_();
        if (!h().isOwnedByUser(getUser()) || (this.g && !a().a())) {
            k();
            return;
        }
        c();
        j();
    }

    @PublishedFor__2_2_0
    public void setForceInitialSync(boolean z) {
        this.g = z;
    }

    @PublishedFor__1_1_0
    public void setUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user must not be null");
        }
        this.h = user;
    }
}
