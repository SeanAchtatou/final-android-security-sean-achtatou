package com.scoreloop.client.android.core.util;

import com.scoreloop.client.android.core.settings.Settings;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoUtil {
    private final Cipher a = a();
    private final byte[] b;
    private SecretKeySpec c;

    public CryptoUtil(String str, String str2) {
        this.b = a(str, str2);
        this.c = new SecretKeySpec(this.b, "AES");
    }

    private Cipher a() {
        try {
            return Settings.a != null ? Cipher.getInstance("AES/CBC/PKCS5Padding", Settings.a) : Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException();
        } catch (NoSuchPaddingException e2) {
            throw new IllegalStateException();
        } catch (NoSuchProviderException e3) {
            throw new IllegalStateException();
        }
    }

    private byte[] a(String str, String str2) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            instance.reset();
            instance.update(str2.getBytes());
            byte[] digest = instance.digest();
            instance.reset();
            instance.update(str.getBytes());
            byte[] digest2 = instance.digest();
            byte[] bArr = new byte[16];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) ((digest[(i + 6) % digest.length] ^ digest2[(i + 3) % digest2.length]) ^ 62);
            }
            return bArr;
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException();
        }
    }

    public String a(String str) {
        return Base64.a(b(str));
    }

    public String a(byte[] bArr) {
        try {
            this.a.init(2, this.c, new IvParameterSpec(this.b));
            return new String(this.a.doFinal(bArr), "UTF8");
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public byte[] b(String str) {
        try {
            this.a.init(1, this.c, new IvParameterSpec(this.b));
            return this.a.doFinal(str.getBytes("UTF8"));
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new IllegalStateException(e2);
        }
    }

    public String c(String str) {
        try {
            return a(Base64.a(str));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
