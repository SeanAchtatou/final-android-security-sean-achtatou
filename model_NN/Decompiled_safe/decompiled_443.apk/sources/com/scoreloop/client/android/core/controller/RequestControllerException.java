package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__2_1_0;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestControllerException extends Exception {
    @PublishedFor__2_1_0
    public static final int CODE_BUDDY_ADD_REQUEST_ALREADY_ADDED = 40;
    @PublishedFor__2_1_0
    public static final int CODE_BUDDY_REMOVE_REQUEST_ALREADY_REMOVED = 41;
    @PublishedFor__2_1_0
    public static final int CODE_SOCIAL_PROVIDER_DISCONNECTED = 110;
    @PublishedFor__2_1_0
    public static final int CODE_UNDEFINED = 0;
    @PublishedFor__2_1_0
    public static final int DETAIL_USER_UPDATE_REQUEST_EMAIL_TAKEN = 16;
    @PublishedFor__2_1_0
    public static final int DETAIL_USER_UPDATE_REQUEST_IMAGE_TOO_LARGE = 32;
    @PublishedFor__2_1_0
    public static final int DETAIL_USER_UPDATE_REQUEST_INVALID_EMAIL = 8;
    @PublishedFor__2_1_0
    public static final int DETAIL_USER_UPDATE_REQUEST_INVALID_USERNAME = 1;
    @PublishedFor__2_1_0
    public static final int DETAIL_USER_UPDATE_REQUEST_UNSUPPORTED_MIME_TYPE = 64;
    @PublishedFor__2_1_0
    public static final int DETAIL_USER_UPDATE_REQUEST_USERNAME_TAKEN = 4;
    @PublishedFor__2_1_0
    public static final int DETAIL_USER_UPDATE_REQUEST_USERNAME_TOO_SHORT = 2;
    @PublishedFor__2_1_0
    public static final String INFO_KEY_DISCONNECTED_PROVIDER_IDENTIFIERS = "disconnectedProviderIdentifiers";
    private static final Integer a = 1;
    private static final Integer b = 4;
    private static final Integer c = 2;
    private static final long serialVersionUID = 1;
    private int d;
    private int e;
    private String f;
    private Map<String, Object> g;

    public RequestControllerException() {
        super("Request failed");
        this.d = 0;
    }

    public RequestControllerException(String str) {
        this();
        this.f = str;
    }

    public static RequestControllerException a(Response response) {
        int f2 = response.f();
        if (f2 == 200 || f2 == 201) {
            return null;
        }
        RequestControllerException requestControllerException = new RequestControllerException("RequestError");
        SetterIntent setterIntent = new SetterIntent();
        try {
            JSONObject b2 = setterIntent.b((JSONObject) response.a(), "error", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
            requestControllerException.a(setterIntent.d(b2, "message", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE));
            Integer a2 = setterIntent.a(b2, "code", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
            requestControllerException.c(a2.intValue());
            requestControllerException.b(setterIntent.a(b2, "details", SetterIntent.KeyMode.COERCE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_AND_COERCES_NULL_VALUE).intValue());
            if (a2 == null) {
                return requestControllerException;
            }
            switch (a2.intValue()) {
                case CODE_SOCIAL_PROVIDER_DISCONNECTED /*110*/:
                    ArrayList arrayList = new ArrayList();
                    if (requestControllerException.hasDetail(a.intValue())) {
                        arrayList.add(SocialProvider.FACEBOOK_IDENTIFIER);
                    }
                    if (requestControllerException.hasDetail(c.intValue())) {
                        arrayList.add(SocialProvider.TWITTER_IDENTIFIER);
                    }
                    if (requestControllerException.hasDetail(b.intValue())) {
                        arrayList.add(SocialProvider.MYSPACE_IDENTIFIER);
                    }
                    HashMap hashMap = new HashMap();
                    hashMap.put(INFO_KEY_DISCONNECTED_PROVIDER_IDENTIFIERS, arrayList);
                    requestControllerException.a(hashMap);
                    return requestControllerException;
                default:
                    return requestControllerException;
            }
        } catch (JSONException e2) {
            return requestControllerException;
        }
    }

    public void a(int i) {
        this.d |= i;
    }

    public void a(String str) {
        this.f = str;
    }

    public void a(Map<String, Object> map) {
        this.g = map;
    }

    public boolean a() {
        return hasDetail(1) | hasDetail(4) | hasDetail(2);
    }

    public void b(int i) {
        this.d = i;
    }

    public void c(int i) {
        this.e = i;
    }

    @PublishedFor__2_1_0
    public int getErrorCode() {
        return this.e;
    }

    @PublishedFor__2_1_0
    public String getErrorMessage() {
        return this.f;
    }

    @PublishedFor__2_1_0
    public Map<String, Object> getUserInfo() {
        return this.g;
    }

    @PublishedFor__2_1_0
    public boolean hasDetail(int i) {
        return (this.d & i) != 0;
    }
}
