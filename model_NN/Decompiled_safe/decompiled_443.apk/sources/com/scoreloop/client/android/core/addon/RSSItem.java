package com.scoreloop.client.android.core.addon;

import android.content.Context;
import android.content.SharedPreferences;
import com.scoreloop.client.android.core.PublishedFor__2_0_0;
import java.util.List;

public class RSSItem {
    private final Context a;
    private String b;
    private String c;
    private String d;
    private boolean e;
    private String f;
    private String g;
    private Boolean h;

    RSSItem(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("context must not be null");
        }
        this.a = context;
    }

    @PublishedFor__2_0_0
    public static void resetPersistentReadFlags(Context context, List<RSSItem> list) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.scoreloop.feed", 0).edit();
        edit.clear();
        edit.commit();
        for (RSSItem rSSItem : list) {
            rSSItem.h = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.e = z;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        try {
            return (getTitle() == null || getIdentifier() == null) ? false : true;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (str == null) {
            throw new IllegalArgumentException("identifier must not be null");
        }
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public void e(String str) {
        this.g = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return getIdentifier().equals(((RSSItem) obj).getIdentifier());
    }

    @PublishedFor__2_0_0
    public String getDescription() {
        return this.b;
    }

    @PublishedFor__2_0_0
    public String getIdentifier() {
        if (this.c != null) {
            return this.c;
        }
        throw new IllegalStateException("identifier of RSSItem must not be null");
    }

    @PublishedFor__2_0_0
    public String getImageUrlString() {
        return this.d;
    }

    @PublishedFor__2_0_0
    public String getLinkUrlString() {
        return this.f;
    }

    @PublishedFor__2_0_0
    public String getTitle() {
        return this.g;
    }

    @PublishedFor__2_0_0
    public boolean hasPersistentReadFlag() {
        if (this.h != null) {
            return this.h.booleanValue();
        }
        this.h = Boolean.valueOf(this.a.getSharedPreferences("com.scoreloop.feed", 0).getBoolean(getIdentifier(), false));
        return this.h.booleanValue();
    }

    public int hashCode() {
        return getIdentifier().hashCode();
    }

    @PublishedFor__2_0_0
    public boolean isSticky() {
        return this.e;
    }

    @PublishedFor__2_0_0
    public void setHasPersistentReadFlag(boolean z) {
        if (hasPersistentReadFlag() != z) {
            SharedPreferences.Editor edit = this.a.getSharedPreferences("com.scoreloop.feed", 0).edit();
            edit.putBoolean(getIdentifier(), z);
            edit.commit();
            this.h = Boolean.valueOf(z);
        }
    }
}
