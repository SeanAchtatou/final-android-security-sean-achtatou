package com.scoreloop.client.android.core.util;

import com.flurry.android.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Base64 {
    static final /* synthetic */ boolean a = (!Base64.class.desiredAssertionStatus());
    private static final byte[] b = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] c = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 0, -9, -9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -9, -9, -9, -1, -9, -9, -9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, -9, -9, -9, -9, 37, -9, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] d = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] e = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] f = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] g = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};

    static class a extends FilterOutputStream {
        private final byte[] a;
        private final boolean b;
        private byte[] c;
        private final int d;
        private final byte[] e;
        private final boolean f;
        private int g;
        private final int h;
        private int i;
        private boolean j;

        public a(OutputStream outputStream) {
            this(outputStream, 1);
        }

        public a(OutputStream outputStream, int i2) {
            super(outputStream);
            this.b = (i2 & 8) != 0;
            this.f = (i2 & 1) != 0;
            this.d = this.f ? 3 : 4;
            this.c = new byte[this.d];
            this.i = 0;
            this.g = 0;
            this.j = false;
            this.a = new byte[4];
            this.h = i2;
            this.e = Base64.c(i2);
        }

        public void a() throws IOException {
            if (this.i <= 0) {
                return;
            }
            if (this.f) {
                this.out.write(Base64.b(this.a, this.c, this.i, this.h));
                this.i = 0;
                return;
            }
            throw new IOException("Base64 input not properly padded.");
        }

        public void close() throws IOException {
            a();
            super.close();
            this.c = null;
            this.out = null;
        }

        public void write(int i2) throws IOException {
            if (this.j) {
                this.out.write(i2);
            } else if (this.f) {
                byte[] bArr = this.c;
                int i3 = this.i;
                this.i = i3 + 1;
                bArr[i3] = (byte) i2;
                if (this.i >= this.d) {
                    this.out.write(Base64.b(this.a, this.c, this.d, this.h));
                    this.g += 4;
                    if (this.b && this.g >= 76) {
                        this.out.write(10);
                        this.g = 0;
                    }
                    this.i = 0;
                }
            } else if (this.e[i2 & 127] > -5) {
                byte[] bArr2 = this.c;
                int i4 = this.i;
                this.i = i4 + 1;
                bArr2[i4] = (byte) i2;
                if (this.i >= this.d) {
                    this.out.write(this.a, 0, Base64.b(this.c, 0, this.a, 0, this.h));
                    this.i = 0;
                }
            } else if (this.e[i2 & 127] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public void write(byte[] bArr, int i2, int i3) throws IOException {
            if (this.j) {
                this.out.write(bArr, i2, i3);
                return;
            }
            for (int i4 = 0; i4 < i3; i4++) {
                write(bArr[i2 + i4]);
            }
        }
    }

    private Base64() {
    }

    public static String a(byte[] bArr) {
        String str = null;
        try {
            str = b(bArr, 0, bArr.length, 0);
        } catch (IOException e2) {
            if (!a) {
                throw new AssertionError(e2.getMessage());
            }
        }
        if (a || str != null) {
            return str;
        }
        throw new AssertionError();
    }

    public static byte[] a(String str) throws IOException {
        return a(str, 0);
    }

    public static byte[] a(String str, int i) throws IOException {
        byte[] bytes;
        ByteArrayOutputStream byteArrayOutputStream;
        GZIPInputStream gZIPInputStream;
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayInputStream byteArrayInputStream2;
        GZIPInputStream gZIPInputStream2;
        ByteArrayOutputStream byteArrayOutputStream2;
        if (str == null) {
            throw new NullPointerException("Input string was null.");
        }
        try {
            bytes = str.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e2) {
            bytes = str.getBytes();
        }
        byte[] a2 = a(bytes, 0, bytes.length, i);
        boolean z = (i & 4) != 0;
        if (a2 != null && a2.length >= 4 && !z && 35615 == ((a2[0] & Constants.UNKNOWN) | ((a2[1] << 8) & Constants.FEMALE))) {
            byte[] bArr = new byte[2048];
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    byteArrayInputStream2 = new ByteArrayInputStream(a2);
                    try {
                        GZIPInputStream gZIPInputStream3 = new GZIPInputStream(byteArrayInputStream2);
                        while (true) {
                            try {
                                int read = gZIPInputStream3.read(bArr);
                                if (read < 0) {
                                    break;
                                }
                                byteArrayOutputStream.write(bArr, 0, read);
                            } catch (IOException e3) {
                                e = e3;
                                GZIPInputStream gZIPInputStream4 = gZIPInputStream3;
                                byteArrayInputStream = byteArrayInputStream2;
                                gZIPInputStream = gZIPInputStream4;
                            } catch (Throwable th) {
                                th = th;
                                byteArrayOutputStream2 = byteArrayOutputStream;
                                gZIPInputStream2 = gZIPInputStream3;
                                try {
                                    byteArrayOutputStream2.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    gZIPInputStream2.close();
                                } catch (Exception e5) {
                                }
                                try {
                                    byteArrayInputStream2.close();
                                } catch (Exception e6) {
                                }
                                throw th;
                            }
                        }
                        a2 = byteArrayOutputStream.toByteArray();
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e7) {
                        }
                        try {
                            gZIPInputStream3.close();
                        } catch (Exception e8) {
                        }
                        try {
                            byteArrayInputStream2.close();
                        } catch (Exception e9) {
                        }
                    } catch (IOException e10) {
                        e = e10;
                        byteArrayInputStream = byteArrayInputStream2;
                        gZIPInputStream = null;
                        try {
                            e.printStackTrace();
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception e11) {
                            }
                            try {
                                gZIPInputStream.close();
                            } catch (Exception e12) {
                            }
                            try {
                                byteArrayInputStream.close();
                            } catch (Exception e13) {
                            }
                            return a2;
                        } catch (Throwable th2) {
                            th = th2;
                            byteArrayOutputStream2 = byteArrayOutputStream;
                            gZIPInputStream2 = gZIPInputStream;
                            byteArrayInputStream2 = byteArrayInputStream;
                            byteArrayOutputStream2.close();
                            gZIPInputStream2.close();
                            byteArrayInputStream2.close();
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        byteArrayOutputStream2 = byteArrayOutputStream;
                        gZIPInputStream2 = null;
                        byteArrayOutputStream2.close();
                        gZIPInputStream2.close();
                        byteArrayInputStream2.close();
                        throw th;
                    }
                } catch (IOException e14) {
                    e = e14;
                    gZIPInputStream = null;
                    byteArrayInputStream = null;
                    e.printStackTrace();
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return a2;
                } catch (Throwable th4) {
                    th = th4;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    byteArrayInputStream2 = null;
                    gZIPInputStream2 = null;
                    byteArrayOutputStream2.close();
                    gZIPInputStream2.close();
                    byteArrayInputStream2.close();
                    throw th;
                }
            } catch (IOException e15) {
                e = e15;
                byteArrayOutputStream = null;
                gZIPInputStream = null;
                byteArrayInputStream = null;
                e.printStackTrace();
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                return a2;
            } catch (Throwable th5) {
                th = th5;
                byteArrayOutputStream2 = null;
                gZIPInputStream2 = null;
                byteArrayInputStream2 = null;
                byteArrayOutputStream2.close();
                gZIPInputStream2.close();
                byteArrayInputStream2.close();
                throw th;
            }
        }
        return a2;
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) throws IOException {
        int i4;
        if (bArr == null) {
            throw new NullPointerException("Cannot decode null source array.");
        } else if (i < 0 || i + i2 > bArr.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and process %d bytes.", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
        } else if (i2 == 0) {
            return new byte[0];
        } else {
            if (i2 < 4) {
                throw new IllegalArgumentException("Base64-encoded string must have at least four characters, but length specified was " + i2);
            }
            byte[] c2 = c(i3);
            byte[] bArr2 = new byte[((i2 * 3) / 4)];
            byte[] bArr3 = new byte[4];
            int i5 = i;
            int i6 = 0;
            int i7 = 0;
            while (true) {
                if (i5 >= i + i2) {
                    i4 = i7;
                    break;
                }
                byte b2 = c2[bArr[i5] & Constants.UNKNOWN];
                if (b2 >= -5) {
                    if (b2 >= -1) {
                        int i8 = i6 + 1;
                        bArr3[i6] = bArr[i5];
                        if (i8 > 3) {
                            int b3 = b(bArr3, 0, bArr2, i7, i3) + i7;
                            if (bArr[i5] == 61) {
                                i4 = b3;
                                break;
                            }
                            i7 = b3;
                            i6 = 0;
                        } else {
                            i6 = i8;
                        }
                    }
                    i5++;
                } else {
                    throw new IOException(String.format("Bad Base64 input character decimal %d in array position %d", Integer.valueOf(bArr[i5] & Constants.UNKNOWN), Integer.valueOf(i5)));
                }
            }
            byte[] bArr4 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr4, 0, i4);
            return bArr4;
        }
    }

    private static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        byte[] b2 = b(i4);
        int i5 = (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0) | (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 2 ? (bArr[i + 2] << 24) >>> 24 : 0);
        switch (i2) {
            case 1:
                bArr2[i3] = b2[i5 >>> 18];
                bArr2[i3 + 1] = b2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = 61;
                bArr2[i3 + 3] = 61;
                return bArr2;
            case 2:
                bArr2[i3] = b2[i5 >>> 18];
                bArr2[i3 + 1] = b2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = b2[(i5 >>> 6) & 63];
                bArr2[i3 + 3] = 61;
                return bArr2;
            case 3:
                bArr2[i3] = b2[i5 >>> 18];
                bArr2[i3 + 1] = b2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = b2[(i5 >>> 6) & 63];
                bArr2[i3 + 3] = b2[i5 & 63];
                return bArr2;
            default:
                return bArr2;
        }
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        if (bArr == null) {
            throw new NullPointerException("Source array was null.");
        } else if (bArr2 == null) {
            throw new NullPointerException("Destination array was null.");
        } else if (i < 0 || i + 3 >= bArr.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and still process four bytes.", Integer.valueOf(bArr.length), Integer.valueOf(i)));
        } else if (i2 < 0 || i2 + 2 >= bArr2.length) {
            throw new IllegalArgumentException(String.format("Destination array with length %d cannot have offset of %d and still store three bytes.", Integer.valueOf(bArr2.length), Integer.valueOf(i2)));
        } else {
            byte[] c2 = c(i3);
            if (bArr[i + 2] == 61) {
                bArr2[i2] = (byte) ((((c2[bArr[i + 1]] & Constants.UNKNOWN) << 12) | ((c2[bArr[i]] & Constants.UNKNOWN) << 18)) >>> 16);
                return 1;
            } else if (bArr[i + 3] == 61) {
                int i4 = ((c2[bArr[i + 2]] & Constants.UNKNOWN) << 6) | ((c2[bArr[i]] & Constants.UNKNOWN) << 18) | ((c2[bArr[i + 1]] & Constants.UNKNOWN) << 12);
                bArr2[i2] = (byte) (i4 >>> 16);
                bArr2[i2 + 1] = (byte) (i4 >>> 8);
                return 2;
            } else {
                byte b2 = (c2[bArr[i + 3]] & Constants.UNKNOWN) | ((c2[bArr[i]] & Constants.UNKNOWN) << 18) | ((c2[bArr[i + 1]] & Constants.UNKNOWN) << 12) | ((c2[bArr[i + 2]] & Constants.UNKNOWN) << 6);
                bArr2[i2] = (byte) (b2 >> 16);
                bArr2[i2 + 1] = (byte) (b2 >> 8);
                bArr2[i2 + 2] = (byte) b2;
                return 3;
            }
        }
    }

    public static String b(byte[] bArr, int i, int i2, int i3) throws IOException {
        byte[] c2 = c(bArr, i, i2, i3);
        try {
            return new String(c2, "US-ASCII");
        } catch (UnsupportedEncodingException e2) {
            return new String(c2);
        }
    }

    private static final byte[] b(int i) {
        return (i & 16) == 16 ? f : (i & 32) == 32 ? b : d;
    }

    /* access modifiers changed from: private */
    public static byte[] b(byte[] bArr, byte[] bArr2, int i, int i2) {
        a(bArr2, 0, i, bArr, 0, i2);
        return bArr;
    }

    /* access modifiers changed from: private */
    public static final byte[] c(int i) {
        return (i & 16) == 16 ? g : (i & 32) == 32 ? c : e;
    }

    public static byte[] c(byte[] bArr, int i, int i2, int i3) throws IOException {
        int i4;
        int i5;
        a aVar;
        GZIPOutputStream gZIPOutputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        a aVar2;
        GZIPOutputStream gZIPOutputStream2;
        if (bArr == null) {
            throw new NullPointerException("Cannot serialize a null array.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Cannot have negative offset: " + i);
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Cannot have length offset: " + i2);
        } else if (i + i2 > bArr.length) {
            throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(bArr.length)));
        } else if ((i3 & 2) != 0) {
            try {
                ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                try {
                    aVar2 = new a(byteArrayOutputStream2, i3 | 1);
                    try {
                        gZIPOutputStream2 = new GZIPOutputStream(aVar2);
                    } catch (IOException e2) {
                        e = e2;
                        aVar = aVar2;
                        gZIPOutputStream = null;
                        byteArrayOutputStream = byteArrayOutputStream2;
                        try {
                            throw e;
                        } catch (Throwable th) {
                            th = th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        aVar = aVar2;
                        gZIPOutputStream = null;
                        byteArrayOutputStream = byteArrayOutputStream2;
                        try {
                            gZIPOutputStream.close();
                        } catch (Exception e3) {
                        }
                        try {
                            aVar.close();
                        } catch (Exception e4) {
                        }
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e5) {
                        }
                        throw th;
                    }
                } catch (IOException e6) {
                    e = e6;
                    aVar = null;
                    gZIPOutputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    throw e;
                } catch (Throwable th3) {
                    th = th3;
                    aVar = null;
                    gZIPOutputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
                try {
                    gZIPOutputStream2.write(bArr, i, i2);
                    gZIPOutputStream2.close();
                    try {
                        gZIPOutputStream2.close();
                    } catch (Exception e7) {
                    }
                    try {
                        aVar2.close();
                    } catch (Exception e8) {
                    }
                    try {
                        byteArrayOutputStream2.close();
                    } catch (Exception e9) {
                    }
                    return byteArrayOutputStream2.toByteArray();
                } catch (IOException e10) {
                    e = e10;
                    aVar = aVar2;
                    gZIPOutputStream = gZIPOutputStream2;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    throw e;
                } catch (Throwable th4) {
                    th = th4;
                    aVar = aVar2;
                    gZIPOutputStream = gZIPOutputStream2;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e11) {
                e = e11;
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                throw e;
            } catch (Throwable th5) {
                th = th5;
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                gZIPOutputStream.close();
                aVar.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = (i3 & 8) != 0;
            int i6 = ((i2 / 3) * 4) + (i2 % 3 > 0 ? 4 : 0);
            if (z) {
                i6 += i6 / 76;
            }
            byte[] bArr2 = new byte[i6];
            int i7 = i2 - 2;
            int i8 = 0;
            int i9 = 0;
            int i10 = 0;
            while (i10 < i7) {
                a(bArr, i10 + i, 3, bArr2, i9, i3);
                int i11 = i8 + 4;
                if (!z || i11 < 76) {
                    i5 = i9;
                } else {
                    bArr2[i9 + 4] = 10;
                    i5 = i9 + 1;
                    i11 = 0;
                }
                i8 = i11;
                i9 = i5 + 4;
                i10 += 3;
            }
            if (i10 < i2) {
                a(bArr, i10 + i, i2 - i10, bArr2, i9, i3);
                i4 = i9 + 4;
            } else {
                i4 = i9;
            }
            if (i4 > bArr2.length - 1) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr3, 0, i4);
            return bArr3;
        }
    }
}
