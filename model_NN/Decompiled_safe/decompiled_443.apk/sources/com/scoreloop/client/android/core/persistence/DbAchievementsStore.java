package com.scoreloop.client.android.core.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import au.com.xandar.jumblee.common.AppConstants;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.AchievementsStore;
import com.scoreloop.client.android.core.model.Award;
import com.scoreloop.client.android.core.model.AwardList;
import com.scoreloop.client.android.core.util.CryptoUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class DbAchievementsStore implements AchievementsStore {
    private final CryptoUtil a;
    private final a b;

    private class a extends SQLiteOpenHelper {
        private final Context b;
        private final AwardList c;
        private final String d;
        private a e;

        private a(Context context, AwardList awardList, String str) {
            super(context, "achievements.db", (SQLiteDatabase.CursorFactory) null, 1);
            this.b = context;
            this.c = awardList;
            this.d = str;
        }

        private void a(SQLiteDatabase sQLiteDatabase) {
            this.e = new a(this.b, this.d);
            for (Award a2 : this.c.getAwards()) {
                for (Achievement a3 : this.e.a(a2)) {
                    DbAchievementsStore.this.a(a3, sQLiteDatabase);
                }
            }
            DbAchievementsStore.this.a(sQLiteDatabase, this.e.a());
        }

        private void b(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE achievements (local_id TEXT PRIMARY KEY,achievement_id TEXT,award_id TEXT NOT NULL,date INTEGER,needs_submit INTEGER NOT NULL,data BLOB NOT NULL);");
            sQLiteDatabase.execSQL("CREATE TABLE achievements_config (key STRING PRIMARY KEY,value TEXT NOT NULL);");
        }

        private void c(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS achievements;");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS achievements_config;");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            b(sQLiteDatabase);
            a(sQLiteDatabase);
        }

        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            super.onOpen(sQLiteDatabase);
            if (this.e != null) {
                this.e.c();
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            c(sQLiteDatabase);
            b(sQLiteDatabase);
        }
    }

    public DbAchievementsStore(Context context, AwardList awardList, String str) {
        this.b = new a(context, awardList, str);
        this.a = new CryptoUtil(context.getPackageName(), "achievements");
    }

    private Achievement a(String str, Award award) {
        try {
            return new Achievement(award, new JSONObject(str), this);
        } catch (JSONException e) {
            return null;
        }
    }

    private Boolean a(int i) {
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: private */
    public void a(SQLiteDatabase sQLiteDatabase, boolean z) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", "did_query_server");
        contentValues.put("value", Boolean.valueOf(z).toString());
        if (sQLiteDatabase.update("achievements_config", contentValues, "key = 'did_query_server'", null) == 0 && sQLiteDatabase.insert("achievements_config", null, contentValues) == -1) {
            throw new IllegalStateException();
        }
    }

    /* access modifiers changed from: private */
    public void a(Achievement achievement, SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase.insert("achievements", null, b(achievement)) == -1) {
            throw new IllegalStateException();
        }
    }

    private boolean a(Achievement achievement, Cursor cursor) {
        boolean a2 = a(achievement.getIdentifier(), cursor.getString(cursor.getColumnIndex("achievement_id")));
        boolean a3 = a(achievement.e(), cursor.getString(cursor.getColumnIndex("local_id")));
        boolean a4 = a(achievement.getAward().getIdentifier(), cursor.getString(cursor.getColumnIndex("award_id")));
        boolean a5 = a(Boolean.valueOf(achievement.needsSubmit()), a(cursor.getInt(cursor.getColumnIndex("needs_submit"))));
        int columnIndex = cursor.getColumnIndex("date");
        return a2 && a3 && a4 && a5 && a(achievement.getDate(), cursor.isNull(columnIndex) ? null : new Date(cursor.getLong(columnIndex)));
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private <T> boolean a(T r2, T r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            if (r3 != 0) goto L_0x0006
            r0 = 1
        L_0x0005:
            return r0
        L_0x0006:
            r0 = 0
            goto L_0x0005
        L_0x0008:
            boolean r0 = r2.equals(r3)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(java.lang.Object, java.lang.Object):boolean");
    }

    private boolean a(Date date, Date date2) {
        return date == null ? date2 == null : date2 != null && Math.abs(date.getTime() - date2.getTime()) < AppConstants.NR_MILLIS_IN_A_DAY;
    }

    private ContentValues b(Achievement achievement) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("achievement_id", achievement.getIdentifier());
        contentValues.put("local_id", achievement.e());
        contentValues.put("award_id", achievement.getAward().getIdentifier());
        contentValues.put("needs_submit", Boolean.valueOf(achievement.needsSubmit()));
        contentValues.put("date", c(achievement));
        contentValues.put("data", this.a.b(d(achievement)));
        return contentValues;
    }

    private Long c(Achievement achievement) {
        if (achievement.getDate() != null) {
            return Long.valueOf(achievement.getDate().getTime());
        }
        return null;
    }

    private String d(Achievement achievement) {
        try {
            return achievement.a(true).toString();
        } catch (JSONException e) {
            throw new IllegalStateException();
        }
    }

    public synchronized List<Achievement> a(Award award) {
        ArrayList arrayList;
        Cursor cursor;
        arrayList = new ArrayList();
        try {
            Cursor query = this.b.getReadableDatabase().query("achievements", null, "award_id = ?", new String[]{award.getIdentifier()}, null, null, null);
            try {
                query.moveToFirst();
                while (!query.isAfterLast()) {
                    Achievement a2 = a(this.a.a(query.getBlob(query.getColumnIndex("data"))), award);
                    if (a2 != null && a(a2, query)) {
                        arrayList.add(a2);
                    }
                    query.moveToNext();
                }
                if (query != null) {
                    query.close();
                }
                this.b.close();
            } catch (Throwable th) {
                Throwable th2 = th;
                cursor = query;
                th = th2;
                if (cursor != null) {
                    cursor.close();
                }
                this.b.close();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
        return arrayList;
    }

    public synchronized void a(Achievement achievement) {
        try {
            SQLiteDatabase writableDatabase = this.b.getWritableDatabase();
            if (writableDatabase.update("achievements", b(achievement), "local_id = ?", new String[]{achievement.e()}) == 0) {
                a(achievement, writableDatabase);
            }
            this.b.close();
        } catch (Exception e) {
            throw new IllegalStateException();
        } catch (Throwable th) {
            this.b.close();
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0057 A[SYNTHETIC, Splitter:B:28:0x0057] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean a() {
        /*
            r11 = this;
            r9 = 0
            r8 = 0
            monitor-enter(r11)
            com.scoreloop.client.android.core.persistence.DbAchievementsStore$a r0 = r11.b     // Catch:{ Exception -> 0x0049, all -> 0x0063 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0049, all -> 0x0063 }
            java.lang.String r1 = "achievements_config"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0049, all -> 0x0063 }
            r3 = 0
            java.lang.String r4 = "value"
            r2[r3] = r4     // Catch:{ Exception -> 0x0049, all -> 0x0063 }
            java.lang.String r3 = "key = 'did_query_server'"
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0049, all -> 0x0063 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0066 }
            if (r1 == 0) goto L_0x003d
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0066 }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0066 }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0066 }
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ all -> 0x0060 }
        L_0x0035:
            com.scoreloop.client.android.core.persistence.DbAchievementsStore$a r0 = r11.b     // Catch:{ all -> 0x0060 }
            r0.close()     // Catch:{ all -> 0x0060 }
            r0 = r1
        L_0x003b:
            monitor-exit(r11)
            return r0
        L_0x003d:
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ all -> 0x0060 }
        L_0x0042:
            com.scoreloop.client.android.core.persistence.DbAchievementsStore$a r0 = r11.b     // Catch:{ all -> 0x0060 }
            r0.close()     // Catch:{ all -> 0x0060 }
            r0 = r9
            goto L_0x003b
        L_0x0049:
            r0 = move-exception
            r0 = r8
        L_0x004b:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0051 }
            r1.<init>()     // Catch:{ all -> 0x0051 }
            throw r1     // Catch:{ all -> 0x0051 }
        L_0x0051:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0055:
            if (r1 == 0) goto L_0x005a
            r1.close()     // Catch:{ all -> 0x0060 }
        L_0x005a:
            com.scoreloop.client.android.core.persistence.DbAchievementsStore$a r1 = r11.b     // Catch:{ all -> 0x0060 }
            r1.close()     // Catch:{ all -> 0x0060 }
            throw r0     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x0063:
            r0 = move-exception
            r1 = r8
            goto L_0x0055
        L_0x0066:
            r1 = move-exception
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.core.persistence.DbAchievementsStore.a():boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(android.database.sqlite.SQLiteDatabase, boolean):void
     arg types: [android.database.sqlite.SQLiteDatabase, int]
     candidates:
      com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(java.lang.String, com.scoreloop.client.android.core.model.Award):com.scoreloop.client.android.core.model.Achievement
      com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(com.scoreloop.client.android.core.model.Achievement, android.database.sqlite.SQLiteDatabase):void
      com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(com.scoreloop.client.android.core.model.Achievement, android.database.Cursor):boolean
      com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(java.lang.Object, java.lang.Object):boolean
      com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(java.util.Date, java.util.Date):boolean
      com.scoreloop.client.android.core.persistence.DbAchievementsStore.a(android.database.sqlite.SQLiteDatabase, boolean):void */
    public synchronized void b() {
        try {
            a(this.b.getWritableDatabase(), true);
            this.b.close();
        } catch (Exception e) {
            throw new IllegalStateException();
        } catch (Throwable th) {
            this.b.close();
            throw th;
        }
    }
}
