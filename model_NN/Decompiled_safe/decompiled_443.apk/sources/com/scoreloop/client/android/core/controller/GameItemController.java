package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.GameItem;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import org.json.JSONObject;

public class GameItemController extends RequestController {
    private GameItem c;

    public GameItemController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() != 200 || response.a() == null) {
            throw new Exception("Request failed");
        }
        this.c.a(((JSONObject) response.a()).getJSONObject(GameItem.a));
        return true;
    }
}
