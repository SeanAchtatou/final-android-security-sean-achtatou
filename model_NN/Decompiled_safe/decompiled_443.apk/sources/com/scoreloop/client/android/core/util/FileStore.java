package com.scoreloop.client.android.core.util;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class FileStore<T> {
    private final Context a;
    private CryptoUtil b;

    public FileStore() {
        this.a = null;
    }

    public FileStore(Context context) {
        this.a = context;
    }

    private boolean a(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            return true;
        }
        File parentFile = file.getParentFile();
        if (parentFile.exists() || parentFile.mkdirs()) {
            return file.createNewFile();
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0035, code lost:
        if (r0 != null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003d, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034 A[ExcHandler: IOException (e java.io.IOException), PHI: r0 
      PHI: (r0v3 java.io.FileOutputStream) = (r0v0 java.io.FileOutputStream), (r0v0 java.io.FileOutputStream), (r0v0 java.io.FileOutputStream), (r0v0 java.io.FileOutputStream), (r0v0 java.io.FileOutputStream), (r0v9 java.io.FileOutputStream), (r0v9 java.io.FileOutputStream) binds: [B:1:0x0003, B:11:0x001c, B:12:?, B:18:0x002d, B:19:?, B:5:0x0012, B:6:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0042 A[SYNTHETIC, Splitter:B:29:0x0042] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(byte[] r8) {
        /*
            r7 = this;
            r5 = 1
            r4 = 0
            r0 = 0
            android.content.Context r1 = r7.a     // Catch:{ IOException -> 0x0034, all -> 0x003c }
            if (r1 == 0) goto L_0x001c
            android.content.Context r1 = r7.a     // Catch:{ IOException -> 0x0034, all -> 0x003c }
            java.lang.String r2 = r7.a()     // Catch:{ IOException -> 0x0034, all -> 0x003c }
            r3 = 1
            java.io.FileOutputStream r0 = r1.openFileOutput(r2, r3)     // Catch:{ IOException -> 0x0034, all -> 0x003c }
        L_0x0012:
            r0.write(r8)     // Catch:{ IOException -> 0x0034, all -> 0x004e }
            if (r0 == 0) goto L_0x001a
            r0.close()     // Catch:{ IOException -> 0x0048 }
        L_0x001a:
            r0 = r5
        L_0x001b:
            return r0
        L_0x001c:
            java.io.File r1 = r7.d()     // Catch:{ IOException -> 0x0034, all -> 0x003c }
            boolean r2 = r7.a(r1)     // Catch:{ IOException -> 0x0034, all -> 0x003c }
            if (r2 != 0) goto L_0x002d
            if (r0 == 0) goto L_0x002b
            r0.close()     // Catch:{ IOException -> 0x0046 }
        L_0x002b:
            r0 = r4
            goto L_0x001b
        L_0x002d:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0034, all -> 0x003c }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0034, all -> 0x003c }
            r0 = r2
            goto L_0x0012
        L_0x0034:
            r1 = move-exception
            if (r0 == 0) goto L_0x003a
            r0.close()     // Catch:{ IOException -> 0x004a }
        L_0x003a:
            r0 = r4
            goto L_0x001b
        L_0x003c:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x0045:
            throw r0
        L_0x0046:
            r0 = move-exception
            goto L_0x002b
        L_0x0048:
            r0 = move-exception
            goto L_0x001a
        L_0x004a:
            r0 = move-exception
            goto L_0x003a
        L_0x004c:
            r1 = move-exception
            goto L_0x0045
        L_0x004e:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.core.util.FileStore.a(byte[]):boolean");
    }

    private CryptoUtil c() {
        if (this.b == null) {
            this.b = new CryptoUtil("shared", this.a != null ? this.a.getPackageName() : "Scoreloop");
        }
        return this.b;
    }

    private File d() {
        return this.a != null ? new File(this.a.getFilesDir(), a()) : new File(new File(Environment.getExternalStorageDirectory(), "Scoreloop"), a());
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0052 A[SYNTHETIC, Splitter:B:28:0x0052] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] e() {
        /*
            r7 = this;
            r5 = 0
            android.content.Context r0 = r7.a     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            if (r0 == 0) goto L_0x0031
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            android.content.Context r1 = r7.a     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            java.lang.String r2 = r7.a()     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            java.io.FileInputStream r1 = r1.openFileInput(r2)     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0040, all -> 0x004e }
        L_0x0014:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0029, all -> 0x005c }
            r1.<init>()     // Catch:{ IOException -> 0x0029, all -> 0x005c }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x0029, all -> 0x005c }
        L_0x001d:
            int r3 = r0.read(r2)     // Catch:{ IOException -> 0x0029, all -> 0x005c }
            r4 = -1
            if (r3 == r4) goto L_0x0043
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ IOException -> 0x0029, all -> 0x005c }
            goto L_0x001d
        L_0x0029:
            r1 = move-exception
        L_0x002a:
            if (r0 == 0) goto L_0x002f
            r0.close()     // Catch:{ IOException -> 0x0058 }
        L_0x002f:
            r0 = r5
        L_0x0030:
            return r0
        L_0x0031:
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            java.io.File r2 = r7.d()     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0040, all -> 0x004e }
            goto L_0x0014
        L_0x0040:
            r0 = move-exception
            r0 = r5
            goto L_0x002a
        L_0x0043:
            byte[] r1 = r1.toByteArray()     // Catch:{ IOException -> 0x0029, all -> 0x005c }
            if (r0 == 0) goto L_0x004c
            r0.close()     // Catch:{ IOException -> 0x0056 }
        L_0x004c:
            r0 = r1
            goto L_0x0030
        L_0x004e:
            r0 = move-exception
            r1 = r5
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ IOException -> 0x005a }
        L_0x0055:
            throw r0
        L_0x0056:
            r0 = move-exception
            goto L_0x004c
        L_0x0058:
            r0 = move-exception
            goto L_0x002f
        L_0x005a:
            r1 = move-exception
            goto L_0x0055
        L_0x005c:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.core.util.FileStore.e():byte[]");
    }

    /* access modifiers changed from: protected */
    public abstract String a();

    /* access modifiers changed from: protected */
    public abstract JSONObject a(Object obj) throws JSONException;

    public T b() {
        if (!d().exists()) {
            return null;
        }
        byte[] e = e();
        if (e == null) {
            return null;
        }
        try {
            return b(new JSONObject(c().a(e)));
        } catch (JSONException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public abstract T b(JSONObject jSONObject) throws JSONException;

    public boolean b(Object obj) {
        try {
            return a(c().b(a(obj).toString()));
        } catch (JSONException e) {
            return false;
        }
    }
}
