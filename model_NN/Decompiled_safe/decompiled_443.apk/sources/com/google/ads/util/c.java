package com.google.ads.util;

import android.content.Context;
import android.util.DisplayMetrics;
import au.com.xandar.jumblee.common.AppConstants;

public final class c {
    private c() {
    }

    private static int a(Context context, float f, int i) {
        return (context.getApplicationInfo().flags & AppConstants.HTTP_CONNECTION_SIZE_BYTES) != 0 ? (int) (((float) i) / f) : i;
    }

    public static int a(Context context, DisplayMetrics displayMetrics) {
        return a(context, displayMetrics.density, displayMetrics.heightPixels);
    }

    public static int b(Context context, DisplayMetrics displayMetrics) {
        return a(context, displayMetrics.density, displayMetrics.widthPixels);
    }
}
