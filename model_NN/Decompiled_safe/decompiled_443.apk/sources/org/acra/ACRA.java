package org.acra;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.EmailIntentSender;
import org.acra.sender.GoogleFormSender;
import org.acra.sender.HttpPostSender;
import org.acra.util.PackageManagerWrapper;

public class ACRA {
    public static final ReportField[] DEFAULT_MAIL_REPORT_FIELDS = {ReportField.USER_COMMENT, ReportField.ANDROID_VERSION, ReportField.APP_VERSION_NAME, ReportField.BRAND, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE};
    public static final ReportField[] DEFAULT_REPORT_FIELDS = {ReportField.REPORT_ID, ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.PACKAGE_NAME, ReportField.FILE_PATH, ReportField.PHONE_MODEL, ReportField.BRAND, ReportField.PRODUCT, ReportField.ANDROID_VERSION, ReportField.BUILD, ReportField.TOTAL_MEM_SIZE, ReportField.AVAILABLE_MEM_SIZE, ReportField.CUSTOM_DATA, ReportField.IS_SILENT, ReportField.STACK_TRACE, ReportField.INITIAL_CONFIGURATION, ReportField.CRASH_CONFIGURATION, ReportField.DISPLAY, ReportField.USER_COMMENT, ReportField.USER_EMAIL, ReportField.USER_APP_START_DATE, ReportField.USER_CRASH_DATE, ReportField.DUMPSYS_MEMINFO, ReportField.DROPBOX, ReportField.LOGCAT, ReportField.EVENTSLOG, ReportField.RADIOLOG, ReportField.DEVICE_ID, ReportField.INSTALLATION_ID, ReportField.DEVICE_FEATURES, ReportField.ENVIRONMENT, ReportField.SHARED_PREFERENCES, ReportField.SETTINGS_SYSTEM, ReportField.SETTINGS_SECURE};
    public static final boolean DEV_LOGGING = false;
    public static final String LOG_TAG = ACRA.class.getSimpleName();
    public static final String NULL_VALUE = "ACRA-NULL-STRING";
    public static final String PREF_ALWAYS_ACCEPT = "acra.alwaysaccept";
    public static final String PREF_DISABLE_ACRA = "acra.disable";
    public static final String PREF_ENABLE_ACRA = "acra.enable";
    public static final String PREF_ENABLE_DEVICE_ID = "acra.deviceid.enable";
    public static final String PREF_ENABLE_SYSTEM_LOGS = "acra.syslog.enable";
    public static final String PREF_USER_EMAIL_ADDRESS = "acra.user.email";
    private static ErrorReporter errorReporterSingleton;
    private static Application mApplication;
    private static SharedPreferences.OnSharedPreferenceChangeListener mPrefListener;
    private static ReportsCrashes mReportsCrashes;

    public static void init(Application app) {
        if (mApplication != null) {
            throw new IllegalStateException("ACRA#init called more than once");
        }
        mApplication = app;
        mReportsCrashes = (ReportsCrashes) mApplication.getClass().getAnnotation(ReportsCrashes.class);
        if (mReportsCrashes == null) {
            Log.e(LOG_TAG, "ACRA#init called but no ReportsCrashes annotation on Application " + mApplication.getPackageName());
            return;
        }
        SharedPreferences prefs = getACRASharedPreferences();
        Log.d(LOG_TAG, "Set OnSharedPreferenceChangeListener.");
        try {
            checkCrashResources();
            Log.d(LOG_TAG, "ACRA is enabled for " + mApplication.getPackageName() + ", intializing...");
            ErrorReporter errorReporter = new ErrorReporter(mApplication.getApplicationContext(), prefs, !shouldDisableACRA(prefs));
            addReportSenders(errorReporter);
            errorReporterSingleton = errorReporter;
        } catch (ACRAConfigurationException e) {
            Log.w(LOG_TAG, "Error : ", e);
        }
        mPrefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (ACRA.PREF_DISABLE_ACRA.equals(key) || ACRA.PREF_ENABLE_ACRA.equals(key)) {
                    ACRA.getErrorReporter().setEnabled(!ACRA.shouldDisableACRA(sharedPreferences));
                }
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(mPrefListener);
    }

    public static ErrorReporter getErrorReporter() {
        if (errorReporterSingleton != null) {
            return errorReporterSingleton;
        }
        throw new IllegalStateException("Cannot access ErrorReporter before ACRA#init");
    }

    private static void addReportSenders(ErrorReporter errorReporter) {
        if (!"".equals(mReportsCrashes.mailTo())) {
            Log.w(LOG_TAG, mApplication.getPackageName() + " reports will be sent by email (if accepted by user).");
            errorReporter.addReportSender(new EmailIntentSender(mApplication));
        } else if (!new PackageManagerWrapper(mApplication).hasPermission("android.permission.INTERNET")) {
            Log.e(LOG_TAG, mApplication.getPackageName() + " should be granted permission " + "android.permission.INTERNET" + " if you want your crash reports to be sent. If you don't want to add this permission to your application you can also enable sending reports by email. If this is your will then provide your email address in @ReportsCrashes(mailTo=\"your.account@domain.com\"");
        } else if (mReportsCrashes.formUri() != null && !"".equals(mReportsCrashes.formUri())) {
            errorReporter.addReportSender(new HttpPostSender(mReportsCrashes.formUri(), null));
        } else if (mReportsCrashes.formKey() != null && !"".equals(mReportsCrashes.formKey().trim())) {
            errorReporter.addReportSender(new GoogleFormSender(mReportsCrashes.formKey()));
        }
    }

    /* access modifiers changed from: private */
    public static boolean shouldDisableACRA(SharedPreferences prefs) {
        try {
            return prefs.getBoolean(PREF_DISABLE_ACRA, !prefs.getBoolean(PREF_ENABLE_ACRA, true));
        } catch (Exception e) {
            return false;
        }
    }

    private static void checkCrashResources() throws ACRAConfigurationException {
        switch (mReportsCrashes.mode()) {
            case TOAST:
                if (mReportsCrashes.resToastText() == 0) {
                    throw new ACRAConfigurationException("TOAST mode: you have to define the resToastText parameter in your application @ReportsCrashes() annotation.");
                }
                return;
            case NOTIFICATION:
                if (mReportsCrashes.resNotifTickerText() == 0 || mReportsCrashes.resNotifTitle() == 0 || mReportsCrashes.resNotifText() == 0 || mReportsCrashes.resDialogText() == 0) {
                    throw new ACRAConfigurationException("NOTIFICATION mode: you have to define at least the resNotifTickerText, resNotifTitle, resNotifText, resDialogText parameters in your application @ReportsCrashes() annotation.");
                }
                return;
            default:
                return;
        }
    }

    public static SharedPreferences getACRASharedPreferences() {
        if (!"".equals(mReportsCrashes.sharedPreferencesName())) {
            Log.d(LOG_TAG, "Retrieve SharedPreferences " + mReportsCrashes.sharedPreferencesName());
            return mApplication.getSharedPreferences(mReportsCrashes.sharedPreferencesName(), mReportsCrashes.sharedPreferencesMode());
        }
        Log.d(LOG_TAG, "Retrieve application default SharedPreferences.");
        return PreferenceManager.getDefaultSharedPreferences(mApplication);
    }

    public static ReportsCrashes getConfig() {
        return mReportsCrashes;
    }
}
