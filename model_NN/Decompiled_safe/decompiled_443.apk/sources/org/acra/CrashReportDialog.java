package org.acra;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.IOException;
import org.acra.collector.CrashReportData;
import org.acra.util.ToastSender;

final class CrashReportDialog extends Activity {
    String mReportFileName;
    /* access modifiers changed from: private */
    public SharedPreferences prefs;
    /* access modifiers changed from: private */
    public EditText userComment;
    /* access modifiers changed from: private */
    public EditText userEmail;

    CrashReportDialog() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mReportFileName = getIntent().getStringExtra("REPORT_FILE_NAME");
        Log.d(ACRA.LOG_TAG, "Opening CrashReportDialog for " + this.mReportFileName);
        if (this.mReportFileName == null) {
            finish();
        }
        requestWindowFeature(3);
        LinearLayout root = new LinearLayout(this);
        root.setOrientation(1);
        root.setPadding(10, 10, 10, 10);
        root.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ScrollView scroll = new ScrollView(this);
        root.addView(scroll, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        TextView text = new TextView(this);
        text.setText(getText(ACRA.getConfig().resDialogText()));
        scroll.addView(text, -1, -1);
        int commentPromptId = ACRA.getConfig().resDialogCommentPrompt();
        if (commentPromptId != 0) {
            TextView label = new TextView(this);
            label.setText(getText(commentPromptId));
            label.setPadding(label.getPaddingLeft(), 10, label.getPaddingRight(), label.getPaddingBottom());
            root.addView(label, new LinearLayout.LayoutParams(-1, -2));
            this.userComment = new EditText(this);
            this.userComment.setLines(2);
            root.addView(this.userComment, new LinearLayout.LayoutParams(-1, -2));
        }
        int emailPromptId = ACRA.getConfig().resDialogEmailPrompt();
        if (emailPromptId != 0) {
            TextView label2 = new TextView(this);
            label2.setText(getText(emailPromptId));
            label2.setPadding(label2.getPaddingLeft(), 10, label2.getPaddingRight(), label2.getPaddingBottom());
            root.addView(label2, new LinearLayout.LayoutParams(-1, -2));
            this.userEmail = new EditText(this);
            this.userEmail.setSingleLine();
            this.userEmail.setInputType(33);
            this.prefs = getSharedPreferences(ACRA.getConfig().sharedPreferencesName(), ACRA.getConfig().sharedPreferencesMode());
            this.userEmail.setText(this.prefs.getString(ACRA.PREF_USER_EMAIL_ADDRESS, ""));
            root.addView(this.userEmail, new LinearLayout.LayoutParams(-1, -2));
        }
        LinearLayout buttons = new LinearLayout(this);
        buttons.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        buttons.setPadding(buttons.getPaddingLeft(), 10, buttons.getPaddingRight(), buttons.getPaddingBottom());
        Button yes = new Button(this);
        yes.setText(17039379);
        yes.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
             arg types: [org.acra.ReportField, java.lang.String]
             candidates:
              ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
              ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
              ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
              ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
            public void onClick(View v) {
                String usrEmail;
                String comment = CrashReportDialog.this.userComment != null ? CrashReportDialog.this.userComment.getText().toString() : null;
                if (CrashReportDialog.this.prefs == null || CrashReportDialog.this.userEmail == null) {
                    usrEmail = "";
                } else {
                    usrEmail = CrashReportDialog.this.userEmail.getText().toString();
                    SharedPreferences.Editor prefEditor = CrashReportDialog.this.prefs.edit();
                    prefEditor.putString(ACRA.PREF_USER_EMAIL_ADDRESS, usrEmail);
                    prefEditor.commit();
                }
                CrashReportPersister persister = new CrashReportPersister(CrashReportDialog.this.getApplicationContext());
                try {
                    Log.d(ACRA.LOG_TAG, "Add user comment to " + CrashReportDialog.this.mReportFileName);
                    CrashReportData crashData = persister.load(CrashReportDialog.this.mReportFileName);
                    crashData.put((Enum) ReportField.USER_COMMENT, (Object) comment);
                    crashData.put((Enum) ReportField.USER_EMAIL, (Object) usrEmail);
                    persister.store(crashData, CrashReportDialog.this.mReportFileName);
                } catch (IOException e) {
                    Log.w(ACRA.LOG_TAG, "User comment not added: ", e);
                }
                Log.v(ACRA.LOG_TAG, "About to start SenderWorker from CrashReportDialog");
                ACRA.getErrorReporter().startSendingReports(false, true);
                int toastId = ACRA.getConfig().resDialogOkToast();
                if (toastId != 0) {
                    ToastSender.sendToast(CrashReportDialog.this.getApplicationContext(), toastId, 1);
                }
                CrashReportDialog.this.finish();
            }
        });
        buttons.addView(yes, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        Button no = new Button(this);
        no.setText(17039369);
        no.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ACRA.getErrorReporter().deletePendingReports();
                CrashReportDialog.this.finish();
            }
        });
        buttons.addView(no, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        root.addView(buttons, new LinearLayout.LayoutParams(-1, -2));
        setContentView(root);
        int resTitle = ACRA.getConfig().resDialogTitle();
        if (resTitle != 0) {
            setTitle(resTitle);
        }
        getWindow().setFeatureDrawableResource(3, ACRA.getConfig().resDialogIcon());
        cancelNotification();
    }

    /* access modifiers changed from: protected */
    public void cancelNotification() {
        ((NotificationManager) getSystemService("notification")).cancel(666);
    }
}
