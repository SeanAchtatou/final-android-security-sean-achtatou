package org.acra;

import android.content.Context;
import au.com.xandar.android.PlatformConstants;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;
import org.acra.collector.CrashReportData;

final class CrashReportPersister {
    private static final int CONTINUE = 3;
    private static final int IGNORE = 5;
    private static final int KEY_DONE = 4;
    private static final String LINE_SEPARATOR = "\n";
    private static final int NONE = 0;
    private static final int SLASH = 1;
    private static final int UNICODE = 2;
    private final Context context;

    CrashReportPersister(Context context2) {
        this.context = context2;
    }

    public CrashReportData load(String fileName) throws IOException {
        CrashReportData load;
        FileInputStream in = this.context.openFileInput(fileName);
        if (in == null) {
            throw new IllegalArgumentException("Invalid crash report fileName : " + fileName);
        }
        try {
            BufferedInputStream bis = new BufferedInputStream(in);
            bis.mark(Integer.MAX_VALUE);
            boolean isEbcdic = isEbcdic(bis);
            bis.reset();
            if (!isEbcdic) {
                load = load(new InputStreamReader(bis, "ISO8859-1"));
            } else {
                load = load(new InputStreamReader(bis));
                in.close();
            }
            return load;
        } finally {
            in.close();
        }
    }

    public void store(CrashReportData crashData, String fileName) throws IOException {
        OutputStream out = this.context.openFileOutput(fileName, 0);
        try {
            StringBuilder buffer = new StringBuilder(200);
            OutputStreamWriter writer = new OutputStreamWriter(out, "ISO8859_1");
            for (Map.Entry<ReportField, String> entry : crashData.entrySet()) {
                dumpString(buffer, ((ReportField) entry.getKey()).toString(), true);
                buffer.append('=');
                dumpString(buffer, (String) entry.getValue(), false);
                buffer.append(LINE_SEPARATOR);
                writer.write(buffer.toString());
                buffer.setLength(0);
            }
            writer.flush();
        } finally {
            out.close();
        }
    }

    private boolean isEbcdic(BufferedInputStream in) throws IOException {
        byte b;
        do {
            b = (byte) in.read();
            if (b == -1) {
                return false;
            }
            if (b == 35 || b == 10 || b == 61) {
                return false;
            }
        } while (b != 21);
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [java.lang.Enum, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0161  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized org.acra.collector.CrashReportData load(java.io.Reader r25) throws java.io.IOException {
        /*
            r24 = this;
            monitor-enter(r24)
            r14 = 0
            r20 = 0
            r7 = 0
            r22 = 40
            r0 = r22
            char[] r0 = new char[r0]     // Catch:{ all -> 0x0040 }
            r6 = r0
            r17 = 0
            r13 = -1
            r10 = 1
            org.acra.collector.CrashReportData r8 = new org.acra.collector.CrashReportData     // Catch:{ all -> 0x0040 }
            r8.<init>()     // Catch:{ all -> 0x0040 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ all -> 0x0040 }
            r0 = r5
            r1 = r25
            r0.<init>(r1)     // Catch:{ all -> 0x0040 }
            r18 = r17
        L_0x001f:
            int r11 = r5.read()     // Catch:{ all -> 0x0040 }
            r22 = -1
            r0 = r11
            r1 = r22
            if (r0 != r1) goto L_0x0043
            r22 = 2
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x01c1
            r22 = 4
            r0 = r7
            r1 = r22
            if (r0 > r1) goto L_0x01c1
            java.lang.IllegalArgumentException r22 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0040 }
            java.lang.String r23 = "luni.08"
            r22.<init>(r23)     // Catch:{ all -> 0x0040 }
            throw r22     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r22 = move-exception
            monitor-exit(r24)
            throw r22
        L_0x0043:
            r0 = r11
            char r0 = (char) r0
            r16 = r0
            r0 = r6
            int r0 = r0.length     // Catch:{ all -> 0x0040 }
            r22 = r0
            r0 = r18
            r1 = r22
            if (r0 != r1) goto L_0x006c
            r0 = r6
            int r0 = r0.length     // Catch:{ all -> 0x0040 }
            r22 = r0
            int r22 = r22 * 2
            r0 = r22
            char[] r0 = new char[r0]     // Catch:{ all -> 0x0040 }
            r15 = r0
            r22 = 0
            r23 = 0
            r0 = r6
            r1 = r22
            r2 = r15
            r3 = r23
            r4 = r18
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ all -> 0x0040 }
            r6 = r15
        L_0x006c:
            r22 = 2
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x00bb
            r22 = 16
            r0 = r16
            r1 = r22
            int r9 = java.lang.Character.digit(r0, r1)     // Catch:{ all -> 0x0040 }
            if (r9 < 0) goto L_0x00aa
            int r22 = r20 << 4
            int r20 = r22 + r9
            int r7 = r7 + 1
            r22 = 4
            r0 = r7
            r1 = r22
            if (r0 < r1) goto L_0x001f
        L_0x008c:
            r14 = 0
            int r17 = r18 + 1
            r0 = r20
            char r0 = (char) r0     // Catch:{ all -> 0x0040 }
            r22 = r0
            r6[r18] = r22     // Catch:{ all -> 0x0040 }
            r22 = 10
            r0 = r16
            r1 = r22
            if (r0 == r1) goto L_0x00b9
            r22 = 133(0x85, float:1.86E-43)
            r0 = r16
            r1 = r22
            if (r0 == r1) goto L_0x00b9
            r18 = r17
            goto L_0x001f
        L_0x00aa:
            r22 = 4
            r0 = r7
            r1 = r22
            if (r0 > r1) goto L_0x008c
            java.lang.IllegalArgumentException r22 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0040 }
            java.lang.String r23 = "luni.09"
            r22.<init>(r23)     // Catch:{ all -> 0x0040 }
            throw r22     // Catch:{ all -> 0x0040 }
        L_0x00b9:
            r18 = r17
        L_0x00bb:
            r22 = 1
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x00f4
            r14 = 0
            switch(r16) {
                case 10: goto L_0x00dc;
                case 13: goto L_0x00d9;
                case 98: goto L_0x00df;
                case 102: goto L_0x00e2;
                case 110: goto L_0x00e5;
                case 114: goto L_0x00e8;
                case 116: goto L_0x00eb;
                case 117: goto L_0x00ee;
                case 133: goto L_0x00dc;
                default: goto L_0x00c6;
            }     // Catch:{ all -> 0x0040 }
        L_0x00c6:
            r10 = 0
            r22 = 4
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x00d1
            r13 = r18
            r14 = 0
        L_0x00d1:
            int r17 = r18 + 1
            r6[r18] = r16     // Catch:{ all -> 0x0040 }
            r18 = r17
            goto L_0x001f
        L_0x00d9:
            r14 = 3
            goto L_0x001f
        L_0x00dc:
            r14 = 5
            goto L_0x001f
        L_0x00df:
            r16 = 8
            goto L_0x00c6
        L_0x00e2:
            r16 = 12
            goto L_0x00c6
        L_0x00e5:
            r16 = 10
            goto L_0x00c6
        L_0x00e8:
            r16 = 13
            goto L_0x00c6
        L_0x00eb:
            r16 = 9
            goto L_0x00c6
        L_0x00ee:
            r14 = 2
            r7 = 0
            r20 = r7
            goto L_0x001f
        L_0x00f4:
            switch(r16) {
                case 10: goto L_0x0148;
                case 13: goto L_0x0152;
                case 33: goto L_0x011d;
                case 35: goto L_0x011d;
                case 58: goto L_0x01a4;
                case 61: goto L_0x01a4;
                case 92: goto L_0x0198;
                case 133: goto L_0x0152;
                default: goto L_0x00f7;
            }     // Catch:{ all -> 0x0040 }
        L_0x00f7:
            boolean r22 = java.lang.Character.isWhitespace(r16)     // Catch:{ all -> 0x0040 }
            if (r22 == 0) goto L_0x01b0
            r22 = 3
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x0105
            r14 = 5
        L_0x0105:
            if (r18 == 0) goto L_0x001f
            r0 = r18
            r1 = r13
            if (r0 == r1) goto L_0x001f
            r22 = 5
            r0 = r14
            r1 = r22
            if (r0 == r1) goto L_0x001f
            r22 = -1
            r0 = r13
            r1 = r22
            if (r0 != r1) goto L_0x01b0
            r14 = 4
            goto L_0x001f
        L_0x011d:
            if (r10 == 0) goto L_0x00f7
        L_0x011f:
            int r11 = r5.read()     // Catch:{ all -> 0x0040 }
            r22 = -1
            r0 = r11
            r1 = r22
            if (r0 == r1) goto L_0x001f
            r0 = r11
            char r0 = (char) r0     // Catch:{ all -> 0x0040 }
            r16 = r0
            r22 = 13
            r0 = r16
            r1 = r22
            if (r0 == r1) goto L_0x001f
            r22 = 10
            r0 = r16
            r1 = r22
            if (r0 == r1) goto L_0x001f
            r22 = 133(0x85, float:1.86E-43)
            r0 = r16
            r1 = r22
            if (r0 != r1) goto L_0x011f
            goto L_0x001f
        L_0x0148:
            r22 = 3
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x0152
            r14 = 5
            goto L_0x001f
        L_0x0152:
            r14 = 0
            r10 = 1
            if (r18 > 0) goto L_0x015a
            if (r18 != 0) goto L_0x0191
            if (r13 != 0) goto L_0x0191
        L_0x015a:
            r22 = -1
            r0 = r13
            r1 = r22
            if (r0 != r1) goto L_0x0163
            r13 = r18
        L_0x0163:
            java.lang.String r19 = new java.lang.String     // Catch:{ all -> 0x0040 }
            r22 = 0
            r0 = r19
            r1 = r6
            r2 = r22
            r3 = r18
            r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x0040 }
            java.lang.Class<org.acra.ReportField> r22 = org.acra.ReportField.class
            r23 = 0
            r0 = r19
            r1 = r23
            r2 = r13
            java.lang.String r23 = r0.substring(r1, r2)     // Catch:{ all -> 0x0040 }
            java.lang.Enum r22 = java.lang.Enum.valueOf(r22, r23)     // Catch:{ all -> 0x0040 }
            r0 = r19
            r1 = r13
            java.lang.String r23 = r0.substring(r1)     // Catch:{ all -> 0x0040 }
            r0 = r8
            r1 = r22
            r2 = r23
            r0.put(r1, r2)     // Catch:{ all -> 0x0040 }
        L_0x0191:
            r13 = -1
            r17 = 0
            r18 = r17
            goto L_0x001f
        L_0x0198:
            r22 = 4
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x01a1
            r13 = r18
        L_0x01a1:
            r14 = 1
            goto L_0x001f
        L_0x01a4:
            r22 = -1
            r0 = r13
            r1 = r22
            if (r0 != r1) goto L_0x00f7
            r14 = 0
            r13 = r18
            goto L_0x001f
        L_0x01b0:
            r22 = 5
            r0 = r14
            r1 = r22
            if (r0 == r1) goto L_0x01be
            r22 = 3
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x00c6
        L_0x01be:
            r14 = 0
            goto L_0x00c6
        L_0x01c1:
            r22 = -1
            r0 = r13
            r1 = r22
            if (r0 != r1) goto L_0x01cc
            if (r18 <= 0) goto L_0x01cc
            r13 = r18
        L_0x01cc:
            if (r13 < 0) goto L_0x021b
            java.lang.String r19 = new java.lang.String     // Catch:{ all -> 0x0040 }
            r22 = 0
            r0 = r19
            r1 = r6
            r2 = r22
            r3 = r18
            r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x0040 }
            java.lang.Class<org.acra.ReportField> r22 = org.acra.ReportField.class
            r23 = 0
            r0 = r19
            r1 = r23
            r2 = r13
            java.lang.String r23 = r0.substring(r1, r2)     // Catch:{ all -> 0x0040 }
            java.lang.Enum r12 = java.lang.Enum.valueOf(r22, r23)     // Catch:{ all -> 0x0040 }
            org.acra.ReportField r12 = (org.acra.ReportField) r12     // Catch:{ all -> 0x0040 }
            r0 = r19
            r1 = r13
            java.lang.String r21 = r0.substring(r1)     // Catch:{ all -> 0x0040 }
            r22 = 1
            r0 = r14
            r1 = r22
            if (r0 != r1) goto L_0x0214
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ all -> 0x0040 }
            r22.<init>()     // Catch:{ all -> 0x0040 }
            r0 = r22
            r1 = r21
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ all -> 0x0040 }
            java.lang.String r23 = "\u0000"
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ all -> 0x0040 }
            java.lang.String r21 = r22.toString()     // Catch:{ all -> 0x0040 }
        L_0x0214:
            r0 = r8
            r1 = r12
            r2 = r21
            r0.put(r1, r2)     // Catch:{ all -> 0x0040 }
        L_0x021b:
            monitor-exit(r24)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: org.acra.CrashReportPersister.load(java.io.Reader):org.acra.collector.CrashReportData");
    }

    private void dumpString(StringBuilder buffer, String string, boolean key) {
        int i = 0;
        if (!key && 0 < string.length() && string.charAt(0) == ' ') {
            buffer.append("\\ ");
            i = 0 + 1;
        }
        while (i < string.length()) {
            char ch = string.charAt(i);
            switch (ch) {
                case 9:
                    buffer.append("\\t");
                    break;
                case 10:
                    buffer.append("\\n");
                    break;
                case 11:
                default:
                    if ("\\#!=:".indexOf(ch) >= 0 || (key && ch == ' ')) {
                        buffer.append('\\');
                    }
                    if (ch >= ' ' && ch <= '~') {
                        buffer.append(ch);
                        break;
                    } else {
                        String hex = Integer.toHexString(ch);
                        buffer.append("\\u");
                        for (int j = 0; j < 4 - hex.length(); j++) {
                            buffer.append("0");
                        }
                        buffer.append(hex);
                        break;
                    }
                    break;
                case PlatformConstants.HONEYCOMB_3_1:
                    buffer.append("\\f");
                    break;
                case 13:
                    buffer.append("\\r");
                    break;
            }
            i++;
        }
    }
}
