package org.acra;

final class ACRAConstants {
    static final String APPROVED_SUFFIX = "-approved";
    static final String EXTRA_REPORT_FILE_NAME = "REPORT_FILE_NAME";
    static final int MAX_SEND_REPORTS = 5;
    static final int NOTIF_CRASH_ID = 666;
    public static final String REPORTFILE_EXTENSION = ".stacktrace";
    static final String SILENT_SUFFIX = ("-" + ReportField.IS_SILENT);

    ACRAConstants() {
    }
}
