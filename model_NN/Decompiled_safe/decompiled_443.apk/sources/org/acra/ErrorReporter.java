package org.acra;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Looper;
import android.os.Process;
import android.text.format.Time;
import android.util.Log;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.acra.annotation.ReportsCrashes;
import org.acra.collector.CrashReportData;
import org.acra.collector.CrashReportDataFactory;
import org.acra.sender.ReportSender;
import org.acra.util.ReportUtils;
import org.acra.util.ToastSender;

public class ErrorReporter implements Thread.UncaughtExceptionHandler {
    private final CrashReportDataFactory crashReportDataFactory;
    private boolean enabled = false;
    private final CrashReportFileNameParser fileNameParser = new CrashReportFileNameParser();
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Thread.UncaughtExceptionHandler mDfltExceptionHandler;
    private final List<ReportSender> mReportSenders = new ArrayList();
    private final ReportingInteractionMode mReportingInteractionMode;
    private final SharedPreferences prefs;

    ErrorReporter(Context context, SharedPreferences prefs2, boolean enabled2) {
        this.mContext = context;
        this.prefs = prefs2;
        this.enabled = enabled2;
        String initialConfiguration = ReportUtils.getCrashConfiguration(this.mContext);
        Time appStartDate = new Time();
        appStartDate.setToNow();
        this.crashReportDataFactory = new CrashReportDataFactory(this.mContext, prefs2, appStartDate, initialConfiguration);
        this.mReportingInteractionMode = ACRA.getConfig().mode();
        this.mDfltExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        checkReportsOnApplicationStart();
    }

    public static ErrorReporter getInstance() {
        return ACRA.getErrorReporter();
    }

    @Deprecated
    public void addCustomData(String key, String value) {
        this.crashReportDataFactory.putCustomData(key, value);
    }

    public String putCustomData(String key, String value) {
        return this.crashReportDataFactory.putCustomData(key, value);
    }

    public String removeCustomData(String key) {
        return this.crashReportDataFactory.removeCustomData(key);
    }

    public String getCustomData(String key) {
        return this.crashReportDataFactory.getCustomData(key);
    }

    public void addReportSender(ReportSender sender) {
        this.mReportSenders.add(sender);
    }

    public void removeReportSender(ReportSender sender) {
        this.mReportSenders.remove(sender);
    }

    public void removeReportSenders(Class<?> senderClass) {
        if (ReportSender.class.isAssignableFrom(senderClass)) {
            for (ReportSender sender : this.mReportSenders) {
                if (senderClass.isInstance(sender)) {
                    this.mReportSenders.remove(sender);
                }
            }
        }
    }

    public void removeAllReportSenders() {
        this.mReportSenders.clear();
    }

    public void setReportSender(ReportSender sender) {
        removeAllReportSenders();
        addReportSender(sender);
    }

    public void uncaughtException(Thread t, Throwable e) {
        if (this.enabled) {
            Log.e(ACRA.LOG_TAG, "ACRA caught a " + e.getClass().getSimpleName() + " exception for " + this.mContext.getPackageName() + ". Building report.");
            Thread worker = handleException(e, this.mReportingInteractionMode, false);
            if (this.mReportingInteractionMode == ReportingInteractionMode.TOAST) {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e1) {
                    Log.e(ACRA.LOG_TAG, "Error : ", e1);
                }
            }
            if (worker != null) {
                while (worker.isAlive()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e12) {
                        Log.e(ACRA.LOG_TAG, "Error : ", e12);
                    }
                }
            }
            if (this.mReportingInteractionMode == ReportingInteractionMode.SILENT || (this.mReportingInteractionMode == ReportingInteractionMode.TOAST && ACRA.getConfig().forceCloseDialogAfterToast())) {
                this.mDfltExceptionHandler.uncaughtException(t, e);
                return;
            }
            Log.e(ACRA.LOG_TAG, this.mContext.getPackageName() + " fatal error : " + e.getMessage(), e);
            Process.killProcess(Process.myPid());
            System.exit(10);
        } else if (this.mDfltExceptionHandler != null) {
            Log.e(ACRA.LOG_TAG, "ACRA is disabled for " + this.mContext.getPackageName() + " - forwarding uncaught Exception on to default ExceptionHandler");
            this.mDfltExceptionHandler.uncaughtException(t, e);
        } else {
            Log.e(ACRA.LOG_TAG, "ACRA is disabled for " + this.mContext.getPackageName() + " - no default ExceptionHandler");
        }
    }

    public Thread handleSilentException(Throwable e) {
        if (this.enabled) {
            return handleException(e, ReportingInteractionMode.SILENT, true);
        }
        Log.d(ACRA.LOG_TAG, "ACRA is disabled. Silent report not sent.");
        return null;
    }

    public void setEnabled(boolean enabled2) {
        Log.i(ACRA.LOG_TAG, "ACRA is " + (enabled2 ? "enabled" : "disabled") + " for " + this.mContext.getPackageName());
        this.enabled = enabled2;
    }

    /* access modifiers changed from: package-private */
    public SendWorker startSendingReports(boolean onlySendSilentReports, boolean approveReportsFirst) {
        SendWorker worker = new SendWorker(this.mContext, this.mReportSenders, onlySendSilentReports, approveReportsFirst);
        worker.start();
        return worker;
    }

    /* access modifiers changed from: package-private */
    public void deletePendingReports() {
        deletePendingReports(true, true, 0);
    }

    private void checkReportsOnApplicationStart() {
        String[] filesList = new CrashReportFinder(this.mContext).getCrashReportFiles();
        if (filesList != null && filesList.length > 0) {
            boolean onlySilentOrApprovedReports = containsOnlySilentOrApprovedReports(filesList);
            if (this.mReportingInteractionMode == ReportingInteractionMode.SILENT || this.mReportingInteractionMode == ReportingInteractionMode.TOAST || (this.mReportingInteractionMode == ReportingInteractionMode.NOTIFICATION && onlySilentOrApprovedReports)) {
                if (this.mReportingInteractionMode == ReportingInteractionMode.TOAST && !onlySilentOrApprovedReports) {
                    ToastSender.sendToast(this.mContext, ACRA.getConfig().resToastText(), 1);
                }
                Log.v(ACRA.LOG_TAG, "About to start ReportSenderWorker from #checkReportOnApplicationStart");
                startSendingReports(false, false);
            } else if (ACRA.getConfig().deleteUnapprovedReportsOnApplicationStart()) {
                deletePendingNonApprovedReports();
            } else {
                notifySendReport(getLatestNonSilentReport(filesList));
            }
        }
    }

    private void deletePendingNonApprovedReports() {
        int nbReportsToKeep;
        if (this.mReportingInteractionMode == ReportingInteractionMode.NOTIFICATION) {
            nbReportsToKeep = 1;
        } else {
            nbReportsToKeep = 0;
        }
        deletePendingReports(false, true, nbReportsToKeep);
    }

    private SendWorker handleException(Throwable e, ReportingInteractionMode reportingInteractionMode, boolean isSilentReport) {
        boolean sendOnlySilentReports = false;
        if (reportingInteractionMode == null) {
            reportingInteractionMode = this.mReportingInteractionMode;
        } else if (reportingInteractionMode == ReportingInteractionMode.SILENT && this.mReportingInteractionMode != ReportingInteractionMode.SILENT) {
            sendOnlySilentReports = true;
        }
        if (e == null) {
            e = new Exception("Report requested by developer");
        }
        if (reportingInteractionMode == ReportingInteractionMode.TOAST || (reportingInteractionMode == ReportingInteractionMode.NOTIFICATION && ACRA.getConfig().resToastText() != 0)) {
            new Thread() {
                public void run() {
                    Looper.prepare();
                    ToastSender.sendToast(ErrorReporter.this.mContext, ACRA.getConfig().resToastText(), 1);
                    Looper.loop();
                }
            }.start();
        }
        CrashReportData crashReportData = this.crashReportDataFactory.createCrashData(e, isSilentReport);
        String reportFileName = getReportFileName(crashReportData);
        saveCrashReportFile(reportFileName, crashReportData);
        if (reportingInteractionMode == ReportingInteractionMode.SILENT || reportingInteractionMode == ReportingInteractionMode.TOAST || this.prefs.getBoolean(ACRA.PREF_ALWAYS_ACCEPT, false)) {
            Log.v(ACRA.LOG_TAG, "About to start ReportSenderWorker from #handleException");
            return startSendingReports(sendOnlySilentReports, true);
        }
        if (reportingInteractionMode == ReportingInteractionMode.NOTIFICATION) {
            notifySendReport(reportFileName);
        }
        return null;
    }

    private void notifySendReport(String reportFileName) {
        NotificationManager notificationManager = (NotificationManager) this.mContext.getSystemService("notification");
        ReportsCrashes conf = ACRA.getConfig();
        Notification notification = new Notification(conf.resNotifIcon(), this.mContext.getText(conf.resNotifTickerText()), System.currentTimeMillis());
        CharSequence contentTitle = this.mContext.getText(conf.resNotifTitle());
        CharSequence contentText = this.mContext.getText(conf.resNotifText());
        Intent notificationIntent = new Intent(this.mContext, CrashReportDialog.class);
        Log.d(ACRA.LOG_TAG, "Creating Notification for " + reportFileName);
        notificationIntent.putExtra("REPORT_FILE_NAME", reportFileName);
        notification.setLatestEventInfo(this.mContext, contentTitle, contentText, PendingIntent.getActivity(this.mContext, 0, notificationIntent, 134217728));
        notificationManager.cancelAll();
        notificationManager.notify(666, notification);
    }

    private String getReportFileName(CrashReportData crashData) {
        Time now = new Time();
        now.setToNow();
        return "" + now.toMillis(false) + (crashData.getProperty(ReportField.IS_SILENT) != null ? ACRAConstants.SILENT_SUFFIX : "") + ACRAConstants.REPORTFILE_EXTENSION;
    }

    private void saveCrashReportFile(String fileName, CrashReportData crashData) {
        try {
            Log.d(ACRA.LOG_TAG, "Writing crash report file.");
            new CrashReportPersister(this.mContext).store(crashData, fileName);
        } catch (Exception e) {
            Log.e(ACRA.LOG_TAG, "An error occurred while writing the report file...", e);
        }
    }

    private String getLatestNonSilentReport(String[] filesList) {
        if (filesList == null || filesList.length <= 0) {
            return null;
        }
        for (int i = filesList.length - 1; i >= 0; i--) {
            if (!this.fileNameParser.isSilent(filesList[i])) {
                return filesList[i];
            }
        }
        return filesList[filesList.length - 1];
    }

    private void deletePendingReports(boolean deleteApprovedReports, boolean deleteNonApprovedReports, int nbOfLatestToKeep) {
        String[] filesList = new CrashReportFinder(this.mContext).getCrashReportFiles();
        Arrays.sort(filesList);
        if (filesList != null) {
            for (int iFile = 0; iFile < filesList.length - nbOfLatestToKeep; iFile++) {
                String fileName = filesList[iFile];
                boolean isReportApproved = this.fileNameParser.isApproved(fileName);
                if ((isReportApproved && deleteApprovedReports) || (!isReportApproved && deleteNonApprovedReports)) {
                    File fileToDelete = new File(this.mContext.getFilesDir(), fileName);
                    if (!fileToDelete.delete()) {
                        Log.e(ACRA.LOG_TAG, "Could not delete report : " + fileToDelete);
                    }
                }
            }
        }
    }

    private boolean containsOnlySilentOrApprovedReports(String[] reportFileNames) {
        for (String reportFileName : reportFileNames) {
            if (!this.fileNameParser.isApproved(reportFileName)) {
                return false;
            }
        }
        return true;
    }
}
