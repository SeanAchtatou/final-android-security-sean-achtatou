package com.admob.android.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.j;
import com.admob.android.ads.view.AdMobWebView;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONObject;

public class AdMobActivity extends Activity {
    private r a;
    private Vector<ae> b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        View view;
        super.onCreate(bundle);
        this.b = new Vector<>();
        Bundle bundleExtra = getIntent().getBundleExtra("o");
        r rVar = new r();
        if (rVar.a(bundleExtra)) {
            this.a = rVar;
        } else {
            this.a = null;
        }
        if (this.a != null) {
            q.a(this.a.c, (JSONObject) null, AdManager.getUserId(this));
            j.a aVar = this.a.a;
            WeakReference weakReference = new WeakReference(this);
            switch (aVar) {
                case CLICK_TO_FULLSCREEN_BROWSER:
                    setTheme(16973831);
                    view = AdMobWebView.a(getApplicationContext(), this.a.d, false, this.a.f, this.a.g, k.a(this), weakReference);
                    break;
                case CLICK_TO_INTERACTIVE_VIDEO:
                    r rVar2 = this.a;
                    ac acVar = new ac(getApplicationContext(), weakReference);
                    acVar.a(rVar2);
                    this.b.add(acVar);
                    view = acVar;
                    break;
                default:
                    view = null;
                    break;
            }
            if (view != null) {
                switch (this.a.e) {
                    case LANDSCAPE:
                        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                            Log.v(AdManager.LOG, "Setting target orientation to landscape");
                        }
                        setRequestedOrientation(0);
                        break;
                    case PORTRAIT:
                        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                            Log.v(AdManager.LOG, "Setting target orientation to portrait");
                        }
                        setRequestedOrientation(1);
                        break;
                    default:
                        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                            Log.v(AdManager.LOG, "Setting target orientation to sensor");
                        }
                        setRequestedOrientation(4);
                        break;
                }
                setContentView(view);
                return;
            }
            finish();
        } else if (InterstitialAd.c.a(AdManager.LOG, 6)) {
            Log.e(AdManager.LOG, "Unable to get openerInfo from intent");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void finish() {
        if (this.a != null && this.a.l) {
            Intent intent = new Intent();
            intent.putExtra(InterstitialAd.ADMOB_INTENT_BOOLEAN, true);
            setResult(-1, intent);
        }
        super.finish();
    }

    public void onConfigurationChanged(Configuration configuration) {
        Iterator<ae> it = this.b.iterator();
        while (it.hasNext()) {
            it.next().a(configuration);
        }
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.clear();
        super.onDestroy();
    }
}
