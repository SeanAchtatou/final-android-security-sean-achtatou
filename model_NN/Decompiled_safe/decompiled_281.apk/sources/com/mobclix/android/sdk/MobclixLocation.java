package com.mobclix.android.sdk;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.Timer;
import java.util.TimerTask;

class MobclixLocation {
    boolean gps_enabled = false;
    LocationManager lm;
    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            MobclixLocation.this.timer1.cancel();
            MobclixLocation.this.locationResult.gotLocation(location);
            MobclixLocation.this.lm.removeUpdates(this);
            MobclixLocation.this.lm.removeUpdates(MobclixLocation.this.locationListenerNetwork);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            MobclixLocation.this.timer1.cancel();
            MobclixLocation.this.locationResult.gotLocation(location);
            MobclixLocation.this.lm.removeUpdates(this);
            MobclixLocation.this.lm.removeUpdates(MobclixLocation.this.locationListenerGps);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    LocationResult locationResult;
    boolean network_enabled = false;
    Timer timer1;

    public static abstract class LocationResult {
        public abstract void gotLocation(Location location);
    }

    MobclixLocation() {
    }

    public boolean getLocation(Context context, LocationResult result) {
        this.locationResult = result;
        if (this.lm == null) {
            this.lm = (LocationManager) context.getSystemService("location");
        }
        try {
            this.gps_enabled = this.lm.isProviderEnabled("gps");
        } catch (Exception e) {
        }
        try {
            this.network_enabled = this.lm.isProviderEnabled("network");
        } catch (Exception e2) {
        }
        if (!this.gps_enabled && !this.network_enabled) {
            return false;
        }
        if (this.gps_enabled) {
            this.lm.requestLocationUpdates("gps", 0, 0.0f, this.locationListenerGps);
        }
        if (this.network_enabled) {
            this.lm.requestLocationUpdates("network", 0, 0.0f, this.locationListenerNetwork);
        }
        this.timer1 = new Timer();
        this.timer1.schedule(new GetLastLocation(), 20000);
        return true;
    }

    class GetLastLocation extends TimerTask {
        GetLastLocation() {
        }

        public void run() {
            MobclixLocation.this.lm.removeUpdates(MobclixLocation.this.locationListenerGps);
            MobclixLocation.this.lm.removeUpdates(MobclixLocation.this.locationListenerNetwork);
            Location net_loc = null;
            Location gps_loc = null;
            if (MobclixLocation.this.gps_enabled) {
                gps_loc = MobclixLocation.this.lm.getLastKnownLocation("gps");
            }
            if (MobclixLocation.this.network_enabled) {
                net_loc = MobclixLocation.this.lm.getLastKnownLocation("network");
            }
            if (gps_loc == null || net_loc == null) {
                if (gps_loc != null) {
                    MobclixLocation.this.locationResult.gotLocation(gps_loc);
                } else if (net_loc != null) {
                    MobclixLocation.this.locationResult.gotLocation(net_loc);
                } else {
                    MobclixLocation.this.locationResult.gotLocation(null);
                }
            } else if (gps_loc.getTime() > net_loc.getTime()) {
                MobclixLocation.this.locationResult.gotLocation(gps_loc);
            } else {
                MobclixLocation.this.locationResult.gotLocation(net_loc);
            }
        }
    }
}
