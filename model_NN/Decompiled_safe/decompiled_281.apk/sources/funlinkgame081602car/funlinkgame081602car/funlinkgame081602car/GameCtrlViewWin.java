package funlinkgame081602car.funlinkgame081602car.funlinkgame081602car;

import android.content.Context;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GameCtrlViewWin extends WinGameViewWin {
    public static boolean CURRENT_CH = false;
    public int CURRENT_TYPE = 0;
    /* access modifiers changed from: private */
    public Point C_POINT;
    public final int GAMETIME = 300;
    public int PROCESS_VALUE = 300;
    /* access modifiers changed from: private */
    public Point P_POINT;
    public final int UPTIME = 1;
    private boolean bIfMakeSound = true;
    private int godanswer = 0;
    private Date lastUpdateTime;
    LinkedList<Line> li;
    private MediaPlayer mFailMediaPlayer06;
    GestureDetector mGestureDetector;
    private ImageView mImageView01;
    private MediaPlayer mMediaPlayer01;
    private MediaPlayer mMediaPlayer02;
    private MediaPlayer mMediaPlayer03;
    private MediaPlayer mMediaPlayer04;
    private MediaPlayer mMediaPlayer05;
    private MediaPlayer mMediaPlayer06;
    private RefreshHandler mRedrawHandler = new RefreshHandler();
    private SensorManager mSensorManager01;
    private TextView mTextView01;
    private final int song1 = 1;
    private final int song2 = 2;
    private final int song3 = 3;
    private final int song4 = 4;
    private final int song5 = 5;
    private final int song6 = 6;
    private float velocityFinal = 0.0f;

    public GameCtrlViewWin(Context context, AttributeSet attrs) {
        super(context, attrs);
        initType();
        initGrid();
        this.much = 64;
        this.mMediaPlayer01 = new MediaPlayer();
        this.mMediaPlayer01 = MediaPlayer.create(context, (int) R.raw.soundb1o);
        this.mMediaPlayer02 = new MediaPlayer();
        this.mMediaPlayer02 = MediaPlayer.create(context, (int) R.raw.soundw2o);
        this.mMediaPlayer03 = new MediaPlayer();
        this.mMediaPlayer03 = MediaPlayer.create(context, (int) R.raw.soundw5o);
        this.mMediaPlayer04 = new MediaPlayer();
        this.mMediaPlayer04 = MediaPlayer.create(context, (int) R.raw.soundw4o);
        this.mMediaPlayer05 = new MediaPlayer();
        this.mMediaPlayer05 = MediaPlayer.create(context, (int) R.raw.soundw11o);
        this.mMediaPlayer06 = new MediaPlayer();
        this.mMediaPlayer06 = MediaPlayer.create(context, (int) R.raw.soundw10o);
        this.mFailMediaPlayer06 = new MediaPlayer();
        this.mFailMediaPlayer06 = MediaPlayer.create(context, (int) R.raw.soundb3o);
    }

    public GameCtrlViewWin(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initType();
        initGrid();
        this.much = 64;
        this.mMediaPlayer01 = new MediaPlayer();
        this.mMediaPlayer01 = MediaPlayer.create(context, (int) R.raw.soundb1o);
        this.mMediaPlayer02 = new MediaPlayer();
        this.mMediaPlayer02 = MediaPlayer.create(context, (int) R.raw.soundw2o);
        this.mMediaPlayer03 = new MediaPlayer();
        this.mMediaPlayer03 = MediaPlayer.create(context, (int) R.raw.soundw5o);
        this.mMediaPlayer04 = new MediaPlayer();
        this.mMediaPlayer04 = MediaPlayer.create(context, (int) R.raw.soundw4o);
        this.mMediaPlayer05 = new MediaPlayer();
        this.mMediaPlayer05 = MediaPlayer.create(context, (int) R.raw.soundw11o);
        this.mMediaPlayer06 = new MediaPlayer();
        this.mMediaPlayer06 = MediaPlayer.create(context, (int) R.raw.soundw10o);
        this.mFailMediaPlayer06 = new MediaPlayer();
        this.mFailMediaPlayer06 = MediaPlayer.create(context, (int) R.raw.soundb3o);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        int selX = (int) (event.getX() / this.width);
        int selY = (int) (event.getY() / this.height);
        if (this.grid[selX][selY] == 0) {
            return true;
        }
        if (!CURRENT_CH) {
            select(selX, selY);
            CURRENT_CH = true;
            this.P_POINT = new Point(selX, selY);
        } else {
            this.C_POINT = new Point(selX, selY);
            this.lineType = 0;
            if (checkLink(this.P_POINT, this.C_POINT)) {
                this.godanswer = new Random().nextInt(6) + 1;
                switch (this.godanswer) {
                    case 1:
                        playMusic(this.mMediaPlayer01);
                        break;
                    case 2:
                        playMusic(this.mMediaPlayer02);
                        break;
                    case 3:
                        playMusic(this.mMediaPlayer03);
                        break;
                    case 4:
                        playMusic(this.mMediaPlayer04);
                        break;
                    case R.styleable.com_admob_android_ads_AdView_isGoneWithoutAd:
                        playMusic(this.mMediaPlayer05);
                        break;
                    case 6:
                        playMusic(this.mMediaPlayer06);
                        break;
                }
                this.isLine = true;
                this.much -= 2;
                if (this.PROCESS_VALUE > 0 && this.PROCESS_VALUE + 1 < 300) {
                    this.PROCESS_VALUE++;
                }
                invalidate();
                this.mRedrawHandler.sleep(300);
            } else {
                playMusic(this.mFailMediaPlayer06);
            }
            CURRENT_CH = false;
        }
        return true;
    }

    public void stopPlayingMusic(MediaPlayer mediaPlayer) {
        if (mediaPlayer.isPlaying()) {
            try {
                mediaPlayer.seekTo(0);
                mediaPlayer.stop();
                mediaPlayer.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void playMusic(MediaPlayer mediaPlayer) {
        stopPlayingMusic(this.mMediaPlayer01);
        stopPlayingMusic(this.mMediaPlayer02);
        stopPlayingMusic(this.mMediaPlayer03);
        stopPlayingMusic(this.mMediaPlayer04);
        if (mediaPlayer != null) {
            try {
                mediaPlayer.seekTo(0);
                mediaPlayer.stop();
                mediaPlayer.prepare();
                mediaPlayer.setVolume(10.0f, 10.0f);
                mediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                mediaPlayer.prepare();
                mediaPlayer.setVolume(10.0f, 10.0f);
                mediaPlayer.start();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void reset() {
        CURRENT_CH = false;
        this.CURRENT_TYPE = 0;
        this.C_POINT = null;
        this.P_POINT = null;
        this.lineType = 0;
        this.isLine = false;
        initType();
        initGrid();
        this.much = 64;
        invalidate();
    }

    public void rearrange() {
        CURRENT_CH = false;
        this.CURRENT_TYPE = 0;
        this.C_POINT = null;
        this.P_POINT = null;
        this.lineType = 0;
        this.isLine = false;
        List<Integer> temp = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (this.grid[i][j] != 0) {
                    temp.add(Integer.valueOf(this.grid[i][j]));
                }
            }
        }
        this.type.clear();
        Random ad = new Random();
        for (int i2 = 0; i2 < temp.size(); i2++) {
            this.type.add((Integer) temp.get(i2));
        }
        temp.clear();
        for (int i3 = 0; i3 < 10; i3++) {
            for (int j2 = 0; j2 < 10; j2++) {
                if (this.grid[i3][j2] != 0) {
                    int index = ad.nextInt(this.type.size());
                    this.grid[i3][j2] = ((Integer) this.type.get(index)).intValue();
                    this.type.remove(index);
                }
            }
        }
        invalidate();
    }

    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            GameCtrlViewWin.this.isLine = false;
            GameCtrlViewWin.this.grid[GameCtrlViewWin.this.P_POINT.x][GameCtrlViewWin.this.P_POINT.y] = 0;
            GameCtrlViewWin.this.grid[GameCtrlViewWin.this.C_POINT.x][GameCtrlViewWin.this.C_POINT.y] = 0;
            GameCtrlViewWin.this.invalidate();
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    public class Point {
        public int x;
        public int y;

        public Point(int newx, int newy) {
            this.x = newx;
            this.y = newy;
        }

        public boolean equals(Point p) {
            if (p.x == this.x && p.y == this.y) {
                return true;
            }
            return false;
        }
    }

    private boolean horizon(Point a, Point b) {
        if (a.x == b.x && a.y == b.y) {
            return false;
        }
        int x_start = a.y <= b.y ? a.y : b.y;
        int x_end = a.y <= b.y ? b.y : a.y;
        for (int x = x_start + 1; x < x_end; x++) {
            if (this.grid[a.x][x] != 0) {
                return false;
            }
        }
        this.p = new Point[]{a, b};
        this.lineType = 1;
        return true;
    }

    private boolean vertical(Point a, Point b) {
        if (a.x == b.x && a.y == b.y) {
            return false;
        }
        int y_start = a.x <= b.x ? a.x : b.x;
        int y_end = a.x <= b.x ? b.x : a.x;
        for (int y = y_start + 1; y < y_end; y++) {
            if (this.grid[y][a.y] != 0) {
                return false;
            }
        }
        this.p = new Point[]{a, b};
        this.lineType = 1;
        return true;
    }

    private boolean oneCorner(Point a, Point b) {
        boolean method2;
        Point c = new Point(a.x, b.y);
        Point d = new Point(b.x, a.y);
        if (this.grid[c.x][c.y] == 0) {
            boolean method1 = horizon(a, c) && vertical(b, c);
            this.p = new Point[]{a, new Point(c.x, c.y), b};
            this.lineType = 2;
            return method1;
        } else if (this.grid[d.x][d.y] != 0) {
            return false;
        } else {
            if (!vertical(a, d) || !horizon(b, d)) {
                method2 = false;
            } else {
                method2 = true;
            }
            this.p = new Point[]{a, new Point(d.x, d.y), b};
            this.lineType = 2;
            return method2;
        }
    }

    class Line {
        public Point a;
        public Point b;
        public int direct;

        public Line() {
        }

        public Line(int direct2, Point a2, Point b2) {
            this.direct = direct2;
            this.a = a2;
            this.b = b2;
        }
    }

    private LinkedList<Line> scan(Point a, Point b) {
        this.li = new LinkedList<>();
        for (int y = a.y; y >= 0; y--) {
            if (this.grid[a.x][y] == 0 && this.grid[b.x][y] == 0 && vertical(new Point(a.x, y), new Point(b.x, y))) {
                this.li.add(new Line(0, new Point(a.x, y), new Point(b.x, y)));
            }
        }
        for (int y2 = a.y; y2 < 10; y2++) {
            if (this.grid[a.x][y2] == 0 && this.grid[b.x][y2] == 0 && vertical(new Point(a.x, y2), new Point(b.x, y2))) {
                this.li.add(new Line(0, new Point(a.x, y2), new Point(b.x, y2)));
            }
        }
        for (int x = a.x; x >= 0; x--) {
            if (this.grid[x][a.y] == 0 && this.grid[x][b.y] == 0 && horizon(new Point(x, a.y), new Point(x, b.y))) {
                this.li.add(new Line(1, new Point(x, a.y), new Point(x, b.y)));
            }
        }
        for (int x2 = a.x; x2 < 10; x2++) {
            if (this.grid[x2][a.y] == 0 && this.grid[x2][b.y] == 0 && horizon(new Point(x2, a.y), new Point(x2, b.y))) {
                this.li.add(new Line(1, new Point(x2, a.y), new Point(x2, b.y)));
            }
        }
        return this.li;
    }

    private boolean twoCorner(Point a, Point b) {
        this.li = scan(a, b);
        if (this.li.isEmpty()) {
            return false;
        }
        for (int index = 0; index < this.li.size(); index++) {
            Line line = this.li.get(index);
            if (line.direct == 1) {
                if (vertical(a, line.a) && vertical(b, line.b)) {
                    this.p = new Point[]{a, line.a, line.b, b};
                    this.lineType = 3;
                    return true;
                }
            } else if (horizon(a, line.a) && horizon(b, line.b)) {
                this.p = new Point[]{a, line.a, line.b, b};
                this.lineType = 3;
                return true;
            }
        }
        return false;
    }

    public boolean checkLink(Point a, Point b) {
        if (this.grid[a.x][a.y] != this.grid[b.x][b.y]) {
            return false;
        }
        if (a.x == b.x && horizon(a, b)) {
            return true;
        }
        if (a.y == b.y && vertical(a, b)) {
            return true;
        }
        if (oneCorner(a, b)) {
            return true;
        }
        return twoCorner(a, b);
    }
}
