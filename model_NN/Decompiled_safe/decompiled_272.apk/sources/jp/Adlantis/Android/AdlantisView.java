package jp.Adlantis.Android;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;
import cross.field.stage.ImageRegister;
import jp.Adlantis.Android.AdManager;
import jp.Adlantis.Android.AsyncImageLoader;

public class AdlantisView extends RelativeLayout {
    private static int defaultBackgroundColor = -939524096;
    static final int touchHighlightAnimationDuration = 150;
    public AdlantisAd ad;
    /* access modifiers changed from: private */
    public AdManager adManager;
    private AdlantisAdView[] adViews;
    public int animationDuration = 500;
    private boolean buttonPressed;
    /* access modifiers changed from: private */
    public int currentAdIndex = 0;
    /* access modifiers changed from: private */
    public boolean doDeferredAdSetup;
    private long idNotSpecifiedWarningInterval = 5000;
    /* access modifiers changed from: private */
    public boolean layoutComplete;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    private Runnable mRotateAdTask = new Runnable() {
        public void run() {
            AdlantisView.this.showNextAd();
            AdlantisView.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + AdlantisView.this.adDisplayInterval());
        }
    };
    private Runnable mUpdateAdsTask = new Runnable() {
        public void run() {
            AdlantisView.this.connect();
        }
    };
    private int previousAdCount = 0;
    /* access modifiers changed from: private */
    public ProgressBar processIndicator;
    private ViewFlipper rootLayout;
    /* access modifiers changed from: private */
    public View touchHighlight;

    private class AdlantisAdView extends ViewSwitcher {
        private static final int BANNER_ALTTEXT_VIEW = 1;
        private static final int BANNER_IMAGE_VIEW = 0;
        private static final int BANNER_VIEW = 0;
        private static final int TEXTAD_VIEW = 1;
        private AdlantisAd ad;
        private TextView adAltText;
        private ImageView adBanner;
        private ViewFlipper adBannerViewFlipper;
        private SizeFitTextView adtext;
        private ImageView adtextIconView;

        public AdlantisAdView(Context context) {
            super(context);
            commonInitLayout();
        }

        public AdlantisAdView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            commonInitLayout();
        }

        private void commonInitLayout() {
            this.adBannerViewFlipper = new ViewFlipper(getContext());
            addView(this.adBannerViewFlipper, 0, new ViewGroup.LayoutParams(-1, -1));
            this.adBannerViewFlipper.setInAnimation(AdlantisView.this.fadeInAnimation());
            this.adBannerViewFlipper.setOutAnimation(AdlantisView.this.fadeOutAnimation());
            this.adBanner = new ImageView(getContext());
            this.adBanner.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.adBannerViewFlipper.addView(this.adBanner, 0, new ViewGroup.LayoutParams(-1, -1));
            this.adAltText = new TextView(getContext());
            this.adAltText.setTextSize(20.0f);
            this.adAltText.setTextColor(-1);
            this.adAltText.setGravity(17);
            this.adBannerViewFlipper.addView(this.adAltText, 1, new ViewGroup.LayoutParams(-1, -1));
            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            addView(relativeLayout, 1, new ViewGroup.LayoutParams(-1, -1));
            float f = getContext().getResources().getDisplayMetrics().density;
            this.adtextIconView = new ImageView(getContext());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (32.0f * f), (int) (32.0f * f));
            layoutParams.addRule(15, -1);
            layoutParams.setMargins((int) (5.0f * f), 0, 0, 0);
            relativeLayout.addView(this.adtextIconView, layoutParams);
            this.adtext = new SizeFitTextView(getContext());
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.addRule(13, -1);
            layoutParams2.addRule(9, -1);
            layoutParams2.setMargins((int) (42.0f * f), 0, 0, 0);
            this.adtext.setTextSize(20.0f);
            this.adtext.setTextColor(-1);
            this.adtext.setLines(1);
            this.adtext.setMaxLines(1);
            relativeLayout.addView(this.adtext, layoutParams2);
            TextView textView = new TextView(getContext());
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.addRule(11, -1);
            layoutParams3.addRule(12, -1);
            layoutParams3.setMargins(0, 0, (int) (4.0f * f), (int) (f * 1.0f));
            textView.setText("Ads by AdLantis");
            textView.setTextSize(12.0f);
            textView.setTextColor(-1);
            relativeLayout.addView(textView, layoutParams3);
        }

        /* access modifiers changed from: private */
        public void setBannerDrawable(Drawable drawable, boolean z) {
            View currentView = this.adBannerViewFlipper.getCurrentView();
            if (this.adBanner != null) {
                this.adBanner.setImageDrawable(drawable);
            }
            if (this.adBanner != currentView && drawable != null) {
                if (z) {
                    this.adBannerViewFlipper.showNext();
                } else {
                    this.adBannerViewFlipper.setDisplayedChild(0);
                }
            }
        }

        /* access modifiers changed from: private */
        public void setIconDrawable(Drawable drawable) {
            if (this.adtextIconView != null) {
                this.adtextIconView.setImageDrawable(drawable);
            }
        }

        public void setAdByIndex(int i) {
            int length;
            AdlantisAd[] access$200 = AdlantisView.this.adsForCurrentOrientation();
            if (access$200 != null && (length = access$200.length) != 0) {
                int i2 = i >= length ? 0 : i;
                AdlantisAd adlantisAd = access$200[i2];
                AdlantisView.this.adManager.adIndex = i2;
                AdlantisAd adlantisAd2 = access$200[(i2 + 1) % length];
                if (this.ad != null) {
                    this.ad.viewingEnded();
                }
                this.ad = adlantisAd;
                this.ad.viewingStarted();
                int adType = this.ad.adType();
                if (adType == 1) {
                    setDisplayedChild(0);
                    this.adAltText.setText(this.ad.altTextString(this));
                    Drawable loadDrawable = AdlantisView.this.adManager.asyncImageLoader().loadDrawable(this.ad.bannerURLForCurrentOrientation(this), new AsyncImageLoader.ImageLoadedCallback() {
                        public void imageLoaded(Drawable drawable, String str) {
                            if (drawable != null) {
                                Log.d("AdlantisAdView", "setAdByIndex.imageLoaded=" + str);
                                AdlantisAdView.this.setBannerDrawable(drawable, true);
                            }
                        }
                    });
                    setBannerDrawable(loadDrawable, false);
                    if (loadDrawable == null && this.adAltText != this.adBannerViewFlipper.getCurrentView()) {
                        this.adBannerViewFlipper.setDisplayedChild(1);
                    }
                } else if (adType == 2) {
                    setDisplayedChild(1);
                    setIconDrawable(AdlantisView.this.adManager.asyncImageLoader().loadDrawable(this.ad.iconURL(), new AsyncImageLoader.ImageLoadedCallback() {
                        public void imageLoaded(Drawable drawable, String str) {
                            AdlantisAdView.this.setIconDrawable(drawable);
                        }
                    }));
                    this.adtext.setTextAndSize(this.ad.textAdString());
                }
                AdlantisView.this.adManager.asyncImageLoader().loadDrawable(adlantisAd2.imageURL(this), null);
            }
        }
    }

    public enum AnimationType {
        NONE,
        FADE,
        SLIDE_FROM_RIGHT,
        SLIDE_FROM_LEFT
    }

    private class SizeFitTextView extends TextView {
        private float maxTextSize = 20.0f;
        private float minTextSize = 9.0f;

        public SizeFitTextView(Context context) {
            super(context);
        }

        private void refitText(String str, int i) {
            float f;
            if (i > 0) {
                int paddingLeft = (i - getPaddingLeft()) - getPaddingRight();
                float f2 = this.maxTextSize;
                TextPaint paint = getPaint();
                while (true) {
                    if (f2 <= this.minTextSize || paint.measureText(str) <= ((float) paddingLeft)) {
                        f = f2;
                    } else {
                        f2 -= 1.0f;
                        if (f2 <= this.minTextSize) {
                            f = this.minTextSize;
                            break;
                        }
                        setTextSize(f2);
                    }
                }
                setTextSize(f);
            }
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            if (i != i3) {
                refitText(getText().toString(), i);
            }
        }

        public void setTextAndSize(String str) {
            setTextSize(this.maxTextSize);
            super.setText(str);
            refitText(str, getWidth());
        }
    }

    public AdlantisView(Context context) {
        super(context);
        commonInit();
    }

    public AdlantisView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/jp.Adlantis.Android", "publisherID");
        if (attributeValue != null) {
            setPublisherID(attributeValue);
        }
        commonInit();
    }

    /* access modifiers changed from: private */
    public void adCountChanged() {
        AdlantisAd[] adsForCurrentOrientation = adsForCurrentOrientation();
        int length = adsForCurrentOrientation != null ? adsForCurrentOrientation.length : 0;
        if (length > 0 && this.previousAdCount == 0) {
            setVisibility(0);
            startAnimation(fadeInAnimation());
        } else if (length == 0 && this.previousAdCount > 0) {
            startAnimation(fadeOutAnimation());
            setVisibility(4);
        }
        this.previousAdCount = length;
    }

    /* access modifiers changed from: private */
    public long adDisplayInterval() {
        return this.adManager.adDisplayInterval;
    }

    private long adFetchInterval() {
        return this.adManager.adFetchInterval;
    }

    /* access modifiers changed from: private */
    public AdManager adManager() {
        if (this.adManager == null) {
            this.adManager = AdManager.getInstance();
        }
        return this.adManager;
    }

    /* access modifiers changed from: private */
    public AdlantisAd[] adsForCurrentOrientation() {
        return adManager().adsForOrientation(getResources().getConfiguration().orientation);
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void commonInit() {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            android.content.Context r0 = r8.getContext()
            jp.Adlantis.Android.AdlantisAd[] r1 = r8.adsForCurrentOrientation()
            if (r1 == 0) goto L_0x000f
            int r2 = r1.length
            if (r2 != 0) goto L_0x0013
        L_0x000f:
            r2 = 4
            r8.setVisibility(r2)
        L_0x0013:
            android.graphics.drawable.Drawable r2 = r8.getBackground()
            if (r2 != 0) goto L_0x001e
            int r2 = jp.Adlantis.Android.AdlantisView.defaultBackgroundColor
            r8.setBackgroundColor(r2)
        L_0x001e:
            r2 = 0
            android.content.pm.PackageManager r3 = r0.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ NameNotFoundException -> 0x00c3 }
            r4 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r3 = r3.getApplicationInfo(r0, r4)     // Catch:{ NameNotFoundException -> 0x00c3 }
            if (r3 == 0) goto L_0x00dc
            android.os.Bundle r0 = r3.metaData     // Catch:{ NameNotFoundException -> 0x00c3 }
            if (r0 == 0) goto L_0x00dc
            jp.Adlantis.Android.AdManager r0 = r8.adManager()     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String[] r0 = r0.testAdRequestUrls     // Catch:{ NameNotFoundException -> 0x00c3 }
            if (r0 != 0) goto L_0x0053
            android.os.Bundle r0 = r3.metaData     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r4 = "Adlantis_adRequestUrl"
            java.lang.Object r0 = r0.get(r4)     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NameNotFoundException -> 0x00c3 }
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ NameNotFoundException -> 0x00c3 }
            r5 = 0
            r4[r5] = r0     // Catch:{ NameNotFoundException -> 0x00c3 }
            if (r0 == 0) goto L_0x0053
            jp.Adlantis.Android.AdManager r0 = r8.adManager()     // Catch:{ NameNotFoundException -> 0x00c3 }
            r0.testAdRequestUrls = r4     // Catch:{ NameNotFoundException -> 0x00c3 }
        L_0x0053:
            android.os.Bundle r0 = r3.metaData     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r4 = "Adlantis_keywords"
            java.lang.Object r0 = r0.get(r4)     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NameNotFoundException -> 0x00c3 }
            if (r0 == 0) goto L_0x0066
            jp.Adlantis.Android.AdManager r4 = r8.adManager()     // Catch:{ NameNotFoundException -> 0x00c3 }
            r4.setKeywords(r0)     // Catch:{ NameNotFoundException -> 0x00c3 }
        L_0x0066:
            android.os.Bundle r0 = r3.metaData     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r4 = "Adlantis_host"
            java.lang.Object r0 = r0.get(r4)     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NameNotFoundException -> 0x00c3 }
            if (r0 == 0) goto L_0x0076
            jp.Adlantis.Android.AdManager r4 = r8.adManager     // Catch:{ NameNotFoundException -> 0x00c3 }
            r4.host = r0     // Catch:{ NameNotFoundException -> 0x00c3 }
        L_0x0076:
            android.os.Bundle r0 = r3.metaData     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r3 = "Adlantis_Publisher_ID"
            java.lang.Object r0 = r0.get(r3)     // Catch:{ NameNotFoundException -> 0x00c3 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NameNotFoundException -> 0x00c3 }
        L_0x0080:
            jp.Adlantis.Android.AdManager r2 = r8.adManager()
            java.lang.String r2 = r2.getPublisherID()
            if (r2 != 0) goto L_0x00de
            if (r0 == 0) goto L_0x00ea
            r8.setPublisherID(r0)
            r0 = r6
        L_0x0090:
            java.lang.String r1 = android.os.Build.VERSION.SDK
            int r1 = java.lang.Integer.parseInt(r1)
            r2 = 7
            if (r1 < r2) goto L_0x00ec
            jp.Adlantis.Android.AdlantisView$3 r1 = new jp.Adlantis.Android.AdlantisView$3
            r1.<init>()
            boolean r1 = r8.post(r1)
        L_0x00a2:
            if (r1 != 0) goto L_0x00a7
            r8.setupLayout()
        L_0x00a7:
            if (r0 == 0) goto L_0x00ac
            r8.startTimers()
        L_0x00ac:
            jp.Adlantis.Android.AdManager r0 = r8.adManager()
            java.lang.String r0 = r0.getPublisherID()
            if (r0 != 0) goto L_0x00c2
            jp.Adlantis.Android.AdlantisView$4 r0 = new jp.Adlantis.Android.AdlantisView$4
            r0.<init>()
            android.os.Handler r1 = r8.mHandler
            long r2 = r8.idNotSpecifiedWarningInterval
            r1.postDelayed(r0, r2)
        L_0x00c2:
            return
        L_0x00c3:
            r0 = move-exception
            java.lang.String r3 = "AdlantisView"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "commonInit"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r3, r0)
        L_0x00dc:
            r0 = r2
            goto L_0x0080
        L_0x00de:
            if (r1 == 0) goto L_0x00e7
            int r0 = r1.length
            if (r0 <= 0) goto L_0x00e7
            r8.doDeferredAdSetup = r7
            r0 = r7
            goto L_0x0090
        L_0x00e7:
            r8.connect()
        L_0x00ea:
            r0 = r6
            goto L_0x0090
        L_0x00ec:
            r1 = r6
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.Adlantis.Android.AdlantisView.commonInit():void");
    }

    /* access modifiers changed from: private */
    public Animation fadeInAnimation() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) this.animationDuration);
        return alphaAnimation;
    }

    /* access modifiers changed from: private */
    public Animation fadeOutAnimation() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration((long) this.animationDuration);
        return alphaAnimation;
    }

    private boolean handleUserEvent() {
        String urlString = this.ad.urlString();
        if (urlString == null) {
            return false;
        }
        this.processIndicator.setVisibility(0);
        this.ad.sendImpressionCount();
        adManager().handleClickRequest(urlString, new AdManager.AdManagerClickUrlProcessedCallback() {
            public void clickProcessed(Uri uri) {
                AdlantisView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                AdlantisView.this.processIndicator.setVisibility(4);
            }
        });
        return true;
    }

    private Animation inFromLeftAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) this.animationDuration);
        return translateAnimation;
    }

    private Animation inFromRightAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) this.animationDuration);
        return translateAnimation;
    }

    private boolean inView(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        boolean z = x >= 0.0f && x <= ((float) getWidth()) && y >= 0.0f && y <= ((float) getHeight());
        return z ? motionEvent.getEdgeFlags() == 0 : z;
    }

    private Animation outToLeftAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) this.animationDuration);
        return translateAnimation;
    }

    private Animation outToRightAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) this.animationDuration);
        return translateAnimation;
    }

    private void setButtonState(boolean z) {
        if (z != this.buttonPressed) {
            if (z) {
                this.touchHighlight.setVisibility(0);
                this.touchHighlight.startAnimation(touchFadeIn());
                setPressed(true);
            } else {
                this.touchHighlight.startAnimation(touchFadeOut());
                setPressed(false);
            }
            this.buttonPressed = z;
        }
    }

    /* access modifiers changed from: private */
    public void setupLayout() {
        Context context = getContext();
        this.rootLayout = new ViewFlipper(context);
        setClickable(true);
        this.rootLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        addView(this.rootLayout);
        this.adViews = new AdlantisAdView[2];
        this.adViews[0] = new AdlantisAdView(context);
        this.adViews[1] = new AdlantisAdView(context);
        this.adViews[0].setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.adViews[1].setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.rootLayout.addView(this.adViews[0]);
        this.rootLayout.addView(this.adViews[1]);
        this.touchHighlight = new View(context);
        this.touchHighlight.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.touchHighlight.setBackgroundColor(1728053247);
        this.touchHighlight.setVisibility(4);
        addView(this.touchHighlight);
        this.processIndicator = new ProgressBar(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams.addRule(13);
        this.processIndicator.setLayoutParams(layoutParams);
        this.processIndicator.setIndeterminate(true);
        this.processIndicator.setVisibility(4);
        addView(this.processIndicator);
        this.layoutComplete = true;
        setAnimationType(AnimationType.FADE);
        if (this.doDeferredAdSetup) {
            int i = this.adManager.adIndex;
            this.currentAdIndex = i;
            setAdByIndex(i);
        }
    }

    /* access modifiers changed from: private */
    public void startTimers() {
        AdlantisAd[] adsForCurrentOrientation = adsForCurrentOrientation();
        if (!(this.adManager == null || adsForCurrentOrientation == null || adsForCurrentOrientation.length <= 0)) {
            this.mHandler.removeCallbacks(this.mRotateAdTask);
            this.mHandler.postDelayed(this.mRotateAdTask, adDisplayInterval());
        }
        this.mHandler.removeCallbacks(this.mUpdateAdsTask);
        this.mHandler.postDelayed(this.mUpdateAdsTask, adFetchInterval());
    }

    private void stopTimers() {
        this.mHandler.removeCallbacks(this.mRotateAdTask);
        this.mHandler.removeCallbacks(this.mUpdateAdsTask);
    }

    public void clearAds() {
        this.mHandler.removeCallbacks(this.mRotateAdTask);
        adManager().ads = null;
        adCountChanged();
    }

    public void connect() {
        this.previousAdCount = 0;
        AdlantisAd[] adsForCurrentOrientation = adsForCurrentOrientation();
        if (adsForCurrentOrientation != null) {
            this.previousAdCount = adsForCurrentOrientation.length;
        }
        if (adManager().getPublisherID() != null) {
            Log.d("AdlantisView", "AdlantisView.connect");
            adManager().connect(getContext(), new AdManager.AdManagerCallback() {
                public void adsLoaded(AdManager adManager) {
                    AdlantisAd[] access$200 = AdlantisView.this.adsForCurrentOrientation();
                    AdlantisView.this.adCountChanged();
                    if (access$200 != null && access$200.length > 0) {
                        int unused = AdlantisView.this.currentAdIndex = adManager.adIndex;
                        if (AdlantisView.this.layoutComplete) {
                            AdlantisView.this.setAdByIndex(AdlantisView.this.currentAdIndex);
                        } else {
                            boolean unused2 = AdlantisView.this.doDeferredAdSetup = true;
                        }
                    }
                    AdlantisView.this.startTimers();
                }
            });
            return;
        }
        Log.e("AdlantisView", "AdlantisView: can't connect because publisherID hasn't been set.");
    }

    public boolean isNetworkAvailable() {
        return AdManager.isNetworkAvailable(getContext());
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        startTimers();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopTimers();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            stopTimers();
            setButtonState(true);
        } else if (action == 2) {
            setButtonState(inView(motionEvent));
        } else if (action == 4 || action == 3) {
            setButtonState(false);
            startTimers();
        } else if (action == 1) {
            setButtonState(false);
            if (inView(motionEvent)) {
                handleUserEvent();
            } else {
                startTimers();
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        switch (i) {
            case 0:
                startTimers();
                return;
            case 4:
                stopTimers();
                return;
            case ImageRegister.LOGO_RIGHT:
                stopTimers();
                return;
            default:
                return;
        }
    }

    public void sendConversionTag(String str) {
        adManager().sendConversionTag(getContext(), str);
    }

    public void setAdByIndex(int i) {
        AdlantisAd[] adsForCurrentOrientation;
        if (this.rootLayout != null && (adsForCurrentOrientation = adsForCurrentOrientation()) != null && adsForCurrentOrientation.length != 0) {
            int i2 = i >= adsForCurrentOrientation.length ? 0 : i;
            AdlantisAd adlantisAd = adsForCurrentOrientation[i2];
            (this.rootLayout.getCurrentView() == this.adViews[0] ? this.adViews[1] : this.adViews[0]).setAdByIndex(i2);
            this.ad = adlantisAd;
            this.rootLayout.showNext();
        }
    }

    public void setAnimationType(AnimationType animationType) {
        if (this.rootLayout != null) {
            switch (animationType) {
                case NONE:
                    this.rootLayout.setInAnimation(null);
                    this.rootLayout.setOutAnimation(null);
                    return;
                case FADE:
                    this.rootLayout.setInAnimation(fadeInAnimation());
                    this.rootLayout.setOutAnimation(fadeOutAnimation());
                    return;
                case SLIDE_FROM_RIGHT:
                    this.rootLayout.setInAnimation(inFromRightAnimation());
                    this.rootLayout.setOutAnimation(outToLeftAnimation());
                    return;
                case SLIDE_FROM_LEFT:
                    this.rootLayout.setInAnimation(inFromLeftAnimation());
                    this.rootLayout.setOutAnimation(outToRightAnimation());
                    return;
                default:
                    return;
            }
        }
    }

    public void setKeywords(String str) {
        adManager().setKeywords(str);
    }

    public void setPublisherID(String str) {
        adManager().setPublisherID(str);
        if (str != null) {
            connect();
        }
    }

    public void showNextAd() {
        AdlantisAd[] adsForCurrentOrientation;
        if (this.adManager != null && (adsForCurrentOrientation = adsForCurrentOrientation()) != null && adsForCurrentOrientation.length > 0) {
            this.currentAdIndex = (this.currentAdIndex + 1) % adsForCurrentOrientation.length;
            setAdByIndex(this.currentAdIndex);
        }
    }

    /* access modifiers changed from: package-private */
    public Animation touchFadeIn() {
        Animation fadeInAnimation = fadeInAnimation();
        fadeInAnimation.setDuration(150);
        fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        return fadeInAnimation;
    }

    /* access modifiers changed from: package-private */
    public Animation touchFadeOut() {
        Animation fadeOutAnimation = fadeOutAnimation();
        fadeOutAnimation.setDuration(150);
        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                AdlantisView.this.touchHighlight.setVisibility(4);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        return fadeOutAnimation;
    }
}
