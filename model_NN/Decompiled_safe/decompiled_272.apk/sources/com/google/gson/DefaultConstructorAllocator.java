package com.google.gson;

import java.lang.reflect.Constructor;

final class DefaultConstructorAllocator {
    private static final Constructor<Null> NULL_CONSTRUCTOR = createNullConstructor();
    private final Cache<Class<?>, Constructor<?>> constructorCache;

    public DefaultConstructorAllocator() {
        this(200);
    }

    public DefaultConstructorAllocator(int cacheSize) {
        this.constructorCache = new LruCache(cacheSize);
    }

    /* access modifiers changed from: package-private */
    public final boolean isInCache(Class<?> cacheKey) {
        return this.constructorCache.getElement(cacheKey) != null;
    }

    private static final Constructor<Null> createNullConstructor() {
        try {
            return getNoArgsConstructor(Null.class);
        } catch (Exception e) {
            return null;
        }
    }

    public <T> T newInstance(Class<T> c) throws Exception {
        Constructor<T> constructor = findConstructor(c);
        if (constructor != null) {
            return constructor.newInstance(new Object[0]);
        }
        return null;
    }

    private <T> Constructor<T> findConstructor(Class<T> c) {
        Constructor<T> cachedElement = this.constructorCache.getElement(c);
        if (cachedElement == null) {
            Constructor<T> noArgsConstructor = getNoArgsConstructor(c);
            if (noArgsConstructor != null) {
                this.constructorCache.addElement(c, noArgsConstructor);
            } else {
                this.constructorCache.addElement(c, NULL_CONSTRUCTOR);
            }
            return noArgsConstructor;
        } else if (cachedElement == NULL_CONSTRUCTOR) {
            return null;
        } else {
            return cachedElement;
        }
    }

    private static <T> Constructor<T> getNoArgsConstructor(Class<T> c) {
        try {
            Constructor<T> declaredConstructor = c.getDeclaredConstructor(new Class[0]);
            declaredConstructor.setAccessible(true);
            return declaredConstructor;
        } catch (Exception e) {
            return null;
        }
    }

    private static final class Null {
        private Null() {
        }
    }
}
