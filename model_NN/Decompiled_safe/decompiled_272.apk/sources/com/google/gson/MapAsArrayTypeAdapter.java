package com.google.gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

final class MapAsArrayTypeAdapter extends BaseMapTypeAdapter implements JsonSerializer<Map<?, ?>>, JsonDeserializer<Map<?, ?>> {
    MapAsArrayTypeAdapter() {
    }

    public /* bridge */ /* synthetic */ JsonElement serialize(Object x0, Type x1, JsonSerializationContext x2) {
        return serialize((Map<?, ?>) ((Map) x0), x1, x2);
    }

    public Map<?, ?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Map<Object, Object> result = constructMapType(typeOfT, context);
        Type[] keyAndValueType = typeToTypeArguments(typeOfT);
        if (json.isJsonArray()) {
            JsonArray array = json.getAsJsonArray();
            for (int i = 0; i < array.size(); i++) {
                JsonArray entryArray = array.get(i).getAsJsonArray();
                result.put(context.deserialize(entryArray.get(0), keyAndValueType[0]), context.deserialize(entryArray.get(1), keyAndValueType[1]));
            }
            checkSize(array, array.size(), result, result.size());
        } else {
            JsonObject object = json.getAsJsonObject();
            for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
                result.put(context.deserialize(new JsonPrimitive((String) entry.getKey()), keyAndValueType[0]), context.deserialize((JsonElement) entry.getValue(), keyAndValueType[1]));
            }
            checkSize(object, object.entrySet().size(), result, result.size());
        }
        return result;
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:24:0x00ab */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v0, resolved type: com.google.gson.JsonArray} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: com.google.gson.JsonObject} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: com.google.gson.JsonArray} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: com.google.gson.JsonArray} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.gson.JsonElement serialize(java.util.Map<?, ?> r12, java.lang.reflect.Type r13, com.google.gson.JsonSerializationContext r14) {
        /*
            r11 = this;
            java.lang.reflect.Type[] r5 = r11.typeToTypeArguments(r13)
            r8 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.util.Set r9 = r12.entrySet()
            java.util.Iterator r3 = r9.iterator()
        L_0x0012:
            boolean r9 = r3.hasNext()
            if (r9 == 0) goto L_0x004b
            java.lang.Object r0 = r3.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r9 = r0.getKey()
            r10 = 0
            r10 = r5[r10]
            com.google.gson.JsonElement r4 = serialize(r14, r9, r10)
            boolean r9 = r4.isJsonObject()
            if (r9 != 0) goto L_0x0035
            boolean r9 = r4.isJsonArray()
            if (r9 == 0) goto L_0x0049
        L_0x0035:
            r9 = 1
        L_0x0036:
            r8 = r8 | r9
            r6.add(r4)
            java.lang.Object r9 = r0.getValue()
            r10 = 1
            r10 = r5[r10]
            com.google.gson.JsonElement r9 = serialize(r14, r9, r10)
            r6.add(r9)
            goto L_0x0012
        L_0x0049:
            r9 = 0
            goto L_0x0036
        L_0x004b:
            if (r8 == 0) goto L_0x0078
            com.google.gson.JsonArray r7 = new com.google.gson.JsonArray
            r7.<init>()
            r2 = 0
        L_0x0053:
            int r9 = r6.size()
            if (r2 >= r9) goto L_0x00ab
            com.google.gson.JsonArray r1 = new com.google.gson.JsonArray
            r1.<init>()
            java.lang.Object r11 = r6.get(r2)
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11
            r1.add(r11)
            int r9 = r2 + 1
            java.lang.Object r11 = r6.get(r9)
            com.google.gson.JsonElement r11 = (com.google.gson.JsonElement) r11
            r1.add(r11)
            r7.add(r1)
            int r2 = r2 + 2
            goto L_0x0053
        L_0x0078:
            com.google.gson.JsonObject r7 = new com.google.gson.JsonObject
            r7.<init>()
            r2 = 0
        L_0x007e:
            int r9 = r6.size()
            if (r2 >= r9) goto L_0x009c
            java.lang.Object r13 = r6.get(r2)
            com.google.gson.JsonElement r13 = (com.google.gson.JsonElement) r13
            java.lang.String r9 = r13.getAsString()
            int r10 = r2 + 1
            java.lang.Object r13 = r6.get(r10)
            com.google.gson.JsonElement r13 = (com.google.gson.JsonElement) r13
            r7.add(r9, r13)
            int r2 = r2 + 2
            goto L_0x007e
        L_0x009c:
            int r9 = r12.size()
            java.util.Set r10 = r7.entrySet()
            int r10 = r10.size()
            r11.checkSize(r12, r9, r7, r10)
        L_0x00ab:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.MapAsArrayTypeAdapter.serialize(java.util.Map, java.lang.reflect.Type, com.google.gson.JsonSerializationContext):com.google.gson.JsonElement");
    }

    private Type[] typeToTypeArguments(Type typeOfT) {
        if (typeOfT instanceof ParameterizedType) {
            Type[] typeArguments = ((ParameterizedType) typeOfT).getActualTypeArguments();
            if (typeArguments.length == 2) {
                return typeArguments;
            }
            throw new IllegalArgumentException("MapAsArrayTypeAdapter cannot handle " + typeOfT);
        }
        return new Type[]{Object.class, Object.class};
    }

    private void checkSize(Object input, int inputSize, Object output, int outputSize) {
        if (inputSize != outputSize) {
            throw new JsonSyntaxException("Input size " + inputSize + " != output size " + outputSize + " for input " + input + " and output " + output);
        }
    }
}
