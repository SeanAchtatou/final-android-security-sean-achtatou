package com.crossfd.android.NinjaExiter.utility;

public class Constants {
    public static final String RANKING_CATEGORY_ALL = "all";
    public static final String RANKING_CATEGORY_MONTHLY = "monthly";
    public static final String RANKING_CATEGORY_TODAY = "today";
    public static final String RANKING_CATEGORY_WEEKLY = "weekly";
    public static final String RANKING_CATEGORY_YESTERDAY = "yesterday";
    public static final String RANKING_COUNTRY_DOMESTIC = "JP";
    public static final String RANKING_COUNTRY_LOCAL = "JP";
    public static final String RANKING_COUNTRY_WORLD = "world";
    public static final String URL_GET_RANKING = "http://198.104.58.57:8080/ninjaexiterweb/get_ranking.rs";
    public static final String URL_INSER_POINT = "http://198.104.58.57:8080/ninjaexiterweb/submit_point.rs";
    public static final String URL_JUMP_COUNT = "http://198.104.58.57:8080/ninjaexiterweb/get_jump_count.rs";
    public static final String URL_PK_INITIAL_PART = "http://198.104.58.57:8080/ninjaexiterweb/get_pk_initial_part.rs";
    public static final String URL_PURCHASED_PRD_LIST = "http://198.104.58.57:8080/ninjaexiterweb/purchased_product_list.rs";
    public static final String URL_PURCHASE_DELETE = "http://198.104.58.57:8080/ninjaexiterweb/purchase_delete.rs";
    public static final String URL_PURCHASE_INSERT = "http://198.104.58.57:8080/ninjaexiterweb/purchase_insert.rs";
}
