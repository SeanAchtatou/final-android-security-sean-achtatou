package cross.field.NinjaExiter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import com.crossfd.android.NinjaExiter.utility.Constants;
import com.crossfd.android.NinjaExiter.utility.RestWebServiceClient;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import cross.field.ranking.RankingActivity;
import cross.field.stage.StageActivity;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class MainActivity extends Activity {
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private String country = "";
    private int disp_height;
    private int disp_width;
    /* access modifiers changed from: private */
    public Intent intent;
    private float ratio_height;
    private float ratio_width;
    Analytics tracker;

    public void getDisplaySize() {
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        setDispWidth(disp.getWidth());
        setDispHeight(disp.getHeight());
    }

    public void getDisplayRatio() {
        getDisplaySize();
        setRatioWidth(((float) getDispWidth()) / 480.0f);
        setRatioHeight(((float) getDispHeight()) / 854.0f);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        getDisplaySize();
        getDisplayRatio();
        setContentView((int) R.layout.main);
        new MainLayout(this);
        this.tracker = new Analytics(GoogleAnalyticsTracker.getInstance(), this);
        this.tracker.trackPageView("Title");
    }

    public void onResume() {
        super.onResume();
        this.tracker.trackPageView("Title");
        SharedPreferences pref = getSharedPreferences(PreferenceKeys.PREFERENCE, 3);
        boolean record = pref.getBoolean(PreferenceKeys.BEST_FLAG, false);
        boolean daily = pref.getBoolean(PreferenceKeys.DAILY_FLAG, false);
        if (record) {
            this.intent = new Intent(this, RankingActivity.class);
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("NewRecord!").setMessage("Sending score").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MainActivity.this.startActivity(MainActivity.this.intent);
                    MainActivity.this.insert(0);
                }
            }).show();
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(PreferenceKeys.BEST_FLAG, false);
            editor.commit();
        } else if (daily) {
            this.intent = new Intent(this, RankingActivity.class);
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("Today's NewRecord!").setMessage("Sending score").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MainActivity.this.startActivity(MainActivity.this.intent);
                    MainActivity.this.insert(1);
                }
            }).show();
            SharedPreferences.Editor editor2 = pref.edit();
            editor2.putBoolean(PreferenceKeys.DAILY_FLAG, false);
            editor2.commit();
        }
    }

    public void insert(int mode) {
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        SharedPreferences pref = getSharedPreferences(PreferenceKeys.PREFERENCE, 3);
        String name = pref.getString(PreferenceKeys.NAME, "NoData");
        String record = String.valueOf((int) pref.getLong("Score0", -1));
        if (mode == 0) {
            record = String.valueOf((int) pref.getLong("Score0", -1));
        }
        if (mode == 1) {
            record = String.valueOf(pref.getInt(PreferenceKeys.DAILY_RECORD, -1));
        }
        RestWebServiceClient restClient = new RestWebServiceClient(Constants.URL_INSER_POINT);
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("user_name", name));
        nameValuePairs.add(new BasicNameValuePair("user_point", record));
        nameValuePairs.add(new BasicNameValuePair("country_code", getResources().getConfiguration().locale.getCountry()));
        nameValuePairs.add(new BasicNameValuePair("device_id", tel.getDeviceId()));
        try {
            restClient.webPost("", nameValuePairs);
        } catch (Exception e) {
            showDialog(this, "Failure", "Transmission failure");
        }
    }

    public void buttonStart(View v) {
        this.tracker.trackEvent("Title", "Click", "Start", 1);
        new EditText(this);
        String str = ((SpannableStringBuilder) ((EditText) findViewById(R.id.editText_edit)).getText()).toString();
        if (str.length() != 0) {
            SharedPreferences.Editor editor = getSharedPreferences(PreferenceKeys.PREFERENCE, 0).edit();
            editor.putString(PreferenceKeys.NAME, str);
            editor.commit();
            startActivity(new Intent(this, StageActivity.class));
            return;
        }
        showDialog(this, "Error", "Input name");
    }

    public void buttonRanking(View v) {
        this.tracker.trackEvent("Title", "Click", "Ranking", 1);
        new EditText(this);
        String str = ((SpannableStringBuilder) ((EditText) findViewById(R.id.editText_edit)).getText()).toString();
        if (str.length() != 0) {
            SharedPreferences.Editor editor = getSharedPreferences(PreferenceKeys.PREFERENCE, 0).edit();
            editor.putString(PreferenceKeys.NAME, str);
            editor.commit();
            startActivity(new Intent(this, RankingActivity.class));
            return;
        }
        showDialog(this, "Error", "Input name");
    }

    public void buttonMore(View v) {
        this.tracker.trackEvent("Title", "Click", "More", 1);
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://goo.gl/M7jDj")));
    }

    public void buttonBuy(View v) {
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            event.getKeyCode();
        }
        return super.dispatchKeyEvent(event);
    }

    public void showDialog(Context context, String title, String text) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(title);
        ad.setMessage(text);
        ad.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
        ad.show();
    }

    public void setDispWidth(int disp_width2) {
        this.disp_width = disp_width2;
    }

    public int getDispWidth() {
        return this.disp_width;
    }

    public void setDispHeight(int disp_height2) {
        this.disp_height = disp_height2;
    }

    public int getDispHeight() {
        return this.disp_height;
    }

    public void setRatioWidth(float ratio_width2) {
        this.ratio_width = ratio_width2;
    }

    public float getRatioWidth() {
        return this.ratio_width;
    }

    public void setRatioHeight(float ratio_height2) {
        this.ratio_height = ratio_height2;
    }

    public float getRatioHeight() {
        return this.ratio_height;
    }

    public void setCountry() {
        this.country = getResources().getConfiguration().locale.getCountry();
    }

    public void setCountry(String country2) {
        this.country = country2;
    }

    public String getCountry() {
        return this.country;
    }
}
