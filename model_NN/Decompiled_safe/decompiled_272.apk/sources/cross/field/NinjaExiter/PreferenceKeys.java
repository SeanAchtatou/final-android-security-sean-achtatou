package cross.field.NinjaExiter;

public class PreferenceKeys {
    public static final String BEST_FLAG = "Best.Flag";
    public static final String BEST_RECORD = "Best.Record";
    public static final String DAILY_FLAG = "Daily.Flag";
    public static final String DAILY_RECORD = "Daily.Record";
    public static final String DAILY_TIME = "Daily.Time";
    public static final String JUMP_FLAG = "Jump.Flag";
    public static final String NAME = "Name";
    public static final String PREFERENCE = "Preference";
    public static final String SCENE = "Scene";
    public static final String SCORE = "Score";
}
