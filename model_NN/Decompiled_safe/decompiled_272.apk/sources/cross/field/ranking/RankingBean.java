package cross.field.ranking;

import android.graphics.drawable.Drawable;

public class RankingBean {
    private String country = "";
    private String date = "";
    private Drawable image;
    private String name = "";
    private String number = "";
    private String time = "";

    public void setNumber(String number2) {
        this.number = number2;
    }

    public String getNumber() {
        return this.number;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public String getTime() {
        return this.time;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getDate() {
        return this.date;
    }

    public void setCountry(String country2) {
        this.country = country2;
    }

    public String getCountry() {
        return this.country;
    }

    public void setImage(Drawable image2) {
        this.image = image2;
    }

    public Drawable getImage() {
        return this.image;
    }
}
