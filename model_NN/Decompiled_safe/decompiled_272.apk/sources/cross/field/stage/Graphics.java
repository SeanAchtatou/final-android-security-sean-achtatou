package cross.field.stage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class Graphics extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    private Canvas canvas;
    private Context context;
    public int disp_height;
    public int disp_width;
    private SurfaceHolder holder;
    private Drawable[] image;
    private Paint paint = new Paint();
    public float ratio_height;
    public float ratio_width;

    public Graphics(SurfaceHolder holder2, Context context2) {
        super(context2);
        this.context = context2;
        this.holder = holder2;
        this.paint.setAntiAlias(true);
        this.image = new ImageRegister(context2).getImage();
        getDisplaySize();
        getDisplayRatio();
    }

    public void getDisplaySize() {
        Display disp = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay();
        this.disp_width = disp.getWidth();
        this.disp_height = disp.getHeight();
    }

    public void getDisplayRatio() {
        getDisplaySize();
        this.ratio_width = ((float) this.disp_width) / 480.0f;
        this.ratio_height = ((float) this.disp_height) / 854.0f;
    }

    public void drawImage(int id, int x, int y, int w, int h) {
        this.image[id].setBounds(x, y, x + w, y + h);
        this.image[id].draw(this.canvas);
    }

    public void drawAlpha(int id, int x, int y, int w, int h, int alpha) {
        this.image[id].setBounds(x, y, x + w, y + h);
        this.image[id].setAlpha(alpha);
        this.image[id].draw(this.canvas);
    }

    public void fade(int alpha) {
        for (Drawable alpha2 : this.image) {
            alpha2.setAlpha(alpha);
        }
    }

    public void drawString(String string, int x, int y, int font, int color) {
        this.paint.setTextSize((float) font);
        this.paint.setColor(color);
        this.canvas.drawText(string, (float) x, (float) y, this.paint);
    }

    public void drawSquare(int x, int y, int w, int h, int color, boolean fill) {
        this.paint.setColor(color);
        this.canvas.drawRect(new RectF((float) x, (float) y, (float) (x + w), (float) (y + h)), this.paint);
        if (fill) {
            this.paint.setStyle(Paint.Style.FILL);
        } else {
            this.paint.setStyle(Paint.Style.STROKE);
        }
    }

    public void drawRoundedSquare(int x, int y, int w, int h, float round, int color, boolean fill) {
        this.paint.setColor(color);
        this.canvas.drawRoundRect(new RectF((float) x, (float) y, (float) (x + w), (float) (y + h)), round, round, this.paint);
        if (fill) {
            this.paint.setStyle(Paint.Style.FILL);
        } else {
            this.paint.setStyle(Paint.Style.STROKE);
        }
    }

    public void lock() {
        this.canvas = this.holder.lockCanvas();
    }

    public void unlock() {
        this.holder.unlockCanvasAndPost(this.canvas);
    }

    public void setcolor(int color) {
        this.paint.setColor(color);
    }

    public void setfontsize(int fontsize) {
        this.paint.setTextSize((float) fontsize);
    }

    public int stringwidth(String string) {
        return (int) this.paint.measureText(string);
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
    }

    public void run() {
    }
}
