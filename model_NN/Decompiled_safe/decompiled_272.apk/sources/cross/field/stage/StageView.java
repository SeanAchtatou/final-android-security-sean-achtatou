package cross.field.stage;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.ArrayList;

public class StageView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static final int END = 4;
    public static final int INIT = 0;
    public static final int PLAY = 1;
    public static final float PLAYER_H = 80.0f;
    public static final float PLAYER_W = 80.0f;
    public static final float PLAYER_X = 120.0f;
    public static final float PLAYER_Y = 520.0f;
    public static final int POINT_H = 30;
    public static final int POINT_W = 30;
    public static final int POINT_X = 10;
    public static final int POINT_Y = 10;
    public static final int RECORD = 3;
    public static final float SCROLL_SPEED = 15.0f;
    private StageActivity activity;
    private Button button;
    private int button_h = 0;
    private int button_w = 0;
    private int button_x = 0;
    private int button_y = 0;
    private Graphics g;
    private SurfaceHolder holder;
    ItemManager item;
    private Player player;
    private Point point;
    private int point_h = 30;
    private int point_w = 30;
    private int point_x = 10;
    private int point_y = 10;
    RoofManager roof;
    private int scene;
    private ArrayList<Long> score;
    private Thread thread;

    public StageView(Context context, SurfaceView sv) {
        super(context);
        this.holder = sv.getHolder();
        this.holder.addCallback(this);
        this.holder.setFixedSize(getWidth(), getHeight());
        setFocusable(true);
        setClickable(true);
        this.g = new Graphics(this.holder, context);
        this.activity = (StageActivity) context;
        init();
    }

    public void init() {
        this.scene = 1;
        this.button_x = 0;
        this.button_y = (int) (((double) this.g.disp_height) * 0.8d);
        this.button_w = (int) (((double) this.g.disp_width) * 1.0d);
        this.button_h = (int) (((double) this.g.disp_height) * 0.1d);
        this.button = new Button(this.activity, this.g, 8, this.button_x, this.button_y, this.button_w, this.button_h, (int) (175.0f * this.g.ratio_width), (int) (this.g.ratio_height * 30.0f));
        this.point_x = (int) (this.g.ratio_width * 10.0f);
        this.point_y = (int) (this.g.ratio_width * 10.0f);
        this.point_w = (int) (this.g.ratio_width * 30.0f);
        this.point_h = (int) (this.g.ratio_width * 30.0f);
        this.point = new Point(this.g, this.point_x, this.point_y, this.point_w, this.point_h);
        this.player = new Player(this.g, 1, this.g.ratio_width * 120.0f, this.g.ratio_height * 520.0f, 80.0f * this.g.ratio_width, 80.0f * this.g.ratio_height);
        this.item = new ItemManager(this.g);
        this.roof = new RoofManager(this.g);
    }

    public void run() {
        while (this.thread != null) {
            update();
            draw();
            scene();
            try {
                Thread.sleep(50);
            } catch (Exception e) {
            }
        }
    }

    public void update() {
        switch (this.scene) {
            case 0:
                this.scene = 1;
                return;
            case 1:
                this.roof.action();
                this.item.action();
                this.player.action();
                this.point.addPoint();
                return;
            case 2:
            case 3:
            default:
                return;
        }
    }

    public void draw() {
        this.g.lock();
        this.g.drawSquare(0, 0, this.g.disp_width, this.g.disp_height, -16777216, true);
        this.g.drawImage(0, 0, 0, this.g.disp_width, this.g.disp_height);
        switch (this.scene) {
            case 0:
                this.player.draw();
                this.item.draw();
                this.roof.draw();
                this.button.draw();
                this.point.drawPoint(this.point.getPoint());
                break;
            case 1:
                this.player.draw();
                this.item.draw();
                this.roof.draw();
                this.button.draw();
                this.point.drawPoint(this.point.getPoint());
                hitPT(this.player, this.item);
                break;
            case 3:
                this.player.draw();
                this.item.draw();
                this.roof.draw();
                this.button.draw();
                this.point.drawPoint(this.point.getPoint());
                break;
            case 4:
                this.player.draw();
                this.item.draw();
                this.roof.draw();
                this.button.draw();
                this.point.drawPoint(this.point.getPoint());
                break;
        }
        this.g.unlock();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x02b0 A[LOOP:1: B:16:0x0132->B:36:0x02b0, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void scene() {
        /*
            r21 = this;
            r0 = r21
            int r0 = r0.scene
            r16 = r0
            switch(r16) {
                case 0: goto L_0x0009;
                case 1: goto L_0x000a;
                case 2: goto L_0x0009;
                case 3: goto L_0x005c;
                case 4: goto L_0x02e1;
                default: goto L_0x0009;
            }
        L_0x0009:
            return
        L_0x000a:
            r0 = r21
            cross.field.stage.Player r0 = r0.player
            r16 = r0
            float r16 = r16.getY()
            r0 = r21
            cross.field.stage.Player r0 = r0.player
            r17 = r0
            float r17 = r17.getH()
            r0 = r17
            double r0 = (double) r0
            r17 = r0
            r19 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            double r17 = r17 * r19
            r0 = r17
            float r0 = (float) r0
            r17 = r0
            float r16 = r16 + r17
            r0 = r21
            cross.field.stage.RoofManager r0 = r0.roof
            r17 = r0
            cross.field.stage.BaseObject r17 = r17.getRoof2()
            float r17 = r17.getY()
            r0 = r21
            cross.field.stage.RoofManager r0 = r0.roof
            r18 = r0
            cross.field.stage.BaseObject r18 = r18.getRoof2()
            float r18 = r18.getH()
            float r17 = r17 + r18
            int r16 = (r16 > r17 ? 1 : (r16 == r17 ? 0 : -1))
            if (r16 >= 0) goto L_0x0009
            r16 = 3
            r0 = r16
            r1 = r21
            r1.scene = r0
            goto L_0x0009
        L_0x005c:
            r0 = r21
            cross.field.stage.StageActivity r0 = r0.activity
            r16 = r0
            java.lang.String r17 = "Preference"
            r18 = 3
            android.content.SharedPreferences r11 = r16.getSharedPreferences(r17, r18)
            android.content.SharedPreferences$Editor r9 = r11.edit()
            java.util.ArrayList r16 = new java.util.ArrayList
            r16.<init>()
            r0 = r16
            r1 = r21
            r1.score = r0
            r10 = 0
        L_0x007a:
            r16 = 100
            r0 = r10
            r1 = r16
            if (r0 < r1) goto L_0x014e
        L_0x0081:
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r17 = r0
            long r17 = r17.getPoint()
            r19 = 10
            long r17 = r17 / r19
            java.lang.Long r17 = java.lang.Long.valueOf(r17)
            r16.add(r17)
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            java.util.Collections.sort(r16)
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            java.util.Collections.reverse(r16)
            r4 = 0
            java.lang.String r16 = "Score0"
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r16 = r0.getLong(r1, r2)     // Catch:{ Exception -> 0x02ec }
            r0 = r16
            int r0 = (int) r0
            r4 = r0
        L_0x00c0:
            java.lang.String r16 = "Best.Flag"
            r17 = 0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            java.util.Date r6 = new java.util.Date
            r6.<init>()
            long r16 = r6.getTime()
            r18 = 86400000(0x5265c00, double:4.2687272E-316)
            long r16 = r16 / r18
            r0 = r16
            double r0 = (double) r0
            r16 = r0
            double r16 = java.lang.Math.floor(r16)
            r0 = r16
            long r0 = (long) r0
            r7 = r0
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r16 = r0
            long r16 = r16.getPoint()
            r18 = 10
            long r16 = r16 / r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r4
            r1 = r16
            if (r0 >= r1) goto L_0x019f
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r17 = r0
            long r17 = r17.getPoint()
            r19 = 10
            long r17 = r17 / r19
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            java.lang.String r16 = "Daily.Time"
            r0 = r9
            r1 = r16
            r2 = r7
            r0.putLong(r1, r2)
            java.lang.String r16 = "Best.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
        L_0x0131:
            r10 = 0
        L_0x0132:
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r16 = r0
            int r16 = r16.size()
            r0 = r10
            r1 = r16
            if (r0 < r1) goto L_0x02b0
            r9.commit()
            r16 = 4
            r0 = r16
            r1 = r21
            r1.scene = r0
            goto L_0x0009
        L_0x014e:
            r16 = -1
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ef }
            java.lang.String r19 = "Score"
            r18.<init>(r19)     // Catch:{ Exception -> 0x02ef }
            r0 = r18
            r1 = r10
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ Exception -> 0x02ef }
            java.lang.String r18 = r18.toString()     // Catch:{ Exception -> 0x02ef }
            r19 = -1
            r0 = r11
            r1 = r18
            r2 = r19
            long r18 = r0.getLong(r1, r2)     // Catch:{ Exception -> 0x02ef }
            int r16 = (r16 > r18 ? 1 : (r16 == r18 ? 0 : -1))
            if (r16 == 0) goto L_0x0081
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ef }
            java.lang.String r17 = "Score"
            r16.<init>(r17)     // Catch:{ Exception -> 0x02ef }
            r0 = r16
            r1 = r10
            java.lang.StringBuilder r16 = r0.append(r1)     // Catch:{ Exception -> 0x02ef }
            java.lang.String r16 = r16.toString()     // Catch:{ Exception -> 0x02ef }
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r12 = r0.getLong(r1, r2)     // Catch:{ Exception -> 0x02ef }
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score     // Catch:{ Exception -> 0x02ef }
            r16 = r0
            java.lang.Long r17 = java.lang.Long.valueOf(r12)     // Catch:{ Exception -> 0x02ef }
            r16.add(r17)     // Catch:{ Exception -> 0x02ef }
        L_0x019b:
            int r10 = r10 + 1
            goto L_0x007a
        L_0x019f:
            java.lang.String r16 = "Daily.Time"
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r14 = r0.getLong(r1, r2)
            java.lang.String r16 = "Daily.Time"
            r17 = -1
            r0 = r11
            r1 = r16
            r2 = r17
            long r14 = r0.getLong(r1, r2)
            java.lang.String r16 = "Daily.Flag"
            r17 = 0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            r16 = -1
            int r16 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r16 != 0) goto L_0x0215
            java.lang.String r16 = "Daily.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r17 = r0
            long r17 = r17.getPoint()
            r19 = 10
            long r17 = r17 / r19
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            long r16 = r6.getTime()
            r18 = 86400000(0x5265c00, double:4.2687272E-316)
            long r16 = r16 / r18
            r0 = r16
            double r0 = (double) r0
            r16 = r0
            double r16 = java.lang.Math.floor(r16)
            r0 = r16
            long r0 = (long) r0
            r14 = r0
            java.lang.String r16 = "Daily.Time"
            r0 = r9
            r1 = r16
            r2 = r14
            r0.putLong(r1, r2)
            goto L_0x0131
        L_0x0215:
            int r16 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r16 >= 0) goto L_0x024b
            java.lang.String r16 = "Daily.Time"
            r0 = r9
            r1 = r16
            r2 = r7
            r0.putLong(r1, r2)
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r17 = r0
            long r17 = r17.getPoint()
            r19 = 10
            long r17 = r17 / r19
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            java.lang.String r16 = "Daily.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
        L_0x024b:
            int r16 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r16 != 0) goto L_0x0131
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r17 = r0
            long r17 = r17.getPoint()
            r19 = 10
            long r17 = r17 / r19
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r11
            r1 = r16
            r2 = r17
            int r5 = r0.getInt(r1, r2)
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r16 = r0
            long r16 = r16.getPoint()
            r18 = 10
            long r16 = r16 / r18
            r0 = r16
            int r0 = (int) r0
            r16 = r0
            r0 = r5
            r1 = r16
            if (r0 >= r1) goto L_0x0131
            java.lang.String r16 = "Daily.Record"
            r0 = r21
            cross.field.stage.Point r0 = r0.point
            r17 = r0
            long r17 = r17.getPoint()
            r19 = 10
            long r17 = r17 / r19
            r0 = r17
            int r0 = (int) r0
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putInt(r1, r2)
            java.lang.String r16 = "Daily.Flag"
            r17 = 1
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putBoolean(r1, r2)
            goto L_0x0131
        L_0x02b0:
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            java.lang.String r17 = "Score"
            r16.<init>(r17)
            r0 = r16
            r1 = r10
            java.lang.StringBuilder r16 = r0.append(r1)
            java.lang.String r16 = r16.toString()
            r0 = r21
            java.util.ArrayList<java.lang.Long> r0 = r0.score
            r17 = r0
            r0 = r17
            r1 = r10
            java.lang.Object r4 = r0.get(r1)
            java.lang.Long r4 = (java.lang.Long) r4
            long r17 = r4.longValue()
            r0 = r9
            r1 = r16
            r2 = r17
            r0.putLong(r1, r2)
            int r10 = r10 + 1
            goto L_0x0132
        L_0x02e1:
            r0 = r21
            cross.field.stage.StageActivity r0 = r0.activity
            r16 = r0
            r16.finish()
            goto L_0x0009
        L_0x02ec:
            r16 = move-exception
            goto L_0x00c0
        L_0x02ef:
            r16 = move-exception
            goto L_0x019b
        */
        throw new UnsupportedOperationException("Method not decompiled: cross.field.stage.StageView.scene():void");
    }

    public boolean touchEvent(MotionEvent event) {
        if (!(this.button.buttonEvent(event) == null || this.button.buttonEvent(event).getAction() != 0 || this.player == null)) {
            this.player.touchEvent(event);
            if (this.player.getMode() == 1) {
                this.button.setId(8);
            } else if (this.player.getMode() == 2) {
                this.button.setId(7);
            }
        }
        switch (event.getAction()) {
        }
        return false;
    }

    public int hitPT(Player player2, ItemManager item2) {
        int effect = 0;
        float top1 = player2.getY();
        float bottom1 = top1 + player2.getH();
        float left1 = player2.getX();
        float right1 = left1 + player2.getW();
        int j = 0;
        while (true) {
            if (j >= item2.getItem().size()) {
                break;
            }
            BaseObject i = item2.getItem().get(j);
            float top2 = i.getY();
            float bottom2 = top2 + ((float) (((double) i.getH()) * 0.7d));
            float left2 = i.getX();
            if (left1 > left2 + i.getW() || right1 < left2 || bottom1 < top2 || top1 > bottom2) {
                j++;
            } else {
                this.roof.setScaleTime(30);
                if (i.getId() == 20) {
                    effect = 20;
                    this.roof.getRoof().setMode(3);
                }
                if (i.getId() == 21) {
                    effect = 21;
                    this.roof.getRoof().setMode(2);
                }
                item2.getItem().remove(j);
            }
        }
        return effect;
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        this.thread = null;
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        this.thread = new Thread(this);
        this.thread.start();
    }
}
