package cross.field.stage;

public class BaseObject {
    private Graphics g;
    private float h = 0.0f;
    private int id = 0;
    private int mode = 0;
    private float w = 0.0f;
    private float x = 0.0f;
    private float xhit = 0.0f;
    private float xoff = 0.0f;
    private float xspeed = 0.0f;
    private float y = 0.0f;
    private float yhit = 0.0f;
    private float yoff = 0.0f;
    private float yspeed = 0.0f;

    public BaseObject(Graphics g2, int id2, float x2, float y2, float w2, float h2) {
        setG(g2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
        setId(id2);
    }

    public BaseObject(Graphics g2, int id2, float x2, float y2, float w2, float h2, float xoff2, float yoff2) {
        setG(g2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
        setId(id2);
        setXOff(xoff2);
        setYOff(yoff2);
    }

    public BaseObject(Graphics g2, int id2, float x2, float y2, float w2, float h2, float xoff2, float yoff2, float xhit2, float yhit2) {
        setG(g2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
        setId(id2);
        setXOff(xoff2);
        setYOff(yoff2);
        setXHit(xhit2);
        setYHit(yhit2);
    }

    public void init(Graphics g2, int id2, float x2, float y2, float w2, float h2) {
        setG(g2);
        setId(id2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
    }

    public void action() {
    }

    public void draw() {
        if (this.id == -1) {
            this.g.drawSquare((int) getX(), (int) getY(), (int) getW(), (int) getH(), -16777216, true);
        } else {
            this.g.drawImage(this.id, (int) (this.x + this.xoff), (int) (this.y + this.yoff), (int) this.w, (int) this.h);
        }
    }

    public void setG(Graphics g2) {
        this.g = g2;
    }

    public Graphics getG() {
        return this.g;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getX() {
        return this.x;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getY() {
        return this.y;
    }

    public void setW(float w2) {
        this.w = w2;
    }

    public float getW() {
        return this.w;
    }

    public void setH(float h2) {
        this.h = h2;
    }

    public float getH() {
        return this.h;
    }

    public void setXSpeed(float xspeed2) {
        this.xspeed = xspeed2;
    }

    public float getXSpeed() {
        return this.xspeed;
    }

    public void setYSpeed(float yspeed2) {
        this.yspeed = yspeed2;
    }

    public float getYSpeed() {
        return this.yspeed;
    }

    public void setXOff(float xoff2) {
        this.xoff = xoff2;
    }

    public float getXOff() {
        return this.xoff;
    }

    public void setYOff(float yoff2) {
        this.yoff = yoff2;
    }

    public float getYOff() {
        return this.yoff;
    }

    public void setXHit(float xhit2) {
        this.xhit = xhit2;
    }

    public float getXHit() {
        return this.xhit;
    }

    public void setYHit(float yhit2) {
        this.yhit = yhit2;
    }

    public float getYHit() {
        return this.yhit;
    }

    public void setMode(int mode2) {
        this.mode = mode2;
    }

    public int getMode() {
        return this.mode;
    }
}
