package cross.field.stage;

public class Block {
    public static final float XSPEED = -25.0f;
    private Graphics g;
    private float h = 0.0f;
    private float id = 0.0f;
    private float w = 0.0f;
    private float x = 0.0f;
    private float xspeed = -25.0f;
    private float y = 0.0f;
    private float yspeed = 0.0f;

    public Block(Graphics g2, int id2, float x2, float y2, float w2, float h2) {
        setG(g2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
        setId((float) id2);
    }

    public void init(Graphics g2, int id2, float x2, float y2, float w2, float h2) {
        setG(g2);
        setId((float) id2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
    }

    public void action() {
        this.x += this.xspeed * this.g.ratio_width;
    }

    public void draw() {
        if (this.id == -1.0f) {
            this.g.drawSquare((int) getX(), (int) getY(), (int) getW(), (int) getH(), -16777216, true);
        }
    }

    public void setG(Graphics g2) {
        this.g = g2;
    }

    public Graphics getG() {
        return this.g;
    }

    public void setId(float id2) {
        this.id = id2;
    }

    public float getId() {
        return this.id;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getX() {
        return this.x;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getY() {
        return this.y;
    }

    public void setW(float w2) {
        this.w = w2;
    }

    public float getW() {
        return this.w;
    }

    public void setH(float h2) {
        this.h = h2;
    }

    public float getH() {
        return this.h;
    }

    public void setXspeed(float xspeed2) {
        this.xspeed = xspeed2;
    }

    public float getXspeed() {
        return this.xspeed;
    }

    public void setYspeed(float yspeed2) {
        this.yspeed = yspeed2;
    }

    public float getYspeed() {
        return this.yspeed;
    }
}
