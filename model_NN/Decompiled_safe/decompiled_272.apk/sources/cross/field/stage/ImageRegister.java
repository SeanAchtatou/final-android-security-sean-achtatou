package cross.field.stage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.SurfaceView;
import cross.field.NinjaExiter.R;

public class ImageRegister extends SurfaceView {
    public static final int BG_MAIN = 0;
    public static final int BUTTON = 5;
    public static final int BUTTON_BACK = 6;
    public static final int DOWN = 21;
    public static final int LOGO_0 = 9;
    public static final int LOGO_1 = 10;
    public static final int LOGO_2 = 11;
    public static final int LOGO_3 = 12;
    public static final int LOGO_4 = 13;
    public static final int LOGO_5 = 14;
    public static final int LOGO_6 = 15;
    public static final int LOGO_7 = 16;
    public static final int LOGO_8 = 17;
    public static final int LOGO_9 = 18;
    public static final int LOGO_COLON = 19;
    public static final int LOGO_LEFT = 7;
    public static final int LOGO_RIGHT = 8;
    public static final int MAX = 24;
    public static final int PLAYER_00 = 1;
    public static final int PLAYER_01 = 2;
    public static final int PLAYER_02 = 3;
    public static final int PLAYER_03 = 4;
    public static final int ROOF = 22;
    public static final int ROOF2 = 23;
    public static final int UP = 20;

    public ImageRegister(Context context) {
        super(context);
    }

    public Drawable[] getImage() {
        Resources r = getResources();
        return new Drawable[]{r.getDrawable(R.drawable.bg_main), r.getDrawable(R.drawable.player00), r.getDrawable(R.drawable.player01), r.getDrawable(R.drawable.player02), r.getDrawable(R.drawable.player03), r.getDrawable(R.drawable.button), r.getDrawable(R.drawable.button_back), r.getDrawable(R.drawable.logo_left), r.getDrawable(R.drawable.logo_right), r.getDrawable(R.drawable.logo_0), r.getDrawable(R.drawable.logo_1), r.getDrawable(R.drawable.logo_2), r.getDrawable(R.drawable.logo_3), r.getDrawable(R.drawable.logo_4), r.getDrawable(R.drawable.logo_5), r.getDrawable(R.drawable.logo_6), r.getDrawable(R.drawable.logo_7), r.getDrawable(R.drawable.logo_8), r.getDrawable(R.drawable.logo_9), r.getDrawable(R.drawable.logo_colon), r.getDrawable(R.drawable.up), r.getDrawable(R.drawable.down), r.getDrawable(R.drawable.roof), r.getDrawable(R.drawable.roof2)};
    }

    public void setImage() {
    }
}
