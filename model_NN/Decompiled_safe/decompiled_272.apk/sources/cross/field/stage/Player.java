package cross.field.stage;

import android.view.MotionEvent;

public class Player {
    public static final int ANM_SPEED = 1;
    public static final int END = 3;
    public static final int INIT = 0;
    public static final int LEFT = 1;
    public static final float LEFT_X = 120.0f;
    public static final int RIGHT = 2;
    public static final float RIGHT_X = 280.0f;
    private int anm_cnt = 0;
    private Graphics g;
    private float h = 0.0f;
    private int image_id = 0;
    private int mode = 0;
    private float w = 0.0f;
    private float x = 0.0f;
    private float xhit;
    private float xoff = 0.0f;
    private float y = 0.0f;
    private float yhit;
    private float yoff = 0.0f;

    public Player(Graphics g2, int image_id2, float x2, float y2, float w2, float h2) {
        setG(g2);
        this.mode = 1;
        setImage_id(image_id2);
        setX(x2);
        setY(y2);
        setW(w2);
        setH(h2);
    }

    public void action() {
        switch (this.mode) {
        }
        animation();
    }

    public void draw() {
        this.g.drawImage(this.image_id, (int) (this.x + this.xoff), (int) (this.y + this.yoff), (int) this.w, (int) this.h);
    }

    public void animation() {
        switch (getMode()) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                if (this.image_id > 4 && this.image_id < 1) {
                    this.image_id = 1;
                }
                this.anm_cnt++;
                if (this.anm_cnt >= 1) {
                    this.anm_cnt = 0;
                    this.image_id++;
                    if (this.image_id > 4) {
                        this.image_id = 1;
                        return;
                    }
                    return;
                }
                return;
        }
    }

    public MotionEvent touchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (this.mode == 1) {
                    this.mode = 2;
                    this.x = 280.0f * this.g.ratio_width;
                    return event;
                } else if (this.mode != 2) {
                    return event;
                } else {
                    this.mode = 1;
                    this.x = 120.0f * this.g.ratio_width;
                    return event;
                }
            case 1:
            case 2:
            case 3:
                return event;
            default:
                return null;
        }
    }

    public void setMode(int mode2) {
        this.mode = mode2;
    }

    public int getMode() {
        return this.mode;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getX() {
        return this.x;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getY() {
        return this.y;
    }

    public void setW(float w2) {
        this.w = w2;
    }

    public float getW() {
        return this.w;
    }

    public void setH(float h2) {
        this.h = h2;
    }

    public float getH() {
        return this.h;
    }

    public void setXoff(float xoff2) {
        this.xoff = xoff2;
    }

    public float getXoff() {
        return this.xoff;
    }

    public void setYoff(float yoff2) {
        this.yoff = yoff2;
    }

    public float getYoff() {
        return this.yoff;
    }

    public void setXhit(float xhit2) {
        this.xhit = xhit2;
    }

    public float getXhit() {
        return this.xhit;
    }

    public void setYhit(float yhit2) {
        this.yhit = yhit2;
    }

    public float getYhit() {
        return this.yhit;
    }

    public void setImage_id(int image_id2) {
        this.image_id = image_id2;
    }

    public int getImageId() {
        return this.image_id;
    }

    public void setAnm_cnt(int anm_cnt2) {
        this.anm_cnt = anm_cnt2;
    }

    public int getAnmCnt() {
        return this.anm_cnt;
    }

    public void setG(Graphics g2) {
        this.g = g2;
    }

    public Graphics getG() {
        return this.g;
    }
}
