package cross.field.stage;

public class RoofManager {
    public static final int EXPAND = 1;
    public static final int EXPAND2 = 2;
    public static float EXPAND_SPEED = 3.0f;
    public static final int INIT = 0;
    public static final int REDUCTION = 3;
    public static final float ROOF_H = 80.0f;
    public static final float ROOF_W = 480.0f;
    public static final float ROOF_X = 0.0f;
    public static final float ROOF_Y = 0.0f;
    public static final int SCALE_TIME = 30;
    Graphics g;
    private BaseObject roof;
    private BaseObject roof2;
    private int scale_time = 0;

    public RoofManager(Graphics g2) {
        this.g = g2;
        init();
    }

    public void init() {
        this.roof = new BaseObject(this.g, 23, this.g.ratio_width * 0.0f, this.g.ratio_height * 0.0f, this.g.ratio_width * 480.0f, 0.0f);
        this.roof.setMode(1);
        setRoof2(new BaseObject(this.g, 22, this.g.ratio_width * 0.0f, this.g.ratio_height * 0.0f, this.g.ratio_width * 480.0f, 80.0f * this.g.ratio_height));
    }

    public void action() {
        switch (getRoof().getMode()) {
            case 0:
            default:
                return;
            case 1:
                getRoof().setH(getRoof().getH() + (EXPAND_SPEED * this.g.ratio_height));
                return;
            case 2:
                getRoof().setH(getRoof().getH() + ((float) (((double) (EXPAND_SPEED * this.g.ratio_height)) * 2.0d)));
                if (getScaleTime() <= 0) {
                    setScaleTime(30);
                }
                setScaleTime(getScaleTime() - 2);
                if (getScaleTime() <= 0) {
                    setScaleTime(30);
                    getRoof().setMode(1);
                    return;
                }
                return;
            case 3:
                getRoof().setH(getRoof().getH() - ((float) (((double) (EXPAND_SPEED * this.g.ratio_height)) * 2.0d)));
                if (getScaleTime() <= 0) {
                    setScaleTime(30);
                }
                setScaleTime(getScaleTime() - 2);
                if (getScaleTime() <= 0) {
                    setScaleTime(30);
                    getRoof().setMode(1);
                }
                if (this.roof.getH() <= 0.0f) {
                    this.roof.setH(0.0f);
                    return;
                }
                return;
        }
    }

    public void draw() {
        this.roof.draw();
        getRoof2().setY(this.roof.getH());
        getRoof2().draw();
    }

    public void setRoof(BaseObject roof3) {
        this.roof = roof3;
    }

    public BaseObject getRoof() {
        return this.roof;
    }

    public void setScaleTime(int scale_time2) {
        this.scale_time = scale_time2;
    }

    public int getScaleTime() {
        return this.scale_time;
    }

    public void setRoof2(BaseObject roof22) {
        this.roof2 = roof22;
    }

    public BaseObject getRoof2() {
        return this.roof2;
    }
}
