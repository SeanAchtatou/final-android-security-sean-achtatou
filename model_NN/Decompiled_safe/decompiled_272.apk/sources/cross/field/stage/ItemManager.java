package cross.field.stage;

import java.util.ArrayList;
import java.util.Random;

public class ItemManager {
    public static final int ITEM_H = 80;
    public static final int ITEM_W = 80;
    public static final int ITEM_X = 120;
    public static final int ITEM_Y = 0;
    public static final int ITEM_YSPEED = 20;
    public static final int LEFT = 120;
    public static final int RATE_EFFECT = 35;
    public static final int RATE_MAX = 20;
    public static final int RATE_MIN = 10;
    public static final int RIGHT = 280;
    private Graphics g;
    private ArrayList<BaseObject> item;
    private Random random;
    private int rate_exist = 0;

    public ItemManager(Graphics g2) {
        this.g = g2;
        init();
    }

    public void init() {
        this.random = new Random();
        setItem(new ArrayList());
    }

    public void createItem() {
        float set_x;
        this.rate_exist--;
        if (this.rate_exist < 0) {
            this.rate_exist = this.random.nextInt(10) + 10;
            int image_id = 0;
            float f = 120.0f * this.g.ratio_width;
            float set_y = 0.0f * this.g.ratio_height;
            float set_w = 80.0f * this.g.ratio_width;
            float set_h = 80.0f * this.g.ratio_height;
            int rate_effect = this.random.nextInt(100);
            if (rate_effect < 35) {
                image_id = 20;
            } else if (rate_effect >= 35) {
                image_id = 21;
            }
            if (this.random.nextBoolean()) {
                set_x = 120.0f * this.g.ratio_width;
            } else {
                set_x = 280.0f * this.g.ratio_width;
            }
            getItem().add(new BaseObject(this.g, image_id, set_x, set_y, set_w, set_h));
        }
    }

    public void deleteItem() {
        ArrayList<Integer> number = new ArrayList<>();
        for (int i = 0; i < getItem().size(); i++) {
            if (getItem().get(i).getY() >= ((float) this.g.disp_height)) {
                number.add(Integer.valueOf(i));
            }
        }
        for (int i2 = 0; i2 < number.size(); i2++) {
            getItem().remove(number.get(i2));
        }
    }

    public void action() {
        createItem();
        for (int i = 0; i < getItem().size(); i++) {
            BaseObject item2 = getItem().get(i);
            item2.setYSpeed(20.0f * this.g.ratio_height);
            item2.setY(item2.getY() + item2.getYSpeed());
        }
        deleteItem();
    }

    public void draw() {
        for (int i = 0; i < getItem().size(); i++) {
            getItem().get(i).draw();
        }
    }

    public void setItem(ArrayList<BaseObject> item2) {
        this.item = item2;
    }

    public ArrayList<BaseObject> getItem() {
        return this.item;
    }
}
