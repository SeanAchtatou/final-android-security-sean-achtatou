package com.waps;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.mobclick.android.ReportPolicy;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class AppConnect {
    public static final String LIBRARY_VERSION_NUMBER = "1.4";
    /* access modifiers changed from: private */
    public static boolean S = true;
    /* access modifiers changed from: private */
    public static boolean T = false;
    private static String U = null;
    /* access modifiers changed from: private */
    public static UpdatePointsNotifier V;
    /* access modifiers changed from: private */
    public static String aa = "";
    /* access modifiers changed from: private */
    public static String ab = "receiver/install?";
    /* access modifiers changed from: private */
    public static String ac = "install";
    private static AppConnect u = null;
    /* access modifiers changed from: private */
    public static n v = null;
    private static DisplayAd w = null;
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "";
    /* access modifiers changed from: private */
    public String E = "";
    private String F = "";
    private String G = "";
    /* access modifiers changed from: private */
    public String H = "";
    private String I = "http://app.waps.cn/action/account/offerlist?";
    private String J = "http://app.waps.cn/action/account/ownslist?";
    /* access modifiers changed from: private */
    public String K = "";
    /* access modifiers changed from: private */
    public String L = "";
    /* access modifiers changed from: private */
    public String M = "";
    /* access modifiers changed from: private */
    public String N = "";
    private String O = "";
    private int P = 0;
    private int Q = 0;
    /* access modifiers changed from: private */
    public String R = "";
    private f W = null;
    private i X = null;
    private g Y = null;
    /* access modifiers changed from: private */
    public h Z = null;
    final String a = "udid";
    private int ad;
    final String b = "device_name";
    final String c = "device_type";
    final String d = "os_version";
    final String e = "country_code";
    final String f = "language";
    final String g = "app_id";
    final String h = "app_version";
    final String i = "sdk_version";
    final String j = "act";
    final String k = "userid";
    final String l = "channel";
    final String m = "points";
    final String n = "install";
    final String o = "uninstall";
    final String p = "load";
    final String q = "device_width";
    final String r = "device_height";
    private j s = null;
    /* access modifiers changed from: private */
    public Context t = null;
    /* access modifiers changed from: private */
    public String x = "";
    private String y = "";
    private String z = "";

    public AppConnect() {
    }

    private AppConnect(Context context) {
        this.L = getParams(context);
        this.s = new j(this, null);
        this.s.execute(new Void[0]);
    }

    public AppConnect(Context context, int i2) {
        this.L = getParams(context);
    }

    private AppConnect(Context context, String str) {
        this.t = context;
        initMetaData();
        this.L = getParams(context);
        this.L += "&userid=" + str;
        this.s = new j(this, null);
        this.s.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void UpdateDialog(String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.t);
            builder.setTitle("新版提示");
            builder.setMessage("有新版本(" + this.R + "),是否下载?");
            builder.setPositiveButton("下载", new d(this, str));
            builder.setNegativeButton("下次再说", new e(this));
            builder.show();
        } catch (Exception e2) {
        }
    }

    private Document buildDocument(String str) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            return newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getArea() {
        int i2 = 0;
        while (U == null && i2 < 20) {
            try {
                Thread.sleep(100);
                i2++;
            } catch (InterruptedException e2) {
            }
        }
        return U;
    }

    public static AppConnect getInstance(Context context) {
        if (v == null) {
            v = new n();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                v.a(true);
            }
        }
        if (u == null) {
            u = new AppConnect(context);
        }
        if (w == null) {
            w = new DisplayAd(context);
        }
        return u;
    }

    public static AppConnect getInstance(Context context, int i2) {
        if (v == null) {
            v = new n();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                v.a(true);
            }
        }
        if (u == null) {
            u = new AppConnect(context, i2);
        }
        if (w == null) {
            w = new DisplayAd(context);
        }
        return u;
    }

    public static AppConnect getInstance(Context context, String str) {
        if (v == null) {
            v = new n();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
                v.a(true);
            }
        }
        if (u == null) {
            u = new AppConnect(context, str);
        }
        if (w == null) {
            w = new DisplayAd(context);
        }
        return u;
    }

    public static AppConnect getInstanceNoConnect(Context context) {
        if (v == null) {
            v = new n();
        }
        if (u == null) {
            u = new AppConnect(context, 0);
        }
        if (w == null) {
            w = new DisplayAd(context);
        }
        return u;
    }

    private String getNodeTrimValue(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim();
    }

    private void getPointsHelper() {
        this.W = new f(this, null);
        this.W.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public boolean handleConnectResponse(String str) {
        try {
            Document buildDocument = buildDocument(str);
            if (!(buildDocument == null || buildDocument.getElementsByTagName("Version") == null)) {
                String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Version"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("Clear"));
                String nodeTrimValue4 = getNodeTrimValue(buildDocument.getElementsByTagName("Area"));
                if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                    if (nodeTrimValue2 != null && !"".equals(nodeTrimValue2)) {
                        this.R = nodeTrimValue2;
                    }
                    if (nodeTrimValue3 != null && !"".equals(nodeTrimValue3.trim())) {
                        T = true;
                    }
                    if (nodeTrimValue4 != null && !"".equals(nodeTrimValue4.trim())) {
                        setArea(nodeTrimValue4);
                    }
                    return true;
                }
            }
        } catch (Exception e2) {
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleGetPointsResponse(String str) {
        String nodeTrimValue;
        Document buildDocument = buildDocument(str);
        if (!(buildDocument == null || (nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"))) == null || !nodeTrimValue.equals("true"))) {
            this.t.getSharedPreferences("Points", 0);
            String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
            String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
            if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                V.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleSpendPointsResponse(String str) {
        Document buildDocument = buildDocument(str);
        if (buildDocument != null) {
            String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
            if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
                if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                    V.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                    return true;
                }
            } else if (nodeTrimValue != null && nodeTrimValue.endsWith("false")) {
                V.getUpdatePointsFailed(getNodeTrimValue(buildDocument.getElementsByTagName("Message")));
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    public void loadApps() {
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
        BufferedReader bufferedReader;
        FileInputStream fileInputStream2;
        File file;
        FileOutputStream fileOutputStream2;
        int i2 = 0;
        new Intent("android.intent.action.MAIN", (Uri) null).addCategory("android.intent.category.LAUNCHER");
        String str = "";
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file2 = new File(Environment.getExternalStorageDirectory().toString() + "/Android");
                File file3 = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (!file2.exists()) {
                    file2.mkdir();
                }
                if (!file3.exists()) {
                    file3.createNewFile();
                }
                FileInputStream fileInputStream3 = new FileInputStream(file3);
                try {
                    BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(fileInputStream3));
                    if (bufferedReader2 != null) {
                        while (true) {
                            try {
                                String readLine = bufferedReader2.readLine();
                                if (readLine == null) {
                                    break;
                                }
                                str = str + readLine;
                            } catch (Exception e2) {
                                e = e2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader3 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader3;
                                try {
                                    e.printStackTrace();
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e3) {
                                        e3.printStackTrace();
                                        return;
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e4) {
                                        e4.printStackTrace();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader4 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader4;
                                fileOutputStream.close();
                                fileInputStream.close();
                                bufferedReader.close();
                                throw th;
                            }
                        }
                    }
                    BufferedReader bufferedReader5 = bufferedReader2;
                    file = file3;
                    fileInputStream2 = fileInputStream3;
                    bufferedReader = bufferedReader5;
                } catch (Exception e5) {
                    e = e5;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    e.printStackTrace();
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                } catch (Throwable th3) {
                    th = th3;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                    throw th;
                }
            } else {
                bufferedReader = null;
                fileInputStream2 = null;
                file = null;
            }
            try {
                List<PackageInfo> installedPackages = this.t.getPackageManager().getInstalledPackages(0);
                for (int i3 = 0; i3 < installedPackages.size(); i3++) {
                    PackageInfo packageInfo = installedPackages.get(i3);
                    int i4 = packageInfo.applicationInfo.flags;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((i4 & 1) <= 0) {
                        i2++;
                        String str2 = packageInfo.packageName;
                        if (str2.startsWith("com.")) {
                            String substring = str2.substring(3, str2.length());
                            if (!str.contains(substring)) {
                                aa += substring + ";";
                            }
                        }
                    }
                }
                byte[] bytes = aa.getBytes("UTF-8");
                if (file != null) {
                    FileOutputStream fileOutputStream3 = new FileOutputStream(file, true);
                    try {
                        fileOutputStream3.write(bytes);
                        fileOutputStream2 = fileOutputStream3;
                    } catch (Exception e6) {
                        e = e6;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        e.printStackTrace();
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                    } catch (Throwable th4) {
                        th = th4;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                        throw th;
                    }
                } else {
                    fileOutputStream2 = null;
                }
                try {
                    fileOutputStream2.close();
                    fileInputStream2.close();
                    bufferedReader.close();
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
            } catch (Exception e8) {
                e = e8;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                e.printStackTrace();
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
            } catch (Throwable th5) {
                th = th5;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
                throw th;
            }
        } catch (Exception e9) {
            e = e9;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            e.printStackTrace();
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
        } catch (Throwable th6) {
            th = th6;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
            throw th;
        }
    }

    private void packageReceiverHelper() {
        this.Y = new g(this, null);
        this.Y.execute(new Void[0]);
    }

    public static void setArea(String str) {
        U = str;
    }

    private void spendPointsHelper() {
        this.X = new i(this, null);
        this.X.execute(new Void[0]);
    }

    public void finalize() {
        u = null;
    }

    public void getDisplayAd(DisplayAdNotifier displayAdNotifier) {
        w.getDisplayAdDataFromServer("http://ads.waps.cn/action/", this.L, displayAdNotifier);
    }

    public String getParams(Context context) {
        this.t = context;
        initMetaData();
        this.L += "app_id=" + this.D + "&";
        this.L += "udid=" + this.x + "&";
        this.L += "app_version=" + this.E + "&";
        this.L += "sdk_version=" + this.F + "&";
        this.L += "device_name=" + this.y + "&";
        this.L += "device_type=" + this.z + "&";
        this.L += "os_version=" + this.A + "&";
        this.L += "country_code=" + this.B + "&";
        this.L += "language=" + this.C + "&";
        this.L += "act=" + context.getPackageName() + "." + context.getClass().getSimpleName();
        if (this.G != null && !"".equals(this.G)) {
            this.L += "&";
            this.L += "channel=" + this.G;
        }
        if (this.P > 0 && this.Q > 0) {
            this.L += "&";
            this.L += "device_width=" + this.P + "&";
            this.L += "device_height=" + this.Q;
        }
        return this.L.replaceAll(" ", "%20");
    }

    public void getPoints(UpdatePointsNotifier updatePointsNotifier) {
        if (u != null) {
            V = updatePointsNotifier;
            u.getPointsHelper();
        }
    }

    public void initMetaData() {
        String obj;
        PackageManager packageManager = this.t.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.t.getPackageName(), 128);
            if (applicationInfo != null && applicationInfo.metaData != null) {
                String string = applicationInfo.metaData.getString("WAPS_ID");
                if (string == null || "".equals(string)) {
                    string = applicationInfo.metaData.getString("APP_ID");
                }
                if (string != null && !string.equals("")) {
                    this.D = string.trim();
                    this.K = this.t.getPackageName();
                    String string2 = applicationInfo.metaData.getString("CLIENT_PACKAGE");
                    if (string2 != null && !string2.equals("")) {
                        this.K = string2;
                    }
                    Object obj2 = applicationInfo.metaData.get("WAPS_PID");
                    if (!(obj2 == null || (obj = obj2.toString()) == null || obj.equals(""))) {
                        this.G = obj;
                    }
                    this.E = packageManager.getPackageInfo(this.t.getPackageName(), 0).versionName;
                    this.z = "android";
                    this.y = Build.MODEL;
                    this.A = Build.VERSION.RELEASE;
                    this.B = Locale.getDefault().getCountry();
                    this.C = Locale.getDefault().getLanguage();
                    this.F = LIBRARY_VERSION_NUMBER;
                    SharedPreferences sharedPreferences = this.t.getSharedPreferences("appPrefrences", 0);
                    String string3 = applicationInfo.metaData.getString("DEVICE_ID");
                    if (string3 == null || string3.equals("")) {
                        TelephonyManager telephonyManager = (TelephonyManager) this.t.getSystemService("phone");
                        if (telephonyManager != null) {
                            this.x = telephonyManager.getDeviceId();
                            if (this.x == null || this.x.length() == 0) {
                                this.x = "0";
                            }
                            try {
                                this.x = this.x.toLowerCase();
                                if (Integer.valueOf(Integer.parseInt(this.x)).intValue() == 0) {
                                    StringBuffer stringBuffer = new StringBuffer();
                                    stringBuffer.append("EMULATOR");
                                    String string4 = sharedPreferences.getString("emulatorDeviceId", null);
                                    if (string4 == null || string4.equals("")) {
                                        for (int i2 = 0; i2 < 32; i2++) {
                                            stringBuffer.append("1234567890abcdefghijklmnopqrstuvw".charAt(((int) (Math.random() * 100.0d)) % 30));
                                        }
                                        this.x = stringBuffer.toString().toLowerCase();
                                        SharedPreferences.Editor edit = sharedPreferences.edit();
                                        edit.putString("emulatorDeviceId", this.x);
                                        edit.commit();
                                    } else {
                                        this.x = string4;
                                    }
                                }
                            } catch (NumberFormatException e2) {
                            }
                        } else {
                            this.x = null;
                        }
                    } else {
                        this.x = string3;
                    }
                    try {
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        ((WindowManager) this.t.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                        this.P = displayMetrics.widthPixels;
                        this.Q = displayMetrics.heightPixels;
                    } catch (Exception e3) {
                    }
                    this.ad = sharedPreferences.getInt("PrimaryColor", 0);
                    String string5 = sharedPreferences.getString("InstallReferral", null);
                    if (string5 != null && !string5.equals("")) {
                        this.M = string5;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e4) {
        }
    }

    public void package_receiver(String str, int i2) {
        switch (i2) {
            case 0:
                ab = "receiver/install?";
                ac = "install";
                break;
            case 1:
                ab = "receiver/load_offer?";
                ac = "load";
                break;
            case ReportPolicy.BATCH_AT_TERMINATE:
                ab = "receiver/load_ad?";
                ac = "load";
                break;
            case ReportPolicy.PUSH:
                ab = "receiver/uninstall?";
                ab = "uninstall";
                break;
            default:
                ab = "receiver/install?";
                ac = "install";
                break;
        }
        this.N = str;
        if (u != null) {
            u.packageReceiverHelper();
        }
    }

    public void showMore(Context context) {
        showMore(context, this.x);
    }

    public void showMore(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.J);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.L);
        intent.putExtra("CLIENT_PACKAGE", this.K);
        context.startActivity(intent);
    }

    public void showOffers(Context context) {
        showOffers(context, this.x);
    }

    public void showOffers(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.I);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.L);
        intent.putExtra("CLIENT_PACKAGE", this.K);
        context.startActivity(intent);
    }

    public void spendPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.H = "" + i2;
            if (u != null) {
                V = updatePointsNotifier;
                u.spendPointsHelper();
            }
        }
    }
}
