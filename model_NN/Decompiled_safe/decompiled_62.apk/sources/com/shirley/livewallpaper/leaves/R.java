package com.shirley.livewallpaper.leaves;

public final class R {

    public static final class array {
        public static final int time_string = 2131034112;
        public static final int time_value = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int back1 = 2130837504;
        public static final int back2 = 2130837505;
        public static final int back3 = 2130837506;
        public static final int back4 = 2130837507;
        public static final int back5 = 2130837508;
        public static final int back6 = 2130837509;
        public static final int icon = 2130837510;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131165184;
        public static final int OfferProgressBar = 2131165187;
        public static final int RelativeLayout01 = 2131165185;
        public static final int offersWebView = 2131165186;
        public static final int proTV = 2131165188;
    }

    public static final class layout {
        public static final int livewallpaper_settings = 2130903040;
        public static final int offers_web_view = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
        public static final int livewallpaper_settings = 2131099651;
        public static final int wallpaper = 2131099650;
    }

    public static final class xml {
        public static final int snow = 2130968576;
    }
}
