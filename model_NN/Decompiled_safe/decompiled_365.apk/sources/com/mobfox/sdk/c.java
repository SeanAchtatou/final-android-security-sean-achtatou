package com.mobfox.sdk;

import android.content.Context;
import android.widget.ViewFlipper;

final class c extends ViewFlipper {
    private /* synthetic */ MobFoxView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(MobFoxView mobFoxView, Context context) {
        super(context);
        this.a = mobFoxView;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }
}
