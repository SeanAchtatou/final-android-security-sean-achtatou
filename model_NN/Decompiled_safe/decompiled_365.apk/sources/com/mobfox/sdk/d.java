package com.mobfox.sdk;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

final class d implements View.OnTouchListener {
    private float a;
    private float b;
    private /* synthetic */ MobFoxView c;

    d(MobFoxView mobFoxView) {
        this.c = mobFoxView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobfox.sdk.MobFoxView.a(com.mobfox.sdk.MobFoxView, boolean):boolean
     arg types: [com.mobfox.sdk.MobFoxView, int]
     candidates:
      com.mobfox.sdk.MobFoxView.a(com.mobfox.sdk.MobFoxView, com.mobfox.sdk.a.a):com.mobfox.sdk.a.a
      com.mobfox.sdk.MobFoxView.a(com.mobfox.sdk.MobFoxView, boolean):boolean */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            boolean unused = this.c.v = false;
            this.a = motionEvent.getX();
            this.b = motionEvent.getY();
        }
        if (motionEvent.getAction() == 2) {
            if (Math.abs(this.a - motionEvent.getX()) > 30.0f) {
                boolean unused2 = this.c.v = true;
            }
            if (Math.abs(this.b - motionEvent.getY()) > 30.0f) {
                boolean unused3 = this.c.v = true;
            }
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "touchMove: " + this.c.v);
            }
            return true;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "size x: " + motionEvent.getX());
                Log.d("MOBFOX", "getHistorySize: " + motionEvent.getHistorySize());
            }
            if (this.c.i != null && !this.c.v) {
                this.c.a();
            }
            return false;
        }
    }
}
