package com.mobfox.sdk;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class k extends WebViewClient {
    private /* synthetic */ InAppWebView a;

    k(InAppWebView inAppWebView) {
        this.a = inAppWebView;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        webView.loadUrl(str);
        return false;
    }
}
