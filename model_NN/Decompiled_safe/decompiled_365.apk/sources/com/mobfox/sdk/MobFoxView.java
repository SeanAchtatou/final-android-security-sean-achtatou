package com.mobfox.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.mobfox.sdk.a.a;
import com.mobfox.sdk.a.b;
import com.mobfox.sdk.a.d;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.MessageFormat;
import java.util.Timer;
import java.util.UUID;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.BasicHttpContext;

public class MobFoxView extends RelativeLayout {
    final Handler a = new Handler();
    final Runnable b = new e(this);
    private boolean c = false;
    private j d;
    private String e;
    private boolean f;
    private Timer g;
    private boolean h = false;
    /* access modifiers changed from: private */
    public a i;
    private Animation j = null;
    private Animation k = null;
    private WebSettings l;
    private d m;
    private LocationManager n;
    private int o;
    private int p;
    private int q;
    private WebView r;
    private WebView s;
    private ViewFlipper t;
    /* access modifiers changed from: private */
    public g u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public Thread w;
    private View.OnTouchListener x = new d(this);
    private int y = 0;

    public MobFoxView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet != null) {
            this.e = attributeSet.getAttributeValue(null, "publisherId");
            String attributeValue = attributeSet.getAttributeValue(null, "mode");
            if (attributeValue != null && attributeValue.equalsIgnoreCase("test")) {
                this.d = j.TEST;
            }
            String attributeValue2 = attributeSet.getAttributeValue(null, "animation");
            if (attributeValue2 == null || !attributeValue2.equals("false")) {
                this.f = true;
            } else {
                this.f = false;
            }
            this.c = attributeSet.getAttributeBooleanValue(null, "includeLocation", false);
        }
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "MobFox SDK Version:1.2");
        }
        this.n = null;
        this.q = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
        this.o = context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
        this.p = context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
        if (this.o == 0 || this.p == 0) {
            this.n = (LocationManager) getContext().getSystemService("location");
        }
        this.r = a(context);
        this.s = a(context);
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "Create view flipper");
        }
        this.t = new c(this, getContext());
        this.t.addView(this.r);
        this.t.addView(this.s);
        addView(this.t);
        this.r.setOnTouchListener(this.x);
        this.s.setOnTouchListener(this.x);
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "animation: " + this.f);
        }
        if (this.f) {
            this.j = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
            this.j.setDuration(1000);
            this.k = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
            this.k.setDuration(1000);
            this.t.setInAnimation(this.j);
            this.t.setOutAnimation(this.k);
        }
    }

    private WebView a(Context context) {
        WebView webView = new WebView(getContext());
        this.l = webView.getSettings();
        this.l.setJavaScriptEnabled(true);
        webView.setBackgroundColor(0);
        webView.setWebViewClient(new h(this, context));
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        return webView;
    }

    static /* synthetic */ void a(MobFoxView mobFoxView) {
        try {
            WebView webView = mobFoxView.t.getCurrentView() == mobFoxView.r ? mobFoxView.s : mobFoxView.r;
            if (mobFoxView.i.a() == i.IMAGE) {
                String format = MessageFormat.format("<body style='\"'margin: 0px; padding: 0px; text-align:center;'\"'><img src='\"'{0}'\"' width='\"'{1}'dp\"' height='\"'{2}'dp\"'/></body>", mobFoxView.i.d(), Integer.valueOf(mobFoxView.i.b()), Integer.valueOf(mobFoxView.i.c()));
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "set image: " + format);
                }
                webView.loadData(Uri.encode("<style>* { -webkit-tap-highlight-color: rgba(0,0,0,0) }</style>" + format), "text/html", "UTF-8");
                if (mobFoxView.u != null && Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "notify bannerListener of load succeeded: " + mobFoxView.u.getClass().getName());
                }
            } else if (mobFoxView.i.a() == i.TEXT) {
                String encode = Uri.encode("<style>* { -webkit-tap-highlight-color: rgba(0,0,0,0) }</style>" + mobFoxView.i.e());
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "set text: " + encode);
                }
                webView.loadData(encode, "text/html", "UTF-8");
                if (mobFoxView.u != null && Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "notify bannerListener of load succeeded: " + mobFoxView.u.getClass().getName());
                }
            } else if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "No Ad");
                return;
            } else {
                return;
            }
            if (mobFoxView.t.getCurrentView() == mobFoxView.r) {
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "show next");
                }
                mobFoxView.t.showNext();
            } else {
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "show previous");
                }
                mobFoxView.t.showPrevious();
            }
            mobFoxView.e();
        } catch (Throwable th) {
            if (Log.isLoggable("MOBFOX", 6)) {
                Log.e("MOBFOX", "Uncaught exception in show content", th);
            }
        }
    }

    private void a(String str) {
        if (this.i.f() == null || !this.i.f().equals(b.INAPP)) {
            getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return;
        }
        Intent intent = new Intent(getContext(), InAppWebView.class);
        intent.putExtra("REDIRECT_URI", this.i.g());
        getContext().startActivity(intent);
    }

    private String c() {
        String string = Settings.Secure.getString(getContext().getContentResolver(), "android_id");
        if (string == null || string.equals("9774d56d682e549c")) {
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String string2 = defaultSharedPreferences.getString("mobfox_device_id", null);
            if (string2 == null) {
                try {
                    String uuid = UUID.randomUUID().toString();
                    MessageDigest instance = MessageDigest.getInstance("MD5");
                    instance.update(uuid.getBytes(), 0, uuid.length());
                    string2 = String.format("%016X", new BigInteger(1, instance.digest()));
                } catch (Exception e2) {
                    Log.d("MOBFOX", "Could not generate pseudo unique id", e2);
                    string2 = "9774d56d682e549c";
                }
                defaultSharedPreferences.edit().putString("mobfox_device_id", string2).commit();
            }
            string = string2;
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "Unknown Android ID using pseudo unique id:" + string);
            }
        }
        return string;
    }

    static /* synthetic */ d d(MobFoxView mobFoxView) {
        Location location;
        if (mobFoxView.m == null) {
            mobFoxView.m = new d();
            if (mobFoxView.q == 0) {
                mobFoxView.m.b(((TelephonyManager) mobFoxView.getContext().getSystemService("phone")).getDeviceId());
                mobFoxView.m.d("3");
            } else {
                mobFoxView.m.b(mobFoxView.c());
                mobFoxView.m.d("N3");
            }
            mobFoxView.m.a(mobFoxView.d);
            mobFoxView.m.c(mobFoxView.e);
            mobFoxView.m.a(mobFoxView.l.getUserAgentString());
        }
        if (mobFoxView.c) {
            if (mobFoxView.n != null) {
                if (mobFoxView.o == 0 && mobFoxView.n.isProviderEnabled("gps")) {
                    location = mobFoxView.n.getLastKnownLocation("gps");
                } else if (mobFoxView.p == 0 && mobFoxView.n.isProviderEnabled("network")) {
                    location = mobFoxView.n.getLastKnownLocation("network");
                }
            }
            location = null;
        } else {
            location = null;
        }
        if (location != null) {
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "location is longitude: " + location.getLongitude() + ", latitude: " + location.getLatitude());
            }
            mobFoxView.m.b(location.getLatitude());
            mobFoxView.m.a(location.getLongitude());
        } else {
            mobFoxView.m.b(0.0d);
            mobFoxView.m.a(0.0d);
        }
        return mobFoxView.m;
    }

    private void d() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "load content");
        }
        if (this.w == null) {
            this.w = new Thread(new b(this));
            this.w.setUncaughtExceptionHandler(new f(this));
            this.w.start();
        }
    }

    private void e() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "start reload timer");
        }
        if (this.g != null) {
            int h2 = this.i.h() * 1000;
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "set timer: " + h2);
            }
            this.g.schedule(new l(this), (long) h2);
        }
    }

    public final void a() {
        if (this.i != null && this.i.g() != null) {
            if (this.i.i()) {
                a(this.i.g());
                return;
            }
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "prefetch url: " + this.i.g());
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpConnectionParams.setSoTimeout(defaultHttpClient.getParams(), 15000);
            HttpConnectionParams.setConnectionTimeout(defaultHttpClient.getParams(), 15000);
            HttpGet httpGet = new HttpGet(this.i.g());
            BasicHttpContext basicHttpContext = new BasicHttpContext();
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet, basicHttpContext);
                if (execute.getStatusLine().getStatusCode() != 200) {
                    throw new IOException(execute.getStatusLine().toString());
                }
                a(((HttpHost) basicHttpContext.getAttribute("http.target_host")).toURI() + ((HttpUriRequest) basicHttpContext.getAttribute("http.request")).getURI());
            } catch (ClientProtocolException e2) {
                Log.e("MOBFOX", "Error in HTTP request", e2);
            } catch (IOException e3) {
                Log.e("MOBFOX", "Error in HTTP request", e3);
            } catch (Throwable th) {
                Log.e("MOBFOX", "Error in HTTP request", th);
            }
        }
    }

    public final void b() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "load next ad");
        }
        d();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (i2 == 0) {
            if (this.g != null) {
                this.g.cancel();
                this.g = null;
            }
            this.g = new Timer();
            if (Log.isLoggable("MOBFOX", 3)) {
                Log.d("MOBFOX", "response: " + this.i);
            }
            if (this.i == null || this.i.h() <= 0) {
                d();
            } else {
                e();
            }
        } else if (this.g != null) {
            try {
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "cancel reload timer");
                }
                this.g.cancel();
                this.g = null;
            } catch (Exception e2) {
                Log.e("MOBFOX", "unable to cancel reloadTimer", e2);
            }
        }
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "onWindowVisibilityChanged: " + i2);
        }
    }

    public void setBannerListener(g gVar) {
        this.u = gVar;
    }

    public void setInternalBrowser(boolean z) {
        this.h = z;
    }
}
