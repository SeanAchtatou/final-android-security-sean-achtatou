package com.mobfox.sdk;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class h extends WebViewClient {
    private final MobFoxView a;
    private Context b;

    public h(MobFoxView mobFoxView, Context context) {
        this.a = mobFoxView;
        this.b = context;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return true;
    }
}
