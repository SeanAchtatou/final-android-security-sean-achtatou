package com.mobfox.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

public class InAppWebView extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String str = (String) getIntent().getExtras().get("REDIRECT_URI");
        if (str != null) {
            WebView webView = new WebView(this);
            webView.setWebViewClient(new k(this));
            webView.loadUrl(str);
            setContentView(webView);
        } else if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "url is null so do not load anything");
        }
    }
}
