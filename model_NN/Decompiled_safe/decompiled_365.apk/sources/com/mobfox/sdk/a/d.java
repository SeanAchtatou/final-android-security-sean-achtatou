package com.mobfox.sdk.a;

import com.mobfox.sdk.j;

public final class d {
    private String a;
    private String b;
    private j c;
    private String d;
    private String e;
    private double f = 0.0d;
    private double g = 0.0d;

    public final String a() {
        return this.a == null ? "" : this.a;
    }

    public final void a(double d2) {
        this.f = d2;
    }

    public final void a(j jVar) {
        this.c = jVar;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b == null ? "" : this.b;
    }

    public final void b(double d2) {
        this.g = d2;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final j c() {
        if (this.c == null) {
            this.c = j.LIVE;
        }
        return this.c;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final String d() {
        return this.e == null ? "" : this.e;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final double e() {
        return this.f;
    }

    public final double f() {
        return this.g;
    }

    public final String g() {
        return this.d == null ? "" : this.d;
    }
}
