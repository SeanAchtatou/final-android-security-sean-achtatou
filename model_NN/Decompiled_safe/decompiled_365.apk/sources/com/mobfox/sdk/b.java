package com.mobfox.sdk;

import android.util.Log;
import com.mobfox.sdk.a.a;

final class b implements Runnable {
    private /* synthetic */ MobFoxView a;

    b(MobFoxView mobFoxView) {
        this.a = mobFoxView;
    }

    public final void run() {
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "starting request thread");
        }
        new m();
        try {
            a unused = this.a.i = m.a(MobFoxView.d(this.a));
            if (this.a.i != null) {
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "response received");
                }
                if (Log.isLoggable("MOBFOX", 3)) {
                    Log.d("MOBFOX", "getVisibility: " + this.a.getVisibility());
                }
                this.a.a.post(this.a.b);
            }
        } catch (Throwable th) {
            if (Log.isLoggable("MOBFOX", 6)) {
                Log.e("MOBFOX", "Uncaught exception in request thread", th);
            }
            if (this.a.u != null) {
                Log.d("MOBFOX", "notify bannerListener: " + this.a.u.getClass().getName());
                if (!(th instanceof a)) {
                    new a(th);
                }
            }
            Log.e("MOBFOX", th.getMessage(), th);
        }
        Thread unused2 = this.a.w = null;
        if (Log.isLoggable("MOBFOX", 3)) {
            Log.d("MOBFOX", "finishing request thread");
        }
    }
}
