package com.mobfox.sdk;

import android.util.Log;
import java.lang.Thread;

final class f implements Thread.UncaughtExceptionHandler {
    private /* synthetic */ MobFoxView a;

    f(MobFoxView mobFoxView) {
        this.a = mobFoxView;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        if (Log.isLoggable("MOBFOX", 6)) {
            Log.e("MOBFOX", "Uncaught exception in request thread", th);
        }
        Thread unused = this.a.w = null;
    }
}
