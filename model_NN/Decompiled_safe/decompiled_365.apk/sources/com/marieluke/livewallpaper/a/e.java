package com.marieluke.livewallpaper.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import com.marieluke.admiralty.free.R;

public final class e {
    private boolean a;
    private boolean b;
    private boolean c;
    private boolean d = true;
    private float e = 0.0f;
    private BitmapFactory.Options f = new BitmapFactory.Options();
    private int g;
    private int h;
    private Context i;
    private int j = 20;
    private final int[] k = {R.drawable.b1, R.drawable.b2, R.drawable.b3, R.drawable.b4, R.drawable.b5, R.drawable.b6, R.drawable.b7, R.drawable.b8, R.drawable.b9, R.drawable.b10, R.drawable.b11, R.drawable.b12, R.drawable.b13, R.drawable.b14, R.drawable.b15, R.drawable.b16, R.drawable.b17, R.drawable.b18, R.drawable.b19, R.drawable.b20};
    private final int[] l = {R.drawable.s1, R.drawable.s2, R.drawable.s3, R.drawable.s4, R.drawable.s5, R.drawable.s6, R.drawable.s7, R.drawable.s8, R.drawable.s9, R.drawable.s10, R.drawable.s11, R.drawable.s12, R.drawable.s13, R.drawable.s14, R.drawable.s15, R.drawable.s16, R.drawable.s17, R.drawable.s18, R.drawable.s19, R.drawable.s20};
    private Bitmap[] m = new Bitmap[this.j];

    public e(Context context) {
        this.f.inPurgeable = true;
        this.f.inPreferredConfig = Bitmap.Config.ARGB_8888;
        this.i = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public final Bitmap a(int i2) {
        Bitmap a2;
        if (!this.d) {
            for (int i3 = 0; i3 < this.m.length; i3++) {
                this.m[i3] = null;
            }
            System.gc();
        }
        try {
            if (this.m[i2] == null) {
                Bitmap[] bitmapArr = this.m;
                int i4 = this.h;
                if (this.a) {
                    this.e = (float) this.g;
                    if (i4 > 480) {
                        a2 = b.a(BitmapFactory.decodeResource(this.i.getResources(), this.k[i2], this.f), ((float) this.h) / Float.parseFloat("1280"));
                    } else {
                        a2 = b.a(BitmapFactory.decodeResource(this.i.getResources(), this.l[i2], this.f), ((float) this.h) / Float.parseFloat("674"));
                    }
                } else if (i4 >= 800) {
                    this.e = Float.parseFloat("1280");
                    a2 = BitmapFactory.decodeResource(this.i.getResources(), this.k[i2], this.f);
                } else if (i4 > 480) {
                    float parseFloat = ((float) this.h) / Float.parseFloat("854");
                    this.e = Float.parseFloat("1280") * parseFloat;
                    a2 = b.a(BitmapFactory.decodeResource(this.i.getResources(), this.k[i2], this.f), parseFloat);
                } else if (this.h == 480) {
                    this.e = Float.parseFloat("674");
                    a2 = BitmapFactory.decodeResource(this.i.getResources(), this.l[i2], this.f);
                } else {
                    float parseFloat2 = ((float) this.h) / Float.parseFloat("480");
                    this.e = Float.parseFloat("674") * parseFloat2;
                    a2 = b.a(BitmapFactory.decodeResource(this.i.getResources(), this.l[i2], this.f), parseFloat2);
                }
                bitmapArr[i2] = a2;
                if (this.a) {
                    Bitmap[] bitmapArr2 = this.m;
                    Bitmap bitmap = this.m[i2];
                    if (bitmap != null) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90.0f);
                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    }
                    bitmapArr2[i2] = bitmap;
                }
                if (this.b) {
                    Bitmap[] bitmapArr3 = this.m;
                    Bitmap bitmap2 = this.m[i2];
                    if (bitmap2 != null) {
                        Bitmap createBitmap = Bitmap.createBitmap(bitmap2.getWidth(), bitmap2.getHeight(), Bitmap.Config.RGB_565);
                        Canvas canvas = new Canvas(createBitmap);
                        Paint paint = new Paint();
                        ColorMatrix colorMatrix = new ColorMatrix();
                        colorMatrix.setSaturation(0.0f);
                        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
                        canvas.drawBitmap(bitmap2, 0.0f, 0.0f, paint);
                        bitmap2 = createBitmap;
                    }
                    bitmapArr3[i2] = bitmap2;
                }
                if (this.c) {
                    Bitmap[] bitmapArr4 = this.m;
                    Bitmap bitmap3 = this.m[i2];
                    if (bitmap3 != null) {
                        Matrix matrix2 = new Matrix();
                        matrix2.setTranslate((float) (bitmap3.getWidth() + 10), (float) (bitmap3.getHeight() + 10));
                        matrix2.preScale(-1.0f, 1.0f);
                        bitmap3 = Bitmap.createBitmap(bitmap3, 0, 0, bitmap3.getWidth(), bitmap3.getHeight(), matrix2, true);
                    }
                    bitmapArr4[i2] = bitmap3;
                }
                if (i2 % 4 == 0 || this.h >= 800) {
                    System.gc();
                }
            }
            return this.m[i2];
        } catch (OutOfMemoryError e2) {
            this.d = false;
            return null;
        }
    }

    public final void a() {
        this.m = new Bitmap[this.j];
    }

    public final void a(int i2, int i3) {
        this.h = i3;
        this.g = i2;
    }

    public final void a(boolean z) {
        this.a = z;
    }

    public final int b() {
        return this.j;
    }

    public final void b(boolean z) {
        this.b = z;
    }

    public final void c(boolean z) {
        this.c = z;
    }

    public final boolean c() {
        return this.a;
    }

    public final boolean d() {
        return this.b;
    }

    public final boolean e() {
        return this.c;
    }

    public final float f() {
        return this.e;
    }
}
