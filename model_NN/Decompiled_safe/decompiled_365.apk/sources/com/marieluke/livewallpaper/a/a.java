package com.marieluke.livewallpaper.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

public final class a {
    private d a;
    private SurfaceHolder b;
    private c c;
    private Context d;
    private e e;

    public final void a() {
        this.a.b();
    }

    public final void a(int i) {
        this.a.a(i);
    }

    public final void a(Context context, SurfaceHolder surfaceHolder, int i, int i2, e eVar) {
        this.a = new d(this, eVar);
        this.b = surfaceHolder;
        this.d = context;
        this.e = eVar;
        this.c = new c(this.d, this.b, this.e);
        this.c.a(i, i2);
    }

    public final void a(Bitmap bitmap) {
        Canvas canvas;
        try {
            canvas = this.b.lockCanvas(null);
        } catch (Exception e2) {
            canvas = null;
        }
        synchronized (this.b) {
            if (bitmap != null) {
                this.c.a(canvas, bitmap);
            } else {
                this.c.a(canvas, (Bitmap) null);
            }
        }
        if (canvas != null) {
            this.b.unlockCanvasAndPost(canvas);
        }
    }

    public final void a(boolean z) {
        this.a.a(z);
    }

    public final void b() {
        a(this.e.a(0));
    }

    public final void b(boolean z) {
        this.a.b(z);
    }

    public final void c() {
        this.a.a();
    }

    public final void c(boolean z) {
        this.e.a(z);
    }

    public final void d() {
        this.a.c();
    }

    public final void d(boolean z) {
        this.e.b(z);
    }

    public final void e() {
        this.a.b();
    }

    public final void e(boolean z) {
        this.e.c(z);
    }

    public final boolean f() {
        return this.e.c();
    }

    public final boolean g() {
        return this.e.d();
    }

    public final boolean h() {
        return this.e.e();
    }

    public final c i() {
        return this.c;
    }

    public final Bitmap j() {
        return this.a.d();
    }

    public final e k() {
        return this.e;
    }
}
