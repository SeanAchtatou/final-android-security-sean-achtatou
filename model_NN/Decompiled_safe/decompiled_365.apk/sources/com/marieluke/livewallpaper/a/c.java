package com.marieluke.livewallpaper.a;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;

public final class c {
    private Context a;
    private SurfaceHolder b;
    private Rect c;
    private Bitmap d;
    private String e;
    private int f;
    private int g;
    private int h;
    private int i;
    private e j;
    private Paint k;
    private Paint l;
    private String m = "PhotoDisplay";

    public c(Context context, SurfaceHolder surfaceHolder, e eVar) {
        this.a = context;
        this.b = surfaceHolder;
        this.c = this.b.getSurfaceFrame();
        this.j = eVar;
        this.k = new Paint();
        this.k.setColor(-1);
        this.k.setStyle(Paint.Style.FILL);
        this.k.setTextSize(12.0f);
        this.l = new Paint();
        this.l.setAlpha(255);
    }

    public final void a(int i2) {
        this.h = i2;
    }

    public final void a(int i2, int i3) {
        this.g = i3;
        this.f = i2;
    }

    public final void a(Canvas canvas, Bitmap bitmap) {
        int i2;
        if (bitmap != null) {
            this.d = bitmap;
        }
        int i3 = this.h;
        if (this.c == null) {
            Log.d(this.m, "surfaceFrame == null!");
        }
        if (i3 == 0) {
            Log.d(this.m, "virtualWidth == 0 !!");
            return;
        }
        int f2 = (int) this.j.f();
        if (f2 > 0) {
            i2 = "l".equals(this.e) ? 0 : "c".equals(this.e) ? f2 - (WallpaperManager.getInstance(this.a).getDesiredMinimumWidth() / 2) : "r".equals(this.e) ? f2 - WallpaperManager.getInstance(this.a).getDesiredMinimumWidth() : 0;
            if (i2 < 0) {
                i2 = 0;
            }
        } else {
            i2 = 0;
        }
        if (canvas != null && this.d != null) {
            if (this.j.c()) {
                canvas.drawBitmap(this.d, 0.0f, 0.0f, this.l);
            } else {
                canvas.drawBitmap(this.d, (float) ((-this.i) - i2), 0.0f, this.l);
            }
        }
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void b(int i2) {
        this.i = i2;
    }
}
