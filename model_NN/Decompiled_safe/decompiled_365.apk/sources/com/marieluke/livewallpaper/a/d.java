package com.marieluke.livewallpaper.a;

import android.graphics.Bitmap;

public final class d extends Thread {
    private a a;
    private boolean b;
    private int c;
    private int d = 10;
    private boolean e;
    private boolean f;
    private boolean g;
    private e h;

    public d(a aVar, e eVar) {
        this.a = aVar;
        this.h = eVar;
    }

    public final void a() {
        this.b = false;
        start();
    }

    public final void a(int i) {
        this.d = i;
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final void b() {
        this.b = false;
        synchronized (this) {
            notify();
        }
    }

    public final void b(boolean z) {
        this.f = z;
    }

    public final void c() {
        this.b = true;
        synchronized (this) {
            notify();
        }
    }

    public final Bitmap d() {
        return this.h.a(this.c);
    }

    public final void run() {
        while (true) {
            try {
                a aVar = this.a;
                if (this.f) {
                    if (this.e) {
                        this.c--;
                        if (this.c <= 0) {
                            this.c = this.h.b() - 1;
                        }
                    } else {
                        this.c++;
                        if (this.c >= this.h.b()) {
                            this.c = 0;
                        }
                    }
                } else if (this.g) {
                    this.c++;
                    if (this.c >= this.h.b()) {
                        this.g = !this.g;
                        this.c -= 2;
                    }
                } else {
                    this.c--;
                    if (this.c <= 0) {
                        this.g = !this.g;
                        this.c += 2;
                    }
                }
                aVar.a(this.h.a(this.c));
                if (this.d == -1) {
                    this.b = true;
                    synchronized (this) {
                        notify();
                    }
                } else {
                    Thread.sleep((long) (1000 / this.d));
                }
                if (this.b) {
                    synchronized (this) {
                        while (this.b) {
                            wait();
                        }
                    }
                } else {
                    continue;
                }
            } catch (Exception e2) {
            }
        }
    }
}
