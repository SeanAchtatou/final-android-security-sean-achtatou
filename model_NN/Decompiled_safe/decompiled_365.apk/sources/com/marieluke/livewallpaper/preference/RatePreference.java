package com.marieluke.livewallpaper.preference;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.Preference;
import android.util.AttributeSet;
import android.widget.Toast;

public class RatePreference extends Preference {
    private Context a;

    public RatePreference(Context context) {
        super(context);
        this.a = context;
    }

    public RatePreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
    }

    public RatePreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = context;
    }

    /* access modifiers changed from: protected */
    public void onClick() {
        super.onClick();
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse("market://details?id=com.marieluke.admiralty.free")));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this.a, "Market not installed.", 10).show();
        }
    }
}
