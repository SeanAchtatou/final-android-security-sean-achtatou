package com.marieluke.livewallpaper.preference;

import android.content.Context;
import android.content.Intent;
import android.preference.Preference;
import android.util.AttributeSet;
import com.marieluke.admiralty.free.R;
import com.marieluke.livewallpaper.b.a;

public class SendToPreference extends Preference {
    private Context a;

    public SendToPreference(Context context) {
        super(context);
        this.a = context;
    }

    public SendToPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
    }

    public SendToPreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = context;
    }

    /* access modifiers changed from: protected */
    public void onClick() {
        super.onClick();
        new a();
        Context context = this.a;
        String string = this.a.getString(R.string.app_name);
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.SUBJECT", string);
        intent.putExtra("android.intent.extra.TEXT", "market://details?id=com.marieluke.admiralty.free");
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.share)));
    }
}
