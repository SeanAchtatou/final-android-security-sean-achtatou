package com.marieluke.livewallpaper.preference;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

public class BasicDialogPreference extends DialogPreference {
    public BasicDialogPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public BasicDialogPreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
