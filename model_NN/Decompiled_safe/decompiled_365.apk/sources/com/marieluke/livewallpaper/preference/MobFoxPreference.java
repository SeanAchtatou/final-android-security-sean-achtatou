package com.marieluke.livewallpaper.preference;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.marieluke.admiralty.free.R;

public class MobFoxPreference extends Preference {
    public MobFoxPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MobFoxPreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public View onCreateView(ViewGroup viewGroup) {
        return ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.ads_preference, (ViewGroup) null);
    }
}
