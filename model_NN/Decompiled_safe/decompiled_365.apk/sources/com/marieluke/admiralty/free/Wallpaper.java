package com.marieluke.admiralty.free;

import android.service.wallpaper.WallpaperService;

public class Wallpaper extends WallpaperService {
    public void onCreate() {
        super.onCreate();
    }

    public WallpaperService.Engine onCreateEngine() {
        return new a(this);
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
