package com.marieluke.admiralty.free;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class WallpaperSettings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getListView().setBackgroundColor(-16777216);
        getListView().setCacheColorHint(-16777216);
        getPreferenceManager().setSharedPreferencesName("com.marieluke.admiralty.free");
        addPreferencesFromResource(R.xml.wallpaper_settings);
        getWindow().setBackgroundDrawable(new ColorDrawable(-1));
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
    }
}
