package com.marieluke.admiralty.free;

import android.app.WallpaperManager;
import android.content.SharedPreferences;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import com.marieluke.livewallpaper.a.e;
import com.marieluke.livewallpaper.b.b;

public final class a extends WallpaperService.Engine implements SharedPreferences.OnSharedPreferenceChangeListener {
    private com.marieluke.livewallpaper.a.a a;
    private int b;
    private float c;
    private float d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private boolean j = false;
    private boolean k = false;
    private boolean l;
    private boolean m = false;
    private boolean n = false;
    private boolean o = false;
    private boolean p = true;
    private String q = "";
    private e r;
    private /* synthetic */ Wallpaper s;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(Wallpaper wallpaper) {
        super(wallpaper);
        this.s = wallpaper;
        this.r = new e(wallpaper.getBaseContext());
        this.a = new com.marieluke.livewallpaper.a.a();
        this.a.a(wallpaper.getBaseContext(), getSurfaceHolder(), this.f, this.e, this.r);
        a();
        this.r.a(this.f, this.e);
        SharedPreferences sharedPreferences = wallpaper.getSharedPreferences("com.marieluke.admiralty.free", 0);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        onSharedPreferenceChanged(sharedPreferences, null);
        b.a(wallpaper.getBaseContext(), "Admiralty Live Wallpaper Free", "Resolution", WallpaperManager.getInstance(wallpaper.getBaseContext()).getDesiredMinimumWidth() + "," + WallpaperManager.getInstance(wallpaper.getBaseContext()).getDesiredMinimumHeight());
    }

    private void a() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.s.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        if (this.s.getResources().getConfiguration().orientation == 2) {
            this.f = displayMetrics.heightPixels;
            this.e = displayMetrics.widthPixels;
        }
        this.e = displayMetrics.heightPixels;
        this.f = displayMetrics.widthPixels;
        this.e = Math.round(((float) this.e) / displayMetrics.scaledDensity);
        this.f = Math.round(((float) this.f) / displayMetrics.scaledDensity);
        this.a.i().a(this.f, this.e);
    }

    public final void onCreate(SurfaceHolder surfaceHolder) {
        super.onCreate(surfaceHolder);
        this.g = getDesiredMinimumWidth();
        this.a.i().a(this.g);
    }

    public final void onDesiredSizeChanged(int i2, int i3) {
        super.onDesiredSizeChanged(i2, i3);
        a();
        this.g = i2;
        this.a.i().a(this.g);
    }

    public final void onDestroy() {
        super.onDestroy();
        this.a.d();
        System.gc();
        b.a();
    }

    public final void onOffsetsChanged(float f2, float f3, float f4, float f5, int i2, int i3) {
        if (this.b != i2 * -1 || this.c != f2 || this.d != f4) {
            this.b = i2 * -1;
            this.c = f2;
            this.d = f4;
            if (this.b != 0 || this.c == 0.0f) {
                this.h = this.b;
            } else {
                this.h = Math.round(((float) this.f) * this.c);
            }
            if (this.f > 1000 || this.m) {
                this.a.i().b(0);
            } else if (this.p) {
                this.a.i().b(this.h);
            }
            if (this.j) {
                this.a.a(this.a.j());
            }
        }
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        boolean z;
        b.a(this.s.getBaseContext(), "Admiralty Live Wallpaper Free", "Preference");
        this.i = Integer.valueOf(sharedPreferences.getString("fps", "10")).intValue();
        this.j = sharedPreferences.getBoolean("static", false);
        this.p = sharedPreferences.getBoolean("scroll", true);
        if (sharedPreferences.getString("move", "f").equals("f")) {
            this.k = false;
        } else {
            this.k = true;
        }
        this.a.a(this.k);
        this.q = sharedPreferences.getString("align", "l");
        this.a.i().a(this.q);
        this.n = sharedPreferences.getBoolean("blackwhite", false);
        this.o = sharedPreferences.getBoolean("flip", false);
        if (sharedPreferences.getString("repeat", "f").equals("f")) {
            this.l = true;
        } else {
            this.l = false;
        }
        this.a.b(this.l);
        if (this.j) {
            this.a.a(-1);
        } else {
            this.a.e();
            this.a.a(this.i);
        }
        this.m = sharedPreferences.getBoolean("rotate", false);
        if (this.m) {
            this.p = false;
        }
        if (this.a.f() != this.m) {
            this.a.c(this.m);
            z = true;
        } else {
            z = false;
        }
        if (this.a.g() != this.n) {
            this.a.d(this.n);
            z = true;
        }
        if (this.a.h() != this.o) {
            this.a.e(this.o);
            z = true;
        }
        if (z) {
            this.a.k().a();
            System.gc();
            try {
                if (isPreview()) {
                    this.a.b();
                }
            } catch (Exception e2) {
            }
        }
    }

    public final void onSurfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        super.onSurfaceChanged(surfaceHolder, i2, i3, i4);
    }

    public final void onSurfaceCreated(SurfaceHolder surfaceHolder) {
        super.onSurfaceCreated(surfaceHolder);
        try {
            if (isPreview()) {
                this.a.b();
            } else {
                this.a.c();
            }
        } catch (Exception e2) {
        }
    }

    public final void onSurfaceDestroyed(SurfaceHolder surfaceHolder) {
        super.onSurfaceDestroyed(surfaceHolder);
        this.a.d();
    }

    public final void onVisibilityChanged(boolean z) {
        super.onVisibilityChanged(z);
        a();
        if (z) {
            this.a.a();
        } else {
            this.a.d();
        }
    }
}
