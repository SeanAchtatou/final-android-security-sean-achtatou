package com.a.a.a.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class t extends SQLiteOpenHelper {
    private final int a;

    public t(Context context) {
        this(context, "google_analytics.db");
    }

    private t(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 3);
        this.a = 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_variables;");
        sQLiteDatabase.execSQL(h.m);
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_var_cache;");
        sQLiteDatabase.execSQL(h.n);
        for (int i = 1; i <= 5; i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_id", (Integer) 0);
            contentValues.put("cv_index", Integer.valueOf(i));
            contentValues.put("cv_name", "");
            contentValues.put("cv_scope", (Integer) 3);
            contentValues.put("cv_value", "");
            sQLiteDatabase.insert("custom_var_cache", "event_id", contentValues);
        }
    }

    private static void b(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS transaction_events;");
        sQLiteDatabase.execSQL(h.o);
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS item_events;");
        sQLiteDatabase.execSQL(h.p);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS events;");
        sQLiteDatabase.execSQL(h.k);
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS session;");
        sQLiteDatabase.execSQL(h.l);
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS install_referrer;");
        sQLiteDatabase.execSQL("CREATE TABLE install_referrer (referrer TEXT PRIMARY KEY NOT NULL);");
        if (this.a > 1) {
            a(sQLiteDatabase);
        }
        if (this.a > 2) {
            b(sQLiteDatabase);
        }
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i < 2 && i2 > 1) {
            a(sQLiteDatabase);
        }
        if (i < 3 && i2 > 2) {
            b(sQLiteDatabase);
        }
    }
}
