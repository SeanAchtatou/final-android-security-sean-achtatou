package com.a.a.a.a;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public final class p {
    private p() {
    }

    static String a(String str, String str2) {
        try {
            return URLEncoder.encode(str, str2).replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("URL encoding failed for: " + str);
        }
    }
}
