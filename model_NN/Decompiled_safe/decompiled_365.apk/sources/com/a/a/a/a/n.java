package com.a.a.a.a;

import android.os.Handler;
import android.os.HandlerThread;

final class n extends HandlerThread {
    private Handler a;
    /* access modifiers changed from: private */
    public final r b;
    /* access modifiers changed from: private */
    public final String c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public k h;
    /* access modifiers changed from: private */
    public final w i;
    /* access modifiers changed from: private */
    public final o j;
    /* access modifiers changed from: private */
    public final a k;

    private n(w wVar, r rVar, String str, String str2, a aVar) {
        super("DispatcherThread");
        this.f = 30;
        this.h = null;
        this.i = wVar;
        this.c = str;
        this.d = str2;
        this.b = rVar;
        this.j = new o(this);
        this.b.a = this.j;
        this.k = aVar;
    }

    /* synthetic */ n(w wVar, String str, String str2, a aVar) {
        this(wVar, str, str2, aVar, (byte) 0);
    }

    private n(w wVar, String str, String str2, a aVar, byte b2) {
        this(wVar, new r(a.a), str, str2, aVar);
    }

    static /* synthetic */ long c(n nVar) {
        long j2 = nVar.g * 2;
        nVar.g = j2;
        return j2;
    }

    public final void a(j[] jVarArr) {
        if (this.a != null) {
            this.a.post(new k(this, jVarArr));
        }
    }

    /* access modifiers changed from: protected */
    public final void onLooperPrepared() {
        this.a = new Handler();
    }
}
