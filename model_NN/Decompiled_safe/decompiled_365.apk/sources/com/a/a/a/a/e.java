package com.a.a.a.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public final class e {
    private static final e a = new e();
    private boolean b = false;
    private boolean c = false;
    private String d;
    private Context e;
    private ConnectivityManager f;
    private String g = "GoogleAnalytics";
    private String h = "1.2";
    private int i;
    /* access modifiers changed from: private */
    public m j;
    private d k;
    private boolean l;
    private boolean m;
    private g n;
    private Map o = new HashMap();
    private Map p = new HashMap();
    /* access modifiers changed from: private */
    public Handler q;
    private Runnable r = new u(this);

    private e() {
    }

    public static e a() {
        return a;
    }

    private void f() {
        if (this.i >= 0 && this.q.postDelayed(this.r, (long) (this.i * 1000)) && this.b) {
            Log.v("GoogleAnalyticsTracker", "Scheduled next dispatch");
        }
    }

    private void g() {
        this.q.removeCallbacks(this.r);
    }

    public final void a(String str, Context context) {
        d dVar;
        m hVar = this.j == null ? new h(new t(context)) : this.j;
        if (this.k == null) {
            dVar = new a(this.g, this.h);
            dVar.a(this.c);
        } else {
            dVar = this.k;
        }
        b bVar = new b(this);
        this.d = str;
        this.e = context;
        this.j = hVar;
        this.j.d();
        this.k = dVar;
        this.k.a(bVar, this.j.e());
        this.m = false;
        if (this.f == null) {
            this.f = (ConnectivityManager) this.e.getSystemService("connectivity");
        }
        if (this.q == null) {
            this.q = new Handler(context.getMainLooper());
        } else {
            g();
        }
        int i2 = this.i;
        this.i = 20;
        if (i2 <= 0) {
            f();
        } else if (i2 > 0) {
            g();
            f();
        }
    }

    public final void a(String str, String str2, String str3) {
        String str4 = this.d;
        j jVar = new j(this.j.c(), str4, str, str2, str3, this.e.getResources().getDisplayMetrics().widthPixels, this.e.getResources().getDisplayMetrics().heightPixels);
        jVar.o = this.n;
        this.n = new g();
        this.j.a(jVar);
        if (this.l) {
            this.l = false;
            f();
        }
    }

    public final boolean b() {
        if (this.b) {
            Log.v("GoogleAnalyticsTracker", "Called dispatch");
        }
        if (this.m) {
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "...but dispatcher was busy");
            }
            f();
            return false;
        }
        NetworkInfo activeNetworkInfo = this.f.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "...but there was no network available");
            }
            f();
            return false;
        } else if (this.j.b() != 0) {
            j[] a2 = this.j.a();
            this.k.a(a2);
            this.m = true;
            f();
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "Sending " + a2.length + " to dispatcher");
            }
            return true;
        } else {
            this.l = true;
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "...but there was nothing to dispatch");
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.m = false;
    }

    public final void d() {
        this.k.b();
        g();
    }

    public final boolean e() {
        return this.b;
    }
}
