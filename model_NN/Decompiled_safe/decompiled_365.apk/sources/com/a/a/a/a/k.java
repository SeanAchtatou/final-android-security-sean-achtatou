package com.a.a.a.a;

import android.util.Log;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import org.apache.http.HttpException;
import org.apache.http.message.BasicHttpRequest;

final class k implements Runnable {
    private final LinkedList a = new LinkedList();
    private /* synthetic */ n b;

    public k(n nVar, j[] jVarArr) {
        this.b = nVar;
        Collections.addAll(this.a, jVarArr);
    }

    public final j a() {
        return (j) this.a.poll();
    }

    public final void run() {
        k unused = this.b.h = this;
        int i = 0;
        while (i < 5 && this.a.size() > 0) {
            long j = 0;
            try {
                if (this.b.e == 500 || this.b.e == 503) {
                    j = (long) (Math.random() * ((double) this.b.g));
                    if (this.b.g < 256) {
                        n.c(this.b);
                    }
                } else {
                    long unused2 = this.b.g = 2;
                }
                Thread.sleep(j * 1000);
                boolean a2 = this.b.k.a();
                if (e.a().e() && a2) {
                    Log.v("GoogleAnalyticsTracker", "dispatching events in dry run mode");
                }
                int i2 = 0;
                while (i2 < this.a.size() && i2 < this.b.f) {
                    j jVar = (j) this.a.get(i2);
                    BasicHttpRequest basicHttpRequest = new BasicHttpRequest("GET", "__##GOOGLEPAGEVIEW##__".equals(jVar.i) ? l.a(jVar, this.b.c) : "__##GOOGLETRANSACTION##__".equals(jVar.i) ? l.c(jVar, this.b.c) : "__##GOOGLEITEM##__".equals(jVar.i) ? l.d(jVar, this.b.c) : l.b(jVar, this.b.c));
                    basicHttpRequest.addHeader("Host", a.a.getHostName());
                    basicHttpRequest.addHeader("User-Agent", this.b.d);
                    if (e.a().e()) {
                        Log.i("GoogleAnalyticsTracker", basicHttpRequest.getRequestLine().toString());
                    }
                    if (a2) {
                        this.b.j.b();
                    } else {
                        this.b.b.a(basicHttpRequest);
                    }
                    i2++;
                }
                if (!a2) {
                    this.b.b.a();
                }
                i++;
            } catch (InterruptedException e) {
                Log.w("GoogleAnalyticsTracker", "Couldn't sleep.", e);
            } catch (IOException e2) {
                Log.w("GoogleAnalyticsTracker", "Problem with socket or streams.", e2);
            } catch (HttpException e3) {
                Log.w("GoogleAnalyticsTracker", "Problem with http streams.", e3);
            }
        }
        this.b.b.b();
        this.b.i.a();
        k unused3 = this.b.h = (k) null;
    }
}
