package com.a.a.a.a;

public final class i {
    private final String a;
    private final String b;
    private final String c;
    private final String d;
    private final double e;
    private final long f;

    /* synthetic */ i(c cVar) {
        this(cVar, (byte) 0);
    }

    private i(c cVar, byte b2) {
        this.a = cVar.a;
        this.b = cVar.b;
        this.e = cVar.c;
        this.f = cVar.d;
        this.c = cVar.e;
        this.d = cVar.f;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final double e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final long f() {
        return this.f;
    }
}
