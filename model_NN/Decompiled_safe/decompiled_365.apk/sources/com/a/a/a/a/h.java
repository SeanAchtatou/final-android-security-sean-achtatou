package com.a.a.a.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

final class h implements m {
    /* access modifiers changed from: private */
    public static final String k = ("CREATE TABLE events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "event_id") + String.format(" '%s' INTEGER NOT NULL,", "user_id") + String.format(" '%s' CHAR(256) NOT NULL,", "account_id") + String.format(" '%s' INTEGER NOT NULL,", "random_val") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_first") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_previous") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_current") + String.format(" '%s' INTEGER NOT NULL,", "visits") + String.format(" '%s' CHAR(256) NOT NULL,", "category") + String.format(" '%s' CHAR(256) NOT NULL,", "action") + String.format(" '%s' CHAR(256), ", "label") + String.format(" '%s' INTEGER,", "value") + String.format(" '%s' INTEGER,", "screen_width") + String.format(" '%s' INTEGER);", "screen_height"));
    /* access modifiers changed from: private */
    public static final String l = ("CREATE TABLE session (" + String.format(" '%s' INTEGER PRIMARY KEY,", "timestamp_first") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_previous") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_current") + String.format(" '%s' INTEGER NOT NULL,", "visits") + String.format(" '%s' INTEGER NOT NULL);", "store_id"));
    /* access modifiers changed from: private */
    public static final String m = ("CREATE TABLE custom_variables (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "cv_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' INTEGER NOT NULL,", "cv_index") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_name") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_value") + String.format(" '%s' INTEGER NOT NULL);", "cv_scope"));
    /* access modifiers changed from: private */
    public static final String n = ("CREATE TABLE custom_var_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "cv_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' INTEGER NOT NULL,", "cv_index") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_name") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_value") + String.format(" '%s' INTEGER NOT NULL);", "cv_scope"));
    /* access modifiers changed from: private */
    public static final String o = ("CREATE TABLE transaction_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "tran_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' TEXT NOT NULL,", "order_id") + String.format(" '%s' TEXT,", "tran_storename") + String.format(" '%s' TEXT NOT NULL,", "tran_totalcost") + String.format(" '%s' TEXT,", "tran_totaltax") + String.format(" '%s' TEXT);", "tran_shippingcost"));
    /* access modifiers changed from: private */
    public static final String p = ("CREATE TABLE item_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "item_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' TEXT NOT NULL,", "order_id") + String.format(" '%s' TEXT NOT NULL,", "item_sku") + String.format(" '%s' TEXT,", "item_name") + String.format(" '%s' TEXT,", "item_category") + String.format(" '%s' TEXT NOT NULL,", "item_price") + String.format(" '%s' TEXT NOT NULL);", "item_count"));
    private t a;
    private int b;
    private long c;
    private long d;
    private long e;
    private int f;
    private int g;
    private boolean h;
    private boolean i;
    private SQLiteStatement j = null;

    h(t tVar) {
        this.a = tVar;
        try {
            tVar.getWritableDatabase().close();
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    private void a(j jVar, long j2) {
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            g gVar = jVar.o;
            if (this.i) {
                if (gVar == null) {
                    gVar = new g();
                    jVar.o = gVar;
                }
                g m2 = m();
                for (int i2 = 1; i2 <= 5; i2++) {
                    s b2 = m2.b(i2);
                    s b3 = gVar.b(i2);
                    if (b2 != null && b3 == null) {
                        gVar.a(b2);
                    }
                }
                this.i = false;
            }
            if (gVar != null) {
                for (int i3 = 1; i3 <= 5; i3++) {
                    if (!gVar.a(i3)) {
                        s b4 = gVar.b(i3);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("event_id", Long.valueOf(j2));
                        contentValues.put("cv_index", Integer.valueOf(b4.d()));
                        contentValues.put("cv_name", b4.b());
                        contentValues.put("cv_scope", Integer.valueOf(b4.a()));
                        contentValues.put("cv_value", b4.c());
                        writableDatabase.insert("custom_variables", "event_id", contentValues);
                        writableDatabase.update("custom_var_cache", contentValues, "cv_index=" + b4.d(), null);
                    }
                }
            }
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0096  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.a.a.a.a.f b(long r11) {
        /*
            r10 = this;
            r8 = 0
            com.a.a.a.a.t r0 = r10.a     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            java.lang.String r1 = "transaction_events"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0081, all -> 0x0092 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            if (r1 == 0) goto L_0x007a
            com.a.a.a.a.v r1 = new com.a.a.a.a.v     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            java.lang.String r2 = "order_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            java.lang.String r3 = "tran_totalcost"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            double r3 = r0.getDouble(r3)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            r1.<init>(r2, r3)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            java.lang.String r2 = "tran_storename"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            com.a.a.a.a.v r1 = r1.a(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            java.lang.String r2 = "tran_totaltax"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            com.a.a.a.a.v r1 = r1.a(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            java.lang.String r2 = "tran_shippingcost"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            com.a.a.a.a.v r1 = r1.b(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            com.a.a.a.a.f r2 = new com.a.a.a.a.f     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            r2.<init>(r1)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009a }
            if (r0 == 0) goto L_0x0078
            r0.close()
        L_0x0078:
            r0 = r2
        L_0x0079:
            return r0
        L_0x007a:
            if (r0 == 0) goto L_0x007f
            r0.close()
        L_0x007f:
            r0 = r8
            goto L_0x0079
        L_0x0081:
            r0 = move-exception
            r1 = r8
        L_0x0083:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009f }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x009f }
            if (r1 == 0) goto L_0x007f
            r1.close()
            goto L_0x007f
        L_0x0092:
            r0 = move-exception
            r1 = r8
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()
        L_0x0099:
            throw r0
        L_0x009a:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0094
        L_0x009f:
            r0 = move-exception
            goto L_0x0094
        L_0x00a1:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.h.b(long):com.a.a.a.a.f");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x009c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.a.a.a.a.i c(long r10) {
        /*
            r9 = this;
            r8 = 0
            com.a.a.a.a.t r0 = r9.a     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            java.lang.String r1 = "item_events"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0087, all -> 0x0098 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            if (r0 == 0) goto L_0x0080
            com.a.a.a.a.c r0 = new com.a.a.a.a.c     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r1 = "order_id"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = "item_sku"
            int r2 = r7.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r2 = r7.getString(r2)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r3 = "item_price"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            double r3 = r7.getDouble(r3)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r5 = "item_count"
            int r5 = r7.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            long r5 = r7.getLong(r5)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            r0.<init>(r1, r2, r3, r5)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r1 = "item_name"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            com.a.a.a.a.c r0 = r0.a(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r1 = "item_category"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            com.a.a.a.a.c r0 = r0.b(r1)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            com.a.a.a.a.i r1 = new com.a.a.a.a.i     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            r1.<init>(r0)     // Catch:{ SQLiteException -> 0x00a5, all -> 0x00a0 }
            if (r7 == 0) goto L_0x007e
            r7.close()
        L_0x007e:
            r0 = r1
        L_0x007f:
            return r0
        L_0x0080:
            if (r7 == 0) goto L_0x0085
            r7.close()
        L_0x0085:
            r0 = r8
            goto L_0x007f
        L_0x0087:
            r0 = move-exception
            r1 = r8
        L_0x0089:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a3 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00a3 }
            if (r1 == 0) goto L_0x0085
            r1.close()
            goto L_0x0085
        L_0x0098:
            r0 = move-exception
            r1 = r8
        L_0x009a:
            if (r1 == 0) goto L_0x009f
            r1.close()
        L_0x009f:
            throw r0
        L_0x00a0:
            r0 = move-exception
            r1 = r7
            goto L_0x009a
        L_0x00a3:
            r0 = move-exception
            goto L_0x009a
        L_0x00a5:
            r0 = move-exception
            r1 = r7
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.h.c(long):com.a.a.a.a.i");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.a.a.a.a.g d(long r12) {
        /*
            r11 = this;
            r9 = 0
            com.a.a.a.a.g r8 = new com.a.a.a.a.g
            r8.<init>()
            com.a.a.a.a.t r0 = r11.a     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r1 = "custom_variables"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.StringBuilder r3 = r3.append(r12)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
        L_0x002a:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            if (r1 == 0) goto L_0x0074
            com.a.a.a.a.s r1 = new com.a.a.a.a.s     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r2 = "cv_index"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            int r2 = r0.getInt(r2)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r3 = "cv_name"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r4 = "cv_value"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r5 = "cv_scope"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            int r5 = r0.getInt(r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            r1.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            r8.a(r1)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            goto L_0x002a
        L_0x0061:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0065:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0087 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0087 }
            if (r1 == 0) goto L_0x0073
            r1.close()
        L_0x0073:
            return r8
        L_0x0074:
            if (r0 == 0) goto L_0x0073
            r0.close()
            goto L_0x0073
        L_0x007a:
            r0 = move-exception
            r1 = r9
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r1.close()
        L_0x0081:
            throw r0
        L_0x0082:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x007c
        L_0x0087:
            r0 = move-exception
            goto L_0x007c
        L_0x0089:
            r0 = move-exception
            r1 = r9
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.h.d(long):com.a.a.a.a.g");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0135  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.a.a.a.a.j[] l() {
        /*
            r21 = this;
            java.util.ArrayList r19 = new java.util.ArrayList
            r19.<init>()
            r12 = 0
            r0 = r21
            com.a.a.a.a.t r0 = r0.a     // Catch:{ SQLiteException -> 0x015f, all -> 0x015a }
            r3 = r0
            android.database.sqlite.SQLiteDatabase r3 = r3.getReadableDatabase()     // Catch:{ SQLiteException -> 0x015f, all -> 0x015a }
            java.lang.String r4 = "events"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            java.lang.String r10 = "event_id"
            r11 = 1000(0x3e8, float:1.401E-42)
            java.lang.String r11 = java.lang.Integer.toString(r11)     // Catch:{ SQLiteException -> 0x015f, all -> 0x015a }
            android.database.Cursor r20 = r3.query(r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ SQLiteException -> 0x015f, all -> 0x015a }
        L_0x0022:
            boolean r3 = r20.moveToNext()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            if (r3 == 0) goto L_0x0143
            com.a.a.a.a.j r3 = new com.a.a.a.a.j     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r4 = 0
            r0 = r20
            r1 = r4
            long r4 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r6 = 1
            r0 = r20
            r1 = r6
            int r6 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r7 = 2
            r0 = r20
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r8 = 3
            r0 = r20
            r1 = r8
            int r8 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r9 = 4
            r0 = r20
            r1 = r9
            int r9 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r10 = 5
            r0 = r20
            r1 = r10
            int r10 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r11 = 6
            r0 = r20
            r1 = r11
            int r11 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r12 = 7
            r0 = r20
            r1 = r12
            int r12 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r13 = 8
            r0 = r20
            r1 = r13
            java.lang.String r13 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r14 = 9
            r0 = r20
            r1 = r14
            java.lang.String r14 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r15 = 10
            r0 = r20
            r1 = r15
            java.lang.String r15 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r16 = 11
            r0 = r20
            r1 = r16
            int r16 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r17 = 12
            r0 = r20
            r1 = r17
            int r17 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r18 = 13
            r0 = r20
            r1 = r18
            int r18 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r3.<init>(r4, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.String r4 = "event_id"
            r0 = r20
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r0 = r20
            r1 = r4
            long r4 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.String r6 = "__##GOOGLETRANSACTION##__"
            java.lang.String r7 = r3.i     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            boolean r6 = r6.equals(r7)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            if (r6 == 0) goto L_0x0101
            r0 = r21
            r1 = r4
            com.a.a.a.a.f r6 = r0.b(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            if (r6 != 0) goto L_0x00e1
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r8.<init>()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.String r9 = "missing expected transaction for event "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            android.util.Log.w(r7, r4)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
        L_0x00e1:
            r3.a(r6)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
        L_0x00e4:
            r0 = r19
            r1 = r3
            r0.add(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            goto L_0x0022
        L_0x00ec:
            r3 = move-exception
            r4 = r20
        L_0x00ef:
            java.lang.String r5 = "GoogleAnalyticsTracker"
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x015d }
            android.util.Log.e(r5, r3)     // Catch:{ all -> 0x015d }
            r3 = 0
            com.a.a.a.a.j[] r3 = new com.a.a.a.a.j[r3]     // Catch:{ all -> 0x015d }
            if (r4 == 0) goto L_0x0100
            r4.close()
        L_0x0100:
            return r3
        L_0x0101:
            java.lang.String r6 = "__##GOOGLEITEM##__"
            java.lang.String r7 = r3.i     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            boolean r6 = r6.equals(r7)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            if (r6 == 0) goto L_0x0139
            r0 = r21
            r1 = r4
            com.a.a.a.a.i r6 = r0.c(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            if (r6 != 0) goto L_0x012c
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r8.<init>()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.String r9 = "missing expected item for event "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            android.util.Log.w(r7, r4)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
        L_0x012c:
            r3.a(r6)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            goto L_0x00e4
        L_0x0130:
            r3 = move-exception
            r4 = r20
        L_0x0133:
            if (r4 == 0) goto L_0x0138
            r4.close()
        L_0x0138:
            throw r3
        L_0x0139:
            r0 = r21
            r1 = r4
            com.a.a.a.a.g r4 = r0.d(r1)     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            r3.o = r4     // Catch:{ SQLiteException -> 0x00ec, all -> 0x0130 }
            goto L_0x00e4
        L_0x0143:
            if (r20 == 0) goto L_0x0148
            r20.close()
        L_0x0148:
            int r3 = r19.size()
            com.a.a.a.a.j[] r3 = new com.a.a.a.a.j[r3]
            r0 = r19
            r1 = r3
            java.lang.Object[] r21 = r0.toArray(r1)
            com.a.a.a.a.j[] r21 = (com.a.a.a.a.j[]) r21
            r3 = r21
            goto L_0x0100
        L_0x015a:
            r3 = move-exception
            r4 = r12
            goto L_0x0133
        L_0x015d:
            r3 = move-exception
            goto L_0x0133
        L_0x015f:
            r3 = move-exception
            r4 = r12
            goto L_0x00ef
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.h.l():com.a.a.a.a.j[]");
    }

    private g m() {
        g gVar = new g();
        try {
            Cursor query = this.a.getReadableDatabase().query("custom_var_cache", null, "cv_scope=1", null, null, null, null);
            while (query.moveToNext()) {
                gVar.a(new s(query.getInt(query.getColumnIndex("cv_index")), query.getString(query.getColumnIndex("cv_name")), query.getString(query.getColumnIndex("cv_value")), query.getInt(query.getColumnIndex("cv_scope"))));
            }
            query.close();
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
        return gVar;
    }

    public final void a(long j2) {
        String str = "event_id=" + j2;
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            if (writableDatabase.delete("events", str, null) != 0) {
                this.g--;
                writableDatabase.delete("custom_variables", str, null);
                writableDatabase.delete("transaction_events", str, null);
                writableDatabase.delete("item_events", str, null);
            }
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x01e1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        android.util.Log.e("GoogleAnalyticsTracker", r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x01ed, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x01ee, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x022b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x022c, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0231, code lost:
        r1.endTransaction();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x022b A[ExcHandler: all (r1v5 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:9:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0231  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.a.a.a.a.j r11) {
        /*
            r10 = this;
            r8 = 0
            int r0 = r10.g
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 < r1) goto L_0x000f
            java.lang.String r0 = "GoogleAnalyticsTracker"
            java.lang.String r1 = "Store full. Not storing last event."
            android.util.Log.w(r0, r1)
        L_0x000e:
            return
        L_0x000f:
            boolean r0 = r10.h
            if (r0 != 0) goto L_0x0055
            com.a.a.a.a.t r0 = r10.a     // Catch:{ SQLiteException -> 0x0152 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0152 }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x0152 }
            r1.<init>()     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.String r2 = "timestamp_previous"
            long r3 = r10.d     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0152 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.String r2 = "timestamp_current"
            long r3 = r10.e     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0152 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.String r2 = "visits"
            int r3 = r10.f     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0152 }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.String r2 = "session"
            java.lang.String r3 = "timestamp_first=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0152 }
            r5 = 0
            long r6 = r10.c     // Catch:{ SQLiteException -> 0x0152 }
            java.lang.String r6 = java.lang.Long.toString(r6)     // Catch:{ SQLiteException -> 0x0152 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0152 }
            r0.update(r2, r1, r3, r4)     // Catch:{ SQLiteException -> 0x0152 }
            r0 = 1
            r10.h = r0     // Catch:{ SQLiteException -> 0x0152 }
        L_0x0055:
            com.a.a.a.a.t r0 = r10.a     // Catch:{ SQLiteException -> 0x02cf, all -> 0x02c8 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x02cf, all -> 0x02c8 }
            r0.beginTransaction()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.<init>()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "user_id"
            int r3 = r11.b     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "account_id"
            java.lang.String r3 = r11.c     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "random_val"
            double r3 = java.lang.Math.random()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r5 = 4746794007244308480(0x41dfffffffc00000, double:2.147483647E9)
            double r3 = r3 * r5
            int r3 = (int) r3     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "timestamp_first"
            long r3 = r10.c     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "timestamp_previous"
            long r3 = r10.d     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "timestamp_current"
            long r3 = r10.e     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "visits"
            int r3 = r10.f     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "category"
            java.lang.String r3 = r11.i     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "action"
            java.lang.String r3 = r11.j     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "label"
            java.lang.String r3 = r11.k     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "value"
            int r3 = r11.l     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "screen_width"
            int r3 = r11.m     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "screen_height"
            int r3 = r11.n     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = "events"
            java.lang.String r3 = "event_id"
            long r1 = r0.insert(r2, r3, r1)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r3 = -1
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 == 0) goto L_0x02bf
            int r1 = r10.g     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            int r1 = r1 + 1
            r10.g = r1     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r1 = "events"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r3 = 0
            java.lang.String r4 = "event_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "event_id DESC"
            r8 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r2 = 0
            r1.moveToPosition(r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r2 = 0
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r1.close()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r1 = r11.i     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r4 = "__##GOOGLETRANSACTION##__"
            boolean r1 = r1.equals(r4)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            if (r1 == 0) goto L_0x0201
            com.a.a.a.a.f r1 = r11.a()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            if (r1 != 0) goto L_0x015e
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r4.<init>()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r5 = "missing transaction details for event "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            android.util.Log.w(r1, r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
        L_0x0148:
            r0.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
        L_0x014b:
            if (r0 == 0) goto L_0x000e
            r0.endTransaction()
            goto L_0x000e
        L_0x0152:
            r0 = move-exception
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x0055
        L_0x015e:
            com.a.a.a.a.t r4 = r10.a     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            android.database.sqlite.SQLiteDatabase r4 = r4.getWritableDatabase()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r5.<init>()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r6 = "event_id"
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r5.put(r6, r2)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r2 = "order_id"
            java.lang.String r3 = r1.a()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r2 = "tran_storename"
            java.lang.String r3 = r1.b()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r2 = "tran_totalcost"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r3.<init>()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            double r6 = r1.c()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r6 = ""
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r2 = "tran_totaltax"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r3.<init>()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            double r6 = r1.d()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r6 = ""
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r2 = "tran_shippingcost"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r3.<init>()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            double r6 = r1.e()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.StringBuilder r1 = r3.append(r6)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r3 = ""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            r5.put(r2, r1)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            java.lang.String r1 = "transaction_events"
            java.lang.String r2 = "event_id"
            r4.insert(r1, r2, r5)     // Catch:{ SQLiteException -> 0x01e1, all -> 0x022b }
            goto L_0x0148
        L_0x01e1:
            r1 = move-exception
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r1 = r1.toString()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            android.util.Log.e(r2, r1)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            goto L_0x0148
        L_0x01ed:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x01f1:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02cc }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x02cc }
            if (r1 == 0) goto L_0x000e
            r1.endTransaction()
            goto L_0x000e
        L_0x0201:
            java.lang.String r1 = r11.i     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r4 = "__##GOOGLEITEM##__"
            boolean r1 = r1.equals(r4)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            if (r1 == 0) goto L_0x02ba
            com.a.a.a.a.i r1 = r11.b()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            if (r1 != 0) goto L_0x0235
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            r4.<init>()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r5 = "missing item details for event "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            android.util.Log.w(r1, r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            goto L_0x0148
        L_0x022b:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x022f:
            if (r1 == 0) goto L_0x0234
            r1.endTransaction()
        L_0x0234:
            throw r0
        L_0x0235:
            com.a.a.a.a.t r4 = r10.a     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            android.database.sqlite.SQLiteDatabase r4 = r4.getWritableDatabase()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.<init>()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r6 = "event_id"
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.put(r6, r2)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r2 = "order_id"
            java.lang.String r3 = r1.a()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r2 = "item_sku"
            java.lang.String r3 = r1.b()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r2 = "item_name"
            java.lang.String r3 = r1.c()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r2 = "item_category"
            java.lang.String r3 = r1.d()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r2 = "item_price"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r3.<init>()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            double r6 = r1.e()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r6 = ""
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.put(r2, r3)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r2 = "item_count"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r3.<init>()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            long r6 = r1.f()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.StringBuilder r1 = r3.append(r6)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r3 = ""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            r5.put(r2, r1)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            java.lang.String r1 = "item_events"
            java.lang.String r2 = "event_id"
            r4.insert(r1, r2, r5)     // Catch:{ SQLiteException -> 0x02ae, all -> 0x022b }
            goto L_0x0148
        L_0x02ae:
            r1 = move-exception
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r1 = r1.toString()     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            android.util.Log.e(r2, r1)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            goto L_0x0148
        L_0x02ba:
            r10.a(r11, r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            goto L_0x0148
        L_0x02bf:
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.String r2 = "Error when attempting to add event to database."
            android.util.Log.e(r1, r2)     // Catch:{ SQLiteException -> 0x01ed, all -> 0x022b }
            goto L_0x014b
        L_0x02c8:
            r0 = move-exception
            r1 = r8
            goto L_0x022f
        L_0x02cc:
            r0 = move-exception
            goto L_0x022f
        L_0x02cf:
            r0 = move-exception
            r1 = r8
            goto L_0x01f1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.h.a(com.a.a.a.a.j):void");
    }

    public final j[] a() {
        return l();
    }

    public final int b() {
        try {
            if (this.j == null) {
                this.j = this.a.getReadableDatabase().compileStatement("SELECT COUNT(*) from events");
            }
            return (int) this.j.simpleQueryForLong();
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
            return 0;
        }
    }

    public final int c() {
        return this.b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d() {
        /*
            r9 = this;
            r1 = 1
            r0 = 0
            r8 = 0
            r9.h = r0
            r9.i = r1
            int r0 = r9.b()
            r9.g = r0
            com.a.a.a.a.t r0 = r9.a     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            java.lang.String r1 = "session"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00b5 }
            if (r2 != 0) goto L_0x008d
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x00b5 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            r9.c = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r9.d = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r9.e = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r2 = 1
            r9.f = r2     // Catch:{ SQLiteException -> 0x00b5 }
            java.security.SecureRandom r2 = new java.security.SecureRandom     // Catch:{ SQLiteException -> 0x00b5 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x00b5 }
            int r2 = r2.nextInt()     // Catch:{ SQLiteException -> 0x00b5 }
            r3 = 2147483647(0x7fffffff, float:NaN)
            r2 = r2 & r3
            r9.b = r2     // Catch:{ SQLiteException -> 0x00b5 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00b5 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_first"
            long r4 = r9.c     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_previous"
            long r4 = r9.d     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_current"
            long r4 = r9.e     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "visits"
            int r4 = r9.f     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "store_id"
            int r4 = r9.b     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "session"
            java.lang.String r4 = "timestamp_first"
            r0.insert(r3, r4, r2)     // Catch:{ SQLiteException -> 0x00b5 }
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()
        L_0x008c:
            return
        L_0x008d:
            r0 = 0
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.c = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 2
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.d = r2     // Catch:{ SQLiteException -> 0x00b5 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x00b5 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            r9.e = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 3
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            int r0 = r0 + 1
            r9.f = r0     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 4
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.b = r0     // Catch:{ SQLiteException -> 0x00b5 }
            goto L_0x0087
        L_0x00b5:
            r0 = move-exception
        L_0x00b6:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00cd }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00cd }
            if (r1 == 0) goto L_0x008c
            r1.close()
            goto L_0x008c
        L_0x00c5:
            r0 = move-exception
            r1 = r8
        L_0x00c7:
            if (r1 == 0) goto L_0x00cc
            r1.close()
        L_0x00cc:
            throw r0
        L_0x00cd:
            r0 = move-exception
            goto L_0x00c7
        L_0x00cf:
            r0 = move-exception
            r1 = r8
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.h.d():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String e() {
        /*
            r10 = this;
            r8 = 0
            com.a.a.a.a.t r0 = r10.a     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            java.lang.String r1 = "install_referrer"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            r3 = 0
            java.lang.String r4 = "referrer"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x004d, all -> 0x0046 }
            if (r1 == 0) goto L_0x0052
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x004d, all -> 0x0046 }
        L_0x0025:
            if (r0 == 0) goto L_0x002a
            r0.close()
        L_0x002a:
            r0 = r1
        L_0x002b:
            return r0
        L_0x002c:
            r0 = move-exception
            r1 = r8
        L_0x002e:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004b }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            r0 = r8
            goto L_0x002b
        L_0x003e:
            r0 = move-exception
            r1 = r8
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()
        L_0x0045:
            throw r0
        L_0x0046:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0040
        L_0x004b:
            r0 = move-exception
            goto L_0x0040
        L_0x004d:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x002e
        L_0x0052:
            r1 = r8
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.a.a.h.e():java.lang.String");
    }
}
