package com.a.a.a.a;

public final class f {
    private final String a;
    private final String b;
    private final double c;
    private final double d;
    private final double e;

    /* synthetic */ f(v vVar) {
        this(vVar, (byte) 0);
    }

    private f(v vVar, byte b2) {
        this.a = vVar.a;
        this.c = vVar.c;
        this.b = vVar.b;
        this.d = vVar.d;
        this.e = vVar.e;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final double c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final double d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final double e() {
        return this.e;
    }
}
