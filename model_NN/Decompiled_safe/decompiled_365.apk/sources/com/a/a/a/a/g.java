package com.a.a.a.a;

final class g {
    private s[] a = new s[5];

    private static void c(int i) {
        if (i <= 0 || i > 5) {
            throw new IllegalArgumentException("Index must be between 1 and 5 inclusive.");
        }
    }

    public final void a(s sVar) {
        c(sVar.d());
        this.a[sVar.d() - 1] = sVar;
    }

    public final boolean a(int i) {
        c(i);
        return this.a[i - 1] == null;
    }

    public final s[] a() {
        return (s[]) this.a.clone();
    }

    public final s b(int i) {
        c(i);
        return this.a[i - 1];
    }

    public final boolean b() {
        for (s sVar : this.a) {
            if (sVar != null) {
                return true;
            }
        }
        return false;
    }
}
