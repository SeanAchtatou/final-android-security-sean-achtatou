package com.a.a.a.a;

public final class c {
    /* access modifiers changed from: private */
    public final String a;
    /* access modifiers changed from: private */
    public final String b;
    /* access modifiers changed from: private */
    public final double c;
    /* access modifiers changed from: private */
    public final long d;
    /* access modifiers changed from: private */
    public String e = null;
    /* access modifiers changed from: private */
    public String f = null;

    public c(String str, String str2, double d2, long j) {
        if (str == null || str.trim().length() == 0) {
            throw new IllegalArgumentException("orderId must not be empty or null");
        } else if (str2 == null || str2.trim().length() == 0) {
            throw new IllegalArgumentException("itemSKU must not be empty or null");
        } else {
            this.a = str;
            this.b = str2;
            this.c = d2;
            this.d = j;
        }
    }

    public final c a(String str) {
        this.e = str;
        return this;
    }

    public final c b(String str) {
        this.f = str;
        return this;
    }
}
