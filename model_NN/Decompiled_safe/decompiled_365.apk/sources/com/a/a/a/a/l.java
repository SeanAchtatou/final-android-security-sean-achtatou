package com.a.a.a.a;

import java.util.Locale;

final class l {
    l() {
    }

    private static String a(j jVar) {
        StringBuilder sb = new StringBuilder();
        g gVar = jVar.o;
        if (gVar == null || !gVar.b()) {
            return "";
        }
        s[] a = gVar.a();
        a(a, sb, 8);
        a(a, sb, 9);
        a(a, sb, 11);
        return sb.toString();
    }

    public static String a(j jVar, String str) {
        String str2 = "";
        if (jVar.j != null) {
            str2 = jVar.j;
        }
        if (!str2.startsWith("/")) {
            str2 = "/" + str2;
        }
        String a = p.a(str2, "UTF-8");
        String a2 = a(jVar);
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(jVar.d);
        if (a2.length() > 0) {
            sb.append("&utme=").append(a2);
        }
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(jVar.m), Integer.valueOf(jVar.n)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmp=").append(a);
        sb.append("&utmac=").append(jVar.c);
        sb.append("&utmcc=").append(e(jVar, str));
        return sb.toString();
    }

    private static String a(String str) {
        return str.replace("'", "'0").replace(")", "'1").replace("*", "'2").replace("!", "'3");
    }

    private static void a(StringBuilder sb, String str, double d) {
        sb.append(str).append("=");
        double floor = Math.floor((d * 1000000.0d) + 0.5d) / 1000000.0d;
        if (floor != 0.0d) {
            sb.append(Double.toString(floor));
        }
    }

    private static void a(StringBuilder sb, String str, String str2) {
        sb.append(str).append("=");
        if (str2 != null && str2.trim().length() > 0) {
            sb.append(p.a(str2, "UTF-8"));
        }
    }

    private static void a(s[] sVarArr, StringBuilder sb, int i) {
        sb.append(i).append("(");
        boolean z = true;
        for (int i2 = 0; i2 < sVarArr.length; i2++) {
            if (sVarArr[i2] != null) {
                s sVar = sVarArr[i2];
                if (!z) {
                    sb.append("*");
                } else {
                    z = false;
                }
                sb.append(sVar.d()).append("!");
                switch (i) {
                    case 8:
                        sb.append(a(p.a(sVar.b(), "UTF-8")));
                        continue;
                    case 9:
                        sb.append(a(p.a(sVar.c(), "UTF-8")));
                        continue;
                    case 11:
                        sb.append(sVar.a());
                        continue;
                }
            }
        }
        sb.append(")");
    }

    public static String b(j jVar, String str) {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(String.format("5(%s*%s", p.a(jVar.i, "UTF-8"), p.a(jVar.j, "UTF-8")));
        if (jVar.k != null) {
            sb2.append("*").append(p.a(jVar.k, "UTF-8"));
        }
        sb2.append(")");
        if (jVar.l >= 0) {
            sb2.append(String.format("(%d)", Integer.valueOf(jVar.l)));
        }
        sb2.append(a(jVar));
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(jVar.d);
        sb.append("&utmt=event");
        sb.append("&utme=").append(sb2.toString());
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(jVar.m), Integer.valueOf(jVar.n)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmac=").append(jVar.c);
        sb.append("&utmcc=").append(e(jVar, str));
        return sb.toString();
    }

    public static String c(j jVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(jVar.d);
        sb.append("&utmt=tran");
        f a = jVar.a();
        if (a != null) {
            a(sb, "&utmtid", a.a());
            a(sb, "&utmtst", a.b());
            a(sb, "&utmtto", a.c());
            a(sb, "&utmttx", a.d());
            a(sb, "&utmtsp", a.e());
            a(sb, "&utmtci", "");
            a(sb, "&utmtrg", "");
            a(sb, "&utmtco", "");
        }
        sb.append("&utmac=").append(jVar.c);
        sb.append("&utmcc=").append(e(jVar, str));
        return sb.toString();
    }

    public static String d(j jVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(jVar.d);
        sb.append("&utmt=item");
        i b = jVar.b();
        if (b != null) {
            a(sb, "&utmtid", b.a());
            a(sb, "&utmipc", b.b());
            a(sb, "&utmipn", b.c());
            a(sb, "&utmiva", b.d());
            a(sb, "&utmipr", b.e());
            sb.append("&utmiqt=");
            if (b.f() != 0) {
                sb.append(b.f());
            }
        }
        sb.append("&utmac=").append(jVar.c);
        sb.append("&utmcc=").append(e(jVar, str));
        return sb.toString();
    }

    private static String e(j jVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("__utma=");
        sb.append("999").append(".");
        sb.append(jVar.b).append(".");
        sb.append(jVar.e).append(".");
        sb.append(jVar.f).append(".");
        sb.append(jVar.g).append(".");
        sb.append(jVar.h);
        if (str != null) {
            sb.append("+__utmz=");
            sb.append("999").append(".");
            sb.append(jVar.e).append(".");
            sb.append("1.1.");
            sb.append(str);
        }
        return p.a(sb.toString(), "UTF-8");
    }
}
