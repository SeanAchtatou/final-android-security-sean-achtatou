package com.a.a.a.a;

public final class v {
    /* access modifiers changed from: private */
    public final String a;
    /* access modifiers changed from: private */
    public String b = null;
    /* access modifiers changed from: private */
    public final double c;
    /* access modifiers changed from: private */
    public double d = 0.0d;
    /* access modifiers changed from: private */
    public double e = 0.0d;

    public v(String str, double d2) {
        if (str == null || str.trim().length() == 0) {
            throw new IllegalArgumentException("orderId must not be empty or null");
        }
        this.a = str;
        this.c = d2;
    }

    public final v a(double d2) {
        this.d = d2;
        return this;
    }

    public final v a(String str) {
        this.b = str;
        return this;
    }

    public final v b(double d2) {
        this.e = d2;
        return this;
    }
}
