package com.a.a.a.a;

import android.os.Build;
import java.util.Locale;
import org.apache.http.HttpHost;

final class a implements d {
    /* access modifiers changed from: private */
    public static final HttpHost a = new HttpHost("www.google-analytics.com", 80);
    private final String b;
    private n c;
    private boolean d;

    public a() {
        this("GoogleAnalytics", "1.2");
    }

    public a(String str, String str2) {
        this.d = false;
        Locale locale = Locale.getDefault();
        Object[] objArr = new Object[7];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = Build.VERSION.RELEASE;
        objArr[3] = locale.getLanguage() != null ? locale.getLanguage().toLowerCase() : "en";
        objArr[4] = locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
        objArr[5] = Build.MODEL;
        objArr[6] = Build.ID;
        this.b = String.format("%s/%s (Linux; U; Android %s; %s-%s; %s Build/%s)", objArr);
    }

    public final void a(w wVar, String str) {
        b();
        this.c = new n(wVar, str, this.b, this);
        this.c.start();
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final void a(j[] jVarArr) {
        if (this.c != null) {
            this.c.a(jVarArr);
        }
    }

    public final boolean a() {
        return this.d;
    }

    public final void b() {
        if (this.c != null && this.c.getLooper() != null) {
            this.c.getLooper().quit();
            this.c = null;
        }
    }
}
