package com.a.a.a.a;

import android.util.Log;
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpConnectionMetrics;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.params.BasicHttpParams;

final class r {
    q a;
    private DefaultHttpClientConnection b;
    private int c;
    private boolean d;
    private HttpHost e;
    private SocketFactory f;

    public r(HttpHost httpHost) {
        this(httpHost, new PlainSocketFactory());
    }

    private r(HttpHost httpHost, SocketFactory socketFactory) {
        this.b = new DefaultHttpClientConnection();
        this.d = true;
        this.e = httpHost;
        this.f = socketFactory;
    }

    private void c() {
        if (this.b != null && this.b.isOpen()) {
            try {
                this.b.close();
            } catch (IOException e2) {
            }
        }
    }

    public final void a() {
        this.b.flush();
        HttpConnectionMetrics metrics = this.b.getMetrics();
        while (metrics.getResponseCount() < metrics.getRequestCount()) {
            HttpResponse receiveResponseHeader = this.b.receiveResponseHeader();
            if (!receiveResponseHeader.getStatusLine().getProtocolVersion().greaterEquals(HttpVersion.HTTP_1_1)) {
                this.a.a();
                this.d = false;
            }
            Header[] headers = receiveResponseHeader.getHeaders("Connection");
            if (headers != null) {
                for (Header value : headers) {
                    if ("close".equalsIgnoreCase(value.getValue())) {
                        this.a.a();
                        this.d = false;
                    }
                }
            }
            this.c = receiveResponseHeader.getStatusLine().getStatusCode();
            if (this.c != 200) {
                this.a.a(this.c);
                c();
                return;
            }
            this.b.receiveResponseEntity(receiveResponseHeader);
            receiveResponseHeader.getEntity().consumeContent();
            this.a.b();
            if (e.a().e()) {
                Log.v("GoogleAnalyticsTracker", "HTTP Response Code: " + receiveResponseHeader.getStatusLine().getStatusCode());
            }
            if (!this.d) {
                c();
                return;
            }
        }
    }

    public final void a(HttpRequest httpRequest) {
        if (this.b == null || !this.b.isOpen()) {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            this.b.bind(this.f.connectSocket(this.f.createSocket(), this.e.getHostName(), this.e.getPort(), null, 0, basicHttpParams), basicHttpParams);
        }
        this.b.sendRequestHeader(httpRequest);
    }

    public final void b() {
        c();
    }
}
