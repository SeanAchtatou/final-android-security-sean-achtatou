package com.google.ads;

public final class R {

    public static final class attr {
    }

    public static final class id {

        /* renamed from: admob_adView_320_50_test_1 */
        public static final int help = 2130903041;

        /* renamed from: admob_adView_320_50_test_2 */
        public static final int main = 2130903042;

        /* renamed from: admob_adView_320_50_test_3 */
        public static final int message_of_the_day = 2130903043;

        /* renamed from: admob_adView_320_50_test_4 */
        public static final int score_dialog = 2130903044;

        /* renamed from: admob_layout_320_50_test */
        public static final int adlayout = 2130903040;
        /* added by JADX */
        public static final int adview = 2131427328;
        /* added by JADX */
        public static final int inneradview = 2131427329;
        /* added by JADX */
        public static final int ownAdView = 2131427330;
        /* added by JADX */
        public static final int helpTitle = 2131427331;
        /* added by JADX */
        public static final int helpText = 2131427332;
        /* added by JADX */
        public static final int btnHelpOk = 2131427333;
        /* added by JADX */
        public static final int btnHelpPrev = 2131427334;
        /* added by JADX */
        public static final int btnHelpNext = 2131427335;
        /* added by JADX */
        public static final int mainFrame = 2131427336;
        /* added by JADX */
        public static final int airAttack = 2131427337;
        /* added by JADX */
        public static final int adParentStub = 2131427338;
        /* added by JADX */
        public static final int adParentView = 2131427339;
        /* added by JADX */
        public static final int helpStub = 2131427340;
        /* added by JADX */

        /* renamed from: help  reason: collision with other field name */
        public static final int f0help = 2131427341;
        /* added by JADX */
        public static final int messageOfTheDay = 2131427342;
        /* added by JADX */
        public static final int highScoreEditLayout = 2131427343;
        /* added by JADX */
        public static final int aircrafts = 2131427344;
        /* added by JADX */
        public static final int waves = 2131427345;
        /* added by JADX */
        public static final int score = 2131427346;
        /* added by JADX */
        public static final int username_view = 2131427347;
        /* added by JADX */
        public static final int username_edit = 2131427348;
        /* added by JADX */
        public static final int market = 2131427349;
    }

    public static final class layout {

        /* renamed from: admob_320_50_test */
        public static final int adicon = 2130837504;
        /* added by JADX */
        public static final int adlayout = 2130903040;
        /* added by JADX */
        public static final int help = 2130903041;
        /* added by JADX */
        public static final int main = 2130903042;
        /* added by JADX */
        public static final int message_of_the_day = 2130903043;
        /* added by JADX */
        public static final int score_dialog = 2130903044;
        /* added by JADX */
        public static final int score_dialog_killed = 2130903045;
    }

    /* added by JADX */
    public static final class drawable {
        /* added by JADX */
        public static final int adicon = 2130837504;
        /* added by JADX */
        public static final int adicon_aircontrol = 2130837505;
        /* added by JADX */
        public static final int background = 2130837506;
        /* added by JADX */
        public static final int blackstar = 2130837507;
        /* added by JADX */
        public static final int bluestar = 2130837508;
        /* added by JADX */
        public static final int bomb = 2130837509;
        /* added by JADX */
        public static final int bomberplane = 2130837510;
        /* added by JADX */
        public static final int bottomless = 2130837511;
        /* added by JADX */
        public static final int bullet = 2130837512;
        /* added by JADX */
        public static final int bulletchopper = 2130837513;
        /* added by JADX */
        public static final int bush1 = 2130837514;
        /* added by JADX */
        public static final int bush2 = 2130837515;
        /* added by JADX */
        public static final int button_bottomless = 2130837516;
        /* added by JADX */
        public static final int button_controls = 2130837517;
        /* added by JADX */
        public static final int button_easy = 2130837518;
        /* added by JADX */
        public static final int button_exit = 2130837519;
        /* added by JADX */
        public static final int button_hard = 2130837520;
        /* added by JADX */
        public static final int button_market = 2130837521;
        /* added by JADX */
        public static final int button_next = 2130837522;
        /* added by JADX */
        public static final int button_next_controls = 2130837523;
        /* added by JADX */
        public static final int button_ok_controls = 2130837524;
        /* added by JADX */
        public static final int button_play = 2130837525;
        /* added by JADX */
        public static final int button_prev = 2130837526;
        /* added by JADX */
        public static final int button_prev_controls = 2130837527;
        /* added by JADX */
        public static final int button_terrain = 2130837528;
        /* added by JADX */
        public static final int classic = 2130837529;
        /* added by JADX */
        public static final int clusterbomb = 2130837530;
        /* added by JADX */
        public static final int clusterbomber = 2130837531;
        /* added by JADX */
        public static final int controls = 2130837532;
        /* added by JADX */
        public static final int crashingplane = 2130837533;
        /* added by JADX */
        public static final int cursor = 2130837534;
        /* added by JADX */
        public static final int dig = 2130837535;
        /* added by JADX */
        public static final int easy = 2130837536;
        /* added by JADX */
        public static final int enemypilot_basic = 2130837537;
        /* added by JADX */
        public static final int enemypilot_injured = 2130837538;
        /* added by JADX */
        public static final int exit = 2130837539;
        /* added by JADX */
        public static final int fire = 2130837540;
        /* added by JADX */
        public static final int fragment = 2130837541;
        /* added by JADX */
        public static final int gasbomb = 2130837542;
        /* added by JADX */
        public static final int greenstar = 2130837543;
        /* added by JADX */
        public static final int grenade = 2130837544;
        /* added by JADX */
        public static final int groundtexture = 2130837545;
        /* added by JADX */
        public static final int hard = 2130837546;
        /* added by JADX */
        public static final int house = 2130837547;
        /* added by JADX */
        public static final int icon = 2130837548;
        /* added by JADX */
        public static final int jump = 2130837549;
        /* added by JADX */
        public static final int market = 2130837550;
        /* added by JADX */
        public static final int move = 2130837551;
        /* added by JADX */
        public static final int musictoggle = 2130837552;
        /* added by JADX */
        public static final int napalmbomb = 2130837553;
        /* added by JADX */
        public static final int napalmbomber = 2130837554;
        /* added by JADX */
        public static final int next = 2130837555;
        /* added by JADX */
        public static final int next_control = 2130837556;
        /* added by JADX */
        public static final int next_controls = 2130837557;
        /* added by JADX */
        public static final int nuke = 2130837558;
        /* added by JADX */
        public static final int nuke_dud = 2130837559;
        /* added by JADX */
        public static final int ok = 2130837560;
        /* added by JADX */
        public static final int own_ad_image = 2130837561;
        /* added by JADX */
        public static final int parachute = 2130837562;
        /* added by JADX */
        public static final int paradropper = 2130837563;
        /* added by JADX */
        public static final int paragun = 2130837564;
        /* added by JADX */
        public static final int paragunner = 2130837565;
        /* added by JADX */
        public static final int paratrooper = 2130837566;
        /* added by JADX */
        public static final int particletoggle = 2130837567;
        /* added by JADX */
        public static final int play = 2130837568;
        /* added by JADX */
        public static final int prev = 2130837569;
        /* added by JADX */
        public static final int prev_control = 2130837570;
        /* added by JADX */
        public static final int prev_controls = 2130837571;
        /* added by JADX */
        public static final int redstar = 2130837572;
        /* added by JADX */
        public static final int rocket = 2130837573;
        /* added by JADX */
        public static final int rocketchopper = 2130837574;
        /* added by JADX */
        public static final int rocketlauncher = 2130837575;
        /* added by JADX */
        public static final int shovel = 2130837576;
        /* added by JADX */
        public static final int soldier1 = 2130837577;
        /* added by JADX */
        public static final int soldier2 = 2130837578;
        /* added by JADX */
        public static final int star_big_on = 2130837579;
        /* added by JADX */
        public static final int stump1 = 2130837580;
        /* added by JADX */
        public static final int stump2 = 2130837581;
        /* added by JADX */
        public static final int stump3 = 2130837582;
        /* added by JADX */
        public static final int supplycrate = 2130837583;
        /* added by JADX */
        public static final int supplyplane = 2130837584;
        /* added by JADX */
        public static final int terrainbutton = 2130837585;
        /* added by JADX */
        public static final int tree1 = 2130837586;
        /* added by JADX */
        public static final int tree2 = 2130837587;
        /* added by JADX */
        public static final int tree3 = 2130837588;
    }

    /* added by JADX */
    public static final class anim {
        /* added by JADX */
        public static final int hyperspace_in = 2130968576;
        /* added by JADX */
        public static final int hyperspace_out = 2130968577;
        /* added by JADX */
        public static final int push_left_in = 2130968578;
        /* added by JADX */
        public static final int push_left_out = 2130968579;
        /* added by JADX */
        public static final int push_up_in = 2130968580;
        /* added by JADX */
        public static final int push_up_out = 2130968581;
    }

    /* added by JADX */
    public static final class xml {
        /* added by JADX */
        public static final int preferences = 2131034112;
    }

    /* added by JADX */
    public static final class raw {
        /* added by JADX */
        public static final int bombexplosion = 2131099648;
        /* added by JADX */
        public static final int bullethitsground = 2131099649;
        /* added by JADX */
        public static final int clusterbombexplosion = 2131099650;
        /* added by JADX */
        public static final int clusterfragmentbounce = 2131099651;
        /* added by JADX */
        public static final int clusterfragmentexplode = 2131099652;
        /* added by JADX */
        public static final int crash = 2131099653;
        /* added by JADX */
        public static final int crashland = 2131099654;
        /* added by JADX */
        public static final int digging = 2131099655;
        /* added by JADX */
        public static final int dudbombticking = 2131099656;
        /* added by JADX */
        public static final int gameover = 2131099657;
        /* added by JADX */
        public static final int gasbombexplosion = 2131099658;
        /* added by JADX */
        public static final int gunfire = 2131099659;
        /* added by JADX */
        public static final int jump = 2131099660;
        /* added by JADX */
        public static final int mouseclick = 2131099661;
        /* added by JADX */
        public static final int napalmexplosion = 2131099662;
        /* added by JADX */
        public static final int nextwave = 2131099663;
        /* added by JADX */
        public static final int nuke_part1 = 2131099664;
        /* added by JADX */
        public static final int nuke_part2 = 2131099665;
        /* added by JADX */
        public static final int ooh = 2131099666;
        /* added by JADX */
        public static final int parachute = 2131099667;
        /* added by JADX */
        public static final int pilotcapture = 2131099668;
        /* added by JADX */
        public static final int rocketexplosion = 2131099669;
        /* added by JADX */
        public static final int rocketlaunch = 2131099670;
        /* added by JADX */
        public static final int splash = 2131099671;
        /* added by JADX */
        public static final int supplycratepickup = 2131099672;
        /* added by JADX */
        public static final int titlesong = 2131099673;
    }

    /* added by JADX */
    public static final class array {
        /* added by JADX */
        public static final int control_options_strings = 2131165184;
        /* added by JADX */
        public static final int control_options = 2131165185;
        /* added by JADX */
        public static final int accell_sensitivity_strings = 2131165186;
        /* added by JADX */
        public static final int accell_sensitivity = 2131165187;
        /* added by JADX */
        public static final int walk_shoot_cutoff_strings = 2131165188;
        /* added by JADX */
        public static final int walk_shoot_cutoff = 2131165189;
        /* added by JADX */
        public static final int particle_options_strings = 2131165190;
        /* added by JADX */
        public static final int particle_options = 2131165191;
        /* added by JADX */
        public static final int snowColors = 2131165192;
        /* added by JADX */
        public static final int grassColors = 2131165193;
        /* added by JADX */
        public static final int houseFoundationColors = 2131165194;
    }

    /* added by JADX */
    public static final class string {
        /* added by JADX */
        public static final int prefKeyControls = 2131230720;
        /* added by JADX */
        public static final int prefKeySound = 2131230721;
        /* added by JADX */
        public static final int prefKeyAccelerometerSensitivity = 2131230722;
        /* added by JADX */
        public static final int prefKeyWalkShootCutoff = 2131230723;
        /* added by JADX */
        public static final int prefKeyParticles = 2131230724;
        /* added by JADX */
        public static final int prefKeyNumberOfGamesPlayed = 2131230725;
        /* added by JADX */
        public static final int prefKeyRateGameCounter = 2131230726;
        /* added by JADX */
        public static final int prefKeyShownRateGameDialog = 2131230727;
        /* added by JADX */
        public static final int prefKeyShowHelpPagesDialogueHasBeenShown = 2131230728;
        /* added by JADX */
        public static final int soldierlives = 2131230729;
        /* added by JADX */
        public static final int app_name = 2131230730;
        /* added by JADX */
        public static final int freeTrialVersion = 2131230731;
        /* added by JADX */
        public static final int canReduceLevels = 2131230732;
        /* added by JADX */
        public static final int onlinePropertyFileName = 2131230733;
        /* added by JADX */
        public static final int versionType = 2131230734;
        /* added by JADX */
        public static final int version = 2131230735;
        /* added by JADX */
        public static final int onlinePropertyCache = 2131230736;
        /* added by JADX */
        public static final int button_ok = 2131230737;
        /* added by JADX */
        public static final int button_next = 2131230738;
        /* added by JADX */
        public static final int button_prev = 2131230739;
        /* added by JADX */
        public static final int button_go_to_market = 2131230740;
        /* added by JADX */
        public static final int button_cancel = 2131230741;
        /* added by JADX */
        public static final int button_gotomain = 2131230742;
        /* added by JADX */
        public static final int dialog_gotomain_title = 2131230743;
        /* added by JADX */
        public static final int dialog_gotomain_body = 2131230744;
        /* added by JADX */
        public static final int menu_exit = 2131230745;
        /* added by JADX */
        public static final int menu_gameover = 2131230746;
        /* added by JADX */
        public static final int menu_pause = 2131230747;
        /* added by JADX */
        public static final int menu_resume = 2131230748;
        /* added by JADX */
        public static final int menu_sound = 2131230749;
        /* added by JADX */
        public static final int menu_settings = 2131230750;
        /* added by JADX */
        public static final int menu_message = 2131230751;
        /* added by JADX */
        public static final int menu_help = 2131230752;
        /* added by JADX */
        public static final int message_paused = 2131230753;
        /* added by JADX */
        public static final int message_paused_sub = 2131230754;
        /* added by JADX */
        public static final int message_of_the_day_title = 2131230755;
        /* added by JADX */
        public static final int message_of_the_day_ok = 2131230756;
        /* added by JADX */
        public static final int buy_the_game_ok = 2131230757;
        /* added by JADX */
        public static final int buy_the_game_title = 2131230758;
        /* added by JADX */
        public static final int buy_the_game_message = 2131230759;
        /* added by JADX */
        public static final int view_help_pages_title = 2131230760;
        /* added by JADX */
        public static final int view_help_pages_message = 2131230761;
        /* added by JADX */
        public static final int view_help_pages_show = 2131230762;
        /* added by JADX */
        public static final int expired_pleaseupdate = 2131230763;
        /* added by JADX */
        public static final int expired_pleaseupdate_warning = 2131230764;
        /* added by JADX */
        public static final int expired_title = 2131230765;
        /* added by JADX */
        public static final int score_dialog_ok = 2131230766;
        /* added by JADX */
        public static final int score_dialog_market = 2131230767;
        /* added by JADX */
        public static final int score_dialog_text_title = 2131230768;
        /* added by JADX */
        public static final int score_dialog_text_title_congrat = 2131230769;
        /* added by JADX */
        public static final int score_dialog_entername = 2131230770;
        /* added by JADX */
        public static final int help_title_move = 2131230771;
        /* added by JADX */
        public static final int help_text_move = 2131230772;
        /* added by JADX */
        public static final int help_title_jump = 2131230773;
        /* added by JADX */
        public static final int help_text_jump = 2131230774;
        /* added by JADX */
        public static final int help_title_fire = 2131230775;
        /* added by JADX */
        public static final int help_text_fire = 2131230776;
        /* added by JADX */
        public static final int help_title_dig = 2131230777;
        /* added by JADX */
        public static final int help_text_dig = 2131230778;
        /* added by JADX */
        public static final int help_title_hints = 2131230779;
        /* added by JADX */
        public static final int help_text_hints = 2131230780;
        /* added by JADX */
        public static final int highscore_todays_greatest_at = 2131230781;
        /* added by JADX */
        public static final int highscore_recent_top_scores = 2131230782;
        /* added by JADX */
        public static final int highscore_all_time_topscores = 2131230783;
        /* added by JADX */
        public static final int highscore_todays_greatest_scores_dash = 2131230784;
        /* added by JADX */
        public static final int highscore_online_top_20_scores_dash = 2131230785;
        /* added by JADX */
        public static final int highscore_unable_to_access_online_highscore = 2131230786;
        /* added by JADX */
        public static final int highscore_local_highscores_dash = 2131230787;
        /* added by JADX */
        public static final int in_game_wave_prefix = 2131230788;
        /* added by JADX */
        public static final int in_game_wave = 2131230789;
        /* added by JADX */
        public static final int in_game_high_altitude_bomber_detected = 2131230790;
        /* added by JADX */
        public static final int in_game_prisoner_exchange = 2131230791;
        /* added by JADX */
        public static final int in_game_warhead_traded_on_black_market = 2131230792;
        /* added by JADX */
        public static final int in_game_disarmed_get_the_warhead = 2131230793;
        /* added by JADX */
        public static final int in_game_depleted_uranium_polution = 2131230794;
        /* added by JADX */
        public static final int in_game_wounded_pilot = 2131230795;
        /* added by JADX */
        public static final int in_game_paratrooper_beware = 2131230796;
        /* added by JADX */
        public static final int in_game_clean_up_fallout = 2131230797;
        /* added by JADX */
        public static final int in_game_taking_a_deep_breath = 2131230798;
        /* added by JADX */
        public static final int supply_crate_emp = 2131230799;
        /* added by JADX */
        public static final int supply_crate_extra_rocket = 2131230800;
        /* added by JADX */
        public static final int supply_crate_armor_piercing_rockets = 2131230801;
        /* added by JADX */
        public static final int supply_crate_multi_jump_boots = 2131230802;
        /* added by JADX */
        public static final int lost_one_rocket = 2131230803;
        /* added by JADX */
        public static final int lost_multi_jump_boots = 2131230804;
        /* added by JADX */
        public static final int lost_penetrating_rockets = 2131230805;
        /* added by JADX */
        public static final int welcome_to_air_attack = 2131230806;
        /* added by JADX */
        public static final int easy = 2131230807;
        /* added by JADX */
        public static final int hard = 2131230808;
        /* added by JADX */
        public static final int classic = 2131230809;
        /* added by JADX */
        public static final int ads = 2131230810;
        /* added by JADX */
        public static final int full = 2131230811;
        /* added by JADX */
        public static final int welcome_your_hill = 2131230812;
        /* added by JADX */
        public static final int welcome_defeat_is_certain = 2131230813;
        /* added by JADX */
        public static final int welcome_survive = 2131230814;
        /* added by JADX */
        public static final int loading = 2131230815;
        /* added by JADX */
        public static final int your_total_score_was = 2131230816;
        /* added by JADX */
        public static final int you_destroyed = 2131230817;
        /* added by JADX */
        public static final int enemy_aircrafts = 2131230818;
        /* added by JADX */
        public static final int you_defeated = 2131230819;
        /* added by JADX */
        public static final int waves = 2131230820;
        /* added by JADX */
        public static final int airattack_ad_text = 2131230821;
        /* added by JADX */
        public static final int market_url_to_purchase_airattack = 2131230822;
        /* added by JADX */
        public static final int aircontrol_ad_text1 = 2131230823;
        /* added by JADX */
        public static final int aircontrol_ad_text2 = 2131230824;
        /* added by JADX */
        public static final int aircontrol_ad_text3 = 2131230825;
        /* added by JADX */
        public static final int aircontrol_ad_text4 = 2131230826;
        /* added by JADX */
        public static final int aircontrol_ad_text5 = 2131230827;
        /* added by JADX */
        public static final int market_url_to_own_ad = 2131230828;
        /* added by JADX */
        public static final int errordialog_title = 2131230829;
        /* added by JADX */
        public static final int errordialog_out_of_memory = 2131230830;
        /* added by JADX */
        public static final int market_url_prefix = 2131230831;
        /* added by JADX */
        public static final int admob_publisherid = 2131230832;
        /* added by JADX */
        public static final int admob_publisherid_lsgv = 2131230833;
    }

    /* added by JADX */
    public static final class integer {
        /* added by JADX */
        public static final int crashOnHitProbability = 2131296256;
        /* added by JADX */
        public static final int pilotEjectProbability = 2131296257;
        /* added by JADX */
        public static final int hostilePilotProbability = 2131296258;
        /* added by JADX */
        public static final int versionCode = 2131296259;
    }

    /* added by JADX */
    public static final class style {
        /* added by JADX */
        public static final int GameWindowTheme = 2131361792;
        /* added by JADX */
        public static final int Text = 2131361793;
        /* added by JADX */
        public static final int HeadlineText1 = 2131361794;
        /* added by JADX */
        public static final int HeadlineText2 = 2131361795;
        /* added by JADX */
        public static final int LargeText = 2131361796;
        /* added by JADX */
        public static final int Button = 2131361797;
        /* added by JADX */
        public static final int Theme_NoBackground = 2131361798;
    }
}
