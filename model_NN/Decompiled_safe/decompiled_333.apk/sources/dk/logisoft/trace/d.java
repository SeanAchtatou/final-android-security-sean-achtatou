package dk.logisoft.trace;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import d.eg;
import java.io.File;

/* compiled from: ProGuard */
public final class d {
    private static String[] a = null;

    public static boolean a(Activity activity) {
        boolean z;
        Thread currentThread = Thread.currentThread();
        if (activity == null) {
            throw new IllegalArgumentException("context was null");
        }
        try {
            try {
                PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
                g.b = packageInfo.versionName + "(" + packageInfo.versionCode + ")";
                g.c = packageInfo.versionCode + "";
                g.f63d = packageInfo.packageName;
                File filesDir = activity.getFilesDir();
                if (filesDir != null) {
                    g.a = filesDir.getAbsolutePath();
                }
                g.e = Build.VERSION.RELEASE;
                g.h = eg.a();
                g.i = Build.MODEL;
                g.j = Build.PRODUCT;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (b().length > 0) {
                z = true;
            } else {
                z = false;
            }
            new e(activity, currentThread).start();
            return z;
        } catch (Exception e2) {
            Log.e("AirAttack", "Failure initializing exception handler", e2);
            return false;
        }
    }

    private static String[] b() {
        if (a != null) {
            return a;
        }
        File file = new File(g.a + "/");
        file.mkdir();
        String[] list = file.list(new f());
        a = list;
        return list;
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public static void a() {
        /*
            r15 = 0
            r14 = 0
            java.lang.String[] r0 = b()     // Catch:{ Exception -> 0x006c }
            if (r0 == 0) goto L_0x0113
            int r1 = r0.length     // Catch:{ Exception -> 0x006c }
            if (r1 <= 0) goto L_0x0113
            r1 = r15
        L_0x000c:
            int r2 = r0.length     // Catch:{ Exception -> 0x006c }
            if (r1 >= r2) goto L_0x0113
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006c }
            r2.<init>()     // Catch:{ Exception -> 0x006c }
            java.lang.String r3 = dk.logisoft.trace.g.a     // Catch:{ Exception -> 0x006c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x006c }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x006c }
            r3 = r0[r1]     // Catch:{ Exception -> 0x006c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x006c }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x006c }
            r3 = r0[r1]     // Catch:{ Exception -> 0x006c }
            java.lang.String r4 = "-"
            r3.split(r4)     // Catch:{ Exception -> 0x006c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006c }
            r3.<init>()     // Catch:{ Exception -> 0x006c }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ Exception -> 0x006c }
            java.io.FileReader r5 = new java.io.FileReader     // Catch:{ Exception -> 0x006c }
            r5.<init>(r2)     // Catch:{ Exception -> 0x006c }
            r4.<init>(r5)     // Catch:{ Exception -> 0x006c }
            r2 = r14
            r5 = r14
            r6 = r14
            r7 = r14
            r8 = r14
        L_0x0045:
            java.lang.String r9 = r4.readLine()     // Catch:{ Exception -> 0x006c }
            if (r9 == 0) goto L_0x00a2
            if (r8 != 0) goto L_0x004f
            r8 = r9
            goto L_0x0045
        L_0x004f:
            if (r7 != 0) goto L_0x0053
            r7 = r9
            goto L_0x0045
        L_0x0053:
            if (r5 != 0) goto L_0x0057
            r5 = r9
            goto L_0x0045
        L_0x0057:
            if (r6 != 0) goto L_0x005b
            r6 = r9
            goto L_0x0045
        L_0x005b:
            if (r2 != 0) goto L_0x005f
            r2 = r9
            goto L_0x0045
        L_0x005f:
            r3.append(r9)     // Catch:{ Exception -> 0x006c }
            java.lang.String r9 = "line.separator"
            java.lang.String r9 = java.lang.System.getProperty(r9)     // Catch:{ Exception -> 0x006c }
            r3.append(r9)     // Catch:{ Exception -> 0x006c }
            goto L_0x0045
        L_0x006c:
            r0 = move-exception
            java.lang.String r1 = "AirAttack"
            java.lang.String r2 = "Error occured handling exception"
            android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x014b }
            java.lang.String[] r0 = b()     // Catch:{ Exception -> 0x0146 }
            r1 = r15
        L_0x0079:
            int r2 = r0.length     // Catch:{ Exception -> 0x0146 }
            if (r1 >= r2) goto L_0x0145
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146 }
            r3.<init>()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r4 = dk.logisoft.trace.g.a     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0146 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0146 }
            r4 = r0[r1]     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0146 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0146 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0146 }
            r2.delete()     // Catch:{ Exception -> 0x0146 }
            int r1 = r1 + 1
            goto L_0x0079
        L_0x00a2:
            r4.close()     // Catch:{ Exception -> 0x006c }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x006c }
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x006c }
            r4.<init>()     // Catch:{ Exception -> 0x006c }
            org.apache.http.client.methods.HttpPost r9 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x006c }
            java.lang.String r10 = dk.logisoft.trace.g.f     // Catch:{ Exception -> 0x006c }
            r9.<init>(r10)     // Catch:{ Exception -> 0x006c }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ Exception -> 0x006c }
            r10.<init>()     // Catch:{ Exception -> 0x006c }
            org.apache.http.message.BasicNameValuePair r11 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x006c }
            java.lang.String r12 = "package_name"
            java.lang.String r13 = dk.logisoft.trace.g.f63d     // Catch:{ Exception -> 0x006c }
            r11.<init>(r12, r13)     // Catch:{ Exception -> 0x006c }
            r10.add(r11)     // Catch:{ Exception -> 0x006c }
            org.apache.http.message.BasicNameValuePair r11 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x006c }
            java.lang.String r12 = "package_version"
            r11.<init>(r12, r7)     // Catch:{ Exception -> 0x006c }
            r10.add(r11)     // Catch:{ Exception -> 0x006c }
            org.apache.http.message.BasicNameValuePair r7 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x006c }
            java.lang.String r11 = "phone_manuf"
            r7.<init>(r11, r5)     // Catch:{ Exception -> 0x006c }
            r10.add(r7)     // Catch:{ Exception -> 0x006c }
            org.apache.http.message.BasicNameValuePair r5 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x006c }
            java.lang.String r7 = "phone_model"
            r5.<init>(r7, r6)     // Catch:{ Exception -> 0x006c }
            r10.add(r5)     // Catch:{ Exception -> 0x006c }
            org.apache.http.message.BasicNameValuePair r5 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x006c }
            java.lang.String r6 = "phone_product"
            r5.<init>(r6, r2)     // Catch:{ Exception -> 0x006c }
            r10.add(r5)     // Catch:{ Exception -> 0x006c }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x006c }
            java.lang.String r5 = "android_version"
            r2.<init>(r5, r8)     // Catch:{ Exception -> 0x006c }
            r10.add(r2)     // Catch:{ Exception -> 0x006c }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x006c }
            java.lang.String r5 = "stacktrace"
            r2.<init>(r5, r3)     // Catch:{ Exception -> 0x006c }
            r10.add(r2)     // Catch:{ Exception -> 0x006c }
            org.apache.http.client.entity.UrlEncodedFormEntity r2 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x006c }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r10, r3)     // Catch:{ Exception -> 0x006c }
            r9.setEntity(r2)     // Catch:{ Exception -> 0x006c }
            r4.execute(r9)     // Catch:{ Exception -> 0x006c }
            int r1 = r1 + 1
            goto L_0x000c
        L_0x0113:
            java.lang.String[] r0 = b()     // Catch:{ Exception -> 0x0141 }
            r1 = r15
        L_0x0118:
            int r2 = r0.length     // Catch:{ Exception -> 0x0141 }
            if (r1 >= r2) goto L_0x0145
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0141 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0141 }
            r3.<init>()     // Catch:{ Exception -> 0x0141 }
            java.lang.String r4 = dk.logisoft.trace.g.a     // Catch:{ Exception -> 0x0141 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0141 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0141 }
            r4 = r0[r1]     // Catch:{ Exception -> 0x0141 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0141 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0141 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0141 }
            r2.delete()     // Catch:{ Exception -> 0x0141 }
            int r1 = r1 + 1
            goto L_0x0118
        L_0x0141:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0145:
            return
        L_0x0146:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0145
        L_0x014b:
            r0 = move-exception
            java.lang.String[] r1 = b()     // Catch:{ Exception -> 0x017a }
            r2 = r15
        L_0x0151:
            int r3 = r1.length     // Catch:{ Exception -> 0x017a }
            if (r2 >= r3) goto L_0x017e
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x017a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017a }
            r4.<init>()     // Catch:{ Exception -> 0x017a }
            java.lang.String r5 = dk.logisoft.trace.g.a     // Catch:{ Exception -> 0x017a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x017a }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x017a }
            r5 = r1[r2]     // Catch:{ Exception -> 0x017a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x017a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x017a }
            r3.<init>(r4)     // Catch:{ Exception -> 0x017a }
            r3.delete()     // Catch:{ Exception -> 0x017a }
            int r2 = r2 + 1
            goto L_0x0151
        L_0x017a:
            r1 = move-exception
            r1.printStackTrace()
        L_0x017e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: dk.logisoft.trace.d.a():void");
    }
}
