package dk.logisoft.trace;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

/* compiled from: ProGuard */
public class ReinstallActivity extends Activity {
    private boolean a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = getIntent().getExtras().getBoolean("FC");
        showDialog(0);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        String str;
        if (this.a) {
            str = "We have detected that the current install may be in a bad state. The game force closed. If it keeps happening, please uninstall the app, and then reinstall from the market.";
        } else {
            str = "We have detected that the current install may be in a bad state. If the game force closes, please uninstall the app, and then reinstall from the market.";
        }
        return new AlertDialog.Builder(this).setIcon(17301543).setTitle("Bad install detected").setMessage(str).setPositiveButton("Close", new i(this)).create();
    }
}
