package dk.logisoft.trace;

import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ProGuard */
final class f implements FilenameFilter {
    f() {
    }

    public final boolean accept(File file, String str) {
        return str.endsWith(".stacktrace");
    }
}
