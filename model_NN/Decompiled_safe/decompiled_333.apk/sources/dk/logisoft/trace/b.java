package dk.logisoft.trace;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.InflateException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.util.Random;

/* compiled from: ProGuard */
public class b implements Thread.UncaughtExceptionHandler {
    private static StringBuffer a;
    private static Throwable c;

    /* renamed from: d  reason: collision with root package name */
    private static String f61d;
    /* access modifiers changed from: private */
    public Thread.UncaughtExceptionHandler b;
    /* access modifiers changed from: private */
    public final Activity e;
    private final Thread f;

    static {
        StringBuffer stringBuffer = new StringBuffer(20000);
        a = stringBuffer;
        stringBuffer.append("0");
    }

    public b(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Activity activity, Thread thread) {
        this.b = uncaughtExceptionHandler;
        this.e = activity;
        this.f = thread;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        boolean z;
        boolean z2 = false;
        a = null;
        System.gc();
        boolean b2 = b(th);
        boolean z3 = (th instanceof a) || a(th);
        if (b2 || z3) {
            z = true;
        } else {
            z = false;
        }
        if (b2 || z3) {
            z2 = true;
        }
        try {
            a(th, z, z2);
        } catch (Exception e2) {
            Log.e("AirAttack", "Failed to handle exception: ", e2);
        }
        if (!thread.equals(this.f) && (b2 || z3)) {
            this.e.runOnUiThread(new c(this, b2, z3, thread));
        } else if (b2) {
            a(this.e);
        } else if (z3) {
            b(this.e);
        } else {
            this.b.uncaughtException(thread, th);
        }
    }

    private boolean a(Throwable th) {
        return ((th instanceof InflateException) && !b(th)) || (th.getCause() != null && a(th.getCause()));
    }

    private boolean b(Throwable th) {
        return (th instanceof OutOfMemoryError) || (th.getCause() != null && b(th.getCause()));
    }

    public static void a(Activity activity) {
        activity.startActivity(new Intent(activity.getBaseContext(), OutOfMemoryActivity.class));
        activity.finish();
        System.exit(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void b(Activity activity) {
        Intent intent = new Intent(activity.getBaseContext(), ReinstallActivity.class);
        intent.putExtra("FC", true);
        activity.startActivity(intent);
        activity.finish();
        System.exit(0);
    }

    private static String a(Throwable th, boolean z, boolean z2) {
        StringWriter stringWriter = new StringWriter(1024);
        PrintWriter printWriter = new PrintWriter(stringWriter);
        synchronized (b.class) {
            if (z) {
                if (z2) {
                    printWriter.append((CharSequence) "Caught exception - shown OutOfMemory or BadInstall dialog: \n");
                } else {
                    printWriter.append((CharSequence) "Caught exception:\n");
                }
            }
            if (c != null) {
                printWriter.append((CharSequence) "Caught and attempted to handle exception: \n");
                c.printStackTrace(printWriter);
                printWriter.append((CharSequence) "\n The following exception was thrown after the above exception: \n ");
                try {
                    new File(f61d).delete();
                } catch (Exception e2) {
                }
                a();
            }
        }
        th.printStackTrace(printWriter);
        String str = g.a + "/" + g.c + "-" + Integer.toString(new Random().nextInt(99999)) + ".stacktrace";
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(str), 8192);
            bufferedWriter.write(g.e + "\n");
            bufferedWriter.write(g.b + "\n");
            bufferedWriter.write(g.h + "\n");
            bufferedWriter.write(g.i + "\n");
            bufferedWriter.write(g.j + "\n");
            bufferedWriter.write(g.k + "\n");
            bufferedWriter.write(stringWriter.toString());
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e3) {
            e3.printStackTrace();
            Log.e("AirAttack", stringWriter.toString());
        }
        return str;
    }

    private static synchronized void a() {
        synchronized (b.class) {
            c = null;
            f61d = null;
        }
    }
}
