package dk.logisoft.trace;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import com.google.ads.R;

/* compiled from: ProGuard */
public class OutOfMemoryActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        showDialog(0);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.errordialog_title);
        builder.setMessage((int) R.string.errordialog_out_of_memory);
        builder.setIcon(17301543);
        builder.setNeutralButton((int) R.string.button_ok, new h(this));
        return builder.create();
    }
}
