package dk.logisoft.trace;

import android.content.DialogInterface;

/* compiled from: ProGuard */
final class i implements DialogInterface.OnClickListener {
    final /* synthetic */ ReinstallActivity a;

    i(ReinstallActivity reinstallActivity) {
        this.a = reinstallActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        this.a.finish();
    }
}
