package dk.logisoft.trace;

import android.app.Activity;
import java.lang.Thread;

/* compiled from: ProGuard */
final class e extends Thread {
    final /* synthetic */ Activity a;
    final /* synthetic */ Thread b;

    e(Activity activity, Thread thread) {
        this.a = activity;
        this.b = thread;
    }

    public final void run() {
        d.a();
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (!(defaultUncaughtExceptionHandler instanceof b)) {
            Thread.setDefaultUncaughtExceptionHandler(new b(defaultUncaughtExceptionHandler, this.a, this.b));
        }
    }
}
