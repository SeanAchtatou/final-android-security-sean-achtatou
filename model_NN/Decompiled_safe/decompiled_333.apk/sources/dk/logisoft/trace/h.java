package dk.logisoft.trace;

import android.content.DialogInterface;

/* compiled from: ProGuard */
final class h implements DialogInterface.OnClickListener {
    final /* synthetic */ OutOfMemoryActivity a;

    h(OutOfMemoryActivity outOfMemoryActivity) {
        this.a = outOfMemoryActivity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        this.a.finish();
    }
}
