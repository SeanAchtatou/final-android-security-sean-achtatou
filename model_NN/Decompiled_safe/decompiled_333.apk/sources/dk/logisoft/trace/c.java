package dk.logisoft.trace;

/* compiled from: ProGuard */
final class c implements Runnable {
    final /* synthetic */ boolean a;
    final /* synthetic */ boolean b;
    final /* synthetic */ Thread c;

    /* renamed from: d  reason: collision with root package name */
    final /* synthetic */ b f62d;

    c(b bVar, boolean z, boolean z2, Thread thread) {
        this.f62d = bVar;
        this.a = z;
        this.b = z2;
        this.c = thread;
    }

    public final void run() {
        try {
            if (this.a) {
                b.a(this.f62d.e);
            } else if (this.b) {
                b.b(this.f62d.e);
            }
        } catch (Throwable th) {
            this.f62d.b.uncaughtException(this.c, th);
        }
    }
}
