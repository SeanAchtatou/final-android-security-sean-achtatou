package dk.logisoft.airattack.ads;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

/* compiled from: ProGuard */
public final class n implements AdListener {
    private final e a;
    private final AdView b;

    public n(e eVar, AdView adView) {
        this.a = eVar;
        this.b = adView;
    }

    public final void onReceiveAd(Ad ad) {
        e eVar = this.a;
        Message obtainMessage = eVar.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putBoolean("admobSuccess", true);
        obtainMessage.setData(bundle);
        eVar.sendMessage(obtainMessage);
        a(this.b);
    }

    public final void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
        e eVar = this.a;
        Message obtainMessage = eVar.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putBoolean("admobFailed", true);
        obtainMessage.setData(bundle);
        eVar.sendMessage(obtainMessage);
    }

    public final void onPresentScreen(Ad ad) {
        a(this.b);
    }

    public final void onDismissScreen(Ad ad) {
    }

    public final void onLeaveApplication(Ad ad) {
    }

    private void a(ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof ViewGroup) {
                a((ViewGroup) childAt);
            } else {
                childAt.setFocusable(false);
            }
        }
        viewGroup.setFocusable(false);
    }
}
