package dk.logisoft.airattack.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.R;
import d.b;
import d.bz;
import d.cb;
import d.eg;
import d.h;
import d.w;
import dk.logisoft.airattack.c;

/* compiled from: ProGuard */
public final class e extends Handler implements d {
    /* access modifiers changed from: private */
    public AdView a;
    private OwnAdView b;
    private c c = new c();

    /* renamed from: d  reason: collision with root package name */
    private volatile boolean f59d = false;
    private b e = null;
    private a f;
    private ViewGroup g;
    private long h;
    private boolean i = false;
    private boolean j = false;
    private boolean k;
    /* access modifiers changed from: private */
    public AdRequest l;
    private final RelativeLayout.LayoutParams m;
    private final RelativeLayout.LayoutParams n;
    private boolean o;
    private ViewGroup p;
    /* access modifiers changed from: private */
    public View q;
    /* access modifiers changed from: private */
    public volatile int r;

    public e(Activity activity, ViewStub viewStub) {
        View inflate = viewStub.inflate();
        this.f = a.a();
        this.g = (ViewGroup) inflate;
        this.p = (ViewGroup) inflate.findViewById(R.id.inneradview);
        this.q = this.p;
        ViewGroup viewGroup = this.p;
        this.a = new AdView(activity, AdSize.BANNER, eg.c.nextFloat() < this.f.b() ? cb.a().a((int) R.string.admob_publisherid_lsgv) : cb.a().a((int) R.string.admob_publisherid));
        viewGroup.addView(this.a);
        this.a.setAdListener(new n(this, this.a));
        this.a.setVisibility(8);
        this.l = new AdRequest();
        this.l.setLocation(w.a().c());
        this.a.loadAd(this.l);
        this.a.setOnClickListener(new f(this));
        this.b = (OwnAdView) inflate.findViewById(R.id.ownAdView);
        this.m = new RelativeLayout.LayoutParams(-2, -2);
        this.m.addRule(10);
        this.m.addRule(14);
        this.n = (RelativeLayout.LayoutParams) this.p.getLayoutParams();
        h.a = this;
    }

    public final int b() {
        return Math.max((int) (48.0f * bz.c), this.a != null ? this.a.getHeight() : 0);
    }

    public final void a(boolean z) {
        if (b(z)) {
            this.o = false;
            sendMessage(b(z, false));
        }
    }

    public final void a(boolean z, boolean z2) {
        if (b(z)) {
            this.o = z && z2;
            handleMessage(b(z, z2));
        }
    }

    private Message b(boolean z, boolean z2) {
        Message obtainMessage = obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putBoolean("visibleAdView", z);
        bundle.putBoolean("adLoc", z2);
        obtainMessage.setData(bundle);
        return obtainMessage;
    }

    public final void a() {
        Message obtainMessage = obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putBoolean("nextAd", true);
        obtainMessage.setData(bundle);
        sendMessage(obtainMessage);
    }

    private boolean b(boolean z) {
        return b.a || (!z && this.f59d);
    }

    public final void handleMessage(Message message) {
        try {
            Bundle data = message.getData();
            if (data.getBoolean("admobFailed")) {
                if (!this.j) {
                    c cVar = this.c;
                    cVar.a = false;
                    cVar.b = true;
                    this.a.setVisibility(8);
                    if (this.f59d && this.e == b.ADMOB) {
                        this.e = null;
                        c(this.k);
                    }
                }
            } else if (data.getBoolean("admobSuccess")) {
                this.j = true;
                if (this.c.c() && this.e != b.ADMOB) {
                    this.i = true;
                }
                this.h = System.currentTimeMillis();
                this.c.a();
            } else if (data.getBoolean("admobSameAd")) {
                this.c.b();
            } else if (data.getBoolean("nextAd")) {
                g();
            } else {
                this.q.clearAnimation();
                boolean z = data.getBoolean("visibleAdView");
                if (z) {
                    c(data.getBoolean("adLoc"));
                } else {
                    h();
                }
                this.f59d = z;
            }
        } catch (RuntimeException e2) {
            Log.e("AirAttack", "Runtime exception occured in add-handling", e2);
        }
    }

    private void g() {
        if (b.a && this.e != null) {
            switch (l.a[this.e.ordinal()]) {
                case 1:
                    if (System.currentTimeMillis() - this.h > ((long) c.a().a("timeout", 20000))) {
                        i();
                        return;
                    }
                    return;
                case 2:
                    return;
                default:
                    throw new IllegalArgumentException("" + this.e);
            }
        }
    }

    private void c(boolean z) {
        b a2 = this.f.a(this.c);
        if (this.f59d) {
            if (z != this.k) {
                this.p.setLayoutParams(z ? this.m : this.n);
                this.k = z;
            }
            if (a2 == this.e) {
                g();
                return;
            }
            h();
        }
        this.k = z;
        this.p.setLayoutParams(z ? this.m : this.n);
        switch (l.a[a2.ordinal()]) {
            case 1:
                this.a.setVisibility(0);
                if (!this.i || this.h + 600000 <= System.currentTimeMillis()) {
                    i();
                }
                this.i = false;
                this.g.setMinimumHeight(((int) (48.0f * bz.c)) + 0);
                this.g.setMinimumWidth((int) (320.0f * bz.c));
                break;
            case 2:
                this.b.setVisibility(0);
                this.g.setMinimumHeight(this.b.a() + 0);
                this.g.setMinimumWidth(bz.a);
                break;
            default:
                throw new IllegalArgumentException("" + a2);
        }
        this.e = a2;
        this.g.setVisibility(0);
    }

    private void h() {
        this.g.setVisibility(8);
        this.b.setVisibility(8);
        if (this.a != null) {
            if (this.a.getVisibility() == 0) {
                this.a.setVisibility(8);
            } else if (this.c.c() || this.c.d()) {
                this.a.setVisibility(0);
                i();
                this.a.setVisibility(8);
            }
        }
        this.e = null;
    }

    private void i() {
        this.a.loadAd(this.l);
        this.c.b();
    }

    public final int c() {
        if (this.o) {
            return b();
        }
        return 0;
    }

    public final void f() {
        m mVar = new m(this, 1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
        mVar.setDuration(1500);
        mVar.setAnimationListener(new g(this, null));
        this.q.startAnimation(mVar);
    }

    public final void a(Runnable runnable) {
        m mVar = new m(this, 1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        mVar.setDuration(1500);
        mVar.setAnimationListener(new h(this, runnable));
        this.q.startAnimation(mVar);
    }

    public final void e() {
        this.q.post(new i(this));
    }

    public final void d() {
        this.q.post(new j(this));
    }
}
