package dk.logisoft.airattack.ads;

import android.view.animation.Animation;

/* compiled from: ProGuard */
final class h implements Animation.AnimationListener {
    final /* synthetic */ Runnable a;
    final /* synthetic */ e b;

    h(e eVar, Runnable runnable) {
        this.b = eVar;
        this.a = runnable;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: dk.logisoft.airattack.ads.e.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      dk.logisoft.airattack.ads.e.a(dk.logisoft.airattack.ads.e, int):int
      dk.logisoft.airattack.ads.e.a(boolean, boolean):void */
    public final void onAnimationEnd(Animation animation) {
        this.b.q.setAnimation(null);
        this.b.a(false, false);
        if (this.a != null) {
            this.a.run();
        }
    }
}
