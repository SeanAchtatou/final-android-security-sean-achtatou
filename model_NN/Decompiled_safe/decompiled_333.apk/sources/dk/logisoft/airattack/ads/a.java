package dk.logisoft.airattack.ads;

import android.util.Log;
import d.n;
import java.util.Properties;
import java.util.Random;

/* compiled from: ProGuard */
public final class a {
    private static a a;
    private int b;
    private int c;

    /* renamed from: d  reason: collision with root package name */
    private int f57d;
    private int e;
    private int f;
    private int g;
    private int h;
    private boolean i;
    private Random j;
    private float k;

    a() {
        this.b = 3;
        this.c = 0;
        this.f57d = 0;
        this.e = 1;
        this.f = 4;
        this.g = 1;
        this.h = 0;
        this.i = true;
        this.k = 1.0f;
        this.j = n.b;
    }

    a(Random random) {
        this.b = 3;
        this.c = 0;
        this.f57d = 0;
        this.e = 1;
        this.f = 4;
        this.g = 1;
        this.h = 0;
        this.i = true;
        this.k = 1.0f;
        this.j = random;
    }

    public static a a() {
        if (a == null) {
            a = new a();
        }
        return a;
    }

    public final float b() {
        return this.k;
    }

    public final void a(Properties properties) {
        this.k = ((float) a(properties, "lsgvperc", 100)) / 100.0f;
        this.f57d = a(properties, "admobSamePenaltyCount", this.f57d);
        this.e = a(properties, "admobFailPenaltyCount", this.e);
        if (this.e <= 0) {
            Log.e("AirAttack", "admobFailPenaltyCount was " + this.e + ", it must be positive");
            this.e = 1;
        }
        int a2 = a(properties, "admob", this.b);
        int a3 = a(properties, "own", this.c);
        if (this.b + this.c > 0) {
            this.b = a2;
            this.c = a3;
        } else {
            Log.e("AirAttack", "Invalid value set, the sum of the values must be at least 1, the values were: " + a2 + ", " + a3);
        }
        this.g = a(properties, "ownOtherFailed", this.g);
        this.f = a(properties, "adsBeforeOwn", this.f);
    }

    private static int a(Properties properties, String str, int i2) {
        String property = properties.getProperty(str);
        try {
            return Integer.parseInt(property);
        } catch (NumberFormatException e2) {
            Log.e("AirAttack", "Invalid value for property: " + str + ", value was:" + property);
            return i2;
        }
    }

    public final b a(c cVar) {
        int i2;
        int i3;
        cVar.a(this.e, this.f57d);
        if (cVar.c()) {
            i3 = this.g;
            i2 = 0;
        } else {
            int i4 = this.b;
            if (this.i) {
                i2 = i4;
                i3 = 0;
            } else {
                i2 = i4;
                i3 = this.c;
            }
        }
        if (cVar.a) {
            cVar.f58d++;
        }
        if (cVar.b) {
            cVar.c++;
        }
        if (this.i) {
            this.i = false;
        }
        int i5 = this.h;
        this.h = i5 + 1;
        if (i5 < this.f && i2 > 0) {
            i3 = 0;
        }
        if (i2 + i3 == 0) {
            i3 = 1;
        }
        if (this.j.nextInt(i3 + i2) < i2) {
            return b.ADMOB;
        }
        return b.OWN;
    }
}
