package dk.logisoft.airattack.ads;

import android.view.animation.Animation;

/* compiled from: ProGuard */
final class g implements Animation.AnimationListener {
    final /* synthetic */ Runnable a;
    final /* synthetic */ e b;

    g(e eVar, Runnable runnable) {
        this.b = eVar;
        this.a = runnable;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        this.b.q.setAnimation(null);
        if (this.a != null) {
            this.a.run();
        }
    }
}
