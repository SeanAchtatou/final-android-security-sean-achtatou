package dk.logisoft.airattack.ads;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.R;
import d.cb;
import d.n;

/* compiled from: ProGuard */
public class OwnAdView extends RelativeLayout {
    private static final int[] b = n.a(new int[]{R.string.aircontrol_ad_text1, R.string.aircontrol_ad_text2, R.string.aircontrol_ad_text3, R.string.aircontrol_ad_text4, R.string.aircontrol_ad_text5});
    private static final Typeface e = Typeface.create(Typeface.SANS_SERIF, 1);
    public final int a;
    private String c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f56d;
    private final Context f;

    public OwnAdView(Context context) {
        this(context, null, 0);
    }

    public OwnAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public OwnAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = (int) (context.getResources().getDisplayMetrics().density * 48.0f);
        int i2 = this.a;
        setFocusable(true);
        setClickable(true);
        context.getResources();
        Bitmap a2 = cb.a().a(new BitmapFactory.Options());
        int height = (i2 - a2.getHeight()) / 2;
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(a2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.getWidth(), a2.getHeight());
        layoutParams.setMargins(height, height, height, height);
        imageView.setLayoutParams(layoutParams);
        imageView.setId(18989);
        imageView.setVisibility(0);
        addView(imageView);
        TextView textView = new TextView(context);
        String a3 = cb.a().a(b[n.a(b.length)]);
        this.c = cb.a().a((int) R.string.market_url_to_own_ad);
        textView.setText(a3);
        textView.setTypeface(e);
        textView.setTextColor(-1);
        textView.setTextSize(13.0f);
        textView.setId(28989);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        if (a2 != null) {
            layoutParams2.addRule(1, 18989);
        }
        layoutParams2.setMargins(0, height, 0, height);
        layoutParams2.addRule(11);
        layoutParams2.addRule(10);
        textView.setLayoutParams(layoutParams2);
        addView(textView);
        textView.setVisibility(0);
        this.f = context;
        this.f56d = false;
        setFocusable(true);
        setClickable(true);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth(), this.a);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.f56d = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.f56d = false;
        super.onDetachedFromWindow();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f56d) {
            return false;
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.c));
        intent.addFlags(268435456);
        try {
            this.f.startActivity(intent);
        } catch (Exception e2) {
            Log.e("AirAttack", "Could not open browser on ad click to " + this.c, e2);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (!(i == 0 || i2 == 0)) {
            Rect rect = new Rect(0, 0, i, i2);
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            paint.setColor(-1);
            paint.setAntiAlias(true);
            canvas.drawRect(rect, paint);
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(-16777216), Color.green(-16777216), Color.blue(-16777216)), -16777216});
            int height = ((int) (((double) rect.height()) * 0.4375d)) + rect.top;
            gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
            gradientDrawable.draw(canvas);
            Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
            Paint paint2 = new Paint();
            paint2.setColor(-16777216);
            canvas.drawRect(rect2, paint2);
            setBackgroundDrawable(new BitmapDrawable(createBitmap));
        }
        System.gc();
    }

    public final int a() {
        return this.a;
    }
}
