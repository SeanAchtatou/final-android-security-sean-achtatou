package dk.logisoft.airattack.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;

/* compiled from: ProGuard */
final class m extends TranslateAnimation {
    final /* synthetic */ e a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(e eVar, Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = eVar;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(e eVar, float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
        this.a = eVar;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(e eVar, int i, float f, int i2, float f2, int i3, float f3, int i4, float f4) {
        super(i, f, i2, f2, i3, f3, i4, f4);
        this.a = eVar;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        float[] fArr = new float[9];
        transformation.getMatrix().getValues(fArr);
        int unused = this.a.r = (int) (-fArr[5]);
    }
}
