package dk.logisoft.airattack.ads;

/* compiled from: ProGuard */
public final class c {
    boolean a;
    boolean b;
    int c;

    /* renamed from: d  reason: collision with root package name */
    int f58d;
    private int e;
    private int f;

    public final void a() {
        this.a = false;
        this.b = false;
        this.c = 0;
        this.f58d = 0;
    }

    public final void b() {
        this.a = true;
        this.b = false;
    }

    public final boolean c() {
        if (this.c >= this.e) {
            a();
        }
        return this.b;
    }

    public final boolean d() {
        if (this.f58d >= this.f) {
            a();
        }
        return this.a;
    }

    public final void a(int i, int i2) {
        this.e = i;
        this.f = i2;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (this.b) {
            sb.append("failed");
        }
        if (this.a) {
            sb.append(" same ");
        }
        if (!this.b && !this.a) {
            sb.append("success");
        }
        if (this.c > 0) {
            sb.append(", failCounter=" + this.c);
        }
        if (this.f58d > 0) {
            sb.append(", sameCounter=" + this.f58d);
        }
        sb.append("]");
        return sb.toString();
    }
}
