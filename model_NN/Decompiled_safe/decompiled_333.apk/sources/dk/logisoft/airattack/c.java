package dk.logisoft.airattack;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import com.google.ads.R;
import d.b;
import d.cb;
import d.j;
import dk.logisoft.airattack.ads.a;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;

/* compiled from: ProGuard */
public final class c {
    private static volatile c a;
    private Properties b = a(j.c());
    private int c = -1;

    /* renamed from: d  reason: collision with root package name */
    private int f60d = -1;
    private String e;
    private int f = -1;
    private boolean g = false;
    private int h = -1;
    private int i = Integer.MAX_VALUE;
    private boolean j = false;
    private boolean k;
    private d l;
    private long m;

    private c(Context context) {
        if (this.b.isEmpty()) {
            this.b = a(context.getResources().getString(R.string.onlinePropertyCache));
        }
        a(this.b);
        a(true);
    }

    public static void a(Context context) {
        if (a == null) {
            a = new c(context);
        } else {
            a.a(false);
        }
    }

    private void a(boolean z) {
        if (z || SystemClock.uptimeMillis() - this.m > 60000) {
            c(false);
            b(false);
            this.l = new d(this);
            this.l.execute("http://games.martineriksen.net/airattack/php/" + cb.a().m);
            this.m = SystemClock.uptimeMillis();
        }
    }

    public static c a() {
        return a;
    }

    private static Properties a(String str) {
        Properties properties = new Properties();
        try {
            properties.load(new ByteArrayInputStream(str.getBytes()));
            return properties;
        } catch (IOException e2) {
            Log.e("AirAttack", "failed loading cached properties", e2);
            return new Properties();
        }
    }

    public final synchronized boolean b() {
        return this.j;
    }

    public final synchronized String c() {
        return this.e;
    }

    public final synchronized int d() {
        return this.f;
    }

    public final synchronized boolean e() {
        return this.g;
    }

    public final synchronized int f() {
        return this.c;
    }

    public final synchronized int g() {
        return this.f60d;
    }

    /* access modifiers changed from: private */
    public synchronized void b(boolean z) {
        this.k = z;
    }

    /* access modifiers changed from: private */
    public synchronized void c(boolean z) {
        this.j = z;
    }

    public final synchronized int a(String str, int i2) {
        int i3;
        String property = this.b.getProperty(str);
        if (property == null || "".equals(property)) {
            i3 = i2;
        } else {
            try {
                i3 = Integer.parseInt(property.trim());
            } catch (NumberFormatException e2) {
                i3 = i2;
            }
        }
        return i3;
    }

    /* access modifiers changed from: private */
    public void a(Properties properties) {
        a.a().a(properties);
        synchronized (this) {
            this.b = properties;
            String str = b.a ? ".trial" : ".full";
            this.e = b(properties.getProperty("message" + str));
            this.f = a(properties, "messageNumber" + str, -1);
            this.g = Boolean.parseBoolean(b(properties.getProperty("messageGoToMarket" + str)));
            this.h = a(properties, "messageMinVersionCode" + str, -1);
            this.i = a(properties, "messageMaxVersionCode" + str, Integer.MAX_VALUE);
            if (this.e == null || "".equals(this.e) || this.h > cb.a().k || this.i < cb.a().k) {
                this.f = -1;
                this.e = "";
            }
            this.c = a(properties, "expiredVersionCode", -1);
            this.f60d = a(properties, "expiredVersionCodeWarning", -1);
        }
    }

    private static int a(Properties properties, String str, int i2) {
        String property = properties.getProperty(str);
        if (property == null || "".equals(property)) {
            return i2;
        }
        try {
            return Integer.parseInt(property.trim());
        } catch (NumberFormatException e2) {
            return i2;
        }
    }

    private static String b(String str) {
        if (str != null) {
            return str.trim();
        }
        return null;
    }
}
