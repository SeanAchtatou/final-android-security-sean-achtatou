package dk.logisoft.airattack;

import android.os.AsyncTask;
import android.util.Log;
import d.j;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/* compiled from: ProGuard */
final class d extends AsyncTask {
    final /* synthetic */ c a;

    d(c cVar) {
        this.a = cVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a((String[]) objArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: dk.logisoft.airattack.c.a(dk.logisoft.airattack.c, boolean):void
     arg types: [dk.logisoft.airattack.c, int]
     candidates:
      dk.logisoft.airattack.c.a(dk.logisoft.airattack.c, java.util.Properties):void
      dk.logisoft.airattack.c.a(java.lang.String, int):int
      dk.logisoft.airattack.c.a(dk.logisoft.airattack.c, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        Properties properties = (Properties) obj;
        if (properties != null) {
            this.a.a(properties);
            this.a.b(true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                properties.store(byteArrayOutputStream, (String) null);
                byteArrayOutputStream.flush();
                j.a(byteArrayOutputStream.toString());
            } catch (IOException e) {
                Log.e("AirAttack", "failed loading cached properties", e);
            }
        } else {
            this.a.b(false);
        }
        this.a.c(true);
    }

    private static Properties a(String str) {
        URL url = new URL(str);
        Properties properties = new Properties();
        InputStream openStream = url.openStream();
        try {
            properties.load(openStream);
            return properties;
        } finally {
            openStream.close();
        }
    }

    private static Properties a(String... strArr) {
        try {
            return a(strArr[0]);
        } catch (IOException e) {
            return null;
        } catch (Exception e2) {
            Log.e("AirAttack", "Error ocurred fetching online-property file", e2);
            return null;
        }
    }
}
