package dk.logisoft.airattack;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewStub;
import android.widget.TextView;
import com.google.ads.R;
import d.Cdo;
import d.b;
import d.bq;
import d.bw;
import d.by;
import d.bz;
import d.ca;
import d.cb;
import d.cc;
import d.cd;
import d.cg;
import d.ck;
import d.cn;
import d.da;
import d.df;
import d.dj;
import d.dk;
import d.du;
import d.j;
import d.w;
import dk.logisoft.trace.d;
import java.util.Locale;

/* compiled from: ProGuard */
public class AirAttackActivity extends Activity {
    public da a;
    /* access modifiers changed from: private */
    public cn b;
    private AirAttackView c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        d.a(this);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(0);
        bz.a(getResources().getDisplayMetrics());
        setContentView((int) R.layout.main);
        setVolumeControlStream(3);
        cc.a(this);
        cc.a((int) R.string.prefKeyControls, "gestures");
        if (!cc.a((int) R.string.prefKeySound)) {
            cc.b(R.string.prefKeySound);
        }
        cc.a((int) R.string.prefKeyAccelerometerSensitivity, "5");
        cc.a((int) R.string.prefKeyParticles, "2");
        cc.a((int) R.string.prefKeyWalkShootCutoff, "300");
        w.a((LocationManager) getSystemService("location"), new Geocoder(getBaseContext(), Locale.ENGLISH), getBaseContext());
        this.c = (AirAttackView) findViewById(R.id.airAttack);
        this.b = AirAttackView.a();
        Cdo.a(this);
        df.a(this);
        dk.a(this);
        cg.a(this);
        du.a(this);
        ck.a(this);
        this.a = new da(this, (ViewStub) findViewById(R.id.helpStub), getResources(), this.b);
        this.b.a(this.a);
        if (b.a) {
            this.c.setAdView(this, (ViewStub) findViewById(R.id.adParentStub));
        }
        b();
        new bw().a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        if (!AirAttackView.a().k()) {
            return super.onKeyDown(i, keyEvent);
        }
        showDialog(5);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.b.d();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b.f();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                return Cdo.b(this, getBaseContext());
            case 2:
                return Cdo.a(this, getBaseContext());
            case 3:
                return df.a(this, getBaseContext(), c.a().e());
            case 4:
                boolean z = j.b() < cb.a().k;
                getBaseContext();
                return ck.a(this, z);
            case 5:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.dialog_gotomain_title).setMessage((int) R.string.dialog_gotomain_body).setCancelable(true).setNegativeButton((int) R.string.button_cancel, new b(this)).setPositiveButton((int) R.string.button_gotomain, new a(this)).create();
            case 6:
                return cg.a(this, getBaseContext());
            case 7:
                return du.a(this, getBaseContext());
            case 8:
                return dk.a((Activity) this);
            default:
                throw new RuntimeException();
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        super.onPrepareDialog(i, dialog);
        switch (i) {
            case 1:
                Cdo.a(dialog);
                return;
            case 2:
                Cdo.b(dialog);
                return;
            case 3:
                df.a(dialog);
                return;
            case 4:
            case 5:
            default:
                return;
            case 6:
                TextView textView = (TextView) dialog.findViewById(R.id.messageOfTheDay);
                textView.setText(Html.fromHtml(cb.a().a((int) R.string.buy_the_game_message)));
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                return;
            case 7:
                TextView textView2 = (TextView) dialog.findViewById(R.id.messageOfTheDay);
                textView2.setText(Html.fromHtml(cb.a().a((int) R.string.view_help_pages_message)));
                textView2.setMovementMethod(LinkMovementMethod.getInstance());
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.b.n();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 3, 0, (int) R.string.menu_settings).setIcon(17301577);
        menu.add(1, 8, 1, (int) R.string.menu_exit);
        menu.add(1, 7, 1, (int) R.string.menu_gameover);
        menu.add(1, 5, 1, (int) R.string.menu_resume);
        menu.add(1, 4, 1, (int) R.string.menu_pause);
        menu.add(1, 2, 1, (int) R.string.menu_message);
        menu.add(1, 9, 1, (int) R.string.menu_help);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                cn.c();
                return true;
            case 3:
                startActivityForResult(new Intent(this, PreferenceActivity.class), 873);
                return true;
            case 4:
                this.b.d();
                return true;
            case 5:
                this.b.e();
                return true;
            case 6:
            default:
                return false;
            case 7:
                this.b.j();
                return true;
            case 8:
                this.b.l();
                return true;
            case 9:
                this.a.b();
                return true;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        boolean z2;
        boolean g = this.b.g();
        MenuItem findItem = menu.findItem(4);
        if (!g) {
            z = true;
        } else {
            z = false;
        }
        findItem.setVisible(z);
        menu.findItem(5).setVisible(g);
        MenuItem findItem2 = menu.findItem(2);
        if (c.a().c() != null) {
            z2 = true;
        } else {
            z2 = false;
        }
        findItem2.setVisible(z2);
        return super.onPrepareOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    public final void a() {
        String str = getResources().getString(R.string.market_url_prefix) + AirAttackActivity.class.getPackage().getName();
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.setFlags(268435456);
            startActivity(intent);
        } catch (Exception e) {
            Log.e("AirAttack", "Error occured going to market url: " + str, e);
        }
    }

    private void b() {
        boolean e = cc.e(R.string.prefKeySound);
        ca.a().a(e);
        by.b().a(e);
        String c2 = cc.c(R.string.prefKeyControls);
        if (c2.equals("gestures")) {
            if (this.b.b != null) {
                this.b.b.a();
                this.b.b = null;
            }
            dj.a(Integer.parseInt(cc.c(R.string.prefKeyWalkShootCutoff)));
        } else if (c2.equals("accelerometer")) {
            if (this.b.b == null) {
                this.b.b = new cd(this);
            }
            this.b.b.a(Integer.parseInt(cc.c(R.string.prefKeyAccelerometerSensitivity)));
            dj.a(100000);
        } else {
            throw new RuntimeException("something went wrong");
        }
        bq.a(Float.parseFloat(cc.c(R.string.prefKeyParticles)));
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        b();
    }
}
