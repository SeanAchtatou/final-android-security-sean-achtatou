package dk.logisoft.airattack;

import android.os.Bundle;
import com.google.ads.R;

/* compiled from: ProGuard */
public class PreferenceActivity extends android.preference.PreferenceActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
    }
}
