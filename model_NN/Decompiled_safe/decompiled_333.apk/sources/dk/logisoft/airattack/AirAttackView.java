package dk.logisoft.airattack;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewStub;
import d.by;
import d.bz;
import d.ca;
import d.cb;
import d.cn;
import dk.logisoft.airattack.ads.d;
import dk.logisoft.airattack.ads.e;
import dk.logisoft.airattack.ads.o;
import java.lang.Thread;

/* compiled from: ProGuard */
public class AirAttackView extends SurfaceView implements SurfaceHolder.Callback {
    private static cn b;
    private final Context a;
    private d c = new o();

    public AirAttackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        cb.a(this.a);
        c.a(context);
        ca.a(this.a);
        by.a(this.a);
        if (b == null || b.getState() == Thread.State.TERMINATED) {
            b = new cn(holder, this.a, this.c, bz.a(), bz.b());
        } else {
            b.a(holder, context, this.c);
        }
        setFocusable(true);
    }

    public static cn a() {
        return b;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        return b.a(motionEvent);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return b.a(keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return b.a(keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        b.b(motionEvent);
        if (motionEvent.getAction() != 2) {
            return true;
        }
        try {
            Thread.sleep(50);
            return true;
        } catch (InterruptedException e) {
            return true;
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (!z) {
            b.h();
        } else {
            b.i();
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (b.getState() == Thread.State.TERMINATED) {
            b = new cn(surfaceHolder, this.a, this.c, bz.a(), bz.b());
        }
        if (b.getState() == Thread.State.NEW) {
            b.start();
        }
        b.b();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        b.a();
    }

    public void setAdView(Activity activity, ViewStub viewStub) {
        this.c = new e(activity, viewStub);
        b.a(this.c);
    }
}
