package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdActivity;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.Map;

/* renamed from: h  reason: default package */
public final class h extends WebViewClient {
    private d a;
    private Map<String, i> b;
    private boolean c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f65d;
    private boolean e = false;
    private boolean f = false;

    public h(d dVar, Map<String, i> map, boolean z, boolean z2) {
        this.a = dVar;
        this.b = map;
        this.c = z;
        this.f65d = z2;
    }

    public final void a() {
        this.e = true;
    }

    public final void b() {
        this.f = true;
    }

    public final void onPageFinished(WebView view, String str) {
        if (this.e) {
            c g = this.a.g();
            if (g != null) {
                g.b();
            } else {
                a.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.e = false;
        }
        if (this.f) {
            a.a(view);
            this.f = false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String url) {
        a.a("shouldOverrideUrlLoading(\"" + url + "\")");
        Uri parse = Uri.parse(url);
        HashMap<String, String> b2 = AdUtil.b(parse);
        if (b2 == null) {
            a.e("An error occurred while parsing the url parameters.");
            return true;
        }
        String str = b2.get("ai");
        if (str != null) {
            this.a.l().a(str);
        }
        if (a.a(parse)) {
            a.a(this.a, this.b, parse, webView);
            return true;
        } else if (this.f65d) {
            if (AdUtil.a(parse)) {
                return super.shouldOverrideUrlLoading(webView, url);
            }
            HashMap hashMap = new HashMap();
            hashMap.put(AdActivity.URL_PARAM, url);
            AdActivity.launchAdActivity(this.a, new e("intent", hashMap));
            return true;
        } else if (this.c) {
            String str2 = (!this.a.w() || !AdUtil.a(parse)) ? "intent" : "webapp";
            HashMap hashMap2 = new HashMap();
            hashMap2.put(AdActivity.URL_PARAM, parse.toString());
            AdActivity.launchAdActivity(this.a, new e(str2, hashMap2));
            return true;
        } else {
            a.e("URL is not a GMSG and can't handle URL: " + url);
            return true;
        }
    }
}
