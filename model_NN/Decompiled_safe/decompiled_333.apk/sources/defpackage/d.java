package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.Ad;
import com.google.ads.AdActivity;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: d  reason: default package */
public final class d {
    private static final Object a = new Object();
    private WeakReference<Activity> b;
    private Ad c;

    /* renamed from: d  reason: collision with root package name */
    private AdListener f11d = null;
    private c e = null;
    private AdRequest f = null;
    private AdSize g;
    private f h = new f();
    private String i;
    private g j;
    private h k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private x r;
    private boolean s;
    private LinkedList<String> t;
    private LinkedList<String> u;
    private int v;

    public d(Activity activity, Ad ad, AdSize adSize, String str, boolean z) {
        this.b = new WeakReference<>(activity);
        this.c = ad;
        this.g = adSize;
        this.i = str;
        this.s = z;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            if (z) {
                long j2 = this.p.getLong("Timeout" + str, -1);
                if (j2 < 0) {
                    this.m = 5000;
                } else {
                    this.m = j2;
                }
            } else {
                this.m = 60000;
            }
        }
        this.r = new x(this);
        this.t = new LinkedList<>();
        this.u = new LinkedList<>();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized boolean A() {
        return this.e != null;
    }

    private synchronized void B() {
        Activity activity = this.b.get();
        if (activity == null) {
            a.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator<String> it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new w(it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    public final synchronized void a() {
        Activity e2 = e();
        if (e2 == null) {
            a.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new g(e2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new h(this, a.b, true, false);
            } else {
                this.k = new h(this, a.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(AdListener adListener) {
        this.f11d = adListener;
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.e = null;
        if (this.c instanceof InterstitialAd) {
            if (errorCode == AdRequest.ErrorCode.NO_FILL) {
                this.h.n();
            } else if (errorCode == AdRequest.ErrorCode.NETWORK_ERROR) {
                this.h.l();
            }
        }
        a.c("onFailedToReceiveAd(" + errorCode + ")");
        if (this.f11d != null) {
            this.f11d.onFailedToReceiveAd(this.c, errorCode);
        }
    }

    public final synchronized void a(AdRequest adRequest) {
        if (A()) {
            a.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.isShowing()) {
            a.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity e2 = e();
            if (e2 == null) {
                a.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e2.getApplicationContext()) && AdUtil.b(e2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = adRequest;
                this.e = new c(this);
                this.e.a(adRequest);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        a.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList<String> linkedList) {
        Iterator<String> it = linkedList.iterator();
        while (it.hasNext()) {
            a.a("Adding a click tracking URL: " + it.next());
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        a((AdListener) null);
        z();
        this.j.destroy();
    }

    public final synchronized void c() {
        if (this.o) {
            a.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            a.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.c instanceof AdView)) {
            a.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            a.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            a.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return this.b.get();
    }

    public final Ad f() {
        return this.c;
    }

    public final synchronized c g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    public final synchronized g i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized h j() {
        return this.k;
    }

    public final AdSize k() {
        return this.g;
    }

    public final f l() {
        return this.h;
    }

    public final synchronized int m() {
        return this.v;
    }

    public final long n() {
        return this.m;
    }

    public final synchronized boolean o() {
        return this.n;
    }

    public final synchronized boolean p() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void q() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            v();
        }
        a.c("onReceiveAd()");
        if (this.f11d != null) {
            this.f11d.onReceiveAd(this.c);
        }
    }

    public final synchronized void r() {
        this.h.o();
        a.c("onDismissScreen()");
        if (this.f11d != null) {
            this.f11d.onDismissScreen(this.c);
        }
    }

    public final synchronized void s() {
        a.c("onPresentScreen()");
        if (this.f11d != null) {
            this.f11d.onPresentScreen(this.c);
        }
    }

    public final synchronized void t() {
        a.c("onLeaveApplication()");
        if (this.f11d != null) {
            this.f11d.onLeaveApplication(this.c);
        }
    }

    public final void u() {
        this.h.b();
        B();
    }

    public final synchronized void v() {
        Activity activity = this.b.get();
        if (activity == null) {
            a.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator<String> it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new w(it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean w() {
        return !this.u.isEmpty();
    }

    public final synchronized void x() {
        if (this.f == null) {
            a.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                a.a("Not refreshing because the ad is not visible.");
            } else {
                a.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            a.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final synchronized void y() {
        this.n = false;
    }

    public final synchronized void z() {
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        this.j.stopLoading();
    }
}
