package d;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.google.ads.R;
import dk.logisoft.airattack.AirAttackActivity;

/* renamed from: d.do  reason: invalid class name */
/* compiled from: ProGuard */
public final class Cdo {
    public static String a = "You have reached the level cap of this version of Air Attack. Please purchase the full version for unlimited levels!";
    /* access modifiers changed from: private */
    public static AirAttackActivity b;
    /* access modifiers changed from: private */
    public static int c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static int f41d;
    /* access modifiers changed from: private */
    public static int e;
    /* access modifiers changed from: private */
    public static cn f;
    /* access modifiers changed from: private */
    public static c g;
    /* access modifiers changed from: private */
    public static boolean h;

    public static void a(AirAttackActivity airAttackActivity) {
        b = airAttackActivity;
    }

    public static AlertDialog a(AirAttackActivity airAttackActivity, Context context) {
        return a(airAttackActivity, context, LayoutInflater.from(airAttackActivity).inflate((int) R.layout.score_dialog_killed, (ViewGroup) null), R.string.score_dialog_text_title_congrat);
    }

    public static AlertDialog b(AirAttackActivity airAttackActivity, Context context) {
        return a(airAttackActivity, context, LayoutInflater.from(airAttackActivity).inflate((int) R.layout.score_dialog, (ViewGroup) null), R.string.score_dialog_text_title);
    }

    private static AlertDialog a(AirAttackActivity airAttackActivity, Context context, View view, int i) {
        EditText editText = (EditText) view.findViewById(R.id.username_edit);
        editText.setText(j.a(context));
        editText.setSingleLine();
        return new AlertDialog.Builder(airAttackActivity).setIcon((int) R.drawable.star_big_on).setTitle(i).setView(view).setCancelable(false).setPositiveButton((int) R.string.score_dialog_ok, new dp(context)).create();
    }

    public static void a(int i, int i2, int i3, cn cnVar, c cVar, boolean z) {
        b.runOnUiThread(new dq(i, i2, i3, cnVar, cVar, z));
    }

    public static void a(Dialog dialog) {
        ((TextView) dialog.findViewById(R.id.waves)).setText(cb.a().a((int) R.string.you_defeated) + " " + f41d + " " + cb.a().a((int) R.string.waves));
        ((TextView) dialog.findViewById(R.id.aircrafts)).setText(cb.a().a((int) R.string.you_destroyed) + " " + c + " " + cb.a().a((int) R.string.enemy_aircrafts));
        ((TextView) dialog.findViewById(R.id.score)).setText(cb.a().a((int) R.string.your_total_score_was) + " " + e);
    }

    public static void b(Dialog dialog) {
        TextView textView = (TextView) dialog.findViewById(R.id.market);
        textView.setText(Html.fromHtml(a));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView) dialog.findViewById(R.id.score)).setText(cb.a().a((int) R.string.your_total_score_was) + " " + e);
    }
}
