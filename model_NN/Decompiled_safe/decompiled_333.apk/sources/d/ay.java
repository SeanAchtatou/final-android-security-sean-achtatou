package d;

/* compiled from: ProGuard */
public class ay extends ai {

    /* renamed from: d  reason: collision with root package name */
    private br f20d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.bs.a(d.bh, d.b, d.k, boolean):void
     arg types: [d.ay, d.b, d.k, int]
     candidates:
      d.av.a(d.q[], d.q[], float, float):void
      d.bs.a(d.bh, d.b, d.k, boolean):void */
    public final void a(k kVar, int i, int i2, b bVar, boolean z) {
        super.a(kVar, i, i2, bVar, z);
        this.c = false;
        this.f20d = (br) this.s.s.a(br.class);
        br brVar = this.f20d;
        b bVar2 = this.s;
        brVar.a((bh) this, bVar2, this.p, false);
        z zVar = (z) bVar2.s.a(z.class);
        zVar.a(bVar2.s, this, brVar, (float) (A() - 5), (float) ((B() / 2) + 3), (float) (brVar.A() / 2), (float) (brVar.B() / 2));
        brVar.a(zVar);
    }

    /* access modifiers changed from: protected */
    public final void a(bh bhVar) {
    }

    /* access modifiers changed from: protected */
    public final void c() {
        ba g;
        if (n.a(200) == 0 && (g = this.s.g()) != null) {
            if (n.a(2) == 0) {
                am amVar = (am) this.s.s.a(am.class);
                amVar.a(this.p, (int) s(), (int) t(), this.s, (int) g.s());
                this.s.a(amVar);
                return;
            }
            this.f20d.a();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (n.a(150) == 0) {
            this.b = ((float) ((n.a(5) - 2) * 2)) / 10.0f;
        }
    }

    public final boolean f() {
        ba g = this.s.g();
        if (g != null) {
            return g.s() < s();
        }
        return super.f();
    }
}
