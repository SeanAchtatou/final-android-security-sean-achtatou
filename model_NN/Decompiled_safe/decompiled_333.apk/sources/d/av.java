package d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class av extends bb {
    private static final v N = new v();
    private static final int a = (cb.a().A().getHeight() / 2);
    protected int A;
    boolean B;
    int C;
    int D;
    private q[] F;
    private q[] G;
    private q[] H = {new q()};
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    private int M;
    private final q[] O = {new q(), new q()};
    private final q[] P = {new q(), new q()};
    private final q[] Q = {new q(), new q()};
    private float b;
    private float c;

    /* renamed from: d  reason: collision with root package name */
    private float f19d;
    private float e;
    private float f;
    private int g;
    private int h;
    private int i;
    private Rect j = new Rect();
    private boolean k;
    private z l;
    private ArrayList m = new ArrayList();
    private q[] n;
    int[] o;
    protected k p;
    protected dz q;
    protected float r;
    protected b s;
    protected int t;
    protected int u;
    protected int v;
    protected int w;
    protected float x;
    protected boolean y;
    protected boolean z;

    public void a(k kVar, float f2, float f3, boolean z2, b bVar) {
        a(kVar, f2, f3, z2, bVar, 10, 0.9f, 0.9f);
    }

    public final void a(k kVar, float f2, float f3, b bVar, int i2) {
        a(kVar, f2, f3, true, bVar, i2, 0.9f, 0.9f);
    }

    public final void b(k kVar, float f2, float f3, b bVar) {
        a(kVar, f2, f3, false, bVar, 10, 1.0f, 1.0f);
    }

    public final void a(k kVar, float f2, float f3, boolean z2, b bVar, int i2, float f4, float f5) {
        a(bVar.s);
        this.b = f4;
        this.c = f5;
        Bitmap a2 = cb.a().a(this);
        a(a2.getWidth(), a2.getHeight());
        a(f2, f3, z2);
        this.p = kVar;
        this.q = kVar.c;
        this.s = bVar;
        this.f = 0.0f;
        this.r = bVar.f();
        this.H[0].a((float) this.v, (float) (this.u - 1));
        a(this.H);
        this.o = ea.a(this);
        this.i = i2;
        this.B = true;
        this.x = 1.0f;
        this.y = true;
        this.z = false;
        this.k = false;
        this.A = 0;
        this.I = true;
        this.J = true;
        this.K = true;
        this.L = true;
        this.l = null;
        this.m.clear();
    }

    /* access modifiers changed from: protected */
    public final void d(int i2) {
        this.i = i2;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
        this.t = i2;
        this.u = i3;
        int i4 = i2 / 2;
        this.v = i4;
        int i5 = i3 / 2;
        this.w = i5;
        int round = Math.round(((float) i2) - (((float) i2) * this.b));
        this.h = round;
        int round2 = Math.round(((float) i3) - (((float) i3) * this.c));
        this.g = round2;
        this.M = ((i4 + i5) - ((round + round2) / 2)) / 2;
        this.K = true;
        this.J = true;
    }

    /* access modifiers changed from: protected */
    public final void a(q[] qVarArr) {
        this.n = qVarArr;
        if (this.F == null || this.F.length != this.n.length) {
            int length = qVarArr.length;
            q[] qVarArr2 = new q[length];
            for (int i2 = 0; i2 < length; i2++) {
                qVarArr2[i2] = new q();
            }
            this.F = qVarArr2;
        }
        if (this.G == null || this.G.length != this.n.length) {
            int length2 = qVarArr.length;
            q[] qVarArr3 = new q[length2];
            for (int i3 = 0; i3 < length2; i3++) {
                qVarArr3[i3] = new q();
            }
            this.G = qVarArr3;
        }
    }

    public final void a(float f2, float f3, boolean z2) {
        this.I = true;
        this.L = true;
        if (z2) {
            this.f19d = f2 - ((float) this.v);
            this.e = f3 - ((float) this.w);
        } else {
            this.f19d = f2;
            this.e = f3;
        }
        a();
    }

    public void a(Canvas canvas) {
        ArrayList arrayList = this.m;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            av avVar = (av) arrayList.get(i2);
            if (avVar.i < this.i) {
                avVar.a(canvas);
            }
        }
        ea.a(canvas, this);
        for (int i3 = 0; i3 < size; i3++) {
            av avVar2 = (av) arrayList.get(i3);
            if (avVar2.i >= this.i) {
                avVar2.a(canvas);
            }
        }
    }

    public final void a(z zVar) {
        this.l = zVar;
        zVar.a.m.add(this);
    }

    public final void m() {
        if (this.l != null) {
            this.l.a.m.remove(this);
            this.l.G();
            this.l = null;
        }
    }

    /* access modifiers changed from: protected */
    public final ArrayList n() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public final void o() {
        this.I = true;
        this.L = true;
        float f2 = this.f19d + ((float) this.v);
        float f3 = this.e + ((float) this.w);
        a(this.t - 1, this.u - 1);
        this.f19d -= (this.f19d + ((float) this.v)) - f2;
        this.e -= (this.e + ((float) this.w)) - f3;
        a();
    }

    /* access modifiers changed from: protected */
    public final void a(float f2) {
        this.J = true;
        this.K = true;
        this.f += f2;
    }

    /* access modifiers changed from: protected */
    public final void b(float f2) {
        this.J = true;
        this.K = true;
        this.f = f2;
    }

    private void a() {
        if (this.L) {
            this.L = false;
            Rect rect = this.j;
            int i2 = this.h;
            int i3 = this.g;
            int i4 = (int) this.e;
            int i5 = (int) this.f19d;
            if (this.f == 0.0f || !(this instanceof bc)) {
                rect.set(i5 + i2, i4 + i3, (i5 + this.t) - i2, (i4 + this.u) - i3);
                return;
            }
            int i6 = this.v + i5;
            int i7 = this.w + i4;
            int i8 = this.M;
            rect.set(i6 - i8, i7 - i8, i6 + i8, i7 + i8);
        }
    }

    public final boolean p() {
        if (!this.E) {
            Log.d("foo", "object: " + getClass());
            throw new RuntimeException("move called on un-initialized object!");
        } else if (this.k) {
            return false;
        } else {
            if (this.l != null) {
                av avVar = this.l.a;
                if (avVar.f()) {
                    a((((avVar.f19d + ((float) avVar.t)) - this.l.c) - ((float) this.t)) + this.l.e, (avVar.e + this.l.f55d) - this.l.f, false);
                } else {
                    a((avVar.f19d + this.l.c) - this.l.e, (avVar.e + this.l.f55d) - this.l.f, false);
                }
                b();
            } else {
                d();
            }
            ArrayList arrayList = this.m;
            if (!arrayList.isEmpty()) {
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((av) arrayList.get(i2)).p();
                }
            }
            a();
            return !this.k;
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    /* access modifiers changed from: protected */
    public void b() {
        throw new RuntimeException("Unimplemented adapter method ! - should only be invoked on children attachee objects");
    }

    public final q[] q() {
        if (this.J) {
            a(this.n, this.F, this.f, (float) this.v, (float) this.w);
            a(this.F, this.G, this.f19d, this.e);
            this.J = false;
            this.I = false;
        } else if (this.I) {
            a(this.F, this.G, this.f19d, this.e);
            this.I = false;
        }
        return this.G;
    }

    private static void a(q[] qVarArr, q[] qVarArr2, float f2, float f3, float f4) {
        int length = qVarArr.length;
        N.a();
        N.a((double) f2, (double) f3, (double) f4);
        N.a(qVarArr, 0, qVarArr2, 0, length);
    }

    private static void a(q[] qVarArr, q[] qVarArr2, float f2, float f3) {
        int length = qVarArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            qVarArr2[i2].a = qVarArr[i2].a + f2;
            qVarArr2[i2].b = qVarArr[i2].b + f3;
        }
    }

    public final q r() {
        q qVar;
        q qVar2;
        q[] qVarArr = this.O;
        q[] qVarArr2 = this.P;
        q[] qVarArr3 = this.Q;
        if (this.K) {
            qVarArr[0].a = 0.0f;
            qVarArr[0].b = (float) this.w;
            qVarArr[1].a = (float) this.t;
            qVarArr[1].b = (float) this.w;
            a(qVarArr, qVarArr2, this.f, (float) this.v, (float) this.w);
            this.K = false;
        }
        if (f()) {
            qVar = qVarArr3[1];
            qVar2 = qVarArr2[1];
        } else {
            qVar = qVarArr3[0];
            qVar2 = qVarArr2[0];
        }
        qVar.a = qVar2.a + this.f19d;
        qVar.b = qVar2.b + this.e;
        return qVar;
    }

    public final float s() {
        return this.f19d + ((float) this.v);
    }

    public final float t() {
        return this.e + ((float) this.w);
    }

    public final float u() {
        return this.e + ((float) this.u);
    }

    public final float v() {
        return this.f19d;
    }

    public final float w() {
        return this.e;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final int c(float f2) {
        try {
            float f3 = this.f19d;
            this.f19d += this.r * f2;
            this.I = true;
            q[] q2 = q();
            if (this.f19d < 0.0f) {
                this.f19d = 0.0f;
                this.I = true;
                q2 = q();
            } else if (this.f19d + ((float) this.t) >= ((float) this.p.b())) {
                this.f19d = (float) (this.p.b() - this.t);
                this.I = true;
                q2 = q();
            }
            float a2 = this.q.a((int) q2[0].a, (int) q2[0].b, a);
            if (a2 >= 0.0f) {
                if (a2 != q2[0].b) {
                    this.e += a2 - q2[0].b;
                    this.I = true;
                }
                this.L = true;
                return 2;
            }
            this.f19d = f3;
            this.J = true;
            float f4 = q2[0].a;
            float f5 = q2[0].b;
            if (f4 < 0.0f || f4 >= ((float) this.p.b()) || f5 < 0.0f || f5 >= ((float) this.p.a())) {
                this.L = true;
                return 0;
            }
            this.L = true;
            return 1;
        } catch (Throwable th) {
            this.L = true;
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final int d(float f2) {
        try {
            float f3 = this.f19d + (this.r * f2);
            int i2 = f2 > 0.0f ? 1 : -1;
            if (!this.z) {
                do {
                    if (i2 > 0) {
                        if (((float) ((int) (this.f19d + 1.0f))) >= f3) {
                        }
                    } else if (this.f19d - 4.0f <= ((float) ((int) f3))) {
                    }
                    this.f19d += (float) i2;
                    this.I = true;
                } while (y());
                this.f19d -= (float) i2;
                this.L = true;
                return 0;
            }
            this.I = true;
            this.f19d = f3;
            float f4 = this.e;
            if (f4 >= ((float) this.p.a()) || this.f19d + ((float) this.t) < 0.0f || this.f19d >= ((float) this.p.b()) || f4 + ((float) this.u) < 0.0f) {
                this.L = true;
                return 1;
            }
            this.L = true;
            return 2;
        } catch (Throwable th) {
            this.L = true;
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final int e(float f2) {
        float f3;
        try {
            float f4 = this.e + (this.r * f2);
            int i2 = f2 > 0.0f ? 1 : -1;
            if (!this.z) {
                do {
                    if (i2 > 0) {
                        if (((float) ((int) (this.e + 1.0f))) >= f4) {
                        }
                    } else if (this.e - 4.0f <= ((float) ((int) f4))) {
                    }
                    f3 = this.e;
                    this.e += (float) i2;
                    this.I = true;
                } while (y());
                this.I = true;
                this.e = f3;
                this.L = true;
                return 0;
            }
            this.I = true;
            this.e = f4;
            float f5 = this.e;
            if (f5 >= ((float) this.p.a()) || this.f19d + ((float) this.t) < 0.0f || this.f19d >= ((float) this.p.b()) || f5 + ((float) this.u) < 0.0f) {
                this.L = true;
                return 1;
            }
            this.L = true;
            return 2;
        } catch (Throwable th) {
            this.L = true;
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final int a(float f2, boolean z2) {
        try {
            float f3 = this.e;
            this.e += this.r * f2;
            this.I = true;
            if (this.z || y()) {
                float f4 = this.e;
                float f5 = this.f19d;
                if (f4 >= ((float) this.p.a()) || ((float) this.t) + f5 < 0.0f || f5 >= ((float) this.p.b()) || f4 + ((float) this.u) < 0.0f) {
                    this.L = true;
                    return 1;
                }
                this.L = true;
                return 2;
            }
            this.e = f3;
            this.I = true;
            if (z2) {
                this.L = true;
                return 0;
            }
            int e2 = e(f2);
            this.L = true;
            return e2;
        } catch (Throwable th) {
            this.L = true;
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final void x() {
        while (!y()) {
            this.I = true;
            this.L = true;
            this.e -= 1.0f;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean y() {
        float f2 = this.f19d;
        float f3 = this.e;
        if (!this.B && (f3 < 0.0f || f3 + ((float) this.u) >= ((float) this.q.d()))) {
            return false;
        }
        if (this.z) {
            return true;
        }
        q[] q2 = q();
        int b2 = this.p.b();
        boolean z2 = true;
        for (q qVar : q2) {
            if (!this.q.a((int) qVar.a, (int) qVar.b) || (!this.y && (f2 < 0.0f || ((float) this.t) + f2 > ((float) b2)))) {
                this.C = (int) qVar.a;
                this.D = (int) qVar.b;
                z2 = false;
            }
        }
        return z2;
    }

    public final boolean a(av avVar) {
        return Rect.intersects(this.j, avVar.j);
    }

    public boolean f() {
        return false;
    }

    public final Rect z() {
        return this.j;
    }

    public final int A() {
        return this.t;
    }

    public final int B() {
        return this.u;
    }

    public final boolean C() {
        return this.k;
    }

    public void b(boolean z2) {
        this.k = z2;
        ArrayList arrayList = this.m;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((av) arrayList.get(i2)).b(true);
        }
    }

    public final float D() {
        return this.f;
    }

    public final float E() {
        return (float) this.v;
    }

    public final float F() {
        return (float) this.w;
    }
}
