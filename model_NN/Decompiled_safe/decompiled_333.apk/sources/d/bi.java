package d;

import android.graphics.Canvas;
import com.google.ads.R;

/* compiled from: ProGuard */
public class bi extends av {
    private static final bn a = new bj(cb.a().a((int) R.string.supply_crate_emp));
    /* access modifiers changed from: private */
    public static final bn b = new bk(cb.a().a((int) R.string.supply_crate_extra_rocket));
    private static final bn c = new bl(cb.a().a((int) R.string.supply_crate_armor_piercing_rockets));

    /* renamed from: d  reason: collision with root package name */
    private static final bn f24d = new bm(cb.a().a((int) R.string.supply_crate_multi_jump_boots));
    private static final bn[] e = {a, b, c, f24d};
    /* access modifiers changed from: private */
    public bn f;
    private b g;

    public final void a(k kVar, float f2, float f3, boolean z, b bVar) {
        super.a(kVar, f2, f3, z, bVar);
        this.g = bVar;
        this.r = bVar.f() * 25.0f;
        this.f = e[n.a(e.length)];
    }

    public final void d() {
        switch (e(1.0f)) {
            case 1:
                if (w() >= ((float) this.p.a())) {
                    b(true);
                    break;
                }
                break;
        }
        ba h = this.g.h(this);
        if (h != null && this.f.a(h, this, this.g)) {
            b(true);
        }
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
        ea.a(canvas, this.f.a, 12, ((int) w()) - 10, (int) s());
    }
}
