package d;

import com.google.ads.R;

/* compiled from: ProGuard */
public class ac extends bc {
    protected boolean a;
    bh b;

    public final void a(k kVar, float f, float f2, float f3, float f4, b bVar, bh bhVar) {
        a(kVar, f, f2, f3, f4, bVar, 3.5f);
        this.b = bhVar;
        this.a = false;
    }

    public final void a(k kVar, float f, float f2, float f3, float f4, b bVar, float f5) {
        cb.a().e();
        a(kVar, f, f2, f3, f4, bVar, 2, bVar.f() * f5);
        this.a = true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.a && n.a(200) == 0) {
            this.p.b((int) s(), (int) t());
            ao.a(cb.a().a((int) R.string.in_game_depleted_uranium_polution), s(), t(), this.s);
        }
    }

    public final void d() {
        super.d();
        bh i = this.s.i(this);
        if (i != null && i != this.b) {
            i.g();
            b(true);
        }
    }
}
