package d;

import android.content.Context;
import android.media.SoundPool;
import com.google.ads.R;

/* compiled from: ProGuard */
public final class by {
    private static int a = 0;
    private static int b = 1;
    private static int c = 1;

    /* renamed from: d  reason: collision with root package name */
    private static int f29d = 6;
    private static by e = null;
    private int A;
    private int B;
    private int C;
    private int D;
    private SoundPool E;
    private boolean F;
    private final Context G;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    private by(Context context) {
        this.G = context;
        b(context);
    }

    private void b(Context context) {
        this.E = new SoundPool(f29d, 3, 1);
        this.f = this.E.load(context, R.raw.rocketlaunch, 1);
        this.g = this.E.load(context, R.raw.rocketexplosion, 1);
        this.h = this.E.load(context, R.raw.splash, 1);
        this.i = this.E.load(context, R.raw.ooh, 1);
        this.l = this.E.load(context, R.raw.rocketlaunch, 1);
        this.m = this.E.load(context, R.raw.gunfire, 1);
        this.j = this.E.load(context, R.raw.crash, 1);
        this.k = this.E.load(context, R.raw.mouseclick, 1);
        this.n = this.E.load(context, R.raw.bombexplosion, 1);
        this.o = this.E.load(context, R.raw.napalmexplosion, 1);
        this.p = this.E.load(context, R.raw.clusterbombexplosion, 1);
        this.r = this.E.load(context, R.raw.nextwave, 1);
        this.s = this.E.load(context, R.raw.bullethitsground, 1);
        this.t = this.E.load(context, R.raw.dudbombticking, 1);
        this.u = this.E.load(context, R.raw.pilotcapture, 1);
        this.v = this.E.load(context, R.raw.supplycratepickup, 1);
        this.q = this.E.load(context, R.raw.clusterfragmentexplode, 1);
        this.w = this.E.load(context, R.raw.clusterfragmentbounce, 1);
        this.x = this.E.load(context, R.raw.crashland, 1);
        this.y = this.E.load(context, R.raw.parachute, 1);
        this.z = this.E.load(context, R.raw.digging, 1);
        this.A = this.E.load(context, R.raw.nuke_part1, 1);
        this.B = this.E.load(context, R.raw.nuke_part2, 1);
        this.C = this.E.load(context, R.raw.gasbombexplosion, 1);
        this.D = this.E.load(context, R.raw.jump, 1);
    }

    private void A() {
        if (this.E != null) {
            this.E.release();
        }
        this.E = null;
    }

    public final synchronized void a() {
        A();
        b(this.G);
    }

    public static void a(Context context) {
        if (e == null) {
            e = new by(context);
        }
    }

    public static by b() {
        return e;
    }

    public final synchronized void a(boolean z2) {
        this.F = z2;
        if (this.F) {
            b(this.G);
        } else {
            A();
        }
    }

    public final void c() {
        if (this.F) {
            this.E.play(this.f, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void d() {
        if (this.F) {
            this.E.play(this.g, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void e() {
        if (this.F) {
            this.E.play(this.i, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void f() {
        if (this.F) {
            this.E.play(this.h, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void g() {
        if (this.F) {
            this.E.play(this.j, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void h() {
        if (this.F) {
            this.E.play(this.k, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void i() {
        if (this.F) {
            this.E.play(this.o, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void j() {
        if (this.F) {
            this.E.play(this.n, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void k() {
        if (this.F) {
            this.E.play(this.l, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void l() {
        if (this.F) {
            this.E.play(this.m, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void m() {
        if (this.F) {
            this.E.play(this.r, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void n() {
        if (this.F) {
            this.E.play(this.p, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void o() {
        if (this.F) {
            this.E.play(this.q, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void p() {
        if (this.F) {
            this.E.play(this.t, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void q() {
        if (this.F) {
            this.E.play(this.v, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void r() {
        if (this.F) {
            this.E.play(this.s, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void s() {
        if (this.F) {
            this.E.play(this.u, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void t() {
        if (this.F) {
            this.E.play(this.w, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void u() {
        if (this.F) {
            this.E.play(this.x, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void v() {
        if (this.F) {
            this.E.play(this.y, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void w() {
        if (this.F) {
            this.E.play(this.z, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void x() {
        if (this.F) {
            this.E.play(this.A, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void y() {
        if (this.F) {
            this.E.play(this.B, 1.0f, 1.0f, b, a, (float) c);
        }
    }

    public final void z() {
        if (this.F) {
            this.E.play(this.C, 1.0f, 1.0f, b, a, (float) c);
        }
    }
}
