package d;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import java.io.IOException;
import java.util.List;

/* compiled from: ProGuard */
public final class w {
    private static w a;
    private String b = null;
    private double c;

    /* renamed from: d  reason: collision with root package name */
    private double f53d;
    private String e;
    private String f;
    private Location g;

    private w(LocationManager locationManager, Geocoder geocoder, Context context) {
        List<String> providers;
        String str;
        if (this.b == null) {
            try {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(2);
                criteria.setCostAllowed(false);
                if (!(locationManager == null || geocoder == null || (providers = locationManager.getProviders(criteria, false)) == null || providers.size() <= 0 || (str = providers.get(0)) == null)) {
                    a(locationManager.getLastKnownLocation(str), geocoder);
                    locationManager.requestLocationUpdates(str, 0, 0.0f, new x(this, geocoder, locationManager), context.getMainLooper());
                }
            } catch (RuntimeException e2) {
                Log.e("AirAttack", "Runtime exception bug", e2);
            } finally {
                d();
            }
        }
    }

    public static void a(LocationManager locationManager, Geocoder geocoder, Context context) {
        if (locationManager == null) {
            throw new IllegalArgumentException("Location service not found");
        } else if (a == null) {
            a = new w(locationManager, geocoder, context);
        }
    }

    public static w a() {
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("Not initialized");
    }

    public final String b() {
        return this.b;
    }

    public final Location c() {
        return this.g;
    }

    private void d() {
        if (this.b == null || this.b.equals("N/A")) {
            this.b = cc.b("COUNTRY", "N/A");
        } else {
            cc.a("COUNTRY", this.b);
        }
        if (this.e == null || this.e.equals("")) {
            this.e = cc.b("CITY", "");
        } else {
            cc.a("CITY", this.e);
        }
        if (this.f == null || this.f.equals("")) {
            this.f = cc.b("POSTAL", "");
        } else {
            cc.a("POSTAL", this.f);
        }
    }

    /* access modifiers changed from: private */
    public void a(Location location, Geocoder geocoder) {
        this.g = location;
        if (location != null) {
            try {
                this.c = location.getLatitude();
                this.f53d = location.getLongitude();
                List<Address> fromLocation = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 3);
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= fromLocation.size()) {
                        break;
                    }
                    Address address = fromLocation.get(i2);
                    if (address.getCountryName() != null) {
                        this.b = address.getCountryName();
                        break;
                    }
                    if (address.getCountryCode() != null) {
                        this.b = address.getCountryCode();
                    }
                    this.e = address.getLocality();
                    this.f = address.getPostalCode();
                    i = i2 + 1;
                }
                d();
            } catch (IOException e2) {
            }
        }
    }
}
