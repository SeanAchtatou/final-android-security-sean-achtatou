package d;

import com.google.ads.R;

/* compiled from: ProGuard */
public class ai extends bh {
    ax a;
    protected float b;
    protected boolean c;

    /* renamed from: d  reason: collision with root package name */
    private int f14d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.bh.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, float, int, d.b]
     candidates:
      d.ai.a(d.k, int, int, d.b, boolean):void
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.bh.a(d.k, float, float, boolean, d.b):void */
    public void a(k kVar, int i, int i2, b bVar, boolean z) {
        a(kVar, (float) i, (float) i2, true, bVar);
        by.b().v();
        this.r = bVar.f() * 30.0f;
        ax axVar = (ax) bVar.s.a(ax.class);
        axVar.a(this.p, (int) s(), (int) t(), bVar);
        z zVar = (z) bVar.s.a(z.class);
        zVar.a(bVar.s, axVar, this, (float) (axVar.A() / 2), (float) axVar.B(), (float) this.v, (float) this.w);
        a(zVar);
        this.a = axVar;
        bVar.a(axVar);
        if (z) {
            ao.a(cb.a().a((int) R.string.in_game_wounded_pilot), s(), w(), bVar);
        }
        this.c = true;
        this.f14d = 0;
    }

    public final void d() {
        super.d();
        if (this.j) {
            if (((float) this.f14d) <= 9.5f) {
                a();
                c(this.b);
            }
            switch (e(2.0f)) {
                case 0:
                    if (((float) this.f14d) <= 9.5f) {
                        this.f14d = 0;
                        break;
                    } else {
                        g();
                        break;
                    }
                case 1:
                    b(true);
                    e();
                    break;
            }
            this.f14d++;
            H();
            i();
            c();
        }
    }

    private void i() {
        if (this.c && n.a(80) == 0) {
            this.s.r.a(s(), t(), new int[]{-65536}, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (n.a(150) == 0) {
            this.b = ((float) (n.a(5) - 2)) / 10.0f;
        }
    }

    private void H() {
        ba h = this.s.h(this);
        if (h != null) {
            a((bh) h);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        q[] q = q();
        if (!this.q.a((int) q[0].a, (int) q[0].b)) {
            e();
        }
        H();
        i();
        c();
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        if (this.a != null) {
            m();
            this.a.a();
            this.a = null;
            x();
        }
    }

    /* access modifiers changed from: protected */
    public void a(bh bhVar) {
        b(true);
        e();
        ao aoVar = (ao) this.s.s.a(ao.class);
        aoVar.a(cb.a().a((int) R.string.in_game_prisoner_exchange), s(), w(), this.p, this.s, 12);
        this.s.a(aoVar);
        by.b().s();
        this.s.i();
    }

    public final void g() {
        super.g();
        e();
    }
}
