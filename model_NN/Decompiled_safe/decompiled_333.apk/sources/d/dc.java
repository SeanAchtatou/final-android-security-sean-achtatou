package d;

import android.view.View;
import com.google.ads.R;

/* compiled from: ProGuard */
final class dc implements View.OnClickListener {
    final /* synthetic */ da a;

    dc(da daVar) {
        this.a = daVar;
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnHelpOk:
                this.a.i.setVisibility(8);
                this.a.h.i();
                cf.a();
                this.a.i.setBackgroundDrawable(null);
                return;
            case R.id.btnHelpPrev:
                if (da.f39d >= 0) {
                    cf.a();
                    this.a.a(da.f());
                    return;
                }
                return;
            case R.id.btnHelpNext:
                if (da.f39d < da.c.length - 1) {
                    cf.a();
                    this.a.a(da.e());
                    return;
                }
                return;
            default:
                throw new IllegalArgumentException("id: " + view.getId());
        }
    }
}
