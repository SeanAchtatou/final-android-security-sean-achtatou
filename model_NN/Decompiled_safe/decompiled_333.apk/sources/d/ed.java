package d;

import java.util.HashMap;

/* compiled from: ProGuard */
public final class ed {
    private HashMap a = new HashMap();

    public final Object a(Class cls) {
        ee eeVar = (ee) this.a.get(cls);
        if (eeVar == null) {
            eeVar = new ee(cls, 50);
            this.a.put(cls, eeVar);
        }
        return eeVar.a();
    }

    public final void a(Object obj) {
        ((ee) this.a.get(obj.getClass())).a(obj);
    }
}
