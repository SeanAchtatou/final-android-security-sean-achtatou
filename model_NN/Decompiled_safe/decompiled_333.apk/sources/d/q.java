package d;

/* compiled from: ProGuard */
public final class q extends p {
    public float a;
    public float b;

    public q() {
    }

    public q(float f, float f2) {
        this.a = f;
        this.b = f2;
    }

    public final double a() {
        return (double) this.a;
    }

    public final double b() {
        return (double) this.b;
    }

    public final void a(double d2, double d3) {
        this.a = (float) d2;
        this.b = (float) d3;
    }

    public final void a(float f, float f2) {
        this.a = f;
        this.b = f2;
    }

    public final String toString() {
        return "Point2D.Float[" + this.a + ", " + this.b + "]";
    }
}
