package d;

/* compiled from: ProGuard */
public abstract class p implements Cloneable {
    public abstract double a();

    public abstract void a(double d2, double d3);

    public abstract double b();

    protected p() {
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(a()) ^ (Double.doubleToLongBits(b()) * 31);
        return ((int) (doubleToLongBits >> 32)) ^ ((int) doubleToLongBits);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof p)) {
            return super.equals(obj);
        }
        p pVar = (p) obj;
        return a() == pVar.a() && b() == pVar.b();
    }
}
