package d;

import android.util.DisplayMetrics;
import android.util.Log;

/* compiled from: ProGuard */
public final class bz {
    public static int a;
    public static int b;
    public static float c = 1.0f;

    /* renamed from: d  reason: collision with root package name */
    public static boolean f30d = false;

    public static void a(DisplayMetrics displayMetrics) {
        if (displayMetrics.widthPixels > displayMetrics.heightPixels) {
            a = displayMetrics.widthPixels;
            b = displayMetrics.heightPixels;
        } else {
            b = displayMetrics.widthPixels;
            a = displayMetrics.heightPixels;
            Log.e("DisplayMetricsManager", "swapped width/height as DisplayMetrics reported wrong portrait/landscape size");
        }
        c = displayMetrics.density;
        f30d = true;
    }

    private static void d() {
        if (!f30d) {
            throw new IllegalStateException("DisplayMetricsManager not initialised yet");
        }
    }

    public static int a() {
        d();
        return a;
    }

    public static int b() {
        d();
        return b;
    }

    public static float c() {
        d();
        return c;
    }
}
