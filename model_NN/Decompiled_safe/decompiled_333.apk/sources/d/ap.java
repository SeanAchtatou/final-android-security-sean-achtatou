package d;

import com.google.ads.R;

/* compiled from: ProGuard */
public class ap extends ai {

    /* renamed from: d  reason: collision with root package name */
    float f17d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.ai.a(d.k, int, int, d.b, boolean):void
     arg types: [d.k, int, int, d.b, int]
     candidates:
      d.bh.a(d.k, float, float, boolean, d.b):void
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void
      d.ai.a(d.k, int, int, d.b, boolean):void */
    public final void a(k kVar, int i, int i2, b bVar) {
        a(kVar, i, i2, bVar, false);
        ao.a(cb.a().a((int) R.string.in_game_paratrooper_beware), s(), w(), bVar);
        this.c = false;
        this.f17d = 0.15f + (n.a() / 6.0f);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        ba g = this.s.g();
        if (g == null) {
            this.b = 0.0f;
        } else if (g.v() < v()) {
            this.b = -this.f17d;
        } else {
            this.b = this.f17d;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(bh bhVar) {
        bhVar.g();
    }
}
