package d;

import android.graphics.Canvas;

/* compiled from: ProGuard */
public final class bp {
    private int a;
    private float b;
    private bq c;

    /* renamed from: d  reason: collision with root package name */
    private int f25d;
    private float e;
    private float f;
    private int g = 0;
    private float h;
    private float i;
    private float j;
    private float k;

    bp() {
    }

    public final void a(bq bqVar, int i2, float f2, float f3, float f4, float f5, float f6, float f7, int i3, int i4) {
        this.h = f2 - 1.0f;
        this.i = f3 - 1.0f;
        this.j = f4;
        this.k = f5;
        this.e = f6;
        this.f = f7;
        this.b = i3 < 500 ? (float) i3 : 500.0f;
        this.a = i2;
        this.c = bqVar;
        this.f25d = i4;
        this.g = 0;
    }

    public final boolean a(int i2, int i3) {
        boolean z;
        boolean z2;
        boolean z3;
        int i4 = (int) this.h;
        int i5 = (int) this.i;
        int i6 = ((int) this.j) + i4;
        int i7 = ((int) this.k) + i5;
        dz dzVar = this.c.b;
        if (this.f25d == 1) {
            boolean z4 = i5 >= i2 && !dzVar.a(i6, i5);
            if (i7 < i2 || dzVar.a(i4, i7)) {
                z3 = false;
            } else {
                z3 = true;
            }
            if (z4) {
                this.j = (-this.j) / 4.0f;
            }
            if (z3) {
                this.k = (-this.k) / 4.0f;
            }
        }
        if (this.f25d == 2) {
            if (i5 < i2 || dzVar.a(i6, i5)) {
                z = false;
            } else {
                z = true;
            }
            if (i7 < i2 || dzVar.a(i4, i7)) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (i7 >= i3 || z || z2) {
                int i8 = (int) this.h;
                this.c.a.c.b(i8, dzVar.b(i8, (int) this.i), this.a);
                return false;
            }
        }
        this.h += this.j;
        this.i += this.k;
        this.j += this.e;
        this.k += this.f;
        int i9 = this.g + 1;
        this.g = i9;
        return ((float) i9) < this.b;
    }

    public final void a(Canvas canvas) {
        ea.a(canvas, (int) this.h, (int) this.i, 3, 3, this.a);
    }
}
