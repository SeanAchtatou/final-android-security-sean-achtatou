package d;

/* compiled from: ProGuard */
public abstract class ef extends Thread {
    private boolean a = false;

    public abstract void a();

    private synchronized void b() {
        this.a = true;
        notifyAll();
    }

    public final void run() {
        try {
            a();
        } finally {
            b();
        }
    }
}
