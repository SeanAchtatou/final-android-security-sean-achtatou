package d;

import android.graphics.Canvas;

/* compiled from: ProGuard */
public final class bq {

    /* renamed from: d  reason: collision with root package name */
    public static final float[] f26d = new float[1000];
    public static final float[] e = new float[1000];
    public static final float[] f = new float[1000];
    public static final int[] g = new int[1000];
    public static int h = 0;
    private static int i = 2500;
    private static float j = 1.0f;
    private static final int[] o = cb.a().a(cb.a().g());
    private static final int[] p = {-16768512, -16751104, -16733696};
    private static final int[] q = {-13421773, -10066330, -8947849};
    k a;
    dz b;
    int c = 0;
    private final bp[] k = new bp[2500];
    private int l = 0;
    private final bp[] m = new bp[5];
    private boolean n;

    static {
        o oVar = new o(System.nanoTime());
        for (int i2 = 0; i2 < 1000; i2++) {
            float nextFloat = oVar.nextFloat() * 6.2831855f;
            f26d[i2] = (float) Math.cos((double) nextFloat);
            e[i2] = (float) Math.sin((double) nextFloat);
            f[i2] = oVar.nextFloat();
            g[i2] = oVar.nextInt(Integer.MAX_VALUE);
        }
    }

    public bq(k kVar) {
        this.a = kVar;
        this.b = kVar.c;
    }

    private void a(bq bqVar, int i2, float f2, float f3, float f4, float f5, float f6, float f7, int i3, int i4) {
        if (this.l < i) {
            bp bpVar = this.k[this.l];
            if (bpVar == null) {
                bp[] bpVarArr = this.k;
                int i5 = this.l;
                bp bpVar2 = new bp();
                bpVarArr[i5] = bpVar2;
                bpVar = bpVar2;
            }
            this.l++;
            bpVar.a(bqVar, i2 | -16777216, f2, f3, f4, f5, f6, f7, i3, i4);
        }
    }

    public final void a(float f2, float f3, int[] iArr, int i2) {
        a(f2, f3, iArr, i2, true, 1, 1.0f);
    }

    private int a(int i2) {
        int i3 = (int) (((float) i2) * j);
        if (i3 <= 0) {
            i3 = 1;
        }
        if (this.n && i3 > 1) {
            i3 = 1;
        }
        int i4 = i - this.l;
        return i3 < i4 ? i3 : i4;
    }

    public final void a(float f2, float f3, int[] iArr, int i2, boolean z, int i3, float f4) {
        int a2 = a(i2) / 2;
        int[] iArr2 = g;
        int i4 = h;
        float[] fArr = f26d;
        float[] fArr2 = e;
        int i5 = 0;
        while (true) {
            int i6 = i5;
            int i7 = i4;
            if (i6 < a2) {
                float f5 = (f[i7] * 1.5f) + 3.0f;
                float f6 = fArr[i7] * f5 * f4;
                float f7 = f5 * fArr2[i7] * f4;
                float f8 = 0.0f;
                if (z) {
                    f8 = 0.5f;
                }
                float f9 = f2;
                float f10 = f3;
                a(this, iArr[i6 % iArr.length], f9, f10, f6, f7, 0.0f, f8, (iArr2[i7] % 25) + 15, i3);
                i4 = (i7 + 1) % 1000;
                i5 = i6 + 1;
            } else {
                h = i7;
                return;
            }
        }
    }

    public final void a(float f2, float f3, float f4, float f5) {
        if (!this.n) {
            int i2 = h;
            float[] fArr = f;
            int i3 = 0;
            float f6 = f4;
            float f7 = f5;
            while (i3 <= 0) {
                float f8 = f6 + (fArr[i2] - 0.5f);
                int i4 = (i2 + 1) % 1000;
                float f9 = (float) (((double) f7) + (((double) fArr[i4]) - 0.5d));
                a(this, -1, f2, f3, f8, f9, 0.0f, 0.0f, (g[i4] % 5) + 2, 0);
                i2 = (i4 + 1) % 1000;
                i3++;
                f6 = f8;
                f7 = f9;
            }
            h = i2;
        }
    }

    public final void a(float f2, float f3, int i2) {
        int i3 = h;
        float[] fArr = f;
        int[] iArr = g;
        int a2 = a(i2);
        int i4 = a2 / 2;
        int i5 = 0;
        while (true) {
            int i6 = i5;
            int i7 = i3;
            if (i6 < a2) {
                a(this, o[iArr[i7] % o.length], (f2 - ((float) i4)) + ((float) i6), f3, 0.0f, (-fArr[i7]) * 2.0f, 0.0f, 0.0f, 10, 0);
                i3 = (i7 + 1) % 1000;
                i5 = i6 + 3;
            } else {
                h = i7;
                return;
            }
        }
    }

    public final void a(float f2, float f3) {
        int i2 = h;
        float[] fArr = f;
        int[] iArr = g;
        int a2 = a(1);
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < a2) {
                int i5 = (i2 + 1) % 1000;
                a(this, p[iArr[i2] % p.length], ((fArr[i2] * 27.0f) + f2) - 13.0f, f3, 0.0f, (-fArr[i5]) - 1.0f, 0.0f, 0.0f, (iArr[i5] % 10) + 20, 0);
                i2 = (i5 + 1) % 1000;
                i3 = i4 + 1;
            } else {
                h = i2;
                return;
            }
        }
    }

    public final void b(float f2, float f3, int i2) {
        int i3;
        int i4 = h;
        float[] fArr = f;
        int[] iArr = g;
        int i5 = i4 % 997;
        int a2 = a(i2 / 21);
        int i6 = i2 / 2;
        int i7 = 0;
        while (true) {
            int i8 = i7;
            i3 = i5;
            if (i8 >= a2) {
                break;
            }
            float f4 = ((fArr[i3] * ((float) i2)) + f2) - ((float) i6);
            float f5 = ((fArr[i3 + 1] * 12.0f) + f3) - 6.0f;
            int i9 = -256;
            if (i8 % 2 == 1) {
                i9 = -14336;
            }
            a(this, i9, f4, f5, 0.0f, -fArr[i3 + 2], 0.0f, 0.0f, 10, 0);
            i5 = (i3 + 3) % 997;
            i7 = i8 + 1;
        }
        int i10 = 0;
        while (!this.n && i10 < a2) {
            a(this, q[iArr[i3] % q.length], ((fArr[i3] * ((float) i2)) + f2) - ((float) i6), f3 - 5.0f, 0.0f, (-fArr[i3 + 1]) - 1.0f, 0.0f, 0.0f, (iArr[i3 + 1] % 30) + 30, 0);
            i10++;
            i3 = (i3 + 2) % 997;
        }
        h = i3;
    }

    public final void b(float f2, float f3) {
        int a2 = a(200);
        int i2 = h;
        float[] fArr = f26d;
        float[] fArr2 = e;
        int i3 = 0;
        while (i3 < a2) {
            a(this, -16776961, f2, f3, fArr[i2] * 30.0f, fArr2[i2] * 30.0f, 0.0f, 0.0f, 100, 0);
            i3++;
            i2 = (i2 + 1) % 1000;
        }
        int i4 = 0;
        while (i4 < a2) {
            float f4 = fArr[i2];
            float f5 = fArr2[i2];
            a(this, -16776961, f2, f3, f4, f5, f4 * 2.0f, f5 * 2.0f, 100, 0);
            i4++;
            i2 = (i2 + 1) % 1000;
        }
        h = i2;
    }

    public final void a() {
        boolean z;
        boolean z2;
        int d2 = this.b.d();
        int a2 = this.a.a();
        int i2 = 0;
        while (i2 < this.l) {
            bp bpVar = this.k[i2];
            if (!bpVar.a(d2, a2)) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (z2) {
                this.l--;
                this.k[i2] = this.k[this.l];
                this.k[this.l] = bpVar;
            } else {
                i2++;
            }
        }
        int i3 = 0;
        while (i3 < this.c) {
            bp bpVar2 = this.m[i3];
            if (!bpVar2.a(d2, a2)) {
                z = true;
            } else {
                z = false;
            }
            if (z) {
                this.c--;
                this.m[i3] = this.m[this.c];
                this.m[this.c] = bpVar2;
            } else {
                i3++;
            }
        }
    }

    public final void a(Canvas canvas) {
        for (int i2 = 0; i2 < this.l; i2++) {
            this.k[i2].a(canvas);
        }
        for (int i3 = 0; i3 < this.c; i3++) {
            this.m[i3].a(canvas);
        }
    }

    public final void a(float f2, float f3, int i2, float f4, boolean z) {
        int i3;
        int a2 = a(i2);
        int i4 = h;
        float[] fArr = f;
        int[] iArr = g;
        float[] fArr2 = f26d;
        float[] fArr3 = e;
        int i5 = 0;
        while (true) {
            int i6 = i5;
            int i7 = i4;
            if (i6 < a2) {
                switch (i6 % 3) {
                    case 0:
                        i3 = -256;
                        break;
                    case 1:
                        i3 = -14336;
                        break;
                    case 2:
                        i3 = -7829368;
                        break;
                    default:
                        i3 = -256;
                        break;
                }
                float f5 = fArr[i7] * 2.0f;
                float f6 = fArr2[i7] * f5 * f4;
                float f7 = fArr3[i7] * f5;
                float f8 = f6 / 30.0f;
                float f9 = (f7 / 30.0f) - 0.05f;
                if (iArr[i7] % 1000 != 0 || !z) {
                    a(this, i3, f2, f3, f6, f7, -f8, -f9, 30, 0);
                } else {
                    float f10 = -f9;
                    if (this.c < 5) {
                        bp bpVar = this.m[this.c];
                        if (bpVar == null) {
                            bp bpVar2 = new bp();
                            this.m[this.c] = bpVar2;
                            bpVar = bpVar2;
                        }
                        this.c = this.c + 1;
                        bpVar.a(this, -22016, f2, f3, f6, f7, 0.0f, f10, 500, 2);
                    }
                }
                i4 = (i7 + 1) % 1000;
                i5 = i6 + 1;
            } else {
                h = i7;
                return;
            }
        }
    }

    public final void c(float f2, float f3, int i2) {
        int i3 = h;
        float[] fArr = f;
        int[] iArr = g;
        int i4 = i3 % 999;
        int i5 = 0;
        while (true) {
            int i6 = i5;
            int i7 = i4;
            if (i6 < 5) {
                int i8 = (iArr[i7] % 10) + 40;
                a(this, -7829368, ((fArr[i7] * 30.0f) + f2) - 15.0f, (((float) (iArr[i7 + 1] % 10)) + f3) - 5.0f, 0.0f, -((f3 - ((float) i2)) / ((float) i8)), 0.0f, 0.0f, i8, 0);
                i4 = (i7 + 2) % 999;
                i5 = i6 + 1;
            } else {
                h = i7;
                return;
            }
        }
    }

    public final void d(float f2, float f3, int i2) {
        int a2 = a(400);
        int i3 = h;
        float[] fArr = f26d;
        float[] fArr2 = e;
        float[] fArr3 = f;
        int i4 = 0;
        while (i4 < a2) {
            float f4 = 5.0f + (fArr3[i3] * 2.0f);
            float f5 = fArr[i3] * f4;
            float f6 = fArr2[i3] * f4;
            a(this, -7829368, f2, f3, f5, f6, -(f5 / ((float) (i2 / 2))), -(f6 / ((float) (i2 / 2))), i2, 0);
            i4++;
            i3 = (i3 + 1) % 1000;
        }
        h = i3;
    }

    public final void a(boolean z) {
        this.n = z;
    }

    public static void a(float f2) {
        j = f2;
    }
}
