package d;

/* compiled from: ProGuard */
public class bs extends av {
    private static boolean e = false;
    protected bh a;
    private int b;
    private long c;

    /* renamed from: d  reason: collision with root package name */
    private int f27d;
    private boolean f;
    private int g;
    private int h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, float, int, d.b]
     candidates:
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void */
    public final void a(bh bhVar, b bVar, k kVar, boolean z) {
        a(kVar, bhVar.v() + ((float) (bhVar.A() / 2)), bhVar.w() + ((float) (bhVar.B() / 15)), true, bVar);
        if (z) {
            z zVar = (z) bVar.s.a(z.class);
            zVar.a(bVar.s, bhVar, this, (float) (bhVar.A() / 2), ((float) bhVar.B()) / 2.5f, (float) this.v, (float) this.w);
            a(zVar);
        }
        this.a = bhVar;
        this.g = -1;
        this.h = -1;
        this.f = false;
        this.c = 0;
        this.b = 1;
    }

    public final boolean f() {
        boolean f2 = this.a.f();
        if (f2) {
            d(5);
        } else {
            d(30);
        }
        return f2;
    }

    public void b() {
        b((float) (-Math.atan2((double) (((float) this.g) - s()), (double) (((float) this.h) - t()))));
    }

    public final void b(int i, int i2) {
        this.g = i;
        this.h = i2;
    }

    public final void c() {
        if ((this.f27d < this.b && this.c + 200 < System.currentTimeMillis()) || e) {
            this.f27d++;
            this.c = System.currentTimeMillis();
            be beVar = (be) this.s.s.a(be.class);
            beVar.a(this.p, s(), t() + 4.0f, this.g, this.h, this.s, this, this.f);
            this.s.a(beVar);
            by.b().c();
        }
    }

    public final void e() {
        this.f27d--;
    }

    public final boolean g() {
        return this.f;
    }

    public final bt h() {
        this.f = true;
        if (!(this.f && this.b > 2)) {
            return bt.FULLY_USED;
        }
        a();
        return bt.DOWNGRADED_ROCKETS;
    }

    public final void i() {
        this.b++;
        a();
    }

    private void a() {
        if (this.f && this.b > 2) {
            this.b = 2;
        }
    }

    public final boolean j() {
        return !this.f || this.b < 2;
    }

    public final boolean k() {
        return this.b > 1;
    }

    public final void l() {
        if (!this.f) {
            throw new RuntimeException("error!");
        }
        this.f = false;
    }

    public final void H() {
        if (this.b <= 1) {
            throw new RuntimeException("too feew rockets!");
        }
        this.b--;
    }
}
