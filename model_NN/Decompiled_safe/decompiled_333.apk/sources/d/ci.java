package d;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.google.ads.R;
import dk.logisoft.airattack.AirAttackActivity;

/* compiled from: ProGuard */
final class ci implements DialogInterface.OnClickListener {
    ci() {
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        AirAttackActivity b = cg.a;
        String str = b.getResources().getString(R.string.market_url_prefix) + AirAttackActivity.class.getPackage().getName() + "full";
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.setFlags(268435456);
            b.startActivity(intent);
        } catch (Exception e) {
            Log.e("AirAttack", "Error occured going to market url: " + str, e);
        }
        dialogInterface.dismiss();
    }
}
