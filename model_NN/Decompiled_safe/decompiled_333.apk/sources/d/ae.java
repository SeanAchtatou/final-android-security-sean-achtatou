package d;

/* compiled from: ProGuard */
public abstract class ae extends av {
    private int a;
    private q[] b = {new q(), new q()};

    /* access modifiers changed from: package-private */
    public abstract void a(bh bhVar);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.q.a(float, float):void
     arg types: [int, float]
     candidates:
      d.q.a(double, double):void
      d.p.a(double, double):void
      d.q.a(float, float):void */
    public final void a(k kVar, float f, float f2, b bVar, int i, int i2) {
        b(kVar, f - ((float) (i / 2)), (f2 - ((float) i2)) + 1.0f, bVar);
        a(i, i2);
        this.r = bVar.f() * 10.0f;
        this.b[0].a(0.0f, (float) (i2 - 1));
        this.b[1].a((float) i, (float) (i2 - 1));
        a(this.b);
        this.a = 20;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005c A[LOOP:0: B:11:0x005a->B:12:0x005c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d() {
        /*
            r7 = this;
            r6 = 1073741824(0x40000000, float:2.0)
            r4 = 0
            r5 = 1
            int r0 = r7.a
            int r1 = r0 - r5
            r7.a = r1
            if (r0 <= 0) goto L_0x00b0
            boolean r0 = r7 instanceof d.aq
            if (r0 == 0) goto L_0x0069
            d.b r0 = r7.s
            java.util.List r0 = r0.f(r7)
        L_0x0016:
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x00b0
            java.lang.Object r0 = r0.get(r4)
            d.ae r0 = (d.ae) r0
            float r1 = r7.s()
            float r2 = r0.s()
            float r1 = r1 - r2
            float r2 = r7.t()
            float r0 = r0.t()
            float r0 = r2 - r0
            float r2 = d.n.a()
            r3 = 1056964608(0x3f000000, float:0.5)
            float r2 = r2 - r3
            d.q r0 = d.n.a(r1, r0)
            float r1 = r0.a
            float r1 = r1 * r6
            float r1 = r1 + r2
            int r1 = r7.d(r1)
            if (r1 != 0) goto L_0x0070
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
            r7.e(r0)
            r1 = r5
        L_0x0050:
            d.b r0 = r7.s
            java.util.List r2 = r0.c(r7)
            int r3 = r2.size()
        L_0x005a:
            if (r4 >= r3) goto L_0x0078
            java.lang.Object r0 = r2.get(r4)
            d.bh r0 = (d.bh) r0
            r7.a(r0)
            int r0 = r4 + 1
            r4 = r0
            goto L_0x005a
        L_0x0069:
            d.b r0 = r7.s
            java.util.List r0 = r0.g(r7)
            goto L_0x0016
        L_0x0070:
            float r0 = r0.b
            float r0 = r0 - r2
            r7.e(r0)
            r1 = r5
            goto L_0x0050
        L_0x0078:
            if (r1 != 0) goto L_0x0081
            int r0 = r7.e(r6)
            switch(r0) {
                case 0: goto L_0x0081;
                case 1: goto L_0x00ac;
                default: goto L_0x0081;
            }
        L_0x0081:
            float r0 = r7.s()
            r1 = 0
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00a8
            float r0 = r7.s()
            d.k r1 = r7.p
            int r1 = r1.b()
            float r1 = (float) r1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x00a8
            float r0 = r7.t()
            d.k r1 = r7.p
            int r1 = r1.a()
            float r1 = (float) r1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00ab
        L_0x00a8:
            r7.b(r5)
        L_0x00ab:
            return
        L_0x00ac:
            r7.b(r5)
            goto L_0x0081
        L_0x00b0:
            r1 = r4
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: d.ae.d():void");
    }
}
