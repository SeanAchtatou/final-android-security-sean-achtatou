package d;

import android.os.Build;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: ProGuard */
public final class bv {
    private static final String a = Long.toString(System.nanoTime(), 36);
    private static String b = "v982hjdf09hj1092hhuhwfgz6t2rghsiw3h2";
    private boolean c;

    /* renamed from: d  reason: collision with root package name */
    private final String f28d = "1";
    private final int e;
    private final int f;

    public bv(int i, int i2) {
        this.e = i;
        this.f = i2;
    }

    public static String a(String str, int i, int i2) {
        return "&map=" + a(str) + "&api=" + Build.VERSION.SDK + "&width=" + i + "&height=" + i2 + ("&sessionid=" + a + "&deviceid=" + "NA" + "&country=" + "NA" + "&gameId=" + 4) + ("&lat=NA&lon=NA" + "&glrend=NA" + "&devmanuf=" + "NA" + "&devbrand=" + a(Build.BRAND) + "&devmodel=" + a(Build.MODEL) + "&devprodu=" + a(Build.PRODUCT) + "&version=" + "NA" + "&versiontype=" + "NA");
    }

    public static String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            Log.e("AirAttack", "UTF-8 encoding not supported on this platform", e2);
            return str + "&error=UTF8_Not_Available";
        }
    }

    static String b(String str) {
        byte[] bArr;
        try {
            byte[] bytes = (str + b).getBytes("UTF-8");
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.reset();
                instance.update(bytes);
                bArr = instance.digest();
            } catch (NoSuchAlgorithmException e2) {
                e2.printStackTrace();
                bArr = null;
            }
            String a2 = a(bArr);
            if (a2.length() == 32) {
                return "&checksum=" + a2;
            }
            throw new RuntimeException("bad checksum computation!!");
        } catch (UnsupportedEncodingException e3) {
            Log.e("AirAttack", "UTF-8 encoding not supported on this platform", e3);
            return "&error=UTF8_Not_Available";
        }
    }

    private static String a(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        String[] strArr = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        StringBuffer stringBuffer = new StringBuffer(bArr.length * 2);
        for (int i = 0; i < bArr.length; i++) {
            stringBuffer.append(strArr[(byte) (((byte) (((byte) (bArr[i] & 240)) >>> 4)) & 15)]);
            stringBuffer.append(strArr[(byte) (bArr[i] & 15)]);
        }
        return new String(stringBuffer);
    }

    public final synchronized void a() {
        this.c = true;
    }
}
