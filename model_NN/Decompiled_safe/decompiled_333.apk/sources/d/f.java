package d;

import android.content.Context;

/* compiled from: ProGuard */
final class f implements Runnable {
    private boolean a;
    private b b;
    private final int c;

    /* renamed from: d  reason: collision with root package name */
    private final int f47d;
    private final Context e;

    public f(int i, int i2, Context context) {
        this.f47d = i2;
        this.c = i;
        this.e = context;
    }

    public final void run() {
        this.b = new b(this.c, this.f47d, this.e);
        synchronized (this) {
            this.a = true;
            notifyAll();
        }
    }

    public final synchronized b a() {
        while (!this.a) {
            try {
                wait();
            } catch (InterruptedException e2) {
            }
        }
        return this.b;
    }
}
