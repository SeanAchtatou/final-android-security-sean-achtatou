package d;

import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;
import com.google.ads.R;
import dk.logisoft.airattack.AirAttackActivity;

/* compiled from: ProGuard */
public final class da {
    private static final int[] a = {R.string.help_title_move, R.string.help_title_jump, R.string.help_title_fire, R.string.help_title_dig, R.string.help_title_hints};
    private static final int[] b = {R.string.help_text_move, R.string.help_text_jump, R.string.help_text_fire, R.string.help_text_dig, R.string.help_text_hints};
    /* access modifiers changed from: private */
    public static final int[] c = {R.drawable.move, R.drawable.jump, R.drawable.fire, R.drawable.dig, R.drawable.background};
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static int f39d = 0;
    private final AirAttackActivity e;
    private final ViewStub f;
    private final Resources g;
    /* access modifiers changed from: private */
    public final cn h;
    /* access modifiers changed from: private */
    public View i;
    private TextView j;
    private TextView k;
    private View l;
    private View m;
    private View n;

    static /* synthetic */ int e() {
        int i2 = f39d + 1;
        f39d = i2;
        return i2;
    }

    static /* synthetic */ int f() {
        int i2 = f39d - 1;
        f39d = i2;
        return i2;
    }

    public da(AirAttackActivity airAttackActivity, ViewStub viewStub, Resources resources, cn cnVar) {
        this.e = airAttackActivity;
        this.f = viewStub;
        this.g = resources;
        this.h = cnVar;
    }

    public final void a() {
        this.e.runOnUiThread(new db(this));
    }

    public final void b() {
        cc.b(R.string.prefKeyShowHelpPagesDialogueHasBeenShown);
        if (this.i != null) {
            this.i.setVisibility(0);
            a(f39d);
        } else {
            this.i = this.f.inflate();
            this.j = (TextView) this.i.findViewById(R.id.helpTitle);
            this.k = (TextView) this.i.findViewById(R.id.helpText);
            this.n = this.i.findViewById(R.id.btnHelpOk);
            this.m = this.i.findViewById(R.id.btnHelpNext);
            this.l = this.i.findViewById(R.id.btnHelpPrev);
            this.i.findViewById(R.id.btnHelpPrev).setVisibility(4);
            dc dcVar = new dc(this);
            this.n.setOnClickListener(dcVar);
            this.m.setOnClickListener(dcVar);
            this.l.setOnClickListener(dcVar);
            a(f39d);
        }
        this.i.invalidate();
        this.i.setClickable(true);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.i.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeResource(this.g, c[i2])));
        this.j.setText(a[i2]);
        this.k.setText(b[i2]);
        if (i2 == c.length - 1) {
            this.m.setVisibility(4);
        } else {
            this.m.setVisibility(0);
        }
        if (i2 == 0) {
            this.l.setVisibility(4);
        } else {
            this.l.setVisibility(0);
        }
    }
}
