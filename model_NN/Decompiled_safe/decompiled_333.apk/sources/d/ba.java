package d;

/* compiled from: ProGuard */
public class ba extends bh {
    private float F;
    private float G;
    private int H;
    private boolean I;
    public boolean a;
    protected int b;
    protected int c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f22d;
    private bs k;
    private boolean l;
    private boolean m;
    private float n;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.bh.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, int, int, d.b]
     candidates:
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.bh.a(d.k, float, float, boolean, d.b):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.bs.a(d.bh, d.b, d.k, boolean):void
     arg types: [d.ba, d.b, d.k, int]
     candidates:
      d.av.a(d.q[], d.q[], float, float):void
      d.bs.a(d.bh, d.b, d.k, boolean):void */
    public final void a(k kVar, int i, b bVar) {
        a(kVar, (float) i, 1.0f, false, bVar);
        this.a = true;
        this.y = false;
        this.r = 2.6666667f;
        bs bsVar = (bs) bVar.s.a(bs.class);
        bsVar.a((bh) this, bVar, this.p, true);
        this.k = bsVar;
        this.F = 1.0f;
        this.c = 2;
        this.b = this.c;
        this.f22d = false;
    }

    public final bs a() {
        return this.k;
    }

    public final void c() {
        this.c = Integer.MAX_VALUE;
    }

    public final boolean e() {
        return this.c != Integer.MAX_VALUE;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void d() {
        super.d();
        if (this.f22d && this.j) {
            this.k.c();
            this.f22d = false;
        }
        if (this.m) {
            int e = e(this.F);
            if ((c(this.n) == 2 || e == 2) && (e == 2 || this.F <= 0.0f)) {
                float f = this.n * this.n * 0.05f;
                if (this.n < 0.0f) {
                    this.n = f + this.n;
                } else {
                    this.n -= f;
                }
                float f2 = this.F * this.F * 0.05f;
                if (this.F < 0.0f) {
                    this.F = f2 + this.F;
                } else {
                    this.F -= f2;
                }
                this.F = (e == 2 ? 0.2f : 0.1f) + this.F;
                return;
            }
            this.m = false;
        } else if (this.j) {
            this.F = Math.min(2.0f, this.F + 0.2f);
            switch (e(this.F)) {
                case 0:
                    this.b = this.c;
                    this.F = 1.0f;
                    break;
                case 1:
                    if (w() >= ((float) this.p.a())) {
                        k();
                        return;
                    }
                    break;
            }
            if (this.G != 0.0f) {
                c(this.G);
                if (this.I) {
                    this.H--;
                    if (this.H == 0) {
                        this.G = 0.0f;
                        this.I = false;
                    }
                }
            }
        }
    }

    public final void b(int i, int i2) {
        this.l = ((float) i) < s();
        this.k.b(i, i2);
        this.f22d = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(float, float, boolean):void
     arg types: [float, int, int]
     candidates:
      d.ba.a(d.k, int, d.b):void
      d.av.a(float, float, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void h() {
        /*
            r11 = this;
            r10 = 100
            r3 = 2
            r9 = 1
            r8 = 0
            r7 = 0
            d.b r0 = r11.s
            int r1 = r0.n
            int r1 = r1 - r9
            r0.n = r1
            if (r1 <= 0) goto L_0x00b5
            r0 = 0
            r1 = r0
            r0 = r8
        L_0x0012:
            int r2 = r0 + 1
            if (r0 >= r10) goto L_0x0071
            if (r1 != 0) goto L_0x0071
            r0 = 3
            int r0 = d.n.a(r0)
            if (r0 != 0) goto L_0x0035
            boolean r0 = r11.e()
            if (r0 != 0) goto L_0x0035
            r11.c = r3
            d.cb r0 = d.cb.a()
            r1 = 2131230804(0x7f080054, float:1.8077671E38)
            java.lang.String r0 = r0.a(r1)
            r1 = r0
            r0 = r2
            goto L_0x0012
        L_0x0035:
            int r0 = d.n.a(r3)
            if (r0 != 0) goto L_0x0056
            d.bs r0 = r11.k
            boolean r0 = r0.g()
            if (r0 == 0) goto L_0x0056
            d.bs r0 = r11.k
            r0.l()
            d.cb r0 = d.cb.a()
            r1 = 2131230805(0x7f080055, float:1.8077673E38)
            java.lang.String r0 = r0.a(r1)
            r1 = r0
            r0 = r2
            goto L_0x0012
        L_0x0056:
            d.bs r0 = r11.k
            boolean r0 = r0.k()
            if (r0 == 0) goto L_0x00b9
            d.bs r0 = r11.k
            r0.H()
            d.cb r0 = d.cb.a()
            r1 = 2131230803(0x7f080053, float:1.807767E38)
            java.lang.String r0 = r0.a(r1)
        L_0x006e:
            r1 = r0
            r0 = r2
            goto L_0x0012
        L_0x0071:
            if (r1 == 0) goto L_0x0095
            d.b r0 = r11.s
            d.ed r0 = r0.s
            java.lang.Class<d.ao> r2 = d.ao.class
            java.lang.Object r0 = r0.a(r2)
            d.ao r0 = (d.ao) r0
            float r2 = r11.s()
            float r3 = r11.w()
            d.k r4 = r11.p
            d.b r5 = r11.s
            r6 = 12
            r0.a(r1, r2, r3, r4, r5, r6)
            d.b r1 = r11.s
            r1.a(r0)
        L_0x0095:
            r11.b(r7)
            d.k r0 = r11.p
            int r0 = r0.b()
            int r0 = r0 / 2
            float r0 = (float) r0
            r11.a(r0, r7, r9)
            r11.i = r10
            r0 = 80
            r11.e = r0
            int r0 = r11.c
            r11.b = r0
            r0 = 1065353216(0x3f800000, float:1.0)
            r11.F = r0
            r11.m = r8
        L_0x00b4:
            return
        L_0x00b5:
            super.h()
            goto L_0x00b4
        L_0x00b9:
            r0 = r1
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: d.ba.h():void");
    }

    public final boolean f() {
        return this.l;
    }

    public final void a(int i) {
        H();
        if (((float) i) < s()) {
            this.l = true;
            this.G = -1.2f;
            return;
        }
        this.l = false;
        this.G = 1.2f;
    }

    public final void i() {
        this.G = 0.0f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void H() {
        this.G = 0.0f;
        if (this.m) {
            this.m = false;
            this.F = Math.max(this.F, -2.25f);
        }
        this.I = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void a(float f, float f2) {
        float min;
        if (this.b > 0) {
            H();
            this.l = f < 0.0f;
            this.m = true;
            this.b--;
            this.F = f2;
            this.n = f;
            if (f == 0.0f) {
                min = 3.0f;
            } else {
                min = Math.min(Math.abs(f2 / f), 3.0f);
            }
            float f3 = (min / 4.0f) + 1.0f;
            if (this.b == 0) {
                f((f3 * 2.6f) - 0.8f);
            } else {
                f(f3 + 2.6f);
            }
        }
    }

    public final void b(float f, float f2) {
        boolean z;
        if (this.b > 0 && f2 != 0.0f) {
            a(f, f2);
        } else if (f != 0.0f) {
            this.m = false;
            H();
            if (f < 0.0f) {
                z = true;
            } else {
                z = false;
            }
            this.l = z;
            this.G = this.l ? -1.2f : 1.2f;
            this.H = 15;
            this.I = true;
        }
    }

    private void f(float f) {
        float sqrt = (float) Math.sqrt((double) ((this.n * this.n) + (this.F * this.F)));
        if (sqrt != 0.0f) {
            float f2 = f / sqrt;
            this.n *= f2;
            this.F = f2 * this.F;
        }
    }
}
