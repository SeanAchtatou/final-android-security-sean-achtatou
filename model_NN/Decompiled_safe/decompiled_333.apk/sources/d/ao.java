package d;

import android.graphics.Canvas;
import java.util.HashSet;

/* compiled from: ProGuard */
public class ao extends av {
    private static HashSet e = new HashSet();
    int a;
    int b;
    private float c;

    /* renamed from: d  reason: collision with root package name */
    private String f16d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, float, int, d.b]
     candidates:
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void */
    public final void a(String str, float f, float f2, k kVar, b bVar, int i) {
        a(kVar, f, f2, true, bVar);
        this.z = true;
        this.f16d = str;
        this.r = this.s.f() * 10.0f;
        this.b = i;
        this.a = 100;
        this.c = -1.0f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(float, boolean):int
     arg types: [float, int]
     candidates:
      d.av.a(int, int):void
      d.av.a(float, boolean):int */
    /* access modifiers changed from: protected */
    public final void d() {
        a(this.c, true);
        int i = this.a;
        this.a = i - 1;
        if (i <= 0) {
            b(true);
        }
    }

    public final void a(Canvas canvas) {
        ea.a(canvas, this.f16d, this.b, (int) u(), (int) s());
    }

    public final boolean a() {
        return this.a <= 0;
    }

    public static void a(String str, float f, float f2, b bVar) {
        if (!e.contains(str)) {
            ao aoVar = (ao) bVar.s.a(ao.class);
            aoVar.a(str, f, f2, bVar.c, bVar, 12);
            bVar.a(aoVar);
            e.add(str);
        }
    }
}
