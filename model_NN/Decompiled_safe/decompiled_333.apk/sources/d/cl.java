package d;

import android.content.DialogInterface;
import dk.logisoft.airattack.AirAttackActivity;

/* compiled from: ProGuard */
final class cl implements DialogInterface.OnClickListener {
    final /* synthetic */ AirAttackActivity a;
    final /* synthetic */ boolean b;

    cl(AirAttackActivity airAttackActivity, boolean z) {
        this.a = airAttackActivity;
        this.b = z;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.a();
        if (!this.b) {
            this.a.finish();
        }
    }
}
