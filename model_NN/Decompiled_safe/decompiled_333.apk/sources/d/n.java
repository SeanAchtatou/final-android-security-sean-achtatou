package d;

import android.graphics.BitmapFactory;

/* compiled from: ProGuard */
public final class n {
    public static final boolean a = b();
    public static o b = new o();
    private static final n c = new n();

    private n() {
    }

    public static int[] a(int[] iArr) {
        o oVar = b;
        for (int length = iArr.length - 1; length > 0; length--) {
            int nextInt = oVar.nextInt(length + 1);
            int i = iArr[length];
            iArr[length] = iArr[nextInt];
            iArr[nextInt] = i;
        }
        return iArr;
    }

    public static int a(int i) {
        try {
            return b.nextInt(i);
        } catch (Exception e) {
            throw new RuntimeException("parameter: " + i, e);
        }
    }

    public static float a() {
        return b.nextFloat();
    }

    public static q a(float f, float f2) {
        float sqrt = (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
        if (sqrt == 0.0f) {
            return new q(0.0f, 0.0f);
        }
        float f3 = 2.0f / sqrt;
        return new q(f3 * f, f3 * f2);
    }

    public static int b(int i) {
        return b.nextInt(i) - (i / 2);
    }

    private static boolean b() {
        try {
            BitmapFactory.Options.class.getField("inScaled");
            return true;
        } catch (SecurityException e) {
            return false;
        } catch (NoSuchFieldException e2) {
            return false;
        }
    }
}
