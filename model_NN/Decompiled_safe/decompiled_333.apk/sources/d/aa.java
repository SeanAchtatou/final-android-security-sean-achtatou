package d;

import android.graphics.Canvas;

/* compiled from: ProGuard */
public abstract class aa extends av {
    private static final String[] j = new String[10];
    protected boolean a;
    private boolean b;
    private long c;

    /* renamed from: d  reason: collision with root package name */
    private int f13d;
    private float e;
    private q f = new q();
    private q g = new q();
    private q[] h = new q[2];
    private int[] i = new int[1];

    /* access modifiers changed from: package-private */
    public abstract void a();

    static {
        for (int i2 = 0; i2 < j.length; i2++) {
            j[i2] = "" + i2;
        }
    }

    public void a(k kVar, int i2, int i3, b bVar) {
        a(kVar, i2, i3, bVar, 1.0f, n.a(80) == 0);
    }

    public final void a(k kVar, int i2, int i3, b bVar, float f2) {
        a(kVar, i2, i3, bVar, f2, n.a(80) == 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, float, int, d.b]
     candidates:
      d.aa.a(d.k, int, int, d.b, float):void
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.q.a(float, float):void
     arg types: [float, int]
     candidates:
      d.q.a(double, double):void
      d.p.a(double, double):void
      d.q.a(float, float):void */
    private void a(k kVar, int i2, int i3, b bVar, float f2, boolean z) {
        a(kVar, (float) i2, (float) i3, true, bVar);
        this.e = f2;
        this.r = bVar.f() * 30.0f;
        this.a = z;
        this.b = false;
        this.f13d = 2;
        a(-1.5f);
        this.f.a((float) this.v, (float) this.u);
        this.g.a((float) this.v, 0.0f);
        this.h[0] = this.f;
        this.h[1] = this.g;
        a(this.h);
        if (!y()) {
            a(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(float, boolean):int
     arg types: [int, int]
     candidates:
      d.av.a(int, int):void
      d.av.a(float, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(float, boolean):int
     arg types: [float, int]
     candidates:
      d.av.a(int, int):void
      d.av.a(float, boolean):int */
    public void d() {
        if (this.a) {
            if (this.b) {
                long currentTimeMillis = 2 - ((System.currentTimeMillis() - this.c) / 1000);
                if (currentTimeMillis != ((long) this.f13d)) {
                    if (currentTimeMillis <= -1) {
                        a(false);
                    } else {
                        this.f13d = (int) currentTimeMillis;
                        by.b().p();
                    }
                }
            }
            if (this.s.i(this) != null && !this.b) {
                this.b = true;
                this.c = System.currentTimeMillis();
                by.b().p();
            }
            switch (a(1.0f, false)) {
                case 0:
                    return;
                case 1:
                    b(true);
                    break;
            }
            a(0.1f);
            return;
        }
        a(0.1f);
        switch (a(this.e, false)) {
            case 0:
                a(true);
                break;
            case 1:
                if (w() > 0.0f) {
                    b(true);
                    break;
                }
                break;
        }
        bh i2 = this.s.i(this);
        if (i2 != null) {
            i2.g();
            a(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (z) {
            this.i[0] = this.p.c(this.C, this.D);
            this.s.r.a(s(), t(), this.i, 25);
        } else {
            this.s.r.a(s(), t(), this.o, 25);
        }
        this.s.c.a((int) s(), (int) t(), 25);
        b(true);
        this.s.a((int) s(), (int) t(), 25);
        by.b().j();
    }

    public final boolean b_() {
        return this.a;
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
        if (this.b) {
            ea.a(canvas, j[this.f13d], 12, ((int) w()) - 10, (int) s());
        }
    }
}
