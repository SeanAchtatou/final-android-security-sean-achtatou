package d;

import com.google.ads.R;

/* compiled from: ProGuard */
public class at extends aa {
    final int b = 1;
    private boolean c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.aa.a(d.k, int, int, d.b, float):void
     arg types: [d.k, int, int, d.b, int]
     candidates:
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void
      d.aa.a(d.k, int, int, d.b, float):void */
    public final void a(k kVar, int i, int i2, b bVar) {
        a(kVar, i, i2, bVar, 0.8f);
        this.a = false;
        this.c = false;
        this.r = bVar.f() * 18.0f;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        b(true);
        au auVar = (au) this.s.s.a(au.class);
        auVar.a(this.s.c, s(), t(), this.s);
        this.s.a(auVar);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.s.r.a(s(), t(), this.o, 20);
        by.b().o();
        if (n.a(1) == 0) {
            this.c = true;
            this.a = true;
            ao.a(cb.a().a((int) R.string.in_game_disarmed_get_the_warhead), s(), t(), this.s);
            return;
        }
        b(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.bi.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, float, int, d.b]
     candidates:
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.bi.a(d.k, float, float, boolean, d.b):void */
    public final void d() {
        super.d();
        if (this.c && this.s.h(this) != null) {
            ao aoVar = (ao) this.s.s.a(ao.class);
            aoVar.a(cb.a().a((int) R.string.in_game_warhead_traded_on_black_market), s(), t(), this.p, this.s, 12);
            this.s.a(aoVar);
            bi biVar = (bi) this.s.s.a(bi.class);
            biVar.a(this.p, s(), t() - 250.0f, true, this.s);
            this.s.a(biVar);
            by.b().q();
            b(true);
        }
    }
}
