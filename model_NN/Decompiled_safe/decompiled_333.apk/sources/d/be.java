package d;

import java.util.List;

/* compiled from: ProGuard */
public class be extends bc {
    int a;
    bs b;
    boolean i;

    public final void a(k kVar, float f, float f2, int i2, int i3, b bVar, bs bsVar, boolean z) {
        cb.a().c();
        a(kVar, f, f2, (float) i2, (float) i3, bVar, 10, 1.0666667f, 3);
        this.b = bsVar;
        this.i = z;
        this.a = kVar.b();
    }

    public final void d() {
        super.d();
        List d2 = this.s.d(this);
        int size = d2.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                break;
            }
            ((y) d2.get(i2)).a(this);
            if (!this.i) {
                by.b().d();
                b(true);
                break;
            }
            i2++;
        }
        List c = this.s.c(this);
        int size2 = c.size();
        int i3 = 0;
        while (true) {
            if (i3 >= size2) {
                break;
            }
            bh bhVar = (bh) c.get(i3);
            if (bhVar instanceof ai) {
                ai aiVar = (ai) bhVar;
                if (!aiVar.g) {
                    aiVar.g();
                    if (!this.i) {
                        by.b().d();
                        b(true);
                        break;
                    }
                } else {
                    continue;
                }
            }
            i3++;
        }
        List e = this.s.e(this);
        int size3 = e.size();
        int i4 = 0;
        while (true) {
            if (i4 >= size3) {
                break;
            }
            ((aa) e.get(i4)).a();
            if (!this.i) {
                b(true);
                break;
            }
            i4++;
        }
        q r = r();
        this.s.r.a(r.a, r.b, -this.f23d, -this.e);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        switch (i2) {
            case 0:
                a();
                return;
            case 1:
                int s = (int) s();
                int t = (int) t();
                if (s < -50 || s > this.a + 50 || t < 0) {
                    b(true);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void b(boolean z) {
        if (!C()) {
            super.b(z);
            this.b.e();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        aa b2 = this.s.b((int) v(), (int) w(), this.c);
        if (b2 != null && b2.b_()) {
            b2.a(true);
        }
        super.a();
    }
}
