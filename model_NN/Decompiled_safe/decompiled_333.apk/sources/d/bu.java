package d;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/* compiled from: ProGuard */
public final class bu extends ef {
    private final bv a;
    private final URL b;
    private final int c;

    public bu(bv bvVar, URL url, int i) {
        if (i == 0 || i == 2 || i == 1) {
            this.a = bvVar;
            this.b = url;
            this.c = i;
            return;
        }
        throw new IllegalArgumentException("received type: " + i);
    }

    public final void a() {
        try {
            a(this.b);
        } catch (IOException e) {
            this.a.a();
        } catch (Exception e2) {
            this.a.a();
            Log.e("AirAttack", "Url: " + this.b, e2);
        }
    }

    public static StringBuffer a(URL url) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()), 1024);
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    return stringBuffer;
                }
                stringBuffer.append(readLine);
                stringBuffer.append("\n");
            } finally {
                bufferedReader.close();
            }
        }
    }
}
