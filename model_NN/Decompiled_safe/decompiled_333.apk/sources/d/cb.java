package d;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.google.ads.R;
import java.lang.reflect.Array;
import java.util.HashMap;

/* compiled from: ProGuard */
public final class cb {
    public static final Bitmap.Config n = Bitmap.Config.ARGB_8888;
    public static final Bitmap.Config o = Bitmap.Config.RGB_565;
    private static cb p = null;
    private Bitmap A;
    private Bitmap B;
    private Bitmap C;
    private Bitmap D;
    private Bitmap E;
    private Bitmap F;
    private Bitmap G;
    private Bitmap H;
    private Bitmap I;
    private Bitmap J;
    private Bitmap K;
    private Bitmap L;
    private Bitmap M;
    private Bitmap N;
    private Bitmap O;
    private Bitmap P;
    private Bitmap Q;
    private Bitmap R;
    private Bitmap S;
    private Bitmap T;
    private Bitmap U;
    private Bitmap V;
    private Bitmap W;
    private Bitmap X;
    private Bitmap Y;
    private Bitmap Z;
    public int[] a;
    private Bitmap aa;
    private Bitmap ab;
    private Bitmap ac;
    private Bitmap ad;
    private Bitmap ae;
    private Bitmap af;
    private Bitmap ag;
    private Bitmap ah;
    private Bitmap ai;
    private Bitmap aj;
    private Bitmap ak;
    private Bitmap al;
    private Bitmap am;
    private Bitmap an;
    private Bitmap ao;
    private Bitmap ap;
    private Bitmap aq;
    private Bitmap ar;
    private Bitmap as;
    private Bitmap at;
    private Resources au;
    private BitmapFactory.Options av = new BitmapFactory.Options();
    private int aw;
    private Bitmap[][] ax = ((Bitmap[][]) Array.newInstance(Bitmap.class, 4, 7));
    private int ay = 10;
    private HashMap az = new HashMap();
    public int[] b;
    public int[] c;

    /* renamed from: d  reason: collision with root package name */
    public int f33d = -1;
    public int e;
    public int f;
    public int g;
    public int h = 0;
    public int i = 6;
    public String j;
    public int k;
    public String l;
    public String m;
    private Bitmap q;
    private Bitmap r;
    private Bitmap s;
    private Bitmap t;
    private Bitmap u;
    private Bitmap v;
    private Bitmap w;
    private Bitmap x;
    private Bitmap y;
    private Bitmap z;

    private static Bitmap a(Drawable drawable) {
        Bitmap.Config config = n;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return createBitmap;
    }

    private cb(Context context) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        this.au = context.getResources();
        if (n.a) {
            try {
                this.av.getClass().getField("inScaled").set(this.av, new Boolean(false));
            } catch (SecurityException e2) {
                Log.e(getClass().getSimpleName(), "Unable to get field inTargetDensity on BitmapFactory.Options", e2);
            } catch (NoSuchFieldException e3) {
                Log.e(getClass().getSimpleName(), "Unable to get field inTargetDensity on BitmapFactory.Options", e3);
            } catch (IllegalArgumentException e4) {
                Log.e(getClass().getSimpleName(), "Unable to set field value on inTargetDensity on BitmapFactory.Options", e4);
            } catch (IllegalAccessException e5) {
                Log.e(getClass().getSimpleName(), "Unable to set field value on inTargetDensity on BitmapFactory.Options", e5);
            }
        }
        this.aw = j.c(context);
        this.m = this.au.getString(R.string.onlinePropertyFileName);
        this.a = this.au.getIntArray(R.array.grassColors);
        this.b = this.au.getIntArray(R.array.snowColors);
        this.c = this.au.getIntArray(R.array.houseFoundationColors);
        this.e = this.au.getInteger(R.integer.crashOnHitProbability);
        this.f = this.au.getInteger(R.integer.pilotEjectProbability);
        this.g = this.au.getInteger(R.integer.hostilePilotProbability);
        String string = this.au.getString(R.string.version);
        this.j = string;
        a(string);
        int integer = this.au.getInteger(R.integer.versionCode);
        this.k = integer;
        if (integer == 0) {
            throw new RuntimeException("Util initialization error");
        }
        String string2 = this.au.getString(R.string.versionType);
        this.l = string2;
        a(string2);
        a(this.a.length != 0);
        if (this.b.length != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        a(z2);
        if (this.c.length != 0) {
            z3 = true;
        } else {
            z3 = false;
        }
        a(z3);
        if (this.e != 0) {
            z4 = true;
        } else {
            z4 = false;
        }
        a(z4);
        if (this.f != 0) {
            z5 = true;
        } else {
            z5 = false;
        }
        a(z5);
        if (this.g != 0) {
            z6 = true;
        } else {
            z6 = false;
        }
        a(z6);
        if (this.m != null) {
            z7 = true;
        } else {
            z7 = false;
        }
        a(z7);
        if (this.j != null) {
            z8 = true;
        } else {
            z8 = false;
        }
        a(z8);
    }

    private static void a(boolean z2) {
        if (!z2) {
            throw new RuntimeException("Util initialization error");
        }
    }

    private static void a(Object obj) {
        if (obj == null) {
            throw new RuntimeException("Util initialization error");
        }
    }

    public static void a(Context context) {
        if (p == null) {
            p = new cb(context);
            return;
        }
        p.au = context.getResources();
    }

    public static cb a() {
        return p;
    }

    private static Bitmap b(Bitmap bitmap) {
        return Bitmap.createScaledBitmap(bitmap, (int) (((double) bitmap.getWidth()) * 0.4d), (int) (((double) bitmap.getHeight()) * 0.4d), true);
    }

    public final Bitmap a(BitmapFactory.Options options) {
        return BitmapFactory.decodeResource(this.au, R.drawable.own_ad_image, options);
    }

    private Bitmap c(int i2) {
        if (n.a) {
            return BitmapFactory.decodeResource(this.au, i2, this.av);
        }
        return BitmapFactory.decodeResource(this.au, i2);
    }

    private Bitmap d(int i2) {
        return BitmapFactory.decodeResource(this.au, i2);
    }

    public final Bitmap b() {
        if (this.u == null) {
            this.u = c(R.drawable.bomberplane);
        }
        return this.u;
    }

    public final Bitmap c() {
        if (this.q == null) {
            this.q = c(R.drawable.rocket);
        }
        return this.q;
    }

    public final Bitmap a(int i2, int i3) {
        Bitmap a2;
        if (this.ax[i2][i3] == null) {
            if (this.r == null) {
                this.r = c(R.drawable.soldier1);
            }
            Bitmap bitmap = this.r;
            int width = bitmap.getWidth() / 2;
            int height = bitmap.getHeight();
            switch (i3) {
                case 0:
                    a2 = a(bitmap, width * i2, width, height);
                    break;
                case 1:
                    if (this.s == null) {
                        this.s = c(R.drawable.soldier2);
                    }
                    a2 = a(this.s, width * i2, width, height);
                    break;
                case 2:
                    if (this.N == null) {
                        this.N = c(R.drawable.paratrooper);
                    }
                    a2 = a(this.N, width * i2, width, height);
                    break;
                case 3:
                    if (this.M == null) {
                        this.M = c(R.drawable.enemypilot_injured);
                    }
                    a2 = a(this.M, width * i2, width, height);
                    break;
                case 4:
                    if (this.G == null) {
                        this.G = c(R.drawable.rocketchopper);
                    }
                    Bitmap bitmap2 = this.G;
                    int width2 = bitmap2.getWidth() / 4;
                    a2 = a(bitmap2, width2 * i2, width2, bitmap2.getHeight());
                    break;
                case 5:
                    if (this.H == null) {
                        this.H = c(R.drawable.bulletchopper);
                    }
                    Bitmap bitmap3 = this.H;
                    int width3 = bitmap3.getWidth() / 4;
                    a2 = a(bitmap3, width3 * i2, width3, bitmap3.getHeight());
                    break;
                case 6:
                    if (this.U == null) {
                        this.U = c(R.drawable.paragunner);
                    }
                    a2 = a(this.U, width * i2, width, height);
                    break;
                default:
                    a2 = null;
                    break;
            }
            this.ax[i2][i3] = a2;
        }
        return this.ax[i2][i3];
    }

    private static Bitmap a(Bitmap bitmap, int i2, int i3, int i4) {
        int[] iArr = new int[(i3 * i4)];
        bitmap.getPixels(iArr, 0, i3, i2, 0, i3, i4);
        return Bitmap.createBitmap(iArr, i3, i4, Bitmap.Config.ARGB_4444);
    }

    public final Bitmap b(int i2, int i3) {
        if (!(this.w != null && this.w.getWidth() == i2 && this.w.getHeight() == i3)) {
            this.w = Bitmap.createScaledBitmap(a(this.au.getDrawable(R.drawable.background)), i2, i3, true);
        }
        return this.w;
    }

    public final Bitmap c(int i2, int i3) {
        if (!(this.v != null && this.v.getWidth() == i2 && this.v.getHeight() == i3)) {
            this.v = Bitmap.createScaledBitmap(a(this.au.getDrawable(R.drawable.groundtexture)), i2, i3, true);
        }
        return this.v;
    }

    public final Bitmap d() {
        switch (n.a(8)) {
            case 0:
                if (this.x == null) {
                    this.x = c(R.drawable.tree1);
                }
                return this.x;
            case 1:
                if (this.y == null) {
                    this.y = c(R.drawable.tree2);
                }
                return this.y;
            case 2:
                if (this.z == null) {
                    this.z = c(R.drawable.tree3);
                }
                return this.z;
            case 3:
                if (this.A == null) {
                    this.A = c(R.drawable.stump1);
                }
                return this.A;
            case 4:
                if (this.B == null) {
                    this.B = c(R.drawable.stump2);
                }
                return this.B;
            case 5:
                if (this.C == null) {
                    this.C = c(R.drawable.stump3);
                }
                return this.C;
            case 6:
                if (this.D == null) {
                    this.D = c(R.drawable.bush1);
                }
                return this.D;
            case 7:
                if (this.E == null) {
                    this.E = c(R.drawable.bush2);
                }
                return this.E;
            default:
                throw new RuntimeException("Invalid number of items in switch statement");
        }
    }

    public final Bitmap e() {
        if (this.I == null) {
            this.I = c(R.drawable.bullet);
        }
        return this.I;
    }

    public final Bitmap f() {
        if (this.J == null) {
            this.J = c(R.drawable.napalmbomber);
        }
        return this.J;
    }

    private Bitmap I() {
        if (this.L == null) {
            this.L = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_4444);
        }
        return this.L;
    }

    public final Bitmap g() {
        if (this.O == null) {
            this.O = c(R.drawable.parachute);
        }
        return this.O;
    }

    public final int[] a(Bitmap bitmap) {
        if (!this.az.containsKey(bitmap)) {
            int[] iArr = new int[this.ay];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                int i3 = 0;
                int i4 = 0;
                while ((-16777216 & i4) == 0) {
                    i4 = bitmap.getPixel(n.a(bitmap.getWidth()), n.a(bitmap.getHeight()));
                    int i5 = i3 + 1;
                    if (i3 > 10) {
                        i4 = -1;
                        i3 = i5;
                    } else {
                        i3 = i5;
                    }
                }
                iArr[i2] = i4;
            }
            this.az.put(bitmap, iArr);
        }
        return (int[]) this.az.get(bitmap);
    }

    public final Bitmap h() {
        if (this.Z == null) {
            this.Z = d(R.drawable.button_terrain);
        }
        return this.Z;
    }

    public final Bitmap i() {
        if (this.V == null) {
            this.V = d(R.drawable.button_easy);
        }
        return this.V;
    }

    public final Bitmap j() {
        if (this.W == null) {
            this.W = d(R.drawable.button_hard);
        }
        return this.W;
    }

    public final Bitmap k() {
        if (this.X == null) {
            this.X = d(R.drawable.button_controls);
        }
        return this.X;
    }

    public final Bitmap l() {
        if (this.Y == null) {
            this.Y = d(R.drawable.button_prev);
        }
        return this.Y;
    }

    public final Bitmap m() {
        if (this.ar == null) {
            this.ar = d(R.drawable.button_bottomless);
        }
        return this.ar;
    }

    public final Bitmap n() {
        if (this.as == null) {
            this.as = d(R.drawable.button_play);
        }
        return this.as;
    }

    public final Bitmap o() {
        if (this.at == null) {
            this.at = d(R.drawable.button_market);
        }
        return this.at;
    }

    public final Bitmap p() {
        if (this.ak == null) {
            this.ak = d(R.drawable.button_play);
        }
        return this.ak;
    }

    public final Bitmap q() {
        if (this.aa == null) {
            this.aa = d(R.drawable.button_exit);
        }
        return this.aa;
    }

    public final Bitmap r() {
        if (this.ab == null) {
            this.ab = d(R.drawable.button_next);
        }
        return this.ab;
    }

    public final Bitmap s() {
        if (this.P == null) {
            this.P = c(R.drawable.crashingplane);
        }
        return this.P;
    }

    public final Bitmap t() {
        if (this.ad == null) {
            this.ad = d(R.drawable.particletoggle);
        }
        return this.ad;
    }

    public final Bitmap u() {
        if (this.ae == null) {
            this.ae = d(R.drawable.musictoggle);
        }
        return this.ae;
    }

    public final Bitmap v() {
        if (this.Q == null) {
            this.Q = c(R.drawable.clusterbomber);
        }
        return this.Q;
    }

    public final Bitmap w() {
        if (this.ap == null) {
            this.ap = c(R.drawable.supplyplane);
        }
        return this.ap;
    }

    public final Bitmap x() {
        if (this.S == null) {
            this.S = c(R.drawable.fragment);
        }
        return this.S;
    }

    public final Bitmap y() {
        if (this.T == null) {
            this.T = c(R.drawable.grenade);
        }
        return this.T;
    }

    public final Bitmap z() {
        if (this.ac == null) {
            this.ac = c(R.drawable.supplycrate);
        }
        return this.ac;
    }

    public final Bitmap A() {
        if (this.af == null) {
            this.af = c(R.drawable.house);
        }
        return this.af;
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x01ae A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.Bitmap a(d.av r5) {
        /*
            r4 = this;
            r2 = 0
            boolean r1 = r5 instanceof d.bh
            if (r1 == 0) goto L_0x01b2
            float r1 = r5.v()
            int r1 = (int) r1
            int r1 = r1 / 10
            int r1 = r1 % 2
            int r3 = java.lang.Math.abs(r1)
            boolean r1 = r5 instanceof d.ba
            if (r1 == 0) goto L_0x0178
            r0 = r5
            d.ba r0 = (d.ba) r0
            r1 = r0
            boolean r1 = r1.a
            if (r1 != 0) goto L_0x0178
            r1 = 1
            android.graphics.Bitmap r1 = r4.a(r3, r1)
        L_0x0023:
            boolean r2 = r5 instanceof d.az
            if (r2 == 0) goto L_0x002b
            android.graphics.Bitmap r1 = r4.b()
        L_0x002b:
            boolean r2 = r5 instanceof d.ag
            if (r2 == 0) goto L_0x0033
            android.graphics.Bitmap r1 = r4.v()
        L_0x0033:
            boolean r2 = r5 instanceof d.as
            if (r2 == 0) goto L_0x003b
            android.graphics.Bitmap r1 = r4.f()
        L_0x003b:
            boolean r2 = r5 instanceof d.aw
            if (r2 == 0) goto L_0x0043
            android.graphics.Bitmap r1 = r4.B()
        L_0x0043:
            boolean r2 = r5 instanceof d.bg
            if (r2 == 0) goto L_0x0059
            long r1 = java.lang.System.currentTimeMillis()
            int r1 = (int) r1
            int r1 = r1 / 25
            int r1 = r1 % 4
            int r1 = java.lang.Math.abs(r1)
            r2 = 4
            android.graphics.Bitmap r1 = r4.a(r1, r2)
        L_0x0059:
            boolean r2 = r5 instanceof d.ad
            if (r2 == 0) goto L_0x006f
            long r1 = java.lang.System.currentTimeMillis()
            int r1 = (int) r1
            int r1 = r1 / 25
            int r1 = r1 % 4
            int r1 = java.lang.Math.abs(r1)
            r2 = 5
            android.graphics.Bitmap r1 = r4.a(r1, r2)
        L_0x006f:
            boolean r2 = r5 instanceof d.ac
            if (r2 == 0) goto L_0x0077
            android.graphics.Bitmap r1 = r4.e()
        L_0x0077:
            boolean r2 = r5 instanceof d.bf
            if (r2 == 0) goto L_0x007f
            android.graphics.Bitmap r1 = r4.c()
        L_0x007f:
            boolean r2 = r5 instanceof d.be
            if (r2 == 0) goto L_0x0087
            android.graphics.Bitmap r1 = r4.c()
        L_0x0087:
            boolean r2 = r5 instanceof d.aa
            if (r2 == 0) goto L_0x009a
            android.graphics.Bitmap r1 = r4.t
            if (r1 != 0) goto L_0x0098
            r1 = 2130837509(0x7f020005, float:1.7279974E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.t = r1
        L_0x0098:
            android.graphics.Bitmap r1 = r4.t
        L_0x009a:
            boolean r2 = r5 instanceof d.at
            if (r2 == 0) goto L_0x00c6
            android.graphics.Bitmap r1 = r4.ag
            if (r1 != 0) goto L_0x00ab
            r1 = 2130837558(0x7f020036, float:1.7280073E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.ag = r1
        L_0x00ab:
            android.graphics.Bitmap r2 = r4.ag
            r0 = r5
            d.at r0 = (d.at) r0
            r1 = r0
            boolean r1 = r1.b_()
            if (r1 == 0) goto L_0x01af
            android.graphics.Bitmap r1 = r4.ah
            if (r1 != 0) goto L_0x00c4
            r1 = 2130837559(0x7f020037, float:1.7280075E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.ah = r1
        L_0x00c4:
            android.graphics.Bitmap r1 = r4.ah
        L_0x00c6:
            boolean r2 = r5 instanceof d.ar
            if (r2 == 0) goto L_0x00d9
            android.graphics.Bitmap r1 = r4.K
            if (r1 != 0) goto L_0x00d7
            r1 = 2130837553(0x7f020031, float:1.7280063E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.K = r1
        L_0x00d7:
            android.graphics.Bitmap r1 = r4.K
        L_0x00d9:
            boolean r2 = r5 instanceof d.af
            if (r2 == 0) goto L_0x00ec
            android.graphics.Bitmap r1 = r4.R
            if (r1 != 0) goto L_0x00ea
            r1 = 2130837530(0x7f02001a, float:1.7280017E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.R = r1
        L_0x00ea:
            android.graphics.Bitmap r1 = r4.R
        L_0x00ec:
            boolean r2 = r5 instanceof d.ak
            if (r2 == 0) goto L_0x00ff
            android.graphics.Bitmap r1 = r4.ai
            if (r1 != 0) goto L_0x00fd
            r1 = 2130837542(0x7f020026, float:1.7280041E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.ai = r1
        L_0x00fd:
            android.graphics.Bitmap r1 = r4.ai
        L_0x00ff:
            boolean r2 = r5 instanceof d.ax
            if (r2 == 0) goto L_0x0107
            android.graphics.Bitmap r1 = r4.g()
        L_0x0107:
            boolean r2 = r5 instanceof d.am
            if (r2 == 0) goto L_0x01a4
            android.graphics.Bitmap r1 = r4.y()
        L_0x010f:
            boolean r2 = r5 instanceof d.bs
            if (r2 == 0) goto L_0x0122
            android.graphics.Bitmap r1 = r4.F
            if (r1 != 0) goto L_0x0120
            r1 = 2130837575(0x7f020047, float:1.7280108E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.F = r1
        L_0x0120:
            android.graphics.Bitmap r1 = r4.F
        L_0x0122:
            boolean r2 = r5 instanceof d.br
            if (r2 == 0) goto L_0x0135
            android.graphics.Bitmap r1 = r4.al
            if (r1 != 0) goto L_0x0133
            r1 = 2130837564(0x7f02003c, float:1.7280086E38)
            android.graphics.Bitmap r1 = r4.c(r1)
            r4.al = r1
        L_0x0133:
            android.graphics.Bitmap r1 = r4.al
        L_0x0135:
            boolean r2 = r5 instanceof d.ae
            if (r2 == 0) goto L_0x013d
            android.graphics.Bitmap r1 = r4.I()
        L_0x013d:
            boolean r2 = r5 instanceof d.au
            if (r2 == 0) goto L_0x0145
            android.graphics.Bitmap r1 = r4.I()
        L_0x0145:
            boolean r2 = r5 instanceof d.ao
            if (r2 == 0) goto L_0x014d
            android.graphics.Bitmap r1 = r4.I()
        L_0x014d:
            boolean r2 = r5 instanceof d.bi
            if (r2 == 0) goto L_0x0155
            android.graphics.Bitmap r1 = r4.z()
        L_0x0155:
            boolean r2 = r5 instanceof d.bo
            if (r2 == 0) goto L_0x015d
            android.graphics.Bitmap r1 = r4.w()
        L_0x015d:
            if (r1 != 0) goto L_0x01ae
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "No image found for: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0178:
            boolean r1 = r5 instanceof d.ba
            if (r1 == 0) goto L_0x0183
            r1 = 0
            android.graphics.Bitmap r1 = r4.a(r3, r1)
            goto L_0x0023
        L_0x0183:
            boolean r1 = r5 instanceof d.ap
            if (r1 == 0) goto L_0x018e
            r1 = 2
            android.graphics.Bitmap r1 = r4.a(r3, r1)
            goto L_0x0023
        L_0x018e:
            boolean r1 = r5 instanceof d.ay
            if (r1 == 0) goto L_0x0199
            r1 = 6
            android.graphics.Bitmap r1 = r4.a(r3, r1)
            goto L_0x0023
        L_0x0199:
            boolean r1 = r5 instanceof d.ai
            if (r1 == 0) goto L_0x01b2
            r1 = 3
            android.graphics.Bitmap r1 = r4.a(r3, r1)
            goto L_0x0023
        L_0x01a4:
            boolean r2 = r5 instanceof d.aj
            if (r2 == 0) goto L_0x010f
            android.graphics.Bitmap r1 = r4.x()
            goto L_0x010f
        L_0x01ae:
            return r1
        L_0x01af:
            r1 = r2
            goto L_0x00c6
        L_0x01b2:
            r1 = r2
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: d.cb.a(d.av):android.graphics.Bitmap");
    }

    public final Bitmap B() {
        if (this.aj == null) {
            this.aj = c(R.drawable.paradropper);
        }
        return this.aj;
    }

    public final Bitmap C() {
        if (this.am == null) {
            this.am = c(R.drawable.redstar);
            this.am = b(this.am);
        }
        return this.am;
    }

    public final Bitmap D() {
        if (this.an == null) {
            this.an = c(R.drawable.bluestar);
            this.an = b(this.an);
        }
        return this.an;
    }

    public final Bitmap E() {
        if (this.ao == null) {
            this.ao = c(R.drawable.blackstar);
            this.ao = b(this.ao);
        }
        return this.ao;
    }

    public final Bitmap F() {
        if (this.aq == null) {
            this.aq = c(R.drawable.shovel);
        }
        return this.aq;
    }

    public final String a(int i2) {
        return this.au.getString(i2);
    }

    public final boolean b(int i2) {
        return Boolean.parseBoolean(this.au.getString(i2));
    }

    public final int G() {
        return Integer.parseInt(this.au.getString(R.string.soldierlives));
    }

    public final synchronized int H() {
        return this.aw;
    }
}
