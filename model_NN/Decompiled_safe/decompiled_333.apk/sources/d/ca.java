package d;

import android.content.Context;
import android.media.MediaPlayer;
import com.google.ads.R;

/* compiled from: ProGuard */
public final class ca {
    private static ca c;
    boolean a = false;
    boolean b = false;

    /* renamed from: d  reason: collision with root package name */
    private MediaPlayer f32d;
    private MediaPlayer e;
    private MediaPlayer f;
    private final Context g;

    private ca(Context context) {
        this.g = context;
    }

    public static synchronized void a(Context context) {
        synchronized (ca.class) {
            if (c == null) {
                c = new ca(context);
            }
        }
    }

    public static synchronized ca a() {
        ca caVar;
        synchronized (ca.class) {
            caVar = c;
        }
        return caVar;
    }

    private void a(MediaPlayer mediaPlayer) {
        h();
        this.f = mediaPlayer;
        if (this.f != null) {
            this.f.seekTo(0);
        }
    }

    private void g() {
        if (this.f != null) {
            this.f.start();
        }
    }

    private void h() {
        if (this.f != null && this.f.isPlaying()) {
            this.f.pause();
        }
    }

    public final synchronized void b() {
        if (this.b) {
            a(this.f32d);
            if (this.a) {
                g();
            }
        }
    }

    public final synchronized void c() {
        if (this.b) {
            a(this.e);
            if (this.a) {
                g();
            }
        }
    }

    public final synchronized void d() {
        if (this.b) {
            h();
            a((MediaPlayer) null);
        }
    }

    public final synchronized void a(boolean z) {
        this.a = z;
        if (!this.a) {
            h();
        } else {
            if (!this.b) {
                this.f32d = MediaPlayer.create(this.g, (int) R.raw.titlesong);
                this.f32d.setLooping(false);
                this.e = MediaPlayer.create(this.g, (int) R.raw.gameover);
                this.e.setLooping(false);
                this.f = this.f32d;
                this.b = true;
            }
            g();
        }
    }

    public final synchronized void e() {
        if (this.a) {
            h();
        }
    }

    public final synchronized void f() {
        if (this.a) {
            g();
        }
    }
}
