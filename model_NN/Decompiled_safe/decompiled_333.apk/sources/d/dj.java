package d;

import android.view.MotionEvent;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class dj {
    public static int a;
    public static int b;
    public static boolean c;

    /* renamed from: d  reason: collision with root package name */
    public static int f40d;
    public static boolean e;
    private static int[] f = new int[10];
    private static int[] g = new int[10];
    private static int[] h = new int[10];
    private static long[] i = new long[10];
    private static long j;
    private static long k;
    private static int l = -1;
    private static int m = -1;
    private static int n;
    private static long o;
    private static boolean p;
    private static boolean q;
    private static int r;
    private static int s;

    public static void a(float f2) {
        l = (int) (2500.0f * f2 * f2);
        m = (int) (1600.0f * f2 * f2);
    }

    public static void a(MotionEvent motionEvent) {
        if (n + 1 == f.length) {
            i = (long[]) ec.a(i);
            f = (int[]) ec.a(f);
            g = (int[]) ec.a(g);
            h = (int[]) ec.a(h);
        }
        i[n] = System.nanoTime();
        f[n] = motionEvent.getAction();
        g[n] = (int) motionEvent.getX();
        h[n] = (int) motionEvent.getY();
        n++;
    }

    public static void a(ArrayList arrayList) {
        if (cf.a(f, g, h, n, arrayList)) {
            n = 0;
        }
    }

    public static void a() {
        n = 0;
    }

    public static void a(d dVar) {
        ba baVar = dVar.a.o;
        if (baVar != null) {
            long nanoTime = System.nanoTime();
            if (o != Long.MAX_VALUE && o != 0 && f40d == 0 && !c) {
                if (e) {
                    if (o + 300000000 < nanoTime) {
                        c = true;
                        r = (int) baVar.s();
                        s = (int) baVar.u();
                        p = false;
                    }
                } else if (o + j < nanoTime) {
                    p = true;
                    q = true;
                    if (((float) a) < baVar.s()) {
                        f40d = -10000;
                    } else {
                        f40d = 10000;
                    }
                    dVar.a(f40d);
                }
            }
            for (int i2 = 0; i2 < n; i2++) {
                int i3 = g[i2];
                int i4 = h[i2];
                switch (f[i2]) {
                    case 0:
                        o = i[i2];
                        a = i3;
                        b = i4;
                        p = true;
                        q = true;
                        int s2 = (int) baVar.s();
                        int t = (int) baVar.t();
                        if (((i3 - s2) * (i3 - s2)) + ((i4 - t) * (i4 - t)) >= m) {
                            e = false;
                            break;
                        } else {
                            e = true;
                            break;
                        }
                    case 1:
                        if (q && o + k >= nanoTime) {
                            int i5 = a;
                            int i6 = b;
                            if (dVar.a.o != null) {
                                dVar.a.o.b(i5, i6);
                            }
                        }
                        o = Long.MAX_VALUE;
                        c = false;
                        r = -1;
                        b(dVar);
                        break;
                    case 2:
                        if (!c) {
                            if (p && ((i3 - a) * (i3 - a)) + ((i4 - b) * (i4 - b)) > l && o + 1000000000 > nanoTime) {
                                dVar.g();
                                float f2 = (float) (i3 - a);
                                float f3 = (float) (i4 - b);
                                if (dVar.a.o != null) {
                                    dVar.a.o.a(f2, f3);
                                }
                                p = false;
                                q = false;
                                o = Long.MAX_VALUE;
                                break;
                            }
                        } else {
                            int s3 = (int) baVar.s();
                            int t2 = (int) baVar.t();
                            if (((s3 - i3) * (s3 - i3)) + ((t2 - i4) * (t2 - i4)) >= 12100) {
                                c = false;
                                r = -1;
                                s = -1;
                                o = Long.MAX_VALUE;
                                b(dVar);
                                break;
                            } else {
                                int i7 = r;
                                int i8 = s;
                                int i9 = i3 - i7;
                                int i10 = i4 - i8;
                                double sqrt = Math.sqrt((double) ((i9 * i9) + (i10 * i10)));
                                for (int i11 = 0; ((double) i11) < sqrt; i11 += 4) {
                                    dVar.a.c.a((int) (((double) i7) + (((double) i9) * (((double) ((float) i11)) / sqrt))), (int) (((double) i8) + (((double) i10) * (((double) ((float) i11)) / sqrt))), 4);
                                }
                                by.b().w();
                                dVar.a(i3);
                                r = i3;
                                s = i4;
                                break;
                            }
                        }
                }
            }
        }
    }

    private static void b(d dVar) {
        dVar.g();
        f40d = 0;
    }

    public static void b() {
        throw new RuntimeException();
    }

    public static void a(int i2) {
        k = (long) (i2 * 1000000);
        j = (long) ((i2 - 100) * 1000000);
    }
}
