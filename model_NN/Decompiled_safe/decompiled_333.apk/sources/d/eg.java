package d;

import android.os.Build;

/* compiled from: ProGuard */
public final class eg {
    public static final boolean a = (Integer.parseInt(Build.VERSION.SDK) >= 4);
    public static final boolean b;
    public static o c = new o();

    static {
        boolean z;
        if (Integer.parseInt(Build.VERSION.SDK) >= 5) {
            z = true;
        } else {
            z = false;
        }
        b = z;
    }

    public static String a() {
        if (!a) {
            return "UnknownApi3";
        }
        try {
            return (String) Build.class.getField("MANUFACTURER").get(null);
        } catch (Exception e) {
            return "Unknown";
        }
    }
}
