package d;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.ads.R;
import dk.logisoft.airattack.AirAttackActivity;

/* compiled from: ProGuard */
public final class df {
    /* access modifiers changed from: private */
    public static AirAttackActivity a;
    /* access modifiers changed from: private */
    public static String b;

    public static void a(AirAttackActivity airAttackActivity) {
        a = airAttackActivity;
    }

    public static AlertDialog a(AirAttackActivity airAttackActivity, Context context, boolean z) {
        AlertDialog.Builder positiveButton = new AlertDialog.Builder(airAttackActivity).setIcon(17301659).setTitle((int) R.string.message_of_the_day_title).setView(LayoutInflater.from(airAttackActivity).inflate((int) R.layout.message_of_the_day, (ViewGroup) null)).setCancelable(false).setPositiveButton((int) R.string.message_of_the_day_ok, new dg());
        if (z) {
            positiveButton.setNeutralButton(context.getString(R.string.button_go_to_market), new dh());
        }
        return positiveButton.create();
    }

    public static void a(Dialog dialog) {
        if (b != null) {
            TextView textView = (TextView) dialog.findViewById(R.id.messageOfTheDay);
            textView.setText(Html.fromHtml(b));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    public static void a(String str) {
        if (str != null) {
            a.runOnUiThread(new di(str));
        }
    }
}
