package d;

import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class r extends Thread {
    private final s a;
    private final URL b;
    private final int c;

    public r(s sVar, URL url, int i) {
        if (i == 0 || i == 2) {
            this.a = sVar;
            this.b = url;
            this.c = i;
            return;
        }
        throw new IllegalArgumentException("received type: " + i);
    }

    public final void run() {
        try {
            StringBuffer a2 = a(this.b);
            ArrayList arrayList = new ArrayList();
            String[] split = a2.toString().split("\n");
            int i = 0;
            while (i < split.length - 1) {
                String str = split[i];
                int i2 = i + 1;
                arrayList.add(new u(str, new Integer(split[i2]).intValue()));
                i = i2 + 1;
            }
            if (this.c == 0) {
                this.a.a(arrayList);
            } else {
                this.a.b(arrayList);
            }
            this.a.a(false);
        } catch (Exception e) {
            this.a.a(true);
            Log.e(getClass().getName(), "Url: " + this.b, e);
        }
    }

    public static StringBuffer a(URL url) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()), 1024);
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    return stringBuffer;
                }
                stringBuffer.append(readLine);
                stringBuffer.append("\n");
            } finally {
                bufferedReader.close();
            }
        }
    }
}
