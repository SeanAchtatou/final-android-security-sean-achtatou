package d;

/* compiled from: ProGuard */
public final class eb {
    int a;
    private final int b;
    private final int c;

    /* renamed from: d  reason: collision with root package name */
    private final dz f46d;
    private boolean e;
    private int f = 0;

    public eb(int i, int i2, dz dzVar) {
        this.c = i;
        this.b = i2;
        this.f46d = dzVar;
        b();
    }

    public final void a() {
        if (this.e) {
            this.a = this.f;
            if (this.f >= this.b) {
                this.e = false;
                return;
            }
            int i = this.c;
            int i2 = this.f * i;
            int[] a2 = this.f46d.a();
            int i3 = i + i2;
            while (true) {
                if (i2 >= i3) {
                    break;
                } else if (a2[i2] != 0) {
                    this.e = false;
                    break;
                } else {
                    i2++;
                }
            }
            this.f++;
        }
    }

    public final void b() {
        this.e = true;
        this.f = 0;
        this.a = 0;
    }

    public final void a(int i, boolean z) {
        if (z && i <= this.a) {
            this.e = true;
            if (z) {
                int i2 = i > 0 ? i : 0;
                this.a = i2;
                this.f = i2;
                return;
            }
            this.f = this.a;
        }
    }
}
