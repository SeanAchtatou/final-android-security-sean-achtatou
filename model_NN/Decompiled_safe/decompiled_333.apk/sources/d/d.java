package d;

import android.content.Context;
import android.util.Log;

/* compiled from: ProGuard */
public final class d {
    public b a = null;
    public g b;
    private final cn c;

    /* renamed from: d  reason: collision with root package name */
    private final int f38d;
    private final int e;
    private long f = 0;
    private f g;
    private c h = c.Easy;
    private int i;
    private final Context j;
    private volatile boolean k = false;

    public d(cn cnVar, int i2, int i3, Context context) {
        this.c = cnVar;
        this.f38d = i2;
        this.e = i3;
        this.j = context;
        a(b.a);
    }

    public final boolean a() {
        return this.b == g.PLAYING || this.b == g.PLAYING_WAVE_READY;
    }

    private void m() {
        Log.d(getClass().getSimpleName(), "CanReduceWaves: " + b.b);
        if (this.g != null) {
            this.a = this.g.a();
        } else {
            this.a = new b(this.f38d, this.e, this.j);
        }
        if (!b.a) {
            this.g = new f(this.f38d, this.e, this.j);
            Thread thread = new Thread(this.g);
            thread.setPriority(1);
            thread.start();
            Log.d(getClass().getName(), "Finished generating new game - trickered GC");
            System.gc();
        }
    }

    public final void b() {
        if (this.b == g.WELCOME) {
            m();
        }
    }

    public final void c() {
        switch (e.a[this.b.ordinal()]) {
            case 1:
                this.f = System.nanoTime() + 100000000;
                return;
            case 2:
            case 3:
                this.f = 40000000 + System.nanoTime();
                if (this.b == g.PLAYING && this.a.l().c()) {
                    int i2 = this.i;
                    this.i = i2 + 1;
                    if (i2 >= 40) {
                        this.b = g.PLAYING_WAVE_READY;
                        this.i = 0;
                        this.c.a(this.a.l().a);
                        if (this.b == g.PLAYING_WAVE_READY) {
                            this.a.l().b();
                            this.b = g.PLAYING;
                        }
                        if (b.b && cb.a().H() < this.a.l().a()) {
                            this.a.b();
                            n();
                        }
                    }
                }
                this.a.e();
                if (this.a.a()) {
                    int i3 = this.i;
                    this.i = i3 + 1;
                    if (i3 >= 40) {
                        n();
                        this.i = 0;
                        return;
                    }
                    return;
                }
                return;
            case 4:
                this.f = System.nanoTime() + 100000000;
                if (this.k) {
                    dk.a();
                    a(b.a);
                    this.k = false;
                    return;
                }
                return;
            default:
                throw new IllegalStateException();
        }
    }

    public final void d() {
        this.c.a(this.a);
    }

    public final void e() {
        long nanoTime = System.nanoTime();
        if (this.f > nanoTime) {
            try {
                Thread.sleep((this.f - nanoTime) / 1000000);
            } catch (InterruptedException e2) {
            }
            this.a.r.a(false);
            return;
        }
        this.a.r.a(true);
    }

    private void a(boolean z) {
        this.b = g.WELCOME;
        ca.a().b();
        this.c.a(z);
        m();
    }

    private void n() {
        ca.a().c();
        Cdo.a(this.a.k(), this.a.l().a() - 1, this.a.m(), this.c, this.a.r(), this.a.c());
        this.b = g.GAMEOVER;
        by.b().a();
    }

    public final void f() {
        this.k = true;
    }

    private void o() {
        this.a.d();
        this.c.e();
        ca.a().d();
        this.a.l().b();
        this.b = g.PLAYING;
        this.g = null;
    }

    public final void a(int i2) {
        if (this.a.o != null) {
            this.a.o.a(i2);
        }
    }

    public final void g() {
        if (this.a.o != null) {
            this.a.o.i();
        }
    }

    public final void h() {
        a(b.a);
    }

    public final void i() {
        this.h = c.Easy;
        this.a.o();
        this.a.a(false);
        o();
    }

    public final void j() {
        this.h = c.Hard;
        this.a.p();
        this.a.a(false);
        o();
    }

    public final void k() {
        this.h = c.BottomLess;
        this.a.q();
        this.a.a(true);
        o();
    }

    public final c l() {
        return this.h;
    }
}
