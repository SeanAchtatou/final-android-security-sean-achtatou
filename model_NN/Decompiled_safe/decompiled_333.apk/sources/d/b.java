package d;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import com.google.ads.R;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public final class b {
    private static Paint D;
    public static final boolean a = cb.a().b((int) R.string.freeTrialVersion);
    public static boolean b = cb.a().b((int) R.string.canReduceLevels);
    public static final ah t = new ah();
    public static final ah u = new ah();
    public static final ah v = new ah();
    public static final ah w = new ah();
    public static final ah x = new ah();
    public static final ah y = new ah();
    private int A;
    private int B;
    private boolean C = false;
    public final k c;

    /* renamed from: d  reason: collision with root package name */
    ArrayList f21d = new ArrayList();
    ArrayList e = new ArrayList();
    ArrayList f = new ArrayList();
    ArrayList g = new ArrayList();
    ArrayList h = new ArrayList();
    ArrayList i = new ArrayList();
    public c j;
    float k = 0.17777778f;
    int l;
    int m;
    public int n = cb.a().G();
    public ba o;
    ArrayList p = new ArrayList();
    ArrayList q = new ArrayList();
    public bq r;
    public ed s;
    private a z;

    static {
        Paint paint = new Paint();
        D = paint;
        paint.setColor(-1);
        D.setStyle(Paint.Style.STROKE);
        D.setPathEffect(new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f));
    }

    b(int i2, int i3, Context context) {
        this.l = i2;
        this.m = i3;
        this.c = k.a(i2, i3);
        this.z = new a(this, this.c);
        this.r = new bq(this.c);
        this.s = new ed();
    }

    public final boolean a() {
        return this.o == null;
    }

    public final void b() {
        this.C = true;
        this.n = 0;
        if (this.o != null) {
            this.o.g();
        }
    }

    public final boolean c() {
        return this.C;
    }

    public final void d() {
        this.o = (ba) this.s.a(ba.class);
        this.o.a(this.c, this.l / 2, this);
        a(this.o);
    }

    public final void e() {
        a(this.f);
        a(this.f21d);
        a(this.e);
        a(this.g);
        a(this.h);
        a(this.i);
        this.r.a();
        k kVar = this.c;
        for (int i2 = 0; i2 < 15; i2++) {
            int a2 = n.a(kVar.e);
            int a3 = n.a(kVar.f49d);
            if (kVar.c(a2, a3) == -22016) {
                kVar.b(a2, a3);
            }
        }
        ArrayList arrayList = this.p;
        int size = arrayList.size();
        for (int i3 = 0; i3 < size; i3++) {
            av avVar = (av) arrayList.get(i3);
            if (avVar instanceof bh) {
                if (avVar instanceof ba) {
                    this.o = null;
                }
                this.f.remove(avVar);
            } else if (avVar instanceof y) {
                this.f21d.remove(avVar);
            } else if (avVar instanceof aa) {
                this.e.remove(avVar);
            } else if (avVar instanceof aq) {
                this.g.remove(avVar);
            } else if (avVar instanceof al) {
                this.h.remove(avVar);
            } else {
                this.i.remove(avVar);
            }
            avVar.G();
        }
        ArrayList arrayList2 = this.q;
        int size2 = arrayList2.size();
        for (int i4 = 0; i4 < size2; i4++) {
            av avVar2 = (av) arrayList2.get(i4);
            if (avVar2 instanceof bh) {
                this.f.add((bh) avVar2);
            } else if (avVar2 instanceof y) {
                this.f21d.add((y) avVar2);
            } else if (avVar2 instanceof aa) {
                this.e.add((aa) avVar2);
            } else if (avVar2 instanceof aq) {
                this.g.add((aq) avVar2);
            } else if (avVar2 instanceof al) {
                this.h.add((al) avVar2);
            } else {
                this.i.add(avVar2);
            }
        }
        this.p.clear();
        this.q.clear();
        this.c.c.c();
    }

    private void a(ArrayList arrayList) {
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            av avVar = (av) arrayList.get(i2);
            if (!avVar.p()) {
                this.p.add(avVar);
                if (avVar instanceof y) {
                    this.B++;
                }
            }
        }
    }

    public final float f() {
        return this.k;
    }

    public final void a(av avVar) {
        if (avVar instanceof y) {
            this.A++;
        }
        this.q.add(avVar);
    }

    public final void a(Canvas canvas) {
        this.c.c.a(canvas);
        this.r.a(canvas);
        a(canvas, this.i);
        a(canvas, this.f21d);
        a(canvas, this.e);
        a(canvas, this.f);
        a(canvas, this.g);
        a(canvas, this.h);
        if (dj.c) {
            float s2 = this.o.s();
            float t2 = this.o.t();
            canvas.drawBitmap(cb.a().F(), (float) ((int) (s2 - ((float) (cb.a().F().getWidth() / 2)))), t2 - 80.0f, (Paint) null);
            canvas.drawCircle(s2, t2, 110.0f, D);
        }
    }

    private static void a(Canvas canvas, ArrayList arrayList) {
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((av) arrayList.get(i2)).a(canvas);
        }
    }

    public final ba g() {
        return this.o;
    }

    public final List b(av avVar) {
        return t.a(avVar, this.i);
    }

    public final List c(av avVar) {
        return u.a(avVar, this.f);
    }

    public final List d(av avVar) {
        return w.a(avVar, this.f21d);
    }

    public final List e(av avVar) {
        return v.a(avVar, this.e);
    }

    public final List f(av avVar) {
        return x.a(avVar, this.g);
    }

    public final List g(av avVar) {
        return y.a(avVar, this.h);
    }

    public final ba h(av avVar) {
        if (this.o == null || !avVar.a(this.o)) {
            return null;
        }
        return this.o;
    }

    public final bh i(av avVar) {
        List c2 = c(avVar);
        if (c2.size() > 0) {
            return (bh) c2.get(0);
        }
        return null;
    }

    public final void a(int i2, int i3, int i4) {
        int i5 = i4 * i4;
        ArrayList arrayList = this.f;
        int size = arrayList.size();
        for (int i6 = 0; i6 < size; i6++) {
            bh bhVar = (bh) arrayList.get(i6);
            int s2 = ((int) bhVar.s()) - i2;
            int t2 = ((int) bhVar.t()) - i3;
            if ((s2 * s2) + (t2 * t2) < i5) {
                bhVar.g();
            }
        }
    }

    public final aa b(int i2, int i3, int i4) {
        int i5 = i4 * i4;
        ArrayList arrayList = this.e;
        int size = arrayList.size();
        for (int i6 = 0; i6 < size; i6++) {
            aa aaVar = (aa) arrayList.get(i6);
            int s2 = ((int) aaVar.s()) - i2;
            int t2 = ((int) aaVar.t()) - i3;
            if ((s2 * s2) + (t2 * t2) < i5) {
                return aaVar;
            }
        }
        return null;
    }

    public final void h() {
        ArrayList arrayList = this.f21d;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            y yVar = (y) arrayList.get(i2);
            if (yVar.v() + ((float) yVar.A()) >= -10.0f && yVar.v() <= ((float) (this.l + 10))) {
                yVar.a();
            }
        }
    }

    public final void i() {
        this.n++;
    }

    public final int j() {
        return this.A - this.B;
    }

    public final int k() {
        return this.B;
    }

    public final a l() {
        return this.z;
    }

    public final int m() {
        return this.z.a() - 1;
    }

    public final boolean n() {
        ArrayList arrayList = this.e;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            aa aaVar = (aa) arrayList.get(i2);
            if (((aaVar instanceof at) && !aaVar.b_()) || (aaVar instanceof ak)) {
                return true;
            }
        }
        ArrayList arrayList2 = this.i;
        int size2 = arrayList2.size();
        for (int i3 = 0; i3 < size2; i3++) {
            if (((av) arrayList2.get(i3)) instanceof au) {
                return true;
            }
        }
        return false;
    }

    public final void o() {
        this.k = 0.13333334f;
        this.j = c.Easy;
    }

    public final void p() {
        this.k = 0.17777778f;
        this.j = c.Hard;
    }

    public final void q() {
        this.k = 0.13333334f;
        this.j = c.BottomLess;
    }

    public final void a(boolean z2) {
        this.c.c.a(z2);
    }

    public final c r() {
        return this.j;
    }
}
