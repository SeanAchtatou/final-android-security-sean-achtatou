package d;

import android.util.Log;
import com.google.ads.R;

/* compiled from: ProGuard */
public final class a {
    private static final o b = new o();
    public int a = 0;
    private final b c;

    /* renamed from: d  reason: collision with root package name */
    private final k f12d;
    private final dz e;
    private int f = 1;
    private int g = 6;
    private ao h;
    private boolean i;

    public a(b bVar, k kVar) {
        this.c = bVar;
        this.f12d = kVar;
        this.e = kVar.c;
    }

    public final int a() {
        return this.a;
    }

    public final void b() {
        int i2;
        if (this.a != this.g) {
            this.a++;
            int pow = (int) Math.pow((double) this.a, 1.12d);
            while (pow > 0) {
                int i3 = 0;
                int i4 = 10000;
                int i5 = 10000;
                while (i5 * i4 > pow) {
                    int i6 = i3 + 1;
                    if (i3 == 1000) {
                        throw new RuntimeException("seems there is a bug in the code...");
                    }
                    int i7 = i6;
                    i4 = b.nextInt(pow) + 1;
                    i5 = b.nextInt(pow) + 1;
                    i3 = i7;
                }
                int i8 = pow - (i5 * i4);
                for (int i9 = 0; i9 < i5; i9++) {
                    i8 = a(i8, i4);
                }
                pow = i8;
            }
            if (this.a % 4 == 0 || cb.a().h != 0) {
                if (h.a != null) {
                    i2 = h.a.c() + 1;
                } else {
                    i2 = 1;
                }
                bo boVar = (bo) this.c.s.a(bo.class);
                boVar.a(this.f12d, d(), i2, this.c);
                this.c.a(boVar);
            }
            ao aoVar = (ao) this.c.s.a(ao.class);
            aoVar.a(cb.a().a((int) R.string.in_game_wave_prefix) + " " + this.a + " " + cb.a().a((int) R.string.in_game_wave), (float) (this.c.c.b() / 2), (float) ((this.c.c.a() / 2) - 60), this.c.c, this.c, 48);
            this.c.a(aoVar);
            by.b().m();
            Log.d(getClass().getName(), "Trickered GC");
            System.gc();
            try {
                Thread.sleep(15);
            } catch (InterruptedException e2) {
            }
        } else if (!this.i) {
            int b2 = this.f12d.b();
            int a2 = this.f12d.a();
            this.h = (ao) this.c.s.a(ao.class);
            this.h.a(cb.a().a((int) R.string.in_game_high_altitude_bomber_detected), (float) (b2 / 2), (float) ((a2 / 2) - 60), this.f12d, this.c, 24);
            this.c.a(this.h);
            this.i = true;
        } else if (this.h.a()) {
            int b3 = this.f12d.b();
            int i10 = -25;
            int i11 = 0;
            while (true) {
                int i12 = i10;
                if (i11 >= this.f) {
                    break;
                }
                if (n.a(5) <= 1) {
                    at atVar = (at) this.c.s.a(at.class);
                    atVar.a(this.f12d, n.a(b3), i12, this.c);
                    this.c.a(atVar);
                } else {
                    ak akVar = (ak) this.c.s.a(ak.class);
                    akVar.a(this.f12d, n.a(b3), i12, this.c);
                    this.c.a(akVar);
                }
                i10 = -n.a(300);
                i11++;
            }
            this.g += n.a(4) + 3;
            if (this.c.j.equals(c.Easy)) {
                this.f = Math.min(this.f + 1, 3);
            } else {
                this.f++;
            }
            this.i = false;
        }
    }

    public final boolean c() {
        return this.c.j() == 0 && !this.c.n();
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:77:0x013c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:76:0x013c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:75:0x013c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:74:0x013c */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:73:0x013c */
    /* JADX WARN: Type inference failed for: r17v4, types: [d.y] */
    /* JADX WARN: Type inference failed for: r17v6, types: [d.y] */
    /* JADX WARN: Type inference failed for: r17v8, types: [d.y] */
    /* JADX WARN: Type inference failed for: r17v10, types: [d.y] */
    /* JADX WARN: Type inference failed for: r17v11 */
    /* JADX WARN: Type inference failed for: r17v13, types: [d.ab] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    private int a(int r17, int r18) {
        /*
            r16 = this;
            d.o r2 = d.a.b
            r3 = 7
            int r2 = r2.nextInt(r3)
            r3 = 6
            if (r2 != r3) goto L_0x020a
            d.o r2 = d.a.b
            boolean r2 = r2.nextBoolean()
            if (r2 == 0) goto L_0x006f
            r2 = 0
            r3 = r2
        L_0x0014:
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = r17
            r14 = r5
            r5 = r4
            r4 = r14
            r15 = r2
            r2 = r6
            r6 = r15
        L_0x0020:
            r0 = r7
            r1 = r18
            if (r0 < r1) goto L_0x0096
            d.o r8 = d.a.b
            r9 = 10
            int r8 = r8.nextInt(r9)
            r0 = r16
            int r0 = r0.a
            r9 = r0
            if (r8 >= r9) goto L_0x0096
            int r8 = r2 + 1
            r9 = 10
            if (r2 >= r9) goto L_0x0096
            d.o r2 = d.a.b
            r9 = 10
            int r2 = r2.nextInt(r9)
            int r2 = r2 + 1
            if (r2 < r8) goto L_0x0096
            d.o r2 = d.a.b
            r9 = 5
            int r2 = r2.nextInt(r9)
            r9 = 1
            if (r2 > r9) goto L_0x0078
            r9 = 2
            if (r6 >= r9) goto L_0x0078
            int r2 = r6 + 1
            int r6 = r7 - r18
            r14 = r4
            r4 = r5
            r5 = r2
            r2 = r14
        L_0x005b:
            r7 = 0
            if (r2 == 0) goto L_0x005f
            r7 = 1
        L_0x005f:
            int r7 = r7 + r5
            int r7 = r7 + r4
            r9 = 4
            if (r7 == r9) goto L_0x0067
            r9 = 6
            if (r7 != r9) goto L_0x0069
        L_0x0067:
            int r6 = r6 - r18
        L_0x0069:
            r7 = r6
            r6 = r5
            r5 = r4
            r4 = r2
            r2 = r8
            goto L_0x0020
        L_0x006f:
            d.o r2 = d.a.b
            r3 = 6
            int r2 = r2.nextInt(r3)
            r3 = r2
            goto L_0x0014
        L_0x0078:
            r9 = 3
            if (r2 > r9) goto L_0x0089
            r9 = 3
            if (r5 >= r9) goto L_0x0089
            int r2 = r5 + 1
            int r5 = r7 - r18
            r14 = r4
            r4 = r2
            r2 = r14
            r15 = r6
            r6 = r5
            r5 = r15
            goto L_0x005b
        L_0x0089:
            r9 = 4
            if (r2 != r9) goto L_0x0204
            if (r4 != 0) goto L_0x0204
            r2 = 1
            int r4 = r7 - r18
            r14 = r5
            r5 = r6
            r6 = r4
            r4 = r14
            goto L_0x005b
        L_0x0096:
            r2 = 0
            r8 = r2
        L_0x0098:
            r0 = r8
            r1 = r18
            if (r0 >= r1) goto L_0x0200
            d.cb r2 = d.cb.a()
            android.graphics.Bitmap r2 = r2.b()
            int r2 = r2.getHeight()
            r0 = r16
            d.k r0 = r0.f12d
            r9 = r0
            int r9 = r9.a()
            int r9 = r9 / 2
            int r9 = r9 - r2
            r0 = r16
            d.dz r0 = r0.e
            r10 = r0
            int r10 = r10.d()
            int r2 = r2 * 2
            int r2 = r10 - r2
            int r2 = java.lang.Math.max(r9, r2)
            r9 = 0
            dk.logisoft.airattack.ads.d r10 = d.h.a
            if (r10 == 0) goto L_0x00d8
            r0 = r16
            int r0 = r0.a
            r9 = r0
            if (r9 != 0) goto L_0x010e
            dk.logisoft.airattack.ads.d r9 = d.h.a
            int r9 = r9.b()
        L_0x00d8:
            int r2 = r2 - r9
            int r10 = d.n.a(r2)
            int r10 = r10 + r9
            int r11 = r2 / 2
            int r11 = d.n.a(r11)
            int r12 = r2 / 2
            int r11 = r11 + r12
            int r11 = r11 + r9
            int r2 = r2 / 3
            int r2 = d.n.a(r2)
            int r9 = r9 + r2
            d.cb r2 = d.cb.a()
            int r2 = r2.f33d
            r12 = -1
            if (r2 == r12) goto L_0x0201
            d.cb r2 = d.cb.a()
            int r2 = r2.f33d
        L_0x00fe:
            r0 = r16
            d.b r0 = r0.c
            r12 = r0
            d.ed r12 = r12.s
            switch(r2) {
                case 0: goto L_0x0115;
                case 1: goto L_0x0152;
                case 2: goto L_0x017a;
                case 3: goto L_0x019b;
                case 4: goto L_0x01bc;
                case 5: goto L_0x01de;
                default: goto L_0x0108;
            }
        L_0x0108:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            r2.<init>()
            throw r2
        L_0x010e:
            dk.logisoft.airattack.ads.d r9 = d.h.a
            int r9 = r9.c()
            goto L_0x00d8
        L_0x0115:
            java.lang.Class<d.ab> r2 = d.ab.class
            java.lang.Object r17 = r12.a(r2)
            d.ab r17 = (d.ab) r17
            r0 = r17
            d.ab r0 = (d.ab) r0
            r2 = r0
            r0 = r16
            d.k r0 = r0.f12d
            r9 = r0
            int r11 = r16.d()
            r0 = r16
            d.b r0 = r0.c
            r12 = r0
            d.cb r13 = d.cb.a()
            r13.b()
            r2.b(r9, r11, r10, r12)
            r2 = r17
        L_0x013c:
            r2.a(r6)
            r2.b(r5)
            r2.a(r4)
            r0 = r16
            d.b r0 = r0.c
            r9 = r0
            r9.a(r2)
            int r2 = r8 + 1
            r8 = r2
            goto L_0x0098
        L_0x0152:
            java.lang.Class<d.as> r2 = d.as.class
            java.lang.Object r17 = r12.a(r2)
            d.y r17 = (d.y) r17
            r0 = r17
            d.as r0 = (d.as) r0
            r2 = r0
            r0 = r16
            d.k r0 = r0.f12d
            r9 = r0
            int r11 = r16.d()
            r0 = r16
            d.b r0 = r0.c
            r12 = r0
            d.cb r13 = d.cb.a()
            r13.f()
            r2.b(r9, r11, r10, r12)
            r2 = r17
            goto L_0x013c
        L_0x017a:
            java.lang.Class<d.ag> r2 = d.ag.class
            java.lang.Object r17 = r12.a(r2)
            d.y r17 = (d.y) r17
            r0 = r17
            d.ag r0 = (d.ag) r0
            r2 = r0
            r0 = r16
            d.k r0 = r0.f12d
            r9 = r0
            int r11 = r16.d()
            r0 = r16
            d.b r0 = r0.c
            r12 = r0
            r2.a(r9, r11, r10, r12)
            r2 = r17
            goto L_0x013c
        L_0x019b:
            java.lang.Class<d.bg> r2 = d.bg.class
            java.lang.Object r17 = r12.a(r2)
            d.y r17 = (d.y) r17
            r0 = r17
            d.bg r0 = (d.bg) r0
            r2 = r0
            r0 = r16
            d.k r0 = r0.f12d
            r9 = r0
            int r10 = r16.d()
            r0 = r16
            d.b r0 = r0.c
            r12 = r0
            r2.a(r9, r10, r11, r12)
            r2 = r17
            goto L_0x013c
        L_0x01bc:
            java.lang.Class<d.ad> r2 = d.ad.class
            java.lang.Object r17 = r12.a(r2)
            d.y r17 = (d.y) r17
            r0 = r17
            d.ad r0 = (d.ad) r0
            r2 = r0
            r0 = r16
            d.k r0 = r0.f12d
            r9 = r0
            int r10 = r16.d()
            r0 = r16
            d.b r0 = r0.c
            r12 = r0
            r2.a(r9, r10, r11, r12)
            r2 = r17
            goto L_0x013c
        L_0x01de:
            java.lang.Class<d.aw> r2 = d.aw.class
            java.lang.Object r17 = r12.a(r2)
            d.aw r17 = (d.aw) r17
            r0 = r17
            d.aw r0 = (d.aw) r0
            r2 = r0
            r0 = r16
            d.k r0 = r0.f12d
            r10 = r0
            int r11 = r16.d()
            r0 = r16
            d.b r0 = r0.c
            r12 = r0
            r2.a(r10, r11, r9, r12)
            r2 = r17
            goto L_0x013c
        L_0x0200:
            return r7
        L_0x0201:
            r2 = r3
            goto L_0x00fe
        L_0x0204:
            r2 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x005b
        L_0x020a:
            r3 = r2
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: d.a.a(int, int):int");
    }

    private int d() {
        int i2 = (this.a * 50) + 600;
        if (b.nextBoolean()) {
            return (b.nextInt(i2) - 100) - i2;
        }
        return i2 + (this.f12d.b() - b.nextInt(i2)) + 100;
    }
}
