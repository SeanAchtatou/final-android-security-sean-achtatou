package d;

import android.graphics.Canvas;
import com.google.ads.R;

/* compiled from: ProGuard */
public class bh extends av {
    private static int[] a = {-65536, -8126464, -3538944};
    protected int e = 80;
    protected boolean f;
    protected boolean g;
    protected boolean h;
    int i;
    protected boolean j;

    public final void a(k kVar, float f2, float f3, boolean z, b bVar) {
        super.a(kVar, f2, f3, z, bVar);
        this.i = 0;
        this.f = false;
        this.g = false;
        this.h = false;
    }

    public void g() {
        j();
    }

    public final void j() {
        if (this.i <= 0 && !this.g && !this.h) {
            this.s.r.a(s(), t(), a, 20);
            this.g = true;
            this.A = 0;
            this.i = 100;
            by.b().e();
        }
    }

    /* access modifiers changed from: protected */
    public final void k() {
        this.h = true;
        this.A = 0;
        by.b().f();
    }

    /* access modifiers changed from: protected */
    public void h() {
        b(true);
    }

    public final void a(Canvas canvas) {
        if (this.i % 2 != 1) {
            super.a(canvas);
            int i2 = this.e;
            if (i2 < 80) {
                float w = w();
                float s = s();
                ea.a(canvas, (int) s, (int) w, ((float) i2) / 80.0f, -65536);
                ao.a(cb.a().a((int) R.string.in_game_taking_a_deep_breath), s, w, this.s);
            }
        }
    }

    public final void l() {
        this.f = true;
    }

    public void d() {
        if (this.f) {
            this.f = false;
            if (this.e == 0) {
                g();
            } else {
                this.e--;
            }
        } else if (this.e < 80) {
            this.e++;
        }
        if (this.i > 0) {
            this.i--;
        }
        if (this.g) {
            int i2 = this.A;
            this.A = i2 + 1;
            if (i2 >= 30) {
                this.g = false;
                h();
            } else {
                a(-0.05f);
                e((float) (((this.A * 3) - 30) / 20));
            }
            this.j = false;
        } else if (this.h) {
            int i3 = this.A;
            this.A = i3 + 1;
            if (i3 >= 60) {
                this.h = false;
                h();
            }
            e(1.0f);
            this.j = false;
        } else {
            this.j = true;
        }
    }
}
