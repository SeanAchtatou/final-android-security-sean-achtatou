package d;

import java.lang.reflect.Array;

/* compiled from: ProGuard */
public final class ec {
    public static Object a(Object obj) {
        int length = Array.getLength(obj);
        Object newInstance = Array.newInstance(obj.getClass().getComponentType(), length * 2);
        System.arraycopy(obj, 0, newInstance, 0, length);
        return newInstance;
    }
}
