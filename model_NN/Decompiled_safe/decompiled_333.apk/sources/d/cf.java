package d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import java.util.ArrayList;

/* compiled from: ProGuard */
public abstract class cf {
    private static cf h = null;
    private static boolean i = true;
    private static boolean j = true;
    private static int k = 1140850687;
    private static int l = -2080375040;
    public final int a;
    public final int b;
    int c;

    /* renamed from: d  reason: collision with root package name */
    int f35d;
    int e;
    int f;
    int g;

    public abstract void b();

    public cf(int i2, String str) {
        this.c = i2;
        Bitmap a2 = dd.a(i2);
        this.f35d = ea.b(i2);
        this.e = ea.a(i2);
        this.b = a2.getHeight();
        this.a = a2.getWidth();
    }

    public final void a(int i2, int i3) {
        this.f = i2;
        this.g = i3;
    }

    public static void a() {
        by.b().h();
    }

    public final void a(Canvas canvas, boolean z) {
        if (h == this) {
            b(canvas, z);
            ea.a(canvas, this.f, this.g, this.a, this.b, l);
            return;
        }
        ea.a(canvas, this.f, this.g, this.a, this.b, k);
        b(canvas, z);
    }

    private void b(Canvas canvas, boolean z) {
        if (z) {
            ea.b(canvas, this.c, this.f, this.g);
        } else {
            ea.a(canvas, this.c, this.f, this.g);
        }
    }

    public static boolean a(int[] iArr, int[] iArr2, int[] iArr3, int i2, ArrayList arrayList) {
        int size = arrayList.size();
        for (int i3 = 0; i3 < i2; i3++) {
            for (int i4 = 0; i4 < size; i4++) {
                cf cfVar = (cf) arrayList.get(i4);
                int i5 = iArr2[i3];
                int i6 = iArr3[i3];
                if (i5 > cfVar.f && i5 < cfVar.f + cfVar.a && i6 > cfVar.g && i6 < cfVar.g + cfVar.b) {
                    if (j) {
                        j = false;
                        h = cfVar;
                        i = true;
                    } else if (h != cfVar) {
                        h = null;
                        i = false;
                    }
                    if (iArr[i3] == 1) {
                        j = true;
                        h = null;
                        if (!i) {
                            return false;
                        }
                        by.b().h();
                        cfVar.b();
                        return true;
                    }
                }
            }
            if (iArr[i3] == 1) {
                j = true;
                h = null;
                return false;
            }
            if (j) {
                j = false;
                h = null;
                i = false;
            }
        }
        for (int i7 = 0; i7 < i2; i7++) {
            if (iArr[i7] == 1) {
                h = null;
            }
        }
        return false;
    }
}
