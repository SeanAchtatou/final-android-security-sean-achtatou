package d;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import com.google.ads.R;
import dk.logisoft.airattack.ads.d;
import dk.logisoft.airattack.c;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class cn extends Thread {
    protected final d a;
    public cd b;
    private final s c;

    /* renamed from: d  reason: collision with root package name */
    private final s f36d;
    private final s e;
    private final ArrayList f = new ArrayList();
    private final ArrayList g = new ArrayList();
    private boolean h;
    private SurfaceHolder i;
    private String j = null;
    private long k = 0;
    private final ea l;
    private boolean m = false;
    /* access modifiers changed from: private */
    public dr n = new dr();
    private final Context o;
    private volatile boolean p = true;
    private volatile boolean q = false;
    private volatile boolean r = false;
    private boolean s = false;
    private boolean t = false;
    /* access modifiers changed from: private */
    public volatile d u;
    private boolean v = false;
    /* access modifiers changed from: private */
    public da w;
    private final cz x;

    static /* synthetic */ void a(cn cnVar) {
        cnVar.f.clear();
        cnVar.f.add(new cv(cnVar, 17, "Easy Game"));
        cnVar.f.add(new cw(cnVar, 18, "Hard Game"));
        cnVar.f.add(new cx(cnVar, 21, "Classic Game"));
        cnVar.f.add(new cy(cnVar, 5, "Generate New Terrain"));
        cnVar.f.add(new cp(cnVar, 20, "Back"));
        cnVar.o();
    }

    static /* synthetic */ void e(cn cnVar) {
        cc.a((int) R.string.prefKeyNumberOfGamesPlayed, cc.d(R.string.prefKeyNumberOfGamesPlayed) + 1);
        if (p() > 0) {
            cnVar.u.e();
        } else {
            cnVar.u.a(false);
        }
    }

    public cn(SurfaceHolder surfaceHolder, Context context, d dVar, int i2, int i3) {
        this.i = surfaceHolder;
        this.o = context;
        this.u = dVar;
        dj.a(context.getResources().getDisplayMetrics().density);
        this.l = new ea(i2, i3);
        this.x = new cz(i3);
        dy.a(this.l);
        this.a = new d(this, i2, i3, context);
        this.c = new s(context, 1, this.l.b(), this.l.a());
        this.f36d = new s(context, 3, this.l.b(), this.l.a());
        this.e = new s(context, 4, this.l.b(), this.l.a());
        this.n.a(this.x);
        m();
    }

    public final synchronized void a(SurfaceHolder surfaceHolder, Context context, d dVar) {
        this.i = surfaceHolder;
        this.u = dVar;
        dj.a(context.getResources().getDisplayMetrics().density);
    }

    public final synchronized void a(d dVar) {
        this.u = dVar;
    }

    public final synchronized void a(da daVar) {
        this.w = daVar;
    }

    public final synchronized void a() {
        this.r = false;
        while (!this.s) {
            d();
            try {
                notifyAll();
                wait();
            } catch (InterruptedException e2) {
            }
        }
    }

    public final synchronized void b() {
        this.r = true;
        notifyAll();
    }

    public final void run() {
        this.u.a(true);
        c a2 = c.a();
        int a3 = j.a(a2.f());
        int g2 = a2.g();
        int i2 = cb.a().k;
        if (i2 <= g2 || i2 <= a3) {
            ck.a();
        }
        this.c.a();
        this.f36d.a();
        this.e.a();
        while (this.p) {
            synchronized (this) {
                while (true) {
                    if (!this.r || !this.t || (this.q && !this.m)) {
                        try {
                            this.s = true;
                            notifyAll();
                            wait();
                            this.s = false;
                        } catch (InterruptedException e2) {
                        }
                        if (this.a.a()) {
                            dj.a();
                        }
                        if (this.r) {
                            a(this.a.a);
                        }
                    }
                }
            }
            synchronized (this) {
                de.a(this.a);
                dt.a(this.a);
                switch (cq.a[this.a.b.ordinal()]) {
                    case 1:
                        dj.a(this.a);
                        dj.a();
                        if (this.b != null) {
                            this.b.a(this.a);
                            break;
                        }
                        break;
                    case 2:
                        dj.b();
                        dj.a();
                        break;
                    case 3:
                        dj.a(this.g);
                        dj.a();
                        break;
                    case 4:
                        dj.a(this.f);
                        dj.a();
                        break;
                    default:
                        throw new RuntimeException();
                }
            }
            this.a.c();
            this.a.d();
            this.a.e();
            if (!this.v && !this.a.a() && c.a().b()) {
                int d2 = c.a().d();
                if (d2 > j.b(this.o)) {
                    c();
                    j.b(this.o, d2);
                }
                this.v = true;
            }
        }
        System.exit(1);
    }

    public static void c() {
        c a2 = c.a();
        if (a2 != null && a2.c() != null) {
            df.a(a2.c());
        }
    }

    public final synchronized void d() {
        if (this.a != null && this.a.a()) {
            this.m = true;
        }
        this.q = true;
        ca.a().e();
    }

    public final synchronized void e() {
        this.q = false;
        notifyAll();
        if (this.t) {
            ca.a().f();
        }
    }

    public final synchronized void f() {
        if (this.a != null && !this.a.a()) {
            e();
        }
    }

    public final synchronized boolean g() {
        return this.q;
    }

    public final synchronized void h() {
        this.t = false;
    }

    public final synchronized void i() {
        this.t = true;
        notifyAll();
        if (!this.q) {
            ca.a().f();
        }
        this.u.a();
    }

    public final void j() {
        synchronized (this) {
            this.a.h();
        }
    }

    public final synchronized boolean a(MotionEvent motionEvent) {
        dt.a(motionEvent);
        return true;
    }

    public final synchronized boolean k() {
        g gVar;
        gVar = this.a.b;
        return (gVar == g.WELCOME || gVar == g.GAMEOVER) ? false : true;
    }

    public final synchronized boolean a(KeyEvent keyEvent) {
        boolean z;
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 82) {
            z = false;
        } else if (this.a.b == g.PLAYING) {
            de.a(keyEvent);
            z = true;
        } else if (keyCode == 31) {
            i.a();
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public final synchronized boolean b(MotionEvent motionEvent) {
        if (this.q && !this.a.a()) {
            e();
        }
        dj.a(motionEvent);
        return true;
    }

    public final synchronized void l() {
        ca.a().e();
        this.h = true;
        this.c.g();
        this.h = false;
        this.p = false;
        e();
        notifyAll();
    }

    public final void m() {
        this.f.clear();
        this.f.add(new co(this, 22, "Play"));
        this.f.add(new cr(this, 9, "Next Info Screen"));
        this.f.add(new cs(this, 19, "Help"));
        if (b.a) {
            this.f.add(new ct(this, 23, "Buy the Game"));
        }
        this.f.add(new cu(this, 3, "Submit Highest Score and Exit"));
        o();
    }

    private void o() {
        int b2 = this.u.b();
        ArrayList arrayList = this.f;
        int b3 = this.l.b() / 2;
        int a2 = this.l.a() - b2;
        int size = arrayList.size();
        if (size > 0) {
            int c2 = (int) (bz.c() * 6.0f);
            int c3 = (a2 - ((cf) arrayList.get(0)).b) - ((int) (bz.c() * 6.0f));
            int i2 = b3 - (((size * ((cf) arrayList.get(0)).a) + ((size - 1) * c2)) / 2);
            for (int i3 = 0; i3 < size; i3++) {
                cf cfVar = (cf) arrayList.get(i3);
                cfVar.a(i2, c3);
                i2 += cfVar.a + c2;
            }
        }
    }

    public final void a(boolean z) {
        this.u.a(true);
        this.n.a(true);
        if (z) {
            m();
        }
    }

    public final void a(b bVar) {
        Canvas canvas;
        synchronized (this) {
            if (this.r) {
                canvas = this.i.lockCanvas();
            } else {
                Log.d(getClass().getName(), "Skipping lockCanvas because surfaceHolderReady is false");
                canvas = null;
            }
        }
        if (canvas != null) {
            try {
                bVar.a(canvas);
                switch (cq.a[this.a.b.ordinal()]) {
                    case 1:
                        this.x.a(canvas, bVar);
                        if (this.q) {
                            this.x.a(canvas);
                            this.m = false;
                            break;
                        }
                        break;
                    case 2:
                        this.x.a(canvas, bVar);
                        if (this.q) {
                            this.x.a(canvas);
                            this.m = false;
                            break;
                        }
                        break;
                    case 3:
                        o();
                        break;
                    case 4:
                        this.n.a(canvas, this.c, this.f36d, this.e, this.a.l(), canvas.getHeight());
                        ArrayList arrayList = this.f;
                        boolean a2 = this.n.a();
                        int size = arrayList.size();
                        for (int i2 = 0; i2 < size; i2++) {
                            ((cf) arrayList.get(i2)).a(canvas, a2);
                        }
                        o();
                        break;
                    default:
                        throw new RuntimeException();
                }
                if (this.h) {
                    ea.a(canvas, "Exiting. Please wait\nwhile submitting High Score...", 20, this.l.a() / 2, this.l.b() / 2);
                }
                if (this.j != null) {
                    ea.a(canvas, this.j, 12, canvas.getHeight() - 3, canvas.getWidth() / 2);
                    if (this.k <= 0) {
                        this.k = System.currentTimeMillis() + 10000;
                    } else if (this.k < System.currentTimeMillis()) {
                        this.j = null;
                        this.k = 0;
                    }
                }
            } finally {
                this.i.unlockCanvasAndPost(canvas);
            }
        }
    }

    public final void a(int i2) {
        if (i2 == p()) {
            this.u.d();
        } else if (i2 < p()) {
            this.u.a();
        }
    }

    private static int p() {
        int d2 = cc.d(R.string.prefKeyNumberOfGamesPlayed);
        if (d2 < 12) {
            return 0;
        }
        return Math.min(c.a().a("wavesCount", 7), ((d2 - 12) / 5) + 1);
    }

    public final void a(String str, int i2, c cVar) {
        String str2 = str + "";
        switch (cq.b[cVar.ordinal()]) {
            case 1:
                this.c.a(str2, i2);
                break;
            case 2:
                this.f36d.a(str2, i2);
                break;
            case 3:
                this.e.a(str2, i2);
                break;
            default:
                throw new RuntimeException();
        }
        this.a.f();
    }

    public final synchronized void n() {
        notifyAll();
    }
}
