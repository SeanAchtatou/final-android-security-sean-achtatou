package d;

/* compiled from: ProGuard */
public class aj extends av {
    float a;
    float b;
    protected float c;

    /* renamed from: d  reason: collision with root package name */
    protected boolean f15d;
    private int e = 0;
    private final int[] f = new int[1];

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, float, int, d.b]
     candidates:
      d.aj.a(d.k, int, int, d.b, float):void
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void */
    public final void a(k kVar, int i, int i2, b bVar, float f2) {
        a(kVar, (float) i, (float) i2, true, bVar);
        float a2 = n.a() * 3.1415927f * 2.0f;
        float a3 = ((n.a() * 1.5f) + 3.0f) * f2;
        this.a = ((float) Math.cos((double) a2)) * a3;
        this.b = ((float) Math.sin((double) a2)) * a3;
        this.r = bVar.f() * 10.0f;
        if (!y()) {
            x();
        }
        this.f15d = true;
        this.c = 1.5f;
    }

    private void c() {
        int[] iArr = this.f;
        iArr[0] = this.p.c(this.C, this.D);
        this.s.r.a(s(), t(), iArr, 20);
        this.s.c.a((int) s(), (int) t(), 10);
        b(true);
        this.s.a((int) s(), (int) t(), 20);
        by.b().o();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(float, boolean):int
     arg types: [float, int]
     candidates:
      d.aj.a(float, float):boolean
      d.av.a(int, int):void
      d.av.a(float, boolean):int */
    public void d() {
        this.b += 0.15f;
        if ((this.a < 0.0f ? -this.a : this.a) < 0.15f) {
            this.a = 0.0f;
        }
        switch (a(this.b, false)) {
            case 0:
                if (this.f15d && n.a(2) == 0) {
                    c();
                    break;
                } else {
                    if (a(this.a, this.b)) {
                        this.b = (-this.b) / this.c;
                    }
                    int i = this.e;
                    this.e = i + 1;
                    if (i < 10) {
                        by.b().t();
                        break;
                    }
                }
                break;
            case 1:
                b(true);
                break;
        }
        switch (d(this.a)) {
            case 0:
                if (this.f15d && n.a(2) == 0) {
                    c();
                }
                this.a = (-this.a) / this.c;
                int i2 = this.e;
                this.e = i2 + 1;
                if (i2 < 10) {
                    by.b().t();
                    break;
                }
                break;
            case 1:
                b(true);
                break;
        }
        if (a(this.a, this.b)) {
            a(0.2f);
        }
        super.d();
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        bh i = this.s.i(this);
        if (i != null) {
            i.g();
        }
    }

    private static boolean a(float f2, float f3) {
        return Math.abs(f3) > 0.15f && Math.abs(f2) > 0.15f;
    }
}
