package d;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class j {
    private static SharedPreferences a;
    private static Context b;

    private static SharedPreferences d(Context context) {
        if (b != context) {
            b = context;
            a = context.getSharedPreferences("AirAttack", 2);
        }
        return a;
    }

    public static String a(Context context) {
        return d(context).getString("highscorename", "");
    }

    public static void a(Context context, String str) {
        SharedPreferences.Editor edit = d(context).edit();
        edit.putString("highscorename", str);
        edit.commit();
    }

    public static ArrayList a(Context context, int i) {
        return b(d(context).getString("hslist" + i, ""));
    }

    private static ArrayList b(String str) {
        String[] split = str.split("/");
        ArrayList arrayList = new ArrayList(20);
        for (int i = 0; i < split.length - 1; i++) {
            try {
                arrayList.add(new u(split[i], Integer.parseInt(split[i + 1])));
            } catch (NumberFormatException e) {
            }
        }
        return arrayList;
    }

    public static void a(Context context, ArrayList arrayList, int i) {
        a(context, arrayList, "hslist" + i);
    }

    private static void a(Context context, ArrayList arrayList, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                u uVar = (u) arrayList.get(i2);
                stringBuffer.append(uVar.a);
                stringBuffer.append("/");
                stringBuffer.append(uVar.b);
                stringBuffer.append("/");
                i = i2 + 1;
            } else {
                SharedPreferences.Editor edit = d(context).edit();
                edit.putString(str, stringBuffer.toString());
                edit.commit();
                return;
            }
        }
    }

    public static String a() {
        String string = Settings.System.getString(b.getContentResolver(), "android_id");
        if (string != null) {
            return string;
        }
        String string2 = d(b).getString("customerid", "");
        if (string2.length() != 0) {
            return string2;
        }
        String l = Long.toString(System.nanoTime(), 36);
        SharedPreferences.Editor edit = d(b).edit();
        edit.putString("customerid", l);
        edit.commit();
        return l;
    }

    public static void b(Context context, int i) {
        SharedPreferences.Editor edit = d(context).edit();
        edit.putInt("messageOfDayNumber", i);
        edit.commit();
    }

    public static int b(Context context) {
        return d(context).getInt("messageOfDayNumber", -1);
    }

    public static int c(Context context) {
        return d(context).getInt("freeLevels", 20000);
    }

    public static int a(int i) {
        int b2 = b();
        if (b2 >= i) {
            return b2;
        }
        SharedPreferences.Editor edit = a.edit();
        edit.putInt("expiredVersionCode", i);
        edit.commit();
        return i;
    }

    public static int b() {
        return a.getInt("expiredVersionCode", -1);
    }

    public static String c() {
        return a.getString("onlineProperties", "");
    }

    public static void a(String str) {
        SharedPreferences.Editor edit = a.edit();
        edit.putString("onlineProperties", str);
        edit.commit();
    }
}
