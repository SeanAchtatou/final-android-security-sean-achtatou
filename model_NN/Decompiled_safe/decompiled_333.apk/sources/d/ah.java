package d;

import android.graphics.Rect;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ProGuard */
public final class ah {
    ArrayList a = new ArrayList();
    List b = Collections.unmodifiableList(this.a);

    public final List a(av avVar, ArrayList arrayList) {
        ArrayList arrayList2 = this.a;
        arrayList2.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            av avVar2 = (av) arrayList.get(i);
            if (avVar2 != avVar) {
                if (Rect.intersects(avVar.z(), avVar2.z())) {
                    arrayList2.add(avVar2);
                }
            }
        }
        return this.b;
    }
}
