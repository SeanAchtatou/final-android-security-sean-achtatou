package d;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/* compiled from: ProGuard */
public final class cc {
    private static Context a;

    public static boolean a(int i) {
        return PreferenceManager.getDefaultSharedPreferences(a).contains(cb.a().a(i));
    }

    public static void a(int i, String str) {
        if (!a(i)) {
            String a2 = cb.a().a(i);
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(a).edit();
            edit.putString(a2, str);
            edit.commit();
        }
    }

    public static void a(int i, int i2) {
        String a2 = cb.a().a(i);
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(a).edit();
        edit.putInt(a2, i2);
        edit.commit();
    }

    public static void a(String str, String str2) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(a).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public static void b(int i) {
        String a2 = cb.a().a(i);
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(a).edit();
        edit.putBoolean(a2, true);
        edit.commit();
    }

    public static String c(int i) {
        return PreferenceManager.getDefaultSharedPreferences(a).getString(cb.a().a(i), "");
    }

    public static String b(String str, String str2) {
        return PreferenceManager.getDefaultSharedPreferences(a).getString(str, str2);
    }

    public static int d(int i) {
        return PreferenceManager.getDefaultSharedPreferences(a).getInt(cb.a().a(i), 0);
    }

    public static boolean e(int i) {
        return PreferenceManager.getDefaultSharedPreferences(a).getBoolean(cb.a().a(i), false);
    }

    public static void a(Context context) {
        a = context;
    }
}
