package d;

import android.content.Context;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;

/* compiled from: ProGuard */
public class s {
    String a;
    private final ArrayList b = new ArrayList();
    private final ArrayList c = new ArrayList();

    /* renamed from: d  reason: collision with root package name */
    private final ArrayList f51d = new ArrayList();
    private final ArrayList e = new ArrayList();
    private String f = "oijeve923487vj9js4pod948bxmvnbqjyrgt";
    private final Context g;
    private boolean h;
    private final u i = new u("None", 0);
    private t j;
    private final int k;
    private final int l;
    private final int m;

    public s(Context context, int i2, int i3, int i4) {
        this.g = context;
        this.k = i2;
        this.l = i3;
        this.m = i4;
        this.a = Long.toString(System.nanoTime(), 36);
    }

    public final void a() {
        this.f51d.addAll(j.a(this.g, this.k));
        String str = "http://games.martineriksen.net/airattack/php/getLastDayTop15.php?android=true" + h();
        try {
            new r(this, new URL("http://games.martineriksen.net/airattack/php/getAllTimeTop10.php?android=true" + h()), 0).start();
            new r(this, new URL(str), 2).start();
        } catch (MalformedURLException e2) {
            a(true);
            Log.e(getClass().getName(), "Url: url", e2);
        }
    }

    public final synchronized ArrayList b() {
        return this.b;
    }

    public final synchronized ArrayList c() {
        return this.c;
    }

    public final synchronized u d() {
        u uVar;
        if (this.c.size() > 0) {
            uVar = (u) this.c.get(0);
        } else {
            uVar = this.i;
        }
        return uVar;
    }

    public final ArrayList e() {
        return this.f51d;
    }

    public final synchronized void a(String str, int i2) {
        String replaceAll = str.substring(0, Math.min(str.length(), 20)).replaceAll("[^\\w ]", "_");
        if (replaceAll.trim().length() == 0) {
            replaceAll = "anonymous";
        }
        u uVar = new u(replaceAll, i2);
        a(this.f51d, uVar);
        a(this.e, uVar);
        a(this.b, uVar);
        a(this.c, uVar);
        j.a(this.g, this.f51d, this.k);
        a(uVar, "submitHighScore.php?android=true");
    }

    private static void a(ArrayList arrayList, u uVar) {
        if (arrayList.size() < 20 || ((u) arrayList.get(arrayList.size() - 1)).b < uVar.b) {
            if (arrayList.size() >= 20) {
                arrayList.remove(arrayList.size() - 1);
            }
            arrayList.add(uVar);
            Collections.sort(arrayList);
        }
    }

    /* access modifiers changed from: private */
    public String h() {
        return "&dif=" + this.k + "&sessionid=" + this.a + "&username=" + b(j.a()) + "&country=" + b(w.a().b()) + "&versiontype=" + cb.a().l + "_" + cb.a().j + "&width=" + this.l + "&height=" + this.m;
    }

    /* access modifiers changed from: private */
    public static String b(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            Log.e(s.class.getName(), "UTF-8 encoding not supported on this platform", e2);
            return str + "&error=UTF8_Not_Available";
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(ArrayList arrayList) {
        this.b.clear();
        this.b.addAll(arrayList);
        Collections.sort(this.b);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(ArrayList arrayList) {
        this.c.clear();
        this.c.addAll(arrayList);
        Collections.sort(this.c);
    }

    /* access modifiers changed from: private */
    public String c(String str) {
        byte[] bArr;
        try {
            byte[] bytes = (str + this.f).getBytes("UTF-8");
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.reset();
                instance.update(bytes);
                bArr = instance.digest();
            } catch (NoSuchAlgorithmException e2) {
                e2.printStackTrace();
                bArr = null;
            }
            String a2 = a(bArr);
            if (a2.length() == 32) {
                return "&checksum=" + a2;
            }
            throw new RuntimeException("bad checksum computation!!");
        } catch (UnsupportedEncodingException e3) {
            Log.e(s.class.getName(), "UTF-8 encoding not supported on this platform", e3);
            return "&error=UTF8_Not_Available";
        }
    }

    private static String a(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        String[] strArr = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        StringBuffer stringBuffer = new StringBuffer(bArr.length * 2);
        for (int i2 = 0; i2 < bArr.length; i2++) {
            stringBuffer.append(strArr[(byte) (((byte) (((byte) (bArr[i2] & 240)) >>> 4)) & 15)]);
            stringBuffer.append(strArr[(byte) (bArr[i2] & 15)]);
        }
        return new String(stringBuffer);
    }

    public final synchronized void a(boolean z) {
        this.h = z;
    }

    public final synchronized boolean f() {
        return this.h;
    }

    private synchronized void a(u uVar, String str) {
        t tVar = new t(this, uVar, str);
        tVar.start();
        this.j = tVar;
    }

    public final synchronized void g() {
        if (this.j != null) {
            this.j.a();
        }
    }
}
