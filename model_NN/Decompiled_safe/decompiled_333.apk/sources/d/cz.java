package d;

import android.graphics.Canvas;
import com.google.ads.R;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class cz {
    public int a = 26;
    public int b = 20;
    public int c = 16;

    /* renamed from: d  reason: collision with root package name */
    public int f37d = 14;
    public int e = 10;
    public int f;
    public final boolean g;
    public final float h;
    private final String i;
    private final String[] j;
    private final String[] k;
    private String l;
    private int m;
    private final int n;

    public cz(int i2) {
        int i3;
        String a2;
        if (b.a) {
            i3 = this.c;
        } else {
            i3 = this.b;
        }
        this.f = i3;
        StringBuilder append = new StringBuilder().append("v").append(cb.a().j).append(" ");
        if (b.a) {
            a2 = cb.a().a((int) R.string.ads);
        } else {
            a2 = cb.a().a((int) R.string.full);
        }
        this.i = append.append(a2).toString();
        this.j = new String[]{" ", "*******  Idea and Coding  *******", "Martin Eriksen", "games.martineriksen.net", "airattackthegame@gmail.com", " ", "*******  Android Coding  *******", "Alex Berg", "airattackthegame@gmail.com"};
        this.k = new String[]{" ", "*******  Sound Effects  *******", "Parag Oswal", "www.paragoswal.com", " ", " ", "*******  Graphics  *******", "Anthony LoPrimo", "AvlAdmn@gmail.com", "http://www.freewebs.com/", "chaosdragon11590.htm"};
        this.l = "0";
        this.m = 0;
        this.n = cb.a().s().getWidth() / 2;
        this.h = ((float) i2) / 320.0f;
        this.a = (int) (((float) this.a) * this.h);
        this.b = (int) (((float) this.b) * this.h);
        this.c = (int) (((float) this.c) * this.h);
        this.f37d = (int) (((float) this.f37d) * this.h);
        this.e = (int) (((float) this.e) * this.h);
        this.f = (int) (((float) this.f) * this.h);
        if (i2 <= 240) {
            this.a = 17;
            this.b = 14;
            this.g = true;
            return;
        }
        this.g = false;
    }

    public final void a(Canvas canvas, s sVar, String str) {
        if (sVar != null) {
            int a2 = ea.a(canvas, cb.a().a((int) R.string.highscore_online_top_20_scores_dash) + " " + str, this.a, this.a + 0, canvas.getWidth() / 2) - 6;
            if (sVar.f()) {
                ea.a(canvas, cb.a().a((int) R.string.highscore_unable_to_access_online_highscore), this.b, canvas.getHeight() / 4, canvas.getWidth() / 2);
                return;
            }
            a(canvas, sVar.b(), this.f, 20, 0, canvas.getWidth(), a2);
        }
    }

    public final void b(Canvas canvas, s sVar, String str) {
        if (sVar != null) {
            int a2 = ea.a(canvas, cb.a().a((int) R.string.highscore_todays_greatest_scores_dash) + " " + str, this.a, this.a + 0, canvas.getWidth() / 2) - 6;
            if (sVar.f()) {
                ea.a(canvas, cb.a().a((int) R.string.highscore_unable_to_access_online_highscore), this.b, canvas.getHeight() / 4, canvas.getWidth() / 2);
                return;
            }
            a(canvas, sVar.c(), this.f, 20, 0, canvas.getWidth(), a2);
        }
    }

    public final void c(Canvas canvas, s sVar, String str) {
        Canvas canvas2 = canvas;
        a(canvas2, sVar.e(), this.f, 20, 0, canvas.getWidth(), ea.a(canvas, cb.a().a((int) R.string.highscore_local_highscores_dash) + " " + str, this.a, this.a + 0, canvas.getWidth() / 2) - 6);
    }

    private static void a(Canvas canvas, ArrayList arrayList, int i2, int i3, int i4, int i5, int i6) {
        int i7 = (i5 / 2) - 20;
        int i8 = i7 - ((int) (((double) i2) * 1.8d));
        int min = Math.min(arrayList.size(), i3);
        int min2 = Math.min(arrayList.size(), i3 / 2);
        int i9 = i4 + 10;
        int i10 = i9 + i7;
        int i11 = 0;
        int i12 = i6;
        while (i11 < min2) {
            ea.a(canvas, ((u) arrayList.get(i11)).a, i2, i12, i9, i8);
            i11++;
            i12 = ea.b(canvas, Integer.toString(((u) arrayList.get(i11)).b), i2, i12, i10);
        }
        int i13 = ((i4 + i5) - i7) - 10;
        int i14 = (i4 + i5) - 10;
        int i15 = min2;
        int i16 = i6;
        while (i15 < min) {
            ea.a(canvas, ((u) arrayList.get(i15)).a, i2, i16, i13, i8);
            i15++;
            i16 = ea.b(canvas, Integer.toString(((u) arrayList.get(i15)).b), i2, i16, i14);
        }
    }

    public final void a(Canvas canvas, int i2) {
        if (!this.g) {
            ea.b(canvas, this.i, this.f37d, i2, canvas.getWidth() - 1);
        }
    }

    public final void d(Canvas canvas, s sVar, String str) {
        int width = canvas.getWidth();
        int i2 = width / 2;
        int i3 = width / 4;
        int a2 = ea.a(canvas, cb.a().a((int) R.string.welcome_to_air_attack), this.a, (canvas.getHeight() / 10) + 0, i2);
        String[] strArr = b.a ? new String[0] : new String[]{"Your hill is being invaded by the enemy", "Defeat is certain", "Survive for as long as you can"};
        if (!b.a) {
            a2 = ea.a(canvas, strArr, this.b, a2, i2);
        }
        int a3 = ea.a(canvas, " ", this.b, a2, i2);
        u d2 = sVar.d();
        int a4 = ea.a(canvas, cb.a().a((int) R.string.highscore_todays_greatest_at) + " " + str + ": " + d2.a + "  " + d2.b, this.b, a3, i2);
        if (!sVar.f()) {
            ea.a(canvas, cb.a().a((int) R.string.highscore_recent_top_scores), this.b, a4, i3);
            int a5 = ea.a(canvas, cb.a().a((int) R.string.highscore_all_time_topscores), this.b, a4, i3 + i2);
            a(canvas, sVar.c(), this.f37d, 10, 0, i2, a5);
            a(canvas, sVar.b(), this.f37d, 10, i2, i2, a5);
        }
    }

    public final void a(Canvas canvas, b bVar) {
        int b2 = ea.b(12);
        int i2 = bVar.n;
        int width = canvas.getWidth() - b2;
        for (int i3 = 1; i3 < i2; i3++) {
            ea.a(canvas, 12, width, 1);
            width -= b2;
        }
        ea.a(canvas, 1, 0, 0);
        int k2 = bVar.k();
        if (this.m != k2) {
            this.m = k2;
            this.l = Integer.toString(k2);
        }
        ea.a(canvas, this.l, 16, 16, this.n);
    }

    public final void a(Canvas canvas) {
        ea.a(canvas, (int) R.string.message_paused_sub, this.b, ea.a(canvas, (int) R.string.message_paused, this.a, (canvas.getHeight() / 2) - this.a, canvas.getWidth() / 2), canvas.getWidth() / 2);
    }
}
