package d;

/* compiled from: ProGuard */
public final class u implements Comparable {
    public String a;
    public int b;
    int c = 25;

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        u uVar = (u) obj;
        if (uVar.b < this.b) {
            return -1;
        }
        return uVar.b > this.b ? 1 : 0;
    }

    public u(String str, int i) {
        this.a = str;
        this.b = i;
    }

    public final String toString() {
        return this.a + a((this.c - this.a.length()) - ("" + this.b).length()) + this.b;
    }

    private static String a(int i) {
        if (i <= 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer(i);
        for (int i2 = 0; i2 < i; i2++) {
            stringBuffer.append(" ");
        }
        return stringBuffer.toString();
    }
}
