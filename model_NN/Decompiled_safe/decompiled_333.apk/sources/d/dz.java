package d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import java.lang.reflect.Array;

/* compiled from: ProGuard */
public final class dz {
    private static final Rect a = new Rect();
    private static final boolean[][] n = ((boolean[][]) Array.newInstance(Boolean.TYPE, 71, 36));
    private static final int[] o = new int[20000];
    private final Bitmap b;
    private final int[] c;

    /* renamed from: d  reason: collision with root package name */
    private final Bitmap f44d;
    private final int e;
    private final int f;
    private final Canvas g;
    private eb h;
    private final Matrix i = new Matrix();
    private final Paint j = new Paint();
    private final Bitmap k;
    private final Canvas l;
    private boolean m;

    public dz(int i2, int i3) {
        this.e = i2;
        this.f = i3;
        this.b = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_4444);
        this.k = Bitmap.createBitmap(i2, i3, Bitmap.Config.RGB_565);
        this.g = new Canvas(this.b);
        this.l = new Canvas(this.k);
        this.f44d = cb.a().b(i2, i3);
        this.c = new int[(i2 * i3)];
        this.h = new eb(i2, i3, this);
        this.m = false;
    }

    public final void a(boolean z) {
        this.m = z;
    }

    public final boolean a(int i2, int i3) {
        int i4 = this.e;
        int i5 = this.f;
        if (i2 < 0 || i3 < 0 || i2 >= i4) {
            return true;
        }
        if (i3 >= i5) {
            return this.m;
        }
        return this.c[(i4 * i3) + i2] == 0;
    }

    public final int b(int i2, int i3) {
        int i4 = this.e;
        int i5 = this.f;
        int[] iArr = this.c;
        if (i2 < 0 || i3 < 0 || i2 >= i4 || i3 >= i5) {
            return i3;
        }
        int i6 = (i3 * i4) + i2;
        if (iArr[i6] != 0) {
            return i3;
        }
        int i7 = i6;
        for (int i8 = i3 + 1; i8 < i5; i8++) {
            if (iArr[i7] != 0) {
                return i8 - 1;
            }
            i7 += i4;
        }
        return i5 - 1;
    }

    public final float a(int i2, int i3, int i4) {
        int i5 = this.e;
        int i6 = this.f;
        int[] iArr = this.c;
        if (i2 >= 0 && i2 < i5 && i3 >= 0 && i3 < i6) {
            if (iArr[(i3 * i5) + i2] == 0) {
                return (float) i3;
            }
            int i7 = i3 - 1;
            int i8 = i3 + 1;
            int i9 = (i7 * i5) + i2;
            int i10 = i7;
            int i11 = i8;
            int i12 = (i8 * i5) + i2;
            int i13 = i11;
            for (int i14 = 0; i14 < i4; i14++) {
                if (i13 < i6 && iArr[i12] == 0) {
                    return (float) i13;
                }
                if (i10 >= 0 && iArr[i9] == 0) {
                    return (float) i10;
                }
                i10--;
                i13++;
                i9 -= i5;
                i12 += i5;
            }
        }
        return -1.0f;
    }

    public final void a(Canvas canvas) {
        a.set(0, this.h.a, this.e, this.f);
        ea.a(canvas, this.k);
    }

    private void a(int i2, int i3, int i4, int i5) {
        Rect rect = a;
        rect.set(i2, i3, i4, i5);
        Canvas canvas = this.l;
        canvas.drawBitmap(this.f44d, rect, rect, (Paint) null);
        canvas.drawBitmap(this.b, rect, rect, (Paint) null);
    }

    public final int c(int i2, int i3) {
        return this.c[(this.e * i3) + i2];
    }

    /* access modifiers changed from: package-private */
    public final int[] a() {
        return this.c;
    }

    private void b(int i2, int i3, int i4, int i5) {
        int i6;
        int i7 = this.e;
        int i8 = this.f;
        if (i2 < i7 && i3 < i8 && i5 > 0 && i4 > 0) {
            int i9 = i2 > 0 ? i2 : 0;
            if (i3 > 0) {
                i6 = i3;
            } else {
                i6 = 0;
            }
            int i10 = i4 + i2 < i7 ? i4 : i7 - i2;
            int i11 = i5 + i3 < i8 ? i5 : i8 - i3;
            this.b.getPixels(this.c, (i6 * i7) + i9, i7, i9, i6, i10, i11);
            a(i9, i6, i9 + i10, i6 + i11);
        }
    }

    public final void a(int i2, int i3, int i4, int[] iArr) {
        int i5 = this.f;
        int i6 = this.e;
        if (i2 < 0 || i2 + i4 >= i6 || i3 < 0 || i3 >= i5) {
            throw new IllegalArgumentException("locX: " + i2 + ", locY: " + i3 + ", houseWidth: " + i4);
        }
        int[] iArr2 = this.c;
        for (int i7 = i3; i7 < i5; i7++) {
            int i8 = (i7 * i6) + i2;
            int i9 = 0;
            while (i9 < i4) {
                if (iArr2[i8] == 0) {
                    iArr2[i8] = iArr[n.a(iArr.length)];
                }
                i9++;
                i8++;
            }
        }
        this.b.setPixels(iArr2, (i3 * i6) + i2, i6, i2, i3, i4, i5 - i3);
    }

    public final void b(int i2, int i3, int i4) {
        int i5;
        this.h.a(i3, i4 != 0);
        Paint paint = this.j;
        paint.setColor(i4);
        this.g.drawRect((float) i2, (float) i3, (float) (i2 + 3), (float) (i3 + 3), paint);
        if (i2 > 0) {
            i5 = i2;
        } else {
            i5 = 0;
        }
        b(i5, i3, 3, 3);
    }

    public final void a(int[] iArr) {
        this.h.b();
        int i2 = this.e;
        int i3 = this.f;
        int[] iArr2 = this.c;
        cb.a().c(i2, i3).getPixels(iArr2, 0, i2, 0, 0, i2, i3);
        int i4 = 0;
        int i5 = 0;
        while (i4 < i3) {
            int i6 = i5;
            for (int i7 = 0; i7 < i2; i7++) {
                if (iArr[i7] > i4) {
                    iArr2[i6] = 0;
                }
                i6++;
            }
            i4++;
            i5 = i6;
        }
        this.b.setPixels(iArr2, 0, i2, 0, 0, i2, i3);
    }

    public final void c(int i2, int i3, int i4) {
        this.h.a(i3, i4 != 0);
        this.b.setPixel(i2, i3, i4);
        this.c[(this.e * i3) + i2] = i4;
    }

    public final void a(av avVar) {
        int v = (int) avVar.v();
        int w = (int) avVar.w();
        this.h.a(w, true);
        Bitmap a2 = cb.a().a(avVar);
        this.g.drawBitmap(a2, ea.a(avVar, (float) v, (float) w, avVar.f(), avVar.D(), a2), null);
        if (avVar.D() == 0.0f) {
            b(v, w, a2.getWidth(), a2.getHeight());
            return;
        }
        int width = a2.getWidth() + a2.getHeight();
        int i2 = width / 2;
        b(((int) avVar.s()) - i2, ((int) avVar.t()) - i2, width, width);
    }

    public final void a(Bitmap bitmap, float f2, int i2, int i3) {
        this.h.a(i3, true);
        this.i.reset();
        this.i.preTranslate((float) i2, (float) i3);
        this.i.preScale(f2, f2);
        this.g.drawBitmap(bitmap, this.i, null);
        this.b.getPixels(this.c, 0, this.e, 0, 0, this.e, this.f);
    }

    public final void b() {
        Canvas canvas = this.l;
        canvas.drawBitmap(this.f44d, 0.0f, 0.0f, (Paint) null);
        canvas.drawBitmap(this.b, 0.0f, 0.0f, (Paint) null);
    }

    public final void a(int i2, int i3, int i4, int i5, boolean z) {
        this.h.a(i3 - i4, false);
        int i6 = this.e;
        int i7 = this.f;
        if (i2 - i4 < i6 && i3 - i4 < i7 && i2 + i4 >= 0 && i3 + i4 >= 0) {
            int i8 = i4 * i4;
            int[] iArr = this.c;
            int max = Math.max(0, i2 - i4);
            int max2 = Math.max(0, i3 - i4);
            int min = Math.min(i6, i2 + i4) - max;
            int min2 = Math.min(i7, i3 + i4) - max2;
            int i9 = (max2 * i6) + max;
            int i10 = max2 + min2;
            for (int i11 = max2; i11 < i10; i11++) {
                int i12 = i11 - i3;
                int i13 = i12 * i12;
                int i14 = i4;
                while ((i14 * i14) + i13 > i8 && i14 > 0) {
                    i14--;
                }
                int i15 = i2 - i14;
                if (i15 <= 0) {
                    i15 = 0;
                }
                int i16 = i14 + i2;
                if (i16 >= i6) {
                    i16 = i6;
                }
                int i17 = (i11 * i6) + i15;
                while (i15 < i16) {
                    int i18 = iArr[i17];
                    if (i5 == 1) {
                        iArr[i17] = 0;
                    } else if (i5 == 2) {
                        if (i18 != 0) {
                            iArr[i17] = -22016;
                        }
                    } else if (i5 != 0) {
                        throw new RuntimeException("unexpected OP: " + i5);
                    } else if (i18 != 0) {
                        iArr[i17] = ((((int) (((double) ((i18 & 255) >> 0)) * 0.7d)) << 0) + ((((int) (((double) ((16711680 & i18) >> 16)) * 0.7d)) << 16) + (((int) (((double) ((65280 & i18) >> 8)) * 0.7d)) << 8))) - 16777216;
                    }
                    i15++;
                    i17++;
                }
            }
            int i19 = 0;
            if (i5 == 1) {
                i19 = a(iArr, i2, i3, i4);
            }
            if (i19 <= i4 || !z) {
                this.b.setPixels(iArr, i9, i6, max, max2, min, min2);
                a(max, max2, max + min, i10);
                return;
            }
            int max3 = Math.max(0, i2 - i19);
            int max4 = Math.max(0, i3 - i19);
            int min3 = Math.min(i6, i2 + i19) - max3;
            int min4 = Math.min(i7, i3 + i19) - max4;
            this.b.setPixels(iArr, max3 + (max4 * i6), i6, max3, max4, min3, min4);
            a(max3, max4, max3 + min3, max4 + min4);
        }
    }

    private int a(int[] iArr, int i2, int i3, int i4) {
        if (i4 > 20) {
            return 0;
        }
        int i5 = i4 + 15;
        int i6 = (i5 * 2) + 1;
        int i7 = i5 + 1;
        int i8 = i2 - i5;
        int i9 = i3 - i5;
        boolean[][] zArr = n;
        int i10 = i6 - 1;
        int i11 = i7 - 1;
        for (int i12 = 1; i12 < i10; i12++) {
            for (int i13 = 1; i13 < i11; i13++) {
                zArr[i12][i13] = false;
            }
            zArr[i12][0] = true;
            zArr[i12][i11] = true;
        }
        for (int i14 = 0; i14 < i7; i14++) {
            zArr[0][i14] = true;
            zArr[i10][i14] = true;
        }
        int i15 = i7 - 2;
        int i16 = 0;
        int i17 = 1;
        while (i17 <= 15) {
            int i18 = i16 + 1;
            o[i16] = i17;
            int i19 = i18 + 1;
            o[i18] = i15;
            int i20 = i19 + 1;
            o[i19] = i10 - i17;
            o[i20] = i15;
            i17++;
            i16 = i20 + 1;
        }
        int i21 = 1;
        int i22 = i16;
        while (i21 < i10) {
            int i23 = i22 + 1;
            o[i22] = i21;
            o[i23] = 1;
            i21++;
            i22 = i23 + 1;
        }
        int i24 = i6 - 2;
        int i25 = 1;
        while (i25 < i11) {
            int i26 = i22 + 1;
            o[i22] = 1;
            int i27 = i26 + 1;
            o[i26] = i25;
            int i28 = i27 + 1;
            o[i27] = i24;
            o[i28] = i25;
            i25++;
            i22 = i28 + 1;
        }
        int i29 = this.e;
        int i30 = this.f;
        int length = o.length - 8;
        int i31 = i22;
        int i32 = 0;
        int i33 = i31;
        while (i33 > i32) {
            int i34 = i32 + 1;
            int i35 = o[i32];
            int i36 = i34 + 1;
            int i37 = o[i34];
            if (!zArr[i35][i37]) {
                int i38 = i35 + i8;
                int i39 = i37 + i9;
                if (i38 >= 0 && i39 >= 0 && i38 < i29 && i39 < i30 && iArr[i38 + (i39 * i29)] != 0) {
                    zArr[i35][i37] = true;
                    if (i33 < length) {
                        int i40 = i33 + 1;
                        o[i33] = i35;
                        int i41 = i40 + 1;
                        o[i40] = i37 - 1;
                        int i42 = i41 + 1;
                        o[i41] = i35;
                        int i43 = i42 + 1;
                        o[i42] = i37 + 1;
                        int i44 = i43 + 1;
                        o[i43] = i35 - 1;
                        int i45 = i44 + 1;
                        o[i44] = i37;
                        int i46 = i45 + 1;
                        o[i45] = i35 + 1;
                        o[i46] = i37;
                        i33 = i46 + 1;
                    }
                }
            }
            i32 = i36;
        }
        int i47 = this.e;
        int i48 = this.f;
        for (int i49 = 1; i49 < i7; i49++) {
            int i50 = i49 + i9;
            int i51 = this.e * i50;
            for (int i52 = 1; i52 < i6; i52++) {
                int i53 = i52 + i8;
                if (!n[i52][i49] && i53 >= 0 && i50 >= 0 && i53 < i47 && i50 < i48 && iArr[i51 + i53] != 0) {
                    iArr[i53 + i51] = 0;
                }
            }
        }
        return i5;
    }

    public final void c() {
        this.h.a();
    }

    public final int d() {
        return this.h.a;
    }
}
