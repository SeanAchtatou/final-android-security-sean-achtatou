package d;

import android.app.AlertDialog;
import com.google.ads.R;
import dk.logisoft.airattack.AirAttackActivity;

/* compiled from: ProGuard */
public final class ck {
    /* access modifiers changed from: private */
    public static AirAttackActivity a;

    public static void a(AirAttackActivity airAttackActivity) {
        a = airAttackActivity;
    }

    public static AlertDialog a(AirAttackActivity airAttackActivity, boolean z) {
        int i;
        AlertDialog.Builder title = new AlertDialog.Builder(airAttackActivity).setIcon(17301543).setTitle((int) R.string.expired_title);
        if (z) {
            i = R.string.expired_pleaseupdate_warning;
        } else {
            i = R.string.expired_pleaseupdate;
        }
        return title.setMessage(i).setCancelable(false).setPositiveButton((int) R.string.button_ok, new cl(airAttackActivity, z)).create();
    }

    public static void a() {
        a.runOnUiThread(new cm());
    }
}
