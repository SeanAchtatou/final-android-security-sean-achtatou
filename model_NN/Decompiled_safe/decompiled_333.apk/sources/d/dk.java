package d;

import android.app.Activity;
import android.app.AlertDialog;
import com.google.ads.R;
import dk.logisoft.airattack.AirAttackActivity;

/* compiled from: ProGuard */
public final class dk {
    /* access modifiers changed from: private */
    public static AirAttackActivity a;

    public static void a(AirAttackActivity airAttackActivity) {
        a = airAttackActivity;
    }

    public static AlertDialog a(Activity activity) {
        return new AlertDialog.Builder(activity).setIcon(17301659).setTitle("Rate the Game?").setMessage("If you think this is a 5 star game please consider helping us out by rating it on the market. Ratings and reviews help spreading the word and encourages us to keep working on the game.").setCancelable(true).setPositiveButton("Rate", new dm(activity)).setNeutralButton("No Thanks", new dl()).create();
    }

    public static void a() {
        int d2 = cc.d(R.string.prefKeyRateGameCounter);
        cc.a((int) R.string.prefKeyRateGameCounter, d2 + 1);
        boolean e = cc.e(R.string.prefKeyShownRateGameDialog);
        if (d2 >= 2 && !e) {
            a.runOnUiThread(new dn());
        }
    }
}
