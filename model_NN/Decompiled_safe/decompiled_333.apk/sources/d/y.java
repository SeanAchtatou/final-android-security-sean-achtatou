package d;

import android.graphics.Canvas;

/* compiled from: ProGuard */
public abstract class y extends av {
    private static final int i = cb.a().f;
    private static final int j = cb.a().e;
    private q[] F = {new q(), new q(), new q(), new q()};
    private int G;
    protected boolean a;
    float b;
    float c;

    /* renamed from: d  reason: collision with root package name */
    float f54d;
    float e;
    boolean f;
    protected int g;
    protected int h;
    private int k;
    private float l;
    private int m;
    private boolean n;

    public final void a(k kVar, int i2, int i3, b bVar, float f2, float f3) {
        a(kVar, (float) i2, (float) i3, false, bVar, 15, f2, f3);
        this.z = true;
        this.B = false;
        int round = Math.round(((float) this.t) - (((float) this.t) * 0.7f));
        int round2 = Math.round(((float) this.u) - (((float) this.u) * 0.7f));
        this.F[0].a((float) (round / 2), (float) (round2 / 2));
        this.F[1].a((float) (round / 2), (float) (this.u - round2));
        this.F[2].a((float) (this.t - round), (float) (round2 / 2));
        this.F[3].a((float) (this.t - round), (float) (this.u - round2));
        a(this.F);
        this.a = false;
        this.l = 1.0f;
        this.f = false;
        this.m = 0;
        this.g = 0;
        this.h = 0;
        this.n = false;
        if (v() + ((float) A()) > ((float) this.p.b())) {
            this.l = -1.0f;
        }
    }

    public void a() {
        this.f = true;
        c();
    }

    public final void a(be beVar) {
        if (this.a) {
            a_();
        } else if (this.m != 0 && this.h == 0) {
            by.b().j();
            this.h = l();
            beVar.b(true);
            this.s.r.a(beVar.s(), beVar.t(), beVar.o, 10);
        } else if (n.a(j) == 0) {
            c();
        } else {
            a_();
        }
    }

    /* access modifiers changed from: protected */
    public void a_() {
        b(true);
        this.s.r.a(s(), t(), this.o, this.v);
        for (int i2 = 0; i2 < 2; i2++) {
            this.s.r.b(s(), t(), this.v * 3);
        }
        by.b().g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.ai.a(d.k, int, int, d.b, boolean):void
     arg types: [d.k, int, int, d.b, int]
     candidates:
      d.bh.a(d.k, float, float, boolean, d.b):void
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void
      d.ai.a(d.k, int, int, d.b, boolean):void */
    public void c() {
        this.z = false;
        this.B = true;
        this.a = true;
        switch (n.a(5)) {
            case 0:
                this.f54d = -0.01f;
                break;
            case 1:
                this.f54d = 0.01f;
                break;
        }
        this.c = 0.01f;
        this.e = (((float) (n.a(3) + 1)) / 10.0f) + 1.0f;
        this.b = 1.0f;
        if ((this instanceof an) && ((an) this).k) {
            this.b = 0.0f;
        }
        if (n.a(i) == 0 && !this.f) {
            ai aiVar = (ai) this.s.s.a(ai.class);
            aiVar.a(this.p, (int) s(), (int) t(), this.s, true);
            this.s.a(aiVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0040, code lost:
        if (e(java.lang.Math.min(r0, 1.5f)) == 0) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d() {
        /*
            r7 = this;
            r6 = 1065353216(0x3f800000, float:1.0)
            r5 = 0
            r4 = 1
            r7.e()
            boolean r0 = r7.a
            if (r0 == 0) goto L_0x009f
            boolean r0 = r7.f
            if (r0 != 0) goto L_0x0024
            d.b r0 = r7.s
            d.bq r0 = r0.r
            float r1 = r7.s()
            float r2 = r7.t()
            int r3 = r7.A()
            int r3 = r3 / 4
            r0.b(r1, r2, r3)
        L_0x0024:
            float r0 = r7.b
            float r1 = r7.l
            float r0 = r0 * r1
            int r0 = r7.d(r0)
            if (r0 == 0) goto L_0x0042
            float r0 = r7.c
            float r1 = r7.e
            float r0 = r0 * r1
            r7.c = r0
            r1 = 1069547520(0x3fc00000, float:1.5)
            float r0 = java.lang.Math.min(r0, r1)
            int r0 = r7.e(r0)
            if (r0 != 0) goto L_0x004f
        L_0x0042:
            r7.b(r4)
            r0 = 3
            int r0 = d.n.a(r0)
            if (r0 != 0) goto L_0x0090
            r7.a_()
        L_0x004f:
            float r0 = r7.f54d
            float r1 = r7.e
            float r1 = r1 - r6
            r2 = 1092616192(0x41200000, float:10.0)
            float r1 = r1 / r2
            float r1 = r1 + r6
            float r0 = r0 * r1
            r7.f54d = r0
            r7.a(r0)
            float r0 = r7.s()
            d.k r1 = r7.p
            int r1 = r1.b()
            float r1 = (float) r1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x008c
            float r0 = r7.s()
            int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x008c
            float r0 = r7.t()
            int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x008c
            float r0 = r7.t()
            d.k r1 = r7.p
            int r1 = r1.a()
            float r1 = (float) r1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x008f
        L_0x008c:
            r7.b(r4)
        L_0x008f:
            return
        L_0x0090:
            d.k r0 = r7.p
            d.dz r0 = r0.c
            r0.a(r7)
            d.by r0 = d.by.b()
            r0.u()
            goto L_0x004f
        L_0x009f:
            float r0 = r7.l
            r7.d(r0)
            int r0 = r7.h
            if (r0 <= 0) goto L_0x008f
            int r0 = r7.h
            int r0 = r0 - r4
            r7.h = r0
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: d.y.d():void");
    }

    /* access modifiers changed from: protected */
    public final void e() {
        ba h2 = this.s.h(this);
        if (h2 != null) {
            h2.g();
        }
    }

    public final boolean f() {
        return this.l < 0.0f;
    }

    public final void a(Canvas canvas) {
        super.a(canvas);
        if (!this.a) {
            if (this.h != 0 && !C() && !this.a) {
                ea.a(canvas, (int) s(), ((int) w()) + 5, ((float) (l() - this.h)) / ((float) l()), -7829368);
            }
            int i2 = 0;
            for (int i3 = 0; i3 < this.m; i3++) {
                ea.a(canvas, 16, ((int) v()) + i2, (int) w());
                i2 += 6;
            }
            for (int i4 = 0; i4 < this.g; i4++) {
                ea.a(canvas, 14, ((int) v()) + i2, (int) w());
                i2 += 6;
            }
            if (this.n) {
                ea.a(canvas, 15, i2 + ((int) v()), (int) w());
            }
        }
    }

    public final void a(int i2) {
        if (i2 <= 0) {
            return;
        }
        if (i2 > 2) {
            throw new RuntimeException("flaklevel can be max 2 - is was: " + i2);
        }
        this.m = i2;
    }

    public void b(int i2) {
        if (i2 <= 0) {
            return;
        }
        if (i2 > 3) {
            throw new RuntimeException("firerate can only be set to 1 or 2 or 3. was: " + i2);
        }
        this.g = i2;
        c((int) (((double) this.k) / Math.pow(2.0d, (double) i2)));
    }

    public void g() {
        if (this.l < 0.0f) {
            this.l = 1.0f;
        } else {
            this.l = -1.0f;
        }
    }

    public final float h() {
        return this.l;
    }

    public final void i() {
        this.l /= 1000.0f;
    }

    private int l() {
        return (int) (215.0d - (35.0d * Math.pow(2.0d, (double) this.m)));
    }

    /* access modifiers changed from: protected */
    public final boolean j() {
        int i2 = this.G;
        this.G = i2 - 1;
        if (i2 > 0) {
            return false;
        }
        this.G = H();
        return true;
    }

    private int H() {
        return n.a(this.k) * 2;
    }

    public final void a(boolean z) {
        if (z) {
            this.n = true;
            this.r = (float) (((double) this.r) * 1.6d);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(int i2) {
        this.k = i2;
        this.G = H();
    }

    /* access modifiers changed from: protected */
    public final int k() {
        return this.k;
    }
}
