package d;

/* compiled from: ProGuard */
public final class ee {
    protected final Class a;
    private Object[] b;
    private int c;

    public ee(Class cls) {
        this(cls, 32);
    }

    public ee(Class cls, int i) {
        this.a = cls;
        this.b = new Object[i];
        a(i, 0);
    }

    public final Object a() {
        if (this.c == this.b.length) {
            int length = this.b.length * 2;
            int length2 = this.b.length;
            this.b = new Object[length];
            a(length - length2, length2);
        }
        Object obj = this.b[this.c];
        this.b[this.c] = null;
        this.c++;
        return obj;
    }

    public final void a(Object obj) {
        int i = this.c;
        if (i > 0) {
            int i2 = i - 1;
            this.c = i2;
            this.b[i2] = obj;
            return;
        }
        throw new RuntimeException("returned to pool but no objects checked out!");
    }

    private void a(int i, int i2) {
        Object[] objArr = this.b;
        int i3 = i2;
        while (i3 < i + i2) {
            try {
                objArr[i3] = this.a.newInstance();
                i3++;
            } catch (Exception e) {
                throw new RuntimeException("Unable to instantiate object with class: " + this.a.getName(), e);
            }
        }
    }
}
