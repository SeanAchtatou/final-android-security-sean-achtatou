package d;

import java.util.Random;

/* compiled from: ProGuard */
public final class o extends Random {
    private long a;
    private int b;

    public o() {
        this(System.nanoTime());
    }

    public o(long j) {
        super(j);
        if (j == 0) {
            throw new IllegalArgumentException("Seed zero not allowed");
        }
        this.a = j;
        this.b = (int) j;
    }

    /* access modifiers changed from: protected */
    public final int next(int i) {
        int i2 = this.b;
        int i3 = i2 ^ (i2 << 13);
        int i4 = i3 ^ (i3 >>> 7);
        int i5 = i4 ^ (i4 << 17);
        this.b = i5;
        return (int) (((long) i5) & ((1 << i) - 1));
    }

    public final int nextInt() {
        int i = this.b;
        int i2 = i ^ (i << 13);
        int i3 = i2 ^ (i2 >>> 7);
        int i4 = i3 ^ (i3 << 17);
        this.b = i4;
        return i4;
    }

    public final long nextLong() {
        long j = this.a;
        long j2 = j ^ (j << 21);
        long j3 = j2 ^ (j2 >>> 35);
        long j4 = j3 ^ (j3 << 4);
        this.a = j4;
        return j4;
    }

    public final int nextInt(int i) {
        int i2;
        if (i <= 0) {
            throw new IllegalArgumentException("n must be positive");
        }
        int i3 = this.b;
        int i4 = i3 ^ (i3 << 13);
        int i5 = i4 ^ (i4 >>> 7);
        int i6 = i5 ^ (i5 << 17);
        this.b = i6;
        if (((-i) & i) != i) {
            return (i6 & Integer.MAX_VALUE) % i;
        }
        if (i == 1) {
            return 0;
        }
        if ((-65536 & i) != 0) {
            i2 = 0 + 16;
        } else {
            i2 = 0;
        }
        if ((-16711936 & i) != 0) {
            i2 += 8;
        }
        if ((-252645136 & i) != 0) {
            i2 += 4;
        }
        if ((-858993460 & i) != 0) {
            i2 += 2;
        }
        if ((-1431655766 & i) != 0) {
            i2++;
        }
        int i7 = i - 1;
        int i8 = 0;
        int i9 = i6;
        for (int i10 = 32; i10 >= i2; i10 -= i2) {
            i8 ^= i9 & i7;
            i9 >>>= i2;
        }
        return i8;
    }

    public final float nextFloat() {
        int i = this.b;
        int i2 = i ^ (i << 13);
        int i3 = i2 ^ (i2 >>> 7);
        int i4 = i3 ^ (i3 << 17);
        this.b = i4;
        return ((float) ((int) (((long) i4) & 16777215))) / 1.6777216E7f;
    }

    public final double nextDouble() {
        long j = this.a;
        long j2 = j ^ (j << 21);
        long j3 = j2 ^ (j2 >>> 35);
        long j4 = j3 ^ (j3 << 4);
        this.a = j4;
        return ((double) (j4 & 9007199254740991L)) / 9.007199254740992E15d;
    }
}
