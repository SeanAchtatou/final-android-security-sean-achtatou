package d;

/* compiled from: ProGuard */
public class an extends y {
    int i;
    float j;
    boolean k;
    bh l;
    int m;
    int n;

    public void a(k kVar, int i2, int i3, b bVar) {
        a(kVar, i2, i3, bVar, 0.9f, 0.7f);
        this.r = bVar.f() * ((float) (n.a(12) + 25));
        this.m = 80;
        this.i = 0;
        this.k = false;
    }

    public final void d() {
        if (this.k) {
            super.e();
            int i2 = this.A;
            this.A = i2 - 1;
            if (i2 >= 30) {
                if (this.A % 10 == 0) {
                    this.j = ((float) (n.a(3) - 1)) / 1.5f;
                }
                if (w() + ((float) this.u) + this.j < ((float) this.q.d()) && w() + this.j > ((float) this.n)) {
                    e(this.j);
                }
            } else if (j()) {
                l();
            }
            if (this.A == 0) {
                I();
            }
            if (this.h > 0) {
                this.h--;
                return;
            }
            return;
        }
        super.d();
        if (!this.a) {
            float v = v();
            if (((float) A()) + v >= ((float) this.p.b()) && h() > 0.0f) {
                H();
                g();
            }
            if (v <= 0.0f && h() < 0.0f) {
                H();
                g();
            }
        }
    }

    private void H() {
        this.l = this.s.g();
        if (this.l != null) {
            this.k = true;
            this.A = 100;
        }
        if (h.a != null) {
            this.n = h.a.c();
        } else {
            this.n = 0;
        }
    }

    private void I() {
        this.k = false;
        this.A = 0;
    }

    /* access modifiers changed from: package-private */
    public void l() {
        throw new RuntimeException("not implemented! must be implemented by subclass!");
    }

    public final void c() {
        if (this.k) {
            I();
            i();
        }
        super.c();
    }

    public final void b(int i2) {
        if (i2 <= 0) {
            return;
        }
        if (i2 > 3) {
            throw new RuntimeException("firerate can only be set to 1 or 2 or 3. was: " + i2);
        }
        this.g = i2;
        for (int i3 = 0; i3 < i2; i3++) {
            c(k() / 2);
        }
    }
}
