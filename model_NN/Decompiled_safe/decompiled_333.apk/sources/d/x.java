package d;

import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/* compiled from: ProGuard */
final class x implements LocationListener {
    final /* synthetic */ Geocoder a;
    final /* synthetic */ LocationManager b;
    final /* synthetic */ w c;

    x(w wVar, Geocoder geocoder, LocationManager locationManager) {
        this.c = wVar;
        this.a = geocoder;
        this.b = locationManager;
    }

    public final void onLocationChanged(Location location) {
        this.c.a(location, this.a);
        this.b.removeUpdates(this);
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
