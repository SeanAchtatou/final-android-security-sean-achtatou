package d;

import android.view.KeyEvent;

/* compiled from: ProGuard */
public final class de {
    private static int[] a = new int[10];
    private static int[] b = new int[10];
    private static int c;

    public static void a(KeyEvent keyEvent) {
        if (c + 1 == a.length) {
            a = (int[]) ec.a(a);
            b = (int[]) ec.a(b);
        }
        a[c] = keyEvent.getAction();
        b[c] = keyEvent.getKeyCode();
        c++;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.ba.a(float, float):void
     arg types: [int, int]
     candidates:
      d.av.a(float, boolean):int
      d.av.a(int, int):void
      d.ba.a(float, float):void */
    public static void a(d dVar) {
        for (int i = 0; i < c; i++) {
            int i2 = b[i];
            ba baVar = dVar.a.o;
            if (baVar != null) {
                switch (i2) {
                    case 19:
                        baVar.a(0.0f, -1.0f);
                        continue;
                    case 21:
                        if (a[i] != 0) {
                            baVar.i();
                            break;
                        } else {
                            baVar.a(-10000);
                            continue;
                        }
                    case 22:
                        if (a[i] != 0) {
                            baVar.i();
                            break;
                        } else {
                            baVar.a(10000);
                            continue;
                        }
                }
            }
        }
        c = 0;
    }
}
