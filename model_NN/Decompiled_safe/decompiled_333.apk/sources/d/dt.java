package d;

import android.view.MotionEvent;

/* compiled from: ProGuard */
public final class dt {
    private static float[] a = new float[10];
    private static float[] b = new float[10];
    private static long[] c = new long[10];

    /* renamed from: d  reason: collision with root package name */
    private static int f43d;
    private static float e;
    private static float f;
    private static long g;

    public static void a(MotionEvent motionEvent) {
        if (f43d + 1 == a.length) {
            a = (float[]) ec.a(a);
            b = (float[]) ec.a(b);
            c = (long[]) ec.a(c);
        }
        a[f43d] = motionEvent.getX();
        b[f43d] = motionEvent.getY();
        c[f43d] = motionEvent.getEventTime();
        f43d++;
    }

    public static void a(d dVar) {
        if (dVar.a.o != null) {
            for (int i = 0; i < f43d; i++) {
                f += a[i];
                e += b[i];
            }
            if (f43d <= 0 || g + 200 >= c[f43d - 1]) {
                f = 0.0f;
                e = 0.0f;
            } else {
                float f2 = f;
                float f3 = e;
                if (dVar.a.o != null) {
                    dVar.a.o.b(f2, f3);
                }
                g = c[f43d - 1];
            }
        }
        f43d = 0;
    }
}
