package d;

import android.util.Log;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/* compiled from: ProGuard */
final class t extends Thread {
    final /* synthetic */ s a;
    private final u b;
    private final String c;

    public t(s sVar, u uVar, String str) {
        this.a = sVar;
        this.b = uVar;
        this.c = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public final synchronized void run() {
        String replace = (("http://games.martineriksen.net/airattack/php/" + this.c + this.a.h()) + "&name=" + s.b(this.b.a) + "&score=" + s.b(this.b.b + "")).replace(' ', '+');
        String str = replace + this.a.c(replace);
        try {
            r.a(new URL(str));
        } catch (MalformedURLException e) {
            Log.e(getClass().getName(), "Url: " + str, e);
        } catch (IOException e2) {
            Log.e(getClass().getName(), "Url: " + str, e2);
        }
        notifyAll();
        return;
    }

    public final synchronized void a() {
        while (isAlive()) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }
}
