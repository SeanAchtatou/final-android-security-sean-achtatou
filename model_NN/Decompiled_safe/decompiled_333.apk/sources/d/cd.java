package d;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/* compiled from: ProGuard */
public final class cd {
    static float a = 1.5707964f;
    private static final float c = 0.7853982f;

    /* renamed from: d  reason: collision with root package name */
    private static final float f34d = (a - c);
    private static final float e = (a + c);
    SensorManager b;
    /* access modifiers changed from: private */
    public float f;
    /* access modifiers changed from: private */
    public float g;
    /* access modifiers changed from: private */
    public float h;
    private float i = 5.0f;
    private SensorEventListener j;
    private int k = 0;

    public cd(Context context) {
        this.b = (SensorManager) context.getSystemService("sensor");
        this.j = new ce(this);
        this.b.registerListener(this.j, this.b.getDefaultSensor(1), 1);
    }

    public final void a(d dVar) {
        float f2 = this.f;
        float f3 = this.g;
        float f4 = this.h;
        float sqrt = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3) + (f4 * f4)));
        float acos = (((sqrt > 0.0f ? (float) Math.acos((double) (f3 / sqrt)) : Float.NaN) - a) * this.i) + a;
        int i2 = this.k;
        this.k = i2 + 1;
        if (i2 % 20 == 0) {
            Log.d("foo", "angle: " + acos);
        }
        if (acos < f34d) {
            dVar.a(1000);
        } else if (acos > e) {
            dVar.a(-1000);
        } else {
            dVar.g();
        }
    }

    public final void a() {
        this.b.unregisterListener(this.j);
    }

    public final void a(int i2) {
        this.i = (float) i2;
    }
}
