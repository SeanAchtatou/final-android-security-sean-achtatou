package d;

/* compiled from: ProGuard */
public class bc extends av {
    int c;

    /* renamed from: d  reason: collision with root package name */
    float f23d;
    float e;
    int[] f = new int[1];
    q[] g = new q[1];
    q h = new q();

    public final void a(k kVar, float f2, float f3, float f4, float f5, b bVar, int i, float f6, int i2) {
        a(kVar, f2, f3, bVar, i2);
        this.r = 10.0f * f6;
        this.c = i;
        this.f23d = f4 - f2;
        this.e = f3 - f5;
        float sqrt = (float) Math.sqrt((double) ((this.f23d * this.f23d) + (this.e * this.e)));
        this.f23d = (this.f23d * f6) / sqrt;
        this.e = (this.e * f6) / sqrt;
        a((float) Math.atan2((double) (-this.e), (double) this.f23d));
        this.h.a((float) this.t, (float) this.w);
        this.g[0] = this.h;
        a(this.g);
    }

    public final void a(k kVar, float f2, float f3, float f4, float f5, b bVar, int i, float f6) {
        a(kVar, f2, f3, f4, f5, bVar, i, f6, 10);
    }

    /* access modifiers changed from: protected */
    public void a() {
        q qVar = q()[0];
        int i = (int) qVar.a;
        int i2 = (int) qVar.b;
        this.f[0] = this.p.c(this.C, this.D);
        this.s.r.a((float) i, (float) i2, this.f, this.c);
        b(true);
        if (this instanceof ac) {
            by.b().r();
            return;
        }
        by.b().d();
        this.p.a(i, i2, this.c);
    }

    public void d() {
        a(e(-this.e));
        a(d(this.f23d));
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        switch (i) {
            case 0:
                a();
                return;
            case 1:
                b(true);
                return;
            default:
                return;
        }
    }
}
