package d;

import android.graphics.Canvas;
import com.google.ads.R;

/* compiled from: ProGuard */
public final class dr {
    private int a = 0;
    private long b = 0;
    private cz c;

    public final void a(cz czVar) {
        this.c = czVar;
    }

    public final void a(boolean z) {
        this.b = System.currentTimeMillis() + 15000;
        this.a = z ? 1 : 0;
    }

    public final void b(boolean z) {
        this.a = (this.a + 1) % 11;
        if (this.a == 0) {
            this.a = 1;
        }
        if (z) {
            this.b = System.currentTimeMillis() + 30000;
        } else {
            this.b = System.currentTimeMillis() + 15000;
        }
    }

    public final void a(Canvas canvas, s sVar, s sVar2, s sVar3, c cVar, int i) {
        if (System.currentTimeMillis() >= this.b) {
            b(false);
        }
        String a2 = cb.a().a((int) R.string.easy);
        String a3 = cb.a().a((int) R.string.hard);
        String a4 = cb.a().a((int) R.string.classic);
        if (this.a != 0) {
            ea.a(canvas, 0, 0, canvas.getWidth(), canvas.getHeight(), -1610612736);
        }
        if (this.a == 1) {
            switch (ds.a[cVar.ordinal()]) {
                case 1:
                    this.c.d(canvas, sVar, a2);
                    break;
                case 2:
                    this.c.d(canvas, sVar2, a3);
                    break;
                case 3:
                    this.c.d(canvas, sVar3, a4);
                    break;
                default:
                    throw new RuntimeException();
            }
        }
        if (this.a == 2) {
            this.c.a(canvas, sVar, a2);
        }
        if (this.a == 3) {
            this.c.c(canvas, sVar, a2);
        }
        if (this.a == 4) {
            this.c.b(canvas, sVar, a2);
        }
        if (this.a == 5) {
            this.c.a(canvas, sVar2, a3);
        }
        if (this.a == 6) {
            this.c.c(canvas, sVar2, a3);
        }
        if (this.a == 7) {
            this.c.b(canvas, sVar2, a3);
        }
        if (this.a == 8) {
            this.c.a(canvas, sVar3, a4);
        }
        if (this.a == 9) {
            this.c.c(canvas, sVar3, a4);
        }
        if (this.a == 10) {
            this.c.b(canvas, sVar3, a4);
        }
        this.c.a(canvas, i - 1);
    }

    public final boolean a() {
        return this.a == 0;
    }
}
