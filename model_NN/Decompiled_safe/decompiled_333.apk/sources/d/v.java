package d;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class v implements Serializable, Cloneable {
    private static int[] i = {4, 5, 4, 5, 2, 3, 6, 7};
    double a;
    double b;
    double c;

    /* renamed from: d  reason: collision with root package name */
    double f52d;
    double e;
    double f;
    transient int g;
    private transient int h;

    private v(double d2, double d3, double d4, double d5, double d6, double d7, int i2) {
        this.a = d2;
        this.b = d3;
        this.c = d4;
        this.f52d = d5;
        this.e = d6;
        this.f = d7;
        this.g = i2;
        this.h = -1;
    }

    public v() {
        this.f52d = 1.0d;
        this.a = 1.0d;
    }

    public v(v vVar) {
        this.a = vVar.a;
        this.b = vVar.b;
        this.c = vVar.c;
        this.f52d = vVar.f52d;
        this.e = vVar.e;
        this.f = vVar.f;
        this.g = vVar.g;
        this.h = vVar.h;
    }

    public v(float f2, float f3, float f4, float f5, float f6, float f7) {
        this.a = (double) f2;
        this.b = (double) f3;
        this.c = (double) f4;
        this.f52d = (double) f5;
        this.e = (double) f6;
        this.f = (double) f7;
        b();
    }

    public v(float[] fArr) {
        this.a = (double) fArr[0];
        this.b = (double) fArr[1];
        this.c = (double) fArr[2];
        this.f52d = (double) fArr[3];
        if (fArr.length > 5) {
            this.e = (double) fArr[4];
            this.f = (double) fArr[5];
        }
        b();
    }

    public v(double d2, double d3, double d4, double d5, double d6, double d7) {
        this.a = d2;
        this.b = d3;
        this.c = d4;
        this.f52d = d5;
        this.e = d6;
        this.f = d7;
        b();
    }

    public v(double[] dArr) {
        this.a = dArr[0];
        this.b = dArr[1];
        this.c = dArr[2];
        this.f52d = dArr[3];
        if (dArr.length > 5) {
            this.e = dArr[4];
            this.f = dArr[5];
        }
        b();
    }

    private void b() {
        if (this.c == 0.0d && this.b == 0.0d) {
            if (this.a == 1.0d && this.f52d == 1.0d) {
                if (this.e == 0.0d && this.f == 0.0d) {
                    this.g = 0;
                    this.h = 0;
                    return;
                }
                this.g = 1;
                this.h = 1;
            } else if (this.e == 0.0d && this.f == 0.0d) {
                this.g = 2;
                this.h = -1;
            } else {
                this.g = 3;
                this.h = -1;
            }
        } else if (this.a == 0.0d && this.f52d == 0.0d) {
            if (this.e == 0.0d && this.f == 0.0d) {
                this.g = 4;
                this.h = -1;
                return;
            }
            this.g = 5;
            this.h = -1;
        } else if (this.e == 0.0d && this.f == 0.0d) {
            this.g = 6;
            this.h = -1;
        } else {
            this.g = 7;
            this.h = -1;
        }
    }

    private static void c() {
        throw new InternalError("missing case in transform state switch");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void a(double d2, double d3) {
        switch (this.g) {
            case 0:
                this.e = d2;
                this.f = d3;
                if (d2 != 0.0d || d3 != 0.0d) {
                    this.g = 1;
                    this.h = 1;
                    return;
                }
                return;
            case 1:
                this.e += d2;
                this.f += d3;
                if (this.e == 0.0d && this.f == 0.0d) {
                    this.g = 0;
                    this.h = 0;
                    return;
                }
                return;
            case 2:
                this.e = this.a * d2;
                this.f = this.f52d * d3;
                if (this.e != 0.0d || this.f != 0.0d) {
                    this.g = 3;
                    this.h |= 1;
                    return;
                }
                return;
            case 3:
                this.e = (this.a * d2) + this.e;
                this.f = (this.f52d * d3) + this.f;
                if (this.e == 0.0d && this.f == 0.0d) {
                    this.g = 2;
                    if (this.h != -1) {
                        this.h--;
                        return;
                    }
                    return;
                }
                return;
            case 4:
                this.e = this.c * d3;
                this.f = this.b * d2;
                if (this.e != 0.0d || this.f != 0.0d) {
                    this.g = 5;
                    this.h |= 1;
                    return;
                }
                return;
            case 5:
                this.e = (this.c * d3) + this.e;
                this.f = (this.b * d2) + this.f;
                if (this.e == 0.0d && this.f == 0.0d) {
                    this.g = 4;
                    if (this.h != -1) {
                        this.h--;
                        return;
                    }
                    return;
                }
                return;
            case 6:
                this.e = (this.a * d2) + (this.c * d3);
                this.f = (this.b * d2) + (this.f52d * d3);
                if (this.e != 0.0d || this.f != 0.0d) {
                    this.g = 7;
                    this.h |= 1;
                    return;
                }
                return;
            case 7:
                break;
            default:
                c();
                break;
        }
        this.e = (this.a * d2) + (this.c * d3) + this.e;
        this.f = (this.b * d2) + (this.f52d * d3) + this.f;
        if (this.e == 0.0d && this.f == 0.0d) {
            this.g = 6;
            if (this.h != -1) {
                this.h--;
            }
        }
    }

    public final void a(double d2, double d3, double d4) {
        a(d3, d4);
        double sin = Math.sin(d2);
        double cos = Math.cos(d2);
        if (Math.abs(sin) < 1.0E-15d) {
            if (cos < 0.0d) {
                this.a = -this.a;
                this.f52d = -this.f52d;
                int i2 = this.g;
                if ((i2 & 4) != 0) {
                    this.c = -this.c;
                    this.b = -this.b;
                } else if (this.a == 1.0d && this.f52d == 1.0d) {
                    this.g = i2 & -3;
                } else {
                    this.g = i2 | 2;
                }
                this.h = -1;
            }
        } else if (Math.abs(cos) < 1.0E-15d) {
            if (sin < 0.0d) {
                double d5 = this.a;
                this.a = -this.c;
                this.c = d5;
                double d6 = this.b;
                this.b = -this.f52d;
                this.f52d = d6;
            } else {
                double d7 = this.a;
                this.a = this.c;
                this.c = -d7;
                double d8 = this.b;
                this.b = this.f52d;
                this.f52d = -d8;
            }
            int i3 = i[this.g];
            if ((i3 & 6) == 2 && this.a == 1.0d && this.f52d == 1.0d) {
                i3 -= 2;
            }
            this.g = i3;
            this.h = -1;
        } else {
            double d9 = this.a;
            double d10 = this.c;
            this.a = (cos * d9) + (sin * d10);
            this.c = (d9 * (-sin)) + (d10 * cos);
            double d11 = this.b;
            double d12 = this.f52d;
            this.b = (cos * d11) + (sin * d12);
            this.f52d = ((-sin) * d11) + (cos * d12);
            b();
        }
        a(-d3, -d4);
    }

    public final void a() {
        this.f52d = 1.0d;
        this.a = 1.0d;
        this.f = 0.0d;
        this.e = 0.0d;
        this.c = 0.0d;
        this.b = 0.0d;
        this.g = 0;
        this.h = 0;
    }

    public final void a(p[] pVarArr, int i2, p[] pVarArr2, int i3, int i4) {
        int i5 = this.g;
        int i6 = i4;
        int i7 = i3;
        int i8 = i2;
        while (true) {
            i6--;
            if (i6 >= 0) {
                int i9 = i8 + 1;
                p pVar = pVarArr[i8];
                double a2 = pVar.a();
                double b2 = pVar.b();
                int i10 = i7 + 1;
                p pVar2 = pVarArr2[i7];
                if (pVar2 == null) {
                    if (pVar instanceof q) {
                        pVar2 = new q();
                    } else {
                        pVar2 = new q();
                    }
                    pVarArr2[i10 - 1] = pVar2;
                }
                switch (i5) {
                    case 0:
                        pVar2.a(a2, b2);
                        i7 = i10;
                        i8 = i9;
                        continue;
                    case 1:
                        pVar2.a(a2 + this.e, b2 + this.f);
                        i7 = i10;
                        i8 = i9;
                        continue;
                    case 2:
                        pVar2.a(a2 * this.a, b2 * this.f52d);
                        i7 = i10;
                        i8 = i9;
                        continue;
                    case 3:
                        pVar2.a((a2 * this.a) + this.e, (b2 * this.f52d) + this.f);
                        i7 = i10;
                        i8 = i9;
                        continue;
                    case 4:
                        pVar2.a(b2 * this.c, a2 * this.b);
                        i7 = i10;
                        i8 = i9;
                        continue;
                    case 5:
                        pVar2.a((b2 * this.c) + this.e, (a2 * this.b) + this.f);
                        i7 = i10;
                        i8 = i9;
                        continue;
                    case 6:
                        pVar2.a((this.a * a2) + (this.c * b2), (a2 * this.b) + (b2 * this.f52d));
                        i7 = i10;
                        i8 = i9;
                        continue;
                    case 7:
                        break;
                    default:
                        c();
                        break;
                }
                pVar2.a((this.a * a2) + (this.c * b2) + this.e, (a2 * this.b) + (b2 * this.f52d) + this.f);
                i7 = i10;
                i8 = i9;
            } else {
                return;
            }
        }
    }

    private static double a(double d2) {
        return Math.rint(d2 * 1.0E15d) / 1.0E15d;
    }

    public final String toString() {
        return "AffineTransform[[" + a(this.a) + ", " + a(this.c) + ", " + a(this.e) + "], [" + a(this.b) + ", " + a(this.f52d) + ", " + a(this.f) + "]]";
    }

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new InternalError();
        }
    }

    public final int hashCode() {
        long doubleToLongBits = (((((((((Double.doubleToLongBits(this.a) * 31) + Double.doubleToLongBits(this.c)) * 31) + Double.doubleToLongBits(this.e)) * 31) + Double.doubleToLongBits(this.b)) * 31) + Double.doubleToLongBits(this.f52d)) * 31) + Double.doubleToLongBits(this.f);
        return ((int) (doubleToLongBits >> 32)) ^ ((int) doubleToLongBits);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof v)) {
            return false;
        }
        v vVar = (v) obj;
        return this.a == vVar.a && this.c == vVar.c && this.e == vVar.e && this.b == vVar.b && this.f52d == vVar.f52d && this.f == vVar.f;
    }
}
