package d;

import com.google.ads.R;

/* compiled from: ProGuard */
public class au extends av {
    float a;
    float b;
    int c;

    /* renamed from: d  reason: collision with root package name */
    int f18d;
    int e;
    int f;
    int g;
    int h;
    float i;
    float j;
    boolean k;
    private int l;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.av.a(d.k, float, float, boolean, d.b):void
     arg types: [d.k, float, float, int, d.b]
     candidates:
      d.av.a(d.q[], d.q[], float, float, float):void
      d.av.a(d.k, float, float, d.b, int):void
      d.av.a(d.k, float, float, boolean, d.b):void */
    public final void a(k kVar, float f2, float f3, b bVar) {
        a(kVar, f2, f3, true, bVar);
        this.a = f3;
        this.b = f3;
        this.c = (int) f2;
        this.l = 75;
        this.f18d = 0;
        this.e = 50;
        this.f = 60;
        this.g = 70;
        this.h = 180;
        this.i = 1.2f;
        this.j = 0.0f;
        this.k = false;
    }

    public final void d() {
        this.f18d++;
        if (this.f18d == 1) {
            this.s.r.d((float) this.c, this.b, this.e + 20);
            this.p.a(this.c, (int) this.b, 75);
            by.b().x();
        }
        if (this.f18d == this.e) {
            this.s.r.a((float) this.c, this.b, new int[]{-256, -14336}, 100, false, 0, 1.5f);
            for (int i2 = 0; i2 < 4; i2++) {
                aq aqVar = (aq) this.s.s.a(aq.class);
                aqVar.a(this.p, (float) this.c, this.a + 75.0f, this.s);
                this.s.a(aqVar);
            }
            this.k = true;
            this.p.b(this.c, (int) this.a, 135);
            this.l = 135;
            by.b().y();
            ao.a(cb.a().a((int) R.string.in_game_clean_up_fallout), s(), t(), this.s);
        }
        if (this.f18d == this.f) {
            this.p.b(this.c, (int) this.a, 142);
            this.l = 142;
        }
        if (this.f18d == this.g) {
            this.p.b(this.c, (int) this.a, 150);
            this.l = 150;
        }
        if (this.f18d == this.h) {
            b(true);
        }
        if (this.k) {
            this.b -= this.i;
            this.i -= this.j;
            this.s.r.a((float) this.c, this.b, 100, 1.0f + ((this.a - this.b) / 20.0f), true);
            this.s.r.c((float) this.c, this.a + 75.0f, ((int) this.b) + 30);
        } else {
            this.s.r.a((float) this.c, this.b, 15, 1.0f, false);
        }
        this.s.a(this.c, (int) this.a, this.l);
    }

    /* access modifiers changed from: protected */
    public final void b() {
    }
}
