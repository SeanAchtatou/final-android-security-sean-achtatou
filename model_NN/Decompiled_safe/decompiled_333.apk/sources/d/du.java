package d;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.google.ads.R;
import dk.logisoft.airattack.AirAttackActivity;

/* compiled from: ProGuard */
public final class du {
    /* access modifiers changed from: private */
    public static AirAttackActivity a;

    public static void a(AirAttackActivity airAttackActivity) {
        a = airAttackActivity;
    }

    public static AlertDialog a(AirAttackActivity airAttackActivity, Context context) {
        AlertDialog.Builder positiveButton = new AlertDialog.Builder(airAttackActivity).setIcon(17301659).setTitle((int) R.string.view_help_pages_title).setView(LayoutInflater.from(airAttackActivity).inflate((int) R.layout.message_of_the_day, (ViewGroup) null)).setCancelable(false).setPositiveButton((int) R.string.buy_the_game_ok, new dv());
        positiveButton.setNeutralButton(context.getString(R.string.view_help_pages_show), new dw());
        return positiveButton.create();
    }

    public static void a() {
        a.runOnUiThread(new dx());
    }
}
