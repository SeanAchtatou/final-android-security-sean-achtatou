package d;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.google.ads.R;

/* compiled from: ProGuard */
final class dm implements DialogInterface.OnClickListener {
    final /* synthetic */ Activity a;

    dm(Activity activity) {
        this.a = activity;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        Activity activity = this.a;
        String str = activity.getResources().getString(R.string.market_url_prefix) + activity.getClass().getPackage().getName();
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.setFlags(268435456);
            activity.startActivity(intent);
        } catch (Exception e) {
            Log.e("AirAttack", "Error occured going to market url: " + str, e);
        }
        dialogInterface.dismiss();
    }
}
