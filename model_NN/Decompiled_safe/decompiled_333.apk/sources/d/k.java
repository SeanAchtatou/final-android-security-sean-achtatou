package d;

import android.graphics.Bitmap;

/* compiled from: ProGuard */
public final class k {
    int[] a = cb.a().a;
    int[] b = cb.a().c;
    public dz c;

    /* renamed from: d  reason: collision with root package name */
    int f49d;
    int e;
    private int f = -100;
    private final double g;
    private int[] h;

    private k(int i, int i2, double d2) {
        this.f49d = i2;
        this.e = i;
        this.g = d2;
        this.c = new dz(i, i2);
        if (d2 <= 0.0d || d2 >= 1.0d) {
            throw new RuntimeException("Fraction ground must be between 0 and 1!");
        }
        c();
        this.c.b();
    }

    public static k a(int i, int i2) {
        return new k(i, i2, 0.1599999964237213d);
    }

    private final void c() {
        int i = (this.f49d * 4) / 5;
        int i2 = (((this.f49d - i) * 3) / 4) + i;
        m mVar = new m(this);
        switch (n.a(6)) {
            case 0:
                mVar.a = 1;
                break;
            case 1:
                mVar.a = 2;
                break;
            case 2:
                mVar.a = 4;
                break;
            case 3:
                mVar.a = 6;
                break;
            case 4:
                mVar.a = 8;
                break;
            case 5:
                mVar.a = 25;
                break;
        }
        mVar.b = d();
        mVar.c = d();
        if (mVar.a == 1) {
            mVar.b += 100;
            mVar.c += 50;
        }
        mVar.c = 50;
        mVar.c = 50;
        if (i2 <= i) {
            throw new RuntimeException("lowestTerrain must be greater than highestTerrain");
        }
        this.h = new int[this.e];
        this.h[0] = n.a(i2 - i) + i;
        this.h[this.e - 1] = i + n.a(i2 - i);
        this.f = mVar.a;
        e(0, this.e - 1);
        e();
        this.c.a(this.h);
        g();
        d(mVar.c, mVar.b);
        for (int i3 = 0; i3 < this.f49d; i3++) {
            this.c.c();
        }
    }

    private static int d() {
        switch (n.a(3)) {
            case 0:
                return 50;
            case 1:
                return 300;
            case 2:
                return 2000;
            default:
                throw new RuntimeException("invalid case");
        }
    }

    public final void b(int i, int i2) {
        this.c.a(i, i2, 8, 2, false);
        this.c.a(i, i2, 5, 1, false);
    }

    private void d(int i, int i2) {
        int i3;
        int i4 = this.e - 100;
        int a2 = n.a(i) + 100;
        int a3 = n.a(i2) + 100;
        int i5 = a2;
        while (a3 < i4 && i5 < i4) {
            if (a3 < i5) {
                int a4 = n.a(3);
                Bitmap A = cb.a().A();
                int width = A.getWidth();
                int height = A.getHeight();
                if (a3 <= 0 || a3 + width >= this.e) {
                    i3 = 0;
                } else {
                    int i6 = a3;
                    for (int i7 = a3; i7 < width + a3; i7++) {
                        if (this.h[i7] < this.h[i6]) {
                            i6 = i7;
                        }
                    }
                    int[] iArr = this.b;
                    int min = Math.min((this.h[i6] - 6) - 1, this.f49d - 2);
                    this.c.a(a3, min, width, iArr);
                    int i8 = min - height;
                    this.c.a(A, 1.0f, a3, i8);
                    while (a4 > 1 && ((float) (i8 - (a4 * height))) < ((float) this.f49d) * 0.33333334f) {
                        a4--;
                    }
                    int i9 = i8;
                    for (int i10 = 0; i10 < a4; i10++) {
                        i9 -= height;
                        this.c.a(A, 1.0f, a3, i9);
                    }
                    i3 = A.getWidth();
                }
                i5 += i3;
                a3 += i3 + n.a(i2);
            } else {
                Bitmap d2 = cb.a().d();
                float a5 = 0.5f - (0.1f * n.a());
                int width2 = (int) (((float) d2.getWidth()) * a5);
                int i11 = (width2 / 2) + i5;
                int i12 = (int) (90.0f * a5);
                int i13 = i11 - (i12 / 2);
                int i14 = i11 + (i12 / 2);
                if (i14 < this.e) {
                    int i15 = i13 + 1;
                    int i16 = this.h[i13];
                    for (int i17 = i15; i17 < i14; i17++) {
                        if (this.h[i17] > i16) {
                            i16 = this.h[i17];
                        }
                    }
                    this.c.a(d2, a5, i5, Math.min(new Float((((float) (i16 - 6)) - (((float) d2.getHeight()) * a5)) + (20.0f * a5)).intValue(), this.f49d));
                }
                i5 += n.a(i) + width2;
                a3 += width2;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0037, code lost:
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (r2 >= r7.h.length) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003d, code lost:
        r3 = r7.h;
        r3[r2] = r3[r2] + 1;
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0012, code lost:
        if (((double) f()) < r0) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001b, code lost:
        if (((double) f()) >= r0) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001d, code lost:
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0021, code lost:
        if (r2 >= r7.h.length) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0023, code lost:
        r3 = r7.h;
        r3[r2] = r3[r2] - 1;
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0035, code lost:
        if (((double) f()) <= r0) goto L_0x0048;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e() {
        /*
            r7 = this;
            r6 = 0
            double r0 = r7.g
            int r2 = r7.e
            double r2 = (double) r2
            double r0 = r0 * r2
            int r2 = r7.f49d
            double r2 = (double) r2
            double r0 = r0 * r2
            int r2 = r7.f()
            double r2 = (double) r2
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 >= 0) goto L_0x002e
        L_0x0014:
            int r2 = r7.f()
            double r2 = (double) r2
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 >= 0) goto L_0x0048
            r2 = r6
        L_0x001e:
            int[] r3 = r7.h
            int r3 = r3.length
            if (r2 >= r3) goto L_0x0014
            int[] r3 = r7.h
            r4 = r3[r2]
            r5 = 1
            int r4 = r4 - r5
            r3[r2] = r4
            int r2 = r2 + 1
            goto L_0x001e
        L_0x002e:
            int r2 = r7.f()
            double r2 = (double) r2
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0048
            r2 = r6
        L_0x0038:
            int[] r3 = r7.h
            int r3 = r3.length
            if (r2 >= r3) goto L_0x002e
            int[] r3 = r7.h
            r4 = r3[r2]
            int r4 = r4 + 1
            r3[r2] = r4
            int r2 = r2 + 1
            goto L_0x0038
        L_0x0048:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: d.k.e():void");
    }

    private int f() {
        int i = 0;
        for (int i2 = 0; i2 < this.h.length; i2++) {
            if (this.h[i2] <= 0) {
                i += this.f49d;
            } else if (this.h[i2] > this.f49d) {
                i += 0;
            } else {
                i += this.f49d - this.h[i2];
            }
        }
        return i;
    }

    private void g() {
        int[] iArr = this.h;
        int[] iArr2 = this.a;
        int i = this.f49d;
        dz dzVar = this.c;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            for (int i3 = 1; i3 < 7; i3++) {
                int i4 = iArr[i2] - i3;
                if (i4 >= 0 && i4 < i) {
                    dzVar.c(i2, i4, iArr2[n.a(iArr2.length)]);
                }
            }
        }
    }

    private void e(int i, int i2) {
        int i3 = i;
        while (i3 + 1 < i2) {
            int i4 = (i3 + i2) / 2;
            this.h[i4] = ((this.h[i3] + this.h[i2]) / 2) + ((n.a(i2 - i3) - ((i2 - i3) / 2)) / this.f);
            e(i3, i4);
            i3 = i4;
        }
    }

    public final int a() {
        return this.f49d;
    }

    public final int b() {
        return this.e;
    }

    public final void a(int i, int i2, int i3) {
        this.c.a(i, i2, i3, 1, true);
    }

    public final void b(int i, int i2, int i3) {
        this.c.a(i, i2, i3, 0, false);
    }

    public final int c(int i, int i2) {
        if (i >= this.e || i2 >= this.f49d || i < 0 || i2 < 0) {
            return -1;
        }
        return this.c.c(i, i2);
    }
}
