package d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Region;
import android.graphics.Typeface;

/* compiled from: ProGuard */
public final class ea extends dy {
    private static final Paint c = new Paint();

    /* renamed from: d  reason: collision with root package name */
    private static final Paint f45d = new Paint();
    private static final Paint e = new Paint();
    private static final Matrix f = new Matrix();
    private static final Path g = new Path();
    private final int a;
    private final int b;

    public ea(int i, int i2) {
        this.a = i;
        this.b = i2;
        f45d.setColor(-1);
        f45d.setAntiAlias(true);
        f45d.setTypeface(Typeface.SERIF);
    }

    public static void a(Canvas canvas, int i, int i2, float f2, int i3) {
        int i4 = i - 20;
        int i5 = i2 - 1;
        int i6 = (int) (40.0f * f2);
        a(canvas, i4, i5, i6, 2, i3);
        a(canvas, i4 + i6, i5, 40 - i6, 2, -1);
    }

    public static void a(Canvas canvas, av avVar) {
        Bitmap a2 = cb.a().a(avVar);
        canvas.drawBitmap(a2, a(avVar, avVar.v(), avVar.w(), avVar.f(), avVar.D(), a2), null);
    }

    public static Matrix a(av avVar, float f2, float f3, boolean z, float f4, Bitmap bitmap) {
        Matrix matrix = f;
        matrix.reset();
        if (z) {
            matrix.setScale(-1.0f, 1.0f);
            matrix.postTranslate((float) bitmap.getWidth(), 0.0f);
        }
        if (f4 != 0.0f) {
            matrix.postRotate((180.0f * f4) / 3.1415927f, avVar.E(), avVar.F());
        }
        matrix.postTranslate(f2, f3);
        return matrix;
    }

    public static int[] a(av avVar) {
        return cb.a().a(cb.a().a(avVar));
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.a;
    }

    public static int a(Canvas canvas, int i, int i2, int i3, int i4) {
        return a(canvas, cb.a().a(i), i2, i3, i4, Paint.Align.CENTER);
    }

    public static int a(Canvas canvas, String str, int i, int i2, int i3) {
        return a(canvas, str, i, i2, i3, Paint.Align.CENTER);
    }

    public static int b(Canvas canvas, String str, int i, int i2, int i3) {
        return a(canvas, str, i, i2, i3, Paint.Align.RIGHT);
    }

    public static int a(Canvas canvas, String str, int i, int i2, int i3, int i4) {
        f45d.setTextAlign(Paint.Align.LEFT);
        f45d.setTextSize((float) i);
        f45d.setUnderlineText(false);
        canvas.save(2);
        canvas.clipRect((float) i3, (float) (i2 - (i * 2)), (float) (i3 + i4), (float) ((i * 2) + i2), Region.Op.REPLACE);
        canvas.drawText(str, (float) i3, (float) i2, f45d);
        canvas.restore();
        return i2 + i;
    }

    private static int a(Canvas canvas, String str, int i, int i2, int i3, Paint.Align align) {
        f45d.setTextAlign(align);
        f45d.setTextSize((float) i);
        f45d.setUnderlineText(false);
        canvas.drawText(str, (float) i3, (float) i2, f45d);
        return i2 + i;
    }

    public static int a(Canvas canvas, String[] strArr, int i, int i2, int i3) {
        Paint.Align align = Paint.Align.CENTER;
        Paint paint = f45d;
        paint.setTextAlign(align);
        paint.setTextSize((float) i);
        int i4 = i2;
        for (String drawText : strArr) {
            paint.setUnderlineText(false);
            canvas.drawText(drawText, (float) i3, (float) i4, paint);
            i4 += i;
        }
        return i4;
    }

    public static void a(Canvas canvas, int i, int i2, int i3) {
        canvas.drawBitmap(dd.a(i), (float) i2, (float) i3, (Paint) null);
    }

    public static void b(Canvas canvas, int i, int i2, int i3) {
        e.setAlpha(128);
        canvas.drawBitmap(dd.a(i), (float) i2, (float) i3, e);
    }

    public static void a(Canvas canvas, int i, int i2, int i3, int i4, int i5) {
        Paint paint = c;
        paint.setColor(i5);
        Canvas canvas2 = canvas;
        canvas2.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), paint);
    }

    public static void a(Canvas canvas, Bitmap bitmap) {
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
    }

    public static int a(int i) {
        return dd.a(i).getHeight();
    }

    public static int b(int i) {
        return dd.a(i).getWidth();
    }
}
