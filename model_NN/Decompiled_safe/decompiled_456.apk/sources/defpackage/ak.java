package defpackage;

import android.content.DialogInterface;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: ak  reason: default package */
public class ak implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity f14a;

    public ak(TransparentActivity transparentActivity) {
        this.f14a = transparentActivity;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f14a.finish();
    }
}
