package defpackage;

import android.content.Context;
import android.text.format.Formatter;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.jni.SecureEngine;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: ay  reason: default package */
public class ay {
    private static byte a(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static long a(AppInfo appInfo) {
        long j = 0;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(appInfo.getPkgName());
        stringBuffer.append("&");
        stringBuffer.append(appInfo.getCertMd5());
        stringBuffer.append("&");
        stringBuffer.append(appInfo.getFileSize());
        byte[] b = b(stringBuffer.toString().getBytes());
        if (b != null) {
            int i = 0;
            while (i < 8) {
                i++;
                j = ((long) (b[i + 8] & 255)) + (j << 8);
            }
        }
        return j;
    }

    public static String a(Context context, long j) {
        return j == -1 ? "0" : Formatter.formatFileSize(context, j);
    }

    public static String a(String str) {
        return str == null ? Constants.STR_EMPTY : str;
    }

    public static final String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(bArr.length);
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() < 2) {
                stringBuffer.append(0);
            }
            stringBuffer.append(hexString.toUpperCase());
        }
        return stringBuffer.toString();
    }

    private static boolean a(Context context) {
        int indexOf;
        String engineVersion = SecureEngine.getEngineVersion(context);
        if (!(engineVersion == null || (indexOf = engineVersion.indexOf("version=")) == -1)) {
            try {
                if (Integer.parseInt(engineVersion.substring(indexOf + 8, indexOf + 13).replace(".", Constants.STR_EMPTY)) / 10 == 21) {
                    return true;
                }
            } catch (Exception e) {
            }
        }
        return false;
    }

    public static boolean a(Context context, String str) {
        boolean z;
        String absolutePath = context.getFilesDir().getAbsolutePath();
        String str2 = absolutePath + "/lib" + str + ".so";
        if (new File(str2).exists()) {
            try {
                System.load(str2);
                z = true;
            } catch (Throwable th) {
                z = false;
            }
        } else {
            z = false;
        }
        if (!z) {
            try {
                System.load(absolutePath.replaceFirst("/files", "/lib") + "/lib" + str + ".so");
                z = true;
            } catch (Throwable th2) {
            }
        }
        if (!z) {
            try {
                System.loadLibrary(str);
                z = true;
            } catch (Throwable th3) {
            }
        }
        return z && a(context);
    }

    public static byte[] b(String str) {
        int length = str.length() / 2;
        byte[] bArr = new byte[length];
        char[] charArray = str.toCharArray();
        for (int i = 0; i < length; i++) {
            int i2 = i * 2;
            bArr[i] = (byte) (a(charArray[i2 + 1]) | (a(charArray[i2]) << 4));
        }
        return bArr;
    }

    public static byte[] b(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002e A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String c(java.lang.String r4) {
        /*
            r1 = 0
            if (r1 != 0) goto L_0x0031
            java.lang.String r0 = android.net.Uri.decode(r4)
            if (r0 == 0) goto L_0x0031
            r2 = 63
            int r2 = r0.indexOf(r2)
            if (r2 <= 0) goto L_0x0016
            r3 = 0
            java.lang.String r0 = r0.substring(r3, r2)
        L_0x0016:
            java.lang.String r2 = "/"
            boolean r2 = r0.endsWith(r2)
            if (r2 != 0) goto L_0x0031
            r2 = 47
            int r2 = r0.lastIndexOf(r2)
            int r2 = r2 + 1
            if (r2 <= 0) goto L_0x0031
            java.lang.String r0 = r0.substring(r2)
        L_0x002c:
            if (r0 != 0) goto L_0x0030
            java.lang.String r0 = "downloadfile"
        L_0x0030:
            return r0
        L_0x0031:
            r0 = r1
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.ay.c(java.lang.String):java.lang.String");
    }
}
