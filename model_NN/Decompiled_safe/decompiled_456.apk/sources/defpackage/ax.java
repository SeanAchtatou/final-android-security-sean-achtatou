package defpackage;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.impl.SecureService;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: ax  reason: default package */
public class ax {
    public static void a(Context context) {
        a(context, 17301628, "QQ安全登录扫描中...", "QQ安全登录扫描中...", Constants.STR_EMPTY);
    }

    public static void a(Context context, int i) {
        String str = "危险！QQ安全登录发现" + i + "个病毒";
        a(context, 17301642, str, str, Constants.STR_EMPTY);
    }

    public static void a(Context context, int i, String str, String str2, String str3) {
        Notification notification = new Notification();
        notification.icon = i;
        notification.when = System.currentTimeMillis();
        notification.flags = 16;
        notification.defaults = 4;
        notification.tickerText = str;
        notification.setLatestEventInfo(context, str2, str3, PendingIntent.getActivity(context, 0, new Intent(), 134217728));
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        notificationManager.cancel(1);
        notificationManager.notify(1, notification);
        notificationManager.cancel(1);
    }

    public static void a(Context context, int i, String str, String str2, String str3, PendingIntent pendingIntent) {
        Notification notification = new Notification();
        notification.icon = i;
        notification.when = System.currentTimeMillis();
        notification.flags = 16;
        notification.defaults = 4;
        notification.tickerText = str;
        notification.setLatestEventInfo(context, str2, str3, pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        notificationManager.cancel(2);
        notificationManager.notify(2, notification);
    }

    public static void a(Context context, Bundle bundle) {
        Notification notification = new Notification();
        notification.icon = 17301633;
        notification.when = System.currentTimeMillis();
        notification.flags = 16;
        notification.defaults = 4;
        PendingIntent activity = PendingIntent.getActivity(context, 0, new Intent(), 134217728);
        String str = Constants.STR_EMPTY;
        if (bundle != null) {
            long j = bundle.getLong("key_total");
            long j2 = bundle.getLong("key_completed");
            int i = bundle.getInt("key_progress");
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("下载：");
            stringBuffer.append(ay.a(context, j2));
            stringBuffer.append("/");
            stringBuffer.append(ay.a(context, j));
            stringBuffer.append("    " + i + "%");
            str = stringBuffer.toString();
        }
        notification.setLatestEventInfo(context, "腾讯手机管家", str, activity);
        ((NotificationManager) context.getSystemService("notification")).notify(2, notification);
    }

    public static void a(Context context, AppInfo appInfo) {
        Notification notification = new Notification();
        notification.icon = 17301642;
        notification.when = System.currentTimeMillis();
        notification.flags = 16;
        notification.defaults = 4;
        String pkgName = appInfo.getPkgName();
        Intent intent = new Intent();
        intent.setClass(context, TransparentActivity.class);
        intent.setAction("1000041");
        intent.putExtra("data", appInfo);
        notification.setLatestEventInfo(context, "QQ安全登录发现病毒", "“" + appInfo.getSoftName() + "应用”已被病毒感染", PendingIntent.getActivity(context, 0, intent, 134217728));
        ((NotificationManager) context.getSystemService("notification")).notify(pkgName.hashCode() + 1, notification);
    }

    public static void b(Context context) {
        a(context, 17301628, "QQ安全登录扫描正常，请放心使用", "QQ安全登录扫描正常，请放心使用", Constants.STR_EMPTY);
    }

    public static void b(Context context, Bundle bundle) {
        d(context);
    }

    public static void c(Context context) {
        a(context, 17301642, "QQ安全登录扫描联网超时，无法判断风险", "QQ安全登录扫描联网超时，无法判断风险", Constants.STR_EMPTY);
    }

    public static void d(Context context) {
        ((NotificationManager) context.getSystemService("notification")).cancel(2);
    }

    public static void e(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SecureService.class);
        intent.setAction("1000011");
        a(context, 17301642, "腾讯手机管家下载失败", "腾讯手机管家下载失败", "点击继续下载？", PendingIntent.getService(context, 0, intent, 134217728));
    }

    public static void f(Context context) {
        Toast.makeText(context, "开始下载手机管家", 0).show();
    }
}
