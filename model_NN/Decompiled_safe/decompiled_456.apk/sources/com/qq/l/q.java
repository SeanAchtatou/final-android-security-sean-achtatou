package com.qq.l;

import android.os.Process;
import com.qq.AppService.AppService;
import java.util.List;
import java.util.Vector;

/* compiled from: ProGuard */
public class q extends a {
    private static q b = null;

    /* renamed from: a  reason: collision with root package name */
    private List<d> f311a;
    private long c;
    private long d;
    private long e;

    private q() {
        this.f311a = null;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f311a = new Vector();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044 A[SYNTHETIC, Splitter:B:15:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0050 A[SYNTHETIC, Splitter:B:21:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(int r7) {
        /*
            r6 = this;
            r0 = 0
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "/proc/uid_stat/"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r4 = "/tcp_snd"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x0027
        L_0x0026:
            return r0
        L_0x0027:
            r4 = 0
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            r5.<init>(r2)     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            r3.<init>(r5)     // Catch:{ Exception -> 0x003d, all -> 0x004d }
        L_0x0032:
            java.lang.String r2 = r3.readLine()     // Catch:{ Exception -> 0x0064 }
            if (r2 == 0) goto L_0x0059
            long r0 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x0064 }
            goto L_0x0032
        L_0x003d:
            r2 = move-exception
            r3 = r4
        L_0x003f:
            r2.printStackTrace()     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0026
        L_0x0048:
            r2 = move-exception
        L_0x0049:
            r2.printStackTrace()
            goto L_0x0026
        L_0x004d:
            r0 = move-exception
        L_0x004e:
            if (r4 == 0) goto L_0x0053
            r4.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r0
        L_0x0054:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0053
        L_0x0059:
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x005f }
            goto L_0x0026
        L_0x005f:
            r2 = move-exception
            goto L_0x0049
        L_0x0061:
            r0 = move-exception
            r4 = r3
            goto L_0x004e
        L_0x0064:
            r2 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.l.q.a(int):long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044 A[SYNTHETIC, Splitter:B:15:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0050 A[SYNTHETIC, Splitter:B:21:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long b(int r7) {
        /*
            r6 = this;
            r0 = 0
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "/proc/uid_stat/"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r4 = "/tcp_rcv"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x0027
        L_0x0026:
            return r0
        L_0x0027:
            r4 = 0
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            r5.<init>(r2)     // Catch:{ Exception -> 0x003d, all -> 0x004d }
            r3.<init>(r5)     // Catch:{ Exception -> 0x003d, all -> 0x004d }
        L_0x0032:
            java.lang.String r2 = r3.readLine()     // Catch:{ Exception -> 0x0064 }
            if (r2 == 0) goto L_0x0059
            long r0 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x0064 }
            goto L_0x0032
        L_0x003d:
            r2 = move-exception
            r3 = r4
        L_0x003f:
            r2.printStackTrace()     // Catch:{ all -> 0x0061 }
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0026
        L_0x0048:
            r2 = move-exception
        L_0x0049:
            r2.printStackTrace()
            goto L_0x0026
        L_0x004d:
            r0 = move-exception
        L_0x004e:
            if (r4 == 0) goto L_0x0053
            r4.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r0
        L_0x0054:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0053
        L_0x0059:
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x005f }
            goto L_0x0026
        L_0x005f:
            r2 = move-exception
            goto L_0x0049
        L_0x0061:
            r0 = move-exception
            r4 = r3
            goto L_0x004e
        L_0x0064:
            r2 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.l.q.b(int):long");
    }

    public static synchronized q m() {
        q qVar;
        synchronized (q.class) {
            if (b == null) {
                b = new q();
            }
            qVar = b;
        }
        return qVar;
    }

    public void n() {
        this.c = System.currentTimeMillis();
        this.d = a(Process.myUid());
        this.e = b(Process.myUid());
    }

    public void o() {
        if (this.c != 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(d() + "_" + e()).append("\t");
            sb.append(c()).append("\t");
            sb.append(g()).append("\t");
            sb.append(System.currentTimeMillis() - this.c).append("\t");
            sb.append((a(Process.myUid()) - this.d) + "_" + (b(Process.myUid()) - this.e)).append("\t");
            sb.append(AppService.I()).append("\t");
            sb.append(e()).append("\t");
            sb.append(f()).append("\t");
            sb.append(i()).append("\t");
            sb.append(h()).append("\t");
            sb.append(j()).append("\t");
            sb.append("null").append("\t");
            sb.append(a());
            this.f311a.add(new d("wcs_flow_st", sb.toString()));
            if (this.f311a.size() >= 4) {
                k().a(this.f311a);
                this.f311a.clear();
            }
            this.c = 0;
            this.d = 0;
            this.e = 0;
        }
    }

    public void l() {
        if (!this.f311a.isEmpty()) {
            k().a(this.f311a);
            this.f311a.clear();
        }
    }
}
