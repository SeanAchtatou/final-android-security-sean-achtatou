package com.qq.a.a;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public String f256a = null;
    /* access modifiers changed from: private */
    public MediaScannerConnection b = null;
    private c c = null;
    /* access modifiers changed from: private */
    public String d = null;
    /* access modifiers changed from: private */
    public String e = null;
    /* access modifiers changed from: private */
    public String[] f = null;
    /* access modifiers changed from: private */
    public Uri g = null;
    /* access modifiers changed from: private */
    public boolean h = false;

    public b(Context context) {
        if (this.c == null) {
            this.c = new c(this);
        }
        if (this.b == null) {
            this.b = new MediaScannerConnection(context, this.c);
        }
    }

    private String b(String str) {
        int lastIndexOf = str.lastIndexOf(".") + 1;
        if (lastIndexOf < 0 || lastIndexOf >= str.length()) {
            return null;
        }
        return str.substring(lastIndexOf, str.length());
    }

    public void a(String str) {
        a(str, b(str));
    }

    public void a(String str, String str2) {
        this.d = str;
        this.e = str2;
        this.b.connect();
    }

    public int a() {
        for (int i = 0; i < 500 && !this.h; i++) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (this.g == null) {
            return 0;
        }
        try {
            return Integer.parseInt(this.g.getLastPathSegment());
        } catch (Exception e3) {
            e3.printStackTrace();
            return -1;
        }
    }

    public void a(String[] strArr, String str) {
        this.f = strArr;
        this.e = str;
        this.b.connect();
    }
}
