package com.qq.AppService;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class aa extends Binder implements z {
    public aa() {
        attachInterface(this, "com.qq.AppService.IMoloServiceCallback");
    }

    public static z a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.qq.AppService.IMoloServiceCallback");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof z)) {
            return new ab(iBinder);
        }
        return (z) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        boolean z;
        ParcelObject parcelObject = null;
        boolean z2 = false;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                a(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 2:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 3:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                a();
                parcel2.writeNoException();
                return true;
            case 4:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                a(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 5:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                b(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 6:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                c(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 7:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                a(parcel.readString(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 8:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                int readInt = parcel.readInt();
                int readInt2 = parcel.readInt();
                String readString = parcel.readString();
                if (parcel.readInt() != 0) {
                    parcelObject = ParcelObject.CREATOR.createFromParcel(parcel);
                }
                a(readInt, readInt2, readString, parcelObject);
                parcel2.writeNoException();
                return true;
            case 9:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                String readString2 = parcel.readString();
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                if (parcel.readInt() != 0) {
                    z = true;
                } else {
                    z = false;
                }
                a(readString2, readString3, readString4, z);
                parcel2.writeNoException();
                return true;
            case 10:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                d(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 11:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                e(parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 12:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                b();
                parcel2.writeNoException();
                return true;
            case 13:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                if (parcel.readInt() != 0) {
                    z2 = true;
                }
                a(z2);
                parcel2.writeNoException();
                return true;
            case 14:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                c();
                parcel2.writeNoException();
                return true;
            case 15:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                a(parcel.readLong(), parcel.readInt());
                parcel2.writeNoException();
                return true;
            case 16:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                d();
                parcel2.writeNoException();
                return true;
            case 17:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                int readInt3 = parcel.readInt();
                int readInt4 = parcel.readInt();
                if (parcel.readInt() != 0) {
                    parcelObject = ParcelObject.CREATOR.createFromParcel(parcel);
                }
                a(readInt3, readInt4, parcelObject);
                parcel2.writeNoException();
                return true;
            case 18:
                parcel.enforceInterface("com.qq.AppService.IMoloServiceCallback");
                a(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.qq.AppService.IMoloServiceCallback");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
