package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;
import com.qq.provider.h;

/* compiled from: ProGuard */
public class SMSReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    public static long f208a = 0;

    public void onReceive(Context context, Intent intent) {
        String action;
        SmsMessage[] smsMessageArr;
        if (context != null && intent != null && h.b && (action = intent.getAction()) != null && action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            Log.d("com.qq.connect", "********* android.provider.Telephony.SMS_RECEIVED *********");
            if (h.b) {
                try {
                    smsMessageArr = a(intent);
                } catch (Throwable th) {
                    th.printStackTrace();
                    smsMessageArr = null;
                }
                if (smsMessageArr != null) {
                    StringBuilder sb = new StringBuilder(4096);
                    int i = 0;
                    while (i < smsMessageArr.length) {
                        try {
                            if (!(smsMessageArr[i] == null || smsMessageArr[i].getDisplayMessageBody() == null)) {
                                sb.append(smsMessageArr[i].getDisplayMessageBody());
                            }
                            i++;
                        } catch (Throwable th2) {
                            return;
                        }
                    }
                    if (sb.length() > 0) {
                        String displayOriginatingAddress = smsMessageArr[0].getDisplayOriginatingAddress();
                        if (sb.charAt(sb.length() - 1) == 10) {
                            sb.deleteCharAt(sb.length() - 1);
                        }
                        String sb2 = sb.toString();
                        f208a = System.currentTimeMillis();
                        if (h.d != null) {
                            h.d.a(sb2, displayOriginatingAddress);
                        }
                    }
                }
            }
        }
    }

    public static SmsMessage[] a(Intent intent) {
        Object[] objArr = (Object[]) intent.getSerializableExtra("pdus");
        byte[][] bArr = new byte[objArr.length][];
        for (int i = 0; i < objArr.length; i++) {
            bArr[i] = (byte[]) objArr[i];
        }
        byte[][] bArr2 = new byte[bArr.length][];
        int length = bArr2.length;
        SmsMessage[] smsMessageArr = new SmsMessage[length];
        for (int i2 = 0; i2 < length; i2++) {
            bArr2[i2] = bArr[i2];
            smsMessageArr[i2] = SmsMessage.createFromPdu(bArr2[i2]);
        }
        return smsMessageArr;
    }
}
