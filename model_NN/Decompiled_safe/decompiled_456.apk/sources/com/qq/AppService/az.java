package com.qq.AppService;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import com.qq.AppService.AppService;
import com.qq.g.c;
import com.qq.ndk.Native;
import com.qq.provider.ab;
import com.qq.provider.h;
import com.qq.provider.n;
import com.qq.provider.p;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.connect.common.Constants;
import com.tencent.wcs.c.b;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/* compiled from: ProGuard */
public class az extends Thread {
    /* access modifiers changed from: private */
    public static volatile boolean e = false;

    /* renamed from: a  reason: collision with root package name */
    InputStream f232a = null;
    OutputStream b = null;
    public volatile boolean c = false;
    /* access modifiers changed from: private */
    public Socket d = null;
    private Context f = null;

    public az(Context context, Socket socket) {
        this.d = socket;
        this.c = true;
        this.f = context;
    }

    public void a() {
        this.c = false;
        if (this.f232a != null) {
            try {
                this.f232a.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.f232a = null;
        }
        if (this.b != null) {
            try {
                this.b.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            this.b = null;
        }
        if (this.d != null) {
            try {
                this.d.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            this.d = null;
        }
    }

    private int b() {
        byte[] bArr = new byte[4];
        if (this.f232a.read(bArr) == 4) {
            return r.a(bArr);
        }
        return 0;
    }

    private byte[] a(int i) {
        if (i <= 0 || i >= 262144) {
            return null;
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        while (i2 < i) {
            int read = this.f232a.read(bArr, i2, i - i2);
            if (read > 0) {
                i2 += read;
            } else if (read == -1) {
                break;
            }
        }
        if (i2 == i) {
            return bArr;
        }
        return null;
    }

    private String b(int i) {
        byte[] a2;
        if (i <= 654976 && (a2 = a(i)) != null) {
            return r.b(a2);
        }
        return null;
    }

    private void a(String str) {
        if (this.b != null) {
            byte[] a2 = r.a(str);
            try {
                this.b.write(r.a(a2.length));
                this.b.write(a2);
                this.b.flush();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        if (this.b != null) {
            try {
                this.b.write(r.a(4));
                this.b.write(r.a(i));
                this.b.flush();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
    }

    private void d(int i) {
        if (this.b != null) {
            try {
                this.b.write(r.a(i));
                this.b.flush();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                Thread.sleep(25);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
    }

    private int a(int i, FileOutputStream fileOutputStream) {
        int i2;
        if (i <= 0 || fileOutputStream == null || this.f232a == null) {
            return 0;
        }
        byte[] bArr = new byte[8192];
        int i3 = 0;
        while (i3 < i) {
            try {
                i2 = this.f232a.read(bArr);
            } catch (SocketTimeoutException e2) {
                i2 = 0;
            }
            if (i2 == -1) {
                return i3;
            }
            if (i2 > 0) {
                fileOutputStream.write(bArr, 0, i2);
                fileOutputStream.flush();
                i3 += i2;
            }
        }
        return i3;
    }

    private void a(Context context, c cVar) {
        int i;
        if (this.d != null) {
            Log.d("com.qq.connect", "installAPK !!!!!!!!!!!!");
            if (cVar.e() < 2) {
                d(1);
                return;
            }
            int h = cVar.h();
            int h2 = cVar.h();
            if (h != 1) {
                d(1);
            } else if (h2 <= 0) {
                d(7);
            } else if (h2 > 4194304) {
                d(7);
            } else {
                File a2 = p.a(context);
                if (a2 == null || !a2.exists()) {
                    a2 = new File(context.getApplicationInfo().dataDir + File.separator + "apk");
                    if (a2.exists() && !a2.isDirectory()) {
                        try {
                            n.b(a2);
                        } catch (Throwable th) {
                        }
                    }
                    if (!a2.exists()) {
                        a2.mkdir();
                    }
                }
                if (!a2.exists() || !a2.isDirectory()) {
                    d(5);
                    return;
                }
                new File(a2.getAbsolutePath() + File.separator + AstApp.i().getPackageName() + ".apk");
                Native nativeR = new Native();
                File file = new File(a2 + File.separator + AstApp.i().getPackageName() + ".apk");
                if (!file.exists()) {
                    nativeR.createFile(file.getAbsolutePath(), 511);
                }
                if (n.a(a2.getAbsolutePath()) < ((long) h2)) {
                    d(9);
                    return;
                }
                d(0);
                if (file.exists() && !file.isFile()) {
                    try {
                        n.b(file);
                    } catch (Throwable th2) {
                    }
                }
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
                if (!file.exists()) {
                    d(6);
                    return;
                }
                FileOutputStream fileOutputStream = null;
                try {
                    fileOutputStream = new FileOutputStream(file);
                } catch (FileNotFoundException e3) {
                    e3.printStackTrace();
                }
                if (fileOutputStream == null) {
                    file.delete();
                    d(11);
                    return;
                }
                try {
                    i = a(h2, fileOutputStream);
                } catch (Exception e4) {
                    e4.printStackTrace();
                    i = 0;
                }
                try {
                    fileOutputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                if (i < h2) {
                    file.delete();
                    d(10);
                    return;
                }
                p.a(context, file);
                d(0);
            }
        }
    }

    private void b(Context context, c cVar) {
        String str;
        String str2;
        String str3;
        Context context2;
        String str4;
        byte[] unEncrypt;
        byte[] unEncrypt2;
        if (this.d != null) {
            if (cVar.e() < 4) {
                c(-2);
                return;
            }
            int h = cVar.h();
            int h2 = cVar.h();
            String j = cVar.j();
            byte[] k = cVar.k();
            String str5 = null;
            String str6 = null;
            String str7 = null;
            b.a("WifiAdminThread Step Check 0 command = " + h + " request = " + h2 + " name = " + j);
            if (h != 0 || h2 != 1) {
                if (h == 0 && h2 == 2) {
                    if (cVar.e() < 6) {
                        c(-2);
                        return;
                    }
                    str6 = cVar.j();
                    str4 = cVar.j();
                    if (r.b(str6) || r.b(str4)) {
                        c(-2);
                        return;
                    } else if (cVar.e() >= 8) {
                        String j2 = cVar.j();
                        str = cVar.j();
                        str2 = j2;
                        str3 = str4;
                    }
                } else if (h == 0 && h2 == 3) {
                    if (cVar.e() < 6) {
                        c(-2);
                        return;
                    }
                    str6 = cVar.j();
                    str4 = cVar.j();
                    if (r.b(str6) || r.b(str4)) {
                        c(-2);
                        return;
                    }
                } else if (h == 0 && h2 == 4) {
                    if (cVar.e() < 7) {
                        c(-2);
                        return;
                    }
                    str6 = cVar.j();
                    String j3 = cVar.j();
                    byte[] k2 = cVar.k();
                    if (!(k2 == null || (unEncrypt2 = new Native().unEncrypt(k2, 0, k2.length)) == null)) {
                        str5 = r.a(unEncrypt2, 0, unEncrypt2.length);
                    }
                    if (r.b(str6) || r.b(j3)) {
                        c(-2);
                        return;
                    }
                    str = null;
                    str2 = str5;
                    str3 = j3;
                } else if (h == 0 && h2 == 5) {
                    if (cVar.e() < 7) {
                        b.a("WifiAdminThread GetElementCount < 7, elementCount = " + cVar.e());
                        c(-2);
                        return;
                    }
                    str6 = cVar.j();
                    String j4 = cVar.j();
                    byte[] k3 = cVar.k();
                    if (!(k3 == null || (unEncrypt = new Native().unEncrypt(k3, 0, k3.length)) == null)) {
                        str5 = r.a(unEncrypt, 0, unEncrypt.length);
                    }
                    if (cVar.e() >= 8) {
                        str7 = cVar.j();
                    }
                    if (r.b(str6) || r.b(j4)) {
                        b.a("WifiAdminThread isEmpty(pc_guid)  || isEmpty(guid)  || isEmpty(uin) " + str6 + " " + j4 + " " + str5);
                        c(-2);
                        return;
                    }
                    str = str7;
                    str2 = str5;
                    str3 = j4;
                } else if (h != 0 || h2 != 10) {
                    b.a("WifiAdminThread  in else ");
                    c(-2);
                    return;
                } else if (cVar.e() < 6) {
                    c(-2);
                    return;
                } else {
                    str6 = cVar.j();
                    str4 = cVar.j();
                    if (r.b(str6) || r.b(str4)) {
                        c(-2);
                        return;
                    }
                }
                str = null;
                str2 = null;
                str3 = str4;
            } else if (cVar.e() < 6) {
                c(-2);
                return;
            } else {
                str2 = cVar.j();
                str3 = cVar.j();
                if (r.b(str2) || r.b(str3)) {
                    c(-2);
                    return;
                } else if (str2 == null || AppService.k.equals(str2)) {
                    str = null;
                } else {
                    c(-2);
                    return;
                }
            }
            if (str3 != null) {
                if (AppService.l == null) {
                    AppService.l = ab.b(context);
                }
                if (AppService.l != null && !AppService.l.equals(str3)) {
                    b.a("WifiAdminThread  guid = " + str3 + " AppService.DeviceID = " + AppService.l);
                    Log.d("com.qq.connect", "AppService.DeviceID:" + AppService.l + "*guid:" + str3);
                    c(-2);
                    return;
                }
            }
            if (h == 0 && h2 == 10) {
                if (!AppService.w()) {
                    AppService.z();
                } else if (AppService.x()) {
                    b.a("WifiAdminThread longconnection exist return to pc !!!");
                    c(0);
                    return;
                }
                b.a("WifiAdminThread start wcs service or waiting start success !!!");
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= 6) {
                        break;
                    }
                    try {
                        if (AppService.w() && AppService.x()) {
                            break;
                        }
                        try {
                            this.f232a.read();
                        } catch (SocketTimeoutException e2) {
                        }
                        i = i2 + 1;
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
                if (!AppService.w() || !AppService.x()) {
                    c(3);
                } else {
                    c(0);
                }
            } else {
                b.a("WifiAdminThread Step Check 1 pc_guid = " + str6 + " guid = " + str3 + " uin = " + str2);
                String I = AppService.I();
                if (h == 0 && ((h2 == 4 || h2 == 5 || h2 == 2) && !r.b(str2) && (r.b(I) || !I.equals(str2)))) {
                    b.a("WifiAdminThread  isEmpty(uin) || isEmpty(AppService.getQQ()) " + str2 + " " + AppService.I());
                    c(-2);
                    AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.CONNECTION_EVENT_QQ_MISMATCH);
                } else if (AppService.g != null && AppService.g.equals(this.d.getInetAddress().toString())) {
                    Log.d("com.qq.connect", "in AccpetAddress ,return!!!!");
                    AppService.h = j;
                    AppService.i = str6;
                    c(0);
                } else if (str6 == null || ((h == 0 && h2 == 3) || ((h == 0 && h2 == 4) || !h.b || h.e == null || !h.e.b))) {
                    Log.d("com.qq.connect", "in login name:" + j);
                    if (k == null) {
                        b.a("WifiAdminThread  bytes = null ");
                        c(-2);
                        return;
                    }
                    Native nativeR = new Native();
                    byte[] unEncrypt3 = nativeR.unEncrypt(k, 0, k.length);
                    if (unEncrypt3 == null) {
                        b.a("WifiAdminThread  bytes = null again ");
                        c(-2);
                        return;
                    }
                    String a2 = r.a(unEncrypt3, 0, unEncrypt3.length);
                    if (a2 == null) {
                        b.a("WifiAdminThread  key = null ");
                        c(-2);
                        return;
                    }
                    c(255);
                    long currentTimeMillis = System.currentTimeMillis();
                    a(Constants.STR_EMPTY + currentTimeMillis);
                    int i3 = 0;
                    byte[] bArr = null;
                    try {
                        i3 = b();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    if (i3 <= 0 || i3 >= 32768) {
                        c(-2);
                        Log.d("com.qq.connect", "read encrypt key_length failed!!!!");
                        b.a("WifiAdminThread  read encrypt key_length failed!!!! again, encrypt_length = " + i3);
                        return;
                    }
                    try {
                        bArr = a(i3);
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                    if (bArr == null) {
                        c(-2);
                        Log.d("com.qq.connect", "read encrypt key_bytes failed!!!!");
                        b.a("WifiAdminThread  read encrypt key_bytes failed!!!!");
                        return;
                    }
                    byte[] unEncrypt4 = nativeR.unEncrypt(bArr, 0, bArr.length);
                    if (unEncrypt4 == null) {
                        c(-2);
                        Log.d("com.qq.connect", " decrypt content failed");
                        b.a("WifiAdminThread  decrypt content failed");
                        return;
                    }
                    String b2 = r.b(unEncrypt4);
                    if (b2 == null) {
                        c(-2);
                        Log.d("com.qq.connect", " decrypt content failed");
                        b.a("WifiAdminThread  decrypt content failed again");
                        return;
                    }
                    long j5 = 0;
                    try {
                        j5 = Long.parseLong(b2);
                    } catch (Exception e5) {
                        e5.printStackTrace();
                    }
                    if (j5 != currentTimeMillis) {
                        c(-2);
                        Log.d("com.qq.connect", " the key is wrong!!!!");
                        return;
                    }
                    if (!(h == 0 && h2 == 1) && ((h == 0 && h2 == 2) || !((h == 0 && h2 == 3) || ((h != 0 || h2 != 4) && h == 0 && h2 == 5)))) {
                    }
                    if (s.c(str6)) {
                        b.a("WifiAdminThread  launch From Mobile validate pc_guid " + str6 + " command " + h + " request " + h2);
                        if (h == 0 && (h2 == 1 || h2 == 2)) {
                            AppService.g = this.d.getInetAddress().toString();
                            AppService.h = j;
                            AppService.i = str6;
                            s.a();
                        } else if (h == 0 && h2 == 3) {
                            AppService.a(str6, j, this.d.getInetAddress().getHostAddress().toString(), true);
                        } else if (!(h == 0 && h2 == 4) && h == 0 && h2 == 5) {
                            AppService.D();
                            AppService.a(AppService.BusinessConnectionType.QQ);
                            AppService.g = this.d.getInetAddress().toString();
                            AppService.h = j;
                            AppService.i = str6;
                            s.a();
                        }
                        c(1);
                        return;
                    }
                    b.a("WifiAdminThread launch From PC");
                    if (h == 0 && h2 == 3) {
                        AppService.a(str6, j, this.d.getInetAddress().getHostAddress().toString(), false);
                        c(12);
                    } else if (h == 0 && h2 == 4) {
                        c(12);
                    } else {
                        c(88888);
                        e = false;
                        Log.d("com.qq.connect", "hasDone!!!!!!");
                        Context b3 = AppService.b();
                        if (b3 == null) {
                            context2 = context;
                        } else {
                            context2 = b3;
                        }
                        ((KeyguardManager) context2.getSystemService("keyguard")).newKeyguardLock("unLock").disableKeyguard();
                        ((PowerManager) context2.getSystemService("power")).newWakeLock(268435462, "unlock").acquire(10000);
                        if (h == 0 && h2 == 1) {
                            s.a(new ba(this, j, str6), j, this.d.getInetAddress().toString(), a2, context, str2);
                            int i4 = 0;
                            while (i4 < 1500) {
                                try {
                                    if (e) {
                                        break;
                                    }
                                    try {
                                        this.f232a.read();
                                    } catch (SocketTimeoutException e6) {
                                    }
                                    i4++;
                                } catch (Exception e7) {
                                    e7.printStackTrace();
                                }
                            }
                            if (!e) {
                                s.a(context);
                            }
                        } else if (h == 0 && (h2 == 2 || h2 == 5)) {
                            if (h2 == 2) {
                                AppService.a(AppService.BusinessConnectionType.QRCODE);
                            }
                            if (h2 == 5) {
                                AppService.a(AppService.BusinessConnectionType.QQ);
                            }
                            s.a(new bb(this, j, str6, h, h2), j, this.d.getInetAddress().toString(), a2, context, str6, str2, str);
                            int i5 = 0;
                            while (i5 < 1500) {
                                try {
                                    if (e) {
                                        break;
                                    }
                                    try {
                                        this.f232a.read();
                                    } catch (SocketTimeoutException e8) {
                                    }
                                    i5++;
                                } catch (Exception e9) {
                                    e9.printStackTrace();
                                }
                            }
                            if (!e) {
                                s.a(context);
                            }
                        }
                        Log.d("com.qq.connect", "end the connection");
                    }
                } else {
                    Log.d("com.qq.connect", "chatManager is running &&&&&&&&& isConnected");
                    c(-1);
                }
            }
        }
    }

    private void b(String str) {
        if (this.b != null) {
            if (str == null) {
                try {
                    this.b.write(r.a(4));
                    this.b.write(r.a(-1));
                    this.b.flush();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else if (str.toLowerCase().equals(ax.b)) {
                byte[] a2 = r.a("129");
                byte[] bArr = r.hr;
                try {
                    this.b.write(r.a(4));
                    this.b.write(r.a(0));
                    this.b.write(r.a(a2.length));
                    this.b.write(a2);
                    this.b.write(r.a(bArr.length));
                    this.b.write(bArr);
                    this.b.flush();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else {
                byte[] a3 = r.a("129");
                byte[] bArr2 = r.hs;
                try {
                    this.b.write(r.a(4));
                    this.b.write(r.a(0));
                    this.b.write(r.a(a3.length));
                    this.b.write(a3);
                    this.b.write(r.a(bArr2.length));
                    this.b.write(bArr2);
                    this.b.flush();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    public void run() {
        byte[] bArr;
        byte[] bArr2 = null;
        super.run();
        this.c = true;
        if (this.d == null) {
            a();
            return;
        }
        try {
            this.d.setSoTimeout(EventDispatcherEnum.CONNECTION_EVENT_START);
        } catch (SocketException e2) {
            e2.printStackTrace();
        }
        try {
            this.f232a = this.d.getInputStream();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        if (this.f232a == null) {
            a();
            return;
        }
        try {
            this.b = this.d.getOutputStream();
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        if (this.b == null) {
            a();
            return;
        }
        try {
            int b2 = b();
            if (b2 == 0) {
                int b3 = b();
                if (b3 > 0) {
                    if (b3 > 0 && b3 < 65536) {
                        bArr2 = a(b3);
                    }
                    if (bArr2 == null) {
                        a();
                        a();
                        this.c = false;
                    }
                    b(this.f, new c(bArr2, 0));
                } else if (b3 == 0) {
                    int b4 = b();
                    if (b4 <= 0 || b4 >= 65536) {
                        bArr = null;
                    } else {
                        bArr = a(b4);
                    }
                    if (bArr == null) {
                        a();
                        a();
                        this.c = false;
                    }
                    a(this.f, new c(bArr, 0));
                }
            } else {
                b(b(b2));
            }
            a();
        } catch (Throwable th) {
            a();
            this.c = false;
            throw th;
        }
        this.c = false;
    }
}
