package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.qq.provider.h;
import com.tencent.assistant.st.a;

/* compiled from: ProGuard */
public final class MainReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private long f204a = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.AppService.ac.a(android.content.Context, boolean):android.content.Intent
     arg types: [android.content.Context, int]
     candidates:
      com.qq.AppService.ac.a(android.content.Context, android.content.Intent):void
      com.qq.AppService.ac.a(android.content.Context, boolean):android.content.Intent */
    public void onReceive(Context context, Intent intent) {
        int a2;
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                Log.d("com.qq.connect", "android.net.conn.CONNECTIVITY_CHANGE");
                if (AppService.c || (a2 = ae.a(context)) == 0 || a2 == -1) {
                }
            } else if (intent.getAction().equals("android.intent.action.ACTION_SHUTDOWN")) {
                h.d();
            } else if (intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
                Log.d("com.qq.connect", "android.intent.action.ACTION_POWER_CONNECTED");
                if (!AppService.c) {
                    Intent intent2 = new Intent();
                    intent2.setClass(context, StartApp.class);
                    intent2.setFlags(268435456);
                    try {
                        context.startActivity(intent2);
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                } else {
                    if (!AppService.e) {
                    }
                }
            } else if (intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                Log.d("com.qq.connect", "android.intent.action.ACTION_POWER_DISCONNECTED");
                if (AppService.c && AppService.e) {
                    a.a().c((byte) 3);
                    AppService.a(0);
                }
            } else if (intent.getAction().equals("android.intent.action.UMS_CONNECTED")) {
                if (!AppService.c) {
                    Intent intent3 = new Intent();
                    intent3.setClass(context, StartApp.class);
                    intent3.setFlags(268435456);
                    try {
                        context.startActivity(intent3);
                    } catch (Throwable th2) {
                        th2.printStackTrace();
                    }
                } else if (!AppService.e) {
                }
                Log.d("com.qq.connect", "android.intent.action.UMS_CONNECTED");
                try {
                    ac.a(context, ac.a(context, true));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equals("android.intent.action.UMS_DISCONNECTED")) {
                Log.d("com.qq.connect", "android.intent.action.UMS_DISCONNECTED");
                if (AppService.c && AppService.e) {
                    AppService.a(0);
                }
                context.sendBroadcast(ac.a(context, false));
            } else if (intent.getAction().equals("android.intent.action.MEDIA_MOUNTED")) {
                h.d(0);
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.f204a > 2000) {
                    this.f204a = currentTimeMillis;
                }
            } else if (intent.getAction().equals("android.intent.action.MEDIA_SHARED")) {
                h.d(1);
                long currentTimeMillis2 = System.currentTimeMillis();
                if (currentTimeMillis2 - this.f204a > 2000) {
                    this.f204a = currentTimeMillis2;
                }
            } else if (intent.getAction().equals("android.intent.action.MEDIA_BAD_REMOVAL")) {
                h.d(2);
            } else if (intent.getAction().equals("android.intent.action.MEDIA_REMOVED")) {
                h.d(3);
            } else if (intent.getAction().equals("android.intent.action.MEDIA_UNMOUNTED")) {
                h.d(2);
            } else if (intent.getAction().equals("android.intent.action.MEDIA_UNMOUNTABLE")) {
                h.d(3);
            } else if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                Log.d("com.qq.connect", "android.provider.Telephony.SMS_RECEIVED >>>>>> ");
            }
        }
    }
}
