package com.tencent.beacon.c.d;

import com.tencent.beacon.e.a;
import com.tencent.beacon.e.c;
import com.tencent.beacon.e.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class b extends c implements Cloneable {
    private static byte[] n;

    /* renamed from: a  reason: collision with root package name */
    public String f2117a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public int d = 0;
    public long e = 0;
    public long f = 0;
    public String g = Constants.STR_EMPTY;
    private long h = 0;
    private long i = 0;
    private long j = 0;
    private int k = 0;
    private int l = 0;
    private byte[] m = null;

    public final void a(d dVar) {
        dVar.a(this.f2117a, 0);
        dVar.a(this.b, 1);
        dVar.a(this.c, 2);
        dVar.a(this.d, 3);
        dVar.a(this.e, 4);
        dVar.a(this.f, 5);
        dVar.a(this.h, 6);
        dVar.a(this.i, 7);
        dVar.a(this.j, 8);
        dVar.a(this.k, 9);
        dVar.a(this.l, 10);
        if (this.m != null) {
            dVar.a(this.m, 11);
        }
        if (this.g != null) {
            dVar.a(this.g, 12);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(long, int, boolean):long */
    public final void a(a aVar) {
        this.f2117a = aVar.b(0, true);
        this.b = aVar.b(1, true);
        this.c = aVar.b(2, true);
        this.d = aVar.a(this.d, 3, true);
        this.e = aVar.a(this.e, 4, true);
        this.f = aVar.a(this.f, 5, true);
        this.h = aVar.a(this.h, 6, false);
        this.i = aVar.a(this.i, 7, false);
        this.j = aVar.a(this.j, 8, false);
        this.k = aVar.a(this.k, 9, false);
        this.l = aVar.a(this.l, 10, false);
        if (n == null) {
            byte[] bArr = new byte[1];
            n = bArr;
            bArr[0] = 0;
        }
        byte[] bArr2 = n;
        this.m = aVar.c(11, false);
        this.g = aVar.b(12, false);
    }
}
