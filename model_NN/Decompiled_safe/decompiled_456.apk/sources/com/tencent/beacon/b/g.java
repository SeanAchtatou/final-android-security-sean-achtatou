package com.tencent.beacon.b;

import com.tencent.beacon.c.e.b;
import com.tencent.beacon.c.e.c;
import com.tencent.beacon.c.e.e;
import com.tencent.beacon.c.e.f;
import com.tencent.beacon.e.a;
import com.tencent.beacon.f.i;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* compiled from: ProGuard */
public final class g implements i {
    public final void a(int i, byte[] bArr, boolean z) {
        f d;
        if (i == 105 && bArr != null && (d = f.d()) != null && f.e()) {
            try {
                e eVar = new e();
                eVar.a(new a(bArr));
                List<b> a2 = a(eVar);
                if (a2 != null && a2.size() > 0) {
                    d.a((b[]) a2.toArray(new b[0]));
                }
            } catch (Throwable th) {
                th.printStackTrace();
                com.tencent.beacon.d.a.d(" process sm strategy error: %s", th.toString());
            }
        }
    }

    public static List<b> a(e eVar) {
        if (eVar == null) {
            com.tencent.beacon.d.a.b("SpeedMonitorStrategy sourcePackage is null", new Object[0]);
            return null;
        }
        ArrayList<c> arrayList = eVar.f2124a;
        ArrayList<b> arrayList2 = eVar.b;
        ArrayList<f> arrayList3 = eVar.c;
        ArrayList arrayList4 = new ArrayList();
        if (arrayList != null) {
            com.tencent.beacon.d.a.b("ipList size:%d", Integer.valueOf(arrayList.size()));
            for (c next : arrayList) {
                b bVar = new b();
                bVar.b(next.f2122a + ":" + next.b);
                bVar.a(new Date().getTime());
                bVar.a("IP");
                bVar.b(next.c);
                arrayList4.add(bVar);
            }
        }
        if (arrayList2 != null) {
            com.tencent.beacon.d.a.b("dnsList size:%d", Integer.valueOf(arrayList2.size()));
            for (b next2 : arrayList2) {
                b bVar2 = new b();
                bVar2.b(next2.f2121a);
                bVar2.a(new Date().getTime());
                bVar2.a("PG");
                bVar2.b(next2.c);
                bVar2.a(next2.b);
                arrayList4.add(bVar2);
            }
        }
        if (arrayList3 != null) {
            com.tencent.beacon.d.a.b("hostList size:%d", Integer.valueOf(arrayList3.size()));
            for (f next3 : arrayList3) {
                b bVar3 = new b();
                bVar3.c(next3.d);
                bVar3.d(next3.f2125a + ":" + next3.b);
                bVar3.b(next3.c);
                bVar3.a(new Date().getTime());
                bVar3.a("HOST");
                arrayList4.add(bVar3);
                com.tencent.beacon.d.a.a(" TxHostSource: " + bVar3.toString(), new Object[0]);
            }
        }
        if (arrayList4.size() > 0) {
            return arrayList4;
        }
        return null;
    }
}
