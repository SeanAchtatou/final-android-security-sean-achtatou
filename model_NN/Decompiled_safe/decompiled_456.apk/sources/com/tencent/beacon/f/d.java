package com.tencent.beacon.f;

import android.content.Context;
import com.tencent.beacon.a.b.c;
import com.tencent.beacon.a.b.g;
import com.tencent.beacon.c.a.b;
import com.tencent.beacon.c.c.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class d extends b {
    private Context e = null;
    private b f = null;

    public d(Context context) {
        super(context, 0, 102);
        this.e = context;
    }

    public final void b(boolean z) {
    }

    public final b a() {
        a aVar;
        com.tencent.beacon.d.a.b("QIMEIUploadDatas.getUploadRequestPackage() start", new Object[0]);
        if (this.f != null) {
            return this.f;
        }
        try {
            com.tencent.beacon.b.a a2 = com.tencent.beacon.b.a.a(this.e);
            if (a2 == null) {
                com.tencent.beacon.d.a.c("QIMEIInfo instance is null, return", new Object[0]);
                return null;
            }
            if (a2 == null) {
                aVar = null;
            } else {
                a aVar2 = new a();
                String b = a2.b();
                if (b == null) {
                    b = Constants.STR_EMPTY;
                }
                aVar2.b = b;
                String d = a2.d();
                if (d == null) {
                    d = Constants.STR_EMPTY;
                }
                aVar2.d = d;
                String c = a2.c();
                if (c == null) {
                    c = Constants.STR_EMPTY;
                }
                aVar2.c = c;
                String e2 = a2.e();
                if (e2 == null) {
                    e2 = Constants.STR_EMPTY;
                }
                aVar2.e = e2;
                String a3 = a2.a();
                if (a3 == null) {
                    a3 = Constants.STR_EMPTY;
                }
                aVar2.f2115a = a3;
                aVar = aVar2;
            }
            g b2 = c.a(this.c).b();
            byte b3 = 1;
            byte b4 = 2;
            String str = "*^@K#K@!";
            if (b2 != null) {
                b3 = b2.i();
                b4 = b2.j();
                str = b2.k();
            }
            byte[] a4 = com.tencent.beacon.b.a.a(aVar.a(), b4, b3, str);
            if (a4 == null) {
                com.tencent.beacon.d.a.d("encodeDatasByZipAndEncry failed!", new Object[0]);
                return null;
            }
            this.f = com.tencent.beacon.b.a.a(this.f2149a, com.tencent.beacon.a.g.m(), a4, b4, b3);
            return this.f;
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
