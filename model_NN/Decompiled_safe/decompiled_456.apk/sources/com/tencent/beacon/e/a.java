package com.tencent.beacon.e;

import android.os.Process;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private ByteBuffer f2127a;
    private String b = "GBK";

    public a() {
    }

    public a(byte[] bArr) {
        this.f2127a = ByteBuffer.wrap(bArr);
    }

    public a(byte[] bArr, int i) {
        this.f2127a = ByteBuffer.wrap(bArr);
        this.f2127a.position(4);
    }

    public final void a(byte[] bArr) {
        this.f2127a = ByteBuffer.wrap(bArr);
    }

    private static int a(b bVar, ByteBuffer byteBuffer) {
        byte b2 = byteBuffer.get();
        bVar.f2128a = (byte) (b2 & 15);
        bVar.b = (b2 & 240) >> 4;
        if (bVar.b != 15) {
            return 1;
        }
        bVar.b = byteBuffer.get() & 255;
        return 2;
    }

    private boolean a(int i) {
        try {
            b bVar = new b();
            while (true) {
                int a2 = a(bVar, this.f2127a.duplicate());
                if (bVar.f2128a == 11) {
                    return false;
                }
                if (i > bVar.b) {
                    this.f2127a.position(a2 + this.f2127a.position());
                    a(bVar.f2128a);
                } else if (i == bVar.b) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (RuntimeException | BufferUnderflowException e) {
            return false;
        }
    }

    private void a() {
        b bVar = new b();
        do {
            a(bVar, this.f2127a);
            a(bVar.f2128a);
        } while (bVar.f2128a != 11);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    private void a(byte b2) {
        int i = 0;
        switch (b2) {
            case 0:
                this.f2127a.position(this.f2127a.position() + 1);
                return;
            case 1:
                this.f2127a.position(2 + this.f2127a.position());
                return;
            case 2:
                this.f2127a.position(this.f2127a.position() + 4);
                return;
            case 3:
                this.f2127a.position(this.f2127a.position() + 8);
                return;
            case 4:
                this.f2127a.position(this.f2127a.position() + 4);
                return;
            case 5:
                this.f2127a.position(this.f2127a.position() + 8);
                return;
            case 6:
                int i2 = this.f2127a.get();
                if (i2 < 0) {
                    i2 += Process.PROC_COMBINE;
                }
                this.f2127a.position(i2 + this.f2127a.position());
                return;
            case 7:
                this.f2127a.position(this.f2127a.getInt() + this.f2127a.position());
                return;
            case 8:
                int a2 = a(0, 0, true);
                while (i < (a2 << 1)) {
                    b bVar = new b();
                    a(bVar, this.f2127a);
                    a(bVar.f2128a);
                    i++;
                }
                return;
            case 9:
                int a3 = a(0, 0, true);
                while (i < a3) {
                    b bVar2 = new b();
                    a(bVar2, this.f2127a);
                    a(bVar2.f2128a);
                    i++;
                }
                return;
            case 10:
                a();
                return;
            case 11:
            case 12:
                return;
            case 13:
                b bVar3 = new b();
                a(bVar3, this.f2127a);
                if (bVar3.f2128a != 0) {
                    throw new RuntimeException("skipField with invalid type, type value: " + ((int) b2) + ", " + ((int) bVar3.f2128a));
                }
                this.f2127a.position(a(0, 0, true) + this.f2127a.position());
                return;
            default:
                throw new RuntimeException("invalid type.");
        }
    }

    public final boolean a(int i, boolean z) {
        if (a((byte) 0, i, z) != 0) {
            return true;
        }
        return false;
    }

    public final byte a(byte b2, int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 0:
                    return this.f2127a.get();
                case 12:
                    return 0;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return b2;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    public final short a(short s, int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 0:
                    return (short) this.f2127a.get();
                case 1:
                    return this.f2127a.getShort();
                case 12:
                    return 0;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return s;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    public final int a(int i, int i2, boolean z) {
        if (a(i2)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 0:
                    return this.f2127a.get();
                case 1:
                    return this.f2127a.getShort();
                case 2:
                    return this.f2127a.getInt();
                case 12:
                    return 0;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return i;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    public final long a(long j, int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 0:
                    return (long) this.f2127a.get();
                case 1:
                    return (long) this.f2127a.getShort();
                case 2:
                    return (long) this.f2127a.getInt();
                case 3:
                    return this.f2127a.getLong();
                case 12:
                    return 0;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return j;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    private float a(float f, int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 4:
                    return this.f2127a.getFloat();
                case 12:
                    return 0.0f;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return f;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    private double a(double d, int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 4:
                    return (double) this.f2127a.getFloat();
                case 5:
                    return this.f2127a.getDouble();
                case 12:
                    return 0.0d;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return d;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    public final String b(int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 6:
                    int i2 = this.f2127a.get();
                    if (i2 < 0) {
                        i2 += Process.PROC_COMBINE;
                    }
                    byte[] bArr = new byte[i2];
                    this.f2127a.get(bArr);
                    try {
                        return new String(bArr, this.b);
                    } catch (UnsupportedEncodingException e) {
                        return new String(bArr);
                    }
                case 7:
                    int i3 = this.f2127a.getInt();
                    if (i3 > 104857600 || i3 < 0 || i3 > this.f2127a.capacity()) {
                        throw new RuntimeException("String too long: " + i3);
                    }
                    byte[] bArr2 = new byte[i3];
                    this.f2127a.get(bArr2);
                    try {
                        return new String(bArr2, this.b);
                    } catch (UnsupportedEncodingException e2) {
                        return new String(bArr2);
                    }
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    public final <K, V> HashMap<K, V> a(Map map, int i, boolean z) {
        return (HashMap) a(new HashMap(), map, i, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.lang.Object, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    private <K, V> Map<K, V> a(Map<K, V> map, Map<K, V> map2, int i, boolean z) {
        if (map2 == null || map2.isEmpty()) {
            return new HashMap();
        }
        Map.Entry next = map2.entrySet().iterator().next();
        Object key = next.getKey();
        Object value = next.getValue();
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 8:
                    int a2 = a(0, 0, true);
                    if (a2 < 0) {
                        throw new RuntimeException("size invalid: " + a2);
                    }
                    for (int i2 = 0; i2 < a2; i2++) {
                        map.put(a(key, 0, true), a(value, 1, true));
                    }
                    return map;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return map;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(byte, int, boolean):byte */
    private boolean[] d(int i, boolean z) {
        boolean z2;
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    int a2 = a(0, 0, true);
                    if (a2 < 0) {
                        throw new RuntimeException("size invalid: " + a2);
                    }
                    boolean[] zArr = new boolean[a2];
                    for (int i2 = 0; i2 < a2; i2++) {
                        if (a((byte) 0, 0, true) != 0) {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        zArr[i2] = z2;
                    }
                    return zArr;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(byte, int, boolean):byte */
    public final byte[] c(int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    int a2 = a(0, 0, true);
                    if (a2 < 0 || a2 > this.f2127a.capacity()) {
                        throw new RuntimeException("size invalid: " + a2);
                    }
                    byte[] bArr = new byte[a2];
                    for (int i2 = 0; i2 < a2; i2++) {
                        bArr[i2] = a(bArr[0], 0, true);
                    }
                    return bArr;
                case 13:
                    b bVar2 = new b();
                    a(bVar2, this.f2127a);
                    if (bVar2.f2128a != 0) {
                        throw new RuntimeException("type mismatch, tag: " + i + ", type: " + ((int) bVar.f2128a) + ", " + ((int) bVar2.f2128a));
                    }
                    int a3 = a(0, 0, true);
                    if (a3 < 0 || a3 > this.f2127a.capacity()) {
                        throw new RuntimeException("invalid size, tag: " + i + ", type: " + ((int) bVar.f2128a) + ", " + ((int) bVar2.f2128a) + ", size: " + a3);
                    }
                    byte[] bArr2 = new byte[a3];
                    this.f2127a.get(bArr2);
                    return bArr2;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short */
    private short[] e(int i, boolean z) {
        short[] sArr = null;
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    break;
                default:
                    throw new RuntimeException("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new RuntimeException("size invalid: " + a2);
            }
            sArr = new short[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                sArr[i2] = a(sArr[0], 0, true);
            }
        } else if (z) {
            throw new RuntimeException("require field not exist.");
        }
        return sArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    private int[] f(int i, boolean z) {
        int[] iArr = null;
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    break;
                default:
                    throw new RuntimeException("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new RuntimeException("size invalid: " + a2);
            }
            iArr = new int[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                iArr[i2] = a(iArr[0], 0, true);
            }
        } else if (z) {
            throw new RuntimeException("require field not exist.");
        }
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(long, int, boolean):long */
    private long[] g(int i, boolean z) {
        long[] jArr = null;
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    break;
                default:
                    throw new RuntimeException("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new RuntimeException("size invalid: " + a2);
            }
            jArr = new long[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                jArr[i2] = a(jArr[0], 0, true);
            }
        } else if (z) {
            throw new RuntimeException("require field not exist.");
        }
        return jArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(float, int, boolean):float
     arg types: [float, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(float, int, boolean):float */
    private float[] h(int i, boolean z) {
        float[] fArr = null;
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    break;
                default:
                    throw new RuntimeException("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new RuntimeException("size invalid: " + a2);
            }
            fArr = new float[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                fArr[i2] = a(fArr[0], 0, true);
            }
        } else if (z) {
            throw new RuntimeException("require field not exist.");
        }
        return fArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(double, int, boolean):double
     arg types: [double, int, int]
     candidates:
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(double, int, boolean):double */
    private double[] i(int i, boolean z) {
        double[] dArr = null;
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    break;
                default:
                    throw new RuntimeException("type mismatch.");
            }
            int a2 = a(0, 0, true);
            if (a2 < 0) {
                throw new RuntimeException("size invalid: " + a2);
            }
            dArr = new double[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                dArr[i2] = a(dArr[0], 0, true);
            }
        } else if (z) {
            throw new RuntimeException("require field not exist.");
        }
        return dArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    private <T> T[] b(T t, int i, boolean z) {
        if (a(i)) {
            b bVar = new b();
            a(bVar, this.f2127a);
            switch (bVar.f2128a) {
                case 9:
                    int a2 = a(0, 0, true);
                    if (a2 < 0) {
                        throw new RuntimeException("size invalid: " + a2);
                    }
                    T[] tArr = (Object[]) Array.newInstance(t.getClass(), a2);
                    for (int i2 = 0; i2 < a2; i2++) {
                        tArr[i2] = a((Object) t, 0, true);
                    }
                    return tArr;
                default:
                    throw new RuntimeException("type mismatch.");
            }
        } else if (!z) {
            return null;
        } else {
            throw new RuntimeException("require field not exist.");
        }
    }

    public final c a(c cVar, int i, boolean z) {
        c cVar2 = null;
        if (a(i)) {
            try {
                cVar2 = (c) cVar.getClass().newInstance();
                b bVar = new b();
                a(bVar, this.f2127a);
                if (bVar.f2128a != 10) {
                    throw new RuntimeException("type mismatch.");
                }
                cVar2.a(this);
                a();
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        } else if (z) {
            throw new RuntimeException("require field not exist.");
        }
        return cVar2;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v14, types: [int] */
    /* JADX WARN: Type inference failed for: r0v35, types: [boolean] */
    /* JADX WARN: Type inference failed for: r0v37 */
    /* JADX WARN: Type inference failed for: r0v40 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(short, int, boolean):short
     arg types: [int, int, boolean]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(long, int, boolean):long
     arg types: [int, int, boolean]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(float, int, boolean):float
     arg types: [int, int, boolean]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(float, int, boolean):float */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(double, int, boolean):double
     arg types: [int, int, boolean]
     candidates:
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(double, int, boolean):double */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> java.lang.Object a(java.lang.Object r5, int r6, boolean r7) {
        /*
            r4 = this;
            r0 = 0
            boolean r1 = r5 instanceof java.lang.Byte
            if (r1 == 0) goto L_0x000e
            byte r0 = r4.a(r0, r6, r7)
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
        L_0x000d:
            return r0
        L_0x000e:
            boolean r1 = r5 instanceof java.lang.Boolean
            if (r1 == 0) goto L_0x001e
            byte r1 = r4.a(r0, r6, r7)
            if (r1 == 0) goto L_0x0019
            r0 = 1
        L_0x0019:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            goto L_0x000d
        L_0x001e:
            boolean r1 = r5 instanceof java.lang.Short
            if (r1 == 0) goto L_0x002b
            short r0 = r4.a(r0, r6, r7)
            java.lang.Short r0 = java.lang.Short.valueOf(r0)
            goto L_0x000d
        L_0x002b:
            boolean r1 = r5 instanceof java.lang.Integer
            if (r1 == 0) goto L_0x0038
            int r0 = r4.a(r0, r6, r7)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            goto L_0x000d
        L_0x0038:
            boolean r1 = r5 instanceof java.lang.Long
            if (r1 == 0) goto L_0x0047
            r0 = 0
            long r0 = r4.a(r0, r6, r7)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            goto L_0x000d
        L_0x0047:
            boolean r1 = r5 instanceof java.lang.Float
            if (r1 == 0) goto L_0x0055
            r0 = 0
            float r0 = r4.a(r0, r6, r7)
            java.lang.Float r0 = java.lang.Float.valueOf(r0)
            goto L_0x000d
        L_0x0055:
            boolean r1 = r5 instanceof java.lang.Double
            if (r1 == 0) goto L_0x0064
            r0 = 0
            double r0 = r4.a(r0, r6, r7)
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            goto L_0x000d
        L_0x0064:
            boolean r1 = r5 instanceof java.lang.String
            if (r1 == 0) goto L_0x006d
            java.lang.String r0 = r4.b(r6, r7)
            goto L_0x000d
        L_0x006d:
            boolean r1 = r5 instanceof java.util.Map
            if (r1 == 0) goto L_0x007f
            java.util.Map r5 = (java.util.Map) r5
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.util.Map r0 = r4.a(r0, r5, r6, r7)
            java.util.HashMap r0 = (java.util.HashMap) r0
            goto L_0x000d
        L_0x007f:
            boolean r1 = r5 instanceof java.util.List
            if (r1 == 0) goto L_0x00b4
            java.util.List r5 = (java.util.List) r5
            if (r5 == 0) goto L_0x008d
            boolean r1 = r5.isEmpty()
            if (r1 == 0) goto L_0x0094
        L_0x008d:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            goto L_0x000d
        L_0x0094:
            java.lang.Object r1 = r5.get(r0)
            java.lang.Object[] r2 = r4.b(r1, r6, r7)
            if (r2 != 0) goto L_0x00a1
            r0 = 0
            goto L_0x000d
        L_0x00a1:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
        L_0x00a6:
            int r3 = r2.length
            if (r0 < r3) goto L_0x00ac
            r0 = r1
            goto L_0x000d
        L_0x00ac:
            r3 = r2[r0]
            r1.add(r3)
            int r0 = r0 + 1
            goto L_0x00a6
        L_0x00b4:
            boolean r1 = r5 instanceof com.tencent.beacon.e.c
            if (r1 == 0) goto L_0x00c0
            com.tencent.beacon.e.c r5 = (com.tencent.beacon.e.c) r5
            com.tencent.beacon.e.c r0 = r4.a(r5, r6, r7)
            goto L_0x000d
        L_0x00c0:
            java.lang.Class r1 = r5.getClass()
            boolean r1 = r1.isArray()
            if (r1 == 0) goto L_0x012b
            boolean r1 = r5 instanceof byte[]
            if (r1 != 0) goto L_0x00d2
            boolean r1 = r5 instanceof java.lang.Byte[]
            if (r1 == 0) goto L_0x00d8
        L_0x00d2:
            byte[] r0 = r4.c(r6, r7)
            goto L_0x000d
        L_0x00d8:
            boolean r1 = r5 instanceof boolean[]
            if (r1 == 0) goto L_0x00e2
            boolean[] r0 = r4.d(r6, r7)
            goto L_0x000d
        L_0x00e2:
            boolean r1 = r5 instanceof short[]
            if (r1 == 0) goto L_0x00ec
            short[] r0 = r4.e(r6, r7)
            goto L_0x000d
        L_0x00ec:
            boolean r1 = r5 instanceof int[]
            if (r1 == 0) goto L_0x00f6
            int[] r0 = r4.f(r6, r7)
            goto L_0x000d
        L_0x00f6:
            boolean r1 = r5 instanceof long[]
            if (r1 == 0) goto L_0x0100
            long[] r0 = r4.g(r6, r7)
            goto L_0x000d
        L_0x0100:
            boolean r1 = r5 instanceof float[]
            if (r1 == 0) goto L_0x010a
            float[] r0 = r4.h(r6, r7)
            goto L_0x000d
        L_0x010a:
            boolean r1 = r5 instanceof double[]
            if (r1 == 0) goto L_0x0114
            double[] r0 = r4.i(r6, r7)
            goto L_0x000d
        L_0x0114:
            java.lang.Object[] r5 = (java.lang.Object[]) r5
            if (r5 == 0) goto L_0x011b
            int r1 = r5.length
            if (r1 != 0) goto L_0x0123
        L_0x011b:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "unable to get type of key and value."
            r0.<init>(r1)
            throw r0
        L_0x0123:
            r0 = r5[r0]
            java.lang.Object[] r0 = r4.b(r0, r6, r7)
            goto L_0x000d
        L_0x012b:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "read object error: unsupport type."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object");
    }

    public final int a(String str) {
        this.b = str;
        return 0;
    }
}
