package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.d;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.beacon.d.b;
import com.tencent.beacon.f.j;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ProGuard */
public final class m implements l {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2141a = false;
    private List<i> b;
    private Context c;
    private Runnable d = new n(this);

    public m(Context context) {
        this.c = context;
        this.b = Collections.synchronizedList(new ArrayList(25));
    }

    public final synchronized boolean a(i iVar) {
        boolean z = false;
        synchronized (this) {
            Object[] objArr = new Object[3];
            objArr[0] = iVar == null ? "null" : iVar.d();
            objArr[1] = true;
            objArr[2] = iVar == null ? "null" : Boolean.valueOf(iVar.f());
            a.f(" BF eN:%s   isRT:%b  isCR:%b", objArr);
            if (this.c == null || iVar == null) {
                a.c("processUA return false, context is null or bean is null !", new Object[0]);
            } else if (!c()) {
                a.c("processUA return false, isEnable is false !", new Object[0]);
            } else {
                g g = t.d().g();
                int a2 = g.a();
                long b2 = (long) (g.b() * 1000);
                if (this.b.size() >= a2) {
                    a.f(" BF mN!", new Object[0]);
                    d.a().a(this.d);
                    d.a().a(103, this.d, b2, b2);
                }
                this.b.add(iVar);
                if (this.b.size() >= a2) {
                    a.c(" err BF 3R! num:" + this.b.size(), new Object[0]);
                }
                if ("rqd_applaunched".equals(iVar.d())) {
                    d.a().a(this.d);
                }
                a.a("processUA:true!", new Object[0]);
                z = true;
            }
        }
        return z;
    }

    public final synchronized List<i> a() {
        ArrayList arrayList;
        if (this.b == null || this.b.size() <= 0 || !c()) {
            arrayList = null;
        } else {
            arrayList = new ArrayList();
            arrayList.addAll(this.b);
            this.b.clear();
            a.b(" get realEventCnt in Mem:" + arrayList.size(), new Object[0]);
        }
        return arrayList;
    }

    private synchronized boolean c() {
        return this.f2141a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.a.d.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.beacon.a.d.a(java.lang.Runnable, long):void
      com.tencent.beacon.a.d.a(int, boolean):void */
    public final synchronized void a(boolean z) {
        if (this.f2141a != z) {
            if (z) {
                this.f2141a = z;
                d.a().a(103, this.d, 5000, (long) (t.d().g().b() * 1000));
            } else {
                d.a().a(103, true);
                if (this.b != null && this.b.size() > 0) {
                    h.a(this.c, this.b);
                }
                this.f2141a = z;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (!c()) {
            a.c(" err su 1R", new Object[0]);
            return;
        }
        List<i> a2 = a();
        if (a2 != null && a2.size() > 0) {
            j f = t.d().f();
            if (!b.b(this.c) || f == null) {
                a.f(" dsu real events 2 db" + a2.size(), new Object[0]);
                h.a(this.c, a2);
                return;
            }
            a.f(" dsu real events 2 up " + a2.size(), new Object[0]);
            f.a(new o(this.c, a2));
        }
    }
}
