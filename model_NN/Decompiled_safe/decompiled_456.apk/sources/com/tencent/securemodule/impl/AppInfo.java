package com.tencent.securemodule.impl;

import java.io.Serializable;

public class AppInfo implements Serializable {
    private static final long serialVersionUID = -7230176056725930756L;
    public String apkPath;
    public int appType;
    public String certMd5;
    public long fileSize;
    public int isOfficial;
    public String pkgName;
    public int safeLevel;
    public int safeType;
    public String softName;
    public int versionCode;
    public String versionName;

    public String getApkPath() {
        return this.apkPath;
    }

    public int getAppType() {
        return this.appType;
    }

    public String getCertMd5() {
        return this.certMd5;
    }

    public long getFileSize() {
        return this.fileSize;
    }

    public String getPkgName() {
        return this.pkgName;
    }

    public String getSoftName() {
        return this.softName;
    }

    public int getVersionCode() {
        return this.versionCode;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setApkPath(String str) {
        this.apkPath = str;
    }

    public void setAppType(int i) {
        this.appType = i;
    }

    public void setCertMd5(String str) {
        this.certMd5 = str;
    }

    public void setFileSize(long j) {
        this.fileSize = j;
    }

    public void setPkgName(String str) {
        this.pkgName = str;
    }

    public void setSoftName(String str) {
        this.softName = str;
    }

    public void setVersionCode(int i) {
        this.versionCode = i;
    }

    public void setVersionName(String str) {
        this.versionName = str;
    }
}
