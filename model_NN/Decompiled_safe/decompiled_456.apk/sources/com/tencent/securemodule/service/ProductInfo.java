package com.tencent.securemodule.service;

public class ProductInfo {
    public int buildNo;
    public String channelId;
    public int productId;
    public String qq;
    public int subPlatformId;
    public String version;

    public ProductInfo() {
        this.subPlatformId = Constants.PLATFROM_ANDROID_PHONE;
    }

    public ProductInfo(int i, String str, int i2, int i3, String str2, String str3) {
        this.productId = i;
        this.version = str;
        this.buildNo = i2;
        this.subPlatformId = i3;
        this.channelId = str2;
        this.qq = str3;
    }

    public int getBuildNo() {
        return this.buildNo;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public int getProductId() {
        return this.productId;
    }

    public String getQq() {
        return this.qq;
    }

    public int getSubPlatformId() {
        return this.subPlatformId;
    }

    public String getVersion() {
        return this.version;
    }

    public void setBuildNo(int i) {
        this.buildNo = i;
    }

    public void setChannelId(String str) {
        this.channelId = str;
    }

    public void setProductId(int i) {
        this.productId = i;
    }

    public void setQq(String str) {
        this.qq = str;
    }

    public void setSubPlatformId(int i) {
        this.subPlatformId = i;
    }

    public void setVersion(String str) {
        this.version = str;
    }
}
