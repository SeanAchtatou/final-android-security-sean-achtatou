package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.protocol.jce.GetCommentListResponse;

/* compiled from: ProGuard */
class p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f3132a;
    private int b = 0;
    private byte[] c;
    private GetCommentListResponse d;
    private int e = -1;

    public p(k kVar) {
        this.f3132a = kVar;
    }

    public void a(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            b();
            this.b = 1;
            this.c = bArr;
            this.e = this.f3132a.a(this.c);
        }
    }

    public int a() {
        return this.e;
    }

    public void b() {
        this.e = -1;
        this.c = null;
        this.d = null;
        this.b = 0;
    }

    public boolean c() {
        return this.b == 1;
    }

    public byte[] d() {
        return this.c;
    }

    public GetCommentListResponse e() {
        return this.d;
    }

    public void a(GetCommentListResponse getCommentListResponse) {
        this.d = getCommentListResponse;
        this.b = 2;
    }

    public void f() {
        this.b = 2;
    }
}
