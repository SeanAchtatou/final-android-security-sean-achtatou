package com.tencent.nucleus.socialcontact.tagpage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.component.TxTextureView;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
public class as implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener {
    private static as b = null;
    private static final Object c = new Object();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RelativeLayout f3190a = null;
    private TextureView d = null;
    private SurfaceView e = null;
    private aw f = null;
    private ay g = null;
    /* access modifiers changed from: private */
    public ImageView h = null;
    private int i = 1;
    /* access modifiers changed from: private */
    public MediaPlayer j = null;
    private int k = 0;
    /* access modifiers changed from: private */
    public String l = Constants.STR_EMPTY;
    private WeakReference<Context> m = null;

    private as() {
    }

    public static as a() {
        as asVar;
        synchronized (c) {
            if (b == null) {
                b = new as();
            }
            asVar = b;
        }
        return asVar;
    }

    public void a(Context context, RelativeLayout relativeLayout, int i2, TPVideoDownInfo tPVideoDownInfo, int i3) {
        if (context != null && relativeLayout != null && tPVideoDownInfo != null) {
            if (this.m == null) {
                this.m = new WeakReference<>(context);
            }
            this.i = i2;
            this.f3190a = relativeLayout;
            this.k = i3;
            if (this.h == null) {
                this.h = b();
            }
            if (this.h != null) {
                this.h.clearAnimation();
                this.h.setVisibility(8);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1, 1);
                layoutParams.addRule(13, -1);
                if (Build.VERSION.SDK_INT >= 14) {
                    this.d = new TxTextureView(this.m.get());
                    relativeLayout.removeAllViews();
                    relativeLayout.addView(this.d, layoutParams);
                    relativeLayout.addView(this.h);
                    if (this.f == null) {
                        this.f = new aw(this, null);
                    }
                    this.f.a(tPVideoDownInfo.c);
                    this.d.setSurfaceTextureListener(this.f);
                } else {
                    this.e = new SurfaceView(this.m.get());
                    this.e.getHolder().setFormat(-3);
                    relativeLayout.removeAllViews();
                    relativeLayout.addView(this.e, layoutParams);
                    relativeLayout.addView(this.h);
                    SurfaceHolder holder = this.e.getHolder();
                    if (holder != null) {
                        if (this.g == null) {
                            this.g = new ay(this, null);
                        }
                        this.g.a(tPVideoDownInfo.c);
                        holder.addCallback(this.g);
                        if (Build.VERSION.SDK_INT < 11) {
                            holder.setType(3);
                        }
                    }
                }
                Animation loadAnimation = AnimationUtils.loadAnimation(AstApp.i(), R.anim.tag_page_video_fullscreen_in);
                loadAnimation.setAnimationListener(new at(this));
                this.f3190a.startAnimation(loadAnimation);
            }
        }
    }

    public ImageView b() {
        if (this.m.get() == null) {
            return null;
        }
        ImageView imageView = new ImageView(this.m.get());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13, -1);
        imageView.setBackgroundColor(Color.parseColor("#ccffffff"));
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        XLog.i("VideoFullScreenManager", "[onError] ---> what = " + i2 + ", extra = " + i3);
        mediaPlayer.reset();
        return false;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        if (this.h != null) {
            this.h.setVisibility(0);
            Animation loadAnimation = AnimationUtils.loadAnimation(AstApp.i(), R.anim.tag_page_video_play_finish);
            loadAnimation.setAnimationListener(new au(this, mediaPlayer));
            this.h.startAnimation(loadAnimation);
        }
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void c() {
        if (this.j != null && this.j.isPlaying()) {
            this.j.stop();
            this.j.release();
            this.j = null;
        }
        if (this.f3190a != null) {
            Animation loadAnimation = AnimationUtils.loadAnimation(AstApp.i(), R.anim.tag_page_video_fullscreen_out);
            loadAnimation.setAnimationListener(new av(this));
            this.f3190a.startAnimation(loadAnimation);
        }
        if (this.m != null) {
            this.m.clear();
        }
        this.m = null;
        this.h = null;
        this.d = null;
        this.e = null;
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.m != null && (this.m.get() instanceof Activity)) {
            ((Activity) this.m.get()).getWindow().setFlags(1024, 1024);
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        int videoWidth = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();
        XLog.i("VideoFullScreenManager", "[onPrepared] ---> videoWidth = " + videoWidth + ", videoHeight = " + videoHeight);
        if (videoHeight != 0 && videoWidth != 0) {
            RelativeLayout.LayoutParams layoutParams = null;
            if (Build.VERSION.SDK_INT >= 14) {
                if (!(this.d == null || (layoutParams = (RelativeLayout.LayoutParams) this.d.getLayoutParams()) == null)) {
                    if (1 == this.i) {
                        layoutParams.width = by.b();
                        layoutParams.height = (int) (((((float) videoHeight) * 1.0f) / ((float) videoWidth)) * ((float) layoutParams.width));
                    } else {
                        layoutParams.width = -1;
                        layoutParams.height = -1;
                    }
                    this.d.setLayoutParams(layoutParams);
                }
                if (this.h != null) {
                    this.h.setLayoutParams(layoutParams);
                }
            } else {
                if (!(this.e == null || (layoutParams = (RelativeLayout.LayoutParams) this.e.getLayoutParams()) == null)) {
                    if (1 == this.i) {
                        layoutParams.width = by.b();
                        layoutParams.height = (int) (((((float) videoHeight) * 1.0f) / ((float) videoWidth)) * ((float) layoutParams.width));
                    } else {
                        layoutParams.width = -1;
                        layoutParams.height = -1;
                    }
                    this.e.setLayoutParams(layoutParams);
                }
                if (this.h != null) {
                    this.h.setLayoutParams(layoutParams);
                }
            }
            XLog.i("VideoFullScreenManager", "[onSurfaceTextureAvailable] ---> mPlayBeginTime = " + this.k);
            mediaPlayer.seekTo(this.k);
        }
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        XLog.i("VideoFullScreenManager", "*** onSeekComplete ***");
        try {
            mediaPlayer.start();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
