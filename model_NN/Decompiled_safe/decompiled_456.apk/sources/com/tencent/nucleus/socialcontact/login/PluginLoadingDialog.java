package com.tencent.nucleus.socialcontact.login;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.listener.UIEventListener;

/* compiled from: ProGuard */
public class PluginLoadingDialog extends Dialog implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    protected final int f3154a = 350;
    protected int b;
    protected float c = 0.0f;
    private ProgressBar d;
    private View e;
    private Button f;
    private View g;

    public PluginLoadingDialog(Context context) {
        super(context, R.style.dialog);
    }

    public void handleUIEvent(Message message) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c();
        setContentView((int) R.layout.plugin_loading_layout);
        d();
    }

    public void onStart() {
        super.onStart();
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        Display defaultDisplay = window.getWindowManager().getDefaultDisplay();
        attributes.width = (int) (((float) defaultDisplay.getWidth()) * 0.8667f);
        attributes.verticalMargin = this.c;
        this.b = defaultDisplay.getHeight();
        window.setAttributes(attributes);
        setCanceledOnTouchOutside(true);
    }

    private void d() {
        this.d = (ProgressBar) findViewById(R.id.loading_progress);
        this.e = findViewById(R.id.error_layout);
        this.g = findViewById(R.id.loading_layout);
        this.f = (Button) findViewById(R.id.reload_btn);
        this.f.setOnClickListener(new o(this));
    }

    public void a() {
        this.g.setVisibility(8);
        this.e.setVisibility(0);
    }

    public void b() {
        this.g.setVisibility(0);
        this.e.setVisibility(8);
    }

    public void c() {
    }

    public void dismiss() {
        super.dismiss();
        getOwnerActivity().finish();
    }
}
