package com.tencent.nucleus.socialcontact.comment;

import android.view.animation.Animation;
import android.widget.TextView;

/* compiled from: ProGuard */
class ao implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListAdapter f3097a;
    private TextView b;

    public ao(CommentReplyListAdapter commentReplyListAdapter, TextView textView) {
        this.f3097a = commentReplyListAdapter;
        this.b = textView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.b.setVisibility(8);
    }

    public void onAnimationRepeat(Animation animation) {
    }
}
