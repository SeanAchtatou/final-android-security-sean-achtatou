package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bk extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f3117a;

    bk(PopViewDialog popViewDialog) {
        this.f3117a = popViewDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.comment.PopViewDialog.a(com.tencent.nucleus.socialcontact.comment.PopViewDialog, boolean):boolean
     arg types: [com.tencent.nucleus.socialcontact.comment.PopViewDialog, int]
     candidates:
      com.tencent.nucleus.socialcontact.comment.PopViewDialog.a(int, java.lang.String):void
      com.tencent.nucleus.socialcontact.comment.PopViewDialog.a(com.tencent.nucleus.socialcontact.comment.PopViewDialog, boolean):boolean */
    public void onTMAClick(View view) {
        this.f3117a.w.setSelected(!this.f3117a.w.isSelected());
        if (this.f3117a.w.isSelected()) {
            boolean unused = this.f3117a.B = true;
            boolean unused2 = this.f3117a.A = false;
            this.f3117a.u.setSelected(false);
            if (!this.f3117a.z.l()) {
                this.f3117a.g.setText((int) R.string.comment_share_wxtips);
                this.f3117a.g.setVisibility(0);
                return;
            }
            return;
        }
        boolean unused3 = this.f3117a.B = false;
        this.f3117a.g.setVisibility(4);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 c = this.f3117a.i();
        if (c != null) {
            c.slotId = a.a(this.f3117a.h(), "005");
            if (!this.f3117a.u.isSelected()) {
                c.status = "01";
            } else {
                c.status = "02";
            }
            c.actionId = 200;
        }
        return c;
    }
}
