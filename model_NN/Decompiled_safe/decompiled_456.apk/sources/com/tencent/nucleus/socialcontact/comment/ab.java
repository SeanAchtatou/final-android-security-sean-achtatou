package com.tencent.nucleus.socialcontact.comment;

import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.socialcontact.login.l;

/* compiled from: ProGuard */
class ab extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListActivity f3084a;

    ab(CommentReplyListActivity commentReplyListActivity) {
        this.f3084a = commentReplyListActivity;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.appdetail_reply_btn_for_login /*2131165890*/:
                this.f3084a.z();
                int unused = this.f3084a.V = 0;
                this.f3084a.B();
                return;
            case R.id.appdetail_reply_btn_for_send /*2131165894*/:
                this.f3084a.z();
                if (TextUtils.isEmpty(this.f3084a.T.getText().toString())) {
                    Toast.makeText(this.f3084a, (int) R.string.please_reply_tip, 0).show();
                    return;
                } else if (this.f3084a.T.getText().toString().length() > 100) {
                    Toast.makeText(this.f3084a, (int) R.string.comment_expendcount_error, 0).show();
                    return;
                } else if (!this.f3084a.U.j()) {
                    this.f3084a.B();
                    return;
                } else {
                    this.f3084a.S.a(false);
                    this.f3084a.J.a(0, this.f3084a.K.h, this.f3084a.T.getText().toString(), this.f3084a.U.q(), l.f().f3165a, this.f3084a.L, this.f3084a.O, this.f3084a.V);
                    return;
                }
            default:
                return;
        }
    }

    public STInfoV2 getStInfo() {
        return null;
    }
}
