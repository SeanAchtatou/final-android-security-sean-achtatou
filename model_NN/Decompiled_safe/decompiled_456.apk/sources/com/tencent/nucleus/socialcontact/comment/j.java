package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import java.util.List;

/* compiled from: ProGuard */
public interface j extends ActionCallback {
    void a(int i, int i2, long j, int i3, long j2);

    void a(int i, int i2, long j, String str, int i3, long j2);

    void a(int i, int i2, long j, String str, String str2, long j2);

    void a(int i, int i2, CommentDetail commentDetail, long j);

    void a(int i, int i2, boolean z, CommentTagInfo commentTagInfo, List<CommentDetail> list, List<CommentDetail> list2, List<CommentTagInfo> list3, boolean z2, byte[] bArr, CommentDetail commentDetail);
}
