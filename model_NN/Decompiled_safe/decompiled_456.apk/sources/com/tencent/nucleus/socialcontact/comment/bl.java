package com.tencent.nucleus.socialcontact.comment;

import android.os.Handler;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class bl implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f3118a;

    bl(PopViewDialog popViewDialog) {
        this.f3118a = popViewDialog;
    }

    public void onGlobalLayout() {
        this.f3118a.j.scrollTo(0, 40);
        if (this.f3118a.j.getRootView().getHeight() - this.f3118a.j.getHeight() > 100) {
            new Handler().postDelayed(new bm(this), 300);
        }
    }
}
