package com.tencent.nucleus.socialcontact.tagpage;

import android.text.TextUtils;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.aq;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
public class TPVideoDownInfo {

    /* renamed from: a  reason: collision with root package name */
    public String f3171a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public long d = 0;
    public long e = 0;
    public long f = 0;
    public DownState g = DownState.INIT;
    public int h = 0;
    public String i;
    public volatile int j = 0;
    public j k = new j();
    public StatInfo l = new StatInfo();

    /* compiled from: ProGuard */
    public enum DownState {
        INIT,
        DOWNLOADING,
        QUEUING,
        SUCC,
        FAIL,
        PAUSED,
        DELETE,
        WAITTING_FOR_WIFI
    }

    public int a() {
        int i2;
        if (this.k.f3205a > 0 && (i2 = (int) ((((double) this.k.b) * 100.0d) / ((double) this.k.f3205a))) != 0) {
            return i2;
        }
        return 1;
    }

    public int b() {
        return 9;
    }

    public boolean c() {
        String str = f() + File.separator + this.f3171a;
        if (!FileUtil.isFileExists(str)) {
            return false;
        }
        this.c = str;
        this.f = System.currentTimeMillis();
        this.g = DownState.SUCC;
        this.h = 0;
        return true;
    }

    public void d() {
        if (this.g == DownState.DOWNLOADING || this.g == DownState.QUEUING) {
            this.g = DownState.PAUSED;
        }
    }

    public boolean e() {
        if (this.g == DownState.SUCC) {
            if (new File(this.c).exists()) {
                return true;
            }
            if (this.k != null) {
                this.k.b = 0;
            }
        }
        return false;
    }

    public String f() {
        return FileUtil.getTPVideoDir();
    }

    public String a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f3171a = aq.b(str);
        }
        return this.f3171a;
    }

    public String toString() {
        return "TPVideoDownInfo[videoName = " + this.f3171a + ", videoUrl = " + this.b + ", videoSavePath = " + this.c + ", videoSize = " + this.d + ", createTime = " + this.e + ", finishTime = " + this.f + ", downState = " + this.g + ", errorCode = " + this.h + ", downIndex = " + this.j + "]";
    }
}
