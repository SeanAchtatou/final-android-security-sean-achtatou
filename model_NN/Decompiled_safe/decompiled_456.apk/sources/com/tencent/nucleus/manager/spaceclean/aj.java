package com.tencent.nucleus.manager.spaceclean;

import android.os.RemoteException;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceScanManager f3021a;

    aj(SpaceScanManager spaceScanManager) {
        this.f3021a = spaceScanManager;
    }

    public void run() {
        if (this.f3021a.z != null) {
            try {
                XLog.d("miles", "SpaceScanManager >> cancelScanRubbish called.");
                this.f3021a.z.cancelScanRubbish();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
