package com.tencent.nucleus.manager.spaceclean;

import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f3020a;
    final /* synthetic */ ad b;
    final /* synthetic */ SpaceScanManager c;

    ai(SpaceScanManager spaceScanManager, ArrayList arrayList, ad adVar) {
        this.c = spaceScanManager;
        this.f3020a = arrayList;
        this.b = adVar;
    }

    public void run() {
        Iterator it = this.f3020a.iterator();
        while (it.hasNext()) {
            FileUtil.deleteFileOrDir((String) it.next());
        }
        XLog.d("miles", "SpaceScanManager >> cleanRubbishAsync onCleanFinished");
        this.b.a(true);
    }
}
