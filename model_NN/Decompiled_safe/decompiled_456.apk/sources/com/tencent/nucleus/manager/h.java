package com.tencent.nucleus.manager;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class h extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f2924a;
    final /* synthetic */ MobileManagerInstallActivity b;

    h(MobileManagerInstallActivity mobileManagerInstallActivity, DownloadInfo downloadInfo) {
        this.b = mobileManagerInstallActivity;
        this.f2924a = downloadInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void onLeftBtnClick() {
        DownloadProxy.a().b(this.f2924a.downloadTicket, false);
    }

    public void onRightBtnClick() {
        this.f2924a.response = null;
        a.a().a(this.f2924a);
        Toast.makeText(this.b, this.b.getResources().getString(R.string.down_add_tips), 0).show();
    }

    public void onCancell() {
    }
}
