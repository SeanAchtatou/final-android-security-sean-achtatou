package com.tencent.nucleus.manager.about;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;

/* compiled from: ProGuard */
class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutDeclareActivity f2747a;

    i(AboutDeclareActivity aboutDeclareActivity) {
        this.f2747a = aboutDeclareActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.f2747a.v, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", "http://dl.3g.qq.com/webapp_dloader/xieyi.jsp");
        intent.putExtra("com.tencent.assistant.activity.BROWSER_ACCELERATE", "0");
        this.f2747a.startActivity(intent);
    }
}
