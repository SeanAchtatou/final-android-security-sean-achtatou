package com.tencent.nucleus.manager.backgroundscan;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.SparseArray;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.ab;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.PushMsgCfg;
import com.tencent.assistant.protocol.jce.PushMsgItem;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.assistant.utils.ar;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.q;
import com.tencent.assistant.utils.t;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;
import com.tencent.nucleus.manager.spaceclean.ad;
import com.tencent.open.utils.SystemUtils;
import com.tencent.securemodule.impl.SecureModuleService;
import com.tencent.securemodule.service.CloudScanListener;
import com.tencent.securemodule.service.ProductInfo;
import com.tencent.tmsecurelite.optimize.f;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class BackgroundScanManager {
    private static BackgroundScanManager b = null;

    /* renamed from: a  reason: collision with root package name */
    final CloudScanListener f2819a;
    private PackageManager c;
    private ActivityManager d;
    /* access modifiers changed from: private */
    public Context e;
    /* access modifiers changed from: private */
    public ab f;
    private PushMsgCfg g;
    private SparseArray<PushMsgItem> h;
    /* access modifiers changed from: private */
    public volatile Map<Byte, SStatus> i;
    /* access modifiers changed from: private */
    public b j;
    private f k;
    /* access modifiers changed from: private */
    public ad l;

    /* compiled from: ProGuard */
    public enum SStatus {
        prepare,
        running,
        finish,
        none
    }

    private BackgroundScanManager() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = new SparseArray<>();
        this.i = Collections.synchronizedMap(new HashMap());
        this.j = new j(this);
        this.k = new k(this);
        this.l = new l(this);
        this.f2819a = new n(this);
        this.e = AstApp.i();
        this.c = this.e.getPackageManager();
        this.d = (ActivityManager) this.e.getSystemService("activity");
        this.f = ApkResourceManager.getInstance().getLocalApkLoader();
        b();
    }

    public static synchronized BackgroundScanManager a() {
        BackgroundScanManager backgroundScanManager;
        synchronized (BackgroundScanManager.class) {
            if (b == null) {
                b = new BackgroundScanManager();
            }
            backgroundScanManager = b;
        }
        return backgroundScanManager;
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        XLog.i("BackgroundScan", "<scan> initCfg");
        byte[] J = m.a().J();
        if (J != null) {
            XLog.d("BackgroundScan", "<scan> load background scan settings");
            this.g = (PushMsgCfg) an.b(J, PushMsgCfg.class);
            if (Global.isDev()) {
                XLog.d("BackgroundScan", "<settings> settings: " + this.g.toString());
            }
            if (this.h.size() > 0) {
                this.h.clear();
            }
            if (this.g.b != null) {
                Iterator<PushMsgItem> it = this.g.b.iterator();
                while (it.hasNext()) {
                    PushMsgItem next = it.next();
                    this.h.put(next.f1449a, next);
                    BackgroundScan a2 = com.tencent.assistant.db.table.f.a().a(next.f1449a);
                    if (a2 == null) {
                        com.tencent.assistant.db.table.f.a().a(new BackgroundScan(next.f1449a, a(next.e), next.b, 0, -1));
                    } else if (j() != this.g.c && next.l) {
                        a2.c = next.b;
                        a2.b = a(next.e);
                        com.tencent.assistant.db.table.f.a().b(a2);
                        a(this.g.c);
                    }
                }
            }
        }
    }

    public static long a(double d2) {
        return (long) (d2 * 60.0d * 60.0d * 1000.0d);
    }

    private void a(int i2) {
        m.a().b("background_scan_cfg_version", Integer.valueOf(i2));
    }

    private int j() {
        return m.a().a("background_scan_cfg_version", 0);
    }

    /* access modifiers changed from: package-private */
    public SparseArray<PushMsgItem> c() {
        if (this.h.size() <= 0) {
            b();
        } else if (m.a().I()) {
            b();
            m.a().p(false);
        }
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        if (this.g == null) {
            b();
        }
        if (this.g != null) {
            return this.g.f1448a;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public BackgroundScan a(byte b2) {
        PushMsgItem pushMsgItem = c().get(b2);
        BackgroundScan a2 = com.tencent.assistant.db.table.f.a().a(b2);
        if (a2 != null) {
            return a2;
        }
        BackgroundScan backgroundScan = new BackgroundScan(pushMsgItem.f1449a, a(pushMsgItem.e), pushMsgItem.b, 0, -1);
        com.tencent.assistant.db.table.f.a().a(backgroundScan);
        return backgroundScan;
    }

    /* access modifiers changed from: private */
    public boolean k() {
        boolean z;
        XLog.i("BackgroundScan", "<scan> initScan");
        SparseArray<PushMsgItem> c2 = c();
        boolean z2 = false;
        for (int i2 = 0; i2 < c2.size(); i2++) {
            byte keyAt = (byte) c2.keyAt(i2);
            BackgroundScan a2 = a(keyAt);
            if (Math.abs(System.currentTimeMillis() - a2.d) <= a2.b || a2.c == 0.0d) {
                z = false;
            } else {
                z = true;
            }
            if (z) {
                z2 = true;
            }
            this.i.put(Byte.valueOf(keyAt), z ? SStatus.prepare : SStatus.none);
            a2.e = -1;
            com.tencent.assistant.db.table.f.a().b(a2);
        }
        return z2;
    }

    public synchronized void e() {
        XLog.i("BackgroundScan", "<scan> scan");
        if (f()) {
            XLog.d("BackgroundScan", "<scan> Background scan is running now");
        } else if (d.b().d()) {
            XLog.d("BackgroundScan", "<scan> Push is running now, so will not scan this time !");
        } else if (d() <= 0 || d() <= d.b().e()) {
            XLog.d("BackgroundScan", "<scan> Has reached the day limit, day limit = " + d());
        } else {
            TemporaryThreadManager.get().start(new g(this));
            try {
                Executors.newSingleThreadScheduledExecutor(new q("backgroupscan")).schedule(new h(this), 300000, TimeUnit.MILLISECONDS);
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: private */
    public List<Byte> a(List<BackgroundScan> list) {
        XLog.i("BackgroundScan", "<push> getWeights");
        ArrayList arrayList = new ArrayList();
        if (list.size() > 0 && c().size() > 0) {
            for (BackgroundScan next : list) {
                if (next.e > 0 && Math.abs(System.currentTimeMillis() - next.d) > next.b) {
                    switch (next.f2818a) {
                        case 1:
                            if (next.e > 60) {
                                break;
                            }
                            arrayList.add(Byte.valueOf(next.f2818a));
                            break;
                        case 2:
                            if (next.e == 1) {
                                break;
                            }
                            arrayList.add(Byte.valueOf(next.f2818a));
                            break;
                        case 3:
                            if (next.e < 3) {
                                break;
                            }
                            arrayList.add(Byte.valueOf(next.f2818a));
                            break;
                        case 4:
                            if (next.e < 83886080) {
                                break;
                            }
                            arrayList.add(Byte.valueOf(next.f2818a));
                            break;
                        case 5:
                            if (next.e < 536870912) {
                                break;
                            }
                            arrayList.add(Byte.valueOf(next.f2818a));
                            break;
                        case 6:
                            if (next.e <= 0) {
                                break;
                            }
                            arrayList.add(Byte.valueOf(next.f2818a));
                            break;
                    }
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (this.i.size() > 0) {
            for (Map.Entry next : this.i.entrySet()) {
                if (next.getValue() == SStatus.running) {
                    XLog.d("BackgroundScan", "<scan> current scanning type = " + next.getKey());
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        instance.set(11, 1);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar instance2 = Calendar.getInstance();
        instance2.set(11, 7);
        instance2.set(12, 0);
        instance2.set(13, 0);
        instance2.set(14, 0);
        if (currentTimeMillis < instance.getTimeInMillis() || currentTimeMillis > instance2.getTimeInMillis()) {
            return false;
        }
        return true;
    }

    public void h() {
        XLog.i("BackgroundScan", "<scan> cancelScan");
        if (!f() || d.b().d()) {
            XLog.d("BackgroundScan", "<scan> nothing to cancel");
        } else {
            TemporaryThreadManager.get().start(new i(this));
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        XLog.i("BackgroundScan", "<scan> healthScan");
        this.i.put((byte) 1, SStatus.running);
        int a2 = ar.a();
        BackgroundScan a3 = a((byte) 1);
        a3.e = (long) a2;
        com.tencent.assistant.db.table.f.a().b(a3);
        this.i.put((byte) 1, SStatus.finish);
        XLog.i("BackgroundScan", "<scan> health scan finish , score = " + a2);
    }

    /* access modifiers changed from: private */
    public void m() {
        int i2 = 1;
        XLog.i("BackgroundScan", "<scan> memoryScan");
        this.i.put((byte) 2, SStatus.running);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        this.d.getMemoryInfo(memoryInfo);
        if (Build.VERSION.SDK_INT > 16) {
            try {
                XLog.d("BackgroundScan", "<scan> memoryScan availMem = " + at.a(memoryInfo.availMem) + ", avalil percent = " + (((float) memoryInfo.availMem) / ((float) memoryInfo.totalMem)));
                if (memoryInfo != null && !memoryInfo.lowMemory) {
                    long j2 = (memoryInfo.totalMem * 25) / 100;
                    if (memoryInfo.threshold > j2) {
                        j2 = memoryInfo.threshold;
                    }
                    if (memoryInfo.availMem <= j2) {
                        i2 = 2;
                    }
                }
                i2 = 0;
            } catch (Throwable th) {
                XLog.e("BackgroundScan", "<scan> memoryEnoughScan 1", th);
                i2 = 0;
            }
        } else {
            try {
                long r = t.r();
                long q = t.q();
                XLog.d("BackgroundScan", "<scan> memoryScan availMem = " + at.a(r) + ", avalil percent = " + (((float) r) / ((float) q)));
                long j3 = (q * 25) / 100;
                if (memoryInfo.threshold > j3) {
                    j3 = memoryInfo.threshold;
                }
                if (memoryInfo.availMem <= j3) {
                    i2 = 2;
                }
            } catch (Throwable th2) {
                XLog.e("BackgroundScan", "<scan> memoryEnoughScan 2", th2);
            }
        }
        BackgroundScan a2 = a((byte) 2);
        a2.e = (long) i2;
        com.tencent.assistant.db.table.f.a().b(a2);
        this.i.put((byte) 2, SStatus.finish);
        XLog.i("BackgroundScan", "<scan> mem scan finish !");
    }

    /* access modifiers changed from: private */
    public void n() {
        XLog.i("BackgroundScan", "<scan> bigfileScan");
        if (!SpaceScanManager.a().o()) {
            this.i.put((byte) 5, SStatus.running);
            SpaceScanManager.a().a(this.l);
            SpaceScanManager.a().n();
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        XLog.i("BackgroundScan", "<scan> pkgScan");
        this.i.put((byte) 3, SStatus.running);
        this.f.a(this.j);
        this.f.a();
    }

    /* access modifiers changed from: private */
    public void p() {
        XLog.i("BackgroundScan", "<scan> rubbishScan");
        if (q()) {
            this.i.put((byte) 4, SStatus.running);
            SpaceScanManager.a().a(this.k);
            return;
        }
        this.i.put((byte) 4, SStatus.none);
        XLog.e("BackgroundScan", "<scan> <腾讯手机管家> is not installed, so will not do rubbish scan !");
    }

    private boolean q() {
        return ApkResourceManager.getInstance().getLocalApkInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME) != null;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        int i2;
        XLog.i("BackgroundScan", "<scan> virusScan");
        if (t.b(this.e) != 0) {
            XLog.i("BackgroundScan", "<scan> virusScan stopped, network is not in wifi !");
            this.i.put((byte) 6, SStatus.none);
            return;
        }
        SecureModuleService instance = SecureModuleService.getInstance(this.e);
        try {
            i2 = Integer.parseInt(Global.getBuildNo());
        } catch (Exception e2) {
            XLog.e("BackgroundScan", "<scan> virusScan", e2);
            i2 = 0;
        }
        String str = SystemUtils.QQ_VERSION_NAME_5_0_0;
        try {
            str = this.c.getPackageInfo(this.e.getPackageName(), 0).versionName;
        } catch (Exception e3) {
            XLog.e("BackgroundScan", "<scan> virusScan", e3);
        }
        if (instance.register(new ProductInfo(41, str, i2, 0, Global.getChannelId(), null)) == 0) {
            instance.registerCloudScanListener(this.e, this.f2819a);
            instance.setNotificationUIEnable(false);
            instance.cloudScan();
            this.i.put((byte) 6, SStatus.running);
            Executors.newSingleThreadScheduledExecutor(new q("backgroupscan")).schedule(new m(this, instance), 60, TimeUnit.SECONDS);
            return;
        }
        XLog.e("BackgroundScan", "<scan> virus scan error !!!");
        this.i.put((byte) 6, SStatus.none);
    }
}
