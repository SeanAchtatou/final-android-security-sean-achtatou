package com.tencent.nucleus.manager.usagestats;

import android.os.Build;
import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.net.c;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.protocol.jce.StatOtherAppList;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.b;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;
import com.tencent.beacon.event.a;
import com.tencent.nucleus.manager.root.e;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class UsagestatsSTManager extends BaseSTManagerV2 {
    private static UsagestatsSTManager f = null;

    /* renamed from: a  reason: collision with root package name */
    private Object f3057a = new Object();
    private volatile boolean c = false;
    private Object d = new Object();
    private volatile boolean e = false;

    /* compiled from: ProGuard */
    public enum ReportScene {
        startup,
        timer,
        temproot
    }

    /* compiled from: ProGuard */
    public enum ReportType {
        normal,
        timely
    }

    public static synchronized UsagestatsSTManager a() {
        UsagestatsSTManager usagestatsSTManager;
        synchronized (UsagestatsSTManager.class) {
            if (f == null) {
                f = new UsagestatsSTManager();
            }
            usagestatsSTManager = f;
        }
        return usagestatsSTManager;
    }

    public byte getSTType() {
        return 17;
    }

    public void flush() {
    }

    public void a(ReportScene reportScene) {
        XLog.i("usagestats", "<UsagestatsSTManager> reportAppUsages");
        TemporaryThreadManager.get().start(new q(this, reportScene));
        TemporaryThreadManager.get().start(new r(this, reportScene));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        if (com.tencent.nucleus.manager.usagestats.o.e() != e()) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        com.tencent.assistant.utils.XLog.e("usagestats", "Already report today's normal app usages !");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r0 = com.tencent.nucleus.manager.usagestats.o.a().h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003a, code lost:
        if (r0.b == 0) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
        com.tencent.assistant.utils.XLog.d("usagestats", "<UsagestatsSTManager> reportAppUsageNormalTimely report success");
        com.tencent.assistantv2.st.b.a().b(getSTType(), r0);
        com.tencent.nucleus.manager.usagestats.o.a().g();
        c();
        a("app_usage_n_collect_all", "success", r4, com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportType.b);
        a("app_usage_n_report_all", null, r4, com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportType.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0069, code lost:
        f();
        r1 = r3.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x006e, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r3.e = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0072, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0077, code lost:
        com.tencent.assistant.utils.XLog.d("usagestats", "<UsagestatsSTManager> reportAppUsageRoot report fail !!!");
        a("app_usage_n_collect_all", "fail", r4, com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportType.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportScene r4) {
        /*
            r3 = this;
            java.lang.String r0 = "usagestats"
            java.lang.String r1 = "<UsagestatsSTManager> reportAppUsageRoot"
            com.tencent.assistant.utils.XLog.i(r0, r1)
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            boolean r0 = r3.e     // Catch:{ all -> 0x002d }
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = "usagestats"
            java.lang.String r2 = "Another reportAppUsageNormalTimely is running ..."
            com.tencent.assistant.utils.XLog.e(r0, r2)     // Catch:{ all -> 0x002d }
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
        L_0x0016:
            return
        L_0x0017:
            r0 = 1
            r3.e = r0     // Catch:{ all -> 0x002d }
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            int r0 = com.tencent.nucleus.manager.usagestats.o.e()
            int r1 = e()
            if (r0 != r1) goto L_0x0030
            java.lang.String r0 = "usagestats"
            java.lang.String r1 = "Already report today's normal app usages !"
            com.tencent.assistant.utils.XLog.e(r0, r1)
            goto L_0x0016
        L_0x002d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002d }
            throw r0
        L_0x0030:
            com.tencent.nucleus.manager.usagestats.o r0 = com.tencent.nucleus.manager.usagestats.o.a()
            com.tencent.assistant.protocol.jce.StatOtherAppList r0 = r0.h()
            int r1 = r0.b
            if (r1 == 0) goto L_0x0077
            java.lang.String r1 = "usagestats"
            java.lang.String r2 = "<UsagestatsSTManager> reportAppUsageNormalTimely report success"
            com.tencent.assistant.utils.XLog.d(r1, r2)
            com.tencent.assistantv2.st.b r1 = com.tencent.assistantv2.st.b.a()
            byte r2 = r3.getSTType()
            r1.b(r2, r0)
            com.tencent.nucleus.manager.usagestats.o r0 = com.tencent.nucleus.manager.usagestats.o.a()
            r0.g()
            r3.c()
            java.lang.String r0 = "app_usage_n_collect_all"
            java.lang.String r1 = "success"
            com.tencent.nucleus.manager.usagestats.UsagestatsSTManager$ReportType r2 = com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportType.timely
            r3.a(r0, r1, r4, r2)
            java.lang.String r0 = "app_usage_n_report_all"
            r1 = 0
            com.tencent.nucleus.manager.usagestats.UsagestatsSTManager$ReportType r2 = com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportType.timely
            r3.a(r0, r1, r4, r2)
        L_0x0069:
            r3.f()
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            r0 = 0
            r3.e = r0     // Catch:{ all -> 0x0074 }
            monitor-exit(r1)     // Catch:{ all -> 0x0074 }
            goto L_0x0016
        L_0x0074:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0074 }
            throw r0
        L_0x0077:
            java.lang.String r0 = "usagestats"
            java.lang.String r1 = "<UsagestatsSTManager> reportAppUsageRoot report fail !!!"
            com.tencent.assistant.utils.XLog.d(r0, r1)
            java.lang.String r0 = "app_usage_n_collect_all"
            java.lang.String r1 = "fail"
            com.tencent.nucleus.manager.usagestats.UsagestatsSTManager$ReportType r2 = com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportType.timely
            r3.a(r0, r1, r4, r2)
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.b(com.tencent.nucleus.manager.usagestats.UsagestatsSTManager$ReportScene):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        if (r0 == 0) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (com.tencent.nucleus.manager.usagestats.o.e() != r0) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        com.tencent.assistant.utils.XLog.e("usagestats", "Already report today's root app usages !");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        if (android.os.Build.VERSION.SDK_INT <= 20) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0038, code lost:
        g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003b, code lost:
        r1 = r3.f3057a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003d, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r3.c = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0041, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0046, code lost:
        d(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        r0 = com.tencent.nucleus.manager.usagestats.o.d();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.ReportScene r4) {
        /*
            r3 = this;
            java.lang.String r0 = "usagestats"
            java.lang.String r1 = "<UsagestatsSTManager> reportAppUsageRoot"
            com.tencent.assistant.utils.XLog.i(r0, r1)
            java.lang.Object r1 = r3.f3057a
            monitor-enter(r1)
            boolean r0 = r3.c     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = "usagestats"
            java.lang.String r2 = "Another reportAppUsageRoot is running ..."
            com.tencent.assistant.utils.XLog.e(r0, r2)     // Catch:{ all -> 0x002f }
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
        L_0x0016:
            return
        L_0x0017:
            r0 = 1
            r3.c = r0     // Catch:{ all -> 0x002f }
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            int r0 = com.tencent.nucleus.manager.usagestats.o.d()
            if (r0 == 0) goto L_0x0032
            int r1 = com.tencent.nucleus.manager.usagestats.o.e()
            if (r1 != r0) goto L_0x0032
            java.lang.String r0 = "usagestats"
            java.lang.String r1 = "Already report today's root app usages !"
            com.tencent.assistant.utils.XLog.e(r0, r1)
            goto L_0x0016
        L_0x002f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            throw r0
        L_0x0032:
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 20
            if (r0 <= r1) goto L_0x0046
            r3.g()
        L_0x003b:
            java.lang.Object r1 = r3.f3057a
            monitor-enter(r1)
            r0 = 0
            r3.c = r0     // Catch:{ all -> 0x0043 }
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            goto L_0x0016
        L_0x0043:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            throw r0
        L_0x0046:
            r3.d(r4)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.usagestats.UsagestatsSTManager.c(com.tencent.nucleus.manager.usagestats.UsagestatsSTManager$ReportScene):void");
    }

    private void g() {
        XLog.i("usagestats", "<UsagestatsSTManager> doAboveLollipopReport");
    }

    private void d(ReportScene reportScene) {
        XLog.i("usagestats", "<UsagestatsSTManager> doBelowLollipopReport");
        if (e.a().c()) {
            StatOtherAppList a2 = o.a().a(false);
            if (a2.b > 0) {
                XLog.d("usagestats", "<UsagestatsSTManager> doBelowLollipopReport report success");
                b.a().b(getSTType(), a2);
                b();
                a("app_usage_r_report_timely", "success", reportScene, ReportType.timely);
                return;
            }
            XLog.e("usagestats", "<UsagestatsSTManager> doBelowLollipopReport no data to report !!!");
            a("app_usage_r_report_timely", "empty", reportScene, ReportType.timely);
        } else if (reportScene == ReportScene.startup) {
            XLog.d("usagestats", "<UsagestatsSTManager> doBelowLollipopReport has not get temproot yet !");
        } else {
            XLog.d("usagestats", "<UsagestatsSTManager> doBelowLollipopReport no temp root at all !!!");
            a("app_usage_r_report_timely", "noroot", reportScene, ReportType.timely);
        }
    }

    public void b() {
        o.a(System.currentTimeMillis());
    }

    public void c() {
        m.a().b("usage_stats_last_normal_report_date", Long.valueOf(System.currentTimeMillis()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public static int e() {
        long a2 = m.a().a("usage_stats_last_normal_report_date", 0L);
        if (a2 != 0) {
            try {
                return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date(a2)));
            } catch (NumberFormatException e2) {
            }
        }
        return 0;
    }

    public final void a(String str, String str2, ReportScene reportScene, ReportType reportType) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        if (!TextUtils.isEmpty(str2)) {
            hashMap.put("B4", str2);
        }
        if (reportScene != null) {
            hashMap.put("B5", reportScene.name());
        }
        hashMap.put("B6", reportType.name());
        hashMap.put("B7", t.v());
        hashMap.put("B8", Build.VERSION.RELEASE);
        boolean z = false;
        if (!TextUtils.isEmpty(str2) && str2.equals("success")) {
            hashMap.put("B9", c.a() ? "1" : "0");
            z = true;
        }
        a.a(str, z, -1, -1, hashMap, true);
    }

    public final void f() {
        List<PluginInfo> a2 = i.b().a(-1);
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        for (PluginInfo next : a2) {
            hashMap.put("B4", String.valueOf(next.getVersion()));
            hashMap.put("B5", String.valueOf(next.getPkgid()));
            hashMap.put("B6", String.valueOf(next.getPackageName()));
            a.a("PluginStatus", true, -1, -1, hashMap, true);
        }
    }
}
