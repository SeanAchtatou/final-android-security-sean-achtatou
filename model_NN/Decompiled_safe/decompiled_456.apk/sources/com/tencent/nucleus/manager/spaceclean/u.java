package com.tencent.nucleus.manager.spaceclean;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class u implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RubbishItemView f3044a;

    u(RubbishItemView rubbishItemView) {
        this.f3044a = rubbishItemView;
    }

    public void onClick(View view) {
        if (this.f3044a.n.g) {
            this.f3044a.n.g = false;
            this.f3044a.j.setVisibility(8);
            this.f3044a.i.setImageResource(R.drawable.icon_open);
            return;
        }
        this.f3044a.n.g = true;
        this.f3044a.j.setVisibility(0);
        if (this.f3044a.p) {
            this.f3044a.j.findViewById(R.id.rubbish_from).setVisibility(0);
        }
        this.f3044a.i.setImageResource(R.drawable.icon_close);
    }
}
