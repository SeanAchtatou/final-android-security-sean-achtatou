package com.tencent.nucleus.manager.uninstallwatch;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.s;
import com.tencent.assistant.utils.t;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
class c {
    c() {
    }

    private static String b() {
        StringBuilder sb = new StringBuilder();
        try {
            if (!TextUtils.isEmpty(t.g())) {
                sb.append("imei=");
                sb.append(t.g());
            }
        } catch (Exception e) {
            XLog.e("uninstall", "<java> imei", e);
        }
        try {
            long p = j.a().p();
            if (p != 0) {
                sb.append("|uin=");
                sb.append(p);
            } else {
                XLog.e("uninstall", "<java> uin is null");
            }
        } catch (Exception e2) {
            XLog.e("uninstall", "<java> uin", e2);
        }
        try {
            String s = j.a().s();
            if (!TextUtils.isEmpty(s)) {
                sb.append("|openid=");
                sb.append(s);
            } else {
                XLog.e("uninstall", "<java> openid is null");
            }
        } catch (Exception e3) {
            XLog.e("uninstall", "<java> openid", e3);
        }
        try {
            String phoneGuid = Global.getPhoneGuid();
            if (!TextUtils.isEmpty(phoneGuid)) {
                sb.append("|guid=");
                sb.append(phoneGuid);
            }
        } catch (Exception e4) {
            XLog.e("uninstall", "<java> guid", e4);
        }
        try {
            String k = t.k();
            if (!TextUtils.isEmpty(k)) {
                sb.append("|machine_id=");
                sb.append(k);
            }
        } catch (Exception e5) {
            XLog.e("uninstall", "<java> machine_id", e5);
        }
        try {
            String qua = Global.getQUA();
            if (!TextUtils.isEmpty(qua)) {
                sb.append("|qua=");
                sb.append(qua);
            }
        } catch (Exception e6) {
            XLog.e("uninstall", "<java> qua", e6);
        }
        try {
            LocalApkInfo b = e.b(AstApp.i().getPackageName());
            if (b != null) {
                sb.append("|install_time=");
                sb.append(b.mInstallDate);
            }
        } catch (Exception e7) {
            XLog.e("uninstall", "<java> apkInfo", e7);
        }
        try {
            int appVersionCode = Global.getAppVersionCode();
            if (appVersionCode != 0) {
                sb.append("|versioncode=");
                sb.append(appVersionCode);
            }
        } catch (Exception e8) {
            XLog.e("uninstall", "<java> versioncode", e8);
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public static String c() {
        return new s().a(b().getBytes(), "&-*)Wb5_U,[^!8'@".getBytes());
    }
}
