package com.tencent.nucleus.manager.apkMgr;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class m extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResultListView f2775a;

    m(ApkResultListView apkResultListView) {
        this.f2775a = apkResultListView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f2775a.f == null || this.f2775a.e == null) {
            RelativeLayout unused = this.f2775a.f = (RelativeLayout) this.f2775a.d.findViewById(R.id.pop_bar);
            this.f2775a.f.setOnClickListener(new n(this));
            return;
        }
        if (i == 0) {
            this.f2775a.f.setVisibility(8);
        }
        int pointToPosition = this.f2775a.e.pointToPosition(0, this.f2775a.e() - 10);
        int pointToPosition2 = this.f2775a.e.pointToPosition(0, 0);
        long j = 0;
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.f2775a.e.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.f2775a.e.getExpandChildAt(pointToPosition2 - this.f2775a.e.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    int unused2 = this.f2775a.k = 100;
                } else {
                    int unused3 = this.f2775a.k = expandChildAt.getHeight();
                }
            }
            if (this.f2775a.k != 0) {
                if (this.f2775a.i > 0 && packedPositionGroup != -1) {
                    int unused4 = this.f2775a.j = packedPositionGroup;
                    String str = (String) this.f2775a.f2763a.getGroup(packedPositionGroup);
                    if (!TextUtils.isEmpty(str)) {
                        this.f2775a.g.setText(str);
                        this.f2775a.f.setVisibility(0);
                    }
                    this.f2775a.h.setSelected(this.f2775a.f2763a.a(packedPositionGroup));
                    this.f2775a.h.setOnClickListener(new o(this, packedPositionGroup));
                }
                if (this.f2775a.i == 0) {
                    this.f2775a.f.setVisibility(8);
                }
                j = expandableListPosition;
            } else {
                return;
            }
        }
        if (this.f2775a.j == -1) {
            return;
        }
        if (j == 0 && (pointToPosition == 0 || pointToPosition == -1)) {
            this.f2775a.f.setVisibility(8);
            return;
        }
        int d = this.f2775a.e();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f2775a.f.getLayoutParams();
        marginLayoutParams.topMargin = -(this.f2775a.k - d);
        this.f2775a.f.setLayoutParams(marginLayoutParams);
    }
}
