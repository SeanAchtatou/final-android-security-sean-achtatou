package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.AlphaAnimation;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2901a;

    e(c cVar) {
        this.f2901a = cVar;
    }

    public void run() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(400);
        alphaAnimation.setFillAfter(true);
        this.f2901a.f2899a.r.startAnimation(alphaAnimation);
        this.f2901a.f2899a.d();
    }
}
