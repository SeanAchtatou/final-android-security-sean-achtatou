package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
class b implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatWindowBigView f2898a;

    b(FloatWindowBigView floatWindowBigView) {
        this.f2898a = floatWindowBigView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        try {
            this.f2898a.v.setImageResource(R.drawable.admin_launch_rocket_fire);
        } catch (Throwable th) {
            t.a().b();
        }
    }
}
