package com.tencent.nucleus.manager.appbackup;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.manager.a;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class AppBackupAppItemInfo extends LinearLayout implements b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public SimpleAppModel f2795a;

    public AppBackupAppItemInfo(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void a(SimpleAppModel simpleAppModel) {
        this.f2795a = simpleAppModel;
        a(this.f2795a.u());
        a.a().a(this.f2795a.q(), this);
    }

    /* access modifiers changed from: private */
    public void a(AppConst.AppState appState) {
        switch (b.f2801a[appState.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                setVisibility(8);
                return;
            default:
                setVisibility(0);
                return;
        }
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        if (str != null && this.f2795a != null && str.equals(this.f2795a.q())) {
            ah.a().post(new a(this, str, appState));
        }
    }
}
