package com.tencent.nucleus.manager.floatingwindow;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class RocketLauncher extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public static int f2888a;
    public static int b;
    public static int c;
    /* access modifiers changed from: private */
    public Animation.AnimationListener A = new aa(this);
    private int d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public View g;
    /* access modifiers changed from: private */
    public ImageView h;
    /* access modifiers changed from: private */
    public ImageView i;
    /* access modifiers changed from: private */
    public ImageView j;
    /* access modifiers changed from: private */
    public ImageView k;
    private ImageView l;
    /* access modifiers changed from: private */
    public ImageView m;
    /* access modifiers changed from: private */
    public AnimationDrawable n;
    private int o = -90;
    private int p = 90;
    private float q = 1.5f;
    private int r;
    private Vibrator s;
    private boolean t;
    private int u;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public boolean w = false;
    /* access modifiers changed from: private */
    public Animation.AnimationListener x;
    /* access modifiers changed from: private */
    public int y;
    private boolean z = true;

    public RocketLauncher(Context context) {
        super(context);
        try {
            LayoutInflater.from(context).inflate((int) R.layout.float_window_rocket_launcher, this);
            this.f = findViewById(R.id.rl_launcher_bg);
            this.g = findViewById(R.id.iv_launcher_mask);
            this.h = (ImageView) findViewById(R.id.iv_launcher_logonotch);
            this.i = (ImageView) findViewById(R.id.iv_launcher_logonotch_fake);
            this.j = (ImageView) findViewById(R.id.iv_launcher_bg);
            this.k = (ImageView) findViewById(R.id.iv_launcher_light);
            this.l = (ImageView) findViewById(R.id.iv_launcher_highlight);
            this.m = (ImageView) findViewById(R.id.iv_launcher_rocket_nofire);
            f2888a = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_laucher_cloud_width);
            this.u = (int) context.getResources().getDimension(R.dimen.floating_window_logo_width);
            this.r = (int) context.getResources().getDimension(R.dimen.floating_window_launcher_width);
            b = (int) context.getResources().getDimension(R.dimen.floating_window_launcher_height);
            c = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_laucher_layout_height);
            this.d = (int) context.getResources().getDimension(R.dimen.floating_window_launcher_background_width);
            this.e = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_height);
            this.y = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_fly_up_offset);
            this.s = (Vibrator) context.getSystemService("vibrator");
            this.h.setVisibility(4);
            this.j.setVisibility(4);
            this.t = false;
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(400);
            this.g.startAnimation(alphaAnimation);
        } catch (Throwable th) {
            t.a().b();
            this.z = false;
        }
    }

    private void a(View view, float f2, float f3, float f4, float f5, boolean z2, long j2) {
        AnimationSet animationSet = new AnimationSet(true);
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(f2, f3, ((float) this.r) / 2.0f, ((float) this.r) / 2.0f, 0.0f, z2);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f4, f5, f4, f5, 1, 0.5f, 1, 0.5f);
        animationSet.addAnimation(rotate3dAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.setFillAfter(true);
        animationSet.setDuration(0);
        view.startAnimation(animationSet);
    }

    /* access modifiers changed from: private */
    public void a(View view, float f2, float f3, long j2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void a(View view, float f2, float f3, float f4, float f5, long j2, long j3, boolean z2, Animation.AnimationListener animationListener) {
        TranslateAnimation translateAnimation = new TranslateAnimation(f2, f3, f4, f5);
        translateAnimation.setFillAfter(z2);
        translateAnimation.setStartOffset(j2);
        translateAnimation.setDuration(j3);
        translateAnimation.setFillAfter(true);
        translateAnimation.setAnimationListener(animationListener);
        view.startAnimation(translateAnimation);
    }

    /* access modifiers changed from: private */
    public Animation b() {
        AnimationSet animationSet = new AnimationSet(true);
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(0.0f, 60.0f, ((float) this.r) / 2.0f, ((float) this.r) / 2.0f, 0.0f, false);
        rotate3dAnimation.setDuration(500);
        rotate3dAnimation.setFillAfter(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setStartOffset(400);
        scaleAnimation.setDuration(200);
        ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 1.0f, 1.2f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation2.setFillAfter(true);
        scaleAnimation2.setStartOffset(900);
        scaleAnimation2.setDuration(200);
        animationSet.addAnimation(rotate3dAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(scaleAnimation2);
        animationSet.setFillAfter(true);
        return animationSet;
    }

    /* access modifiers changed from: private */
    public Animation c() {
        AnimationSet animationSet = new AnimationSet(true);
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(0.0f, 60.0f, ((float) this.r) / 2.0f, ((float) this.r) / 2.0f, 100.0f, false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f, 1, 0.5f, 1, 0.5f);
        animationSet.addAnimation(rotate3dAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.setDuration(500);
        animationSet.setFillAfter(true);
        return animationSet;
    }

    private Animation b(Animation.AnimationListener animationListener) {
        AnimationSet animationSet = new AnimationSet(true);
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(0.0f, 60.0f, ((float) this.r) / 2.0f, ((float) this.r) / 2.0f, 0.0f, false);
        rotate3dAnimation.setDuration(0);
        rotate3dAnimation.setFillAfter(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.1f, 2.5f, 0.1f, 2.5f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(600);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setDuration(200);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation2.setFillAfter(true);
        alphaAnimation2.setStartOffset(400);
        alphaAnimation2.setDuration(200);
        animationSet.addAnimation(rotate3dAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(alphaAnimation2);
        animationSet.setFillAfter(true);
        animationSet.setAnimationListener(animationListener);
        return animationSet;
    }

    private Animation c(Animation.AnimationListener animationListener) {
        AnimationSet animationSet = new AnimationSet(false);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-this.y), 0.0f);
        translateAnimation.setDuration(300);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new LinearInterpolator());
        translateAnimation.setAnimationListener(new y(this));
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) ((-by.c()) - this.e));
        translateAnimation2.setStartOffset(300);
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setDuration(800);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.5f, 1.0f, 2.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setStartOffset(300);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(300);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(translateAnimation2);
        animationSet.addAnimation(scaleAnimation);
        animationSet.setFillAfter(true);
        animationSet.setAnimationListener(animationListener);
        return animationSet;
    }

    public void a(int i2) {
        int i3;
        if (!this.z) {
            XLog.e("floatingwindow", "RocketLauncher >> startFlipAnimation isViewCreated = false. return");
            return;
        }
        FloatWindowSmallView i4 = n.a().i();
        if (i4 != null) {
            i3 = i4.b();
        } else {
            i3 = i2;
        }
        if (i2 > i3) {
            i2 = i3;
        }
        int i5 = ((i2 * 90) / i3) - 90;
        a(this.h, (float) this.o, (float) i5, 1.0f, 1.0f, true, 0);
        this.o = i5;
        int i6 = 90 - ((i2 * 90) / i3);
        float f2 = 1.5f - ((0.5f * ((float) i2)) / ((float) i3));
        a(this.j, (float) this.p, (float) i6, this.q, f2, false, 0);
        this.p = i6;
        this.q = f2;
    }

    public void a(boolean z2, boolean z3) {
        if (!this.z) {
            XLog.e("floatingwindow", "RocketLauncher >> updateLaucherStatus isViewCreated = false. return");
        } else if (z3) {
            if (!this.t) {
                this.s.vibrate(new long[]{100, 200, 100, 200}, -1);
                this.t = true;
            }
            try {
                this.h.setImageResource(R.drawable.admin_launch_logonotch_white);
            } catch (Throwable th) {
                XLog.e("floatingwindow", "RocketLauncher >> <updateLaucherStatus> throws exception.");
            }
        } else {
            this.t = false;
            this.s.cancel();
            try {
                this.h.setImageResource(R.drawable.admin_launch_logonotch);
            } catch (Throwable th2) {
                XLog.e("floatingwindow", "RocketLauncher >> <updateLaucherStatus> throws exception.");
            }
            this.m.clearAnimation();
            if (z2) {
                this.j.clearAnimation();
                this.m.setVisibility(4);
            }
            this.m.setVisibility(4);
        }
    }

    public void a(int i2, int i3) {
        if (!this.z) {
            XLog.e("floatingwindow", "RocketLauncher >> startLauchPrepareAnim isViewCreated = false. return");
            return;
        }
        this.v = true;
        int b2 = (by.b() / 2) - (this.u / 2);
        int top = (this.f.getTop() + (this.d / 2)) - (this.u / 2);
        this.k.clearAnimation();
        this.i.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation((float) (i2 - b2), 0.0f, (float) (i3 - top), 0.0f);
        translateAnimation.setDuration(200);
        translateAnimation.setFillAfter(true);
        translateAnimation.setAnimationListener(new z(this));
        this.h.startAnimation(translateAnimation);
    }

    public void a() {
        if (!this.z) {
            XLog.e("floatingwindow", "RocketLauncher >> clearLauchPrepareAnim isViewCreated = false. return");
            return;
        }
        this.h.clearAnimation();
        this.j.clearAnimation();
        this.m.clearAnimation();
        this.k.clearAnimation();
        this.m.setVisibility(4);
        this.k.setVisibility(4);
    }

    public void a(Animation.AnimationListener animationListener) {
        if (!this.z) {
            XLog.e("floatingwindow", "RocketLauncher >> launchRocket isViewCreated = false. return");
        } else if (this.v) {
            this.w = true;
            this.x = animationListener;
        } else {
            this.l.setVisibility(4);
            this.l.startAnimation(b((Animation.AnimationListener) null));
            try {
                this.m.setImageResource(R.drawable.admin_launch_rocket);
            } catch (Throwable th) {
                XLog.e("floatingwindow", "RocketLauncher >> <launchRocket> throws exception");
            }
            this.m.startAnimation(c(animationListener));
        }
    }
}
