package com.tencent.nucleus.manager.component;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class ab implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ al f2848a;
    final /* synthetic */ TxManagerCommContainView b;

    ab(TxManagerCommContainView txManagerCommContainView, al alVar) {
        this.b = txManagerCommContainView;
        this.f2848a = alVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.component.TxManagerCommContainView.a(com.tencent.nucleus.manager.component.TxManagerCommContainView, boolean):boolean
     arg types: [com.tencent.nucleus.manager.component.TxManagerCommContainView, int]
     candidates:
      com.tencent.nucleus.manager.component.TxManagerCommContainView.a(java.lang.String, int):android.text.SpannableString
      com.tencent.nucleus.manager.component.TxManagerCommContainView.a(com.tencent.nucleus.manager.component.TxManagerCommContainView, float):void
      com.tencent.nucleus.manager.component.TxManagerCommContainView.a(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.component.TxManagerCommContainView.a(android.app.Activity, com.tencent.assistant.plugin.PluginStartEntry):void
      com.tencent.nucleus.manager.component.TxManagerCommContainView.a(android.view.View, android.widget.RelativeLayout$LayoutParams):void
      com.tencent.nucleus.manager.component.TxManagerCommContainView.a(java.lang.String, android.text.SpannableString):void
      com.tencent.nucleus.manager.component.TxManagerCommContainView.a(com.tencent.nucleus.manager.component.TxManagerCommContainView, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        boolean unused = this.b.z = true;
        if (this.b.z) {
            this.b.g();
        }
        if (this.f2848a != null) {
            this.f2848a.b();
        }
    }
}
