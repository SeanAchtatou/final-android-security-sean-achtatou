package com.tencent.nucleus.manager.component;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.assistant.plugin.PluginProxyUtils;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/* compiled from: ProGuard */
public class RollTextView extends TextView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public double f2841a;
    /* access modifiers changed from: private */
    public double b;
    /* access modifiers changed from: private */
    public double c;
    private DecimalFormat d = new DecimalFormat("0.0");
    private DecimalFormat e = new DecimalFormat("0");
    private TextView f = null;
    private Context g;
    private DecimalFormat h = null;
    private float i = 1.0f;
    private Typeface j;
    private boolean k = true;
    private int l = 1;
    /* access modifiers changed from: private */
    public Handler m = new o(this);

    static /* synthetic */ double c(RollTextView rollTextView, double d2) {
        double d3 = rollTextView.f2841a + d2;
        rollTextView.f2841a = d3;
        return d3;
    }

    public void a(int i2) {
        this.l = i2;
        if (this.l == 1) {
            setTextColor(Color.argb(255, 0, 163, 224));
        } else {
            setTextColor(Color.argb(255, 52, 52, 52));
        }
    }

    public void a(boolean z) {
        this.k = z;
    }

    public RollTextView(Context context) {
        super(context);
        this.g = context;
        this.f2841a = 0.0d;
        this.j = PluginProxyUtils.getTypeFace();
        setTypeface(this.j);
        setTextSize(1, 32.0f);
    }

    public RollTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = context;
        this.f2841a = 0.0d;
        this.j = PluginProxyUtils.getTypeFace();
        setTextSize(32.0f);
        setTypeface(this.j);
    }

    /* access modifiers changed from: private */
    public String c(double d2) {
        String.valueOf(d2);
        if (this.h != null) {
            return this.h.format(d2);
        }
        if (d2 < 100.0d) {
            return this.d.format(d2);
        }
        return this.e.format(d2);
    }

    public void a(DecimalFormat decimalFormat) {
        this.h = decimalFormat;
    }

    public void a(double d2) {
        this.m.removeMessages(100000);
        if (d2 == 0.0d || !this.d.format(d2).equals(this.d.format(this.f2841a))) {
            this.b = d2;
            this.c = (this.b - this.f2841a) / 20.0d;
            this.c = new BigDecimal(this.c).setScale(2, 4).doubleValue();
            this.m.sendEmptyMessage(100000);
        }
    }

    public void b(double d2) {
        this.m.removeMessages(100000);
        if (d2 < 0.0d) {
            d2 = 0.0d;
        }
        this.b = d2;
        this.f2841a = d2;
        this.m.sendEmptyMessage(100000);
    }

    public void a(TextView textView) {
        this.f = textView;
        this.f.setTypeface(this.j);
        this.f.setTextSize(20.0f);
    }

    /* access modifiers changed from: private */
    public double d(double d2) {
        if (d2 < 0.0d) {
            d2 = 0.0d;
        }
        while (d2 >= 1000.0d) {
            d2 /= 1024.0d;
        }
        return d2;
    }

    /* access modifiers changed from: private */
    public void e(double d2) {
        String str;
        if (this.f != null) {
            if (d2 < 1000.0d) {
                str = "KB";
            } else {
                double d3 = d2 / 1024.0d;
                if (d3 < 1000.0d) {
                    str = "MB";
                } else {
                    double d4 = d3 / 1024.0d;
                    str = "GB";
                }
            }
            this.f.setText(str);
            if (!this.k) {
                return;
            }
            if (this.l == 1) {
                this.f.setTextColor(Color.argb(255, 0, 163, 224));
            } else {
                this.f.setTextColor(Color.argb(255, 52, 52, 52));
            }
        }
    }
}
