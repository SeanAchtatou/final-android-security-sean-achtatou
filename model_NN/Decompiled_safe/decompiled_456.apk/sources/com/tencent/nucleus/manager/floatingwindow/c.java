package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatWindowBigView f2899a;

    c(FloatWindowBigView floatWindowBigView) {
        this.f2899a = floatWindowBigView;
    }

    public void run() {
        if (!n.a().a(this.f2899a.A)) {
            AnimationSet animationSet = new AnimationSet(true);
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.2f, 1.0f, 1.2f, 1.0f, 1, 0.5f, 1, 0.5f);
            scaleAnimation.setFillAfter(true);
            scaleAnimation.setDuration(300);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(300);
            animationSet.addAnimation(scaleAnimation);
            animationSet.addAnimation(alphaAnimation);
            animationSet.setFillAfter(true);
            animationSet.setAnimationListener(new d(this));
            this.f2899a.q.setVisibility(8);
            this.f2899a.s.setVisibility(8);
            this.f2899a.r.setVisibility(0);
            this.f2899a.t.setTextSize(2, 22.0f);
            this.f2899a.u.setTextSize(2, 11.0f);
            this.f2899a.t.b((((double) this.f2899a.A) * 1.0d) / 1024.0d);
            this.f2899a.r.startAnimation(animationSet);
            this.f2899a.postDelayed(new e(this), 1500);
            return;
        }
        this.f2899a.B.clearAnimation();
        try {
            this.f2899a.B.setBackgroundResource(R.drawable.admin_launch_circle);
        } catch (Throwable th) {
            t.a().b();
        }
        this.f2899a.d();
    }
}
