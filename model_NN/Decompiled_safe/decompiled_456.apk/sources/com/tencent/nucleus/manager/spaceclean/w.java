package com.tencent.nucleus.manager.spaceclean;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class w extends BaseExpandableListAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LinkedHashMap<Integer, ArrayList<n>> f3046a = new LinkedHashMap<>();
    private ArrayList<Integer> b = new ArrayList<>();
    private Context c;
    private LayoutInflater d;
    private boolean e = true;
    private Handler f = null;
    private b g = null;
    private Comparator<n> h = new x(this);

    public w(Context context) {
        this.c = context;
        this.d = LayoutInflater.from(this.c);
        RubbishItemView.f3007a = true;
    }

    public void a(Map<Integer, ArrayList<n>> map) {
        if (map != null && !map.isEmpty()) {
            this.f3046a.clear();
            this.f3046a.putAll(map);
            List list = this.f3046a.get(1);
            if (list != null && list.size() > 0) {
                Collections.sort(list, this.h);
            }
            this.b.clear();
            for (Map.Entry next : this.f3046a.entrySet()) {
                this.b.add((Integer) next.getKey());
                Iterator it = ((ArrayList) next.getValue()).iterator();
                while (it.hasNext()) {
                    ((n) it.next()).a();
                }
            }
            Collections.sort(this.b);
            notifyDataSetChanged();
        }
    }

    public Object getChild(int i, int i2) {
        ArrayList arrayList;
        if (this.f3046a == null || this.b == null || i < 0 || this.b.size() <= i || (arrayList = this.f3046a.get(this.b.get(i))) == null || i2 < 0 || arrayList.size() <= i2) {
            return null;
        }
        return arrayList.get(i2);
    }

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        n nVar;
        RubbishItemView rubbishItemView;
        ArrayList arrayList = this.f3046a.get(this.b.get(i));
        if (arrayList == null || i2 > arrayList.size() - 1) {
            nVar = null;
        } else {
            nVar = (n) arrayList.get(i2);
        }
        if (nVar == null) {
            XLog.d("miles", "RubbishResultAdapter >> 垃圾清理获取的rubbishInfo == null");
            if (view == null) {
                rubbishItemView = new RubbishItemView(this.c);
                rubbishItemView.a(this.f);
                XLog.d("miles", "RubbishResultAdapter >> 创建一个新的RubbishItemView");
            } else {
                rubbishItemView = view;
            }
            XLog.d("miles", "RubbishResultAdapter >> 直接返回convertView");
            return rubbishItemView;
        }
        if (this.e && i2 == 0 && this.b.get(i).intValue() == 1) {
            nVar.g = true;
            this.e = false;
        }
        STInfoV2 a2 = a(i, i2);
        if (view == null) {
            RubbishItemView rubbishItemView2 = new RubbishItemView(this.c, nVar, a2);
            rubbishItemView2.a(this.f);
            return rubbishItemView2;
        }
        ((RubbishItemView) view).a(nVar, a2, this.c.getResources().getDimension(R.dimen.common_rubbish_detail_item_height));
        return view;
    }

    public void a(Handler handler) {
        this.f = handler;
    }

    private STInfoV2 a(int i, int i2) {
        if (this.g == null) {
            this.g = new b();
        }
        String b2 = b(i, i2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, 100);
        buildSTInfo.slotId = b2;
        this.g.exposure(buildSTInfo);
        return buildSTInfo;
    }

    private String b(int i, int i2) {
        if (i == 0) {
            return "00_" + bm.a(i2 + 1);
        }
        if (i == 1) {
            return "01_" + bm.a(i2 + 1);
        }
        return null;
    }

    public int getChildrenCount(int i) {
        ArrayList arrayList;
        if (this.f3046a == null || i >= this.b.size() || (arrayList = this.f3046a.get(this.b.get(i))) == null) {
            return 0;
        }
        return arrayList.size();
    }

    public Object getGroup(int i) {
        if (this.b == null || this.b.size() <= 0 || i >= this.b.size() || i < 0) {
            return null;
        }
        return a(i);
    }

    public int getGroupCount() {
        if (this.f3046a != null) {
            return this.f3046a.size();
        }
        return 0;
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        y yVar;
        if (view == null) {
            view = this.d.inflate((int) R.layout.group_item, (ViewGroup) null);
            y yVar2 = new y(this);
            yVar2.f3048a = (TextView) view.findViewById(R.id.group_title);
            yVar2.b = (TextView) view.findViewById(R.id.select_all);
            yVar2.b.setVisibility(8);
            view.setTag(yVar2);
            yVar = yVar2;
        } else {
            yVar = (y) view.getTag();
        }
        yVar.f3048a.setText(a(i));
        return view;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    private String a(int i) {
        List list;
        if (this.b == null || i >= this.b.size()) {
            return Constants.STR_EMPTY;
        }
        if (this.b.get(i).intValue() == 1) {
            List list2 = this.f3046a.get(1);
            return list2 != null ? String.format(this.c.getString(R.string.rubbish_clear_soft_cache_title), at.c(a(list2))) : Constants.STR_EMPTY;
        } else if (this.b.get(i).intValue() != 0 || (list = this.f3046a.get(0)) == null) {
            return Constants.STR_EMPTY;
        } else {
            return String.format(this.c.getString(R.string.rubbish_clear_retail_rubbish_title), at.c(a(list)));
        }
    }

    private long a(List<n> list) {
        long j = 0;
        Iterator<n> it = list.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            n next = it.next();
            if (next != null) {
                j = j2 + next.d;
            } else {
                j = j2;
            }
        }
    }
}
