package com.tencent.nucleus.manager.securescan;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class NoRiskFoundPage extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    ImageView f2977a;
    ImageView b;
    private Context c;
    private LayoutInflater d;

    public NoRiskFoundPage(Context context) {
        super(context);
        this.c = context;
        a(context);
    }

    public NoRiskFoundPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
        a(context);
    }

    private void a(Context context) {
        this.d = LayoutInflater.from(context);
        this.d.inflate((int) R.layout.activity_noriskfound, this);
        this.f2977a = (ImageView) findViewById(R.id.resultIcon);
        this.b = (ImageView) findViewById(R.id.success_duigou);
    }

    public void setVisibility(int i) {
        if (i == 0) {
            ScaleAnimation scaleAnimation = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, 1, 0.5f, 1, 0.5f);
            scaleAnimation.setDuration(400);
            scaleAnimation.setInterpolator(new OvershootInterpolator(1.2f));
            ScaleAnimation scaleAnimation2 = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, 1, 0.5f, 1, 0.5f);
            scaleAnimation2.setDuration(400);
            scaleAnimation2.setInterpolator(new OvershootInterpolator(1.2f));
            scaleAnimation.setAnimationListener(new b(this));
            scaleAnimation2.setAnimationListener(new c(this));
            this.f2977a.startAnimation(scaleAnimation);
            ah.a().postDelayed(new d(this, scaleAnimation2), 200);
        }
        super.setVisibility(i);
    }
}
