package com.tencent.nucleus.manager.spaceclean;

import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.optimize.f;

/* compiled from: ProGuard */
class ah implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f3019a;
    final /* synthetic */ SpaceScanManager b;

    ah(SpaceScanManager spaceScanManager, f fVar) {
        this.b = spaceScanManager;
        this.f3019a = fVar;
    }

    public void run() {
        XLog.d("miles", "SpaceScanManager startScanRubbish called");
        this.b.p.a((f) null);
        try {
            if (this.b.z != null) {
                XLog.d("miles", "isRubbishScanning = " + this.b.m);
                if (this.b.m) {
                    XLog.d("miles", "SpaceScanManager >> isRubbishScanning = true. cancelScanRubbish first.");
                    this.b.z.cancelScanRubbish();
                    AstApp.i().j().dispatchMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL));
                    Thread.sleep(1500);
                    boolean unused = this.b.m = true;
                    this.b.p.a(this.f3019a);
                    this.b.z.startScanRubbish(this.b.p);
                    return;
                }
                boolean unused2 = this.b.m = true;
                this.b.p.a(this.f3019a);
                this.b.z.startScanRubbish(this.b.p);
                return;
            }
            XLog.d("miles", "mService is null. callback onScanFinished now.");
            this.f3019a.b();
        } catch (Exception e) {
            XLog.d("miles", "startScanRubbish throws a Exception.");
            e.printStackTrace();
        }
    }
}
