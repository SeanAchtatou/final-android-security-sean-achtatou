package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class y implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RocketLauncher f2921a;

    y(RocketLauncher rocketLauncher) {
        this.f2921a = rocketLauncher;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(300);
        alphaAnimation.setFillAfter(true);
        this.f2921a.g.startAnimation(alphaAnimation);
        this.f2921a.f.startAnimation(alphaAnimation);
        n.a().n();
    }
}
