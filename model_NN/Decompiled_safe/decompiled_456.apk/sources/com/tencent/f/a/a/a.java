package com.tencent.f.a.a;

import android.text.TextUtils;
import com.tencent.d.a.f;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.zip.ZipException;

/* compiled from: ProGuard */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final c f2530a = new c(101010256);
    /* access modifiers changed from: private */
    public static final d b = new d(38650);

    public static String a(File file, String str) {
        RandomAccessFile randomAccessFile;
        Throwable th;
        String str2 = null;
        f.a("ApkExternalInfoTool", "enter");
        if (file != null) {
            f.a("ApkExternalInfoTool", "apkFile filename:" + file.getName());
            try {
                randomAccessFile = new RandomAccessFile(file, "r");
                try {
                    byte[] a2 = a(randomAccessFile);
                    if (a2 == null) {
                        f.a("ApkExternalInfoTool", "null == readComment");
                        f.a("ApkExternalInfoTool", "exit");
                        if (randomAccessFile != null) {
                            randomAccessFile.close();
                        }
                        f.a("ApkExternalInfoTool", "exit");
                    } else {
                        b bVar = new b(null);
                        bVar.a(a2);
                        str2 = bVar.f2531a.getProperty(str);
                        if (randomAccessFile != null) {
                            randomAccessFile.close();
                        }
                        f.a("ApkExternalInfoTool", "exit");
                    }
                } catch (Throwable th2) {
                    th = th2;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                randomAccessFile = null;
                th = th4;
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
                f.a("ApkExternalInfoTool", "exit");
                throw th;
            }
        }
        return str2;
    }

    public static String a(File file) {
        return a(file, "channelId");
    }

    public static byte[] a(RandomAccessFile randomAccessFile) {
        boolean z = true;
        long length = randomAccessFile.length() - 22;
        randomAccessFile.seek(length);
        byte[] a2 = f2530a.a();
        int read = randomAccessFile.read();
        while (true) {
            if (read != -1) {
                if (read == a2[0] && randomAccessFile.read() == a2[1] && randomAccessFile.read() == a2[2] && randomAccessFile.read() == a2[3]) {
                    break;
                }
                length--;
                randomAccessFile.seek(length);
                read = randomAccessFile.read();
            } else {
                z = false;
                break;
            }
        }
        if (!z) {
            f.c("ApkExternalInfoTool", "archive is not a ZIP archive");
            throw new ZipException("archive is not a ZIP archive");
        }
        randomAccessFile.seek(16 + length + 4);
        byte[] bArr = new byte[2];
        randomAccessFile.readFully(bArr);
        int b2 = new d(bArr).b();
        if (b2 == 0) {
            return null;
        }
        byte[] bArr2 = new byte[b2];
        randomAccessFile.read(bArr2);
        return bArr2;
    }

    public static boolean a(String str, String str2) {
        int i = 0;
        f.a("ApkExternalInfoTool", "enter");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            f.a("ApkExternalInfoTool", "TextUtils.isEmpty(oldFilePath) || TextUtils.isEmpty(newFilePath)");
            f.a("ApkExternalInfoTool", "exit");
            throw new Exception("Your file path is empty!");
        }
        f.a("ApkExternalInfoTool", "oldFilePath =" + str + "; newFilePath = " + str2);
        RandomAccessFile randomAccessFile = new RandomAccessFile(str, "r");
        RandomAccessFile randomAccessFile2 = new RandomAccessFile(str2, "rw");
        if (randomAccessFile.length() == 0 || randomAccessFile2.length() == 0) {
            randomAccessFile.close();
            randomAccessFile2.close();
            throw new Exception("Your file length is zero !!");
        }
        byte[] a2 = a(randomAccessFile);
        byte[] a3 = a(randomAccessFile2);
        if (a2 != null) {
            int length = a2.length;
            if (a3 != null) {
                i = a3.length;
            }
            long length2 = (randomAccessFile2.length() - ((long) i)) + ((long) length);
            f.a("ApkExternalInfoTool", "oldCommentLength = " + length + ", newCommentLength = " + i + ", oldArchiveFileLength = " + randomAccessFile.length() + ", newArchiveFileLength = " + length2);
            randomAccessFile2.seek((randomAccessFile2.length() - 2) - ((long) i));
            randomAccessFile2.write(new d(length).a());
            randomAccessFile2.write(a2);
            randomAccessFile2.setLength(length2);
            if (randomAccessFile != null) {
                randomAccessFile.close();
            }
            if (randomAccessFile2 != null) {
                randomAccessFile2.close();
            }
            f.a("ApkExternalInfoTool", "exit");
            return true;
        }
        f.b("ApkExternalInfoTool", "old comment is null");
        return false;
    }
}
