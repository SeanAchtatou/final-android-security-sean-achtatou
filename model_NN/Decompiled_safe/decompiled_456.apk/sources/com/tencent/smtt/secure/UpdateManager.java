package com.tencent.smtt.secure;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Base64;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.QbSdkLog;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class UpdateManager {
    private static final int DEFAULT_SDK_VERION = 1;
    private static final String DEFAULT_UPDATE_FILE_SHA1 = "ml4VnBfuz4JqqEKwf71fCA3uO3U=";
    private static final String DEFUALT_JS_VERSION = "1";
    private static final int DOWN_OVER = 2;
    private static final int DOWN_UPDATE = 1;
    private static final int NEED_UPDATE = 0;
    private static final String SHARED_PREFERENCE_CURRENT_UPDATE_FILE_SHA1 = "SHARED_PREFERENCE_CURRENT_UPDATE_FILE_SHA1";
    private static final String SHARED_PREFERENCE_CURRENT_VERSION = "SHARED_PREFERENCE_CURRENT_VERSION";
    private static final String SHARED_PREFERENCE_LAST_UPDATE_TIME = "SHARED_PREFERENCE_LAST_UPDATE_TIME";
    private static final String SHARED_PREFERENCE_NAME = "SHARED_PREFERENCE_SECURE_WEBVIEW";
    private static final String SIGNATURE = "30820254308201bda00302010202044c5eafe7300d06092a864886f70d01010b0500305d310b300906035504061302434e310b3009060355040813024744310b300906035504071302535a3110300e060355040a130754656e63656e743110300e060355040b130754656e63656e743110300e0603550403130754656e63656e74301e170d3134313033313038333935345a170d3135313033313038333935345a305d310b300906035504061302434e310b3009060355040813024744310b300906035504071302535a3110300e060355040a130754656e63656e743110300e060355040b130754656e63656e743110300e0603550403130754656e63656e7430819f300d06092a864886f70d010101050003818d0030818902818100b293a98fe569b7f9ba099e041c25038d8230e6fcbcee332499723e7d3c635795f6f8c04cdb25683080390119c4e5575bdf9d94b1969caeae09927ee38eb8e3ad9a5003a3dcc9055196341a50f5b06a6ec6e8c415ea8e42dee8d8838096022c3b54b299aafe3d2f934b65864506b379210382f826103476087d47c5191fb00e4b0203010001a321301f301d0603551d0e04160414b049af36c79e57278b3fda5ff8b1152ede6c83ca300d06092a864886f70d01010b0500038181003820e8817688a08d8bfef1cc3c5e7fe3343fa5786db96680d55a6d89145498fa1ae7f2de349e9deecd8ae9499e95a870f5810a1d9d81662f41ff29c23c0ddb51878b6926943fd5771d0e3dc463a7f0deb881355df3a45a206508ae5bc2c818038b0cd8fff3f52aeac3c70464c886917c67bc391fdae70a79fe02b9657190e6d4";
    private static final String UPDATE_FILE_NAME = "secure_update.apk";
    private static final String UPDATE_FILE_URL = "http://ter.sec.qq.com/secure_update.apk";
    private static final String UPDATE_INFO_URL = "http://ter.sec.qq.com/secure_update.info";
    private static final String UPDATE_JS_IN_APK = "assets/safe_uxss.js";
    private static final String UPDATE_SHA1_PREFIX = "SHA1:";
    private static final String UPDATE_VERSION_PREFIX = "VERSION:";
    private Thread downLoadThread = null;
    private Context mContext = null;
    /* access modifiers changed from: private */
    public String mCurrentUpdateFileSHA1 = Constants.STR_EMPTY;
    private Runnable mDownloadRunnable = new Runnable() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
        public void run() {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(UpdateManager.this.mUpdateFileUrl).openConnection();
                conn.connect();
                InputStream is = conn.getInputStream();
                FileOutputStream fos = new FileOutputStream(UpdateManager.this.mLocalUpdateFileName, false);
                byte[] buf = new byte[1024];
                while (true) {
                    int numRead = is.read(buf);
                    if (numRead > 0) {
                        fos.write(buf, 0, numRead);
                        if (UpdateManager.this.mInterceptFlag) {
                            break;
                        }
                    } else {
                        UpdateManager.this.mHandler.sendEmptyMessage(2);
                        break;
                    }
                }
                fos.close();
                is.close();
            } catch (Throwable e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                QbSdkLog.e("UpdateManager", "mDownloadRunnable - exceptions:" + errors.toString());
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler mHandler = null;
    /* access modifiers changed from: private */
    public boolean mInterceptFlag = false;
    private String mLocalJSFileName = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String mLocalUpdateFileName = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String mUpdateFileSHA1 = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String mUpdateFileUrl = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String mUpdateFileVersion = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String mUpdateInfoUrl = Constants.STR_EMPTY;

    public static int getSDKVersionCode() {
        return 1;
    }

    public static int getJSVersion(Context context) {
        int version;
        try {
            version = Integer.parseInt(context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0).getString(SHARED_PREFERENCE_CURRENT_VERSION, "1"));
        } catch (Exception e) {
            version = 1;
        }
        if (version <= 0 || version > 19860429) {
            return 1;
        }
        return version;
    }

    static class UpdateHandler extends Handler {
        private WeakReference<UpdateManager> mUpdateManager;

        public UpdateHandler(UpdateManager updateManager) {
            this.mUpdateManager = new WeakReference<>(updateManager);
        }

        public void handleMessage(Message msg) {
            UpdateManager updateManager = this.mUpdateManager.get();
            if (updateManager != null) {
                switch (msg.what) {
                    case 0:
                        updateManager.downloadFile();
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        updateManager.installFile();
                        return;
                }
            }
        }
    }

    private String getUpdateParams() {
        Context context = this.mContext;
        String pkg = context.getPackageName();
        String sysv = String.valueOf(Build.VERSION.SDK_INT);
        String brand = Build.MODEL + "_" + Build.BRAND;
        String deviceId = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        String pkgv = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        String sdkv = String.valueOf(getSDKVersionCode());
        String jsv = String.valueOf(getJSVersion(context));
        try {
            deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
        }
        if (deviceId == null) {
            deviceId = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        }
        try {
            String pkgVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (pkgVersion != null && pkgVersion.length() > 0) {
                pkgv = pkgVersion;
            }
        } catch (Exception e2) {
        }
        try {
            return ((((((Constants.STR_EMPTY + "?pkg=" + URLEncoder.encode(pkg, "UTF-8")) + "&sysv=" + URLEncoder.encode(sysv, "UTF-8")) + "&brand=" + URLEncoder.encode(brand, "UTF-8")) + "&did=" + URLEncoder.encode(deviceId, "UTF-8")) + "&pkgv=" + URLEncoder.encode(pkgv, "UTF-8")) + "&sdkv=" + URLEncoder.encode(sdkv, "UTF-8")) + "&jsv=" + URLEncoder.encode(jsv, "UTF-8");
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
            return Constants.STR_EMPTY;
        } catch (Exception e4) {
            e4.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    public UpdateManager(Context context, String jsFileName) {
        this.mContext = context.getApplicationContext();
        this.mLocalJSFileName = this.mContext.getFilesDir() + "/" + jsFileName;
        String params = getUpdateParams();
        this.mUpdateInfoUrl = UPDATE_INFO_URL + params;
        this.mUpdateFileUrl = UPDATE_FILE_URL + params;
        this.mLocalUpdateFileName = this.mContext.getFilesDir() + "/" + UPDATE_FILE_NAME;
        this.mCurrentUpdateFileSHA1 = getCurrentUpdateFileSHA1();
        this.mHandler = new UpdateHandler(this);
        checkUpdateInfo();
    }

    public void stopUpdate() {
        this.mInterceptFlag = true;
    }

    private String getCurrentUpdateFileSHA1() {
        return this.mContext.getSharedPreferences(SHARED_PREFERENCE_NAME, 0).getString(SHARED_PREFERENCE_CURRENT_UPDATE_FILE_SHA1, DEFAULT_UPDATE_FILE_SHA1);
    }

    public void checkUpdateInfo() {
        new Thread(new Runnable() {
            public void run() {
                ArrayList<String> arSites;
                try {
                    if (UpdateManager.this.checkLastUpdateTime() && (arSites = new UrlDownload(UpdateManager.this.mUpdateInfoUrl).getListString()) != null) {
                        for (int i = 0; i < arSites.size(); i++) {
                            String str = arSites.get(i);
                            if (str.startsWith(UpdateManager.UPDATE_SHA1_PREFIX)) {
                                String unused = UpdateManager.this.mUpdateFileSHA1 = str.substring(UpdateManager.UPDATE_SHA1_PREFIX.length()).trim();
                            } else if (str.startsWith(UpdateManager.UPDATE_VERSION_PREFIX)) {
                                String unused2 = UpdateManager.this.mUpdateFileVersion = str.substring(UpdateManager.UPDATE_VERSION_PREFIX.length()).trim();
                            }
                        }
                        if (!UpdateManager.this.mUpdateFileSHA1.equals(Constants.STR_EMPTY) && !UpdateManager.this.mUpdateFileVersion.equals(Constants.STR_EMPTY)) {
                            if (!UpdateManager.this.mUpdateFileSHA1.equals(UpdateManager.this.mCurrentUpdateFileSHA1)) {
                                UpdateManager.this.mHandler.sendEmptyMessage(0);
                            } else {
                                UpdateManager.this.updateLastUpdateTime(null);
                            }
                        }
                    }
                } catch (Throwable e) {
                    StringWriter errors = new StringWriter();
                    e.printStackTrace(new PrintWriter(errors));
                    QbSdkLog.e("UpdateManager", "checkUpdateInfo - exceptions:" + errors.toString());
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void updateLastUpdateTime(String version) {
        SharedPreferences.Editor editor = this.mContext.getSharedPreferences(SHARED_PREFERENCE_NAME, 0).edit();
        editor.putString(SHARED_PREFERENCE_LAST_UPDATE_TIME, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        if (version != null) {
            editor.putString(SHARED_PREFERENCE_CURRENT_VERSION, version);
        }
        editor.commit();
    }

    private void updateCurrentUpdateFileSHA1(String sha1) {
        SharedPreferences.Editor editor = this.mContext.getSharedPreferences(SHARED_PREFERENCE_NAME, 0).edit();
        editor.putString(SHARED_PREFERENCE_CURRENT_UPDATE_FILE_SHA1, sha1);
        editor.commit();
    }

    /* access modifiers changed from: private */
    public boolean checkLastUpdateTime() {
        String lastUpdateTimeStr = this.mContext.getSharedPreferences(SHARED_PREFERENCE_NAME, 0).getString(SHARED_PREFERENCE_LAST_UPDATE_TIME, Constants.STR_EMPTY);
        if (lastUpdateTimeStr == Constants.STR_EMPTY) {
            return true;
        }
        try {
            Date lastUpdateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lastUpdateTimeStr);
            Date now = new Date();
            if ((now.getTime() > lastUpdateTime.getTime() ? now.getTime() - lastUpdateTime.getTime() : lastUpdateTime.getTime() - now.getTime()) / TesDownloadConfig.TES_CONFIG_CHECK_PERIOD > 1) {
                return true;
            }
            return false;
        } catch (ParseException e) {
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void downloadFile() {
        this.downLoadThread = new Thread(this.mDownloadRunnable);
        this.downLoadThread.start();
    }

    /* access modifiers changed from: private */
    public void installFile() {
        boolean success;
        if (new File(this.mLocalUpdateFileName).exists() && this.mUpdateFileSHA1.equals(getSHA1String(this.mLocalUpdateFileName))) {
            try {
                success = extractFile(this.mLocalUpdateFileName, UPDATE_JS_IN_APK, this.mLocalJSFileName);
            } catch (Exception e) {
                e.printStackTrace();
                success = false;
            }
            if (success) {
                updateLastUpdateTime(this.mUpdateFileVersion);
                updateCurrentUpdateFileSHA1(this.mUpdateFileSHA1);
            }
        }
    }

    public boolean extractFile(String apkFileName, String fileNameInApk, String fileName) throws Exception {
        Certificate[] certs;
        JarFile jarFile = new JarFile(apkFileName);
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry je = entries.nextElement();
            if (!je.isDirectory() && je.getName().equals(fileNameInApk) && (certs = loadCertificates(jarFile, je)) != null && certs.length == 1) {
                if (!new Signature(certs[0].getEncoded()).equals(new Signature(SIGNATURE))) {
                    return false;
                }
                if (readJarFile(jarFile, je, fileName)) {
                    return true;
                }
                File file = new File(fileName);
                if (file.exists()) {
                    file.delete();
                }
                return false;
            }
        }
        return false;
    }

    private Certificate[] loadCertificates(JarFile jarFile, JarEntry je) {
        try {
            byte[] readBuffer = new byte[8192];
            InputStream is = new BufferedInputStream(jarFile.getInputStream(je));
            do {
            } while (is.read(readBuffer, 0, readBuffer.length) != -1);
            is.close();
            if (je != null) {
                return je.getCertificates();
            }
            return null;
        } catch (IOException | RuntimeException e) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    private boolean readJarFile(JarFile jarFile, JarEntry je, String fileName) {
        try {
            byte[] readBuffer = new byte[8192];
            InputStream is = new BufferedInputStream(jarFile.getInputStream(je));
            FileOutputStream fos = new FileOutputStream(fileName, false);
            while (true) {
                int length = is.read(readBuffer, 0, readBuffer.length);
                if (length != -1) {
                    fos.write(readBuffer, 0, length);
                } else {
                    is.close();
                    fos.close();
                    return true;
                }
            }
        } catch (IOException | RuntimeException e) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    public boolean copyFile(String dest, String src) {
        try {
            FileOutputStream fos = new FileOutputStream(dest, false);
            FileInputStream fis = new FileInputStream(src);
            byte[] buf = new byte[1024];
            while (true) {
                int count = fis.read(buf);
                if (count <= 0) {
                    fos.close();
                    fis.close();
                    return true;
                }
                fos.write(buf, 0, count);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getSHA1String(String filePath) {
        if (filePath != null && filePath.length() > 0) {
            try {
                if (filePath.length() > 0) {
                    InputStream fis = new FileInputStream(filePath);
                    BufferedInputStream bin = new BufferedInputStream(fis);
                    MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
                    messageDigest.reset();
                    byte[] buffer = new byte[1024];
                    while (true) {
                        int numRead = bin.read(buffer);
                        if (numRead > 0) {
                            messageDigest.update(buffer, 0, numRead);
                        } else {
                            bin.close();
                            fis.close();
                            return Base64.encodeToString(messageDigest.digest(), 0).trim();
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
        return Constants.STR_EMPTY;
    }
}
