package com.tencent.smtt.secure;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class ReportThread extends Thread {
    public static final String KEY_INDEX = "KEY_INDEX";
    public static final String KEY_TYPE = "KEY_TYPE";
    public static final String KEY_VALUE = "KEY_VALUE";
    public static final int MSG_CONSOLE = 0;
    public static final int MSG_WEBVIEW = 1;
    private ReportHandler mHandler = null;
    private Object mLock = new Object();
    private Looper mLooper = null;

    public void report(int type, int index, int value) {
        if (this.mHandler != null) {
            synchronized (this.mLock) {
                if (this.mHandler != null) {
                    Message msg = this.mHandler.obtainMessage();
                    Bundle data = new Bundle();
                    data.putInt(KEY_TYPE, type);
                    data.putInt(KEY_INDEX, index);
                    data.putInt(KEY_VALUE, value);
                    msg.setData(data);
                    this.mHandler.sendMessage(msg);
                }
            }
        }
    }

    public void stopThread() {
        if (this.mLooper != null) {
            synchronized (this.mLock) {
                if (this.mLooper != null) {
                    this.mLooper.quit();
                    this.mHandler.removeCallbacksAndMessages(null);
                    this.mHandler = null;
                    this.mLooper = null;
                }
            }
        }
    }

    static class ReportHandler extends Handler {
        ReportHandler() {
        }

        /* JADX WARN: Type inference failed for: r15v31, types: [java.net.URLConnection] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0158  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x015d A[SYNTHETIC, Splitter:B:24:0x015d] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0190  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0195 A[SYNTHETIC, Splitter:B:32:0x0195] */
        /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r20) {
            /*
                r19 = this;
                android.os.Bundle r5 = r20.getData()
                java.lang.String r15 = "KEY_TYPE"
                int r15 = r5.getInt(r15)
                java.lang.String r11 = java.lang.String.valueOf(r15)
                java.lang.String r15 = "KEY_INDEX"
                int r15 = r5.getInt(r15)
                java.lang.String r10 = java.lang.String.valueOf(r15)
                java.lang.String r15 = "KEY_VALUE"
                int r15 = r5.getInt(r15)
                java.lang.String r14 = java.lang.String.valueOf(r15)
                java.lang.StringBuilder r15 = new java.lang.StringBuilder
                r15.<init>()
                java.lang.String r16 = android.os.Build.MODEL
                java.lang.StringBuilder r15 = r15.append(r16)
                java.lang.String r16 = ","
                java.lang.StringBuilder r15 = r15.append(r16)
                java.lang.String r16 = android.os.Build.BRAND
                java.lang.StringBuilder r15 = r15.append(r16)
                java.lang.String r2 = r15.toString()
                int r15 = r2.length()
                r16 = 30
                r0 = r16
                if (r15 <= r0) goto L_0x0050
                r15 = 0
                r16 = 30
                r0 = r16
                java.lang.String r2 = r2.substring(r15, r0)
            L_0x0050:
                int r15 = android.os.Build.VERSION.SDK_INT
                java.lang.String r1 = java.lang.String.valueOf(r15)
                r4 = 0
                r8 = 0
                java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x012c }
                r15.<init>()     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "http://zyjc.sec.qq.com/reportVT?version="
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                int r16 = com.tencent.smtt.secure.UpdateManager.getSDKVersionCode()     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = java.lang.String.valueOf(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r17 = "UTF-8"
                java.lang.String r16 = java.net.URLEncoder.encode(r16, r17)     // Catch:{ Throwable -> 0x012c }
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "&type="
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "UTF-8"
                r0 = r16
                java.lang.String r16 = java.net.URLEncoder.encode(r11, r0)     // Catch:{ Throwable -> 0x012c }
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "&index="
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "UTF-8"
                r0 = r16
                java.lang.String r16 = java.net.URLEncoder.encode(r10, r0)     // Catch:{ Throwable -> 0x012c }
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "&value="
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "UTF-8"
                r0 = r16
                java.lang.String r16 = java.net.URLEncoder.encode(r14, r0)     // Catch:{ Throwable -> 0x012c }
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "&brand="
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "UTF-8"
                r0 = r16
                java.lang.String r16 = java.net.URLEncoder.encode(r2, r0)     // Catch:{ Throwable -> 0x012c }
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "&version="
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r16 = "UTF-8"
                r0 = r16
                java.lang.String r16 = java.net.URLEncoder.encode(r1, r0)     // Catch:{ Throwable -> 0x012c }
                java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Throwable -> 0x012c }
                java.lang.String r13 = r15.toString()     // Catch:{ Throwable -> 0x012c }
                java.net.URL r12 = new java.net.URL     // Catch:{ Throwable -> 0x012c }
                r12.<init>(r13)     // Catch:{ Throwable -> 0x012c }
                java.net.URLConnection r15 = r12.openConnection()     // Catch:{ Throwable -> 0x012c }
                r0 = r15
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x012c }
                r4 = r0
                java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x012c }
                java.io.InputStream r15 = r4.getInputStream()     // Catch:{ Throwable -> 0x012c }
                r9.<init>(r15)     // Catch:{ Throwable -> 0x012c }
                java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x01cb, all -> 0x01c8 }
                r3.<init>(r9)     // Catch:{ Throwable -> 0x01cb, all -> 0x01c8 }
            L_0x00ee:
                java.lang.String r15 = r3.readLine()     // Catch:{ Throwable -> 0x01cb, all -> 0x01c8 }
                if (r15 != 0) goto L_0x00ee
                if (r4 == 0) goto L_0x00f9
                r4.disconnect()
            L_0x00f9:
                if (r9 == 0) goto L_0x01cf
                r9.close()     // Catch:{ Throwable -> 0x0100 }
                r8 = r9
            L_0x00ff:
                return
            L_0x0100:
                r6 = move-exception
                java.io.StringWriter r7 = new java.io.StringWriter
                r7.<init>()
                java.io.PrintWriter r15 = new java.io.PrintWriter
                r15.<init>(r7)
                r6.printStackTrace(r15)
                java.lang.String r15 = "ReportThread"
                java.lang.StringBuilder r16 = new java.lang.StringBuilder
                r16.<init>()
                java.lang.String r17 = "ReportHandler - exceptions2:"
                java.lang.StringBuilder r16 = r16.append(r17)
                java.lang.String r17 = r7.toString()
                java.lang.StringBuilder r16 = r16.append(r17)
                java.lang.String r16 = r16.toString()
                com.tencent.smtt.sdk.QbSdkLog.e(r15, r16)
                r8 = r9
                goto L_0x00ff
            L_0x012c:
                r6 = move-exception
            L_0x012d:
                java.io.StringWriter r7 = new java.io.StringWriter     // Catch:{ all -> 0x018d }
                r7.<init>()     // Catch:{ all -> 0x018d }
                java.io.PrintWriter r15 = new java.io.PrintWriter     // Catch:{ all -> 0x018d }
                r15.<init>(r7)     // Catch:{ all -> 0x018d }
                r6.printStackTrace(r15)     // Catch:{ all -> 0x018d }
                java.lang.String r15 = "ReportThread"
                java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ all -> 0x018d }
                r16.<init>()     // Catch:{ all -> 0x018d }
                java.lang.String r17 = "ReportHandler - exceptions1:"
                java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ all -> 0x018d }
                java.lang.String r17 = r7.toString()     // Catch:{ all -> 0x018d }
                java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ all -> 0x018d }
                java.lang.String r16 = r16.toString()     // Catch:{ all -> 0x018d }
                com.tencent.smtt.sdk.QbSdkLog.e(r15, r16)     // Catch:{ all -> 0x018d }
                if (r4 == 0) goto L_0x015b
                r4.disconnect()
            L_0x015b:
                if (r8 == 0) goto L_0x00ff
                r8.close()     // Catch:{ Throwable -> 0x0161 }
                goto L_0x00ff
            L_0x0161:
                r6 = move-exception
                java.io.StringWriter r7 = new java.io.StringWriter
                r7.<init>()
                java.io.PrintWriter r15 = new java.io.PrintWriter
                r15.<init>(r7)
                r6.printStackTrace(r15)
                java.lang.String r15 = "ReportThread"
                java.lang.StringBuilder r16 = new java.lang.StringBuilder
                r16.<init>()
                java.lang.String r17 = "ReportHandler - exceptions2:"
                java.lang.StringBuilder r16 = r16.append(r17)
                java.lang.String r17 = r7.toString()
                java.lang.StringBuilder r16 = r16.append(r17)
                java.lang.String r16 = r16.toString()
                com.tencent.smtt.sdk.QbSdkLog.e(r15, r16)
                goto L_0x00ff
            L_0x018d:
                r15 = move-exception
            L_0x018e:
                if (r4 == 0) goto L_0x0193
                r4.disconnect()
            L_0x0193:
                if (r8 == 0) goto L_0x0198
                r8.close()     // Catch:{ Throwable -> 0x0199 }
            L_0x0198:
                throw r15
            L_0x0199:
                r6 = move-exception
                java.io.StringWriter r7 = new java.io.StringWriter
                r7.<init>()
                java.io.PrintWriter r16 = new java.io.PrintWriter
                r0 = r16
                r0.<init>(r7)
                r0 = r16
                r6.printStackTrace(r0)
                java.lang.String r16 = "ReportThread"
                java.lang.StringBuilder r17 = new java.lang.StringBuilder
                r17.<init>()
                java.lang.String r18 = "ReportHandler - exceptions2:"
                java.lang.StringBuilder r17 = r17.append(r18)
                java.lang.String r18 = r7.toString()
                java.lang.StringBuilder r17 = r17.append(r18)
                java.lang.String r17 = r17.toString()
                com.tencent.smtt.sdk.QbSdkLog.e(r16, r17)
                goto L_0x0198
            L_0x01c8:
                r15 = move-exception
                r8 = r9
                goto L_0x018e
            L_0x01cb:
                r6 = move-exception
                r8 = r9
                goto L_0x012d
            L_0x01cf:
                r8 = r9
                goto L_0x00ff
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.smtt.secure.ReportThread.ReportHandler.handleMessage(android.os.Message):void");
        }
    }

    public void run() {
        Looper.prepare();
        this.mHandler = new ReportHandler();
        this.mLooper = Looper.myLooper();
        Looper.loop();
    }
}
