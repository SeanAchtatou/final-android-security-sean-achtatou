package com.tencent.smtt.utils;

import java.lang.reflect.Method;

public class ReflectionUtils {
    private ReflectionUtils() {
    }

    public static Object invokeStatic(String className, String methodName) {
        try {
            return Class.forName(className).getMethod(methodName, new Class[0]).invoke(null, new Object[0]);
        } catch (Throwable th) {
            return null;
        }
    }

    public static Object invokeStatic(Class<?> classObj, String methodName, Class<?>[] parameterTypes, Object... args) {
        try {
            Method method = classObj.getMethod(methodName, parameterTypes);
            method.setAccessible(true);
            return method.invoke(null, args);
        } catch (Throwable ignore) {
            ignore.printStackTrace();
            return null;
        }
    }

    public static Object invokeInstance(Object obj, String methodName) {
        return invokeInstance(obj, methodName, null, new Object[0]);
    }

    public static Object invokeInstance(Object obj, String methodName, Class<?>[] paramTypes, Object... params) {
        if (obj == null) {
            return null;
        }
        try {
            Method m = obj.getClass().getMethod(methodName, paramTypes);
            m.setAccessible(true);
            if (params.length == 0) {
                params = null;
            }
            return m.invoke(obj, params);
        } catch (Throwable th) {
            return null;
        }
    }

    public static Method getParentDeclaredMethod(Object object, String methodName, Class<?>... parameterTypes) {
        Class<?> clazz = object.getClass();
        while (clazz != Object.class && clazz != null) {
            try {
                return clazz.getDeclaredMethod(methodName, parameterTypes);
            } catch (Exception e) {
                clazz = clazz.getSuperclass();
            }
        }
        return null;
    }
}
