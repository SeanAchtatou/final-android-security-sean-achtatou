package com.tencent.smtt.export.external.extension.interfaces;

public interface IX5WebSettingsExtension {
    boolean getPageSolarEnableFlag();

    boolean isFitScreen();

    boolean isReadModeWebView();

    boolean isWapSitePreferred();

    boolean isWebViewInBackground();

    void setDayOrNight(boolean z);

    void setEnableUnderLine(boolean z);

    void setFitScreen(boolean z);

    void setImgAsDownloadFile(boolean z);

    void setOnlyDomTreeBuild(boolean z);

    void setPageCacheCapacity(int i);

    void setPageSolarEnableFlag(boolean z);

    void setPreFectch(boolean z);

    void setPreFectchEnableWhenHasMedia(boolean z);

    void setReadModeWebView(boolean z);

    void setRememberScaleValue(boolean z);

    void setShouldTrackVisitedLinks(boolean z);

    void setWapSitePreferred(boolean z);

    void setWebViewInBackground(boolean z);
}
