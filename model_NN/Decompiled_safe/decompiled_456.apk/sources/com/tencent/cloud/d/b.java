package com.tencent.cloud.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.ag;
import com.tencent.assistant.module.ak;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GetRecommendTabPageRequest;
import com.tencent.assistant.protocol.jce.GetRecommendTabPageResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.cloud.d.a.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class b extends BaseEngine<a> implements ak {

    /* renamed from: a  reason: collision with root package name */
    private static b f2300a = null;
    /* access modifiers changed from: private */
    public long b = -1;
    /* access modifiers changed from: private */
    public List<SimpleAppModel> c;
    /* access modifiers changed from: private */
    public byte[] d;
    /* access modifiers changed from: private */
    public boolean e = true;
    private int f = -1;
    private int g = -1;
    private int h = 0;
    private List<SimpleAppModel> i = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList<ColorCardItem> j = new ArrayList<>();
    /* access modifiers changed from: private */
    public byte[] k;
    /* access modifiers changed from: private */
    public boolean l = true;
    private i m;
    private com.tencent.assistant.model.b n = new com.tencent.assistant.model.b();

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f2300a == null) {
                f2300a = new b();
            }
            bVar = f2300a;
        }
        return bVar;
    }

    private b() {
        ag.b().a(this);
        this.m = new i(this);
    }

    public com.tencent.assistant.model.b b() {
        this.n.b(this.b);
        this.n.b(f());
        this.n.a(this.j);
        return this.n;
    }

    public GetRecommendTabPageResponse c() {
        return i.y().b((byte[]) null);
    }

    /* access modifiers changed from: private */
    public boolean g() {
        boolean z = false;
        GetRecommendTabPageResponse c2 = c();
        if (c2 == null) {
            return false;
        }
        long a2 = m.a().a((byte) 5);
        if (c2.e != a2) {
            return false;
        }
        if (a2 != -1 && c2.e != a2) {
            return false;
        }
        ArrayList<ColorCardItem> b2 = c2.b();
        this.c = k.b(c2.b);
        if (this.c == null || this.c.size() == 0 || b2 == null || b2.size() == 0) {
            return false;
        }
        if (1 == c2.f) {
            z = true;
        }
        this.e = z;
        this.d = c2.d;
        this.k = this.d;
        if (this.j == null) {
            this.j = new ArrayList<>();
        }
        this.j.clear();
        this.j.addAll(b2);
        if (this.i == null) {
            this.i = new ArrayList();
        }
        this.i.clear();
        this.i.addAll(this.c);
        this.b = c2.d();
        this.n.b(this.b);
        notifyDataChangedInMainThread(new c(this, b2));
        if (this.l) {
            this.m.a(this.k);
        }
        return true;
    }

    public void onLocalDataHasUpdate() {
        if (this.b != m.a().a((byte) 5)) {
            d();
        }
    }

    public void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public int d() {
        if (this.f > 0) {
            cancel(this.f);
        }
        this.f = b(new byte[0]);
        return this.f;
    }

    private int a(byte[] bArr) {
        if (this.g > 0) {
            cancel(this.g);
        }
        this.g = b(bArr);
        return this.g;
    }

    public int e() {
        if (this.k == null || this.k.length == 0) {
            return -1;
        }
        if (this.m.c()) {
            int a2 = this.m.a();
            this.m.b();
            return a2;
        } else if (this.k != this.m.d() || this.m.e() == null) {
            return a(this.k);
        } else {
            i j2 = this.m.clone();
            this.b = j2.h();
            if (this.b != m.a().a((byte) 5)) {
                return a(this.k);
            }
            boolean z = this.b != j2.h();
            int a3 = j2.a();
            this.n.b(this.b);
            if (z) {
                this.c = j2.e();
                this.d = j2.g();
                this.e = j2.f();
                this.i.clear();
            }
            ArrayList arrayList = new ArrayList(j2.e());
            this.i.addAll(arrayList);
            this.l = j2.f();
            this.k = j2.g();
            notifyDataChangedInMainThread(new d(this, a3, arrayList));
            if (this.m.e) {
                this.m.a(this.m.g());
            }
            return this.m.a();
        }
    }

    private int b(byte[] bArr) {
        return a(-1, bArr);
    }

    /* access modifiers changed from: private */
    public int a(int i2, byte[] bArr) {
        this.h = 0;
        GetRecommendTabPageRequest getRecommendTabPageRequest = new GetRecommendTabPageRequest();
        getRecommendTabPageRequest.f1322a = 30;
        if (bArr == null) {
            bArr = new byte[0];
        }
        getRecommendTabPageRequest.b = bArr;
        return send(i2, getRecommendTabPageRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        boolean z2 = true;
        if (jceStruct2 != null) {
            GetRecommendTabPageResponse getRecommendTabPageResponse = (GetRecommendTabPageResponse) jceStruct2;
            GetRecommendTabPageRequest getRecommendTabPageRequest = (GetRecommendTabPageRequest) jceStruct;
            ArrayList<SimpleAppModel> b2 = k.b(getRecommendTabPageResponse.a());
            if (i2 == this.m.a()) {
                i iVar = this.m;
                long j2 = getRecommendTabPageResponse.e;
                if (getRecommendTabPageResponse.f != 1) {
                    z2 = false;
                }
                iVar.a(j2, b2, z2, getRecommendTabPageResponse.c());
            } else if (this.k != getRecommendTabPageResponse.c()) {
                boolean z3 = getRecommendTabPageResponse.e != this.b || getRecommendTabPageRequest.b == null || getRecommendTabPageRequest.b.length == 0;
                if (getRecommendTabPageResponse.f == 1) {
                    z = true;
                } else {
                    z = false;
                }
                byte[] c2 = getRecommendTabPageResponse.c();
                this.b = getRecommendTabPageResponse.e;
                this.n.b(this.b);
                if (z3) {
                    if (this.j == null) {
                        this.j = new ArrayList<>();
                    }
                    this.j.clear();
                    this.j.addAll(getRecommendTabPageResponse.b());
                    this.c = b2;
                    this.d = c2;
                    this.e = z;
                    this.i.clear();
                }
                if (!(b2 == null || b2.size() == 0)) {
                    this.i.addAll(b2);
                }
                this.l = z;
                this.k = c2;
                notifyDataChangedInMainThread(new e(this, i2, z, c2, z3, b2));
                if (this.l) {
                    this.m.a(this.k);
                }
            }
            i.y().a(getRecommendTabPageRequest.b, getRecommendTabPageResponse);
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i2 != this.m.a()) {
            GetRecommendTabPageRequest getRecommendTabPageRequest = (GetRecommendTabPageRequest) jceStruct;
            if (getRecommendTabPageRequest.b == null || getRecommendTabPageRequest.b.length == 0) {
                this.h = i3;
                TemporaryThreadManager.get().start(new f(this, i2, i3));
                return;
            }
            notifyDataChangedInMainThread(new h(this, i2, i3));
            return;
        }
        this.m.i();
    }

    public List<SimpleAppModel> f() {
        return this.i;
    }

    public String toString() {
        return getClass().getSimpleName().toString();
    }
}
