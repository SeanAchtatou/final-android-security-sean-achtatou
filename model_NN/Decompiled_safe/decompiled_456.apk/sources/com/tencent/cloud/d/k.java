package com.tencent.cloud.d;

import android.os.Handler;
import android.util.SparseArray;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.CftGetNavigationRequest;
import com.tencent.assistant.protocol.jce.CftGetNavigationResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.an;
import com.tencent.cloud.d.a.b;
import java.util.Iterator;

/* compiled from: ProGuard */
public class k extends BaseEngine<b> {

    /* renamed from: a  reason: collision with root package name */
    private static k f2309a;
    private static Handler e = null;
    private SparseArray<p> b = new SparseArray<>();
    private final Object c = new Object();
    /* access modifiers changed from: private */
    public final int[] d = {16};

    public static synchronized k a() {
        k kVar;
        synchronized (k.class) {
            f2309a = new k();
            kVar = f2309a;
        }
        return kVar;
    }

    private k() {
        b(this.d[0]);
        try {
            e = ah.a("CftGetNavigationEngineHandler");
        } catch (Throwable th) {
            t.a().b();
        }
        if (e != null) {
            e.postDelayed(new l(this), 5);
        } else {
            XLog.e("CftGetNavigationEngine", "GetNavigationEngine HandlerUtils.getHandler() == null");
        }
    }

    /* access modifiers changed from: protected */
    public p a(int i) {
        CftGetNavigationResponse cftGetNavigationResponse;
        byte[] g = m.a().g(i);
        if (g != null && g.length > 0) {
            try {
                cftGetNavigationResponse = (CftGetNavigationResponse) an.b(g, CftGetNavigationResponse.class);
            } catch (Exception e2) {
                XLog.e("CftGetNavigationEngine", "response to navi object fail.typeid:" + i + ".ex:" + e2);
            }
            return p.a(i, cftGetNavigationResponse);
        }
        cftGetNavigationResponse = null;
        return p.a(i, cftGetNavigationResponse);
    }

    public p b(int i) {
        p pVar = this.b.get(i);
        if (pVar == null) {
            synchronized (this.c) {
                pVar = this.b.get(i);
                if (pVar == null) {
                    pVar = a(i);
                    this.b.put(i, pVar);
                }
            }
        }
        return pVar;
    }

    /* access modifiers changed from: private */
    public int a(long j, int i) {
        CftGetNavigationRequest cftGetNavigationRequest = new CftGetNavigationRequest();
        cftGetNavigationRequest.f1188a = j;
        cftGetNavigationRequest.b = i;
        return send(cftGetNavigationRequest);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        String str;
        int i2;
        CftGetNavigationResponse cftGetNavigationResponse = (CftGetNavigationResponse) jceStruct2;
        CftGetNavigationRequest cftGetNavigationRequest = (CftGetNavigationRequest) jceStruct;
        if (cftGetNavigationResponse == null || cftGetNavigationRequest == null || cftGetNavigationResponse.b == cftGetNavigationRequest.a()) {
            XLog.e("CftGetNavigationEngine", "CftGetNavigationEngine has null value.seq:" + i + ",request:" + jceStruct + ",response:" + cftGetNavigationResponse);
            return;
        }
        int b2 = cftGetNavigationRequest.b();
        p b3 = p.b(b2, cftGetNavigationResponse);
        m.a().b("key_navi_reminder_t_" + b2, (Object) 0);
        if (b3 != null) {
            byte[] a2 = an.a(cftGetNavigationResponse);
            if (a2 != null) {
                m.a().b(b2, a2);
            }
            p pVar = this.b.get(b2);
            if (pVar == null || pVar.b == null || pVar.b == null || pVar.b.size() <= 0) {
                notifyDataChangedInMainThread(new n(this, i, b2, b3));
                return;
            }
            Iterator<o> it = pVar.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    str = null;
                    i2 = 0;
                    break;
                }
                o next = it.next();
                if (next.c > 0) {
                    i2 = next.c;
                    String str2 = next.f2313a;
                    m.a().b("key_navi_reminder_t_" + b2, Integer.valueOf(i2));
                    str = str2;
                    break;
                }
            }
            for (o next2 : pVar.b) {
                if (next2.f2313a == null || !next2.f2313a.equals(str)) {
                    next2.c = 0;
                } else {
                    next2.c = i2;
                }
            }
            notifyDataChangedInMainThread(new m(this, i, b2, pVar));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("CftGetNavigationEngine", "CftGetNavigationEngine error code:" + i2 + ".seq:" + i + ",request:" + jceStruct + ",response:" + jceStruct2);
    }
}
