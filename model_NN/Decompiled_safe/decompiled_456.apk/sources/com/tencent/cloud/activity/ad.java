package com.tencent.cloud.activity;

import android.os.Bundle;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewClientExtension;
import com.tencent.smtt.export.external.extension.interfaces.IX5WebViewExtension;

/* compiled from: ProGuard */
class ad implements IX5WebViewClientExtension {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VideoActivity f2170a;

    private ad(VideoActivity videoActivity) {
        this.f2170a = videoActivity;
    }

    public void handlePluginTag(String str, String str2, boolean z, String str3) {
    }

    public void hideAddressBar() {
    }

    public void onDoubleTapStart() {
    }

    public void onFlingScrollBegin(int i, int i2, int i3) {
    }

    public void onFlingScrollEnd() {
    }

    public void onHideListBox() {
    }

    public void onHistoryItemChanged() {
    }

    public void onInputBoxTextChanged(IX5WebViewExtension iX5WebViewExtension, String str) {
    }

    public Object onMiscCallBack(String str, Bundle bundle) {
        if (!"onVideoScreenModeChanged".equals(str) || bundle == null || 102 != bundle.getInt(AppConst.KEY_FROM_TYPE) || 101 != bundle.getInt("to")) {
            return null;
        }
        this.f2170a.finish();
        XLog.i(VideoActivity.f2164a, "*** onMiscCallBack finish VideoActivity ***");
        return true;
    }

    public void onMissingPluginClicked(String str, String str2, String str3, int i) {
    }

    public void onNativeCrashReport(int i, String str) {
    }

    public void onPinchToZoomStart() {
    }

    public void onPreReadFinished() {
    }

    public void onPromptScaleSaved() {
    }

    public void onReportAdFilterInfo(int i, int i2, String str, boolean z) {
    }

    public void onScrollChanged(int i, int i2, int i3, int i4) {
    }

    public void onSetButtonStatus(boolean z, boolean z2) {
    }

    public void onShowListBox(String[] strArr, int[] iArr, int[] iArr2, int i) {
    }

    public void onShowMutilListBox(String[] strArr, int[] iArr, int[] iArr2, int[] iArr3) {
    }

    public void onSlidingTitleOffScreen(int i, int i2) {
    }

    public void onSoftKeyBoardHide(int i) {
    }

    public void onSoftKeyBoardShow() {
    }

    public void onTransitionToCommitted() {
    }

    public void onUploadProgressChange(int i, int i2, String str) {
    }

    public void onUploadProgressStart(int i) {
    }

    public void onUrlChange(String str, String str2) {
    }

    public boolean preShouldOverrideUrlLoading(IX5WebViewExtension iX5WebViewExtension, String str) {
        return false;
    }
}
