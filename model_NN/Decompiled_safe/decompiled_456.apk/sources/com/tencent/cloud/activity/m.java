package com.tencent.cloud.activity;

import android.view.View;
import com.tencent.assistant.protocol.jce.TagGroup;

/* compiled from: ProGuard */
class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailActivity f2196a;

    m(CategoryDetailActivity categoryDetailActivity) {
        this.f2196a = categoryDetailActivity;
    }

    public void onClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null && this.f2196a.C != null) {
            this.f2196a.C.k();
            this.f2196a.C.d(tagGroup.b());
        }
    }
}
