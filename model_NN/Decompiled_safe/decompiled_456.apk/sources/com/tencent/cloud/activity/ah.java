package com.tencent.cloud.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class ah implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VideoActivityV2 f2174a;

    private ah(VideoActivityV2 videoActivityV2) {
        this.f2174a = videoActivityV2;
    }

    /* synthetic */ ah(VideoActivityV2 videoActivityV2, ae aeVar) {
        this(videoActivityV2);
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        XLog.i(VideoActivityV2.f2165a, "[MyDownloaderListener] : url = " + str);
        XLog.i(VideoActivityV2.f2165a, "[MyDownloaderListener] : userAgent = " + str2);
        XLog.i(VideoActivityV2.f2165a, "[MyDownloaderListener] : contentDisposition = " + str3);
        XLog.i(VideoActivityV2.f2165a, "[MyDownloaderListener] : mimetype = " + str4);
        XLog.i(VideoActivityV2.f2165a, "[MyDownloaderListener] : contentLength = " + j);
        try {
            this.f2174a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException | Exception e) {
        }
    }
}
