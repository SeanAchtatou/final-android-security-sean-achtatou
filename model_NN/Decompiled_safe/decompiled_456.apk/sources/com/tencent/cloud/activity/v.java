package com.tencent.cloud.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.activity.a;

/* compiled from: ProGuard */
public abstract class v extends a {
    FrameLayout R;
    View S;
    boolean T = true;
    private final String U = "TreasuryTabActivity:";
    private int V = 5;

    public abstract View H();

    public void d(Bundle bundle) {
        super.d(bundle);
        b((int) R.layout.act6);
        this.R = (FrameLayout) c((int) R.id.container);
    }

    public void d(boolean z) {
        if (this.T) {
            this.S = H();
            this.R.addView(this.S, new FrameLayout.LayoutParams(-1, -1));
            this.T = false;
        }
        if (this.S == null) {
            return;
        }
        if (this.S instanceof VideoTabActivity) {
            ((VideoTabActivity) this.S).a();
        } else if (this.S instanceof EBookTabActivity) {
            ((EBookTabActivity) this.S).a();
        }
    }

    public v() {
        super(MainActivity.t());
    }

    public void k() {
        super.k();
        if (this.S == null) {
            return;
        }
        if (this.S instanceof VideoTabActivity) {
            ((VideoTabActivity) this.S).b();
        } else if (this.S instanceof EBookTabActivity) {
            ((EBookTabActivity) this.S).b();
        }
    }

    public void D() {
    }

    public int E() {
        return 0;
    }

    public int F() {
        return this.V;
    }

    public int G() {
        if (!(this.R == null || this.S == null)) {
            if (this.S instanceof VideoTabActivity) {
                return ((VideoTabActivity) this.S).c();
            }
            if (this.S instanceof EBookTabActivity) {
                return ((EBookTabActivity) this.S).c();
            }
        }
        return 2000;
    }

    public void C() {
        if (this.R == null || this.R.getChildAt(0) == null || this.S != null) {
            B();
        }
    }
}
