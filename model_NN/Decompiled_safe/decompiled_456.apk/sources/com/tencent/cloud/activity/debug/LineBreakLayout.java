package com.tencent.cloud.activity.debug;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: ProGuard */
public class LineBreakLayout extends ViewGroup {
    public LineBreakLayout(Context context) {
        super(context);
    }

    public LineBreakLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LineBreakLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int size = View.MeasureSpec.getSize(i);
        int childCount = getChildCount();
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (i5 < childCount) {
            View childAt = getChildAt(i5);
            childAt.measure(0, 0);
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            if (measuredHeight > i6) {
                i6 = measuredHeight;
            }
            if (i7 + measuredWidth + 0 > size) {
                i4 = i6 + 0 + i8;
                i3 = 0;
            } else {
                measuredHeight = i6;
                i3 = i7;
                i4 = i8;
            }
            int i9 = measuredWidth + 0 + i3;
            i5++;
            i6 = measuredHeight;
            int i10 = i9;
            i8 = i4;
            i7 = i10;
        }
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(i6 + 0 + i8, View.MeasureSpec.getMode(i2)));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i + 0;
        int i6 = i2 + 0;
        int i7 = 0;
        for (int i8 = 0; i8 < getChildCount(); i8++) {
            View childAt = getChildAt(i8);
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            if (measuredHeight > i7) {
                i7 = measuredHeight;
            }
            if (i5 + measuredWidth + 0 > i3) {
                i5 = i + 0;
                i6 += i7 + 0;
                i7 = measuredHeight;
            }
            childAt.layout(i5, i6, i5 + measuredWidth, measuredHeight + i6);
            i5 += measuredWidth + 0;
        }
    }
}
