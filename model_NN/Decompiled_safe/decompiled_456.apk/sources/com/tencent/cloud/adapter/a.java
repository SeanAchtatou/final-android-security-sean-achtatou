package com.tencent.cloud.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.BreakTextView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.updaterec.UpdateRecOneMoreListView;
import com.tencent.cloud.updaterec.f;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.process.s;

/* compiled from: ProGuard */
public abstract class a {
    public static Pair<View, b> a(Context context, LayoutInflater layoutInflater, View view, int i) {
        b bVar;
        if (view == null || !(view.getTag() instanceof b)) {
            view = layoutInflater.inflate((int) R.layout.app_updatelist_item, (ViewGroup) null);
            b bVar2 = new b();
            bVar2.f2211a = view;
            bVar2.b = (RelativeLayout) view.findViewById(R.id.root);
            bVar2.c = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            bVar2.d = (TextView) view.findViewById(R.id.title);
            bVar2.f = (DownloadButton) view.findViewById(R.id.state_app_btn);
            bVar2.e = (RelativeLayout) view.findViewById(R.id.title_layout);
            bVar2.g = (ListItemInfoView) view.findViewById(R.id.download_info);
            bVar2.h = (TextView) view.findViewById(R.id.desc);
            bVar2.l = (ImageView) view.findViewById(R.id.ignore_img);
            if (i == 2) {
                bVar2.l.setImageDrawable(context.getResources().getDrawable(R.drawable.reminder));
            }
            bVar2.j = (BreakTextView) view.findViewById(R.id.new_feature);
            bVar2.k = (TextView) view.findViewById(R.id.new_feature_all);
            bVar2.i = (RelativeLayout) view.findViewById(R.id.description_bottomlayout);
            bVar2.m = (ImageView) view.findViewById(R.id.show_more);
            bVar2.n = new f();
            bVar2.n.f2337a = (UpdateRecOneMoreListView) view.findViewById(R.id.one_more_list);
            bVar2.n.b = (RelativeLayout) view.findViewById(R.id.one_more_list_parent);
            bVar2.n.d = (TextView) view.findViewById(R.id.one_more_title);
            bVar2.e.setTag(bVar2);
            view.setTag(bVar2);
            bVar = bVar2;
        } else {
            bVar = (b) view.getTag();
        }
        return Pair.create(view, bVar);
    }

    public static void a(Context context, b bVar, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        bVar.b.setPadding(0, 0, 0, by.a(context, 5.0f));
        bVar.d.setText(simpleAppModel.d);
        bVar.g.a(simpleAppModel);
        bVar.c.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        bVar.f.a(simpleAppModel);
        if (s.a(simpleAppModel)) {
            bVar.f.setClickable(false);
        } else {
            bVar.f.setClickable(true);
            bVar.f.a(sTInfoV2);
        }
        String a2 = a(simpleAppModel.c);
        if (TextUtils.isEmpty(a2)) {
            bVar.h.setVisibility(8);
            ViewGroup.LayoutParams layoutParams = bVar.e.getLayoutParams();
            if (layoutParams instanceof RelativeLayout.LayoutParams) {
                ((RelativeLayout.LayoutParams) layoutParams).setMargins(((RelativeLayout.LayoutParams) layoutParams).leftMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_noreason_margin_top), ((RelativeLayout.LayoutParams) layoutParams).rightMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_noreason_margin_bottom));
                return;
            }
            return;
        }
        bVar.h.setVisibility(0);
        bVar.h.setText(Html.fromHtml(a2));
        ViewGroup.LayoutParams layoutParams2 = bVar.e.getLayoutParams();
        if (layoutParams2 instanceof RelativeLayout.LayoutParams) {
            ((RelativeLayout.LayoutParams) layoutParams2).setMargins(((RelativeLayout.LayoutParams) layoutParams2).leftMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_margin_top), ((RelativeLayout.LayoutParams) layoutParams2).rightMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_margin_bottom));
        }
    }

    private static String a(String str) {
        AppUpdateInfo a2 = j.b().a(str);
        return (a2 == null || a2.v == null) ? Constants.STR_EMPTY : a2.v;
    }
}
