package com.tencent.cloud.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.activity.SpecailTopicDetailActivity;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class v extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f2231a;
    final /* synthetic */ SpecialTopicAdapter b;

    v(SpecialTopicAdapter specialTopicAdapter, STInfoV2 sTInfoV2) {
        this.b = specialTopicAdapter;
        this.f2231a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        AppGroupInfo appGroupInfo = (AppGroupInfo) view.getTag(R.id.group_info);
        if (appGroupInfo.g == null || TextUtils.isEmpty(appGroupInfo.g.f1125a)) {
            Intent intent = new Intent(this.b.f2210a, SpecailTopicDetailActivity.class);
            intent.putExtra("com.tencent.assistant.APP_GROUP_INFO", appGroupInfo);
            this.b.f2210a.startActivity(intent);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", appGroupInfo.g);
        b.b(this.b.f2210a, appGroupInfo.g.f1125a, bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.f2231a != null) {
            this.f2231a.actionId = 200;
        }
        return this.f2231a;
    }
}
