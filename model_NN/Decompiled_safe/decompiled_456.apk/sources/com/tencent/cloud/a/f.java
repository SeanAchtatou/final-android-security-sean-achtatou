package com.tencent.cloud.a;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.bm;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.link.b;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public String f2160a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public int f = 0;
    public int g = 0;
    public boolean h = false;

    public static f a(String str, int i) {
        String str2 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Uri parse = Uri.parse(str);
        if (!a(parse, i)) {
            return null;
        }
        String queryParameter = parse.getQueryParameter("actionurl");
        if (!TextUtils.isEmpty(queryParameter)) {
            try {
                str2 = URLDecoder.decode(queryParameter, "UTF-8");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
        f fVar = new f();
        fVar.f2160a = str;
        fVar.b = str2;
        fVar.c = parse.getQueryParameter(SocialConstants.PARAM_COMMENT);
        fVar.e = parse.getQueryParameter("openTitle");
        fVar.f = bm.d(parse.getQueryParameter("openStyle"));
        fVar.d = parse.getQueryParameter("downloadTitle");
        fVar.g = bm.d(parse.getQueryParameter("downloadStyle"));
        fVar.h = bm.d(parse.getQueryParameter("showPopup")) == 1;
        return fVar;
    }

    private static boolean a(Uri uri, int i) {
        if (uri == null) {
            return false;
        }
        String host = uri.getHost();
        if (TextUtils.isEmpty(host) || !host.equalsIgnoreCase("applink")) {
            return false;
        }
        String queryParameter = uri.getQueryParameter("minversion");
        if (TextUtils.isEmpty(queryParameter) || bm.a(queryParameter, -1) <= i) {
            return true;
        }
        return false;
    }

    public Pair<Integer, Integer> a(AppConst.AppState appState) {
        int i = 0;
        if (appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) {
            i = this.g;
        } else if (appState == AppConst.AppState.INSTALLED) {
            i = this.f;
        }
        if (i == 1) {
            return Pair.create(Integer.valueOf((int) R.drawable.state_bg_common_selector), Integer.valueOf((int) R.color.state_normal));
        }
        if (i == 2) {
            return Pair.create(Integer.valueOf((int) R.drawable.state_bg_install_selector), Integer.valueOf((int) R.color.state_install));
        }
        return null;
    }

    public Pair<Integer, Integer> a() {
        if (this.f == 1) {
            return Pair.create(Integer.valueOf((int) R.drawable.state_bg_open), Integer.valueOf((int) R.color.state_open));
        }
        if (this.f == 2) {
            return Pair.create(Integer.valueOf((int) R.drawable.state_bg_install), Integer.valueOf((int) R.color.state_install));
        }
        return null;
    }

    public String b(AppConst.AppState appState) {
        if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && !TextUtils.isEmpty(this.d)) {
            return this.d;
        }
        if (appState != AppConst.AppState.INSTALLED || TextUtils.isEmpty(this.e)) {
            return null;
        }
        return this.e;
    }

    private static boolean a(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (b.a(AstApp.i(), new Intent("android.intent.action.VIEW", Uri.parse(str)))) {
                return true;
            }
        }
        return false;
    }

    public boolean b() {
        if (!TextUtils.isEmpty(this.b) && a(this.b)) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.b));
            if (b.a(AstApp.i(), intent)) {
                intent.setFlags(268435456);
                AstApp.i().startActivity(intent);
                return true;
            }
        }
        return false;
    }

    public AppConst.TwoBtnDialogInfo a(SimpleAppModel simpleAppModel, DownloadInfo downloadInfo, h hVar) {
        if (this.h && m.a().ar()) {
            return new g(simpleAppModel, hVar, this.d);
        }
        return null;
    }
}
