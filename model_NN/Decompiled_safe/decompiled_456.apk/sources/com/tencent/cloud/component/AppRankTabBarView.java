package com.tencent.cloud.component;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.TotalTabLayout;
import com.tencent.assistant.utils.by;
import com.tencent.pangu.component.search.c;

/* compiled from: ProGuard */
public class AppRankTabBarView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    float f2262a;
    boolean b;
    int c;
    boolean d;
    int e;
    int f;
    private final String g;
    private SparseArray<TextView> h;
    /* access modifiers changed from: private */
    public String[] i;
    private int j;
    private int k;
    /* access modifiers changed from: private */
    public c l;
    /* access modifiers changed from: private */
    public Paint m;
    /* access modifiers changed from: private */
    public Paint n;
    /* access modifiers changed from: private */
    public int o;
    private int p;
    private float q;
    private int r;
    private Context s;
    private HorizontalScrollView t;
    private LinearLayout u;

    public AppRankTabBarView(Context context) {
        this(context, null);
    }

    public AppRankTabBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AppRankTabBarView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.g = "TabBarView";
        this.j = -1;
        this.f2262a = 0.0f;
        this.b = true;
        this.c = 0;
        this.d = true;
        this.e = -1;
        this.f = -1;
        this.k = -1;
        this.l = null;
        this.s = context;
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        setGravity(3);
        this.m = new Paint();
        this.m.setAntiAlias(true);
        this.m.setStyle(Paint.Style.FILL);
        this.m.setColor(-16732673);
        this.m.setStrokeWidth((float) b());
        this.n = new Paint();
        this.n.setAntiAlias(true);
        this.n.setStyle(Paint.Style.FILL);
        this.n.setColor(-3750459);
        this.n.setStrokeWidth((float) c());
        setWillNotDraw(false);
        setOrientation(0);
        this.t = new HorizontalScrollView(this.s);
        this.t.setFadingEdgeLength(0);
        this.t.setHorizontalScrollBarEnabled(false);
        this.u = new a(this, this.s);
        this.u.setWillNotDraw(false);
        this.u.setOrientation(0);
        this.u.setGravity(19);
        this.t.addView(this.u);
        addView(this.t, new LinearLayout.LayoutParams(-1, by.a(getContext(), 38.0f)));
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        this.p = d(i2);
        d();
    }

    private int d(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return size;
        }
        return 480;
    }

    private void d() {
        this.r = (int) (((float) this.p) / this.q);
        int childCount = this.u.getChildCount();
        ViewGroup.LayoutParams layoutParams = this.u.getLayoutParams();
        layoutParams.width = this.p * childCount;
        this.u.setLayoutParams(layoutParams);
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = this.u.getChildAt(i2);
            ViewGroup.LayoutParams layoutParams2 = childAt.getLayoutParams();
            layoutParams2.width = this.r;
            childAt.setLayoutParams(layoutParams2);
        }
        invalidate();
    }

    public void a(String[] strArr) {
        float f2 = 4.0f;
        if (strArr == null || strArr.length == 0) {
            this.h = new SparseArray<>(1);
        } else {
            this.h = new SparseArray<>(strArr.length);
        }
        int length = strArr != null ? strArr.length : 0;
        if (((float) length) < 4.0f) {
            f2 = (float) length;
        }
        this.q = f2;
        this.i = strArr;
        e();
    }

    private void e() {
        if (this.i != null && this.i.length > 0) {
            for (int i2 = 0; i2 < this.i.length; i2++) {
                TextView textView = new TextView(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.r, by.a(getContext(), 38.0f));
                textView.setText(this.i[i2]);
                textView.setGravity(17);
                textView.setTextSize(2, 16.0f);
                textView.setTextColor(getContext().getResources().getColor(R.color.second_tab_unselected_color));
                textView.setBackgroundResource(R.drawable.v2_button_background_selector);
                this.u.addView(textView, layoutParams);
                this.h.append(i2, textView);
            }
        }
    }

    private void a(TextView textView) {
        int[] iArr = new int[2];
        textView.getLocationOnScreen(iArr);
        int indexOfValue = this.h.indexOfValue(textView);
        int size = this.h.size();
        int i2 = iArr[0];
        if (this.p - i2 < i2 * 2) {
            this.t.smoothScrollBy(i2, iArr[1]);
        } else if (indexOfValue == 0 || indexOfValue == 1) {
            this.t.smoothScrollTo(0, iArr[1]);
        } else if (indexOfValue == size - 1) {
            this.t.smoothScrollTo(this.p, iArr[1]);
        }
    }

    public void a(int i2, boolean z) {
        if (this.k != i2) {
            this.k = i2;
            int size = this.h.size();
            for (int i3 = 0; i3 < size; i3++) {
                TextView textView = this.h.get(i3);
                if (i3 == i2) {
                    textView.setTextColor(getContext().getResources().getColor(R.color.second_tab_selected_color));
                    a(textView);
                } else {
                    textView.setTextColor(getContext().getResources().getColor(R.color.second_tab_unselected_color));
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.AppRankTabBarView.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.cloud.component.AppRankTabBarView.a(android.content.Context, android.util.AttributeSet):void
      com.tencent.cloud.component.AppRankTabBarView.a(int, float):void
      com.tencent.cloud.component.AppRankTabBarView.a(int, boolean):void */
    public void a(int i2) {
        c(i2);
        a(i2, false);
        b bVar = new b(this, null);
        for (int i3 = 0; i3 < this.i.length; i3++) {
            View b2 = b(i3);
            if (b2 != null) {
                b2.setId(i3 + TotalTabLayout.START_ID_INDEX);
                b2.setOnClickListener(bVar);
            }
        }
    }

    public void a(c cVar) {
        this.l = cVar;
    }

    public View b(int i2) {
        if (i2 >= 0 && this.h.size() >= i2 + 1) {
            return this.h.get(i2);
        }
        return null;
    }

    public int a() {
        if (this.i == null || this.i.length <= 0) {
            return this.p;
        }
        return this.r;
    }

    public int b() {
        return by.a(getContext(), 3.0f);
    }

    public int c() {
        return 1;
    }

    public void c(int i2) {
        b(i2, 0.0f);
    }

    private void b(int i2, float f2) {
        this.o = (int) ((((float) i2) + f2) * ((float) a()));
        this.u.invalidate();
    }

    public void a(int i2, float f2) {
        b(i2, f2);
    }
}
