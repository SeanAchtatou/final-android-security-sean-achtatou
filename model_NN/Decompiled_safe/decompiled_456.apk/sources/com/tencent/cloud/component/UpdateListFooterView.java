package com.tencent.cloud.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class UpdateListFooterView extends TXLoadingLayoutBase {
    private Context c;
    private TextView d;
    private LinearLayout e;
    private int f = 60;

    public UpdateListFooterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UpdateListFooterView(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        this.c = context;
        a(context);
    }

    private void a(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.updatelist_footer_layout, this);
        this.d = (TextView) inflate.findViewById(R.id.updatelist_footer_text);
        this.d.setTag(R.id.tma_st_slot_tag, "08_001");
        this.e = (LinearLayout) inflate.findViewById(R.id.layout_id);
        reset();
    }

    public void a(String str) {
        this.d.setText(str);
    }

    public void a(int i) {
        this.e.setBackgroundResource(i);
        this.f = 50;
    }

    public void a(View.OnClickListener onClickListener) {
        this.e.setOnClickListener(onClickListener);
    }

    public void b(int i) {
        switch (i) {
            case 1:
            case 2:
                setClickable(false);
                setEnabled(false);
                this.d.setVisibility(0);
                this.e.getLayoutParams().height = by.a(this.c, (float) this.f);
                this.e.setVisibility(0);
                setVisibility(0);
                break;
            case 3:
                setClickable(false);
                setEnabled(false);
                this.d.setVisibility(8);
                this.e.setVisibility(8);
                setVisibility(8);
                break;
        }
        requestLayout();
    }

    public void reset() {
    }

    public void pullToRefresh() {
    }

    public void releaseToRefresh() {
    }

    public void refreshing() {
    }

    public void loadFinish(String str) {
    }

    public void onPull(int i) {
    }

    public void hideAllSubViews() {
    }

    public void showAllSubViews() {
    }

    public void setWidth(int i) {
        getLayoutParams().width = i;
        requestLayout();
    }

    public void setHeight(int i) {
        getLayoutParams().height = i;
        requestLayout();
    }

    public int getContentSize() {
        return this.e.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    public void refreshSuc() {
    }

    public void refreshFail(String str) {
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }
}
