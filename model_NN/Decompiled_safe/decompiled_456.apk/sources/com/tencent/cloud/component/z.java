package com.tencent.cloud.component;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ad;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f2298a;

    z(y yVar) {
        this.f2298a = yVar;
    }

    public void run() {
        String str;
        if (this.f2298a.extraMsgView == null || this.f2298a.c.f2296a.p == null) {
            str = null;
        } else if (this.f2298a.c.f2296a.p.isChecked()) {
            str = "1";
        } else {
            str = "0";
        }
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_UPDATE_UPDATEALL, "03_001", STConst.ST_PAGE_UPDATE, STConst.ST_DEFAULT_SLOT, 200);
        sTInfoV2.status = str;
        sTInfoV2.pushInfo = this.f2298a.c.f2296a.C;
        l.a(sTInfoV2);
        int unused = this.f2298a.c.f2296a.B = 0;
        int unused2 = this.f2298a.c.f2296a.A = 0;
        this.f2298a.c.f2296a.n();
        if (this.f2298a.extraMsgView != null && this.f2298a.c.f2296a.p != null && this.f2298a.c.f2296a.p.isChecked()) {
            if (this.f2298a.c.f2296a.q != null) {
                this.f2298a.c.f2296a.p();
                return;
            }
            ad adVar = new ad();
            SimpleAppModel simpleAppModel = new SimpleAppModel();
            simpleAppModel.f938a = this.f2298a.f2297a.e;
            simpleAppModel.c = this.f2298a.f2297a.g;
            simpleAppModel.ac = this.f2298a.f2297a.f;
            adVar.a(simpleAppModel);
            adVar.register(this.f2298a.c.f2296a);
        }
    }
}
