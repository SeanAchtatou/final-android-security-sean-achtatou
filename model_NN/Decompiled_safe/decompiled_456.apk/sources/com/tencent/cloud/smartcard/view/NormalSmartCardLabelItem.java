package com.tencent.cloud.smartcard.view;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.protocol.jce.SmartCardTagInfo;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.utils.by;
import com.tencent.cloud.smartcard.b.c;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardLabelItem extends NormalSmartcardBaseItem {
    private TextView i;
    private LinearLayout l;
    private LinearLayout m;
    private List<Button> n;

    public NormalSmartCardLabelItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        setBackgroundDrawable(null);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1693a, R.layout.smartcard_label_frame_layout, this);
        this.i = (TextView) this.c.findViewById(R.id.card_title);
        this.n = new ArrayList(6);
        this.l = (LinearLayout) this.c.findViewById(R.id.label_line_1);
        this.m = (LinearLayout) this.c.findViewById(R.id.label_line_2);
        int a2 = by.a(this.f1693a, 2.5f);
        int a3 = by.a(this.f1693a, 3.0f);
        int a4 = by.a(this.f1693a, 49.0f);
        if (this.d instanceof c) {
            for (int i2 = 0; i2 < 6; i2++) {
                Button button = new Button(this.f1693a);
                button.setBackgroundResource(R.drawable.bg_card_selector);
                button.setGravity(17);
                button.setSingleLine(true);
                this.n.add(button);
                button.setTextSize(2, 15.0f);
                button.setSingleLine(true);
                button.setPadding(a3, 0, a3, 0);
                button.setEllipsize(TextUtils.TruncateAt.END);
                button.setTextColor(getResources().getColor(R.color.smartcard_tag_font_bg));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, a4, 1.0f);
                layoutParams.setMargins(a2, a2, a2, a2);
                if (i2 >= 3) {
                    this.m.addView(button, layoutParams);
                } else {
                    this.l.addView(button, layoutParams);
                }
            }
        }
        f();
    }

    private void f() {
        if (this.d instanceof c) {
            c cVar = (c) this.d;
            this.i.setText(cVar.l);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < 6) {
                    SmartCardTagInfo smartCardTagInfo = cVar.h().b().get(i3);
                    Button button = this.n.get(i3);
                    button.setText(Html.fromHtml(smartCardTagInfo.f1522a));
                    button.setOnClickListener(new a(this, smartCardTagInfo, i3));
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.d instanceof c) {
            return ((c) this.d).h().c();
        }
        return super.d(i2);
    }
}
