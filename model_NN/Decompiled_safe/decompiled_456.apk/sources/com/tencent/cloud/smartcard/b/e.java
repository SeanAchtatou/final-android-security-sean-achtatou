package com.tencent.cloud.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.ContentAggregationSimpleItem;
import com.tencent.assistant.protocol.jce.SmartCardContentAggregationSimple;
import com.tencent.assistant.smartcard.d.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class e extends a {
    private ArrayList<ContentAggregationSimpleItem> e;

    public boolean a(byte b, JceStruct jceStruct) {
        if (jceStruct == null || !(jceStruct instanceof SmartCardContentAggregationSimple)) {
            return false;
        }
        SmartCardContentAggregationSimple smartCardContentAggregationSimple = (SmartCardContentAggregationSimple) jceStruct;
        a(smartCardContentAggregationSimple.b);
        a(b, smartCardContentAggregationSimple.f1505a);
        this.e = smartCardContentAggregationSimple.c;
        if (this.e == null || this.e.size() < 3) {
            return false;
        }
        return true;
    }

    public ArrayList<ContentAggregationSimpleItem> h() {
        return this.e;
    }

    public List<SimpleAppModel> a() {
        return null;
    }
}
