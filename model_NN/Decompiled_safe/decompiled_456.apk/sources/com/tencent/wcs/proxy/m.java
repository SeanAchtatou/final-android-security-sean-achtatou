package com.tencent.wcs.proxy;

import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.ah;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f4142a;

    m(l lVar) {
        this.f4142a = lVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wcs.proxy.l.a(com.tencent.wcs.proxy.l, boolean):boolean
     arg types: [com.tencent.wcs.proxy.l, int]
     candidates:
      com.tencent.wcs.proxy.l.a(int, com.qq.taf.jce.JceStruct):void
      com.tencent.wcs.proxy.l.a(int, com.tencent.wcs.proxy.b):void
      com.tencent.wcs.proxy.l.a(com.tencent.wcs.proxy.l, boolean):boolean */
    public void run() {
        if (this.f4142a.c != null) {
            this.f4142a.c.g();
        }
        int i = 0;
        while (true) {
            if (i >= 20) {
                break;
            }
            if (this.f4142a.d != null) {
                this.f4142a.d.d();
            }
            if (this.f4142a.d == null || this.f4142a.f == null || !this.f4142a.f.a(this.f4142a.b)) {
                b.a("WirelessProxyService trying to reconnect server , for the " + i + " time, " + " net unavailable");
            } else {
                b.a("WirelessProxyService trying to reconnect server , for the " + i + " time, " + this.f4142a.f.getClass().getSimpleName() + " available");
                this.f4142a.d.c();
            }
            if (!this.f4142a.p()) {
                i++;
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                }
            } else if (this.f4142a.c != null) {
                b.a("WirelessProxyService trying to reconnect server, success !!! retryCount: " + i);
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_WCS_START, true));
                this.f4142a.c.f();
            }
        }
        if (i == 20) {
            b.a("WirelessProxyService trying to reconnect server failed, stop wireless service");
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.CONNECTION_EVENT_WCS_START, false));
            ah.a().post(new n(this));
            if (this.f4142a.c != null) {
                this.f4142a.c.c();
            }
        }
        boolean unused = this.f4142a.h = false;
    }
}
