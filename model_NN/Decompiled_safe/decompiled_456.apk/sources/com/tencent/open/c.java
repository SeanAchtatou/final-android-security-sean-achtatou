package com.tencent.open;

import android.net.Uri;
import android.webkit.WebView;
import com.tencent.open.a.n;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class c {
    /* access modifiers changed from: private */
    public static final String b = (n.d + ".JsBridge");

    /* renamed from: a  reason: collision with root package name */
    protected HashMap<String, e> f3261a = new HashMap<>();

    public void a(e eVar, String str) {
        this.f3261a.put(str, eVar);
    }

    public void a(String str, String str2, List<String> list, d dVar) {
        n.b(b, "getResult---objName = " + str + " methodName = " + str2);
        int size = list.size();
        for (int i = 0; i < size; i++) {
            try {
                list.set(i, URLDecoder.decode(list.get(i), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        e eVar = this.f3261a.get(str);
        if (eVar != null) {
            n.b(b, "call----");
            eVar.call(str2, list, dVar);
            return;
        }
        n.b(b, "not call----objName NOT FIND");
        if (dVar != null) {
            dVar.a();
        }
    }

    public boolean a(WebView webView, String str) {
        n.b(b, "-->canHandleUrl---url = " + str);
        if (str == null || !Uri.parse(str).getScheme().equals("jsbridge")) {
            return false;
        }
        ArrayList arrayList = new ArrayList(Arrays.asList((str + "/#").split("/")));
        if (arrayList.size() < 6) {
            return false;
        }
        List subList = arrayList.subList(4, arrayList.size() - 1);
        d dVar = new d(webView, 4, str);
        webView.getUrl();
        a((String) arrayList.get(2), (String) arrayList.get(3), subList, dVar);
        return true;
    }
}
