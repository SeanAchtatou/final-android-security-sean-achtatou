package com.tencent.open.a;

import android.os.StatFs;
import java.io.File;

/* compiled from: ProGuard */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private File f3243a;
    private long b;
    private long c;

    public File a() {
        return this.f3243a;
    }

    public void a(File file) {
        this.f3243a = file;
    }

    public long b() {
        return this.b;
    }

    public void a(long j) {
        this.b = j;
    }

    public long c() {
        return this.c;
    }

    public void b(long j) {
        this.c = j;
    }

    public static k b(File file) {
        k kVar = new k();
        kVar.a(file);
        StatFs statFs = new StatFs(file.getAbsolutePath());
        long blockSize = (long) statFs.getBlockSize();
        kVar.a(((long) statFs.getBlockCount()) * blockSize);
        kVar.b(((long) statFs.getAvailableBlocks()) * blockSize);
        return kVar;
    }

    public String toString() {
        return String.format("[%s : %d / %d]", a().getAbsolutePath(), Long.valueOf(c()), Long.valueOf(b()));
    }
}
