package com.tencent.open.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: ProGuard */
public class AsynLoadImg {
    /* access modifiers changed from: private */
    public static String c;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f3268a;
    /* access modifiers changed from: private */
    public AsynLoadImgBack b;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public Handler e;
    private Runnable f = new Runnable() {
        public void run() {
            boolean z;
            n.a("AsynLoadImg", "saveFileRunnable:");
            String str = "share_qq_" + Util.encrypt(AsynLoadImg.this.f3268a) + ".jpg";
            String str2 = AsynLoadImg.c + str;
            File file = new File(str2);
            Message obtainMessage = AsynLoadImg.this.e.obtainMessage();
            if (file.exists()) {
                obtainMessage.arg1 = 0;
                obtainMessage.obj = str2;
                n.a("AsynLoadImg", "file exists: time:" + (System.currentTimeMillis() - AsynLoadImg.this.d));
            } else {
                Bitmap bitmap = AsynLoadImg.getbitmap(AsynLoadImg.this.f3268a);
                if (bitmap != null) {
                    z = AsynLoadImg.this.saveFile(bitmap, str);
                } else {
                    n.a("AsynLoadImg", "saveFileRunnable:get bmp fail---");
                    z = false;
                }
                if (z) {
                    obtainMessage.arg1 = 0;
                    obtainMessage.obj = str2;
                } else {
                    obtainMessage.arg1 = 1;
                }
                n.a("AsynLoadImg", "file not exists: download time:" + (System.currentTimeMillis() - AsynLoadImg.this.d));
            }
            AsynLoadImg.this.e.sendMessage(obtainMessage);
        }
    };

    public AsynLoadImg(Activity activity) {
        this.e = new Handler(activity.getMainLooper()) {
            public void handleMessage(Message message) {
                n.a("AsynLoadImg", "handleMessage:" + message.arg1);
                if (message.arg1 == 0) {
                    AsynLoadImg.this.b.saved(message.arg1, (String) message.obj);
                } else {
                    AsynLoadImg.this.b.saved(message.arg1, null);
                }
            }
        };
    }

    public void save(String str, AsynLoadImgBack asynLoadImgBack) {
        n.a("AsynLoadImg", "--save---");
        if (str == null || str.equals(Constants.STR_EMPTY)) {
            asynLoadImgBack.saved(1, null);
        } else if (!Util.hasSDCard()) {
            asynLoadImgBack.saved(2, null);
        } else {
            c = Environment.getExternalStorageDirectory() + "/tmp/";
            this.d = System.currentTimeMillis();
            this.f3268a = str;
            this.b = asynLoadImgBack;
            new Thread(this.f).start();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x006d A[SYNTHETIC, Splitter:B:19:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0079 A[SYNTHETIC, Splitter:B:25:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean saveFile(android.graphics.Bitmap r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.String r0 = com.tencent.open.utils.AsynLoadImg.c
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x005f }
            r2.<init>(r0)     // Catch:{ IOException -> 0x005f }
            boolean r3 = r2.exists()     // Catch:{ IOException -> 0x005f }
            if (r3 != 0) goto L_0x0011
            r2.mkdir()     // Catch:{ IOException -> 0x005f }
        L_0x0011:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x005f }
            r2.<init>()     // Catch:{ IOException -> 0x005f }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ IOException -> 0x005f }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ IOException -> 0x005f }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x005f }
            java.lang.String r2 = "AsynLoadImg"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x005f }
            r3.<init>()     // Catch:{ IOException -> 0x005f }
            java.lang.String r4 = "saveFile:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x005f }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ IOException -> 0x005f }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x005f }
            com.tencent.open.a.n.a(r2, r3)     // Catch:{ IOException -> 0x005f }
            java.io.File r3 = new java.io.File     // Catch:{ IOException -> 0x005f }
            r3.<init>(r0)     // Catch:{ IOException -> 0x005f }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x005f }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005f }
            r0.<init>(r3)     // Catch:{ IOException -> 0x005f }
            r2.<init>(r0)     // Catch:{ IOException -> 0x005f }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ IOException -> 0x0085, all -> 0x0082 }
            r1 = 80
            r6.compress(r0, r1, r2)     // Catch:{ IOException -> 0x0085, all -> 0x0082 }
            r2.flush()     // Catch:{ IOException -> 0x0085, all -> 0x0082 }
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ IOException -> 0x005a }
        L_0x0058:
            r0 = 1
        L_0x0059:
            return r0
        L_0x005a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0058
        L_0x005f:
            r0 = move-exception
        L_0x0060:
            r0.printStackTrace()     // Catch:{ all -> 0x0076 }
            java.lang.String r0 = "AsynLoadImg"
            java.lang.String r2 = "saveFile bmp fail---"
            com.tencent.open.a.n.a(r0, r2)     // Catch:{ all -> 0x0076 }
            r0 = 0
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ IOException -> 0x0071 }
            goto L_0x0059
        L_0x0071:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0059
        L_0x0076:
            r0 = move-exception
        L_0x0077:
            if (r1 == 0) goto L_0x007c
            r1.close()     // Catch:{ IOException -> 0x007d }
        L_0x007c:
            throw r0
        L_0x007d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x007c
        L_0x0082:
            r0 = move-exception
            r1 = r2
            goto L_0x0077
        L_0x0085:
            r0 = move-exception
            r1 = r2
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.AsynLoadImg.saveFile(android.graphics.Bitmap, java.lang.String):boolean");
    }

    public static Bitmap getbitmap(String str) {
        n.a("AsynLoadImg", "getbitmap:" + str);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            n.a("AsynLoadImg", "image download finished." + str);
            return decodeStream;
        } catch (IOException e2) {
            e2.printStackTrace();
            n.a("AsynLoadImg", "getbitmap bmp fail---");
            return null;
        }
    }
}
