package com.tencent.assistant.adapter.a;

import android.content.Context;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.l;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bz;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a implements l {
    public a(Context context, ListView listView, d dVar) {
        a(context, listView, dVar);
    }

    public void a(Context context, ListView listView, d dVar) {
        bz.a("OMAEDC_CTX", context);
        bz.a("OMAEDC_LV", listView);
        bz.a("OMAEDC_ADP", dVar);
    }

    public void a(int i, int i2, ArrayList<SimpleAppModel> arrayList) {
        ListView listView = (ListView) bz.a("OMAEDC_LV");
        Context context = (Context) bz.a("OMAEDC_CTX");
        d dVar = (d) bz.a("OMAEDC_ADP");
        if (listView != null && dVar != null && context != null) {
            int childCount = listView.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                c a2 = dVar.a(listView.getChildAt(i3).getTag());
                TextView c = dVar.c(listView.getChildAt(i3).getTag());
                if (!(a2 == null || a2.f579a == null)) {
                    Integer num = (Integer) a2.f579a.getTag();
                    String a3 = dVar.a();
                    if (num != null && num.intValue() == i) {
                        if (arrayList != null && arrayList.size() > 0) {
                            if (c != null) {
                                c.setVisibility(8);
                            }
                            e.a(context, a2, arrayList, a3);
                            return;
                        } else if (i2 == 0) {
                            e.a(a2);
                            return;
                        } else {
                            if (c != null) {
                                c.setVisibility(8);
                            }
                            e.a(context, a2);
                            ah.a().postDelayed(new b(this, a2, i, c, a2), 3000);
                            return;
                        }
                    }
                }
            }
        }
    }
}
