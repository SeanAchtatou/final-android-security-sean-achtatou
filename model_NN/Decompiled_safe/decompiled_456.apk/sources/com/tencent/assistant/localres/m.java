package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.SimpleAppModel;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* compiled from: ProGuard */
public class m {

    /* renamed from: a  reason: collision with root package name */
    private static Comparator<SimpleAppModel> f844a = new n();
    private static Comparator<LocalApkInfo> b = new p();
    private static Comparator<LocalApkInfo> c = new q();
    private static Comparator<SimpleAppModel> d = new r();
    private static Comparator<LocalApkInfo> e = new s();
    private static Comparator<LocalApkInfo> f = new t();
    private static Comparator<LocalApkInfo> g = new u();
    private static Comparator<LocalApkInfo> h = new v();
    private static Comparator<LocalApkInfo> i = new w();
    private static Comparator<LocalApkInfo> j = new o();

    public static List<LocalApkInfo> a(List<LocalApkInfo> list) {
        if (!(list == null || list.size() == 0)) {
            try {
                Collections.sort(list, b);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return list;
    }

    public static List<LocalApkInfo> b(List<LocalApkInfo> list) {
        if (list == null || list.size() == 0) {
            return list;
        }
        try {
            Collections.sort(list, e);
            return list;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static List<LocalApkInfo> c(List<LocalApkInfo> list) {
        if (!(list == null || list.size() == 0)) {
            try {
                Collections.sort(list, c);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return list;
    }

    public static List<LocalApkInfo> d(List<LocalApkInfo> list) {
        if (!(list == null || list.size() == 0)) {
            try {
                Collections.sort(list, f);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return list;
    }

    public static List<LocalApkInfo> e(List<LocalApkInfo> list) {
        if (!(list == null || list.size() == 0)) {
            try {
                Collections.sort(list, g);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return list;
    }

    public static List<LocalApkInfo> f(List<LocalApkInfo> list) {
        if (!(list == null || list.size() == 0)) {
            try {
                Collections.sort(list, h);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return list;
    }

    public static List<LocalApkInfo> g(List<LocalApkInfo> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, j);
        }
        return list;
    }
}
