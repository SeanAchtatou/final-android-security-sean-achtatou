package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.callback.CallbackHelper;
import java.util.List;

/* compiled from: ProGuard */
class ae implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalApkInfo f811a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ab c;

    ae(ab abVar, LocalApkInfo localApkInfo, boolean z) {
        this.c = abVar;
        this.f811a = localApkInfo;
        this.b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.callback.b.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, com.tencent.assistant.localres.model.LocalApkInfo, boolean, boolean):void
     arg types: [?[OBJECT, ARRAY], com.tencent.assistant.localres.model.LocalApkInfo, boolean, int]
     candidates:
      com.tencent.assistant.localres.callback.b.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean, boolean, int):void
      com.tencent.assistant.localres.callback.b.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, com.tencent.assistant.localres.model.LocalApkInfo, boolean, boolean):void */
    /* renamed from: a */
    public void call(b bVar) {
        bVar.a((List<LocalApkInfo>) null, this.f811a, this.b, false);
    }
}
