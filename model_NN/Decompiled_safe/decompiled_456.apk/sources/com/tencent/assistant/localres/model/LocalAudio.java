package com.tencent.assistant.localres.model;

/* compiled from: ProGuard */
public class LocalAudio extends LocalMedia {
    public boolean alarmFlag;
    public String album;
    public String albumId;
    public String albumKey;
    public String artist;
    public String artistKey;
    public String composer;
    public int createYear;
    public int duration;
    public boolean musicFlag;
    public boolean notificationFlag;
    public boolean podcastFlag;
    public boolean ringtoneFlag;
}
