package com.tencent.assistant.db;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.an;
import com.tencent.assistant.utils.aq;
import java.io.File;
import java.util.List;

/* compiled from: ProGuard */
public class a {
    public static boolean a(String str, JceStruct jceStruct) {
        return a(str, (String) null, jceStruct);
    }

    public static boolean a(String str, String str2, JceStruct jceStruct) {
        if (jceStruct == null) {
            return false;
        }
        return a(str, str2, an.a(jceStruct));
    }

    public static boolean a(String str, String str2, String str3, JceStruct jceStruct) {
        if (jceStruct == null) {
            return false;
        }
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        return a(str, str2, str3, an.a(jceStruct));
    }

    public static boolean a(String str, List<? extends JceStruct> list) {
        if (list == null) {
            return false;
        }
        return a(str, an.a(list));
    }

    private static boolean a(String str, byte[] bArr) {
        return a(str, (String) null, bArr);
    }

    private static boolean a(String str, String str2, byte[] bArr) {
        return FileUtil.write2File(bArr, a(str, str2));
    }

    private static boolean a(String str, String str2, String str3, byte[] bArr) {
        return FileUtil.write2File(bArr, a(str, str2, str3));
    }

    public static JceStruct a(String str, Class<? extends JceStruct> cls) {
        return a(str, (String) null, cls);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0024 A[SYNTHETIC, Splitter:B:14:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002c A[SYNTHETIC, Splitter:B:20:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.qq.taf.jce.JceStruct a(java.lang.String r4, java.lang.String r5, java.lang.Class<? extends com.qq.taf.jce.JceStruct> r6) {
        /*
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0028, all -> 0x001e }
            r1.<init>()     // Catch:{ Exception -> 0x0028, all -> 0x001e }
            java.lang.String r2 = a(r4, r5)     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            boolean r2 = com.tencent.assistant.utils.FileUtil.readFile(r2, r1)     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            if (r2 == 0) goto L_0x0018
            byte[] r2 = r1.toByteArray()     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r2, r6)     // Catch:{ Exception -> 0x003e, all -> 0x003c }
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x003a }
        L_0x001d:
            return r0
        L_0x001e:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0027:
            throw r0
        L_0x0028:
            r1 = move-exception
            r1 = r0
        L_0x002a:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x001d
        L_0x0030:
            r1 = move-exception
        L_0x0031:
            r1.printStackTrace()
            goto L_0x001d
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0027
        L_0x003a:
            r1 = move-exception
            goto L_0x0031
        L_0x003c:
            r0 = move-exception
            goto L_0x0022
        L_0x003e:
            r2 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.a.a(java.lang.String, java.lang.String, java.lang.Class):com.qq.taf.jce.JceStruct");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029 A[SYNTHETIC, Splitter:B:16:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0038 A[SYNTHETIC, Splitter:B:23:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.qq.taf.jce.JceStruct a(java.lang.String r4, java.lang.String r5, java.lang.String r6, java.lang.Class<? extends com.qq.taf.jce.JceStruct> r7) {
        /*
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x001e, all -> 0x0032 }
            r1.<init>()     // Catch:{ Throwable -> 0x001e, all -> 0x0032 }
            java.lang.String r2 = a(r4, r5, r6)     // Catch:{ Throwable -> 0x0045 }
            boolean r2 = com.tencent.assistant.utils.FileUtil.readFile(r2, r1)     // Catch:{ Throwable -> 0x0045 }
            if (r2 == 0) goto L_0x0018
            byte[] r2 = r1.toByteArray()     // Catch:{ Throwable -> 0x0045 }
            com.qq.taf.jce.JceStruct r0 = com.tencent.assistant.utils.an.b(r2, r7)     // Catch:{ Throwable -> 0x0045 }
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x0041 }
        L_0x001d:
            return r0
        L_0x001e:
            r1 = move-exception
            r1 = r0
        L_0x0020:
            com.tencent.assistant.manager.t r2 = com.tencent.assistant.manager.t.a()     // Catch:{ all -> 0x0043 }
            r2.b()     // Catch:{ all -> 0x0043 }
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x002d }
            goto L_0x001d
        L_0x002d:
            r1 = move-exception
        L_0x002e:
            r1.printStackTrace()
            goto L_0x001d
        L_0x0032:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x003b:
            throw r0
        L_0x003c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003b
        L_0x0041:
            r1 = move-exception
            goto L_0x002e
        L_0x0043:
            r0 = move-exception
            goto L_0x0036
        L_0x0045:
            r2 = move-exception
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.a.a(java.lang.String, java.lang.String, java.lang.String, java.lang.Class):com.qq.taf.jce.JceStruct");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0024 A[SYNTHETIC, Splitter:B:14:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002c A[SYNTHETIC, Splitter:B:20:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<? extends com.qq.taf.jce.JceStruct> b(java.lang.String r4, java.lang.Class<? extends com.qq.taf.jce.JceStruct> r5) {
        /*
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0028, all -> 0x001e }
            r1.<init>()     // Catch:{ Exception -> 0x0028, all -> 0x001e }
            java.lang.String r2 = c(r4)     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            boolean r2 = com.tencent.assistant.utils.FileUtil.readFile(r2, r1)     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            if (r2 == 0) goto L_0x0018
            byte[] r2 = r1.toByteArray()     // Catch:{ Exception -> 0x003e, all -> 0x003c }
            java.util.List r0 = com.tencent.assistant.utils.an.a(r2, r5)     // Catch:{ Exception -> 0x003e, all -> 0x003c }
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x003a }
        L_0x001d:
            return r0
        L_0x001e:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0027:
            throw r0
        L_0x0028:
            r1 = move-exception
            r1 = r0
        L_0x002a:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x001d
        L_0x0030:
            r1 = move-exception
        L_0x0031:
            r1.printStackTrace()
            goto L_0x001d
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0027
        L_0x003a:
            r1 = move-exception
            goto L_0x0031
        L_0x003c:
            r0 = move-exception
            goto L_0x0022
        L_0x003e:
            r2 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.a.b(java.lang.String, java.lang.Class):java.util.List");
    }

    public static boolean a(String str) {
        File[] listFiles = new File(FileUtil.getCacheDir()).listFiles(new b(str));
        if (listFiles == null) {
            return true;
        }
        for (File delete : listFiles) {
            delete.delete();
        }
        return true;
    }

    public static boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        File file = new File(c(str));
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public static String c(String str) {
        return a(str, (String) null);
    }

    public static String a(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(FileUtil.getCacheDir());
        stringBuffer.append(File.separator);
        stringBuffer.append(aq.b(str));
        if (!TextUtils.isEmpty(str2)) {
            stringBuffer.append(".");
            stringBuffer.append(str2);
        }
        return stringBuffer.toString();
    }

    public static String a(String str, String str2, String str3) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        stringBuffer.append(File.separator);
        stringBuffer.append(aq.b(str2));
        if (!TextUtils.isEmpty(str3)) {
            stringBuffer.append(".");
            stringBuffer.append(str3);
        }
        return stringBuffer.toString();
    }
}
