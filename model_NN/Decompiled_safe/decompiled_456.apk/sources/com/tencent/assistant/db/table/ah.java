package com.tencent.assistant.db.table;

import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.helper.StatDbHelper;

/* compiled from: ProGuard */
public class ah implements IBaseTable {

    /* renamed from: a  reason: collision with root package name */
    public static final String f748a = ah.class.getSimpleName();

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "useraction_log_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists useraction_log_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, scene INTEGER ,action INTEGER, extraData TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return StatDbHelper.get(AstApp.i());
    }
}
