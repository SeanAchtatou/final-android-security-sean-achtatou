package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.DownloadDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.pangu.download.n;

/* compiled from: ProGuard */
public class m implements IBaseTable {
    public n a(Cursor cursor) {
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        n nVar = new n();
        nVar.f3767a = cursor.getString(cursor.getColumnIndexOrThrow("downloadTicket"));
        nVar.b = cursor.getString(cursor.getColumnIndexOrThrow("packageName"));
        nVar.c = (int) cursor.getLong(cursor.getColumnIndexOrThrow("versionCode"));
        nVar.d = cursor.getInt(cursor.getColumnIndexOrThrow("chanelId"));
        return nVar;
    }

    public void a(ContentValues contentValues, n nVar) {
        if (nVar != null) {
            contentValues.put("downloadTicket", nVar.f3767a);
            contentValues.put("packageName", nVar.b);
            contentValues.put("versionCode", Integer.valueOf(nVar.c));
            contentValues.put("chanelId", Integer.valueOf(nVar.d));
        }
    }

    public int tableVersion() {
        return 2;
    }

    public String tableName() {
        return "download_outer_mapping";
    }

    public String createTableSQL() {
        return "CREATE TABLE IF NOT EXISTS download_outer_mapping (_id INTEGER PRIMARY KEY AUTOINCREMENT,downloadTicket TEXT,packageName TEXT,versionCode INTEGER,chanelId INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
        if (i == 1 && i2 == 2) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS download_outer_mapping (_id INTEGER PRIMARY KEY AUTOINCREMENT,downloadTicket TEXT,packageName TEXT,versionCode INTEGER,chanelId INTEGER);");
            sQLiteDatabase.execSQL("INSERT INTO download_outer_mapping" + "( downloadTicket," + "packageName," + "versionCode," + "chanelId )" + " SELECT " + "downloadTicket, " + "packageName, " + "versionCode, " + "0" + " FROM downloadsinfo;");
        }
    }

    public SqliteHelper getHelper() {
        return DownloadDbHelper.get(AstApp.i());
    }

    public void a(n nVar) {
        if (nVar != null) {
            try {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                if (a(nVar, writableDatabaseWrapper) <= 0) {
                    ContentValues contentValues = new ContentValues();
                    a(contentValues, nVar);
                    writableDatabaseWrapper.insert("download_outer_mapping", null, contentValues);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int a(n nVar, SQLiteDatabaseWrapper sQLiteDatabaseWrapper) {
        if (nVar == null) {
            return -1;
        }
        try {
            ContentValues contentValues = new ContentValues();
            a(contentValues, nVar);
            return sQLiteDatabaseWrapper.update("download_outer_mapping", contentValues, "downloadTicket = ?", new String[]{nVar.f3767a});
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            getHelper().getWritableDatabaseWrapper().delete("download_outer_mapping", "downloadTicket = ?", new String[]{str});
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r2 == null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        if (r2 == null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        r2.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.pangu.download.n> a() {
        /*
            r5 = this;
            r2 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r1 = r5.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r1 = r1.getReadableDatabaseWrapper()
            java.lang.String r3 = "select * from download_outer_mapping order by _id desc"
            r4 = 0
            android.database.Cursor r2 = r1.rawQuery(r3, r4)     // Catch:{ Exception -> 0x0030 }
            if (r2 == 0) goto L_0x002a
            boolean r1 = r2.moveToFirst()     // Catch:{ Exception -> 0x0030 }
            if (r1 == 0) goto L_0x002a
        L_0x001d:
            com.tencent.pangu.download.n r1 = r5.a(r2)     // Catch:{ Exception -> 0x0030 }
            r0.add(r1)     // Catch:{ Exception -> 0x0030 }
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x0030 }
            if (r1 != 0) goto L_0x001d
        L_0x002a:
            if (r2 == 0) goto L_0x002f
        L_0x002c:
            r2.close()
        L_0x002f:
            return r0
        L_0x0030:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0037 }
            if (r2 == 0) goto L_0x002f
            goto L_0x002c
        L_0x0037:
            r0 = move-exception
            if (r2 == 0) goto L_0x003d
            r2.close()
        L_0x003d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.m.a():java.util.List");
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
