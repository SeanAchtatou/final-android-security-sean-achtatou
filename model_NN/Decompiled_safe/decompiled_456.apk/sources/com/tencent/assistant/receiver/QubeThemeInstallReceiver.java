package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class QubeThemeInstallReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        XLog.i("QubeThemeInstallReceiver", "[onReceive] ---> action : " + action);
        if ("com.android.qlauncher.action.THEME_INSTALL_COMPLETE".equals(action)) {
            String stringExtra = intent.getStringExtra("theme_installState");
            XLog.i("QubeThemeInstallReceiver", "[onReceive] ---> ret : " + stringExtra);
            if (!"success".equals(stringExtra) && "fail".equals(stringExtra)) {
            }
        }
    }
}
