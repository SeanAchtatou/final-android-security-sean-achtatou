package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.manager.spaceclean.SpaceScanManager;

/* compiled from: ProGuard */
class l extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f540a;

    l(ApkMgrActivity apkMgrActivity) {
        this.f540a = apkMgrActivity;
    }

    public void onTMAClick(View view) {
        if (this.f540a.y.getFooterViewEnable()) {
            if (this.f540a.B.e()) {
                boolean unused = this.f540a.F = true;
                if (SpaceScanManager.a().e()) {
                    SpaceScanManager.a().a(this.f540a.W);
                    XLog.i("ApkMgrActivity", "startScanRubbish in ApkMgrActivity");
                } else {
                    this.f540a.D();
                }
            }
            this.f540a.z();
            boolean unused2 = this.f540a.D = true;
            this.f540a.y.updateContent(this.f540a.getString(R.string.apkmgr_is_deleting));
            this.f540a.y.setFooterViewEnable(false);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f540a, 200);
        buildSTInfo.slotId = "05_001";
        return buildSTInfo;
    }
}
