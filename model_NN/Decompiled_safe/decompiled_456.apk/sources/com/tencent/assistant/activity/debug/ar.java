package com.tencent.assistant.activity.debug;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class ar implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f491a;
    final /* synthetic */ DActivity b;

    ar(DActivity dActivity, List list) {
        this.b = dActivity;
        this.f491a = list;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        Map map = (Map) this.f491a.get(i);
        PluginProxyActivity.a(this.b, (String) map.get("packagename"), ((Integer) map.get("version")).intValue(), map.get("sa").toString(), ((Integer) map.get("inprocess")).intValue(), null, (String) map.get("launchApplication"));
        this.b.c.setImageDrawable((Drawable) map.get("icon"));
    }
}
