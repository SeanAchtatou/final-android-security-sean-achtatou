package com.tencent.assistant.activity;

import android.os.CountDownTimer;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class cz extends CountDownTimer {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SplashActivity f464a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cz(SplashActivity splashActivity, long j, long j2) {
        super(j, j2);
        this.f464a = splashActivity;
    }

    public void onTick(long j) {
        if (this.f464a.h != null) {
            this.f464a.h.setText(String.format(AstApp.i().getResources().getString(R.string.jump), Long.valueOf(j / 1000)));
        }
    }

    public void onFinish() {
        this.f464a.c();
    }
}
