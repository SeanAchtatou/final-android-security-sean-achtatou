package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class bn extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f425a;

    private bn(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f425a = installedAppManagerActivity;
    }

    /* synthetic */ bn(InstalledAppManagerActivity installedAppManagerActivity, bc bcVar) {
        this(installedAppManagerActivity);
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null) {
            Message obtainMessage = this.f425a.X.obtainMessage();
            obtainMessage.obj = localApkInfo;
            obtainMessage.arg1 = i;
            obtainMessage.what = 10702;
            this.f425a.X.removeMessages(10702);
            this.f425a.X.sendMessage(obtainMessage);
        }
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        if (list != null) {
            int unused = this.f425a.N = list.size();
            int unused2 = this.f425a.O = 0;
            for (LocalApkInfo localApkInfo : list) {
                if (ApkResourceManager.getInstance().getPkgSize(localApkInfo.mPackageName) > 0) {
                    InstalledAppManagerActivity.n(this.f425a);
                }
            }
            Message obtainMessage = this.f425a.X.obtainMessage();
            obtainMessage.obj = list;
            obtainMessage.what = 10701;
            this.f425a.X.removeMessages(10701);
            this.f425a.X.sendMessage(obtainMessage);
        }
    }

    public void onLoadInstalledApkExtraSuccess(List<LocalApkInfo> list) {
        if (this.f425a.X != null) {
            Message obtainMessage = this.f425a.X.obtainMessage();
            obtainMessage.obj = list;
            obtainMessage.what = 10701;
            this.f425a.X.removeMessages(10701);
            this.f425a.X.sendMessage(obtainMessage);
        }
    }

    public void onInstalledAppTimeUpdate(List<LocalApkInfo> list) {
        XLog.d("miles", "onInstalledAppTimeUpdate. size = " + list.size());
        if (this.f425a.X != null) {
            Message obtainMessage = this.f425a.X.obtainMessage();
            obtainMessage.obj = list;
            obtainMessage.what = 10701;
            this.f425a.X.removeMessages(10701);
            this.f425a.X.sendMessage(obtainMessage);
        }
    }

    public void onLoadInstalledApkFail(int i, String str) {
        this.f425a.X.sendEmptyMessage(10703);
    }

    public void onGetPkgSizeFinish(LocalApkInfo localApkInfo) {
        InstalledAppManagerActivity.n(this.f425a);
        Iterator it = this.f425a.u.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            LocalApkInfo localApkInfo2 = (LocalApkInfo) it.next();
            if (localApkInfo.mPackageName.equals(localApkInfo2.mPackageName)) {
                localApkInfo2.occupySize = localApkInfo.occupySize;
                break;
            }
        }
        if (this.f425a.N == this.f425a.O) {
            XLog.d("miles", "InstalledAppManagerActivity >> totalAppCount = sizeAlreadyGetAppCount =" + this.f425a.O);
            Message obtainMessage = this.f425a.X.obtainMessage();
            obtainMessage.obj = localApkInfo;
            obtainMessage.arg1 = 4;
            obtainMessage.what = 10702;
            this.f425a.X.removeMessages(10702);
            this.f425a.X.sendMessage(obtainMessage);
        }
    }
}
