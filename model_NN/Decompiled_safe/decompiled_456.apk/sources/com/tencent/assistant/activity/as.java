package com.tencent.assistant.activity;

import com.tencent.pangu.manager.SelfUpdateManager;
import com.tencent.pangu.module.a.m;

/* compiled from: ProGuard */
class as extends m {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f403a;

    as(BaseActivity baseActivity) {
        this.f403a = baseActivity;
    }

    public void onCheckSelfUpdateFinish(int i, int i2, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo) {
        if (SelfUpdateManager.a().j()) {
            this.f403a.u();
        } else {
            SelfUpdateManager.a().a(SelfUpdateManager.SelfUpdateType.SILENT);
        }
    }
}
