package com.tencent.assistant.activity.pictureprocessor;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/* compiled from: ProGuard */
public class DrawCallbackProgressBar extends ProgressBar {

    /* renamed from: a  reason: collision with root package name */
    private c f546a;

    public DrawCallbackProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public DrawCallbackProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DrawCallbackProgressBar(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        if (this.f546a != null) {
            this.f546a.a();
        }
        super.onDraw(canvas);
    }

    public synchronized void a(c cVar) {
        this.f546a = cVar;
    }
}
