package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class as implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f492a;

    as(DActivity dActivity) {
        this.f492a = dActivity;
    }

    public void onClick(View view) {
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.b = 18042262;
        simpleAppModel.f938a = 7960;
        simpleAppModel.Q = 0;
        simpleAppModel.R = 0;
        simpleAppModel.ac = Constants.STR_EMPTY;
        simpleAppModel.n = 1397799450;
        simpleAppModel.i = "http://dd.myapp.com/16891/20B585EA18AFCFECC82A281456287A8D.apk?fsname=com%2Edianping%2Ev1%5F6%2E5%2E2%5F652.apk";
        simpleAppModel.d = "大众点评";
        simpleAppModel.q = 0.0d;
        simpleAppModel.p = 10000;
        simpleAppModel.X = "微信登陆bug修复";
        simpleAppModel.k = 10348625;
        simpleAppModel.ad = 0;
        simpleAppModel.c = "com.dianping.v1";
        simpleAppModel.g = 652;
        simpleAppModel.f = "6.5.2";
        DownloadInfo createDownloadInfo = DownloadInfo.createDownloadInfo(simpleAppModel, new StatInfo());
        a.a().a(createDownloadInfo);
        if (createDownloadInfo != null) {
            createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
            createDownloadInfo.uiType = SimpleDownloadInfo.UIType.WISE_APP_UPDATE;
        }
        DownloadProxy.a().d(createDownloadInfo);
    }
}
