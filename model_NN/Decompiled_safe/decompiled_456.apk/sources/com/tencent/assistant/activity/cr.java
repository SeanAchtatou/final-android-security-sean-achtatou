package com.tencent.assistant.activity;

import android.os.IBinder;
import android.os.Message;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.spaceclean.ac;
import com.tencent.tmsecurelite.optimize.f;

/* compiled from: ProGuard */
class cr extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f456a;

    cr(SpaceCleanActivity spaceCleanActivity) {
        this.f456a = spaceCleanActivity;
    }

    public IBinder asBinder() {
        return null;
    }

    public void c() {
        XLog.d("miles", "SpaceCleanActivity onScanStarted.");
        long unused = this.f456a.S = System.currentTimeMillis();
        ac.a().b();
    }

    public void a(int i) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.SpaceCleanActivity.e(com.tencent.assistant.activity.SpaceCleanActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.SpaceCleanActivity, int]
     candidates:
      com.tencent.assistant.activity.SpaceCleanActivity.e(com.tencent.assistant.activity.SpaceCleanActivity, long):long
      com.tencent.assistant.activity.SpaceCleanActivity.e(com.tencent.assistant.activity.SpaceCleanActivity, boolean):boolean */
    public void b() {
        XLog.d("miles", "SpaceCleanActivity onScanFinished.");
        if (this.f456a.Y.hasMessages(25)) {
            this.f456a.Y.removeMessages(25);
        }
        Message obtain = Message.obtain(this.f456a.Y, 25);
        obtain.obj = Long.valueOf(this.f456a.I);
        obtain.sendToTarget();
        XLog.d("miles", "-----------扫描结束----------");
        if (this.f456a.I == 0) {
            boolean unused = this.f456a.y = true;
        }
        long unused2 = this.f456a.T = System.currentTimeMillis();
        this.f456a.D();
        ac.a().c();
    }

    public void a() {
        XLog.d("miles", "SpaceCleanActivity onScanCanceled");
        ac.a().c();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009e, code lost:
        if (android.text.TextUtils.isEmpty(r5.trim()) != false) goto L_0x00a0;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x002b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r8, com.tencent.tmsecurelite.commom.DataEntity r9) {
        /*
            r7 = this;
            com.tencent.nucleus.manager.spaceclean.ac r0 = com.tencent.nucleus.manager.spaceclean.ac.a()
            r0.a(r8, r9)
            java.lang.String r0 = "rubbish.suggest"
            boolean r1 = r9.getBoolean(r0)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r0 = "app.pkg"
            java.lang.String r2 = r9.getString(r0)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r0 = "rubbish.size"
            long r3 = r9.getLong(r0)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r0 = "rubbish.desc"
            java.lang.String r5 = r9.getString(r0)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r0 = "rubbish.path.array"
            org.json.JSONArray r6 = r9.getJSONArray(r0)     // Catch:{ Exception -> 0x0088 }
            switch(r8) {
                case 1: goto L_0x0082;
                case 2: goto L_0x0094;
                case 3: goto L_0x00c0;
                case 4: goto L_0x00c9;
                default: goto L_0x0028;
            }     // Catch:{ Exception -> 0x0088 }
        L_0x0028:
            com.tencent.assistant.activity.SpaceCleanActivity r2 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            monitor-enter(r2)     // Catch:{ Exception -> 0x0088 }
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ all -> 0x00e7 }
            com.tencent.assistant.activity.SpaceCleanActivity.h(r0, r3)     // Catch:{ all -> 0x00e7 }
            monitor-exit(r2)     // Catch:{ all -> 0x00e7 }
            if (r1 == 0) goto L_0x0038
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            com.tencent.assistant.activity.SpaceCleanActivity.i(r0, r3)     // Catch:{ Exception -> 0x0088 }
        L_0x0038:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            com.tencent.assistant.activity.SpaceCleanActivity.w(r0)     // Catch:{ Exception -> 0x0088 }
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            int r0 = r0.H     // Catch:{ Exception -> 0x0088 }
            int r0 = r0 % 4
            if (r0 != 0) goto L_0x0081
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            r1 = 0
            int unused = r0.H = r1     // Catch:{ Exception -> 0x0088 }
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            android.os.Handler r0 = r0.Y     // Catch:{ Exception -> 0x0088 }
            r1 = 24
            boolean r0 = r0.hasMessages(r1)     // Catch:{ Exception -> 0x0088 }
            if (r0 == 0) goto L_0x0066
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            android.os.Handler r0 = r0.Y     // Catch:{ Exception -> 0x0088 }
            r1 = 24
            r0.removeMessages(r1)     // Catch:{ Exception -> 0x0088 }
        L_0x0066:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            android.os.Handler r0 = r0.Y     // Catch:{ Exception -> 0x0088 }
            r1 = 24
            android.os.Message r0 = android.os.Message.obtain(r0, r1)     // Catch:{ Exception -> 0x0088 }
            com.tencent.assistant.activity.SpaceCleanActivity r1 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            long r1 = r1.I     // Catch:{ Exception -> 0x0088 }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x0088 }
            r0.obj = r1     // Catch:{ Exception -> 0x0088 }
            r0.sendToTarget()     // Catch:{ Exception -> 0x0088 }
        L_0x0081:
            return
        L_0x0082:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            r0.a(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0028
        L_0x0088:
            r0 = move-exception
            java.lang.String r1 = "miles"
            java.lang.String r2 = "SpaceCleanActivty. onRubbishFound Exception."
            com.tencent.assistant.utils.XLog.d(r1, r2)
            r0.printStackTrace()
            goto L_0x0081
        L_0x0094:
            if (r5 == 0) goto L_0x00a0
            java.lang.String r0 = r5.trim()     // Catch:{ Exception -> 0x0088 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0088 }
            if (r0 == 0) goto L_0x00a2
        L_0x00a0:
            java.lang.String r5 = "未安装"
        L_0x00a2:
            if (r1 != 0) goto L_0x00b7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0088 }
            r0.<init>()     // Catch:{ Exception -> 0x0088 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "(建议保留)"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x0088 }
        L_0x00b7:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "安装包"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0028
        L_0x00c0:
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "垃圾文件"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0028
        L_0x00c9:
            if (r5 == 0) goto L_0x00dc
            java.lang.String r0 = "破损安装包"
            boolean r0 = r5.contains(r0)     // Catch:{ Exception -> 0x0088 }
            if (r0 == 0) goto L_0x00dc
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "安装包"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0028
        L_0x00dc:
            if (r5 == 0) goto L_0x0028
            com.tencent.assistant.activity.SpaceCleanActivity r0 = r7.f456a     // Catch:{ Exception -> 0x0088 }
            java.lang.String r2 = "垃圾文件"
            r0.b(r1, r2, r3, r5, r6)     // Catch:{ Exception -> 0x0088 }
            goto L_0x0028
        L_0x00e7:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00e7 }
            throw r0     // Catch:{ Exception -> 0x0088 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.activity.cr.a(int, com.tencent.tmsecurelite.commom.DataEntity):void");
    }
}
