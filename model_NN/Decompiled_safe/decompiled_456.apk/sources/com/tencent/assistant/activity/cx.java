package com.tencent.assistant.activity;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.DownloadProgressButton;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class cx extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f462a;
    final /* synthetic */ SplashActivity b;

    cx(SplashActivity splashActivity, String str) {
        this.b = splashActivity;
        this.f462a = str;
    }

    public void onTMAClick(View view) {
        if (!TextUtils.isEmpty(this.f462a)) {
            b.a(this.b, this.f462a);
            if (this.b.j != null) {
                this.b.j.cancel();
            }
        }
    }

    public STInfoV2 getStInfo() {
        return new STInfoV2(STConst.ST_PAGE_SPLASH_URL, DownloadProgressButton.TMA_ST_NAVBAR_DOWNLOAD_TAG, 2000, STConst.ST_DEFAULT_SLOT, 200);
    }
}
