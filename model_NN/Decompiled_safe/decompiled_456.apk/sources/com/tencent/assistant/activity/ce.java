package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.d;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ce extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f443a;

    ce(SpaceCleanActivity spaceCleanActivity) {
        this.f443a = spaceCleanActivity;
    }

    public void a(int i, ArrayList<DataEntity> arrayList) {
        XLog.d("miles", "onArrayResultGot.. paramAnonymousInt = " + i + " DataEntity = " + arrayList.toString());
    }

    public void a(int i, DataEntity dataEntity) {
        XLog.d("miles", "onResultGot.. paramAnonymousInt = " + i + " DataEntity = " + dataEntity.toString());
        this.f443a.Y.removeMessages(30);
        Message.obtain(this.f443a.Y, 27).sendToTarget();
        long unused = this.f443a.V = System.currentTimeMillis();
        this.f443a.a("SpaceCleanDoClear", i == 0, i);
    }
}
