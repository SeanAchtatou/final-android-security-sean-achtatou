package com.tencent.assistant.plugin.mgr;

import com.tencent.assistant.plugin.GetPluginListEngine;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import java.util.List;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1100a;

    f(c cVar) {
        this.f1100a = cVar;
    }

    public void run() {
        List<PluginDownloadInfo> list = GetPluginListEngine.getInstance().getList();
        if (list == null || list.size() == 0) {
            GetPluginListEngine.getInstance().refreshData(0);
        } else {
            this.f1100a.b(list);
        }
    }
}
