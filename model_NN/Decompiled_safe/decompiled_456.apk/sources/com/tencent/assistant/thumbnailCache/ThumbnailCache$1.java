package com.tencent.assistant.thumbnailCache;

import android.graphics.Bitmap;
import java.util.LinkedHashMap;
import java.util.Map;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
/* compiled from: ProGuard */
class ThumbnailCache$1 extends LinkedHashMap<String, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1786a;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.util.LinkedHashMap.<init>(int, float, boolean):void in method: com.tencent.assistant.thumbnailCache.ThumbnailCache$1.<init>(com.tencent.assistant.thumbnailCache.b, int, float, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.util.LinkedHashMap.<init>(int, float, boolean):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    ThumbnailCache$1(com.tencent.assistant.thumbnailCache.b r1, int r2, float r3, boolean r4) {
        /*
            r0 = this;
            r0.f1786a = r1
            r0.<init>(r2, r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.ThumbnailCache$1.<init>(com.tencent.assistant.thumbnailCache.b, int, float, boolean):void");
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<String, Bitmap> entry) {
        if (this.f1786a.m / 1024 > this.f1786a.n) {
            if (!(entry == null || entry.getValue() == null)) {
                b.a(this.f1786a, (long) (entry.getValue().getHeight() * entry.getValue().getRowBytes()));
            }
            return true;
        } else if (size() <= this.f1786a.e) {
            return false;
        } else {
            synchronized (this.f1786a.b) {
                this.f1786a.b.put(entry.getKey(), new a(entry.getKey(), entry.getValue(), this.f1786a.c));
            }
            return true;
        }
    }
}
