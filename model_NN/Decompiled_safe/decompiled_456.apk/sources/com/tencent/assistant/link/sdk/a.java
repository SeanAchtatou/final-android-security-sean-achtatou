package com.tencent.assistant.link.sdk;

import android.content.Context;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static int f794a = 0;
    public static int b = 1;
    public static int c = 2;

    public static void a(Context context, String str, String str2, String str3) {
        com.tencent.assistant.link.sdk.c.a aVar = new com.tencent.assistant.link.sdk.c.a(context);
        com.tencent.assistant.link.sdk.b.a aVar2 = new com.tencent.assistant.link.sdk.b.a(str, str2, str3);
        aVar2.d = f794a;
        aVar.a(aVar2);
    }

    public static void a(Context context, String str, String str2) {
        new com.tencent.assistant.link.sdk.c.a(context).a(str, str2);
    }

    public static void a(Context context, String str, String str2, int i) {
        new com.tencent.assistant.link.sdk.c.a(context).a(str, Constants.STR_EMPTY, i);
    }
}
