package com.tencent.assistant.a;

import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class a {
    public static String A = "code";
    public static String B = "mmtoken";
    public static String C = "channelid";
    public static String D = "selflink";
    public static String E = "sdcardlink";
    public static String F = "encryptdata";
    public static String G = "is_from_push_click";
    public static String H = "scrollgroup";
    public static String I = "sdkid";
    public static String J = "gray_versioncode";
    public static String K = "downl_url";
    public static String L = "downl_biz_id";
    public static String M = "down_ticket";
    public static String N = "page_call_from_install";
    public static String O = "page_call_from_download";
    public static String P = "file_size_to_download";
    public static String Q = "sdk_update_app_list";
    public static String R = "push_update_app_list";
    public static String S = "update_mode";
    public static String T = "update_from";
    public static String U = "column";
    public static String V = "replyId";
    public static String W = "recommendId";
    public static String X = "search_type";
    public static String Y = "search_hint";
    public static String Z = "search_word";

    /* renamed from: a  reason: collision with root package name */
    public static String f382a = "appid";
    public static String aa = "search_hot_word_category";
    public static String ab = "search_scroll_result_tab";
    public static String ac = "traceid";
    public static String ad = "ipc_stat_extra";
    public static String ae = "call_pkg_name";
    public static String af = "down_uri";
    public static String ag = "request_type";
    public static String b = "apkid";
    public static String c = "pname";
    public static String d = "appname";
    public static String e = "oplist";
    public static String f = "uuid";
    public static String g = "verifytype";
    public static String h = "versioncode";
    public static String i = "sngappid";
    public static String j = "via";
    public static String k = "actiontype";
    public static String l = "taskid";
    public static String m = "hostappid";
    public static String n = "hostpname";
    public static String o = "hostversioncode";
    public static String p = SocialConstants.PARAM_URL;
    public static String q = "openid";
    public static String r = Constants.PARAM_ACCESS_TOKEN;
    public static String s = "actionflag";
    public static String t = "from_action";
    public static String u = "from_sdk";
    public static String v = "has_identity";
    public static String w = "from_notification";
    public static String x = "cfr";
    public static String y = "uin";
    public static String z = "uintype";
}
