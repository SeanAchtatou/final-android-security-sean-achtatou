package com.tencent.assistant.protocol.a.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.plugin.component.PluginRequestWrapper;
import com.tencent.assistant.protocol.a.b;

/* compiled from: ProGuard */
public class d extends com.tencent.assistant.protocol.a.d {
    public boolean a(JceStruct jceStruct) {
        if (jceStruct == null) {
            return false;
        }
        return jceStruct instanceof PluginRequestWrapper;
    }

    public int b(JceStruct jceStruct) {
        if (jceStruct == null) {
            return -1;
        }
        return ((PluginRequestWrapper) jceStruct).getCmdId();
    }

    public JceStruct c(JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        return ((PluginRequestWrapper) jceStruct).getRequestJceStruct();
    }

    public b a() {
        return new e();
    }
}
