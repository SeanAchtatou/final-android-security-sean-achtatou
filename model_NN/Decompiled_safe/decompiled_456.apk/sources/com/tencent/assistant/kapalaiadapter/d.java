package com.tencent.assistant.kapalaiadapter;

import android.os.Build;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f790a = true;
    public static boolean b = true;
    public static boolean c = true;
    public static boolean d = true;
    public static int[] e = new int[2];
    public static boolean f = true;
    public static int[] g = new int[2];
    public static int[] h = {-1, 1};
    public static int[] i = {-1, 1};
    public static int[] j = {-1, 0, 1, 2};

    static {
        String lowerCase = Build.MODEL.toLowerCase();
        String lowerCase2 = Build.MANUFACTURER.toLowerCase();
        if (lowerCase2.indexOf("htc") >= 0) {
            e.a().b(lowerCase);
        } else if (lowerCase2.indexOf("samsung") >= 0 || lowerCase2.indexOf("samsng") >= 0) {
            e.a().a(lowerCase);
        } else if (lowerCase2.indexOf("motorola") >= 0) {
            e.a().c(lowerCase);
        } else if (lowerCase2.indexOf("huawei") >= 0) {
            e.a().d(lowerCase);
        } else if (lowerCase2.indexOf("hisense") >= 0) {
            e.a().f(lowerCase);
        } else if (lowerCase2.indexOf("xiaomi") >= 0) {
            e.a().g(lowerCase);
        } else if (lowerCase2.indexOf("zte") >= 0) {
            e.a().e(lowerCase);
        } else if (lowerCase2.indexOf("meizu") >= 0) {
            e.a().h(lowerCase);
        } else if (lowerCase2.indexOf("alps") >= 0) {
            e.a().i(lowerCase);
        } else if (lowerCase2.indexOf("oppo") >= 0) {
            e.a().o(lowerCase);
        } else if (lowerCase2.indexOf("k-touch") >= 0) {
            e.a().j(lowerCase);
        } else if (lowerCase2.indexOf("yulong") >= 0) {
            e.a().q(lowerCase);
        } else if (lowerCase2.indexOf("coolpad") >= 0) {
            e.a().k(lowerCase);
        } else if (lowerCase2.indexOf("lenovo") >= 0) {
            e.a().l(lowerCase);
        } else if (lowerCase2.indexOf("bbk") >= 0) {
            e.a().m(lowerCase);
        } else if (lowerCase2.indexOf("gionee") >= 0) {
            e.a().p(lowerCase);
        } else if (lowerCase2.indexOf("sony ericsson") >= 0) {
            e.a().n(lowerCase);
        }
    }
}
