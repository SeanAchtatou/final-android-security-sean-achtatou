package com.tencent.assistant.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.assistant.db.table.af;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.model.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.am;
import com.tencent.assistant.utils.aq;
import com.tencent.assistant.utils.t;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class SplashManager implements p {

    /* renamed from: a  reason: collision with root package name */
    private static SplashManager f852a;
    /* access modifiers changed from: private */
    public af b = new af();
    private k c;
    private final int d = 1024;
    private Bitmap e = null;
    private Bitmap f = null;

    /* compiled from: ProGuard */
    enum ImageType {
        SPLASH,
        BUTTON_BG
    }

    private SplashManager() {
    }

    public static synchronized SplashManager a() {
        SplashManager splashManager;
        synchronized (SplashManager.class) {
            if (f852a == null) {
                f852a = new SplashManager();
            }
            splashManager = f852a;
        }
        return splashManager;
    }

    public k b() {
        Exception e2;
        k kVar;
        try {
            List<k> a2 = this.b.a(true);
            if (a2 != null && !a2.isEmpty()) {
                Iterator<k> it = a2.iterator();
                while (it.hasNext()) {
                    kVar = it.next();
                    long currentTimeMillis = System.currentTimeMillis() / 1000;
                    if (kVar.d() <= currentTimeMillis && kVar.e() >= currentTimeMillis) {
                        if (kVar.g() <= 0 || kVar.h() < kVar.g()) {
                            String j = kVar.j();
                            if (TextUtils.isEmpty(j)) {
                                j = a(kVar.i());
                            }
                            if (c(j)) {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = false;
                                options.inSampleSize = 1;
                                try {
                                    this.e = am.a(j, options, 0);
                                } catch (Exception e3) {
                                    XLog.e("splashInfo", e3.toString());
                                }
                            }
                            if (kVar.r() == 1) {
                                String v = kVar.v();
                                if (c(v)) {
                                    BitmapFactory.Options options2 = new BitmapFactory.Options();
                                    options2.inJustDecodeBounds = false;
                                    options2.inSampleSize = 1;
                                    try {
                                        this.f = am.a(v, options2, 0);
                                    } catch (Exception e4) {
                                        XLog.e("splashInfo", e4.toString());
                                    }
                                }
                            }
                            try {
                                this.c = kVar;
                                return kVar;
                            } catch (Exception e5) {
                                e2 = e5;
                            }
                        }
                    }
                }
            }
            return null;
        } catch (Exception e6) {
            Exception exc = e6;
            kVar = null;
            e2 = exc;
        }
        XLog.e("splashInfo", e2.toString());
        return kVar;
    }

    public Bitmap c() {
        return this.e;
    }

    public Bitmap d() {
        return this.f;
    }

    public void a(k kVar) {
        TemporaryThreadManager.get().start(new r(this, kVar));
    }

    public void a(List<k> list) {
        b(list);
    }

    private void b(List<k> list) {
        TemporaryThreadManager.get().start(new s(this, list));
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0081 A[SYNTHETIC, Splitter:B:13:0x0081] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008a A[SYNTHETIC, Splitter:B:18:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.graphics.Bitmap r8, com.tencent.assistant.model.k r9) {
        /*
            r7 = this;
            r5 = 1000(0x3e8, double:4.94E-321)
            r2 = 0
            android.graphics.Bitmap r0 = r7.a(r8)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            android.graphics.Bitmap r0 = r7.b(r0)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            r1.<init>()     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.String r3 = "dpi="
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            int r3 = r0.getWidth()     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.String r3 = "*"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            int r3 = r0.getHeight()     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.String r3 = ";time="
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            long r3 = r9.d()     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            long r3 = r3 * r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.String r3 = com.tencent.assistant.utils.bo.a(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.String r3 = "~"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            long r3 = r9.e()     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            long r3 = r3 * r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.String r3 = com.tencent.assistant.utils.bo.a(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            r9.e(r1)     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            r1.<init>()     // Catch:{ Exception -> 0x007a, all -> 0x0087 }
            android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x0093 }
            r3 = 100
            r0.compress(r2, r3, r1)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r0 = r9.j()     // Catch:{ Exception -> 0x0093 }
            com.tencent.assistant.utils.FileUtil.write2File(r1, r0)     // Catch:{ Exception -> 0x0093 }
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0079:
            return
        L_0x007a:
            r0 = move-exception
            r1 = r2
        L_0x007c:
            r0.printStackTrace()     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ IOException -> 0x0085 }
            goto L_0x0079
        L_0x0085:
            r0 = move-exception
            goto L_0x0079
        L_0x0087:
            r0 = move-exception
        L_0x0088:
            if (r2 == 0) goto L_0x008d
            r2.close()     // Catch:{ IOException -> 0x008e }
        L_0x008d:
            throw r0
        L_0x008e:
            r1 = move-exception
            goto L_0x008d
        L_0x0090:
            r0 = move-exception
            r2 = r1
            goto L_0x0088
        L_0x0093:
            r0 = move-exception
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.manager.SplashManager.a(android.graphics.Bitmap, com.tencent.assistant.model.k):void");
    }

    private Bitmap a(Bitmap bitmap) {
        Bitmap bitmap2 = null;
        if (bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                if (byteArrayOutputStream.toByteArray().length / 1024 > 1024) {
                    byteArrayOutputStream.reset();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = false;
                    bitmap = BitmapFactory.decodeStream(byteArrayInputStream, null, options);
                }
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                int i = t.b;
                bitmap2 = a(bitmap, i, Math.round((((float) height) / ((float) width)) * ((float) i)));
                if (byteArrayOutputStream != null) {
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (Throwable th) {
                if (byteArrayOutputStream != null) {
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e3) {
                    }
                }
                throw th;
            }
        }
        return bitmap2;
    }

    private Bitmap a(Bitmap bitmap, int i, int i2) {
        if (bitmap == null) {
            return null;
        }
        return Bitmap.createScaledBitmap(bitmap, i, i2, true);
    }

    private Bitmap b(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int i = t.c;
        if (height > i) {
            return Bitmap.createBitmap(bitmap, 0, (height - i) / 2, width, i);
        }
        return bitmap;
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        return FileUtil.getPicDir() + File.separator + b(str);
    }

    private String b(String str) {
        return aq.b(str);
    }

    /* access modifiers changed from: private */
    public boolean c(String str) {
        return FileUtil.getFileLastModified(str) > 0;
    }

    public void thumbnailRequestCompleted(o oVar) {
    }

    public void thumbnailRequestFailed(o oVar) {
    }

    /* access modifiers changed from: private */
    public void a(List<String> list, ImageType imageType) {
        k kVar;
        k kVar2;
        String str;
        if (list == null || list.isEmpty()) {
            XLog.d("splashInfo", "list is empty ");
            return;
        }
        int i = 0;
        String str2 = null;
        while (i < list.size()) {
            String str3 = list.get(i);
            if (imageType == ImageType.SPLASH) {
                kVar = this.b.a(str3);
                if (kVar != null) {
                    kVar2 = kVar;
                    str = kVar.j();
                }
                kVar2 = kVar;
                str = str2;
            } else if (imageType == ImageType.BUTTON_BG) {
                kVar = this.b.b(str3);
                if (kVar != null) {
                    kVar2 = kVar;
                    str = kVar.v();
                }
                kVar2 = kVar;
                str = str2;
            } else {
                XLog.e("splashInfo", "imageType error");
                return;
            }
            if (kVar2 == null) {
                XLog.d("splashInfo", "splashInfo is null ");
            } else {
                for (int i2 = 0; i2 < 5; i2++) {
                    Bitmap a2 = com.tencent.assistant.thumbnailCache.k.b().a(str3, 2, this);
                    XLog.d("splashInfo", "imageType = " + imageType + ", 拉取成功？  = " + (a2 != null ? "成功" : "失败"));
                    boolean z = true;
                    if (a2 == null || a2.isRecycled()) {
                        long j = (long) ((i2 + 1) * EventDispatcherEnum.CACHE_EVENT_START);
                        try {
                            XLog.d("splashInfo", "imageType = " + imageType + " 图片," + "休眠 " + j + "毫秒后获取");
                            Thread.sleep(j);
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                            XLog.e("splashInfo", e2.toString());
                        }
                    } else {
                        try {
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            a2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            if (TextUtils.isEmpty(str)) {
                                Log.e("splashInfo", "error, saveLocalPath is empty");
                            } else {
                                FileUtil.write2File(byteArrayOutputStream, str);
                                if (byteArrayOutputStream != null) {
                                    byteArrayOutputStream.close();
                                }
                                if (imageType == ImageType.SPLASH) {
                                    a(a2, kVar2);
                                }
                                this.b.b(kVar2);
                                XLog.d("splashInfo", "width = " + a2.getWidth() + ", height = " + a2.getHeight() + ", splashInfo = " + kVar2 + ", ImageSavePath = " + a(str3));
                                if (z) {
                                    break;
                                }
                            }
                        } catch (IOException e3) {
                            e3.printStackTrace();
                            z = false;
                        }
                    }
                }
            }
            i++;
            str2 = str;
        }
    }
}
