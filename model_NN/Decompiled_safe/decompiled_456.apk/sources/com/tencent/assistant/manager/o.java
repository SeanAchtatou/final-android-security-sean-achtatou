package com.tencent.assistant.manager;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import java.util.HashMap;

/* compiled from: ProGuard */
public class o implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static o f901a;
    private HashMap<Long, Boolean> b = new HashMap<>();
    private HashMap<Long, Integer> c = new HashMap<>();

    public o() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIEBUMBER_SUCC, this);
    }

    public static synchronized o a() {
        o oVar;
        synchronized (o.class) {
            if (f901a == null) {
                f901a = new o();
            }
            oVar = f901a;
        }
        return oVar;
    }

    public void a(Long l, int i) {
        if (!this.c.containsKey(l)) {
            this.c.put(l, Integer.valueOf(i));
        } else if (i > this.c.get(l).intValue()) {
            this.c.put(l, Integer.valueOf(i));
            this.b.remove(l);
        }
    }

    public void a(long j) {
        this.b.put(Long.valueOf(j), Boolean.TRUE);
    }

    public boolean b(long j) {
        if (this.b.containsKey(Long.valueOf(j))) {
            return this.b.get(Long.valueOf(j)).booleanValue();
        }
        return false;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_VIEBUMBER_SUCC:
                if (message.obj != null && (message.obj instanceof Long)) {
                    a().a(((Long) message.obj).longValue());
                    return;
                }
                return;
            default:
                return;
        }
    }
}
