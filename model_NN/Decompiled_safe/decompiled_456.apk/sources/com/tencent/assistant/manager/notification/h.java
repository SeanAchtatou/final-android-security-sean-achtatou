package com.tencent.assistant.manager.notification;

import android.os.Build;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static h f883a;
    /* access modifiers changed from: private */
    public m b;
    private m c;
    private ApkResCallback.Stub d = new i(this);

    public static synchronized h a() {
        h hVar;
        synchronized (h.class) {
            if (f883a == null) {
                f883a = new h();
            }
            hVar = f883a;
        }
        return hVar;
    }

    public h() {
        b();
        if (this.b == null) {
            this.b = new m();
        }
        if (this.c == null) {
            this.c = new m();
        }
    }

    public void a(String str) {
        if (this.b != null) {
            this.b.a(str);
        }
        if (this.c != null) {
            this.c.a(str);
        }
    }

    public void a(String str, String str2, int i, String str3) {
        if (!Build.BRAND.contains("Meizu")) {
            this.c.a(str, str2, i, str3);
        }
    }

    public void b(String str) {
        n nVar;
        DownloadInfo f;
        String string;
        if (!Build.BRAND.contains("Meizu")) {
            if (this.c == null && (f = DownloadProxy.a().f(str)) != null && !TextUtils.isEmpty(f.downloadTicket)) {
                if (!f.isSllUpdateApk() && !f.isUpdateApk()) {
                    if (!TextUtils.isEmpty(f.name)) {
                        string = f.name;
                    } else {
                        string = AstApp.i().getString(R.string.unknowappname_install_notification_title);
                    }
                    int i = (int) f.appId;
                    XLog.d("InstallPush", "id: " + i);
                    a().a(str, string, i, f.iconUrl);
                } else {
                    return;
                }
            }
            if (this.c != null) {
                nVar = this.c.b(str);
            } else {
                nVar = null;
            }
            if (nVar != null) {
                this.b.a(nVar.c(), nVar.b(), nVar.a(), nVar.d());
                j.a().a(this.b);
            }
        }
    }

    private void b() {
        ApkResourceManager.getInstance().registerApkResCallback(this.d);
    }
}
