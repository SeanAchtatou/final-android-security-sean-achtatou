package com.tencent.assistant.manager.notification.a;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.a.a;
import com.tencent.assistant.manager.notification.NotificationService;
import com.tencent.assistant.manager.notification.a.a.f;
import com.tencent.assistant.manager.notification.o;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.manager.notification.y;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.PushIconInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.r;
import com.tencent.connect.common.Constants;
import java.lang.reflect.Field;
import java.util.ArrayList;

/* compiled from: ProGuard */
public abstract class e extends a {
    protected PushInfo c = null;
    protected byte[] d = null;
    protected ActionUrl e = null;
    protected int f;
    protected String g = Constants.STR_EMPTY;
    protected ArrayList<String> h = null;
    protected RemoteViews i = null;
    protected RemoteViews j = null;
    protected RemoteViews k = null;
    protected Integer l = null;
    protected Integer m = null;
    protected float n = -1.0f;
    protected float o = -1.0f;

    /* access modifiers changed from: protected */
    public abstract boolean f();

    /* access modifiers changed from: protected */
    public abstract boolean g();

    /* access modifiers changed from: protected */
    public abstract boolean h();

    public e(int i2, PushInfo pushInfo, byte[] bArr) {
        super(i2);
        XLog.d("hoganliu", "PushNotification()");
        if (pushInfo != null) {
            this.c = pushInfo;
            this.d = bArr;
            this.e = pushInfo.i;
            this.f = pushInfo.g;
            this.g = e();
            o a2 = o.a();
            this.l = a2.d();
            this.n = a2.e();
            this.m = a2.b();
            this.o = a2.c();
        }
    }

    /* access modifiers changed from: protected */
    public String e() {
        return (this.c != null ? this.c.f1446a : 0) + "|" + (this.c != null ? String.valueOf(this.c.k) : Constants.STR_EMPTY) + "|" + this.f + "|0";
    }

    public String a(int i2) {
        return (this.c != null ? this.c.f1446a : 0) + "|" + (this.c != null ? String.valueOf(this.c.k) : Constants.STR_EMPTY) + "|" + this.f + "|" + i2;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        if (!f() || !g()) {
            return false;
        }
        this.b = y.a(AstApp.i(), R.drawable.logo32, this.i, Html.fromHtml(this.c != null ? this.c.b : Constants.STR_EMPTY), System.currentTimeMillis(), j(), l(), true, false);
        if (this.b == null || !h()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        if (this.c == null || this.c.d == null || TextUtils.isEmpty(this.c.d.f1125a)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (this.c != null && !TextUtils.isEmpty(this.c.b)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(int i2) {
        this.i = c(i2);
        if (this.l != null) {
            this.i.setTextColor(R.id.title, this.l.intValue());
        }
        this.i.setFloat(R.id.title, "setTextSize", this.n);
        this.i.setTextViewText(R.id.title, Html.fromHtml(this.c != null ? this.c.b : Constants.STR_EMPTY));
        if (r.d() >= 20) {
            this.i.setInt(R.id.root, "setBackgroundResource", R.color.notification_bg_50);
        }
        PushIconInfo pushIconInfo = this.c != null ? this.c.f : null;
        if (pushIconInfo == null || TextUtils.isEmpty(pushIconInfo.b)) {
            pushIconInfo = m();
        }
        if (pushIconInfo.f1445a == 7) {
            this.i.setImageViewResource(R.id.big_icon, Integer.parseInt(pushIconInfo.b));
            return;
        }
        f fVar = new f(pushIconInfo);
        fVar.a(new f(this));
        a(fVar);
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.i != null) {
            if (this.m != null) {
                this.i.setTextColor(R.id.content, this.m.intValue());
            }
            this.i.setFloat(R.id.content, "setTextSize", this.o);
            this.i.setTextViewText(R.id.content, Html.fromHtml(this.c != null ? this.c.c : Constants.STR_EMPTY));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.a.e.a(com.tencent.assistant.protocol.jce.ActionUrl, boolean):android.content.Intent
     arg types: [com.tencent.assistant.protocol.jce.ActionUrl, int]
     candidates:
      com.tencent.assistant.manager.notification.a.e.a(android.app.Notification, android.widget.RemoteViews):boolean
      com.tencent.assistant.manager.notification.a.a.a(com.tencent.assistant.manager.notification.a.a, int):void
      com.tencent.assistant.manager.notification.a.e.a(com.tencent.assistant.protocol.jce.ActionUrl, boolean):android.content.Intent */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public PendingIntent j() {
        if (this.c == null) {
            return null;
        }
        Intent a2 = a(this.c.d, false);
        a2.putExtra("notification_push_from_right_button", false);
        return PendingIntent.getService(AstApp.i(), this.f864a, a2, 268435456);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.a.e.a(com.tencent.assistant.protocol.jce.ActionUrl, boolean):android.content.Intent
     arg types: [com.tencent.assistant.protocol.jce.ActionUrl, int]
     candidates:
      com.tencent.assistant.manager.notification.a.e.a(android.app.Notification, android.widget.RemoteViews):boolean
      com.tencent.assistant.manager.notification.a.a.a(com.tencent.assistant.manager.notification.a.a, int):void
      com.tencent.assistant.manager.notification.a.e.a(com.tencent.assistant.protocol.jce.ActionUrl, boolean):android.content.Intent */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public PendingIntent k() {
        Intent a2 = a(this.e, true);
        a2.putExtra("notification_push_from_right_button", true);
        return PendingIntent.getService(AstApp.i(), v.d(this.f864a), a2, 268435456);
    }

    private Intent a(ActionUrl actionUrl, boolean z) {
        Intent intent = new Intent(AstApp.i(), NotificationService.class);
        intent.putExtra("notification_id", this.f864a);
        intent.putExtra("notification_push_recommend_id", this.d);
        intent.putExtra("notification_push_id", this.c.f1446a);
        intent.putExtra("notification_push_extra", this.g);
        intent.putExtra("notification_push_type", this.c.j);
        intent.putExtra("notification_push_TITLE", this.c.b);
        intent.putExtra("notification_push_CONTENT", this.c.c);
        if (actionUrl != null && !TextUtils.isEmpty(actionUrl.f1125a)) {
            intent.putExtra("notification_action", actionUrl);
        }
        if (z && this.h != null && this.h.size() > 0) {
            XLog.d("hoganliu", "getMinorIntent: autoUpdateAppList.size(): " + this.h.size());
            intent.putStringArrayListExtra(a.R, this.h);
        }
        return intent;
    }

    /* access modifiers changed from: protected */
    public PendingIntent l() {
        if (this.c == null) {
            return null;
        }
        Intent intent = new Intent(AstApp.i(), NotificationService.class);
        intent.setAction("android.intent.action.DELETE");
        intent.putExtra("notification_id", this.f864a);
        intent.putExtra("notification_push_recommend_id", this.d);
        intent.putExtra("notification_push_id", this.c.f1446a);
        intent.putExtra("notification_push_extra", this.g);
        intent.putExtra("notification_push_type", this.c.j);
        intent.putExtra("notification_push_TITLE", this.c.b);
        intent.putExtra("notification_push_CONTENT", this.c.c);
        return PendingIntent.getService(AstApp.i(), this.f864a, intent, 268435456);
    }

    private PushIconInfo m() {
        PushIconInfo pushIconInfo = new PushIconInfo();
        pushIconInfo.a((byte) 2);
        pushIconInfo.a(AstApp.i().getPackageName());
        return pushIconInfo;
    }

    public static boolean a(Notification notification, RemoteViews remoteViews) {
        if (notification == null || remoteViews == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 16) {
            return true;
        }
        try {
            Field declaredField = notification.getClass().getDeclaredField("bigContentView");
            Field declaredField2 = notification.getClass().getDeclaredField("priority");
            if (declaredField != null) {
                declaredField.setAccessible(true);
                declaredField.set(notification, remoteViews);
            }
            if (declaredField2 == null) {
                return true;
            }
            declaredField2.setAccessible(true);
            declaredField2.set(notification, 2);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static RemoteViews c(int i2) {
        return new RemoteViews(AstApp.i().getPackageName(), i2);
    }
}
