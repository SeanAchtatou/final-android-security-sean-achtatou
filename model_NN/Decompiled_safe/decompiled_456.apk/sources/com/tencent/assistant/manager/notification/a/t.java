package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.tencent.assistant.manager.notification.a.a.e;

/* compiled from: ProGuard */
class t implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RemoteViews f875a;
    final /* synthetic */ int[] b;
    final /* synthetic */ int c;
    final /* synthetic */ s d;

    t(s sVar, RemoteViews remoteViews, int[] iArr, int i) {
        this.d = sVar;
        this.f875a = remoteViews;
        this.b = iArr;
        this.c = i;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f875a.setViewVisibility(this.b[this.c], 0);
            this.f875a.setImageViewBitmap(this.b[this.c], bitmap);
        }
    }
}
