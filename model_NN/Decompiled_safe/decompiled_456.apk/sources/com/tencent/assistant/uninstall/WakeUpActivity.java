package com.tencent.assistant.uninstall;

import android.app.Activity;
import android.os.Bundle;
import com.tencent.nucleus.manager.uninstallwatch.e;

/* compiled from: ProGuard */
public class WakeUpActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        e.a().a(getIntent());
        finish();
    }
}
