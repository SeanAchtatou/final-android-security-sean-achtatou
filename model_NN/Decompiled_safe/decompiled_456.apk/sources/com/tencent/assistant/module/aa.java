package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.g;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
class aa implements CallbackHelper.Caller<g> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f955a;
    final /* synthetic */ boolean b;
    final /* synthetic */ LinkedHashMap c;
    final /* synthetic */ ArrayList d;
    final /* synthetic */ boolean e;
    final /* synthetic */ y f;

    aa(y yVar, int i, boolean z, LinkedHashMap linkedHashMap, ArrayList arrayList, boolean z2) {
        this.f = yVar;
        this.f955a = i;
        this.b = z;
        this.c = linkedHashMap;
        this.d = arrayList;
        this.e = z2;
    }

    /* renamed from: a */
    public void call(g gVar) {
        gVar.a(this.f955a, 0, this.b, this.c, this.d, this.e);
    }
}
