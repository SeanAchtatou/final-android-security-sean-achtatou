package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.o;
import com.tencent.assistant.protocol.jce.SyncVerifyRstRequest;
import com.tencent.assistant.protocol.jce.SyncVerifyRstResponse;
import com.tencent.assistant.protocol.jce.VerifyInfo;

/* compiled from: ProGuard */
public class bj extends BaseEngine<o> {
    public int a(VerifyInfo verifyInfo) {
        if (verifyInfo == null) {
            return -1;
        }
        SyncVerifyRstRequest syncVerifyRstRequest = new SyncVerifyRstRequest();
        syncVerifyRstRequest.f1575a = verifyInfo;
        return send(syncVerifyRstRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new bk(this, i, ((SyncVerifyRstResponse) jceStruct2).f1576a));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new bl(this, i, i2));
    }
}
