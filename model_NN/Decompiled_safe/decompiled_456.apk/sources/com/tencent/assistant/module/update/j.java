package com.tencent.assistant.module.update;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.c;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.p;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.protocol.jce.AppInfoForIgnore;
import com.tencent.assistant.protocol.jce.AppInfoForUpdate;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.ClientStatus;
import com.tencent.assistant.protocol.jce.GetAppUpdateRequest;
import com.tencent.assistant.protocol.jce.GetAppUpdateResponse;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.g;
import com.tencent.assistant.utils.h;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.b;
import com.tencent.cloud.d.s;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.appbackup.o;
import com.tencent.nucleus.manager.usagestats.a;
import com.tencent.pangu.manager.ak;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class j extends p {

    /* renamed from: a  reason: collision with root package name */
    public static long f1036a = 0;
    private static j b = null;
    private static boolean c = false;
    /* access modifiers changed from: private */
    public a d = new a();
    private Set<r> e = Collections.synchronizedSet(new HashSet());
    private HashSet<r> f = new HashSet<>();
    /* access modifiers changed from: private */
    public c g = new c(AstApp.i());
    /* access modifiers changed from: private */
    public int h = -1;
    private int i = -1;
    private int j = 0;

    private j() {
        g();
        c = true;
    }

    public static boolean a() {
        return c;
    }

    public static synchronized j b() {
        j jVar;
        synchronized (j.class) {
            if (b == null) {
                b = new j();
            }
            jVar = b;
        }
        return jVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.update.j.a(java.util.Map<java.lang.Integer, java.util.ArrayList<com.tencent.assistant.protocol.jce.AppUpdateInfo>>, boolean):java.util.Map<java.lang.String, com.tencent.assistant.protocol.jce.AppUpdateInfo>
     arg types: [java.util.Map<java.lang.Integer, java.util.ArrayList<com.tencent.assistant.protocol.jce.AppUpdateInfo>>, int]
     candidates:
      com.tencent.assistant.module.update.j.a(com.tencent.assistant.module.update.j, int):int
      com.tencent.assistant.module.update.j.a(com.tencent.assistant.protocol.jce.LbsData, int):int
      com.tencent.assistant.module.update.j.a(com.tencent.assistant.module.update.AppUpdateConst$RequestLaunchType, boolean):void
      com.tencent.assistant.module.update.j.a(java.lang.String, int):void
      com.tencent.assistant.module.update.j.a(java.util.Map<java.lang.Integer, java.util.ArrayList<com.tencent.assistant.protocol.jce.AppUpdateInfo>>, boolean):java.util.Map<java.lang.String, com.tencent.assistant.protocol.jce.AppUpdateInfo> */
    /* access modifiers changed from: private */
    public void g() {
        a(i.y().i(), false);
        k();
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED));
    }

    public void a(String str, int i2) {
        this.d.d(str);
        d(str);
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED));
        TemporaryThreadManager.get().start(new k(this));
    }

    public void b(String str, int i2) {
        Iterator<r> it = this.f.iterator();
        while (it.hasNext()) {
            r next = it.next();
            if (next.f1044a.equalsIgnoreCase(str)) {
                a(next);
                j();
                it.remove();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        String str;
        GetAppUpdateRequest getAppUpdateRequest;
        boolean z2;
        byte[] bArr;
        Iterator<Map.Entry<String, AppUpdateInfo>> it;
        Map.Entry next;
        String str2;
        if (this.h != i2 && this.i != i2) {
            return;
        }
        if (this.i == i2) {
            Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_SUCCESS);
            obtainMessage.arg1 = this.i;
            obtainMessage.arg2 = this.j;
            AstApp.i().j().sendMessage(obtainMessage);
            this.i = -1;
            return;
        }
        this.h = -1;
        m.a().b("app_update_response_succ_time", Long.valueOf(System.currentTimeMillis()));
        if (jceStruct == null || !(jceStruct instanceof GetAppUpdateRequest)) {
            z = false;
            str = null;
            getAppUpdateRequest = null;
            z2 = false;
        } else {
            getAppUpdateRequest = (GetAppUpdateRequest) jceStruct;
            byte a2 = getAppUpdateRequest.a();
            if (getAppUpdateRequest.j != null) {
                str2 = String.valueOf(getAppUpdateRequest.j.f1194a);
            } else {
                str2 = null;
            }
            XLog.d("AppUpdateEngine", "   reqFlag = " + ((int) a2));
            if (a2 == 0 || 2 == a2 || 4 == a2) {
                if (getAppUpdateRequest.j == null || getAppUpdateRequest.j.f1194a != AppUpdateConst.RequestLaunchType.TYPE_INTIME_UPDATE_PUSH.ordinal()) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (4 != a2) {
                    m.a().b("app_upload_all_succ_time", Long.valueOf(System.currentTimeMillis()));
                }
            } else {
                z2 = false;
            }
            if (getAppUpdateRequest.k > 0) {
                str = str2;
                z = true;
            } else {
                str = str2;
                z = false;
            }
        }
        if (z2) {
            m.a().b("app_update_refresh_suc_time", Long.valueOf(System.currentTimeMillis()));
        }
        if (!z) {
            m.a().b("app_update_full_response_time", Long.valueOf(System.currentTimeMillis()));
        }
        XLog.d("AppUpdateEngine", "协议成功回调  isNeedUpdateTime = " + z2 + " incUpdate=" + z);
        if (jceStruct2 != null && (jceStruct2 instanceof GetAppUpdateResponse)) {
            GetAppUpdateResponse getAppUpdateResponse = (GetAppUpdateResponse) jceStruct2;
            m.a().b("app_update_response_server_time", Long.valueOf(getAppUpdateResponse.f));
            m.a().b("key_app_update_full_response_valid_time", Integer.valueOf(getAppUpdateResponse.g * 1000));
            Map<String, AppUpdateInfo> a3 = a(getAppUpdateResponse.a(), z);
            if (!(getAppUpdateRequest == null || getAppUpdateRequest.j == null || getAppUpdateRequest.j.f1194a != AppUpdateConst.RequestLaunchType.TYPE_STARTUP.ordinal())) {
                f();
            }
            if (a3 == null || (it = a3.entrySet().iterator()) == null || !it.hasNext() || (next = it.next()) == null || next.getValue() == null) {
                bArr = null;
            } else {
                bArr = ((AppUpdateInfo) next.getValue()).u;
            }
            com.tencent.assistantv2.st.page.c.a(getAppUpdateResponse.d, bArr);
            if (!m.a().q() || getAppUpdateResponse.d == null) {
                int i3 = 7;
                if (!m.a().q()) {
                    i3 = 1;
                }
                com.tencent.assistantv2.st.page.c.a(getAppUpdateResponse.d, bArr, i3, str);
            } else {
                v.a().a(112, getAppUpdateResponse.d, bArr, false, str);
            }
            a(a3);
            k();
            ak.a().a(a3.size() - this.e.size());
        }
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(1016));
        s.b().a(d());
        s.b().a();
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i2, int i3, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.h != i2 && this.i != i2) {
            return;
        }
        if (this.i == i2) {
            Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_FAIL);
            obtainMessage.arg1 = this.i;
            obtainMessage.arg2 = i3;
            AstApp.i().j().sendMessage(obtainMessage);
            this.i = -1;
            return;
        }
        this.h = -1;
        XLog.d("AppUpdateEngine", "协议失败回调 errorCode=" + i3);
        if (jceStruct != null && (jceStruct instanceof GetAppUpdateRequest) && jceStruct2 != null && (jceStruct2 instanceof GetAppUpdateResponse)) {
            GetAppUpdateRequest getAppUpdateRequest = (GetAppUpdateRequest) jceStruct;
            ClientStatus clientStatus = getAppUpdateRequest.j;
            byte b2 = ((GetAppUpdateResponse) jceStruct2).e;
            XLog.d("AppUpdateEngine", "协议失败回调  clientStatus=" + clientStatus + ", retryFlagFromServer=" + ((int) b2));
            boolean z = false;
            if (clientStatus != null && clientStatus.f1194a == AppUpdateConst.RequestLaunchType.TYPE_STARTUP.ordinal() && b2 < 0) {
                if (m.a().ap() > 0 && m.a().aq() < m.a().ap()) {
                    z = true;
                }
                XLog.d("AppUpdateEngine", "协议失败回调，重试逻辑  failRetry=" + z + ", MaxRetryTimes=" + m.a().ap() + ", currentRetryTimes=" + m.a().aq());
            }
            if (z) {
                m.a().k(m.a().aq() + 1);
                b(b2);
                a(AppUpdateConst.RequestLaunchType.TYPE_STARTUP, getAppUpdateRequest.b(), (Map<String, String>) null);
            }
        }
        TemporaryThreadManager.get().start(new l(this));
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_CHECKUPDATE_FAIL));
    }

    private boolean h() {
        Map<Integer, ArrayList<AppUpdateInfo>> i2 = i.y().i();
        if (i2 == null || i2.size() <= 0) {
            return false;
        }
        return true;
    }

    private Map<String, AppUpdateInfo> a(Map<Integer, ArrayList<AppUpdateInfo>> map, boolean z) {
        Map map2;
        List list;
        Hashtable hashtable = new Hashtable();
        Map hashtable2 = new Hashtable();
        Hashtable hashtable3 = new Hashtable();
        if (z) {
            Map i2 = i.y().i();
            if (i2 != null) {
                int i3 = 1;
                while (true) {
                    int i4 = i3;
                    if (i4 > 3) {
                        break;
                    }
                    ArrayList<AppUpdateInfo> b2 = b((List) i2.get(Integer.valueOf(i4)));
                    if (b2 == null) {
                        i2.remove(Integer.valueOf(i4));
                    } else {
                        i2.put(Integer.valueOf(i4), b2);
                        Iterator<AppUpdateInfo> it = b2.iterator();
                        while (it.hasNext()) {
                            AppUpdateInfo next = it.next();
                            if (next != null && !TextUtils.isEmpty(next.f1162a)) {
                                hashtable3.put(next.f1162a, Integer.valueOf(i4));
                                hashtable.put(next.f1162a, next);
                            }
                        }
                    }
                    i3 = i4 + 1;
                }
                map2 = i2;
            } else {
                map2 = new Hashtable();
            }
        } else {
            map2 = hashtable2;
        }
        int i5 = 1;
        while (true) {
            int i6 = i5;
            if (i6 <= 3) {
                ArrayList arrayList = map.get(Integer.valueOf(i6));
                if (i6 == 1 && (list = map.get(0)) != null && !list.isEmpty()) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.addAll(0, list);
                }
                ArrayList<AppUpdateInfo> b3 = b(arrayList);
                if (b3 != null && !b3.isEmpty()) {
                    Iterator<AppUpdateInfo> it2 = b3.iterator();
                    while (it2.hasNext()) {
                        AppUpdateInfo next2 = it2.next();
                        if (next2 != null && !TextUtils.isEmpty(next2.f1162a)) {
                            hashtable.put(next2.f1162a, next2);
                        }
                    }
                    if (hashtable3 == null || hashtable3.size() <= 0) {
                        map2.put(Integer.valueOf(i6), b3);
                    } else {
                        Iterator<AppUpdateInfo> it3 = b3.iterator();
                        while (it3.hasNext()) {
                            AppUpdateInfo next3 = it3.next();
                            if (next3 != null && !TextUtils.isEmpty(next3.f1162a) && hashtable3.containsKey(next3.f1162a)) {
                                int intValue = ((Integer) hashtable3.get(next3.f1162a)).intValue();
                                ArrayList arrayList2 = (ArrayList) map2.get(Integer.valueOf(intValue));
                                if (arrayList2 != null) {
                                    ArrayList arrayList3 = new ArrayList();
                                    Iterator it4 = arrayList2.iterator();
                                    while (it4.hasNext()) {
                                        AppUpdateInfo appUpdateInfo = (AppUpdateInfo) it4.next();
                                        if (appUpdateInfo != null && !TextUtils.isEmpty(appUpdateInfo.f1162a) && !next3.f1162a.equals(appUpdateInfo.f1162a)) {
                                            arrayList3.add(appUpdateInfo);
                                        }
                                    }
                                    map2.put(Integer.valueOf(intValue), arrayList3);
                                }
                            }
                        }
                        ArrayList arrayList4 = (ArrayList) map2.get(Integer.valueOf(i6));
                        if (arrayList4 != null) {
                            arrayList4.addAll(0, b3);
                            map2.put(Integer.valueOf(i6), arrayList4);
                        } else {
                            map2.put(Integer.valueOf(i6), b3);
                        }
                    }
                }
                i5 = i6 + 1;
            } else {
                i.y().a(map2);
                this.d.a(map2);
                return hashtable;
            }
        }
    }

    private ArrayList<AppUpdateInfo> b(List<AppUpdateInfo> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayList<AppUpdateInfo> arrayList = new ArrayList<>();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return arrayList;
            }
            AppUpdateInfo appUpdateInfo = list.get(i3);
            if (!TextUtils.isEmpty(appUpdateInfo.f1162a) && g.a(appUpdateInfo)) {
                arrayList.add(appUpdateInfo);
            }
            i2 = i3 + 1;
        }
    }

    private void a(Map<String, AppUpdateInfo> map) {
        if (map != null && !map.isEmpty()) {
            HashMap hashMap = new HashMap();
            Iterator<Map.Entry<String, AppUpdateInfo>> it = map.entrySet().iterator();
            while (it != null && it.hasNext()) {
                Map.Entry next = it.next();
                if (!(next == null || next.getValue() == null || ((AppUpdateInfo) next.getValue()).r <= 0)) {
                    hashMap.put(((AppUpdateInfo) next.getValue()).f1162a, Long.valueOf(((AppUpdateInfo) next.getValue()).o));
                }
            }
            ApkResourceManager.getInstance().updateAppId(hashMap);
        }
    }

    private void c(List<AppUpdateInfo> list) {
        if (list != null && !list.isEmpty()) {
            HashMap hashMap = new HashMap();
            for (AppUpdateInfo next : list) {
                if (next.o > 0) {
                    hashMap.put(next.f1162a, Long.valueOf(next.o));
                }
            }
            ApkResourceManager.getInstance().updateAppId(hashMap);
        }
    }

    public void a(AppUpdateConst.RequestLaunchType requestLaunchType, LbsData lbsData, Map<String, String> map) {
        TemporaryThreadManager.get().start(new m(this, requestLaunchType, lbsData, map));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public int a(LbsData lbsData, int i2) {
        byte b2;
        b.a().a(a.a());
        boolean c2 = bo.c(m.a().a("app_upload_all_succ_time", 0L));
        ArrayList<AppInfoForUpdate> a2 = a(c2);
        if (!c2 && a2 != null && a2.size() >= 80) {
            return a(lbsData, i2, a2);
        }
        if (c2) {
            b2 = 1;
        } else {
            b2 = o.a() ? (byte) 2 : 0;
        }
        return a(lbsData, i2, a2, b2);
    }

    private int a(LbsData lbsData, int i2, ArrayList<AppInfoForUpdate> arrayList) {
        com.tencent.cloud.d.a.a().a(lbsData, i2, arrayList);
        return -1;
    }

    public void a(LbsData lbsData, int i2, boolean z) {
        byte b2;
        if (z) {
            b2 = 4;
            m.a().b("app_upload_all_succ_time", Long.valueOf(System.currentTimeMillis()));
        } else {
            b2 = o.a() ? (byte) 2 : 0;
        }
        this.h = a(lbsData, i2, a(z), b2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private int a(LbsData lbsData, int i2, ArrayList<AppInfoForUpdate> arrayList, byte b2) {
        GetAppUpdateRequest getAppUpdateRequest = new GetAppUpdateRequest();
        ArrayList<AppInfoForIgnore> i3 = i();
        ArrayList<AutoDownloadInfo> b3 = k.b();
        getAppUpdateRequest.a(b2);
        if (b2 == 2) {
            getAppUpdateRequest.a(t.v());
        }
        if (lbsData != null) {
            getAppUpdateRequest.a(lbsData);
        }
        getAppUpdateRequest.a(arrayList);
        getAppUpdateRequest.b(i3);
        getAppUpdateRequest.c(b3);
        if (lbsData != null) {
            getAppUpdateRequest.a(lbsData);
        }
        ClientStatus clientStatus = new ClientStatus();
        clientStatus.f1194a = i2;
        clientStatus.c = m.a().V();
        getAppUpdateRequest.j = clientStatus;
        if (System.currentTimeMillis() - m.a().a("app_update_full_response_time", 0L) >= m.a().a("key_app_update_full_response_valid_time", 7200000L) || !h()) {
            getAppUpdateRequest.k = 0;
        } else {
            getAppUpdateRequest.k = m.a().a("app_update_response_server_time", 0L);
        }
        return send(getAppUpdateRequest);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public int a(ArrayList<AppInfoForUpdate> arrayList, LbsData lbsData, byte b2, int i2) {
        b.a().a(a.a());
        GetAppUpdateRequest getAppUpdateRequest = new GetAppUpdateRequest();
        getAppUpdateRequest.a(b2);
        getAppUpdateRequest.a(arrayList);
        getAppUpdateRequest.b(null);
        getAppUpdateRequest.c(null);
        if (lbsData != null) {
            getAppUpdateRequest.a(lbsData);
        }
        ClientStatus clientStatus = new ClientStatus();
        clientStatus.f1194a = i2;
        clientStatus.c = m.a().V();
        getAppUpdateRequest.j = clientStatus;
        if (System.currentTimeMillis() - m.a().a("app_update_full_response_time", 0L) >= m.a().a("key_app_update_full_response_valid_time", 7200000L)) {
            getAppUpdateRequest.k = 0;
        } else {
            getAppUpdateRequest.k = m.a().a("app_update_response_server_time", 0L);
        }
        XLog.d("AppUpdateEngine", "发起协议 inc flag=" + ((int) getAppUpdateRequest.b) + " action=" + i2 + " lastBackgroundTime=" + clientStatus.c + ", lastGetAppUpdateTime:" + getAppUpdateRequest.k);
        return send(getAppUpdateRequest);
    }

    public int c() {
        b.a().a(a.a());
        GetAppUpdateRequest getAppUpdateRequest = new GetAppUpdateRequest();
        ArrayList<AppInfoForUpdate> a2 = a(false);
        if (a2 != null) {
            this.j = a2.size();
        } else {
            this.j = 0;
        }
        getAppUpdateRequest.a((byte) 3);
        getAppUpdateRequest.a(a2);
        getAppUpdateRequest.b(null);
        getAppUpdateRequest.c(null);
        String v = t.v();
        XLog.d("BACKUP_TAG", "deviceName = " + v);
        if (TextUtils.isEmpty(v)) {
            v = "未知设备";
        }
        getAppUpdateRequest.a(v);
        this.i = send(getAppUpdateRequest);
        return this.i;
    }

    public static ArrayList<AppInfoForUpdate> a(boolean z) {
        boolean z2 = true;
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        com.tencent.assistant.localres.m.d(localApkInfos);
        if (z) {
            localApkInfos = d(localApkInfos);
        }
        if (localApkInfos == null || localApkInfos.isEmpty()) {
            z2 = false;
        }
        if (z2) {
            return e(localApkInfos);
        }
        return null;
    }

    private ArrayList<AppInfoForIgnore> i() {
        ArrayList<AppInfoForIgnore> arrayList = new ArrayList<>();
        for (r next : this.e) {
            AppInfoForIgnore appInfoForIgnore = new AppInfoForIgnore();
            appInfoForIgnore.a(next.f1044a);
            appInfoForIgnore.a(next.c);
            appInfoForIgnore.b(next.b);
            arrayList.add(appInfoForIgnore);
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private static List<LocalApkInfo> d(List<LocalApkInfo> list) {
        long a2 = m.a().a("app_upload_all_succ_time", 0L);
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            LocalApkInfo localApkInfo = list.get(i2);
            if (localApkInfo != null && bo.c(localApkInfo.mInstallDate) && localApkInfo.mInstallDate >= a2) {
                arrayList.add(localApkInfo);
            }
        }
        return arrayList;
    }

    private static ArrayList<AppInfoForUpdate> e(List<LocalApkInfo> list) {
        ArrayList<AppInfoForUpdate> arrayList = new ArrayList<>();
        for (LocalApkInfo b2 : list) {
            AppInfoForUpdate b3 = b(b2);
            if (b3 != null) {
                arrayList.add(b3);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public static AppInfoForUpdate b(LocalApkInfo localApkInfo) {
        if (localApkInfo == null) {
            return null;
        }
        AppInfoForUpdate appInfoForUpdate = new AppInfoForUpdate();
        if (localApkInfo.mAppid > 0) {
            appInfoForUpdate.e = localApkInfo.mAppid;
        } else {
            appInfoForUpdate.f1157a = localApkInfo.mPackageName;
        }
        appInfoForUpdate.c = localApkInfo.mVersionCode;
        appInfoForUpdate.g = localApkInfo.mVersionName;
        appInfoForUpdate.b = localApkInfo.signature == null ? Constants.STR_EMPTY : localApkInfo.signature;
        appInfoForUpdate.d = localApkInfo.manifestMd5;
        appInfoForUpdate.f = localApkInfo.getAppType();
        appInfoForUpdate.j = localApkInfo.mAppName;
        appInfoForUpdate.i = (long) localApkInfo.launchCount;
        appInfoForUpdate.h = localApkInfo.mLastLaunchTime;
        if (localApkInfo.mInstallDate >= m.a().a("app_update_response_succ_time", 0L) - 60000) {
            appInfoForUpdate.r = 1;
        } else {
            appInfoForUpdate.r = 0;
        }
        return appInfoForUpdate;
    }

    public List<AppUpdateInfo> d() {
        return this.d.a();
    }

    public AppUpdateInfo a(String str) {
        return this.d.a(str);
    }

    public boolean b(String str) {
        return a(str) != null;
    }

    public ArrayList<Integer> c(String str) {
        return this.d.b(str);
    }

    public synchronized Set<r> e() {
        return this.e;
    }

    public synchronized boolean a(SimpleAppModel simpleAppModel) {
        boolean z = false;
        synchronized (this) {
            if (simpleAppModel != null) {
                if (this.d.c(simpleAppModel.c)) {
                    a(new r(simpleAppModel.c, simpleAppModel.f, simpleAppModel.g, false));
                    z = true;
                }
            }
        }
        return z;
    }

    private void a(r rVar) {
        if (rVar != null && !this.e.contains(rVar)) {
            this.e.add(rVar);
            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(1019));
            b(rVar);
        }
    }

    private synchronized void d(String str) {
        if (!TextUtils.isEmpty(str)) {
            ArrayList arrayList = new ArrayList(this.e);
            ArrayList arrayList2 = new ArrayList(1);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                r rVar = (r) it.next();
                if (rVar.f1044a.equalsIgnoreCase(str)) {
                    arrayList2.add(rVar);
                }
            }
            if (!arrayList2.isEmpty()) {
                this.e.removeAll(arrayList2);
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE));
                c((r) arrayList2.get(0));
                this.f.add(arrayList2.get(0));
                arrayList2.clear();
            }
        }
    }

    public synchronized void b(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            r rVar = new r(simpleAppModel.c, simpleAppModel.f, simpleAppModel.g, false);
            if (this.e.contains(rVar)) {
                this.e.remove(rVar);
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE));
                c(rVar);
            }
        }
    }

    private void j() {
        ArrayList arrayList = new ArrayList();
        for (AppUpdateInfo next : this.d.a()) {
            r rVar = new r(next.f1162a, next.n, next.d, h.b(next.q));
            for (r next2 : this.e) {
                if (next2.f1044a.equals(rVar.f1044a) && (next2.c == rVar.c || !rVar.d)) {
                    arrayList.add(next2);
                }
            }
        }
        this.e.clear();
        this.e.addAll(arrayList);
    }

    private void k() {
        this.e.clear();
        this.e.addAll(this.g.a());
        j();
    }

    private void b(r rVar) {
        TemporaryThreadManager.get().start(new n(this, rVar));
    }

    private void c(r rVar) {
        TemporaryThreadManager.get().start(new o(this, rVar));
    }

    public List<AppUpdateInfo> a(int i2) {
        if (this.d != null) {
            return this.d.a(i2);
        }
        return null;
    }

    public void a(List<AppUpdateInfo> list) {
        if (list != null && !list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppUpdateInfo next : list) {
                if (a(next)) {
                    arrayList.add(next);
                }
            }
            if (!arrayList.isEmpty()) {
                this.d.a(3, arrayList);
                c(arrayList);
                k();
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED));
            }
        }
    }

    private boolean a(AppUpdateInfo appUpdateInfo) {
        LocalApkInfo installedApkInfo;
        if (appUpdateInfo == null || TextUtils.isEmpty(appUpdateInfo.f1162a) || (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(appUpdateInfo.f1162a)) == null) {
            return false;
        }
        AppUpdateInfo a2 = this.d.a(appUpdateInfo.f1162a);
        if (a2 != null) {
            if (a2.d >= appUpdateInfo.d) {
                return false;
            }
            appUpdateInfo.E = installedApkInfo.manifestMd5;
            appUpdateInfo.C = installedApkInfo.mVersionCode;
            return true;
        } else if (installedApkInfo.mVersionCode >= appUpdateInfo.d) {
            return false;
        } else {
            appUpdateInfo.E = installedApkInfo.manifestMd5;
            appUpdateInfo.C = installedApkInfo.mVersionCode;
            return true;
        }
    }

    public void a(AppUpdateConst.RequestLaunchType requestLaunchType, boolean z) {
        TemporaryThreadManager.get().start(new p(this, z, requestLaunchType));
    }

    public void f() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUA());
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", String.valueOf(f1036a));
        hashMap.put("B4", String.valueOf(m.a().ap()));
        hashMap.put("B5", String.valueOf(m.a().aq()));
        com.tencent.beacon.event.a.a("StatAppUpdateResponseSuccess", true, 0, 0, hashMap, true);
    }

    public void b(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUA());
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", String.valueOf(f1036a));
        hashMap.put("B4", String.valueOf(m.a().ap()));
        hashMap.put("B5", String.valueOf(i2));
        hashMap.put("B6", String.valueOf(m.a().aq()));
        hashMap.put("B7", String.valueOf(System.currentTimeMillis()));
        com.tencent.beacon.event.a.a("StatAppUpdateResponseFailRetry", true, 0, 0, hashMap, true);
    }
}
