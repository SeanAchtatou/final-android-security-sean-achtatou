package com.tencent.assistant.module;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a.d;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.protocol.jce.DataUpdateInfo;
import com.tencent.assistant.protocol.jce.GetUnionUpdateInfoRequest;
import com.tencent.assistant.protocol.jce.GetUnionUpdateInfoResponse;
import com.tencent.assistant.protocol.jce.NotifyInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.about.n;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class ag extends p {

    /* renamed from: a  reason: collision with root package name */
    static final byte[] f960a = {1, 3, 4, 5, 6, JceStruct.SIMPLE_LIST, 14, 17, 19};
    private static ag e;
    protected ReferenceQueue<ak> b = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<ak>> c = new ConcurrentLinkedQueue<>();
    final HashMap<Byte, am> d = new HashMap<>();
    private long f = 0;
    private UIEventListener g = new ah(this);

    private ag() {
        this.d.put((byte) 1, n.a());
    }

    public void a() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this.g);
    }

    public static synchronized ag b() {
        ag agVar;
        synchronized (ag.class) {
            if (e == null) {
                e = new ag();
            }
            agVar = e;
        }
        return agVar;
    }

    public void a(ak akVar) {
        if (akVar != null) {
            while (true) {
                Reference<? extends ak> poll = this.b.poll();
                if (poll == null) {
                    break;
                }
                this.c.remove(poll);
            }
            Iterator<WeakReference<ak>> it = this.c.iterator();
            while (it.hasNext()) {
                if (((ak) it.next().get()) == akVar) {
                    return;
                }
            }
            this.c.add(new WeakReference(akVar, this.b));
        }
    }

    private boolean a(byte b2) {
        return b2 == 19;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        XLog.d("linmg", "GetUnionUpdateInfoEngine onRequestSuccessed....");
        GetUnionUpdateInfoResponse getUnionUpdateInfoResponse = (GetUnionUpdateInfoResponse) jceStruct2;
        ArrayList<DataUpdateInfo> a2 = getUnionUpdateInfoResponse.a();
        if (a2 != null && a2.size() > 0) {
            boolean z2 = false;
            Iterator<DataUpdateInfo> it = a2.iterator();
            while (true) {
                z = z2;
                if (!it.hasNext()) {
                    break;
                }
                DataUpdateInfo next = it.next();
                if (a(next.f1213a)) {
                    XLog.d("linmg", "GetUnionUpdateInfoEngine is InTime");
                    d.a().a(next);
                } else {
                    z = true;
                    m.a().a(next.f1213a, next.b);
                }
                z2 = z;
            }
            if (z) {
                c();
            }
        }
        a(getUnionUpdateInfoResponse.b());
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    private void a(List<NotifyInfo> list) {
        if (list != null && list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (NotifyInfo next : list) {
                if (next != null) {
                    byte b2 = next.f1419a;
                    if (b2 == 1) {
                        am amVar = this.d.get(Byte.valueOf(b2));
                        if (amVar != null) {
                            amVar.a(next.b);
                        }
                    } else if (b2 == 2) {
                        QuickEntranceNotify quickEntranceNotify = new QuickEntranceNotify();
                        quickEntranceNotify.f937a = next.c;
                        quickEntranceNotify.b = next.d;
                        quickEntranceNotify.e = next.e;
                        arrayList.add(quickEntranceNotify);
                    }
                }
            }
            if (arrayList.size() > 0) {
                a((ArrayList<QuickEntranceNotify>) arrayList);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        Iterator<WeakReference<ak>> it = this.c.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next().get();
            if (akVar != null) {
                akVar.onLocalDataHasUpdate();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<QuickEntranceNotify> arrayList) {
        Iterator<WeakReference<ak>> it = this.c.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next().get();
            if (akVar != null) {
                akVar.onPromptHasNewNotify(arrayList);
            }
        }
    }

    public int d() {
        if (System.currentTimeMillis() - this.f < 5000) {
            return -1;
        }
        this.f = System.currentTimeMillis();
        XLog.d("NotifyPrompt", "****************** NotifyInfo request");
        GetUnionUpdateInfoRequest getUnionUpdateInfoRequest = new GetUnionUpdateInfoRequest();
        getUnionUpdateInfoRequest.f1340a = new ArrayList<>();
        for (byte b2 : f960a) {
            DataUpdateInfo dataUpdateInfo = new DataUpdateInfo();
            dataUpdateInfo.f1213a = b2;
            dataUpdateInfo.b = m.a().a(b2);
            getUnionUpdateInfoRequest.f1340a.add(dataUpdateInfo);
        }
        return send(getUnionUpdateInfoRequest);
    }
}
