package com.tencent.assistant.utils;

import com.tencent.pangu.download.DownloadInfo;
import java.util.Comparator;

/* compiled from: ProGuard */
public class cb implements Comparator<DownloadInfo> {
    /* renamed from: a */
    public int compare(DownloadInfo downloadInfo, DownloadInfo downloadInfo2) {
        return downloadInfo.createTime - downloadInfo2.createTime > 0 ? 1 : -1;
    }
}
