package com.tencent.assistant.utils;

import android.content.SharedPreferences;
import com.qq.AppService.AstApp;

/* compiled from: ProGuard */
public class be {
    public static boolean a() {
        return c().getBoolean("floating_show", true);
    }

    public static boolean b() {
        return c().getBoolean("floating_show_for_usercare", false);
    }

    public static void a(boolean z) {
        SharedPreferences.Editor edit = c().edit();
        edit.putBoolean("floating_show", true);
        if (!b() && z) {
            edit.putBoolean("floating_show_for_usercare", true);
        }
        edit.commit();
    }

    private static SharedPreferences c() {
        return AstApp.i().getApplicationContext().getSharedPreferences("assistant_settings", 0);
    }
}
