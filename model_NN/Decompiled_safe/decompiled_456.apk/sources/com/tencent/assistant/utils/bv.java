package com.tencent.assistant.utils;

import java.text.SimpleDateFormat;

/* compiled from: ProGuard */
final class bv extends ThreadLocal<SimpleDateFormat> {
    bv() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public SimpleDateFormat initialValue() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    }
}
