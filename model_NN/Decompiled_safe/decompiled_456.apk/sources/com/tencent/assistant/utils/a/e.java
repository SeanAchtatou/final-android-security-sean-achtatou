package com.tencent.assistant.utils.a;

/* compiled from: ProGuard */
public class e {
    public static short a(short s) {
        return (short) (((s & 255) << 8) | (((s >> 8) & 255) << 0));
    }

    public static int a(int i) {
        return (((i >> 0) & 255) << 24) | (((i >> 8) & 255) << 16) | (((i >> 16) & 255) << 8) | (((i >> 24) & 255) << 0);
    }
}
