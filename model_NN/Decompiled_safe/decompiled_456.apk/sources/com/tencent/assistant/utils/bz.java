package com.tencent.assistant.utils;

import android.text.TextUtils;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public abstract class bz {

    /* renamed from: a  reason: collision with root package name */
    private static ConcurrentHashMap<String, List<WeakReference<Object>>> f1843a;
    private static ReferenceQueue<Object> b = new ReferenceQueue<>();
    private static ConcurrentHashMap<String, WeakReference<Object>> c;
    private static ReferenceQueue<Object> d;

    static {
        f1843a = null;
        c = null;
        d = null;
        f1843a = new ConcurrentHashMap<>(5);
        c = new ConcurrentHashMap<>(5);
        d = new ReferenceQueue<>();
    }

    public static void a(String str, Object obj) {
        if (!TextUtils.isEmpty(str) && obj != null) {
            while (true) {
                Reference<? extends Object> poll = d.poll();
                if (poll != null) {
                    c.remove(poll);
                } else {
                    c.put(str, new WeakReference(obj));
                    return;
                }
            }
        }
    }

    public static <T> T a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        WeakReference weakReference = c.get(str);
        if (weakReference == null || weakReference.get() == null) {
            return null;
        }
        try {
            return weakReference.get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void b(String str, Object obj) {
        List<WeakReference> list;
        if (!TextUtils.isEmpty(str) && obj != null) {
            synchronized (f1843a) {
                List list2 = f1843a.get(str);
                if (list2 != null) {
                    while (true) {
                        Reference<? extends Object> poll = b.poll();
                        if (poll == null) {
                            break;
                        }
                        list2.remove(poll);
                    }
                }
                if (list2 == null) {
                    ArrayList arrayList = new ArrayList();
                    f1843a.put(str, arrayList);
                    list = arrayList;
                } else {
                    list = list2;
                }
                for (WeakReference weakReference : list) {
                    Object obj2 = weakReference.get();
                    if (obj2 != null && obj2.equals(obj)) {
                        return;
                    }
                }
                list.add(new WeakReference(obj, b));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void c(java.lang.String r5, java.lang.Object r6) {
        /*
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            if (r0 != 0) goto L_0x0008
            if (r6 != 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.List<java.lang.ref.WeakReference<java.lang.Object>>> r2 = com.tencent.assistant.utils.bz.f1843a
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.List<java.lang.ref.WeakReference<java.lang.Object>>> r0 = com.tencent.assistant.utils.bz.f1843a     // Catch:{ all -> 0x003c }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x003c }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x003c }
            if (r0 == 0) goto L_0x003f
            java.util.Iterator r3 = r0.iterator()     // Catch:{ all -> 0x003c }
        L_0x001a:
            boolean r1 = r3.hasNext()     // Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x003f
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x003c }
            java.lang.ref.WeakReference r1 = (java.lang.ref.WeakReference) r1     // Catch:{ all -> 0x003c }
            java.lang.Object r4 = r1.get()     // Catch:{ all -> 0x003c }
            if (r4 != r6) goto L_0x001a
            r0.remove(r1)     // Catch:{ all -> 0x003c }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x003c }
            if (r0 == 0) goto L_0x003a
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.List<java.lang.ref.WeakReference<java.lang.Object>>> r0 = com.tencent.assistant.utils.bz.f1843a     // Catch:{ all -> 0x003c }
            r0.remove(r5)     // Catch:{ all -> 0x003c }
        L_0x003a:
            monitor-exit(r2)     // Catch:{ all -> 0x003c }
            goto L_0x0008
        L_0x003c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x003c }
            throw r0
        L_0x003f:
            monitor-exit(r2)     // Catch:{ all -> 0x003c }
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.bz.c(java.lang.String, java.lang.Object):void");
    }

    public static List<WeakReference<Object>> b(String str) {
        return f1843a.get(str);
    }
}
