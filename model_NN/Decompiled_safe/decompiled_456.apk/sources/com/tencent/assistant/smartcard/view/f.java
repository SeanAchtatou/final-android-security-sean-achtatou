package com.tencent.assistant.smartcard.view;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.smartcard.d.i;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f1778a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardGiftNode c;

    f(NormalSmartCardGiftNode normalSmartCardGiftNode, i iVar, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardGiftNode;
        this.f1778a = iVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        b.a(this.c.f1771a, this.f1778a.f1755a.aa);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.updateStatusToDetail(this.f1778a.f1755a);
            this.b.recommendId = this.f1778a.f1755a.y;
        }
        return this.b;
    }
}
