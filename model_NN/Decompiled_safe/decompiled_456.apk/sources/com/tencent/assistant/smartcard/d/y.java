package com.tencent.assistant.smartcard.d;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.bm;

/* compiled from: ProGuard */
public class y {

    /* renamed from: a  reason: collision with root package name */
    public SimpleAppModel f1768a;
    public String b;
    public String c;

    public Spanned a() {
        if (TextUtils.isEmpty(this.b)) {
            return bm.f1838a;
        }
        try {
            return Html.fromHtml(this.b);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
