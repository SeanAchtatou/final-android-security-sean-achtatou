package com.tencent.assistant.smartcard.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class ao implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1716a;
    final /* synthetic */ SmartSquareCardWithUser b;

    ao(SmartSquareCardWithUser smartSquareCardWithUser, String str) {
        this.b = smartSquareCardWithUser;
        this.f1716a = str;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b.c(this.b.a(this.b.f1693a)));
        b.b(this.b.f1693a, this.f1716a, bundle);
    }
}
