package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.d.d;
import com.tencent.assistant.smartcard.d.n;

/* compiled from: ProGuard */
public class NormalSmartcardCpaAdvertiseItem extends NormalSmartcardTopicItem {
    public NormalSmartcardCpaAdvertiseItem(Context context) {
        super(context);
    }

    public NormalSmartcardCpaAdvertiseItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardCpaAdvertiseItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void f() {
        d dVar = null;
        if (this.d instanceof d) {
            dVar = (d) this.d;
        }
        if (dVar != null) {
            setOnClickListener(new q(this, dVar.b));
            this.l.setInvalidater(this.g);
            this.l.updateImageView(dVar.f1751a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            this.n.setVisibility(8);
            dVar.a(false);
            if (dVar.a()) {
                this.i.setVisibility(0);
            } else {
                this.i.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        if (this.d instanceof d) {
            return ((d) this.d).b;
        }
        return null;
    }
}
