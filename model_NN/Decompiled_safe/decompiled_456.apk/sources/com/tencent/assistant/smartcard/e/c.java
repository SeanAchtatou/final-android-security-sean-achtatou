package com.tencent.assistant.smartcard.e;

import android.util.Pair;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.d.m;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.w;
import java.util.List;

/* compiled from: ProGuard */
public class c extends z {
    public boolean a(n nVar, List<Long> list) {
        Pair<Boolean, w> b = b(nVar);
        if (nVar instanceof m) {
            if (!((m) nVar).h() && ((m) nVar).e != null && ((m) nVar).e.size() == 0) {
                return false;
            }
            if (((m) nVar).e == null || ((m) nVar).e.size() < 3) {
                return false;
            }
        }
        if (!((Boolean) b.first).booleanValue()) {
            return false;
        }
        return true;
    }
}
