package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.k;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.r;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.appdetail.process.s;

/* compiled from: ProGuard */
public class NormalSmartCardPlayerSHowItem extends NormalSmartcardBaseItem {
    private TXAppIconView i;
    private DownloadButton l;
    private TextView m;
    private ListItemInfoView n;
    private TextView o;
    private RelativeLayout p;
    private TXImageView q;

    public NormalSmartCardPlayerSHowItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardPlayerSHowItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    public NormalSmartCardPlayerSHowItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_playershow, this);
        this.i = (TXAppIconView) findViewById(R.id.app_icon_img);
        this.l = (DownloadButton) findViewById(R.id.state_app_btn);
        this.m = (TextView) findViewById(R.id.title);
        this.n = (ListItemInfoView) findViewById(R.id.download_info);
        this.o = (TextView) findViewById(R.id.desc);
        this.p = (RelativeLayout) findViewById(R.id.video_img_layout);
        this.q = (TXImageView) findViewById(R.id.video_img);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        r rVar;
        if (this.d instanceof r) {
            rVar = (r) this.d;
        } else {
            rVar = null;
        }
        if (rVar != null && rVar.d != null) {
            d e = k.e(rVar.d);
            this.i.updateImageView(rVar.d.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.m.setText(rVar.d.d);
            if (1 == (((int) (rVar.d.B >> 2)) & 3)) {
                Drawable drawable = this.f1693a.getResources().getDrawable(R.drawable.appdownload_icon_original);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                this.m.setCompoundDrawablePadding(by.b(6.0f));
                this.m.setCompoundDrawables(null, null, drawable, null);
            } else {
                this.m.setCompoundDrawables(null, null, null, null);
            }
            this.n.a(rVar.d, e);
            this.l.a(rVar.d, e);
            STInfoV2 a2 = a("03_001", 200);
            if (a2 != null) {
                a2.updateWithSimpleAppModel(rVar.d);
            }
            if (s.a(rVar.d)) {
                this.l.setClickable(false);
            } else {
                this.l.setClickable(true);
                this.l.a(a2);
            }
            this.o.setText(rVar.d.X);
            this.q.updateImageView(rVar.f1761a, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            setOnClickListener(new e(this, rVar, a2));
            this.p.setOnClickListener(new f(this, rVar, a2));
        }
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        if (this.d instanceof r) {
            return ((r) this.d).d;
        }
        return super.c();
    }
}
