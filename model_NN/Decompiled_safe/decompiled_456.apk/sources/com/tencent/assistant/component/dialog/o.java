package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f661a;
    final /* synthetic */ Dialog b;

    o(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f661a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f661a != null) {
            this.f661a.onLeftBtnClick();
            this.b.dismiss();
        }
    }
}
