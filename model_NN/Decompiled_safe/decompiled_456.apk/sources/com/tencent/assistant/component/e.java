package com.tencent.assistant.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f665a;
    final /* synthetic */ AppConst.AppState b;
    final /* synthetic */ AppStateButton c;

    e(AppStateButton appStateButton, String str, AppConst.AppState appState) {
        this.c = appStateButton;
        this.f665a = str;
        this.b = appState;
    }

    public void run() {
        String access$000 = this.c.getDownloadTicket();
        if (access$000 != null && access$000.equals(this.f665a)) {
            this.c.updateStateBtn(this.f665a, this.b);
        }
    }
}
