package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class r implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f664a;
    final /* synthetic */ Dialog b;

    r(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f664a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f664a != null) {
            this.f664a.onLeftBtnClick();
            this.b.dismiss();
        }
    }
}
