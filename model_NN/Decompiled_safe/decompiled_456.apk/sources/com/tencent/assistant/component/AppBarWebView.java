package com.tencent.assistant.component;

import android.content.Context;
import android.view.MotionEvent;
import android.webkit.WebView;
import com.jg.EType;
import com.jg.JgClassChecked;
import java.lang.reflect.Method;

@JgClassChecked(author = 71024, fComment = "确认已进行安全校验", lastDate = "20140527", level = 1, reviewer = 89182, vComment = {EType.JSEXECUTECHECK})
/* compiled from: ProGuard */
public class AppBarWebView extends WebView {
    private float distanceX;
    private float distanceY;
    private float dx;
    private float dy;
    private boolean isScrollEnable = false;
    private float lastX;
    private float lastY;
    private int mTouchSlop = 25;

    public AppBarWebView(Context context) {
        super(context);
        try {
            Method method = getClass().getMethod("removeJavascriptInterface", String.class);
            if (method != null) {
                method.invoke(this, "searchBoxJavaBridge_");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setScrollEnable(boolean z) {
        this.isScrollEnable = z;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        requestDisallowInterceptTouchEvent(true);
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.lastX = motionEvent.getRawX();
                this.lastY = motionEvent.getRawY();
                this.distanceX = 0.0f;
                this.distanceY = 0.0f;
                requestDisallowInterceptTouchEvent(true);
                break;
            case 2:
                float rawY = motionEvent.getRawY();
                float rawX = motionEvent.getRawX();
                this.dx = rawX - this.lastX;
                this.dy = rawY - this.lastY;
                this.distanceX += this.dx;
                this.distanceY += this.dy;
                this.lastX = rawX;
                this.lastY = rawY;
                if (isTop() && this.dy > 0.0f) {
                    this.isScrollEnable = false;
                    requestDisallowInterceptTouchEvent(false);
                    return false;
                } else if (this.distanceX <= ((float) this.mTouchSlop) || Math.abs(this.distanceX) <= Math.abs(this.distanceY)) {
                    requestDisallowInterceptTouchEvent(this.isScrollEnable);
                    break;
                } else {
                    requestDisallowInterceptTouchEvent(false);
                    return false;
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    private boolean isTop() {
        return getScrollY() == 0;
    }
}
