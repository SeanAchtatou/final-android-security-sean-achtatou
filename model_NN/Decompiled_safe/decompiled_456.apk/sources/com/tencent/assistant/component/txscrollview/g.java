package com.tencent.assistant.component.txscrollview;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.utils.ah;
import com.tencent.connect.common.Constants;
import java.util.HashMap;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXImageView f709a;

    g(TXImageView tXImageView) {
        this.f709a = tXImageView;
    }

    public void run() {
        String str;
        if (TextUtils.isEmpty(this.f709a.mImageUrlString)) {
            str = Constants.STR_EMPTY;
        } else {
            str = this.f709a.mImageUrlString;
        }
        Bitmap a2 = k.b().a(str, this.f709a.mImageType.getThumbnailRequestType(), this.f709a.self);
        if (this.f709a.invalidater == null) {
            ah.a().post(new h(this, str, a2));
        } else if (!TextUtils.isEmpty(str) && str.equals(this.f709a.mImageUrlString)) {
            if (a2 != null && !a2.isRecycled()) {
                Bitmap unused = this.f709a.mBitmap = a2;
            }
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(0);
            HashMap hashMap = new HashMap();
            hashMap.put("URL", str);
            hashMap.put("TYPE", Integer.valueOf(this.f709a.mImageType.getThumbnailRequestType()));
            viewInvalidateMessage.params = hashMap;
            viewInvalidateMessage.target = this.f709a.invalidateHandler;
            this.f709a.invalidater.dispatchMessage(viewInvalidateMessage);
        }
    }
}
