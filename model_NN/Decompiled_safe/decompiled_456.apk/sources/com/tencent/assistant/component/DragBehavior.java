package com.tencent.assistant.component;

import android.view.MotionEvent;
import com.tencent.assistant.component.TouchAnalizer;

/* compiled from: ProGuard */
public class DragBehavior extends TouchBehavior {
    public static final int DRAG_END = 2;
    public static final int DRAG_MOVE = 1;
    public static final int DRAG_START = 0;
    private float deltaX;
    private float deltaY;
    private int dragPoint = -1;
    private float lastX;
    private float lastY;
    private float oriX;
    private float oriY;

    public DragBehavior(TouchAnalizer touchAnalizer) {
        super(touchAnalizer);
        this.type = TouchAnalizer.BehaviorType.DRAG;
    }

    public int analizeTouchEvent(MotionEvent motionEvent) {
        int i = 3;
        switch (motionEvent.getAction()) {
            case 0:
                this.dragPoint = 0;
                float x = motionEvent.getX();
                this.lastX = x;
                this.oriX = x;
                float y = motionEvent.getY();
                this.lastY = y;
                this.oriY = y;
                this.deltaY = 0.0f;
                this.deltaX = 0.0f;
                if (!this.manager.onBehavior(TouchAnalizer.BehaviorType.DRAG, this.oriX, this.oriY, 0)) {
                    i = 0;
                }
                return i;
            case 1:
                if (!this.manager.onBehavior(TouchAnalizer.BehaviorType.DRAG, this.oriX + this.deltaX, this.oriY + this.deltaY, 2)) {
                    i = 0;
                }
                this.deltaY = 0.0f;
                this.deltaX = 0.0f;
                this.manager.pauseBehavior(TouchAnalizer.BehaviorType.DRAG);
                return i;
            case 2:
                if (this.dragPoint == 2) {
                    this.deltaX += ((motionEvent.getX() + motionEvent.getX(1)) / 2.0f) - this.lastX;
                    this.deltaY += ((motionEvent.getY() + motionEvent.getY(1)) / 2.0f) - this.lastY;
                    this.lastX = (motionEvent.getX() + motionEvent.getX(1)) / 2.0f;
                    this.lastY = (motionEvent.getY() + motionEvent.getY(1)) / 2.0f;
                } else if (this.dragPoint == 0) {
                    this.deltaX += motionEvent.getX() - this.lastX;
                    this.deltaY += motionEvent.getY() - this.lastY;
                    this.lastX = motionEvent.getX();
                    this.lastY = motionEvent.getY();
                } else if (this.dragPoint == 1) {
                    this.deltaX += motionEvent.getX() - this.lastX;
                    this.deltaY += motionEvent.getY() - this.lastY;
                    this.lastX = motionEvent.getX();
                    this.lastY = motionEvent.getY();
                }
                if (!this.manager.onBehavior(TouchAnalizer.BehaviorType.DRAG, this.oriX + this.deltaX, this.oriY + this.deltaY, 1)) {
                    i = 0;
                }
                return i;
            case 5:
            case 261:
                this.dragPoint = 2;
                this.lastX = (motionEvent.getX() + motionEvent.getX(1)) / 2.0f;
                this.lastY = (motionEvent.getY() + motionEvent.getY(1)) / 2.0f;
                return 0;
            case 6:
                this.dragPoint = 1;
                this.lastX = motionEvent.getX(1);
                this.lastY = motionEvent.getY(1);
                return 0;
            case 262:
                this.dragPoint = 0;
                this.lastX = motionEvent.getX();
                this.lastY = motionEvent.getY();
                return 0;
            default:
                return 0;
        }
    }
}
