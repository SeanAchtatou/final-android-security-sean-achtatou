package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.manager.t;
import java.util.HashMap;

/* compiled from: ProGuard */
class j extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXImageView f712a;

    j(TXImageView tXImageView) {
        this.f712a = tXImageView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        HashMap hashMap;
        if (viewInvalidateMessage != null) {
            if (viewInvalidateMessage.params != null) {
                hashMap = (HashMap) viewInvalidateMessage.params;
            } else {
                hashMap = null;
            }
            if (hashMap != null) {
                String str = (String) hashMap.get("URL");
                if (viewInvalidateMessage.what == 1) {
                    if (str.equals(this.f712a.mImageUrlString)) {
                        this.f712a.sendImageRequestToInvalidater();
                    }
                } else if (((Integer) hashMap.get("TYPE")).intValue() == this.f712a.mImageType.getThumbnailRequestType() && str.equals(this.f712a.mImageUrlString)) {
                    if (this.f712a.mBitmap == null || this.f712a.mBitmap.isRecycled()) {
                        try {
                            this.f712a.setImageResource(this.f712a.defaultResId);
                        } catch (Throwable th) {
                            t.a().b();
                        }
                        boolean unused = this.f712a.mIsLoadFinish = true;
                        this.f712a.onImageLoadFinishCallListener(this.f712a.mBitmap);
                        return;
                    }
                    try {
                        this.f712a.setImageBitmap(this.f712a.mBitmap);
                    } catch (Throwable th2) {
                        t.a().b();
                    }
                    boolean unused2 = this.f712a.mIsLoadFinish = true;
                    this.f712a.onImageLoadFinishCallListener(this.f712a.mBitmap);
                }
            }
        }
    }
}
