package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class p implements TXScrollViewBase.ISmoothScrollRunnableListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXRefreshListView f718a;

    p(TXRefreshListView tXRefreshListView) {
        this.f718a = tXRefreshListView;
    }

    public void onSmoothScrollFinished() {
        if (this.f718a.k != null) {
            this.f718a.k.onTXRefreshListViewRefresh(this.f718a.n);
        }
    }
}
