package com.tencent.assistant.component.categorydetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.TagGroup;

/* compiled from: ProGuard */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatTagHeader f644a;

    private b(FloatTagHeader floatTagHeader) {
        this.f644a = floatTagHeader;
    }

    public void onClick(View view) {
        if (((TagGroup) view.getTag()) != null && !view.equals(this.f644a.d.get(this.f644a.i))) {
            this.f644a.selectTag(((Integer) view.getTag(R.id.category_detail_btn_index)).intValue());
            if (this.f644a.h != null) {
                this.f644a.h.onClick(view);
            }
        }
    }
}
