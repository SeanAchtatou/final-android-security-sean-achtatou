package com.tencent.assistant.component.dialog;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class q implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f663a;

    q(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        this.f663a = twoBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f663a.onCancell();
    }
}
