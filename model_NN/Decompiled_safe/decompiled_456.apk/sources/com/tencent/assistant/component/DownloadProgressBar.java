package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.tencent.android.qqdownloader.R;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
public class DownloadProgressBar extends ProgressBar {
    private static final int MSG_INVALIDATE = 1;
    private static Bitmap bitmap;
    private static Bitmap cover;
    private boolean isDownloading;
    private Handler mHandler;
    private int position;

    /* compiled from: ProGuard */
    public enum ProgressDownloadType {
        DOWNLOADING,
        QUEUING,
        PAUSED,
        DEFAULT
    }

    public DownloadProgressBar(Context context) {
        this(context, null);
    }

    public DownloadProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.position = 0;
        this.isDownloading = false;
        this.mHandler = new i(this);
        setBackgroundResource(R.drawable.appdownload_progressbar_bg);
        createAniBitmap();
        setMyProgressDrawable(R.drawable.progress_bar_download);
    }

    public void setProgressAndDownloading(int i, ProgressDownloadType progressDownloadType) {
        this.isDownloading = progressDownloadType == ProgressDownloadType.DOWNLOADING;
        if (isDownloading(progressDownloadType)) {
            setProgress(i);
            setSecondaryProgress(0);
        } else {
            setSecondaryProgress(i);
            setProgress(0);
        }
        if (!this.mHandler.hasMessages(1)) {
            invalidate();
        }
    }

    private boolean isDownloading(ProgressDownloadType progressDownloadType) {
        return progressDownloadType == ProgressDownloadType.DOWNLOADING || progressDownloadType == ProgressDownloadType.QUEUING;
    }

    private void setMyProgressDrawable(int i) {
        Rect bounds = getProgressDrawable().getBounds();
        setProgressDrawable(getContext().getResources().getDrawable(i));
        getProgressDrawable().setBounds(bounds);
    }

    private void drawView(Canvas canvas) {
        int i = 5;
        Paint paint = new Paint(1);
        int width = bitmap.getWidth();
        bitmap.getHeight();
        this.position += 3;
        if (this.position > getWidth()) {
            this.position = -width;
        }
        int progress = (int) ((((double) getProgress()) / 100.0d) * ((double) getWidth()));
        if (progress > 5) {
            i = progress;
        }
        canvas.clipRect(new Rect(2, 0, i, getHeight()));
        canvas.drawBitmap(bitmap, (float) this.position, 0.0f, paint);
        canvas.drawBitmap(cover, 0.0f, -1.5f, paint);
        canvas.save(31);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.isDownloading) {
            createAniBitmap();
            if (bitmap != null && !bitmap.isRecycled() && cover != null && !cover.isRecycled()) {
                drawView(canvas);
            }
            this.mHandler.removeMessages(1);
            this.mHandler.sendEmptyMessageDelayed(1, 50);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (this.isDownloading) {
            this.mHandler.removeMessages(1);
            this.mHandler.sendEmptyMessageDelayed(1, 0);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.mHandler.removeMessages(1);
        if (DownloadProxy.a().g() == 0) {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
                bitmap = null;
            }
            if (cover != null && !cover.isRecycled()) {
                cover.recycle();
                cover = null;
            }
        }
        super.onDetachedFromWindow();
    }

    private void createAniBitmap() {
        if (bitmap == null || bitmap.isRecycled()) {
            bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.loading);
        }
        if (cover == null || cover.isRecycled()) {
            cover = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.loading_cover);
        }
    }
}
