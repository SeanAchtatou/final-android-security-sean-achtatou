package com.tencent.assistant.component;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.assistant.component.WifiTransferMediaAdapter;
import com.tencent.assistant.localres.model.LocalImage;

/* compiled from: ProGuard */
public class PhotoBackupGridViewAdapter extends ImageGridViewAdapter {
    private SparseBooleanArray arrays = new SparseBooleanArray();

    public PhotoBackupGridViewAdapter(Context context) {
        super(context);
    }

    public void checkItem(int i) {
        this.arrays.put(i, true);
        notifyDataSetChanged();
    }

    public void unCheckItem(int i) {
        this.arrays.delete(i);
        notifyDataSetChanged();
    }

    public boolean isCheckItem(int i) {
        return this.arrays.get(i);
    }

    public int getCheckedSize() {
        return this.arrays.size();
    }

    public int[] getCheckedItemIds() {
        int checkedSize = getCheckedSize();
        int[] iArr = new int[checkedSize];
        for (int i = 0; i < checkedSize; i++) {
            iArr[i] = this.arrays.keyAt(i);
        }
        return iArr;
    }

    public void checkIDs(int[] iArr) {
        if (iArr != null && iArr.length != 0) {
            for (int put : iArr) {
                this.arrays.put(put, true);
            }
            notifyDataSetChanged();
        }
    }

    public void clearChoice() {
        this.arrays.clear();
        notifyDataSetChanged();
    }

    public void removeRegister() {
        if (this.mMediaLoader != null) {
            this.mMediaLoader.unregisterListener(this);
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = createConvertView();
        }
        LocalImage localImage = (LocalImage) this.mMediaList.get(i);
        WifiTransferMediaAdapter.BaseViewHolder baseViewHolder = (WifiTransferMediaAdapter.BaseViewHolder) view.getTag();
        baseViewHolder.mCheckBox.setVisibility(8);
        baseViewHolder.mTvCheckBox.setVisibility(0);
        if (baseViewHolder != null) {
            baseViewHolder.mTvCheckBox.setSelected(isCheckItem(localImage.id));
            if (baseViewHolder.mTvCheckBox.getCompoundDrawables()[2] != null) {
                baseViewHolder.mTvCheckBox.getCompoundDrawables()[2].setAlpha(204);
            }
        }
        fillConvertView(view, localImage, i);
        return view;
    }
}
