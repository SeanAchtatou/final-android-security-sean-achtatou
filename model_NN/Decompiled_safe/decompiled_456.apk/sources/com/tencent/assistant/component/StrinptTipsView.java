package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class StrinptTipsView extends LinearLayout {
    public static final int VIEW_TYPE_BLUE = 1;
    public static final int VIEW_TYPE_GRAY = 0;

    public StrinptTipsView(Context context) {
        super(context);
        init();
    }

    public StrinptTipsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        setOrientation(0);
    }

    public void setSelect(int i, int i2, int i3) {
        removeAllViews();
        if (i2 <= i && i >= 2) {
            for (int i4 = 0; i4 < i; i4++) {
                ImageView imageView = new ImageView(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) by.a(7.0f), (int) by.a(7.0f));
                if (i3 == 1) {
                    if (i4 == i2) {
                        imageView.setBackgroundResource(R.drawable.bibei_zhishiqi_on);
                    } else {
                        imageView.setBackgroundResource(R.drawable.bibei_zhishiqi_off);
                    }
                } else if (i4 == i2) {
                    imageView.setBackgroundResource(R.drawable.banner_indicator_01);
                } else {
                    imageView.setBackgroundResource(R.drawable.banner_indicator_02);
                }
                layoutParams.leftMargin = (int) by.a(5.0f);
                layoutParams.rightMargin = (int) by.a(5.0f);
                addView(imageView, layoutParams);
            }
        }
    }
}
