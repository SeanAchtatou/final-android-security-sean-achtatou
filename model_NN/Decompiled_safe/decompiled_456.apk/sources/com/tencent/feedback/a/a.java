package com.tencent.feedback.a;

import android.content.Context;
import com.tencent.feedback.eup.jni.d;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.ao;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public final class a {
    private static a d = null;

    /* renamed from: a  reason: collision with root package name */
    private AtomicInteger f2534a = new AtomicInteger(0);
    private long b = -1;
    private Context c;

    private a(Context context) {
        this.c = context;
        Context context2 = context;
        ao.a(context).a(new d(context2, context.getFilesDir().getAbsolutePath() + "/eup", ac.c() - 604800, 10, "tomb_", ".txt"));
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (d == null) {
                d = new a(context);
            }
            aVar = d;
        }
        return aVar;
    }

    public final boolean a() {
        return this.f2534a.get() != 0;
    }
}
