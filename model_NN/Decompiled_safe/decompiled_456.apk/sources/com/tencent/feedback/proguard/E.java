package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class E extends C0008j {

    /* renamed from: a  reason: collision with root package name */
    public String f2568a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    private String d = Constants.STR_EMPTY;

    public final void a(ag agVar) {
        this.f2568a = agVar.b(0, false);
        this.b = agVar.b(1, false);
        this.c = agVar.b(2, false);
        this.d = agVar.b(3, false);
    }

    public final void a(ah ahVar) {
        if (this.f2568a != null) {
            ahVar.a(this.f2568a, 0);
        }
        if (this.b != null) {
            ahVar.a(this.b, 1);
        }
        if (this.c != null) {
            ahVar.a(this.c, 2);
        }
        if (this.d != null) {
            ahVar.a(this.d, 3);
        }
    }

    public final void a(StringBuilder sb, int i) {
    }
}
