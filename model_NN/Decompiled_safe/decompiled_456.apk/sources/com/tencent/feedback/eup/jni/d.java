package com.tencent.feedback.eup.jni;

import android.content.Context;
import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.e;
import com.tencent.feedback.proguard.an;
import com.tencent.feedback.proguard.ao;
import java.io.File;
import java.util.Collections;
import java.util.LinkedList;

/* compiled from: ProGuard */
public final class d implements an {

    /* renamed from: a  reason: collision with root package name */
    public final String f2562a;
    private String b;
    private File c;
    /* access modifiers changed from: private */
    public long d;
    private int e;
    private int f;
    private Context g;

    static /* synthetic */ int a(d dVar) {
        int i = dVar.f;
        dVar.f = i + 1;
        return i;
    }

    public d(Context context, String str, long j, int i, String str2, String str3) {
        this.c = new File(str);
        this.d = j;
        this.e = i;
        this.g = context;
        this.f2562a = str2;
        this.b = str3;
    }

    private static void a(String str, String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            g.c("rqdp{  fileNameList == null || fileNameList.length <= 0}", new Object[0]);
            return;
        }
        for (String file : strArr) {
            File file2 = new File(str, file);
            if (file2.exists() && file2.canWrite()) {
                g.b("rqdp{  file delete} %s", file2.getPath());
                file2.delete();
            }
        }
    }

    public final void d() {
    }

    public final void e() {
        g.a("on query end clear", new Object[0]);
        this.f = 0;
        if (this.c == null || !this.c.exists() || !this.c.isDirectory()) {
            Object[] objArr = new Object[1];
            objArr[0] = this.c == null ? "null" : this.c.getAbsolutePath();
            g.c("rqdp{  TombFilesCleanTask end for dir not avaliable %s}", objArr);
            return;
        }
        g.a("rqdp{ start to find native record}", new Object[0]);
        e a2 = c.a(this.g, this.c.getAbsolutePath());
        if (a2 != null) {
            g.a("found a record insert %s", a2.e());
            a2.c(true);
            if (a2.i() > this.d) {
                g.a("avail add", new Object[0]);
                com.tencent.feedback.eup.g.a(this.g, a2);
            } else {
                g.a("unavail drop", new Object[0]);
            }
        }
        c.a(this.c.getAbsolutePath());
        g.a("rqdp{  start to clean} %s.* rqdp{  in dir} %s rqdp{  which time <} %s rqdp{  and max file nums should <} %s", this.f2562a, this.c.getAbsolutePath(), Long.valueOf(this.d), Integer.valueOf(this.e));
        LinkedList linkedList = new LinkedList();
        String[] list = this.c.list(new f(this, this.f2562a.length(), this.b.length(), linkedList));
        int length = list != null ? list.length : 0;
        if (length > 0) {
            g.b("rqdp{  delete old num} %d", Integer.valueOf(length));
            a(this.c.getAbsolutePath(), list);
        }
        int i = (this.f - length) - this.e;
        int size = linkedList.size();
        if (i > 0 && size > 0) {
            g.a("rqdp{  should delete not too old file num} %d", Integer.valueOf(i));
            Collections.sort(linkedList);
            if (size <= i) {
                i = size;
            }
            String[] strArr = new String[i];
            StringBuffer stringBuffer = new StringBuffer();
            int i2 = 0;
            while (i2 < linkedList.size() && i2 < strArr.length) {
                stringBuffer.append(this.f2562a);
                stringBuffer.append(linkedList.get(i2));
                stringBuffer.append(".txt");
                strArr[i2] = stringBuffer.toString();
                stringBuffer.delete(0, stringBuffer.length());
                i2++;
            }
            g.b("rqdp{  delete not too old files} %d", Integer.valueOf(strArr.length));
            a(this.c.getAbsolutePath(), strArr);
        }
        ao.a(this.g).b(this);
        g.a("rqdp{  clean end!}", new Object[0]);
    }

    public final void f() {
        g.a("on app first run delete record file", new Object[0]);
        c.a(this.c.getAbsolutePath());
    }
}
