package com.tencent.feedback.eup;

import android.content.Context;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.b.b;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.f;
import com.tencent.feedback.common.g;
import com.tencent.feedback.common.h;
import com.tencent.feedback.eup.jni.NativeExceptionUpload;
import com.tencent.feedback.eup.jni.a;
import com.tencent.feedback.eup.jni.d;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.ao;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class c {
    public static void a(Context context, b bVar, b bVar2, boolean z, d dVar) {
        l.a(context, "10000", false, l.a(context, z), bVar2, bVar, dVar);
    }

    public static void a(Context context, String str, boolean z) {
        a(context, str, z, (List<File>) null, (File) null);
    }

    public static void a(Context context, String str, boolean z, List<File> list, File file) {
        if (com.tencent.feedback.eup.jni.b.a() != null) {
            g.a("already inited Native", new Object[0]);
            return;
        }
        if (file != null) {
            if (!NativeExceptionUpload.loadRQDNativeLib(file)) {
                g.d("load lib fail %s close native return!", file.getAbsoluteFile());
                return;
            }
            g.a("load lib sucess from specify!", new Object[0]);
        } else if (!NativeExceptionUpload.loadRQDNativeLib()) {
            g.d("load lib fail default close native return!", new Object[0]);
            return;
        } else {
            g.a("load lib sucess default!", new Object[0]);
        }
        com.tencent.feedback.eup.jni.b a2 = com.tencent.feedback.eup.jni.b.a(context);
        a2.a(str);
        NativeExceptionUpload.setmHandler(a2);
        if (file != null) {
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(file);
        }
        if (context == null || str == null) {
            g.c("rqdp{  nreg param!}", new Object[0]);
        } else {
            d r = l.l().r();
            ao.a(context).a(new d(context, str, ac.c() - ((long) (((r.d() * 24) * 3600) * 1000)), r.a(), "tomb_", ".txt"));
            g.a("add clean task to query listener", new Object[0]);
            String str2 = "/data/data/" + context.getPackageName() + "/lib/";
            if (e.a(context).A() == null) {
                g.b("no setted SO , query so!", new Object[0]);
                com.tencent.feedback.common.c.a().a(new a(context, str2, list));
            }
        }
        f.a(context);
        String d = f.d();
        f.a(context);
        NativeExceptionUpload.registEUP(str, d, Integer.parseInt(f.c()));
        NativeExceptionUpload.enableNativeEUP(true);
        if (z) {
            NativeExceptionUpload.setNativeLogMode(3);
        } else {
            NativeExceptionUpload.setNativeLogMode(5);
        }
    }

    public static d a() {
        try {
            return l.l().r();
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    public static boolean a(Context context, String str, String str2, String str3) {
        if (str == null || str2 == null) {
            return false;
        }
        e a2 = e.a(context);
        if (str3 == null) {
            str3 = Constants.STR_EMPTY;
        }
        return a2.a(str, str2, str3);
    }

    public static void a(Context context, String str) {
        if (str != null) {
            e.a(context).e(str);
        }
    }

    public static void a(boolean z) {
        l l = l.l();
        if (l != null) {
            l.a(z);
        }
    }

    public static void b(boolean z) {
        NativeExceptionUpload.enableNativeEUP(z);
    }

    public static void a(boolean z, boolean z2) {
        g.a(z ? new h() : null);
        if (z) {
            g.c("'setLogAble(boolean)' is true , so running in debug model , close it when you release!", new Object[0]);
        }
    }

    public static void b(Context context, String str) {
        e.a(context).b(str);
    }

    public static void c(Context context, String str) {
        e.a(context).f(str);
        g.b("setted version %s", str);
    }
}
