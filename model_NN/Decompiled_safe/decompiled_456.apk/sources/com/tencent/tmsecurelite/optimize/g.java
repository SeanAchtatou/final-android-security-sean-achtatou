package com.tencent.tmsecurelite.optimize;

import android.os.IBinder;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public final class g implements ISystemOptimize {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f4039a;

    g(IBinder iBinder) {
        this.f4039a = iBinder;
    }

    public IBinder asBinder() {
        return this.f4039a;
    }

    public ArrayList<DataEntity> findAppsWithCache() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            return DataEntity.readFromParcel(obtain2);
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean clearAppsCache() {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public ArrayList<DataEntity> findAppsWithAutoboot(boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            if (!z) {
                i = 1;
            }
            obtain.writeInt(i);
            this.f4039a.transact(3, obtain, obtain2, 0);
            obtain2.readException();
            return DataEntity.readFromParcel(obtain2);
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean setAutobootState(String str, boolean z) {
        int i;
        boolean z2 = true;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeString(str);
            if (z) {
                i = 0;
            } else {
                i = 1;
            }
            obtain.writeInt(i);
            this.f4039a.transact(4, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() != 0) {
                z2 = false;
            }
            return z2;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean setAutobootStates(List<String> list, boolean z) {
        int i;
        boolean z2 = true;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStringList(list);
            if (z) {
                i = 0;
            } else {
                i = 1;
            }
            obtain.writeInt(i);
            this.f4039a.transact(5, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() != 0) {
                z2 = false;
            }
            return z2;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public ArrayList<DataEntity> getAllRuningTask(boolean z) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            if (!z) {
                i = 1;
            }
            obtain.writeInt(i);
            this.f4039a.transact(6, obtain, obtain2, 0);
            obtain2.readException();
            return DataEntity.readFromParcel(obtain2);
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean killTask(String str) {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeString(str);
            this.f4039a.transact(7, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean killTasks(List<String> list) {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStringList(list);
            this.f4039a.transact(8, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int getMemoryUsage() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(12, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean isMemoryReachWarning() {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(13, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void getSysRubbishSize(a aVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) aVar);
            this.f4039a.transact(14, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void cleanSysRubbish() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(15, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean checkVersion(int i) {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeInt(i);
            this.f4039a.transact(9, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean hasRoot() {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(10, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public boolean askForRoot() {
        boolean z = false;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(11, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = true;
            }
            return z;
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void getMemoryUsageAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(27, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void findAppsWithCacheAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(16, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void clearAppsCacheAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(17, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void findAppsWithAutobootAsync(boolean z, b bVar) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            if (!z) {
                i = 1;
            }
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(18, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void setAutobootStateAsync(String str, boolean z, b bVar) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeString(str);
            if (!z) {
                i = 1;
            }
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(19, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void setAutobootStatesAsync(List<String> list, boolean z, b bVar) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStringList(list);
            if (!z) {
                i = 1;
            }
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(20, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void getAllRuningTaskAsync(boolean z, b bVar) {
        int i = 0;
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            if (!z) {
                i = 1;
            }
            obtain.writeInt(i);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(21, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void killTaskAsync(String str, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeString(str);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(22, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void killTasksAsync(List<String> list, b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStringList(list);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(23, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void isMemoryReachWarningAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(28, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int getSysRubbishSizeAsync(a aVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) aVar);
            this.f4039a.transact(29, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void cleanSysRubbishAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(30, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void hasRootAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(25, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void askForRootAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(26, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void updateTmsConfigAsync(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(31, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int startScanRubbish(b bVar) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            this.f4039a.transact(32, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public int cancelScanRubbish() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            this.f4039a.transact(33, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readInt();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void cleanRubbishAsync(b bVar, ArrayList<String> arrayList) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(ISystemOptimize.INTERFACE);
            obtain.writeStrongBinder((IBinder) bVar);
            obtain.writeStringList(arrayList);
            this.f4039a.transact(34, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
