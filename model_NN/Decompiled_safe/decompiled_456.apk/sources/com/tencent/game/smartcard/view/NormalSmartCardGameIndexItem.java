package com.tencent.game.smartcard.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.component.NormalSmartCardAppVerticalNode;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.f.b;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.smartcard.b.d;
import com.tencent.game.smartcard.b.a;
import com.tencent.game.smartcard.component.NormalSmartCardAppHorizontalNodeWithRank;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardGameIndexItem extends NormalSmartcardBaseItem {
    private RelativeLayout i;
    private TextView l;
    private TextView m;
    private TextView n;
    private LinearLayout o;
    private LinearLayout p;
    private ImageView q;
    private TextView r;
    private List<NormalSmartCardAppHorizontalNodeWithRank> s;

    public NormalSmartCardGameIndexItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        setBackgroundResource(R.drawable.common_card_normal);
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.c = inflate(this.f1693a, R.layout.smartcard_game_index_layout, this);
            this.l = (TextView) this.c.findViewById(R.id.card_title);
            this.m = (TextView) this.c.findViewById(R.id.card_sub_title);
            this.n = (TextView) this.c.findViewById(R.id.more_txt);
            this.i = (RelativeLayout) this.c.findViewById(R.id.card_title_layout);
            this.o = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
            this.p = (LinearLayout) this.c.findViewById(R.id.card_app_vertical_list_layout);
            this.q = (ImageView) this.c.findViewById(R.id.image_title);
            this.r = (TextView) this.c.findViewById(R.id.sub_mid_title);
            f();
        } catch (Resources.NotFoundException e) {
        }
    }

    private void f() {
        a aVar = (a) this.d;
        int i2 = aVar.f;
        switch (i2) {
            case 1:
            case 2:
            case 3:
            case 4:
                this.i.setVisibility(8);
                try {
                    this.q.setImageResource(getResources().getIdentifier("game_title_" + i2, "drawable", "com.tencent.android.qqdownloader"));
                } catch (OutOfMemoryError e) {
                    XLog.d("SmartCard", "has OutOfMemoryError: " + e.getMessage());
                }
                this.q.setVisibility(0);
                if (i2 == 2 && !TextUtils.isEmpty(aVar.m)) {
                    this.r.setText(aVar.m);
                    this.r.setVisibility(0);
                    break;
                } else {
                    this.r.setVisibility(8);
                    break;
                }
            default:
                this.q.setVisibility(8);
                this.r.setVisibility(8);
                if (TextUtils.isEmpty(aVar.l)) {
                    this.i.setVisibility(8);
                    break;
                } else {
                    this.l.setText(aVar.l);
                    int a2 = b.a(aVar.f());
                    if (a2 != 0) {
                        Drawable drawable = getResources().getDrawable(a2);
                        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                        this.l.setCompoundDrawables(drawable, null, null, null);
                        this.l.setCompoundDrawablePadding(by.a(this.f1693a, 7.0f));
                        this.l.setPadding(0, 0, 0, 0);
                    } else {
                        this.l.setCompoundDrawables(null, null, null, null);
                        this.l.setPadding(by.a(this.f1693a, 7.0f), 0, 0, 0);
                    }
                    this.i.setVisibility(0);
                    if (!TextUtils.isEmpty(aVar.m)) {
                        this.m.setText(aVar.m);
                        this.m.setPadding(by.a(this.f1693a, 7.0f), 0, 0, 0);
                        this.m.setVisibility(0);
                    } else {
                        this.m.setVisibility(8);
                    }
                    if (!TextUtils.isEmpty(aVar.p) && !TextUtils.isEmpty(aVar.o)) {
                        this.n.setText(aVar.p);
                        this.n.setOnClickListener(this.k);
                        this.n.setVisibility(0);
                        break;
                    } else {
                        this.n.setVisibility(8);
                        break;
                    }
                }
                break;
        }
        if (aVar.i() == 2) {
            this.o.setVisibility(8);
            a(aVar);
            return;
        }
        this.p.setVisibility(8);
        b(aVar);
    }

    private void a(a aVar) {
        int i2;
        boolean z;
        List<y> list = aVar.e;
        this.p.setVisibility(0);
        int size = list.size();
        if (size > 3) {
            i2 = 3;
        } else {
            i2 = size;
        }
        int childCount = this.p.getChildCount();
        if (childCount < i2) {
            for (int i3 = 0; i3 < i2 - childCount; i3++) {
                NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode = new NormalSmartCardAppVerticalNode(this.f1693a);
                normalSmartCardAppVerticalNode.setMinimumHeight(by.a(this.f1693a, 90.0f));
                this.p.addView(normalSmartCardAppVerticalNode, new LinearLayout.LayoutParams(-1, -2, 1.0f));
            }
        }
        int childCount2 = this.p.getChildCount();
        for (int i4 = 0; i4 < childCount2; i4++) {
            NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode2 = (NormalSmartCardAppVerticalNode) this.p.getChildAt(i4);
            if (normalSmartCardAppVerticalNode2 != null) {
                if (i4 < i2) {
                    normalSmartCardAppVerticalNode2.setVisibility(0);
                } else {
                    normalSmartCardAppVerticalNode2.setVisibility(8);
                }
            }
        }
        for (int i5 = 0; i5 < i2; i5++) {
            SimpleAppModel simpleAppModel = list.get(i5).f1768a;
            NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode3 = (NormalSmartCardAppVerticalNode) this.p.getChildAt(i5);
            Spanned a2 = list.get(i5).a();
            STInfoV2 a3 = a(list.get(i5), i5);
            int c = c(a(this.f1693a));
            if (i5 < i2 - 1) {
                z = true;
            } else {
                z = false;
            }
            normalSmartCardAppVerticalNode3.a(simpleAppModel, a2, a3, c, z, ListItemInfoView.InfoType.CATEGORY_SIZE);
        }
    }

    private void b(a aVar) {
        int i2;
        List<y> list = aVar.e;
        if (list != null) {
            this.o.setVisibility(0);
            if (list.size() >= 3) {
                int size = list.size();
                if (list.size() > 3) {
                    i2 = 3;
                } else {
                    i2 = size;
                }
                if (this.s == null) {
                    g();
                    this.s = new ArrayList(3);
                    for (int i3 = 0; i3 < i2; i3++) {
                        if (list.get(i3).f1768a != null) {
                            NormalSmartCardAppHorizontalNodeWithRank normalSmartCardAppHorizontalNodeWithRank = new NormalSmartCardAppHorizontalNodeWithRank(this.f1693a);
                            this.s.add(normalSmartCardAppHorizontalNodeWithRank);
                            normalSmartCardAppHorizontalNodeWithRank.setPadding(0, by.a(getContext(), 10.0f), 0, by.a(getContext(), 2.0f));
                            this.o.addView(normalSmartCardAppHorizontalNodeWithRank, new LinearLayout.LayoutParams(0, -2, 1.0f));
                            normalSmartCardAppHorizontalNodeWithRank.a(list.get(i3).f1768a, list.get(i3).a(), a(list.get(i3), i3), c(a(this.f1693a)), i3, aVar.g, aVar.f);
                        }
                    }
                    return;
                }
                for (int i4 = 0; i4 < i2; i4++) {
                    this.s.get(i4).a(list.get(i4).f1768a, list.get(i4).a(), a(list.get(i4), i4), c(a(this.f1693a)), i4, aVar.g, aVar.f);
                }
            }
        }
    }

    private void g() {
        this.s = null;
        this.o.removeAllViews();
    }

    private STInfoV2 a(y yVar, int i2) {
        STInfoV2 a2 = a(com.tencent.assistantv2.st.page.a.a("05", i2), 100);
        if (!(a2 == null || yVar == null)) {
            a2.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.d instanceof d) {
            return ((d) this.d).e();
        }
        return super.d(i2);
    }
}
