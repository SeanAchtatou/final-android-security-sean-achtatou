package com.tencent.game.smartcard.component;

import android.content.Context;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;

/* compiled from: ProGuard */
public class NormalSmartCardAppHorizontalNodeWithRank extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    TXImageView f2718a;
    TextView b;
    TextView c;
    DownloadButton d;
    TXDwonloadProcessBar e;
    ImageView f;
    RelativeLayout g;

    public NormalSmartCardAppHorizontalNodeWithRank(Context context) {
        this(context, null);
    }

    public NormalSmartCardAppHorizontalNodeWithRank(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        try {
            LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_horizontal_list_app_node, this);
            this.f2718a = (TXImageView) findViewById(R.id.icon);
            this.b = (TextView) findViewById(R.id.name);
            this.c = (TextView) findViewById(R.id.down_times);
            this.e = (TXDwonloadProcessBar) findViewById(R.id.progress);
            this.d = (DownloadButton) findViewById(R.id.btn);
            this.f = (ImageView) findViewById(R.id.rank_icon);
            this.g = (RelativeLayout) findViewById(R.id.title_layout);
            setOrientation(1);
            setGravity(1);
        } catch (InflateException e2) {
        }
    }

    public void a(SimpleAppModel simpleAppModel, Spanned spanned, STInfoV2 sTInfoV2, int i, int i2, boolean z, int i3) {
        if (this.f2718a != null && simpleAppModel != null) {
            this.f2718a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.b.setText(simpleAppModel.d);
            this.c.setVisibility(0);
            if (i3 == 2) {
                if (!TextUtils.isEmpty(simpleAppModel.S)) {
                    this.c.setText(simpleAppModel.S);
                } else {
                    simpleAppModel.t();
                    if (!TextUtils.isEmpty(simpleAppModel.aK.f946a)) {
                        this.c.setText(simpleAppModel.aK.f946a);
                    }
                }
            } else if (!TextUtils.isEmpty(spanned)) {
                this.c.setText(spanned);
            } else {
                simpleAppModel.t();
                if (!TextUtils.isEmpty(simpleAppModel.aK.f946a)) {
                    this.c.setText(simpleAppModel.aK.f946a);
                }
            }
            this.e.a(simpleAppModel, new View[]{this.c});
            this.d.a(simpleAppModel);
            this.d.a(sTInfoV2);
            this.f2718a.setTag(simpleAppModel.q());
            if (z && i2 >= 0 && i2 < 6) {
                int identifier = getResources().getIdentifier("common_ranking_num_" + (i2 + 1), "drawable", "com.tencent.android.qqdownloader");
                if (identifier > 0) {
                    this.f.setImageResource(identifier);
                    this.f.setVisibility(0);
                }
                ViewGroup.LayoutParams layoutParams = this.g.getLayoutParams();
                layoutParams.height = by.a(getContext(), 64.0f);
                this.g.setLayoutParams(layoutParams);
            } else if (this.f.getVisibility() == 0) {
                ViewGroup.LayoutParams layoutParams2 = this.g.getLayoutParams();
                layoutParams2.height = -2;
                this.g.setLayoutParams(layoutParams2);
                this.f.setVisibility(8);
            }
            setOnClickListener(new a(this, i, simpleAppModel, sTInfoV2));
        }
    }
}
