package com.tencent.game.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.categorydetail.FloatTagHeader;
import com.tencent.assistant.component.txscrollview.IScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.by;
import com.tencent.game.c.a;

/* compiled from: ProGuard */
public class GameCategoryDetailListPage extends RelativeLayout implements e {

    /* renamed from: a  reason: collision with root package name */
    private Context f2651a;
    private GameCategoryDetailListView b;
    private LoadingView c;
    private NormalErrorRecommendPage d;
    private LoadingView e;
    private NormalErrorRecommendPage f;
    /* access modifiers changed from: private */
    public long g = 0;
    private View.OnClickListener h = new b(this);

    public GameCategoryDetailListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
        this.f2651a = context;
    }

    public GameCategoryDetailListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
        this.f2651a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.game.component.GameCategoryDetailListView.a(com.tencent.assistant.component.categorydetail.FloatTagHeader, boolean):void
     arg types: [com.tencent.assistant.component.categorydetail.FloatTagHeader, int]
     candidates:
      com.tencent.game.component.GameCategoryDetailListView.a(com.tencent.game.component.GameCategoryDetailListView, java.util.List):void
      com.tencent.game.component.GameCategoryDetailListView.a(boolean, int):void
      com.tencent.assistant.component.txscrollview.TXRefreshListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.game.component.GameCategoryDetailListView.a(com.tencent.assistant.component.categorydetail.FloatTagHeader, boolean):void */
    public GameCategoryDetailListPage(Context context, TXScrollViewBase.ScrollMode scrollMode, a aVar, FloatTagHeader floatTagHeader) {
        super(context);
        a(context);
        this.f2651a = context;
        this.b.a(aVar);
        this.b.a(this);
        this.b.setDivider(null);
        this.b.a(floatTagHeader, false);
    }

    private void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.game_category_detail_list_page, this);
        this.b = (GameCategoryDetailListView) findViewById(R.id.applist);
        this.b.setVisibility(8);
        this.b.setOverscrollFooter(null);
        this.b.setDivider(null);
        this.b.setSelector(new ColorDrawable(0));
        this.b.setCacheColorHint(17170445);
        View view = new View(getContext());
        view.setLayoutParams(new AbsListView.LayoutParams(-1, by.a(getContext(), 4.0f)));
        view.setBackgroundColor(getResources().getColor(17170445));
        this.b.addHeaderView(view);
        this.c = (LoadingView) findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.e = (LoadingView) findViewById(R.id.sub_loading_view);
        this.e.setVisibility(8);
        this.d = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.h);
        this.f = (NormalErrorRecommendPage) findViewById(R.id.error_page2);
        this.f.setButtonClickListener(this.h);
    }

    public ListView a() {
        return this.b.getListView();
    }

    public void a(BaseAdapter baseAdapter) {
        this.b.setAdapter(baseAdapter);
    }

    public void a(IScrollListener iScrollListener) {
        this.b.setIScrollerListener(iScrollListener);
    }

    public void a(long j) {
        this.g = j;
        this.b.a(j);
    }

    public void a(View.OnClickListener onClickListener) {
        this.b.a(onClickListener);
    }

    public void b(View.OnClickListener onClickListener) {
        this.b.b(onClickListener);
    }

    public void a(int i) {
        this.c.setVisibility(8);
        this.b.setVisibility(8);
        this.d.setVisibility(0);
        this.d.setErrorType(i);
    }

    public void b() {
        this.b.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void c() {
    }

    public void d() {
        this.c.setVisibility(0);
        this.b.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void e() {
        this.e.setVisibility(0);
        this.f.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void b(int i) {
        this.e.setVisibility(8);
        this.f.setVisibility(0);
        this.f.setErrorType(i);
    }

    public void f() {
        this.e.setVisibility(8);
        this.f.setVisibility(8);
        this.d.setVisibility(8);
        this.b.setVisibility(0);
    }

    public void g() {
        this.b.recycleData();
    }

    public void h() {
        this.b.g();
    }

    public void i() {
        this.b.q();
    }

    public void j() {
        if (this.b != null) {
            this.b.b(false);
        }
    }

    public void k() {
        if (this.b != null) {
            this.b.s();
        }
    }
}
