package com.tencent.game.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.game.d.a.d;
import com.tencent.game.smartcard.b.b;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class k implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankAggregationListView f2665a;

    k(GameRankAggregationListView gameRankAggregationListView) {
        this.f2665a = gameRankAggregationListView;
    }

    public void a(int i, int i2, List<b> list, int i3) {
        if (this.f2665a.y != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f2665a.E);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put(0, list);
            hashMap.put(1, Integer.valueOf(i3));
            viewInvalidateMessage.params = hashMap;
            this.f2665a.y.sendMessage(viewInvalidateMessage);
        }
    }
}
