package com.tencent.game.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.game.d.a.a;
import com.tencent.game.d.j;
import java.util.List;

/* compiled from: ProGuard */
public class GameCategoryListView extends TXGetMoreListView {
    private final int A = 1;
    private final int B = 2;
    private final int C = 0;
    private final int D = 1;
    private final int E = 2;
    private a F = new g(this);
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler G = new h(this);
    private j u = null;
    /* access modifiers changed from: private */
    public long v = 0;
    /* access modifiers changed from: private */
    public i w;
    private int x = 3;
    /* access modifiers changed from: private */
    public AppCategoryListAdapter y;
    /* access modifiers changed from: private */
    public ViewPageScrollListener z;

    public void a(i iVar) {
        this.w = iVar;
    }

    public GameCategoryListView(Context context) {
        super(context);
        g();
        setDivider(null);
    }

    public GameCategoryListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        g();
        setDivider(null);
    }

    public GameCategoryListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context);
        g();
        setDivider(null);
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        this.z = viewPageScrollListener;
    }

    public void g() {
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.y = (AppCategoryListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.y = (AppCategoryListAdapter) ((ListView) this.s).getAdapter();
        }
    }

    public void q() {
    }

    public void recycleData() {
        super.recycleData();
        if (this.u != null) {
            this.u.unregister(this.F);
        }
    }

    public void r() {
        if (this.y == null) {
            g();
        }
        if (this.y == null || this.y.getCount() <= 0) {
            s();
            this.u.b();
            return;
        }
        this.z.sendMessage(new ViewInvalidateMessage(2, null, this.G));
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        if (i2 == 0) {
            this.w.a();
            if (!(this.y == null || list3 == null)) {
                this.y.a(list, list2, list3);
                l.a((int) STConst.ST_PAGE_GAME_CATEGORY, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
            }
        } else {
            l.a((int) STConst.ST_PAGE_GAME_CATEGORY, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                this.w.a(30);
                return;
            } else if (this.x <= 0) {
                this.w.a(20);
                return;
            } else {
                s();
                this.u.c();
                this.x--;
            }
        }
        if (this.y == null) {
            g();
        }
        if (this.y != null) {
            this.y.notifyDataSetChanged();
        }
    }

    public void a(long j) {
        this.v = j;
    }

    public ListView getListView() {
        return (ListView) this.s;
    }

    public void a(j jVar) {
        if (!(jVar == null || this.u == null)) {
            this.u = jVar;
        }
        s();
    }

    private void s() {
        if (this.u == null) {
            this.u = new j();
            this.u.register(this.F);
        }
    }
}
