package com.tencent.game.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.a;
import com.tencent.connect.common.Constants;
import com.tencent.game.component.GameRankNormalListView;
import com.tencent.game.component.s;
import com.tencent.pangu.adapter.RankNormalListAdapter;

/* compiled from: ProGuard */
public class n extends a implements UIEventListener, s {
    protected View.OnClickListener R = new o(this);
    private final String S = "GameBandNetFragment:";
    private LoadingView T;
    private NormalErrorRecommendPage U;
    /* access modifiers changed from: private */
    public GameRankNormalListView V;
    private com.tencent.game.d.a W = null;
    /* access modifiers changed from: private */
    public RankNormalListAdapter X = null;
    private long Y;
    private long Z;
    private long aa;
    private long ab;
    private ListViewScrollListener ac = new p(this);
    private ApkResCallback.Stub ad = new q(this);

    public n() {
    }

    public n(Activity activity) {
        super(activity);
    }

    public void d(Bundle bundle) {
        super.d(bundle);
        b((int) R.layout.gameband_normal_fragment);
        H();
        this.V.b(true);
    }

    public void H() {
        this.V = (GameRankNormalListView) c((int) R.id.applist);
        this.V.setTopPaddingSize(6);
        this.V.setVisibility(8);
        this.V.G = true;
        this.V.a((a) this);
        this.T = (LoadingView) c((int) R.id.loading_view);
        this.T.setVisibility(0);
        this.U = (NormalErrorRecommendPage) c((int) R.id.error_page);
        this.U.setButtonClickListener(this.R);
        this.U.setIsAutoLoading(true);
        this.Y = b().getLong("subId");
        this.Z = b().getLong("subAppListType");
        this.aa = b().getLong("subPageSize");
        this.ab = b().getLong("flag");
        this.W = new com.tencent.game.d.a(this.Y, (int) this.Z, (short) ((int) this.aa));
        this.V.a(this.W);
        this.V.a((s) this);
        this.V.setDivider(null);
        this.V.a(this.ac);
        this.X = new RankNormalListAdapter(this.P, this.V, this.W.a());
        this.X.a(G(), -100);
        this.X.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        this.X.a(a((byte) ((int) this.ab)));
        this.X.a(I());
        this.V.s();
        this.V.setAdapter(this.X);
        this.X.a(this.ac);
    }

    public String I() {
        return Constants.STR_EMPTY;
    }

    public void d(boolean z) {
        this.V.r();
        this.X.notifyDataSetChanged();
        ApkResourceManager.getInstance().registerApkResCallback(this.ad);
    }

    public void k() {
        super.k();
        this.X.b();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.ad);
    }

    public void n() {
        super.n();
        this.X.b();
        this.V.recycleData();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.ad);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW, this);
    }

    public void e(boolean z) {
        if (z) {
            this.X.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        } else {
            this.X.a(RankNormalListAdapter.ListType.LISTTYPENORMAL);
        }
    }

    public void f(boolean z) {
        this.X.a(z);
    }

    public void d(int i) {
        this.X.b(i);
    }

    public void D() {
        Log.e("YYB5_0", "GameBandNetFragment:onPageTurnBackground------------2:::");
    }

    public int E() {
        return 0;
    }

    public int F() {
        return 2;
    }

    public void J() {
        this.T.setVisibility(0);
        this.V.setVisibility(8);
        this.U.setVisibility(8);
    }

    public void e(int i) {
        this.T.setVisibility(8);
        this.V.setVisibility(8);
        this.U.setVisibility(0);
        this.U.setErrorType(i);
    }

    public void K() {
        this.V.setVisibility(0);
        this.U.setVisibility(8);
        this.T.setVisibility(8);
    }

    public void L() {
    }

    private boolean a(byte b) {
        return (b & 1) == 1;
    }

    private void N() {
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW:
                N();
                return;
            default:
                return;
        }
    }

    public void C() {
        B();
    }

    public int G() {
        return STConst.ST_PAGE_GAME_RANKING;
    }

    public String M() {
        return "08";
    }
}
