package com.tencent.downloadsdk.a;

import com.tencent.downloadsdk.storage.a.f;
import java.util.ArrayList;
import java.util.Iterator;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public String f2452a;
    public long b;
    public long c;
    public long d;
    public long e;
    private ArrayList<Long> f = new ArrayList<>();
    private ArrayList<b> g = new ArrayList<>();
    private long h;
    private f i = new f();

    public c(String str) {
        this.f2452a = str;
        this.i.a(this);
    }

    public static void a(String str) {
        new f().a(str);
    }

    public static String b(long j) {
        return c(j) + "KB/s";
    }

    public static double c(long j) {
        return ((double) Math.round((((double) j) / 1024.0d) * 100.0d)) / 100.0d;
    }

    public synchronized b a(long j, int i2) {
        b bVar;
        int indexOf = this.f.indexOf(Long.valueOf(j));
        if (indexOf == -1) {
            bVar = new b((long) i2);
            this.f.add(Long.valueOf(j));
            this.g.add(bVar);
        } else {
            bVar = this.g.get(indexOf);
        }
        return bVar;
    }

    public void a() {
        this.h = System.currentTimeMillis();
        if (this.b == 0) {
            this.b = this.h;
        }
    }

    public void a(long j) {
        if (this.h != 0) {
            this.c = System.currentTimeMillis();
            this.d = (this.c - this.h) + this.d;
            this.e = j;
            this.i.a(this.f2452a, j, this.h, this.c, this.d, d());
        }
    }

    public void b() {
        this.h = 0;
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.i.a(this.f2452a);
    }

    public long c() {
        long j = 0;
        Iterator<b> it = this.g.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            j = j2 + it.next().e;
        }
    }

    public long d() {
        if (this.d > 0) {
            return (this.e * 1000) / this.d;
        }
        return 0;
    }
}
