package com.tencent.pangu.component;

import android.view.animation.Animation;
import android.widget.TextView;

/* compiled from: ProGuard */
class n implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetailView f3684a;
    private TextView b;

    public n(CommentDetailView commentDetailView, TextView textView) {
        this.f3684a = commentDetailView;
        this.b = textView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.b.setVisibility(8);
    }

    public void onAnimationRepeat(Animation animation) {
    }
}
