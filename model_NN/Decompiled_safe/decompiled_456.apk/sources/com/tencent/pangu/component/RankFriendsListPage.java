package com.tencent.pangu.component;

import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.b;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
public class RankFriendsListPage extends RankNormalListPage implements UIEventListener, NetworkMonitor.ConnectivityChangeListener {
    private RankFriendsLoginErrorView g;
    private APN h = APN.NO_NETWORK;

    public RankFriendsListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public RankFriendsListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public RankFriendsListPage(Context context, TXScrollViewBase.ScrollMode scrollMode, b bVar) {
        super(context, scrollMode, bVar);
    }

    public int a() {
        if (this.g == null || this.g.getVisibility() != 0) {
            return STConst.ST_PAGE_RANK_FRIENDS;
        }
        return STConst.ST_PAGE_RANK_FRIENDS_LOGIN;
    }

    public void a(Context context) {
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.rankfriendslist_component_view, this);
        this.e = (RankFriendsListView) inflate.findViewById(R.id.friends_applist);
        this.e.setVisibility(8);
        this.e.setDivider(null);
        this.e.setTopPaddingSize(0);
        this.c = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) inflate.findViewById(R.id.friends_error_page);
        this.d.setActivityPageId(STConst.ST_PAGE_RANK_FRIENDS);
        this.d.setErrorText(this.f3498a.getString(R.string.rank_friends_empty_text));
        this.d.setErrorImage(R.drawable.emptypage_pic_06);
        this.d.setVisibility(8);
        this.d.setButtonClickListener(this.f);
        this.g = (RankFriendsLoginErrorView) inflate.findViewById(R.id.loginErrorPage);
        t.a().a(this);
    }

    public void b() {
        e();
        j();
    }

    private void i() {
        this.d.setVisibility(8);
        this.c.setVisibility(8);
        this.e.setVisibility(8);
        this.g.setVisibility(0);
        this.g.a();
    }

    public void c() {
        super.c();
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
    }

    public void d() {
        super.d();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGOUT, this);
    }

    public void a(int i) {
        if (i == 10) {
            super.a(90);
        } else {
            super.a(i);
        }
        this.g.setVisibility(8);
    }

    public void e() {
        super.e();
        this.g.setVisibility(8);
    }

    public void f() {
        super.f();
        this.g.setVisibility(8);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                j();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGOUT:
                i();
                return;
            default:
                return;
        }
    }

    private void j() {
        if (!c.a()) {
            a(30);
        } else if (j.a().j()) {
            this.g.setVisibility(8);
            this.e.b(true);
        } else {
            this.e.v();
            i();
        }
    }

    public void onConnected(APN apn) {
        XLog.d("leobi", "app onConnected" + this.e.w());
        if (this.e.w() == null || this.e.w().getCount() <= 0) {
            b();
        }
    }

    public void onDisconnected(APN apn) {
        this.h = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.h = apn2;
    }
}
