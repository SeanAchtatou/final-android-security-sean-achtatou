package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/* compiled from: ProGuard */
public class InnerScrollView extends ScrollView implements bd {

    /* renamed from: a  reason: collision with root package name */
    private float f3555a = 10.0f;
    private be b;
    private bf c;

    public void a(be beVar) {
        this.b = beVar;
    }

    public void a(bf bfVar) {
        this.c = bfVar;
    }

    public InnerScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    public InnerScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public InnerScrollView(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent == null) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean a() {
        return getScrollY() != 0;
    }

    public void a(int i) {
        scrollTo(getScrollX(), getScrollY() - i);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        if (getChildAt(getChildCount() - 1).getBottom() - (getHeight() + getScrollY()) > 0 || this.b == null) {
            super.onScrollChanged(i, i2, i3, i4);
        } else {
            this.b.d();
        }
    }

    public void a(boolean z) {
        if (this.c != null) {
            this.c.a(z);
        }
    }

    public void b(int i) {
        post(new bg(this, i));
    }
}
