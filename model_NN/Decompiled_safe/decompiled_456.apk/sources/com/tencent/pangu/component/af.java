package com.tencent.pangu.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.game.activity.n;
import com.tencent.nucleus.manager.component.SwitchButton;

/* compiled from: ProGuard */
class af extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankCustomizeListView f3515a;

    af(RankCustomizeListView rankCustomizeListView) {
        this.f3515a = rankCustomizeListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f3515a.u++;
        if (this.f3515a.u < 10) {
            int i = this.f3515a.u - 1;
            this.f3515a.l[i] = true;
            if (i > 0) {
                this.f3515a.l[i - 1] = false;
            }
            this.f3515a.h.postDelayed(new ag(this, i), 2000);
            if (this.f3515a.u == 5) {
                this.f3515a.g.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f3515a.u > 5) {
                this.f3515a.k.setText(RankCustomizeListView.G[this.f3515a.u % 5]);
                this.f3515a.k.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f3515a.h;
            if (this.f3515a.h.b()) {
                z = false;
            }
            switchButton.b(z);
            m.a().t(this.f3515a.h.b());
            a.a().b().a(this.f3515a.h.b());
            if (this.f3515a.h.b()) {
                Toast.makeText(this.f3515a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else {
                Toast.makeText(this.f3515a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel, 4).show();
            }
            this.f3515a.z.a();
            if (this.f3515a.z.getGroupCount() > 0) {
                for (int i2 = 0; i2 < this.f3515a.z.getGroupCount(); i2++) {
                    this.f3515a.expandGroup(i2);
                }
                this.f3515a.updateFootViewText();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.f3515a.w.G(), "08", this.f3515a.w.G(), "08", 200);
        if (!this.f3515a.v || !(this.f3515a.w instanceof n)) {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a("08", 0);
        } else {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a(((n) this.f3515a.w).M(), 0);
        }
        if (!a.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
