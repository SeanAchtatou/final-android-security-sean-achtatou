package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.graphics.Paint;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class ExpandableTextViewV5 extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private Context f3549a;
    private int b = -1;
    private boolean c = true;
    private Paint d = getPaint();
    private int e = 1;
    private CharSequence f;

    public ExpandableTextViewV5(Context context) {
        super(context);
        this.f3549a = context;
        setMaxLines(this.e);
        setEllipsize(TextUtils.TruncateAt.valueOf("END"));
    }

    public ExpandableTextViewV5(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3549a = context;
        setMaxLines(this.e);
        setEllipsize(TextUtils.TruncateAt.valueOf("END"));
    }

    public ExpandableTextViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3549a = context;
        setMaxLines(this.e);
        setEllipsize(TextUtils.TruncateAt.valueOf("END"));
    }

    public void a(boolean z) {
        if (TextUtils.isEmpty(this.f) && getText() != null && !getText().equals("暂无")) {
            this.f = getText();
        }
        this.c = !z;
        if (z) {
            setMaxLines(this.e);
            if (this.f != null) {
                if (this.f.toString().startsWith(this.f3549a.getString(R.string.update_info_description_txt))) {
                    SpannableString spannableString = new SpannableString(this.f.toString().replace("\n", Constants.STR_EMPTY));
                    spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.appdetail_update_info_title)), 0, 5, 33);
                    setText(spannableString);
                    return;
                }
                setText(this.f.toString().replace("\n", Constants.STR_EMPTY));
                return;
            }
            return;
        }
        setText(this.f);
        setMaxLines(100);
    }

    public boolean a() {
        return !this.c;
    }

    public boolean b() {
        if (this.f == null || TextUtils.isEmpty(this.f) || getText() == null || getText().equals("暂无")) {
            return false;
        }
        if (this.f.toString().split("\n").length >= 3 || this.f.length() >= 55) {
            return true;
        }
        return false;
    }
}
