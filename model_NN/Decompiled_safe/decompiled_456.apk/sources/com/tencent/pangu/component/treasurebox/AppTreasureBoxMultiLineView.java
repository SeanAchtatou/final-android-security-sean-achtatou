package com.tencent.pangu.component.treasurebox;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import com.tencent.android.qqdownloader.b;

/* compiled from: ProGuard */
public class AppTreasureBoxMultiLineView extends View {

    /* renamed from: a  reason: collision with root package name */
    private final String f3731a = "TTDashLineView";
    private Paint b;
    private int c;
    private int d;
    private int e;
    private final int f = 10526880;
    private final int g = 2;
    private final int h = 20;

    public AppTreasureBoxMultiLineView(Context context) {
        super(context);
        a(context, null);
    }

    public AppTreasureBoxMultiLineView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.c = 10526880;
        this.d = 2;
        this.e = 20;
        this.d = (int) TypedValue.applyDimension(1, 2.0f, displayMetrics);
        this.e = (int) TypedValue.applyDimension(1, 20.0f, displayMetrics);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.k);
        this.c = obtainStyledAttributes.getColor(0, this.c);
        this.d = obtainStyledAttributes.getDimensionPixelSize(1, this.d);
        this.e = obtainStyledAttributes.getDimensionPixelSize(2, this.e);
        obtainStyledAttributes.recycle();
        this.b = new Paint();
        this.b.setColor(this.c);
        this.b.setAntiAlias(true);
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth((float) this.d);
    }

    public void onDraw(Canvas canvas) {
        float f2;
        float f3;
        super.onDraw(canvas);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < getWidth() + getHeight()) {
                float width = i2 > getWidth() ? (float) getWidth() : (float) i2;
                if (i2 > getWidth()) {
                    f2 = (float) (i2 - getWidth());
                } else {
                    f2 = 0.0f;
                }
                if (i2 > getHeight()) {
                    f3 = (float) (i2 - getHeight());
                } else {
                    f3 = 0.0f;
                }
                canvas.drawLine(width, f2, f3, i2 > getHeight() ? (float) getHeight() : (float) i2, this.b);
                i = this.e + i2;
            } else {
                return;
            }
        }
    }
}
