package com.tencent.pangu.component.search;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.module.h;

/* compiled from: ProGuard */
public class SearchHotwordsItem extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f3694a;
    private TextView b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public AdvancedHotWord d;
    private SimpleAppModel e;
    private final int[] f;
    private final int[] g;
    /* access modifiers changed from: private */
    public g h;
    private String i;
    private View.OnClickListener j;

    public SearchHotwordsItem(Context context) {
        this(context, null);
    }

    public SearchHotwordsItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = new int[]{-1, R.drawable.search_hot_word_icon_soso, R.drawable.search_hot_word_icon_libao, R.drawable.search_hot_word_icon_biaosheng, R.drawable.search_hot_word_icon_jingpin, R.drawable.search_hot_word_icon_bibei};
        this.g = new int[]{R.color.search_hot_word_txt_normal, R.color.search_hot_word_txt_soso, R.color.search_hot_word_txt_libao, R.color.search_hot_word_txt_biaosheng, R.color.search_hot_word_txt_jingpin, R.color.search_hot_word_txt_bibei};
        this.i = STConst.ST_STATUS_DEFAULT;
        this.j = new f(this);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        inflate(context, R.layout.search_hot_words_item, this);
        setGravity(17);
        setOrientation(0);
        setBackgroundResource(R.drawable.bg_card_selector);
        int a2 = by.a(getContext(), 8.0f);
        setPadding(a2, 0, a2, 0);
        this.f3694a = (ImageView) findViewById(R.id.app_icon);
        this.b = (TextView) findViewById(R.id.app_hotwords);
        setOnClickListener(this.j);
    }

    public void a(int i2, AdvancedHotWord advancedHotWord) {
        this.c = i2;
        this.d = advancedHotWord;
        a();
    }

    public void a(g gVar) {
        this.h = gVar;
    }

    private void a() {
        boolean a2 = h.a().a(this.d);
        if (this.d != null) {
            this.e = k.a(this.d.d);
            if (this.d.b != 2) {
                this.b.setText(this.d.f1126a);
            } else if (this.e != null) {
                this.b.setText(this.e.d);
            }
            int i2 = this.d.i;
            if (i2 <= 0 || i2 >= this.f.length) {
                this.f3694a.setVisibility(8);
                this.b.setTextColor(getResources().getColorStateList(this.g[0]));
            } else {
                this.f3694a.setVisibility(0);
                try {
                    this.f3694a.setImageResource(this.f[i2]);
                } catch (Throwable th) {
                    t.a().b();
                }
                this.b.setTextColor(getResources().getColorStateList(this.g[i2]));
            }
            switch (i2) {
                case 0:
                    this.i = a2 ? "01" : "03";
                    break;
                case 1:
                    this.i = a2 ? STConst.ST_STATUS_DEFAULT : "07";
                    break;
                case 2:
                    this.i = a2 ? "02" : STConst.ST_STATUS_DEFAULT;
                    break;
                case 3:
                    this.i = a2 ? STConst.ST_STATUS_DEFAULT : "05";
                    break;
                case 4:
                    this.i = a2 ? STConst.ST_STATUS_DEFAULT : "06";
                    break;
                case 5:
                    this.i = a2 ? STConst.ST_STATUS_DEFAULT : "04";
                    break;
            }
        }
        a(100);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (getContext() instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), i2);
            buildSTInfo.slotId = a.a("04", this.c);
            buildSTInfo.status = this.i;
            l.a(buildSTInfo);
        }
    }
}
