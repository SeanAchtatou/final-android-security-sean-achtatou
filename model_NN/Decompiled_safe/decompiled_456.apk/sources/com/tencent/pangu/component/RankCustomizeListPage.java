package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.y;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.a;
import com.tencent.pangu.adapter.al;

/* compiled from: ProGuard */
public class RankCustomizeListPage extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener, ai {

    /* renamed from: a  reason: collision with root package name */
    private Context f3496a = null;
    private LayoutInflater b = null;
    private LoadingView c;
    private NormalErrorRecommendPage d;
    private RankCustomizeListView e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    private View.OnClickListener h = new z(this);
    private APN i = APN.NO_NETWORK;

    public RankCustomizeListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3496a = context;
        a(context);
    }

    public RankCustomizeListPage(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f3496a = context;
        a(context);
    }

    public RankCustomizeListPage(Context context, y yVar, int i2, int i3) {
        super(context);
        this.f3496a = context;
        a(context);
        this.e.a(yVar, i2, i3);
        this.e.a(this);
        this.f = i2;
        this.g = i3;
    }

    private void a(Context context) {
        t.a().a(this);
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.rank_customize_component_view, this);
        this.e = (RankCustomizeListView) inflate.findViewById(R.id.applist);
        this.e.setVisibility(8);
        this.c = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.h);
    }

    public void a(int i2) {
        this.c.setVisibility(8);
        this.e.setVisibility(8);
        this.d.setVisibility(0);
        this.d.setErrorType(i2);
    }

    public void a() {
        this.c.setVisibility(0);
        this.e.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void a(al alVar, ListViewScrollListener listViewScrollListener) {
        this.e.k();
        this.e.setAdapter(alVar);
        this.e.a(listViewScrollListener);
        alVar.a(listViewScrollListener);
    }

    public void a(int i2, int i3) {
        a();
        this.e.c(i2, i3);
    }

    public void b() {
        this.e.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void c() {
    }

    public void d() {
        this.e.d();
    }

    public void e() {
        if (this.e != null) {
            this.e.g();
        }
    }

    public int f() {
        return STConst.ST_PAGE_RANK_RECOMMEND;
    }

    public void onConnected(APN apn) {
        if (this.e.m() == null || this.e.m().getGroupCount() <= 0) {
            a(this.f, this.g);
        }
    }

    public void onDisconnected(APN apn) {
        this.i = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.i = apn2;
    }

    public boolean g() {
        if (this.e != null) {
            return this.e.n();
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.RankCustomizeListView.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.pangu.component.RankCustomizeListView.a(boolean, boolean):void */
    public void a(boolean z) {
        if (this.e != null) {
            this.e.a(z, false);
        }
    }

    public void h() {
        if (this.e != null) {
            this.e.l();
        }
    }

    public void a(a aVar) {
        if (this.e != null) {
            this.e.a(aVar);
        }
    }
}
