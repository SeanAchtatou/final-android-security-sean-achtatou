package com.tencent.pangu.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.socialcontact.comment.CommentReplyListActivity;

/* compiled from: ProGuard */
class k extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f3682a;
    final /* synthetic */ CommentDetailView b;

    k(CommentDetailView commentDetailView, View view) {
        this.b = commentDetailView;
        this.f3682a = view;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.d, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL_COMMENT;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = this.b.M + ((String) view.getTag(R.id.tma_st_slot_tag));
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommentReplyListActivity.n, (CommentDetail) this.f3682a.getTag(R.id.comment_page_detail));
        bundle.putLong(CommentReplyListActivity.u, this.b.r.f938a);
        bundle.putLong(CommentReplyListActivity.v, this.b.t);
        bundle.putInt(CommentReplyListActivity.x, this.b.v);
        bundle.putString(CommentReplyListActivity.w, this.b.r.c);
        if (this.b.p != null) {
            this.b.p.a(bundle);
        }
    }
}
