package com.tencent.pangu.component.homeEntry;

import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.aj;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f3678a;

    f(e eVar) {
        this.f3678a = eVar;
    }

    public void run() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3678a.f3677a.d, 100);
        buildSTInfo.scene = STConst.ST_PAGE_COMPETITIVE;
        if (this.f3678a.f3677a.f3672a != null) {
            aj.a(this.f3678a.f3677a.f3672a.e, buildSTInfo);
        }
        b.getInstance().exposure(buildSTInfo);
    }
}
