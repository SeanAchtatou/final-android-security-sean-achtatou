package com.tencent.pangu.component;

import android.app.Dialog;
import android.content.DialogInterface;
import com.tencent.assistant.component.SecondNavigationTitleView;

/* compiled from: ProGuard */
class av implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f3630a;
    final /* synthetic */ RecommandApplinkDialog b;

    av(RecommandApplinkDialog recommandApplinkDialog, Dialog dialog) {
        this.b = recommandApplinkDialog;
        this.f3630a = dialog;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f3630a.dismiss();
        this.b.a(SecondNavigationTitleView.TMA_ST_NAVBAR_HOME_TAG, 200, this.b.b.c);
    }
}
