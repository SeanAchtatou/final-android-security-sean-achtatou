package com.tencent.pangu.component.banner;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class i extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3643a;
    final /* synthetic */ int b;
    final /* synthetic */ h c;

    i(h hVar, Context context, int i) {
        this.c = hVar;
        this.f3643a = context;
        this.b = i;
    }

    public void onTMAClick(View view) {
        b.a(this.f3643a, this.c.f3641a.e);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3643a, 200);
        buildSTInfo.slotId = a.a(this.c.b, this.b);
        return buildSTInfo;
    }
}
