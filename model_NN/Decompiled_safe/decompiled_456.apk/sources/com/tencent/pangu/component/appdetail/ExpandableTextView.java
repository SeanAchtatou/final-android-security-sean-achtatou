package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.by;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class ExpandableTextView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private final int f3548a = 35;
    private final int b = 5;
    private Context c;
    private int d = -1;
    private boolean e = true;
    private Paint f = getPaint();
    private int g = 2;
    private String h;

    public ExpandableTextView(Context context) {
        super(context);
        this.c = context;
    }

    public ExpandableTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.c = context;
    }

    public ExpandableTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.h = this.c.getString(R.string.update_info_description_txt);
        this.f.setColor(getCurrentTextColor());
        this.f.setTextSize(getTextSize());
        canvas.translate((float) getPaddingLeft(), ((float) getPaddingTop()) - this.f.getFontMetrics().descent);
        int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
        int measuredWidth2 = ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()) - a();
        Paint.FontMetrics fontMetrics = this.f.getFontMetrics();
        int ceil = (int) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
        int i = 0;
        String obj = getText().toString();
        if (!this.e) {
            obj = obj.replaceAll("\n", Constants.STR_EMPTY);
        }
        String str = obj;
        int i2 = 0;
        while (!TextUtils.isEmpty(str)) {
            if (str.indexOf("\n") == 0) {
                str = str.substring(1, str.length());
            } else if (this.e) {
                i += ceil;
                if (i2 != 0) {
                    i += by.a(this.c, 5.0f);
                }
                int breakText = this.f.breakText(str, true, (float) measuredWidth, null);
                String substring = str.substring(0, breakText);
                int indexOf = substring.indexOf("\n");
                i2++;
                if (indexOf == -1) {
                    str = str.substring(breakText, str.length());
                    if (TextUtils.isEmpty(str)) {
                        if (this.f.measureText(substring) <= ((float) measuredWidth2)) {
                            canvas.drawText(substring, 0.0f, (float) i, this.f);
                            return;
                        }
                        canvas.drawText(substring, 0.0f, (float) i, this.f);
                        int a2 = i + ceil + by.a(this.c, 5.0f);
                        return;
                    } else if (i2 != 1 || !str.startsWith(this.h)) {
                        canvas.drawText(substring, 0.0f, (float) i, this.f);
                    } else {
                        this.f.setColor(getResources().getColor(R.color.appdetail_update_info_title));
                        canvas.drawText(this.h, 0.0f, (float) i, this.f);
                        this.f.setColor(getCurrentTextColor());
                    }
                } else if (i2 != 1 || !str.startsWith(this.h)) {
                    String substring2 = str.substring(0, indexOf);
                    str = str.substring(indexOf + 1, str.length());
                    canvas.drawText(substring2, 0.0f, (float) i, this.f);
                } else {
                    str.substring(0, indexOf);
                    str = str.substring(indexOf + 1, str.length());
                    this.f.setColor(getResources().getColor(R.color.appdetail_update_info_title));
                    canvas.drawText(this.h, 0.0f, (float) i, this.f);
                    this.f.setColor(getCurrentTextColor());
                }
            } else {
                int i3 = i + ceil;
                if (i2 != 0) {
                    i3 += by.a(this.c, 5.0f);
                }
                int breakText2 = this.f.breakText(str, true, (float) measuredWidth, null);
                String substring3 = str.substring(0, breakText2);
                i2++;
                str = str.substring(breakText2, str.length());
                if (TextUtils.isEmpty(str) || i2 >= this.g) {
                    if (this.f.measureText(substring3) > ((float) measuredWidth2)) {
                        canvas.drawText(substring3.substring(0, this.f.breakText(substring3, true, (float) measuredWidth2, null)) + "...", 0.0f, (float) i, this.f);
                        return;
                    } else if (!TextUtils.isEmpty(str)) {
                        canvas.drawText(substring3 + "...", 0.0f, (float) i, this.f);
                        return;
                    } else {
                        canvas.drawText(substring3, 0.0f, (float) i, this.f);
                        return;
                    }
                } else if (i2 != 1 || !substring3.startsWith(this.h)) {
                    canvas.drawText(substring3, 0.0f, (float) i, this.f);
                } else {
                    this.f.setColor(getResources().getColor(R.color.appdetail_update_info_title));
                    canvas.drawText(this.h, 0.0f, (float) i, this.f);
                    this.f.setColor(getCurrentTextColor());
                    canvas.drawText(substring3.substring(this.h.length()), this.f.measureText(this.h), (float) i, this.f);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
        int measuredWidth2 = ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()) - a();
        Paint.FontMetrics fontMetrics = this.f.getFontMetrics();
        int ceil = (int) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
        int i3 = 0;
        String obj = getText().toString();
        if (!this.e) {
            obj.replaceAll("\n", Constants.STR_EMPTY);
        }
        int i4 = 0;
        while (true) {
            if (TextUtils.isEmpty(obj)) {
                break;
            } else if (obj.indexOf("\n") != 0 || i4 != 0) {
                if (!this.e) {
                    int i5 = i3 + ceil;
                    if (i4 != 0) {
                        i5 += by.a(this.c, 5.0f);
                    }
                    i4++;
                    obj = obj.substring(this.f.breakText(obj, true, (float) measuredWidth, null), obj.length());
                    if (TextUtils.isEmpty(obj) || i4 >= this.g) {
                        break;
                    }
                } else {
                    i3 += ceil;
                    if (i4 != 0) {
                        i3 += by.a(this.c, 5.0f);
                    }
                    int breakText = this.f.breakText(obj, true, (float) measuredWidth, null);
                    String substring = obj.substring(0, breakText);
                    int indexOf = substring.indexOf("\n");
                    i4++;
                    if (indexOf != -1) {
                        obj.substring(0, indexOf);
                        obj = obj.substring(indexOf + 1, obj.length());
                    } else {
                        obj = obj.substring(breakText, obj.length());
                        if (TextUtils.isEmpty(obj)) {
                            if (this.f.measureText(substring) > ((float) measuredWidth2)) {
                                i3 = i3 + ceil + by.a(this.c, 5.0f);
                            }
                        }
                    }
                }
            }
        }
        setMeasuredDimension(View.MeasureSpec.getSize(i), View.MeasureSpec.getSize(i2) + getPaddingTop() + i3 + getPaddingBottom());
    }

    private int a() {
        if (this.d < 0) {
            this.d = by.a(this.c, 35.0f);
        }
        return this.d;
    }
}
