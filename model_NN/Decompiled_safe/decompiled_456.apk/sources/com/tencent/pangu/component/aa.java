package com.tencent.pangu.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
class aa implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankCustomizeListView f3510a;

    aa(RankCustomizeListView rankCustomizeListView) {
        this.f3510a = rankCustomizeListView;
    }

    public void a(int i, int i2, boolean z, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, ArrayList<Long> arrayList, boolean z2) {
        if (this.f3510a.C != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f3510a.N);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            hashMap.put("hasNext", Boolean.valueOf(z2));
            hashMap.put("key_Appid", arrayList);
            hashMap.put("key_data", map);
            viewInvalidateMessage.params = hashMap;
            this.f3510a.C.sendMessage(viewInvalidateMessage);
        }
    }
}
