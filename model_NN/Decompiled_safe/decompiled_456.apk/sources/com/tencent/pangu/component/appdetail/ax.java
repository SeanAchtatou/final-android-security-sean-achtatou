package com.tencent.pangu.component.appdetail;

import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.pictureprocessor.ShowPictureActivity;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.SnapshotsPic;
import com.tencent.assistant.protocol.jce.Video;
import com.tencent.pangu.link.b;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ax implements aw {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HorizonScrollPicViewer f3581a;

    ax(HorizonScrollPicViewer horizonScrollPicViewer) {
        this.f3581a = horizonScrollPicViewer;
    }

    public void a(int i, String str, View view) {
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        view.getLocationOnScreen(iArr);
        int[] iArr2 = {iArr[0], iArr[1], view.getWidth(), view.getHeight()};
        if (i < this.f3581a.g.size()) {
            String str2 = "tpmast://videoplay?" + ((Video) this.f3581a.g.get(i)).b + "&" + "from_detail" + "=1";
            if (!c.a()) {
                Toast.makeText(this.f3581a.getContext(), (int) R.string.disconnected, 4).show();
            } else if (c.e()) {
                b.b(this.f3581a.f3554a, str2);
            } else {
                ay ayVar = new ay(this, str2);
                Resources resources = this.f3581a.getContext().getResources();
                ayVar.hasTitle = false;
                ayVar.contentRes = resources.getString(R.string.detail_video_play_tips);
                ayVar.lBtnTxtRes = resources.getString(R.string.detail_video_play_ignore);
                ayVar.rBtnTxtRes = resources.getString(R.string.detail_video_play_continue);
                DialogUtils.show2BtnDialog(ayVar);
            }
        } else {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (int i2 = 0; i2 < this.f3581a.f.size(); i2++) {
                arrayList.add(((SnapshotsPic) this.f3581a.f.get(i2)).f1531a);
                if (this.f3581a.c) {
                    arrayList2.add(((SnapshotsPic) this.f3581a.f.get(i2)).b);
                } else {
                    arrayList2.add(((SnapshotsPic) this.f3581a.f.get(i2)).c);
                }
            }
            Intent intent = new Intent(this.f3581a.f3554a, ShowPictureActivity.class);
            intent.putExtra("imagePos", iArr2);
            intent.putStringArrayListExtra("picUrls", arrayList);
            intent.putExtra("startPos", i);
            intent.putStringArrayListExtra("thumbnails", arrayList2);
            if (this.f3581a.i != null) {
                this.f3581a.i.a(true);
            }
            this.f3581a.f3554a.startActivity(intent);
        }
    }
}
