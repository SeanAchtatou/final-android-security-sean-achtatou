package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.assistant.model.c;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3589a;
    final /* synthetic */ c b;
    final /* synthetic */ bk c;

    bl(bk bkVar, int i, c cVar) {
        this.c = bkVar;
        this.f3589a = i;
        this.b = cVar;
    }

    public void run() {
        int i = 0;
        while (true) {
            if (i < this.c.f3588a.m.size()) {
                if (this.c.f3588a.k[i].getTag() != null && this.c.f3588a.k[i].getTag().toString().equals(Constants.STR_EMPTY + this.f3589a)) {
                    this.c.f3588a.k[i].a(this.b.a());
                    this.c.f3588a.o.set(i, this.b.a());
                    this.c.f3588a.l[i].a(this.b.a(), this.c.f3588a.j[i]);
                    this.c.f3588a.k[i].c();
                    break;
                }
                i++;
            } else {
                i = -1;
                break;
            }
        }
        if (i >= 0) {
            this.c.f3588a.a(this.b.a(), (View) null, i);
        }
    }
}
