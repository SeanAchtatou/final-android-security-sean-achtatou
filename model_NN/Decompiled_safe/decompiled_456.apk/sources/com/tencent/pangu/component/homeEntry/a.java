package com.tencent.pangu.component.homeEntry;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.aj;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomeEntryCellBase f3673a;

    a(HomeEntryCellBase homeEntryCellBase) {
        this.f3673a = homeEntryCellBase;
    }

    public void onTMAClick(View view) {
        XLog.v("home_entry", "HomeEntryCellBaseOnClickListener---onTMAClick--");
        if (this.f3673a.b != null && this.f3673a.b.e != null && !TextUtils.isEmpty(this.f3673a.b.e.f1125a)) {
            b.a(this.f3673a.f3671a, this.f3673a.b.e);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3673a.getContext(), 200);
        buildSTInfo.scene = STConst.ST_PAGE_COMPETITIVE;
        if (this.f3673a.b != null) {
            aj.a(this.f3673a.b.f, buildSTInfo);
        }
        XLog.v("home_entry", "HomeEntryCellBaseOnClickListener---getStInfo--stInfo.slotId= " + buildSTInfo.slotId + "--stInfo.pushInfo= " + buildSTInfo.pushInfo);
        return buildSTInfo;
    }
}
