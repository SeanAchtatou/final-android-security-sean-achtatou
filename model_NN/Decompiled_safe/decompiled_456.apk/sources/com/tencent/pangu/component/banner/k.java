package com.tencent.pangu.component.banner;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class k extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3644a;
    final /* synthetic */ int b;
    final /* synthetic */ j c;

    k(j jVar, Context context, int i) {
        this.c = jVar;
        this.f3644a = context;
        this.b = i;
    }

    public void onTMAClick(View view) {
        b.a(this.f3644a, this.c.f3641a.e);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3644a, 200);
        if (this.c.f3641a.b.equals(j.j)) {
            buildSTInfo.scene = STConst.ST_PAGE_GAME_FULI;
            buildSTInfo.slotId = "06_008";
        } else {
            buildSTInfo.slotId = a.a(this.c.b, this.b);
        }
        return buildSTInfo;
    }
}
