package com.tencent.pangu.component;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.mediadownload.q;
import com.tencent.pangu.mediadownload.r;
import com.tencent.pangu.model.AbstractDownloadInfo;

/* compiled from: ProGuard */
public class VideoDownloadButton extends Button implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f3508a;
    /* access modifiers changed from: private */
    public q b;

    public VideoDownloadButton(Context context) {
        this(context, null);
    }

    public VideoDownloadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3508a = context;
        b();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_HOST_GET_FAIL, this);
    }

    private void b() {
        setHeight(this.f3508a.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.f3508a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.f3508a.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
    }

    public void a(q qVar, STInfoV2 sTInfoV2) {
        this.b = qVar;
        if (this.b != null) {
            a();
            a(sTInfoV2);
        }
    }

    public void a(STInfoV2 sTInfoV2) {
        if (this.b != null) {
            setOnClickListener(new bs(this, sTInfoV2));
        }
    }

    public void a() {
        if (this.b != null) {
            a(this.b.s);
        }
    }

    private void a(AbstractDownloadInfo.DownState downState) {
        by b2 = b(downState);
        setTextColor(this.f3508a.getResources().getColor(b2.b));
        a(this.f3508a.getResources().getString(b2.f3664a));
        setBackgroundDrawable(this.f3508a.getResources().getDrawable(b2.c));
    }

    private by b(AbstractDownloadInfo.DownState downState) {
        by byVar = new by(null);
        byVar.c = R.drawable.state_bg_common_selector;
        byVar.b = R.color.state_normal;
        byVar.f3664a = R.string.appbutton_unknown;
        switch (bv.f3662a[downState.ordinal()]) {
            case 1:
                byVar.f3664a = R.string.appbutton_download;
                break;
            case 2:
                byVar.f3664a = R.string.downloading_display_pause;
                break;
            case 3:
                byVar.f3664a = R.string.queuing;
                break;
            case 4:
            case 5:
            case 6:
                byVar.f3664a = R.string.appbutton_continuing;
                byVar.b = R.color.state_install;
                byVar.c = R.drawable.state_bg_install_selector;
                break;
            case 7:
                byVar.f3664a = R.string.videobutton_play;
                break;
            default:
                byVar.f3664a = R.string.appbutton_unknown;
                break;
        }
        return byVar;
    }

    private void a(String str) {
        if (str.length() == 4) {
            setMinWidth(this.f3508a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
        } else {
            setMinWidth(this.f3508a.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        }
        setText(str);
    }

    public void handleUIEvent(Message message) {
        if (message.what == 1166) {
            String str = (String) message.obj;
            if (!TextUtils.isEmpty(str) && this.b != null && str.equals(this.b.e)) {
                Context context = getContext();
                String string = getContext().getResources().getString(R.string.video_down_get_fail);
                Object[] objArr = new Object[1];
                objArr[0] = TextUtils.isEmpty(this.b.i) ? getContext().getResources().getString(R.string.video_down_tips_unknow_player) : this.b.i;
                Toast.makeText(context, String.format(string, objArr), 0).show();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(q qVar, String str) {
        if (qVar != null) {
            switch (bv.f3662a[qVar.s.ordinal()]) {
                case 1:
                case 4:
                case 6:
                    r.c().a(qVar);
                    return;
                case 2:
                case 3:
                    r.c().a(qVar.m);
                    return;
                case 5:
                default:
                    return;
                case 7:
                    int a2 = r.c().a(getContext(), qVar);
                    if (a2 == -2 || a2 == -3) {
                        DialogUtils.show2BtnDialog(new bw(getContext(), qVar, a2, null));
                        return;
                    } else if (a2 == -4) {
                        bt btVar = new bt(this, qVar);
                        btVar.titleRes = getContext().getResources().getString(R.string.video_play_tips);
                        btVar.contentRes = String.format(getContext().getResources().getString(R.string.video_play_tips_content), new Object[0]);
                        btVar.lBtnTxtRes = getContext().getResources().getString(R.string.video_down_cancel);
                        btVar.rBtnTxtRes = getContext().getResources().getString(R.string.video_play_reload);
                        DialogUtils.show2BtnDialog(btVar);
                        return;
                    } else {
                        return;
                    }
            }
        }
    }
}
