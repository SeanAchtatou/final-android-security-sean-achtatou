package com.tencent.pangu.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.w;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
public class ShareAppBar extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f3503a;
    private TextView b;
    private TextView c;
    private TextView d;
    private IWXAPI e;
    private bm f;
    private bn g = null;
    private View.OnClickListener h = new bl(this);

    public ShareAppBar(Context context) {
        super(context);
        a(context);
    }

    public ShareAppBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public void a(bn bnVar) {
        this.g = bnVar;
    }

    public void a(bm bmVar) {
        this.f = bmVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.pangu.component.ShareAppBar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(Context context) {
        setOrientation(0);
        LayoutInflater.from(context).inflate((int) R.layout.share_bar_common_layout, (ViewGroup) this, true);
        this.f3503a = (TextView) findViewById(R.id.tv_share_qq);
        this.b = (TextView) findViewById(R.id.tv_share_qz);
        this.c = (TextView) findViewById(R.id.tv_share_wx);
        this.d = (TextView) findViewById(R.id.tv_share_timeline);
        this.f3503a.setOnClickListener(this.h);
        this.b.setOnClickListener(this.h);
        this.c.setOnClickListener(this.h);
        this.d.setOnClickListener(this.h);
    }

    public void a() {
        boolean z;
        boolean z2;
        boolean z3 = true;
        a(this.b, !f());
        TextView textView = this.f3503a;
        if (!j.a().t() || !j.a().u() || !w.a()) {
            z = false;
        } else {
            z = true;
        }
        a(textView, z);
        if (this.e == null) {
            this.e = WXAPIFactory.createWXAPI(AstApp.i().getApplicationContext(), "wx3909f6add1206543", false);
        }
        TextView textView2 = this.c;
        if (this.e.getWXAppSupportAPI() > 553779201) {
            z2 = true;
        } else {
            z2 = false;
        }
        a(textView2, z2);
        TextView textView3 = this.d;
        if (this.e.getWXAppSupportAPI() <= 553779201) {
            z3 = false;
        }
        a(textView3, z3);
        if (this.f3503a != null && this.b != null && this.c != null && this.d != null && 8 == this.f3503a.getVisibility() && 8 == this.b.getVisibility() && 8 == this.c.getVisibility() && 8 == this.d.getVisibility() && this.g != null) {
            this.g.a();
        }
    }

    private boolean f() {
        return j.a().l();
    }

    private void a(TextView textView, boolean z) {
        if (textView != null) {
            textView.setVisibility(z ? 0 : 8);
        }
    }

    public void b() {
        if (this.f != null) {
            this.f.a();
        }
    }

    public void c() {
        if (this.f != null) {
            this.f.b();
        }
    }

    public void d() {
        if (this.f != null) {
            this.f.c();
        }
    }

    public void e() {
        if (this.f != null) {
            this.f.d();
        }
    }
}
