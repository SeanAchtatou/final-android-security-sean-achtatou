package com.tencent.pangu.c;

import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.pangu.model.ShareAppModel;
import com.tencent.pangu.model.ShareBaseModel;

/* compiled from: ProGuard */
class i extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f3478a;

    i(e eVar) {
        this.f3478a = eVar;
    }

    public void onRightBtnClick() {
        if (this.f3478a.d == null) {
            Toast.makeText(AstApp.i().getApplicationContext(), (int) R.string.share_info_err, 0).show();
            return;
        }
        if (this.f3478a.d instanceof ShareAppModel) {
            ShareAppModel shareAppModel = (ShareAppModel) this.f3478a.d;
            this.f3478a.a(shareAppModel.e, this.f3478a.c(shareAppModel), this.f3478a.b(shareAppModel), shareAppModel.b, shareAppModel.f, null);
        }
        if (this.f3478a.d instanceof ShareBaseModel) {
            ShareBaseModel shareBaseModel = (ShareBaseModel) this.f3478a.d;
            this.f3478a.a(-1, shareBaseModel.f3880a, shareBaseModel.b, shareBaseModel.e, shareBaseModel.c, shareBaseModel.d);
        }
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
