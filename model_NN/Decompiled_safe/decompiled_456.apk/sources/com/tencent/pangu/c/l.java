package com.tencent.pangu.c;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.bg;

/* compiled from: ProGuard */
class l implements CallbackHelper.Caller<d> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3481a;
    final /* synthetic */ JceStruct b;
    final /* synthetic */ JceStruct c;
    final /* synthetic */ e d;

    l(e eVar, int i, JceStruct jceStruct, JceStruct jceStruct2) {
        this.d = eVar;
        this.f3481a = i;
        this.b = jceStruct;
        this.c = jceStruct2;
    }

    /* renamed from: a */
    public void call(d dVar) {
        XLog.i("ShareEngine", "share success");
        dVar.a(this.f3481a, this.b, this.c);
        if (this.d.n) {
            bg.a(AstApp.i().getApplicationContext(), (int) R.string.share_success, 0);
        }
    }
}
