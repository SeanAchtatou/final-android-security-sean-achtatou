package com.tencent.pangu.manager;

import android.util.Log;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/* compiled from: ProGuard */
class ap implements h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CountDownLatch f3798a;
    final /* synthetic */ ak b;

    ap(ak akVar, CountDownLatch countDownLatch) {
        this.b = akVar;
        this.f3798a = countDownLatch;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, boolean):boolean
     arg types: [com.tencent.pangu.manager.ak, int]
     candidates:
      com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, com.tencent.pangu.download.DownloadInfo):void
      com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, java.util.concurrent.CountDownLatch):void
      com.tencent.pangu.manager.ak.a(int, com.tencent.pangu.download.DownloadInfo):void
      com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, boolean):boolean */
    public void onGetAppInfoFail(int i, int i2) {
        if (this.f3798a != null) {
            boolean unused = this.b.e = false;
            this.f3798a.countDown();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.pangu.download.DownloadInfo, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, boolean):boolean
     arg types: [com.tencent.pangu.manager.ak, int]
     candidates:
      com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, com.tencent.pangu.download.DownloadInfo):void
      com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, java.util.concurrent.CountDownLatch):void
      com.tencent.pangu.manager.ak.a(int, com.tencent.pangu.download.DownloadInfo):void
      com.tencent.pangu.manager.ak.a(com.tencent.pangu.manager.ak, boolean):boolean */
    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        DownloadInfo downloadInfo;
        DownloadInfo downloadInfo2;
        SimpleAppModel a2 = k.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        StatInfo statInfo = new StatInfo(a2.b, STConst.ST_PAGE_APP_DETAIL, 0, null, 0);
        if (a3 == null || !a3.needReCreateInfo(a2)) {
            downloadInfo = a3;
        } else {
            DownloadProxy.a().b(a3.downloadTicket);
            downloadInfo = null;
        }
        if (downloadInfo == null) {
            downloadInfo2 = DownloadInfo.createDownloadInfo(a2, statInfo);
            DownloadProxy.a().d(downloadInfo2);
        } else {
            downloadInfo2 = downloadInfo;
        }
        switch (this.b.k(downloadInfo2)) {
            case 0:
                List<DownloadInfo> e = DownloadProxy.a().e("com.tencent.qlauncher");
                if (e != null && e.size() > 0) {
                    ak.a(true);
                    a.a().a(e.get(0), true);
                }
                if (this.f3798a != null) {
                    boolean unused = this.b.e = true;
                    break;
                }
                break;
            case 1:
                Log.d("QubeManager", "<Qube> download qlauncher form server");
                downloadInfo2.autoInstall = true;
                DownloadProxy.a().d(downloadInfo2);
                ak.a(true);
                DownloadProxy.a().c(downloadInfo2);
                if (this.f3798a != null) {
                    boolean unused2 = this.b.e = true;
                    break;
                }
                break;
            case 2:
                Log.d("QubeManager", "<Qube> the qlauncher installed is the lastest !!!");
                ak.d.sendEmptyMessage(4);
                if (this.f3798a != null) {
                    boolean unused3 = this.b.e = false;
                    break;
                }
                break;
        }
        if (this.f3798a != null) {
            this.f3798a.countDown();
        }
    }
}
