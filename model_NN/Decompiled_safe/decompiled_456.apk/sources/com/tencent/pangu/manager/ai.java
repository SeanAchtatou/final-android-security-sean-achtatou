package com.tencent.pangu.manager;

import android.text.TextUtils;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.download.DownloadInfo;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class ai extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f3792a;

    private ai(ac acVar) {
        this.f3792a = acVar;
    }

    /* synthetic */ ai(ac acVar, ad adVar) {
        this(acVar);
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && !TextUtils.isEmpty(localApkInfo.mPackageName)) {
            TemporaryThreadManager.get().start(new aj(this, i, localApkInfo));
        }
    }

    public void onDeleteApkSuccess(LocalApkInfo localApkInfo) {
        if (TextUtils.isEmpty(ApkResourceManager.getInstance().getDdownloadTicket(localApkInfo)) && localApkInfo != null && !TextUtils.isEmpty(localApkInfo.mPackageName)) {
            String unused = this.f3792a.b = this.f3792a.b(this.f3792a.b, localApkInfo.mPackageName);
            XLog.d("AreadyRemind", "after delete " + this.f3792a.b);
            if (this.f3792a.f3786a != null && !this.f3792a.f3786a.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.f3792a.f3786a);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    if (((DownloadInfo) it.next()).packageName.equals(localApkInfo.mPackageName)) {
                        this.f3792a.f3786a.remove(localApkInfo);
                    }
                }
                arrayList.clear();
            }
        }
    }
}
