package com.tencent.pangu.manager;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.downloadsdk.ae;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.download.m;

/* compiled from: ProGuard */
class o implements ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f3838a;

    o(DownloadProxy downloadProxy) {
        this.f3838a = downloadProxy;
    }

    public void a(int i, String str, String str2) {
        DownloadInfo d = this.f3838a.d(str);
        if (d != null) {
            a.a().b.remove(str);
            d.downloadingPath = str2;
            if (d.fileType == SimpleDownloadInfo.DownloadType.APK) {
                d.downloadState = SimpleDownloadInfo.DownloadState.COMPLETE;
            } else if (d.fileType == SimpleDownloadInfo.DownloadType.PLUGIN) {
                d.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
            }
            d.errorCode = 0;
            this.f3838a.a(d, SimpleDownloadInfo.DownloadState.COMPLETE);
            this.f3838a.e.a(d);
            if (ak.a().b(d)) {
                ak.a().g(d);
            }
        }
    }

    public void a(int i, String str) {
        DownloadInfo d = this.f3838a.d(str);
        if (d != null) {
            a.a().b.remove(str);
            d.downloadState = SimpleDownloadInfo.DownloadState.DOWNLOADING;
            d.errorCode = 0;
            if (d.fileType == SimpleDownloadInfo.DownloadType.APK) {
                this.f3838a.d.sendMessage(this.f3838a.d.obtainMessage(1002, str));
            }
            this.f3838a.e.a(d);
            if (ak.a().b(d)) {
                ak.a().g(d);
            }
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
        DownloadInfo d = this.f3838a.d(str);
        if (d == null) {
            return;
        }
        if (d.response == null || d.response.b <= 0) {
            if (d.response == null) {
                d.response = new m();
            }
            d.response.b = j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void a(int i, String str, long j, long j2, double d) {
        DownloadInfo d2 = this.f3838a.d(str);
        if (d2 != null) {
            if (d2.fileType == SimpleDownloadInfo.DownloadType.APK && d2.downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                this.f3838a.d.sendMessage(this.f3838a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, str));
            }
            SimpleDownloadInfo.DownloadState downloadState = d2.downloadState;
            d2.downloadState = SimpleDownloadInfo.DownloadState.DOWNLOADING;
            d2.errorCode = 0;
            d2.response.f3766a = j2;
            d2.response.b = j;
            d2.response.c = bm.a(d);
            this.f3838a.a(d2, SimpleDownloadInfo.DownloadState.DOWNLOADING);
            if (ak.a().b(d2) && downloadState != d2.downloadState) {
                ak.a().g(d2);
            }
            com.tencent.assistant.m.a().b("key_space_clean_has_run_clean", (Object) false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void a(int i, String str, String str2, String str3) {
        DownloadInfo d = this.f3838a.d(str);
        if (d != null) {
            a.a().b.remove(str);
            d.downloadingPath = str2;
            if (d.fileType == SimpleDownloadInfo.DownloadType.APK) {
                d.downloadState = SimpleDownloadInfo.DownloadState.COMPLETE;
            } else if (d.fileType == SimpleDownloadInfo.DownloadType.PLUGIN) {
                d.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
            }
            d.errorCode = 0;
            d.response.f3766a = d.response.b;
            this.f3838a.a(d, SimpleDownloadInfo.DownloadState.COMPLETE);
            this.f3838a.e.a(d);
            if (ak.a().b(d)) {
                ak.a().g(d);
            }
            com.tencent.assistant.m.a().b("key_space_clean_has_run_clean", (Object) false);
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        boolean z = true;
        boolean z2 = false;
        DownloadInfo d = this.f3838a.d(str);
        if (d == null) {
            z = false;
        } else if (d.sllUpdate != 1 || ((i2 >= -400 || i2 <= -600) && i2 != -10)) {
            a.a().b.remove(str);
            boolean isUiTypeWiseDownload = d.isUiTypeWiseDownload();
            if (d.uiType != SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD) {
                z = false;
            }
            d.downloadState = SimpleDownloadInfo.DownloadState.FAIL;
            this.f3838a.a(d, SimpleDownloadInfo.DownloadState.FAIL);
            d.errorCode = i2;
            this.f3838a.e.a(d);
            if (ak.a().b(d)) {
                ak.a().g(d);
            }
            z2 = isUiTypeWiseDownload;
        } else {
            a.a().a(str);
            return;
        }
        if (!z2 && !z) {
            if (i2 == -12) {
                TemporaryThreadManager.get().start(new p(this, d));
            } else if (this.f3838a.a(i2)) {
                TemporaryThreadManager.get().start(new q(this));
            } else if (i2 == -11) {
                ah.a().post(new r(this));
            }
            FileUtil.tryRefreshPath(i2);
        }
    }

    public void b(int i, String str) {
        DownloadInfo d = this.f3838a.d(str);
        if (d != null) {
            a.a().b.remove(str);
            if (d.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || d.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                d.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
                d.errorCode = 0;
                this.f3838a.a(d, SimpleDownloadInfo.DownloadState.PAUSED);
                this.f3838a.e.a(d);
                if (ak.a().b(d)) {
                    ak.a().g(d);
                }
            }
        }
    }

    public void b(int i, String str, String str2) {
    }
}
