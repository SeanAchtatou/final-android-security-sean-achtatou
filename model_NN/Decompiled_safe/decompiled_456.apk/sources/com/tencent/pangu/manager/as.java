package com.tencent.pangu.manager;

import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
/* synthetic */ class as {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3801a = new int[SimpleDownloadInfo.DownloadState.values().length];

    static {
        try {
            f3801a[SimpleDownloadInfo.DownloadState.INIT.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.ILLEGAL.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.DOWNLOADING.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.PAUSED.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.FAIL.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.QUEUING.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.DELETED.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.SUCC.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.COMPLETE.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f3801a[SimpleDownloadInfo.DownloadState.INSTALLED.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
    }
}
