package com.tencent.pangu.manager;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.download.DownloadInfo;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3793a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ ai c;

    aj(ai aiVar, int i, LocalApkInfo localApkInfo) {
        this.c = aiVar;
        this.f3793a = i;
        this.b = localApkInfo;
    }

    public void run() {
        if (this.f3793a == 2) {
            String unused = this.c.f3792a.b = this.c.f3792a.b(this.c.f3792a.b, this.b.mPackageName);
            XLog.d("AreadyRemind", "after delete " + this.c.f3792a.b);
            if (this.c.f3792a.f3786a != null && !this.c.f3792a.f3786a.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.c.f3792a.f3786a);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    if (((DownloadInfo) it.next()).packageName.equals(this.b.mPackageName)) {
                        this.c.f3792a.f3786a.remove(this.b);
                    }
                }
                arrayList.clear();
            }
        }
    }
}
