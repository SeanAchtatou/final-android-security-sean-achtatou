package com.tencent.pangu.manager;

import android.text.TextUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import java.util.ArrayList;

/* compiled from: ProGuard */
class u extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f3844a;

    private u(DownloadProxy downloadProxy) {
        this.f3844a = downloadProxy;
    }

    /* synthetic */ u(DownloadProxy downloadProxy, g gVar) {
        this(downloadProxy);
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        ArrayList<DownloadInfo> a2;
        if (localApkInfo != null && !TextUtils.isEmpty(localApkInfo.mPackageName) && (a2 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK)) != null) {
            if (i == 1) {
                if (m.a().l()) {
                    String unused = this.f3844a.b(localApkInfo, a2);
                }
                String b = this.f3844a.a(localApkInfo, a2);
                a.a().a(localApkInfo, a2);
                this.f3844a.d.sendMessage(this.f3844a.d.obtainMessage(1013, b));
                this.f3844a.d.sendMessage(this.f3844a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_INSTALL, localApkInfo.mPackageName));
            } else if (i == 2) {
                this.f3844a.d.sendMessage(this.f3844a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, localApkInfo.mPackageName));
            }
        }
    }

    public void onDeleteApkSuccess(LocalApkInfo localApkInfo) {
        String ddownloadTicket = ApkResourceManager.getInstance().getDdownloadTicket(localApkInfo);
        if (TextUtils.isEmpty(ddownloadTicket)) {
            this.f3844a.d.sendMessage(this.f3844a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APK_DELETE, ddownloadTicket));
        }
    }
}
