package com.tencent.pangu.manager;

import android.text.TextUtils;
import com.tencent.assistant.db.table.m;
import com.tencent.pangu.download.n;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ProGuard */
public class w {

    /* renamed from: a  reason: collision with root package name */
    private static w f3846a;
    private List<n> b;
    private m c;

    public static synchronized w a() {
        w wVar;
        synchronized (w.class) {
            if (f3846a == null) {
                f3846a = new w();
            }
            wVar = f3846a;
        }
        return wVar;
    }

    private w() {
        b();
    }

    private void b() {
        this.b = Collections.synchronizedList(new ArrayList());
        this.c = new m();
        List<n> a2 = this.c.a();
        if (a2 != null && !a2.isEmpty()) {
            this.b.addAll(a2);
        }
    }

    public boolean a(String str, int i, int i2) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        for (n next : this.b) {
            if (str.equalsIgnoreCase(next.b) && i == next.c && i2 == next.d) {
                return true;
            }
        }
        return false;
    }

    public boolean a(String str, String str2, int i, int i2) {
        if (TextUtils.isEmpty(str) || a(str2, i, i2)) {
            return false;
        }
        n nVar = new n(str, str2, i, i2);
        if (a(str2, i, i2)) {
            return false;
        }
        this.b.add(nVar);
        this.c.a(nVar);
        return true;
    }

    public boolean a(String str) {
        if (TextUtils.isEmpty(str) || this.b.isEmpty()) {
            return false;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.b.size(); i++) {
            if (str.equalsIgnoreCase(this.b.get(i).f3767a)) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        if (arrayList.isEmpty()) {
            return false;
        }
        for (int size = arrayList.size() - 1; size > 0; size--) {
            this.b.remove(size);
        }
        this.c.a(str);
        return true;
    }
}
