package com.tencent.pangu.manager;

import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class bc extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3812a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ au c;

    bc(au auVar, DownloadInfo downloadInfo, STInfoV2 sTInfoV2) {
        this.c = auVar;
        this.f3812a = downloadInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        try {
            this.c.b.setVisibility(8);
            a.a().a(this.f3812a.packageName, this.f3812a.applinkInfo);
        } catch (Exception e) {
            bd bdVar = new bd(this);
            bdVar.titleRes = AstApp.i().getResources().getString(R.string.down_uninstall_title);
            bdVar.contentRes = AstApp.i().getResources().getString(R.string.down_cannot_open_tips);
            bdVar.btnTxtRes = AstApp.i().getResources().getString(R.string.down_uninstall_tips_close);
            DialogUtils.show1BtnDialog(bdVar);
        }
    }

    public STInfoV2 getStInfo() {
        this.b.slotId = "03_001_200";
        this.b.actionId = 200;
        return this.b;
    }
}
