package com.tencent.pangu.manager;

import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
class bm implements NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfUpdateManager f3821a;

    bm(SelfUpdateManager selfUpdateManager) {
        this.f3821a = selfUpdateManager;
    }

    public void onConnected(APN apn) {
        if (apn != APN.WIFI || c.l() || this.f3821a.i != SelfUpdateManager.SelfUpdateType.SILENT) {
            return;
        }
        if (this.f3821a.e == null) {
            this.f3821a.a(false);
        } else {
            this.f3821a.a(this.f3821a.i);
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
