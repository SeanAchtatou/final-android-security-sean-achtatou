package com.tencent.pangu.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.model.b;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.aj;
import com.tencent.assistantv2.adapter.smartlist.SmartItemType;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import com.tencent.pangu.component.homeEntry.HomeEntryTemplateView;

/* compiled from: ProGuard */
public class DiscoverPageListAdapter extends SmartListAdapter {

    /* renamed from: a  reason: collision with root package name */
    private HomeEntryTemplateView f3401a = null;
    private View b = null;

    public DiscoverPageListAdapter(Context context, View view, b bVar) {
        super(context, view, bVar);
    }

    /* access modifiers changed from: protected */
    public SmartListAdapter.SmartListType o() {
        return SmartListAdapter.SmartListType.DiscoverPage;
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        return "07";
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (SmartItemType.BANNER.ordinal() == getItemViewType(i)) {
            this.f3401a = aj.a(d(), this.f3401a);
            if (this.f3401a != null) {
                return this.f3401a;
            }
            XLog.v("home_entry", "DiscoverPageListAdapter--getView--create new exception ");
            if (this.b == null) {
                this.b = new LinearLayout(d());
            }
            return this.b;
        }
        View view2 = super.getView(i, view, viewGroup);
        if (LaunchSpeedSTManager.d().a() || !(viewGroup instanceof ListView) || ((ListView) viewGroup).getFirstVisiblePosition() != 0 || i == 0 || !LaunchSpeedSTManager.d().a(i)) {
            return view2;
        }
        AstApp.i().j().removeMessages(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_ADAPTER_GET_VIEW_END);
        AstApp.i().j().sendMessageDelayed(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_ADAPTER_GET_VIEW_END), 500);
        return view2;
    }
}
