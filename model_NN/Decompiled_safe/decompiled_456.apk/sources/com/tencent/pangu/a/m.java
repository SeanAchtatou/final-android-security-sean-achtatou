package com.tencent.pangu.a;

import android.os.Handler;
import android.os.Message;
import com.tencent.pangu.component.treasurebox.AppTreasureEntryBlinkEyesView;

/* compiled from: ProGuard */
class m extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3299a;

    m(h hVar) {
        this.f3299a = hVar;
    }

    public void handleMessage(Message message) {
        boolean z = true;
        int i = 0;
        switch (message.what) {
            case 1:
                if (message.arg1 != 1) {
                    z = false;
                }
                if (!this.f3299a.g) {
                    if (this.f3299a.e != null) {
                        AppTreasureEntryBlinkEyesView b = this.f3299a.e;
                        if (!z) {
                            i = 8;
                        }
                        b.setVisibility(i);
                    }
                    if (this.f3299a.f != null) {
                        this.f3299a.f.setVisibility(8);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
