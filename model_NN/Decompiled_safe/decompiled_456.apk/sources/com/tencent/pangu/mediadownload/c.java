package com.tencent.pangu.mediadownload;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public String f3855a;
    public String b;
    public String c;
    public String d;
    public String e;
    public int f;
    public int g;
    public int h;
    public long i;
    public long j;
    public String k;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (this.c != null) {
            if (this.c.equals(cVar.c)) {
                return true;
            }
        } else if (cVar.c == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.c != null) {
            return this.c.hashCode();
        }
        return 0;
    }
}
