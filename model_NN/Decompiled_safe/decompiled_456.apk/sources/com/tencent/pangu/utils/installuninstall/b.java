package com.tencent.pangu.utils.installuninstall;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* compiled from: ProGuard */
final class b extends Handler {
    b(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        if (message.obj != null && (message.obj instanceof m)) {
            m mVar = (m) message.obj;
            if (mVar.b != null && mVar.f4008a != null) {
                mVar.b.a(mVar.f4008a);
                boolean unused = InstallUninstallDialogManager.h = true;
            }
        }
    }
}
