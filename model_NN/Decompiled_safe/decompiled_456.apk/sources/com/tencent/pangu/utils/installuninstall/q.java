package com.tencent.pangu.utils.installuninstall;

import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f4010a;
    final /* synthetic */ boolean b;
    final /* synthetic */ p c;

    q(p pVar, DownloadInfo downloadInfo, boolean z) {
        this.c = pVar;
        this.f4010a = downloadInfo;
        this.b = z;
    }

    public void run() {
        if (this.f4010a != null) {
            this.c.a(this.f4010a.downloadTicket, this.f4010a.packageName, this.f4010a.name, this.f4010a.getFilePath(), this.f4010a.versionCode, this.f4010a.signatrue, this.f4010a.downloadTicket, this.f4010a.fileSize, this.b, this.f4010a);
        }
    }
}
