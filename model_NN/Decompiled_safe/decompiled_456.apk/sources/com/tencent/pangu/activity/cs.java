package com.tencent.pangu.activity;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cs extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f3370a;

    cs(StartPopWindowActivity startPopWindowActivity) {
        this.f3370a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        this.f3370a.D();
        this.f3370a.t();
        Toast.makeText(this.f3370a.K, (int) R.string.pop_title_download_wifi_toast, 4).show();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3370a.K, 200);
        buildSTInfo.slotId = a.a("04", "003");
        if (this.f3370a.J != null && this.f3370a.F.a() == this.f3370a.J.size()) {
            buildSTInfo.actionId = 200;
            buildSTInfo.status = "02";
        }
        return buildSTInfo;
    }
}
