package com.tencent.pangu.activity;

import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.w;
import com.tencent.pangu.download.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bp implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopUpNecessaryAcitivity f3340a;

    bp(PopUpNecessaryAcitivity popUpNecessaryAcitivity) {
        this.f3340a = popUpNecessaryAcitivity;
    }

    public void run() {
        ArrayList<w> c = this.f3340a.y.c();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < c.size()) {
                w wVar = c.get(i2);
                if (wVar != null) {
                    this.f3340a.a(wVar);
                    StatInfo statInfo = new StatInfo();
                    statInfo.scene = wVar.c;
                    statInfo.slotId = this.f3340a.b(wVar.b);
                    statInfo.extraData = wVar.f3744a != null ? wVar.f3744a.c + "|" + wVar.f3744a.g : Constants.STR_EMPTY;
                    a.a().a(wVar.f3744a, statInfo);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
