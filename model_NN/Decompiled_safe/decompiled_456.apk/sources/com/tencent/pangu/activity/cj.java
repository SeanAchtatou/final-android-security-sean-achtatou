package com.tencent.pangu.activity;

import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.DownloadButton;

/* compiled from: ProGuard */
class cj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3361a;

    cj(SearchActivity searchActivity) {
        this.f3361a = searchActivity;
    }

    public void onClick(View view) {
        SimpleAppModel simpleAppModel;
        String str = (String) view.getTag(R.id.search_key_word);
        Integer num = (Integer) view.getTag(R.id.search_source_scene);
        if (view instanceof DownloadButton) {
            simpleAppModel = (SimpleAppModel) view.getTag(R.id.search_app_info);
            AppConst.AppState d = k.d(simpleAppModel);
            if (d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE) {
                int unused = this.f3361a.N = (int) STConst.ST_PAGE_SEARCH_DIRECT;
            } else {
                return;
            }
        } else if (num != null) {
            int unused2 = this.f3361a.N = num.intValue();
            XLog.d("SearchActivity", "sourceScent changed! sourceScent = " + this.f3361a.N);
            simpleAppModel = null;
        } else {
            int unused3 = this.f3361a.N = (int) STConst.ST_PAGE_SEARCH_SUGGEST;
            simpleAppModel = null;
        }
        this.f3361a.K.removeMessages(0);
        this.f3361a.G.a(this.f3361a.I);
        this.f3361a.A();
        this.f3361a.n = true;
        if (!TextUtils.isEmpty(str)) {
            String obj = this.f3361a.v.a().getText().toString();
            if (str.length() > SearchActivity.L) {
                str = str.substring(0, SearchActivity.L);
            }
            try {
                this.f3361a.v.a().setText(str);
                this.f3361a.v.a().setSelection(str.length());
            } catch (Throwable th) {
            }
            this.f3361a.c(8);
            this.f3361a.z.b(obj);
            this.f3361a.a(simpleAppModel);
        }
    }
}
