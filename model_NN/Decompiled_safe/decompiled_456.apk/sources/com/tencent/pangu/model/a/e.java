package com.tencent.pangu.model.a;

import com.tencent.pangu.model.a.d;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public abstract class e<K, V extends d> {

    /* renamed from: a  reason: collision with root package name */
    private ConcurrentHashMap<K, V> f3884a = new ConcurrentHashMap<>();

    public ConcurrentHashMap<K, V> a() {
        return this.f3884a;
    }

    public V a(Object obj) {
        if (this.f3884a != null) {
            return (d) this.f3884a.get(obj);
        }
        this.f3884a = new ConcurrentHashMap<>();
        return null;
    }

    public void a(Object obj, d dVar) {
        if (this.f3884a == null) {
            this.f3884a = new ConcurrentHashMap<>();
        }
        this.f3884a.put(obj, dVar);
    }

    public void b(K k, V v) {
        if (this.f3884a == null) {
            this.f3884a = new ConcurrentHashMap<>();
        }
        d dVar = (d) this.f3884a.get(k);
        if (dVar == null) {
            this.f3884a.put(k, v);
        } else {
            dVar.a(v);
        }
    }

    public void a(e eVar) {
        ConcurrentHashMap a2;
        if (eVar != null && eVar.b() > 0 && (a2 = eVar.a()) != null) {
            for (Map.Entry entry : a2.entrySet()) {
                b(entry.getKey(), (d) entry.getValue());
            }
        }
    }

    public int b() {
        if (this.f3884a != null) {
            return this.f3884a.size();
        }
        return -1;
    }

    public void c() {
        if (this.f3884a != null) {
            this.f3884a.clear();
        }
    }
}
