package com.tencent.pangu.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class ShareBaseModel extends ShareModel {
    public static final Parcelable.Creator<ShareBaseModel> CREATOR = new f();

    /* renamed from: a  reason: collision with root package name */
    public String f3880a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3880a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
    }
}
