package com.tencent.pangu.model;

import android.text.TextUtils;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.cloud.model.SimpleEbookModel;
import com.tencent.cloud.model.SimpleVideoModel;
import java.util.List;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public int f3886a = 0;
    public int b = 1;
    public SimpleAppModel c;
    public List<SimpleAppModel> d;
    public SimpleVideoModel e;
    public SimpleEbookModel f;
    public n g;
    public String h;

    public int a() {
        return this.f3886a;
    }

    public List<SimpleAppModel> b() {
        return this.d;
    }

    public b() {
    }

    public b(SimpleAppModel simpleAppModel) {
        this.c = simpleAppModel;
        this.b = 1;
    }

    public SimpleAppModel c() {
        return this.c;
    }

    public SimpleVideoModel d() {
        return this.e;
    }

    public SimpleEbookModel e() {
        return this.f;
    }

    public n f() {
        return this.g;
    }

    public boolean g() {
        if (this.b == 1 && this.c != null) {
            if (!TextUtils.isEmpty(this.c.X)) {
                return true;
            }
            if (this.c.av == null || this.c.av.a() == 0) {
                return false;
            }
            return true;
        }
        return false;
    }

    public boolean h() {
        return (this.c == null && this.d == null && this.e == null && this.f == null && this.g == null) ? false : true;
    }
}
