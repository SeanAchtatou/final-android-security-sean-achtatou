package com.tencent.pangu.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.SplashManager;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.manager.l;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.callback.GetOpRegularPushCallback;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.FlashInfo;
import com.tencent.assistant.protocol.jce.GetOpRegularInfoRequest;
import com.tencent.assistant.protocol.jce.GetOpRegularInfoResponse;
import com.tencent.assistant.protocol.jce.OpRegularInfo;
import com.tencent.assistant.protocol.jce.OpRegularParam;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.a.c;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class as extends BaseEngine<GetOpRegularPushCallback> {

    /* renamed from: a  reason: collision with root package name */
    private static as f3910a = null;
    private static long b = 0;

    public static synchronized as a() {
        as asVar;
        synchronized (as.class) {
            if (f3910a == null) {
                f3910a = new as();
            }
            asVar = f3910a;
        }
        return asVar;
    }

    public int b() {
        int i = 1;
        int i2 = 0;
        XLog.d("splashInfo", "sendRequest ..");
        GetOpRegularInfoRequest getOpRegularInfoRequest = new GetOpRegularInfoRequest();
        getOpRegularInfoRequest.f1312a = new ArrayList<>();
        OpRegularParam opRegularParam = new OpRegularParam();
        opRegularParam.f1426a = 1;
        opRegularParam.c = 0;
        getOpRegularInfoRequest.f1312a.add(opRegularParam);
        OpRegularParam opRegularParam2 = new OpRegularParam();
        opRegularParam2.f1426a = 2;
        opRegularParam2.c = 0;
        getOpRegularInfoRequest.f1312a.add(opRegularParam2);
        OpRegularParam opRegularParam3 = new OpRegularParam();
        opRegularParam3.f1426a = 3;
        opRegularParam3.c = 0;
        opRegularParam3.b = l.a().b();
        opRegularParam3.d = k.a();
        try {
            StringBuilder append = new StringBuilder().append(m.a().t() ? 1 : 0).append(Constants.STR_EMPTY);
            if (!m.a().r()) {
                i = 0;
            }
            i2 = Integer.parseInt(append.append(i).toString(), 2);
        } catch (Exception e) {
        }
        if (c.a().c()) {
            i2 |= 4;
        }
        opRegularParam3.e = i2;
        getOpRegularInfoRequest.f1312a.add(opRegularParam3);
        return send(getOpRegularInfoRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null) {
            XLog.d("splashInfo", "response succeed ~ ~ ~");
            GetOpRegularInfoResponse getOpRegularInfoResponse = (GetOpRegularInfoResponse) jceStruct2;
            i.y().a(getOpRegularInfoResponse);
            m.a().b("otherpush_update_refresh_suc_time", Long.valueOf(System.currentTimeMillis()));
            PushInfo pushInfo = null;
            if (getOpRegularInfoResponse.a() != null && !getOpRegularInfoResponse.a().isEmpty()) {
                getOpRegularInfoResponse.a().size();
                a(a((byte) 1));
                pushInfo = b(a((byte) 3));
                a(pushInfo);
            }
            com.tencent.assistantv2.st.page.c.a(pushInfo);
            return;
        }
        XLog.d("splashInfo", "null response ... ");
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("splashInfo", "response failed ");
    }

    public ArrayList<Object> a(byte b2) {
        GetOpRegularInfoResponse d = i.y().d();
        ArrayList<Object> arrayList = new ArrayList<>();
        if (d == null) {
            return arrayList;
        }
        ArrayList<OpRegularInfo> a2 = d.a();
        if (a2 == null || a2.size() <= 0) {
            return arrayList;
        }
        Class<? extends JceStruct> b3 = b(b2);
        if (b3 == null) {
            return arrayList;
        }
        Iterator<OpRegularInfo> it = a2.iterator();
        while (it.hasNext()) {
            OpRegularInfo next = it.next();
            if (next.f1425a == b2) {
                arrayList.add(an.b(next.b, b3));
            }
        }
        return arrayList;
    }

    private Class<? extends JceStruct> b(byte b2) {
        switch (b2) {
            case 1:
                return FlashInfo.class;
            case 2:
                return PopUpInfo.class;
            case 3:
                return PushInfo.class;
            default:
                return null;
        }
    }

    private void a(ArrayList<Object> arrayList) {
        ArrayList arrayList2 = new ArrayList();
        if (arrayList != null && arrayList.size() > 0) {
            Iterator<Object> it = arrayList.iterator();
            while (it.hasNext()) {
                FlashInfo flashInfo = (FlashInfo) it.next();
                XLog.d("splashInfo", "flashInfo = " + flashInfo);
                if (flashInfo != null) {
                    arrayList2.add(new com.tencent.assistant.model.k(flashInfo));
                }
            }
        }
        SplashManager.a().a(arrayList2);
    }

    private void a(PushInfo pushInfo) {
        if (pushInfo != null) {
            l.a().a(pushInfo);
        }
    }

    private PushInfo b(ArrayList<Object> arrayList) {
        if (arrayList == null || arrayList.size() <= 0) {
            return null;
        }
        return (PushInfo) arrayList.get(0);
    }

    public static synchronized long c() {
        long j;
        synchronized (as.class) {
            j = b;
        }
        return j;
    }
}
