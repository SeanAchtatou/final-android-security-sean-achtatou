package com.tencent.pangu.module;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.CheckSelfUpdateRequest;
import com.tencent.assistant.protocol.jce.CheckSelfUpdateResponse;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.manager.SelfUpdateManager;
import com.tencent.pangu.module.a.l;

/* compiled from: ProGuard */
public class ax extends BaseEngine<l> {

    /* renamed from: a  reason: collision with root package name */
    private static ax f3915a = null;
    private int b = 0;
    private CheckSelfUpdateResponse c = null;
    private boolean d = false;
    private boolean e = false;

    public static synchronized ax a() {
        ax axVar;
        synchronized (ax.class) {
            if (f3915a == null) {
                f3915a = new ax();
            }
            axVar = f3915a;
        }
        return axVar;
    }

    public SelfUpdateManager.SelfUpdateInfo b() {
        return a(this.c);
    }

    public int a(String str, boolean z, boolean z2, long j) {
        this.d = z2;
        if (this.b > 0) {
            cancel(this.b);
        }
        this.b = a(str, z, j);
        return this.b;
    }

    private int a(String str, boolean z, long j) {
        int i = 1;
        CheckSelfUpdateRequest checkSelfUpdateRequest = new CheckSelfUpdateRequest();
        checkSelfUpdateRequest.f1192a = t.o();
        checkSelfUpdateRequest.b = str;
        if (z) {
            checkSelfUpdateRequest.d = 1;
        } else {
            checkSelfUpdateRequest.d = 0;
        }
        if (Global.isLite()) {
            checkSelfUpdateRequest.c = 1;
        } else {
            checkSelfUpdateRequest.c = 0;
        }
        XLog.d("jiaxinwu", "request versionCode" + checkSelfUpdateRequest.f1192a + "request.isManual " + ((int) checkSelfUpdateRequest.d) + "request.flag" + ((int) checkSelfUpdateRequest.c));
        checkSelfUpdateRequest.e = j;
        int a2 = m.a().a("bao_history_version_code", -1);
        if (a2 < 0 || a2 >= t.o()) {
            checkSelfUpdateRequest.f = t.o();
            if (!Global.hasInit()) {
                Global.init();
            }
            checkSelfUpdateRequest.g = Global.getQUA();
            this.e = false;
        } else {
            checkSelfUpdateRequest.f = a2;
            checkSelfUpdateRequest.g = m.a().a("key_history_qua", Constants.STR_EMPTY);
            this.e = true;
        }
        if (!AstApp.i().l()) {
            i = 0;
        }
        checkSelfUpdateRequest.h = (byte) i;
        XLog.d("CheckSelfUpdate", "send request versionCode" + checkSelfUpdateRequest.f1192a + "request.isManual " + ((int) checkSelfUpdateRequest.d) + "request.flag" + ((int) checkSelfUpdateRequest.c) + "request.isForeground" + ((int) checkSelfUpdateRequest.h));
        return send(checkSelfUpdateRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        CheckSelfUpdateResponse checkSelfUpdateResponse = (CheckSelfUpdateResponse) jceStruct2;
        if (checkSelfUpdateResponse != null) {
            XLog.d("CheckSelfUpdate", "success");
            if (checkSelfUpdateResponse.b != 0) {
                XLog.d("CheckSelfUpdate", "request success");
                i.y().a(checkSelfUpdateResponse);
                this.c = checkSelfUpdateResponse;
                m.a().b("same_version_cover", Boolean.valueOf(checkSelfUpdateResponse.B == 1));
                int o = t.o();
                if (checkSelfUpdateResponse.B != 1) {
                    a(i, checkSelfUpdateResponse);
                } else if (checkSelfUpdateResponse.b == o) {
                    if (SelfUpdateManager.a().a(checkSelfUpdateResponse.b)) {
                        SelfUpdateManager.a().b(checkSelfUpdateResponse.b);
                    }
                    XLog.d("CheckSelfUpdate", "processSameVersionSelfUpdate");
                    b(i, checkSelfUpdateResponse);
                } else {
                    XLog.d("CheckSelfUpdate", "processNormalSelfUpdate");
                    a(i, checkSelfUpdateResponse);
                }
                if (checkSelfUpdateResponse.A == 1) {
                    com.tencent.pangu.module.wisedownload.i.a().a(1);
                }
            } else {
                return;
            }
        }
        if (this.e) {
            m.a().b("bao_history_version_code", Integer.valueOf(t.o()));
            if (!Global.hasInit()) {
                Global.init();
            }
            m.a().b("key_history_qua", Global.getQUA());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("CheckSelfUpdate", "fail");
        if (!this.d) {
            m.a().b("update_force", (Object) false);
            if (SelfUpdateManager.a().h()) {
                ah.a().post(new ay(this));
                SelfUpdateManager.a().b(false);
            }
        } else {
            this.d = false;
        }
        notifyDataChangedInMainThread(new az(this, i, i2));
    }

    /* access modifiers changed from: private */
    public SelfUpdateManager.SelfUpdateInfo a(CheckSelfUpdateResponse checkSelfUpdateResponse) {
        boolean z = true;
        if (checkSelfUpdateResponse == null) {
            return null;
        }
        SelfUpdateManager.SelfUpdateInfo selfUpdateInfo = new SelfUpdateManager.SelfUpdateInfo();
        selfUpdateInfo.d = SelfUpdateManager.a().f();
        selfUpdateInfo.f3781a = checkSelfUpdateResponse.r;
        selfUpdateInfo.b = checkSelfUpdateResponse.q;
        selfUpdateInfo.c = checkSelfUpdateResponse.s;
        selfUpdateInfo.e = checkSelfUpdateResponse.b;
        selfUpdateInfo.f = checkSelfUpdateResponse.c;
        selfUpdateInfo.g = checkSelfUpdateResponse.e;
        selfUpdateInfo.h = checkSelfUpdateResponse.f;
        selfUpdateInfo.i = checkSelfUpdateResponse.h;
        selfUpdateInfo.j = k.a((byte) 1, checkSelfUpdateResponse.n);
        selfUpdateInfo.k = checkSelfUpdateResponse.i;
        selfUpdateInfo.l = checkSelfUpdateResponse.g;
        selfUpdateInfo.m = checkSelfUpdateResponse.k;
        selfUpdateInfo.n = k.a((byte) 2, checkSelfUpdateResponse.o);
        selfUpdateInfo.p = checkSelfUpdateResponse.l;
        selfUpdateInfo.o = checkSelfUpdateResponse.j;
        selfUpdateInfo.r = checkSelfUpdateResponse.y;
        selfUpdateInfo.s = checkSelfUpdateResponse.x;
        selfUpdateInfo.t = checkSelfUpdateResponse.m;
        if (checkSelfUpdateResponse.p != 1) {
            z = false;
        }
        selfUpdateInfo.u = z;
        selfUpdateInfo.v = checkSelfUpdateResponse.t;
        selfUpdateInfo.w = checkSelfUpdateResponse.u;
        selfUpdateInfo.x = checkSelfUpdateResponse.v;
        selfUpdateInfo.y = checkSelfUpdateResponse.w;
        selfUpdateInfo.q = checkSelfUpdateResponse.z;
        selfUpdateInfo.z = checkSelfUpdateResponse.A;
        selfUpdateInfo.A = checkSelfUpdateResponse.B;
        return selfUpdateInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    private void a(int i, CheckSelfUpdateResponse checkSelfUpdateResponse) {
        boolean z = true;
        int o = t.o();
        if (checkSelfUpdateResponse.b > o) {
            m.a().b("update_force", Boolean.valueOf(checkSelfUpdateResponse.d == 1));
            if (checkSelfUpdateResponse.b > m.a().a("update_newest_versioncode", 0)) {
                m a2 = m.a();
                if (TextUtils.isEmpty(checkSelfUpdateResponse.k)) {
                    z = false;
                }
                a2.b("update_isdiff", Boolean.valueOf(z));
            }
            m.a().b("update_newest_versioncode", Integer.valueOf(checkSelfUpdateResponse.b));
            SelfUpdateManager.a().o().a(checkSelfUpdateResponse.b);
            notifyDataChangedInMainThread(new ba(this, checkSelfUpdateResponse, i));
            if (this.d) {
                this.d = false;
            }
        } else if (this.d) {
            this.d = false;
            notifyDataChangedInMainThread(new bb(this, i));
            return;
        } else {
            m.a().b("update_force", (Object) false);
            m.a().b("update_isdiff", (Object) false);
            m.a().b("update_newest_versioncode", Integer.valueOf(o));
            if (SelfUpdateManager.a().h()) {
                ah.a().post(new bc(this));
                SelfUpdateManager.a().b(false);
            }
        }
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SELFUPDATE_UPDATE));
    }

    private void b(int i, CheckSelfUpdateResponse checkSelfUpdateResponse) {
        boolean z = true;
        if (SelfUpdateManager.a().a(checkSelfUpdateResponse.b)) {
            SelfUpdateManager.a().b(checkSelfUpdateResponse.b);
        }
        m.a().b("update_force", Boolean.valueOf(checkSelfUpdateResponse.d == 1));
        if (checkSelfUpdateResponse.b > m.a().a("update_newest_versioncode", 0)) {
            m a2 = m.a();
            if (TextUtils.isEmpty(checkSelfUpdateResponse.k)) {
                z = false;
            }
            a2.b("update_isdiff", Boolean.valueOf(z));
        }
        m.a().b("update_newest_versioncode", Integer.valueOf(checkSelfUpdateResponse.b));
        SelfUpdateManager.a().o().a(checkSelfUpdateResponse.b);
        notifyDataChangedInMainThread(new bd(this, checkSelfUpdateResponse, i));
        if (this.d) {
            this.d = false;
        }
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SELFUPDATE_UPDATE));
    }
}
