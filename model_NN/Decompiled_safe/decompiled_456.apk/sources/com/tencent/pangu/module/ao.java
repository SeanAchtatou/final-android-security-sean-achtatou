package com.tencent.pangu.module;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ao {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f3906a;
    private int b;
    /* access modifiers changed from: private */
    public int c;
    private long d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public ar f;
    private ar g;
    private ArrayList<SimpleAppModel> h;
    private ArrayList<SmartCardWrapper> i;
    private Handler j;

    private ao(ad adVar) {
        this.f3906a = adVar;
        this.b = 0;
        this.c = -1;
        this.d = -1;
        this.e = false;
        this.h = null;
        this.i = null;
        this.j = new ap(this, Looper.getMainLooper());
    }

    /* synthetic */ ao(ad adVar, ae aeVar) {
        this(adVar);
    }

    public synchronized void a(ar arVar, long j2) {
        if (arVar != null) {
            if (!(arVar.f3909a == null || arVar.f3909a.length == 0)) {
                this.j.removeMessages(6666);
                a();
                this.b = 1;
                Message obtainMessage = this.j.obtainMessage(6666);
                this.f = arVar;
                this.j.sendMessageDelayed(obtainMessage, j2);
            }
        }
    }

    public synchronized void a(ar arVar) {
        a(arVar, 200);
    }

    public synchronized void a() {
        this.b = 0;
        this.c = -1;
        this.f = null;
        this.g = null;
        this.e = false;
        this.h = null;
    }

    public synchronized int b() {
        return this.c;
    }

    public synchronized long c() {
        return this.d;
    }

    public synchronized ar d() {
        return this.f;
    }

    public ar e() {
        return this.g;
    }

    public synchronized ArrayList<SimpleAppModel> f() {
        return this.h;
    }

    public synchronized ArrayList<SmartCardWrapper> g() {
        return this.i;
    }

    public synchronized void a(long j2, ArrayList<SimpleAppModel> arrayList, ArrayList<SmartCardWrapper> arrayList2, boolean z, ar arVar) {
        this.d = j2;
        this.h = arrayList;
        this.e = z;
        this.i = arrayList2;
        this.g = arVar;
        this.b = 2;
    }

    public void h() {
        this.b = 2;
    }
}
