package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.db.table.ab;
import com.tencent.assistant.event.EventController;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.protocol.jce.DesktopShortCut;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.receiver.StorageLowReceiver;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.a.a;
import com.tencent.assistantv2.a.h;
import com.tencent.assistantv2.a.i;
import com.tencent.assistantv2.adapter.b;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.MainActionHeaderView;
import com.tencent.assistantv2.component.TXViewPager;
import com.tencent.assistantv2.component.TabView;
import com.tencent.assistantv2.component.TopTabContainer;
import com.tencent.assistantv2.component.TopTabWidget;
import com.tencent.assistantv2.manager.MainTabType;
import com.tencent.assistantv2.st.business.LaunchSpeedSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.c;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.module.a.k;
import com.tencent.pangu.module.a.o;
import com.tencent.pangu.module.au;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class MainActivity extends BaseActivity implements UIEventListener {
    public static int A;
    private static MainActivity M;
    public static int z = 0;
    GetPhoneUserAppListResponse B;
    boolean C = true;
    boolean D = true;
    boolean E = false;
    p F = new n(this);
    private Bundle G = new Bundle();
    private TXViewPager H;
    /* access modifiers changed from: private */
    public b I;
    /* access modifiers changed from: private */
    public TopTabWidget J;
    /* access modifiers changed from: private */
    public TopTabContainer K;
    /* access modifiers changed from: private */
    public int L = 0;
    private boolean N = false;
    /* access modifiers changed from: private */
    public boolean O = true;
    /* access modifiers changed from: private */
    public HorizontalScrollView P;
    private int Q;
    private int R = -1;
    /* access modifiers changed from: private */
    public float S = 0.0f;
    private h T;
    /* access modifiers changed from: private */
    public MainActionHeaderView U;
    private int V = 0;
    private StorageLowReceiver W = null;
    private boolean X = false;
    private Toast Y = null;
    private long Z = 0;
    /* access modifiers changed from: private */
    public ArrayList<DesktopShortCut> aa = null;
    /* access modifiers changed from: private */
    public ab ab = null;
    /* access modifiers changed from: private */
    public o ac = new m(this);
    /* access modifiers changed from: private */
    public k ad = new o(this);
    public boolean n = false;
    MainTabType u = MainTabType.VIDEO;
    int v = 0;
    Intent w;
    int x = 5;
    aa y;

    public int f() {
        if (this.I == null || this.I.a(this.L) == null) {
            return 2000;
        }
        return ((a) this.I.a(this.L)).G();
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    public void onCreate(Bundle bundle) {
        LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Main_onCreate_Begin);
        M = this;
        super.onCreate(bundle);
        w();
        System.currentTimeMillis();
        overridePendingTransition(-1, -1);
        System.currentTimeMillis();
        try {
            setContentView((int) R.layout.main);
            this.T = a.a().a(0);
            this.U = (MainActionHeaderView) findViewById(R.id.myactionbar);
            this.K = (TopTabContainer) findViewById(R.id.mytabs);
            this.P = (HorizontalScrollView) findViewById(R.id.my_tab_scroller);
            this.J = this.K.a();
            this.H = (TXViewPager) findViewById(R.id.viewpager);
            this.S = (float) (getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_lenth) + A);
            if (this.U != null) {
                this.U.a();
            }
            System.currentTimeMillis();
            x();
            this.K.a(this.L, this.S);
            f(this.L);
            c(getIntent());
            if (!(this.I == null || this.I.a(this.L) == null)) {
                TemporaryThreadManager.get().startDelayed(new i(this, this.L), 2000);
            }
            EventController.getInstance().addUIEventListener(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_DATA_LOAD, this);
            EventController.getInstance().addUIEventListener(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_ADAPTER_GET_VIEW_END, this);
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Main_onCreate_End);
        } catch (Throwable th) {
            this.X = true;
            t.a().b();
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
        z = RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal();
        if (z2 && this.B != null && this.B.c.size() > 0) {
            ah.a().post(new s(this));
        }
    }

    public static MainActivity t() {
        return M;
    }

    private void x() {
        int i = 0;
        int c = a.a().c(0);
        while (true) {
            int i2 = i;
            if (i2 < this.T.b.size()) {
                a(this.T.b.get(i2), i2, c);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.G.clear();
        c(intent);
    }

    private void c(Intent intent) {
        int i;
        int i2 = 0;
        this.w = intent;
        if (intent.getBooleanExtra("action_key_push_huanji", false)) {
            c.a(122, 14, false);
        }
        int intExtra = intent.getIntExtra("com.tencent.assistantv2.TAB_TYPE", MainTabType.DISCOVER.ordinal());
        Iterator<i> it = this.T.b.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext() || intExtra == it.next().b) {
                this.L = i;
                this.H.setCurrentItem(this.L);
                View b = this.J.b(this.L);
            } else {
                i2 = i + 1;
            }
        }
        this.L = i;
        this.H.setCurrentItem(this.L);
        View b2 = this.J.b(this.L);
        if (b2 != null) {
            b2.setSelected(true);
        }
        h(this.L);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            this.G.putAll(extras);
        }
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        TemporaryThreadManager.get().startDelayed(new t(this), 2000);
    }

    public Bundle u() {
        Bundle bundle = new Bundle(this.G);
        this.G.clear();
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        int i;
        this.n = true;
        super.onResume();
        if (!this.X) {
            if (this.C) {
                i = 50;
            } else {
                i = 10;
            }
            if (this.N && this.L < this.I.getCount()) {
                a aVar = (a) this.I.a(this.L);
                this.N = false;
                if (aVar.A()) {
                    ah.a().postDelayed(new u(this, aVar), (long) i);
                }
            }
            this.V = this.C ? EventDispatcherEnum.CACHE_EVENT_START : 100;
            ah.a().postDelayed(new v(this), (long) this.V);
            if (this.C) {
                this.C = false;
            }
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Main_onResume_Enter);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.n = false;
        super.onPause();
        if (!this.X) {
            overridePendingTransition(-1, -1);
            ah.a().post(new w(this));
            this.N = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!this.X) {
            ah.a().postDelayed(new x(this), (long) this.V);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.X) {
            B();
            ah.a().postDelayed(new y(this), (long) this.V);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_DATA_LOAD, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FIRST_PAGE_ADAPTER_GET_VIEW_END, this);
            a.a().b();
        }
    }

    private void f(int i) {
        this.H = (TXViewPager) findViewById(R.id.viewpager);
        if (this.I == null) {
            this.I = new b(e(), this, this.T.b, null);
        }
        this.H.setAdapter(this.I);
        this.H.setOnPageChangeListener(new ac(this));
        this.J.a(new z(this));
    }

    public void a(int i, boolean z2) {
        g(i);
        this.H.setCurrentItem(i);
        if (z2) {
            this.H.requestFocus(2);
        }
    }

    private void g(int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("04", i);
            if (i == this.I.getCount() - 1) {
                buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("04", i + 2);
                buildSTInfo.status = b(i);
            } else if (this.I.getCount() >= 2 && i == this.I.getCount() - 2) {
                buildSTInfo.status = b(i);
            }
        }
        l.a(buildSTInfo);
    }

    /* access modifiers changed from: package-private */
    public String b(int i) {
        if (i == this.I.getCount() - 1) {
            return bm.b(this.T.b.get(i).b);
        }
        if (this.I.getCount() < 2 || i != this.I.getCount() - 2) {
            return STConst.ST_STATUS_DEFAULT;
        }
        return bm.b(this.T.b.get(i).b);
    }

    public boolean a(SmartListAdapter.SmartListType smartListType) {
        i a2 = this.T.a(this.L);
        if (a2 == null) {
            return false;
        }
        MainTabType a3 = a2.a();
        if (a3 == MainTabType.DISCOVER && smartListType == SmartListAdapter.SmartListType.DiscoverPage) {
            return true;
        }
        if (a3 == MainTabType.APP && smartListType == SmartListAdapter.SmartListType.AppPage) {
            return true;
        }
        if (a3 == MainTabType.GAME && smartListType == SmartListAdapter.SmartListType.GamePage) {
            return true;
        }
        return false;
    }

    private void a(i iVar, int i, int i2) {
        TabView tabView = new TabView(getBaseContext());
        TextView textView = (TextView) tabView.findViewById(R.id.title);
        String str = iVar.f1863a;
        if (str.length() > 2) {
            str = str.substring(0, 2);
        }
        textView.setText(str);
        if (i2 < iVar.c) {
            tabView.a(iVar.c);
            this.R = i;
        }
        tabView.setTag(R.id.tma_st_slot_tag, Integer.valueOf(iVar.b));
        tabView.setTag(Integer.valueOf(i));
        this.J.addView(tabView);
        if (i < this.T.b.size() - 1) {
            this.J.a(i, this);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, String str) {
        a aVar = (a) this.I.a(i);
        if (aVar != null) {
            a(((BaseActivity) aVar.P).n(), com.tencent.assistantv2.st.page.a.a("04", i2), str);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i) {
        int i2;
        int i3;
        if (this.I.getCount() > 0) {
            if (this.Q == 0) {
                this.Q = getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_lenth);
            }
            int i4 = ((this.Q + A) * i) - A;
            try {
                i2 = getWindowManager().getDefaultDisplay().getWidth();
            } catch (Exception e) {
                Point point = new Point();
                getWindowManager().getDefaultDisplay().getSize(point);
                i2 = point.x;
            }
            int scrollX = this.P.getScrollX();
            if (i4 < scrollX) {
                i3 = i4 - scrollX;
            } else {
                i3 = ((this.Q + i4) + A) - scrollX > i2 ? (((this.Q + i4) + A) - scrollX) - i2 : 0;
            }
            if (i3 != 0) {
                ab abVar = new ab(this);
                abVar.a(scrollX, i3);
                this.P.post(abVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void h(int i) {
        int i2 = 0;
        if (this.I != null && this.I.a(i) != null) {
            a aVar = (a) this.I.a(i);
            d(aVar.E());
            e(aVar.F());
            if (this.R == i) {
                int c = a.a().c(0) + 1;
                i a2 = this.T.a(i);
                if (a2 != null && a2.c >= c) {
                    a.a().b(0);
                    i2 = c;
                }
                View b = this.J.b(i);
                if (b != null) {
                    ((TabView) b).a(i2);
                }
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        y();
        return true;
    }

    private void y() {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.Z > 2000) {
                if (this.Y != null) {
                    this.Y.cancel();
                }
                this.Z = currentTimeMillis;
                Toast makeText = Toast.makeText(this, getString(R.string.app_return_click_title), 0);
                this.Y = makeText;
                makeText.show();
                return;
            }
            if (this.Y != null) {
                this.Y.cancel();
            }
            FunctionUtils.a(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void d(int i) {
        if (this.U != null) {
            XLog.d("yanhui-srt", "main:type:" + i);
            this.U.a(i);
        }
    }

    public void e(int i) {
        if (this.U != null) {
            XLog.d("yanhui-hwc", "main:category:" + i);
            this.U.b(i);
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (this.D) {
            this.D = false;
            A();
            LaunchSpeedSTManager.d().a(LaunchSpeedSTManager.TYPE_TIME_POINT.Main_onWindowFocusChanged_Enter);
            ah.a().postDelayed(new j(this), 5000);
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        if (!this.E) {
            this.E = true;
            TemporaryThreadManager.get().startDelayed(new k(this), 500);
            TemporaryThreadManager.get().startDelayed(new l(this), 5000);
        }
    }

    /* access modifiers changed from: private */
    public void a(DesktopShortCut desktopShortCut) {
        this.ab.a(desktopShortCut);
        if (!TextUtils.isEmpty(desktopShortCut.c())) {
            if (ApkResourceManager.getInstance().getInstalledApkInfo(desktopShortCut.c()) == null && !TextUtils.isEmpty(desktopShortCut.a())) {
                com.tencent.assistant.thumbnailCache.k.b().a(desktopShortCut.a(), 1, this.F);
            }
        } else if (!TextUtils.isEmpty(desktopShortCut.e().a())) {
            com.tencent.assistant.thumbnailCache.k.b().a(desktopShortCut.a(), 1, this.F);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(DesktopShortCut desktopShortCut, DesktopShortCut desktopShortCut2) {
        if (TextUtils.isEmpty(desktopShortCut.c()) || TextUtils.isEmpty(desktopShortCut2.c())) {
            if (TextUtils.isEmpty(desktopShortCut.e().f1125a) || TextUtils.isEmpty(desktopShortCut2.e().f1125a)) {
                return false;
            }
            return desktopShortCut.e().f1125a.equals(desktopShortCut2.e().f1125a);
        } else if (!desktopShortCut.c().equals(desktopShortCut2.c())) {
            return false;
        } else {
            if ((TextUtils.isEmpty(desktopShortCut.d()) || TextUtils.isEmpty(desktopShortCut2.d()) || !desktopShortCut.d().equals(desktopShortCut2.d())) && (!TextUtils.isEmpty(desktopShortCut.d()) || !TextUtils.isEmpty(desktopShortCut2.d()))) {
                return false;
            }
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void a(ArrayList<DesktopShortCut> arrayList) {
        if (arrayList != null && arrayList.size() != 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < arrayList.size()) {
                    DesktopShortCut desktopShortCut = arrayList.get(i2);
                    if (!TextUtils.isEmpty(desktopShortCut.d) && ApkResourceManager.getInstance().getInstalledApkInfo(desktopShortCut.c()) != null && desktopShortCut.f() == 1) {
                        Intent intent = new Intent("com.tencent.assistant.SHORTCUT");
                        intent.setClassName("com.tencent.android.qqdownloader", "com.tencent.assistant.activity.ShortCutActivity");
                        intent.putExtra("pkgName", desktopShortCut.c());
                        if (!TextUtils.isEmpty(desktopShortCut.d())) {
                            intent.putExtra("channelId", desktopShortCut.d());
                        }
                        e.a(this, desktopShortCut.b(), intent);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Bitmap bitmap, String str, Intent intent) {
        e.a(this, bitmap, str, intent);
    }

    private void A() {
        if (this.W == null) {
            this.W = new StorageLowReceiver();
            registerReceiver(this.W, new IntentFilter("android.intent.action.DEVICE_STORAGE_LOW"));
        }
    }

    private void B() {
        if (this.W != null) {
            unregisterReceiver(this.W);
        }
    }

    public void showErrorPage(boolean z2) {
        if (this.I != null) {
            this.I.a(z2, this.v);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
     arg types: [com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void */
    /* access modifiers changed from: private */
    public void a(GetPhoneUserAppListResponse getPhoneUserAppListResponse, boolean z2) {
        if (AstApp.i().l()) {
            com.tencent.pangu.utils.e.a(this, false, f(), getPhoneUserAppListResponse, z2);
        } else {
            v.a().a(getPhoneUserAppListResponse, false);
        }
    }

    /* access modifiers changed from: private */
    public void d(Intent intent) {
        ah.a().post(new p(this, intent));
    }

    /* access modifiers changed from: package-private */
    public void v() {
        au auVar = new au();
        if (this.y == null) {
            this.y = new aa(this, null);
        }
        auVar.register(this.y);
        auVar.a(true);
        auVar.a();
    }

    /* access modifiers changed from: package-private */
    public void b(Intent intent) {
        r rVar = new r(this);
        rVar.hasTitle = true;
        rVar.titleRes = getResources().getString(R.string.video_down_tips);
        rVar.blockCaller = true;
        boolean booleanExtra = intent.getBooleanExtra("action_key_push_huanji", false);
        XLog.e("zhangyuanchao", "-------isFromPush-------" + booleanExtra);
        if (booleanExtra) {
            rVar.contentRes = getResources().getString(R.string.recover_old_apps_txt_with_tips);
        } else {
            rVar.contentRes = getResources().getString(R.string.recover_old_apps_txt);
        }
        rVar.lBtnTxtRes = getResources().getString(R.string.down_page_dialog_left_del);
        rVar.rBtnTxtRes = getResources().getString(R.string.qq_login_title);
        DialogUtils.show2BtnDialog(rVar);
        l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_QQ_LOGIN, STConst.ST_DEFAULT_SLOT, 0, STConst.ST_DEFAULT_SLOT, 100));
    }

    /* access modifiers changed from: package-private */
    public void w() {
        try {
            if ((com.tencent.assistant.utils.t.c == 800 && com.tencent.assistant.utils.t.b == 480) || ((com.tencent.assistant.utils.t.c == 854 && com.tencent.assistant.utils.t.b == 480) || ((com.tencent.assistant.utils.t.c == 480 && com.tencent.assistant.utils.t.b == 320) || ((com.tencent.assistant.utils.t.c == 960 && com.tencent.assistant.utils.t.b == 640) || (com.tencent.assistant.utils.t.c == 320 && com.tencent.assistant.utils.t.b == 240))))) {
                A = getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_divider_lenth_for_special);
            } else {
                A = getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_divider_lenth);
            }
        } catch (Exception e) {
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                if (message.obj instanceof Bundle) {
                    Bundle bundle = (Bundle) message.obj;
                    if (bundle.containsKey(AppConst.KEY_FROM_TYPE) && bundle.getInt(AppConst.KEY_FROM_TYPE) == 18) {
                        v();
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_FIRST_PAGE_DATA_LOAD:
                z();
                return;
            case EventDispatcherEnum.UI_EVENT_FIRST_PAGE_ADAPTER_GET_VIEW_END:
                LaunchSpeedSTManager.d().b();
                return;
            default:
                return;
        }
    }
}
