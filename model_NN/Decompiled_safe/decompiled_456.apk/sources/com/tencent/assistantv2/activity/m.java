package com.tencent.assistantv2.activity;

import com.tencent.assistant.db.table.ab;
import com.tencent.assistant.protocol.jce.DesktopShortCut;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.module.a.o;
import java.util.ArrayList;

/* compiled from: ProGuard */
class m extends o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f1879a;

    m(MainActivity mainActivity) {
        this.f1879a = mainActivity;
    }

    public void a(int i, int i2, ArrayList<DesktopShortCut> arrayList) {
        if (i2 == 0) {
            l.a(new STInfoV2(204002, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 100));
            if (this.f1879a.ab == null) {
                ab unused = this.f1879a.ab = new ab();
            }
            ArrayList<DesktopShortCut> a2 = this.f1879a.ab.a();
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList unused2 = this.f1879a.aa = arrayList;
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    DesktopShortCut desktopShortCut = arrayList.get(i3);
                    if (a2 == null || a2.size() == 0) {
                        this.f1879a.a(desktopShortCut);
                    } else {
                        boolean z = false;
                        for (int i4 = 0; i4 < a2.size(); i4++) {
                            if (this.f1879a.a(desktopShortCut, a2.get(i3))) {
                                z = true;
                            }
                        }
                        if (!z) {
                            this.f1879a.a(desktopShortCut);
                        }
                    }
                }
                this.f1879a.a(a2);
            }
        }
    }
}
