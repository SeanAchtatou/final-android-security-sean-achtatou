package com.tencent.assistantv2.st.page;

import com.tencent.assistant.AppConst;
import com.tencent.pangu.model.AbstractDownloadInfo;

/* compiled from: ProGuard */
/* synthetic */ class b {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f2061a = new int[AppConst.AppState.values().length];
    static final /* synthetic */ int[] b = new int[AbstractDownloadInfo.DownState.values().length];

    static {
        try {
            b[AbstractDownloadInfo.DownState.INIT.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            b[AbstractDownloadInfo.DownState.SUCC.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            b[AbstractDownloadInfo.DownState.PAUSED.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            b[AbstractDownloadInfo.DownState.FAIL.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            b[AbstractDownloadInfo.DownState.DOWNLOADING.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f2061a[AppConst.AppState.DOWNLOAD.ordinal()] = 1;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f2061a[AppConst.AppState.UPDATE.ordinal()] = 2;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f2061a[AppConst.AppState.DOWNLOADED.ordinal()] = 3;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f2061a[AppConst.AppState.INSTALLED.ordinal()] = 4;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f2061a[AppConst.AppState.PAUSED.ordinal()] = 5;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f2061a[AppConst.AppState.FAIL.ordinal()] = 6;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f2061a[AppConst.AppState.DOWNLOADING.ordinal()] = 7;
        } catch (NoSuchFieldError e12) {
        }
    }
}
