package com.tencent.assistantv2.st;

import android.os.Handler;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.db.table.z;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.callback.m;
import com.tencent.assistant.protocol.jce.StatAppDownlaodWithChunk;
import com.tencent.assistant.protocol.jce.StatAppInstall;
import com.tencent.assistant.protocol.jce.StatReportItem;
import com.tencent.assistant.protocol.jce.StatUserAction;
import com.tencent.assistant.st.STListener;
import com.tencent.assistant.st.g;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.an;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class b implements m, a {
    private static b f = null;
    private static Handler h = null;

    /* renamed from: a  reason: collision with root package name */
    private Map<Integer, BaseSTManagerV2> f2020a = Collections.synchronizedMap(new HashMap(10));
    /* access modifiers changed from: private */
    public Map<Integer, List<Long>> b = new HashMap();
    /* access modifiers changed from: private */
    public Map<Integer, g> c = Collections.synchronizedMap(new HashMap());
    /* access modifiers changed from: private */
    public k d = new k();
    /* access modifiers changed from: private */
    public com.tencent.assistant.st.b e = new com.tencent.assistant.st.b();
    private UIEventListener g = new c(this);

    private b() {
        h = ah.a("LogHandler");
        this.e.register(this);
        this.d.a(this);
        AstApp.i().k().addUIEventListener(1032, this.g);
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f == null) {
                f = new b();
            }
            bVar = f;
        }
        return bVar;
    }

    public void a(byte b2, JceStruct jceStruct) {
        if (h != null) {
            h.postDelayed(new d(this, b2, jceStruct), 20);
        } else {
            z.a().a(b2, an.a(jceStruct));
        }
    }

    public void b(byte b2, JceStruct jceStruct) {
        byte[] a2 = h.a(jceStruct);
        if (a2 != null) {
            b(b2, a2);
        }
    }

    public void b() {
        e eVar = new e(this);
        if (h != null) {
            h.postDelayed(eVar, 3000);
        } else {
            TemporaryThreadManager.get().start(eVar);
        }
    }

    public void c() {
        XLog.d("reportSTInstall", "实时系统安装日志的上报开始...");
        f fVar = new f(this);
        if (h != null) {
            h.post(fVar);
        } else {
            TemporaryThreadManager.get().start(fVar);
        }
    }

    public void a(STListener sTListener) {
        if (sTListener != null) {
            g gVar = new g(this, sTListener);
            if (h != null) {
                h.post(gVar);
            } else {
                TemporaryThreadManager.get().start(gVar);
            }
        }
    }

    public void d() {
        Object value;
        Object[] array = this.f2020a.entrySet().toArray();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f2020a.size()) {
                if (array != null && i2 < array.length && (value = ((Map.Entry) array[i2]).getValue()) != null && (value instanceof BaseSTManagerV2)) {
                    ((BaseSTManagerV2) value).flush();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void e() {
        d();
    }

    public BaseSTManagerV2 a(int i) {
        return this.f2020a.get(Integer.valueOf(i));
    }

    public void a(int i, BaseSTManagerV2 baseSTManagerV2) {
        if (baseSTManagerV2 != null) {
            baseSTManagerV2.setILogger(this);
            this.f2020a.put(Integer.valueOf(i), baseSTManagerV2);
        }
    }

    public void a(byte b2, List<byte[]> list) {
        i.a(b2, list);
    }

    public void a(byte b2, byte[] bArr) {
        TemporaryThreadManager.get().startDelayed(new h(this, b2, bArr), 1500);
    }

    public void a(int i, int i2) {
        byte[] a2;
        XLog.d("logReport", "**** onReportFinish errorCode" + i2);
        if (i2 != 0) {
            g gVar = this.c.get(Integer.valueOf(i));
            if (gVar != null) {
                if (gVar.c && (a2 = a(a(gVar.b.c, 4), gVar.b.f1562a)) != null && a2.length > 0) {
                    z.a().a(gVar.b.f1562a, a2);
                }
                this.c.remove(Integer.valueOf(i));
                return;
            }
            return;
        }
        List remove = this.b.remove(Integer.valueOf(i));
        if (remove != null && remove.size() > 0) {
            this.d.a(remove);
        }
    }

    /* access modifiers changed from: private */
    public void b(byte b2, byte[] bArr) {
        StatReportItem statReportItem = new StatReportItem();
        statReportItem.f1562a = b2;
        statReportItem.c = bArr;
        statReportItem.b = 0;
        ArrayList arrayList = new ArrayList();
        arrayList.add(statReportItem);
        XLog.d("logReport", "**** sendData 实时上报");
        int a2 = this.e.a(arrayList);
        if (this.c != null) {
            g gVar = new g();
            gVar.b = statReportItem;
            gVar.c = true;
            this.c.put(Integer.valueOf(a2), gVar);
        }
        if (bArr != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(bArr);
            i.a(b2, arrayList2);
        }
    }

    private byte[] a(byte[] bArr, int i) {
        if (bArr.length <= i) {
            return bArr;
        }
        byte[] bArr2 = new byte[(bArr.length - i)];
        System.arraycopy(bArr, i, bArr2, 0, bArr2.length);
        return bArr2;
    }

    private byte[] a(byte[] bArr, byte b2) {
        switch (b2) {
            case 5:
                StatAppInstall statAppInstall = (StatAppInstall) an.b(bArr, StatAppInstall.class);
                if (statAppInstall == null) {
                    return bArr;
                }
                statAppInstall.z = 1;
                return an.a(statAppInstall);
            case 6:
                StatUserAction statUserAction = (StatUserAction) an.b(bArr, StatUserAction.class);
                if (statUserAction == null) {
                    return bArr;
                }
                statUserAction.o = 1;
                return an.a(statUserAction);
            case 14:
                StatAppDownlaodWithChunk statAppDownlaodWithChunk = (StatAppDownlaodWithChunk) an.b(bArr, StatAppDownlaodWithChunk.class);
                if (statAppDownlaodWithChunk == null) {
                    return bArr;
                }
                statAppDownlaodWithChunk.B = 1;
                return an.a(statAppDownlaodWithChunk);
            default:
                return bArr;
        }
    }
}
