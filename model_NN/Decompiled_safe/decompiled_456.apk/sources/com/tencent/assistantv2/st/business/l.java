package com.tencent.assistantv2.st.business;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.utils.ai;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.cc;
import com.tencent.assistant.utils.t;
import com.tencent.feedback.eup.b;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
final class l implements b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2039a = false;

    l() {
    }

    public byte[] a(boolean z, String str, String str2, String str3, int i, long j) {
        return null;
    }

    public String b(boolean z, String str, String str2, String str3, int i, long j) {
        int i2;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("deviceInfo:").append(t.t()).append("\n");
        stringBuffer.append("qua:").append(Global.getQUA()).append("\n");
        StringBuilder sb = new StringBuilder();
        if (AstApp.m() != null) {
            sb.append(AstApp.m().getClass().getSimpleName());
        }
        sb.append(",").append(AstApp.o());
        stringBuffer.append("versionName:").append(Global.getAppVersionName4Crash() + "\n");
        AppConst.IdentityType d = j.a().d();
        StringBuffer stringBuffer2 = new StringBuffer();
        if (d == AppConst.IdentityType.WX) {
            stringBuffer2.append("wx:");
            stringBuffer2.append(j.a().s());
        } else if (d == AppConst.IdentityType.MOBILEQ) {
            stringBuffer2.append("qq:");
            stringBuffer2.append(j.a().p());
        } else {
            stringBuffer2.append(Global.getChannelId());
        }
        stringBuffer2.append("--");
        stringBuffer2.append(sb.toString());
        try {
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            if (contextClassLoader != null) {
                stringBuffer2.append("--");
                stringBuffer2.append(contextClassLoader.toString());
            }
        } catch (Exception | SecurityException e) {
        }
        stringBuffer2.append("--").append(k.f());
        stringBuffer.append("contact:").append(stringBuffer2.toString() + "\n");
        stringBuffer.append("uuid:").append(Global.getUUID4Crash() + "\n");
        StringBuffer append = stringBuffer.append("appliationCreateTime:");
        StringBuilder sb2 = new StringBuilder();
        if (AstApp.d > 0) {
            i2 = bo.a(Long.valueOf(AstApp.d));
        } else {
            i2 = 0;
        }
        append.append(sb2.append(i2).append("\n").toString());
        stringBuffer.append("crashTime:").append(bo.a() + "\n");
        String a2 = at.a(Runtime.getRuntime().maxMemory());
        String a3 = at.a(Runtime.getRuntime().totalMemory());
        stringBuffer.append("vmMemory:").append("maxMemory=" + a2 + ", totalMemory=" + a3 + ", freeMemory=" + at.a(Runtime.getRuntime().freeMemory()) + "\n");
        stringBuffer.append("QBver:" + cc.a() + "\n");
        stringBuffer.append("X5ver:" + cc.a(AstApp.i()) + "\n");
        return stringBuffer.toString();
    }

    public void a(boolean z) {
    }

    public boolean b(boolean z) {
        try {
            if (!this.f2039a) {
                return true;
            }
            new ai();
            ai.a();
            return true;
        } catch (Throwable th) {
            return true;
        }
    }

    public boolean a(boolean z, String str, String str2, String str3, int i, long j, String str4, String str5, String str6) {
        try {
            this.f2039a = false;
            return true;
        } catch (Throwable th) {
            this.f2039a = false;
            return true;
        }
    }
}
