package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.z;
import com.tencent.assistant.protocol.jce.StatAppInfo;
import com.tencent.assistant.protocol.jce.StatAppUnInstall;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.an;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class u extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private StatAppUnInstall f2045a;

    public u() {
        this.f2045a = null;
        this.f2045a = new StatAppUnInstall();
    }

    public void a(short s, short s2, ArrayList<StatAppInfo> arrayList) {
        this.f2045a.f1542a = s;
        this.f2045a.c = s2;
        if (this.f2045a.c > 0) {
            this.f2045a.b = true;
        }
        this.f2045a.d = h.a();
        this.f2045a.e = STConst.ST_PAGE_ASSISTANT;
        this.f2045a.f = arrayList;
        a();
    }

    private void a() {
        if (this.f2045a != null) {
            byte[] a2 = an.a(this.f2045a);
            if (a2.length > 0) {
                z.a().a(getSTType(), a2);
            }
        }
    }

    public byte getSTType() {
        return 10;
    }

    public void flush() {
        a();
    }
}
