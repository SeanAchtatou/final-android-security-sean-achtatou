package com.tencent.assistantv2.st.model;

import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class StatInfo extends STCommonInfo {

    /* renamed from: a  reason: collision with root package name */
    public long f2058a;
    public int b = 0;
    public String c = Constants.STR_EMPTY;
    public int d = 0;

    public StatInfo() {
    }

    public StatInfo(long j, int i, long j2, String str, long j3) {
        this.sourceScene = i;
        this.f2058a = j2;
        this.extraData = str;
        this.searchId = j3;
    }

    public StatInfo(long j, int i, long j2, String str, long j3, String str2) {
        this.sourceScene = i;
        this.f2058a = j2;
        this.extraData = str;
        this.searchId = j3;
        this.slotId = str2;
    }

    public StatInfo(STCommonInfo sTCommonInfo) {
        if (sTCommonInfo != null) {
            this.scene = sTCommonInfo.scene;
            this.sourceScene = sTCommonInfo.sourceScene;
            this.slotId = sTCommonInfo.slotId;
            this.sourceSceneSlotId = sTCommonInfo.sourceSceneSlotId;
            this.status = sTCommonInfo.status;
            this.actionId = sTCommonInfo.actionId;
            this.extraData = sTCommonInfo.extraData;
            this.recommendId = sTCommonInfo.recommendId;
            this.contentId = sTCommonInfo.contentId;
            this.pushId = sTCommonInfo.pushId;
            this.pushInfo = sTCommonInfo.pushInfo;
            this.callerVia = sTCommonInfo.callerVia;
            this.callerUin = sTCommonInfo.callerUin;
            this.callerPackageName = sTCommonInfo.callerPackageName;
            this.callerVersionCode = sTCommonInfo.callerVersionCode;
            this.traceId = sTCommonInfo.traceId;
            this.searchId = sTCommonInfo.searchId;
            this.searchPreId = sTCommonInfo.searchPreId;
            this.expatiation = sTCommonInfo.expatiation;
            this.rankGroupId = sTCommonInfo.rankGroupId;
            this.d = sTCommonInfo.actionFlag;
        }
    }
}
