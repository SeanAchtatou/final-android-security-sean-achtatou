package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.z;
import com.tencent.assistant.protocol.jce.StatPhotoBackup;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.an;

/* compiled from: ProGuard */
public class t extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private StatPhotoBackup f2044a;

    public t() {
        this.f2044a = null;
        this.f2044a = new StatPhotoBackup();
    }

    public void a(byte b, boolean z, short s) {
        if (this.f2044a != null) {
            this.f2044a.f1560a = b;
            this.f2044a.b = z;
            this.f2044a.c = s;
            this.f2044a.d = h.a();
        }
        a();
    }

    public void a() {
        byte[] a2;
        if (this.f2044a != null && (a2 = an.a(this.f2044a)) != null && a2.length > 0) {
            z.a().a(getSTType(), a2);
        }
    }

    public byte getSTType() {
        return 9;
    }

    public void flush() {
        a();
    }
}
