package com.tencent.assistantv2.st.business;

import android.os.Handler;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.z;
import com.tencent.assistant.protocol.jce.StatUserAction;
import com.tencent.assistant.st.f;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.an;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: ProGuard */
public class v extends BaseSTManagerV2 {
    private static Handler c = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public List<StatUserAction> f2046a = Collections.synchronizedList(new ArrayList());

    public v() {
        if (c == null) {
            c = ah.a("UserActionLogHandler");
        }
    }

    public byte getSTType() {
        return 6;
    }

    public void flush() {
        a();
    }

    public void report(STInfoV2 sTInfoV2) {
        if (c != null) {
            try {
                c.postDelayed(new w(this, sTInfoV2), 50);
            } catch (Exception e) {
            }
        }
    }

    public void reportRealTime(STInfoV2 sTInfoV2) {
        StatUserAction a2 = a(sTInfoV2);
        XLog.d("logReport", "**** 实时上报 类型 secen =" + a2.f1568a + " type = " + a2.b);
        if (sTInfoV2.recommendId != null) {
            XLog.d("logReport", "**** 实时上报 stInfo.recommendId.length=" + sTInfoV2.recommendId.length);
        }
        byte[] a3 = h.a(a2);
        if (this.b != null) {
            this.b.a(getSTType(), a3);
        }
    }

    public void a() {
        try {
            if (this.f2046a != null) {
                ArrayList arrayList = new ArrayList();
                ArrayList<StatUserAction> arrayList2 = new ArrayList<>(this.f2046a);
                for (StatUserAction a2 : arrayList2) {
                    arrayList.add(an.a(a2));
                }
                if (z.a().a(getSTType(), arrayList)) {
                    this.f2046a.removeAll(arrayList2);
                }
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public StatUserAction a(STInfoV2 sTInfoV2) {
        if (sTInfoV2 != null) {
            XLog.d("NewLog", "******** useraction scene=" + sTInfoV2.scene + " sourceScene=" + sTInfoV2.sourceScene + " status=" + sTInfoV2.status + " action=" + sTInfoV2.actionId + " slotId=" + sTInfoV2.getFinalSlotId() + " sourceslotId=" + sTInfoV2.sourceSceneSlotId + " extraData=" + sTInfoV2.extraData + " via=" + sTInfoV2.callerVia + " searchId=" + sTInfoV2.searchId + " searchPreId=" + sTInfoV2.searchPreId + " recommenId=" + sTInfoV2.recommendId + " traceId=" + sTInfoV2.traceId + " contentId=" + sTInfoV2.contentId + " pushInfo=" + sTInfoV2.pushInfo + " actionFlag=" + sTInfoV2.actionFlag + " isImmediately=" + sTInfoV2.isImmediately);
            if (sTInfoV2.recommendId != null) {
                XLog.d("NewLog", "******** useraction recommenId.length=" + sTInfoV2.recommendId.length);
            }
        }
        StatUserAction statUserAction = new StatUserAction();
        if (sTInfoV2 != null) {
            statUserAction.f1568a = sTInfoV2.scene;
            statUserAction.e = sTInfoV2.sourceScene;
            statUserAction.f = sTInfoV2.getFinalSlotId();
            statUserAction.u = sTInfoV2.sourceSceneSlotId;
            statUserAction.b = sTInfoV2.actionId;
            statUserAction.c = sTInfoV2.extraData;
            statUserAction.g = sTInfoV2.appId;
            statUserAction.p = sTInfoV2.packageName;
            statUserAction.h = 2;
            statUserAction.i = sTInfoV2.recommendId;
            statUserAction.w = sTInfoV2.pushInfo;
            statUserAction.v = sTInfoV2.contentId;
            statUserAction.r = sTInfoV2.searchPreId + "_" + sTInfoV2.searchId;
            statUserAction.s = sTInfoV2.expatiation;
            statUserAction.l = bm.c(sTInfoV2.callerUin);
            statUserAction.m = sTInfoV2.callerVia;
            statUserAction.t = bm.d(sTInfoV2.callerVersionCode);
            statUserAction.y = sTInfoV2.traceId;
            statUserAction.z = sTInfoV2.actionFlag;
        }
        statUserAction.j = d();
        if (sTInfoV2 == null || sTInfoV2.pushId <= 0) {
            statUserAction.n = f.b();
        } else {
            statUserAction.n = sTInfoV2.pushId;
        }
        statUserAction.q = f.a();
        statUserAction.x = Global.getAppVersion() + "_" + Global.getBuildNo();
        statUserAction.d = h.a();
        return statUserAction;
    }
}
