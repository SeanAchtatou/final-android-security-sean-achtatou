package com.tencent.assistantv2.component.fps;

import android.widget.ImageView;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class c extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FPSRankNormalItem f1998a;

    c(FPSRankNormalItem fPSRankNormalItem) {
        this.f1998a = fPSRankNormalItem;
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            a.a().a(downloadInfo);
            com.tencent.assistant.utils.a.a((ImageView) this.f1998a.j.findViewWithTag(downloadInfo.downloadTicket));
        }
    }
}
