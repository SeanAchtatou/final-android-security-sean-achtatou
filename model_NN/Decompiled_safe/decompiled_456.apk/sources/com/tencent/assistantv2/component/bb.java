package com.tencent.assistantv2.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bb extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f1977a;

    bb(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f1977a = secondNavigationTitleViewV5;
    }

    public void onTMAClick(View view) {
        this.f1977a.b.startActivity(new Intent(this.f1977a.b, MainActivity.class));
    }

    public STInfoV2 getStInfo() {
        return this.f1977a.e(a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, "001"));
    }
}
