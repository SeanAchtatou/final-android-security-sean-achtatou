package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/* compiled from: ProGuard */
public class FPSRankCutlineView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1993a = true;

    public FPSRankCutlineView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public FPSRankCutlineView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FPSRankCutlineView(Context context) {
        super(context);
    }

    public void setVisibility(int i) {
        this.f1993a = false;
        super.setVisibility(i);
        this.f1993a = true;
    }

    public void requestLayout() {
        if (this.f1993a) {
            super.requestLayout();
        }
    }
}
