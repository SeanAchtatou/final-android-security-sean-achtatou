package com.tencent.assistantv2.component;

import android.app.Activity;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

/* compiled from: ProGuard */
class ap implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public boolean f1963a = false;
    long b = 0;
    int c;
    int d;
    int e = 50;
    final /* synthetic */ MovingProgressBar f;
    private long g = -1;
    private int h = (-this.f.f);
    private Interpolator i = new DecelerateInterpolator();

    public ap(MovingProgressBar movingProgressBar) {
        this.f = movingProgressBar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.f1963a) {
            if (this.g == -1) {
                this.g = System.currentTimeMillis();
            } else {
                this.b = ((System.currentTimeMillis() - this.g) * 1000) / ((long) this.f.e);
                this.b = Math.max(Math.min(this.b, 1000L), 0L);
                this.c = (this.f.getWidth() + this.f.f) - this.h;
                this.d = Math.round((((float) this.c) * this.i.getInterpolation(((float) this.b) / 1000.0f)) + ((float) this.h));
                this.f.b.leftMargin = this.d;
                this.f.f1937a.layout(this.f.b.leftMargin, this.f.f1937a.getTop(), this.f.f1937a.getWidth() + this.f.b.leftMargin, this.f.f1937a.getBottom());
                this.f.f1937a.requestLayout();
            }
            if (this.b >= 1000) {
                a();
            } else if (this.f1963a && (this.f.h instanceof Activity) && !((Activity) this.f.h).isFinishing()) {
                this.f.postDelayed(this, (long) this.e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f1963a && (this.f.h instanceof Activity) && !((Activity) this.f.h).isFinishing()) {
            this.g = System.currentTimeMillis();
            this.f.postDelayed(this, (long) this.e);
        }
    }
}
