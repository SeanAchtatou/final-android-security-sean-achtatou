package com.tencent.assistantv2.component;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class TXViewPager extends ViewPager {
    public TXViewPager(Context context) {
        super(context);
    }

    public TXViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return super.onTouchEvent(motionEvent);
        } catch (Exception e) {
            XLog.i("TxViewPager", e.getMessage());
            return true;
        }
    }
}
