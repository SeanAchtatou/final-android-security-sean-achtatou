package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/* compiled from: ProGuard */
public class FPSImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private int f1989a = -1;
    private int b = -1;

    public FPSImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public FPSImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FPSImageView(Context context) {
        super(context);
    }

    public void setVisibility(int i) {
        if (getVisibility() != i) {
            super.setVisibility(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (FPSTextView.a()) {
            super.onMeasure(i, i2);
        } else if (this.f1989a == -1 || this.f1989a == 16777215 || this.b == -1 || this.b == 16777215 || this.b > 300 || this.f1989a > 300) {
            super.onMeasure(i, i2);
            this.f1989a = getMeasuredWidth();
            this.b = getMeasuredHeight();
        } else {
            setMeasuredDimension(this.f1989a, this.b);
        }
    }
}
