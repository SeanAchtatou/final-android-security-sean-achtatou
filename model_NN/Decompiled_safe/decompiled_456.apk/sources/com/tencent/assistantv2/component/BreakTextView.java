package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: ProGuard */
public class BreakTextView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private e f1925a;

    public void a(e eVar) {
        this.f1925a = eVar;
    }

    public BreakTextView(Context context) {
        super(context);
    }

    public BreakTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private int b() {
        return (getWidth() - getPaddingLeft()) - getPaddingRight();
    }

    public boolean a() {
        if (getPaint().measureText(getText().toString()) > ((float) b())) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f1925a != null) {
            this.f1925a.a(a());
        }
    }
}
