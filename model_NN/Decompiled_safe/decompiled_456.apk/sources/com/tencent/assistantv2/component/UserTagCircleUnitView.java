package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class UserTagCircleUnitView extends TextView {
    private static final String c = UserTagCircleUnitView.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    protected float f1947a;
    protected int b;
    private int d;
    private float e;
    private float f;
    private int g;
    private Context h;
    private float i;
    private float j;
    private float k;
    private int l;
    private float m;
    private float n;
    private float o;
    private float p;
    private float q;
    private int r;
    private int s;
    private int t;
    private int u;
    private boolean v;
    private AppTagInfo w;

    public UserTagCircleUnitView(Context context) {
        this(context, null);
        this.h = context;
    }

    public UserTagCircleUnitView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.h = context;
    }

    public UserTagCircleUnitView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = 0;
        this.e = 0.01f;
        this.f = 1.0f;
        this.g = 1;
        this.i = 0.0f;
        this.j = 0.0f;
        this.k = 0.0f;
        this.l = 0;
        this.m = 0.0f;
        this.n = 0.0f;
        this.o = 0.0f;
        this.p = 0.0f;
        this.q = 0.0f;
        this.r = 0;
        this.s = 0;
        this.t = 1048575;
        this.u = 16777215;
        this.f1947a = 0.0f;
        this.b = 16777215;
        this.v = false;
        this.w = null;
        this.h = context;
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.s);
        if (obtainStyledAttributes != null) {
            float f2 = (float) obtainStyledAttributes.getInt(0, 0);
            this.i = f2;
            this.j = f2;
            this.l = obtainStyledAttributes.getInt(1, 0);
            this.k = obtainStyledAttributes.getFloat(2, 0.0f);
            float a2 = (float) by.a(this.h, obtainStyledAttributes.getFloat(3, 0.0f));
            this.n = a2;
            this.o = a2;
            this.g = obtainStyledAttributes.getInt(4, 1);
            obtainStyledAttributes.recycle();
        }
        setBackgroundResource(R.drawable.user_tags_circle_bg);
        a(getText().toString());
        setMaxLines(2);
        setGravity(17);
        setTextColor(this.b);
        this.m = UserTagAnimationView.a(this.l);
        this.p = (float) (((double) this.m) * Math.cos((((double) this.j) * 3.141592653589793d) / 180.0d));
        this.q = (float) (((double) this.m) * Math.sin((((double) this.j) * 3.141592653589793d) / 180.0d));
    }

    public void a() {
        if (this.g == 1) {
            this.j = (this.j + this.k) % 360.0f;
        } else if (this.g == 2) {
            this.j = (this.j - this.k) % 360.0f;
        }
        double d2 = (((double) this.j) * 3.141592653589793d) / 180.0d;
        this.p = (float) (((double) this.m) * Math.cos(d2));
        this.q = (float) (Math.sin(d2) * ((double) this.m));
        if (!this.v) {
            this.o = this.n;
            this.d = 0;
            this.f = 1.0f;
        } else if (this.d < 10) {
            this.f += this.e;
            this.o = this.f * this.n;
            this.d++;
        }
    }

    public void a(String str) {
        int length = str.length();
        if (length >= 5) {
            String str2 = str.toString();
            setText(str2.substring(0, 3) + "\n" + str2.substring(3, 5));
            setTextSize((float) by.b(getContext(), getTextSize() - 2.0f));
        } else if (length >= 4) {
            String str3 = str.toString();
            setText(str3.substring(0, 2) + "\n" + str3.substring(2));
        } else {
            setText(str);
        }
    }

    public void setTextColor(int i2) {
        super.setTextColor(Color.rgb((16711680 & i2) >> 16, (65280 & i2) >> 8, i2 & 255));
    }

    public float b() {
        return this.p;
    }

    public float c() {
        return this.q;
    }

    public float d() {
        return this.o;
    }

    public boolean e() {
        return this.v;
    }

    public void a(boolean z) {
        this.v = z;
    }

    public float f() {
        return this.f;
    }

    public AppTagInfo g() {
        return this.w;
    }

    public void a(AppTagInfo appTagInfo) {
        this.w = appTagInfo;
    }
}
