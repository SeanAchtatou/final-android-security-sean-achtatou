package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.cloud.model.SimpleEbookModel;
import com.tencent.pangu.model.b;

/* compiled from: ProGuard */
public class g extends a {
    private final String c = "EbookNormalItemView";
    private IViewInvalidater d;
    private aa e;

    public g(Context context, aa aaVar, IViewInvalidater iViewInvalidater) {
        super(context, aaVar);
        this.d = iViewInvalidater;
        this.e = aaVar;
    }

    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f1900a).inflate((int) R.layout.ebook_normal_universal_item, (ViewGroup) null);
        i iVar = new i();
        iVar.f1912a = (TXAppIconView) inflate.findViewById(R.id.ebook_icon);
        iVar.f1912a.setInvalidater(this.d);
        iVar.b = (TextView) inflate.findViewById(R.id.ebook_title);
        iVar.c = (TextView) inflate.findViewById(R.id.ebook_author);
        iVar.d = (TextView) inflate.findViewById(R.id.ebook_view_count);
        iVar.e = (TextView) inflate.findViewById(R.id.ebook_desc);
        return Pair.create(inflate, iVar);
    }

    public void a(View view, Object obj, int i, b bVar) {
        SimpleEbookModel simpleEbookModel;
        i iVar = (i) obj;
        if (bVar != null) {
            simpleEbookModel = bVar.e();
        } else {
            simpleEbookModel = null;
        }
        view.setOnClickListener(new j(this, this.f1900a, i, simpleEbookModel, this.b));
        a(iVar, simpleEbookModel, i);
    }

    private void a(i iVar, SimpleEbookModel simpleEbookModel, int i) {
        if (simpleEbookModel != null && iVar != null) {
            iVar.f1912a.updateImageView(simpleEbookModel.c, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            iVar.b.setText(simpleEbookModel.b);
            if (!TextUtils.isEmpty(bm.f(simpleEbookModel.d))) {
                iVar.c.setPadding(0, 0, by.a(this.f1900a, 10.0f), 0);
                iVar.c.setText(simpleEbookModel.d);
            } else {
                iVar.c.setPadding(0, 0, 0, 0);
            }
            iVar.d.setText(simpleEbookModel.h);
            iVar.e.setText(bm.e(simpleEbookModel.e));
        }
    }
}
