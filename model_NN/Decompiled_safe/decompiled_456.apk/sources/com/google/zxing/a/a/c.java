package com.google.zxing.a.a;

import android.os.Process;
import com.google.zxing.FormatException;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f118a = {'*', '*', '*', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private static final char[] b = {'!', '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_'};
    private static final char[] c = {'*', '*', '*', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private static final char[] d = {'\'', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '{', '|', '}', '~', 127};

    private c() {
    }

    private static byte a(int i, int i2) {
        int i3 = i - (((i2 * 149) % 255) + 1);
        if (i3 < 0) {
            i3 += Process.PROC_COMBINE;
        }
        return (byte) i3;
    }

    private static int a(com.google.zxing.common.c cVar, StringBuffer stringBuffer, StringBuffer stringBuffer2) {
        boolean z = false;
        do {
            int a2 = cVar.a(8);
            if (a2 == 0) {
                throw FormatException.a();
            } else if (a2 <= 128) {
                stringBuffer.append((char) ((z ? a2 + 128 : a2) - 1));
                return 1;
            } else if (a2 == 129) {
                return 0;
            } else {
                if (a2 <= 229) {
                    int i = a2 - 130;
                    if (i < 10) {
                        stringBuffer.append('0');
                    }
                    stringBuffer.append(i);
                } else if (a2 == 230) {
                    return 2;
                } else {
                    if (a2 == 231) {
                        return 6;
                    }
                    if (!(a2 == 232 || a2 == 233 || a2 == 234)) {
                        if (a2 == 235) {
                            z = true;
                        } else if (a2 == 236) {
                            stringBuffer.append("[)>\u001e05\u001d");
                            stringBuffer2.insert(0, "\u001e\u0004");
                        } else if (a2 == 237) {
                            stringBuffer.append("[)>\u001e06\u001d");
                            stringBuffer2.insert(0, "\u001e\u0004");
                        } else if (a2 == 238) {
                            return 4;
                        } else {
                            if (a2 == 239) {
                                return 3;
                            }
                            if (a2 == 240) {
                                return 5;
                            }
                            if (a2 != 241 && a2 >= 242) {
                                throw FormatException.a();
                            }
                        }
                    }
                }
            }
        } while (cVar.a() > 0);
        return 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.google.zxing.common.g a(byte[] r8) {
        /*
            r1 = 0
            r3 = 1
            com.google.zxing.common.c r4 = new com.google.zxing.common.c
            r4.<init>(r8)
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r0 = 100
            r5.<init>(r0)
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r0 = 0
            r6.<init>(r0)
            java.util.Vector r0 = new java.util.Vector
            r0.<init>(r3)
            r2 = r3
        L_0x001a:
            if (r2 != r3) goto L_0x0046
            int r2 = a(r4, r5, r6)
        L_0x0020:
            if (r2 == 0) goto L_0x0028
            int r7 = r4.a()
            if (r7 > 0) goto L_0x001a
        L_0x0028:
            int r2 = r6.length()
            if (r2 <= 0) goto L_0x0035
            java.lang.String r2 = r6.toString()
            r5.append(r2)
        L_0x0035:
            com.google.zxing.common.g r2 = new com.google.zxing.common.g
            java.lang.String r3 = r5.toString()
            boolean r4 = r0.isEmpty()
            if (r4 == 0) goto L_0x0042
            r0 = r1
        L_0x0042:
            r2.<init>(r8, r3, r0, r1)
            return r2
        L_0x0046:
            switch(r2) {
                case 2: goto L_0x004e;
                case 3: goto L_0x0053;
                case 4: goto L_0x0057;
                case 5: goto L_0x005b;
                case 6: goto L_0x005f;
                default: goto L_0x0049;
            }
        L_0x0049:
            com.google.zxing.FormatException r0 = com.google.zxing.FormatException.a()
            throw r0
        L_0x004e:
            a(r4, r5)
        L_0x0051:
            r2 = r3
            goto L_0x0020
        L_0x0053:
            b(r4, r5)
            goto L_0x0051
        L_0x0057:
            c(r4, r5)
            goto L_0x0051
        L_0x005b:
            d(r4, r5)
            goto L_0x0051
        L_0x005f:
            a(r4, r5, r0)
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.a.a.c.a(byte[]):com.google.zxing.common.g");
    }

    private static void a(int i, int i2, int[] iArr) {
        int i3 = ((i << 8) + i2) - 1;
        int i4 = i3 / 1600;
        iArr[0] = i4;
        int i5 = i3 - (i4 * 1600);
        int i6 = i5 / 40;
        iArr[1] = i6;
        iArr[2] = i5 - (i6 * 40);
    }

    private static void a(com.google.zxing.common.c cVar, StringBuffer stringBuffer) {
        int a2;
        boolean z;
        int i;
        int[] iArr = new int[3];
        boolean z2 = false;
        while (cVar.a() != 8 && (a2 = cVar.a(8)) != 254) {
            a(a2, cVar.a(8), iArr);
            int i2 = 0;
            int i3 = 0;
            while (i2 < 3) {
                int i4 = iArr[i2];
                switch (i3) {
                    case 0:
                        if (i4 >= 3) {
                            if (!z2) {
                                stringBuffer.append(f118a[i4]);
                                int i5 = i3;
                                z = z2;
                                i = i5;
                                break;
                            } else {
                                stringBuffer.append((char) (f118a[i4] + 128));
                                i = i3;
                                z = false;
                                break;
                            }
                        } else {
                            z = z2;
                            i = i4 + 1;
                            break;
                        }
                    case 1:
                        if (z2) {
                            stringBuffer.append((char) (i4 + 128));
                            z2 = false;
                        } else {
                            stringBuffer.append(i4);
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 2:
                        if (i4 < 27) {
                            if (z2) {
                                stringBuffer.append((char) (b[i4] + 128));
                                z2 = false;
                            } else {
                                stringBuffer.append(b[i4]);
                            }
                        } else if (i4 == 27) {
                            throw FormatException.a();
                        } else if (i4 == 30) {
                            z2 = true;
                        } else {
                            throw FormatException.a();
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 3:
                        if (z2) {
                            stringBuffer.append((char) (i4 + 224));
                            z2 = false;
                        } else {
                            stringBuffer.append((char) (i4 + 96));
                        }
                        z = z2;
                        i = 0;
                        break;
                    default:
                        throw FormatException.a();
                }
                i2++;
                int i6 = i;
                z2 = z;
                i3 = i6;
            }
            if (cVar.a() <= 0) {
                return;
            }
        }
    }

    private static void a(com.google.zxing.common.c cVar, StringBuffer stringBuffer, Vector vector) {
        int a2 = cVar.a(8);
        if (a2 == 0) {
            a2 = cVar.a() / 8;
        } else if (a2 >= 250) {
            a2 = ((a2 - 249) * 250) + cVar.a(8);
        }
        byte[] bArr = new byte[a2];
        for (int i = 0; i < a2; i++) {
            if (cVar.a() < 8) {
                throw FormatException.a();
            }
            bArr[i] = a(cVar.a(8), i);
        }
        vector.addElement(bArr);
        try {
            stringBuffer.append(new String(bArr, "ISO8859_1"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(new StringBuffer().append("Platform does not support required encoding: ").append(e).toString());
        }
    }

    private static void b(com.google.zxing.common.c cVar, StringBuffer stringBuffer) {
        int a2;
        boolean z;
        int i;
        int[] iArr = new int[3];
        boolean z2 = false;
        while (cVar.a() != 8 && (a2 = cVar.a(8)) != 254) {
            a(a2, cVar.a(8), iArr);
            int i2 = 0;
            int i3 = 0;
            while (i2 < 3) {
                int i4 = iArr[i2];
                switch (i3) {
                    case 0:
                        if (i4 >= 3) {
                            if (!z2) {
                                stringBuffer.append(c[i4]);
                                int i5 = i3;
                                z = z2;
                                i = i5;
                                break;
                            } else {
                                stringBuffer.append((char) (c[i4] + 128));
                                i = i3;
                                z = false;
                                break;
                            }
                        } else {
                            z = z2;
                            i = i4 + 1;
                            break;
                        }
                    case 1:
                        if (z2) {
                            stringBuffer.append((char) (i4 + 128));
                            z2 = false;
                        } else {
                            stringBuffer.append(i4);
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 2:
                        if (i4 < 27) {
                            if (z2) {
                                stringBuffer.append((char) (b[i4] + 128));
                                z2 = false;
                            } else {
                                stringBuffer.append(b[i4]);
                            }
                        } else if (i4 == 27) {
                            throw FormatException.a();
                        } else if (i4 == 30) {
                            z2 = true;
                        } else {
                            throw FormatException.a();
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 3:
                        if (z2) {
                            stringBuffer.append((char) (d[i4] + 128));
                            z2 = false;
                        } else {
                            stringBuffer.append(d[i4]);
                        }
                        z = z2;
                        i = 0;
                        break;
                    default:
                        throw FormatException.a();
                }
                i2++;
                int i6 = i;
                z2 = z;
                i3 = i6;
            }
            if (cVar.a() <= 0) {
                return;
            }
        }
    }

    private static void c(com.google.zxing.common.c cVar, StringBuffer stringBuffer) {
        int a2;
        int[] iArr = new int[3];
        while (cVar.a() != 8 && (a2 = cVar.a(8)) != 254) {
            a(a2, cVar.a(8), iArr);
            for (int i = 0; i < 3; i++) {
                int i2 = iArr[i];
                if (i2 == 0) {
                    stringBuffer.append(13);
                } else if (i2 == 1) {
                    stringBuffer.append('*');
                } else if (i2 == 2) {
                    stringBuffer.append('>');
                } else if (i2 == 3) {
                    stringBuffer.append(' ');
                } else if (i2 < 14) {
                    stringBuffer.append((char) (i2 + 44));
                } else if (i2 < 40) {
                    stringBuffer.append((char) (i2 + 51));
                } else {
                    throw FormatException.a();
                }
            }
            if (cVar.a() <= 0) {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void d(com.google.zxing.common.c r5, java.lang.StringBuffer r6) {
        /*
            r1 = 0
            r0 = r1
        L_0x0002:
            int r2 = r5.a()
            r3 = 16
            if (r2 > r3) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            r4 = r1
        L_0x000c:
            r2 = 4
            if (r4 >= r2) goto L_0x002a
            r2 = 6
            int r2 = r5.a(r2)
            r3 = 11111(0x2b67, float:1.557E-41)
            if (r2 != r3) goto L_0x0035
            r0 = 1
            r3 = r0
        L_0x001a:
            if (r3 != 0) goto L_0x0025
            r0 = r2 & 32
            if (r0 != 0) goto L_0x0033
            r0 = r2 | 64
        L_0x0022:
            r6.append(r0)
        L_0x0025:
            int r0 = r4 + 1
            r4 = r0
            r0 = r3
            goto L_0x000c
        L_0x002a:
            if (r0 != 0) goto L_0x000a
            int r2 = r5.a()
            if (r2 > 0) goto L_0x0002
            goto L_0x000a
        L_0x0033:
            r0 = r2
            goto L_0x0022
        L_0x0035:
            r3 = r0
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.a.a.c.d(com.google.zxing.common.c, java.lang.StringBuffer):void");
    }
}
