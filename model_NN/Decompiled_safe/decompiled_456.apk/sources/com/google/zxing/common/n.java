package com.google.zxing.common;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private final float f167a;
    private final float b;
    private final float c;
    private final float d;
    private final float e;
    private final float f;
    private final float g;
    private final float h;
    private final float i;

    private n(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        this.f167a = f2;
        this.b = f5;
        this.c = f8;
        this.d = f3;
        this.e = f6;
        this.f = f9;
        this.g = f4;
        this.h = f7;
        this.i = f10;
    }

    public static n a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        float f10 = f9 - f7;
        float f11 = ((f3 - f5) + f7) - f9;
        if (f10 == 0.0f && f11 == 0.0f) {
            return new n(f4 - f2, f6 - f4, f2, f5 - f3, f7 - f5, f3, 0.0f, 0.0f, 1.0f);
        }
        float f12 = f4 - f6;
        float f13 = f8 - f6;
        float f14 = ((f2 - f4) + f6) - f8;
        float f15 = f5 - f7;
        float f16 = (f12 * f10) - (f13 * f15);
        float f17 = ((f10 * f14) - (f13 * f11)) / f16;
        float f18 = ((f12 * f11) - (f14 * f15)) / f16;
        return new n((f4 - f2) + (f17 * f4), (f8 - f2) + (f18 * f8), f2, (f17 * f5) + (f5 - f3), (f18 * f9) + (f9 - f3), f3, f17, f18, 1.0f);
    }

    public static n a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16, float f17) {
        return a(f10, f11, f12, f13, f14, f15, f16, f17).a(b(f2, f3, f4, f5, f6, f7, f8, f9));
    }

    public static n b(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        return a(f2, f3, f4, f5, f6, f7, f8, f9).a();
    }

    /* access modifiers changed from: package-private */
    public n a() {
        return new n((this.e * this.i) - (this.f * this.h), (this.f * this.g) - (this.d * this.i), (this.d * this.h) - (this.e * this.g), (this.c * this.h) - (this.b * this.i), (this.f167a * this.i) - (this.c * this.g), (this.b * this.g) - (this.f167a * this.h), (this.b * this.f) - (this.c * this.e), (this.c * this.d) - (this.f167a * this.f), (this.f167a * this.e) - (this.b * this.d));
    }

    /* access modifiers changed from: package-private */
    public n a(n nVar) {
        return new n((this.f167a * nVar.f167a) + (this.d * nVar.b) + (this.g * nVar.c), (this.f167a * nVar.d) + (this.d * nVar.e) + (this.g * nVar.f), (this.f167a * nVar.g) + (this.d * nVar.h) + (this.g * nVar.i), (this.b * nVar.f167a) + (this.e * nVar.b) + (this.h * nVar.c), (this.b * nVar.d) + (this.e * nVar.e) + (this.h * nVar.f), (this.b * nVar.g) + (this.e * nVar.h) + (this.h * nVar.i), (this.c * nVar.f167a) + (this.f * nVar.b) + (this.i * nVar.c), (this.c * nVar.d) + (this.f * nVar.e) + (this.i * nVar.f), (this.c * nVar.g) + (this.f * nVar.h) + (this.i * nVar.i));
    }

    public void a(float[] fArr) {
        int length = fArr.length;
        float f2 = this.f167a;
        float f3 = this.b;
        float f4 = this.c;
        float f5 = this.d;
        float f6 = this.e;
        float f7 = this.f;
        float f8 = this.g;
        float f9 = this.h;
        float f10 = this.i;
        for (int i2 = 0; i2 < length; i2 += 2) {
            float f11 = fArr[i2];
            float f12 = fArr[i2 + 1];
            float f13 = (f4 * f11) + (f7 * f12) + f10;
            fArr[i2] = (((f2 * f11) + (f5 * f12)) + f8) / f13;
            fArr[i2 + 1] = (((f11 * f3) + (f12 * f6)) + f9) / f13;
        }
    }
}
