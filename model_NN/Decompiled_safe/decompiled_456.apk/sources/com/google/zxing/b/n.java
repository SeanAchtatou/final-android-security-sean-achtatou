package com.google.zxing.b;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.h;
import com.google.zxing.i;
import com.google.zxing.j;
import com.google.zxing.k;
import java.util.Hashtable;

public abstract class n extends k {
    static final int[] b = {1, 1, 1};
    static final int[] c = {1, 1, 1, 1, 1};
    static final int[][] d = {new int[]{3, 2, 1, 1}, new int[]{2, 2, 2, 1}, new int[]{2, 1, 2, 2}, new int[]{1, 4, 1, 1}, new int[]{1, 1, 3, 2}, new int[]{1, 2, 3, 1}, new int[]{1, 1, 1, 4}, new int[]{1, 3, 1, 2}, new int[]{1, 2, 1, 3}, new int[]{3, 1, 1, 2}};
    static final int[][] e = new int[20][];

    /* renamed from: a  reason: collision with root package name */
    private final StringBuffer f149a = new StringBuffer(20);
    private final m f = new m();
    private final g g = new g();

    static {
        int i = 10;
        for (int i2 = 0; i2 < 10; i2++) {
            e[i2] = d[i2];
        }
        while (true) {
            int i3 = i;
            if (i3 < 20) {
                int[] iArr = d[i3 - 10];
                int[] iArr2 = new int[iArr.length];
                for (int i4 = 0; i4 < iArr.length; i4++) {
                    iArr2[i4] = iArr[(iArr.length - i4) - 1];
                }
                e[i3] = iArr2;
                i = i3 + 1;
            } else {
                return;
            }
        }
    }

    protected n() {
    }

    static int a(a aVar, int[] iArr, int i, int[][] iArr2) {
        a(aVar, i, iArr);
        int i2 = 107;
        int i3 = -1;
        int length = iArr2.length;
        int i4 = 0;
        while (i4 < length) {
            int a2 = a(iArr, iArr2[i4], 179);
            if (a2 < i2) {
                i3 = i4;
            } else {
                a2 = i2;
            }
            i4++;
            i2 = a2;
        }
        if (i3 >= 0) {
            return i3;
        }
        throw NotFoundException.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.a, int, int, int[]]
     candidates:
      com.google.zxing.b.n.a(com.google.zxing.common.a, int[], int, int[][]):int
      com.google.zxing.b.n.a(int, com.google.zxing.common.a, int[], java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[] */
    static int[] a(a aVar) {
        int i = 0;
        int[] iArr = null;
        boolean z = false;
        while (!z) {
            iArr = a(aVar, i, false, b);
            int i2 = iArr[0];
            i = iArr[1];
            int i3 = i2 - (i - i2);
            if (i3 >= 0) {
                z = aVar.a(i3, i2, false);
            }
        }
        return iArr;
    }

    static int[] a(a aVar, int i, boolean z, int[] iArr) {
        int length = iArr.length;
        int[] iArr2 = new int[length];
        int a2 = aVar.a();
        boolean z2 = false;
        int i2 = i;
        while (i2 < a2) {
            z2 = !aVar.a(i2);
            if (z == z2) {
                break;
            }
            i2++;
        }
        int i3 = i2;
        int i4 = 0;
        boolean z3 = z2;
        int i5 = i3;
        for (int i6 = i2; i6 < a2; i6++) {
            if (aVar.a(i6) ^ z3) {
                iArr2[i4] = iArr2[i4] + 1;
            } else {
                if (i4 != length - 1) {
                    i4++;
                } else if (a(iArr2, iArr, 179) < 107) {
                    return new int[]{i5, i6};
                } else {
                    i5 += iArr2[0] + iArr2[1];
                    for (int i7 = 2; i7 < length; i7++) {
                        iArr2[i7 - 2] = iArr2[i7];
                    }
                    iArr2[length - 2] = 0;
                    iArr2[length - 1] = 0;
                    i4--;
                }
                iArr2[i4] = 1;
                z3 = !z3;
            }
        }
        throw NotFoundException.a();
    }

    private static boolean b(String str) {
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        for (int i2 = length - 2; i2 >= 0; i2 -= 2) {
            int charAt = str.charAt(i2) - '0';
            if (charAt < 0 || charAt > 9) {
                throw FormatException.a();
            }
            i += charAt;
        }
        int i3 = i * 3;
        for (int i4 = length - 1; i4 >= 0; i4 -= 2) {
            int charAt2 = str.charAt(i4) - '0';
            if (charAt2 < 0 || charAt2 > 9) {
                throw FormatException.a();
            }
            i3 += charAt2;
        }
        return i3 % 10 == 0;
    }

    /* access modifiers changed from: protected */
    public abstract int a(a aVar, int[] iArr, StringBuffer stringBuffer);

    public h a(int i, a aVar, Hashtable hashtable) {
        return a(i, aVar, a(aVar), hashtable);
    }

    public h a(int i, a aVar, int[] iArr, Hashtable hashtable) {
        String a2;
        k kVar = hashtable == null ? null : (k) hashtable.get(d.h);
        if (kVar != null) {
            kVar.a(new j(((float) (iArr[0] + iArr[1])) / 2.0f, (float) i));
        }
        StringBuffer stringBuffer = this.f149a;
        stringBuffer.setLength(0);
        int a3 = a(aVar, iArr, stringBuffer);
        if (kVar != null) {
            kVar.a(new j((float) a3, (float) i));
        }
        int[] a4 = a(aVar, a3);
        if (kVar != null) {
            kVar.a(new j(((float) (a4[0] + a4[1])) / 2.0f, (float) i));
        }
        int i2 = a4[1];
        int i3 = (i2 - a4[0]) + i2;
        if (i3 >= aVar.a() || !aVar.a(i2, i3, false)) {
            throw NotFoundException.a();
        }
        String stringBuffer2 = stringBuffer.toString();
        if (!a(stringBuffer2)) {
            throw ChecksumException.a();
        }
        com.google.zxing.a b2 = b();
        h hVar = new h(stringBuffer2, null, new j[]{new j(((float) (iArr[1] + iArr[0])) / 2.0f, (float) i), new j(((float) (a4[1] + a4[0])) / 2.0f, (float) i)}, b2);
        try {
            h a5 = this.f.a(i, aVar, a4[1]);
            hVar.a(a5.d());
            hVar.a(a5.b());
        } catch (ReaderException e2) {
        }
        if ((com.google.zxing.a.f.equals(b2) || com.google.zxing.a.d.equals(b2)) && (a2 = this.g.a(stringBuffer2)) != null) {
            hVar.a(i.g, a2);
        }
        return hVar;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return b(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.a, int, int, int[]]
     candidates:
      com.google.zxing.b.n.a(com.google.zxing.common.a, int[], int, int[][]):int
      com.google.zxing.b.n.a(int, com.google.zxing.common.a, int[], java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[] */
    /* access modifiers changed from: package-private */
    public int[] a(a aVar, int i) {
        return a(aVar, i, false, b);
    }

    /* access modifiers changed from: package-private */
    public abstract com.google.zxing.a b();
}
