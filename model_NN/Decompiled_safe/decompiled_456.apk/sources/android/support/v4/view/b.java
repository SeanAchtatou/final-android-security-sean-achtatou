package android.support.v4.view;

import android.support.v4.view.AccessibilityDelegateCompatIcs;
import android.support.v4.view.a.a;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
class b implements AccessibilityDelegateCompatIcs.AccessibilityDelegateBridge {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AccessibilityDelegateCompat f85a;
    final /* synthetic */ a b;

    b(a aVar, AccessibilityDelegateCompat accessibilityDelegateCompat) {
        this.b = aVar;
        this.f85a = accessibilityDelegateCompat;
    }

    public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        return this.f85a.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        this.f85a.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(View view, Object obj) {
        this.f85a.onInitializeAccessibilityNodeInfo(view, new a(obj));
    }

    public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        this.f85a.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.f85a.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    public void sendAccessibilityEvent(View view, int i) {
        this.f85a.sendAccessibilityEvent(view, i);
    }

    public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
        this.f85a.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }
}
