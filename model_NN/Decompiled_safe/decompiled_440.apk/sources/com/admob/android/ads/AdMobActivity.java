package com.admob.android.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.j;
import com.admob.android.ads.q;
import com.admob.android.ads.view.AdMobWebView;
import com.wallpaperpuzzle.spiderman.R;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONObject;

public class AdMobActivity extends Activity {
    private r a;
    private Vector<ae> b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        View view;
        super.onCreate(bundle);
        this.b = new Vector<>();
        Bundle bundleExtra = getIntent().getBundleExtra("o");
        r rVar = new r();
        if (rVar.a(bundleExtra)) {
            this.a = rVar;
        } else {
            this.a = null;
        }
        if (this.a != null) {
            q.a(this.a.c, (JSONObject) null, AdManager.getUserId(this));
            j.a aVar = this.a.a;
            WeakReference weakReference = new WeakReference(this);
            switch (AnonymousClass1.a[aVar.ordinal()]) {
                case R.styleable.com_admob_android_ads_AdView_primaryTextColor /*1*/:
                    setTheme(16973831);
                    view = AdMobWebView.a(getApplicationContext(), this.a.d, false, this.a.f, this.a.g, k.a(this), weakReference);
                    break;
                case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                    r rVar2 = this.a;
                    ac acVar = new ac(getApplicationContext(), weakReference);
                    acVar.a(rVar2);
                    this.b.add(acVar);
                    view = acVar;
                    break;
                default:
                    view = null;
                    break;
            }
            if (view != null) {
                switch (AnonymousClass1.b[this.a.e.ordinal()]) {
                    case R.styleable.com_admob_android_ads_AdView_primaryTextColor /*1*/:
                        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                            Log.v(AdManager.LOG, "Setting target orientation to landscape");
                        }
                        setRequestedOrientation(0);
                        break;
                    case R.styleable.com_admob_android_ads_AdView_secondaryTextColor /*2*/:
                        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                            Log.v(AdManager.LOG, "Setting target orientation to portrait");
                        }
                        setRequestedOrientation(1);
                        break;
                    default:
                        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                            Log.v(AdManager.LOG, "Setting target orientation to sensor");
                        }
                        setRequestedOrientation(4);
                        break;
                }
                setContentView(view);
                return;
            }
            finish();
        } else if (InterstitialAd.c.a(AdManager.LOG, 6)) {
            Log.e(AdManager.LOG, "Unable to get openerInfo from intent");
        }
    }

    /* renamed from: com.admob.android.ads.AdMobActivity$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[j.a.values().length];
        static final /* synthetic */ int[] b = new int[q.a.values().length];

        static {
            try {
                b[q.a.LANDSCAPE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[q.a.PORTRAIT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[q.a.ANY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[j.a.CLICK_TO_FULLSCREEN_BROWSER.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[j.a.CLICK_TO_INTERACTIVE_VIDEO.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void finish() {
        if (this.a != null && this.a.l) {
            Intent intent = new Intent();
            intent.putExtra(InterstitialAd.ADMOB_INTENT_BOOLEAN, true);
            setResult(-1, intent);
        }
        super.finish();
    }

    public void onConfigurationChanged(Configuration configuration) {
        Iterator<ae> it = this.b.iterator();
        while (it.hasNext()) {
            it.next().a(configuration);
        }
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.clear();
        super.onDestroy();
    }
}
