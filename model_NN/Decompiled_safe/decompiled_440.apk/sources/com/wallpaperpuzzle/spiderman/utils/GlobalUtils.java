package com.wallpaperpuzzle.spiderman.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.Display;

public class GlobalUtils {
    private static Resources mResources;

    public static void init(Activity pContext) {
        mResources = pContext.getResources();
    }

    public static int convertDip(int pix) {
        return (int) TypedValue.applyDimension(1, (float) pix, mResources.getDisplayMetrics());
    }

    public static int getOrientation(Activity action) {
        Display getOrient = action.getWindowManager().getDefaultDisplay();
        if (getOrient.getWidth() < getOrient.getHeight()) {
            return 1;
        }
        return 2;
    }

    public static void startActivity(Activity activity, Class className) {
        startActivity(activity, className, new Intent());
    }

    public static void startActivity(Activity activity, Class className, Intent intent) {
        intent.setFlags(67108864);
        intent.setClass(activity, className);
        activity.startActivity(intent);
    }
}
