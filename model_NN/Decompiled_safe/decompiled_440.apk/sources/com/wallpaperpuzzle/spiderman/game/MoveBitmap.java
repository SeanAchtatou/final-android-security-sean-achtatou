package com.wallpaperpuzzle.spiderman.game;

import android.graphics.Bitmap;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class MoveBitmap implements Serializable {
    private static String TAG = "MoveBitmap";
    private List<Bitmap> mBitmapList = new ArrayList();
    private Map<Integer, Bitmap> mBitmapListView = new HashMap();
    private Parameters mParameters;
    private List<Integer> mShuffleBitmap = new Vector();
    private boolean mShuffled = false;

    public MoveBitmap(List<Bitmap> bitmapList) {
        setBitmapList(bitmapList);
    }

    public MoveBitmap(List<Bitmap> bitmapList, Parameters parameters) {
        setBitmapList(bitmapList);
        setParameters(parameters);
    }

    public void shuffle() {
        if (this.mBitmapList != null && this.mBitmapList.size() != 0) {
            this.mShuffleBitmap.clear();
            for (int i = 0; i < this.mBitmapList.size(); i++) {
                this.mShuffleBitmap.add(Integer.valueOf(i));
            }
            Collections.shuffle(this.mShuffleBitmap);
            this.mShuffled = true;
        }
    }

    public void prepareViewBitmap(int orientation) {
        if (!isShuffled()) {
            return;
        }
        if (ResourceLauncher.getBitmapsShufflePortrait() == null || ResourceLauncher.getBitmapsShuffleLandscape() == null) {
            this.mBitmapListView.clear();
            for (Integer shuffleId : this.mShuffleBitmap) {
                this.mBitmapListView.put(shuffleId, Bitmap.createScaledBitmap(this.mBitmapList.get(shuffleId.intValue()), getParameters().getBitmapWidth(), getParameters().getBitmapHeight(), false));
            }
            if (1 == orientation) {
                if (ResourceLauncher.getBitmapsShufflePortrait() == null) {
                    ResourceLauncher.setBitmapsShufflePortrait(this.mBitmapListView);
                }
            } else if (ResourceLauncher.getBitmapsShuffleLandscape() == null) {
                ResourceLauncher.setBitmapsShuffleLandscape(this.mBitmapListView);
            }
        } else if (1 == orientation) {
            this.mBitmapListView = ResourceLauncher.getBitmapsShufflePortrait();
        } else {
            this.mBitmapListView = ResourceLauncher.getBitmapsShuffleLandscape();
        }
    }

    public void recalculateShuffle(int bitmapIdDown, int bitmapIdUp) {
        if (bitmapIdDown != bitmapIdUp) {
            for (int i = 0; i < this.mShuffleBitmap.size(); i++) {
                Integer shuffleId = this.mShuffleBitmap.get(i);
                if (bitmapIdDown == shuffleId.intValue()) {
                    this.mShuffleBitmap.set(i, Integer.valueOf(bitmapIdUp));
                }
                if (bitmapIdUp == shuffleId.intValue()) {
                    this.mShuffleBitmap.set(i, Integer.valueOf(bitmapIdDown));
                }
            }
        }
    }

    public List<Bitmap> getBitmapList() {
        return this.mBitmapList;
    }

    public void setBitmapList(List<Bitmap> mBitmapList2) {
        this.mBitmapList = mBitmapList2;
    }

    public List<Integer> getShuffleBitmap() {
        return this.mShuffleBitmap;
    }

    public void setShuffleBitmap(List<Integer> mShuffleBitmap2) {
        this.mShuffleBitmap = mShuffleBitmap2;
    }

    public Map<Integer, Bitmap> getBitmapView() {
        return this.mBitmapListView;
    }

    public boolean isShuffled() {
        return this.mShuffled;
    }

    public void setShuffled(boolean mShuffled2) {
        this.mShuffled = mShuffled2;
    }

    public Parameters getParameters() {
        return this.mParameters;
    }

    public void setParameters(Parameters parameters) {
        this.mParameters = parameters;
    }

    public static class Parameters {
        private int mBitmapHeight;
        private int mBitmapWidth;
        private int mMoveHeight;
        private int mMoveWidth;
        private float mXScale;
        private float mYScale;

        public Parameters() {
        }

        public Parameters(int moveWidth, int moveHeight, float xScale, float yScale, int bitmapWidth, int bitmapHeight) {
            this.mMoveWidth = moveWidth;
            this.mMoveHeight = moveHeight;
            this.mXScale = xScale;
            this.mYScale = yScale;
            this.mBitmapWidth = bitmapWidth;
            this.mBitmapHeight = bitmapHeight;
        }

        public int getMoveHeight() {
            return this.mMoveHeight;
        }

        public void setMoveHeight(int moveHeight) {
            this.mMoveHeight = moveHeight;
        }

        public int getMoveWidth() {
            return this.mMoveWidth;
        }

        public void setMoveWidth(int moveWidth) {
            this.mMoveWidth = moveWidth;
        }

        public float getXScale() {
            return this.mXScale;
        }

        public void setXScale(float xScale) {
            this.mXScale = xScale;
        }

        public float getYScale() {
            return this.mYScale;
        }

        public void setYScale(float yScale) {
            this.mYScale = yScale;
        }

        public int getBitmapWidth() {
            return this.mBitmapWidth;
        }

        public void setBitmapWidth(int mBitmapWidth2) {
            this.mBitmapWidth = mBitmapWidth2;
        }

        public int getBitmapHeight() {
            return this.mBitmapHeight;
        }

        public void setBitmapHeight(int mBitmapHeight2) {
            this.mBitmapHeight = mBitmapHeight2;
        }
    }
}
