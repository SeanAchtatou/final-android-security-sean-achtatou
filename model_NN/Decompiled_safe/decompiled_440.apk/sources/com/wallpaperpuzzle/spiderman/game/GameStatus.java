package com.wallpaperpuzzle.spiderman.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
