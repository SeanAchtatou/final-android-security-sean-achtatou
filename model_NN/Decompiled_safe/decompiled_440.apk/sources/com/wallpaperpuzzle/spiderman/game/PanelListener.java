package com.wallpaperpuzzle.spiderman.game;

public interface PanelListener {
    void onComplete();

    void onNext();
}
