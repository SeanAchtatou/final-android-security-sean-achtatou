package com.wallpaperpuzzle.spiderman.game;

import android.graphics.Bitmap;

public class Graphic {
    private static String TAG = "Graphic";
    private Bitmap mBitmap;
    private int mBitmapId;
    private int mX = 0;
    private int mY = 0;

    public Graphic(int x, int y) {
        setX(x);
        setY(y);
    }

    public void setBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return this.mBitmap;
    }

    public int getBitmapId() {
        return this.mBitmapId;
    }

    public void setBitmapId(int mBitmapId2) {
        this.mBitmapId = mBitmapId2;
    }

    public int getX() {
        return this.mX;
    }

    public void setX(int value) {
        this.mX = value;
    }

    public int getTouchedX() {
        return this.mX - (this.mBitmap.getWidth() / 2);
    }

    public int getTouchedY() {
        return this.mY - (this.mBitmap.getHeight() / 2);
    }

    public int getY() {
        return this.mY;
    }

    public void setY(int value) {
        this.mY = value;
    }
}
