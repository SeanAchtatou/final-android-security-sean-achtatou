package com.wallpaperpuzzle.spiderman;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.wallpaperpuzzle.spiderman.game.GameType;
import com.wallpaperpuzzle.spiderman.utils.GlobalUtils;
import java.lang.ref.SoftReference;
import java.util.HashMap;

public class ImageLevelAdapter extends BaseAdapter {
    private static final int GALLERY_HEIGHT = 90;
    private static final int GALLERY_WIDTH = 120;
    public static final String IMAGE = "image_";
    private static final String RES_TYPE = "drawable";
    private static String TAG = "ImageLevelAdapter";
    private static ImageLevelAdapter imageLevelAdapter;
    private static Context mContext;
    private static Integer[] mGameTypes = {Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x3.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x4.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x5.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal()), Integer.valueOf(GameType.x6.getOrdinal())};
    /* access modifiers changed from: private */
    public static Integer[] mImageIds = new Integer[mGameTypes.length];
    private int mCount;
    private int mGalleryItemBackground;
    private int mHeight;
    private ImageCache mImageCache = new ImageCache();
    private int mWidth;

    private ImageLevelAdapter(Context pContext, TypedArray pA, int pCount) {
        mContext = pContext;
        this.mCount = pCount;
        this.mGalleryItemBackground = pA.getResourceId(0, 0);
        pA.recycle();
        initResources();
        if (this.mCount < mImageIds.length) {
            this.mCount++;
        }
        this.mWidth = GlobalUtils.convertDip(GALLERY_WIDTH);
        this.mHeight = GlobalUtils.convertDip(GALLERY_HEIGHT);
    }

    public static void update(int position) {
        if (position == getImageLevelAdapter().getCount() - 1 && position < mGameTypes.length - 1) {
            getImageLevelAdapter().setCount(getImageLevelAdapter().getCount() + 1);
            getImageLevelAdapter().notifyParent();
        }
    }

    private void notifyParent() {
        super.notifyDataSetChanged();
    }

    private void initResources() {
        Resources resources = mContext.getResources();
        String packageName = mContext.getPackageName();
        for (int i = 0; i < mGameTypes.length; i++) {
            mImageIds[i] = Integer.valueOf(resources.getIdentifier(IMAGE + (i + 1), RES_TYPE, packageName));
        }
    }

    public static Bitmap decodeResource(int resource) {
        BitmapFactory.Options ops = new BitmapFactory.Options();
        ops.inPurgeable = true;
        return BitmapFactory.decodeResource(mContext.getResources(), resource, ops);
    }

    public static ImageLevelAdapter initImageLevelAdapter(Context pContext, TypedArray pA, int count) {
        if (imageLevelAdapter == null) {
            imageLevelAdapter = new ImageLevelAdapter(pContext, pA, count);
        }
        return imageLevelAdapter;
    }

    public static ImageLevelAdapter getImageLevelAdapter() {
        return imageLevelAdapter;
    }

    public static GameType getGameTypeByPosition(int position) {
        return GameType.values()[mGameTypes[position].intValue()];
    }

    public static Bitmap getBitmapByPosition(int position) {
        return getImageLevelAdapter().getImageCache().getImage(Integer.valueOf(position));
    }

    public static int getMaxItem() {
        return mImageIds.length - 1;
    }

    public int getDefaultPosition() {
        return getCount() - 1;
    }

    public int getCount() {
        return this.mCount;
    }

    public void setCount(int pCount) {
        this.mCount = pCount;
    }

    public ImageCache getImageCache() {
        return this.mImageCache;
    }

    public Object getItem(int position) {
        return mImageIds[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Bitmap bitmap = this.mImageCache.getImage(Integer.valueOf(position));
        ImageView imageView = new ImageView(mContext);
        imageView.setImageBitmap(bitmap);
        imageView.setLayoutParams(new Gallery.LayoutParams(this.mWidth, this.mHeight));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setBackgroundResource(this.mGalleryItemBackground);
        return imageView;
    }

    public static class ImageCache {
        HashMap<Integer, SoftReference<Bitmap>> myCache = new HashMap<>();

        public Bitmap getImage(Integer key) {
            Integer id = ImageLevelAdapter.mImageIds[key.intValue()];
            SoftReference<Bitmap> ref = this.myCache.get(id);
            if ((ref != null ? (Bitmap) ref.get() : null) == null) {
                ref = new SoftReference<>(ImageLevelAdapter.decodeResource(id.intValue()));
                this.myCache.put(id, ref);
            }
            return ref.get();
        }
    }
}
