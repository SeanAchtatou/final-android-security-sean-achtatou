package com.wallpaperpuzzle.spiderman.audio.sound;

import com.wallpaperpuzzle.spiderman.R;

public enum Sound {
    pressbutton(R.raw.pressbutton),
    startpuzzle(R.raw.startpuzzle),
    switchpuzzle(R.raw.switchpuzzle),
    levelcompleted(R.raw.levelcompleted);
    
    private int sound;

    private Sound(int sound2) {
        this.sound = sound2;
    }

    public int getSound() {
        return this.sound;
    }

    public void setSound(int sound2) {
        this.sound = sound2;
    }
}
