package random.display.puzzle;

import android.content.Context;
import android.graphics.Rect;

public class Puzzle3x3 extends AbstractPuzzleView {
    private static final int m = 3;
    private static final int n = 3;
    private float hExponent;
    private float wExponent;

    public Puzzle3x3(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.wExponent = ((float) w) / 3.0f;
        this.hExponent = ((float) h) / 3.0f;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* access modifiers changed from: protected */
    public Rect[] getArraySrc() {
        Rect[] rv = new Rect[9];
        int count = 0;
        for (int hi = 0; hi < 3; hi++) {
            for (int wi = 0; wi < 3; wi++) {
                rv[count] = new Rect(Math.round(this.wExponent * ((float) wi)), Math.round(this.hExponent * ((float) hi)), Math.round(this.wExponent * ((float) (wi + 1))), Math.round(this.hExponent * ((float) (hi + 1))));
                count++;
            }
        }
        return rv;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: int[]} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int[] getArrayDst() {
        /*
            r7 = this;
            r6 = 9
            int[] r1 = new int[r6]
            short[] r3 = new short[r6]
            r0 = 0
        L_0x0007:
            if (r0 < r6) goto L_0x0012
            java.util.Random r2 = new java.util.Random
            r2.<init>()
            r0 = 0
        L_0x000f:
            if (r0 < r6) goto L_0x0018
            return r1
        L_0x0012:
            r3[r0] = r0
            int r5 = r0 + 1
            short r0 = (short) r5
            goto L_0x0007
        L_0x0018:
            int r5 = r6 - r0
            int r5 = r2.nextInt(r5)
            int r5 = r5 + r0
            short r4 = (short) r5
            short r5 = r3[r4]
            r1[r0] = r5
            short r5 = r3[r0]
            r3[r4] = r5
            int r5 = r0 + 1
            short r0 = (short) r5
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: random.display.puzzle.Puzzle3x3.getArrayDst():int[]");
    }
}
