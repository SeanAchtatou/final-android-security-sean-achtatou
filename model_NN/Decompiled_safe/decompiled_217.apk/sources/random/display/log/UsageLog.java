package random.display.log;

import org.json.JSONException;
import org.json.JSONObject;

public class UsageLog {
    private String appName;
    private String keyword;
    private String source;
    private String url;
    private UsageType usage;

    public UsageLog() {
    }

    public UsageLog(String appName2, String url2, String source2, String keyword2, UsageType usage2) {
        setUrl(url2);
        setSource(source2);
        setKeyword(keyword2);
        setUsage(usage2);
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName2) {
        this.appName = appName2;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source2) {
        this.source = source2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword2) {
        this.keyword = keyword2;
    }

    public UsageType getUsage() {
        return this.usage;
    }

    public void setUsage(UsageType usage2) {
        this.usage = usage2;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsObj = new JSONObject();
        jsObj.put("appName", this.appName);
        jsObj.put("url", this.url);
        jsObj.put("source", this.source);
        jsObj.put("keyword", this.keyword);
        jsObj.put("funcName", this.usage.name());
        return jsObj;
    }
}
