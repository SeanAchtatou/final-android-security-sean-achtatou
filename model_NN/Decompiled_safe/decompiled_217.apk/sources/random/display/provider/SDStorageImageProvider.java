package random.display.provider;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.File;
import java.io.FileFilter;

public class SDStorageImageProvider extends AbstractImageProvider {
    private String currentFileName;
    private int m_Index = 0;
    private String storagePath;

    public SDStorageImageProvider() {
    }

    public SDStorageImageProvider(String path) {
        setStoragePath(path);
    }

    public String getStoragePath() {
        return this.storagePath;
    }

    public void setStoragePath(String storagePath2) {
        this.storagePath = storagePath2;
    }

    public void reset() {
        this.m_Index = 0;
    }

    public String getCurrentImageId() {
        return this.currentFileName;
    }

    public String getCurrentSource() {
        return getStoragePath();
    }

    public String getCurrentUrl() {
        return String.valueOf(getStoragePath()) + this.currentFileName;
    }

    public void initialize() {
    }

    public Bitmap downloadImage(String[] keywords) {
        return selectImage();
    }

    private File getDirectory() {
        String strExternalState = Environment.getExternalStorageState();
        if ("mounted".equals(strExternalState) || "mounted_ro".equals(strExternalState)) {
            return Environment.getExternalStorageDirectory();
        }
        return null;
    }

    private Bitmap selectImage() {
        File fiSDCard = getDirectory();
        if (fiSDCard == null) {
            return null;
        }
        File fiFolder = new File(String.valueOf(fiSDCard.getParent()) + "/" + fiSDCard.getName() + getStoragePath());
        if (!fiFolder.exists() || !fiFolder.isDirectory()) {
            return null;
        }
        File[] aList = fiFolder.listFiles(new FileFilter() {
            public boolean accept(File arg0) {
                return arg0.isFile() && arg0.getName().toLowerCase().endsWith(".png");
            }
        });
        if (this.m_Index < aList.length) {
            this.currentFileName = aList[this.m_Index].getName();
            int i = this.m_Index;
            this.m_Index = i + 1;
            return BitmapFactory.decodeFile(aList[i].getAbsolutePath());
        } else if (aList.length <= 0) {
            return null;
        } else {
            this.m_Index = 0;
            this.currentFileName = aList[this.m_Index].getName();
            return BitmapFactory.decodeFile(aList[this.m_Index].getAbsolutePath());
        }
    }
}
