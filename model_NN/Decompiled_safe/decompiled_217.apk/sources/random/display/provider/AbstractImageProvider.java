package random.display.provider;

import android.graphics.Bitmap;

public abstract class AbstractImageProvider implements ImageProvider {
    private String[] keywords = null;
    private ImageProviderListener listener;

    public String[] getKeywords() {
        return this.keywords;
    }

    public void setKeywords(String[] keywords2) {
        this.keywords = keywords2;
    }

    public void setImageProviderListener(ImageProviderListener l) {
        this.listener = l;
    }

    public ImageProviderListener getImageProviderListener() {
        return this.listener;
    }

    /* access modifiers changed from: protected */
    public void dispatchDownloadFailed() {
        if (getImageProviderListener() != null) {
            getImageProviderListener().onDownloadFailed();
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchNetworkError() {
        if (getImageProviderListener() != null) {
            getImageProviderListener().onNetworkError();
        }
    }

    public Bitmap downloadImage() {
        return downloadImage(getKeywords());
    }

    public boolean hasKeywords() {
        return getKeywords() != null;
    }

    public String getCurrentKeyword() {
        return "";
    }

    public String getCurrentSource() {
        return "";
    }

    public String getCurrentUrl() {
        return "";
    }

    public String getCurrentImageId() {
        return "";
    }
}
