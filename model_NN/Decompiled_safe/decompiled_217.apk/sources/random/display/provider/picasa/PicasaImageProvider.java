package random.display.provider.picasa;

import android.graphics.Bitmap;
import android.util.Log;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import random.display.provider.AbstractImageProvider;
import random.display.utils.HttpClientUtils;

public class PicasaImageProvider extends AbstractImageProvider {
    private JSONArray cache = null;
    private String choosedKeyword = null;
    private String currentImageId;
    private int fetchCount = 0;
    private String lastUrl;

    /* renamed from: random  reason: collision with root package name */
    private Random f1random = new Random(System.currentTimeMillis());

    public void initialize() {
    }

    private JSONObject doQuery(String keyword) throws IOException, JSONException {
        String strRet = HttpClientUtils.doGet("https://picasaweb.google.com/data/feed/api/all?max-results=100&kind=photo&imgmax=640&fields=entry(content)&access=public&alt=json&q=" + URLEncoder.encode(keyword));
        if (strRet == null) {
            return null;
        }
        return new JSONObject(strRet);
    }

    public Bitmap downloadImage(String[] keywords) {
        if (keywords == null || keywords.length == 0) {
            return null;
        }
        try {
            if (this.cache == null || this.fetchCount >= 5) {
                this.choosedKeyword = keywords[this.f1random.nextInt(keywords.length)];
                Log.d("PicasaImageProvider", "choosed keyword: " + this.choosedKeyword);
                JSONObject jsRoot = doQuery(this.choosedKeyword);
                if (jsRoot.getJSONObject("feed") == null || jsRoot.getJSONObject("feed").getJSONArray("entry") == null || jsRoot.getJSONObject("feed").getJSONArray("entry").length() == 0) {
                    Log.e("PicasaImageProvider", "the keyword " + this.choosedKeyword + " doesn't have any feedback while querying.");
                    return null;
                }
                this.cache = jsRoot.getJSONObject("feed").getJSONArray("entry");
                this.fetchCount = 0;
            } else {
                this.fetchCount++;
            }
            String strImageUrl = this.cache.getJSONObject(this.f1random.nextInt(this.cache.length())).getJSONObject("content").getString("src");
            this.lastUrl = strImageUrl;
            Log.d("PicasaImageProvider", "try to download image from " + strImageUrl);
            return HttpClientUtils.downloadImage("GET", strImageUrl);
        } catch (IOException e) {
            Log.e("PicasaImageProvider", "the keyword " + this.choosedKeyword + " IOException.", e);
            return null;
        } catch (JSONException e2) {
            Log.e("PicasaImageProvider", "the keyword " + this.choosedKeyword + " JSONException.", e2);
            return null;
        }
    }

    public String getCurrentKeyword() {
        return this.choosedKeyword;
    }

    public String getCurrentSource() {
        return "picasa";
    }

    public String getCurrentUrl() {
        return this.lastUrl;
    }

    public String getCurrentImageId() {
        return super.getCurrentImageId();
    }
}
