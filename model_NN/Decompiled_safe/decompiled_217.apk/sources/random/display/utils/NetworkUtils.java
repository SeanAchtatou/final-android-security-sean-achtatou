package random.display.utils;

import android.app.Activity;
import android.net.ConnectivityManager;

public class NetworkUtils {
    public static boolean isOnline(Activity act) {
        ConnectivityManager cm = (ConnectivityManager) act.getSystemService("connectivity");
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        }
        return cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
