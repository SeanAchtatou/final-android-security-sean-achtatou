package random.display.utils;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

public class ToastUtils {
    public static void showToast(final String txt, View view, final Context ctx) {
        view.post(new Runnable() {
            public void run() {
                Toast.makeText(ctx, txt, 1).show();
            }
        });
    }
}
