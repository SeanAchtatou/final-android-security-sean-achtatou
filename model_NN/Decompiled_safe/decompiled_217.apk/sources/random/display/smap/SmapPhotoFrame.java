package random.display.smap;

import random.display.provider.ImageProvider;
import random.display.provider.SDStorageImageProvider;
import random.display.widget.AbstractPhotoFrame;

public class SmapPhotoFrame extends AbstractPhotoFrame {
    private static SDStorageImageProvider m_ImageProvider = new SDStorageImageProvider("/smap/");

    /* access modifiers changed from: protected */
    public String getActivityName() {
        return "random.display.smap/random.display.smap.Smap";
    }

    /* access modifiers changed from: protected */
    public int getWaitTimeout() {
        return 20000;
    }

    /* access modifiers changed from: protected */
    public ImageProvider getImageProvider() {
        return m_ImageProvider;
    }
}
