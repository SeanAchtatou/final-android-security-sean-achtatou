package random.display.smap;

import android.util.Log;
import java.util.Random;
import random.display.base.AbstractRandomDisplayActivity;
import random.display.provider.ImageProvider;
import random.display.provider.flickr.FlickrImageProvider;
import random.display.provider.picasa.PicasaImageProvider;

public class Smap extends AbstractRandomDisplayActivity {
    private FlickrImageProvider fip = new FlickrImageProvider();
    private PicasaImageProvider pip = new PicasaImageProvider();

    /* renamed from: random  reason: collision with root package name */
    private Random f2random = new Random(System.currentTimeMillis());

    public Smap() {
        this.fip.setKeywords(new String[]{"nakai masahiro", "kimura takuya -knife -100_3995 -Profile -nikon -ride -tokyo -casio -travel", "SMAP -bus -hiroshima -shop -store -machine -Tokyo -canon -drink -concert -コンサート -matsuri -fieldtrip -NYC -kitty -eye -sport -pumpkin -halloween -spy -hawaii -お台場 -Geun -Labu -gendesu -Shibuya -Serbia -jingle"});
        this.pip.setKeywords(new String[]{"\"kimura takuya\""});
    }

    /* access modifiers changed from: protected */
    public String getStorageFolderName() {
        return "/smap/";
    }

    /* access modifiers changed from: protected */
    public String[] getKeywords() {
        return null;
    }

    /* access modifiers changed from: protected */
    public ImageProvider getImageProvider() {
        if (this.f2random.nextBoolean()) {
            Log.i("Smap", "use flickr");
            return this.fip;
        }
        Log.i("Smap", "use picasa");
        return this.pip;
    }

    /* access modifiers changed from: protected */
    public String getAdmobId() {
        return "a14e487a1e6a709";
    }

    /* access modifiers changed from: protected */
    public int getAdmobFilter() {
        return 2;
    }
}
