package random.display.base;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

public interface RandomDisplayResourceProvider {
    ViewGroup getAdmobContainer();

    FrameLayout getFrameLayout();

    String getImageDownloadErrorText();

    ImageView getImageView();

    String getLoadingText();

    String getNetworkButtonLabel();

    String getNetworkErrorContent();

    String getNetworkErrorTitle();

    ImageButton getNextBtn();

    String getPlayPuzzleLabel();

    String getSavePuzzleSrcLabel();

    String getSavingMenuText();

    String getSetAsWallpaperMenuText();

    ImageButton getZoonInBtn();

    ImageButton getZoonOutBtn();
}
