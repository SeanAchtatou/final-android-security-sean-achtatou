package com.raidroid.rdnightclock;

public final class R {

    public static final class array {
        public static final int Preference_ColorThemes = 2131099648;
        public static final int Preference_ColorThemes_Values = 2131099649;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int aboutText = 2131230721;
        public static final int ampm = 2131230728;
        public static final int day = 2131230730;
        public static final int dayName = 2131230733;
        public static final int dvider = 2131230726;
        public static final int hours = 2131230725;
        public static final int imageView1 = 2131230722;
        public static final int item01 = 2131230734;
        public static final int item02 = 2131230735;
        public static final int item03 = 2131230736;
        public static final int minutes = 2131230727;
        public static final int month = 2131230731;
        public static final int relativeLayout1 = 2131230724;
        public static final int relativeLayout2 = 2131230729;
        public static final int textView1 = 2131230723;
        public static final int versionName = 2131230720;
        public static final int year = 2131230732;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class menu {
        public static final int options_menu = 2131165184;
    }

    public static final class string {
        public static final int About = 2131034117;
        public static final int Menu_Close = 2131034118;
        public static final int Menu_Settings = 2131034116;
        public static final int Pref_24HrsFormatOnOff = 2131034119;
        public static final int Pref_24HrsFormatOnOff_Desc = 2131034120;
        public static final int Pref_BlinkDisplay = 2131034123;
        public static final int Pref_BlinkDisplay_Desc = 2131034124;
        public static final int Pref_ColorThemes = 2131034121;
        public static final int Pref_ColorThemes_Desc = 2131034122;
        public static final int Pref_ScreenBrightness_Level = 2131034125;
        public static final int app_name = 2131034112;
        public static final int eula_accept = 2131034114;
        public static final int eula_refuse = 2131034115;
        public static final int eula_title = 2131034113;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
