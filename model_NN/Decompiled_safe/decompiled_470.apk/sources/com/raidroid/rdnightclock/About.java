package com.raidroid.rdnightclock;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;

public class About extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        DisplayRequest(GetAboutFromTtx());
    }

    private void DisplayRequest(String str) {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
        }
        ((TextView) findViewById(R.id.versionName)).setText("Version: " + info.versionName);
        ((TextView) findViewById(R.id.aboutText)).setText(str);
    }

    private String GetAboutFromTtx() {
        BufferedReader in = null;
        try {
            BufferedReader in2 = new BufferedReader(new InputStreamReader(getAssets().open("ABOUT"), "ISO-8859-9"));
            try {
                StringBuilder buffer = new StringBuilder();
                while (true) {
                    String line = in2.readLine();
                    if (line == null) {
                        String about = buffer.toString();
                        closeStream(in2);
                        return about;
                    }
                    buffer.append(line).append(10);
                }
            } catch (IOException e) {
                in = in2;
            } catch (Throwable th) {
                th = th;
                in = in2;
                closeStream(in);
                throw th;
            }
        } catch (IOException e2) {
            closeStream(in);
            return "";
        } catch (Throwable th2) {
            th = th2;
            closeStream(in);
            throw th;
        }
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }
}
