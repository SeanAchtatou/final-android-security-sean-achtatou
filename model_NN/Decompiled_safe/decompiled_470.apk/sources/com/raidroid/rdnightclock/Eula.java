package com.raidroid.rdnightclock;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;

class Eula {
    private static final String ASSET_EULA = "EULA";
    private static final String PREFERENCES_EULA = "com.raidroid.rdnightclock_preferences";
    private static final String PREFERENCE_EULA_ACCEPTED = "eula.accepted";

    interface OnEulaAgreedTo {
        void onEulaAgreedTo();
    }

    Eula() {
    }

    static boolean show(final Activity activity) {
        final SharedPreferences preferences = activity.getSharedPreferences(PREFERENCES_EULA, 0);
        if (preferences.getBoolean(PREFERENCE_EULA_ACCEPTED, false)) {
            return true;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle((int) R.string.eula_title);
        builder.setCancelable(true);
        builder.setPositiveButton((int) R.string.eula_accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Eula.accept(preferences);
                if (activity instanceof OnEulaAgreedTo) {
                    ((OnEulaAgreedTo) activity).onEulaAgreedTo();
                }
            }
        });
        builder.setNegativeButton((int) R.string.eula_refuse, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Eula.refuse(activity);
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                Eula.refuse(activity);
            }
        });
        builder.setMessage(readEula(activity));
        builder.create().show();
        return false;
    }

    /* access modifiers changed from: private */
    public static void accept(SharedPreferences preferences) {
        preferences.edit().putBoolean(PREFERENCE_EULA_ACCEPTED, true).commit();
    }

    /* access modifiers changed from: private */
    public static void refuse(Activity activity) {
        activity.finish();
    }

    private static CharSequence readEula(Activity activity) {
        BufferedReader in = null;
        try {
            BufferedReader in2 = new BufferedReader(new InputStreamReader(activity.getAssets().open(ASSET_EULA)));
            try {
                StringBuilder buffer = new StringBuilder();
                while (true) {
                    String line = in2.readLine();
                    if (line == null) {
                        closeStream(in2);
                        return buffer;
                    }
                    buffer.append(line).append(10);
                }
            } catch (IOException e) {
                in = in2;
            } catch (Throwable th) {
                th = th;
                in = in2;
                closeStream(in);
                throw th;
            }
        } catch (IOException e2) {
            closeStream(in);
            return "";
        } catch (Throwable th2) {
            th = th2;
            closeStream(in);
            throw th;
        }
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }
}
