package com.raidroid.rdnightclock;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RDNightClock extends Activity {
    private int brightnessLevel = 1;
    private boolean is24HrsFormat = false;
    /* access modifiers changed from: private */
    public boolean isBlinking = false;
    private boolean isScreenOn = false;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            RDNightClock.this.ScreenUpdate();
            if (!RDNightClock.this.isBlinking) {
                RDNightClock.this.mHandler.removeCallbacks(RDNightClock.this.mUpdateTimeTask);
                RDNightClock.this.mHandler.postDelayed(this, 60000);
                return;
            }
            RDNightClock.this.mHandler.removeCallbacks(RDNightClock.this.mUpdateTimeTask);
            RDNightClock.this.mHandler.postDelayed(this, 2000);
        }
    };

    /* access modifiers changed from: protected */
    public void onResume() {
        SharedPreferences sp = getSharedPreferences("com.raidroid.rdnightclock_preferences", 0);
        this.is24HrsFormat = sp.getBoolean("24hrsOnOff", false);
        this.isBlinking = sp.getBoolean("blinkDisplayOnOff", false);
        this.brightnessLevel = sp.getInt("ScreenBrightnessLevel", 1);
        if (this.brightnessLevel < 1) {
            this.brightnessLevel = 1;
        }
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = ((float) this.brightnessLevel) / 100.0f;
        getWindow().setAttributes(lp);
        if (!this.isBlinking) {
            resetUIVisibility();
        }
        Typeface tf1 = Typeface.createFromAsset(getAssets(), "fonts/text.ttf");
        TextView ampm = (TextView) findViewById(R.id.ampm);
        ampm.setTypeface(tf1);
        if (this.is24HrsFormat) {
            ampm.setVisibility(8);
        } else {
            ampm.setVisibility(0);
        }
        setColorTheme();
        this.mHandler.removeCallbacks(this.mUpdateTimeTask);
        this.mHandler.post(this.mUpdateTimeTask);
        super.onResume();
    }

    public void onCreate(Bundle savedInstanceState) {
        Eula.show(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        SharedPreferences sp = getSharedPreferences("com.raidroid.rdnightclock_preferences", 0);
        this.is24HrsFormat = sp.getBoolean("24hrsOnOff", false);
        this.isBlinking = sp.getBoolean("blinkDisplayOnOff", false);
        this.brightnessLevel = sp.getInt("ScreenBrightnessLevel", 1);
        if (this.brightnessLevel < 1) {
            this.brightnessLevel = 1;
        }
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = ((float) this.brightnessLevel) / 100.0f;
        getWindow().setAttributes(lp);
        Typeface tf1 = Typeface.createFromAsset(getAssets(), "fonts/text.ttf");
        Typeface tf2 = Typeface.createFromAsset(getAssets(), "fonts/number.ttf");
        ((TextView) findViewById(R.id.hours)).setTypeface(tf2);
        ((TextView) findViewById(R.id.dvider)).setTypeface(tf2);
        ((TextView) findViewById(R.id.minutes)).setTypeface(tf2);
        ((TextView) findViewById(R.id.day)).setTypeface(tf2);
        ((TextView) findViewById(R.id.month)).setTypeface(tf1);
        ((TextView) findViewById(R.id.year)).setTypeface(tf2);
        ((TextView) findViewById(R.id.dayName)).setTypeface(tf1);
        TextView ampm = (TextView) findViewById(R.id.ampm);
        ampm.setTypeface(tf1);
        if (this.is24HrsFormat) {
            ampm.setVisibility(8);
        } else {
            ampm.setVisibility(0);
        }
        setColorTheme();
        this.mHandler.removeCallbacks(this.mUpdateTimeTask);
        this.mHandler.post(this.mUpdateTimeTask);
    }

    public void setColorTheme() {
        String themeColor = getSharedPreferences("com.raidroid.rdnightclock_preferences", 0).getString("colorTheme", "#FFFFFF");
        boolean isCorrectColor = true;
        try {
            Color.parseColor(themeColor);
        } catch (Exception e) {
            isCorrectColor = false;
        }
        if (isCorrectColor) {
            ((TextView) findViewById(R.id.hours)).setTextColor(Color.parseColor(themeColor));
            ((TextView) findViewById(R.id.dvider)).setTextColor(Color.parseColor(themeColor));
            ((TextView) findViewById(R.id.minutes)).setTextColor(Color.parseColor(themeColor));
            ((TextView) findViewById(R.id.day)).setTextColor(Color.parseColor(themeColor));
            ((TextView) findViewById(R.id.month)).setTextColor(Color.parseColor(themeColor));
            ((TextView) findViewById(R.id.year)).setTextColor(Color.parseColor(themeColor));
            ((TextView) findViewById(R.id.dayName)).setTextColor(Color.parseColor(themeColor));
            ((TextView) findViewById(R.id.ampm)).setTextColor(Color.parseColor(themeColor));
        }
    }

    public void ScreenUpdate() {
        String strHour;
        if (this.isBlinking) {
            updateUIVisibility();
        }
        if (this.is24HrsFormat) {
            strHour = new SimpleDateFormat("HH").format(new Date());
        } else {
            strHour = new SimpleDateFormat("KK").format(new Date());
        }
        String strMinutes = new SimpleDateFormat("mm").format(new Date());
        String strDay = new SimpleDateFormat("dd").format(new Date());
        String strMonthName = new SimpleDateFormat("MMM").format(new Date());
        String strYear = new SimpleDateFormat("yyyy").format(new Date());
        String strDayName = new SimpleDateFormat("EE").format(new Date());
        String strAmPm = new SimpleDateFormat("a").format(new Date());
        ((TextView) findViewById(R.id.hours)).setText(strHour);
        ((TextView) findViewById(R.id.minutes)).setText(strMinutes);
        ((TextView) findViewById(R.id.day)).setText(strDay);
        ((TextView) findViewById(R.id.month)).setText(replaceTurkishChars(strMonthName.toUpperCase()));
        ((TextView) findViewById(R.id.year)).setText(strYear);
        ((TextView) findViewById(R.id.dayName)).setText(replaceTurkishChars(strDayName.toUpperCase()));
        if (!this.is24HrsFormat) {
            ((TextView) findViewById(R.id.ampm)).setText(strAmPm.toUpperCase());
        }
    }

    private String replaceTurkishChars(String str) {
        return str.replace("Ğ", "G").replace("İ", "I").replace("Ş", "S");
    }

    public void updateUIVisibility() {
        RelativeLayout rl1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        RelativeLayout rl2 = (RelativeLayout) findViewById(R.id.relativeLayout2);
        if (this.isScreenOn) {
            rl1.setVisibility(8);
            rl2.setVisibility(8);
            this.isScreenOn = false;
            return;
        }
        rl1.setVisibility(0);
        rl2.setVisibility(0);
        this.isScreenOn = true;
    }

    public void resetUIVisibility() {
        RelativeLayout rl1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        RelativeLayout rl2 = (RelativeLayout) findViewById(R.id.relativeLayout2);
        if (!this.isScreenOn) {
            rl1.setVisibility(0);
            rl2.setVisibility(0);
            this.isScreenOn = false;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.hasSubMenu() || item.getOrder() != 0) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.item01:
                startActivity(new Intent(this, About.class));
                return true;
            case R.id.item02:
                startActivity(new Intent(this, Preferences.class));
                return true;
            case R.id.item03:
                finish();
                return true;
            default:
                return true;
        }
    }
}
