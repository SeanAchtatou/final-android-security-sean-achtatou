package com.kidsfun.game.ocean;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.view.Display;
import android.view.WindowManager;
import java.util.Timer;
import java.util.TimerTask;

public class Splash extends Activity {
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            Splash.this.startActivity(new Intent(Splash.this, Applause.class));
            Splash.this.finish();
        }
    };
    private SharedPreferences mBaseSettings;
    private MediaPlayer mMusicPlayer;
    TimerTask mTask = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = 1;
            Splash.this.handler.sendMessage(message);
        }
    };
    Timer mTimer = new Timer();

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        setContentView((int) R.layout.splash);
        this.mMusicPlayer = MediaPlayer.create(getBaseContext(), (int) R.raw.splash);
        this.mMusicPlayer.start();
        this.mTimer.schedule(this.mTask, 2000, 2000);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void finish() {
        if (this.mMusicPlayer != null) {
            if (this.mMusicPlayer.isPlaying()) {
                this.mMusicPlayer.stop();
            }
            this.mMusicPlayer.release();
        }
        super.finish();
        Process.killProcess(Process.myPid());
    }
}
