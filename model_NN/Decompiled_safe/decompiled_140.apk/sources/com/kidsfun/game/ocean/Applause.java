package com.kidsfun.game.ocean;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.droidlib.net.GameAd;
import com.scoreloop.android.coreui.HighscoresActivity;
import com.scoreloop.android.coreui.ScoreloopManager;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Applause extends Activity {
    private final int MAX_GAME_TIME = 100;
    private final String PREFS_NAME = "com.feasy.app.memory.crazyfarm2";
    private final int REQUEST_CODE = 2;
    private final String SL_GAME_ID = "c1538351-0b1d-4706-9571-4f4784a8080f";
    private final String SL_GAME_SECRET = "Da7LGFF2FjuBy6Qw2501pie9V4BPb/xOgi7Y47hYcHAXxNMDYzVJQw==";
    private final int SOUND_EFFECT_CLICK = 4;
    private final int SOUND_EFFECT_MATCH = 1;
    private final int SOUND_EFFECT_WIN = 2;
    private final int SOUND_EFFECT_WRONG = 3;
    /* access modifiers changed from: private */
    public int[] checkedImageList = new int[20];
    /* access modifiers changed from: private */
    public int[] checkedTwoImg = new int[2];
    private int[][] columnImageIdList = {new int[]{R.id.img11, R.id.img12, R.id.img13, R.id.img14, R.id.img15}, new int[]{R.id.img21, R.id.img22, R.id.img23, R.id.img24, R.id.img25}, new int[]{R.id.img31, R.id.img32, R.id.img33, R.id.img34, R.id.img35}, new int[]{R.id.img41, R.id.img42, R.id.img43, R.id.img44, R.id.img45}, new int[]{R.id.img51, R.id.img52, R.id.img53, R.id.img54, R.id.img55}, new int[]{R.id.img61, R.id.img62, R.id.img63, R.id.img64, R.id.img65}};
    /* access modifiers changed from: private */
    public int finishednum = 0;
    private int[] firstColumnImg = {R.id.img11, R.id.img21, R.id.img31, R.id.img41, R.id.img51, R.id.img61};
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (Applause.this.mGameStatus == GAME_STATUS.GS_PLAYING) {
                switch (msg.what) {
                    case R.styleable.com_google_ads_AdView_primaryTextColor:
                        if (!Applause.this.updateProgressBar()) {
                            boolean unused = Applause.this.setGameOver();
                            break;
                        }
                        break;
                    case R.styleable.com_google_ads_AdView_secondaryTextColor:
                        Applause.this.doFinishLevel();
                        break;
                }
            }
            super.handleMessage(msg);
        }
    };
    private int heightnum = 3;
    private ImageView img_play;
    private ImageView img_ringtone;
    private boolean isShowAd;
    /* access modifiers changed from: private */
    public int lastcheckedImageId = -1;
    /* access modifiers changed from: private */
    public ImageView lastimg;
    private int level = 1;
    private ProgressBar mBar;
    private boolean mChangeTheme;
    private CheckBox mCheckBoxPause;
    private CheckBox mCheckBoxSound;
    AlertDialog mExitDlg;
    AlertDialog mGameOverDlg;
    GAME_STATUS mGameStatus;
    private int mGameTime;
    private ImageView mImgMain;
    /* access modifiers changed from: private */
    public boolean mIsMusic;
    private ListView mList;
    private boolean mMatchSound;
    private final int mMaxSkinCount = 3;
    private MediaPlayer mMusicPlayer;
    private int mPlayIndex;
    private int mPlayTime;
    private View mPlayView;
    private Random mRand = new Random();
    private int mSkinIndex;
    private HashMap<Integer, Integer> mSoundMap;
    private MediaPlayer mSoundPlayer;
    private SoundPool mSoundPool;
    /* access modifiers changed from: private */
    public int mTapCnt;
    TimerTask mTask = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = 1;
            Applause.this.handler.sendMessage(message);
        }
    };
    Timer mTimer = new Timer();
    /* access modifiers changed from: private */
    public TextView mTvTapCnt;
    private TextView mTvTime;
    private boolean mVibrate;
    private boolean m_IsSilent = false;
    private int m_ScreenHeight = 480;
    private int m_ScreenWidth = 320;
    /* access modifiers changed from: private */
    public int maxfinishednum = 36;
    public final int[] musicResId = {R.raw.bg_01, R.raw.bg_02, R.raw.bg_03, R.raw.bg_04};
    public int[] plants = {R.drawable.aa, R.drawable.bb, R.drawable.cc, R.drawable.dd, R.drawable.ee, R.drawable.ff, R.drawable.gg, R.drawable.hh, R.drawable.ii, R.drawable.jj, R.drawable.kk, R.drawable.ll, R.drawable.mm, R.drawable.nn, R.drawable.oo};
    /* access modifiers changed from: private */
    public int[] randImage = new int[20];
    private int score = 0;
    private int[] selectedImage = new int[20];
    private ProgressDialog waitDlg = null;
    private int widthnum = 3;

    private enum GAME_STATUS {
        GS_INIT,
        GS_PLAYING,
        GS_PAUSE,
        GS_LEVELUP,
        GS_GAMEOVER
    }

    public void PostMsg(int msgId) {
        Message message = new Message();
        message.what = msgId;
        this.handler.sendMessage(message);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.m_ScreenWidth = display.getWidth();
        this.m_ScreenHeight = display.getHeight();
        this.mGameTime = 100;
        this.mPlayIndex = -1;
        this.mPlayView = null;
        this.mSoundPlayer = null;
        this.m_IsSilent = false;
        loadGameParam();
        initSP();
        initScoreLoop();
        initGame();
        this.mTimer.schedule(this.mTask, 1000, 2000);
        GameAd.init(0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    public void initGame() {
        this.mGameStatus = GAME_STATUS.GS_INIT;
        stopSP(2);
        this.mPlayTime = 0;
        switch (((int) Math.floor((double) (this.level / 2))) + 1) {
            case 0:
                setContentView((int) R.layout.main2x3);
                this.widthnum = 2;
                this.heightnum = 3;
                break;
            case R.styleable.com_google_ads_AdView_primaryTextColor:
                setContentView((int) R.layout.main3x4);
                this.widthnum = 3;
                this.heightnum = 4;
                break;
            case R.styleable.com_google_ads_AdView_secondaryTextColor:
                setContentView((int) R.layout.main4x4);
                this.widthnum = 4;
                this.heightnum = 4;
                break;
            case R.styleable.com_google_ads_AdView_keywords:
                setContentView((int) R.layout.main4x5);
                this.widthnum = 4;
                this.heightnum = 5;
                break;
            case R.styleable.com_google_ads_AdView_refreshInterval:
                setContentView((int) R.layout.main4x6);
                this.widthnum = 4;
                this.heightnum = 6;
                break;
            case R.styleable.com_google_ads_AdView_testing:
                setContentView((int) R.layout.main5x6);
                this.widthnum = 5;
                this.heightnum = 6;
                break;
            default:
                setContentView((int) R.layout.main5x6);
                this.widthnum = 5;
                this.heightnum = 6;
                break;
        }
        this.selectedImage = new int[(this.widthnum * this.heightnum)];
        this.randImage = new int[(this.widthnum * this.heightnum)];
        this.checkedImageList = new int[(this.widthnum * this.heightnum)];
        this.checkedTwoImg = new int[2];
        this.checkedTwoImg[0] = -1;
        this.checkedTwoImg[1] = -1;
        this.maxfinishednum = this.widthnum * this.heightnum;
        this.mBar = (ProgressBar) findViewById(R.id.game_timebar);
        this.mTvTime = (TextView) findViewById(R.id.game_time);
        this.mTvTapCnt = (TextView) findViewById(R.id.tap_cnt);
        this.mTapCnt = 0;
        this.mImgMain = (ImageView) findViewById(R.id.game_icon);
        this.finishednum = 0;
        setRandomSkin();
        updateBkg();
        int[] newlist = new int[(this.plants.length * 2)];
        System.arraycopy(this.plants, 0, newlist, 0, this.plants.length);
        System.arraycopy(this.plants, 0, newlist, this.plants.length, this.plants.length);
        int r = new Random().nextInt(this.plants.length);
        System.arraycopy(newlist, r, this.selectedImage, 0, this.maxfinishednum / 2);
        System.arraycopy(newlist, r, this.selectedImage, this.maxfinishednum / 2, this.maxfinishednum / 2);
        randImageType(this.selectedImage);
        ((RelativeLayout) findViewById(R.id.main)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("Applause", "initGame(): relative.onClick()");
            }
        });
        int z = 0;
        for (int i = 0; i < this.heightnum; i++) {
            int j = 0;
            while (j < this.widthnum) {
                clickImage((ImageView) findViewById(this.columnImageIdList[i][j]), z);
                this.checkedImageList[z] = 0;
                j++;
                z++;
            }
        }
        if (!(this.mGameStatus == GAME_STATUS.GS_PAUSE || this.mGameStatus == GAME_STATUS.GS_PLAYING)) {
            Log.v("main", "inigame(), mGameStatus = " + this.mGameStatus.toString());
        }
        ((Button) findViewById(R.id.button_menu)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Applause.this.playSP(4);
                Applause.this.openOptionsMenu();
            }
        });
        ((Button) findViewById(R.id.button_cup)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Applause.this.playSP(4);
                Applause.this.showSumbitScoreDlg(true);
            }
        });
        this.mGameStatus = GAME_STATUS.GS_INIT;
        this.mGameTime = Math.min(300, ((this.level - 1) * 10) + 100);
        this.mBar.setMax(this.mGameTime);
        this.mBar.setProgress(this.mGameTime);
        this.mGameStatus = GAME_STATUS.GS_PLAYING;
        updateScoreAndLevel();
        updateGameIcon();
        updateTimeText();
        playMusic();
        setCheckBox();
    }

    public void newGame() {
        this.level = 1;
        this.score = 0;
        initGame();
    }

    /* access modifiers changed from: private */
    public boolean setGameOver() {
        Log.v("main", "setGameOver(), mTimer.cancel()");
        this.mGameStatus = GAME_STATUS.GS_GAMEOVER;
        showGameOverDlg();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean setGamePause(boolean b) {
        if (!b) {
            this.mGameStatus = GAME_STATUS.GS_PLAYING;
            return true;
        }
        this.mGameStatus = GAME_STATUS.GS_PAUSE;
        return true;
    }

    /* access modifiers changed from: private */
    public void nextLevel() {
        setNextLevel();
        initGame();
    }

    public void doCloseCard() {
        doCountDown(this.checkedTwoImg[0]);
        doCountDown(this.checkedTwoImg[1]);
    }

    public void clickImage(final ImageView img, final int rid) {
        img.setBackgroundResource(getCurSkinImageId(2));
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Applause.this.mGameStatus == GAME_STATUS.GS_PAUSE) {
                    Toast.makeText(Applause.this, "Game is pause,please resume first!", 0).show();
                } else if (Applause.this.mGameStatus == GAME_STATUS.GS_GAMEOVER) {
                    Applause.this.newGame();
                } else {
                    Applause applause = Applause.this;
                    applause.mTapCnt = applause.mTapCnt + 1;
                    if (Applause.this.mTvTapCnt != null) {
                        Applause.this.mTvTapCnt.setText(new StringBuilder().append(Applause.this.mTapCnt).toString());
                    } else {
                        Log.v("Applause", "TvTapCnt is null");
                    }
                    if (!(Applause.this.lastcheckedImageId != -1 || Applause.this.checkedTwoImg[0] == -1 || Applause.this.checkedTwoImg[1] == -1)) {
                        Log.v("Applause", "checkedTwoImg........");
                        Applause.this.doCountDown(Applause.this.checkedTwoImg[0]);
                        Applause.this.doCountDown(Applause.this.checkedTwoImg[1]);
                    }
                    if (Applause.this.finishednum == Applause.this.maxfinishednum) {
                        Log.v("Applause", "clickImage():Pass Level, finishedNum=MaxNum");
                    }
                    if (Applause.this.checkedImageList[rid] != 1 && rid != Applause.this.lastcheckedImageId) {
                        Log.v("Applause", "rid != lastcheckedImageId");
                        img.setBackgroundResource(Applause.this.randImage[rid]);
                        Animation anim2 = AnimationUtils.loadAnimation(Applause.this, R.anim.slide_out_left);
                        anim2.setDuration(400);
                        img.startAnimation(anim2);
                        if (Applause.this.lastcheckedImageId == -1) {
                            Log.v("Applause", "checkImage():lastcheckedImageId = -1");
                            Applause.this.lastcheckedImageId = rid;
                            Applause.this.lastimg = img;
                            Applause.this.checkedTwoImg[0] = rid;
                            img.startAnimation(AnimationUtils.loadAnimation(Applause.this, R.anim.grow_fade_in_center_loop));
                        } else if (Applause.this.randImage[rid] == Applause.this.randImage[Applause.this.lastcheckedImageId]) {
                            Log.v("Applause", "clickImage():Right: lastcheckedImageId(" + Applause.this.lastcheckedImageId + ") != -1");
                            Applause.this.scoreAdd();
                            Animation anim22 = AnimationUtils.loadAnimation(Applause.this, R.anim.grow_fade_in_center_match);
                            Applause.this.lastimg.startAnimation(anim22);
                            img.startAnimation(anim22);
                            Applause.this.updateScoreAndLevel();
                            Applause.this.checkedTwoImg[0] = -1;
                            Applause.this.checkedTwoImg[1] = -1;
                            Applause applause2 = Applause.this;
                            applause2.finishednum = applause2.finishednum + 2;
                            Applause.this.playSP(1);
                            Applause.this.gameAddTime();
                            Applause.this.checkedImageList[rid] = 1;
                            Applause.this.checkedImageList[Applause.this.lastcheckedImageId] = 1;
                            Applause.this.lastcheckedImageId = -1;
                            if (Applause.this.finishednum >= Applause.this.maxfinishednum) {
                                Applause.this.playSP(2);
                                Applause.this.PostMsg(2);
                            }
                        } else {
                            Log.v("Applause", "clickImage():Wrong: lastcheckedImageId == -1");
                            Applause.this.checkedTwoImg[1] = rid;
                            Applause.this.lastcheckedImageId = -1;
                            Applause.this.scoreReduce();
                            Applause.this.playSP(3);
                            Applause.this.updateScoreAndLevel();
                            Applause.this.playVibrate();
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void scoreAdd() {
        this.score += this.level * 5;
    }

    /* access modifiers changed from: private */
    public void scoreReduce() {
        this.score = Math.max(0, this.score - 2);
    }

    /* access modifiers changed from: private */
    public int getHighScore() {
        return this.score;
    }

    public boolean updateProgressBar() {
        if (this.mBar == null) {
            return true;
        }
        if (this.mGameStatus == GAME_STATUS.GS_GAMEOVER || this.mGameStatus == GAME_STATUS.GS_LEVELUP) {
            return true;
        }
        this.mGameTime--;
        this.mPlayTime++;
        if (this.mGameTime >= 0) {
            this.mBar.setProgress(this.mGameTime);
            updateTimeText();
        }
        if (this.mGameTime > 0) {
            return true;
        }
        this.mGameTime = 0;
        return false;
    }

    public void updateTimeText() {
    }

    public void updateGameIcon() {
        int i = this.level - 1;
        if (this.level < this.plants.length) {
        }
    }

    public void gameAddTime() {
        if (this.mGameStatus == GAME_STATUS.GS_PLAYING) {
            this.mGameTime += this.level + 3;
        }
    }

    private void setNextLevel() {
        this.level++;
    }

    public void doCountDown(int resid) {
        int imgResId = getCurSkinImageId(2);
        ImageView img = (ImageView) findViewById(this.columnImageIdList[(int) Math.floor((double) (resid / this.widthnum))][resid % this.widthnum]);
        img.setBackgroundResource(imgResId);
        Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        anim2.setDuration(300);
        img.startAnimation(anim2);
    }

    public void randImageType(int[] selectedImage2) {
        int len = selectedImage2.length;
        int[] result = new int[len];
        Random random = new Random();
        for (int i = 0; i < len; i++) {
            int r = random.nextInt(len - i);
            result[i] = selectedImage2[r];
            selectedImage2[r] = selectedImage2[(len - 1) - i];
        }
        for (int i2 = 0; i2 < this.maxfinishednum; i2++) {
            this.randImage[i2] = result[i2];
        }
    }

    public void playVibrate() {
        if (this.mVibrate) {
            try {
                ((Vibrator) getSystemService("vibrator")).vibrate(new long[]{10, 30, 20, 10}, -1);
            } catch (Exception e) {
            }
        }
    }

    private void initSP() {
        this.mSoundPool = new SoundPool(30, 3, 100);
        this.mSoundMap = new HashMap<>();
        this.mSoundMap.put(1, Integer.valueOf(this.mSoundPool.load(getBaseContext(), R.raw.matchit, 0)));
        this.mSoundMap.put(2, Integer.valueOf(this.mSoundPool.load(getBaseContext(), R.raw.win, 0)));
        this.mSoundMap.put(3, Integer.valueOf(this.mSoundPool.load(getBaseContext(), R.raw.wrong, 0)));
        this.mSoundMap.put(4, Integer.valueOf(this.mSoundPool.load(getBaseContext(), R.raw.click, 0)));
    }

    public void playSP(int soundId) {
        if (this.mIsMusic) {
            this.mSoundPool.play(this.mSoundMap.get(Integer.valueOf(soundId)).intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
        }
    }

    public void stopSP(int soundId) {
        this.mSoundPool.stop(this.mSoundMap.get(Integer.valueOf(soundId)).intValue());
    }

    private void freeSP() {
        if (this.mSoundPool != null) {
            this.mSoundPool.release();
            this.mSoundPool = null;
        }
        if (this.mSoundMap != null) {
            this.mSoundMap.clear();
            this.mSoundMap = null;
        }
    }

    public void playMusic() {
        if (this.mIsMusic) {
            stopMusic();
            int resId = this.musicResId[this.mRand.nextInt(this.musicResId.length)];
            if (this.mMusicPlayer == null) {
                this.mMusicPlayer = MediaPlayer.create(getBaseContext(), resId);
            }
            this.mMusicPlayer.start();
            this.mMusicPlayer.setLooping(true);
        }
    }

    public void stopMusic() {
        if (this.mMusicPlayer != null) {
            if (this.mMusicPlayer.isPlaying()) {
                this.mMusicPlayer.stop();
            }
            this.mMusicPlayer.release();
            this.mMusicPlayer = null;
        }
    }

    private void pauseMusic() {
        if (this.mMusicPlayer != null && this.mMusicPlayer.isPlaying()) {
            this.mMusicPlayer.pause();
        }
    }

    private void resumeMusic() {
        if (this.mMusicPlayer != null) {
            this.mMusicPlayer.start();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        openOptionsMenu();
        return true;
    }

    private void exitConfirm() {
        new AlertDialog.Builder(this).setMessage("Do you really want to quit?").setTitle("Exit").setIcon((int) R.drawable.icon).setPositiveButton("More Game", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Applause.this.showMoreApp(0);
            }
        }).setNeutralButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Applause.this.exitApp();
            }
        }).create().show();
    }

    /* access modifiers changed from: package-private */
    public void mySleep(int millSecond) {
        try {
            Thread.sleep((long) millSecond);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void showAd() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public boolean saveas(int ressound) {
        InputStream fIn = getBaseContext().getResources().openRawResource(ressound);
        try {
            byte[] buffer = new byte[fIn.available()];
            fIn.read(buffer);
            fIn.close();
            if (!new File("/sdcard/media/audio/ringtones/").exists()) {
                new File("/sdcard/media/audio/ringtones/").mkdirs();
            }
            try {
                FileOutputStream save = new FileOutputStream(String.valueOf("/sdcard/media/audio/ringtones/") + "examplefile.midi");
                save.write(buffer);
                save.flush();
                save.close();
                sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + "/sdcard/media/audio/ringtones/" + "examplefile.midi")));
                File k = new File("/sdcard/media/audio/ringtones/", "examplefile.midi");
                ContentValues values = new ContentValues();
                values.put("_data", k.getAbsolutePath());
                values.put("title", "mario");
                values.put("mime_type", "audio/midi");
                values.put("artist", "jazz");
                values.put("is_ringtone", (Boolean) true);
                values.put("is_notification", (Boolean) true);
                values.put("is_alarm", (Boolean) true);
                values.put("is_music", (Boolean) false);
                RingtoneManager.setActualDefaultRingtoneUri(this, 1, getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath()), values));
                return true;
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e2) {
                return false;
            }
        } catch (IOException e3) {
            return false;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 3, 0, "New Game").setIcon((int) R.drawable.m1);
        menu.add(0, 5, 0, "More Game").setIcon((int) R.drawable.m2);
        menu.add(0, 99, 0, "Exit").setIcon((int) R.drawable.m3);
        return super.onPrepareOptionsMenu(menu);
    }

    public void updateScoreAndLevel() {
        ((TextView) findViewById(R.id.game_score)).setText("Score " + Integer.toString(this.score));
        ((TextView) findViewById(R.id.game_level)).setText("Level: " + Integer.toString(this.level));
    }

    /* access modifiers changed from: package-private */
    public void showMoreApp(int selIndex) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GameAd.getMoreGameUrl())));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        pauseMusic();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        resumeMusic();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.styleable.com_google_ads_AdView_primaryTextColor:
                showSumbitScoreDlg(true);
                break;
            case R.styleable.com_google_ads_AdView_keywords:
                this.level = 1;
                initGame();
                break;
            case R.styleable.com_google_ads_AdView_testing:
                showMoreApp(0);
                break;
            case 9:
                if (this.mGameStatus != GAME_STATUS.GS_PLAYING) {
                    setGamePause(false);
                    break;
                } else {
                    setGamePause(true);
                    break;
                }
            case 99:
                startActivity(new Intent(this, AdSplash.class));
                exitApp();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showOptionDlg() {
        Intent intent = new Intent();
        intent.setClass(this, OptionActivity.class);
        intent.putExtra("GameMusic", this.mIsMusic);
        intent.putExtra("MatchSound", this.mMatchSound);
        intent.putExtra("Vibrate", this.mVibrate);
        intent.putExtra("ChangeTheme", this.mChangeTheme);
        startActivityForResult(intent, 2);
    }

    public void loadGameParam() {
        SharedPreferences settings = getSharedPreferences("com.feasy.app.memory.crazyfarm2", 0);
        this.mIsMusic = settings.getBoolean("isMusic", true);
        this.mMatchSound = settings.getBoolean("isMatchSound", true);
        this.mVibrate = settings.getBoolean("isVibrate", true);
        this.mChangeTheme = settings.getBoolean("isChangeTheme", true);
        this.mSkinIndex = settings.getInt("skinIndex", -1);
    }

    public void saveGameParam() {
        SharedPreferences.Editor editor = getSharedPreferences("com.feasy.app.memory.crazyfarm2", 0).edit();
        editor.putBoolean("isMusic", this.mIsMusic);
        editor.putBoolean("isMatchSound", this.mMatchSound);
        editor.putBoolean("isVibrate", this.mVibrate);
        editor.putBoolean("isChangeTheme", this.mChangeTheme);
        editor.putInt("skinIndex", this.mSkinIndex);
        editor.commit();
    }

    public void exitApp() {
        stopMusic();
        freeSP();
        saveGameParam();
        this.mTimer.cancel();
        finish();
    }

    public void showGameOverDlg2() {
        View entryDlg = LayoutInflater.from(this).inflate((int) R.layout.popup_lv, (ViewGroup) null);
        this.mGameOverDlg = new AlertDialog.Builder(this).setView(entryDlg).setTitle("Game Over").create();
        ListView lv = (ListView) entryDlg.findViewById(R.id.lv);
        ArrayList<HashMap<String, Object>> users = new ArrayList<>();
        HashMap<String, Object> rec1 = new HashMap<>();
        HashMap<String, Object> rec2 = new HashMap<>();
        HashMap<String, Object> rec3 = new HashMap<>();
        rec1.put("img", Integer.valueOf((int) R.drawable.ic_menu_emoticons));
        rec1.put("name", "Try Again");
        users.add(rec1);
        rec2.put("img", Integer.valueOf((int) R.drawable.ic_menu_star));
        rec2.put("name", "New Game");
        users.add(rec2);
        rec3.put("img", Integer.valueOf((int) R.drawable.ic_menu_revert));
        rec3.put("name", "Back");
        users.add(rec3);
        lv.setAdapter((ListAdapter) new SimpleAdapter(this, users, R.layout.popup_lv_item, new String[]{"img", "name"}, new int[]{R.id.img_icon, R.id.tv_name}));
        lv.setBackgroundColor(-16777216);
        lv.setItemsCanFocus(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int index = arg2;
                if (index == 0) {
                    Applause.this.initGame();
                } else if (1 == index) {
                    Applause.this.newGame();
                }
                Applause.this.mGameOverDlg.dismiss();
            }
        });
        this.mGameOverDlg.show();
    }

    private void updateBkg() {
        ((RelativeLayout) findViewById(R.id.main)).setBackgroundResource(getCurSkinImageId(1));
    }

    public int getCurSkinImageId(int imgType) {
        int bkgResId = R.drawable.bkg0;
        int imgResId = R.drawable.image0;
        switch (this.mSkinIndex) {
            case 0:
                bkgResId = R.drawable.bkg0;
                imgResId = R.drawable.image0;
                break;
            case R.styleable.com_google_ads_AdView_primaryTextColor:
                bkgResId = R.drawable.bkg1;
                imgResId = R.drawable.image1;
                break;
            case R.styleable.com_google_ads_AdView_secondaryTextColor:
                bkgResId = R.drawable.bkg2;
                imgResId = R.drawable.image2;
                break;
        }
        if (imgType == 1) {
            return bkgResId;
        }
        return imgResId;
    }

    public void setRandomSkin() {
        this.mSkinIndex = this.mRand.nextInt(3);
    }

    private void initScoreLoop() {
        ScoreloopManager.init(this, "c1538351-0b1d-4706-9571-4f4784a8080f", "Da7LGFF2FjuBy6Qw2501pie9V4BPb/xOgi7Y47hYcHAXxNMDYzVJQw==");
        ScoreloopManager.setNumberOfModes(1);
    }

    public void showGlobalScore() {
        startActivity(new Intent(this, HighscoresActivity.class));
    }

    /* access modifiers changed from: private */
    public void waitDlgShow() {
        waitDlgClose();
        this.waitDlg = ProgressDialog.show(this, "Ranking", "Please wait,Submit Score to Server", true);
        this.waitDlg.setCancelable(true);
    }

    /* access modifiers changed from: private */
    public void waitDlgClose() {
        if (this.waitDlg != null && this.waitDlg.isShowing()) {
            this.waitDlg.dismiss();
        }
    }

    public void doFinishLevel() {
        this.mGameStatus = GAME_STATUS.GS_LEVELUP;
        AlertDialog dlg = new AlertDialog.Builder(this).setTitle("Congratulation!").setMessage(String.valueOf("Level: " + Integer.toString(this.level) + "\n") + "Your score: " + Integer.toString(this.score) + "\nPass Time: " + Integer.toString(this.mPlayTime) + "s\n").setIcon((int) R.drawable.ic_happy).setPositiveButton("Next Level", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).create();
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                Applause.this.nextLevel();
            }
        });
        dlg.show();
    }

    private void setCheckBox() {
        this.mCheckBoxPause = (CheckBox) findViewById(R.id.ck_pause);
        this.mCheckBoxPause.setChecked(true);
        this.mCheckBoxPause.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Applause.this.playSP(4);
                if (isChecked) {
                    boolean unused = Applause.this.setGamePause(false);
                    return;
                }
                boolean unused2 = Applause.this.setGamePause(true);
                Applause.this.showHint("Game Pause!");
            }
        });
        this.mCheckBoxSound = (CheckBox) findViewById(R.id.ck_sound);
        this.mCheckBoxSound.setChecked(this.mIsMusic);
        this.mCheckBoxSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Applause.this.playSP(4);
                Applause.this.mIsMusic = isChecked;
                if (isChecked) {
                    Applause.this.playMusic();
                } else {
                    Applause.this.stopMusic();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void restoreGamePause() {
        if (this.mCheckBoxPause != null) {
            if (this.mCheckBoxPause.isChecked()) {
                setGamePause(false);
            } else {
                setGamePause(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showHint(String msg) {
        Toast.makeText(this, msg, 1).show();
    }

    public void showGameOverDlg() {
        AlertDialog dlg = new AlertDialog.Builder(this).setTitle("Game over").setMessage("Submit your current score (" + getHighScore() + ") to the global server? ").setIcon((int) R.drawable.ic_sad).setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                Applause.this.waitDlgShow();
                ScoreloopManager.submitScore(Applause.this.getHighScore(), 0, new ScoreSubmitObserver(Applause.this, null));
            }
        }).setNeutralButton("Try again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                Applause.this.initGame();
            }
        }).create();
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
            }
        });
        dlg.show();
    }

    public void showSumbitScoreDlg(boolean isFirst) {
        String sMsg;
        if (isFirst) {
            sMsg = "Submit your current score (" + getHighScore() + ") to the global high scores list? ";
        } else {
            sMsg = "Network error, submit failed, Try again?";
        }
        setGamePause(true);
        AlertDialog dlg = new AlertDialog.Builder(this).setMessage(sMsg).setTitle("Global Ranking").setIcon((int) R.drawable.ic_cup).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Applause.this.waitDlgShow();
                ScoreloopManager.submitScore(Applause.this.getHighScore(), 0, new ScoreSubmitObserver(Applause.this, null));
            }
        }).setNeutralButton("Leader Boards", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                Applause.this.showGlobalScore();
            }
        }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).create();
        if (dlg != null) {
            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialog) {
                    Applause.this.restoreGamePause();
                }
            });
            dlg.show();
        }
    }

    private class ScoreSubmitObserver implements RequestControllerObserver {
        private ScoreSubmitObserver() {
        }

        /* synthetic */ ScoreSubmitObserver(Applause applause, ScoreSubmitObserver scoreSubmitObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            Log.v("Main", "requestControllerDidFail(), submit failed");
            Applause.this.showSumbitScoreDlg(false);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            Log.v("Main", "requestControllerDidReceiveResponse()....ok");
            Applause.this.waitDlgClose();
            Applause.this.showGlobalScore();
        }
    }
}
