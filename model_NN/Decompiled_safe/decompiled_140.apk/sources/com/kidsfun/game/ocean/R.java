package com.kidsfun.game.ocean;

public final class R {

    public static final class anim {
        public static final int alpha_rotate = 2130968576;
        public static final int alpha_scale = 2130968577;
        public static final int alpha_scale_rotate = 2130968578;
        public static final int alpha_scale_translate = 2130968579;
        public static final int alpha_scale_translate_rotate = 2130968580;
        public static final int alpha_translate = 2130968581;
        public static final int alpha_translate_rotate = 2130968582;
        public static final int decelerate_interpolator = 2130968583;
        public static final int gokou = 2130968584;
        public static final int grow_fade_in_center = 2130968585;
        public static final int grow_fade_in_center_loop = 2130968586;
        public static final int grow_fade_in_center_match = 2130968587;
        public static final int layout_anim_ctrl = 2130968588;
        public static final int my_alpha_action = 2130968589;
        public static final int my_rotate_action = 2130968590;
        public static final int my_scale_action = 2130968591;
        public static final int my_translate_action = 2130968592;
        public static final int myanimation_simple = 2130968593;
        public static final int myown_design = 2130968594;
        public static final int scale_rotate = 2130968595;
        public static final int scale_translate = 2130968596;
        public static final int scale_translate_rotate = 2130968597;
        public static final int slide_in_left = 2130968598;
        public static final int slide_in_right = 2130968599;
        public static final int slide_out_left = 2130968600;
        public static final int slide_out_right = 2130968601;
        public static final int translate_alpha_anim = 2130968602;
        public static final int translate_rotate = 2130968603;
        public static final int welcome = 2130968604;
    }

    public static final class array {
        public static final int sl_game_modes = 2131165184;
    }

    public static final class attr {
        public static final int adSize = 2130771974;
        public static final int adUnitId = 2130771975;
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
        public static final int testing = 2130771973;
    }

    public static final class color {
        public static final int sl_selector_color = 2131361792;
    }

    public static final class drawable {
        public static final int aa = 2130837504;
        public static final int app1 = 2130837505;
        public static final int app2 = 2130837506;
        public static final int bb = 2130837507;
        public static final int bestscore = 2130837508;
        public static final int bg_head = 2130837509;
        public static final int bg_splash = 2130837510;
        public static final int bkg0 = 2130837511;
        public static final int bkg1 = 2130837512;
        public static final int bkg2 = 2130837513;
        public static final int btn_cup = 2130837514;
        public static final int btn_menu = 2130837515;
        public static final int cc = 2130837516;
        public static final int checkbox_pause = 2130837517;
        public static final int checkbox_sound = 2130837518;
        public static final int cup_default = 2130837519;
        public static final int cup_pressed = 2130837520;
        public static final int dd = 2130837521;
        public static final int ee = 2130837522;
        public static final int ff = 2130837523;
        public static final int gad_bg_splash = 2130837524;
        public static final int gad_btn_close = 2130837525;
        public static final int gad_btn_download = 2130837526;
        public static final int gad_btn_moregame = 2130837527;
        public static final int gad_close_down = 2130837528;
        public static final int gad_close_up = 2130837529;
        public static final int gad_download_down = 2130837530;
        public static final int gad_download_up = 2130837531;
        public static final int gad_more_down = 2130837532;
        public static final int gad_more_up = 2130837533;
        public static final int game_logo = 2130837534;
        public static final int gg = 2130837535;
        public static final int hh = 2130837536;
        public static final int ic_bye = 2130837537;
        public static final int ic_clock = 2130837538;
        public static final int ic_cup = 2130837539;
        public static final int ic_flower = 2130837540;
        public static final int ic_happy = 2130837541;
        public static final int ic_menu_block = 2130837542;
        public static final int ic_menu_close_clear_cancel = 2130837543;
        public static final int ic_menu_emoticons = 2130837544;
        public static final int ic_menu_info_details = 2130837545;
        public static final int ic_menu_refresh = 2130837546;
        public static final int ic_menu_revert = 2130837547;
        public static final int ic_menu_star = 2130837548;
        public static final int ic_menu_view = 2130837549;
        public static final int ic_moregame = 2130837550;
        public static final int ic_new_game = 2130837551;
        public static final int ic_quit = 2130837552;
        public static final int ic_sad = 2130837553;
        public static final int ic_settings = 2130837554;
        public static final int ic_sound_off = 2130837555;
        public static final int ic_sound_on = 2130837556;
        public static final int icon = 2130837557;
        public static final int ii = 2130837558;
        public static final int image0 = 2130837559;
        public static final int image1 = 2130837560;
        public static final int image2 = 2130837561;
        public static final int jj = 2130837562;
        public static final int kk = 2130837563;
        public static final int ll = 2130837564;
        public static final int m1 = 2130837565;
        public static final int m2 = 2130837566;
        public static final int m3 = 2130837567;
        public static final int memory_logo = 2130837568;
        public static final int menu_default = 2130837569;
        public static final int menu_pressed = 2130837570;
        public static final int menu_quit = 2130837571;
        public static final int mm = 2130837572;
        public static final int nn = 2130837573;
        public static final int oo = 2130837574;
        public static final int other = 2130837575;
        public static final int pause = 2130837576;
        public static final int pause_btn = 2130837577;
        public static final int play = 2130837578;
        public static final int play_btn = 2130837579;
        public static final int pp = 2130837580;
        public static final int qq = 2130837581;
        public static final int rr = 2130837582;
        public static final int sl_bg_btn = 2130837583;
        public static final int sl_bg_btn_pre = 2130837584;
        public static final int sl_bg_dropdown = 2130837585;
        public static final int sl_bg_dropdown_pre = 2130837586;
        public static final int sl_bg_h1 = 2130837587;
        public static final int sl_bg_list = 2130837588;
        public static final int sl_bg_list_pre = 2130837589;
        public static final int sl_divider = 2130837590;
        public static final int sl_divider_list = 2130837591;
        public static final int sl_logo = 2130837592;
        public static final int sl_menu_highscores = 2130837593;
        public static final int sl_menu_profile = 2130837594;
        public static final int sl_selector_btn = 2130837595;
        public static final int sl_selector_dropdown = 2130837596;
        public static final int sl_selector_list = 2130837597;
        public static final int ss = 2130837598;
        public static final int tt = 2130837599;
        public static final int uu = 2130837600;
        public static final int vv = 2130837601;
    }

    public static final class id {
        public static final int BANNER = 2131099648;
        public static final int IAB_BANNER = 2131099650;
        public static final int IAB_LEADERBOARD = 2131099651;
        public static final int IAB_MRECT = 2131099649;
        public static final int TextView02 = 2131099733;
        public static final int ad2 = 2131099682;
        public static final int ad_splash_layout = 2131099652;
        public static final int btn_cancel = 2131099717;
        public static final int btn_close = 2131099655;
        public static final int btn_download = 2131099653;
        public static final int btn_more = 2131099654;
        public static final int btn_profile = 2131099725;
        public static final int btn_save = 2131099718;
        public static final int button_cup = 2131099684;
        public static final int button_menu = 2131099686;
        public static final int cb_changetheme = 2131099716;
        public static final int cb_music = 2131099713;
        public static final int cb_sound = 2131099714;
        public static final int cb_vibrate = 2131099715;
        public static final int ck_pause = 2131099685;
        public static final int ck_sound = 2131099687;
        public static final int email = 2131099732;
        public static final int game_Score = 2131099711;
        public static final int game_icon = 2131099657;
        public static final int game_level = 2131099658;
        public static final int game_mode_spinner = 2131099724;
        public static final int game_score = 2131099659;
        public static final int game_time = 2131099661;
        public static final int game_timebar = 2131099660;
        public static final int head1 = 2131099683;
        public static final int highscores_list_item = 2131099728;
        public static final int img11 = 2131099662;
        public static final int img12 = 2131099663;
        public static final int img13 = 2131099664;
        public static final int img14 = 2131099665;
        public static final int img15 = 2131099693;
        public static final int img16 = 2131099699;
        public static final int img21 = 2131099666;
        public static final int img22 = 2131099667;
        public static final int img23 = 2131099668;
        public static final int img24 = 2131099669;
        public static final int img25 = 2131099694;
        public static final int img26 = 2131099700;
        public static final int img31 = 2131099670;
        public static final int img32 = 2131099671;
        public static final int img33 = 2131099672;
        public static final int img34 = 2131099673;
        public static final int img35 = 2131099695;
        public static final int img36 = 2131099701;
        public static final int img41 = 2131099674;
        public static final int img42 = 2131099675;
        public static final int img43 = 2131099676;
        public static final int img44 = 2131099677;
        public static final int img45 = 2131099696;
        public static final int img46 = 2131099702;
        public static final int img51 = 2131099678;
        public static final int img52 = 2131099679;
        public static final int img53 = 2131099680;
        public static final int img54 = 2131099681;
        public static final int img55 = 2131099697;
        public static final int img56 = 2131099703;
        public static final int img61 = 2131099689;
        public static final int img62 = 2131099690;
        public static final int img63 = 2131099691;
        public static final int img64 = 2131099692;
        public static final int img65 = 2131099698;
        public static final int img66 = 2131099704;
        public static final int img71 = 2131099705;
        public static final int img72 = 2131099706;
        public static final int img73 = 2131099707;
        public static final int img74 = 2131099708;
        public static final int img75 = 2131099709;
        public static final int img76 = 2131099710;
        public static final int img_icon = 2131099720;
        public static final int img_logo = 2131099736;
        public static final int list_view = 2131099726;
        public static final int login = 2131099730;
        public static final int lv = 2131099719;
        public static final int main = 2131099656;
        public static final int myscore_view = 2131099727;
        public static final int progress_indicator = 2131099723;
        public static final int rank = 2131099729;
        public static final int score = 2131099731;
        public static final int spinnerTarget = 2131099735;
        public static final int tap_cnt = 2131099688;
        public static final int title_login = 2131099722;
        public static final int tv_name = 2131099721;
        public static final int tv_sound = 2131099712;
        public static final int update_button = 2131099734;
    }

    public static final class layout {
        public static final int ad_splash = 2130903040;
        public static final int main = 2130903041;
        public static final int main2x3 = 2130903042;
        public static final int main3x4 = 2130903043;
        public static final int main4x4 = 2130903044;
        public static final int main4x5 = 2130903045;
        public static final int main4x6 = 2130903046;
        public static final int main5x6 = 2130903047;
        public static final int main6x8 = 2130903048;
        public static final int main_droid = 2130903049;
        public static final int main_old = 2130903050;
        public static final int option = 2130903051;
        public static final int popup_lv = 2130903052;
        public static final int popup_lv_item = 2130903053;
        public static final int sl_highscores = 2130903054;
        public static final int sl_highscores_list_item = 2130903055;
        public static final int sl_profile = 2130903056;
        public static final int sl_spinner_item = 2130903057;
        public static final int splash = 2130903058;
    }

    public static final class raw {
        public static final int bg_01 = 2131034112;
        public static final int bg_02 = 2131034113;
        public static final int bg_03 = 2131034114;
        public static final int bg_04 = 2131034115;
        public static final int click = 2131034116;
        public static final int matchit = 2131034117;
        public static final int matchit2 = 2131034118;
        public static final int splash = 2131034119;
        public static final int win = 2131034120;
        public static final int wrong = 2131034121;
    }

    public static final class string {
        public static final int app_name = 2131230734;
        public static final int hello = 2131230733;
        public static final int sl_email = 2131230724;
        public static final int sl_error_message_email_already_taken = 2131230731;
        public static final int sl_error_message_invalid_email_format = 2131230732;
        public static final int sl_error_message_name_already_taken = 2131230730;
        public static final int sl_error_message_network = 2131230729;
        public static final int sl_error_message_not_on_highscore_list = 2131230728;
        public static final int sl_highscores = 2131230721;
        public static final int sl_login = 2131230723;
        public static final int sl_next = 2131230726;
        public static final int sl_prev = 2131230725;
        public static final int sl_profile = 2131230720;
        public static final int sl_top = 2131230727;
        public static final int sl_update_profile = 2131230722;
    }

    public static final class style {
        public static final int sl_heading = 2131296256;
        public static final int sl_normal = 2131296258;
        public static final int sl_title_bar = 2131296257;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval, R.attr.testing, R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 6;
        public static final int com_google_ads_AdView_adUnitId = 7;
        public static final int com_google_ads_AdView_backgroundColor = 0;
        public static final int com_google_ads_AdView_keywords = 3;
        public static final int com_google_ads_AdView_primaryTextColor = 1;
        public static final int com_google_ads_AdView_refreshInterval = 4;
        public static final int com_google_ads_AdView_secondaryTextColor = 2;
        public static final int com_google_ads_AdView_testing = 5;
    }
}
