package com.google.ads;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Set;

public class InstallReceiver extends BroadcastReceiver {
    private static String a(String str, String str2, String str3) {
        if (str != null) {
            try {
                StringBuilder sb = null;
                for (String str4 : str.split("&")) {
                    if (str4.startsWith("admob_")) {
                        String[] split = str4.substring("admob_".length()).split("=");
                        String encode = URLEncoder.encode(split[0], "UTF-8");
                        String encode2 = URLEncoder.encode(split[1], "UTF-8");
                        if (sb == null) {
                            sb = new StringBuilder(128);
                        } else {
                            sb.append("&");
                        }
                        sb.append(encode).append("=").append(encode2);
                    }
                }
                if (sb != null) {
                    sb.append("&").append("isu").append("=").append(URLEncoder.encode(str2, "UTF-8"));
                    sb.append("&").append("app_id").append("=").append(URLEncoder.encode(str3, "UTF-8"));
                    return "http://a.admob.com/f0?" + sb.toString();
                }
            } catch (UnsupportedEncodingException e) {
                a.a("Could not create install URL.  Install not tracked.", e);
            }
        }
        return null;
    }

    public void forwardIntent(Context context, Intent intent) {
        ActivityInfo receiverInfo;
        Set<String> keySet;
        String str;
        Exception exc;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (!(packageManager == null || (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, InstallReceiver.class), 128)) == null || receiverInfo.metaData == null || (keySet = receiverInfo.metaData.keySet()) == null)) {
                for (String string : keySet) {
                    try {
                        String string2 = receiverInfo.metaData.getString(string);
                        try {
                            if (!string2.equals("com.google.android.apps.analytics.AnalyticsReceiver")) {
                                ((BroadcastReceiver) Class.forName(string2).newInstance()).onReceive(context, intent);
                                a.a("Successfully forwarded install notification to " + string2);
                            }
                        } catch (Exception e) {
                            Exception exc2 = e;
                            str = string2;
                            exc = exc2;
                            a.b("Could not forward Market's INSTALL_REFERRER intent to " + str, exc);
                        }
                    } catch (Exception e2) {
                        Exception exc3 = e2;
                        str = null;
                        exc = exc3;
                        a.b("Could not forward Market's INSTALL_REFERRER intent to " + str, exc);
                    }
                }
            }
            try {
                ((BroadcastReceiver) Class.forName("com.google.android.apps.analytics.AnalyticsReceiver").newInstance()).onReceive(context, intent);
                a.a("Successfully forwarded install notification to com.google.android.apps.analytics.AnalyticsReceiver");
            } catch (ClassNotFoundException e3) {
                a.d("Google Analytics not installed.");
            } catch (Exception e4) {
                a.b("Exception from the Google Analytics install receiver.", e4);
            }
        } catch (Exception e5) {
            a.b("Unhandled exception while forwarding install intents. Possibly lost some install information.", e5);
        }
    }

    public void onReceive(Context context, Intent intent) {
        try {
            String stringExtra = intent.getStringExtra("referrer");
            String a2 = AdUtil.a(context);
            String a3 = a(stringExtra, a2, context.getPackageName());
            if (a3 != null) {
                a.d("Processing install from an AdMob ad (" + a3 + ").");
            }
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(a3).openConnection();
            AdUtil.a(httpURLConnection, context.getApplicationContext());
            httpURLConnection.setRequestProperty("X-Admob-Isu", a2);
            httpURLConnection.getResponseCode();
            forwardIntent(context, intent);
        } catch (Exception e) {
            a.a("Unhandled exception processing Market install.", e);
        }
    }
}
