package com.a.a;

import android.view.MotionEvent;
import android.view.View;
import com.kidsfun.game.ocean.R;

class j implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f15a;

    j(q qVar) {
        this.f15a = qVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.q.a(com.a.a.q, boolean):void
     arg types: [com.a.a.q, int]
     candidates:
      com.a.a.q.a(java.lang.String, java.util.Map):java.net.URL
      com.a.a.q.a(java.lang.Throwable, boolean):void
      com.a.a.q.a(boolean, boolean):void
      com.a.a.q.a(com.a.a.q, boolean):void */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.f15a.k.setBackgroundColor(q.f);
                return true;
            case R.styleable.com_google_ads_AdView_primaryTextColor /*1*/:
                this.f15a.k.setBackgroundColor(q.e);
                this.f15a.a(true);
                return true;
            default:
                return false;
        }
    }
}
