package com.a.a;

import android.content.Context;
import com.scoreloop.client.android.core.utils.Logger;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashMap;

public class l extends q {
    private static final String d = l.class.getSimpleName();
    /* access modifiers changed from: private */
    public a e;
    private e f = new h(this);
    /* access modifiers changed from: private */
    public boolean g;

    public l(Context context, g gVar) {
        super(context, gVar);
    }

    private void b(String str) {
        this.e = a.a(this.f17a, this.f);
        HashMap hashMap = new HashMap();
        hashMap.put("auth_token", str);
        if (this.f17a.f() != null) {
            hashMap.put("generate_session_secret", "1");
        }
        if (this.f17a.g() != null) {
            this.e.b(this.f17a.g(), hashMap);
        } else {
            this.e.a("facebook.auth.getSession", hashMap);
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        HashMap hashMap = new HashMap();
        hashMap.put("fbconnect", "1");
        hashMap.put("connect_display", "touch");
        hashMap.put("api_key", this.f17a.e());
        hashMap.put("next", "fbconnect://success");
        try {
            a("http://www.facebook.com/login.php", "GET", hashMap, null, !this.g);
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.g = true;
        g();
    }

    /* access modifiers changed from: protected */
    public void a(URI uri) {
        String query = uri.getQuery();
        int indexOf = query.indexOf("auth_token=");
        if (indexOf != -1) {
            int indexOf2 = query.indexOf("&");
            int length = indexOf + "auth_token=".length();
            String substring = indexOf2 == -1 ? query.substring(length) : query.substring(length, indexOf2 - length);
            if (substring != null) {
                b(substring);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.e == null) {
            Logger.a(d, "This should not be null, at least on iPhone it is not...");
        } else {
            this.e.e();
        }
    }
}
