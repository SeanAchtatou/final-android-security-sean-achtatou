package com.scoreloop.android.coreui;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.ProgressBar;
import com.kidsfun.game.ocean.R;

abstract class BaseActivity extends ActivityGroup {
    static final int DIALOG_ERROR_EMAIL_ALREADY_TAKEN = 8;
    static final int DIALOG_ERROR_INVALID_EMAIL_FORMAT = 10;
    static final int DIALOG_ERROR_NAME_ALREADY_TAKEN = 11;
    static final int DIALOG_ERROR_NETWORK = 3;
    static final int DIALOG_ERROR_NOT_ON_HIGHSCORE_LIST = 1;

    BaseActivity() {
    }

    private Dialog createErrorDialog(int resId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(resId).setCancelable(true);
        Dialog dialog = builder.create();
        dialog.getWindow().requestFeature(1);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return createErrorDialog(R.string.sl_error_message_not_on_highscore_list);
            case R.styleable.com_google_ads_AdView_secondaryTextColor:
            case R.styleable.com_google_ads_AdView_refreshInterval:
            case R.styleable.com_google_ads_AdView_testing:
            case R.styleable.com_google_ads_AdView_adSize:
            case R.styleable.com_google_ads_AdView_adUnitId:
            case 9:
            default:
                return null;
            case 3:
                return createErrorDialog(R.string.sl_error_message_network);
            case DIALOG_ERROR_EMAIL_ALREADY_TAKEN /*8*/:
                return createErrorDialog(R.string.sl_error_message_email_already_taken);
            case DIALOG_ERROR_INVALID_EMAIL_FORMAT /*10*/:
                return createErrorDialog(R.string.sl_error_message_invalid_email_format);
            case DIALOG_ERROR_NAME_ALREADY_TAKEN /*11*/:
                return createErrorDialog(R.string.sl_error_message_name_already_taken);
        }
    }

    /* access modifiers changed from: package-private */
    public void setProgressIndicator(boolean visible) {
        ((ProgressBar) findViewById(R.id.progress_indicator)).setVisibility(visible ? 0 : 4);
    }
}
