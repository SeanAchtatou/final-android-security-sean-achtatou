package com.scoreloop.client.android.core.controller;

import android.content.Context;
import com.kidsfun.game.ocean.R;
import com.scoreloop.client.android.core.controller.AddressBook;
import com.scoreloop.client.android.core.model.Range;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UsersController extends RequestController {
    private Range c;
    private Integer d;
    private boolean e;
    private SearchSpec f;
    private String g;
    private String h;
    private LoginSearchOperator i;
    private List j;
    private Integer k;

    public enum LoginSearchOperator {
        EXACT_MATCH,
        LIKE,
        PREFIX;

        public static LoginSearchOperator[] a() {
            return (LoginSearchOperator[]) d.clone();
        }
    }

    public UsersController(RequestControllerObserver requestControllerObserver) {
        this(Session.getCurrentSession(), requestControllerObserver);
    }

    public UsersController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.e = true;
        this.i = LoginSearchOperator.PREFIX;
        this.d = 25;
    }

    private D a(String str, SearchSpec searchSpec) {
        this.h = null;
        this.g = str;
        this.f = searchSpec;
        return i();
    }

    private D i() {
        D d2 = new D(this, d(), b(), this.e, this.d);
        if (this.h == null && this.f == null) {
            throw new IllegalStateException("Set a search list or use one of searchBy.. methods first");
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.g);
            jSONObject.put("definition", this.f.a());
            jSONObject.put("id", this.h);
            d2.b(jSONObject);
            if (this.c != null && this.c.getLength() > 0) {
                d2.b(Integer.valueOf(this.c.getLocation()));
                d2.a(Integer.valueOf(this.c.getLength()));
            }
            return d2;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid search spec data");
        }
    }

    private SearchSpec j() {
        SearchSpec searchSpec = new SearchSpec(new C0010i("login", S.ASCENDING));
        if (!this.e) {
            searchSpec.a(new C0017p("skills_game_id", C0003b.IS, b().getIdentifier()));
        }
        return searchSpec;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int f2 = response.f();
        if (f2 != 200) {
            throw new Exception("Request failed with status:" + f2);
        } else if (response.c()) {
            JSONArray d2 = response.d();
            ArrayList arrayList = new ArrayList();
            int length = d2.length();
            for (int i2 = 0; i2 < length; i2++) {
                arrayList.add(new User(d2.getJSONObject(i2).getJSONObject("user")));
            }
            this.j = arrayList;
            return true;
        } else {
            this.k = Integer.valueOf(response.e().getInt("users_count"));
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public int getCountOfUsers() {
        if (this.k != null) {
            return this.k.intValue();
        }
        if (this.j != null) {
            return this.j.size();
        }
        return 0;
    }

    public Integer getLimit() {
        return this.d;
    }

    public Range getRange() {
        return this.c;
    }

    public LoginSearchOperator getSearchOperator() {
        return this.i;
    }

    public boolean getSearchesGlobal() {
        return this.e;
    }

    public User getUserForIndex(int i2) {
        return (User) this.j.get(i2);
    }

    public List getUsers() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void h() {
        super.h();
        this.k = null;
        this.j = null;
    }

    public boolean isOverLimit() {
        return this.k != null;
    }

    public void loadRange() {
        D i2 = i();
        h();
        a(i2);
    }

    public void searchByEmail(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter anEmail cannot be null");
        }
        SearchSpec j2 = j();
        j2.a(new C0017p("email", C0003b.EXACT, str));
        D a2 = a("UserEMailSearch", j2);
        h();
        a(a2);
    }

    public void searchByLocalAddressBook(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Parameter aContext cannot be null");
        }
        SearchSpec j2 = j();
        AddressBook a2 = AddressBook.a();
        a2.a(AddressBook.HashAlgorithm.MD5);
        a2.a("shwu2831j78s");
        List a3 = a2.a(context);
        if (a3.size() == 0) {
            this.j = new ArrayList();
            a();
            return;
        }
        if (a3.size() == 1) {
            j2.a(new C0017p("email_digest", C0003b.EQUALS_ANY, (String) a3.get(0)));
        } else {
            j2.a(new C0017p("email_digest", C0003b.EQUALS_ANY, a3));
        }
        D a4 = a("UserAddressBookSearch", j2);
        h();
        a(a4);
    }

    public void searchByLogin(String str) {
        C0003b bVar;
        if (str == null) {
            throw new IllegalArgumentException("Parameter aLogin cannot be null");
        }
        SearchSpec j2 = j();
        if (this.i != null) {
            switch (C0019r.f63a[this.i.ordinal()]) {
                case R.styleable.com_google_ads_AdView_primaryTextColor:
                    bVar = C0003b.LIKE;
                    break;
                case R.styleable.com_google_ads_AdView_secondaryTextColor:
                    bVar = C0003b.EXACT;
                    break;
                default:
                    bVar = C0003b.BEGINS_WITH;
                    break;
            }
        } else {
            bVar = C0003b.BEGINS_WITH;
        }
        j2.a(new C0017p("login", bVar, str));
        D a2 = a("UserLoginSearch", j2);
        h();
        a(a2);
    }

    public void searchBySocialProvider(SocialProvider socialProvider) {
        if (socialProvider == null) {
            throw new IllegalArgumentException("Parameter aSocialProvider cannot be null");
        }
        SearchSpec j2 = j();
        j2.a(new C0017p("social_provider_id", C0003b.EXACT, socialProvider.getIdentifier()));
        D a2 = a("#user_social_provider_search", j2);
        a2.a(f());
        h();
        a(a2);
    }

    public void setLimit(Integer num) {
        this.d = num;
    }

    public void setRange(Range range) {
        this.c = range;
    }

    public void setSearchOperator(LoginSearchOperator loginSearchOperator) {
        if (loginSearchOperator == null) {
            throw new IllegalArgumentException("Parameter aSearchOperator cannot be null");
        }
        this.i = loginSearchOperator;
    }

    public void setSearchesGlobal(boolean z) {
        this.e = z;
    }
}
