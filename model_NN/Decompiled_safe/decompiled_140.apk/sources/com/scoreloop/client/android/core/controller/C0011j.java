package com.scoreloop.client.android.core.controller;

import com.kidsfun.game.ocean.R;
import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import java.nio.channels.IllegalSelectorException;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.j  reason: case insensitive filesystem */
class C0011j extends Request {

    /* renamed from: a  reason: collision with root package name */
    private final Device f58a;
    private final J b;

    public C0011j(RequestCompletionCallback requestCompletionCallback, Device device, J j) {
        super(requestCompletionCallback);
        this.f58a = device;
        this.b = j;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            switch (C0020s.f64a[this.b.ordinal()]) {
                case R.styleable.com_google_ads_AdView_primaryTextColor:
                    jSONObject.put("uuid", this.f58a.f());
                    jSONObject.put("system", this.f58a.b());
                    break;
                case R.styleable.com_google_ads_AdView_secondaryTextColor:
                    jSONObject.put("device", this.f58a.g());
                    break;
                case R.styleable.com_google_ads_AdView_keywords:
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("uuid", this.f58a.f());
                    jSONObject2.put("id", this.f58a.a());
                    jSONObject2.put("system", this.f58a.b());
                    jSONObject2.put("state", "freed");
                    jSONObject.put("device", jSONObject2);
                    break;
                case R.styleable.com_google_ads_AdView_refreshInterval:
                    jSONObject.put("device", this.f58a.g());
                default:
                    throw new IllegalSelectorException();
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid device data", e);
        }
    }

    public RequestMethod b() {
        switch (C0020s.f64a[this.b.ordinal()]) {
            case R.styleable.com_google_ads_AdView_primaryTextColor:
                return RequestMethod.GET;
            case R.styleable.com_google_ads_AdView_secondaryTextColor:
                return RequestMethod.POST;
            case R.styleable.com_google_ads_AdView_keywords:
            case R.styleable.com_google_ads_AdView_refreshInterval:
                return RequestMethod.PUT;
            default:
                throw new IllegalSelectorException();
        }
    }

    public String c() {
        return "/service/device";
    }

    public J d() {
        return this.b;
    }
}
