package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

class z extends Request {

    /* renamed from: a  reason: collision with root package name */
    private final Game f68a;
    private final Integer b;
    private final Integer c;
    private final Integer e;
    private final String f;
    private final User g;

    public z(RequestCompletionCallback requestCompletionCallback, Game game, User user, String str, Integer num, Integer num2, Integer num3) {
        super(requestCompletionCallback);
        this.f68a = game;
        this.g = user;
        this.f = str;
        this.b = num;
        this.e = num2;
        this.c = num3;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("search_list_id", this.f);
            if (this.g != null) {
                jSONObject.put("user_id", this.g.getIdentifier());
            }
            jSONObject.put("mode", this.b);
            jSONObject.put("offset", this.c);
            jSONObject.put("per_page", this.e);
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid challenge data", e2);
        }
    }

    public RequestMethod b() {
        return RequestMethod.GET;
    }

    public String c() {
        return String.format("/service/games/%s/challenges", this.f68a.getIdentifier());
    }
}
