package com.scoreloop.client.android.core.ui;

import android.app.Activity;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.utils.OAuthBuilder;
import java.net.URL;
import java.util.HashMap;

public class TwitterAuthViewController extends AuthViewController {

    /* renamed from: a  reason: collision with root package name */
    TwitterAuthDialog f114a;
    private String b;

    public TwitterAuthViewController(SocialProviderControllerObserver socialProviderControllerObserver) {
        super(socialProviderControllerObserver);
    }

    public void a(Activity activity) {
        OAuthBuilder oAuthBuilder = new OAuthBuilder();
        HashMap hashMap = new HashMap();
        hashMap.put("oauth_token", this.b);
        URL a2 = oAuthBuilder.a("http://twitter.com/oauth/authorize", hashMap);
        this.f114a = new TwitterAuthDialog(activity, 16973841, this);
        this.f114a.b();
        this.f114a.setCancelable(true);
        this.f114a.setCanceledOnTouchOutside(true);
        this.f114a.setOnDismissListener(new i(this));
        this.f114a.setOnCancelListener(new j(this));
        this.f114a.a(a2.toString());
        this.f114a.show();
    }

    public void a(String str) {
        this.b = str;
    }

    public String b() {
        return this.b;
    }
}
