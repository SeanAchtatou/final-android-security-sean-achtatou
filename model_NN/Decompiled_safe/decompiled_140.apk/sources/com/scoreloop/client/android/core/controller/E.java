package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Message;
import com.kidsfun.game.ocean.R;

class E extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private ChallengeController f35a;

    public E(ChallengeController challengeController) {
        this.f35a = challengeController;
    }

    public void handleMessage(Message message) {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) message.obj;
        switch (message.what) {
            case R.styleable.com_google_ads_AdView_primaryTextColor:
                challengeControllerObserver.onCannotAcceptChallenge(this.f35a);
                return;
            case R.styleable.com_google_ads_AdView_secondaryTextColor:
                challengeControllerObserver.onCannotRejectChallenge(this.f35a);
                return;
            case R.styleable.com_google_ads_AdView_keywords:
                challengeControllerObserver.onInsufficientBalance(this.f35a);
                return;
            default:
                return;
        }
    }
}
