package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.server.Request;

/* synthetic */ class M {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f38a = new int[Request.State.a().length];

    static {
        try {
            f38a[Request.State.COMPLETED.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f38a[Request.State.FAILED.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f38a[Request.State.CANCELLED.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
    }
}
