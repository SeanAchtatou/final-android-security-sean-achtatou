package com.scoreloop.client.android.core.model;

import org.json.JSONObject;

public class Payment {

    /* renamed from: a  reason: collision with root package name */
    private String f79a;
    private Money b;
    private State c;

    public enum State {
        CREATED,
        BOOKED,
        CANCELED,
        FAILED
    }

    public Payment(JSONObject jSONObject) {
        a(jSONObject);
    }

    public static String c() {
        return "payment";
    }

    public String a() {
        return this.f79a;
    }

    public void a(JSONObject jSONObject) {
        if (jSONObject.has("id")) {
            this.f79a = jSONObject.getString("id");
        }
        if (jSONObject.has("credit_money")) {
            this.b.a(jSONObject.getJSONObject("credit_money"));
        }
        if (jSONObject.has("state")) {
        }
    }

    public State b() {
        return this.c;
    }
}
