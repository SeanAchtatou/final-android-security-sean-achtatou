package com.scoreloop.client.android.core.controller;

import com.kidsfun.game.ocean.R;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.util.HashMap;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserController extends RequestController {
    private static final Pattern c = Pattern.compile(".*taken.*", 2);
    private User d;

    public UserController(UserControllerObserver userControllerObserver) {
        this(Session.getCurrentSession(), userControllerObserver);
    }

    public UserController(Session session, UserControllerObserver userControllerObserver) {
        super(session, userControllerObserver);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int f = response.f();
        RequestMethod b = request.b();
        JSONObject e = response.e();
        switch (f) {
            case 200:
            case 201:
                switch (C0013l.f60a[b.ordinal()]) {
                    case R.styleable.com_google_ads_AdView_primaryTextColor:
                    case R.styleable.com_google_ads_AdView_secondaryTextColor:
                    case R.styleable.com_google_ads_AdView_keywords:
                        if (!e.has("user") && !e.has("context")) {
                            throw new Exception("Request failed");
                        }
                    case R.styleable.com_google_ads_AdView_refreshInterval:
                        if (!e.has("buddy_id")) {
                            throw new Exception("Request failed");
                        }
                        break;
                }
                if (b != RequestMethod.DELETE) {
                    User user = getUser();
                    if (e.has("user")) {
                        user.a(e.getJSONObject("user"));
                    }
                    if (e.has("context")) {
                        user.setContext(JSONUtils.a(e.getJSONObject("context")));
                        user.e(e.getString("version"));
                    }
                }
                return true;
            case 409:
                User user2 = getUser();
                HashMap hashMap = new HashMap();
                hashMap.put("oldUserContext", new HashMap(user2.getContext()));
                user2.setContext(JSONUtils.a(e.getJSONObject("context")));
                user2.e(e.getString("version"));
                hashMap.put("newUserContext", new HashMap(user2.getContext()));
                throw new Exception("Request failed");
            case 422:
                if (b == RequestMethod.PUT || b == RequestMethod.POST) {
                    JSONObject jSONObject = e.getJSONObject("error");
                    JSONArray jSONArray = jSONObject.get("args") instanceof JSONArray ? jSONObject.getJSONArray("args") : null;
                    int length = jSONArray == null ? 0 : jSONArray.length();
                    boolean z = false;
                    boolean z2 = false;
                    for (int i = 0; i < length; i++) {
                        JSONArray jSONArray2 = jSONArray.getJSONArray(i);
                        String string = jSONArray2.getString(0);
                        String string2 = jSONArray2.getString(1);
                        if ("email".equalsIgnoreCase(string.trim())) {
                            if (c.matcher(string2).matches()) {
                                z = true;
                                z2 = true;
                            } else {
                                z = true;
                            }
                        }
                    }
                    if (!z) {
                        ((UserControllerObserver) c()).onUsernameAlreadyTaken(this);
                        return false;
                    } else if (z2) {
                        ((UserControllerObserver) c()).onEmailAlreadyTaken(this);
                        return false;
                    } else {
                        ((UserControllerObserver) c()).onEmailInvalidFormat(this);
                        return false;
                    }
                } else {
                    throw new Exception("Request failed");
                }
            default:
                throw new Exception("Request failed");
        }
    }

    public void addAsBuddy() {
        User user = getUser();
        User f = f();
        if (user.f()) {
            a(new Exception("User you're trying to add as buddy is a current session user"));
            return;
        }
        C0018q qVar = new C0018q(d(), user, f);
        h();
        a(qVar);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public User getUser() {
        if (this.d == null) {
            this.d = super.f();
            if (this.d == null) {
                throw new IllegalStateException("No session user assigned");
            }
        }
        return this.d;
    }

    public void removeAsBuddy() {
        L l = new L(d(), getUser(), f());
        h();
        a(l);
    }

    public void requestBuddies() {
        y yVar = new y(d(), b(), getUser());
        h();
        a(yVar);
    }

    public void requestUser() {
        I i = new I(d(), b(), getUser());
        h();
        a(i);
    }

    public void requestUserContext() {
        C0015n nVar = new C0015n(d(), getUser(), b());
        h();
        a(nVar);
    }

    public void requestUserDetail() {
        User user = getUser();
        User f = f();
        if (user.f()) {
            f = null;
        }
        K k = new K(d(), b(), user, f);
        h();
        a(k);
    }

    public void requestUserWithIdentifier(String str) {
        if (str == null) {
            throw new IllegalArgumentException("anIdentifier parameter cannot be null");
        }
        this.d = new User();
        this.d.d(str);
        requestUser();
    }

    public void setUser(User user) {
        this.d = user;
    }

    public void updateUser() {
        C0002a aVar = new C0002a(d(), b(), getUser());
        h();
        a(aVar);
    }

    public void updateUserContext() {
        F f = new F(d(), getUser(), b());
        h();
        a(f);
    }
}
