package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Session;

/* renamed from: com.scoreloop.client.android.core.controller.e  reason: case insensitive filesystem */
final class C0006e implements SocialProviderControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookSocialProviderController f54a;

    private C0006e(FacebookSocialProviderController facebookSocialProviderController) {
        this.f54a = facebookSocialProviderController;
    }

    public void didEnterInvalidCredentials() {
        this.f54a.e.a(false);
        this.f54a.d().didEnterInvalidCredentials();
    }

    public void didFail(Throwable th) {
        this.f54a.e.a(false);
        this.f54a.d().didFail(th);
    }

    public void didSucceed() {
        if (!this.f54a.c) {
            UserController unused = this.f54a.d = new UserController(Session.getCurrentSession(), new V(this.f54a));
            this.f54a.d.setUser(Session.getCurrentSession().getUser());
            this.f54a.d.requestUser();
        }
    }

    public void userDidCancel() {
        this.f54a.d().userDidCancel();
        this.f54a.e.a(false);
    }
}
