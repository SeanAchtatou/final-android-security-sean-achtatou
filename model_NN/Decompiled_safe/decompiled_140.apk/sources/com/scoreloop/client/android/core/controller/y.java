package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;

class y extends I {
    public y(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
        super(requestCompletionCallback, game, user);
    }

    public String c() {
        if (this.f36a == null || this.f36a.getIdentifier() == null) {
            return String.format("/service/users/%s/buddies", this.b.getIdentifier());
        }
        return String.format("/service/users/%s/buddies", this.b.getIdentifier());
    }
}
