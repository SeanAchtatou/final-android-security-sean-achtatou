package com.scoreloop.client.android.core.controller;

/* renamed from: com.scoreloop.client.android.core.controller.m  reason: case insensitive filesystem */
enum C0014m {
    StartPayment,
    SubmitPayment;

    public static C0014m[] a() {
        return (C0014m[]) c.clone();
    }
}
