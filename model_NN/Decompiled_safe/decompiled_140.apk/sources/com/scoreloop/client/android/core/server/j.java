package com.scoreloop.client.android.core.server;

import android.os.Handler;
import android.os.Message;
import com.kidsfun.game.ocean.R;
import com.scoreloop.client.android.core.server.Request;

class j extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Server f104a;

    private j(Server server) {
        this.f104a = server;
    }

    public void handleMessage(Message message) {
        Request a2 = this.f104a.f;
        Request unused = this.f104a.f = null;
        if (a2.l() != Request.State.CANCELLED) {
            switch (message.what) {
                case R.styleable.com_google_ads_AdView_primaryTextColor:
                    Response response = (Response) message.obj;
                    Integer b = response.b();
                    if (b != null && b.intValue() == a2.j()) {
                        a2.a(response);
                        break;
                    } else {
                        a2.a(new Exception("Invalid response ID, expected:" + a2.j() + ", but was:" + b));
                        break;
                    }
                case R.styleable.com_google_ads_AdView_secondaryTextColor:
                    a2.a((Exception) message.obj);
                    break;
                case R.styleable.com_google_ads_AdView_keywords:
                    a2.a((Exception) message.obj);
                    break;
                default:
                    throw new IllegalStateException("Unknown message type");
            }
            a2.g().a(a2);
        }
        this.f104a.d();
        if (this.f104a.f == null) {
            this.f104a.e();
        }
    }
}
