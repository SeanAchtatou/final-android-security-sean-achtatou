package com.scoreloop.client.android.core.server;

import org.json.JSONObject;

interface BayeuxConnectionObserver {

    public interface SecureBayeuxConnectionObserver extends BayeuxConnectionObserver {
    }

    void a(c cVar, Integer num, Object obj, String str, int i);

    void a(c cVar, JSONObject jSONObject);

    JSONObject b(c cVar, JSONObject jSONObject);
}
