package com.scoreloop.client.android.core.controller;

import android.os.Message;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import org.json.JSONObject;

public class ChallengeController extends RequestController {
    private Challenge c;
    private E d;

    public ChallengeController(ChallengeControllerObserver challengeControllerObserver) {
        this(Session.getCurrentSession(), challengeControllerObserver);
    }

    public ChallengeController(Session session, ChallengeControllerObserver challengeControllerObserver) {
        super(session, challengeControllerObserver);
        this.d = new E(this);
    }

    private void a(int i, RequestControllerObserver requestControllerObserver) {
        Message obtainMessage = this.d.obtainMessage(i);
        obtainMessage.obj = requestControllerObserver;
        obtainMessage.sendToTarget();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) this.f44a;
        int f = response.f();
        if (f == 200 || f == 201) {
            JSONObject jSONObject = response.e().getJSONObject("challenge");
            Challenge challenge = getChallenge();
            challenge.a(jSONObject);
            User f2 = f();
            if ((f2.equals(challenge.getContender()) && !challenge.isCreated()) || (f2.equals(challenge.getContestant()) && (challenge.isRejected() || challenge.isComplete()))) {
                e().setChallenge(null);
            }
            challengeControllerObserver.requestControllerDidReceiveResponse(this);
            return false;
        }
        Integer a2 = a(response.e());
        if (a2 == null) {
            throw new Exception("Request failed with status:" + f);
        }
        switch (a2.intValue()) {
            case 22:
            case 27:
                challengeControllerObserver.onCannotAcceptChallenge(this);
                return false;
            case 23:
            case 25:
            case 26:
            default:
                return false;
            case 24:
                challengeControllerObserver.onInsufficientBalance(this);
                return false;
        }
    }

    public void acceptChallenge() {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) this.f44a;
        if (e().getBalance().compareTo(getChallenge().getStake()) < 0) {
            a(3, challengeControllerObserver);
        } else if (!getChallenge().a(f())) {
            a(1, challengeControllerObserver);
        } else {
            getChallenge().a(f(), true);
            submitChallenge();
        }
    }

    public void createChallenge(Money money, User user) {
        if (money == null) {
            throw new IllegalArgumentException("aSomeMoney parameter cannot be null");
        }
        User f = f();
        if (f.a().compareTo(money) < 0) {
            throw new IllegalStateException("User's balance is not sufficient");
        } else if (f.equals(user)) {
            throw new IllegalStateException("User cannot challenge himself");
        } else {
            Challenge challenge = new Challenge(money);
            challenge.setContender(f);
            challenge.setContestant(user);
            this.c = challenge;
            e().setChallenge(challenge);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public Challenge getChallenge() {
        if (this.c == null) {
            this.c = e().getChallenge();
        }
        return this.c;
    }

    public void rejectChallenge() {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) this.f44a;
        if (!getChallenge().isAssigned() || !getChallenge().getContestant().equals(f())) {
            a(2, challengeControllerObserver);
            return;
        }
        getChallenge().a(f(), false);
        submitChallenge();
    }

    public void setChallenge(Challenge challenge) {
        this.c = challenge;
    }

    public void submitChallenge() {
        Challenge challenge = getChallenge();
        if (challenge == null) {
            throw new IllegalStateException("Set the challenge first");
        }
        Request vVar = challenge.getIdentifier() == null ? new C0023v(d(), e(), b(), challenge) : new H(d(), e(), b(), challenge);
        e().setChallenge(challenge);
        h();
        a(vVar);
    }

    public void submitChallengeScore(Score score) {
        Challenge challenge = getChallenge();
        if (score == null) {
            throw new IllegalArgumentException("aScore parameter can't be null");
        } else if (!f().equals(score.getUser())) {
            throw new IllegalStateException("User is not participating in the challenge");
        } else if (challenge == null || challenge.getMode() == score.getMode()) {
            score.a(f());
            challenge.a(score);
            submitChallenge();
        } else {
            throw new IllegalStateException("Score mode does not match challenge mode");
        }
    }
}
