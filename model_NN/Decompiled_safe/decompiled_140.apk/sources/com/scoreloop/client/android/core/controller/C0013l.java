package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.server.RequestMethod;

/* renamed from: com.scoreloop.client.android.core.controller.l  reason: case insensitive filesystem */
/* synthetic */ class C0013l {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f60a = new int[RequestMethod.a().length];

    static {
        try {
            f60a[RequestMethod.PUT.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f60a[RequestMethod.POST.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f60a[RequestMethod.GET.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f60a[RequestMethod.DELETE.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
    }
}
