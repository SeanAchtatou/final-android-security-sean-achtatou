package com.xieory.XFDiamondP;

import android.app.Activity;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class fullScreenAct extends Activity {
    boolean TouchMove = false;
    boolean drawGlow = false;
    float glowX = 0.0f;
    float glowY = 0.0f;
    /* access modifiers changed from: private */
    public ImageView imgv;
    private Integer[] mImageIds = {Integer.valueOf((int) R.drawable.img1), Integer.valueOf((int) R.drawable.img2), Integer.valueOf((int) R.drawable.img3), Integer.valueOf((int) R.drawable.img4), Integer.valueOf((int) R.drawable.img5), Integer.valueOf((int) R.drawable.img6), Integer.valueOf((int) R.drawable.img7), Integer.valueOf((int) R.drawable.img8), Integer.valueOf((int) R.drawable.img9), Integer.valueOf((int) R.drawable.img10), Integer.valueOf((int) R.drawable.img11), Integer.valueOf((int) R.drawable.img12), Integer.valueOf((int) R.drawable.img13), Integer.valueOf((int) R.drawable.img14), Integer.valueOf((int) R.drawable.img15), Integer.valueOf((int) R.drawable.img16), Integer.valueOf((int) R.drawable.img17), Integer.valueOf((int) R.drawable.img18), Integer.valueOf((int) R.drawable.img19), Integer.valueOf((int) R.drawable.img20), Integer.valueOf((int) R.drawable.img21), Integer.valueOf((int) R.drawable.img22), Integer.valueOf((int) R.drawable.img23), Integer.valueOf((int) R.drawable.img24), Integer.valueOf((int) R.drawable.img25), Integer.valueOf((int) R.drawable.img26), Integer.valueOf((int) R.drawable.img27), Integer.valueOf((int) R.drawable.img28), Integer.valueOf((int) R.drawable.img29), Integer.valueOf((int) R.drawable.img30), Integer.valueOf((int) R.drawable.img31), Integer.valueOf((int) R.drawable.img32), Integer.valueOf((int) R.drawable.img33), Integer.valueOf((int) R.drawable.img34), Integer.valueOf((int) R.drawable.img35), Integer.valueOf((int) R.drawable.img36), Integer.valueOf((int) R.drawable.img37), Integer.valueOf((int) R.drawable.img38), Integer.valueOf((int) R.drawable.img39), Integer.valueOf((int) R.drawable.img40), Integer.valueOf((int) R.drawable.img41), Integer.valueOf((int) R.drawable.img42), Integer.valueOf((int) R.drawable.img43), Integer.valueOf((int) R.drawable.img44), Integer.valueOf((int) R.drawable.img45), Integer.valueOf((int) R.drawable.img46), Integer.valueOf((int) R.drawable.img47), Integer.valueOf((int) R.drawable.img48), Integer.valueOf((int) R.drawable.img49), Integer.valueOf((int) R.drawable.img50), Integer.valueOf((int) R.drawable.img51), Integer.valueOf((int) R.drawable.img52), Integer.valueOf((int) R.drawable.img53), Integer.valueOf((int) R.drawable.img54), Integer.valueOf((int) R.drawable.img55), Integer.valueOf((int) R.drawable.img56), Integer.valueOf((int) R.drawable.img57), Integer.valueOf((int) R.drawable.img58), Integer.valueOf((int) R.drawable.img59), Integer.valueOf((int) R.drawable.img60)};
    /* access modifiers changed from: private */
    public int moveSx = 0;
    /* access modifiers changed from: private */
    public int moveSy = 0;
    private String sCurrentImg = "1";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Bundle();
        Bundle bundle = getIntent().getExtras();
        if (!(bundle == null || bundle.getString("CURRENTIMG") == null)) {
            this.sCurrentImg = bundle.getString("CURRENTIMG");
        }
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        this.imgv = new ImageView(this);
        this.imgv.setScaleType(ImageView.ScaleType.MATRIX);
        Matrix mat = new Matrix();
        mat.setScale(1.0f, 1.0f);
        this.imgv.setImageMatrix(mat);
        this.imgv.setImageResource(this.mImageIds[Integer.valueOf(this.sCurrentImg).intValue() - 1].intValue());
        this.imgv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.imgv.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    Log.i("event.getAction()", "MotionEvent.ACTION_DOWN)");
                    fullScreenAct.this.TouchMove = false;
                } else if (event.getAction() == 1) {
                    Log.i("event.getAction()", "MotionEvent.ACTION_UP)");
                    if (!fullScreenAct.this.TouchMove) {
                        fullScreenAct.this.finish();
                    } else {
                        fullScreenAct.this.TouchMove = false;
                    }
                } else if (event.getAction() == 2) {
                    Log.i("event.getAction()", "MotionEvent.ACTION_MOVE)");
                    fullScreenAct.this.TouchMove = true;
                    fullScreenAct.this.imgv.setScaleType(ImageView.ScaleType.MATRIX);
                    Matrix mat = new Matrix();
                    mat.setScale(1.0f, 1.0f);
                    float tmpfloat = event.getX() - fullScreenAct.this.glowX;
                    fullScreenAct fullscreenact = fullScreenAct.this;
                    fullscreenact.moveSx = fullscreenact.moveSx + ((int) tmpfloat);
                    if (fullScreenAct.this.moveSx < -320 || fullScreenAct.this.moveSx > 0) {
                        fullScreenAct fullscreenact2 = fullScreenAct.this;
                        fullscreenact2.moveSx = fullscreenact2.moveSx - ((int) tmpfloat);
                    }
                    mat.postTranslate((float) fullScreenAct.this.moveSx, (float) fullScreenAct.this.moveSy);
                    fullScreenAct.this.imgv.setImageMatrix(mat);
                }
                fullScreenAct.this.glowX = event.getX();
                fullScreenAct.this.glowY = event.getY();
                return true;
            }
        });
        setContentView(this.imgv);
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            System.out.print("event.getAction() == MotionEvent.ACTION_DOWN");
            this.drawGlow = true;
        } else if (event.getAction() == 1) {
            System.out.print("event.getAction() == MotionEvent.ACTION_UP");
            this.drawGlow = false;
        } else if (event.getAction() == 2) {
            System.out.print("event.getAction() == MotionEvent.ACTION_MOVE");
            this.imgv.setScaleType(ImageView.ScaleType.MATRIX);
            Matrix mat = new Matrix();
            mat.setScale(1.0f, 1.0f);
            this.moveSx -= 160;
            mat.postTranslate((float) this.moveSx, (float) this.moveSy);
            this.imgv.setImageMatrix(mat);
        }
        return true;
    }
}
