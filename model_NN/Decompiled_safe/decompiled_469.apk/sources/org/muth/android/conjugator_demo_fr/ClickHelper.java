package org.muth.android.conjugator_demo_fr;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import java.util.logging.Logger;
import org.muth.android.base.UpdateHelper;

public class ClickHelper {
    private static Logger logger = Logger.getLogger("conj");

    public static void handleMenu(Activity activity, String str) {
        if (str != null && str.startsWith("@")) {
            String[] split = str.split("@");
            logger.info("handle " + split.length + " " + str);
            if (split[1].equals("url")) {
                logger.info("url " + split.length + split[2]);
                UpdateHelper.launchUrlApp(activity, split[2]);
            } else if (split[1].equals("act")) {
                if (split.length < 4) {
                    logger.info("act " + split.length + " " + split[2]);
                } else {
                    logger.info("act " + split.length + " " + split[2] + " " + split[3]);
                }
                String packageName = activity.getPackageName();
                Intent intent = new Intent();
                if (split.length == 4) {
                    intent.setData(Uri.parse(split[3]));
                } else {
                    intent.setType("");
                }
                intent.setClassName(packageName, packageName + "." + split[2]);
                try {
                    activity.startActivity(intent);
                } catch (Exception e) {
                    logger.info("error " + e.toString());
                }
            } else {
                logger.info("unhandled " + split.length + " " + str);
            }
        }
    }
}
