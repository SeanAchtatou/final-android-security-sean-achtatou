package org.muth.android.conjugator_demo_fr;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import org.muth.android.base.Tracker;
import org.muth.android.conjugator_demo_pt.R;

public class ActivitySearchPrefs extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Tracker.TrackPage(this);
        setPreferenceScreen(createPreferenceHierarchy());
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen createPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
        CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
        checkBoxPreference.setKey("Search:Misc:AlternativeSpelling");
        checkBoxPreference.setTitle((int) R.string.pref_ignore_accents);
        createPreferenceScreen.addPreference(checkBoxPreference);
        CheckBoxPreference checkBoxPreference2 = new CheckBoxPreference(this);
        checkBoxPreference2.setKey("Search:Misc:UsePronouns");
        checkBoxPreference2.setTitle((int) R.string.pref_show_pronouns);
        createPreferenceScreen.addPreference(checkBoxPreference2);
        CheckBoxPreference checkBoxPreference3 = new CheckBoxPreference(this);
        checkBoxPreference3.setKey("Search:Misc:SearchTranslation");
        checkBoxPreference3.setTitle((int) R.string.pref_search_translations);
        createPreferenceScreen.addPreference(checkBoxPreference3);
        return createPreferenceScreen;
    }
}
