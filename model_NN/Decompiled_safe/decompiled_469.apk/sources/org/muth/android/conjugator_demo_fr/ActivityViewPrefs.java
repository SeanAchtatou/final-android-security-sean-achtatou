package org.muth.android.conjugator_demo_fr;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import java.util.Iterator;
import org.muth.android.base.ListPreferenceWithSummary;
import org.muth.android.base.Tracker;
import org.muth.android.base.UpdateHelper;
import org.muth.android.conjugator_demo_pt.R;
import org.muth.android.verbs.VerbDatabase;
import org.muth.android.verbs.VerbRenderer;

public class ActivityViewPrefs extends PreferenceActivity {
    private VerbRenderer mRenderer;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mRenderer = VerbRenderer.GetSingleton(VerbDatabase.GetSingleton(getResources().openRawResource(R.raw.str), getResources().openRawResource(R.raw.vf), UpdateHelper.getPhoneLanguage(this), null), getString(R.string.language));
        Tracker.TrackPage(this);
        setPreferenceScreen(createPreferenceHierarchy());
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen createPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
        CheckBoxPreference checkBoxPreference = new CheckBoxPreference(this);
        checkBoxPreference.setKey("View:Misc:UsePronouns");
        checkBoxPreference.setTitle((int) R.string.pref_show_pronouns);
        createPreferenceScreen.addPreference(checkBoxPreference);
        CheckBoxPreference checkBoxPreference2 = new CheckBoxPreference(this);
        checkBoxPreference2.setKey("View:Misc:ShowTitle");
        checkBoxPreference2.setTitle((int) R.string.pref_show_title);
        createPreferenceScreen.addPreference(checkBoxPreference2);
        CheckBoxPreference checkBoxPreference3 = new CheckBoxPreference(this);
        checkBoxPreference3.setKey("View:Misc:ShowTranslation");
        checkBoxPreference3.setTitle((int) R.string.pref_show_translation);
        createPreferenceScreen.addPreference(checkBoxPreference3);
        CheckBoxPreference checkBoxPreference4 = new CheckBoxPreference(this);
        checkBoxPreference4.setKey("View:Misc:UseTTS");
        checkBoxPreference4.setTitle((int) R.string.pref_use_tts);
        createPreferenceScreen.addPreference(checkBoxPreference4);
        CheckBoxPreference checkBoxPreference5 = new CheckBoxPreference(this);
        checkBoxPreference5.setKey("View:Misc:RememberScale");
        checkBoxPreference5.setTitle((int) R.string.pref_remember_zoom);
        createPreferenceScreen.addPreference(checkBoxPreference5);
        String[] strArr = {getString(R.string.pref_background_dark), getString(R.string.pref_background_lite)};
        ListPreferenceWithSummary listPreferenceWithSummary = new ListPreferenceWithSummary(this, "View:Misc:Background", strArr, new String[]{"dark", "lite"});
        listPreferenceWithSummary.setDialogTitle((int) R.string.pref_background);
        listPreferenceWithSummary.setTitle((int) R.string.pref_background);
        createPreferenceScreen.addPreference(listPreferenceWithSummary);
        String[] strArr2 = {"1", "2", "3", "4", "5", "6", "7", "8"};
        ListPreferenceWithSummary listPreferenceWithSummary2 = new ListPreferenceWithSummary(this, "View:Misc:DispColumns", strArr2, strArr2);
        listPreferenceWithSummary2.setDialogTitle((int) R.string.pref_columns);
        listPreferenceWithSummary2.setTitle((int) R.string.pref_columns);
        createPreferenceScreen.addPreference(listPreferenceWithSummary2);
        PreferenceCategory preferenceCategory = new PreferenceCategory(this);
        preferenceCategory.setTitle((int) R.string.pref_tense_selection);
        createPreferenceScreen.addPreference(preferenceCategory);
        Iterator<String> it = this.mRenderer.getTenseList().iterator();
        while (it.hasNext()) {
            String next = it.next();
            CheckBoxPreference checkBoxPreference6 = new CheckBoxPreference(this);
            checkBoxPreference6.setKey("View:Tenses:" + next);
            checkBoxPreference6.setTitle(this.mRenderer.nameTenseLong(next));
            createPreferenceScreen.addPreference(checkBoxPreference6);
        }
        return createPreferenceScreen;
    }
}
