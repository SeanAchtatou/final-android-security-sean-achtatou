package org.muth.android.base;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.logging.Logger;

public class FileCache {
    private static Logger logger = Logger.getLogger("base");
    private static HashMap<String, Object> mCache = new HashMap<>(50);

    public static void WriteToFile(String str, Serializable serializable, boolean z, int i, boolean z2) throws IOException {
        FileIO.WriteToFile(str, serializable, z, i, z2);
        mCache.put(str, serializable);
    }

    public static boolean Delete(String str) {
        if (mCache.containsKey(str)) {
            mCache.remove(str);
        }
        return new File(str).delete();
    }

    public static Object ReadFromFile(String str, boolean z, boolean z2) throws IOException, ClassNotFoundException {
        if (!mCache.containsKey(str)) {
            mCache.put(str, FileIO.ReadFromFile(str, z, Boolean.valueOf(z2)));
        }
        return mCache.get(str);
    }
}
