package org.muth.android.base;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Comparator;
import java.util.Random;

public class FlashCardItem implements Serializable {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static short[] INITIAL_INTERVAL = {0, 0, 1, 3, 4, 5};
    public static final short INTERVAL_NEXT_DAY = 1;
    public static final short INTERVAL_SAME_DAY = 0;
    public static final byte MAGIC_GRADE_FIRST_AQUIRE = 101;
    public static final byte MAGIC_GRADE_FIRST_LEARN_AHEAD = 102;
    public static final byte MAGIC_GRADE_FIRST_RETENTION = 100;
    public static final byte TYPE_ACQ = 1;
    public static final byte TYPE_RET = 2;
    public static final byte TYPE_SPECIAL = 3;
    public static final byte TYPE_UNSEEN = 0;
    private static final long serialVersionUID = 3;
    private short mAcquireReps;
    private short mAcquireRepsSinceLapse;
    private short mEasiness;
    private byte mGrade;
    private byte[] mId;
    private short mLastRep;
    /* access modifiers changed from: private */
    public short mNextRep;
    private short mRetentionReps;
    private short mRetentionRepsSinceLapse;

    static {
        boolean z;
        if (!FlashCardItem.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = $assertionsDisabled;
        }
        $assertionsDisabled = z;
    }

    private static short CalculateInitialInterval(short s) {
        return INITIAL_INTERVAL[s];
    }

    private static short RecalculateEasiness(short s, short s2) {
        short s3;
        if (s2 == 2) {
            s3 = (short) (s - 16);
        } else if (s2 == 3) {
            s3 = (short) (s - 14);
        } else {
            s3 = s2 == 5 ? (short) (s + 10) : s;
        }
        if (s3 < 130) {
            s3 = 130;
        }
        if (s3 > 1000) {
            return 1000;
        }
        return s3;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeShort(this.mId.length);
        objectOutputStream.write(this.mId);
        objectOutputStream.writeByte(this.mGrade);
        objectOutputStream.writeShort(this.mEasiness);
        objectOutputStream.writeByte(this.mAcquireReps);
        objectOutputStream.writeByte(this.mRetentionReps);
        objectOutputStream.writeByte(this.mAcquireRepsSinceLapse);
        objectOutputStream.writeByte(this.mRetentionRepsSinceLapse);
        objectOutputStream.writeShort(this.mLastRep);
        objectOutputStream.writeShort(this.mNextRep);
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        this.mId = new byte[objectInputStream.readShort()];
        objectInputStream.read(this.mId);
        this.mGrade = objectInputStream.readByte();
        this.mEasiness = objectInputStream.readShort();
        this.mAcquireReps = (short) objectInputStream.readByte();
        this.mRetentionReps = (short) objectInputStream.readByte();
        this.mAcquireRepsSinceLapse = (short) objectInputStream.readByte();
        this.mRetentionRepsSinceLapse = (short) objectInputStream.readByte();
        this.mLastRep = objectInputStream.readShort();
        this.mNextRep = objectInputStream.readShort();
    }

    public FlashCardItem(byte[] bArr, byte b, short s, byte b2, byte b3, byte b4, byte b5, short s2, short s3) {
        this.mId = bArr;
        this.mGrade = b;
        this.mEasiness = s;
        this.mAcquireReps = (short) b2;
        this.mRetentionReps = (short) b3;
        this.mAcquireRepsSinceLapse = (short) b4;
        this.mRetentionRepsSinceLapse = (short) b5;
        this.mLastRep = s2;
        this.mNextRep = s3;
    }

    public FlashCardItem(byte[] bArr) {
        this.mId = bArr;
        this.mGrade = -1;
        this.mEasiness = 250;
        this.mAcquireReps = 0;
        this.mRetentionReps = 0;
        this.mAcquireRepsSinceLapse = 0;
        this.mRetentionRepsSinceLapse = 0;
        this.mLastRep = -1;
        this.mNextRep = -1;
    }

    public static FlashCardItem MakeSpecial(byte b, short s) {
        return new FlashCardItem(new byte[0], b, s, (byte) 0, (byte) 0, (byte) 0, (byte) 0, -1, -1);
    }

    public static FlashCardItem MakeFromString(String str) throws UnsupportedEncodingException {
        String[] split = str.split(",", 9);
        if ($assertionsDisabled || split.length == 9) {
            return new FlashCardItem(split[8].getBytes("UTF-8"), Byte.parseByte(split[0]), Short.parseShort(split[1]), Byte.parseByte(split[2]), Byte.parseByte(split[3]), Byte.parseByte(split[4]), Byte.parseByte(split[5]), Short.parseShort(split[6]), Short.parseShort(split[7]));
        }
        throw new AssertionError();
    }

    public String ToString() throws UnsupportedEncodingException {
        return ((int) this.mGrade) + "," + ((int) this.mEasiness) + "," + ((int) this.mAcquireReps) + "," + ((int) this.mRetentionReps) + "," + ((int) this.mAcquireRepsSinceLapse) + "," + ((int) this.mRetentionRepsSinceLapse) + "," + ((int) this.mLastRep) + "," + ((int) this.mNextRep) + "," + new String(this.mId, "UTF-8");
    }

    public String InfoString(short s) {
        return "now: " + ((int) s) + "\n" + "Grade: " + ((int) this.mGrade) + "\n" + "Easiness: " + ((int) this.mEasiness) + "\n" + "AcquireReps: " + ((int) this.mAcquireReps) + "\n" + "RetentionReps: " + ((int) this.mRetentionReps) + "\n" + "AcquireRepsSinceLapse: " + ((int) this.mAcquireRepsSinceLapse) + "\n" + "RetentionRepsSinceLapse: " + ((int) this.mRetentionRepsSinceLapse) + "\n" + "LastRep: " + ((int) this.mLastRep) + "\n" + "NextRep: " + ((int) this.mNextRep) + "\n";
    }

    private short CalculateIntervalNoise(Random random, short s) {
        int nextInt;
        if (s == 0) {
            nextInt = 0;
        } else if (s == 1) {
            nextInt = random.nextInt(2);
        } else if (s <= 10) {
            nextInt = random.nextInt(3) - 1;
        } else if (s <= 60) {
            nextInt = random.nextInt(7) - 3;
        } else {
            int i = s / 20;
            nextInt = random.nextInt((i * 2) + 1) - i;
        }
        return (short) nextInt;
    }

    public byte[] Id() {
        return this.mId;
    }

    public byte Grade() {
        return this.mGrade;
    }

    public short NextRep() {
        return this.mNextRep;
    }

    public short LastRep() {
        return this.mLastRep;
    }

    public boolean IsSpecial() {
        if (Grade() >= 100) {
            return true;
        }
        return $assertionsDisabled;
    }

    public short GetPayload() {
        return this.mEasiness;
    }

    public short Easiness() {
        return this.mEasiness;
    }

    public short Type() {
        if (this.mGrade < 0) {
            return 0;
        }
        if (this.mGrade <= 1) {
            return 1;
        }
        if (this.mGrade <= 5) {
            return 2;
        }
        return 3;
    }

    public short Interval() {
        return (short) (this.mNextRep - this.mLastRep);
    }

    public int RepInterval() {
        return this.mNextRep - this.mLastRep;
    }

    public static Comparator<FlashCardItem> CompShortestInterval() {
        return new Comparator<FlashCardItem>() {
            public int compare(FlashCardItem flashCardItem, FlashCardItem flashCardItem2) {
                return flashCardItem.RepInterval() - flashCardItem2.RepInterval();
            }
        };
    }

    public static Comparator<FlashCardItem> CompEarliestRep() {
        return new Comparator<FlashCardItem>() {
            public int compare(FlashCardItem flashCardItem, FlashCardItem flashCardItem2) {
                return flashCardItem.mNextRep - flashCardItem2.mNextRep;
            }
        };
    }

    public short TotalReps() {
        return (short) (this.mAcquireReps + this.mRetentionReps);
    }

    public short RentionReps() {
        return this.mRetentionReps;
    }

    public void RegisterAcquisitionRepetition() {
        if (this.mAcquireReps < 127) {
            this.mAcquireReps = (short) (this.mAcquireReps + 1);
        }
        if (this.mAcquireRepsSinceLapse < 127) {
            this.mAcquireRepsSinceLapse = (short) (this.mAcquireRepsSinceLapse + 1);
        }
    }

    public void RegisterRetentionRepetition(short s) {
        if (this.mRetentionReps < 127) {
            this.mRetentionReps = (short) (this.mRetentionReps + 1);
        }
        if (s < 1) {
            this.mAcquireRepsSinceLapse = 0;
            this.mRetentionRepsSinceLapse = 0;
        } else if (this.mRetentionRepsSinceLapse < 127) {
            this.mRetentionRepsSinceLapse = (short) (this.mRetentionRepsSinceLapse + 1);
        }
    }

    private short CalculateIntervalRetention(short s, short s2, int i) {
        if (!$assertionsDisabled && (2 > s2 || s2 > 5)) {
            throw new AssertionError();
        } else if (this.mRetentionRepsSinceLapse == 1) {
            return (short) (600 / i);
        } else {
            short Interval = Interval();
            if (s < this.mNextRep) {
                return Interval;
            }
            int i2 = (Interval * this.mEasiness) / i;
            if (i2 < 1) {
                i2 = 1;
            }
            return (short) i2;
        }
    }

    public int Update(short s, byte b, Random random, int i) {
        if ($assertionsDisabled || !IsSpecial()) {
            short Grade = (short) Grade();
            if (Grade <= 1) {
                RegisterAcquisitionRepetition();
            } else {
                RegisterRetentionRepetition((short) b);
            }
            if (b <= 1) {
                this.mNextRep = -1;
                this.mGrade = b;
                this.mLastRep = s;
            } else if (Grade < 0) {
                this.mNextRep = (short) (CalculateInitialInterval((short) b) + s);
                this.mGrade = b;
                this.mLastRep = s;
            } else if (Grade <= 1) {
                this.mNextRep = (short) (s + 1);
                this.mGrade = b;
                this.mLastRep = s;
            } else {
                short s2 = 0;
                if (s >= this.mNextRep) {
                    this.mEasiness = RecalculateEasiness(this.mEasiness, (short) b);
                }
                short CalculateIntervalRetention = CalculateIntervalRetention(s, (short) b, i);
                if (random != null) {
                    s2 = CalculateIntervalNoise(random, CalculateIntervalRetention);
                }
                this.mNextRep = (short) (s2 + CalculateIntervalRetention + s);
                this.mGrade = b;
                this.mLastRep = s;
            }
            return Grade;
        }
        throw new AssertionError();
    }

    public int Update(short s, byte b, Random random) {
        return Update(s, b, random, 100);
    }
}
