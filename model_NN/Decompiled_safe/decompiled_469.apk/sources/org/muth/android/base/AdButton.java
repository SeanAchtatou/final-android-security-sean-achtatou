package org.muth.android.base;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import java.util.Random;

public class AdButton {
    private Button mAd;
    /* access modifiers changed from: private */
    public String mAdBanner;
    /* access modifiers changed from: private */
    public String mAdColor;
    private Random mGenerator = new Random();

    public AdButton(final Activity activity, Button button, final String str) {
        this.mAd = button;
        this.mAd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Tracker.TrackPage("/Ad");
                Tracker.TrackEvent(str, "AdBanner", AdButton.this.mAdBanner, 0);
                Tracker.TrackEvent(str, "AdColor", AdButton.this.mAdColor, 0);
                UpdateHelper.launchUrlApp(activity, (String) view.getTag());
            }
        });
    }

    public AdButton(final Activity activity, final String str) {
        this.mAd = new Button(activity);
        this.mAd.setTypeface(Typeface.DEFAULT_BOLD);
        this.mAd.setTextSize(0, this.mAd.getTextSize() * 1.1f);
        this.mAd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Tracker.TrackPage("/Ad");
                Tracker.TrackEvent(str, "AdBanner", AdButton.this.mAdBanner, 0);
                Tracker.TrackEvent(str, "AdColor", AdButton.this.mAdColor, 0);
                UpdateHelper.launchUrlApp(activity, (String) view.getTag());
            }
        });
    }

    public void Update(PreferenceHelper preferenceHelper) {
        int i;
        String[] split = preferenceHelper.getStringPreference("Ad:Misc:Message").split("@");
        String[] split2 = preferenceHelper.getStringPreference("Ad:Misc:Url").split("@");
        String[] split3 = preferenceHelper.getStringPreference("Ad:Misc:Color").split("@");
        int nextInt = this.mGenerator.nextInt(split.length);
        this.mAdBanner = split[nextInt];
        this.mAd.setText(this.mAdBanner);
        if (nextInt >= split2.length) {
            i = 0;
        } else {
            i = nextInt;
        }
        this.mAd.setTag(split2[i]);
        this.mAdColor = split3[this.mGenerator.nextInt(split3.length)];
        this.mAd.setBackgroundColor(-16777216 + Integer.parseInt(this.mAdColor, 16));
    }

    public Button GetButton() {
        return this.mAd;
    }
}
