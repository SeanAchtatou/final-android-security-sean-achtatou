package org.muth.android.base;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import java.util.logging.Logger;

public class BaseActivityAbout {
    private static Logger logger = Logger.getLogger("base");

    protected static String getPhoneInfo() {
        return Build.MANUFACTURER + "," + Build.MODEL + "," + Build.PRODUCT + "," + Build.DEVICE + "," + Build.BRAND;
    }

    protected static String getOSInfo() {
        return Build.DISPLAY + "," + Build.USER + "," + Build.TAGS;
    }

    protected static String getOSVersionInfo() {
        return Build.VERSION.CODENAME + "," + Build.ID + "," + Build.VERSION.INCREMENTAL + "," + Build.VERSION.RELEASE + "," + Build.VERSION.SDK_INT;
    }

    protected static String getTTSInfo(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        return Settings.Secure.getString(contentResolver, "tts_default_synth") + "," + Settings.Secure.getString(contentResolver, "tts_default_lang") + "," + Settings.Secure.getString(contentResolver, "tts_use_defaults");
    }

    public static String GetStandardInfo(PreferenceHelper preferenceHelper, Context context) {
        String str = ((((((((("" + "This version: " + UpdateHelper.appVersionCode(context) + " (" + UpdateHelper.appVersionName(context) + ") " + "\n") + "Latest version: " + preferenceHelper.getStringPreference("Debug:Misc:LatestVersion") + "\n") + "Package: " + context.getPackageName() + "\n") + "Locale: " + UpdateHelper.getPhoneCountry(context) + " " + UpdateHelper.getPhoneLanguage(context) + "\n") + "Memory: " + (Runtime.getRuntime().totalMemory() / 1024) + "k\n") + "Phone: " + getPhoneInfo() + "\n") + "CPU: " + Build.BOARD + "," + Build.CPU_ABI + "\n") + "OS1: " + getOSInfo() + "\n") + "OS2: " + getOSVersionInfo() + "\n") + "TTS: " + getTTSInfo(context) + "\n";
        if (UpdateHelper.appIsDebuggable(context)) {
            str = str + "\n\nWARNING: DEBUG VERSION";
        }
        return str + "\n";
    }

    public static String MakeEmailUrl(PreferenceHelper preferenceHelper, Context context, String str) {
        try {
            return preferenceHelper.getStringPreference("Debug:Misc:UrlEmail") + "?subject=" + Uri.encode(str, "UTF-8") + "&body=" + Uri.encode("YOUR MESSAGE HERE\n\n\nDO NOT DELETE INFO BELOW:\n\n" + GetStandardInfo(preferenceHelper, context), "UTF-8");
        } catch (Exception e) {
            return preferenceHelper.getStringPreference("Debug:Misc:UrlEmail");
        }
    }

    public static String MakeTellUrl(Context context, String str) {
        return "mailto:?subject=" + Uri.encode(str, "UTF-8") + "&body=" + Uri.encode("Found this great language app for my android phone\nlink: " + ("https://market.android.com/details?id=" + context.getPackageName()));
    }
}
