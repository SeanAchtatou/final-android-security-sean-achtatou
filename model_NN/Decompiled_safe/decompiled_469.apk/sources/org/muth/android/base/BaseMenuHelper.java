package org.muth.android.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import java.util.Vector;
import java.util.logging.Logger;

public class BaseMenuHelper {
    /* access modifiers changed from: private */
    public static Logger logger = Logger.getLogger("base");
    private Vector<MenuAction> mActions = new Vector<>();
    protected Activity mActivity;

    public static class MenuAction {
        public boolean mFinish;
        public int mIcon;
        public char mShortcut;
        public char mShortcut2;
        public int mTitle;

        public MenuAction(int i, char c, char c2, int i2, boolean z) {
            this.mShortcut = c;
            this.mShortcut2 = c2;
            this.mTitle = i;
            this.mIcon = i2;
            this.mFinish = z;
        }

        public void setFinish(boolean z) {
            this.mFinish = z;
        }

        public void setIcon(int i) {
            this.mIcon = i;
        }

        public void Invoke(Activity activity) {
        }
    }

    public static class MenuActionIntend extends MenuAction {
        private String mClass;
        private String mIntend;

        public MenuActionIntend(int i, char c, char c2, String str, String str2, int i2) {
            super(i, c, c2, i2, false);
            this.mClass = str;
            this.mIntend = str2;
        }

        public void Invoke(Activity activity) {
            BaseMenuHelper.logger.warning("menu intend " + this.mClass);
            String packageName = activity.getPackageName();
            try {
                activity.startActivity(new Intent().setType(this.mIntend).setClassName(packageName, packageName + "." + this.mClass));
            } catch (Exception e) {
                BaseMenuHelper.logger.severe("error starting intend: " + e.toString());
            }
            if (this.mFinish) {
                activity.finish();
            }
        }
    }

    public static class MenuActionDialog extends MenuAction {
        private String mButton;
        private String mContent;
        /* access modifiers changed from: private */
        public boolean mFinish;
        private boolean mHtml;
        private String mTitle;

        public MenuActionDialog(int i, char c, char c2, String str, String str2, String str3, boolean z, int i2) {
            super(i, c, c2, i2, false);
            this.mButton = str2;
            this.mContent = str3;
            this.mHtml = z;
            this.mTitle = str;
        }

        public String GetContents() {
            return this.mContent;
        }

        public String GetTitle() {
            return this.mTitle;
        }

        public void Invoke(final Activity activity) {
            BaseMenuHelper.logger.warning("menu dialog ");
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            String GetTitle = GetTitle();
            if (GetTitle != null) {
                builder.setTitle(GetTitle);
            }
            if (this.mHtml) {
                WebView webView = new WebView(activity);
                BaseMenuHelper.logger.warning("menu dialog " + Integer.toHexString(17170445));
                webView.setBackgroundColor(0);
                webView.loadDataWithBaseURL("local://local/", "<body text=white>" + GetContents(), "text/html", "utf-8", null);
                builder.setView(webView);
            } else {
                builder.setMessage(GetContents());
            }
            AnonymousClass1 r0 = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (MenuActionDialog.this.mFinish) {
                        activity.finish();
                    }
                }
            };
            builder.setCancelable(true);
            builder.setPositiveButton(this.mButton, r0);
            builder.show();
        }
    }

    public BaseMenuHelper(Activity activity) {
        this.mActivity = activity;
    }

    public void Register(Menu menu) {
        for (int i = 0; i < this.mActions.size(); i++) {
            MenuAction elementAt = this.mActions.elementAt(i);
            MenuItem add = menu.add(0, i, 0, elementAt.mTitle);
            add.setShortcut(elementAt.mShortcut, elementAt.mShortcut2);
            if (elementAt.mIcon != 0) {
                add.setIcon(elementAt.mIcon);
            }
        }
    }

    public void HandleSelection(MenuItem menuItem) {
        this.mActions.elementAt(menuItem.getItemId()).Invoke(this.mActivity);
    }

    public void AddAction(MenuAction menuAction) {
        this.mActions.addElement(menuAction);
    }
}
