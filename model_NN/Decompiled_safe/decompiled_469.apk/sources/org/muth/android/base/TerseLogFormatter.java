package org.muth.android.base;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class TerseLogFormatter extends Formatter {
    static final SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy//MM/dd HH:mm:ss.SSS");

    public String format(LogRecord logRecord) {
        StringBuffer stringBuffer = new StringBuffer(1000);
        stringBuffer.append(mDateFormat.format(new Date(logRecord.getMillis())));
        stringBuffer.append(" ");
        stringBuffer.append(logRecord.getLevel());
        stringBuffer.append(" ");
        stringBuffer.append(formatMessage(logRecord));
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }
}
