package org.muth.android.base;

import android.content.Context;
import android.os.Build;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.logging.Logger;

public class Tracker {
    private static String gPrefix = "";
    private static GoogleAnalyticsTracker gTracker = null;
    private static Logger logger = Logger.getLogger("base");

    static void DeviceStats(PreferenceHelper preferenceHelper, Context context) {
        String phoneCountry = UpdateHelper.getPhoneCountry(context);
        TrackEvent("Settings", "Locale", phoneCountry + " " + UpdateHelper.getPhoneLanguage(context), 0);
        TrackEvent("Settings", "Phone", phoneCountry + " " + Build.MODEL, 0);
        TrackEvent("Settings", "Board", phoneCountry + " " + Build.BOARD, 0);
        TrackEvent("Settings", "CpuAbi", phoneCountry + " " + Build.CPU_ABI, 0);
        TrackEvent("Settings", "OS", phoneCountry + " " + Build.ID, 0);
        TrackEvent("Settings", "SDK", phoneCountry + " " + Build.VERSION.RELEASE, 0);
    }

    static void FrequentUserStats(PreferenceHelper preferenceHelper) {
        int preferenceInt = preferenceHelper.getPreferenceInt("Debug:Track:Invocations") + 1;
        preferenceHelper.setStringPreference("Debug:Track:Invocations", Integer.toString(preferenceInt));
        if (preferenceInt % 50 == 0) {
            TrackPage(String.format("/Usage%06d", Integer.valueOf(preferenceInt)));
        }
    }

    static void InstallAndUpdateStats(PreferenceHelper preferenceHelper, Context context) {
        String stringPreference = preferenceHelper.getStringPreference("Debug:Track:Version");
        String appVersionName = UpdateHelper.appVersionName(context);
        if (!stringPreference.equals(appVersionName)) {
            logger.warning("new version: " + appVersionName + " last version: " + stringPreference);
            if (stringPreference.equals("")) {
                TrackPage("/InstallNew");
            } else {
                TrackPage("/InstallUpdate");
            }
            preferenceHelper.setStringPreference("Debug:Track:Version", appVersionName);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0011, code lost:
        if (r5.getPreferenceBool("Debug:Track:Disable") == false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void StartTracker(java.lang.String r2, java.lang.String r3, int r4, org.muth.android.base.PreferenceHelper r5, android.content.Context r6) {
        /*
            java.lang.Class<org.muth.android.base.Tracker> r0 = org.muth.android.base.Tracker.class
            monitor-enter(r0)
            com.google.android.apps.analytics.GoogleAnalyticsTracker r1 = org.muth.android.base.Tracker.gTracker     // Catch:{ all -> 0x002c }
            if (r1 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r0)
            return
        L_0x0009:
            if (r5 == 0) goto L_0x0013
            java.lang.String r1 = "Debug:Track:Disable"
            boolean r1 = r5.getPreferenceBool(r1)     // Catch:{ all -> 0x002c }
            if (r1 != 0) goto L_0x0007
        L_0x0013:
            com.google.android.apps.analytics.GoogleAnalyticsTracker r1 = com.google.android.apps.analytics.GoogleAnalyticsTracker.getInstance()     // Catch:{ all -> 0x002c }
            org.muth.android.base.Tracker.gTracker = r1     // Catch:{ all -> 0x002c }
            com.google.android.apps.analytics.GoogleAnalyticsTracker r1 = org.muth.android.base.Tracker.gTracker     // Catch:{ all -> 0x002c }
            r1.start(r2, r4, r6)     // Catch:{ all -> 0x002c }
            org.muth.android.base.Tracker.gPrefix = r3     // Catch:{ all -> 0x002c }
            if (r5 == 0) goto L_0x0007
            InstallAndUpdateStats(r5, r6)     // Catch:{ all -> 0x002c }
            FrequentUserStats(r5)     // Catch:{ all -> 0x002c }
            DeviceStats(r5, r6)     // Catch:{ all -> 0x002c }
            goto L_0x0007
        L_0x002c:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.muth.android.base.Tracker.StartTracker(java.lang.String, java.lang.String, int, org.muth.android.base.PreferenceHelper, android.content.Context):void");
    }

    public static synchronized void TrackPage(String str) {
        synchronized (Tracker.class) {
            if (gTracker != null) {
                gTracker.trackPageView(gPrefix + str);
            }
        }
    }

    public static synchronized void TrackPage(Object obj) {
        synchronized (Tracker.class) {
            String[] split = obj.getClass().getName().split("[.]");
            TrackPage("/" + split[split.length - 1]);
        }
    }

    public static synchronized void TrackEvent(String str, String str2, String str3, int i) {
        synchronized (Tracker.class) {
            if (gTracker != null) {
                gTracker.trackEvent(str, str2, str3, i);
            }
        }
    }
}
