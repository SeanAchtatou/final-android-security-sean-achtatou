package org.muth.android.base;

import android.content.Context;
import android.preference.ListPreference;
import android.view.View;
import android.view.ViewGroup;
import java.util.logging.Logger;

public class ListPreferenceWithSummary extends ListPreference {
    private static Logger logger = Logger.getLogger("base");

    public ListPreferenceWithSummary(Context context, String str, String[] strArr, String[] strArr2) {
        super(context);
        setEntries(strArr);
        setEntryValues(strArr2);
        setKey(str);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean z) {
        super.onDialogClosed(z);
        UpdateSummary();
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        UpdateSummary();
        return super.onCreateDialogView();
    }

    /* access modifiers changed from: protected */
    public View onCreateView(ViewGroup viewGroup) {
        UpdateSummary();
        return super.onCreateView(viewGroup);
    }

    public void UpdateSummary() {
        setSummary(getEntry());
    }
}
