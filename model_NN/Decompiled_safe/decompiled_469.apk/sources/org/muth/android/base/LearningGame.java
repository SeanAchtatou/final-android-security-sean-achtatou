package org.muth.android.base;

import android.os.Handler;
import java.util.List;

public class LearningGame<Item> {
    private static final String TAG = "Notes";
    private Item mCurrentItem;
    private final LearningDrill<Item> mDrill;
    private boolean mGuessedWrong;
    private final Handler mHandler = new Handler();
    private Item mLastGuess;
    private int mNumCorrectAnswers;
    private int mNumFalseAnswers;
    private final int mNumRounds;
    private final int mNumSecs;
    private int mSecRemaining;
    private final Runnable mTask = new Runnable() {
        public void run() {
            LearningGame.this.TaskDecrementTimeAndSchedule();
        }
    };

    public LearningGame(int i, int i2, int i3, List<Item> list) {
        int i4;
        if (i3 >= (list.size() / 2) + 1) {
            i4 = (list.size() / 2) + 1;
        } else {
            i4 = i3;
        }
        this.mDrill = new LearningDrill<>(i4, list);
        this.mNumSecs = i;
        this.mNumRounds = i2;
    }

    public int Timeout() {
        return this.mNumSecs;
    }

    public int Rounds() {
        return this.mNumRounds;
    }

    public void NewGame() {
        this.mNumCorrectAnswers = 0;
        this.mNumFalseAnswers = 0;
        NextRound();
    }

    /* access modifiers changed from: private */
    public void TaskDecrementTimeAndSchedule() {
        if (this.mNumSecs != 0) {
            this.mSecRemaining--;
            if (this.mSecRemaining <= 0) {
                CheckAnswer(null);
            } else {
                this.mHandler.removeCallbacks(this.mTask);
                this.mHandler.postDelayed(this.mTask, 1000);
            }
        }
        UpdateCallback(this.mCurrentItem, this.mLastGuess, this.mNumCorrectAnswers, this.mNumFalseAnswers, this.mGuessedWrong, this.mSecRemaining);
    }

    public void StopTimer() {
        this.mHandler.removeCallbacks(this.mTask);
    }

    private void NextRound() {
        if (this.mNumRounds == 0 || this.mNumCorrectAnswers + this.mNumFalseAnswers != this.mNumRounds) {
            this.mCurrentItem = this.mDrill.GetNextItem();
            this.mLastGuess = null;
            this.mSecRemaining = this.mNumSecs;
            if (this.mNumSecs == 0) {
                this.mSecRemaining = -1;
            }
            this.mGuessedWrong = false;
            TaskDecrementTimeAndSchedule();
            return;
        }
        UpdateCallback(null, null, this.mNumCorrectAnswers, this.mNumFalseAnswers, this.mGuessedWrong, this.mSecRemaining);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Item
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void CheckAnswer(Item r8) {
        /*
            r7 = this;
            r7.StopTimer()
            r7.mLastGuess = r8
            Item r0 = r7.mCurrentItem
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x001b
            boolean r0 = r7.mGuessedWrong
            if (r0 != 0) goto L_0x0017
            int r0 = r7.mNumCorrectAnswers
            int r0 = r0 + 1
            r7.mNumCorrectAnswers = r0
        L_0x0017:
            r7.NextRound()
        L_0x001a:
            return
        L_0x001b:
            boolean r0 = r7.mGuessedWrong
            if (r0 != 0) goto L_0x0028
            r0 = 1
            r7.mGuessedWrong = r0
            int r0 = r7.mNumFalseAnswers
            int r0 = r0 + 1
            r7.mNumFalseAnswers = r0
        L_0x0028:
            Item r1 = r7.mCurrentItem
            Item r2 = r7.mLastGuess
            int r3 = r7.mNumCorrectAnswers
            int r4 = r7.mNumFalseAnswers
            boolean r5 = r7.mGuessedWrong
            int r6 = r7.mSecRemaining
            r0 = r7
            r0.UpdateCallback(r1, r2, r3, r4, r5, r6)
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.muth.android.base.LearningGame.CheckAnswer(java.lang.Object):void");
    }

    /* access modifiers changed from: protected */
    public void UpdateCallback(Item item, Item item2, int i, int i2, boolean z, int i3) {
    }
}
