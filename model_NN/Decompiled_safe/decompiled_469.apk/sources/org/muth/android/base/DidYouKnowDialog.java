package org.muth.android.base;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.util.logging.Logger;

public class DidYouKnowDialog {
    /* access modifiers changed from: private */
    public static Logger logger = Logger.getLogger("base");

    public DidYouKnowDialog(Context context, final PreferenceHelper preferenceHelper, final String str, String str2, String str3, String str4, String str5) {
        if (preferenceHelper.getPreferenceBool(str)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setNegativeButton(str5, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    DidYouKnowDialog.logger.info("@@ did you know dialog - never show again pressed");
                    preferenceHelper.setBooleanPreference(str, false);
                }
            });
            builder.setPositiveButton(str4, (DialogInterface.OnClickListener) null);
            if (str2 != null) {
                builder.setTitle(str2);
            }
            if (str3 != null) {
                builder.setMessage(str3);
            }
            builder.show();
        }
    }
}
