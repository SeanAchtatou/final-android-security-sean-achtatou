package org.muth.android.base;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class HtmlArchive {
    private static Logger logger = Logger.getLogger("base");
    private byte[] mData;
    protected final Vector<String> mHtmlFiles = new Vector<>(100);
    protected final String mLocal;

    /* access modifiers changed from: protected */
    public String getHtmlFilename(String str) {
        String str2 = "html-" + this.mLocal + "/" + str;
        Iterator<String> it = this.mHtmlFiles.iterator();
        while (it.hasNext()) {
            if (str2.equals(it.next())) {
                return str2;
            }
        }
        String str3 = "html/" + str;
        Iterator<String> it2 = this.mHtmlFiles.iterator();
        while (it2.hasNext()) {
            if (str3.equals(it2.next())) {
                return str3;
            }
        }
        return null;
    }

    public byte[] getHtmlData(String str) {
        ZipEntry nextEntry;
        logger.info("retrieving: " + str);
        byte[] bArr = new byte[0];
        String htmlFilename = getHtmlFilename(str);
        if (htmlFilename == null) {
            logger.severe("no local file named " + str);
            return bArr;
        }
        logger.severe("using file " + htmlFilename);
        ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(this.mData));
        do {
            try {
                nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    logger.severe("no proper zip entry found");
                    return bArr;
                }
            } catch (Exception e) {
                logger.severe("exception " + e);
                return bArr;
            }
        } while (!nextEntry.getName().equals(htmlFilename));
        int size = (int) nextEntry.getSize();
        logger.severe("found archive entry of size " + size);
        byte[] bArr2 = new byte[size];
        for (int i = 0; i < size; i += zipInputStream.read(bArr2, i, size - i)) {
        }
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public void LoadListing() {
        logger.info("loading html db listing");
        ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(this.mData));
        while (true) {
            ZipEntry zipEntry = null;
            try {
                zipEntry = zipInputStream.getNextEntry();
            } catch (Exception e) {
                logger.severe("exception reading html toc " + e);
            }
            if (zipEntry == null) {
                logger.info("num db entries found: " + this.mHtmlFiles.size());
                return;
            } else {
                logger.info("html: " + zipEntry.getName() + " " + zipEntry.getSize() + " " + zipEntry.getCompressedSize());
                this.mHtmlFiles.add(zipEntry.getName());
            }
        }
    }

    public HtmlArchive(InputStream inputStream, String str) {
        logger.info("creating html archive");
        this.mLocal = str;
        try {
            this.mData = FileIO.ReadFromStream(inputStream, true, true);
            LoadListing();
        } catch (Exception e) {
            logger.severe("exception reading stream " + e.toString());
            this.mData = new byte[0];
        }
    }
}
