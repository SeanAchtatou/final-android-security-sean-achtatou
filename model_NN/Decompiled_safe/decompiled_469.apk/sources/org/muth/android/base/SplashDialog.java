package org.muth.android.base;

import android.app.ProgressDialog;
import android.content.Context;
import java.util.logging.Logger;

public class SplashDialog {
    /* access modifiers changed from: private */
    public static Logger logger = Logger.getLogger("base");
    boolean mAbort = false;
    final Context mContext;
    final int mIcon;
    final int mInc;
    final int mMax;
    ProgressDialog mSplash;
    final String mTitle;

    public Runnable MakeRunnableStart() {
        return new Runnable() {
            public void run() {
                SplashDialog.logger.info("splash start");
                SplashDialog.this.mSplash = new ProgressDialog(SplashDialog.this.mContext);
                if (SplashDialog.this.mIcon < 0) {
                    SplashDialog.this.mSplash.setIcon(SplashDialog.this.mIcon);
                }
                SplashDialog.this.mSplash.setTitle(SplashDialog.this.mTitle);
                SplashDialog.this.mSplash.setProgressStyle(1);
                SplashDialog.this.mSplash.setMax(SplashDialog.this.mMax);
                SplashDialog.this.mSplash.show();
            }
        };
    }

    public Runnable MakeRunnableInc() {
        return new Runnable() {
            public void run() {
                SplashDialog.this.mSplash.incrementProgressBy(SplashDialog.this.mInc);
            }
        };
    }

    public Runnable MakeRunnableStop() {
        return new Runnable() {
            public void run() {
                SplashDialog.logger.info("splash stop");
                if (!SplashDialog.this.mAbort) {
                    SplashDialog.this.mSplash.dismiss();
                }
            }
        };
    }

    public void Abort() {
        this.mAbort = true;
    }

    public SplashDialog(Context context, int i, String str, int i2, int i3) {
        this.mContext = context;
        this.mIcon = i;
        this.mTitle = str;
        this.mMax = i2;
        this.mInc = i3;
    }
}
