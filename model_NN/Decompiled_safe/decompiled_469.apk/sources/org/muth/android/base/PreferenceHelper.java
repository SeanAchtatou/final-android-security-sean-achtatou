package org.muth.android.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;
import junit.framework.Assert;

public class PreferenceHelper {
    static final /* synthetic */ boolean $assertionsDisabled = (!PreferenceHelper.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final int PREF_TYPE_BOOLEAN = 3;
    public static final int PREF_TYPE_DATE = 4;
    public static final int PREF_TYPE_INT = 2;
    public static final int PREF_TYPE_STRING = 1;
    private static Logger logger = Logger.getLogger("base");
    private final Context mContext;
    private final Vector<PrefInfo> mPrefInfo = new Vector<>();
    private final SharedPreferences mSettings;

    private static class PrefInfo {
        public String mDefault;
        public String mFamily;
        public String mGroup;
        public String mKey = (this.mGroup + ":" + this.mFamily + ":" + this.mName).intern();
        public String mName;
        public int mType;

        public PrefInfo(String str, String str2, String str3, String str4, int i) {
            this.mFamily = str2.intern();
            this.mGroup = str.intern();
            this.mName = str3.intern();
            this.mDefault = str4.intern();
            this.mType = i;
        }

        public static class PrefInfoComparator implements Comparator<PrefInfo> {
            public int compare(PrefInfo prefInfo, PrefInfo prefInfo2) {
                return prefInfo.mKey.compareTo(prefInfo2.mKey);
            }
        }
    }

    public void AddPref(String str, String str2, String str3, String str4, int i) {
        PrefInfo prefInfo = new PrefInfo(str, str2, str3, str4, i);
        Iterator<PrefInfo> it = this.mPrefInfo.iterator();
        while (it.hasNext()) {
            PrefInfo next = it.next();
            if (!$assertionsDisabled && next.mKey.equals(prefInfo.mKey)) {
                throw new AssertionError();
            }
        }
        this.mPrefInfo.add(prefInfo);
    }

    public void AddPref(String str, String str2, int i) {
        String[] split = str.split(":");
        AddPref(split[0], split[1], split[2], str2, i);
    }

    private void AddStandardPrefs() {
        AddPref("Debug", "Misc", "Eula", "false", 3);
        AddPref("Debug", "Misc", "LogLevel", "WARNING", 1);
        AddPref("Debug", "Misc", "DebugMode", "false", 3);
        AddPref("Debug", "Misc", "Expired", "false", 3);
        AddPref("Debug", "Misc", "LastCheck", "2009/01/01", 4);
        AddPref("Debug", "Misc", "LastWarning", "2009/01/01", 4);
        AddPref("Debug", "Misc", "InstallDate", "2000/01/01", 4);
        AddPref("Debug", "Misc", "LatestVersion", "0", 2);
        AddPref("Debug", "Misc", "DemoMode", "true", 3);
        AddPref("Debug", "Misc", "UrlPrefix", "http://muth.org/Android/Update", 1);
        AddPref("Debug", "Misc", "UrlMarket", "market://search?q=robert%20muth", 1);
        AddPref("Debug", "Misc", "UrlEmail", "mailto:rm%2bandroid@muth.org", 1);
        AddPref("Ad", "Misc", "Message", "More apps from this author", 1);
        AddPref("Ad", "Misc", "Url", "market://search?q=robert%20muth", 1);
        AddPref("Ad", "Misc", "Color", "cc0000", 1);
        AddPref("Debug", "Track", "Version", "", 1);
        AddPref("Debug", "Track", "Invocations", "0", 2);
        AddPref("Debug", "Track", "Disable", "false", 3);
    }

    public PreferenceHelper(Context context) {
        this.mContext = context;
        this.mSettings = PreferenceManager.getDefaultSharedPreferences(context);
        AddStandardPrefs();
    }

    public boolean IsKnownPref(String str) {
        Iterator<PrefInfo> it = this.mPrefInfo.iterator();
        while (it.hasNext()) {
            if (it.next().mKey == str) {
                return true;
            }
        }
        return $assertionsDisabled;
    }

    public void resetAllPreferences() {
        SharedPreferences.Editor edit = this.mSettings.edit();
        edit.clear();
        edit.commit();
    }

    public void setPreferenceTimeNow(String str) {
        setStringPreference(str, new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime()));
    }

    public synchronized void setStringPreference(String str, String str2) {
        if ($assertionsDisabled || IsKnownPref(str)) {
            SharedPreferences.Editor edit = this.mSettings.edit();
            edit.putString(str, str2);
            edit.commit();
        } else {
            throw new AssertionError();
        }
    }

    public synchronized void setIntPreference(String str, int i) {
        if ($assertionsDisabled || IsKnownPref(str)) {
            SharedPreferences.Editor edit = this.mSettings.edit();
            edit.putString(str, Integer.toString(i));
            edit.commit();
        } else {
            throw new AssertionError();
        }
    }

    public synchronized void setBooleanPreference(String str, boolean z) {
        if ($assertionsDisabled || IsKnownPref(str)) {
            SharedPreferences.Editor edit = this.mSettings.edit();
            edit.putBoolean(str, z);
            edit.commit();
        } else {
            throw new AssertionError();
        }
    }

    public String getStringPreference(String str) {
        if ($assertionsDisabled || IsKnownPref(str)) {
            return this.mSettings.getString(str, "");
        }
        throw new AssertionError();
    }

    public long getPreferenceTimeInMSecs(String str) {
        try {
            return new SimpleDateFormat("yyyy/MM/dd").parse(getStringPreference(str)).getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    public int getPreferenceInt(String str) {
        try {
            return Integer.valueOf(getStringPreference(str)).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean getPreferenceBool(String str) {
        if ($assertionsDisabled || IsKnownPref(str)) {
            return this.mSettings.getBoolean(str, $assertionsDisabled);
        }
        throw new AssertionError();
    }

    public long getPreferenceAgeInMSecs(String str) {
        try {
            return Calendar.getInstance().getTime().getTime() - new SimpleDateFormat("yyyy/MM/dd").parse(getStringPreference(str)).getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    public HashSet<String> getStringPreferenceSet(String str) {
        HashSet<String> hashSet = new HashSet<>(20);
        Iterator<PrefInfo> it = this.mPrefInfo.iterator();
        while (it.hasNext()) {
            PrefInfo next = it.next();
            if (next.mKey.startsWith(str) && getPreferenceBool(next.mKey)) {
                hashSet.add(next.mKey.substring(str.length()));
            }
        }
        return hashSet;
    }

    public void MaybeInitPrefs() {
        SharedPreferences.Editor edit = this.mSettings.edit();
        Iterator<PrefInfo> it = this.mPrefInfo.iterator();
        while (it.hasNext()) {
            PrefInfo next = it.next();
            if (!this.mSettings.contains(next.mKey)) {
                logger.info("initialize " + next.mKey);
                if (next.mType == 3) {
                    edit.putBoolean(next.mKey, next.mDefault == "true" ? true : $assertionsDisabled);
                } else if (next.mType == 1) {
                    edit.putString(next.mKey, next.mDefault);
                } else if (next.mType == 2) {
                    edit.putString(next.mKey, next.mDefault);
                } else if (next.mType == 4) {
                    edit.putString(next.mKey, next.mDefault);
                } else {
                    logger.severe("unexpected pref " + next.mKey);
                    Assert.fail("unexpected pref " + next.mKey);
                }
            }
        }
        edit.commit();
    }
}
