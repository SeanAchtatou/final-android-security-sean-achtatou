package org.muth.android.base;

import java.util.Arrays;

public final class ByteArrayWrapper {
    private final byte[] data;

    public ByteArrayWrapper(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException();
        }
        this.data = bArr;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ByteArrayWrapper)) {
            return false;
        }
        return Arrays.equals(this.data, ((ByteArrayWrapper) obj).data);
    }

    public int hashCode() {
        return Arrays.hashCode(this.data);
    }

    public byte[] data() {
        return this.data;
    }
}
