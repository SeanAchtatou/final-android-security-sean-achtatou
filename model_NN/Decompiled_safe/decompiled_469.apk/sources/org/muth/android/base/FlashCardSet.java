package org.muth.android.base;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.Vector;

public class FlashCardSet implements Serializable {
    static final /* synthetic */ boolean $assertionsDisabled = (!FlashCardSet.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    static final long serialVersionUID = 3;
    private Vector<FlashCardItem> mCards = new Vector<>(10);
    private transient List<FlashCardItem> mReviewStack = null;
    private String mTitle = "";

    static short Time2Day(long j) {
        long rawOffset = ((((long) TimeZone.getDefault().getRawOffset()) + j) - -1280385024) / 86400000;
        if (!$assertionsDisabled && rawOffset <= 0) {
            throw new AssertionError();
        } else if ($assertionsDisabled || rawOffset < 30000) {
            return (short) ((int) rawOffset);
        } else {
            throw new AssertionError();
        }
    }

    public static short Now() {
        return Time2Day(System.currentTimeMillis());
    }

    public int JoinCards(List<byte[]> list) {
        int i = 0;
        HashMap hashMap = new HashMap(NumCards() * 2);
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            FlashCardItem next = it.next();
            hashMap.put(new String(next.Id()), next);
        }
        Vector<FlashCardItem> vector = new Vector<>(list.size());
        for (byte[] next2 : list) {
            String str = new String(next2);
            if (hashMap.containsKey(str)) {
                vector.add(hashMap.get(str));
            } else {
                i++;
                vector.add(new FlashCardItem(next2));
            }
            i = i;
        }
        this.mCards = vector;
        return i;
    }

    public int NumCards() {
        return this.mCards.size();
    }

    public int NumCardScheduled(short s) {
        int i = 0;
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            FlashCardItem next = it.next();
            if (s >= next.NextRep() && next.Grade() >= 2) {
                i++;
            }
        }
        return i;
    }

    public int NumCardWithinRepRange(short s, short s2) {
        int i = 0;
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            FlashCardItem next = it.next();
            if (s < next.NextRep() && next.NextRep() <= s2) {
                i++;
            }
        }
        return i;
    }

    public void AddCard(FlashCardItem flashCardItem) {
        this.mCards.add(flashCardItem);
    }

    public void SetCards(Vector<FlashCardItem> vector) {
        this.mCards = vector;
    }

    public List<FlashCardItem> GetCards() {
        return this.mCards;
    }

    public void SetTitle(String str) {
        this.mTitle = str;
    }

    public String Title() {
        return this.mTitle;
    }

    public String Filename() {
        try {
            return URLEncoder.encode(this.mTitle, "UTF-8");
        } catch (Exception e) {
            return "INVALID_NAME_UTF8_EXCEPTION";
        }
    }

    public int NumCardsBads() {
        int i = 0;
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            if (it.next().Type() != 2) {
                i++;
            }
        }
        return i;
    }

    public int NumCardsGood() {
        int i = 0;
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            if (it.next().Type() == 2) {
                i++;
            }
        }
        return i;
    }

    public int NumCardsWithGrade(short s) {
        int i = 0;
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            if (it.next().Grade() == s) {
                i++;
            }
        }
        return i;
    }

    public void ReorgAfterRegrade(FlashCardItem flashCardItem, short s, short s2) {
        if (s >= 2 && s2 < 2) {
            this.mCards.remove(flashCardItem);
            this.mCards.insertElementAt(flashCardItem, 0);
        }
        if (s2 >= 2) {
            do {
            } while (this.mReviewStack.remove(flashCardItem));
        }
    }

    public List<FlashCardItem> GetItemsByGrade(short s, int i, boolean z, boolean z2, Random random) {
        boolean z3;
        List<FlashCardItem> vector = new Vector<>(i);
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            FlashCardItem next = it.next();
            if (next.Grade() == s) {
                if (next.RentionReps() > 0) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (z == z3) {
                    vector.add(next);
                }
            }
        }
        if (vector.size() > i) {
            if (random != null) {
                Collections.shuffle(vector, random);
            }
            vector = vector.subList(0, i);
        }
        if ($assertionsDisabled || vector.size() <= i) {
            return vector;
        }
        throw new AssertionError();
    }

    public Vector<FlashCardItem> GetReviewItemsRetention(short s) {
        Vector<FlashCardItem> vector = new Vector<>(10);
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            FlashCardItem next = it.next();
            if (next.Grade() >= 2 && s >= next.NextRep()) {
                vector.add(next);
            }
        }
        Collections.sort(vector, FlashCardItem.CompShortestInterval());
        return vector;
    }

    public Vector<FlashCardItem> GetReviewItemsLearnAhead(short s) {
        Vector<FlashCardItem> vector = new Vector<>(10);
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            FlashCardItem next = it.next();
            if (next.Grade() >= 2 && next.NextRep() > s) {
                vector.add(next);
            }
        }
        Collections.sort(vector, FlashCardItem.CompEarliestRep());
        return vector;
    }

    public List<FlashCardItem> GetReviewItemsAquire(int i, int i2, int i3, Random random) {
        Vector vector = new Vector(i);
        List<FlashCardItem> GetItemsByGrade = GetItemsByGrade(0, i2, true, true, null);
        List<FlashCardItem> GetItemsByGrade2 = GetItemsByGrade(1, i, true, $assertionsDisabled, null);
        for (int i4 = 0; i4 < i3; i4++) {
            vector.addAll(GetItemsByGrade);
        }
        vector.addAll(GetItemsByGrade2);
        if (GetItemsByGrade.size() >= i2 || vector.size() >= i) {
            return vector;
        }
        int size = i2 - GetItemsByGrade.size();
        List<FlashCardItem> GetItemsByGrade3 = GetItemsByGrade(0, size, $assertionsDisabled, true, null);
        List<FlashCardItem> GetItemsByGrade4 = GetItemsByGrade(1, i, $assertionsDisabled, $assertionsDisabled, null);
        for (int i5 = 0; i5 < i3; i5++) {
            vector.addAll(GetItemsByGrade3);
        }
        vector.addAll(GetItemsByGrade4);
        if (GetItemsByGrade3.size() >= size || vector.size() >= i) {
            return vector;
        }
        List<FlashCardItem> GetItemsByGrade5 = GetItemsByGrade(-1, size - GetItemsByGrade3.size(), $assertionsDisabled, true, random);
        for (int i6 = 0; i6 < i3; i6++) {
            vector.addAll(GetItemsByGrade5);
        }
        return vector;
    }

    public FlashCardItem GetNextCard(boolean z, short s, Random random) {
        if (z) {
            this.mReviewStack = new Vector(0);
            Vector<FlashCardItem> GetReviewItemsRetention = GetReviewItemsRetention(s);
            this.mReviewStack.add(FlashCardItem.MakeSpecial(FlashCardItem.MAGIC_GRADE_FIRST_RETENTION, (short) GetReviewItemsRetention.size()));
            this.mReviewStack.addAll(GetReviewItemsRetention);
            this.mReviewStack.add(FlashCardItem.MakeSpecial(FlashCardItem.MAGIC_GRADE_FIRST_AQUIRE, 0));
        }
        if (this.mReviewStack.size() == 0) {
            this.mReviewStack = GetReviewItemsAquire(10, 5, 2, random);
        }
        if (this.mReviewStack.size() == 0) {
            Vector<FlashCardItem> GetReviewItemsLearnAhead = GetReviewItemsLearnAhead(s);
            if (GetReviewItemsLearnAhead.size() > 0) {
                this.mReviewStack.add(FlashCardItem.MakeSpecial(FlashCardItem.MAGIC_GRADE_FIRST_LEARN_AHEAD, (short) GetReviewItemsLearnAhead.size()));
                this.mReviewStack.addAll(GetReviewItemsLearnAhead);
            }
        }
        if (this.mReviewStack.size() > 0) {
            return this.mReviewStack.remove(0);
        }
        return null;
    }

    public Vector<Integer> RepList() {
        Vector<Integer> vector = new Vector<>(this.mCards.size());
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            vector.add(Integer.valueOf(it.next().NextRep()));
        }
        Collections.sort(vector);
        return vector;
    }

    public Vector<Integer> GradeHistogram() {
        Vector<Integer> vector = new Vector<>(7);
        for (int i = 0; i <= 6; i++) {
            vector.add(0);
        }
        Iterator<FlashCardItem> it = this.mCards.iterator();
        while (it.hasNext()) {
            byte Grade = it.next().Grade();
            if ($assertionsDisabled || Grade <= 5) {
                if (Grade < 0) {
                    Grade = 6;
                }
                vector.set(Grade, Integer.valueOf(vector.get(Grade).intValue() + 1));
            } else {
                throw new AssertionError();
            }
        }
        return vector;
    }

    public String DebugString() {
        StringBuilder sb = new StringBuilder(2000);
        Vector<Integer> GradeHistogram = GradeHistogram();
        sb.append("\ngrade histogram\n");
        for (int i = 0; i < GradeHistogram.size(); i++) {
            if (GradeHistogram.get(i).intValue() > 0) {
                sb.append("" + i + ": " + GradeHistogram.get(i) + "\n");
            }
        }
        sb.append("\nrep histogram\n");
        Vector<Integer> RepList = RepList();
        int i2 = 0;
        int i3 = -1;
        for (int i4 = 0; i4 < RepList.size(); i4++) {
            int intValue = RepList.get(i4).intValue();
            if (intValue == i3) {
                i2++;
            } else {
                if (i2 > 0) {
                    sb.append(i3 + ": " + i2 + "\n");
                }
                i2 = 1;
                i3 = intValue;
            }
        }
        if (i2 > 0) {
            sb.append(i3 + ": " + i2 + "\n");
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void BuildMultiColumnTable(int i, String str, String str2, List<String> list, List<String> list2, StringBuilder sb) {
        if (!$assertionsDisabled && list.size() != list2.size()) {
            throw new AssertionError();
        } else if ($assertionsDisabled || list.size() % i == 0) {
            sb.append("<table border>");
            sb.append("<tr>");
            for (int i2 = 0; i2 < i; i2++) {
                sb.append("<th>" + str + "</th>");
                sb.append("<th>" + str2 + "</th>");
            }
            sb.append("</tr>");
            int i3 = 0;
            while (i3 < list.size()) {
                sb.append("<tr>");
                for (int i4 = 0; i4 < i; i4++) {
                    sb.append("<td>" + list.get(i3 + i4) + "</td>");
                    sb.append("<td align=right>" + list2.get(i3 + i4) + "</td>");
                }
                sb.append("</tr>");
                i3 += i;
            }
            sb.append("</table>");
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public void BuildTwoRowTable(String str, String str2, List<String> list, List<String> list2, StringBuilder sb) {
        if ($assertionsDisabled || list.size() == list2.size()) {
            sb.append("<table border>\n");
            sb.append("<tr>");
            sb.append("<th>" + str + "</th>");
            for (int i = 0; i < list.size(); i++) {
                sb.append("<th>" + list.get(i) + "</th>");
            }
            sb.append("</tr>\n");
            sb.append("<tr>");
            sb.append("<th>" + str2 + "</th>");
            for (int i2 = 0; i2 < list.size(); i2++) {
                sb.append("<td align=right>" + list2.get(i2) + "</td>");
            }
            sb.append("</tr>\n");
            sb.append("</table>\n");
            return;
        }
        throw new AssertionError();
    }

    public String StatsString(int i) {
        StringBuilder sb = new StringBuilder(2000);
        sb.append("<h3>" + TitleString() + "</h3>");
        sb.append("<h4>Grade Distrbution</h4>");
        Vector vector = new Vector(9);
        for (short s = 0; s <= 5; s = (short) (s + 1)) {
            vector.add("" + ((int) s));
        }
        Vector<Integer> GradeHistogram = GradeHistogram();
        Vector vector2 = new Vector(9);
        for (short s2 = 0; s2 <= 5; s2 = (short) (s2 + 1)) {
            vector2.add("" + GradeHistogram.get(s2));
        }
        vector.add("-");
        vector2.add("" + GradeHistogram.get(6));
        BuildTwoRowTable("Grade", "&nbsp;#&nbsp;", vector, vector2, sb);
        sb.append("<h4>Review Schedule</h4>");
        vector2.clear();
        vector.clear();
        Vector<Integer> RepList = RepList();
        RepList.add(1000000);
        int i2 = 0;
        while (i2 < RepList.size() && RepList.get(i2).intValue() == -1) {
            i2++;
        }
        int[] iArr = {-1, 0, 1, 2, 7, 14};
        int i3 = 0;
        int i4 = i2;
        while (i3 < iArr.length) {
            if (iArr[i3] < 0) {
                vector.add("over");
            } else {
                vector.add(iArr[i3] + "d");
            }
            int i5 = 0;
            int i6 = i4;
            while (RepList.get(i6).intValue() <= iArr[i3] + i) {
                i6++;
                i5++;
            }
            vector2.add("" + i5);
            i3++;
            i4 = i6;
        }
        vector.add("&infin;");
        vector2.add("" + ((RepList.size() - 1) - i4));
        vector.add("-");
        vector2.add("" + i2);
        BuildTwoRowTable("Day", "&nbsp;#&nbsp;", vector, vector2, sb);
        return sb.toString();
    }

    public String TitleString() {
        return "[" + Title() + "] " + NumCards() + " Items";
    }
}
