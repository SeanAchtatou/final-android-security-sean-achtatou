package org.muth.android.base;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.util.List;
import java.util.logging.Logger;

public class MultipleChoiceDialog extends AlertDialog.Builder {
    static final int META_OFFSET = 0;
    /* access modifiers changed from: private */
    public static Logger logger = Logger.getLogger("base");
    DialogInterface.OnMultiChoiceClickListener mChoiceListener;
    /* access modifiers changed from: private */
    public List<Boolean> mFlagArraySaved;
    /* access modifiers changed from: private */
    public boolean[] mFlags;
    private CharSequence[] mItems;
    /* access modifiers changed from: private */
    public Runnable mRunnableOK;

    public MultipleChoiceDialog(Context context, List<String> list, List<Boolean> list2, String str, Runnable runnable, String str2, String str3, String str4, String str5) {
        super(context);
        this.mItems = new CharSequence[(list.size() + 0)];
        for (int i = 0; i < list.size(); i++) {
            this.mItems[i + 0] = list.get(i);
        }
        this.mFlags = new boolean[(list2.size() + 0)];
        for (int i2 = 0; i2 < list2.size(); i2++) {
            this.mFlags[i2 + 0] = list2.get(i2).booleanValue();
        }
        this.mFlagArraySaved = list2;
        this.mRunnableOK = runnable;
        logger.info("@@ creating muliple choice dialog");
        this.mChoiceListener = new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialogInterface, int i, boolean z) {
                MultipleChoiceDialog.logger.info("@@ multi choice dialog - clicked " + i);
            }
        };
        setMultiChoiceItems(this.mItems, this.mFlags, this.mChoiceListener);
        setPositiveButton(str2, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MultipleChoiceDialog.logger.info("@@ multi choice dialog - ok button pressed");
                for (int i2 = 0; i2 < MultipleChoiceDialog.this.mFlagArraySaved.size(); i2++) {
                    MultipleChoiceDialog.this.mFlagArraySaved.set(i2, Boolean.valueOf(MultipleChoiceDialog.this.mFlags[i2 + 0]));
                }
                if (MultipleChoiceDialog.this.mRunnableOK != null) {
                    MultipleChoiceDialog.this.mRunnableOK.run();
                }
            }
        });
        setNegativeButton(str5, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MultipleChoiceDialog.logger.info("@@ multi choice dialog - cancel button pressed");
            }
        });
        if (str != null) {
            setTitle(str);
        }
    }
}
