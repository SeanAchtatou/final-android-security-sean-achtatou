package org.muth.android.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Environment;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class UpdateHelper {
    static final /* synthetic */ boolean $assertionsDisabled = (!UpdateHelper.class.desiredAssertionStatus());
    private static Logger logger = Logger.getLogger("base");

    public static void launchIntend(Activity activity, Intent intent) {
        try {
            activity.startActivity(intent);
        } catch (Exception e) {
            logger.severe("could not launch: " + intent.toString() + " error: " + e.toString());
        }
    }

    public static void launchUrlApp(Activity activity, String str) {
        logger.info("launching app: " + str);
        launchIntend(activity, new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    public static void launchActivityWithType(Activity activity, String str, String str2) {
        String packageName = activity.getPackageName();
        launchIntend(activity, new Intent().setClassName(packageName, packageName + "." + str).setType(str2));
    }

    public static void launchActivityWithData(Activity activity, String str, String str2) {
        String packageName = activity.getPackageName();
        launchIntend(activity, new Intent().setClassName(packageName, packageName + "." + str).setData(Uri.parse(str2)));
    }

    private static class KillOnClick implements DialogInterface.OnClickListener {
        private Activity mActivity;
        private String mUrl;

        public KillOnClick(Activity activity, String str) {
            this.mActivity = activity;
            this.mUrl = str;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            UpdateHelper.launchUrlApp(this.mActivity, this.mUrl);
            this.mActivity.finish();
        }
    }

    public static PackageInfo appPackageInfo(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (Exception e) {
            logger.warning("bad package info " + e.toString());
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public static boolean appIsDemoVersion(Context context) {
        String packageName = context.getPackageName();
        if (packageName.indexOf("_demo_") > 0) {
            return true;
        }
        if (packageName.indexOf("_pro_") > 0) {
            return false;
        }
        if (packageName.indexOf("conjugator_es") > 0) {
            return true;
        }
        if (packageName.indexOf("kana_lite") > 0) {
            return true;
        }
        if (packageName.indexOf("kana") > 0) {
            return false;
        }
        if ($assertionsDisabled) {
            return true;
        }
        throw new AssertionError();
    }

    public static boolean appIsDebuggable(Context context) {
        return (appPackageInfo(context).applicationInfo.flags & 2) != 0;
    }

    public static int appVersionCode(Context context) {
        return appPackageInfo(context).versionCode;
    }

    public static String appVersionFlavor(Context context) {
        String appVersionName = appVersionName(context);
        return appVersionName.substring(appVersionName.lastIndexOf(".") + 1);
    }

    public static String appVersionName(Context context) {
        return appPackageInfo(context).versionName;
    }

    public static String getPhoneLanguage(Context context) {
        return context.getResources().getConfiguration().locale.getLanguage().toLowerCase();
    }

    public static String getPhoneCountry(Context context) {
        return context.getResources().getConfiguration().locale.getCountry().toLowerCase();
    }

    public static String getConfigInfo(Context context) {
        return appVersionName(context) + "-" + getPhoneCountry(context);
    }

    public static String makeUpdateUrl(Context context, PreferenceHelper preferenceHelper, boolean z, boolean z2) {
        String str;
        String str2;
        if (z) {
            str = "." + appVersionCode(context);
        } else {
            str = "";
        }
        if (z2) {
            str2 = "." + getPhoneLanguage(context);
        } else {
            str2 = "";
        }
        return preferenceHelper.getStringPreference("Debug:Misc:UrlPrefix") + "/" + context.getPackageName() + str2 + str + ".v2.html";
    }

    public static String makeUpdateAgent(Context context) {
        return context.getPackageName() + "-" + getConfigInfo(context);
    }

    public static String appStorageDir(Context context) {
        return Environment.getExternalStorageDirectory() + "/" + context.getPackageName();
    }

    private static void processPrefsFromStream(PreferenceHelper preferenceHelper, InputStreamReader inputStreamReader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                logger.warning("update info: " + readLine);
                String[] split = readLine.split(" +", 2);
                if (split.length == 2) {
                    if (split[1].equals("false")) {
                        preferenceHelper.setBooleanPreference(split[0], false);
                    } else if (split[1].equals("true")) {
                        preferenceHelper.setBooleanPreference(split[0], true);
                    } else {
                        preferenceHelper.setStringPreference(split[0], split[1]);
                    }
                }
            } else {
                return;
            }
        }
    }

    private static void fetchRecentUpdateInfo(List<String> list, String str, PreferenceHelper preferenceHelper) {
        logger.info("update checks, num urls: " + list.size());
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        try {
            for (String uri : list) {
                URI uri2 = new URI(uri);
                HttpPost httpPost = new HttpPost(uri2);
                logger.info("using agent " + str);
                httpPost.setHeader("user-agent", str);
                logger.warning("starting lookup: " + uri2.toString());
                HttpResponse execute = defaultHttpClient.execute(httpPost);
                StatusLine statusLine = execute.getStatusLine();
                if (statusLine.getStatusCode() != 200) {
                    logger.warning("bad status code " + statusLine);
                } else {
                    processPrefsFromStream(preferenceHelper, new InputStreamReader(execute.getEntity().getContent(), "utf-8"));
                    return;
                }
            }
        } catch (Exception e) {
            logger.warning("exception " + e);
        }
    }

    public static void refreshUpdateInfo(Context context, PreferenceHelper preferenceHelper, boolean z) {
        long preferenceAgeInMSecs = preferenceHelper.getPreferenceAgeInMSecs("Debug:Misc:LastCheck") / 86400000;
        logger.warning("refresh update info last check " + preferenceAgeInMSecs + " force " + z);
        if (!z && preferenceAgeInMSecs < 7) {
            logger.warning("refresh update skipping");
        } else if (!networkAvailable(context)) {
            logger.warning("no network available");
        } else {
            Vector vector = new Vector(5);
            vector.add(makeUpdateUrl(context, preferenceHelper, true, true));
            vector.add(makeUpdateUrl(context, preferenceHelper, true, false));
            vector.add(makeUpdateUrl(context, preferenceHelper, false, true));
            vector.add(makeUpdateUrl(context, preferenceHelper, false, false));
            fetchRecentUpdateInfo(vector, makeUpdateAgent(context), preferenceHelper);
            preferenceHelper.setPreferenceTimeNow("Debug:Misc:LastCheck");
        }
    }

    public static void refreshUpdateInfoBackground(final Context context, final PreferenceHelper preferenceHelper, final boolean z) {
        logger.warning("refresh update info in background, force " + z);
        new Thread() {
            public void run() {
                UpdateHelper.refreshUpdateInfo(context, preferenceHelper, z);
            }
        }.start();
    }

    private static void showUpdateDialog(Activity activity, PreferenceHelper preferenceHelper, String str, boolean z) {
        if (z || preferenceHelper.getPreferenceAgeInMSecs("Debug:Misc:LastWarning") / 86400000 >= 2) {
            preferenceHelper.setPreferenceTimeNow("Debug:Misc:LastWarning");
            new AlertDialog.Builder(activity).setTitle("Warning").setPositiveButton("Update", new KillOnClick(activity, preferenceHelper.getStringPreference("Debug:Misc:UrlMarket"))).setNegativeButton("Ignore", (DialogInterface.OnClickListener) null).setMessage(str).create().show();
        }
    }

    private static boolean isUpdateAvailable(Context context, PreferenceHelper preferenceHelper) {
        return Integer.parseInt(preferenceHelper.getStringPreference("Debug:Misc:LatestVersion")) > appVersionCode(context);
    }

    public static boolean hasAppExpired(PreferenceHelper preferenceHelper) {
        return preferenceHelper.getPreferenceBool("Debug:Misc:Expired");
    }

    public static boolean checkForExpiration(Activity activity, PreferenceHelper preferenceHelper, String str, String str2) {
        logger.warning("expiration check");
        if (!hasAppExpired(preferenceHelper)) {
            return false;
        }
        new AlertDialog.Builder(activity).setTitle("Error").setPositiveButton(str2, new KillOnClick(activity, preferenceHelper.getStringPreference("Debug:Misc:UrlMarket"))).setMessage(str).create().show();
        return true;
    }

    public static void checkForUpdates(Activity activity, PreferenceHelper preferenceHelper, String str) {
        if (isUpdateAvailable(activity, preferenceHelper)) {
            logger.info("show update dialog");
            showUpdateDialog(activity, preferenceHelper, str, false);
        }
    }

    public static boolean startupChecks(Activity activity, PreferenceHelper preferenceHelper, String str, String str2, String str3) {
        if (preferenceHelper.getPreferenceTimeInMSecs("Debug:Misc:InstallDate") == 0) {
            logger.info("first time startup");
            refreshUpdateInfoBackground(activity, preferenceHelper, true);
            preferenceHelper.setPreferenceTimeNow("Debug:Misc:InstallDate");
        } else {
            refreshUpdateInfoBackground(activity, preferenceHelper, false);
        }
        checkForUpdates(activity, preferenceHelper, str);
        return checkForExpiration(activity, preferenceHelper, str2, str3);
    }

    public static boolean networkAvailable(Context context) {
        return true;
    }
}
