package org.muth.android.base;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import java.util.Locale;
import java.util.logging.Logger;

public class TtsHelper implements TextToSpeech.OnInitListener {
    private static Logger logger = Logger.getLogger("base");
    private final String mButtonDisable;
    private final String mButtonInstall;
    private final String mButtonOk;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final String mErrorInit;
    private final String mLanguage;
    private TextToSpeech mTts;

    private static String LanguageToVoice(String str) {
        if (str.equals("en")) {
            return "eng";
        }
        if (str.equals("es")) {
            return "spa";
        }
        if (str.equals("fr")) {
            return "fra";
        }
        if (str.equals("de")) {
            return "deu";
        }
        if (str.equals("it")) {
            return "ita";
        }
        if (str.equals("pt")) {
            return "por";
        }
        throw new RuntimeException("unknow language " + str);
    }

    public TtsHelper(Context context, String str, String str2, String str3, String str4, String str5) {
        this.mTts = new TextToSpeech(context, this);
        this.mContext = context;
        this.mLanguage = LanguageToVoice(str);
        this.mButtonOk = str3;
        this.mButtonInstall = str4;
        this.mButtonDisable = str2;
        this.mErrorInit = str5;
    }

    public void Shutdown() {
        if (this.mTts != null) {
            this.mTts.shutdown();
        }
    }

    private void SpeechInstallDialog(String str) {
        AnonymousClass1 r0 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                TtsHelper.this.DisableCallback();
            }
        };
        new AlertDialog.Builder(this.mContext).setMessage(str).setNegativeButton(this.mButtonDisable, r0).setNeutralButton(this.mButtonOk, (DialogInterface.OnClickListener) null).setPositiveButton(this.mButtonInstall, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                intent.setAction("android.speech.tts.engine.INSTALL_TTS_DATA");
                TtsHelper.this.mContext.startActivity(intent);
            }
        }).show();
    }

    public void onInit(int i) {
        logger.info("tts oninit status " + i);
        if (i != 0) {
            this.mTts = null;
            SpeechInstallDialog(this.mErrorInit);
            return;
        }
        this.mTts.setSpeechRate(0.8f);
        Locale locale = new Locale(this.mLanguage);
        logger.info("tts requesting locale " + locale);
        int language = this.mTts.setLanguage(locale);
        logger.info("tts languages status " + language);
        if (language == 0 || language == 1 || language == 2) {
            logger.info("tts current " + this.mTts.getLanguage());
            return;
        }
        this.mTts = null;
        SpeechInstallDialog(this.mErrorInit);
    }

    public void DisableCallback() {
    }

    public void Speak(String str) {
        if (this.mTts != null) {
            this.mTts.speak(str, 1, null);
        }
    }
}
