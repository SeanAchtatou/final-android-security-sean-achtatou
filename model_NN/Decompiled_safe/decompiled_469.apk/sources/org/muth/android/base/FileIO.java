package org.muth.android.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class FileIO {
    static final /* synthetic */ boolean $assertionsDisabled = (!FileIO.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public static Logger logger = Logger.getLogger("base");

    public static int ReadRiffHeader(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[4];
        inputStream.read(bArr);
        if (!$assertionsDisabled && bArr[0] != 65) {
            throw new AssertionError();
        } else if (!$assertionsDisabled && bArr[1] != 73) {
            throw new AssertionError();
        } else if (!$assertionsDisabled && bArr[2] != 70) {
            throw new AssertionError();
        } else if ($assertionsDisabled || bArr[3] == 70) {
            int i = 0;
            for (int i2 = 0; i2 < 4; i2++) {
                i |= (inputStream.read() & 255) << (i2 * 8);
            }
            int read = inputStream.read();
            for (int i3 = 1; i3 < read; i3++) {
                inputStream.read();
            }
            return i - read;
        } else {
            throw new AssertionError();
        }
    }

    public static void WriteRiffHeader(int i, int i2, OutputStream outputStream) throws IOException {
        int i3 = i2 + i;
        if ($assertionsDisabled || i > 0) {
            byte[] bArr = new byte[(i + 8)];
            bArr[0] = 65;
            bArr[1] = 73;
            bArr[2] = 70;
            bArr[3] = 70;
            bArr[4] = (byte) (i3 & 255);
            bArr[5] = (byte) ((i3 >> 8) & 255);
            bArr[6] = (byte) ((i3 >> 16) & 255);
            bArr[7] = (byte) ((i3 >> 24) & 255);
            bArr[8] = (byte) i;
            for (int i4 = 9; i4 < i; i4++) {
                bArr[i4] = (byte) ((i4 * i3) >> 6);
            }
            outputStream.write(bArr);
            return;
        }
        throw new AssertionError();
    }

    public static byte[] GzipBytes(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        gZIPOutputStream.write(bArr);
        gZIPOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] UngzipBytes(InputStream inputStream) throws IOException {
        int read;
        GZIPInputStream gZIPInputStream = new GZIPInputStream(inputStream);
        byte[] bArr = new byte[2560000];
        int i = 0;
        do {
            read = gZIPInputStream.read(bArr, i, 10000);
            if (read > 0) {
                i += read;
                continue;
            }
        } while (read >= 0);
        byte[] bArr2 = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr2[i2] = bArr[i2];
        }
        return bArr2;
    }

    public static byte[] UngzipBytes(byte[] bArr) throws IOException {
        return UngzipBytes(new ByteArrayInputStream(bArr));
    }

    public static boolean FileExists(String str) {
        return new File(str).exists();
    }

    public static void FileCopy(String str, String str2) throws IOException {
        File file = new File(str);
        File file2 = new File(str2 + "[tmp]");
        FileInputStream fileInputStream = new FileInputStream(file);
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read == -1) {
                fileInputStream.close();
                fileOutputStream.close();
                file2.renameTo(new File(str2));
                return;
            }
            fileOutputStream.write(bArr, 0, read);
        }
    }

    public static void Backup(String str) {
        logger.info("creating backups for  " + str);
        try {
            File file = new File(str);
            File file2 = new File(str + "[001]");
            File file3 = new File(str + "[002]");
            file3.renameTo(new File(str + "[003]"));
            file2.renameTo(file3);
            file.renameTo(file2);
        } catch (Exception e) {
            logger.severe("Failure " + e);
        }
    }

    public static void WriteToFile(String str, Serializable serializable, boolean z, int i, boolean z2) throws IOException {
        logger.info("writing obj unit to file " + str);
        File file = new File(str + "[tmp]");
        long currentTimeMillis = System.currentTimeMillis();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(serializable);
        objectOutputStream.close();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        logger.info("serialization complete " + byteArray.length + " bytes, " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        if (z) {
            byteArray = GzipBytes(byteArray);
            logger.info("compressing complete " + byteArray.length + " bytes, " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        }
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        if (i > 0) {
            WriteRiffHeader(i, byteArray.length, fileOutputStream);
        }
        fileOutputStream.write(byteArray);
        fileOutputStream.close();
        if (z2) {
            Backup(str);
        }
        logger.info("renaming");
        file.renameTo(new File(str));
        logger.info("writing complete " + file.length() + " bytes, " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
    }

    private static class RunnableWriter implements Runnable {
        private boolean mBackups;
        private boolean mCompress;
        private String mFilename;
        private int mHeaderSize;
        private Serializable mObj;
        private boolean mOverwrite;

        public RunnableWriter(String str, Serializable serializable, boolean z, int i, boolean z2) {
            this.mFilename = str;
            this.mObj = serializable;
            this.mCompress = z;
            this.mHeaderSize = i;
            this.mBackups = z2;
        }

        public void run() {
            try {
                FileIO.WriteToFile(this.mFilename, this.mObj, this.mCompress, this.mHeaderSize, this.mBackups);
            } catch (Exception e) {
                FileIO.logger.severe("Failure " + e);
            }
        }
    }

    public static void WriteToFileInBackground(String str, Serializable serializable, boolean z, int i, boolean z2) {
        new Thread(new RunnableWriter(str, serializable, z, i, z2)).start();
    }

    public static byte[] ReadFromStream(InputStream inputStream, boolean z, Boolean bool) throws IOException {
        if (bool.booleanValue()) {
            logger.info("payload size " + ReadRiffHeader(inputStream));
        }
        byte[] bArr = new byte[inputStream.available()];
        logger.info("raw/compressed size " + bArr.length);
        inputStream.read(bArr);
        if (z) {
            bArr = UngzipBytes(bArr);
        }
        logger.info("uncompressed size " + bArr.length);
        return bArr;
    }

    public static Object ReadFromFile(String str, boolean z, Boolean bool) throws IOException, ClassNotFoundException {
        logger.info("read obj from file " + str);
        File file = new File(str);
        if (!file.exists()) {
            logger.info("file does not exists");
            return null;
        }
        long currentTimeMillis = System.currentTimeMillis();
        ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(ReadFromStream(new FileInputStream(file), z, bool)));
        Object readObject = objectInputStream.readObject();
        objectInputStream.close();
        logger.info("reading complete " + file.length() + " bytes, " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        return readObject;
    }
}
