package org.muth.android.base;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.Vector;

public class ViewAdapter extends BaseAdapter {
    private Vector<View> mView;

    public ViewAdapter(Vector<View> vector) {
        this.mView = vector;
    }

    public int getCount() {
        return this.mView.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return this.mView.get(i);
    }
}
