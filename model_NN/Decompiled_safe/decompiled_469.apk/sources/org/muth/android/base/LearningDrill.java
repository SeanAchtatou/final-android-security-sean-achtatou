package org.muth.android.base;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import junit.framework.Assert;

public class LearningDrill<Item> {
    private static final String TAG = "@Base";
    private static final int VAL_INIT = 1000;
    private final LinkedList<Item> mHistory;
    private final List<Item> mItems;
    private int mProbScale;
    private int[] mProbabilities;
    private final Random mRandom;

    public LearningDrill(int i, List<Item> list) {
        Assert.assertTrue(list.size() > i);
        this.mRandom = new Random();
        this.mItems = list;
        this.mHistory = new LinkedList<>();
        this.mProbabilities = new int[list.size()];
        for (int i2 = 0; i2 < i; i2++) {
            this.mHistory.add(null);
        }
        this.mProbScale = list.size() * VAL_INIT;
        for (int i3 = 0; i3 < list.size(); i3++) {
            this.mProbabilities[i3] = VAL_INIT;
        }
    }

    public Item GetNextItem() {
        int i;
        do {
            int nextInt = this.mRandom.nextInt(this.mProbScale);
            i = 0;
            while (this.mProbabilities[i] < nextInt) {
                nextInt -= this.mProbabilities[i];
                i++;
            }
        } while (this.mHistory.contains(this.mItems.get(i)));
        this.mHistory.remove(0);
        this.mHistory.add(this.mItems.get(i));
        return this.mItems.get(i);
    }

    public void RateItem(Item item, boolean z) {
        int i;
        int i2 = 0;
        while (this.mItems.get(i2) != item) {
            i2++;
        }
        int i3 = this.mProbabilities[i2];
        if (!z) {
            i = i3 * 4;
        } else if (i3 >= 8) {
            i = i3 / 8;
        } else {
            i = i3;
        }
        this.mProbScale = (i - i3) + this.mProbScale;
        this.mProbabilities[i2] = i;
    }
}
