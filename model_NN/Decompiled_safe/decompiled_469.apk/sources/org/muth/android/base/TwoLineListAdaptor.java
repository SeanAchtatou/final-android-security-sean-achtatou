package org.muth.android.base;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import java.util.logging.Logger;
import junit.framework.Assert;

public class TwoLineListAdaptor extends BaseAdapter {
    private static Logger logger = Logger.getLogger("verbs");
    private LayoutInflater mInflater;
    private final List<String> mLower;
    private final float mSize1;
    private final List<String> mUpper;

    public TwoLineListAdaptor(Context context, float f, List<String> list, List<String> list2) {
        logger.info("two line view of size  " + list2.size());
        this.mUpper = list;
        this.mLower = list2;
        this.mSize1 = f;
        Assert.assertEquals(list.size(), list2.size());
        this.mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return this.mUpper.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        ViewHolder viewHolder;
        logger.info("populate pos " + i);
        if (view == null) {
            view2 = this.mInflater.inflate(17367053, (ViewGroup) null);
            viewHolder = new ViewHolder();
            viewHolder.text1 = (TextView) view2.findViewById(16908308);
            viewHolder.text1.setTextSize(0, this.mSize1);
            viewHolder.text1.setTypeface(Typeface.DEFAULT);
            viewHolder.text2 = (TextView) view2.findViewById(16908309);
            logger.info("populate " + viewHolder.text1 + " " + viewHolder.text2);
            view2.setTag(viewHolder);
        } else {
            view2 = view;
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.text1.setText(this.mUpper.get(i));
        viewHolder.text2.setText(this.mLower.get(i));
        return view2;
    }

    static class ViewHolder {
        TextView text1;
        TextView text2;

        ViewHolder() {
        }
    }
}
