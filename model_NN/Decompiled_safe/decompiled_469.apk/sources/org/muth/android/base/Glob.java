package org.muth.android.base;

public class Glob {
    private boolean mIgnoreCase;
    private String mPattern;
    private String mText;

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0008  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean HasWildcards(java.lang.String r3) {
        /*
            r2 = 0
            r0 = r2
        L_0x0002:
            int r1 = r3.length()
            if (r0 >= r1) goto L_0x0014
            char r1 = r3.charAt(r0)
            switch(r1) {
                case 42: goto L_0x0012;
                case 63: goto L_0x0012;
                default: goto L_0x000f;
            }
        L_0x000f:
            int r0 = r0 + 1
            goto L_0x0002
        L_0x0012:
            r0 = 1
        L_0x0013:
            return r0
        L_0x0014:
            r0 = r2
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: org.muth.android.base.Glob.HasWildcards(java.lang.String):boolean");
    }

    public boolean match(String str, String str2, boolean z) {
        this.mText = str;
        this.mPattern = str2;
        this.mIgnoreCase = z;
        return matchCharacter(0, 0);
    }

    private boolean matchCharacter(int i, int i2) {
        if (i >= this.mPattern.length() && i2 >= this.mText.length()) {
            return true;
        }
        if (i >= this.mPattern.length()) {
            return false;
        }
        switch (this.mPattern.charAt(i)) {
            case '*':
                if (i + 1 >= this.mPattern.length() || i2 >= this.mText.length()) {
                    return true;
                }
                for (int i3 = i2; i3 < this.mText.length(); i3++) {
                    if (matchCharacter(i + 1, i3)) {
                        return true;
                    }
                }
                return false;
            case '?':
                if (i2 >= this.mText.length()) {
                    return false;
                }
                break;
            default:
                if (i2 >= this.mText.length()) {
                    return false;
                }
                String substring = this.mText.substring(i2, i2 + 1);
                String substring2 = this.mPattern.substring(i, i + 1);
                if (this.mIgnoreCase) {
                    if (substring.compareToIgnoreCase(substring2) != 0) {
                        return false;
                    }
                } else if (substring.compareTo(substring2) != 0) {
                    return false;
                }
                break;
        }
        return matchCharacter(i + 1, i2 + 1);
    }
}
