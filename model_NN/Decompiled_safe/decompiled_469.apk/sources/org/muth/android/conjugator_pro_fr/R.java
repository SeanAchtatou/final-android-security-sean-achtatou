package org.muth.android.conjugator_pro_fr;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int textsz_LARGE = 2131099656;
        public static final int textsz_Large = 2131099655;
        public static final int textsz_MEDIUM = 2131099653;
        public static final int textsz_Medium = 2131099652;
        public static final int textsz_SMALL = 2131099650;
        public static final int textsz_Small = 2131099649;
        public static final int textsz_large = 2131099654;
        public static final int textsz_medium = 2131099651;
        public static final int textsz_small = 2131099648;
    }

    public static final class drawable {
        public static final int logo = 2130837504;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131165185;
        public static final int about_app = 2131165186;
        public static final int about_copyright = 2131165187;
        public static final int about_email_author = 2131165189;
        public static final int about_extra = 2131165193;
        public static final int about_info = 2131165192;
        public static final int about_more_apps = 2131165190;
        public static final int about_rate_app = 2131165191;
        public static final int about_tell_a_fried = 2131165188;
        public static final int about_tos = 2131165194;
        public static final int scroll1 = 2131165184;
    }

    public static final class layout {
        public static final int about = 2130903040;
    }

    public static final class raw {
        public static final int html = 2130968576;
        public static final int str = 2130968577;
        public static final int vf = 2130968578;
    }

    public static final class string {
        public static final int app_name = 2131034152;
        public static final int button_email_author = 2131034114;
        public static final int button_more_apps = 2131034115;
        public static final int button_never_again = 2131034119;
        public static final int button_ok = 2131034118;
        public static final int button_rate_app = 2131034113;
        public static final int button_tell_a_friend = 2131034112;
        public static final int button_tts_disable = 2131034121;
        public static final int button_tts_install = 2131034120;
        public static final int copyright = 2131034117;
        public static final int error_tts_init = 2131034146;
        public static final int expired = 2131034125;
        public static final int info_general = 2131034151;
        public static final int language = 2131034153;
        public static final int menu_about = 2131034131;
        public static final int menu_browse = 2131034129;
        public static final int menu_grammar = 2131034130;
        public static final int menu_help = 2131034133;
        public static final int menu_preferences = 2131034132;
        public static final int menu_search = 2131034127;
        public static final int menu_test = 2131034128;
        public static final int more_verbs_in_pro_version = 2131034123;
        public static final int new_search = 2131034124;
        public static final int no_results = 2131034122;
        public static final int pref_background = 2131034142;
        public static final int pref_background_dark = 2131034143;
        public static final int pref_background_lite = 2131034144;
        public static final int pref_columns = 2131034139;
        public static final int pref_ignore_accents = 2131034140;
        public static final int pref_remember_zoom = 2131034145;
        public static final int pref_search_translations = 2131034134;
        public static final int pref_show_pronouns = 2131034135;
        public static final int pref_show_title = 2131034136;
        public static final int pref_show_translation = 2131034137;
        public static final int pref_tense_selection = 2131034141;
        public static final int pref_use_tts = 2131034138;
        public static final int terms_of_service = 2131034116;
        public static final int text_columns = 2131034147;
        public static final int text_pronouns = 2131034148;
        public static final int title_browse = 2131034155;
        public static final int title_search = 2131034156;
        public static final int title_tips_and_tricks = 2131034150;
        public static final int title_view = 2131034149;
        public static final int tts_language = 2131034154;
        public static final int update_available = 2131034126;
    }
}
