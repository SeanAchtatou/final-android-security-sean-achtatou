package org.muth.android.conjugator_demo_pt;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Vector;
import java.util.logging.Logger;
import org.muth.android.base.PreferenceHelper;
import org.muth.android.base.Tracker;
import org.muth.android.base.UpdateHelper;
import org.muth.android.base.ViewAdapter;
import org.muth.android.verbs.VerbDatabase;
import org.muth.android.verbs.VerbRenderer;
import org.muth.android.verbs.VerbRendererSearch;

public class ActivitySearch extends ListActivity {
    /* access modifiers changed from: private */
    public static Logger logger = Logger.getLogger("conj");
    private MenuHelper mMenuHelper;
    private PreferenceHelper mPrefs;
    private VerbRenderer mRenderer;
    private VerbRendererSearch mRendererSearch;

    /* access modifiers changed from: protected */
    public void EditMode() {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.logo);
        linearLayout.addView(imageView, layoutParams);
        EditText editText = new EditText(this);
        editText.setSingleLine();
        editText.setHint((int) R.string.title_search);
        editText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((EditText) view).setText("");
            }
        });
        imageView.setAdjustViewBounds(true);
        int textSize = (int) (2.5d * ((double) editText.getTextSize()));
        imageView.setMaxHeight(textSize);
        imageView.setMaxWidth(textSize);
        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                EditText editText = (EditText) view;
                ActivitySearch.logger.info("key event");
                if (keyEvent.getAction() == 0) {
                    switch (i) {
                        case 66:
                            String lowerCase = editText.getText().toString().trim().toLowerCase();
                            if (!lowerCase.equals("")) {
                                ActivitySearch.this.ShowResult(lowerCase);
                                Tracker.TrackEvent("Verb-" + ActivitySearch.this.getString(R.string.language), "Search", lowerCase, 0);
                            }
                            return true;
                    }
                }
                return false;
            }
        });
        linearLayout.addView(editText, new LinearLayout.LayoutParams(-1, -2));
        logger.info("edit hot");
        setListAdapter(null);
        getListView().addHeaderView(linearLayout);
        setListAdapter(new ViewAdapter(new Vector(0)));
    }

    /* access modifiers changed from: private */
    public void ShowResult(String str) {
        String str2;
        String str3;
        logger.info("Looking up form [" + str + "]");
        Tracker.TrackEvent("Click", "Search", str, 0);
        this.mPrefs.getPreferenceBool("Search:Misc:UsePronouns");
        Vector<View> renderSearch = this.mRendererSearch.renderSearch(str, this.mPrefs.getPreferenceBool("Search:Misc:AlternativeSpelling"), this.mPrefs.getPreferenceBool("Search:Misc:SearchTranslation"), this);
        if (renderSearch == null) {
            logger.info("nothing found");
            String str4 = "\n" + getString(R.string.no_results);
            if (getPackageName().indexOf("_pro_") < 0) {
                str2 = str4 + "\n\n" + getString(R.string.more_verbs_in_pro_version);
                str3 = "@url@" + this.mPrefs.getStringPreference("Ad:Misc:Url");
            } else {
                str2 = str4;
                str3 = "@ignore";
            }
            Vector<View> vector = new Vector<>(5);
            TextView textView = new TextView(this);
            textView.setText(str2);
            textView.setTextAppearance(this, 16973890);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setTag(str3);
            textView.setTextColor(-1);
            vector.add(textView);
            renderSearch = vector;
        }
        setListAdapter(new ViewAdapter(renderSearch));
    }

    /* access modifiers changed from: protected */
    public PreferenceHelper GetPreferences() {
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        preferenceHelper.AddPref("Search", "Misc", "AlternativeSpelling", "true", 3);
        preferenceHelper.AddPref("Search", "Misc", "UsePronouns", "true", 3);
        preferenceHelper.AddPref("Search", "Misc", "SearchTranslation", "true", 3);
        preferenceHelper.MaybeInitPrefs();
        return preferenceHelper;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        requestWindowFeature(1);
        super.onCreate(bundle);
        String[] split = getPackageName().split("[.]");
        Tracker.StartTracker("UA-16819459-1", "/" + split[split.length - 1], 60, this.mPrefs, this);
        Tracker.TrackPage(this);
        Intent intent = getIntent();
        logger.info("activity search " + intent.getAction() + " " + intent.getData());
        this.mPrefs = GetPreferences();
        this.mMenuHelper = new MenuHelper(this, "ActivitySearchPrefs");
        if (!UpdateHelper.checkForExpiration(this, this.mPrefs, getString(R.string.expired), getString(R.string.button_ok))) {
            VerbDatabase GetSingleton = VerbDatabase.GetSingleton(getResources().openRawResource(R.raw.str), getResources().openRawResource(R.raw.vf), UpdateHelper.getPhoneLanguage(this), null);
            this.mRenderer = VerbRenderer.GetSingleton(GetSingleton, getString(R.string.language));
            this.mRendererSearch = new VerbRendererSearch(GetSingleton, this.mRenderer);
            EditMode();
            if (intent.getData() != null) {
                str = intent.getData().getHost();
            } else {
                str = "";
            }
            if (!str.equals("")) {
                ShowResult(str);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        String str = (String) view.getTag();
        logger.warning("search click on " + i + " " + str);
        if (i != 0) {
            if (str.startsWith("@url@")) {
                Tracker.TrackPage("/NotFound");
            }
            ClickHelper.handleMenu(this, str);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.mMenuHelper.Register(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        this.mMenuHelper.HandleSelection(menuItem);
        return true;
    }
}
