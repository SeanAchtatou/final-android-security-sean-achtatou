package org.muth.android.conjugator_demo_pt;

import android.app.Activity;
import org.muth.android.base.BaseMenuHelper;

public class MenuHelper extends BaseMenuHelper {
    public MenuHelper(Activity activity, String str) {
        super(activity);
        AddAction(new BaseMenuHelper.MenuActionIntend(R.string.menu_browse, '2', 'b', "ActivityBrowse", "", 17301660));
        AddAction(new BaseMenuHelper.MenuActionIntend(R.string.menu_search, '7', 's', "ActivitySearch", "", 17301583));
        AddAction(new BaseMenuHelper.MenuActionIntend(R.string.menu_about, '*', 'a', "ActivityAbout", "", 17301569));
        AddAction(new BaseMenuHelper.MenuActionIntend(R.string.menu_help, '1', 'h', "ActivityGrammar", "local://local/help.html", 17301568));
        AddAction(new BaseMenuHelper.MenuActionIntend(R.string.menu_grammar, '4', 'g', "ActivityGrammar", "local://local/index.html", 17301566));
        if (str != null) {
            AddAction(new BaseMenuHelper.MenuActionIntend(R.string.menu_preferences, '#', 'p', str, "", 17301577));
        }
    }
}
