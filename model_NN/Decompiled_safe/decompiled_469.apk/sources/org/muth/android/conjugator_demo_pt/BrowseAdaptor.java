package org.muth.android.conjugator_demo_pt;

import android.app.Activity;
import java.util.Iterator;
import java.util.logging.Logger;
import org.muth.android.verbs.VerbListAdaptor;
import org.muth.android.verbs.VerbRenderer;

/* compiled from: ActivityBrowse */
class BrowseAdaptor extends VerbListAdaptor {
    private static Logger logger = Logger.getLogger("conj");

    public BrowseAdaptor(Activity activity, VerbRenderer verbRenderer) {
        super(verbRenderer);
        logger.info("creating browser adator");
        long currentTimeMillis = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder(50);
        Iterator<String> it = verbRenderer.getImportantVerbs().iterator();
        while (it.hasNext()) {
            String next = it.next();
            sb.setLength(0);
            verbRenderer.renderTitle(sb, verbRenderer.GetVerbId(next), true, true);
            AddEntry(sb.toString(), "@act@ActivityView@http://" + next);
        }
        AddEntry("", "@ignore");
        fillVerbs();
        logger.info("adapter creation took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
    }
}
