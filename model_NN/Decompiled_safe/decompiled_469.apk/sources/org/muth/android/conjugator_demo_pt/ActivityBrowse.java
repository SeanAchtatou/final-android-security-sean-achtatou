package org.muth.android.conjugator_demo_pt;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.muth.android.base.AdButton;
import org.muth.android.base.PreferenceHelper;
import org.muth.android.base.SplashDialog;
import org.muth.android.base.Tracker;
import org.muth.android.base.UpdateHelper;
import org.muth.android.verbs.VerbDatabase;
import org.muth.android.verbs.VerbRenderer;

public class ActivityBrowse extends ListActivity {
    private static Logger logger = Logger.getLogger("conj");
    private AdButton mAd;
    private AdButton mAd2;
    private BrowseAdaptor mAdapter;
    private Random mGenerator;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private long mKeyboardLastEvent = 0;
    private String mKeyboardPrefix = "";
    private MenuHelper mMenuHelper;
    private PreferenceHelper mPrefs;
    private VerbRenderer mRenderer;
    /* access modifiers changed from: private */
    public SplashDialog mSplash;

    /* access modifiers changed from: private */
    public void loadDatabase(Runnable runnable) {
        this.mRenderer = VerbRenderer.GetSingleton(VerbDatabase.GetSingleton(getResources().openRawResource(R.raw.str), getResources().openRawResource(R.raw.vf), UpdateHelper.getPhoneLanguage(this), runnable), getString(R.string.language));
        logger.info("db load complete");
    }

    /* access modifiers changed from: private */
    public void prepareAdaptor() {
        this.mAdapter = new BrowseAdaptor(this, this.mRenderer);
        logger.info("setting browse adapter");
        setListAdapter(this.mAdapter);
    }

    private PreferenceHelper InitPreferences() {
        new PreferenceHelper(this);
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        preferenceHelper.AddPref("Dialog", "Misc", "LastTipShowing", "2009/01/01", 4);
        preferenceHelper.MaybeInitPrefs();
        return preferenceHelper;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mSplash.Abort();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mAd.Update(this.mPrefs);
        this.mAd2.Update(this.mPrefs);
        Toast.makeText(this, (int) R.string.info_general, 1);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Logger.getLogger("conj").getParent().setLevel(Level.OFF);
        requestWindowFeature(1);
        Intent intent = getIntent();
        setDefaultKeyMode(2);
        logger.info("@@ creating browser " + intent.getAction() + " " + intent.getData());
        this.mGenerator = new Random();
        this.mPrefs = InitPreferences();
        String[] split = getPackageName().split("[.]");
        Tracker.StartTracker("UA-16819459-1", "/" + split[split.length - 1], 60, this.mPrefs, this);
        Tracker.TrackPage(this);
        this.mMenuHelper = new MenuHelper(this, null);
        if (!UpdateHelper.startupChecks(this, this.mPrefs, getString(R.string.update_available), getString(R.string.expired), getString(R.string.button_ok))) {
            getListView().setFastScrollEnabled(true);
            this.mAd = new AdButton(this, "Ad-conj-" + getString(R.string.language));
            getListView().addHeaderView(this.mAd.GetButton());
            this.mAd2 = new AdButton(this, "Ad-conj-" + getString(R.string.language));
            getListView().addFooterView(this.mAd2.GetButton());
            this.mSplash = new SplashDialog(this, R.drawable.logo, getString(R.string.app_name) + "\n" + getString(R.string.copyright), 100, 4);
            this.mHandler = new Handler();
            this.mSplash.MakeRunnableStart().run();
            new Thread() {
                public void run() {
                    ActivityBrowse.this.loadDatabase(ActivityBrowse.this.mSplash.MakeRunnableInc());
                    ActivityBrowse.this.mHandler.post(new Runnable() {
                        public void run() {
                            ActivityBrowse.this.prepareAdaptor();
                        }
                    });
                    ActivityBrowse.this.mHandler.post(ActivityBrowse.this.mSplash.MakeRunnableStop());
                }
            }.start();
            logger.info("last tip: " + this.mPrefs.getStringPreference("Dialog:Misc:LastTipShowing"));
            if (this.mPrefs.getPreferenceAgeInMSecs("Dialog:Misc:LastTipShowing") / 86400000 > 1) {
                this.mPrefs.setPreferenceTimeNow("Dialog:Misc:LastTipShowing");
                String[] split2 = getString(R.string.info_general).split("@");
                Toast.makeText(this, split2[this.mGenerator.nextInt(split2.length)], 1).show();
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        logger.warning("key pressed " + i);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis > this.mKeyboardLastEvent + 1000) {
            this.mKeyboardPrefix = "";
        }
        if (keyEvent.getAction() != 0 || 29 > i || i > 54) {
            if (keyEvent.getAction() == 0 && 84 == i) {
                String packageName = getPackageName();
                Intent intent = new Intent();
                intent.setType("");
                intent.setClassName(packageName, packageName + "." + "ActivitySearch");
                try {
                    startActivity(intent);
                } catch (Exception e) {
                    logger.info("error " + e.toString());
                }
            }
            return super.onKeyDown(i, keyEvent);
        }
        if (currentTimeMillis > this.mKeyboardLastEvent + 1000) {
            this.mKeyboardPrefix = "";
        }
        this.mKeyboardLastEvent = currentTimeMillis;
        this.mKeyboardPrefix += ((char) ((i - 29) + 97));
        logger.info("looking up prefix " + this.mKeyboardPrefix);
        setSelection(this.mAdapter.findFirstPosStartingWith(this.mKeyboardPrefix));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        String str = (String) view.getTag();
        logger.warning("browser click on " + i + " " + str);
        ClickHelper.handleMenu(this, str);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.mMenuHelper.Register(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        this.mMenuHelper.HandleSelection(menuItem);
        return true;
    }
}
