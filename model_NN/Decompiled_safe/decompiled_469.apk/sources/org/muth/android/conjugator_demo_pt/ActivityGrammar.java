package org.muth.android.conjugator_demo_pt;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.logging.Logger;
import org.muth.android.base.HtmlArchive;
import org.muth.android.base.PreferenceHelper;
import org.muth.android.base.Tracker;
import org.muth.android.base.UpdateHelper;

public class ActivityGrammar extends Activity {
    private static Logger logger = Logger.getLogger("conj");
    private HtmlArchive mHtmlArchive;
    private PreferenceHelper mPrefs;

    /* access modifiers changed from: protected */
    public void loadUrlFancy(WebView webView, String str, boolean z) {
        Tracker.TrackEvent("Click", "Url", str, 0);
        logger.info("url load: " + str);
        if (!str.startsWith("local://local/")) {
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } catch (Exception e) {
                logger.severe("could not launch: " + str);
            }
        } else if (z) {
            String packageName = getPackageName();
            try {
                startActivity(new Intent().setType(str).setClassName(packageName, packageName + "." + "ActivityGrammar"));
            } catch (Exception e2) {
                logger.severe("error starting intend: " + e2.toString());
            }
        } else {
            webView.loadDataWithBaseURL("local://local/", new String(this.mHtmlArchive.getHtmlData(str.substring(14))), "text/html", "utf-8", null);
        }
    }

    public void Log(String str) {
        logger.info("JS: " + str);
    }

    public String Language() {
        return getString(R.string.language);
    }

    public boolean IsDemo() {
        return UpdateHelper.appIsDemoVersion(this);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Tracker.TrackPage(this);
        this.mPrefs = new PreferenceHelper(this);
        this.mPrefs.MaybeInitPrefs();
        this.mHtmlArchive = new HtmlArchive(getResources().openRawResource(R.raw.html), UpdateHelper.getPhoneLanguage(this));
        String type = getIntent().getType();
        logger.info("activity grammar " + type);
        WebView webView = new WebView(this);
        webView.addJavascriptInterface(this, "myPrefs");
        webView.getSettings().setJavaScriptEnabled(true);
        WebSettings settings = webView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                ActivityGrammar.this.loadUrlFancy(webView, str, true);
                return true;
            }
        });
        requestWindowFeature(1);
        loadUrlFancy(webView, type, false);
        setContentView(webView);
    }
}
