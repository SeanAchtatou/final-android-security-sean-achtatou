package org.muth.android.conjugator_demo_pt;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Iterator;
import java.util.logging.Logger;
import org.muth.android.base.PreferenceHelper;
import org.muth.android.base.Tracker;
import org.muth.android.base.TtsHelper;
import org.muth.android.base.UpdateHelper;
import org.muth.android.verbs.VerbDatabase;
import org.muth.android.verbs.VerbRenderer;

public class ActivityView extends Activity {
    private static final String Epilog = "<h3>&nbsp;</h3></body></html>";
    private static Logger logger = Logger.getLogger("conj");
    private MenuHelper mMenuHelper;
    /* access modifiers changed from: private */
    public PreferenceHelper mPrefs;
    private VerbRenderer mRenderer;
    private TtsHelper mTts = null;
    private String mVerbname;
    private WebView mWebView;

    private String GetProlog(String str) {
        StringBuilder sb = new StringBuilder(1024);
        sb.append("<html><head>");
        sb.append("<style type=text/css>.tense {font-weight: bold; font-size: 110%; white-space: no-wrap; color: olive;}.i {color: #dd0000;}.s {color: #00dd00;}.o {color: #2020dd;}.a {text-decoration: line-through;}.u {text-decoration: line-through;}.reg {color: black;}.audio {background-repeat: no-repeat;        background-position:center;        background-size: 100%;        display: inline-block;        height: .8em; width: .8em;}");
        if (str.equals("dark")) {
            sb.append("body { color: #eeeeee;}.reg {color: #dddddd;}.audio {background-image: url(file:///android_asset/speaker_grey.png);}");
        } else {
            sb.append(".reg {color: black;}.audio {background-image: url(file:///android_asset/speaker_black.png);}");
        }
        if (Build.VERSION.SDK_INT < 8) {
            sb.append(".audio { display: none; }");
        } else {
            sb.append(".audio { display: inline-block; }");
        }
        sb.append("</style>");
        sb.append("<script type='text/javascript'>function Words(s) { window.Activity.ReadWords(s); }function Tense(s) { window.Activity.ShowTense(s); }</script></head><body>");
        return sb.toString();
    }

    public void ReadWords(String str) {
        logger.info("ReadWords [" + str + "]");
        if (this.mTts != null) {
            this.mTts.Speak(str);
        }
    }

    public void ShowTense(String str) {
        logger.info("ShowTense [" + str + "]");
    }

    private void HandleUnknownVerb() {
        if (UpdateHelper.appIsDemoVersion(this)) {
            Button button = new Button(this);
            button.setText(((Object) getText(R.string.no_results)) + ":\n" + this.mVerbname + "\n\n" + getString(R.string.more_verbs_in_pro_version));
            button.setTypeface(Typeface.DEFAULT_BOLD);
            button.setBackgroundColor(-3407872);
            button.setTextAppearance(this, 16973890);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    UpdateHelper.launchUrlApp(this, ActivityView.this.mPrefs.getStringPreference("Ad:Misc:Url"));
                }
            });
            setContentView(button);
            return;
        }
        TextView textView = new TextView(this);
        textView.setText(((Object) getText(R.string.no_results)) + ":\n" + this.mVerbname);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextAppearance(this, 16973890);
        setContentView(textView);
    }

    private void UpdateView() {
        long currentTimeMillis = System.currentTimeMillis();
        String renderVerbHtml = this.mRenderer.renderVerbHtml(this.mVerbname, this.mPrefs.getStringPreferenceSet("View:Tenses:"), this.mPrefs.getPreferenceInt("View:Misc:DispColumns"), this.mPrefs.getPreferenceBool("View:Misc:ShowTitle"), this.mPrefs.getPreferenceBool("View:Misc:ShowTranslation"), this.mPrefs.getPreferenceBool("View:Misc:UsePronouns"), this.mPrefs.getPreferenceBool("View:Misc:UseTTS"));
        logger.info("page generation took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        if (renderVerbHtml == null) {
            HandleUnknownVerb();
            return;
        }
        String stringPreference = this.mPrefs.getStringPreference("View:Misc:Background");
        String GetProlog = GetProlog(stringPreference);
        if (stringPreference.equals("dark")) {
            this.mWebView.setBackgroundColor(-16777216);
        } else {
            this.mWebView.setBackgroundColor(-1118482);
        }
        this.mWebView.addJavascriptInterface(this, "Activity");
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.loadDataWithBaseURL("local://local/", GetProlog + renderVerbHtml + Epilog, "text/html", "utf-8", null);
        if (this.mPrefs.getPreferenceBool("View:Misc:RememberScale")) {
            this.mWebView.setInitialScale(this.mPrefs.getPreferenceInt("View:Misc:SavedScale"));
        }
    }

    /* access modifiers changed from: protected */
    public PreferenceHelper GetPreferences() {
        PreferenceHelper preferenceHelper = new PreferenceHelper(this);
        preferenceHelper.AddPref("View", "Misc", "DispColumns", "2", 2);
        preferenceHelper.AddPref("View", "Misc", "DispLang", "", 1);
        preferenceHelper.AddPref("View", "Misc", "UsePronouns", "false", 3);
        preferenceHelper.AddPref("View", "Misc", "ShowTitle", "true", 3);
        preferenceHelper.AddPref("View", "Misc", "ShowTranslation", "true", 3);
        preferenceHelper.AddPref("View", "Misc", "UseTTS", "true", 3);
        preferenceHelper.AddPref("View", "Misc", "Background", "dark", 1);
        preferenceHelper.AddPref("View", "Misc", "SavedScale", "100", 2);
        preferenceHelper.AddPref("View", "Misc", "RememberScale", "true", 3);
        Iterator<String> it = this.mRenderer.getTenseList().iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next.equals("ImperfectSubjunctiveSe")) {
                preferenceHelper.AddPref("View", "Tenses", next, "false", 3);
            } else {
                preferenceHelper.AddPref("View", "Tenses", next, "true", 3);
            }
        }
        preferenceHelper.MaybeInitPrefs();
        return preferenceHelper;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        UpdateView();
        if (!this.mPrefs.getPreferenceBool("View:Misc:UseTTS")) {
            this.mTts = null;
        } else if (this.mTts == null) {
            this.mTts = new TtsHelper(this, getString(R.string.language), getString(R.string.button_tts_disable), getString(R.string.button_ok), getString(R.string.button_tts_install), getString(R.string.error_tts_init)) {
                public void DisableCallback() {
                    ActivityView.this.mPrefs.setBooleanPreference("View:Misc:UseTTS", false);
                }
            };
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mPrefs.setIntPreference("View:Misc:SavedScale", (int) (100.0f * this.mWebView.getScale()));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mVerbname = getIntent().getData().getHost();
        logger.info("@@ viewer action " + this.mVerbname);
        requestWindowFeature(1);
        String[] split = getPackageName().split("[.]");
        Tracker.StartTracker("UA-16819459-1", "/" + split[split.length - 1], 60, this.mPrefs, this);
        Tracker.TrackPage(this);
        Tracker.TrackEvent("Verb-" + getString(R.string.language), "View", this.mVerbname, 0);
        this.mRenderer = VerbRenderer.GetSingleton(VerbDatabase.GetSingleton(getResources().openRawResource(R.raw.str), getResources().openRawResource(R.raw.vf), UpdateHelper.getPhoneLanguage(this), null), getString(R.string.language));
        this.mPrefs = GetPreferences();
        this.mMenuHelper = new MenuHelper(this, "ActivityViewPrefs");
        this.mWebView = new WebView(this);
        WebSettings settings = this.mWebView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        setContentView(this.mWebView);
    }

    private static LinearLayout.LayoutParams Fraction(float f) {
        return new LinearLayout.LayoutParams(-1, -1, f);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.mMenuHelper.Register(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        this.mMenuHelper.HandleSelection(menuItem);
        return true;
    }
}
