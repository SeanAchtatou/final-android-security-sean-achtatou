package org.muth.android.conjugator_demo_pt;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.logging.Logger;
import org.muth.android.base.BaseActivityAbout;
import org.muth.android.base.PreferenceHelper;
import org.muth.android.base.Tracker;
import org.muth.android.base.UpdateHelper;
import org.muth.android.verbs.VerbRenderer;

public class ActivityAbout extends Activity {
    private static Logger logger = Logger.getLogger("base");
    private PreferenceHelper mPrefs;

    /* access modifiers changed from: protected */
    public String GetExtraInfo() {
        return "Storage: " + UpdateHelper.appStorageDir(this) + "\n" + VerbRenderer.GetSingleton(null, null).Info();
    }

    /* access modifiers changed from: protected */
    public void Hookup(PreferenceHelper preferenceHelper) {
        logger.info("gui hook up");
        final String str = "market://details?id=" + getPackageName();
        ((Button) findViewById(R.id.about_rate_app)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Tracker.TrackPage("/AboutRateApp");
                UpdateHelper.launchUrlApp(this, str);
            }
        });
        final String MakeEmailUrl = BaseActivityAbout.MakeEmailUrl(this.mPrefs, this, getString(R.string.app_name));
        ((Button) findViewById(R.id.about_email_author)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Tracker.TrackPage("/AboutEmailAuthor");
                UpdateHelper.launchUrlApp(this, MakeEmailUrl);
            }
        });
        final String stringPreference = this.mPrefs.getStringPreference("Debug:Misc:UrlMarket");
        ((Button) findViewById(R.id.about_more_apps)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Tracker.TrackPage("/AboutMoreApps");
                UpdateHelper.launchUrlApp(this, stringPreference);
            }
        });
        final String MakeTellUrl = BaseActivityAbout.MakeTellUrl(this, getString(R.string.app_name));
        ((Button) findViewById(R.id.about_tell_a_fried)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Tracker.TrackPage("/AboutTellFriend");
                UpdateHelper.launchUrlApp(this, MakeTellUrl);
            }
        });
        ((TextView) findViewById(R.id.about_info)).setText(BaseActivityAbout.GetStandardInfo(this.mPrefs, this));
        ((TextView) findViewById(R.id.about_extra)).setText(GetExtraInfo());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        logger.info("@@ activity about");
        Tracker.TrackPage(this);
        System.gc();
        this.mPrefs = new PreferenceHelper(this);
        this.mPrefs.MaybeInitPrefs();
        UpdateHelper.refreshUpdateInfoBackground(this, this.mPrefs, true);
        setContentView((int) R.layout.about);
        Hookup(this.mPrefs);
    }
}
