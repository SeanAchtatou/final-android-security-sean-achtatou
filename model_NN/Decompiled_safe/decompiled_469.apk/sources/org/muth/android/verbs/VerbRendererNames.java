package org.muth.android.verbs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;
import junit.framework.Assert;

/* compiled from: VerbRenderer */
class VerbRendererNames {
    static final /* synthetic */ boolean $assertionsDisabled = (!VerbRendererNames.class.desiredAssertionStatus());
    private static Logger logger = Logger.getLogger("verbs");
    private final HashMap<String, String[]> EXPAND_TENSES_MAP = new HashMap<>(50);
    private final Vector<String> FORM_SEQUENCE = new Vector<>(10);
    private final HashMap<String, String> MAP_FORM_TO_PRONOUN = new HashMap<>(10);
    private final HashMap<String, String> MAP_PRONOUN_TO_FORM = new HashMap<>(10);
    private final HashMap<String, String> TENSES_NAME = new HashMap<>(50);
    private final HashMap<String, String> TENSES_NAME_LONG = new HashMap<>(50);
    private final Vector<String> TENSES_SEQUENCE = new Vector<>(100);
    private final HashSet<String> TENSES_WITHOUT_PRONOUNS = new HashSet<>(50);

    private void addTranslation(String str, String str2, String str3) {
        this.TENSES_NAME.put(str2, str3);
        this.TENSES_NAME_LONG.put(str2, str3);
    }

    private void addTense(String str, String str2, String str3) {
        this.TENSES_NAME.put(str2, str3);
        this.TENSES_NAME_LONG.put(str2, str3);
        this.TENSES_SEQUENCE.add(str2);
    }

    private void addTense2(String str, String str2, String str3, String str4) {
        this.TENSES_NAME.put(str2, str3);
        this.TENSES_NAME_LONG.put(str2, str4);
        this.TENSES_SEQUENCE.add(str2);
    }

    private void addPronoun(String str, String str2, String str3) {
        this.MAP_FORM_TO_PRONOUN.put(str2, str3);
        this.MAP_PRONOUN_TO_FORM.put(str3, str2);
        this.FORM_SEQUENCE.add(str2);
    }

    private void addTenseExpansion(String str, String... strArr) {
        this.EXPAND_TENSES_MAP.put(str, strArr);
    }

    public VerbRendererNames(String str) {
        addTenseExpansion("Participle", "@pronno", "@conj@Participle");
        addTenseExpansion("PresentParticiple", "@pronno", "@conj@PresentParticiple");
        addTenseExpansion("Gerund", "@pronno", "@conj@Gerund");
        addTenseExpansion("Infinitive", "@pronno", "@conj@Infinitive");
        addTenseExpansion("Translation", "@conj@Translation");
        addTenseExpansion("Present", "@pron", "@conj@Present");
        addTenseExpansion("Preterite", "@pron", "@conj@Preterite");
        addTenseExpansion("Imperfect", "@pron", "@conj@Imperfect");
        addTenseExpansion("Future", "@pron", "@conj@Future");
        addTenseExpansion("Conditional", "@pron", "@conj@Conditional");
        addTenseExpansion("Imperative", "@pronopt", "@conj@Imperative");
        addTenseExpansion("PersonalInfinitive", "@pron", "@conj@PersonalInfinitive");
        addTenseExpansion("Pluperfect", "@pron", "@conj@Pluperfect");
        addTenseExpansion("PersonalInfinitivePerfect", "@pron", "@conjaux@PersonalInfinitive", "@first@Participle");
        addTenseExpansion("PresentSubjunctive", "@pron", "@conj@PresentSubjunctive");
        addTenseExpansion("PreteriteSubjunctive", "@pron", "@conj@PreteriteSubjunctive");
        addTenseExpansion("ImperfectSubjunctive", "@pron", "@conj@ImperfectSubjunctive");
        addTenseExpansion("FutureSubjunctive", "@pron", "@conj@FutureSubjunctive");
        addTenseExpansion("PresentPerfect", "@pron", "@conjaux@Present", "@first@Participle");
        addTenseExpansion("PreteritePerfect", "@pron", "@conjaux@Preterite", "@first@Participle");
        addTenseExpansion("PlusquamPerfect", "@pron", "@conjaux@Imperfect", "@first@Participle");
        addTenseExpansion("FuturePerfect", "@pron", "@conjaux@Future", "@first@Participle");
        addTenseExpansion("ConditionalPerfect", "@pron", "@conjaux@Conditional", "@first@Participle");
        addTenseExpansion("PresentPerfectSubjunctive", "@pron", "@conjaux@PresentSubjunctive", "@first@Participle");
        addTenseExpansion("PreteritePerfectSubjunctive", "@pron", "@conjaux@PreteriteSubjunctive", "@first@Participle");
        addTenseExpansion("PlusquamPerfectSubjunctive", "@pron", "@conjaux@ImperfectSubjunctive", "@first@Participle");
        addTenseExpansion("FuturePerfectSubjunctive", "@pron", "@conjaux@FutureSubjunctive", "@first@Participle");
        addTenseExpansion("PlusquamPerfectSubjunctiveRa", "@pron", "@conjaux@ImperfectSubjunctiveRa", "@first@Participle");
        addTenseExpansion("PlusquamPerfectSubjunctiveSe", "@pron", "@conjaux@ImperfectSubjunctiveSe", "@first@Participle");
        addTenseExpansion("ImperfectSubjunctiveRa", "@pron", "@conj@ImperfectSubjunctiveRa");
        addTenseExpansion("ImperfectSubjunctiveSe", "@pron", "@conj@ImperfectSubjunctiveSe");
        addTenseExpansion("FutureGerman", "@pron", "@conjwerden@Present", "@first@Infinitive");
        addTenseExpansion("ConditionalGerman", "@pron", "@conjwerden@PreteriteSubjunctive", "@first@Infinitive");
        addTenseExpansion("FuturePerfectGerman", "@pron", "@conjwerden@Present", "@first@Participle", "@firstaux@Infinitive");
        addTenseExpansion("FutureEnglish", "@pron", "@conjwill@Present", "@first@Infinitive");
        addTenseExpansion("PerfectEnglish", "@pron", "@conjhave@Present", "@first@Participle");
        addTenseExpansion("PluperfectEnglish", "@pron", "@conjhave@Preterite", "@first@Participle");
        addTenseExpansion("FuturePerfectEnglish", "@pron", "@conjwill@Present", "have", "@first@Participle");
        addTenseExpansion("ConditionalEnglish", "@pron", "@conjwill@Preterite", "@first@Infinitive");
        addTenseExpansion("ConditionalPerfectEnglish", "@pron", "@conjwill@Preterite", "have", "@first@Participle");
        addTenseExpansion("PresentContinuous", "@pron", "@conjbe@Present", "@first@Gerund");
        addTenseExpansion("PreteriteContinuous", "@pron", "@conjbe@Preterite", "@first@Gerund");
        addTenseExpansion("FutureContinuous", "@pron", "@conjwill@Present", "be", "@first@Gerund");
        addTenseExpansion("PerfectContinuous", "@pron", "@conjhave@Present", "been", "@first@Gerund");
        addTenseExpansion("PluperfectContinuous", "@pron", "@conjhave@Preterite", "been", "@first@Gerund");
        addTenseExpansion("FuturePerfectContinuous", "@pron", "@conjwill@Present", "have", "been", "@first@Gerund");
        addTenseExpansion("ConditionalContinuous", "@pron", "@conjwill@Preterite", "be", "@first@Gerund");
        addTenseExpansion("ConditionalPerfectContinuous", "@pron", "@conjwill@Preterite", "have", "been", "@first@Gerund");
        this.TENSES_WITHOUT_PRONOUNS.add("Participle");
        this.TENSES_WITHOUT_PRONOUNS.add("PresentParticiple");
        this.TENSES_WITHOUT_PRONOUNS.add("Gerund");
        this.TENSES_WITHOUT_PRONOUNS.add("Infinitive");
        this.TENSES_WITHOUT_PRONOUNS.add("Translation");
        if (str.equals("es")) {
            addPronoun("es", "1", "yo");
            addPronoun("es", "2", "tú");
            addPronoun("es", "3", "él");
            addPronoun("es", "4", "nosotros");
            addPronoun("es", "5", "vosotros");
            addPronoun("es", "6", "ellos");
        } else if (str.equals("de")) {
            addPronoun("de", "1", "ich");
            addPronoun("de", "2", "du");
            addPronoun("de", "3", "er");
            addPronoun("de", "4", "wir");
            addPronoun("de", "5", "ihr");
            addPronoun("de", "6", "sie");
        } else if (str.equals("en")) {
            addPronoun("de", "1", "I");
            addPronoun("de", "2", "you");
            addPronoun("de", "3", "he");
            addPronoun("de", "4", "we");
            addPronoun("de", "5", "you");
            addPronoun("de", "6", "they");
        } else if (str.equals("fr")) {
            addPronoun("fr", "1", "je");
            addPronoun("fr", "1x", "j'");
            addPronoun("fr", "2", "tu");
            addPronoun("fr", "3", "il");
            addPronoun("fr", "4", "nous");
            addPronoun("fr", "5", "vous");
            addPronoun("fr", "6", "ils");
        } else if (str.equals("it")) {
            addPronoun("it", "1", "io");
            addPronoun("it", "2", "tu");
            addPronoun("it", "3", "lui");
            addPronoun("it", "4", "noi");
            addPronoun("it", "5", "voi");
            addPronoun("it", "6", "loro");
        } else if (str.equals("pt")) {
            addPronoun("pt", "1", "eu");
            addPronoun("pt", "2", "tu");
            addPronoun("pt", "3", "ele");
            addPronoun("pt", "4", "nós");
            addPronoun("pt", "5", "vós");
            addPronoun("pt", "6", "eles");
        } else if (!$assertionsDisabled) {
            throw new AssertionError();
        }
        if (str.equals("es")) {
            addTense("es", "Present", "Presente");
            addTense("es", "Preterite", "Pretérito");
            addTense("es", "Imperfect", "Imperfecto");
            addTense("es", "Future", "Futuro");
            addTense("es", "Conditional", "Condicional");
            addTense("es", "Imperative", "Imperativo");
            addTense2("es", "PresentSubjunctive", "Presente Sub", "Presente Subjuntivo");
            addTense2("es", "ImperfectSubjunctiveRa", "Imp Sub Ra", "Imperfecto Subjuntivo Ra");
            addTense2("es", "ImperfectSubjunctiveSe", "Imp Sub Se", "Imperfecto Subjuntivo Se");
            addTense2("es", "FutureSubjunctive", "Futuro Sub", "Futuro Subjuntivo");
            addTense("es", "Gerund", "Gerundio");
            addTense("es", "Participle", "Participio");
            addTense("es", "Infinitive", "Infinitivo");
            addTense2("es", "PresentPerfect", "Presente Perf", "Presente Perfecto");
            addTense2("es", "PreteritePerfect", "Pretérito Ant", "Pretérito Anterior");
            addTense2("es", "PlusquamPerfect", "Pluscuamperf", "Pluscuamperfecto");
            addTense2("es", "FuturePerfect", "Futuro Perf", "Futuro Perfecto");
            addTense2("es", "ConditionalPerfect", "Condicional Perf", "Condicional Perfecto");
            addTense2("es", "PresentPerfectSubjunctive", "Presente Perf Sub", "Presente Perfecto Subjuntivo");
            addTense2("es", "PlusquamPerfectSubjunctiveSe", "Pluscuamperf Sub Se", "Pluscuamperfecto Subjuntivo Se");
            addTense2("es", "PlusquamPerfectSubjunctiveRa", "Pluscuamperf Sub Ra", "Pluscuamperfecto Subjuntivo Ra");
            addTense2("es", "FuturePerfectSubjunctive", "Futuro Perf Sub", "Futuro Perfecto Subjuntivo");
            addTranslation("es", "Translation", "Traducción");
        } else if (str.equals("de")) {
            addTense("de", "Present", "Präsens");
            addTense("de", "Preterite", "Präteritum");
            addTense("de", "FutureGerman", "Futur 1");
            addTense("de", "ConditionalGerman", "Konditional");
            addTense("de", "Imperative", "Imperativ");
            addTense("de", "PresentSubjunctive", "Konjunktiv 1");
            addTense("de", "PreteriteSubjunctive", "Konjunktiv 2");
            addTense("de", "Infinitive", "Infinitiv");
            addTense2("de", "Gerund", "Part Präsens", "Partizip Präsens");
            addTense2("de", "Participle", "Part Perfekt", "Partizip Perfekt");
            addTense("de", "PresentPerfect", "Perfekt");
            addTense("de", "PreteritePerfect", "Plusquamperfekt");
            addTense("de", "FuturePerfectGerman", "Futur 2");
            addTranslation("de", "Translation", "Übersetzung");
        } else if (str.equals("en")) {
            addTense("en", "Present", "Present");
            addTense("en", "Preterite", "Preterite");
            addTense("en", "FutureEnglish", "Future");
            addTense("en", "Infinitive", "Infinitive");
            addTense("en", "Gerund", "Gerund");
            addTense("en", "Participle", "Participle");
            addTense("en", "PerfectEnglish", "Present Perfect");
            addTense("en", "PluperfectEnglish", "Past Perfect");
            addTense("en", "FuturePerfectEnglish", "Future Perfect");
            addTense("en", "ConditionalEnglish", "Conditional");
            addTense("en", "ConditionalPerfectEnglish", "Conditional Perfect");
            addTense2("en", "PresentContinuous", "Present Cont", "Present Continuous");
            addTense2("en", "PreteriteContinuous", "Preterite Cont", "Preterite Continuous");
            addTense2("en", "FutureContinuous", "Future Cont", "Future Continuous");
            addTense2("en", "PerfectContinuous", "Perfect Cont", "Present Perfect Continuous");
            addTense2("en", "PluperfectContinuous", "Pluperfect Cont", "Past Perfect Continuous");
            addTense2("en", "FuturePerfectContinuous", "Future Perfect Cont", "Future Perfect Continuous");
            addTense2("en", "ConditionalContinuous", "Conditional Cont", "Conditional Continuous");
            addTense2("en", "ConditionalPerfectContinuous", "Conditional Perfect Cont", "Conditional Perfect Continuous");
            addTranslation("en", "Translation", "Translation");
        } else if (str.equals("fr")) {
            addTense("fr", "Present", "Présent");
            addTense("fr", "Preterite", "Passé Simple");
            addTense("fr", "Imperfect", "Imparfait");
            addTense("fr", "Future", "Futur");
            addTense2("fr", "Conditional", "Cond Présent", "Conditionnel Présent");
            addTense("fr", "Imperative", "Impératif");
            addTense2("fr", "PresentSubjunctive", "Présent Sub", "Présent Subjonctif");
            addTense2("fr", "ImperfectSubjunctive", "Imparfait Sub", "Imparfait Subjonctif");
            addTense("fr", "Gerund", "Participe Présent");
            addTense("fr", "Participle", "Participle Passé");
            addTense("fr", "Infinitive", "Infinitif");
            addTense("fr", "PresentPerfect", "Passé Composé");
            addTense("fr", "PreteritePerfect", "Pretérito Antérieur");
            addTense("fr", "PlusquamPerfect", "Plus-que-parfait");
            addTense("fr", "FuturePerfect", "Futur Antérieur");
            addTense2("fr", "ConditionalPerfect", "Cond Passé", "Conditionnel Passé");
            addTense2("fr", "PresentPerfectSubjunctive", "Passé Comp Sub", "Passé Composé Subjonctif");
            addTense2("fr", "PlusquamPerfectSubjunctive", "Plus-que-parfait Sub", "Plus-que-parfait Subjonctif");
            addTranslation("fr", "Translation", "Traduction");
        } else if (str.equals("it")) {
            addTense("it", "Present", "Presente");
            addTense("it", "Preterite", "Passato Remoto");
            addTense("it", "Imperfect", "Imperfetto");
            addTense("it", "Future", "Futuro");
            addTense("it", "Conditional", "Condizionale");
            addTense("it", "Imperative", "Imperativo");
            addTense2("it", "PresentSubjunctive", "Presente Cong", "Presente Congiuntivo");
            addTense2("it", "ImperfectSubjunctive", "Imperfetto Cong", "Imperfetto Congiuntivo");
            addTense("it", "Gerund", "Gerundio");
            addTense2("it", "PresentParticiple", "Part Presente", "Participio Presente");
            addTense2("it", "Participle", "Part Passato", "Participio Passato");
            addTense("it", "Infinitive", "Infinito");
            addTense2("it", "PresentPerfect", "Pass Prossimo", "Passato Prossimo");
            addTense2("it", "PlusquamPerfect", "Tra Prossimo", "Trapassato Prossimo");
            addTense2("it", "PreteritePerfect", "Tra Remoto", "Trapassato Remoto");
            addTense("it", "FuturePerfect", "Future Anteriore");
            addTense2("it", "ConditionalPerfect", "Passato Cond", "Passato Condizionale");
            addTense2("it", "PresentPerfectSubjunctive", "Passato Conj", "Passato Congiuntivo");
            addTense2("it", "PlusquamPerfectSubjunctive", "Tra Conj", "Trapassato Congiuntivo");
            addTranslation("it", "Translation", "Traduzione");
        } else if (str.equals("pt")) {
            addTense("pt", "Present", "Presente");
            addTense2("pt", "Preterite", "Pret Perfeito", "Pretérito Perfeito");
            addTense2("pt", "Pluperfect", "Pret Mais-que-p", "Pretérito Mais-que-perfeito");
            addTense2("pt", "Imperfect", "Pret Imperfeito", "Pretérito Imperfeito");
            addTense("pt", "Future", "Futuro");
            addTense("pt", "Conditional", "Condicional");
            addTense2("pt", "PresentSubjunctive", "Presente Sub", "Presente Subjuntivo");
            addTense2("pt", "ImperfectSubjunctive", "Imperfeito Sub", "Imperfeito Subjuntivo");
            addTense2("pt", "FutureSubjunctive", "Futuro Sub", "Futuro Subjuntivo");
            addTense2("pt", "PersonalInfinitive", "Infinitivo Pes", "Infinitivo Pessoal");
            addTense("pt", "Imperative", "Imperativo");
            addTense("pt", "Gerund", "Gerúndio");
            addTense("pt", "Participle", "Particípio");
            addTense("pt", "Infinitive", "Infinitivo");
            addTense2("pt", "PresentPerfect", "Pret Perf Comp", "Pretérito Perfeito Composto");
            addTense2("pt", "PlusquamPerfect", "Pret Mais-que-p Comp", "Pretérito Mais-que-perfeito Composto");
            addTense2("pt", "FuturePerfect", "Futuro Comp", "Futuro Composto");
            addTense2("pt", "ConditionalPerfect", "Cond Comp", "Condicional Composto");
            addTense2("pt", "PresentPerfectSubjunctive", "Pret Perf Comp Subj", "Pretérito Perfeito Composto Subjuntivo");
            addTense2("pt", "PlusquamPerfectSubjunctive", "Pret Mais-que-p Comp Subj", "Pretérito Mais-que-perfeito Composto Subjuntivo");
            addTense2("pt", "FuturePerfectSubjunctive", "Futuro Comp Sub", "Futuro Composto Subjuntivo");
            addTense2("pt", "PersonalInfinitivePerfect", "Inf Pes Comp", "Infinitivo Pessoal Composto");
            addTranslation("pt", "Translation", "Tradução");
        } else {
            Assert.fail("unknown language");
        }
    }

    public String nameTenseShort(String str) {
        return this.TENSES_NAME.get(str);
    }

    public String nameTenseLong(String str) {
        return this.TENSES_NAME_LONG.get(str);
    }

    public String namePronoun(String str) {
        if (str.length() == 0) {
            return "";
        }
        return this.MAP_FORM_TO_PRONOUN.get(str);
    }

    public Vector<String> getTenseList() {
        return this.TENSES_SEQUENCE;
    }

    public String[] getExpandedFormat(String str) {
        return this.EXPAND_TENSES_MAP.get(str);
    }

    public boolean hasTensePronouns(String str) {
        return !this.TENSES_WITHOUT_PRONOUNS.contains(str);
    }

    public Vector<String> getFormList() {
        Vector<String> vector = new Vector<>(10);
        vector.add("1");
        vector.add("2");
        vector.add("3");
        vector.add("4");
        vector.add("5");
        vector.add("6");
        return vector;
    }

    public Vector<String> getExpandedFormList(Set<String> set) {
        String str;
        Vector<String> vector = new Vector<>(this.FORM_SEQUENCE.size());
        Iterator<String> it = this.FORM_SEQUENCE.iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next.length() != 1) {
                str = next.substring(0, 1);
            } else {
                str = next;
            }
            if (set.contains(str)) {
                vector.add(str);
            }
        }
        return vector;
    }
}
