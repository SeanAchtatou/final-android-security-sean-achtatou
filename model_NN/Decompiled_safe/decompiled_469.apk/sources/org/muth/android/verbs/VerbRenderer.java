package org.muth.android.verbs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.muth.android.verbs.VerbDatabase;

public class VerbRenderer {
    static final /* synthetic */ boolean $assertionsDisabled = (!VerbRenderer.class.desiredAssertionStatus());
    private static Logger logger = Logger.getLogger("verbs");
    private static VerbRenderer singleton = null;
    private final VerbDatabase mDb;
    private final String mLanguage;
    private final VerbRendererNames mNames;
    private final HashMap<Integer, String> mPronounOrTenseNameCache = new HashMap<>(100);

    public VerbRenderer(VerbDatabase verbDatabase, String str) {
        this.mDb = verbDatabase;
        this.mLanguage = str;
        this.mNames = new VerbRendererNames(str);
    }

    public String Language() {
        return this.mLanguage;
    }

    public static VerbRenderer GetSingleton(VerbDatabase verbDatabase, String str) {
        if (singleton == null) {
            singleton = new VerbRenderer(verbDatabase, str);
        }
        return singleton;
    }

    public void renderConjugation(StringBuilder sb, int i) {
        sb.append(this.mDb.getConjugation(i));
    }

    public void renderTranslation(StringBuilder sb, int i) {
        sb.append(this.mDb.getTranslation(i));
    }

    public void renderTitle(StringBuilder sb, int i, boolean z, boolean z2) {
        sb.append(this.mDb.getInfinitive(i));
        if (z) {
            sb.append(" (");
            renderConjugation(sb, i);
            sb.append(")");
        }
        if (z2) {
            sb.append(" ");
            renderTranslation(sb, i);
        }
    }

    public void renderHtmlTitle(StringBuilder sb, int i, boolean z, boolean z2) {
        sb.append("<h2>");
        sb.append(this.mDb.getInfinitive(i));
        if (z) {
            sb.append("&nbsp;");
            sb.append("<conjugation>(");
            renderConjugation(sb, i);
            sb.append(")</conjugation>");
        }
        sb.append("</h2>");
        if (z2) {
            sb.append("<translation><h3>(");
            renderTranslation(sb, i);
            sb.append(")</h3></translation>");
        }
    }

    public int GetVerbId(String str) {
        return this.mDb.makeId(str);
    }

    public String Info() {
        return "" + "DB version: " + this.mDb.version() + " " + this.mDb.numVerbs() + ":" + this.mNames.getTenseList().size() + "\n";
    }

    private int getActualConjugatedVerb(String str, int i) {
        if (str.equals("conjaux")) {
            return getPerfectTenseAuxiliary(i);
        }
        if (str.equals("conjwerden")) {
            return this.mDb.makeId("werden");
        }
        if (str.equals("conjhave")) {
            return this.mDb.makeId("have");
        }
        if (str.equals("conjwill")) {
            return this.mDb.makeId("will");
        }
        if (str.equals("conjbe")) {
            return this.mDb.makeId("be");
        }
        Assert.assertEquals("conj", str);
        return i;
    }

    private int getFirstVerb(String str, int i) {
        if (str.equals("firstaux")) {
            return getPerfectTenseAuxiliary(i);
        }
        Assert.assertEquals("first", str);
        return i;
    }

    private void renderHtmlPlaybackString(StringBuilder sb, String[] strArr, String[] strArr2, boolean z) {
        for (int i = 0; i < strArr2.length; i++) {
            String str = strArr2[i];
            if (!str.equals("@pronopt") && (!str.equals("@pron") || z)) {
                sb.append(strArr[i].replaceAll("'", "&#39;"));
                sb.append(" ");
            }
        }
    }

    private String renderOneFromHtml(StringBuilder sb, String[] strArr, String[] strArr2, String str, boolean z, boolean z2) {
        if (z2) {
            sb.append("<div class=playback onclick='javascript:Words(\"");
            renderHtmlPlaybackString(sb, strArr, strArr2, z);
            sb.append("\")'><span class=audio></span><span>&nbsp;</span>");
        }
        String str2 = "";
        for (int i = 0; i < strArr2.length; i++) {
            if (strArr2[i].startsWith("@pron")) {
                str2 = strArr[i];
                if (str.equals(str2)) {
                    sb.append("&#x02022;");
                }
                if (z) {
                    sb.append(str2);
                }
            } else {
                sb.append(strArr[i].replaceAll(" ", "&nbsp;"));
            }
            sb.append("&nbsp;");
        }
        if (z2) {
            sb.append("</div>");
        }
        return str2;
    }

    private Vector<String[]> renderVerbTenseGeneric(int i, String[] strArr, String str) {
        Vector<VerbDatabase.VerbForm> vector;
        String str2;
        Vector<VerbDatabase.VerbForm> vector2 = null;
        String[] strArr2 = new String[strArr.length];
        int i2 = 0;
        for (int i3 = 0; i3 < strArr.length; i3++) {
            String str3 = strArr[i3];
            if (str3.charAt(0) != '@') {
                strArr2[i3] = str3;
            } else if (str3.startsWith("@pron")) {
                strArr2[i3] = str3;
            } else if (str3.startsWith("@conj")) {
                Assert.assertEquals((Object) null, vector2);
                String[] split = str3.split("@");
                Assert.assertEquals(split.length, 3);
                i2 = getActualConjugatedVerb(split[1], i);
                vector2 = this.mDb.findTense(i2, this.mDb.makeId(split[2]));
                strArr2[i3] = str3;
            } else if (str3.startsWith("@first")) {
                String[] split2 = str3.split("@");
                Assert.assertEquals(split2.length, 3);
                strArr2[i3] = this.mDb.getFirstForTense(getFirstVerb(split2[1], i), this.mDb.makeId(split2[2]));
                if ((this.mLanguage.equals("fr") || this.mLanguage.equals("it")) && split2[2].equals("Participle")) {
                    String makeString = this.mDb.makeString(i2);
                    if (makeString.equals("essere") || makeString.equals("etre")) {
                        strArr2[i3] = "@hack@" + strArr2[i3];
                    }
                }
            } else {
                logger.severe("unknown meta prefix " + str3);
                Assert.fail("unknown meta prefix " + str3);
            }
        }
        if (str != null) {
            vector = new Vector<>(6);
            Iterator<VerbDatabase.VerbForm> it = vector2.iterator();
            while (it.hasNext()) {
                VerbDatabase.VerbForm next = it.next();
                if (formsMatch(this.mDb.makeString(next.mPerson), str)) {
                    vector.add(next);
                }
            }
        } else {
            vector = vector2;
        }
        Assert.assertNotNull(vector);
        Vector<String[]> vector3 = new Vector<>();
        Iterator<VerbDatabase.VerbForm> it2 = vector.iterator();
        while (it2.hasNext()) {
            VerbDatabase.VerbForm next2 = it2.next();
            String[] strArr3 = new String[(strArr2.length + 1)];
            String str4 = "";
            for (int i4 = 0; i4 < strArr2.length; i4++) {
                String str5 = strArr2[i4];
                if (str5.startsWith("@conj")) {
                    strArr3[i4] = this.mDb.makeString(next2.mForm);
                    int length = strArr2.length;
                    if (str5.startsWith("@conj@")) {
                        str2 = this.mDb.makeString(next2.mExtra);
                    } else {
                        str2 = "";
                    }
                    strArr3[length] = str2;
                } else if (str5.equals("@pron")) {
                    str4 = this.mDb.makeString(next2.mPerson);
                    strArr3[i4] = this.mNames.namePronoun(str4);
                } else if (str5.equals("@pronno")) {
                    strArr3[i4] = "";
                } else if (str5.equals("@pronopt")) {
                    str4 = this.mDb.makeString(next2.mPerson);
                    strArr3[i4] = "(" + this.mNames.namePronoun(str4) + ")";
                } else if (!str5.startsWith("@hack@")) {
                    Assert.assertFalse(str5.charAt(0) == '@');
                    strArr3[i4] = strArr2[i4];
                } else if (str4.charAt(0) <= '3') {
                    strArr3[i4] = str5.substring(6);
                } else {
                    if (this.mLanguage.equals("fr")) {
                        strArr3[i4] = str5.substring(6) + "s";
                    }
                    if (this.mLanguage.equals("it")) {
                        strArr3[i4] = str5.substring(6, str5.length() - 1) + "i";
                    }
                }
            }
            vector3.add(strArr3);
        }
        return vector3;
    }

    private void renderVerbTenseHtml(StringBuilder sb, int i, String str, boolean z, boolean z2, boolean z3) {
        String nameTenseShort;
        logger.info("render verb html tense " + str);
        if (z) {
            nameTenseShort = this.mNames.nameTenseLong(str);
        } else {
            nameTenseShort = this.mNames.nameTenseShort(str);
        }
        String[] expandedFormat = this.mNames.getExpandedFormat(str);
        Assert.assertNotNull(expandedFormat);
        Vector<String[]> renderVerbTenseGeneric = renderVerbTenseGeneric(i, expandedFormat, null);
        sb.append("<div style='white-space:nowrap;' class=tense onclick='javascript:Tense(\"");
        sb.append(str);
        sb.append("\")'>");
        sb.append(nameTenseShort);
        sb.append("</div>\n");
        String str2 = "@@@@@@@";
        Iterator<String[]> it = renderVerbTenseGeneric.iterator();
        while (true) {
            String str3 = str2;
            if (it.hasNext()) {
                String[] next = it.next();
                String str4 = next[next.length - 1];
                if (str4.equals("")) {
                    str4 = "reg";
                }
                sb.append("<div style='white-space:nowrap;' class='form " + str4 + "'>");
                str2 = renderOneFromHtml(sb, next, expandedFormat, str3, z2, z3);
                sb.append("</div>\n");
            } else {
                return;
            }
        }
    }

    public String renderVerbHtml(String str, HashSet<String> hashSet, int i, boolean z, boolean z2, boolean z3, boolean z4) {
        int i2;
        StringBuilder sb = new StringBuilder(4096);
        logger.info("render verb html " + str);
        if (i == 0) {
            i2 = 3;
        } else {
            i2 = i;
        }
        int makeId = this.mDb.makeId(str);
        if (makeId < 0) {
            return null;
        }
        Vector vector = new Vector(20);
        Iterator<String> it = getTenseList().iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (hashSet.contains(next)) {
                vector.add(next);
            }
        }
        if (z) {
            sb.append("<h3 style='white-space:nowrap;'>");
            renderTitle(sb, makeId, true, z2);
            sb.append("</h3>");
        }
        sb.append("<table>\n");
        Iterator it2 = vector.iterator();
        int i3 = 0;
        while (it2.hasNext()) {
            String str2 = (String) it2.next();
            if (i3 % i2 == 0) {
                sb.append("<tr>");
            }
            sb.append("<td valign=top>");
            renderVerbTenseHtml(sb, makeId, str2, i2 == 1, z3, z4);
            sb.append("</td>");
            int i4 = i3 + 1;
            if (i4 % i2 == 0) {
                sb.append("</tr>\n");
            }
            i3 = i4;
        }
        if (i3 % i2 != 0) {
            for (int i5 = i3; i5 % i2 != 0; i5++) {
                sb.append("<td>&nbsp;</td>");
            }
            sb.append("</tr>\n");
        }
        sb.append("</table>\n");
        logger.info("render verb html size " + sb.length());
        return sb.toString();
    }

    public Vector<String> getImportantVerbs() {
        return this.mDb.getVerbsMatchingLabel("top10");
    }

    public Vector<String> getTenseList() {
        return this.mNames.getTenseList();
    }

    public Vector<String> getFormList() {
        return this.mNames.getFormList();
    }

    public Vector<String> getExpandedFormList(Set<String> set) {
        return this.mNames.getExpandedFormList(set);
    }

    public String nameTenseShort(String str) {
        return this.mNames.nameTenseShort(str);
    }

    public String nameTenseLong(String str) {
        return this.mNames.nameTenseLong(str);
    }

    public String namePronoun(String str) {
        return this.mNames.namePronoun(str);
    }

    public Vector<String> getAllVerbNames() {
        return this.mDb.getAllVerbNames();
    }

    public int[] getAllVerbNameIds() {
        return this.mDb.getAllVerbNameIds();
    }

    public Vector<String> getVerbsMatchingConjugation(String str) {
        return this.mDb.getVerbsMatchingConjugation(str);
    }

    public Vector<String> getVerbsMatchingLabel(String str) {
        return this.mDb.getVerbsMatchingLabel(str);
    }

    public boolean hasTensePronouns(String str) {
        return this.mNames.hasTensePronouns(str);
    }

    private static boolean formsMatch(String str, String str2) {
        if (str.equals(str2)) {
            return true;
        }
        if (str.length() < 1 || str2.length() < 1 || str.charAt(0) != str2.charAt(0)) {
            return false;
        }
        return true;
    }

    public static String join(String[] strArr, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr.length; i++) {
            if (i != 0) {
                stringBuffer.append(str);
            }
            stringBuffer.append(strArr[i]);
        }
        return stringBuffer.toString();
    }

    public Vector<String> Conjugate(String str, String str2, String str3) {
        int makeId = this.mDb.makeId(str);
        if (makeId < 0) {
            logger.severe("unknown verb: " + str);
            return null;
        }
        String[] expandedFormat = this.mNames.getExpandedFormat(str2);
        Assert.assertNotNull(expandedFormat);
        for (int i = 0; i < expandedFormat.length; i++) {
            if (expandedFormat[i].equals("@pronopt")) {
                expandedFormat[i] = "@pronno";
            }
            if (expandedFormat[i].equals("@pronopt")) {
                expandedFormat[i] = "@pronno";
            }
        }
        Vector<String[]> renderVerbTenseGeneric = renderVerbTenseGeneric(makeId, expandedFormat, str3);
        Vector<String> vector = new Vector<>(5);
        Iterator<String[]> it = renderVerbTenseGeneric.iterator();
        while (it.hasNext()) {
            String[] next = it.next();
            next[next.length - 1] = "";
            vector.add(join(next, " "));
        }
        return vector;
    }

    public int getPerfectTenseAuxiliary(int i) {
        String str;
        String str2 = ";" + this.mDb.getConjugation(i) + ";";
        if (this.mLanguage.equals("de")) {
            VerbDatabase verbDatabase = this.mDb;
            if (str2.contains(";sein;")) {
                str = "sein";
            } else {
                str = "haben";
            }
            return verbDatabase.makeId(str);
        } else if (this.mLanguage.equals("es")) {
            return this.mDb.makeId("haber");
        } else {
            if (this.mLanguage.equals("fr")) {
                return this.mDb.makeId(str2.contains(";etre;") ? "etre" : "avoir");
            } else if (this.mLanguage.equals("it")) {
                return this.mDb.makeId(str2.contains(";essere;") ? "essere" : "avere");
            } else if (this.mLanguage.equals("pt")) {
                return this.mDb.makeId("ter");
            } else {
                if ($assertionsDisabled) {
                    return -1;
                }
                throw new AssertionError();
            }
        }
    }
}
