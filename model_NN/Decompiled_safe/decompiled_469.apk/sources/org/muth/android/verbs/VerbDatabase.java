package org.muth.android.verbs;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import org.muth.android.base.FileIO;
import org.muth.android.base.Glob;

public final class VerbDatabase {
    static final /* synthetic */ boolean $assertionsDisabled = (!VerbDatabase.class.desiredAssertionStatus());
    public static final VerbForm EMPTY_FORM = new VerbForm(ZEROS, 0);
    public static final int[] ZEROS = {0, 0, 0, 0, 0, 0};
    private static Logger logger = Logger.getLogger("verbs");
    private static VerbDatabase singleton = null;
    final int ARCHAIC;
    final int CONJUGATION;
    final int DEFAULT_PHONE_LANGUAGE;
    final int INFINITIVE;
    final int LABEL;
    final int PARTICIPLE;
    final int PHONE_LANGUAGE;
    final int TRANSLATION;
    private int[] mAllSorted = null;
    private final HashMap<Integer, Vector<VerbForm>> mAllVerbs = new HashMap<>(1500, 1.0f);
    private int[] mFirstOccur = null;
    private byte[] mStr = null;
    private VerbForm mVersion = null;

    public static boolean isNonAscii(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) >= 128) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String translateAccents(String str) {
        return str.replace(224, 'a').replace(225, 'a').replace(226, 'a').replace(228, 'a').replace(232, 'e').replace(233, 'e').replace(234, 'e').replace(235, 'e').replace(236, 'i').replace(237, 'i').replace(238, 'i').replace(239, 'i').replace(242, 'o').replace(243, 'o').replace(244, 'o').replace(246, 'o').replace(249, 'u').replace(250, 'u').replace(251, 'u').replace(252, 'u').replace(241, 'n').replace(231, 'c').replace(223, 's');
    }

    public static final class VerbForm {
        public final int mExtra;
        public final int mForm;
        public final int mFormAlt;
        public final int mPerson;
        public final int mTense;

        public VerbForm(int[] iArr, int i) {
            this.mTense = iArr[i + 0];
            this.mPerson = iArr[i + 1];
            this.mForm = iArr[i + 2];
            this.mFormAlt = iArr[i + 3];
            this.mExtra = iArr[i + 4];
        }

        public VerbForm(int i) {
            this.mTense = 0;
            this.mPerson = 0;
            this.mForm = i;
            this.mFormAlt = i;
            this.mExtra = 0;
        }

        public boolean isVerbMarker() {
            return this.mTense == 0;
        }
    }

    public String version() {
        if (this.mVersion == null) {
            return "@no-version@";
        }
        return makeString(this.mVersion.mExtra);
    }

    private final int cmpUnsignedBytes(int i, int i2) {
        int i3;
        int i4;
        if (i < 0) {
            i3 = i + 256;
        } else {
            i3 = i;
        }
        if (i2 < 0) {
            i4 = i2 + 256;
        } else {
            i4 = i2;
        }
        return i3 - i4;
    }

    private final int strCmp(int i, byte[] bArr) {
        for (int i2 = 0; i2 < bArr.length; i2++) {
            int cmpUnsignedBytes = cmpUnsignedBytes(this.mStr[i + i2], bArr[i2]);
            if (cmpUnsignedBytes != 0) {
                return cmpUnsignedBytes;
            }
        }
        return cmpUnsignedBytes(this.mStr[bArr.length + i], 10);
    }

    private final byte[] decode(String str) {
        try {
            return str.getBytes("utf-8");
        } catch (Exception e) {
            logger.severe("Failure decode [" + str + "]");
            return new byte[0];
        }
    }

    public final int findVerbPos(String str) {
        byte[] decode = decode(str);
        logger.info("start");
        int i = 0;
        int length = this.mAllSorted.length - 1;
        while (true) {
            if (!$assertionsDisabled && i > length) {
                throw new AssertionError();
            } else if (i == length) {
                return i;
            } else {
                int i2 = (i + length) / 2;
                int strCmp = strCmp(this.mAllSorted[i2], decode);
                if (strCmp < 0) {
                    i = i2 + 1;
                    if (i > length) {
                        return i2;
                    }
                } else if (strCmp <= 0) {
                    return i2;
                } else {
                    length = i2 - 1;
                    if (i > length) {
                        return i2;
                    }
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final int[] initializeFirstOccur(byte[] r5) {
        /*
            r3 = 256(0x100, float:3.59E-43)
            r2 = 0
            r4 = -1
            int[] r0 = new int[r3]
            r1 = r2
        L_0x0007:
            if (r1 >= r3) goto L_0x000e
            r0[r1] = r4
            int r1 = r1 + 1
            goto L_0x0007
        L_0x000e:
            r1 = r2
        L_0x000f:
            int r2 = r5.length
            r3 = 1
            int r2 = r2 - r3
            if (r1 >= r2) goto L_0x002c
            byte r2 = r5[r1]
            r3 = 10
            if (r2 == r3) goto L_0x001d
        L_0x001a:
            int r1 = r1 + 1
            goto L_0x000f
        L_0x001d:
            int r2 = r1 + 1
            byte r2 = r5[r2]
            if (r2 >= 0) goto L_0x0025
            int r2 = r2 + 256
        L_0x0025:
            r3 = r0[r2]
            if (r3 != r4) goto L_0x001a
            r0[r2] = r1
            goto L_0x001a
        L_0x002c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.muth.android.verbs.VerbDatabase.initializeFirstOccur(byte[]):int[]");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int makeId(java.lang.String r11) {
        /*
            r10 = this;
            r9 = 10
            r8 = 1
            r7 = 0
            r6 = -1
            int r0 = r11.length()
            if (r0 != 0) goto L_0x000d
            r0 = r7
        L_0x000c:
            return r0
        L_0x000d:
            byte[] r0 = r10.decode(r11)
            byte r1 = r0[r7]
            if (r1 >= 0) goto L_0x0017
            int r1 = r1 + 256
        L_0x0017:
            int[] r2 = r10.mFirstOccur
            r2 = r2[r1]
            if (r2 != r6) goto L_0x001f
            r0 = r6
            goto L_0x000c
        L_0x001f:
            int[] r2 = r10.mFirstOccur
            r1 = r2[r1]
        L_0x0023:
            byte[] r2 = r10.mStr
            int r2 = r2.length
            int r2 = r2 - r8
            if (r1 >= r2) goto L_0x005b
            byte[] r2 = r10.mStr
            byte r2 = r2[r1]
            if (r2 == r9) goto L_0x0032
        L_0x002f:
            int r1 = r1 + 1
            goto L_0x0023
        L_0x0032:
            int r2 = r1 + 1
            int r3 = r0.length
            int r3 = r3 + r2
            byte[] r4 = r10.mStr
            byte r3 = r4[r3]
            if (r3 != r9) goto L_0x002f
            r3 = r7
        L_0x003d:
            int r4 = r0.length
            if (r3 >= r4) goto L_0x002f
            byte[] r4 = r10.mStr
            int r5 = r2 + r3
            byte r4 = r4[r5]
            byte r5 = r0[r3]
            int r4 = r10.cmpUnsignedBytes(r4, r5)
            if (r4 < 0) goto L_0x002f
            if (r4 <= 0) goto L_0x0052
            r0 = r6
            goto L_0x000c
        L_0x0052:
            int r4 = r0.length
            int r4 = r4 - r8
            if (r3 != r4) goto L_0x0058
            r0 = r2
            goto L_0x000c
        L_0x0058:
            int r3 = r3 + 1
            goto L_0x003d
        L_0x005b:
            r0 = r6
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.muth.android.verbs.VerbDatabase.makeId(java.lang.String):int");
    }

    public final String makeString(int i) {
        if (i == 0) {
            return "";
        }
        if (!$assertionsDisabled && i <= 0) {
            throw new AssertionError();
        } else if ($assertionsDisabled || this.mStr[i - 1] == 10) {
            int i2 = i;
            while (this.mStr[i2] != 10) {
                i2++;
            }
            try {
                return new String(this.mStr, i, i2 - i, "utf-8");
            } catch (Exception e) {
                logger.severe("Failure makeString " + e);
                return "@ERROR@";
            }
        } else {
            throw new AssertionError();
        }
    }

    public int numVerbs() {
        return this.mAllVerbs.size();
    }

    private int[] makeSortedInfinitiveArray() {
        int[] iArr = new int[numVerbs()];
        int i = 0;
        for (Integer intValue : this.mAllVerbs.keySet()) {
            iArr[i] = intValue.intValue();
            i++;
        }
        Arrays.sort(iArr);
        return iArr;
    }

    private void registerVerbForm(int i, VerbForm verbForm) {
        if (i == 0) {
            this.mVersion = verbForm;
            return;
        }
        Vector vector = this.mAllVerbs.get(Integer.valueOf(i));
        if (vector == null) {
            vector = new Vector(64);
            this.mAllVerbs.put(Integer.valueOf(i), vector);
        }
        vector.add(verbForm);
    }

    private void processVf(byte[] bArr, Runnable runnable) {
        logger.info("processing vf");
        long currentTimeMillis = System.currentTimeMillis();
        int[] iArr = new int[10];
        int i = 0;
        byte b = 0;
        int i2 = 0;
        int i3 = 0;
        for (byte b2 : bArr) {
            if (b2 == 44 || b2 == 10) {
                if (i2 == 0 || $assertionsDisabled || this.mStr[i2 - 1] == 10) {
                    iArr[i] = i2;
                    i++;
                    i2 = 0;
                } else {
                    throw new AssertionError();
                }
            } else if ($assertionsDisabled || (48 <= b2 && b2 <= 57)) {
                i2 = ((i2 * 10) + b2) - 48;
            } else {
                throw new AssertionError();
            }
            if (b2 == 10) {
                if (this.mStr[iArr[0]] > b) {
                    if (runnable != null) {
                        runnable.run();
                    }
                    b = this.mStr[iArr[0]];
                }
                if ($assertionsDisabled || i == 6) {
                    registerVerbForm(iArr[0], new VerbForm(iArr, 1));
                    i3++;
                    i = 0;
                } else {
                    throw new AssertionError();
                }
            }
        }
        logger.info("found " + i3 + " forms, used " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
    }

    private VerbDatabase(InputStream inputStream, InputStream inputStream2, String str, Runnable runnable) {
        logger.info("loading strings");
        long currentTimeMillis = System.currentTimeMillis();
        try {
            this.mStr = FileIO.ReadFromStream(inputStream, true, true);
        } catch (Exception e) {
            logger.severe("Failure loading string" + e);
        }
        logger.info("string loading took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        long currentTimeMillis2 = System.currentTimeMillis();
        this.mFirstOccur = initializeFirstOccur(this.mStr);
        logger.info("initialize first occur took " + (System.currentTimeMillis() - currentTimeMillis2) + "ms");
        if (runnable != null) {
            runnable.run();
        }
        logger.info("loading forms");
        long currentTimeMillis3 = System.currentTimeMillis();
        byte[] bArr = new byte[0];
        try {
            bArr = FileIO.ReadFromStream(inputStream2, true, true);
        } catch (Exception e2) {
            logger.severe("Failure loading forms " + e2);
        }
        logger.info("form loading took " + (System.currentTimeMillis() - currentTimeMillis3) + "ms");
        if (runnable != null) {
            runnable.run();
        }
        processVf(bArr, runnable);
        long currentTimeMillis4 = System.currentTimeMillis();
        this.mAllSorted = makeSortedInfinitiveArray();
        logger.info("sorted infinitives took " + (System.currentTimeMillis() - currentTimeMillis4) + "ms");
        this.CONJUGATION = makeId("Conjugation");
        this.TRANSLATION = makeId("Translation");
        this.INFINITIVE = makeId("Infinitive");
        this.PARTICIPLE = makeId("Participle");
        this.ARCHAIC = makeId("a");
        this.LABEL = makeId("Label");
        this.DEFAULT_PHONE_LANGUAGE = makeId("en");
        this.PHONE_LANGUAGE = makeId(str);
    }

    public static synchronized VerbDatabase GetSingleton(InputStream inputStream, InputStream inputStream2, String str, Runnable runnable) {
        VerbDatabase verbDatabase;
        synchronized (VerbDatabase.class) {
            if (singleton == null) {
                singleton = new VerbDatabase(inputStream, inputStream2, str, runnable);
            }
            verbDatabase = singleton;
        }
        return verbDatabase;
    }

    public Vector<VerbForm> findTense(int i, int i2) {
        Vector<VerbForm> vector = new Vector<>(6);
        if (this.mAllVerbs.containsKey(Integer.valueOf(i))) {
            Iterator it = this.mAllVerbs.get(Integer.valueOf(i)).iterator();
            while (it.hasNext()) {
                VerbForm verbForm = (VerbForm) it.next();
                if (verbForm.mTense == i2) {
                    vector.add(verbForm);
                }
            }
        }
        return vector;
    }

    public int[] getAllVerbNameIds() {
        return this.mAllSorted;
    }

    public Vector<String> getAllVerbNames() {
        int[] allVerbNameIds = getAllVerbNameIds();
        Vector<String> vector = new Vector<>(allVerbNameIds.length);
        for (int makeString : allVerbNameIds) {
            vector.add(makeString(makeString));
        }
        return vector;
    }

    public Vector<VerbForm> findMatchingInfinitives(String str, boolean z) {
        Glob glob = new Glob();
        Vector<VerbForm> vector = new Vector<>(6);
        for (int i : getAllVerbNameIds()) {
            Iterator it = this.mAllVerbs.get(Integer.valueOf(i)).iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                VerbForm verbForm = (VerbForm) it.next();
                if (verbForm.mTense == this.INFINITIVE) {
                    if (glob.match(makeString(verbForm.mForm), str, false)) {
                        vector.add(new VerbForm(i));
                        vector.add(verbForm);
                    } else if (z && glob.match(makeString(verbForm.mFormAlt), str, false)) {
                        vector.add(new VerbForm(i));
                        vector.add(verbForm);
                    }
                }
            }
        }
        return vector;
    }

    public Vector<VerbForm> findVerbForm(String str, boolean z) {
        String str2;
        logger.info("initial query " + str);
        Vector<VerbForm> vector = new Vector<>(6);
        if (str.equals("")) {
            return vector;
        }
        if (z) {
            str2 = translateAccents(str);
        } else {
            str2 = str;
        }
        logger.info("final query " + str2);
        int makeId = makeId(str2);
        logger.info("final query id " + makeId);
        if (makeId < 0) {
            return vector;
        }
        for (int i : getAllVerbNameIds()) {
            Iterator it = this.mAllVerbs.get(Integer.valueOf(i)).iterator();
            boolean z2 = false;
            while (it.hasNext()) {
                VerbForm verbForm = (VerbForm) it.next();
                if (z) {
                    if (makeId == verbForm.mFormAlt) {
                        if (!z2) {
                            vector.add(new VerbForm(i));
                            z2 = true;
                        }
                        vector.add(verbForm);
                    }
                } else if (makeId == verbForm.mForm) {
                    if (!z2) {
                        vector.add(new VerbForm(i));
                        z2 = true;
                    }
                    vector.add(verbForm);
                }
                z2 = z2;
            }
        }
        return vector;
    }

    public String getTranslation(int i) {
        String str = "";
        Iterator<VerbForm> it = findTense(i, this.TRANSLATION).iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            VerbForm next = it.next();
            if (next.mPerson == this.PHONE_LANGUAGE) {
                return makeString(next.mExtra);
            }
            if (next.mPerson == this.DEFAULT_PHONE_LANGUAGE) {
                str = makeString(next.mExtra);
            } else {
                str = str2;
            }
        }
    }

    public Vector<VerbForm> findTranslations(String str) {
        int i;
        int i2;
        Vector<VerbForm> vector = new Vector<>(5);
        for (int i3 : getAllVerbNameIds()) {
            Iterator it = this.mAllVerbs.get(Integer.valueOf(i3)).iterator();
            int i4 = 0;
            while (true) {
                if (!it.hasNext()) {
                    i = i4;
                    break;
                }
                VerbForm verbForm = (VerbForm) it.next();
                if (verbForm.mTense == this.TRANSLATION) {
                    if (verbForm.mPerson == this.PHONE_LANGUAGE) {
                        i = verbForm.mExtra;
                        break;
                    }
                    if (verbForm.mPerson == this.DEFAULT_PHONE_LANGUAGE) {
                        i2 = verbForm.mExtra;
                    } else {
                        i2 = i4;
                    }
                    i4 = i2;
                }
            }
            if (i != 0 && makeString(i).indexOf(str) >= 0) {
                vector.add(new VerbForm(i3));
            }
        }
        return vector;
    }

    private HashSet<Integer> StringListToTokenHashSet(List<String> list) {
        HashSet<Integer> hashSet = new HashSet<>(list.size() * 2);
        for (String makeId : list) {
            hashSet.add(Integer.valueOf(makeId(makeId)));
        }
        return hashSet;
    }

    public Vector<VerbForm> findMatchingForms(List<String> list, List<String> list2, List<String> list3) {
        long currentTimeMillis = System.currentTimeMillis();
        Vector<VerbForm> vector = new Vector<>((((list.size() * list2.size()) * list3.size()) * 3) / 2);
        HashSet<Integer> StringListToTokenHashSet = StringListToTokenHashSet(list2);
        HashSet<Integer> StringListToTokenHashSet2 = StringListToTokenHashSet(list3);
        for (String makeId : list) {
            int makeId2 = makeId(makeId);
            Iterator it = this.mAllVerbs.get(Integer.valueOf(makeId2)).iterator();
            boolean z = false;
            while (it.hasNext()) {
                VerbForm verbForm = (VerbForm) it.next();
                if (StringListToTokenHashSet.contains(Integer.valueOf(verbForm.mTense)) && StringListToTokenHashSet2.contains(Integer.valueOf(verbForm.mPerson))) {
                    if (!z) {
                        z = true;
                        vector.add(new VerbForm(makeId2));
                    }
                    vector.add(verbForm);
                }
            }
        }
        vector.add(new VerbForm(0));
        logger.info("found matching forms: " + vector.size() + "  in " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        return vector;
    }

    public String getConjugation(int i) {
        Iterator<VerbForm> it = findTense(i, this.CONJUGATION).iterator();
        if (it.hasNext()) {
            return makeString(it.next().mExtra);
        }
        return "reg";
    }

    public Vector<String> getVerbsMatching(String str, int i) {
        String str2 = ";" + str + ";";
        Vector<String> vector = new Vector<>(10);
        for (int i2 : getAllVerbNameIds()) {
            Iterator it = this.mAllVerbs.get(Integer.valueOf(i2)).iterator();
            while (it.hasNext()) {
                VerbForm verbForm = (VerbForm) it.next();
                if (verbForm.mTense == i && (";" + makeString(verbForm.mExtra) + ";").contains(str2)) {
                    vector.add(makeString(i2));
                }
            }
        }
        return vector;
    }

    public Vector<String> getVerbsMatchingConjugation(String str) {
        return getVerbsMatching(str, this.CONJUGATION);
    }

    public Vector<String> getVerbsMatchingLabel(String str) {
        return getVerbsMatching(str, this.LABEL);
    }

    public String getFirstForTense(int i, int i2) {
        Iterator<VerbForm> it = findTense(i, i2).iterator();
        while (it.hasNext()) {
            VerbForm next = it.next();
            if (next.mExtra != this.ARCHAIC) {
                return makeString(next.mForm);
            }
        }
        return "@unknown@";
    }

    public String getInfinitive(int i) {
        return getFirstForTense(i, this.INFINITIVE);
    }

    public String getParticiple(int i) {
        return getFirstForTense(i, this.PARTICIPLE);
    }
}
