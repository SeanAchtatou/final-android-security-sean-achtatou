package org.muth.android.verbs;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import java.util.Vector;
import java.util.logging.Logger;
import junit.framework.Assert;

public class VerbListAdaptor extends BaseAdapter implements SectionIndexer {
    static final /* synthetic */ boolean $assertionsDisabled = (!VerbListAdaptor.class.desiredAssertionStatus());
    private static Logger logger = Logger.getLogger("verbs");
    private final Vector<String> mInfinitives;
    private final Vector<String> mMeta = new Vector<>(1200);
    private final VerbRenderer mRenderer;
    private final int[] mSectionMap;
    private final Vector<String> mText = new Vector<>(1200);
    private final int[] mVerbnameIds;

    public void AddEntry(String str, String str2) {
        this.mText.add(str);
        this.mMeta.add(str2);
    }

    public int getCount() {
        return this.mText.size();
    }

    /* access modifiers changed from: protected */
    public void fillVerbs() {
        this.mSectionMap[0] = 0;
        this.mSectionMap[1] = getCount();
        for (int i = 2; i < 27; i++) {
            this.mSectionMap[i] = -1;
        }
        StringBuilder sb = new StringBuilder(200);
        int i2 = 0;
        while (i2 < this.mInfinitives.size()) {
            String str = this.mInfinitives.get(i2);
            int charAt = (str.charAt(0) - 'a') + 1;
            if ($assertionsDisabled || (1 <= charAt && charAt <= 26)) {
                if (this.mSectionMap[charAt] == -1) {
                    this.mSectionMap[charAt] = getCount();
                }
                sb.setLength(0);
                this.mRenderer.renderTitle(sb, this.mVerbnameIds[i2], true, true);
                AddEntry(sb.toString(), "@act@ActivityView@http://" + str);
                i2++;
            } else {
                throw new AssertionError();
            }
        }
        for (int i3 = 2; i3 < 27; i3++) {
            if (this.mSectionMap[i3] < 0) {
                this.mSectionMap[i3] = this.mSectionMap[i3 - 1];
            }
        }
    }

    public VerbListAdaptor(VerbRenderer verbRenderer) {
        this.mRenderer = verbRenderer;
        this.mInfinitives = this.mRenderer.getAllVerbNames();
        this.mVerbnameIds = this.mRenderer.getAllVerbNameIds();
        Assert.assertEquals(this.mInfinitives.size(), this.mVerbnameIds.length);
        this.mSectionMap = new int[27];
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView;
        TextView textView2;
        LinearLayout linearLayout;
        if (view == null) {
            LinearLayout linearLayout2 = new LinearLayout(viewGroup.getContext());
            linearLayout2.setOrientation(0);
            TextView textView3 = new TextView(viewGroup.getContext());
            textView3.setTextAppearance(viewGroup.getContext(), 16973890);
            textView3.setTextColor(-1118482);
            linearLayout2.addView(textView3);
            textView = new TextView(viewGroup.getContext());
            textView.setTextAppearance(viewGroup.getContext(), 16973892);
            textView.setTextColor(-4473925);
            linearLayout2.addView(textView);
            linearLayout2.setPadding(0, 0, 0, 2);
            textView2 = textView3;
            linearLayout = linearLayout2;
        } else {
            LinearLayout linearLayout3 = (LinearLayout) view;
            textView = (TextView) linearLayout3.getChildAt(1);
            textView2 = (TextView) linearLayout3.getChildAt(0);
            linearLayout = linearLayout3;
        }
        String str = this.mMeta.get(i);
        linearLayout.setTag(str);
        String str2 = this.mText.get(i);
        int indexOf = str2.indexOf(40);
        if ($assertionsDisabled || indexOf > 0) {
            textView2.setTypeface(Typeface.DEFAULT);
            textView.setTypeface(Typeface.DEFAULT);
            textView.setGravity(3);
            if (str.startsWith("@act@ActivityView")) {
                textView2.setGravity(3);
                textView2.setTypeface(Typeface.DEFAULT);
                textView2.setText(str2.substring(0, indexOf));
                textView.setVisibility(0);
                textView.setText(str2.substring(indexOf));
            } else {
                textView2.setGravity(17);
                textView2.setTypeface(Typeface.DEFAULT_BOLD);
                textView2.setText(str2);
                textView.setVisibility(8);
                textView.setText("");
            }
            return linearLayout;
        }
        throw new AssertionError();
    }

    public int findFirstPosStartingWith(String str) {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            if (i >= this.mInfinitives.size() || this.mInfinitives.get(i).compareTo(str) > 0) {
                logger.info("found pos " + i);
            } else {
                i2 = i + 1;
            }
        }
        logger.info("found pos " + i);
        return (this.mText.size() + i) - 1;
    }

    public int getPositionForSection(int i) {
        logger.info("sec -> pos " + i + " " + this.mSectionMap[i]);
        return this.mSectionMap[i];
    }

    public int getSectionForPosition(int i) {
        logger.info("pos -> sec " + i);
        if (i < this.mSectionMap[1]) {
            return 0;
        }
        int charAt = (this.mInfinitives.get(i).charAt(0) + 1) - 97;
        if (charAt < 0) {
            return 0;
        }
        if (charAt > 26) {
            return 26;
        }
        return charAt;
    }

    public Object[] getSections() {
        logger.info("get  secs ");
        String[] strArr = new String[27];
        strArr[0] = "";
        for (int i = 0; i < 26; i++) {
            strArr[i + 1] = Character.toString((char) (i + 97));
        }
        return strArr;
    }
}
