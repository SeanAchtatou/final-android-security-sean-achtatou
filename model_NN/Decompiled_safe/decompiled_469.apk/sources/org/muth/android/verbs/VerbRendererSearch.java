package org.muth.android.verbs;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;
import org.muth.android.base.Glob;
import org.muth.android.verbs.VerbDatabase;

public class VerbRendererSearch {
    private static Logger logger = Logger.getLogger("verbs");
    private VerbDatabase mDb;
    private VerbRenderer mRenderer;

    public VerbRendererSearch(VerbDatabase verbDatabase, VerbRenderer verbRenderer) {
        this.mDb = verbDatabase;
        this.mRenderer = verbRenderer;
    }

    public Vector<View> renderSearch(String str, boolean z, boolean z2, Context context) {
        Vector<VerbDatabase.VerbForm> findVerbForm;
        Vector<View> vector = new Vector<>(5);
        if (Glob.HasWildcards(str)) {
            findVerbForm = this.mDb.findMatchingInfinitives(str, z);
        } else {
            findVerbForm = this.mDb.findVerbForm(str, z);
            if (z2) {
                logger.info("search translations");
                findVerbForm.addAll(this.mDb.findTranslations(str));
            }
        }
        logger.info("found " + findVerbForm.size() + " forms");
        if (findVerbForm.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(200);
        String str2 = null;
        Iterator<VerbDatabase.VerbForm> it = findVerbForm.iterator();
        while (it.hasNext()) {
            VerbDatabase.VerbForm next = it.next();
            TextView textView = new TextView(context);
            if (next.isVerbMarker()) {
                str2 = this.mDb.makeString(next.mForm);
                sb.setLength(0);
                this.mRenderer.renderTitle(sb, next.mForm, true, true);
                textView.setText(sb.toString());
                textView.setTextAppearance(context, 16973890);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                textView.setTextColor(-1);
                textView.setGravity(17);
                textView.setTag("@act@ActivityView@http://" + str2);
                vector.add(textView);
            } else {
                String makeString = this.mDb.makeString(next.mTense);
                String makeString2 = this.mDb.makeString(next.mForm);
                String makeString3 = this.mDb.makeString(next.mPerson);
                sb.setLength(0);
                sb.append(this.mRenderer.nameTenseLong(makeString));
                sb.append(": ");
                if (makeString3.length() != 0) {
                    sb.append(this.mRenderer.namePronoun(makeString3));
                    sb.append(" ");
                }
                sb.append(makeString2);
                textView.setTextAppearance(context, 16973890);
                textView.setText(sb.toString());
                textView.setTag("@act@ActivityView@http://" + str2);
                textView.setTextColor(-1);
                vector.add(textView);
            }
        }
        return vector;
    }
}
