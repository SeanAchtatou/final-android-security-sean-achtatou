package org.muth.android.verbs;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.muth.android.base.FlashCardItem;
import org.muth.android.base.FlashCardSet;

public class TrainingUnit extends FlashCardSet implements Serializable {
    private static Logger logger = Logger.getLogger("verbs");
    static final long serialVersionUID = 7;
    private transient List<byte[]> mContents = null;
    public transient List<Boolean> mPersonFlags = new Vector(10);
    public transient List<String> mPersons = new Vector(10);
    public transient List<Boolean> mTenseFlags = new Vector(20);
    public transient List<String> mTenses = new Vector(20);
    public transient List<Boolean> mVerbFlags = new Vector(200);
    public transient List<String> mVerbs = new Vector(200);

    public class RenderedCard {
        public String mBackHtml = "";
        public String mBackLanguage = "";
        public String mBackVoice = "";
        public String mFrontHtml = "";
        public String mFrontLanguage = "";
        public String mFrontVoice = "";
        public String mVerb = "";

        public RenderedCard() {
        }
    }

    public static TrainingUnit MakeTrainingUnit(String str, String str2, VerbRenderer verbRenderer) {
        Vector<String> vector;
        logger.info("MakeTrainingUnit: [" + str + "] [" + str2 + "]");
        TrainingUnit trainingUnit = new TrainingUnit(verbRenderer, false);
        trainingUnit.SetPersonsAll();
        logger.info("Num Persons: " + trainingUnit.numPersons());
        if (str2.equals("basic")) {
            trainingUnit.SetTensesBasic(verbRenderer);
        } else {
            trainingUnit.SetTenses(new HashSet(Arrays.asList(str2.split(","))));
        }
        logger.info("Num Tenses: " + trainingUnit.numTenses());
        if (str.startsWith("conjugation:")) {
            vector = verbRenderer.getVerbsMatchingConjugation(str.substring(12));
        } else if (str.startsWith("label:")) {
            vector = verbRenderer.getVerbsMatchingLabel(str.substring(6));
        } else if (str.startsWith("all")) {
            vector = verbRenderer.getAllVerbNames();
        } else {
            logger.severe("bad verb specifier");
            vector = new Vector<>();
        }
        trainingUnit.SetVerbs(new HashSet(vector));
        logger.info("Num Verbs: " + trainingUnit.numVerbs());
        trainingUnit.UpdateContents(verbRenderer);
        trainingUnit.UpdateCards();
        logger.info("Num Cards: " + trainingUnit.NumCards());
        return trainingUnit;
    }

    public TrainingUnit(VerbRenderer verbRenderer, boolean z) {
        InitializeVerbInfo(verbRenderer, z);
    }

    public int Size() {
        if (this.mContents == null) {
            return NumCards();
        }
        return this.mContents.size();
    }

    public void SetTenses(HashSet<String> hashSet) {
        for (int i = 0; i < this.mTenses.size(); i++) {
            this.mTenseFlags.set(i, Boolean.valueOf(hashSet.contains(this.mTenses.get(i))));
        }
    }

    public void SetTensesBasic(VerbRenderer verbRenderer) {
        SetTenses(new HashSet(BasicTenses(verbRenderer.Language())));
    }

    public void SetPersonsAll() {
        for (int i = 0; i < this.mPersons.size(); i++) {
            this.mPersonFlags.set(i, true);
        }
    }

    public void SetPersons(HashSet<String> hashSet) {
        for (int i = 0; i < this.mPersons.size(); i++) {
            this.mPersonFlags.set(i, Boolean.valueOf(hashSet.contains(this.mPersons.get(i))));
        }
    }

    public void SetVerbs(HashSet<String> hashSet) {
        for (int i = 0; i < this.mVerbs.size(); i++) {
            this.mVerbFlags.set(i, Boolean.valueOf(hashSet.contains(this.mVerbs.get(i))));
        }
    }

    public Vector<String> BasicTenses(String str) {
        Vector<String> vector = new Vector<>(20);
        vector.add("Present");
        if (str.equals("es") || str.equals("fr") || str.equals("pt") || str.equals("it")) {
            vector.add("Imperfect");
            vector.add("Future");
        }
        vector.add("Participle");
        return vector;
    }

    public void InitializeVerbInfo(VerbRenderer verbRenderer, boolean z) {
        this.mVerbs = new Vector(verbRenderer.getAllVerbNames());
        this.mVerbFlags = new Vector(this.mVerbs.size());
        for (String next : this.mVerbs) {
            this.mVerbFlags.add(new Boolean(false));
        }
        this.mTenses = new Vector(z ? BasicTenses(verbRenderer.Language()) : verbRenderer.getTenseList());
        this.mTenses.add("Translation");
        this.mTenseFlags = new Vector(this.mTenses.size());
        for (String next2 : this.mTenses) {
            this.mTenseFlags.add(new Boolean(false));
        }
        this.mPersons = new Vector(verbRenderer.getFormList());
        this.mPersonFlags = new Vector(this.mPersons.size());
        for (String next3 : this.mPersons) {
            this.mPersonFlags.add(new Boolean(false));
        }
    }

    private static byte[] TripleToId(String str, String str2, String str3) {
        try {
            return (str + "," + str2 + "," + str3).toString().getBytes("UTF-8");
        } catch (Exception e) {
            return new byte[]{64, 44, 64, 44, 64};
        }
    }

    private static String[] IdToTriple(byte[] bArr) {
        try {
            String[] split = new String(bArr, "utf8").split(",");
            String[] strArr = {"", "", ""};
            for (int i = 0; i < split.length; i++) {
                strArr[i] = split[i];
            }
            return strArr;
        } catch (Exception e) {
            logger.severe("decode error");
            return new String[]{"@", "@", "@"};
        }
    }

    public void InitializeVerbInfoFromExistingItems() {
        HashSet hashSet = new HashSet(GetCards().size());
        HashSet hashSet2 = new HashSet(50);
        HashSet hashSet3 = new HashSet(10);
        for (FlashCardItem Id : GetCards()) {
            String[] IdToTriple = IdToTriple(Id.Id());
            hashSet.add(IdToTriple[0]);
            hashSet2.add(IdToTriple[1]);
            hashSet3.add(IdToTriple[2]);
        }
        SetVerbs(hashSet);
        SetTenses(hashSet2);
        SetPersons(hashSet3);
    }

    public void UpdateContents(VerbRenderer verbRenderer) {
        logger.info("updating contents");
        this.mContents = new Vector((numPersons() * numTenses() * numVerbs()) + 100);
        for (int i = 0; i < this.mVerbs.size(); i++) {
            if (this.mVerbFlags.get(i).booleanValue()) {
                String str = this.mVerbs.get(i);
                for (int i2 = 0; i2 < this.mTenses.size(); i2++) {
                    if (this.mTenseFlags.get(i2).booleanValue()) {
                        String str2 = this.mTenses.get(i2);
                        if (!verbRenderer.hasTensePronouns(str2)) {
                            this.mContents.add(TripleToId(str, str2, ""));
                        } else {
                            for (int i3 = 0; i3 < this.mPersons.size(); i3++) {
                                if (this.mPersonFlags.get(i3).booleanValue()) {
                                    String str3 = this.mPersons.get(i3);
                                    if (!str2.equals("Imperative") || str3.equals("2") || str3.equals("5")) {
                                        this.mContents.add(TripleToId(str, str2, str3));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        logger.info("no of forms found: " + this.mContents.size());
    }

    public RenderedCard GetCard(FlashCardItem flashCardItem, VerbRenderer verbRenderer) {
        String[] IdToTriple = IdToTriple(flashCardItem.Id());
        Assert.assertEquals(IdToTriple.length, 3);
        int GetVerbId = verbRenderer.GetVerbId(IdToTriple[0]);
        String str = IdToTriple[2];
        String str2 = IdToTriple[1];
        RenderedCard renderedCard = new RenderedCard();
        renderedCard.mVerb = IdToTriple[0];
        StringBuilder sb = new StringBuilder(200);
        if (str2.equals("Translation")) {
            sb.setLength(0);
            verbRenderer.renderHtmlTitle(sb, GetVerbId, true, false);
            renderedCard.mFrontHtml = sb.toString();
            sb.setLength(0);
            verbRenderer.renderTitle(sb, GetVerbId, false, false);
            renderedCard.mFrontVoice = sb.toString();
            renderedCard.mFrontLanguage = "";
            sb.setLength(0);
            sb.append("<h2>");
            verbRenderer.renderTranslation(sb, GetVerbId);
            sb.append("</h2>");
            renderedCard.mBackHtml = sb.toString();
            renderedCard.mBackVoice = "";
            renderedCard.mBackLanguage = "";
        } else {
            sb.setLength(0);
            verbRenderer.renderHtmlTitle(sb, GetVerbId, true, true);
            sb.append("<p>");
            sb.append("<h2>[");
            sb.append(verbRenderer.namePronoun(str));
            sb.append("]&nbsp;");
            sb.append(verbRenderer.nameTenseLong(str2));
            sb.append("</h2>");
            renderedCard.mFrontHtml = sb.toString();
            sb.setLength(0);
            verbRenderer.renderTitle(sb, GetVerbId, false, false);
            sb.append("  ");
            sb.append(verbRenderer.namePronoun(str));
            sb.append("  ");
            sb.append(verbRenderer.nameTenseLong(str2));
            renderedCard.mFrontVoice = sb.toString();
            sb.setLength(0);
            sb.append("<translation><h3>(");
            verbRenderer.renderTranslation(sb, GetVerbId);
            sb.append(")</h3></translation>");
            sb.append("<p>");
            Vector<String> Conjugate = verbRenderer.Conjugate(IdToTriple[0], IdToTriple[1], IdToTriple[2]);
            Iterator<String> it = Conjugate.iterator();
            while (it.hasNext()) {
                sb.append("<h2>");
                sb.append(it.next());
                sb.append("</h2><p>");
            }
            renderedCard.mBackHtml = sb.toString();
            sb.setLength(0);
            Iterator<String> it2 = Conjugate.iterator();
            while (it2.hasNext()) {
                sb.append(it2.next());
                sb.append(" ");
            }
            renderedCard.mBackVoice = sb.toString();
        }
        return renderedCard;
    }

    public void UpdateCards() {
        if (this.mContents != null) {
            logger.info("updating cards " + this.mContents.size() + " " + GetCards().size());
            logger.info("new cards " + JoinCards(this.mContents));
            return;
        }
        logger.info("first time content update");
    }

    public int numVerbs() {
        int i = 0;
        for (Boolean booleanValue : this.mVerbFlags) {
            if (booleanValue.booleanValue()) {
                i++;
            }
        }
        return i;
    }

    public int numTenses() {
        int i = 0;
        for (Boolean booleanValue : this.mTenseFlags) {
            if (booleanValue.booleanValue()) {
                i++;
            }
        }
        return i;
    }

    public int numPersons() {
        int i = 0;
        for (Boolean booleanValue : this.mPersonFlags) {
            if (booleanValue.booleanValue()) {
                i++;
            }
        }
        return i;
    }
}
