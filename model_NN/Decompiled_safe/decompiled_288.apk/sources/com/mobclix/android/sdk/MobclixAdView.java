package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ViewFlipper;
import com.mobclix.android.sdk.Mobclix;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Timer;

public abstract class MobclixAdView extends ViewFlipper {
    private static final long MINIMUM_REFRESH_TIME = 15000;
    private String TAG = "mobclix-adview";
    MobclixCreative ad = null;
    String adCode = "";
    /* access modifiers changed from: private */
    public String adSpace = "";
    private Thread adThread;
    int allowAutoplay = -1;
    int backgroundColor = 0;
    Context context;
    Mobclix controller = Mobclix.getInstance();
    final AdResponseHandler handler = new AdResponseHandler(this, null);
    float height;
    private boolean isManuallyPaused = false;
    HashSet<MobclixAdViewListener> listeners = new HashSet<>();
    int ordinal = 0;
    MobclixCreative prevAd = null;
    final RemoteConfigReadyHandler rcHandler = new RemoteConfigReadyHandler(this, null);
    /* access modifiers changed from: private */
    public long refreshRate = 0;
    boolean restored = false;
    boolean rotate = true;
    float scale = 1.0f;
    String size;
    boolean testMode = false;
    private Timer timer = null;
    float width;

    MobclixAdView(Context a, String s) {
        super(a);
        this.context = a;
        this.size = s;
        try {
            initialize((Activity) a);
        } catch (Exception e) {
        }
    }

    MobclixAdView(Context a, String s, AttributeSet attrs) {
        super(a, attrs);
        this.context = a;
        this.size = s;
        String colorString = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "background");
        if (colorString != null) {
            this.backgroundColor = Color.parseColor(colorString);
        }
        try {
            initialize((Activity) a);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w(this.TAG, "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer.purge();
        }
        if (this.ad != null) {
            this.ad.onPause();
        }
        super.finalize();
    }

    public boolean addMobclixAdViewListener(MobclixAdViewListener l) {
        if (l == null) {
            return false;
        }
        return this.listeners.add(l);
    }

    public boolean removeMobclixAdViewListener(MobclixAdViewListener l) {
        return this.listeners.remove(l);
    }

    public void setLayoutParams(ViewGroup.LayoutParams params) {
        int formatWidth = Integer.parseInt(this.size.split("x")[0]);
        int formatHeight = Integer.parseInt(this.size.split("x")[1]);
        if (params.width == formatWidth && params.height == formatHeight) {
            this.width = (float) formatWidth;
            this.height = (float) formatHeight;
        } else {
            this.width = TypedValue.applyDimension(1, (float) formatWidth, getResources().getDisplayMetrics());
            this.height = TypedValue.applyDimension(1, (float) formatHeight, getResources().getDisplayMetrics());
            this.scale = getResources().getDisplayMetrics().density;
        }
        params.width = (int) this.width;
        params.height = (int) this.height;
        super.setLayoutParams(params);
    }

    public void setRefreshTime(long rate) {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer.purge();
        }
        this.refreshRate = rate;
        if (this.refreshRate < 0) {
            stopFetchAdRequestTimer();
            return;
        }
        if (this.refreshRate < MINIMUM_REFRESH_TIME) {
            this.refreshRate = MINIMUM_REFRESH_TIME;
        }
        try {
            this.timer = new Timer();
            this.timer.scheduleAtFixedRate(new FetchAdResponseThread(this.handler), this.refreshRate, this.refreshRate);
        } catch (Exception e) {
        }
    }

    public void setAllowAutoplay(boolean auto) {
        this.allowAutoplay = auto ? 1 : 0;
    }

    public void setShouldRotate(boolean shouldRotate) {
        this.rotate = shouldRotate;
    }

    public void setTestMode(boolean t) {
        this.testMode = t;
    }

    public void setAdSpace(String a) {
        this.adSpace = a;
    }

    public boolean allowAutoplay() {
        return this.allowAutoplay == 1;
    }

    public boolean shouldRotate() {
        return this.rotate;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            stopFetchAdRequestTimer();
            if (this.ad != null) {
                this.ad.onPause();
            }
        } else if (this.ad != null) {
            if (!this.isManuallyPaused) {
                resume();
            }
            if (this.ad != null) {
                this.ad.onResume();
            }
        }
    }

    private void stopFetchAdRequestTimer() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer.purge();
        }
    }

    public void pause() {
        this.isManuallyPaused = true;
        stopFetchAdRequestTimer();
    }

    public void resume() {
        if (this.controller.isEnabled(this.size)) {
            if (this.refreshRate != 0) {
                setRefreshTime(this.refreshRate);
            } else {
                setRefreshTime(this.controller.getRefreshTime(this.size));
            }
        }
        this.isManuallyPaused = false;
    }

    private void initialize(Activity a) {
        try {
            Mobclix.onCreate(a);
            if (this.controller.getUserAgent().equals("")) {
                WebSettings settings = new WebView(getContext()).getSettings();
                try {
                    this.controller.setUserAgent((String) settings.getClass().getDeclaredMethod("getUserAgentString", new Class[0]).invoke(settings, new Object[0]));
                } catch (Exception e) {
                    this.controller.setUserAgent("Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2");
                }
            }
        } catch (Exception e2) {
        }
        setBackgroundColor(this.backgroundColor);
        CookieManager.getInstance().setAcceptCookie(true);
        try {
            this.adSpace = getTag().toString();
        } catch (Exception e3) {
        }
        try {
            Class<?> clazz = Class.forName("com.admob.android.ads.AdManager");
            clazz.getMethod("setTestDevices", String[].class).invoke(null, new String[]{(String) clazz.getField("TEST_EMULATOR").get(null)});
        } catch (Exception e4) {
        }
        new Thread(new WaitForRemoteConfigThread(this, null)).start();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        if (this.timer != null) {
            this.timer.cancel();
            this.timer.purge();
        }
        if (this.ad == null) {
            return null;
        }
        this.ad.onPause();
        Bundle savedInstanceState = new Bundle();
        savedInstanceState.putString("response", this.ad.rawResponse);
        return savedInstanceState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(View.BaseSavedState.EMPTY_STATE);
        try {
            String response = ((Bundle) state).getString("response");
            if (!response.equals("")) {
                restoreAd(response);
                this.restored = true;
            }
        } catch (Exception e) {
        }
    }

    public void getAd() {
        if (this.adThread != null) {
            this.adThread.interrupt();
        }
        this.adThread = new Thread(new FetchAdResponseThread(this.handler));
        this.adThread.start();
    }

    /* access modifiers changed from: package-private */
    public void getAd(String params) {
        if (this.adThread != null) {
            this.adThread.interrupt();
        }
        FetchAdResponseThread fetchAdResponseThread = new FetchAdResponseThread(this.handler);
        fetchAdResponseThread.setNextRequestParams(params);
        this.adThread = new Thread(fetchAdResponseThread);
        this.adThread.start();
    }

    private void restoreAd(String response) {
        try {
            Mobclix.onCreate((Activity) getContext());
        } catch (Exception e) {
        }
        try {
            if (this.adThread != null) {
                this.adThread.interrupt();
            }
            if (this.controller.isEnabled(this.size)) {
                this.ad = new MobclixCreative(this, response, true);
                setRefreshTime(this.controller.getRefreshTime(this.size));
                return;
            }
            Iterator<MobclixAdViewListener> it = this.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    listener.onFailedLoad(this, MobclixAdViewListener.ADSIZE_DISABLED);
                }
            }
        } catch (Exception e2) {
        }
    }

    public void cancelAd() {
        if (this.adThread != null) {
            this.adThread.interrupt();
        }
        if (this.timer != null) {
            this.timer.cancel();
            this.timer.purge();
        }
    }

    private class WaitForRemoteConfigThread implements Runnable {
        private WaitForRemoteConfigThread() {
        }

        /* synthetic */ WaitForRemoteConfigThread(MobclixAdView mobclixAdView, WaitForRemoteConfigThread waitForRemoteConfigThread) {
            this();
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x0013  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                long r1 = java.lang.System.currentTimeMillis()
                java.lang.Long r0 = java.lang.Long.valueOf(r1)
            L_0x0008:
                com.mobclix.android.sdk.MobclixAdView r1 = com.mobclix.android.sdk.MobclixAdView.this
                com.mobclix.android.sdk.Mobclix r1 = r1.controller
                int r1 = r1.isRemoteConfigSet()
                r2 = 1
                if (r1 == r2) goto L_0x0022
                long r1 = java.lang.System.currentTimeMillis()
                long r3 = r0.longValue()
                long r1 = r1 - r3
                r3 = 10000(0x2710, double:4.9407E-320)
                int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
                if (r1 < 0) goto L_0x0008
            L_0x0022:
                com.mobclix.android.sdk.MobclixAdView r1 = com.mobclix.android.sdk.MobclixAdView.this
                com.mobclix.android.sdk.MobclixAdView$RemoteConfigReadyHandler r1 = r1.rcHandler
                r2 = 0
                r1.sendEmptyMessage(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixAdView.WaitForRemoteConfigThread.run():void");
        }
    }

    private class RemoteConfigReadyHandler extends Handler {
        private RemoteConfigReadyHandler() {
        }

        /* synthetic */ RemoteConfigReadyHandler(MobclixAdView mobclixAdView, RemoteConfigReadyHandler remoteConfigReadyHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            if (!MobclixAdView.this.restored) {
                if (MobclixAdView.this.controller.isEnabled(MobclixAdView.this.size)) {
                    MobclixAdView.this.getAd();
                    if (MobclixAdView.this.refreshRate != 0) {
                        MobclixAdView.this.setRefreshTime(MobclixAdView.this.refreshRate);
                    } else if (MobclixAdView.this.controller.getRefreshTime(MobclixAdView.this.size) >= MobclixAdView.MINIMUM_REFRESH_TIME) {
                        MobclixAdView.this.setRefreshTime(MobclixAdView.this.controller.getRefreshTime(MobclixAdView.this.size));
                    }
                    if (MobclixAdView.this.allowAutoplay == -1) {
                        MobclixAdView.this.setAllowAutoplay(MobclixAdView.this.controller.shouldAutoplay(MobclixAdView.this.size));
                        return;
                    }
                    return;
                }
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        listener.onFailedLoad(MobclixAdView.this, MobclixAdViewListener.ADSIZE_DISABLED);
                    }
                }
            }
        }
    }

    private class FetchAdResponseThread extends Mobclix.FetchResponseThread {
        String nextRequestParams = "";

        FetchAdResponseThread(Handler h) {
            super("", h);
        }

        public void run() {
            try {
                if (!((ActivityManager) MobclixAdView.this.context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName().equals(MobclixAdView.this.context.getPackageName())) {
                    MobclixAdView.this.ordinal = 0;
                    return;
                }
            } catch (Exception e) {
            }
            setUrl(getAdUrl());
            super.run();
        }

        /* access modifiers changed from: package-private */
        public void setNextRequestParams(String p) {
            if (p == null) {
                p = "";
            }
            this.nextRequestParams = p;
        }

        private String getAdUrl() {
            StringBuffer data = new StringBuffer();
            StringBuffer keywordsBuffer = new StringBuffer();
            String keywords = "";
            StringBuffer queryBuffer = new StringBuffer();
            try {
                data.append(MobclixAdView.this.controller.getAdServer());
                data.append("?p=android");
                if (MobclixAdView.this.adCode.equals("")) {
                    data.append("&i=").append(URLEncoder.encode(MobclixAdView.this.controller.getApplicationId(), "UTF-8"));
                    data.append("&s=").append(URLEncoder.encode(MobclixAdView.this.size, "UTF-8"));
                } else {
                    data.append("&a=").append(URLEncoder.encode(MobclixAdView.this.adCode, "UTF-8"));
                }
                data.append("&av=").append(URLEncoder.encode(MobclixAdView.this.controller.getApplicationVersion(), "UTF-8"));
                data.append("&u=").append(URLEncoder.encode(MobclixAdView.this.controller.getDeviceId(), "UTF-8"));
                data.append("&andid=").append(URLEncoder.encode(MobclixAdView.this.controller.getAndroidId(), "UTF-8"));
                data.append("&v=").append(URLEncoder.encode(MobclixAdView.this.controller.getMobclixVersion()));
                data.append("&ct=").append(URLEncoder.encode(MobclixAdView.this.controller.getConnectionType()));
                if (!MobclixAdView.this.controller.getGPS().equals("null")) {
                    data.append("&ll=").append(URLEncoder.encode(MobclixAdView.this.controller.getGPS(), "UTF-8"));
                }
                data.append("&l=").append(URLEncoder.encode(MobclixAdView.this.controller.getLocale(), "UTF-8"));
                data.append("&dt=").append(URLEncoder.encode(Build.MODEL, "UTF-8"));
                data.append("&sv=").append(URLEncoder.encode(MobclixAdView.this.controller.getAndroidVersion(), "UTF-8"));
                data.append("&ua=").append(URLEncoder.encode(MobclixAdView.this.controller.getUserAgent(), "UTF-8"));
                data.append("&o=").append(MobclixAdView.this.ordinal);
                MobclixAdView.this.ordinal++;
                if (MobclixAdView.this.allowAutoplay == 1) {
                    data.append("&ap=1");
                } else {
                    data.append("&ap=0");
                }
                if (MobclixAdView.this.adSpace != null && !MobclixAdView.this.adSpace.equals("")) {
                    data.append("&as=").append(URLEncoder.encode(MobclixAdView.this.adSpace));
                }
                if (MobclixAdView.this.testMode) {
                    data.append("&t=1");
                }
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        keywords = listener.keywords();
                    }
                    if (keywords == null) {
                        keywords = "";
                    }
                    if (!keywords.equals("")) {
                        if (keywordsBuffer.length() == 0) {
                            keywordsBuffer.append("&k=").append(URLEncoder.encode(keywords, "UTF-8"));
                        } else {
                            keywordsBuffer.append("%2C").append(URLEncoder.encode(keywords, "UTF-8"));
                        }
                    }
                    String query = listener.query();
                    if (query == null) {
                        query = "";
                    }
                    if (!query.equals("")) {
                        if (queryBuffer.length() == 0) {
                            queryBuffer.append("&q=").append(URLEncoder.encode(query, "UTF-8"));
                        } else {
                            queryBuffer.append("%2B").append(URLEncoder.encode(keywords, "UTF-8"));
                        }
                    }
                }
                if (keywordsBuffer.length() > 0) {
                    data.append(keywordsBuffer);
                }
                if (queryBuffer.length() > 0) {
                    data.append(queryBuffer);
                }
                if (!this.nextRequestParams.equals("")) {
                    data.append(this.nextRequestParams);
                }
                this.nextRequestParams = "";
                return data.toString();
            } catch (Exception e) {
                return "";
            }
        }
    }

    private class AdResponseHandler extends Handler {
        private AdResponseHandler() {
        }

        /* synthetic */ AdResponseHandler(MobclixAdView mobclixAdView, AdResponseHandler adResponseHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            String type = msg.getData().getString("type");
            if (type.equals("success")) {
                if (MobclixAdView.this.ad != null) {
                    MobclixAdView.this.prevAd = MobclixAdView.this.ad;
                }
                String response = msg.getData().getString("response");
                MobclixAdView.this.ad = new MobclixCreative(MobclixAdView.this, response, false);
            } else if (type.equals("failure")) {
                int errorCode = msg.getData().getInt("errorCode");
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.listeners.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener listener = it.next();
                    if (listener != null) {
                        listener.onFailedLoad(MobclixAdView.this, errorCode);
                    }
                }
            }
        }
    }
}
