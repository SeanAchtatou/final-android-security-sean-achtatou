package com.mobclix.android.sdk;

public class MobclixSimpleAdViewListener implements MobclixAdViewListener {
    public void onSuccessfulLoad(MobclixAdView adView) {
    }

    public void onFailedLoad(MobclixAdView adView, int errorCode) {
    }

    public void onAdClick(MobclixAdView adView) {
    }

    public void onCustomAdTouchThrough(MobclixAdView adView, String string) {
    }

    public boolean onOpenAllocationLoad(MobclixAdView adView, int openAllocationCode) {
        return false;
    }

    public String keywords() {
        return null;
    }

    public String query() {
        return null;
    }
}
