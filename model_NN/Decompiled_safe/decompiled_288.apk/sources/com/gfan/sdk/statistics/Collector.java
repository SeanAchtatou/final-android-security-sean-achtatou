package com.gfan.sdk.statistics;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.Thread;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Collector {
    private static final String MAC = "FF9B8CB4-E13B-44a7-B3C6-B385D8EB8167";
    private static final long SESSION_TIMEOUT = 15000;
    private static final String TYPE = "tongjisdk";
    private static final String URL = "http://data.gfan.com";
    private static final String VERSION = "0.3.2";
    private static long activityPauseTime = 0;
    private static long activityResumeTime = 0;
    private static long appStarttime = 0;
    /* access modifiers changed from: private */
    public static String appkey = "";
    private static Context context;
    /* access modifiers changed from: private */
    public static String cpid = "";
    private static Handler hander;
    private static String opid = "gfan";
    /* access modifiers changed from: private */
    public static String rid = "";
    private static TelephonyManager telManager;
    private static long timesum = 0;

    public interface IResponse {
        void onFailed(Exception exc);

        void onSuccess(HttpResponse httpResponse);
    }

    static {
        HandlerThread localHandlerThread = new HandlerThread("Statistics");
        localHandlerThread.start();
        hander = new Handler(localHandlerThread.getLooper());
    }

    private static String getSimSerialNumber() {
        return telManager.getSimSerialNumber();
    }

    private static String getIMEI() {
        return telManager.getDeviceId();
    }

    private static String getCompanyID() {
        String sim = getSimSerialNumber();
        if (sim == null) {
            return "no sim";
        }
        try {
            return sim.substring(4, 6);
        } catch (Exception e) {
            return "no sim";
        }
    }

    private static String getDevice() {
        return Build.MODEL;
    }

    private static String getOS() {
        return Build.VERSION.SDK;
    }

    private static String getMetricsd(Context context2) {
        if (!(context2 instanceof Activity)) {
            return "";
        }
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context2).getWindowManager().getDefaultDisplay().getMetrics(dm);
        return String.valueOf(dm.widthPixels) + "x" + dm.heightPixels;
    }

    private static String getProvince() {
        String sim = getSimSerialNumber();
        if (sim == null) {
            return "no sim";
        }
        try {
            return sim.substring(8, 10);
        } catch (Exception e) {
            return "no sim";
        }
    }

    private static String getCountry() {
        String sim = getSimSerialNumber();
        if (sim == null) {
            return "no sim";
        }
        try {
            return sim.substring(0, 4);
        } catch (Exception e) {
            return "no sim";
        }
    }

    private static String getRid(Context context2) {
        ConnectDBUtil db = ConnectDBUtil.getConnection(context2);
        Cursor cursor = db.ridSelect();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            rid = cursor.getString(cursor.getColumnIndex("rid"));
        }
        cursor.close();
        db.close();
        return rid;
    }

    private static String getPackageName(Context context2) {
        return context2.getPackageName();
    }

    private static String getVersion(Context context2) {
        try {
            return context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("SDK", "not found app version");
            return "";
        }
    }

    private static void setActivityPauseTime() {
        activityPauseTime = System.currentTimeMillis();
        timesum += activityPauseTime - activityResumeTime;
    }

    private static void sendMoblieInfo(final Context context2, final long appStarttime2) throws Exception {
        if (validate()) {
            StringBuilder str = new StringBuilder();
            str.append("{\"msgtype\":\"baseinfo\",\"body\":");
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("country", getCountry()).put("companyid", getCompanyID()).put("device", getDevice()).put("imei", getIMEI()).put("metricsd", getMetricsd(context2)).put("os", getOS()).put("province", getProvince()).put("sim", getSimSerialNumber());
                str.append(jsonObject);
            } catch (JSONException e) {
                Log.e("SDK", "e", e);
            }
            buildTail(str, VERSION, TYPE);
            hander.post(new MessageHandler(str.toString(), new IResponse() {
                public void onSuccess(HttpResponse response) {
                    String result = "";
                    try {
                        result = EntityUtils.toString(response.getEntity(), "UTF-8");
                    } catch (ParseException e) {
                        Log.e("SDK", "e", e);
                    } catch (IOException e2) {
                        Log.e("SDK", "e", e2);
                    }
                    if (result.indexOf(",") > 0) {
                        try {
                            String[] data1 = result.split(",")[1].split(":");
                            Collector.rid = data1[1].substring(1, data1[1].length() - 2);
                            ConnectDBUtil db = ConnectDBUtil.getConnection(context2);
                            db.ridInsert(Collector.rid);
                            db.close();
                            try {
                                Collector.appkey = Collector.getAppkey(context2);
                                Collector.cpid = Collector.getCpID(context2);
                                Collector.sendCpInfo(context2);
                                Collector.sendAppInfo(context2, appStarttime2);
                                Collector.sendApkListInfo(context2);
                            } catch (PackageManager.NameNotFoundException e3) {
                                Log.e("SDK", "the did or cpid does not set");
                            }
                        } catch (Exception e4) {
                            Log.e("SDK", "rid error, maybe the appkey is wrong.");
                        }
                    } else {
                        Log.e("SDK", result);
                    }
                }

                public void onFailed(Exception e) {
                    Log.e("SDK", "sendMoblieInfo failed");
                }
            }));
            return;
        }
        Log.e("SDK", "the SDK configured failed to initialize");
    }

    private static void buildTail(StringBuilder str, String version, String type) {
        str.append(",\"mac\":\"").append(MAC).append("\",\"appkey\":\"").append(appkey).append("\",\"cpid\":\"").append(cpid).append("\",\"cpidmac\":\"").append(getCpidMac()).append("\",\"opid\":\"").append(opid).append("\",\"sdkversion\":\"").append(version).append("\",\"type\":\"").append(type).append("\"}");
    }

    public static void setAppClickCount(String clickname) {
        if (!validate() || context == null) {
            Log.e("SDK", "the SDK configured failed to initialize");
            return;
        }
        ConnectDBUtil db = ConnectDBUtil.getConnection(context);
        Cursor cursor = db.AppSelectClickname(clickname, appStarttime);
        if (cursor.getCount() == 0) {
            db.AppInsert(clickname, appStarttime, 1);
        } else {
            cursor.moveToFirst();
            db.AppUpdate(clickname, appStarttime, cursor.getInt(cursor.getColumnIndex("clickcount")) + 1);
        }
        cursor.close();
        db.close();
    }

    private static String getAppClickCount(Context context2, long starttime) {
        StringBuilder clickcountlist = new StringBuilder();
        ConnectDBUtil db = ConnectDBUtil.getConnection(context2);
        Cursor cursor = db.AppSelect(starttime);
        if (cursor.getCount() == 0) {
            clickcountlist.append("[{\"\":\"0\"}]");
        } else {
            clickcountlist.append("[");
            cursor.moveToFirst();
            do {
                String clickname = cursor.getString(cursor.getColumnIndex("clickname"));
                clickcountlist.append("{\"").append(clickname).append("\":\"").append(cursor.getInt(cursor.getColumnIndex("clickcount"))).append("\"},");
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return String.valueOf(clickcountlist.substring(0, clickcountlist.length() - 1)) + "]";
    }

    /* access modifiers changed from: private */
    public static void clearAppClickCount(Context context2, long appStarttime2) {
        ConnectDBUtil db = ConnectDBUtil.getConnection(context2);
        db.AppClear(appStarttime2);
        db.close();
    }

    /* access modifiers changed from: private */
    public static void sendAppInfo(Context context2, long appStarttime2) throws Exception {
        if (validate()) {
            ConnectDBUtil db = ConnectDBUtil.getConnection(context2);
            Cursor cursor = db.BackupStartInfoSelect();
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    postAppInfo(context2, cursor.getString(cursor.getColumnIndex("version")), cursor.getLong(cursor.getColumnIndex("starttime")), cursor.getString(cursor.getColumnIndex("mac")), cursor.getString(cursor.getColumnIndex("cpidmac")), cursor.getString(cursor.getColumnIndex("opid")), cursor.getString(cursor.getColumnIndex("sdk_version")), cursor.getString(cursor.getColumnIndex("sdk_type")), cursor.getLong(cursor.getColumnIndex("id")));
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
            postAppInfo(context2, getVersion(context2), appStarttime2, MAC, getCpidMac(), opid, VERSION, TYPE, -1);
            return;
        }
        Log.e("SDK", "the SDK configured failed to initialize");
    }

    private static void sendLeaveInfo(Context context2, long appStarttime2, long timesum2) throws Exception {
        if (validate()) {
            sendCachedLeaveInfo(context2);
            postLeaveInfo(context2, getVersion(context2), appStarttime2, System.currentTimeMillis(), timesum2, MAC, getCpidMac(), opid, VERSION, TYPE, -1);
            return;
        }
        Log.e("SDK", "the SDK configured failed to initialize");
    }

    private static void sendCachedLeaveInfo(Context context2) {
        ConnectDBUtil db = ConnectDBUtil.getConnection(context2);
        Cursor cursor = db.BackupAppInfoSelect();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                postLeaveInfo(context2, cursor.getString(cursor.getColumnIndex("version")), cursor.getLong(cursor.getColumnIndex("starttime")), cursor.getLong(cursor.getColumnIndex("endtime")), cursor.getLong(cursor.getColumnIndex("timesum")), cursor.getString(cursor.getColumnIndex("mac")), cursor.getString(cursor.getColumnIndex("cpidmac")), cursor.getString(cursor.getColumnIndex("opid")), cursor.getString(cursor.getColumnIndex("sdk_version")), cursor.getString(cursor.getColumnIndex("sdk_type")), cursor.getLong(cursor.getColumnIndex("id")));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
    }

    private static void postLeaveInfo(Context context2, String version, long starttime, long endtime, long timesum2, String mac, String cpidmac, String opid2, String sdkVersion, String sdkType, long backupId) {
        try {
            StringBuilder str = new StringBuilder();
            str.append("{\"msgtype\":\"leaveinfo\",\"body\":");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("rid", rid).put("version", version).put("starttime", Long.toString(starttime)).put("endtime", Long.toString(endtime)).put("timesum", Long.toString(timesum2));
            str.append(jsonObject);
            buildTail(str, sdkVersion, sdkType);
            str.insert(str.length() - 1, ",\"clickcount\":" + getAppClickCount(context2, starttime));
            Handler handler = hander;
            String sb = str.toString();
            final Context context3 = context2;
            final long j = starttime;
            final long j2 = backupId;
            final String str2 = version;
            final long j3 = endtime;
            final long j4 = timesum2;
            final String str3 = mac;
            final String str4 = cpidmac;
            final String str5 = opid2;
            final String str6 = sdkVersion;
            final String str7 = sdkType;
            handler.post(new MessageHandler(sb, new IResponse() {
                public void onSuccess(HttpResponse response) {
                    Collector.clearAppClickCount(context3, j);
                    if (j2 > 0) {
                        ConnectDBUtil db = ConnectDBUtil.getConnection(context3);
                        db.BackupAppInfoClear(j2);
                        db.close();
                    }
                }

                public void onFailed(Exception e) {
                    if (j2 < 0) {
                        ConnectDBUtil db = ConnectDBUtil.getConnection(context3);
                        db.BackupAppInfoInsert(str2, j, j3, j4, str3, str4, str5, str6, str7);
                        db.close();
                    }
                }
            }));
        } catch (JSONException e) {
            Log.e("SDK", "JSONException in postLeaveInfo");
        }
    }

    private static void postAppInfo(Context context2, String version, long starttime, String mac, String cpidmac, String opid2, String sdkVersion, String sdkType, long backupId) {
        try {
            StringBuilder str = new StringBuilder();
            str.append("{\"msgtype\":\"appinfo\",\"body\":");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("rid", rid).put("version", version).put("starttime", Long.toString(starttime)).put("endtime", "0").put("timesum", "0").put("clickcount", new JSONArray());
            str.append(jsonObject);
            buildTail(str, sdkVersion, sdkType);
            Handler handler = hander;
            String sb = str.toString();
            final long j = backupId;
            final Context context3 = context2;
            final String str2 = version;
            final long j2 = starttime;
            final String str3 = mac;
            final String str4 = cpidmac;
            final String str5 = opid2;
            final String str6 = sdkVersion;
            final String str7 = sdkType;
            handler.post(new MessageHandler(sb, new IResponse() {
                public void onSuccess(HttpResponse response) {
                    if (j > 0) {
                        ConnectDBUtil db = ConnectDBUtil.getConnection(context3);
                        db.BackupStartInfoClear(j);
                        db.close();
                    }
                }

                public void onFailed(Exception e) {
                    if (j < 0) {
                        ConnectDBUtil db = ConnectDBUtil.getConnection(context3);
                        db.BackupStartInfoInsert(str2, j2, str3, str4, str5, str6, str7);
                        db.close();
                    }
                }
            }));
        } catch (JSONException e) {
            Log.e("SDK", "JSONException in postappinfo");
        }
    }

    private static class ApkInfo implements Runnable {
        private static final Object mutex = new Object();
        private Context context;
        private IResponse iResponse;
        private String message;

        public ApkInfo(String message2, IResponse iResponse2, Context context2) {
            this.message = message2;
            this.iResponse = iResponse2;
            this.context = context2;
        }

        public void run() {
            synchronized (mutex) {
                StringBuilder pkgname = new StringBuilder();
                List<ApplicationInfo> list = this.context.getPackageManager().getInstalledApplications(0);
                int count = list.size();
                for (int i = 0; i < count; i++) {
                    pkgname.append(list.get(i).packageName);
                    pkgname.append(",");
                }
                if (pkgname.length() > 0) {
                    this.message = this.message.replace("${packagelist}", pkgname.substring(0, pkgname.length() - 1));
                } else {
                    this.message = this.message.replace("${packagelist}", "");
                }
                boolean unused = Collector.sendMessage(this.message, this.iResponse);
            }
        }
    }

    public static void sendApkListInfo(final Context context2) throws Exception {
        if (validate()) {
            StringBuilder str = new StringBuilder();
            str.append("{\"msgtype\":\"applist\",\"body\":");
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("rid", rid);
                jsonObject.put("packagelist", "${packagelist}");
                str.append(jsonObject);
                buildTail(str, VERSION, TYPE);
                hander.post(new ApkInfo(str.toString(), new IResponse() {
                    public void onSuccess(HttpResponse response) {
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context2.getApplicationContext());
                        if (preferences != null) {
                            preferences.edit().putLong("com.gfan.sdk.lastSendAppListTime", System.currentTimeMillis()).commit();
                        }
                    }

                    public void onFailed(Exception e) {
                    }
                }, context2));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("error", "the SDK configured failed to initialize");
        }
    }

    public static void comment(Context context2, String comment, IResponse response) {
        if (validate()) {
            try {
                if (!rid.equals("") && comment != null) {
                    StringBuilder str = new StringBuilder();
                    str.append("{\"msgtype\":\"plinfo\",\"body\":");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("rid", rid);
                    jsonObject.put("comment", comment);
                    jsonObject.put("version", getVersion(context2));
                    str.append(jsonObject);
                    buildTail(str, VERSION, TYPE);
                    hander.post(new MessageHandler(str.toString(), response));
                }
            } catch (JSONException e) {
                response.onFailed(e);
                Log.e("SDK", "sendPlInfo failed");
            }
        } else {
            response.onFailed(new RuntimeException("sdk未初始化"));
            Log.e("error", "the SDK configured failed to initialize");
        }
    }

    private static String getCpidMac() {
        return "";
    }

    /* access modifiers changed from: private */
    public static void sendCpInfo(Context context2) throws Exception {
        if (validate()) {
            try {
                StringBuilder str = new StringBuilder();
                str.append("{\"msgtype\":\"cpinfo\",\"body\":");
                str.append(new JSONObject().put("rid", rid).put("cpid", cpid));
                buildTail(str, VERSION, TYPE);
                hander.post(new MessageHandler(str.toString(), new IResponse() {
                    public void onSuccess(HttpResponse response) {
                    }

                    public void onFailed(Exception e) {
                        Log.e("SDK", "sendCpInfo failed");
                    }
                }));
            } catch (JSONException e) {
                Log.e("SDK", "sendCpInfo failed");
            }
        } else {
            Log.e("SDK", "the SDK configured failed to initialize");
        }
    }

    private static final class MessageHandler implements Runnable {
        private static final Object mutex = new Object();
        private IResponse iResponse;
        private String message;

        MessageHandler(String message2, IResponse iResponse2) {
            this.iResponse = iResponse2;
            this.message = message2;
        }

        public void run() {
            try {
                synchronized (mutex) {
                    boolean unused = Collector.sendMessage(this.message, this.iResponse);
                }
            } catch (Exception e) {
                Log.e("SDK", "Exception occurred when sending message.");
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean sendMessage(String message, IResponse iResponse) {
        HttpPost httppost = new HttpPost(URL);
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, 60000);
        HttpConnectionParams.setSoTimeout(params, 60000);
        HttpClient httpclient = new DefaultHttpClient(params);
        httppost.setHeader("Content-Type", "application/json");
        try {
            httppost.setEntity(new StringEntity(message, "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);
            int code = response.getStatusLine().getStatusCode();
            if (200 == code) {
                if (iResponse != null) {
                    iResponse.onSuccess(response);
                }
                httpclient.getConnectionManager().shutdown();
                return true;
            }
            if (iResponse != null) {
                iResponse.onFailed(new IOException(new StringBuilder(String.valueOf(code)).toString()));
            }
            httpclient.getConnectionManager().shutdown();
            return false;
        } catch (ClientProtocolException e) {
            ClientProtocolException e2 = e;
            if (iResponse != null) {
                iResponse.onFailed(e2);
            }
            httpclient.getConnectionManager().shutdown();
            return false;
        } catch (IOException e3) {
            IOException e4 = e3;
            if (iResponse != null) {
                iResponse.onFailed(e4);
            }
            httpclient.getConnectionManager().shutdown();
        } catch (Exception e5) {
            Exception e6 = e5;
            if (iResponse != null) {
                iResponse.onFailed(e6);
            }
            httpclient.getConnectionManager().shutdown();
        } catch (Throwable th) {
            httpclient.getConnectionManager().shutdown();
            throw th;
        }
        return false;
    }

    private static boolean validate() {
        if ("".equals(appkey)) {
            return false;
        }
        return true;
    }

    public static void onError(Context context2) {
        Thread.UncaughtExceptionHandler currentHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (!(currentHandler instanceof DefaultExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(currentHandler, context2));
        }
    }

    private static String[] searchForStackTraces(Context context2) {
        File dir = new File(String.valueOf(context2.getFilesDir().getAbsolutePath()) + "/");
        dir.mkdir();
        return dir.list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".stacktrace");
            }
        });
    }

    private static void sendErrorsInfo(Context context2) {
        if (validate()) {
            String rid2 = getRid(context2);
            final String[] traces = searchForStackTraces(context2);
            if (traces != null && traces.length > 0) {
                final String dir = context2.getFilesDir().getAbsolutePath();
                StringBuilder str = new StringBuilder();
                str.append("{\"msgtype\":\"errorinfo\",\"body\":");
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("rid", rid2).put("version", getVersion(context2)).put("device", getDevice()).put("os", getOS());
                    JSONArray jsonArray = new JSONArray();
                    jsonObject.put("errors", jsonArray);
                    for (int i = 0; i < traces.length; i++) {
                        JSONObject errorObject = new JSONObject();
                        String time = traces[i].substring(0, traces[i].indexOf("."));
                        StringBuilder contents = new StringBuilder();
                        BufferedReader input = new BufferedReader(new FileReader(String.valueOf(dir) + "/" + traces[i]));
                        while (true) {
                            String line = input.readLine();
                            if (line == null) {
                                break;
                            }
                            contents.append(line);
                            contents.append(System.getProperty("line.separator"));
                        }
                        input.close();
                        errorObject.put("time", time).put("error", contents.toString());
                        jsonArray.put(i, errorObject);
                    }
                } catch (FileNotFoundException | IOException | Exception | JSONException e) {
                }
                str.append(jsonObject);
                buildTail(str, VERSION, TYPE);
                hander.post(new MessageHandler(str.toString(), new IResponse() {
                    public void onSuccess(HttpResponse response) {
                        int i = 0;
                        while (i < traces.length) {
                            try {
                                new File(String.valueOf(dir) + "/" + traces[i]).delete();
                                i++;
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            }
                        }
                    }

                    public void onFailed(Exception e) {
                        Log.e("SDK", "sendErrorInfo failed");
                    }
                }));
                return;
            }
            return;
        }
        Log.e("SDK", "the SDK configured failed to initialize");
    }

    /* access modifiers changed from: private */
    public static String getCpID(Context context2) throws PackageManager.NameNotFoundException {
        try {
            return context2.getPackageManager().getApplicationInfo(getPackageName(context2), 128).metaData.get("gfan_cpid").toString();
        } catch (NullPointerException e) {
            Log.e("SDK", "The cpid must be set.");
            return "";
        }
    }

    /* access modifiers changed from: private */
    public static String getAppkey(Context context2) throws PackageManager.NameNotFoundException {
        try {
            return context2.getPackageManager().getApplicationInfo(getPackageName(context2), 128).metaData.get("gfan_statistics_appkey").toString();
        } catch (NullPointerException e) {
            Log.e("SDK", "The gfan_statistics_appkey must be set.");
            return "";
        }
    }

    public static void onResume(Context context2) {
        if (context2 instanceof Activity) {
            context = context2;
            activityResumeTime = System.currentTimeMillis();
            if (appkey.length() == 0 || cpid.length() == 0) {
                try {
                    appkey = getAppkey(context2);
                    cpid = getCpID(context2);
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e("SDK", "The did or cpid not set");
                    return;
                }
            }
            if (telManager == null) {
                telManager = (TelephonyManager) context2.getSystemService("phone");
            }
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context2.getApplicationContext());
            if (preferences != null) {
                appStarttime = preferences.getLong("com.gfan.sdk.appStarttime", System.currentTimeMillis());
                activityPauseTime = preferences.getLong("com.gfan.sdk.activityPauseTime", 0);
                timesum = preferences.getLong("com.gfan.sdk.timesum", 0);
                try {
                    if (getRid(context2).length() == 0) {
                        sendMoblieInfo(context2, appStarttime);
                    } else if (activityPauseTime == 0 || activityResumeTime - activityPauseTime > SESSION_TIMEOUT) {
                        sendAppInfo(context2, appStarttime);
                        if (activityResumeTime > 604800000 + preferences.getLong("com.gfan.sdk.lastSendAppListTime", 0)) {
                            sendApkListInfo(context2);
                        }
                        if (activityPauseTime > 0) {
                            sendLeaveInfo(context2, appStarttime, timesum);
                            sendErrorsInfo(context2);
                            appStarttime = activityResumeTime;
                            timesum = 0;
                        }
                    }
                } catch (Exception e2) {
                }
            }
        }
    }

    public static void onPause(Context context2) {
        if (context2 instanceof Activity) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context2.getApplicationContext());
            setActivityPauseTime();
            preferences.edit().putLong("com.gfan.sdk.appStarttime", appStarttime).putLong("com.gfan.sdk.activityPauseTime", activityPauseTime).putLong("com.gfan.sdk.timesum", timesum).commit();
        }
    }
}
