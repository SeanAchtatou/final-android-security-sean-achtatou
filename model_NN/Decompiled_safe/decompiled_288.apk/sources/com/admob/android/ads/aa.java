package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admob.android.ads.ac;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Vector;

/* compiled from: AdMobVideoOverlayNative */
public final class aa {
    ViewGroup a = null;
    boolean b = false;
    private RelativeLayout c = null;
    private RelativeLayout d = null;
    private Button e = null;
    private WeakReference<Activity> f;

    public final void a(Context context, String str, p pVar, float f2, ac acVar, r rVar, WeakReference<Activity> weakReference) {
        this.f = weakReference;
        Rect rect = new Rect(0, 0, (int) (320.0f * f2), (int) (34.0f * f2));
        Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        if (createBitmap != null) {
            Canvas canvas = new Canvas(createBitmap);
            j.a(canvas, rect, -16777216, -1, 127, 0.5f);
            Paint paint = new Paint();
            paint.setColor(-7829368);
            canvas.drawLines(new float[]{0.0f, 0.0f, 320.0f * f2, 0.0f, 0.0f, (34.0f * f2) - 1.0f, 320.0f * f2, (34.0f * f2) - 1.0f}, paint);
            this.c = new RelativeLayout(context);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
            bitmapDrawable.setAlpha(200);
            this.c.setBackgroundDrawable(bitmapDrawable);
        }
        TextView textView = new TextView(context);
        textView.setText(pVar.b);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        textView.setTextSize(14.0f);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextColor(-1);
        textView.setPadding((int) (3.0f * f2), 0, 0, 0);
        layoutParams.addRule(9);
        layoutParams.addRule(15);
        this.c.addView(textView, layoutParams);
        TextView textView2 = new TextView(context);
        textView2.setText(t.a("Ads by AdMob"));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        textView2.setTextSize(11.0f);
        textView2.setTextColor(-1);
        textView2.setPadding(0, 0, (int) (3.0f * f2), 0);
        layoutParams2.addRule(11);
        layoutParams2.addRule(15);
        this.c.addView(textView2, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, (int) (34.0f * f2));
        layoutParams3.addRule(10);
        this.c.setVisibility(4);
        acVar.addView(this.c, layoutParams3);
        Rect rect2 = new Rect(0, 0, (int) (320.0f * f2), (int) (50.0f * f2));
        Bitmap createBitmap2 = Bitmap.createBitmap(rect2.width(), rect2.height(), Bitmap.Config.ARGB_8888);
        if (createBitmap2 != null) {
            Canvas canvas2 = new Canvas(createBitmap2);
            j.a(canvas2, rect2, -16777216, -1, 127, 0.5f);
            Paint paint2 = new Paint();
            paint2.setColor(-7829368);
            canvas2.drawLines(new float[]{0.0f, 0.0f, 320.0f * f2, 0.0f, 0.0f, (50.0f * f2) - 1.0f, 320.0f * f2, (50.0f * f2) - 1.0f}, paint2);
            this.d = new RelativeLayout(context);
            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(createBitmap2);
            bitmapDrawable2.setAlpha(200);
            this.d.setBackgroundDrawable(bitmapDrawable2);
        }
        Vector<o> vector = rVar.h.m;
        if (vector != null) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(0);
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -1);
            Iterator<o> it = vector.iterator();
            while (it.hasNext()) {
                o next = it.next();
                LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams((int) (64.0f * f2), -2);
                Button button = new Button(context);
                BitmapDrawable bitmapDrawable3 = new BitmapDrawable(rVar.b().get(next.a));
                bitmapDrawable3.setBounds(0, 0, (int) (28.0f * f2), (int) (28.0f * f2));
                button.setCompoundDrawables(null, bitmapDrawable3, null, null);
                button.setBackgroundDrawable(null);
                button.setBackgroundColor(0);
                button.setTextSize(12.0f);
                button.setTextColor(-1);
                button.setText(next.c);
                button.setPadding(0, (int) (2.0f * f2), 0, (int) (2.0f * f2));
                button.setOnClickListener(new ac.e(acVar, next, this.f));
                Context context2 = context;
                linearLayout.addView(new x(context2, button, (int) (64.0f * f2), (int) (50.0f * f2), rVar.b().get(next.b)), layoutParams5);
                ImageView imageView = new ImageView(context);
                Bitmap createBitmap3 = Bitmap.createBitmap(1, (int) (34.0f * f2), Bitmap.Config.ARGB_8888);
                if (createBitmap3 == null) {
                    imageView = null;
                } else {
                    Canvas canvas3 = new Canvas(createBitmap3);
                    Paint paint3 = new Paint();
                    paint3.setColor(-7829368);
                    canvas3.drawLines(new float[]{0.0f, 0.0f, 0.0f, 34.0f * f2}, paint3);
                    imageView.setBackgroundDrawable(new BitmapDrawable(createBitmap3));
                }
                linearLayout.addView(imageView, layoutParams4);
            }
            RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams6.addRule(15);
            this.d.addView(linearLayout, layoutParams6);
        }
        this.e = new Button(context);
        this.e.setOnClickListener(new ac.i(acVar, false));
        this.e.setBackgroundResource(17301509);
        this.e.setTextSize(1, 13.0f);
        this.e.setText(str);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams((int) (54.0f * f2), (int) (36.0f * f2));
        layoutParams7.addRule(11);
        layoutParams7.addRule(15);
        layoutParams7.setMargins(0, 0, (int) (2.0f * f2), 0);
        this.d.addView(this.e, layoutParams7);
        RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-1, (int) (50.0f * f2));
        layoutParams8.addRule(12);
        this.d.setVisibility(4);
        acVar.addView(this.d, layoutParams8);
        acVar.setOnTouchListener(new ac.d(acVar));
    }

    public final void a() {
        ac.b(this.c);
        ac.b(this.d);
        this.b = false;
    }

    public final void b() {
        this.d.bringToFront();
        this.c.bringToFront();
        ac.a(this.d);
        ac.a(this.c);
        this.b = true;
    }
}
