package com.admob.android.ads;

import android.os.Bundle;
import com.admob.android.ads.AdView;

/* compiled from: MovieButton */
public final class o implements n {
    public String a;
    public String b;
    public String c;
    public r d = new r();
    public String e;
    public String f;

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("ad", this.a);
        bundle.putString("au", this.b);
        bundle.putString("t", this.c);
        bundle.putBundle("oi", AdView.a.a(this.d));
        bundle.putString("ap", this.e);
        bundle.putString("json", this.f);
        return bundle;
    }

    public final boolean a(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        this.a = bundle.getString("ad");
        this.b = bundle.getString("au");
        this.c = bundle.getString("t");
        if (!this.d.a(bundle.getBundle("oi"))) {
            return false;
        }
        this.e = bundle.getString("ap");
        this.f = bundle.getString("json");
        return true;
    }
}
