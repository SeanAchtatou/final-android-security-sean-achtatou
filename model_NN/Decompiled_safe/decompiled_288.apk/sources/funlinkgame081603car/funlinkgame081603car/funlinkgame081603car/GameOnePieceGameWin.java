package funlinkgame081603car.funlinkgame081603car.funlinkgame081603car;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

public class GameOnePieceGameWin extends Activity {
    public static final int END_ID = 3;
    public static final int REARRARY_ID = 2;
    public static final int START_ID = 1;
    private GameCtrlViewWin cv;
    /* access modifiers changed from: private */
    public int dormant = 1000;
    /* access modifiers changed from: private */
    public boolean isCancel = true;
    private RefreshHandler mRedrawHandler = new RefreshHandler();
    private ProgressBar pb;
    private TextView show_RemainTime;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.gamemainwin);
        setVolumeControlStream(3);
        findViews();
        this.mRedrawHandler.sleep((long) this.dormant);
    }

    class RefreshHandler extends Handler {
        RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            if (GameOnePieceGameWin.this.isCancel) {
                GameOnePieceGameWin.this.run();
            }
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    public void run() {
        if (this.cv.PROCESS_VALUE > 0 && this.cv.much != 0) {
            this.cv.PROCESS_VALUE--;
            this.pb.setProgress(this.cv.PROCESS_VALUE);
            this.show_RemainTime.setText(String.valueOf(this.cv.PROCESS_VALUE));
            this.mRedrawHandler.sleep((long) this.dormant);
        } else if (this.cv.PROCESS_VALUE == 0 && this.cv.much != 0) {
            this.cv.setEnabled(false);
            dialogForFail().show();
        } else if (this.cv.PROCESS_VALUE != 0 && this.cv.much == 0) {
            this.cv.setEnabled(false);
            dialogForSucceed().show();
        }
    }

    private void findViews() {
        this.pb = (ProgressBar) findViewById(R.id.pb);
        this.show_RemainTime = (TextView) findViewById(R.id.show_remainTime);
        this.cv = (GameCtrlViewWin) findViewById(R.id.cv);
        ProgressBar progressBar = this.pb;
        this.cv.getClass();
        progressBar.setMax(300);
        this.pb.incrementProgressBy(-1);
        this.pb.setProgress(this.cv.PROCESS_VALUE);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        new AlertDialog.Builder(this).setTitle((int) R.string.exit_title).setMessage((int) R.string.exit_message).setPositiveButton((int) R.string.str_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                GameOnePieceGameWin.this.onKeyDown(3, new KeyEvent(3, 0));
                Intent it = new Intent();
                it.setAction("android.intent.action.MAIN");
                it.addCategory("android.intent.category.HOME");
                it.setFlags(270532608);
                GameOnePieceGameWin.this.startActivity(it);
                GameOnePieceGameWin.this.finish();
                System.gc();
            }
        }).setNegativeButton((int) R.string.str_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
            }
        }).show();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                newPlay();
                break;
            case 2:
                this.cv.rearrange();
                this.cv.PROCESS_VALUE -= 5;
                this.pb.setProgress(this.cv.PROCESS_VALUE);
                break;
            case 3:
                this.isCancel = false;
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.isCancel = false;
        this.pb = null;
        this.cv = null;
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.isCancel = false;
        super.onDestroy();
        System.exit(0);
        Process.killProcess(Process.myPid());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.isCancel = false;
        trynewPlay();
        this.isCancel = true;
        super.onStart();
    }

    public void newPlay() {
        this.cv.reset();
        ProgressBar progressBar = this.pb;
        this.cv.getClass();
        progressBar.setProgress(300);
        GameCtrlViewWin gameCtrlViewWin = this.cv;
        this.cv.getClass();
        gameCtrlViewWin.PROCESS_VALUE = 300;
        this.mRedrawHandler.sleep((long) this.dormant);
        this.cv.setEnabled(true);
    }

    public void trynewPlay() {
    }

    public AlertDialog dialogForSucceed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.icon).setMessage((int) R.string.succeedInfo).setPositiveButton((int) R.string.next, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameOnePieceGameWin gameOnePieceGameWin = GameOnePieceGameWin.this;
                gameOnePieceGameWin.dormant = gameOnePieceGameWin.dormant - 300;
                GameOnePieceGameWin.this.newPlay();
            }
        }).setNeutralButton((int) R.string.again_challenge, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameOnePieceGameWin.this.newPlay();
            }
        });
        return builder.create();
    }

    public AlertDialog dialogForFail() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.icon).setMessage((int) R.string.failInfo).setPositiveButton((int) R.string.again_challenge, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameOnePieceGameWin.this.newPlay();
            }
        }).setNegativeButton((int) R.string.exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameOnePieceGameWin.this.isCancel = false;
                GameOnePieceGameWin.this.finish();
            }
        });
        return builder.create();
    }
}
