package com.kenai.jbosh;

final class g extends a<String> {
    private final String[] a;

    private g(String str) {
        super(str);
        this.a = str.split("\\ +");
    }

    static g a(String str) {
        if (str == null) {
            return null;
        }
        return new g(str);
    }
}
