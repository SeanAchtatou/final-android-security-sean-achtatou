package com.kenai.jbosh;

final class e extends a<String> {
    private final String[] a;

    private e(String str) {
        super(str);
        this.a = str.split("[\\s,]+");
    }

    static e a(String str) {
        if (str == null) {
            return null;
        }
        return new e(str);
    }
}
