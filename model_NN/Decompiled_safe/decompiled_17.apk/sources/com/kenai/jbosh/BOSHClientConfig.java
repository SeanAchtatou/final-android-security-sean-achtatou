package com.kenai.jbosh;

import java.net.URI;
import javax.net.ssl.SSLContext;

public final class BOSHClientConfig {
    private final URI a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final int g;
    private final SSLContext h;
    private final boolean i;

    public static final class Builder {
        private final URI a;
        private final String b;
        private String c;
        private String d;
        private String e;
        private String f;
        private int g;
        private SSLContext h;
        private Boolean i;

        private Builder(URI uri, String str) {
            this.a = uri;
            this.b = str;
        }

        public static Builder a(URI uri, String str) {
            if (uri == null) {
                throw new IllegalArgumentException("Connection manager URI must not be null");
            } else if (str == null) {
                throw new IllegalArgumentException("Target domain must not be null");
            } else {
                String scheme = uri.getScheme();
                if ("http".equals(scheme) || "https".equals(scheme)) {
                    return new Builder(uri, str);
                }
                throw new IllegalArgumentException("Only 'http' and 'https' URI are allowed");
            }
        }

        public Builder a(String str, int i2) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("Proxy host name cannot be null or empty");
            } else if (i2 <= 0) {
                throw new IllegalArgumentException("Proxy port must be > 0");
            } else {
                this.f = str;
                this.g = i2;
                return this;
            }
        }

        public BOSHClientConfig a() {
            return new BOSHClientConfig(this.a, this.b, this.c, this.d == null ? "en" : this.d, this.e, this.f, this.f == null ? 0 : this.g, this.h, this.i == null ? false : this.i.booleanValue());
        }
    }

    private BOSHClientConfig(URI uri, String str, String str2, String str3, String str4, String str5, int i2, SSLContext sSLContext, boolean z) {
        this.a = uri;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = i2;
        this.h = sSLContext;
        this.i = z;
    }

    public URI a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public String f() {
        return this.f;
    }

    public int g() {
        return this.g;
    }
}
