package com.kenai.jbosh;

import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class ac {
    private static final SecureRandom a = new SecureRandom();
    private static final Lock b = new ReentrantLock();
    private AtomicLong c;

    ac() {
        this.c = new AtomicLong();
        this.c = new AtomicLong(b());
    }

    private long b() {
        long nextLong;
        b.lock();
        do {
            try {
                nextLong = a.nextLong() & 9007199254740991L;
            } finally {
                b.unlock();
            }
        } while (nextLong > 9007194959773696L);
        return nextLong;
    }

    public long a() {
        return this.c.getAndIncrement();
    }
}
