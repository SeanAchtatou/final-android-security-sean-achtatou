package com.kenai.jbosh;

import android.content.Context;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

final class d implements ab {
    private final Lock a = new ReentrantLock();
    private BOSHClientConfig b;
    private HttpClient c;

    d() {
        HttpClient.class.getName();
    }

    private HttpClient b(BOSHClientConfig bOSHClientConfig) {
        DefaultHttpClient defaultHttpClient;
        synchronized (this) {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            ConnManagerParams.setMaxTotalConnections(basicHttpParams, 100);
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
            if (!(bOSHClientConfig == null || bOSHClientConfig.f() == null || bOSHClientConfig.g() == 0)) {
                basicHttpParams.setParameter("http.route.default-proxy", new HttpHost(bOSHClientConfig.f(), bOSHClientConfig.g()));
            }
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
            socketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            schemeRegistry.register(new Scheme("https", socketFactory, 443));
            defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
            defaultHttpClient.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(0, false));
        }
        return defaultHttpClient;
    }

    /* JADX INFO: finally extract failed */
    public aa a(x xVar, AbstractBody abstractBody, Context context) {
        this.a.lock();
        try {
            if (this.c == null) {
                this.c = b(this.b);
            }
            HttpClient httpClient = this.c;
            HttpParams params = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            if (xVar != null) {
                HttpConnectionParams.setSoTimeout(params, (int) (1000 * ((long) (((Integer) xVar.b().a()).intValue() + 30))));
            }
            BOSHClientConfig bOSHClientConfig = this.b;
            this.a.unlock();
            return new c(httpClient, bOSHClientConfig, xVar, abstractBody, context);
        } catch (Throwable th) {
            this.a.unlock();
            throw th;
        }
    }

    public void a() {
        this.a.lock();
        try {
            if (this.c != null) {
                this.c.getConnectionManager().shutdown();
            }
        } finally {
            this.b = null;
            this.c = null;
            this.a.unlock();
        }
    }

    public void a(BOSHClientConfig bOSHClientConfig) {
        this.a.lock();
        try {
            this.b = bOSHClientConfig;
            this.c = b(bOSHClientConfig);
        } finally {
            this.a.unlock();
        }
    }
}
