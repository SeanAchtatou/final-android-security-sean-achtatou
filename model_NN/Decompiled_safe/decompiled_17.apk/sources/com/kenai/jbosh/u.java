package com.kenai.jbosh;

import java.util.HashMap;
import java.util.Map;

final class u {
    private final Map<BodyQName, String> a = new HashMap();

    u() {
    }

    /* access modifiers changed from: package-private */
    public Map<BodyQName, String> a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(BodyQName bodyQName, String str) {
        this.a.put(bodyQName, str);
    }
}
