package com.kenai.jbosh;

final class f extends a<String> {
    private f(String str) {
        super(str);
    }

    static f a(String str) {
        if (str == null) {
            return null;
        }
        return new f(str);
    }
}
