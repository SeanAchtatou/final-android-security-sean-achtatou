package com.kenai.jbosh;

import java.util.concurrent.TimeUnit;

final class l extends b {
    private l(String str) {
        super(str);
        a(0);
    }

    static l a(String str) {
        if (str == null) {
            return null;
        }
        return new l(str);
    }

    public int c() {
        return (int) TimeUnit.MILLISECONDS.convert((long) b(), TimeUnit.SECONDS);
    }
}
