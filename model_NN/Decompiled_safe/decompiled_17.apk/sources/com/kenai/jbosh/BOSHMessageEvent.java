package com.kenai.jbosh;

import java.util.EventObject;

public final class BOSHMessageEvent extends EventObject {
    private final AbstractBody a;

    private BOSHMessageEvent(Object obj, AbstractBody abstractBody) {
        super(obj);
        if (abstractBody == null) {
            throw new IllegalArgumentException("message body may not be null");
        }
        this.a = abstractBody;
    }

    static BOSHMessageEvent a(BOSHClient bOSHClient, AbstractBody abstractBody) {
        return new BOSHMessageEvent(bOSHClient, abstractBody);
    }

    static BOSHMessageEvent b(BOSHClient bOSHClient, AbstractBody abstractBody) {
        return new BOSHMessageEvent(bOSHClient, abstractBody);
    }

    public AbstractBody a() {
        return this.a;
    }
}
