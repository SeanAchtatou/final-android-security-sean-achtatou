package com.kenai.jbosh;

abstract class b extends a<Integer> {
    protected b(String str) {
        super(Integer.valueOf(a(str)));
    }

    private static int a(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw new BOSHException("Could not parse an integer from the value provided: " + str, e);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        int intValue = ((Integer) a()).intValue();
        if (intValue < i) {
            throw new BOSHException("Illegal attribute value '" + intValue + "' provided.  " + "Must be >= " + i);
        }
    }

    public int b() {
        return ((Integer) a()).intValue();
    }
}
