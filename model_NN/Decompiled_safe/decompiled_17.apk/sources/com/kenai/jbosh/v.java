package com.kenai.jbosh;

import java.lang.ref.SoftReference;
import java.util.logging.Logger;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

final class v implements t {
    private static final Logger a = Logger.getLogger(v.class.getName());
    private static final ThreadLocal<SoftReference<XmlPullParser>> b = new w();

    v() {
    }

    private static XmlPullParser a() {
        XmlPullParser xmlPullParser = (XmlPullParser) b.get().get();
        if (xmlPullParser != null) {
            return xmlPullParser;
        }
        try {
            XmlPullParserFactory newInstance = XmlPullParserFactory.newInstance();
            newInstance.setNamespaceAware(true);
            newInstance.setValidating(false);
            XmlPullParser newPullParser = newInstance.newPullParser();
            b.set(new SoftReference(newPullParser));
            return newPullParser;
        } catch (Exception e) {
            throw new IllegalStateException("Could not create XmlPull parser", e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0041, code lost:
        r0 = r4.getPrefix();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0045, code lost:
        if (r0 != null) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0047, code lost:
        r0 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1 = r4.getNamespace();
        r2 = r4.getName();
        r5 = new com.kenai.jbosh.QName(r1, r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x005e, code lost:
        if (com.kenai.jbosh.v.a.isLoggable(java.util.logging.Level.FINEST) == false) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0060, code lost:
        com.kenai.jbosh.v.a.finest("Start element: ");
        com.kenai.jbosh.v.a.finest("    prefix: " + r0);
        com.kenai.jbosh.v.a.finest("    URI: " + r1);
        com.kenai.jbosh.v.a.finest("    local: " + r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00af, code lost:
        r0 = com.kenai.jbosh.AbstractBody.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00b7, code lost:
        if (r0.a(r5) != false) goto L_0x018d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00fd, code lost:
        throw new java.lang.IllegalStateException("Root element was not '" + r0.b() + "' in the '" + r0.a() + "' namespace.  (Was '" + r2 + "' in '" + r1 + "')");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00fe, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0117, code lost:
        throw new com.kenai.jbosh.BOSHException("Could not parse body:\n" + r11, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x011e, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0122, code lost:
        if (r2 >= r4.getAttributeCount()) goto L_0x0190;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0124, code lost:
        r0 = r4.getAttributeNamespace(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x012c, code lost:
        if (r0.length() != 0) goto L_0x018b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x012e, code lost:
        r1 = r4.getNamespace(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0134, code lost:
        r0 = r4.getAttributePrefix(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0138, code lost:
        if (r0 != null) goto L_0x013c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x013a, code lost:
        r0 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r5 = r4.getAttributeName(r2);
        r6 = r4.getAttributeValue(r2);
        r0 = com.kenai.jbosh.BodyQName.a(r1, r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0150, code lost:
        if (com.kenai.jbosh.v.a.isLoggable(java.util.logging.Level.FINEST) == false) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0152, code lost:
        com.kenai.jbosh.v.a.finest("        Attribute: {" + r1 + "}" + r5 + " = '" + r6 + "'");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0184, code lost:
        r3.a(r0, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0187, code lost:
        r0 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x018b, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x018d, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        if (com.kenai.jbosh.v.a.isLoggable(java.util.logging.Level.FINEST) == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        com.kenai.jbosh.v.a.finest("Start tag: " + r4.getName());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.kenai.jbosh.u a(java.lang.String r11) {
        /*
            r10 = this;
            com.kenai.jbosh.u r3 = new com.kenai.jbosh.u
            r3.<init>()
            org.xmlpull.v1.XmlPullParser r4 = a()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.io.StringReader r0 = new java.io.StringReader     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r0.<init>(r11)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r4.setInput(r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            int r0 = r4.getEventType()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
        L_0x0015:
            r1 = 1
            if (r0 == r1) goto L_0x0190
            r1 = 2
            if (r0 != r1) goto L_0x0118
            java.util.logging.Logger r0 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Level r1 = java.util.logging.Level.FINEST     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            boolean r0 = r0.isLoggable(r1)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r0 == 0) goto L_0x0041
            java.util.logging.Logger r0 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r1.<init>()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r2 = "Start tag: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r2 = r4.getName()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r1 = r1.toString()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r0.finest(r1)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
        L_0x0041:
            java.lang.String r0 = r4.getPrefix()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r0 != 0) goto L_0x0049
            java.lang.String r0 = ""
        L_0x0049:
            java.lang.String r1 = r4.getNamespace()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r2 = r4.getName()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            com.kenai.jbosh.QName r5 = new com.kenai.jbosh.QName     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r5.<init>(r1, r2, r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Logger r6 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Level r7 = java.util.logging.Level.FINEST     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            boolean r6 = r6.isLoggable(r7)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r6 == 0) goto L_0x00af
            java.util.logging.Logger r6 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r7 = "Start element: "
            r6.finest(r7)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Logger r6 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r7.<init>()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r8 = "    prefix: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r6.finest(r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Logger r0 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r6.<init>()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r7 = "    URI: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r6 = r6.append(r1)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r6 = r6.toString()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r0.finest(r6)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Logger r0 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r6.<init>()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r7 = "    local: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r6 = r6.toString()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r0.finest(r6)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
        L_0x00af:
            com.kenai.jbosh.BodyQName r0 = com.kenai.jbosh.AbstractBody.c()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            boolean r5 = r0.a(r5)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r5 != 0) goto L_0x018d
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r4.<init>()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r5 = "Root element was not '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r5 = r0.b()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r5 = "' in the '"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r0 = r0.a()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r4 = "' namespace.  (Was '"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r2 = "' in '"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r1 = "')"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r3.<init>(r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            throw r3     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
        L_0x00fe:
            r0 = move-exception
        L_0x00ff:
            com.kenai.jbosh.BOSHException r1 = new com.kenai.jbosh.BOSHException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not parse body:\n"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x0118:
            int r0 = r4.next()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            goto L_0x0015
        L_0x011e:
            int r0 = r4.getAttributeCount()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r2 >= r0) goto L_0x0190
            java.lang.String r0 = r4.getAttributeNamespace(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            int r1 = r0.length()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r1 != 0) goto L_0x018b
            r0 = 0
            java.lang.String r0 = r4.getNamespace(r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r1 = r0
        L_0x0134:
            java.lang.String r0 = r4.getAttributePrefix(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r0 != 0) goto L_0x013c
            java.lang.String r0 = ""
        L_0x013c:
            java.lang.String r5 = r4.getAttributeName(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r6 = r4.getAttributeValue(r2)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            com.kenai.jbosh.BodyQName r0 = com.kenai.jbosh.BodyQName.a(r1, r5, r0)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Logger r7 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.util.logging.Level r8 = java.util.logging.Level.FINEST     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            boolean r7 = r7.isLoggable(r8)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            if (r7 == 0) goto L_0x0184
            java.util.logging.Logger r7 = com.kenai.jbosh.v.a     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r8.<init>()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r9 = "        Attribute: {"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r1 = r8.append(r1)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r8 = "}"
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r5 = " = '"
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            java.lang.String r1 = r1.toString()     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            r7.finest(r1)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
        L_0x0184:
            r3.a(r0, r6)     // Catch:{ RuntimeException -> 0x00fe, XmlPullParserException -> 0x0191, IOException -> 0x0194 }
            int r0 = r2 + 1
            r2 = r0
            goto L_0x011e
        L_0x018b:
            r1 = r0
            goto L_0x0134
        L_0x018d:
            r0 = 0
            r2 = r0
            goto L_0x011e
        L_0x0190:
            return r3
        L_0x0191:
            r0 = move-exception
            goto L_0x00ff
        L_0x0194:
            r0 = move-exception
            goto L_0x00ff
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenai.jbosh.v.a(java.lang.String):com.kenai.jbosh.u");
    }
}
