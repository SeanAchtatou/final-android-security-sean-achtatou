package com.kenai.jbosh;

import org.apache.commons.httpclient.cookie.Cookie2;

final class q {
    static final BodyQName A = BodyQName.a("http://www.w3.org/XML/1998/namespace", "lang", "xml");
    static final BodyQName a = BodyQName.a("accept");
    static final BodyQName b = BodyQName.a("authid");
    static final BodyQName c = BodyQName.a("ack");
    static final BodyQName d = BodyQName.a("charsets");
    static final BodyQName e = BodyQName.a("condition");
    static final BodyQName f = BodyQName.a("content");
    static final BodyQName g = BodyQName.a("from");
    static final BodyQName h = BodyQName.a("hold");
    static final BodyQName i = BodyQName.a("inactivity");
    static final BodyQName j = BodyQName.a("key");
    static final BodyQName k = BodyQName.a("maxpause");
    static final BodyQName l = BodyQName.a("newkey");
    static final BodyQName m = BodyQName.a("pause");
    static final BodyQName n = BodyQName.a("polling");
    static final BodyQName o = BodyQName.a("report");
    static final BodyQName p = BodyQName.a("requests");
    static final BodyQName q = BodyQName.a("rid");
    static final BodyQName r = BodyQName.a("route");
    static final BodyQName s = BodyQName.a(Cookie2.SECURE);
    static final BodyQName t = BodyQName.a("sid");
    static final BodyQName u = BodyQName.a("stream");
    static final BodyQName v = BodyQName.a("time");
    static final BodyQName w = BodyQName.a("to");
    static final BodyQName x = BodyQName.a("type");
    static final BodyQName y = BodyQName.a("ver");
    static final BodyQName z = BodyQName.a("wait");
}
