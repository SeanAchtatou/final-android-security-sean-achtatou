package com.kenai.jbosh;

public class BOSHException extends Exception {
    public BOSHException(String str) {
        super(str);
    }

    public BOSHException(String str, Throwable th) {
        super(str, th);
    }
}
