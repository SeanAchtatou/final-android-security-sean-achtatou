package com.kenai.jbosh;

import java.lang.ref.SoftReference;
import org.xmlpull.v1.XmlPullParser;

final class w extends ThreadLocal<SoftReference<XmlPullParser>> {
    w() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public SoftReference<XmlPullParser> initialValue() {
        return new SoftReference<>(null);
    }
}
