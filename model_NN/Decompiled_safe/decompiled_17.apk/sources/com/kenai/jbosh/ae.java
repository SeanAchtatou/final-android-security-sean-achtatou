package com.kenai.jbosh;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.HttpStatus;

final class ae {
    static final ae a = a("bad-request", "The format of an HTTP header or binding element received from the client is unacceptable (e.g., syntax error).", Integer.valueOf((int) HttpStatus.SC_BAD_REQUEST));
    static final ae b = a("host-gone", "The target domain specified in the 'to' attribute or the target host or port specified in the 'route' attribute is no longer serviced by the connection manager.");
    static final ae c = a("host-unknown", "The target domain specified in the 'to' attribute or the target host or port specified in the 'route' attribute is unknown to the connection manager.");
    static final ae d = a("improper-addressing", "The initialization element lacks a 'to' or 'route' attribute (or the attribute has no value) but the connection manager requires one.");
    static final ae e = a("internal-server-error", "The connection manager has experienced an internal error that prevents it from servicing the request.");
    static final ae f = a("item-not-found", "(1) 'sid' is not valid, (2) 'stream' is not valid, (3) 'rid' is larger than the upper limit of the expected window, (4) connection manager is unable to resend response, (5) 'key' sequence is invalid.", Integer.valueOf((int) HttpStatus.SC_NOT_FOUND));
    static final ae g = a("other-request", "Another request being processed at the same time as this request caused the session to terminate.");
    static final ae h = a("policy-violation", "The client has broken the session rules (polling too frequently, requesting too frequently, sending too many simultaneous requests).", Integer.valueOf((int) HttpStatus.SC_FORBIDDEN));
    static final ae i = a("remote-connection-failed", "The connection manager was unable to connect to, or unable to connect securely to, or has lost its connection to, the server.");
    static final ae j = a("remote-stream-error", "Encapsulated transport protocol error.");
    static final ae k = a("see-other-uri", "The connection manager does not operate at this URI (e.g., the connection manager accepts only SSL or TLS connections at some https: URI rather than the http: URI requested by the client).");
    static final ae l = a("system-shutdown", "The connection manager is being shut down. All active HTTP sessions are being terminated. No new sessions can be created.");
    static final ae m = a("undefined-condition", "Unknown or undefined error condition.");
    private static final Map<String, ae> n = new HashMap();
    private static final Map<Integer, ae> o = new HashMap();
    private final String p;
    private final String q;

    private ae(String str, String str2) {
        this.p = str;
        this.q = str2;
    }

    static ae a(int i2) {
        return o.get(Integer.valueOf(i2));
    }

    static ae a(String str) {
        return n.get(str);
    }

    private static ae a(String str, String str2) {
        return a(str, str2, null);
    }

    private static ae a(String str, String str2, Integer num) {
        if (str == null) {
            throw new IllegalArgumentException("condition may not be null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("message may not be null");
        } else if (n.get(str) != null) {
            throw new IllegalStateException("Multiple definitions of condition: " + str);
        } else {
            ae aeVar = new ae(str, str2);
            n.put(str, aeVar);
            if (num != null) {
                if (o.get(num) != null) {
                    throw new IllegalStateException("Multiple definitions of code: " + num);
                }
                o.put(num, aeVar);
            }
            return aeVar;
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.q;
    }
}
