package com.kenai.jbosh;

import java.util.Map;

public abstract class AbstractBody {
    AbstractBody() {
    }

    static BodyQName c() {
        return BodyQName.a("body");
    }

    public final String a(BodyQName bodyQName) {
        return a().get(bodyQName);
    }

    public abstract Map<BodyQName, String> a();

    public abstract String b();
}
