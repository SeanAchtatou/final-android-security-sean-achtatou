package com.kenai.jbosh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;

public final class BOSHClientConnEvent extends EventObject {
    private final boolean a;
    private final List<ComposableBody> b;
    private final Throwable c;

    private BOSHClientConnEvent(BOSHClient bOSHClient, boolean z, List<ComposableBody> list, Throwable th) {
        super(bOSHClient);
        this.a = z;
        this.c = th;
        if (this.a) {
            if (th != null) {
                throw new IllegalStateException("Cannot be connected and have a cause");
            } else if (list != null && list.size() > 0) {
                throw new IllegalStateException("Cannot be connected and have outstanding requests");
            }
        }
        if (list == null) {
            this.b = Collections.emptyList();
        } else {
            this.b = Collections.unmodifiableList(new ArrayList(list));
        }
    }

    static BOSHClientConnEvent a(BOSHClient bOSHClient) {
        return new BOSHClientConnEvent(bOSHClient, true, null, null);
    }

    static BOSHClientConnEvent a(BOSHClient bOSHClient, List<ComposableBody> list, Throwable th) {
        return new BOSHClientConnEvent(bOSHClient, false, list, th);
    }

    static BOSHClientConnEvent b(BOSHClient bOSHClient) {
        return new BOSHClientConnEvent(bOSHClient, false, null, null);
    }

    public boolean a() {
        return this.a;
    }

    public Throwable b() {
        return this.c;
    }
}
