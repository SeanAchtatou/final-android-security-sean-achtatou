package com.kenai.jbosh;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

public final class ComposableBody extends AbstractBody {
    private static final Pattern a = Pattern.compile("<body(?:[\t\n\r ][^>]*?)?(/>|>)", 64);
    private final Map<BodyQName, String> b;
    /* access modifiers changed from: private */
    public final String c;
    private final AtomicReference<String> d;

    public static final class Builder {
        private Map<BodyQName, String> a;
        private boolean b;
        private String c;

        private Builder() {
        }

        /* access modifiers changed from: private */
        public static Builder b(ComposableBody composableBody) {
            Builder builder = new Builder();
            builder.a = composableBody.a();
            builder.b = true;
            builder.c = composableBody.c;
            return builder;
        }

        public Builder a(BodyQName bodyQName, String str) {
            if (this.a == null) {
                this.a = new HashMap();
            } else if (this.b) {
                this.a = new HashMap(this.a);
                this.b = false;
            }
            if (str == null) {
                this.a.remove(bodyQName);
            } else {
                this.a.put(bodyQName, str);
            }
            return this;
        }

        public Builder a(String str) {
            if (str == null) {
                throw new IllegalArgumentException("payload XML argument cannot be null");
            }
            this.c = str;
            return this;
        }

        public Builder a(String str, String str2) {
            return a(BodyQName.a("http://www.w3.org/XML/1998/namespace", str, "xmlns"), str2);
        }

        public ComposableBody a() {
            if (this.a == null) {
                this.a = new HashMap();
            }
            if (this.c == null) {
                this.c = "";
            }
            return new ComposableBody(this.a, this.c);
        }
    }

    private ComposableBody(Map<BodyQName, String> map, String str) {
        this.d = new AtomicReference<>();
        this.b = map;
        this.c = str;
    }

    private String a(String str) {
        return str.replace("'", "&apos;");
    }

    public static Builder d() {
        return new Builder();
    }

    private String g() {
        BodyQName c2 = c();
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(c2.b());
        for (Map.Entry next : this.b.entrySet()) {
            sb.append(" ");
            BodyQName bodyQName = (BodyQName) next.getKey();
            String c3 = bodyQName.c();
            if (c3 != null && c3.length() > 0) {
                sb.append(c3);
                sb.append(":");
            }
            sb.append(bodyQName.b());
            sb.append("='");
            sb.append(a((String) next.getValue()));
            sb.append("'");
        }
        sb.append(" ");
        sb.append("xmlns");
        sb.append("='");
        sb.append(c2.a());
        sb.append("'>");
        if (this.c != null) {
            sb.append(this.c);
        }
        sb.append("</body>");
        return sb.toString();
    }

    public Map<BodyQName, String> a() {
        return Collections.unmodifiableMap(this.b);
    }

    public String b() {
        String str = this.d.get();
        if (str != null) {
            return str;
        }
        String g = g();
        this.d.set(g);
        return g;
    }

    public Builder e() {
        return Builder.b(this);
    }

    public String f() {
        return this.c;
    }
}
