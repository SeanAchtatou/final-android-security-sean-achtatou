package com.kenai.jbosh;

public final class BodyQName {
    private final QName a;

    private BodyQName(QName qName) {
        this.a = qName;
    }

    public static BodyQName a(String str) {
        return a("xm", str, null);
    }

    public static BodyQName a(String str, String str2) {
        return a(str, str2, null);
    }

    public static BodyQName a(String str, String str2, String str3) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("URI is required and may not be null/empty");
        } else if (str2 != null && str2.length() != 0) {
            return (str3 == null || str3.length() == 0) ? new BodyQName(new QName(str, str2)) : new BodyQName(new QName(str, str2, str3));
        } else {
            throw new IllegalArgumentException("Local arg is required and may not be null/empty");
        }
    }

    public String a() {
        return this.a.a();
    }

    /* access modifiers changed from: package-private */
    public boolean a(QName qName) {
        return this.a.equals(qName);
    }

    public String b() {
        return this.a.b();
    }

    public String c() {
        return this.a.c();
    }

    public boolean equals(Object obj) {
        if (obj instanceof BodyQName) {
            return this.a.equals(((BodyQName) obj).a);
        }
        return false;
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
