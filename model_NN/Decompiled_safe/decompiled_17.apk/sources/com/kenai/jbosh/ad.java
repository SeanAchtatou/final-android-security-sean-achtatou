package com.kenai.jbosh;

import java.util.Collections;
import java.util.Map;

final class ad extends AbstractBody {
    private static final t a = new v();
    private final Map<BodyQName, String> b;
    private final String c;

    private ad(Map<BodyQName, String> map, String str) {
        this.b = map;
        this.c = str;
    }

    public static ad a(String str) {
        return new ad(a.a(str).a(), str);
    }

    public Map<BodyQName, String> a() {
        return Collections.unmodifiableMap(this.b);
    }

    public String b() {
        return this.c;
    }
}
