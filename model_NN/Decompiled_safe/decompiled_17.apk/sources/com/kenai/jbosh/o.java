package com.kenai.jbosh;

final class o extends a<String> implements Comparable {
    private static final o a;
    private final int b;
    private final int c;

    static {
        try {
            a = a("1.8");
        } catch (BOSHException e) {
            throw new IllegalStateException(e);
        }
    }

    private o(String str) {
        super(str);
        int indexOf = str.indexOf(46);
        if (indexOf <= 0) {
            throw new BOSHException("Illegal ver attribute value (not in major.minor form): " + str);
        }
        String substring = str.substring(0, indexOf);
        try {
            this.b = Integer.parseInt(substring);
            if (this.b < 0) {
                throw new BOSHException("Major version may not be < 0");
            }
            String substring2 = str.substring(indexOf + 1);
            try {
                this.c = Integer.parseInt(substring2);
                if (this.c < 0) {
                    throw new BOSHException("Minor version may not be < 0");
                }
            } catch (NumberFormatException e) {
                throw new BOSHException("Could not parse ver attribute value (minor ver): " + substring2, e);
            }
        } catch (NumberFormatException e2) {
            throw new BOSHException("Could not parse ver attribute value (major ver): " + substring, e2);
        }
    }

    static o a(String str) {
        if (str == null) {
            return null;
        }
        return new o(str);
    }

    static o b() {
        return a;
    }

    public int compareTo(Object obj) {
        if (!(obj instanceof o)) {
            return 0;
        }
        o oVar = (o) obj;
        if (this.b >= oVar.b) {
            if (this.b > oVar.b) {
                return 1;
            }
            if (this.c >= oVar.c) {
                return this.c <= oVar.c ? 0 : 1;
            }
        }
        return -1;
    }
}
