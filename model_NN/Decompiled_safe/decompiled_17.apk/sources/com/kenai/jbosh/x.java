package com.kenai.jbosh;

final class x {
    private final n a;
    private final p b;
    private final o c;
    private final l d;
    private final i e;
    private final m f;
    private final h g;
    private final e h;
    private final j i;
    private final f j;
    private final g k;
    private final boolean l;

    private x(n nVar, p pVar, o oVar, l lVar, i iVar, m mVar, h hVar, e eVar, j jVar, f fVar, g gVar, boolean z) {
        this.a = nVar;
        this.b = pVar;
        this.c = oVar;
        this.d = lVar;
        this.e = iVar;
        this.f = mVar;
        this.g = hVar;
        this.h = eVar;
        this.i = jVar;
        this.j = fVar;
        this.k = gVar;
        this.l = z;
    }

    static x a(AbstractBody abstractBody, AbstractBody abstractBody2) {
        f a2 = f.a(abstractBody2.a(q.c));
        return new x(n.a(a(abstractBody2, q.t)), p.a(a(abstractBody2, q.z)), o.a(abstractBody2.a(q.y)), l.a(abstractBody2.a(q.n)), i.a(abstractBody2.a(q.i)), m.a(abstractBody2.a(q.p)), h.a(abstractBody2.a(q.h)), e.a(abstractBody2.a(q.a)), j.a(abstractBody2.a(q.k)), a2, g.a(abstractBody2.a(q.d)), a2 != null && ((String) a2.a()).equals(abstractBody.a(q.q)));
    }

    private static String a(AbstractBody abstractBody, BodyQName bodyQName) {
        String a2 = abstractBody.a(bodyQName);
        if (a2 != null) {
            return a2;
        }
        throw new BOSHException("Connection Manager session creation response did not include required '" + bodyQName.b() + "' attribute");
    }

    /* access modifiers changed from: package-private */
    public n a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public p b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public o c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public l d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public j e() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.l;
    }
}
