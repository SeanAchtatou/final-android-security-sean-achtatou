package com.kenai.jbosh;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

final class z {
    private static final Logger a = Logger.getLogger(z.class.getName());
    private final AbstractBody b;
    private final Lock c = new ReentrantLock();
    private final Condition d = this.c.newCondition();
    private aa e;

    z(AbstractBody abstractBody) {
        if (abstractBody == null) {
            throw new IllegalArgumentException("Request body cannot be null");
        }
        this.b = abstractBody;
    }

    /* access modifiers changed from: package-private */
    public AbstractBody a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(aa aaVar) {
        this.c.lock();
        try {
            if (this.e != null) {
                throw new IllegalStateException("HTTPResponse was already set");
            }
            this.e = aaVar;
            this.d.signalAll();
        } finally {
            this.c.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public aa b() {
        this.c.lock();
        while (this.e == null) {
            try {
                this.d.await();
            } catch (InterruptedException e2) {
                a.log(Level.FINEST, "Interrupted", (Throwable) e2);
            } catch (Throwable th) {
                this.c.unlock();
                throw th;
            }
        }
        aa aaVar = this.e;
        this.c.unlock();
        return aaVar;
    }
}
