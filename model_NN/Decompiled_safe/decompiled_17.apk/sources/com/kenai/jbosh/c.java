package com.kenai.jbosh;

import android.content.Context;
import android.net.Uri;
import com.xiaomi.channel.commonutils.b.a;
import com.xiaomi.channel.commonutils.c.b;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

final class c implements aa {
    private final Lock a = new ReentrantLock();
    private final HttpContext b;
    private final HttpClient c;
    private HttpPost d;
    private long e;
    private boolean f;
    private BOSHException g;
    private AbstractBody h;
    private int i;

    c(HttpClient httpClient, BOSHClientConfig bOSHClientConfig, x xVar, AbstractBody abstractBody, Context context) {
        this.c = httpClient;
        this.b = new BasicHttpContext();
        this.f = false;
        try {
            String b2 = abstractBody.b();
            this.e = Long.parseLong(abstractBody.a(q.q));
            String valueOf = String.valueOf((int) (1000.0d * Math.random()));
            String encode = URLEncoder.encode(b.a("xm-http-bind&" + b2));
            if (a.b(context)) {
                String host = bOSHClientConfig.a().getHost();
                Uri.Builder buildUpon = Uri.parse(a.a(bOSHClientConfig.a().toURL())).buildUpon();
                buildUpon.appendQueryParameter("t", valueOf);
                this.d = new HttpPost(buildUpon.build().toString());
                this.d.addHeader("X-Online-Host", host);
            } else {
                Uri.Builder buildUpon2 = Uri.parse(bOSHClientConfig.a().toString()).buildUpon();
                buildUpon2.appendQueryParameter("t", valueOf);
                this.d = new HttpPost(buildUpon2.build().toString());
            }
            this.d.addHeader("X-Content-Sig", encode);
            this.d.addHeader("Connection", "Keep-Alive");
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(y.a(b2.getBytes("UTF-8")));
            byteArrayEntity.setContentType(FilePart.DEFAULT_CONTENT_TYPE);
            this.d.setEntity(byteArrayEntity);
        } catch (Exception e2) {
            this.g = new BOSHException("Could not generate request", e2);
        }
    }

    private void e() {
        synchronized (this) {
            com.xiaomi.channel.commonutils.logger.b.b("SMACK-BOSH: requesting, rid=" + this.e + " url=" + this.d.getURI().toString());
            int i2 = 0;
            BOSHException bOSHException = null;
            while (true) {
                if (i2 >= 3) {
                    break;
                }
                try {
                    HttpResponse execute = this.c.execute(this.d, this.b);
                    byte[] byteArray = EntityUtils.toByteArray(execute.getEntity());
                    int statusCode = execute.getStatusLine().getStatusCode();
                    com.xiaomi.channel.commonutils.logger.b.b("SMACK-BOSH: get server response, code=" + statusCode);
                    if (statusCode == 200 && byteArray != null && execute.containsHeader("X-Content-Sig")) {
                        byte[] b2 = y.b(byteArray);
                        String decode = URLDecoder.decode(execute.getLastHeader("X-Content-Sig").getValue());
                        ad a2 = ad.a(new String(b2, "UTF-8"));
                        String a3 = b.a("xm-http-bind&" + a2.b());
                        if (a3.equals(decode)) {
                            this.h = a2;
                            com.xiaomi.channel.commonutils.logger.b.b("SMACK-BOSH: server response, rid=" + this.e);
                            this.i = statusCode;
                            this.f = true;
                            bOSHException = null;
                            break;
                        }
                        com.xiaomi.channel.commonutils.logger.b.c("SMACK-BOSH: the server signature doesn't match, drop the response. received " + decode + ", expected " + a3);
                        this.g = new BOSHException("signature mismatch");
                        a();
                        break;
                    }
                    a();
                } catch (Exception e2) {
                    if (e2 instanceof SocketException) {
                        BOSHException bOSHException2 = new BOSHException("Could not obtain response", e2);
                        com.xiaomi.channel.commonutils.logger.b.a("SMACK-BOSH: request error, retry=" + i2, e2);
                        i2++;
                        bOSHException = bOSHException2;
                    } else {
                        a();
                        this.g = new BOSHException("Could not obtain response", e2);
                        throw this.g;
                    }
                }
            }
            if (i2 == 3) {
                a();
                this.g = bOSHException;
                throw this.g;
            }
        }
    }

    public void a() {
        if (this.d != null) {
            this.d.abort();
            this.g = new BOSHException("HTTP request aborted");
        }
    }

    /* JADX INFO: finally extract failed */
    public AbstractBody b() {
        if (this.g != null) {
            throw this.g;
        }
        this.a.lock();
        try {
            if (!this.f) {
                e();
            }
            this.a.unlock();
            return this.h;
        } catch (Throwable th) {
            this.a.unlock();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public int c() {
        if (this.g != null) {
            throw this.g;
        }
        this.a.lock();
        try {
            if (!this.f) {
                e();
            }
            this.a.unlock();
            return this.i;
        } catch (Throwable th) {
            this.a.unlock();
            throw th;
        }
    }

    public long d() {
        return this.e;
    }
}
