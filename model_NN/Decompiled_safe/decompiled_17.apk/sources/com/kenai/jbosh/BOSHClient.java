package com.kenai.jbosh;

import android.content.Context;
import android.text.TextUtils;
import com.kenai.jbosh.ComposableBody;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.xiaomi.channel.commonutils.logger.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.httpclient.HttpStatus;

public final class BOSHClient {
    static final /* synthetic */ boolean a = (!BOSHClient.class.desiredAssertionStatus());
    private static final Logger b = Logger.getLogger(BOSHClient.class.getName());
    private static final int c = Integer.getInteger(BOSHClient.class.getName() + ".emptyRequestDelay", 100).intValue();
    private static final int d = Integer.getInteger(BOSHClient.class.getName() + ".pauseMargin", (int) HttpStatus.SC_INTERNAL_SERVER_ERROR).intValue();
    private static final boolean e;
    private List<ComposableBody> A = new ArrayList();
    private volatile long B = 0;
    /* access modifiers changed from: private */
    public volatile long C = 0;
    private Context D;
    private final Set<BOSHClientConnListener> f = new CopyOnWriteArraySet();
    private final Set<BOSHClientRequestListener> g = new CopyOnWriteArraySet();
    private final Set<BOSHClientResponseListener> h = new CopyOnWriteArraySet();
    /* access modifiers changed from: private */
    public final ReentrantLock i = new ReentrantLock();
    private final Condition j = this.i.newCondition();
    private final Condition k = this.i.newCondition();
    private final Condition l = this.i.newCondition();
    private final Condition m = this.i.newCondition();
    private long n = 0;
    private final BOSHClientConfig o;
    private final Runnable p = new r(this);
    private final ab q = new d();
    private final AtomicReference<a> r = new AtomicReference<>();
    private final ac s = new ac();
    private final ScheduledExecutorService t = Executors.newSingleThreadScheduledExecutor();
    private ThreadPoolExecutor u;
    private ScheduledFuture<?> v;
    private x w;
    /* access modifiers changed from: private */
    public Queue<z> x = new LinkedList();
    private SortedSet<Long> y = new TreeSet();
    private Long z = -1L;

    static abstract class a {
        a() {
        }

        /* access modifiers changed from: package-private */
        public abstract z a(z zVar);
    }

    static {
        boolean z2 = false;
        String str = BOSHClient.class.getSimpleName() + ".assertionsEnabled";
        if (System.getProperty(str) != null) {
            z2 = Boolean.getBoolean(str);
        } else if (!a) {
            z2 = true;
        }
        e = z2;
    }

    private BOSHClient(BOSHClientConfig bOSHClientConfig, Context context) {
        this.o = bOSHClientConfig;
        this.D = context.getApplicationContext();
        c();
    }

    public static BOSHClient a(BOSHClientConfig bOSHClientConfig, Context context) {
        if (bOSHClientConfig != null) {
            return new BOSHClient(bOSHClientConfig, context);
        }
        throw new IllegalArgumentException("Client configuration may not be null");
    }

    private ComposableBody a(long j2, ComposableBody composableBody) {
        k();
        ComposableBody.Builder e2 = composableBody.e();
        e2.a(q.w, this.o.b());
        e2.a(q.A, this.o.d());
        e2.a(q.y, o.b().toString());
        e2.a(q.z, "300");
        e2.a(q.h, "1");
        e2.a(q.q, Long.toString(j2));
        a(e2);
        b(e2);
        e2.a(q.c, "1");
        e2.a(q.t, (String) null);
        return e2.a();
    }

    private ae a(int i2, AbstractBody abstractBody) {
        k();
        if (a(abstractBody)) {
            return ae.a(abstractBody.a(q.e));
        }
        if (this.w == null || this.w.c() != null) {
            return null;
        }
        return ae.a(i2);
    }

    private void a(long j2) {
        k();
        if (j2 < 0) {
            throw new IllegalArgumentException("Empty request delay must be >= 0 (was: " + j2 + ")");
        }
        h();
        if (d()) {
            b.b("SMACK-BOSH: Scheduling empty request in " + j2 + LocaleUtil.MALAY);
            try {
                this.v = this.t.schedule(this.p, j2, TimeUnit.MILLISECONDS);
            } catch (RejectedExecutionException e2) {
                b.a("SMACK-BOSH: Could not schedule empty request", e2);
            }
            this.l.signalAll();
        }
    }

    private void a(AbstractBody abstractBody, int i2) {
        ae a2 = a(i2, abstractBody);
        if (a2 != null) {
            throw new BOSHException("Terminal binding condition encountered: " + a2.a() + "  (" + a2.b() + ")");
        }
    }

    private void a(AbstractBody abstractBody, AbstractBody abstractBody2) {
        k();
        if (this.w.f() && abstractBody2.a(q.o) == null) {
            String a2 = abstractBody2.a(q.c);
            Long valueOf = a2 == null ? Long.valueOf(Long.parseLong(abstractBody.a(q.q))) : Long.valueOf(Long.parseLong(a2));
            if (b.isLoggable(Level.FINEST)) {
                b.finest("Removing pending acks up to: " + valueOf);
            }
            Iterator<ComposableBody> it = this.A.iterator();
            while (it.hasNext()) {
                if (Long.valueOf(Long.parseLong(it.next().a(q.q))).compareTo(valueOf) <= 0) {
                    it.remove();
                }
            }
        }
    }

    private void a(ComposableBody.Builder builder) {
        k();
        String e2 = this.o.e();
        if (e2 != null) {
            builder.a(q.r, e2);
        }
    }

    private void a(z zVar) {
        this.x.add(zVar);
        this.u.execute(new s(this));
    }

    /* JADX INFO: finally extract failed */
    private void a(Throwable th) {
        l();
        this.i.lock();
        try {
            if (this.u != null) {
                this.u.shutdownNow();
                this.u = null;
                this.i.unlock();
                if (th == null) {
                    n();
                } else {
                    b(th);
                }
                this.i.lock();
                try {
                    h();
                    this.x = null;
                    this.w = null;
                    this.y = null;
                    this.A = null;
                    this.j.signalAll();
                    this.k.signalAll();
                    this.l.signalAll();
                    this.m.signalAll();
                    this.i.unlock();
                    this.q.a();
                } catch (Throwable th2) {
                    this.i.unlock();
                    throw th2;
                }
            }
        } finally {
            this.i.unlock();
        }
    }

    private static boolean a(AbstractBody abstractBody) {
        return "terminate".equals(abstractBody.a(q.x));
    }

    private ComposableBody b(long j2, ComposableBody composableBody) {
        k();
        ComposableBody.Builder e2 = composableBody.e();
        e2.a(q.t, this.w.a().toString());
        e2.a(q.q, Long.toString(j2));
        return e2.a();
    }

    private void b(ComposableBody.Builder builder) {
        k();
        String c2 = this.o.c();
        if (c2 != null) {
            builder.a(q.g, c2);
        }
    }

    /* JADX INFO: finally extract failed */
    private void b(z zVar) {
        ArrayList<z> arrayList = null;
        l();
        try {
            aa b2 = zVar.b();
            AbstractBody b3 = b2.b();
            int c2 = b2.c();
            this.i.lock();
            try {
                long d2 = b2.d();
                if (this.B == d2) {
                    this.B = 0;
                }
                if (d2 <= this.n) {
                    this.m.signalAll();
                } else {
                    b.b("SMACK-BOSH: responded rid(" + d2 + ") is not expected (" + this.n + "), wait");
                    if (!this.m.await(30, TimeUnit.SECONDS)) {
                        b.c("SMACK-BOSH: wait for " + this.n + " timeout, terminate!");
                        a(new BOSHException("wait timeout for rid" + this.n));
                        this.i.unlock();
                        return;
                    }
                }
                this.n = 1 + this.n;
                this.i.unlock();
                g(b3);
                AbstractBody a2 = zVar.a();
                this.i.lock();
                try {
                    if (d()) {
                        if (this.w == null) {
                            this.w = x.a(a2, b3);
                            m();
                        }
                        x xVar = this.w;
                        a(b3, c2);
                        if (a(b3)) {
                            this.i.unlock();
                            a((Throwable) null);
                            if (this.i.isHeldByCurrentThread()) {
                                try {
                                    if (this.x != null && this.x.isEmpty() && !g()) {
                                        long c3 = c(a2);
                                        if (c3 > 0) {
                                            a(c3);
                                        }
                                    }
                                    this.k.signalAll();
                                } finally {
                                    this.i.unlock();
                                }
                            } else {
                                b.b("SMACK-BOSH: lock is not held by this thread, don't schedule empty request");
                            }
                        } else {
                            if (b(b3)) {
                                ArrayList<z> arrayList2 = new ArrayList<>(this.x.size());
                                for (z a3 : this.x) {
                                    arrayList2.add(new z(a3.a()));
                                }
                                for (z a4 : arrayList2) {
                                    a(a4);
                                }
                                arrayList = arrayList2;
                            } else {
                                a(a2, b3);
                                d(a2);
                                z e2 = e(b3);
                                if (e2 != null) {
                                    arrayList = new ArrayList<>(1);
                                    arrayList.add(e2);
                                    a(e2);
                                }
                            }
                            if (this.i.isHeldByCurrentThread()) {
                                try {
                                    if (this.x != null && this.x.isEmpty() && !g()) {
                                        long c4 = c(a2);
                                        if (c4 > 0) {
                                            a(c4);
                                        }
                                    }
                                    this.k.signalAll();
                                } finally {
                                    this.i.unlock();
                                }
                            } else {
                                b.b("SMACK-BOSH: lock is not held by this thread, don't schedule empty request");
                            }
                            if (arrayList != null) {
                                for (z zVar2 : arrayList) {
                                    zVar2.a(this.q.a(xVar, zVar2.a(), this.D));
                                    f(zVar2.a());
                                }
                            }
                        }
                    } else if (this.i.isHeldByCurrentThread()) {
                        try {
                            if (this.x != null && this.x.isEmpty() && !g()) {
                                long c5 = c(a2);
                                if (c5 > 0) {
                                    a(c5);
                                }
                            }
                            this.k.signalAll();
                        } finally {
                            this.i.unlock();
                        }
                    } else {
                        b.b("SMACK-BOSH: lock is not held by this thread, don't schedule empty request");
                    }
                } catch (BOSHException e3) {
                    b.a("SMACK-BOSH: Could not process response", e3);
                    this.i.unlock();
                    a(e3);
                    if (this.i.isHeldByCurrentThread()) {
                        if (this.x != null && this.x.isEmpty() && !g()) {
                            long c6 = c(a2);
                            if (c6 > 0) {
                                a(c6);
                            }
                        }
                        this.k.signalAll();
                        this.i.unlock();
                        return;
                    }
                    b.b("SMACK-BOSH: lock is not held by this thread, don't schedule empty request");
                } catch (InterruptedException e4) {
                    b.a("SMACK-BOSH: Could not process response", e4);
                    this.i.unlock();
                    a(e4);
                    if (this.i.isHeldByCurrentThread()) {
                        if (this.x != null && this.x.isEmpty() && !g()) {
                            long c7 = c(a2);
                            if (c7 > 0) {
                                a(c7);
                            }
                        }
                        this.k.signalAll();
                        this.i.unlock();
                        return;
                    }
                    b.b("SMACK-BOSH: lock is not held by this thread, don't schedule empty request");
                } catch (Throwable th) {
                    this.i.unlock();
                    throw th;
                }
            } catch (InterruptedException e5) {
                a(e5);
            } finally {
                this.i.unlock();
            }
        } catch (BOSHException e6) {
            b.a("SMACK-BOSH: Could not obtain response", e6);
            a(e6);
        } catch (InterruptedException e7) {
            b.a("Interrupted", e7);
            a(e7);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void b(Throwable th) {
        l();
        BOSHClientConnEvent bOSHClientConnEvent = null;
        for (BOSHClientConnListener next : this.f) {
            if (bOSHClientConnEvent == null) {
                bOSHClientConnEvent = BOSHClientConnEvent.a(this, this.A, th);
            }
            try {
                next.a(bOSHClientConnEvent);
            } catch (Exception e2) {
                b.log(Level.WARNING, "Unhandled Exception", (Throwable) e2);
            }
        }
    }

    private static boolean b(AbstractBody abstractBody) {
        return "error".equals(abstractBody.a(q.x));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, com.kenai.jbosh.BOSHException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private long c(AbstractBody abstractBody) {
        k();
        if (!(this.w == null || this.w.e() == null)) {
            try {
                k a2 = k.a(abstractBody.a(q.m));
                if (a2 != null) {
                    long c2 = (long) (a2.c() - d);
                    return c2 < 0 ? (long) c : c2;
                }
            } catch (BOSHException e2) {
                b.log(Level.FINEST, "Could not extract", (Throwable) e2);
            }
        }
        return i();
    }

    private void c() {
        l();
        this.i.lock();
        try {
            this.q.a(this.o);
            this.u = new ThreadPoolExecutor(2, 2, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());
        } finally {
            this.i.unlock();
        }
    }

    private void d(AbstractBody abstractBody) {
        k();
        Long valueOf = Long.valueOf(Long.parseLong(abstractBody.a(q.q)));
        if (this.z.equals(-1L)) {
            this.z = valueOf;
            return;
        }
        this.y.add(valueOf);
        Long valueOf2 = Long.valueOf(this.z.longValue() + 1);
        while (!this.y.isEmpty() && valueOf2.equals(this.y.first())) {
            this.z = valueOf2;
            this.y.remove(valueOf2);
            valueOf2 = Long.valueOf(valueOf2.longValue() + 1);
        }
    }

    private boolean d() {
        k();
        return this.u != null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.kenai.jbosh.z e(com.kenai.jbosh.AbstractBody r8) {
        /*
            r7 = this;
            r0 = 0
            r7.k()
            com.kenai.jbosh.BodyQName r1 = com.kenai.jbosh.q.o
            java.lang.String r2 = r8.a(r1)
            if (r2 != 0) goto L_0x000d
        L_0x000c:
            return r0
        L_0x000d:
            long r3 = java.lang.Long.parseLong(r2)
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            com.kenai.jbosh.BodyQName r1 = com.kenai.jbosh.q.v
            java.lang.String r1 = r8.a(r1)
            long r4 = java.lang.Long.parseLong(r1)
            java.lang.Long r1 = java.lang.Long.valueOf(r4)
            java.util.logging.Logger r4 = com.kenai.jbosh.BOSHClient.b
            java.util.logging.Level r5 = java.util.logging.Level.FINE
            boolean r4 = r4.isLoggable(r5)
            if (r4 == 0) goto L_0x0055
            java.util.logging.Logger r4 = com.kenai.jbosh.BOSHClient.b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Received report of missing request (RID="
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r3)
            java.lang.String r6 = ", time="
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = "ms)"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            r4.fine(r1)
        L_0x0055:
            java.util.List<com.kenai.jbosh.ComposableBody> r1 = r7.A
            java.util.Iterator r4 = r1.iterator()
            r1 = r0
        L_0x005c:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0080
            if (r1 != 0) goto L_0x0080
            java.lang.Object r0 = r4.next()
            com.kenai.jbosh.AbstractBody r0 = (com.kenai.jbosh.AbstractBody) r0
            com.kenai.jbosh.BodyQName r5 = com.kenai.jbosh.q.q
            java.lang.String r5 = r0.a(r5)
            long r5 = java.lang.Long.parseLong(r5)
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            boolean r5 = r3.equals(r5)
            if (r5 == 0) goto L_0x00b0
        L_0x007e:
            r1 = r0
            goto L_0x005c
        L_0x0080:
            if (r1 != 0) goto L_0x00a1
            com.kenai.jbosh.BOSHException r0 = new com.kenai.jbosh.BOSHException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Report of missing message with RID '"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "' but local copy of that request was not found"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00a1:
            com.kenai.jbosh.z r0 = new com.kenai.jbosh.z
            r0.<init>(r1)
            r7.a(r0)
            java.util.concurrent.locks.Condition r1 = r7.j
            r1.signalAll()
            goto L_0x000c
        L_0x00b0:
            r0 = r1
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenai.jbosh.BOSHClient.e(com.kenai.jbosh.AbstractBody):com.kenai.jbosh.z");
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public void e() {
        z zVar;
        z f2 = f();
        if (f2 != null) {
            this.i.lock();
            try {
                long longValue = Long.valueOf(f2.a().a(q.q)).longValue();
                if (this.n == 0) {
                    this.n = longValue;
                }
                this.i.unlock();
                a aVar = this.r.get();
                if (aVar != null) {
                    zVar = aVar.a(f2);
                    if (zVar == null) {
                        b.log(Level.FINE, "Discarding exchange on request of test hook: RID=" + f2.a().a(q.q));
                        return;
                    }
                } else {
                    zVar = f2;
                }
                b(zVar);
            } catch (Throwable th) {
                this.i.unlock();
                throw th;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private z f() {
        z poll;
        l();
        this.i.lock();
        do {
            try {
                if (this.x == null) {
                    this.i.unlock();
                    return null;
                }
                poll = this.x.poll();
                if (poll == null) {
                    this.j.await();
                    continue;
                }
            } catch (InterruptedException e2) {
                b.log(Level.FINEST, "Interrupted", (Throwable) e2);
                continue;
            } catch (Throwable th) {
                this.i.unlock();
                throw th;
            }
        } while (poll == null);
        this.i.unlock();
        return poll;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void f(AbstractBody abstractBody) {
        l();
        BOSHMessageEvent bOSHMessageEvent = null;
        for (BOSHClientRequestListener next : this.g) {
            if (bOSHMessageEvent == null) {
                bOSHMessageEvent = BOSHMessageEvent.a(this, abstractBody);
            }
            try {
                next.a(bOSHMessageEvent);
            } catch (Exception e2) {
                b.log(Level.WARNING, "Unhandled Exception", (Throwable) e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void g(AbstractBody abstractBody) {
        l();
        BOSHMessageEvent bOSHMessageEvent = null;
        for (BOSHClientResponseListener next : this.h) {
            if (bOSHMessageEvent == null) {
                bOSHMessageEvent = BOSHMessageEvent.b(this, abstractBody);
            }
            try {
                next.a(bOSHMessageEvent);
            } catch (Exception e2) {
                b.log(Level.WARNING, "Unhandled Exception", (Throwable) e2);
            }
        }
    }

    private boolean g() {
        return (this.v != null && !this.v.isDone()) || this.B > 0;
    }

    private void h() {
        k();
        if (this.v != null) {
            this.v.cancel(false);
            this.v = null;
        }
    }

    private long i() {
        k();
        l d2 = this.w.d();
        long j2 = (long) c;
        if (d2 != null) {
            long c2 = (long) d2.c();
            if (c2 > j2) {
                return c2;
            }
        }
        return j2;
    }

    /* access modifiers changed from: private */
    public void j() {
        l();
        try {
            a(ComposableBody.d().a());
        } catch (BOSHException e2) {
            a(e2);
        }
    }

    private void k() {
        if (e && !this.i.isHeldByCurrentThread()) {
            throw new AssertionError("Lock is not held by current thread");
        }
    }

    private void l() {
        if (e && this.i.isHeldByCurrentThread()) {
            throw new AssertionError("Lock is held by current thread");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void m() {
        boolean isHeldByCurrentThread = this.i.isHeldByCurrentThread();
        if (isHeldByCurrentThread) {
            this.i.unlock();
        }
        try {
            BOSHClientConnEvent bOSHClientConnEvent = null;
            for (BOSHClientConnListener next : this.f) {
                if (bOSHClientConnEvent == null) {
                    bOSHClientConnEvent = BOSHClientConnEvent.a(this);
                }
                next.a(bOSHClientConnEvent);
            }
            if (isHeldByCurrentThread) {
                this.i.lock();
            }
        } catch (Exception e2) {
            b.log(Level.WARNING, "Unhandled Exception", (Throwable) e2);
        } catch (Throwable th) {
            if (isHeldByCurrentThread) {
                this.i.lock();
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void n() {
        l();
        BOSHClientConnEvent bOSHClientConnEvent = null;
        for (BOSHClientConnListener next : this.f) {
            if (bOSHClientConnEvent == null) {
                bOSHClientConnEvent = BOSHClientConnEvent.b(this);
            }
            try {
                next.a(bOSHClientConnEvent);
            } catch (Exception e2) {
                b.log(Level.WARNING, "Unhandled Exception", (Throwable) e2);
            }
        }
    }

    public void a() {
        a(new BOSHException("Session explicitly closed by caller"));
    }

    public void a(BOSHClientConnListener bOSHClientConnListener) {
        if (bOSHClientConnListener == null) {
            throw new IllegalArgumentException("Listener may not be null");
        }
        this.f.add(bOSHClientConnListener);
    }

    public void a(BOSHClientRequestListener bOSHClientRequestListener) {
        if (bOSHClientRequestListener == null) {
            throw new IllegalArgumentException("Listener may not be null");
        }
        this.g.add(bOSHClientRequestListener);
    }

    public void a(BOSHClientResponseListener bOSHClientResponseListener) {
        if (bOSHClientResponseListener == null) {
            throw new IllegalArgumentException("Listener may not be null");
        }
        this.h.add(bOSHClientResponseListener);
    }

    /* JADX INFO: finally extract failed */
    public void a(ComposableBody composableBody) {
        ComposableBody b2;
        l();
        if (composableBody == null) {
            throw new IllegalArgumentException("Message body may not be null");
        }
        this.i.lock();
        if (d() || a((AbstractBody) composableBody)) {
            try {
                long a2 = this.s.a();
                if (TextUtils.isEmpty(composableBody.f())) {
                    this.B = a2;
                }
                x xVar = this.w;
                if (xVar != null || !this.x.isEmpty()) {
                    b2 = b(a2, composableBody);
                    if (this.w.f()) {
                        this.A.add(b2);
                    }
                } else {
                    b2 = a(a2, composableBody);
                }
                z zVar = new z(b2);
                a(zVar);
                this.j.signalAll();
                h();
                this.i.unlock();
                AbstractBody a3 = zVar.a();
                zVar.a(this.q.a(xVar, a3, this.D));
                f(a3);
            } catch (Throwable th) {
                this.i.unlock();
                throw th;
            }
        } else {
            throw new BOSHException("Cannot send message when session is closed");
        }
    }

    public void b() {
        if (System.currentTimeMillis() - this.C > 30000 && this.u.getActiveCount() > 1) {
            a(new BOSHException("SMACK-BOSH: request timeout happened, reset connection"));
        } else if (this.u.getActiveCount() <= 0 || g()) {
            this.i.lock();
            try {
                a(0);
            } finally {
                this.i.unlock();
            }
        }
    }

    public void b(ComposableBody composableBody) {
        if (composableBody == null) {
            throw new IllegalArgumentException("Message body may not be null");
        }
        ComposableBody.Builder e2 = composableBody.e();
        e2.a(q.x, "terminate");
        a(e2.a());
    }
}
