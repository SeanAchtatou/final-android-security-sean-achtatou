package com.kenai.jbosh;

import java.util.concurrent.TimeUnit;

final class k extends b {
    private k(String str) {
        super(str);
        a(1);
    }

    static k a(String str) {
        if (str == null) {
            return null;
        }
        return new k(str);
    }

    public int c() {
        return (int) TimeUnit.MILLISECONDS.convert((long) b(), TimeUnit.SECONDS);
    }
}
