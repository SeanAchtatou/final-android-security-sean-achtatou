package com.kenai.jbosh;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

final class y {
    public static byte[] a(byte[] bArr) {
        GZIPOutputStream gZIPOutputStream;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            try {
                gZIPOutputStream.write(bArr);
                gZIPOutputStream.close();
                byteArrayOutputStream.close();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                try {
                    gZIPOutputStream.close();
                    byteArrayOutputStream.close();
                } catch (IOException e) {
                }
                return byteArray;
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            gZIPOutputStream = null;
            try {
                gZIPOutputStream.close();
                byteArrayOutputStream.close();
            } catch (IOException e2) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(byte[] r5) {
        /*
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            r0.<init>(r5)
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x002c }
            r1.<init>(r0)     // Catch:{ all -> 0x002c }
            r0 = 512(0x200, float:7.175E-43)
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0037 }
        L_0x0013:
            int r3 = r1.read(r0)     // Catch:{ all -> 0x0037 }
            if (r3 <= 0) goto L_0x001d
            r4 = 0
            r2.write(r0, r4, r3)     // Catch:{ all -> 0x0037 }
        L_0x001d:
            if (r3 >= 0) goto L_0x0013
            byte[] r0 = r2.toByteArray()     // Catch:{ all -> 0x0037 }
            if (r1 == 0) goto L_0x0028
            r1.close()
        L_0x0028:
            r2.close()
            return r0
        L_0x002c:
            r0 = move-exception
            r1 = 0
        L_0x002e:
            if (r1 == 0) goto L_0x0033
            r1.close()
        L_0x0033:
            r2.close()
            throw r0
        L_0x0037:
            r0 = move-exception
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenai.jbosh.y.b(byte[]):byte[]");
    }
}
