package com.upyun.api;

import com.tencent.mm.sdk.message.RMsgInfoDB;
import com.tencent.tauth.Constants;
import com.upyun.api.utils.SimpleMultipartEntity;
import com.upyun.api.utils.UpYunException;
import java.io.File;
import java.io.IOException;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class Uploader {
    public static String upload(String policy, String signature, String bucket, String sourceFile) throws UpYunException {
        if (bucket == null || bucket.equals("")) {
            throw new UpYunException(10, "bucket can not be empty.");
        } else if (sourceFile == null || sourceFile.equals("")) {
            throw new UpYunException(11, "source file can not be empty.");
        } else if (policy == null || policy.equals("")) {
            throw new UpYunException(12, "policy can not be empty.");
        } else if (signature == null || signature.equals("")) {
            throw new UpYunException(13, "signature can not be empty.");
        } else {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://v0.api.upyun.com/" + bucket + CookieSpec.PATH_DELIM);
            try {
                SimpleMultipartEntity sme = new SimpleMultipartEntity();
                sme.addPart("policy", policy);
                sme.addPart("signature", signature);
                sme.addPart("file", new File(sourceFile));
                httppost.setEntity(sme);
                HttpResponse response = httpclient.execute(httppost);
                int code = response.getStatusLine().getStatusCode();
                String str = EntityUtils.toString(response.getEntity());
                if (code == 200) {
                    return new JSONObject(str).getString(Constants.PARAM_URL);
                }
                JSONObject obj = new JSONObject(str);
                String msg = new String(obj.getString(RMsgInfoDB.TABLE).getBytes("UTF-8"), "UTF-8");
                String url = obj.getString(Constants.PARAM_URL);
                long time = obj.getLong("time");
                boolean isSigned = false;
                String signString = "";
                if (!obj.isNull("sign")) {
                    signString = obj.getString("sign");
                    isSigned = true;
                } else if (!obj.isNull("non-sign")) {
                    signString = obj.getString("non-sign");
                    isSigned = false;
                }
                UpYunException exception = new UpYunException(code, msg);
                exception.isSigned = isSigned;
                exception.url = url;
                exception.time = time;
                exception.signString = signString;
                throw exception;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                throw new UpYunException(30, e.getMessage());
            } catch (IOException e2) {
                e2.printStackTrace();
                throw new UpYunException(31, e2.getMessage());
            } catch (OutOfMemoryError e3) {
                e3.printStackTrace();
                throw new UpYunException(33, e3.getMessage());
            } catch (JSONException e4) {
                throw new UpYunException(32, e4.getMessage());
            }
        }
    }
}
