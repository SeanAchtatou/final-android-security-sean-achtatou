package com.upyun.api.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class UpYunUtils {
    public static String makePolicy(String saveKey, long expiration, String bucket) throws UpYunException {
        return makePolicy(saveKey, expiration, bucket, null);
    }

    public static String makePolicy(String saveKey, long expiration, String bucket, HashMap<String, Object> params) throws UpYunException {
        if (saveKey == null || saveKey.equals("")) {
            throw new UpYunException(20, "miss param saveKey");
        } else if (expiration == 0) {
            throw new UpYunException(20, "miss param expiration");
        } else if (bucket == null || bucket.equals("")) {
            throw new UpYunException(20, "miss param bucket");
        } else {
            JSONObject obj = new JSONObject();
            try {
                obj.put("save-key", saveKey);
                obj.put("expiration", expiration);
                obj.put("bucket", bucket);
                if (params != null) {
                    for (String key : params.keySet()) {
                        obj.put(key, params.get(key));
                    }
                }
                return Base64Coder.encodeString(obj.toString());
            } catch (JSONException e) {
                throw new UpYunException(21, e.getMessage());
            }
        }
    }

    public static String signature(String source) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(source.getBytes());
            byte[] mdbytes = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : mdbytes) {
                String hex = Integer.toHexString(b & 255);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
