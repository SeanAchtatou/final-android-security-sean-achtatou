package com.upyun.api.utils;

public class UpYunException extends Exception {
    private static final long serialVersionUID = 3854772125385537971L;
    public int code;
    public boolean isSigned;
    public String message;
    public String signString;
    public long time;
    public String url;

    public UpYunException(int code2, String message2) {
        this.code = code2;
        this.message = message2;
    }

    public String toString() {
        return "UpYunException [code=" + this.code + ", " + (this.message != null ? "message=" + this.message + ", " : "") + (this.url != null ? "url=" + this.url + ", " : "") + "time=" + this.time + ", " + (this.signString != null ? "signString=" + this.signString + ", " : "") + "isSigned=" + this.isSigned + "]";
    }
}
