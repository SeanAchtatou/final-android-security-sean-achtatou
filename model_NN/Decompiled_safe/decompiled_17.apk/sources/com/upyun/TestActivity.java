package com.upyun;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;
import com.tencent.mm.sdk.platformtools.Util;
import com.upyun.api.Uploader;
import com.upyun.api.utils.UpYunException;
import com.upyun.api.utils.UpYunUtils;
import java.io.File;

public class TestActivity extends Activity {
    private static final String BUCKET = "testiamge";
    /* access modifiers changed from: private */
    public static final long EXPIRATION = ((System.currentTimeMillis() / 1000) + 50000);
    /* access modifiers changed from: private */
    public static final String SOURCE_FILE = (String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator + "IMAG1104.jpg");
    private static final String TEST_API_KEY = "fSqzkFcEP+PMTirIOpsAuzLiMZ4=";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        new UploadTask().execute(new Void[0]);
    }

    public class UploadTask extends AsyncTask<Void, Void, String> {
        public UploadTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            try {
                String policy = UpYunUtils.makePolicy(String.valueOf(File.separator) + "test" + File.separator + System.currentTimeMillis() + Util.PHOTO_DEFAULT_EXT, TestActivity.EXPIRATION, TestActivity.BUCKET);
                return Uploader.upload(policy, UpYunUtils.signature(String.valueOf(policy) + "&" + TestActivity.TEST_API_KEY), TestActivity.BUCKET, TestActivity.SOURCE_FILE);
            } catch (UpYunException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
            if (result != null) {
                Toast.makeText(TestActivity.this.getApplicationContext(), "成功", 1).show();
            } else {
                Toast.makeText(TestActivity.this.getApplicationContext(), "失败", 1).show();
            }
        }
    }
}
