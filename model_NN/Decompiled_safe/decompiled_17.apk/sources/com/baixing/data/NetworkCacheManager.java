package com.baixing.data;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;
import com.baixing.network.ICacheProxy;
import com.baixing.util.Util;
import java.util.ArrayList;
import java.util.List;

public class NetworkCacheManager implements ICacheProxy {
    private BXDatabaseHelper dbManager = null;
    protected final List<Pair<String, String>> storeList = new ArrayList();

    static NetworkCacheManager createInstance(Context context) {
        return new NetworkCacheManager(context);
    }

    private NetworkCacheManager(Context context) {
        this.dbManager = new BXDatabaseHelper(context, "network.db", null, 2);
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    Pair<String, String> item = null;
                    synchronized (NetworkCacheManager.this.storeList) {
                        if (NetworkCacheManager.this.storeList.size() > 0) {
                            item = NetworkCacheManager.this.storeList.remove(0);
                        } else {
                            try {
                                NetworkCacheManager.this.storeList.wait(300000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (item != null) {
                        NetworkCacheManager.this.storeCacheNetworkRequest((String) item.first, (String) item.second);
                    }
                }
            }
        }).start();
    }

    public void ClearCache() {
        SQLiteDatabase db = this.dbManager.getWritableDatabase();
        try {
            db.execSQL("DELETE from " + BXDatabaseHelper.TABLENAME, new String[0]);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        db.close();
    }

    public void putCacheNetworkRequest(String request, String result) {
        String request2 = Util.extractUrlWithoutSecret(request);
        synchronized (this.storeList) {
            this.storeList.add(Pair.create(request2, result));
            this.storeList.notifyAll();
        }
    }

    /* access modifiers changed from: private */
    public void storeCacheNetworkRequest(String request, String result) {
        synchronized (this.dbManager) {
            SQLiteDatabase db = null;
            try {
                db = this.dbManager.getWritableDatabase();
                String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
                db.execSQL("insert into " + BXDatabaseHelper.TABLENAME + "(url, response, timestamp) values(?,?,?)", new String[]{request, result, timestamp});
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (db != null) {
                db.close();
            }
        }
    }

    public String getCacheNetworkRequest(String request) {
        String response;
        String request2 = Util.extractUrlWithoutSecret(request);
        synchronized (this.dbManager) {
            response = null;
            SQLiteDatabase db = null;
            try {
                db = this.dbManager.getReadableDatabase();
                Cursor c = db.rawQuery("SELECT * from " + BXDatabaseHelper.TABLENAME + " WHERE url=?", new String[]{request2});
                while (true) {
                    if (c.moveToNext()) {
                        int index = c.getColumnIndex("response");
                        if (index >= 0) {
                            response = c.getString(index);
                            break;
                        }
                    } else {
                        break;
                    }
                }
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Throwable e2) {
                e2.printStackTrace();
            }
            if (db != null) {
                db.close();
            }
        }
        return response;
    }

    public void deleteOldRecorders(int intervalInSec) {
        SQLiteDatabase db = null;
        try {
            db = this.dbManager.getWritableDatabase();
            db.execSQL("DELETE from " + BXDatabaseHelper.TABLENAME + " WHERE timestamp<?", new String[]{String.valueOf((System.currentTimeMillis() / 1000) - ((long) intervalInSec))});
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (db != null) {
            db.close();
        }
    }

    public void onSave(String url, String jsonStr) {
        putCacheNetworkRequest(url, jsonStr);
    }

    public String onLoad(String url) {
        return getCacheNetworkRequest(url);
    }
}
