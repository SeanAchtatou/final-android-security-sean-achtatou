package com.baixing.data;

import android.content.Context;
import com.baixing.entity.UserBean;
import com.baixing.util.Util;

public class AccountManager {
    private static UserBean currentUser;

    AccountManager() {
    }

    public void logout() {
        currentUser = null;
    }

    public UserBean getCurrentUser() {
        if (currentUser != null) {
            return currentUser;
        }
        currentUser = reloadUser();
        return currentUser;
    }

    public boolean isUserLogin() {
        UserBean user = getCurrentUser();
        return (user == null || user.getPhone() == null || user.getPhone().length() <= 0) ? false : true;
    }

    public String getMyId(Context context) {
        if (currentUser != null) {
            return currentUser.getId();
        }
        currentUser = getCurrentUser();
        if (currentUser == null) {
            return null;
        }
        return currentUser.getId();
    }

    public void updatePassword(Context cxt, String password) {
        if (currentUser != null) {
            currentUser.setPassword(password);
            Util.saveDataToLocate(cxt, "user", currentUser);
        }
    }

    public String refreshAndGetMyId(Context context) {
        currentUser = null;
        currentUser = getCurrentUser();
        if (currentUser == null) {
            return null;
        }
        return currentUser.getId();
    }

    private UserBean reloadUser() {
        return (UserBean) Util.loadDataFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "user", UserBean.class);
    }
}
