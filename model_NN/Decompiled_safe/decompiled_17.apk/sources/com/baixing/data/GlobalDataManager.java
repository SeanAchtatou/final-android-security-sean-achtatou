package com.baixing.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Pair;
import com.baixing.entity.Ad;
import com.baixing.entity.Category;
import com.baixing.entity.CityDetail;
import com.baixing.entity.CityList;
import com.baixing.entity.Filterss;
import com.baixing.entity.UserBean;
import com.baixing.imageCache.ImageCacheManager;
import com.baixing.imageCache.ImageLoaderManager;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiParams;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.util.FavoriteNetworkUtil;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class GlobalDataManager implements Observer {
    protected static final String PREFS_DEVICE_ID = "device_id";
    protected static final String PREFS_FILE = "device_id.xml";
    public static WeakReference<Context> context = null;
    private static GlobalDataManager instance = null;
    public static final String kWBBaixingAppKey = "3747392969";
    public static final String kWBBaixingAppSecret = "ff394d0df1cfc41c7d89ce934b5aa8fc";
    private static int lastDestoryInstanceHash = 0;
    private static boolean needNotifiySwitchMode = true;
    private static SharedPreferences preferences = null;
    private static boolean textMode = false;
    public static boolean update = false;
    private AccountManager accountManager = new AccountManager();
    private String address = "";
    private Category allCategory = new Category();
    private String channelId;
    private String cityEnglishName = "";
    private String cityId = "";
    public String cityName = "";
    private ImageCacheManager imageCacheMgr;
    private ImageLoaderManager imageLoaderMgr;
    private Class lastActiveCls;
    public List<CityDetail> listCityDetails = new ArrayList();
    public List<Filterss> listFilterss = new ArrayList();
    public List<CityDetail> listHotCity = new ArrayList();
    public List<Ad> listMyPost = null;
    private List<Ad> listMyStore = new ArrayList();
    private List<String> listRemark = new ArrayList();
    private LocationManager locationManager = new LocationManager(context);
    private NetworkCacheManager networkCache = NetworkCacheManager.createInstance(context.get());
    private String phoneNumber = "";
    public HashMap<String, List<CityDetail>> shengMap = new HashMap<>();
    private String userToken = "";
    private String version = "";

    public void updateLastUsedCategory(String name, String englishName) {
        List<String> categories = getLastUsedCategory();
        if (categories == null) {
            categories = new ArrayList<>();
        }
        String newCategory = name + "," + englishName;
        int index = categories.indexOf(newCategory);
        if (index != 0) {
            if (index > 0) {
                categories.remove(index);
            }
            categories.add(0, newCategory);
            if (categories.size() > 3) {
                categories = categories.subList(0, 3);
            }
            Util.saveDataToLocate(getApplicationContext(), "lastUsedCategories", categories);
        }
    }

    public List<String> getLastUsedCategory() {
        return (List) Util.loadDataFromLocate(getApplicationContext(), "lastUsedCategories", List.class);
    }

    public final ImageCacheManager getImageManager() {
        if (this.imageCacheMgr == null) {
            this.imageCacheMgr = new ImageCacheManager(getApplicationContext());
        }
        return this.imageCacheMgr;
    }

    public ImageLoaderManager getImageLoaderMgr() {
        if (this.imageLoaderMgr == null) {
            this.imageLoaderMgr = new ImageLoaderManager(getImageManager());
        }
        return this.imageLoaderMgr;
    }

    public static void setTextMode(boolean tMode) {
        textMode = tMode;
        needNotifiySwitchMode = false;
        if (preferences == null) {
            preferences = context.get() != null ? context.get().getApplicationContext().getSharedPreferences("QuanleimuPreferences", 0) : null;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isTextMode", tMode);
        editor.putBoolean("needNotifyUser", false);
        editor.commit();
    }

    public static boolean isTextMode() {
        return textMode;
    }

    public static boolean needNotifySwitchMode() {
        return needNotifiySwitchMode;
    }

    public void setLastActiveActivity(Class cls) {
        this.lastActiveCls = cls;
    }

    public Class getLastActiveClass() {
        return this.lastActiveCls;
    }

    public List<Ad> getListMyStore() {
        return this.listMyStore;
    }

    public void clearMyStore() {
        if (this.listMyStore != null) {
            this.listMyStore.clear();
            saveFavoriteToLocal();
        }
    }

    private void saveFavoriteToLocal() {
        if (!getInstance().getAccountManager().isUserLogin()) {
            Util.saveDataToLocate(getApplicationContext(), "listMyStore", this.listMyStore);
        }
    }

    public List<Ad> addFav(Ad detail) {
        if (this.listMyStore == null) {
            this.listMyStore = new ArrayList();
        }
        this.listMyStore.add(0, detail);
        BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_FAV_ADDED, detail);
        saveFavoriteToLocal();
        return this.listMyStore;
    }

    public boolean isFav(Ad detail) {
        if (detail == null) {
            return false;
        }
        List<Ad> myStore = getInstance().getListMyStore();
        if (myStore == null) {
            return false;
        }
        for (int i = 0; i < myStore.size(); i++) {
            if (myStore.get(i).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                return true;
            }
        }
        return false;
    }

    public List<Ad> removeFav(Ad detail) {
        if (this.listMyStore == null || detail == null) {
            return this.listMyStore;
        }
        int i = 0;
        while (true) {
            if (i >= this.listMyStore.size()) {
                break;
            } else if (detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(this.listMyStore.get(i).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                this.listMyStore.remove(i);
                BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_FAV_REMOVE, detail);
                break;
            } else {
                i++;
            }
        }
        saveFavoriteToLocal();
        return this.listMyStore;
    }

    public void updateFav(List<Ad> favs) {
        if (favs == null) {
            favs = new ArrayList<>();
        }
        this.listMyStore = favs;
        saveFavoriteToLocal();
    }

    public List<Ad> getListMyPost() {
        return this.listMyPost;
    }

    public void updateMyAd(Ad ad) {
        if (this.listMyPost == null) {
            this.listMyPost = new ArrayList();
        }
        int index = -1;
        int i = 0;
        while (true) {
            try {
                if (i >= this.listMyPost.size()) {
                    break;
                } else if (this.listMyPost.get(i).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(ad.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                    index = i;
                    break;
                } else {
                    i++;
                }
            } catch (Throwable th) {
                return;
            }
        }
        if (index != -1) {
            this.listMyPost.add(index, ad);
            this.listMyPost.remove(index + 1);
            return;
        }
        this.listMyPost.add(0, ad);
    }

    public boolean isMyAd(Ad ad) {
        UserBean user = getInstance().getAccountManager().getCurrentUser();
        if (user == null || ad == null || user.getId() == null || ad.getValueByKey(ApiParams.KEY_USERID) == null || !user.getId().equals("u" + ad.getValueByKey(ApiParams.KEY_USERID))) {
            return false;
        }
        return true;
    }

    public void setListMyPost(List<Ad> listMyPost2) {
        this.listMyPost = listMyPost2;
    }

    public List<String> getListRemark() {
        return this.listRemark;
    }

    public void updateRemark(String[] list) {
        this.listRemark = new ArrayList();
        if (list != null) {
            for (String s : list) {
                this.listRemark.add(s);
            }
        }
    }

    public void updateRemark(List<String> list) {
        this.listRemark = new ArrayList();
        if (list != null) {
            this.listRemark.addAll(list);
        }
    }

    public void updateCityList(CityList cityList) {
        if (cityList != null && cityList.getListDetails() != null && cityList.getListDetails().size() != 0) {
            getInstance().setListCityDetails(cityList.getListDetails());
            byte[] cityData = Util.loadData(getApplicationContext(), "cityName");
            String cityName2 = cityData == null ? null : new String(cityData);
            if (cityName2 != null && !cityName2.equals("")) {
                List<CityDetail> cityDetails = getInstance().getListCityDetails();
                boolean exist = false;
                int i = 0;
                while (true) {
                    if (i >= cityDetails.size()) {
                        break;
                    } else if (cityName2.equals(cityDetails.get(i).getName())) {
                        getInstance().setCityEnglishName(cityDetails.get(i).getEnglishName());
                        getInstance().setCityName(cityName2);
                        getInstance().setCityId(cityDetails.get(i).getId());
                        exist = true;
                        break;
                    } else {
                        i++;
                    }
                }
                if (!exist) {
                    getInstance().setCityEnglishName("shanghai");
                    getInstance().setCityName("上海");
                    getInstance().setCityId("m30");
                }
            }
        }
    }

    public void setPhoneNumber(String number) {
        this.phoneNumber = number;
    }

    public void setLoginUserToken(String token) {
        this.userToken = token;
    }

    public String getLoginUserToken() {
        return this.userToken;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setAddress(String ad) {
        this.address = ad;
    }

    public String getAddress() {
        return this.address;
    }

    public List<CityDetail> getListHotCity() {
        return this.listHotCity;
    }

    public void setListHotCity(List<CityDetail> listHotCity2) {
        this.listHotCity = listHotCity2;
    }

    public HashMap<String, List<CityDetail>> getShengMap() {
        return this.shengMap;
    }

    public void setShengMap(HashMap<String, List<CityDetail>> shengMap2) {
        this.shengMap = shengMap2;
    }

    public List<CityDetail> getListCityDetails() {
        return this.listCityDetails;
    }

    public void setListCityDetails(List<CityDetail> listCityDetails2) {
        this.listCityDetails = listCityDetails2;
    }

    public String getCityEnglishName() {
        return this.cityEnglishName;
    }

    public void setCityEnglishName(String cityEnglishName2) {
        this.cityEnglishName = cityEnglishName2;
    }

    public String queryCategoryDisplayName(String englishName) {
        Category cat = this.allCategory.findCategoryByEnglishName(englishName);
        return cat == null ? englishName : cat.getName();
    }

    public List<Category> getFirstLevelCategory() {
        return this.allCategory.getChildren();
    }

    public String getCityName() {
        return this.cityName;
    }

    public void setCityName(String cityName2) {
        this.cityName = cityName2;
    }

    public String getCityId() {
        return this.cityId;
    }

    public void setCityId(String cityId2) {
        this.cityId = cityId2;
    }

    public static void resetApplication() {
        if (instance != null) {
            lastDestoryInstanceHash = instance.hashCode();
        }
        instance = null;
    }

    static void initStaticFields() {
        lastDestoryInstanceHash = 0;
    }

    public static boolean isAppDestroy(int appHash) {
        return appHash != 0 && appHash == lastDestoryInstanceHash;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.baixing.data.GlobalDataManager getInstance() {
        /*
            android.content.SharedPreferences r0 = com.baixing.data.GlobalDataManager.preferences     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            if (r0 != 0) goto L_0x002f
            java.lang.ref.WeakReference<android.content.Context> r0 = com.baixing.data.GlobalDataManager.context     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            java.lang.Object r0 = r0.get()     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            java.lang.String r1 = "QuanleimuPreferences"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            com.baixing.data.GlobalDataManager.preferences = r0     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            android.content.SharedPreferences r0 = com.baixing.data.GlobalDataManager.preferences     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            java.lang.String r1 = "isTextMode"
            r2 = 0
            boolean r0 = r0.getBoolean(r1, r2)     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            com.baixing.data.GlobalDataManager.textMode = r0     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            android.content.SharedPreferences r0 = com.baixing.data.GlobalDataManager.preferences     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            java.lang.String r1 = "needNotifyUser"
            r2 = 1
            boolean r0 = r0.getBoolean(r1, r2)     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            com.baixing.data.GlobalDataManager.needNotifiySwitchMode = r0     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
        L_0x002f:
            com.baixing.data.GlobalDataManager r0 = com.baixing.data.GlobalDataManager.instance     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            if (r0 != 0) goto L_0x003a
            com.baixing.data.GlobalDataManager r0 = new com.baixing.data.GlobalDataManager     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            r0.<init>()     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
            com.baixing.data.GlobalDataManager.instance = r0     // Catch:{ Throwable -> 0x003f, all -> 0x003d }
        L_0x003a:
            com.baixing.data.GlobalDataManager r0 = com.baixing.data.GlobalDataManager.instance
            return r0
        L_0x003d:
            r0 = move-exception
            throw r0
        L_0x003f:
            r0 = move-exception
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.data.GlobalDataManager.getInstance():com.baixing.data.GlobalDataManager");
    }

    public LocationManager getLocationManager() {
        return this.locationManager;
    }

    public AccountManager getAccountManager() {
        return this.accountManager;
    }

    public NetworkCacheManager getNetworkCacheManager() {
        return this.networkCache;
    }

    public String getVersion() {
        return this.version;
    }

    public String getChannelId() {
        return this.channelId;
    }

    private GlobalDataManager() {
        Context androidContext = null;
        androidContext = context != null ? context.get() : androidContext;
        if (androidContext != null) {
            try {
                this.version = Util.getVersion(androidContext);
                this.channelId = String.valueOf(androidContext.getPackageManager().getApplicationInfo(androidContext.getPackageName(), 128).metaData.get("UMENG_CHANNEL"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGIN);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_USER_CREATE);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_NEW_PASSWORD);
    }

    public Context getApplicationContext() {
        if (context == null || context.get() == null) {
            return null;
        }
        return context.get();
    }

    public void update(Observable observable, Object data) {
        Context cxt;
        if (data instanceof BxMessageCenter.IBxNotification) {
            BxMessageCenter.IBxNotification note = (BxMessageCenter.IBxNotification) data;
            if (IBxNotificationNames.NOTIFICATION_USER_CREATE.equals(note.getName()) || IBxNotificationNames.NOTIFICATION_LOGIN.equals(note.getName()) || IBxNotificationNames.NOTIFICATION_LOGOUT.equals(note.getName())) {
                Context cxt2 = context.get();
                if (cxt2 != null) {
                    this.accountManager.refreshAndGetMyId(cxt2);
                }
            } else if (IBxNotificationNames.NOTIFICATION_NEW_PASSWORD.equals(note.getName()) && (cxt = context.get()) != null) {
                this.accountManager.updatePassword(cxt, (String) note.getObject());
            }
        }
    }

    public void loadCategorySync() {
        Pair<Long, String> pair = null;
        if (Util.loadDataFromLocate(getApplicationContext(), "category.3.6.0", String.class) != null) {
            pair = Util.loadJsonAndTimestampFromLocate(getApplicationContext(), "saveFirstStepCate");
        }
        if (pair == null || pair.second == null || ((String) pair.second).length() == 0) {
            pair = Util.loadDataAndTimestampFromAssets(getApplicationContext(), "cateJson.txt");
        }
        String json = (String) pair.second;
        if (json != null && json.length() > 0) {
            this.allCategory = JsonUtil.loadCategoryTree(TextUtil.decodeUnicode(json));
        }
    }

    public void loadCitySync() {
        new CityList();
        Pair<Long, String> pair = null;
        if (Util.loadDataFromLocate(getApplicationContext(), "city.3.6.0", String.class) != null) {
            pair = Util.loadJsonAndTimestampFromLocate(getApplicationContext(), "cityjson");
        }
        if (pair == null || pair.second == null || ((String) pair.second).length() == 0) {
            pair = Util.loadDataAndTimestampFromAssets(getApplicationContext(), "cityjson.txt");
        }
        if (pair.second != null && ((String) pair.second).length() != 0) {
            getInstance().updateCityList(JsonUtil.parseCityListFromJson((String) pair.second));
        }
    }

    public void loadPersonalSync() {
        ReferralUtil.getInstance().register(IBxNotificationNames.NOTIFICATION_LOGIN);
        getInstance().updateRemark((String[]) Util.loadDataFromLocate(getApplicationContext(), "listRemark", String[].class));
        UserBean user = null;
        if (Util.loadDataFromLocate(getApplicationContext(), "loginToken", String.class) == null) {
            getInstance().getAccountManager().logout();
            Util.deleteDataFromLocate(getInstance().getApplicationContext(), "user");
            Util.deleteDataFromLocate(getApplicationContext(), "isPromoter");
        } else {
            getInstance().setLoginUserToken((String) Util.loadDataFromLocate(getApplicationContext(), "loginToken", String.class));
            user = getAccountManager().getCurrentUser();
        }
        if (user == null || TextUtils.isEmpty(user.getPhone())) {
            Ad[] store = (Ad[]) Util.loadDataFromLocate(getApplicationContext(), "listMyStore", Ad[].class);
            if (store != null) {
                for (Ad addFav : store) {
                    addFav(addFav);
                }
                return;
            }
            return;
        }
        FavoriteNetworkUtil.retreiveFavorites(getApplicationContext(), user);
        FavoriteNetworkUtil.syncFavorites(getApplicationContext(), user);
    }
}
