package com.baixing.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class BXDatabaseHelper extends SQLiteOpenHelper {
    public static String TABLENAME = "networkReqAndRes";

    public BXDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory cursorFactory, int version) {
        super(context, name, cursorFactory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists " + TABLENAME + "(url BLOB, response BLOB, timestamp INTEGER, UNIQUE (url) ON CONFLICT REPLACE)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("TEST_UPGRADE", "upgrage database.");
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}
