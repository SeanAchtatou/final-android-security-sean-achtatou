package com.baixing.data;

import android.content.Context;
import android.location.Location;
import com.baidu.mapapi.CoordinateConvert;
import com.baidu.mapapi.GeoPoint;
import com.baixing.entity.BXLocation;
import com.baixing.util.LocationService;
import com.baixing.util.Util;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class LocationManager implements LocationService.BXLocationServiceListener {
    private WeakReference<Context> context;
    String currentCity;
    BXLocation location = null;
    /* access modifiers changed from: private */
    public List<onLocationFetchedListener> locationFetchListeners = new ArrayList();
    boolean location_updated = false;

    public interface onLocationFetchedListener {
        void onGeocodedLocationFetched(BXLocation bXLocation);

        void onLocationFetched(BXLocation bXLocation);
    }

    LocationManager(WeakReference<Context> cxt) {
        this.context = cxt;
    }

    public String getCurrentCity() {
        return this.currentCity;
    }

    public boolean addLocationListener(onLocationFetchedListener listener) {
        if (this.context.get() == null) {
            return false;
        }
        if (listener == null || this.locationFetchListeners.contains(listener)) {
            return false;
        }
        this.locationFetchListeners.add(listener);
        LocationService.getInstance().addLocationListener(this.context.get(), this);
        BXLocation curLocation = getCurrentPosition(true);
        if (curLocation == null) {
            return false;
        }
        listener.onLocationFetched(curLocation);
        if (curLocation.geocoded) {
            listener.onGeocodedLocationFetched(curLocation);
        }
        return true;
    }

    public boolean removeLocationListener(onLocationFetchedListener listener) {
        return this.locationFetchListeners.remove(listener);
    }

    public void setLocation(BXLocation location_) {
        if (this.context.get() != null) {
            if (this.location == null) {
                getCurrentPosition(false);
            }
            if (location_ != null) {
                if (location_.geocoded) {
                    this.location = location_;
                } else {
                    this.location.fLat = location_.fLat;
                    this.location.fLon = location_.fLon;
                    if (this.location.geocoded) {
                        float[] results = {0.0f, 0.0f, 0.0f};
                        Location.distanceBetween((double) this.location.fGeoCodedLat, (double) this.location.fGeoCodedLon, (double) location_.fLat, (double) location_.fLon, results);
                        if (results[0] > 50.0f) {
                            this.location.geocoded = false;
                        }
                    }
                }
                Util.saveDataToLocateDelay(this.context.get(), "location_data", this.location);
                this.location_updated = true;
            }
        }
    }

    public BXLocation getCurrentPosition(boolean bRealLocality) {
        BXLocation bXLocation;
        if (this.context.get() == null) {
            return null;
        }
        if (this.location == null) {
            this.location = (BXLocation) Util.loadDataFromLocate(this.context.get(), "location_data", BXLocation.class);
            if (this.location == null) {
                this.location = new BXLocation(true);
            } else if (this.location.cityName == null || this.location.cityName.length() == 0) {
                this.location.geocoded = false;
            }
        }
        if (this.location_updated) {
            bXLocation = this.location;
        } else {
            bXLocation = bRealLocality ? null : this.location;
        }
        return bXLocation;
    }

    public void onLocationUpdated(Location location_) {
        BXLocation newLocation = new BXLocation(false);
        newLocation.fLat = (float) location_.getLatitude();
        newLocation.fLon = (float) location_.getLongitude();
        setLocation(newLocation);
        GeoPoint point = CoordinateConvert.bundleDecode(CoordinateConvert.fromWgs84ToBaidu(new GeoPoint((int) (location_.getLatitude() * 1000000.0d), (int) (location_.getLongitude() * 1000000.0d))));
        LocationService.getInstance().reverseGeocode((float) ((((double) point.getLatitudeE6()) * 1.0d) / 1000000.0d), (float) ((((double) point.getLongitudeE6()) * 1.0d) / 1000000.0d), new LocationService.BXRgcListener() {
            public void onRgcUpdated(BXLocation location) {
                LocationManager.this.parse4City(location);
                if (location != null) {
                    LocationManager.this.setLocation(location);
                }
                for (onLocationFetchedListener listener : LocationManager.this.locationFetchListeners) {
                    listener.onGeocodedLocationFetched(location);
                }
            }
        });
        for (onLocationFetchedListener listener : this.locationFetchListeners) {
            listener.onLocationFetched(newLocation);
        }
    }

    /* access modifiers changed from: private */
    public void parse4City(BXLocation location2) {
        if (location2 != null && location2.cityName != null) {
            this.currentCity = location2.cityName;
        }
    }
}
