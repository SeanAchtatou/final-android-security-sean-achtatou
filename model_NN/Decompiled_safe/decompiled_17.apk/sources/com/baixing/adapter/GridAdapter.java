package com.baixing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.quanleimu.activity.R;
import java.util.List;

public class GridAdapter extends BaseAdapter {
    private int colCount;
    private Context context;
    private List<GridInfo> list;
    private LayoutInflater mInflater;

    public static class GridHolder {
        ImageButton imageBtn;
        public View starIcon;
        public TextView text;
    }

    public static class GridInfo {
        public int imgResourceId;
        public int number = 0;
        public boolean starred = false;
        public String text;
    }

    public GridAdapter(Context c) {
        this.context = c;
    }

    public void setList(List<GridInfo> list2, int columnCount) {
        this.list = list2;
        this.colCount = columnCount;
        this.mInflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
    }

    public int getCount() {
        int size = this.list.size();
        return size % this.colCount == 0 ? size : (this.colCount + size) - (size % this.colCount);
    }

    public Object getItem(int index) {
        return this.list.get(index);
    }

    public long getItemId(int index) {
        return (long) index;
    }

    public boolean areAllItemsEnabled() {
        return this.list.size() % this.colCount == 0;
    }

    public boolean isEnabled(int position) {
        return position < this.list.size();
    }

    public View getView(int index, View convertView, ViewGroup parent) {
        GridHolder holder;
        int i;
        GridInfo info = null;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.categorygriditem, (ViewGroup) null);
            holder = new GridHolder();
            holder.imageBtn = (ImageButton) convertView.findViewById(R.id.itemicon);
            holder.imageBtn.setClickable(false);
            holder.imageBtn.setFocusable(false);
            holder.text = (TextView) convertView.findViewById(R.id.itemtext);
            convertView.setTag(holder);
        } else {
            holder = (GridHolder) convertView.getTag();
        }
        if (index < this.list.size()) {
            info = this.list.get(index);
        }
        if (info != null) {
            String text = info.text;
            if (info.number > 0) {
                text = String.format("%s(%d)", text, Integer.valueOf(info.number));
            }
            holder.text.setText(text);
            holder.imageBtn.setImageBitmap(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(info.imgResourceId));
            View view = holder.starIcon;
            if (info.starred) {
                i = 0;
            } else {
                i = 8;
            }
            view.setVisibility(i);
            convertView.setEnabled(true);
        } else {
            holder.starIcon.setVisibility(8);
            convertView.setEnabled(false);
        }
        if (index == 0 || (index + 1) % this.colCount != 0) {
            convertView.setBackgroundResource(R.drawable.bg_grid_selector);
        } else {
            convertView.setBackgroundResource(R.drawable.bg_grid_selector_left);
        }
        return convertView;
    }
}
