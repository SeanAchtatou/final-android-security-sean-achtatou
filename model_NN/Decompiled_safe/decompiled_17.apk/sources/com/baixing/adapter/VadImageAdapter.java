package com.baixing.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.baixing.data.GlobalDataManager;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;
import java.util.List;

public class VadImageAdapter extends BaseAdapter {
    Context context;
    List<String> listUrl;
    private WeakReference<Bitmap> mb_loading = null;
    private int pageIndex;
    private IImageProvider provider;

    public interface IImageProvider {
        void onShowView(ImageView imageView, String str, String str2, int i);
    }

    public VadImageAdapter(Context context2, List<String> listUrl2, int detailPostion, IImageProvider imageLoader) {
        this.context = context2;
        this.listUrl = listUrl2;
        this.pageIndex = detailPostion;
        this.provider = imageLoader;
        this.mb_loading = new WeakReference<>(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.icon_vad_loading));
    }

    public void setContent(List<String> listUrl2) {
        this.listUrl = listUrl2;
    }

    public int getCount() {
        return this.listUrl.size();
    }

    public List<String> getImages() {
        return this.listUrl;
    }

    public Object getItem(int arg0) {
        return this.listUrl.get(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View root = convertView;
        if (root == null) {
            root = LayoutInflater.from(this.context).inflate((int) R.layout.item_detailview, (ViewGroup) null);
        }
        ImageView iv = (ImageView) root.findViewById(R.id.ivGoods);
        iv.setImageBitmap(this.mb_loading.get());
        if (!(this.listUrl.size() == 0 || this.listUrl.get(position) == null || this.listUrl.get(position).equals(""))) {
            String prevTag = (String) iv.getTag();
            iv.setTag(this.listUrl.get(position));
            if (this.provider != null) {
                this.provider.onShowView(iv, this.listUrl.get(position), prevTag, this.pageIndex);
            }
        }
        return root;
    }
}
