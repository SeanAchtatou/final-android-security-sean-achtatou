package com.baixing.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.AdSeperator;
import com.baixing.network.NetworkUtil;
import com.baixing.util.TextUtil;
import com.baixing.view.AdViewHistory;
import com.baixing.widget.AnimatingImageView;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VadListAdapter extends BaseAdapter {
    private Context context;
    private Bitmap defaultBk2;
    private Bitmap downloadFailBk;
    private List<GroupItem> groups = new ArrayList();
    /* access modifiers changed from: private */
    public Handler handler = null;
    private boolean hasDelBtn = false;
    private List<Ad> list = new ArrayList();
    /* access modifiers changed from: private */
    public int messageWhat = -1;
    private boolean showImage = true;
    private boolean uiHold = false;
    private AdViewHistory vadHistory;

    public static final class GroupItem {
        public String filterHint;
        public boolean isCountVisible = true;
        public int resultCount;
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        if (observer != null) {
            super.unregisterDataSetObserver(observer);
        }
    }

    public void setUiHold(boolean hold) {
        this.uiHold = hold;
    }

    public void setOperateMessage(Handler h, int messageWhat2) {
        this.handler = h;
        this.messageWhat = messageWhat2;
    }

    public void setHasDelBtn(boolean has) {
        this.hasDelBtn = has;
    }

    public List<Ad> getList() {
        return this.list;
    }

    public void setList(List<Ad> list2) {
        this.list = list2;
    }

    public void updateGroups(List<GroupItem> outerGroup) {
        this.groups.clear();
        if (outerGroup != null) {
            this.groups.addAll(outerGroup);
        }
    }

    public void setList(List<Ad> list2, List<GroupItem> outerGroup) {
        this.list = list2;
        updateGroups(outerGroup);
    }

    public VadListAdapter(Context context2, List<Ad> list2, AdViewHistory adViewHistory) {
        boolean z = false;
        this.context = context2;
        this.list = list2;
        this.showImage = (!GlobalDataManager.isTextMode() || NetworkUtil.isWifiConnection(context2)) ? true : z;
        this.vadHistory = adViewHistory;
    }

    public void setImageVisible(boolean showImage2) {
        this.showImage = showImage2;
    }

    public int getCount() {
        return ((this.list == null || this.list.size() == 0) ? 0 : this.list.size()) + getGroupCount();
    }

    private int getGroupCount() {
        if (this.groups == null) {
            return 0;
        }
        return this.groups.size();
    }

    public Object getItem(int arg0) {
        return Integer.valueOf(arg0);
    }

    public long getItemId(int position) {
        return (long) getRealIndex(position);
    }

    static class ViewHolder {
        View actionLine;
        View divider;
        ImageView ivInfo;
        View operateView;
        View pbView;
        TextView tvDateAndAddress;
        TextView tvDes;
        TextView tvPrice;
        TextView tvShared;
        TextView tvUpdateDate;

        ViewHolder() {
        }
    }

    private boolean isGroupPosition(int pos) {
        if (this.groups == null || this.groups.size() == 0) {
            return false;
        }
        int skip = 0;
        for (int i = 0; i < this.groups.size(); i++) {
            if (skip == pos) {
                Log.d("LIST", "position is group " + pos);
                return true;
            }
            skip += this.groups.get(i).resultCount + 1;
        }
        return false;
    }

    private GroupItem findGroupByPos(int position) {
        int skip = 0;
        for (int i = 0; i < this.groups.size(); i++) {
            if (skip == position) {
                return this.groups.get(i);
            }
            skip += this.groups.get(i).resultCount + 1;
        }
        return null;
    }

    private int getRealIndex(int position) {
        if (this.groups == null || this.groups.size() == 0) {
            return position;
        }
        if (isGroupPosition(position)) {
            return -1;
        }
        int skip = 0;
        for (int i = 0; i < this.groups.size(); i++) {
            if (position <= this.groups.get(i).resultCount + skip) {
                return (position - i) - 1;
            }
            skip += this.groups.get(i).resultCount + 1;
        }
        return -1;
    }

    public View getView(int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View v = convertView;
        if (v == null || v.getTag() == null || !(v.getTag() instanceof ViewHolder)) {
            v = LayoutInflater.from(this.context).inflate((int) R.layout.item_goodlist_with_title, (ViewGroup) null);
            holder = new ViewHolder();
            holder.tvDes = (TextView) v.findViewById(R.id.tvDes);
            holder.tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            holder.tvDateAndAddress = (TextView) v.findViewById(R.id.tvDateAndAddress);
            holder.operateView = v.findViewById(R.id.rlListOperate);
            holder.actionLine = v.findViewById(R.id.lineView);
            holder.ivInfo = (ImageView) v.findViewById(R.id.ivInfo);
            holder.pbView = v.findViewById(R.id.pbLoadingProgress);
            holder.tvUpdateDate = (TextView) v.findViewById(R.id.tvUpdateDate);
            holder.divider = v.findViewById(R.id.vad_divider);
            holder.tvShared = (TextView) v.findViewById(R.id.sharedTag);
            ((AnimatingImageView) holder.ivInfo).setForefrontView(holder.pbView);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        int position = getRealIndex(pos);
        if (isGroupPosition(pos)) {
            v.findViewById(R.id.filter_view_root).setVisibility(0);
            v.findViewById(R.id.goods_item_view_root).setVisibility(8);
            GroupItem g = findGroupByPos(pos);
            ((TextView) v.findViewById(R.id.filter_view_root).findViewById(R.id.filter_string)).setText(g.filterHint);
            TextView countTxt = (TextView) v.findViewById(R.id.filter_view_root).findViewById(R.id.filter_result_count);
            countTxt.setText(g.resultCount + "");
            if (!g.isCountVisible) {
                countTxt.setVisibility(8);
                v.findViewById(R.id.textView2).setVisibility(8);
            }
            holder.divider.setVisibility(8);
            v.setEnabled(false);
        } else if (this.list.size() <= pos || !(this.list.get(position) instanceof AdSeperator)) {
            v.setEnabled(true);
            holder.divider.setVisibility(0);
            v.findViewById(R.id.filter_view_root).setVisibility(8);
            v.findViewById(R.id.goods_item_view_root).setVisibility(0);
            if (this.defaultBk2 == null) {
                this.defaultBk2 = GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.icon_listing_nopic);
            }
            if (this.downloadFailBk == null) {
                this.downloadFailBk = GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.home_bg_thumb_2x);
            }
            holder.ivInfo.setScaleType(ImageView.ScaleType.CENTER_CROP);
            if (!this.showImage) {
                holder.ivInfo.setVisibility(8);
            }
            if (this.showImage) {
                ArrayList<String> arrayList = new ArrayList<>();
                String strTag = null;
                if (holder.ivInfo.getTag() != null) {
                    strTag = holder.ivInfo.getTag().toString();
                }
                if (this.list.get(position).getImageList() == null || this.list.get(position).getImageList().equals("") || this.list.get(position).getImageList().getSquare() == null || this.list.get(position).getImageList().getSquare().equals("")) {
                    if (strTag != null && strTag.length() > 0) {
                        arrayList.add(strTag);
                    }
                    if (strTag == null || strTag.length() > 0) {
                        holder.ivInfo.setTag("");
                        holder.ivInfo.setImageBitmap(this.defaultBk2);
                    }
                } else {
                    String b = TextUtil.filterString(this.list.get(position).getImageList().getSquare(), new char[]{'\\', '\"'});
                    if (b.contains(",")) {
                        String[] c = b.split(",");
                        if (c[0] == null || c[0].equals("")) {
                            if (v.getTag() != null && v.getTag().toString().length() > 0) {
                                arrayList.add(v.getTag().toString());
                            }
                            if (strTag == null || strTag.length() > 0) {
                                holder.ivInfo.setTag("");
                                holder.ivInfo.setImageBitmap(this.defaultBk2);
                            }
                        } else {
                            if (strTag != null && strTag.length() > 0 && !strTag.equals(c[0])) {
                                arrayList.add(strTag);
                            }
                            if (strTag == null || !strTag.equals(c[0])) {
                                holder.ivInfo.setTag(c[0]);
                                holder.ivInfo.setVisibility(4);
                                holder.pbView.setVisibility(0);
                                GlobalDataManager.getInstance().getImageLoaderMgr().showImg(holder.ivInfo, c[0], strTag, this.context, new WeakReference(this.downloadFailBk));
                            }
                        }
                    } else if (b == null || b.equals("")) {
                        if (strTag != null && strTag.length() > 0) {
                            arrayList.add(strTag);
                        }
                        if (strTag == null || strTag.length() > 0) {
                            holder.ivInfo.setTag("");
                            holder.ivInfo.setImageBitmap(this.defaultBk2);
                        }
                    } else {
                        if (strTag != null && strTag.length() > 0 && !strTag.equals(b)) {
                            arrayList.add(strTag);
                        }
                        if (strTag == null || !strTag.equals(b)) {
                            holder.ivInfo.setTag(b);
                            holder.ivInfo.setVisibility(4);
                            holder.pbView.setVisibility(0);
                            GlobalDataManager.getInstance().getImageLoaderMgr().showImg(holder.ivInfo, b, strTag, this.context, new WeakReference(this.downloadFailBk));
                        }
                    }
                }
                if (arrayList.size() > 0) {
                    for (String Cancel : arrayList) {
                        GlobalDataManager.getInstance().getImageLoaderMgr().Cancel(Cancel, holder.ivInfo);
                    }
                }
            }
            Ad detailObj = this.list.get(position);
            boolean isValidMessage = isValidMessage(detailObj);
            if (this.hasDelBtn) {
                holder.operateView.setVisibility(0);
                holder.actionLine.setVisibility(0);
                holder.operateView.findViewById(R.id.btnListOperate).setBackgroundResource(isValidMessage ? R.drawable.btn_circle_arrow : R.drawable.icon_warning);
            } else {
                holder.operateView.setVisibility(8);
                holder.actionLine.setVisibility(8);
            }
            Pair<String, String> priceP = getPrice(detailObj);
            if (priceP == null) {
                holder.tvPrice.setVisibility(8);
            } else {
                holder.tvPrice.setVisibility(0);
                holder.tvPrice.setText((CharSequence) priceP.second);
            }
            holder.tvDes.setText(detailObj.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE));
            holder.tvDes.setTypeface(null, 1);
            if (this.vadHistory == null || !this.vadHistory.isReaded(detailObj.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                holder.tvDes.setTextColor(this.context.getResources().getColor(R.color.common_black));
                v.setBackgroundColor(this.context.getResources().getColor(R.color.bg_screen));
            } else {
                v.setBackgroundColor(Color.rgb(240, 240, 240));
            }
            String dateV = detailObj.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_DATE);
            if (dateV == null || dateV.equals("")) {
                holder.tvUpdateDate.setText("");
                holder.tvUpdateDate.setVisibility(4);
            } else {
                holder.tvUpdateDate.setText(TextUtil.timeTillNow(new Date(Long.parseLong(dateV) * 1000).getTime(), v.getContext()));
            }
            String areaV = getArea(this.list.get(position));
            if (areaV == null || areaV.equals("")) {
                holder.tvDateAndAddress.setText("");
            } else {
                holder.tvDateAndAddress.setText(areaV);
            }
            if (isValidMessage) {
                holder.tvPrice.setVisibility(priceP == null ? 8 : 0);
                holder.tvUpdateDate.setVisibility(0);
                holder.tvDateAndAddress.setTextColor(this.context.getResources().getColor(R.color.vad_list_sub_info));
            } else {
                holder.tvPrice.setVisibility(8);
                holder.tvUpdateDate.setVisibility(8);
                holder.tvDateAndAddress.setText("审核未通过");
                holder.tvDateAndAddress.setTextColor(-65536);
            }
            final int i = position;
            holder.operateView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Message msg = VadListAdapter.this.handler.obtainMessage();
                    msg.arg2 = i;
                    msg.what = VadListAdapter.this.messageWhat;
                    VadListAdapter.this.handler.sendMessage(msg);
                }
            });
            if (detailObj.getValueByKey("shared").equals("1")) {
                holder.tvShared.setVisibility(0);
            } else {
                holder.tvShared.setVisibility(8);
            }
        } else {
            v.findViewById(R.id.filter_view_root).setVisibility(0);
            v.findViewById(R.id.goods_item_view_root).setVisibility(8);
            ((TextView) v.findViewById(R.id.filter_view_root).findViewById(R.id.filter_string)).setText(this.list.get(position).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE));
            v.setEnabled(false);
            v.setClickable(false);
            holder.divider.setVisibility(8);
        }
        return v;
    }

    private boolean isValidMessage(Ad detail) {
        return !detail.getValueByKey("status").equals("4") && !detail.getValueByKey("status").equals("20");
    }

    private String getArea(Ad detail) {
        int index;
        String cityName = GlobalDataManager.getInstance().getCityName();
        String areaV = detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_AREANAME);
        if (TextUtils.isEmpty(cityName)) {
            return areaV;
        }
        if (areaV != null && areaV.startsWith(cityName) && (index = areaV.indexOf(cityName) + cityName.length()) < areaV.length() - 2) {
            areaV = areaV.substring(index + 1);
        }
        return areaV;
    }

    private Pair<String, String> getPrice(Ad detail) {
        String price = detail.getValueByKey("价格");
        String sallary = detail.getValueByKey("工资");
        if (!TextUtils.isEmpty(price)) {
            return new Pair<>("价格", price);
        }
        if (!TextUtils.isEmpty(sallary)) {
            return new Pair<>("工资", sallary);
        }
        return null;
    }
}
