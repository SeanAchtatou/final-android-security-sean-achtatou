package com.baixing.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.quanleimu.activity.R;
import java.util.List;

public class CommonItemAdapter extends BXAlphabetSortableAdapter {
    private int bottom;
    private boolean hasArrow;
    private boolean hasSearchBar;
    private int iconId;
    private int left;
    private int right;
    private boolean sorted;
    private Object tag;
    private int top;

    public void setPadding(int left2, int right2, int top2, int bottom2) {
        this.left = left2;
        this.right = right2;
        this.top = top2;
        this.bottom = bottom2;
    }

    public void setTag(Object obj) {
        this.tag = obj;
    }

    public Object getTag() {
        return this.tag;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CommonItemAdapter(android.content.Context r5, java.util.List<? extends java.lang.Object> r6, int r7, boolean r8) {
        /*
            r4 = this;
            r1 = 1
            r2 = 0
            r3 = -1
            if (r6 == 0) goto L_0x003c
            int r0 = r6.size()
            if (r0 <= r7) goto L_0x003c
            r0 = r1
        L_0x000c:
            r4.<init>(r5, r6, r0)
            r4.hasArrow = r1
            r0 = 2130837505(0x7f020001, float:1.7279966E38)
            r4.iconId = r0
            r4.left = r3
            r4.right = r3
            r4.top = r3
            r4.bottom = r3
            r4.sorted = r2
            r4.hasSearchBar = r1
            if (r6 == 0) goto L_0x003e
            int r0 = r6.size()
            if (r0 <= r7) goto L_0x003e
        L_0x002a:
            r4.sorted = r1
            boolean r0 = r4.sorted
            if (r0 == 0) goto L_0x0039
            if (r8 == 0) goto L_0x0039
            java.util.List r0 = r4.list
            java.lang.String r1 = "placeholder"
            r0.add(r2, r1)
        L_0x0039:
            r4.hasSearchBar = r8
            return
        L_0x003c:
            r0 = r2
            goto L_0x000c
        L_0x003e:
            r1 = r2
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.adapter.CommonItemAdapter.<init>(android.content.Context, java.util.List, int, boolean):void");
    }

    public void setHasArrow(boolean has) {
        this.hasArrow = has;
    }

    public void setList(List<? extends Object> list_) {
        this.list.clear();
        this.list.addAll(list_);
        notifyDataSetChanged();
    }

    public List<? extends Object> getList() {
        return this.list;
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int arg0) {
        return this.list.get(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public void setRightIcon(int resourceId) {
        this.iconId = resourceId;
    }

    class ViewHolder {
        public ImageView iv;
        public TextView tv;

        ViewHolder() {
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        ViewHolder holder;
        View header;
        if (position == 0 && this.sorted && this.hasSearchBar) {
            return LayoutInflater.from(this.context).inflate((int) R.layout.searchbar, (ViewGroup) null);
        }
        if (this.sorted && (header = getHeaderIfItIs(position, convertView)) != null) {
            return header;
        }
        if (convertView == null || convertView.getTag() == null || !(convertView.getTag() instanceof ViewHolder)) {
            v = LayoutInflater.from(this.context).inflate((int) R.layout.item_common, (ViewGroup) null);
            holder = new ViewHolder();
            holder.tv = (TextView) v.findViewById(R.id.tvCateName);
            holder.iv = (ImageView) v.findViewById(R.id.ivChoose);
            v.setTag(holder);
        } else {
            v = convertView;
            holder = (ViewHolder) convertView.getTag();
        }
        if (this.left >= 0 && this.right >= 0 && this.top >= 0 && this.bottom >= 0) {
            v.setPadding(this.left, this.top, this.right, this.bottom);
        }
        holder.tv.setText(this.list.get(position).toString());
        if (this.hasArrow) {
            holder.iv.setVisibility(0);
            holder.iv.setImageBitmap(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(this.iconId));
        } else {
            holder.iv.setVisibility(8);
        }
        return v;
    }
}
