package com.baixing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.baixing.util.BXHanzi2Pinyin;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BXAlphabetSortableAdapter extends BaseAdapter implements Comparator<Object> {
    private static final BXAlphabetSortableAdapter COMPARATOR = new BXAlphabetSortableAdapter();
    protected List<Object> backupList = null;
    protected Context context;
    protected List<Object> list = new ArrayList();

    public class BXHeader {
        public String text;

        public BXHeader() {
        }

        public String toString() {
            return this.text;
        }
    }

    public static class BXPinyinSortItem {
        public String firstChars;
        public Object obj;
        public String pinyin;

        public String toString() {
            return this.obj.toString();
        }
    }

    private BXAlphabetSortableAdapter() {
    }

    public void doFilter(String text) {
        if (this.backupList == null) {
            this.backupList = new ArrayList();
            this.backupList.addAll(this.list);
        }
        this.list.clear();
        for (int i = 0; i < this.backupList.size(); i++) {
            if (this.backupList.get(i) instanceof BXHeader) {
                this.list.add(this.backupList.get(i));
                if (i > 0 && this.list.size() > 1 && (this.list.get(this.list.size() - 2) instanceof BXHeader)) {
                    this.list.remove(this.list.size() - 2);
                }
            } else if (this.backupList.get(i) instanceof BXPinyinSortItem) {
                BXPinyinSortItem item = (BXPinyinSortItem) this.backupList.get(i);
                if (item.firstChars.contains(text) || item.pinyin.contains(text) || item.obj.toString().contains(text)) {
                    this.list.add(item);
                }
                if (this.list.size() > 0 && (this.list.get(this.list.size() - 1) instanceof BXHeader)) {
                    this.list.remove(this.list.size() - 1);
                }
            }
        }
        notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public View getHeaderIfItIs(int index, View convertView) {
        if (!(getItem(index) instanceof BXHeader)) {
            return null;
        }
        if (convertView == null || convertView.findViewById(R.id.headertext) == null) {
            View v = LayoutInflater.from(this.context).inflate((int) R.layout.alphabetheader, (ViewGroup) null);
            ((TextView) v.findViewById(R.id.headertext)).setText(getItem(index).toString());
            return v;
        }
        ((TextView) convertView.findViewById(R.id.headertext)).setText(getItem(index).toString());
        return convertView;
    }

    public BXAlphabetSortableAdapter(Context context2, List<? extends Object> list2, boolean sort) {
        if (list2 != null) {
            this.context = context2;
            if (!sort) {
                this.list.addAll(list2);
                return;
            }
            for (int i = 0; i < list2.size(); i++) {
                BXPinyinSortItem item = new BXPinyinSortItem();
                item.pinyin = "";
                item.firstChars = "";
                if (list2.get(i).toString().equals("全部")) {
                    item.pinyin = "#";
                } else if (list2.get(i).toString().equals("重庆")) {
                    item.pinyin = "chongqing";
                    item.firstChars = "cq";
                } else {
                    int j = 0;
                    while (j < list2.get(i).toString().length()) {
                        try {
                            String[] py = list2.get(i).toString().charAt(j) == 38271 ? new String[]{"chang"} : new String[]{BXHanzi2Pinyin.hanziToPinyin(String.valueOf(list2.get(i).toString().charAt(j)))};
                            if (py == null || py.length == 0) {
                                char tp = (list2.get(i).toString().charAt(j) < 'A' || list2.get(i).toString().charAt(j) > 'Z') ? list2.get(i).toString().charAt(j) : (char) ((list2.get(i).toString().charAt(j) + 'a') - 65);
                                item.pinyin += tp;
                                item.firstChars += tp;
                                j++;
                            } else {
                                item.pinyin += py[0];
                                item.firstChars += py[0].charAt(0);
                                j++;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            item.pinyin += list2.get(i).toString().charAt(j);
                            item.firstChars += item.pinyin.charAt(0);
                        }
                    }
                }
                item.pinyin = item.pinyin.trim();
                item.obj = list2.get(i);
                this.list.add(item);
            }
            Collections.sort(this.list, COMPARATOR);
            int index = 0;
            char prePinyin = 0;
            int count = list2.size();
            do {
                BXPinyinSortItem item2 = (BXPinyinSortItem) this.list.get(index);
                if (!(item2.pinyin.charAt(0) == prePinyin || item2.pinyin.charAt(0) == ((char) (prePinyin + ' ')))) {
                    prePinyin = item2.pinyin.charAt(0);
                    if (prePinyin >= 'a' && prePinyin <= 'z') {
                        prePinyin = (char) ((prePinyin + 'A') - 97);
                    }
                    if ((prePinyin >= 'A' && prePinyin <= 'Z') || prePinyin == '#') {
                        BXHeader header = new BXHeader();
                        header.text = String.valueOf(prePinyin);
                        this.list.add(index, header);
                        count++;
                    }
                }
                index++;
            } while (index < count);
        }
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int arg0) {
        return this.list.get(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater inflater = LayoutInflater.from(this.context);
        if (convertView == null) {
            v = inflater.inflate((int) R.layout.item_common, (ViewGroup) null);
        } else {
            v = convertView;
        }
        ((TextView) v.findViewById(R.id.tvCateName)).setText(this.list.get(position).toString());
        return v;
    }

    public int compare(Object lhs, Object rhs) {
        return ((BXPinyinSortItem) lhs).pinyin.compareTo(((BXPinyinSortItem) rhs).pinyin);
    }
}
