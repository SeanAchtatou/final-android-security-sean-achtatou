package com.baixing.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.baixing.adapter.BXAlphabetSortableAdapter;
import com.quanleimu.activity.R;
import java.util.List;

public class CheckableAdapter extends BXAlphabetSortableAdapter {
    private int bottom;
    private int checkedResourceId;
    private boolean hasSearchBar;
    private int left;
    private int right;
    private boolean sorted;
    private Object tag;
    private int top;
    private int uncheckedResourceId;

    public static class CheckableItem {
        public boolean checked;
        public String id;
        public String txt;

        public String toString() {
            return this.txt;
        }
    }

    public void setPadding(int left2, int right2, int top2, int bottom2) {
        this.left = left2;
        this.right = right2;
        this.top = top2;
        this.bottom = bottom2;
    }

    public void setTag(Object obj) {
        this.tag = obj;
    }

    public Object getTag() {
        return this.tag;
    }

    public void setItemCheckStatus(int position, boolean check) {
        CheckableItem item = getItem(position) instanceof BXAlphabetSortableAdapter.BXPinyinSortItem ? (CheckableItem) ((BXAlphabetSortableAdapter.BXPinyinSortItem) getItem(position)).obj : (CheckableItem) getItem(position);
        item.checked = check;
        this.list.remove(position);
        this.list.add(position, item);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CheckableAdapter(android.content.Context r5, java.util.List<? extends com.baixing.adapter.CheckableAdapter.CheckableItem> r6, int r7, boolean r8) {
        /*
            r4 = this;
            r1 = 1
            r2 = 0
            r3 = -1
            if (r6 == 0) goto L_0x0039
            int r0 = r6.size()
            if (r0 <= r7) goto L_0x0039
            r0 = r1
        L_0x000c:
            r4.<init>(r5, r6, r0)
            r4.checkedResourceId = r3
            r4.uncheckedResourceId = r3
            r4.sorted = r2
            r4.hasSearchBar = r2
            r4.left = r3
            r4.right = r3
            r4.top = r3
            r4.bottom = r3
            if (r6 == 0) goto L_0x003b
            int r0 = r6.size()
            if (r0 <= r7) goto L_0x003b
        L_0x0027:
            r4.sorted = r1
            boolean r0 = r4.sorted
            if (r0 == 0) goto L_0x0036
            if (r8 == 0) goto L_0x0036
            java.util.List r0 = r4.list
            java.lang.String r1 = "placeholder"
            r0.add(r2, r1)
        L_0x0036:
            r4.hasSearchBar = r8
            return
        L_0x0039:
            r0 = r2
            goto L_0x000c
        L_0x003b:
            r1 = r2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.adapter.CheckableAdapter.<init>(android.content.Context, java.util.List, int, boolean):void");
    }

    public void setList(List<? extends CheckableItem> list_) {
        this.list.clear();
        this.list.addAll(list_);
        notifyDataSetChanged();
    }

    public List<? extends Object> getList() {
        return this.list;
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int arg0) {
        return this.list.get(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public void setIconId(int checkedId, int uncheckedId) {
        this.checkedResourceId = checkedId;
        this.uncheckedResourceId = uncheckedId;
    }

    class ViewHolder {
        CheckBox box;
        TextView tvCateName;

        ViewHolder() {
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(this.context);
        if (position == 0 && this.sorted && this.hasSearchBar) {
            return inflater.inflate((int) R.layout.searchbar, (ViewGroup) null);
        }
        View header = getHeaderIfItIs(position, convertView);
        if (header != null) {
            return header;
        }
        if (convertView == null || convertView.getTag() == null || !(convertView.getTag() instanceof ViewHolder)) {
            v = inflater.inflate((int) R.layout.item_text_checkbox, (ViewGroup) null);
            holder = new ViewHolder();
            holder.tvCateName = (TextView) v.findViewById(R.id.checktext);
            holder.box = (CheckBox) v.findViewById(R.id.checkitem);
            v.setTag(holder);
        } else {
            v = convertView;
            holder = (ViewHolder) v.getTag();
        }
        if (this.left >= 0 && this.right >= 0 && this.top >= 0 && this.bottom >= 0) {
            v.setPadding(this.left, this.top, this.right, this.bottom);
        }
        holder.tvCateName.setText(this.list.get(position).toString());
        CheckableItem item = this.list.get(position) instanceof BXAlphabetSortableAdapter.BXPinyinSortItem ? (CheckableItem) ((BXAlphabetSortableAdapter.BXPinyinSortItem) this.list.get(position)).obj : (CheckableItem) this.list.get(position);
        holder.box.setChecked(item.checked);
        if (this.checkedResourceId > 0 && this.uncheckedResourceId > 0) {
            if (item.checked) {
                holder.box.setButtonDrawable(this.checkedResourceId);
            } else {
                holder.box.setButtonDrawable(this.uncheckedResourceId);
            }
        }
        return v;
    }
}
