package com.baixing.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.baixing.entity.ChatMessage;
import com.baixing.entity.compare.MsgTimeComparator;
import com.quanleimu.activity.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ChatMessageAdapter extends BaseAdapter {
    private final long MSG_GROUP_TIME = 43200;
    private LayoutInflater inflater;
    private List<ChatMessage> msgList = new ArrayList();
    private String myId;

    public ChatMessageAdapter(String myId2) {
        this.myId = myId2;
    }

    public void appendData(ChatMessage msg) {
        List<ChatMessage> newList = new ArrayList<>();
        newList.addAll(this.msgList);
        if (this.msgList.size() > 0) {
            long lastTime = this.msgList.get(this.msgList.size() - 1).getTimestamp();
            if (lastTime > msg.getTimestamp()) {
                msg.setTimestamp(1 + lastTime);
            }
        }
        newList.add(msg);
        refreshData(newList);
    }

    public void appendData(List<ChatMessage> newData, boolean head) {
        Collections.sort(newData, new MsgTimeComparator());
        if (head) {
            this.msgList.addAll(0, newData);
        } else {
            this.msgList.addAll(newData);
        }
    }

    public void refreshData(List<ChatMessage> newList) {
        Collections.sort(newList, new MsgTimeComparator());
        this.msgList = newList;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.msgList.size();
    }

    public int getMyMsgCount() {
        int myCount = 0;
        for (ChatMessage msg : this.msgList) {
            if (msg.getFrom().equals(this.myId)) {
                myCount++;
            }
        }
        return myCount;
    }

    public Object getItem(int arg0) {
        return this.msgList.get(arg0);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ChatMessage preMsg = null;
        if (this.inflater == null) {
            this.inflater = LayoutInflater.from(parent.getContext());
        }
        View item = convertView;
        if (item == null) {
            item = this.inflater.inflate((int) R.layout.item_im_message, (ViewGroup) null);
        }
        item.findViewById(R.id.tvTime).setVisibility(8);
        ChatMessage msg = this.msgList.get(position);
        if (position >= 0) {
            if (position != 0) {
                preMsg = this.msgList.get(position - 1);
            }
            if (position == 0 || (preMsg != null && Long.valueOf(msg.getTimestamp()).longValue() - Long.valueOf(preMsg.getTimestamp()).longValue() >= 43200)) {
                ((TextView) item.findViewById(R.id.tvTime)).setText(new SimpleDateFormat("MM-dd HH:mm").format(new Date(Long.valueOf(msg.getTimestamp()).longValue() * 1000)));
                item.findViewById(R.id.tvTime).setVisibility(0);
            }
        }
        loadMessageItem(this.myId.equalsIgnoreCase(msg.getFrom()), item, msg);
        return item;
    }

    private void loadMessageItem(boolean isMine, View parent, ChatMessage msg) {
        int i;
        View msgItem;
        int i2 = 8;
        View send = parent.findViewById(R.id.my_item);
        View receive = parent.findViewById(R.id.received_item);
        if (isMine) {
            i = 0;
        } else {
            i = 8;
        }
        send.setVisibility(i);
        if (!isMine) {
            i2 = 0;
        }
        receive.setVisibility(i2);
        if (isMine) {
            msgItem = send;
        } else {
            msgItem = receive;
        }
        View msgParent = msgItem.findViewById(R.id.im_message_content_parent);
        ((TextView) msgItem.findViewById(R.id.im_message_content)).setText(msg.getMessage());
        msgParent.setPadding(msgParent.getPaddingLeft() / 3, msgParent.getPaddingTop() / 6, msgParent.getPaddingRight() / 3, msgParent.getPaddingBottom() / 6);
    }
}
