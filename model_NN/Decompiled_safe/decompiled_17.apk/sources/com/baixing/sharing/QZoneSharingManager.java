package com.baixing.sharing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.util.Util;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

public class QZoneSharingManager extends BaseSharingManager implements IUiListener {
    public static final String STRING_ACCESS_TOKEN = "qzoneaccess";
    public static final String STRING_EXPIRES_IN = "expires_in";
    public static final String STRING_OPENID = "qzonopenid";
    public static final String mAppid = "100358719";
    private final int MSG_AUTO_FAIL = 2;
    private final int MSG_AUTO_SUCCEED = 1;
    private final int MSG_UPLOADIMG_FINISH = 3;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                QZoneSharingManager.this.uploadImage();
            } else if (msg.what == 3) {
                QZoneSharingManager.this.share2QZone(QZoneSharingManager.this.mAd);
            }
        }
    };
    private String mAccessToken;
    private Activity mActivity;
    /* access modifiers changed from: private */
    public Ad mAd;
    private String mExpires_in;
    private String mImageUrl;
    private String mOpenId;
    private Tencent mTencent;

    public QZoneSharingManager(Activity startingActivity) {
        this.mActivity = startingActivity;
        this.mAccessToken = (String) Util.loadDataFromLocate(this.mActivity, STRING_ACCESS_TOKEN, String.class);
        this.mOpenId = (String) Util.loadDataFromLocate(this.mActivity, STRING_OPENID, String.class);
        this.mExpires_in = (String) Util.loadDataFromLocate(this.mActivity, "expires_in", String.class);
        this.mTencent = Tencent.createInstance(mAppid, this.mActivity.getApplicationContext());
        if (this.mAccessToken != null && this.mOpenId != null && this.mExpires_in != null) {
            this.mTencent.setAccessToken(this.mAccessToken, this.mExpires_in);
            this.mTencent.setOpenId(this.mOpenId);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005e A[SYNTHETIC, Splitter:B:24:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0089 A[SYNTHETIC, Splitter:B:38:0x0089] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0092 A[SYNTHETIC, Splitter:B:43:0x0092] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void uploadImage() {
        /*
            r14 = this;
            com.baixing.entity.Ad r11 = r14.mAd
            if (r11 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            com.baixing.entity.Ad r11 = r14.mAd
            com.baixing.entity.ImageList r7 = r11.getImageList()
            if (r7 == 0) goto L_0x0096
            java.lang.String r0 = r7.getBig()
            if (r0 == 0) goto L_0x0096
            int r11 = r0.length()
            if (r11 <= 0) goto L_0x0096
            com.baixing.data.GlobalDataManager r11 = com.baixing.data.GlobalDataManager.getInstance()
            com.baixing.imageCache.ImageCacheManager r11 = r11.getImageManager()
            java.lang.String r12 = ","
            java.lang.String[] r12 = r0.split(r12)
            r13 = 0
            r12 = r12[r13]
            java.lang.String r9 = r11.getFileInDiskCache(r12)
            if (r9 == 0) goto L_0x0096
            int r11 = r9.length()
            if (r11 <= 0) goto L_0x0096
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r5 = 0
            java.io.File r3 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00a8, IOException -> 0x0086, all -> 0x008f }
            r3.<init>(r9)     // Catch:{ FileNotFoundException -> 0x00a8, IOException -> 0x0086, all -> 0x008f }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00a8, IOException -> 0x0086, all -> 0x008f }
            r6.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00a8, IOException -> 0x0086, all -> 0x008f }
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r11]     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00a5, all -> 0x00a2 }
            r8 = 0
        L_0x004b:
            int r8 = r6.read(r4)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00a5, all -> 0x00a2 }
            if (r8 <= 0) goto L_0x007c
            java.lang.String r2 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00a5, all -> 0x00a2 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00a5, all -> 0x00a2 }
            r10.append(r2)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00a5, all -> 0x00a2 }
            goto L_0x004b
        L_0x005a:
            r11 = move-exception
            r5 = r6
        L_0x005c:
            if (r5 == 0) goto L_0x0061
            r5.close()     // Catch:{ IOException -> 0x009e }
        L_0x0061:
            if (r10 == 0) goto L_0x0096
            int r11 = r10.length()
            if (r11 <= 0) goto L_0x0096
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            java.lang.String r11 = "picture"
            java.lang.String r12 = r10.toString()
            byte[] r12 = r12.getBytes()
            r1.putByteArray(r11, r12)
            goto L_0x0004
        L_0x007c:
            if (r6 == 0) goto L_0x00aa
            r6.close()     // Catch:{ IOException -> 0x0083 }
            r5 = r6
            goto L_0x0061
        L_0x0083:
            r11 = move-exception
            r5 = r6
            goto L_0x0061
        L_0x0086:
            r11 = move-exception
        L_0x0087:
            if (r5 == 0) goto L_0x0061
            r5.close()     // Catch:{ IOException -> 0x008d }
            goto L_0x0061
        L_0x008d:
            r11 = move-exception
            goto L_0x0061
        L_0x008f:
            r11 = move-exception
        L_0x0090:
            if (r5 == 0) goto L_0x0095
            r5.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x0095:
            throw r11
        L_0x0096:
            android.os.Handler r11 = r14.handler
            r12 = 3
            r11.sendEmptyMessage(r12)
            goto L_0x0004
        L_0x009e:
            r11 = move-exception
            goto L_0x0061
        L_0x00a0:
            r12 = move-exception
            goto L_0x0095
        L_0x00a2:
            r11 = move-exception
            r5 = r6
            goto L_0x0090
        L_0x00a5:
            r11 = move-exception
            r5 = r6
            goto L_0x0087
        L_0x00a8:
            r11 = move-exception
            goto L_0x005c
        L_0x00aa:
            r5 = r6
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.sharing.QZoneSharingManager.uploadImage():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.sharing.QZoneSharingFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* access modifiers changed from: private */
    public void share2QZone(Ad ad) {
        String imgUrl = BaseSharingManager.getThumbnailUrl(this.mAd);
        String imgPath = (imgUrl == null || imgUrl.length() == 0) ? "" : GlobalDataManager.getInstance().getImageManager().getFileInDiskCache(imgUrl);
        Bundle bundle = new Bundle();
        bundle.putString(BaseSharingFragment.EXTRA_WEIBO_CONTENT, "我用百姓网App 发布了\"" + this.mAd.getValueByKey(Constants.PARAM_TITLE) + "\"" + "麻烦朋友们帮忙转发一下～ ");
        if (imgPath == null || imgPath.length() == 0) {
            imgPath = "";
        }
        bundle.putString(BaseSharingFragment.EXTRA_PIC_URI, imgPath);
        bundle.putString(BaseSharingFragment.EXTRA_ACCESS_TOKEN, this.mAccessToken);
        bundle.putString(BaseSharingFragment.EXTRA_EXPIRES_IN, this.mExpires_in);
        bundle.putString(QZoneSharingFragment.EXTRA_OPEN_ID, this.mOpenId);
        bundle.putString(QZoneSharingFragment.EXTRA_LINK, this.mAd.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_LINK));
        bundle.putString(QZoneSharingFragment.EXTRA_TITLE, this.mAd.getValueByKey(Constants.PARAM_TITLE));
        bundle.putString(QZoneSharingFragment.EXTRA_SUMMARY, this.mAd.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_DESCRIPTION));
        ((BaseActivity) this.mActivity).pushFragment((BaseFragment) new QZoneSharingFragment(), bundle, false);
    }

    public void auth() {
        this.mTencent.login(this.mActivity, "get_user_info,get_user_profile,add_share,add_topic,list_album,upload_pic,add_album", this);
    }

    public void share(Ad ad) {
        this.mAd = ad;
        if (this.mAccessToken == null || this.mOpenId == null || this.mExpires_in == null) {
            auth();
        } else if (ad.getImageList() == null || ad.getImageList().getBig() == null || ad.getImageList().getBig().length() <= 0) {
            share2QZone(ad);
        } else {
            uploadImage();
        }
    }

    public void release() {
    }

    public void onCancel() {
    }

    public void onComplete(JSONObject arg0) {
        if (arg0 != null) {
            try {
                this.mAccessToken = arg0.getString("access_token");
                this.mExpires_in = arg0.getString("expires_in");
                this.mOpenId = arg0.getString(Constants.PARAM_OPEN_ID);
                Util.saveDataToLocate(this.mActivity, STRING_ACCESS_TOKEN, this.mAccessToken);
                Util.saveDataToLocate(this.mActivity, STRING_OPENID, this.mOpenId);
                Util.saveDataToLocate(this.mActivity, "expires_in", this.mExpires_in);
                this.handler.sendEmptyMessage(1);
                this.mActivity.sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_QZONE_AUTH_SUCCESS));
            } catch (JSONException e) {
            }
        }
    }

    public void onError(UiError arg0) {
    }
}
