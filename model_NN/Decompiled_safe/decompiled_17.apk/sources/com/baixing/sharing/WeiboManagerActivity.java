package com.baixing.sharing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.sharing.WeiboSSOSharingManager;
import com.baixing.sharing.weibo.Oauth2AccessToken;
import com.baixing.sharing.weibo.SsoHandler;
import com.baixing.sharing.weibo.Weibo;
import com.baixing.sharing.weibo.WeiboAuthListener;
import com.baixing.sharing.weibo.WeiboDialogError;
import com.baixing.sharing.weibo.WeiboException;
import com.baixing.util.ViewUtil;

public class WeiboManagerActivity extends Activity {
    private SsoHandler mSsoHandler = null;

    public void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(null);
        super.onCreate(savedInstanceState);
    }

    public void doAuthSSO() {
        CookieSyncManager.createInstance(this);
        CookieManager.getInstance().removeAllCookie();
        this.mSsoHandler = new SsoHandler(this, Weibo.getInstance(GlobalDataManager.kWBBaixingAppKey, "http://www.baixing.com"));
        this.mSsoHandler.authorize(new WeiboAuthListener() {
            public void onCancel() {
                WeiboManagerActivity.this.finish();
            }

            public void onComplete(Bundle values) {
                String token = values.getString("access_token");
                String expires_in = values.getString("expires_in");
                if (new Oauth2AccessToken(token, expires_in).isSessionValid()) {
                    WeiboSSOSharingManager.WeiboAccessTokenWrapper wtw = new WeiboSSOSharingManager.WeiboAccessTokenWrapper();
                    wtw.setToken(token);
                    wtw.setExpires_in(expires_in);
                    WeiboSSOSharingManager.saveToken(WeiboManagerActivity.this, wtw);
                    WeiboManagerActivity.this.sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_WEIBO_AUTH_DONE));
                }
                WeiboManagerActivity.this.finish();
            }

            public void onError(WeiboDialogError arg0) {
                ViewUtil.showToast(WeiboManagerActivity.this, arg0.getMessage(), true);
            }

            public void onWeiboException(WeiboException arg0) {
                ViewUtil.showToast(WeiboManagerActivity.this, arg0.getMessage(), true);
            }
        });
    }

    public void onResume() {
        super.onResume();
        if (this.mSsoHandler == null) {
            doAuthSSO();
        }
    }

    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.mSsoHandler != null) {
            this.mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
        }
    }
}
