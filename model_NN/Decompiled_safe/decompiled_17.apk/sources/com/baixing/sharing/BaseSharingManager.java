package com.baixing.sharing;

import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.ImageList;

public abstract class BaseSharingManager {
    public abstract void auth();

    public abstract void release();

    public abstract void share(Ad ad);

    protected static String getThumbnailUrl(Ad goodDetail) {
        ImageList il = goodDetail.getImageList();
        String imgPath = "";
        if (il == null) {
            return "";
        }
        String imgUrl = il.getSquare();
        if (imgUrl != null && imgUrl.length() > 0) {
            imgUrl = imgUrl.split(",")[0];
            imgPath = GlobalDataManager.getInstance().getImageManager().getFileInDiskCache(imgUrl);
        }
        if (imgPath != null && imgPath.length() != 0) {
            return imgUrl;
        }
        String imgUrl2 = il.getSquare();
        if (imgUrl2 == null || imgUrl2.length() <= 0) {
            return imgUrl2;
        }
        return imgUrl2.split(",")[0];
    }
}
