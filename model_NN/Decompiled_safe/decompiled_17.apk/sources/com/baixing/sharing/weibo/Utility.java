package com.baixing.sharing.weibo;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class Utility {
    private static byte[] decodes = new byte[256];
    private static char[] encodes = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    public static Bundle parseUrl(String url) {
        try {
            URL u = new URL(url);
            Bundle b = decodeUrl(u.getQuery());
            b.putAll(decodeUrl(u.getRef()));
            return b;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }

    public static Bundle decodeUrl(String s) {
        Bundle params = new Bundle();
        if (s != null) {
            for (String parameter : s.split("&")) {
                String[] v = parameter.split("=");
                if (v != null && v.length >= 2) {
                    params.putString(URLDecoder.decode(v[0]), URLDecoder.decode(v[1]));
                }
            }
        }
        return params;
    }

    public static String encodeUrl(WeiboParameters parameters) {
        if (parameters == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (int loc = 0; loc < parameters.size(); loc++) {
            if (first) {
                first = false;
            } else {
                sb.append("&");
            }
            String _key = parameters.getKey(loc);
            if (parameters.getValue(_key) == null) {
                Log.i("encodeUrl", "key:" + _key + " 's value is null");
            } else {
                sb.append(URLEncoder.encode(parameters.getKey(loc)) + "=" + URLEncoder.encode(parameters.getValue(loc)));
            }
        }
        return sb.toString();
    }

    public static String encodeParameters(WeiboParameters httpParams) {
        if (httpParams == null || isBundleEmpty(httpParams)) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        int j = 0;
        for (int loc = 0; loc < httpParams.size(); loc++) {
            String key = httpParams.getKey(loc);
            if (j != 0) {
                buf.append("&");
            }
            try {
                buf.append(URLEncoder.encode(key, "UTF-8")).append("=").append(URLEncoder.encode(httpParams.getValue(key), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
            j++;
        }
        return buf.toString();
    }

    public static void showAlert(Context context, String title, String text) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(text);
        alertBuilder.create().show();
    }

    private static boolean isBundleEmpty(WeiboParameters bundle) {
        if (bundle == null || bundle.size() == 0) {
            return true;
        }
        return false;
    }

    public static String encodeBase62(byte[] data) {
        String valueOf;
        StringBuffer sb = new StringBuffer(data.length * 2);
        int pos = 0;
        int val = 0;
        for (byte b : data) {
            val = (val << 8) | (b & 255);
            pos += 8;
            while (pos > 5) {
                pos -= 6;
                char c = encodes[val >> pos];
                if (c == 'i') {
                    valueOf = "ia";
                } else {
                    valueOf = c == '+' ? "ib" : c == '/' ? "ic" : Character.valueOf(c);
                }
                sb.append(valueOf);
                val &= (1 << pos) - 1;
            }
        }
        if (pos > 0) {
            char c2 = encodes[val << (6 - pos)];
            sb.append(c2 == 'i' ? "ia" : c2 == '+' ? "ib" : c2 == '/' ? "ic" : Character.valueOf(c2));
        }
        return sb.toString();
    }

    public static byte[] decodeBase62(String string) {
        if (string == null) {
            return null;
        }
        char[] data = string.toCharArray();
        ByteArrayOutputStream baos = new ByteArrayOutputStream(string.toCharArray().length);
        int pos = 0;
        int val = 0;
        int i = 0;
        while (i < data.length) {
            char c = data[i];
            if (c == 'i') {
                i++;
                char c2 = data[i];
                if (c2 == 'a') {
                    c = 'i';
                } else if (c2 == 'b') {
                    c = '+';
                } else if (c2 == 'c') {
                    c = '/';
                } else {
                    i--;
                    c = data[i];
                }
            }
            val = (val << 6) | decodes[c];
            pos += 6;
            while (pos > 7) {
                pos -= 8;
                baos.write(val >> pos);
                val &= (1 << pos) - 1;
            }
            i++;
        }
        return baos.toByteArray();
    }

    private static boolean deleteDependon(File file, int maxRetryCount) {
        int retryCount = 1;
        if (maxRetryCount < 1) {
            maxRetryCount = 5;
        }
        boolean isDeleted = false;
        if (file != null) {
            while (!isDeleted && retryCount <= maxRetryCount && file.isFile() && file.exists()) {
                isDeleted = file.delete();
                if (!isDeleted) {
                    retryCount++;
                }
            }
        }
        return isDeleted;
    }

    private static void mkdirs(File dir_) {
        if (dir_ != null && !dir_.exists() && !dir_.mkdirs()) {
            throw new RuntimeException("fail to make " + dir_.getAbsolutePath());
        }
    }

    private static void createNewFile(File file_) {
        if (file_ != null && !__createNewFile(file_)) {
            throw new RuntimeException(file_.getAbsolutePath() + " doesn't be created!");
        }
    }

    private static void delete(File f) {
        if (f != null && f.exists() && !f.delete()) {
            throw new RuntimeException(f.getAbsolutePath() + " doesn't be deleted!");
        }
    }

    private static boolean __createNewFile(File file_) {
        if (file_ == null) {
            return false;
        }
        makesureParentExist(file_);
        if (file_.exists()) {
            delete(file_);
        }
        try {
            return file_.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean deleteDependon(String filepath, int maxRetryCount) {
        if (TextUtils.isEmpty(filepath)) {
            return false;
        }
        return deleteDependon(new File(filepath), maxRetryCount);
    }

    /* access modifiers changed from: private */
    public static boolean deleteDependon(String filepath) {
        return deleteDependon(filepath, 0);
    }

    private static boolean doesExisted(File file) {
        return file != null && file.exists();
    }

    /* access modifiers changed from: private */
    public static boolean doesExisted(String filepath) {
        if (TextUtils.isEmpty(filepath)) {
            return false;
        }
        return doesExisted(new File(filepath));
    }

    private static void makesureParentExist(File file_) {
        File parent;
        if (file_ != null && (parent = file_.getParentFile()) != null && !parent.exists()) {
            mkdirs(parent);
        }
    }

    private static void makesureFileExist(File file) {
        if (file != null && !file.exists()) {
            makesureParentExist(file);
            createNewFile(file);
        }
    }

    /* access modifiers changed from: private */
    public static void makesureFileExist(String filePath_) {
        if (filePath_ != null) {
            makesureFileExist(new File(filePath_));
        }
    }

    public static boolean isWifi(Context mContext) {
        NetworkInfo activeNetInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetInfo == null || activeNetInfo.getType() != 1) {
            return false;
        }
        return true;
    }

    public static final class UploadImageUtils {
        private static void revitionImageSizeHD(String picfile, int size, int quality) throws IOException {
            Bitmap outputBitmap;
            if (size <= 0) {
                throw new IllegalArgumentException("size must be greater than 0!");
            } else if (!Utility.doesExisted(picfile)) {
                if (picfile == null) {
                    picfile = "null";
                }
                throw new FileNotFoundException(picfile);
            } else if (!BitmapHelper.verifyBitmap(picfile)) {
                throw new IOException("");
            } else {
                int photoSizesOrg = size * 2;
                FileInputStream input = new FileInputStream(picfile);
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(input, null, opts);
                try {
                    input.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                int i = 0;
                while (true) {
                    if ((opts.outWidth >> i) <= photoSizesOrg && (opts.outHeight >> i) <= photoSizesOrg) {
                        break;
                    }
                    i++;
                }
                opts.inSampleSize = (int) Math.pow(2.0d, (double) i);
                opts.inJustDecodeBounds = false;
                Bitmap temp = safeDecodeBimtapFile(picfile, opts);
                if (temp == null) {
                    throw new IOException("Bitmap decode error!");
                }
                boolean unused = Utility.deleteDependon(picfile);
                Utility.makesureFileExist(picfile);
                float rateOutPut = ((float) size) / ((float) (temp.getWidth() > temp.getHeight() ? temp.getWidth() : temp.getHeight()));
                if (rateOutPut < 1.0f) {
                    while (true) {
                        try {
                            outputBitmap = Bitmap.createBitmap((int) (((float) temp.getWidth()) * rateOutPut), (int) (((float) temp.getHeight()) * rateOutPut), Bitmap.Config.ARGB_8888);
                            break;
                        } catch (OutOfMemoryError e) {
                            System.gc();
                            rateOutPut = (float) (((double) rateOutPut) * 0.8d);
                        }
                    }
                    if (outputBitmap == null) {
                        temp.recycle();
                    }
                    Canvas canvas = new Canvas(outputBitmap);
                    Matrix matrix = new Matrix();
                    matrix.setScale(rateOutPut, rateOutPut);
                    canvas.drawBitmap(temp, matrix, new Paint());
                    temp.recycle();
                    temp = outputBitmap;
                }
                FileOutputStream output = new FileOutputStream(picfile);
                if (opts == null || opts.outMimeType == null || !opts.outMimeType.contains("png")) {
                    temp.compress(Bitmap.CompressFormat.JPEG, quality, output);
                } else {
                    temp.compress(Bitmap.CompressFormat.PNG, quality, output);
                }
                try {
                    output.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                temp.recycle();
            }
        }

        private static void revitionImageSize(String picfile, int size, int quality) throws IOException {
            if (size <= 0) {
                throw new IllegalArgumentException("size must be greater than 0!");
            } else if (!Utility.doesExisted(picfile)) {
                if (picfile == null) {
                    picfile = "null";
                }
                throw new FileNotFoundException(picfile);
            } else if (!BitmapHelper.verifyBitmap(picfile)) {
                throw new IOException("");
            } else {
                FileInputStream input = new FileInputStream(picfile);
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(input, null, opts);
                try {
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int i = 0;
                while (true) {
                    if ((opts.outWidth >> i) <= size && (opts.outHeight >> i) <= size) {
                        break;
                    }
                    i++;
                }
                opts.inSampleSize = (int) Math.pow(2.0d, (double) i);
                opts.inJustDecodeBounds = false;
                Bitmap temp = safeDecodeBimtapFile(picfile, opts);
                if (temp == null) {
                    throw new IOException("Bitmap decode error!");
                }
                boolean unused = Utility.deleteDependon(picfile);
                Utility.makesureFileExist(picfile);
                FileOutputStream output = new FileOutputStream(picfile);
                if (opts == null || opts.outMimeType == null || !opts.outMimeType.contains("png")) {
                    temp.compress(Bitmap.CompressFormat.JPEG, quality, output);
                } else {
                    temp.compress(Bitmap.CompressFormat.PNG, quality, output);
                }
                try {
                    output.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                temp.recycle();
            }
        }

        public static boolean revitionPostImageSize(String picfile) {
            try {
                if (Weibo.isWifi) {
                    revitionImageSizeHD(picfile, 1600, 75);
                } else {
                    revitionImageSize(picfile, 1024, 75);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        private static Bitmap safeDecodeBimtapFile(String bmpFile, BitmapFactory.Options opts) {
            BitmapFactory.Options optsTmp = opts;
            if (optsTmp == null) {
                optsTmp = new BitmapFactory.Options();
                optsTmp.inSampleSize = 1;
            }
            Bitmap bmp = null;
            FileInputStream input = null;
            int i = 0;
            while (true) {
                FileInputStream input2 = input;
                if (i >= 5) {
                    break;
                }
                try {
                    input = new FileInputStream(bmpFile);
                    try {
                        bmp = BitmapFactory.decodeStream(input, null, opts);
                        try {
                            input.close();
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    } catch (OutOfMemoryError e2) {
                        e = e2;
                    } catch (FileNotFoundException e3) {
                    }
                } catch (OutOfMemoryError e4) {
                    e = e4;
                    input = input2;
                    e.printStackTrace();
                    optsTmp.inSampleSize *= 2;
                    try {
                        input.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    i++;
                } catch (FileNotFoundException e5) {
                }
                i++;
            }
            return bmp;
        }
    }
}
