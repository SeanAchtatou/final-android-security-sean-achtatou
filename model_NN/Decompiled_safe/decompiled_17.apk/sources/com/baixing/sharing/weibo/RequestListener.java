package com.baixing.sharing.weibo;

import java.io.IOException;

public interface RequestListener {
    void onComplete(String str);

    void onError(WeiboException weiboException);

    void onIOException(IOException iOException);
}
