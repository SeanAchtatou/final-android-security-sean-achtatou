package com.baixing.sharing.weibo;

import android.text.TextUtils;
import com.baixing.sharing.weibo.Utility;
import com.baixing.util.TraceUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class HttpManager {
    private static final String BOUNDARY = getBoundry();
    private static final String END_MP_BOUNDARY = ("--" + BOUNDARY + "--");
    public static final String HTTPMETHOD_GET = "GET";
    private static final String HTTPMETHOD_POST = "POST";
    private static final String MP_BOUNDARY = ("--" + BOUNDARY);
    private static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private static final int SET_CONNECTION_TIMEOUT = 5000;
    private static final int SET_SOCKET_TIMEOUT = 20000;

    public static String openUrl(String url, String method, WeiboParameters params, String file) throws WeiboException {
        try {
            HttpClient client = getNewHttpClient();
            HttpUriRequest request = null;
            client.getParams().setParameter("http.route.default-proxy", NetStateManager.getAPN());
            if (method.equals("GET")) {
                request = new HttpGet(url + "?" + Utility.encodeUrl(params));
            } else if (method.equals("POST")) {
                HttpUriRequest post = new HttpPost(url);
                request = post;
                String _contentType = params.getValue("content-type");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                if (!TextUtils.isEmpty(file)) {
                    paramToUpload(bos, params);
                    post.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
                    Utility.UploadImageUtils.revitionPostImageSize(file);
                    imageContentToUpload(bos, file);
                } else {
                    if (_contentType != null) {
                        params.remove("content-type");
                        post.setHeader("Content-Type", _contentType);
                    } else {
                        post.setHeader("Content-Type", PostMethod.FORM_URL_ENCODED_CONTENT_TYPE);
                    }
                    bos.write(Utility.encodeParameters(params).getBytes("UTF-8"));
                }
                byte[] data = bos.toByteArray();
                bos.close();
                post.setEntity(new ByteArrayEntity(data));
            } else if (method.equals("DELETE")) {
                request = new HttpDelete(url);
            }
            HttpResponse response = client.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return readHttpResponse(response);
            }
            throw new WeiboException(readHttpResponse(response), statusCode);
        } catch (IOException e) {
            throw new WeiboException((Exception) e);
        }
    }

    private static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 10000);
            HttpConnectionParams.setSoTimeout(params, 10000);
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, SET_SOCKET_TIMEOUT);
            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    private static class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);
            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            this.sslContext.init(null, new TrustManager[]{tm}, null);
        }

        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return this.sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        public Socket createSocket() throws IOException {
            return this.sslContext.getSocketFactory().createSocket();
        }
    }

    private static void paramToUpload(OutputStream baos, WeiboParameters params) throws WeiboException {
        int loc = 0;
        while (loc < params.size()) {
            String key = params.getKey(loc);
            StringBuilder temp = new StringBuilder(10);
            temp.setLength(0);
            temp.append(MP_BOUNDARY).append(TraceUtil.LINE);
            temp.append("content-disposition: form-data; name=\"").append(key).append("\"\r\n\r\n");
            temp.append(params.getValue(key)).append(TraceUtil.LINE);
            try {
                baos.write(temp.toString().getBytes());
                loc++;
            } catch (IOException e) {
                throw new WeiboException((Exception) e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0094 A[SYNTHETIC, Splitter:B:26:0x0094] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void imageContentToUpload(java.io.OutputStream r10, java.lang.String r11) throws com.baixing.sharing.weibo.WeiboException {
        /*
            if (r11 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = com.baixing.sharing.weibo.HttpManager.MP_BOUNDARY
            java.lang.StringBuilder r8 = r7.append(r8)
            java.lang.String r9 = "\r\n"
            r8.append(r9)
            java.lang.String r8 = "Content-Disposition: form-data; name=\"pic\"; filename=\""
            java.lang.StringBuilder r8 = r7.append(r8)
            java.lang.String r9 = "news_image"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "\"\r\n"
            r8.append(r9)
            java.lang.String r3 = "image/png"
            java.lang.String r8 = "Content-Type: "
            java.lang.StringBuilder r8 = r7.append(r8)
            java.lang.StringBuilder r8 = r8.append(r3)
            java.lang.String r9 = "\r\n\r\n"
            r8.append(r9)
            java.lang.String r8 = r7.toString()
            byte[] r6 = r8.getBytes()
            r4 = 0
            r10.write(r6)     // Catch:{ IOException -> 0x00a2 }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00a2 }
            r5.<init>(r11)     // Catch:{ IOException -> 0x00a2 }
            r8 = 51200(0xc800, float:7.1746E-41)
            byte[] r0 = new byte[r8]     // Catch:{ IOException -> 0x0089, all -> 0x009f }
        L_0x004b:
            int r1 = r5.read(r0)     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            r8 = -1
            if (r1 != r8) goto L_0x0084
            java.lang.String r8 = "\r\n"
            byte[] r8 = r8.getBytes()     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            r10.write(r8)     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            r8.<init>()     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            java.lang.String r9 = "\r\n"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            java.lang.String r9 = com.baixing.sharing.weibo.HttpManager.END_MP_BOUNDARY     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            byte[] r8 = r8.getBytes()     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            r10.write(r8)     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            if (r5 == 0) goto L_0x0002
            r5.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0002
        L_0x007d:
            r2 = move-exception
            com.baixing.sharing.weibo.WeiboException r8 = new com.baixing.sharing.weibo.WeiboException
            r8.<init>(r2)
            throw r8
        L_0x0084:
            r8 = 0
            r10.write(r0, r8, r1)     // Catch:{ IOException -> 0x0089, all -> 0x009f }
            goto L_0x004b
        L_0x0089:
            r2 = move-exception
            r4 = r5
        L_0x008b:
            com.baixing.sharing.weibo.WeiboException r8 = new com.baixing.sharing.weibo.WeiboException     // Catch:{ all -> 0x0091 }
            r8.<init>(r2)     // Catch:{ all -> 0x0091 }
            throw r8     // Catch:{ all -> 0x0091 }
        L_0x0091:
            r8 = move-exception
        L_0x0092:
            if (r4 == 0) goto L_0x0097
            r4.close()     // Catch:{ IOException -> 0x0098 }
        L_0x0097:
            throw r8
        L_0x0098:
            r2 = move-exception
            com.baixing.sharing.weibo.WeiboException r8 = new com.baixing.sharing.weibo.WeiboException
            r8.<init>(r2)
            throw r8
        L_0x009f:
            r8 = move-exception
            r4 = r5
            goto L_0x0092
        L_0x00a2:
            r2 = move-exception
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.sharing.weibo.HttpManager.imageContentToUpload(java.io.OutputStream, java.lang.String):void");
    }

    private static String readHttpResponse(HttpResponse response) {
        try {
            InputStream inputStream = response.getEntity().getContent();
            ByteArrayOutputStream content = new ByteArrayOutputStream();
            Header header = response.getFirstHeader("Content-Encoding");
            if (header != null && header.getValue().toLowerCase().indexOf("gzip") > -1) {
                inputStream = new GZIPInputStream(inputStream);
            }
            byte[] sBuffer = new byte[512];
            while (true) {
                int readBytes = inputStream.read(sBuffer);
                if (readBytes == -1) {
                    return new String(content.toByteArray());
                }
                content.write(sBuffer, 0, readBytes);
            }
        } catch (IOException | IllegalStateException e) {
            return "";
        }
    }

    static String getBoundry() {
        StringBuffer _sb = new StringBuffer();
        for (int t = 1; t < 12; t++) {
            long time = System.currentTimeMillis() + ((long) t);
            if (time % 3 == 0) {
                _sb.append(((char) ((int) time)) % 9);
            } else if (time % 3 == 1) {
                _sb.append((char) ((int) (65 + (time % 26))));
            } else {
                _sb.append((char) ((int) (97 + (time % 26))));
            }
        }
        return _sb.toString();
    }
}
