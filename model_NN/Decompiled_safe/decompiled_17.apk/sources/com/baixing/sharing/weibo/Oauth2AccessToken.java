package com.baixing.sharing.weibo;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class Oauth2AccessToken {
    private String mAccessToken = "";
    private long mExpiresTime = 0;
    private String mRefreshToken = "";

    public Oauth2AccessToken() {
    }

    public Oauth2AccessToken(String responsetext) {
        if (responsetext != null && responsetext.indexOf("{") >= 0) {
            try {
                JSONObject json = new JSONObject(responsetext);
                setToken(json.optString("access_token"));
                setExpiresIn(json.optString("expires_in"));
                setRefreshToken(json.optString(Weibo.KEY_REFRESHTOKEN));
            } catch (JSONException e) {
            }
        }
    }

    public Oauth2AccessToken(String accessToken, String expires_in) {
        this.mAccessToken = accessToken;
        this.mExpiresTime = System.currentTimeMillis() + (Long.parseLong(expires_in) * 1000);
    }

    public boolean isSessionValid() {
        return !TextUtils.isEmpty(this.mAccessToken) && (this.mExpiresTime == 0 || System.currentTimeMillis() < this.mExpiresTime);
    }

    public String getToken() {
        return this.mAccessToken;
    }

    public String getRefreshToken() {
        return this.mRefreshToken;
    }

    public void setRefreshToken(String mRefreshToken2) {
        this.mRefreshToken = mRefreshToken2;
    }

    public long getExpiresTime() {
        return this.mExpiresTime;
    }

    public void setExpiresIn(String expiresIn) {
        if (expiresIn != null && !expiresIn.equals("0")) {
            setExpiresTime(System.currentTimeMillis() + (Long.parseLong(expiresIn) * 1000));
        }
    }

    public void setExpiresTime(long mExpiresTime2) {
        this.mExpiresTime = mExpiresTime2;
    }

    public void setToken(String mToken) {
        this.mAccessToken = mToken;
    }
}
