package com.baixing.sharing.weibo;

import android.os.Bundle;

public interface WeiboAuthListener {
    void onCancel();

    void onComplete(Bundle bundle);

    void onError(WeiboDialogError weiboDialogError);

    void onWeiboException(WeiboException weiboException);
}
