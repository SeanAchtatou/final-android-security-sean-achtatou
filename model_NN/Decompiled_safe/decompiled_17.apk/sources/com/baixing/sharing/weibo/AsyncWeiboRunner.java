package com.baixing.sharing.weibo;

public class AsyncWeiboRunner {
    public static void request(final String url, final WeiboParameters params, final String httpMethod, final RequestListener listener) {
        new Thread() {
            public void run() {
                try {
                    listener.onComplete(HttpManager.openUrl(url, httpMethod, params, params.getValue("pic")));
                } catch (WeiboException e) {
                    listener.onError(e);
                }
            }
        }.start();
    }
}
