package com.baixing.sharing;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.tencent.tauth.Constants;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class WeixinSharingManager extends BaseSharingManager {
    private static final String WX_APP_ID = "wx488dcea3081f1a46";
    private boolean friends = true;
    private Activity mActivity;
    private IWXAPI mApi;

    public WeixinSharingManager(Activity activity, boolean friends2) {
        this.mActivity = activity;
        this.friends = friends2;
        this.mApi = WXAPIFactory.createWXAPI(this.mActivity, WX_APP_ID, false);
        this.mApi.registerApp(WX_APP_ID);
    }

    public static boolean isWXInstalled(Context ctx) {
        IWXAPI api = WXAPIFactory.createWXAPI(ctx, WX_APP_ID, false);
        if (api == null) {
            return false;
        }
        return api.isWXAppInstalled();
    }

    public void share(Ad ad) {
        WeakReference<Bitmap> thumbnail;
        String title = "我用@百姓网 发布了：" + ad.getValueByKey(Constants.PARAM_TITLE) + ",请各位朋友们帮忙转发下哦~";
        String imgUrl = BaseSharingManager.getThumbnailUrl(ad);
        WXMediaMessage obj = new WXMediaMessage();
        String description = "";
        if (ad.getMetaData() != null) {
            for (int i = 0; i < ad.getMetaData().size(); i++) {
                String[] ms = ad.getMetaData().get(i).split(" ");
                if (ms != null && ms.length == 2) {
                    description = description + "，" + ms[1];
                }
            }
        }
        if (description.charAt(0) == 65292) {
            description = description.substring(1);
        }
        obj.description = description;
        obj.title = title;
        WXWebpageObject webObj = new WXWebpageObject();
        webObj.webpageUrl = ad.getValueByKey("link").replace(".baixing.com/", ".baixing.com/m/");
        obj.mediaObject = webObj;
        if (!(imgUrl == null || (thumbnail = GlobalDataManager.getInstance().getImageManager().getFromCache(imgUrl)) == null || thumbnail.get() == null)) {
            obj.setThumbImage(thumbnail.get());
        }
        sendWXRequest(obj, ad);
    }

    private void sendWXRequest(WXMediaMessage msg, Ad ad) {
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = msg;
        if (this.mApi.getWXAppSupportAPI() >= 553779201 && this.friends) {
            req.scene = 1;
        }
        this.mApi.sendReq(req);
    }

    public void release() {
    }

    private String convert2JSONString(Ad detail) {
        JSONObject obj = new JSONObject();
        Object[] keyAry = detail.getKeys().toArray();
        JSONArray jsonAry = new JSONArray();
        JSONObject subObj = new JSONObject();
        int i = 0;
        while (i < keyAry.length) {
            try {
                String key = (String) keyAry[i];
                String value = detail.getValueByKey((String) keyAry[i]);
                if (value == null) {
                    value = "";
                }
                subObj.put(key, value);
                i++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (detail.getImageList() != null) {
            JSONObject jsonImgs = new JSONObject();
            if (detail.getImageList().getBig() != null && !detail.getImageList().getBig().equals("")) {
                JSONArray imgAry = new JSONArray();
                String[] bigs = detail.getImageList().getBig().split(",");
                for (String put : bigs) {
                    imgAry.put(put);
                }
                jsonImgs.put("big", imgAry);
            }
            if (detail.getImageList().getSquare() != null && !detail.getImageList().getSquare().equals("")) {
                JSONArray imgAry2 = new JSONArray();
                String[] resizes = detail.getImageList().getSquare().split(",");
                for (String put2 : resizes) {
                    imgAry2.put(put2);
                }
                jsonImgs.put("resize180", imgAry2);
            }
            subObj.put("images", jsonImgs);
        }
        if (detail.getMetaData() != null && detail.getMetaData().size() > 0) {
            JSONArray jsonMetaAry = new JSONArray();
            for (int t = 0; t < detail.getMetaData().size(); t++) {
                jsonMetaAry.put(detail.getMetaData().get(t));
            }
            subObj.put("metaData", jsonMetaAry);
        }
        jsonAry.put(subObj);
        obj.put(ThirdpartyTransitActivity.Key_Data, jsonAry);
        obj.put("count", 1);
        return obj.toString();
    }

    public void auth() {
    }
}
