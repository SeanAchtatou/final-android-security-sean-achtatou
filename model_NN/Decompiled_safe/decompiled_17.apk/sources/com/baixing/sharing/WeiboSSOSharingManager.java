package com.baixing.sharing;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.sharing.weibo.Oauth2AccessToken;
import com.baixing.sharing.weibo.SsoHandler;
import com.baixing.sharing.weibo.Weibo;
import com.baixing.sharing.weibo.WeiboAuthListener;
import com.baixing.sharing.weibo.WeiboDialogError;
import com.baixing.sharing.weibo.WeiboException;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.tencent.tauth.Constants;
import java.io.Serializable;

public class WeiboSSOSharingManager extends BaseSharingManager {
    public static final String STRING_WEIBO_ACCESS_TOKEN = "weiboaccesstoken";
    static final String kWBBaixingAppKey = "3747392969";
    private static final String kWBBaixingAppSecret = "ff394d0df1cfc41c7d89ce934b5aa8fc";
    /* access modifiers changed from: private */
    public boolean isActive = true;
    /* access modifiers changed from: private */
    public BaseActivity mActivity;
    /* access modifiers changed from: private */
    public Ad mAd;
    private SsoHandler mSsoHandler;
    /* access modifiers changed from: private */
    public WeiboAccessTokenWrapper mToken;
    private Weibo mWeibo;
    private BroadcastReceiver msgListener;

    public static class WeiboAccessTokenWrapper implements Serializable {
        private static final long serialVersionUID = 973987291134876738L;
        private String expires_in;
        private String token;

        public String getToken() {
            return this.token;
        }

        public void setToken(String token2) {
            this.token = token2;
        }

        public String getExpires_in() {
            return this.expires_in;
        }

        public void setExpires_in(String expires_in2) {
            this.expires_in = expires_in2;
        }
    }

    public void doAuthorizeCallBack(int requestCode, int resultCode, Intent data) {
        if (this.mSsoHandler != null) {
            this.mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
        }
    }

    /* access modifiers changed from: private */
    public WeiboAccessTokenWrapper loadToken() {
        return (WeiboAccessTokenWrapper) Util.loadDataFromLocate(this.mActivity, STRING_WEIBO_ACCESS_TOKEN, WeiboAccessTokenWrapper.class);
    }

    public static void saveToken(Context context, WeiboAccessTokenWrapper token) {
        Util.saveDataToLocate(context, STRING_WEIBO_ACCESS_TOKEN, token);
    }

    public WeiboSSOSharingManager(BaseActivity activity) {
        this.mActivity = activity;
        this.mToken = loadToken();
    }

    class AuthDialogListener implements WeiboAuthListener {
        AuthDialogListener() {
        }

        public void onComplete(Bundle values) {
            String token = values.getString("access_token");
            String expires_in = values.getString("expires_in");
            Oauth2AccessToken accessToken = new Oauth2AccessToken(token, expires_in);
            if (accessToken.isSessionValid()) {
                WeiboAccessTokenWrapper wtw = new WeiboAccessTokenWrapper();
                wtw.setToken(token);
                wtw.setExpires_in(expires_in);
                WeiboSSOSharingManager.saveToken(WeiboSSOSharingManager.this.mActivity, wtw);
                WeiboSSOSharingManager.this.doShare2Weibo(accessToken);
            }
        }

        public void onError(WeiboDialogError e) {
            ViewUtil.showToast(WeiboSSOSharingManager.this.mActivity, e.getMessage(), false);
        }

        public void onCancel() {
        }

        public void onWeiboException(WeiboException e) {
            ViewUtil.showToast(WeiboSSOSharingManager.this.mActivity, e.getMessage(), false);
        }
    }

    public void auth() {
        try {
            Class<?> cls = Class.forName("com.baixing.sharing.weibo.SsoHandler");
            authSSO();
        } catch (ClassNotFoundException e) {
            authTraditional();
        }
    }

    public void share(Ad ad) {
        this.mAd = ad;
        if (!(this.mToken == null || this.mToken.getExpires_in() == null || this.mToken.getExpires_in().length() <= 0 || this.mToken.getToken() == null || this.mToken.getToken().length() <= 0)) {
            Oauth2AccessToken accessToken = new Oauth2AccessToken(this.mToken.getToken(), this.mToken.getExpires_in());
            if (accessToken.isSessionValid()) {
                doShare2Weibo(accessToken);
                return;
            }
        }
        auth();
    }

    private void authTraditional() {
        CookieSyncManager.createInstance(this.mActivity);
        CookieManager.getInstance().removeAllCookie();
        this.mWeibo = Weibo.getInstance("3747392969", "http://www.baixing.com");
        this.mWeibo.authorize(this.mActivity, new AuthDialogListener());
    }

    /* access modifiers changed from: private */
    public void unregisterListener() {
        if (this.msgListener != null) {
            this.mActivity.unregisterReceiver(this.msgListener);
        }
    }

    private void authSSO() {
        Intent intent = new Intent();
        intent.setClass(this.mActivity, WeiboManagerActivity.class);
        intent.putExtra("ad", this.mAd);
        this.mActivity.startActivity(intent);
        this.isActive = false;
        unregisterListener();
        this.msgListener = new BroadcastReceiver() {
            public void onReceive(Context outerContext, Intent outerIntent) {
                if (outerIntent.getAction().equals(CommonIntentAction.ACTION_BROADCAST_WEIBO_AUTH_DONE)) {
                    WeiboAccessTokenWrapper unused = WeiboSSOSharingManager.this.mToken = WeiboSSOSharingManager.this.loadToken();
                } else if (outerIntent.getAction().equals(CommonIntentAction.ACTION_BROADCAST_SHARE_BACK_TO_FRONT)) {
                    boolean unused2 = WeiboSSOSharingManager.this.isActive = true;
                }
                if (WeiboSSOSharingManager.this.mToken != null && WeiboSSOSharingManager.this.isActive) {
                    if (WeiboSSOSharingManager.this.mAd != null) {
                        WeiboSSOSharingManager.this.share(WeiboSSOSharingManager.this.mAd);
                    }
                    WeiboSSOSharingManager.this.unregisterListener();
                }
            }
        };
        this.mActivity.registerReceiver(this.msgListener, new IntentFilter(CommonIntentAction.ACTION_BROADCAST_WEIBO_AUTH_DONE));
        this.mActivity.registerReceiver(this.msgListener, new IntentFilter(CommonIntentAction.ACTION_BROADCAST_SHARE_BACK_TO_FRONT));
    }

    public void release() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.sharing.WeiboSharingFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* access modifiers changed from: private */
    public void doShare2Weibo(Oauth2AccessToken accessToken) {
        String imgUrl = BaseSharingManager.getThumbnailUrl(this.mAd);
        String imgPath = (imgUrl == null || imgUrl.length() == 0) ? "" : GlobalDataManager.getInstance().getImageManager().getFileInDiskCache(imgUrl);
        Bundle bundle = new Bundle();
        bundle.putString(BaseSharingFragment.EXTRA_WEIBO_CONTENT, "我用@百姓网 发布了\"" + this.mAd.getValueByKey(Constants.PARAM_TITLE) + "\"" + "麻烦朋友们帮忙转发一下～ " + this.mAd.getValueByKey("link"));
        if (imgPath == null || imgPath.length() == 0) {
            imgPath = "";
        }
        bundle.putString(BaseSharingFragment.EXTRA_PIC_URI, imgPath);
        bundle.putString(BaseSharingFragment.EXTRA_ACCESS_TOKEN, accessToken.getToken());
        bundle.putString(BaseSharingFragment.EXTRA_EXPIRES_IN, String.valueOf(accessToken.getExpiresTime()));
        this.mActivity.pushFragment((BaseFragment) new WeiboSharingFragment(), bundle, false);
    }
}
