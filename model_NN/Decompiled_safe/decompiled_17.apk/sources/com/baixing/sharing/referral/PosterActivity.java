package com.baixing.sharing.referral;

import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.BaseTabActivity;
import com.baixing.data.GlobalDataManager;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.quanleimu.activity.R;
import com.umeng.common.Log;
import java.lang.ref.WeakReference;

public class PosterActivity extends BaseTabActivity {
    private static final String TAG = PosterActivity.class.getSimpleName();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.sharing.referral.PosterFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public void onCreate(Bundle savedBundle) {
        PerformanceTracker.stamp(PerformEvent.Event.E_PostActivity_OnCreate_Begin);
        super.onCreate(savedBundle);
        if (GlobalDataManager.context == null || GlobalDataManager.context.get() == null) {
            GlobalDataManager.context = new WeakReference<>(this);
        }
        setContentView((int) R.layout.main_post);
        onSetRootView(findViewById(R.id.root));
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            Bundle bundle = new Bundle();
            Log.d(TAG, bundle.toString());
            pushFragment((BaseFragment) new PosterFragment(), bundle, false);
        }
        this.globalTabCtrl.attachView(findViewById(R.id.common_tab_layout), this);
        initTitleAction();
        PerformanceTracker.stamp(PerformEvent.Event.E_PostActivity_OnCreate_Leave);
    }

    /* access modifiers changed from: protected */
    public void onFragmentEmpty() {
        afterChange(2);
        finish();
    }

    public void handleBack() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(findViewById(R.id.contentLayout).getWindowToken(), 0);
        BaseFragment currentFragmet = getCurrentFragment();
        if (currentFragmet != null) {
            try {
                if (currentFragmet.handleBack()) {
                    return;
                }
                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    popFragment(currentFragmet);
                } else {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getTabIndex() {
        return 3;
    }
}
