package com.baixing.sharing.referral;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.BXLocation;
import com.baixing.entity.BXThumbnail;
import com.baixing.entity.PostGoodsBean;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.ErrorHandler;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.util.post.ImageUploader;
import com.baixing.util.post.PostCommonValues;
import com.baixing.util.post.PostLocationService;
import com.baixing.util.post.PostNetworkService;
import com.baixing.util.post.PostUtil;
import com.baixing.widget.VerifyFailDialog;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class PosterFragment extends BaseFragment implements View.OnClickListener, ImageUploader.Callback, BaseApiCommand.Callback {
    private static final int IMG_STATE_FAIL = 3;
    private static final int IMG_STATE_UPLOADED = 2;
    private static final int IMG_STATE_UPLOADING = 1;
    /* access modifiers changed from: private */
    public static boolean IsShowDlg = false;
    static final String KEY_IS_EDITPOST = "isEditPost";
    private static final int MSG_GEOCODING_TIMEOUT = 65553;
    private static final int MSG_GET_USER_BEGIN = 104;
    private static final int MSG_GET_USER_END = 105;
    public static final int MSG_HAIBAO_EXCEPTION = 103;
    public static final int MSG_HAIBAO_FAILED = 102;
    public static final int MSG_HAIBAO_SUCCEED = 101;
    private static final int MSG_IMAGE_STATE_CHANGE = 14;
    static final int MSG_POST_SUCCEED = -268435440;
    private static final int MSG_UPDATE_IMAGE_LIST = 13;
    private static final int NONE = 0;
    private static String addr;
    private static String images;
    private static String phoneNumber;
    private static String qrcode;
    protected List<String> bmpUrls = new ArrayList();
    protected String cityEnglishName = "";
    private BXLocation detailLocation = null;
    private boolean finishRightNow = false;
    protected boolean isNewPost = true;
    private long lastClickPostTime = 0;
    protected LinearLayout layout_txt;
    protected ArrayList<String> photoList = new ArrayList<>();
    private PostLocationService postLBS;
    private LinkedHashMap<String, PostGoodsBean> postList = new LinkedHashMap<>();
    private String qrCodeId = null;
    private Button scanQRCode;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != 0) {
            if (resultCode == 1) {
                this.finishRightNow = true;
                return;
            }
            Log.d("QLM", requestCode + "");
            if (resultCode == -1 && requestCode == 100) {
                this.qrCodeId = getQRCodeId(data.getExtras().getString("qrcode"));
                if (this.qrCodeId != null) {
                    this.scanQRCode.setText(getResources().getString(R.string.button_referral_poster_scan_success));
                    this.scanQRCode.setClickable(false);
                    this.scanQRCode.setBackgroundResource(R.drawable.btn_sms_on);
                    return;
                }
                return;
            }
            if (resultCode == -1) {
                this.photoList.clear();
                if (data.getExtras().containsKey(CommonIntentAction.EXTRA_IMAGE_LIST)) {
                    this.photoList.addAll(data.getStringArrayListExtra(CommonIntentAction.EXTRA_IMAGE_LIST));
                }
            }
            this.handler.sendEmptyMessage(MSG_UPDATE_IMAGE_LIST);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        PerformanceTracker.stamp(PerformEvent.Event.E_PGFrag_OnCreate_Start);
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.postList.putAll((HashMap) savedInstanceState.getSerializable("postList"));
            this.photoList.addAll((List) savedInstanceState.getSerializable("listUrl"));
        }
        doClearUpImages();
        this.postLBS = new PostLocationService(this.handler);
        this.cityEnglishName = GlobalDataManager.getInstance().getCityEnglishName();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("postList", this.postList);
        outState.putSerializable("listUrl", this.photoList);
    }

    /* access modifiers changed from: private */
    public void doClearUpImages() {
        this.photoList.clear();
        ImageUploader.getInstance().clearAll();
    }

    public boolean handleBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("退出发布？");
        builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PosterFragment.this.doClearUpImages();
                boolean unused = PosterFragment.this.finishFragment();
            }
        });
        builder.create().show();
        return true;
    }

    public void onResume() {
        super.onResume();
        if (this.finishRightNow) {
            this.finishRightNow = false;
            getView().post(new Runnable() {
                public void run() {
                    PosterFragment.this.doClearUpImages();
                    boolean unused = PosterFragment.this.finishFragment();
                }
            });
            return;
        }
        this.postLBS.start();
        if (getView() != null) {
            getView().post(new Runnable() {
                public void run() {
                    int titleWidth = PosterFragment.this.getView().findViewById(R.id.linearTop).getWidth();
                    int leftWidth = PosterFragment.this.getView().findViewById(R.id.left_action).getWidth();
                    int maxWidth = ((titleWidth - (leftWidth * 2)) - PosterFragment.this.getView().findViewById(R.id.imageView1).getWidth()) - (PosterFragment.this.getView().findViewById(R.id.ll_post_title).getPaddingRight() * 4);
                    if (maxWidth > 0) {
                        ((TextView) PosterFragment.this.getView().findViewById(R.id.tv_title_post)).setMaxWidth(maxWidth);
                    }
                }
            });
        }
    }

    public void onPause() {
        this.postLBS.stop();
        super.onPause();
    }

    public void onDestroy() {
        if (this.layout_txt != null) {
            for (int i = 0; i < this.layout_txt.getChildCount(); i++) {
                View child = this.layout_txt.getChildAt(i);
                if (child != null) {
                    child.setTag(PostCommonValues.HASH_CONTROL, null);
                }
            }
            View description = this.layout_txt.findViewById(R.id.img_description);
            if (description != null) {
                description.setTag(PostCommonValues.HASH_CONTROL, null);
            }
        }
        super.onDestroy();
    }

    public void onStackTop(boolean isBack) {
        if (isBack) {
            final ScrollView scroll = (ScrollView) getView().findViewById(R.id.goodscontent);
            scroll.post(new Runnable() {
                public void run() {
                    scroll.fullScroll(130);
                }
            });
        }
        if (this.isNewPost) {
            startImgSelDlg(1, "跳过\n拍照");
        }
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup v = (ViewGroup) inflater.inflate((int) R.layout.referralpostview, (ViewGroup) null);
        ((EditText) v.findViewById(R.id.edit_poster_location)).setOnFocusChangeListener(new PostUtil.BorderChangeListener(getActivity(), v.findViewById(R.id.locationinputlayout)));
        ((EditText) v.findViewById(R.id.edit_poster_contact)).setOnFocusChangeListener(new PostUtil.BorderChangeListener(getActivity(), v.findViewById(R.id.contactinputlayout)));
        Button button = (Button) v.findViewById(R.id.iv_post_finish);
        button.setOnClickListener(this);
        button.setText("立即免费发布");
        this.scanQRCode = (Button) v.findViewById(R.id.btn_qrcode_scan);
        this.scanQRCode.setOnClickListener(this);
        this.scanQRCode.setText(getResources().getString(R.string.button_referral_poster_qrcodescan));
        return v;
    }

    /* access modifiers changed from: protected */
    public void startImgSelDlg(int cancelResultCode, String finishActionLabel) {
        PerformanceTracker.stamp(PerformEvent.Event.E_Send_Camera_Bootup);
        Intent backIntent = new Intent();
        backIntent.setClass(getActivity(), getActivity().getClass());
        Intent goIntent = new Intent();
        goIntent.putExtra("extra.common.intent", backIntent);
        goIntent.setAction(CommonIntentAction.ACTION_IMAGE_CAPTURE);
        goIntent.putExtra("extra.image.reqcode", 1);
        goIntent.putStringArrayListExtra(CommonIntentAction.EXTRA_IMAGE_LIST, this.photoList);
        goIntent.putExtra(CommonIntentAction.EXTRA_FINISH_ACTION_LABEL, finishActionLabel);
        goIntent.putExtra("extra.common.finishCode", cancelResultCode);
        getActivity().startActivity(goIntent);
    }

    /* access modifiers changed from: protected */
    public String getCityEnglishName() {
        return this.cityEnglishName;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_post_image /*2131165198*/:
                startImgSelDlg(0, "完成");
                return;
            case R.id.postinputlayout /*2131165431*/:
                final View et2 = v.findViewById(R.id.postinput);
                if (et2 != null) {
                    et2.postDelayed(new Runnable() {
                        public void run() {
                            if (et2 != null) {
                                et2.requestFocus();
                                ((InputMethodManager) et2.getContext().getSystemService("input_method")).showSoftInput(et2, 1);
                            }
                        }
                    }, 100);
                    return;
                }
                return;
            case R.id.img_description /*2131165501*/:
                final View et = v.findViewById(R.id.description_input);
                if (et != null) {
                    et.postDelayed(new Runnable() {
                        public void run() {
                            if (et != null) {
                                ((InputMethodManager) et.getContext().getSystemService("input_method")).showSoftInput(et, 1);
                            }
                        }
                    }, 100);
                    return;
                }
                return;
            case R.id.iv_post_finish /*2131165506*/:
                if (Math.abs(System.currentTimeMillis() - this.lastClickPostTime) > 500) {
                    postAction();
                    this.lastClickPostTime = System.currentTimeMillis();
                    return;
                }
                return;
            case R.id.btn_qrcode_scan /*2131165534*/:
                Intent backIntent = new Intent();
                backIntent.setClass(getActivity(), getActivity().getClass());
                Intent goIntent = new Intent();
                goIntent.putExtra("extra.common.intent", backIntent);
                goIntent.setAction(CommonIntentAction.EXTRA_QRCODE_SCAN_REQUEST);
                goIntent.putExtra("extra.image.reqcode", 100);
                getActivity().startActivity(goIntent);
                return;
            case R.id.delete_btn /*2131165583*/:
                final String img = (String) v.getTag();
                showAlert(null, "是否删除该照片", new BaseFragment.DialogAction(R.string.yes) {
                    public void doAction() {
                        ImageUploader.getInstance().cancel(img);
                        if (PosterFragment.this.photoList.remove(img)) {
                            ViewGroup parent = (ViewGroup) PosterFragment.this.getView().findViewById(R.id.image_list_parent);
                            View v = PosterFragment.this.findImageViewByTag(img);
                            if (v != null) {
                                parent.removeView(v);
                            }
                            PosterFragment.this.showAddImageButton(parent, LayoutInflater.from(v.getContext()), true);
                        }
                    }
                }, null);
                return;
            default:
                return;
        }
    }

    private String getImgUrls() {
        this.bmpUrls.clear();
        this.bmpUrls.addAll(ImageUploader.getInstance().getServerUrlList());
        String images2 = "";
        if (this.bmpUrls != null) {
            for (int i = 0; i < this.bmpUrls.size(); i++) {
                String uploadUrl = PostNetworkService.getUploadedUrl(this.bmpUrls.get(i));
                if (!TextUtils.isEmpty(uploadUrl)) {
                    images2 = images2 + " " + uploadUrl + "#up";
                }
            }
            if (images2 != null && images2.length() > 0 && images2.charAt(0) == ',') {
                images2 = images2.substring(1);
            }
            if (images2 != null && images2.length() > 0) {
                return images2;
            }
        }
        ViewUtil.showToast(getActivity(), "请拍摄海报照片", false);
        return null;
    }

    private String getMobile() {
        String contentContact = ((EditText) getView().findViewById(R.id.edit_poster_contact)).getText().toString();
        if (TextUtils.isEmpty(contentContact)) {
            ViewUtil.showToast(getActivity(), "请输入店家手机", false);
            return null;
        } else if (Util.isValidMobile(contentContact.trim())) {
            return contentContact.trim();
        } else {
            ViewUtil.showToast(getActivity(), "请输入有效手机号", false);
            return null;
        }
    }

    private String getAddr() {
        String contentAddr = ((EditText) getView().findViewById(R.id.edit_poster_location)).getText().toString();
        if (!TextUtils.isEmpty(contentAddr)) {
            return contentAddr.trim();
        }
        ViewUtil.showToast(getActivity(), "请输入店家地址", false);
        return null;
    }

    private String getQRCodeID() {
        if (!TextUtils.isEmpty(this.qrCodeId)) {
            return this.qrCodeId;
        }
        ViewUtil.showToast(getActivity(), "请扫描二维码", false);
        return null;
    }

    private String getQRCodeId(String qrCodeStr) {
        int start = qrCodeStr.indexOf("id=") + "id=".length();
        if (start == "id=".length() - 1 || !ReferralUtil.isValidQRCodeID(qrCodeStr.substring(start))) {
            return null;
        }
        return qrCodeStr.substring(start);
    }

    private void postAction() {
        if (ImageUploader.getInstance().hasPendingJob()) {
            ViewUtil.showToast(getActivity(), "图片上传中", false);
            return;
        }
        images = getImgUrls();
        if (images != null) {
            addr = getAddr();
            if (addr != null) {
                phoneNumber = getMobile();
                if (phoneNumber != null) {
                    qrcode = getQRCodeID();
                    if (qrcode != null) {
                        startVerify();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void startVerify() {
        IsShowDlg = false;
        ApiParams params = new ApiParams();
        params.addParam("mobile", phoneNumber);
        BaseApiCommand.createCommand("User.sendMobileCode/", true, params).execute(getAppContext(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                Toast.makeText(GlobalDataManager.getInstance().getApplicationContext(), "验证码获取失败，请重试", 0).show();
            }

            public void onNetworkDone(String apiName, String responseData) {
                if (!PosterFragment.IsShowDlg) {
                    PosterFragment.this.showVerifyDlg();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showVerifyDlg() {
        IsShowDlg = true;
        if (getFragmentManager() != null) {
            VerifyFailDialog dlg = new VerifyFailDialog(new VerifyFailDialog.VerifyListener() {
                public void onReVerify(String mobile) {
                    PosterFragment.this.startVerify();
                }

                public void onSendVerifyCode(String code) {
                    PosterFragment.this.verifyAndPost(code);
                }
            });
            dlg.setCancelable(true);
            dlg.show(getFragmentManager(), (String) null);
        }
    }

    /* access modifiers changed from: private */
    public void verifyAndPost(String code) {
        IsShowDlg = false;
        ApiParams params = new ApiParams();
        params.addParam("mobile", phoneNumber);
        try {
            JSONObject response = new JSONObject(BaseApiCommand.createCommand("User.isMobileAvailable/", true, params).executeSync(getAppContext()));
            this.handler.obtainMessage(MSG_GET_USER_BEGIN).sendToTarget();
            if (response.getBoolean("result")) {
                ApiParams registerParams = new ApiParams();
                registerParams.addParam("mobile", phoneNumber);
                registerParams.addParam("mobile_code", code);
                registerParams.addParam("password", ReferralPost.getPasswd(phoneNumber));
                BaseApiCommand.createCommand("User.register/", true, registerParams).execute(getAppContext(), this);
                return;
            }
            ApiParams loginParams = new ApiParams();
            loginParams.addParam("type", "mobile_code");
            loginParams.addParam("identity", phoneNumber);
            loginParams.addParam("password", code);
            BaseApiCommand.createCommand("User.login/", true, loginParams).execute(getAppContext(), this);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getAppContext(), "手机验证解析失败，请重试", 0).show();
        }
    }

    private void buildFixedPostLayout(HashMap<String, PostGoodsBean> pl) {
        if (pl != null && pl.size() != 0) {
            HashMap<String, PostGoodsBean> pm = new HashMap<>();
            Object[] postListKeySetArray = pl.keySet().toArray();
            for (int i = 0; i < pl.size(); i++) {
                int j = 0;
                while (true) {
                    if (j >= PostCommonValues.fixedItemNames.length) {
                        break;
                    }
                    PostGoodsBean bean = pl.get(postListKeySetArray[i]);
                    if (bean.getName().equals(PostCommonValues.fixedItemNames[j])) {
                        pm.put(PostCommonValues.fixedItemNames[j], bean);
                        break;
                    }
                    j++;
                }
            }
            updateImageInfo(this.layout_txt);
        }
    }

    /* access modifiers changed from: protected */
    public void setPhoneAndAddrLeftIcon() {
        int resId;
        int resId2;
        Button pBtn = getView() == null ? null : (Button) getView().findViewById(R.id.btn_contact);
        Button aBtn = getView() == null ? null : (Button) getView().findViewById(R.id.btn_address);
        if (aBtn != null && pBtn != null) {
            String text = pBtn.getText().toString();
            if (text == null || text.length() <= 0) {
                resId = R.drawable.icon_post_call_disable;
            } else {
                resId = R.drawable.icon_post_call;
            }
            BitmapDrawable bd = new BitmapDrawable(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(resId));
            bd.setBounds(0, 0, 45, 45);
            pBtn.setCompoundDrawables(bd, null, null, null);
            String text2 = aBtn.getText().toString();
            if (text2 == null || text2.length() <= 0) {
                resId2 = R.drawable.icon_location_disable;
            } else {
                resId2 = R.drawable.icon_location;
            }
            BitmapDrawable bd2 = new BitmapDrawable(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(resId2));
            bd2.setBounds(0, 0, 45, 45);
            aBtn.setCompoundDrawables(bd2, null, null, null);
        }
    }

    /* access modifiers changed from: protected */
    public void buildPostLayout(HashMap<String, PostGoodsBean> pl) {
        getView().findViewById(R.id.goodscontent).setVisibility(0);
        getView().findViewById(R.id.networkErrorView).setVisibility(8);
        reCreateTitle();
        refreshHeader();
        if (pl != null && pl.size() != 0) {
            buildFixedPostLayout(pl);
        }
    }

    /* access modifiers changed from: protected */
    public final void updateImageInfo(View rootView) {
        ViewGroup list;
        if (rootView != null && (list = (ViewGroup) rootView.findViewById(R.id.image_list_parent)) != null) {
            list.removeAllViews();
            LayoutInflater inflator = LayoutInflater.from(rootView.getContext());
            Iterator i$ = this.photoList.iterator();
            while (i$.hasNext()) {
                String img = i$.next();
                View imgParent = inflator.inflate((int) R.layout.post_image, (ViewGroup) null);
                imgParent.setTag(img);
                imgParent.setOnClickListener(this);
                imgParent.setId(R.id.delete_btn);
                int margin = (int) getResources().getDimension(R.dimen.post_img_margin);
                int wh = (int) getResources().getDimension(R.dimen.post_img_size);
                ViewGroup.MarginLayoutParams layParams = new ViewGroup.MarginLayoutParams(wh + margin, (margin * 2) + wh);
                layParams.setMargins(0, margin, margin, margin);
                list.addView(imgParent, layParams);
                ImageUploader.getInstance().registerCallback(img, this);
            }
            if (this.photoList == null || this.photoList.size() < 6) {
                showAddImageButton(list, inflator, false);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showAddImageButton(ViewGroup parent, LayoutInflater inflator, boolean scroolNow) {
        long j = 0;
        try {
            View addBtn = parent.getChildAt(parent.getChildCount() - 1);
            if (addBtn == null || addBtn.getId() != R.id.add_post_image) {
                View addBtn2 = inflator.inflate((int) R.layout.post_image, (ViewGroup) null);
                ((ImageView) addBtn2.findViewById(R.id.result_image)).setImageResource(R.drawable.btn_add_picture);
                addBtn2.setOnClickListener(this);
                addBtn2.setId(R.id.add_post_image);
                int margin = (int) getResources().getDimension(R.dimen.post_img_margin);
                int wh = (int) getResources().getDimension(R.dimen.post_img_size);
                ViewGroup.MarginLayoutParams layParams = new ViewGroup.MarginLayoutParams(wh, (margin * 2) + wh);
                layParams.setMargins(0, margin, 0, margin);
                parent.addView(addBtn2, layParams);
                if (!scroolNow) {
                    j = 300;
                }
                return;
            }
            final HorizontalScrollView hs = (HorizontalScrollView) parent.getParent();
            hs.postDelayed(new Runnable() {
                public void run() {
                    hs.scrollBy(1000, 0);
                }
            }, scroolNow ? j : 300);
        } finally {
            final HorizontalScrollView hs2 = (HorizontalScrollView) parent.getParent();
            AnonymousClass11 r10 = new Runnable() {
                public void run() {
                    hs2.scrollBy(1000, 0);
                }
            };
            if (!scroolNow) {
                j = 300;
            }
            hs2.postDelayed(r10, j);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [?, ?, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        int i = 0;
        if (msg.what != 66064) {
            hideProgress();
        }
        switch (msg.what) {
            case -9:
                hideProgress();
                ErrorHandler.getInstance().handleMessage(msg);
                return;
            case MSG_UPDATE_IMAGE_LIST /*13*/:
                updateImageInfo(rootView);
                return;
            case 14:
                BXThumbnail img = (BXThumbnail) msg.obj;
                int state = msg.arg1;
                ViewGroup imgParent = (ViewGroup) findImageViewByTag(img.getLocalPath());
                if (imgParent != null) {
                    ImageView imgView = (ImageView) imgParent.findViewById(R.id.result_image);
                    View loadingState = imgParent.findViewById(R.id.loading_status);
                    if (state == 1 || state == 2) {
                        imgView.setImageBitmap(img.getThumbnail());
                        if (state != 1) {
                            i = 4;
                        }
                        loadingState.setVisibility(i);
                        return;
                    }
                    imgView.setImageResource(R.drawable.icon_load_fail);
                    loadingState.setVisibility(8);
                    return;
                }
                return;
            case 101:
                Toast.makeText(activity.getApplicationContext(), "海报推广成功！", 0).show();
                finishFragment();
                return;
            case 102:
                Toast.makeText(activity.getApplicationContext(), "推广记录保存失败", 0).show();
                return;
            case MSG_HAIBAO_EXCEPTION /*103*/:
                Toast.makeText(activity.getApplicationContext(), "未知错误，请先确认网络已连接", 0).show();
                return;
            case MSG_GET_USER_BEGIN /*104*/:
                showProgress("请稍候", "正在校验发送信息", true);
                return;
            case MSG_GET_USER_END /*105*/:
                hideProgress();
                return;
            case PostCommonValues.MSG_GEOCODING_FETCHED /*65552*/:
            case MSG_GEOCODING_TIMEOUT /*65553*/:
                showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_waiting, false);
                this.handler.removeMessages(MSG_GEOCODING_TIMEOUT);
                this.handler.removeMessages(PostCommonValues.MSG_GEOCODING_FETCHED);
                return;
            case PostCommonValues.MSG_GPS_LOC_FETCHED /*66064*/:
                this.detailLocation = (BXLocation) msg.obj;
                return;
            default:
                return;
        }
    }

    public void onStart() {
        super.onStart();
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_leftActionHint = "返回";
        title.m_leftActionImage = R.drawable.icon_close;
        title.m_titleControls = LayoutInflater.from(getActivity()).inflate((int) R.layout.title_post, (ViewGroup) null);
        title.m_titleControls.setClickable(false);
        ((TextView) title.m_titleControls.findViewById(R.id.ll_post_title).findViewById(R.id.tv_title_post)).setText("海报推广");
        ((ImageView) title.m_titleControls.findViewById(R.id.ll_post_title).findViewById(R.id.imageView1)).setVisibility(8);
    }

    public boolean hasGlobalTab() {
        return false;
    }

    public void onUploadDone(String imagePath, String serverUrl, Bitmap thumbnail) {
        this.handler.sendMessage(this.handler.obtainMessage(14, 2, 0, BXThumbnail.createThumbnail(imagePath, thumbnail)));
    }

    public void onUploading(String imagePath, Bitmap thumbnail) {
        this.handler.sendMessage(this.handler.obtainMessage(14, 1, 0, BXThumbnail.createThumbnail(imagePath, thumbnail)));
    }

    public void onUploadFail(String imagePath, Bitmap thumbnail) {
        this.handler.sendMessage(this.handler.obtainMessage(14, 3, 0, BXThumbnail.createThumbnail(imagePath, thumbnail)));
    }

    /* access modifiers changed from: private */
    public View findImageViewByTag(String imagePath) {
        ViewGroup root = (ViewGroup) getView().findViewById(R.id.image_list_parent);
        if (root == null) {
            return null;
        }
        int c = root.getChildCount();
        for (int i = 0; i < c; i++) {
            View child = root.getChildAt(i);
            if (imagePath.equals(child.getTag())) {
                return child;
            }
        }
        return null;
    }

    public void onNetworkDone(String apiName, String responseData) {
        if (apiName.equals("User.login/") || apiName.equals("User.register/")) {
            try {
                ReferralNetwork.getInstance().savePromoStore(new JSONObject(responseData).getJSONObject("result").getJSONObject("user").getString(LocaleUtil.INDONESIAN).substring(1), addr, images, qrcode, this.detailLocation, this.handler);
                this.handler.obtainMessage(MSG_GET_USER_END).sendToTarget();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onNetworkFail(String apiName, ApiError error) {
        if ((apiName.equals("User.login/") || apiName.equals("User.register/")) && !IsShowDlg) {
            showVerifyDlg();
        }
    }
}
