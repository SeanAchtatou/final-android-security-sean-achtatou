package com.baixing.sharing.referral;

import android.content.Intent;

public interface ReferralCallback {
    void doAction(Intent intent);
}
