package com.baixing.sharing.referral;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.baixing.view.fragment.LoginFragment;
import com.baixing.view.fragment.WebViewFragment;
import com.baixing.widget.EditUsernameDialogFragment;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class AppShareFragment extends BaseFragment implements View.OnClickListener, EditUsernameDialogFragment.ICallback, Observer {
    private String APP_DOWN_BASE = "http://baixing.com/pages/d/?id=";
    private Button appDetailButton;
    private Button bluetoothButton;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String promoterId;
    private ImageView qrcodeImageView;
    private TextView txtLoginShare;
    /* access modifiers changed from: private */
    public String userToken;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = getString(R.string.title_referral_promote);
        title.m_leftActionHint = "完成";
    }

    public boolean hasGlobalTab() {
        return true;
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View referralmain = inflater.inflate((int) R.layout.referral_share, (ViewGroup) null);
        this.context = GlobalDataManager.getInstance().getApplicationContext();
        this.txtLoginShare = (TextView) referralmain.findViewById(R.id.txt_login_to_share);
        int size = (Util.getWidthByContext(this.context) * 2) / 3;
        this.txtLoginShare.setWidth(size);
        this.txtLoginShare.setHeight(size);
        this.txtLoginShare.setBackgroundColor(-7829368);
        this.txtLoginShare.getBackground().setAlpha(208);
        this.txtLoginShare.getPaint().setFakeBoldText(true);
        this.txtLoginShare.setClickable(true);
        this.txtLoginShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppShareFragment.this.pushFragment(new LoginFragment(), AppShareFragment.this.createArguments("登录", ""));
            }
        });
        this.appDetailButton = (Button) referralmain.findViewById(R.id.btn_referral_share_detail);
        this.appDetailButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PARAM_TITLE, AppShareFragment.this.getString(R.string.button_referral_detail_app));
                bundle.putString(Constants.PARAM_URL, "http://www.baixing.com/arch/promoRedirect/?userId=" + AppShareFragment.this.promoterId + "&token=" + AppShareFragment.this.userToken + "&taskType=" + ReferralUtil.TASK_APP);
                bundle.putBoolean("isGet", false);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.APP_SHARE_DETAIL).end();
                AppShareFragment.this.pushFragment(new WebViewFragment(), bundle);
            }
        });
        this.qrcodeImageView = (ImageView) referralmain.findViewById(R.id.img_referral_qrcode);
        UserBean curUser = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (curUser == null || !Util.isValidMobile(curUser.getPhone())) {
            this.promoterId = Util.getDeviceUdid(this.context);
        } else {
            this.promoterId = curUser.getId().substring(1);
        }
        this.qrcodeImageView.setImageBitmap(ReferralUtil.getQRCodeBitmap(GlobalDataManager.getInstance().getApplicationContext(), this.APP_DOWN_BASE + this.promoterId));
        this.bluetoothButton = (Button) referralmain.findViewById(R.id.btn_referral_bluetooth);
        return referralmain;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView textView = (TextView) view.findViewById(R.id.txt_app_share_tips);
        String promoInfo = (String) Util.loadDataFromLocate(getAppContext(), ReferralUtil.LOCATE_EVENT_INFO, String.class);
        final String promoUrl = (String) Util.loadDataFromLocate(getAppContext(), ReferralUtil.LOCATE_EVENT_URL, String.class);
        if (TextUtils.isEmpty(promoInfo) || TextUtils.isEmpty(promoUrl)) {
            textView.setVisibility(8);
            this.txtLoginShare.setVisibility(8);
            this.appDetailButton.setVisibility(8);
            return;
        }
        textView.setText(Html.fromHtml("<u>" + promoInfo + "</u>"));
        textView.setTextColor(-16776961);
        textView.setVisibility(0);
        textView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PARAM_TITLE, AppShareFragment.this.getString(R.string.title_referral_intro));
                bundle.putString(Constants.PARAM_URL, promoUrl);
                bundle.putBoolean("isGet", true);
                AppShareFragment.this.pushFragment(new WebViewFragment(), bundle);
            }
        });
        this.txtLoginShare.setVisibility(0);
        this.appDetailButton.setVisibility(0);
    }

    public void onResume() {
        super.onResume();
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.SHAREAPP).end();
        loginToDisplayInfo();
    }

    private void loginToDisplayInfo() {
        String promoUrl = (String) Util.loadDataFromLocate(getAppContext(), ReferralUtil.LOCATE_EVENT_URL, String.class);
        if (TextUtils.isEmpty((String) Util.loadDataFromLocate(getAppContext(), ReferralUtil.LOCATE_EVENT_INFO, String.class)) || TextUtils.isEmpty(promoUrl) || GlobalDataManager.getInstance().getAccountManager().isUserLogin()) {
            this.txtLoginShare.setVisibility(8);
            this.bluetoothButton.setEnabled(true);
            this.appDetailButton.setEnabled(true);
            this.bluetoothButton.setBackgroundResource(R.drawable.post_finish_btn);
            this.appDetailButton.setBackgroundResource(R.drawable.post_finish_btn);
            UserBean curUser = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
            if (curUser == null || !Util.isValidMobile(curUser.getPhone())) {
                this.promoterId = Util.getDeviceUdid(this.context);
            } else {
                this.promoterId = curUser.getId().substring(1);
                this.userToken = GlobalDataManager.getInstance().getLoginUserToken();
            }
            this.qrcodeImageView.setImageBitmap(ReferralUtil.getQRCodeBitmap(GlobalDataManager.getInstance().getApplicationContext(), this.APP_DOWN_BASE + this.promoterId));
            this.bluetoothButton.setOnClickListener(new BtnBluetoothOnClickListener());
            return;
        }
        this.bluetoothButton.setEnabled(false);
        this.bluetoothButton.setBackgroundResource(R.drawable.btn_sms_on);
        this.appDetailButton.setVisibility(8);
        this.txtLoginShare.setVisibility(0);
    }

    class BtnBluetoothOnClickListener implements View.OnClickListener {
        BtnBluetoothOnClickListener() {
        }

        public void onClick(View v) {
            String version = Util.getVersion(AppShareFragment.this.context);
            if (TextUtils.isEmpty(version)) {
                version = "3.5";
            }
            String apkName = Environment.getExternalStorageDirectory().getPath() + "/baixing-" + version + "-" + AppShareFragment.this.promoterId + "-.apk";
            List<ApplicationInfo> appinfo_list = AppShareFragment.this.context.getPackageManager().getInstalledApplications(0);
            String originPath = null;
            for (int x = 0; x < appinfo_list.size(); x++) {
                if (appinfo_list.get(x).publicSourceDir.contains("com.quanleimu.activity")) {
                    originPath = appinfo_list.get(x).publicSourceDir;
                    Log.d("QLM", "originPath: " + originPath);
                }
            }
            if (originPath != null) {
                try {
                    InputStream is = new FileInputStream(new File(originPath));
                    int length = is.available();
                    Log.d("QLM", "is.available: " + length);
                    if (length > 0) {
                        FileOutputStream fos = new FileOutputStream(new File(apkName));
                        byte[] buffer = new byte[length];
                        while (true) {
                            length = is.read(buffer, 0, length);
                            if (length == -1) {
                                break;
                            }
                            fos.write(buffer, 0, length);
                        }
                        fos.close();
                    }
                    is.close();
                } catch (IOException e) {
                    Log.e("QLM", e.getMessage());
                    e.printStackTrace();
                    Toast.makeText(AppShareFragment.this.context, "写SD卡失败", 0).show();
                }
                Intent intent = new Intent();
                intent.setAction("android.intent.action.SEND");
                intent.setType("*/*");
                intent.setFlags(268435456);
                intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(apkName)));
                try {
                    intent.setClassName("com.android.bluetooth", "com.android.bluetooth.opp.BluetoothOppLauncherActivity");
                    AppShareFragment.this.startActivity(intent);
                } catch (ActivityNotFoundException e2) {
                    intent.setClassName("com.mediatek.bluetooth", "com.mediatek.bluetooth.BluetoothShareGatewayActivity");
                    AppShareFragment.this.startActivity(intent);
                }
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SHARE_BY_BLUETOOTH).end();
                return;
            }
            Log.e("QLM", "No apk found");
            Toast.makeText(AppShareFragment.this.context, "找不到百姓网安装包", 0).show();
        }
    }

    public void update(Observable observable, Object data) {
    }

    public void onEditSucced(String newUserName) {
    }

    public void onClick(View v) {
    }
}
