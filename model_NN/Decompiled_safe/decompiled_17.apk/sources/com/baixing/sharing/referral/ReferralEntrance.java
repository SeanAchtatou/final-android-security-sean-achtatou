package com.baixing.sharing.referral;

import android.content.Context;
import android.os.Bundle;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.widget.CustomizeGridView;
import com.quanleimu.activity.R;
import java.util.List;

public class ReferralEntrance {
    private static ReferralEntrance instance = null;

    public static ReferralEntrance getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new ReferralEntrance();
        return instance;
    }

    public void addAppShareGrid(Context context, List<CustomizeGridView.GridInfo> gitems) {
        CustomizeGridView.GridInfo gi = new CustomizeGridView.GridInfo();
        gi.img = GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.icon_category_referral);
        gi.text = context.getString(R.string.title_referral_promote);
        gitems.add(gi);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.sharing.referral.AppShareFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public void pushFragment(BaseFragment fragement, CustomizeGridView.GridInfo info) {
        BaseActivity activity = (BaseActivity) fragement.getActivity();
        if (info.text.equals(activity.getString(R.string.title_referral_promote))) {
            activity.pushFragment((BaseFragment) new AppShareFragment(), new Bundle(), false);
        }
    }
}
