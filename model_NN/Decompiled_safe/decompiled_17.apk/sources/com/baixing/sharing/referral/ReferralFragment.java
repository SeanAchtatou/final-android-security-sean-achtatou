package com.baixing.sharing.referral;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.PostActivity;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.util.Util;
import com.baixing.view.fragment.WebViewFragment;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.util.Observable;
import java.util.Observer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReferralFragment extends BaseFragment implements View.OnClickListener, Observer {
    private static Context context;
    View referralmain = null;
    String token;
    String userId;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
        context = GlobalDataManager.getInstance().getApplicationContext();
        UserBean curUser = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (curUser != null && Util.isValidMobile(curUser.getPhone())) {
            this.userId = curUser.getId().substring(1);
            this.token = GlobalDataManager.getInstance().getLoginUserToken();
        }
        ReferralNetwork.getInstance().addObserver(this);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = getString(R.string.title_referral_setting);
        title.m_leftActionHint = "完成";
    }

    public boolean hasGlobalTab() {
        return true;
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.referralmain = inflater.inflate((int) R.layout.referral_info, (ViewGroup) null);
        ((Button) this.referralmain.findViewById(R.id.btn_referral_post)).setOnClickListener(this);
        ((Button) this.referralmain.findViewById(R.id.btn_referral_haibao)).setOnClickListener(this);
        ((Button) this.referralmain.findViewById(R.id.btn_referral_posts_detail)).setOnClickListener(this);
        ((Button) this.referralmain.findViewById(R.id.btn_referral_haibao_detail)).setOnClickListener(this);
        return this.referralmain;
    }

    public int[] excludedOptionMenus() {
        return new int[]{1, 2, 4, 5, 3};
    }

    public void update(Observable observable, Object data) {
        if (this.referralmain != null && (data instanceof String)) {
            displayInfo(this.referralmain, (String) data);
        }
    }

    private void displayInfo(View view, String jsonData) {
        try {
            JSONObject jsonInfo = new JSONObject(jsonData);
            JSONArray jSONArray = jsonInfo.getJSONArray("info");
            jsonInfo.getJSONArray("app");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.btn_referral_post /*2131165520*/:
                intent.setClass(context, PostActivity.class);
                break;
            case R.id.btn_referral_haibao /*2131165521*/:
                intent.setClass(context, PosterActivity.class);
                break;
            case R.id.btn_referral_posts_detail /*2131165522*/:
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PARAM_TITLE, getString(R.string.button_referral_detail_post));
                bundle.putString(Constants.PARAM_URL, "http://www.baixing.com/arch/promoRedirect/?userId=" + this.userId + "&token=" + this.token + "&taskType=" + ReferralUtil.TASK_AD);
                bundle.putBoolean("isGet", false);
                pushFragment(new WebViewFragment(), bundle);
                return;
            case R.id.btn_referral_haibao_detail /*2131165523*/:
                Bundle bundle2 = new Bundle();
                bundle2.putString(Constants.PARAM_TITLE, getString(R.string.button_referral_detail_haibao));
                bundle2.putString(Constants.PARAM_URL, "http://www.baixing.com/arch/promoRedirect/?userId=" + this.userId + "&token=" + this.token + "&taskType=" + ReferralUtil.TASK_STORE);
                bundle2.putBoolean("isGet", false);
                pushFragment(new WebViewFragment(), bundle2);
                return;
        }
        intent.putExtra("isReferral", true);
        intent.addFlags(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
        intent.addFlags(4194304);
        intent.addFlags(65536);
        context.startActivity(intent);
    }
}
