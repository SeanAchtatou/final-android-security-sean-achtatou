package com.baixing.sharing.referral;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.BXLocation;
import com.baixing.network.api.ApiConfiguration;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import org.json.JSONException;
import org.json.JSONObject;

public class ReferralNetwork extends Observable {
    private static final String TAG = ReferralNetwork.class.getSimpleName();
    private static ReferralNetwork instance = null;

    public static ReferralNetwork getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new ReferralNetwork();
        return instance;
    }

    public void savePromoStore(String businessUserId, String businessAddr, String imageUrls, String qrCodeID, BXLocation detailLocation, Handler handler) {
        Context context = GlobalDataManager.getInstance().getApplicationContext();
        String promoterUserId = GlobalDataManager.getInstance().getAccountManager().getCurrentUser().getId().substring(1);
        String latlng = getLatLng(detailLocation);
        String gpsAddress = getGPSAddr(detailLocation);
        ApiParams params = new ApiParams();
        params.addParam("promoterUserId", promoterUserId);
        params.addParam("images", imageUrls);
        params.addParam("ownerId", businessUserId);
        params.addParam("address", businessAddr);
        params.addParam("latlng", latlng);
        params.addParam("gpsAddr", gpsAddress);
        params.addParam("qrcodeId", qrCodeID);
        ApiConfiguration.getHost();
        String storeId = getStoreId(BaseApiCommand.createCommand("Promo.saveStore/", false, params).executeSync(context));
        if (storeId != null) {
            HashMap<String, String> attrs = new HashMap<>();
            attrs.put("storeId", storeId);
            if (getInstance().savePromoTask(ReferralUtil.TASK_STORE, promoterUserId, businessUserId, null, null, latlng, attrs)) {
                handler.sendEmptyMessage(101);
            } else {
                handler.sendEmptyMessage(102);
            }
        } else {
            handler.sendEmptyMessage(PosterFragment.MSG_HAIBAO_EXCEPTION);
        }
    }

    private String getStoreId(String jsonResult) {
        JSONObject result;
        JSONObject error;
        String code;
        try {
            JSONObject obj = new JSONObject(jsonResult);
            if (obj == null || (error = (result = obj.getJSONObject("result")).getJSONObject("error")) == null || (code = error.getString("code")) == null || !code.equals("0")) {
                return null;
            }
            return result.getString(LocaleUtil.INDONESIAN);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getLatLng(BXLocation location) {
        if (location == null) {
            return "";
        }
        try {
            return "(" + location.fLat + "," + location.fLon + ")";
        } catch (Exception e) {
            return "";
        }
    }

    private String getGPSAddr(BXLocation location) {
        if (location == null) {
            return "";
        }
        String address = (location.detailAddress == null || location.detailAddress.equals("")) ? (location.subCityName == null || location.subCityName.equals("")) ? "" : location.subCityName : location.detailAddress;
        return TextUtils.isEmpty(address) ? "" : address;
    }

    public boolean savePromoTask(String taskType, String parentUserId, String childUserId, String childUdid, String IP, String latlng, Map<String, String> attrs) {
        SharedPreferences preferences = GlobalDataManager.getInstance().getApplicationContext().getSharedPreferences(ReferralUtil.REFERRAL_STATUS, 0);
        if (taskType.equals(ReferralUtil.TASK_APP)) {
            if (TextUtils.isEmpty(childUserId) && preferences.contains(ReferralUtil.ACTIVATE_KEY)) {
                return false;
            }
            if (!TextUtils.isEmpty(childUserId) && preferences.getBoolean(ReferralUtil.ACTIVATE_KEY, false)) {
                return false;
            }
        }
        ApiParams taskParams = new ApiParams();
        taskParams.addParam("parentUserId", parentUserId);
        taskParams.addParam("taskType", taskType);
        if (childUdid != null) {
            taskParams.addParam("childUdid", childUdid);
        }
        if (childUserId != null) {
            taskParams.addParam("childUserId", childUserId);
        }
        if (IP != null) {
            taskParams.addParam("IP", IP);
        }
        if (latlng != null) {
            taskParams.addParam("latlng", latlng);
        }
        if (taskType.equals(ReferralUtil.TASK_APP)) {
            if (attrs == null) {
                attrs = new HashMap<>();
            }
            attrs.put("appPromo", String.valueOf(preferences.getInt(ReferralUtil.SHARETYPE_KEY, 0)));
        }
        if (!(attrs == null || attrs.size() == 0)) {
            JSONObject json = new JSONObject();
            for (Map.Entry<String, String> entry : attrs.entrySet()) {
                try {
                    json.put((String) entry.getKey(), entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            taskParams.addParam("attr", json.toString());
            Log.d("savePoster", taskParams.toString());
        }
        try {
            JSONObject obj = new JSONObject(BaseApiCommand.createCommand("Promo.saveTask/", false, taskParams).executeSync(GlobalDataManager.getInstance().getApplicationContext()));
            if (obj != null) {
                JSONObject error = obj.getJSONObject("result").getJSONObject("error");
                Log.d("savePoster", error.toString());
                if (error != null) {
                    String code = error.getString("code");
                    if (taskType.equals(ReferralUtil.TASK_APP)) {
                        SharedPreferences.Editor editor = preferences.edit();
                        if (TextUtils.isEmpty(childUserId)) {
                            editor.putBoolean(ReferralUtil.ACTIVATE_KEY, false);
                        } else {
                            editor.putBoolean(ReferralUtil.ACTIVATE_KEY, true);
                        }
                        editor.commit();
                    }
                    if (code != null && code.equals("0")) {
                        return true;
                    }
                }
            }
            return false;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
