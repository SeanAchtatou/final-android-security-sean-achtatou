package com.baixing.sharing.referral;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.network.NetworkUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.Util;
import com.baixing.util.post.PostNetworkService;
import com.baixing.widget.VerifyFailDialog;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class ReferralPost implements ReferralCallback, BaseApiCommand.Callback {
    /* access modifiers changed from: private */
    public static boolean IsShowDlg;
    private static final String TAG = ReferralPost.class.getSimpleName();
    private static FragmentManager fragmentManager = null;
    private static Handler handler = null;
    private static ReferralPost instance = null;
    private static String phoneNumber;
    private static PostNetworkService postNetworkService = null;
    private String businessId = null;
    private String promoterId = null;

    public static ReferralPost getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new ReferralPost();
        return instance;
    }

    public static void Config(FragmentManager fragmentManager2, PostNetworkService postNetworkService2, Handler handler2) {
        fragmentManager = fragmentManager2;
        postNetworkService = postNetworkService2;
        handler = handler2;
    }

    public static String getPasswd(String phone) {
        return NetworkUtil.getMD5(phone.substring(3, 10));
    }

    public void postNewAd(String phone) {
        phoneNumber = phone;
        IsShowDlg = false;
        ApiParams params = new ApiParams();
        params.addParam("mobile", phoneNumber);
        BaseApiCommand.createCommand("User.sendMobileCode/", true, params).execute(GlobalDataManager.getInstance().getApplicationContext(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
            }

            public void onNetworkDone(String apiName, String responseData) {
                if (!ReferralPost.IsShowDlg) {
                    ReferralPost.this.showVerifyDlg();
                }
            }
        });
    }

    private void helpSendPost(String userId, String mobile, String token) {
        handler.obtainMessage(20).sendToTarget();
        UserBean otherBean = new UserBean();
        otherBean.setId(userId);
        otherBean.setPhone(mobile);
        this.businessId = userId;
        UserBean ownerBean = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (ownerBean == null) {
            Log.e(TAG, "promoter didn't login");
            return;
        }
        this.promoterId = ownerBean.getId();
        String promoterToken = GlobalDataManager.getInstance().getLoginUserToken();
        GlobalDataManager.getInstance().getAccountManager().logout();
        Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "user", otherBean);
        GlobalDataManager.getInstance().setLoginUserToken(token);
        postNetworkService.doRegisterAndVerify(phoneNumber);
        GlobalDataManager.getInstance().getAccountManager().logout();
        Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "user", ownerBean);
        GlobalDataManager.getInstance().setLoginUserToken(promoterToken);
    }

    /* access modifiers changed from: private */
    public void showVerifyDlg() {
        IsShowDlg = true;
        if (fragmentManager != null) {
            VerifyFailDialog dlg = new VerifyFailDialog(new VerifyFailDialog.VerifyListener() {
                public void onReVerify(String mobile) {
                    ReferralPost.this.postNewAd(mobile);
                }

                public void onSendVerifyCode(String code) {
                    ReferralPost.this.verifyAndPost(code);
                }
            });
            dlg.setCancelable(true);
            dlg.show(fragmentManager, (String) null);
        }
    }

    /* access modifiers changed from: private */
    public void verifyAndPost(String code) {
        IsShowDlg = false;
        handler.obtainMessage(19).sendToTarget();
        ApiParams params = new ApiParams();
        params.addParam("mobile", phoneNumber);
        try {
            if (new JSONObject(BaseApiCommand.createCommand("User.isMobileAvailable/", true, params).executeSync(GlobalDataManager.getInstance().getApplicationContext())).getBoolean("result")) {
                ApiParams registerParams = new ApiParams();
                registerParams.addParam("mobile", phoneNumber);
                registerParams.addParam("mobile_code", code);
                registerParams.addParam("password", getPasswd(phoneNumber));
                BaseApiCommand.createCommand("User.register/", true, registerParams).execute(GlobalDataManager.getInstance().getApplicationContext(), this);
                return;
            }
            ApiParams loginParams = new ApiParams();
            loginParams.addParam("type", "mobile_code");
            loginParams.addParam("identity", phoneNumber);
            loginParams.addParam("password", code);
            BaseApiCommand.createCommand("User.login/", true, loginParams).execute(GlobalDataManager.getInstance().getApplicationContext(), this);
        } catch (JSONException e) {
            e.printStackTrace();
            handler.obtainMessage(20).sendToTarget();
        }
    }

    public void doAction(Intent intent) {
        String action = intent.getAction();
        if (!CommonIntentAction.ACTION_SEND_MSG.equals(action) && CommonIntentAction.ACTION_SENT_POST.equals(action) && this.promoterId != null && this.businessId != null) {
            HashMap<String, String> attrs = new HashMap<>();
            attrs.put("adId", intent.getStringExtra("adId"));
            if (ReferralNetwork.getInstance().savePromoTask(ReferralUtil.TASK_AD, this.promoterId.substring(1), this.businessId.substring(1), null, null, null, attrs)) {
            }
        }
    }

    public void onNetworkDone(String apiName, String responseData) {
        if (apiName.equals("User.login/") || apiName.equals("User.register/")) {
            try {
                JSONObject result = new JSONObject(responseData).getJSONObject("result");
                JSONObject user = result.getJSONObject("user");
                helpSendPost(user.getString(LocaleUtil.INDONESIAN), user.getString("mobile"), result.getString("token"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onNetworkFail(String apiName, ApiError error) {
        if ((apiName.equals("User.login/") || apiName.equals("User.register/")) && !IsShowDlg) {
            showVerifyDlg();
        }
    }
}
