package com.baixing.sharing.referral;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import com.baixing.data.AccountManager;
import com.baixing.data.GlobalDataManager;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.Util;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Pattern;
import org.apache.commons.httpclient.HttpState;
import org.json.JSONException;
import org.json.JSONObject;

public class ReferralUtil implements Observer {
    public static final String ACTIVATE_KEY = "com.baixing.sharing.referral.activated";
    public static final String CURUSERID_KEY = "com.baixing.sharing.referral.userid";
    public static final String DLGSHOWN_KEY = "com.baixing.sharing.referral.dlgshown";
    public static final String IS_PROMO_KEY = "com.baixing.sharing.referral.ispromoter";
    public static final String LOCATE_EVENT_INFO = "PromoEventInfo";
    public static final String LOCATE_EVENT_URL = "PromoEventUrl";
    public static final String PROMOTER_KEY = "com.baixing.sharing.referral.promoter";
    public static final String PROMO_DETIAL_URL = "http://www.baixing.com/arch/promoRedirect/";
    public static final String REFERRAL_STATUS = "REFERRAL_STATUS";
    public static final String ROLE_ADMIN = "admin";
    public static final String ROLE_MANAGER = "areaManager";
    public static final String ROLE_NORMAL = "normal";
    public static final String ROLE_PROMOTER = "promoter";
    public static final String SHARETYPE_KEY = "com.baixing.sharing.referral.appsharetype";
    public static final int SHARE_BY_BLUETOOTH = 3;
    public static final int SHARE_BY_HAIBAO = 2;
    public static final int SHARE_BY_QRCODE = 1;
    /* access modifiers changed from: private */
    public static final String TAG = ReferralUtil.class.getSimpleName();
    public static final String TASK_AD = "post_store_ad";
    public static final String TASK_APP = "download_app";
    public static final String TASK_STORE = "add_new_store";
    private static ReferralUtil instance = null;
    /* access modifiers changed from: private */
    public Handler handler;

    public static ReferralUtil getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new ReferralUtil();
        return instance;
    }

    public void register(String notificationName) {
        BxMessageCenter.defaultMessageCenter().registerObserver(this, notificationName);
    }

    public void setHandler(Handler handler2) {
        this.handler = handler2;
    }

    public static boolean isValidQRCodeID(String codeId) {
        return Pattern.compile("^\\w{8}$").matcher(codeId).matches();
    }

    public static boolean isValidUserId(String userId) {
        return Pattern.compile("^\\d+$").matcher(userId).matches();
    }

    private void setPromoUserType() {
        AccountManager am = GlobalDataManager.getInstance().getAccountManager();
        if (am.isUserLogin()) {
            String userId = am.getCurrentUser().getId().substring(1);
            if (!TextUtils.isEmpty(userId)) {
                ApiParams params = new ApiParams();
                params.addParam(ApiParams.KEY_USERID, userId);
                BaseApiCommand.createCommand("Promo.getPromoUserType/", true, params).execute(GlobalDataManager.getInstance().getApplicationContext(), new BaseApiCommand.Callback() {
                    public void onNetworkFail(String apiName, ApiError error) {
                        Log.d(ReferralUtil.TAG, error.toString());
                        ReferralUtil.this.handler.sendEmptyMessage(200);
                    }

                    public void onNetworkDone(String apiName, String responseData) {
                        try {
                            JSONObject result = new JSONObject(responseData).getJSONObject("result");
                            String code = result.getJSONObject("error").getString("code");
                            if (code != null && code.equals("0")) {
                                String userType = result.getString("type");
                                if (TextUtils.isEmpty(userType) || (!userType.equals(ReferralUtil.ROLE_PROMOTER) && !userType.equals(ReferralUtil.ROLE_MANAGER) && !userType.equals(ReferralUtil.ROLE_ADMIN))) {
                                    Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "isPromoter", HttpState.PREEMPTIVE_DEFAULT);
                                } else {
                                    Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "isPromoter", "true");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (ReferralUtil.this.handler != null) {
                            ReferralUtil.this.handler.sendEmptyMessage(200);
                        }
                    }
                });
            }
        }
    }

    public static boolean isPromoter() {
        if (!GlobalDataManager.getInstance().getAccountManager().isUserLogin() || Util.loadDataFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "isPromoter", String.class) == null || !((String) Util.loadDataFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "isPromoter", String.class)).equals("true")) {
            return false;
        }
        return true;
    }

    public static Bitmap getQRCodeBitmap(Context context, String content) {
        int size = (Util.getWidthByContext(context) * 2) / 3;
        try {
            return encodeAsBitmap(content, BarcodeFormat.QR_CODE, size, size);
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    static Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int desiredWidth, int desiredHeight) throws WriterException {
        Hashtable<EncodeHintType, String> hints = null;
        String encoding = guessAppropriateEncoding(contents);
        if (encoding != null) {
            hints = new Hashtable<>(2);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        BitMatrix result = new MultiFormatWriter().encode(contents, format, desiredWidth, desiredHeight, hints);
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[(width * height)];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? -16777216 : -1;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 255) {
                return "UTF-8";
            }
        }
        return null;
    }

    public void update(Observable observable, Object data) {
        if ((data instanceof BxMessageCenter.IBxNotification) && IBxNotificationNames.NOTIFICATION_LOGIN.equals(((BxMessageCenter.IBxNotification) data).getName())) {
            setPromoUserType();
        }
    }
}
