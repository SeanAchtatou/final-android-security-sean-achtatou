package com.baixing.sharing.referral;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.entity.UserProfile;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.NetworkUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.contact.RContact;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterOrLoginDlg extends DialogFragment {
    private static final String API_SECRET = "c6dd9d408c0bcbeda381d42955e08a3f";
    private static final int MAX_TRY_TIMES = 15;
    public static final int REQ_LOGIN = 1010;
    /* access modifiers changed from: private */
    public static final String TAG = RegisterOrLoginDlg.class.getSimpleName();
    static int tryTimes;
    Handler handler;
    boolean isSent = false;
    Button normalRegisterLogin;
    String password;
    ProgressBar progressBar;
    Button quickRegisterLogin;
    LinearLayout registerLoginLayout;
    BroadcastReceiver smsDeliveredReceiver;
    BroadcastReceiver smsSentReceiver;
    Timer timer;
    String token;

    public void setHandler(Handler handler2) {
        this.handler = handler2;
    }

    public Dialog onCreateDialog(Bundle savedInstanceBundle) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = inflater.inflate((int) R.layout.dialog_register_login, (ViewGroup) null);
        this.registerLoginLayout = (LinearLayout) v.findViewById(R.id.register_login);
        this.quickRegisterLogin = (Button) v.findViewById(R.id.btn_register_login);
        this.normalRegisterLogin = (Button) v.findViewById(R.id.btn_normal_login);
        this.progressBar = (ProgressBar) v.findViewById(R.id.circleProgressBar);
        builder.setView(v).setTitle("请登录");
        final AlertDialog Dlg = builder.create();
        this.smsSentReceiver = new BroadcastReceiver() {
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case -1:
                        if (!RegisterOrLoginDlg.this.isSent) {
                            ViewUtil.showToast(RegisterOrLoginDlg.this.getActivity(), "短信发送成功", false);
                        }
                        RegisterOrLoginDlg.this.isSent = true;
                        RegisterOrLoginDlg.this.closeDlg();
                        RegisterOrLoginDlg.tryTimes = 0;
                        new Asker(2);
                        return;
                    case 0:
                    default:
                        return;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        ViewUtil.showToast(RegisterOrLoginDlg.this.getActivity(), "短信发送失败", false);
                        RegisterOrLoginDlg.this.reRegister();
                        return;
                }
            }
        };
        this.smsDeliveredReceiver = new BroadcastReceiver() {
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case -1:
                        if (!RegisterOrLoginDlg.this.isSent) {
                            ViewUtil.showToast(RegisterOrLoginDlg.this.getActivity(), "短信发送成功", false);
                        }
                        RegisterOrLoginDlg.this.isSent = true;
                        RegisterOrLoginDlg.this.closeDlg();
                        RegisterOrLoginDlg.tryTimes = 0;
                        new Asker(2);
                        return;
                    case 0:
                        ViewUtil.showToast(RegisterOrLoginDlg.this.getActivity(), "短信发送失败", false);
                        RegisterOrLoginDlg.this.reRegister();
                        return;
                    default:
                        return;
                }
            }
        };
        getActivity().registerReceiver(this.smsSentReceiver, new IntentFilter("SENT_SMS_ACTION"));
        getActivity().registerReceiver(this.smsDeliveredReceiver, new IntentFilter("DELIVERED_SMS_ACTION"));
        this.quickRegisterLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.QUICK_REGISTER_LOGIN).end();
                if (NetworkUtil.isNetworkActive(RegisterOrLoginDlg.this.getActivity())) {
                    RegisterOrLoginDlg.this.registerLoginLayout.setVisibility(8);
                    RegisterOrLoginDlg.this.progressBar.setVisibility(0);
                    Dlg.setTitle("请稍候");
                    PendingIntent piSent = PendingIntent.getBroadcast(RegisterOrLoginDlg.this.getActivity(), 0, new Intent("SENT_SMS_ACTION"), 0);
                    PendingIntent piDelivered = PendingIntent.getBroadcast(RegisterOrLoginDlg.this.getActivity(), 0, new Intent("DELIVERED_SMS_ACTION"), 0);
                    SmsManager smsManager = SmsManager.getDefault();
                    RegisterOrLoginDlg.this.token = NetworkUtil.getMD5(Util.getDeviceUdid(RegisterOrLoginDlg.this.getActivity()) + RegisterOrLoginDlg.API_SECRET).substring(0, 6);
                    smsManager.sendTextMessage("1069013360002", null, "easyRegister_" + RegisterOrLoginDlg.this.token, piSent, piDelivered);
                    Log.d("sms", "sent");
                    return;
                }
                new AlertDialog.Builder(RegisterOrLoginDlg.this.getActivity()).setTitle("网络错误").setMessage("网络连接失败，请确认网络连接").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (Build.VERSION.SDK_INT > 10) {
                            RegisterOrLoginDlg.this.startActivity(new Intent("android.settings.SETTINGS"));
                        } else {
                            RegisterOrLoginDlg.this.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
                        }
                    }
                }).show();
            }
        });
        this.normalRegisterLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.INDEX_START_LOGIN).end();
                RegisterOrLoginDlg.this.handler.obtainMessage(RegisterOrLoginDlg.REQ_LOGIN).sendToTarget();
                RegisterOrLoginDlg.this.dismiss();
            }
        });
        return Dlg;
    }

    /* access modifiers changed from: private */
    public void sendEasyRegisterCmd(final TimerTask task) {
        Log.d("sms", "register");
        if (tryTimes > 15) {
            this.timer.cancel();
            Log.d("sms", "fail");
            ViewUtil.showToast(GlobalDataManager.getInstance().getApplicationContext(), "一键注册登录失败", false);
        }
        tryTimes++;
        ApiParams params = new ApiParams();
        params.addParam("token", this.token);
        params.addParam(ApiParams.KEY_UDID, Util.getDeviceUdid(getActivity()));
        BaseApiCommand.createCommand("Promo.easyRegister/", false, params).execute(GlobalDataManager.getInstance().getApplicationContext(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                Log.d(RegisterOrLoginDlg.TAG, error.toString());
            }

            public void onNetworkDone(String apiName, String responseData) {
                JSONObject error;
                String code;
                try {
                    Log.d(RegisterOrLoginDlg.TAG, responseData.toString());
                    task.cancel();
                    JSONObject obj = new JSONObject(responseData);
                    if (obj != null && (error = obj.getJSONObject("result").getJSONObject("error")) != null && (code = error.getString("code")) != null && code.equals("0")) {
                        RegisterOrLoginDlg.this.timer.cancel();
                        RegisterOrLoginDlg.this.doLoginAction(obj);
                    }
                } catch (JSONException e) {
                    Log.d(RegisterOrLoginDlg.TAG, e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void doLoginAction(JSONObject responseObj) throws JSONException {
        JSONObject error;
        ApiParams params = new ApiParams();
        params.addParam("mobile", responseObj.getString("mobile"));
        try {
            JSONObject jSONObject = new JSONObject(BaseApiCommand.createCommand("getUser", false, params).executeSync(GlobalDataManager.getInstance().getApplicationContext()));
            if (jSONObject != null && (error = jSONObject.getJSONObject("error")) != null) {
                String code = error.getString("code");
                if (code == null || !code.equals("0")) {
                    ViewUtil.showToast(GlobalDataManager.getInstance().getApplicationContext(), "一键注册登录失败", false);
                    Log.d("sms", "fail");
                    return;
                }
                String mobile = jSONObject.getString("mobile");
                String password2 = jSONObject.getString("password");
                String nickname = jSONObject.getString(RContact.COL_NICKNAME);
                String createdTime = jSONObject.getString("createdTime");
                String id = jSONObject.getString(LocaleUtil.INDONESIAN);
                UserBean loginBean = new UserBean();
                loginBean.setId(id);
                loginBean.setPhone(mobile);
                loginBean.setPassword(Util.getDecryptedPassword(password2));
                Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "user", loginBean);
                UserProfile profile = new UserProfile();
                profile.mobile = mobile;
                profile.nickName = nickname;
                profile.userId = id;
                profile.createTime = createdTime;
                Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "userProfile", profile);
                BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_LOGIN, loginBean);
                Log.d("sms", "login");
                ViewUtil.showToast(GlobalDataManager.getInstance().getApplicationContext(), "一键注册登录成功", true);
                if (!TextUtils.isEmpty(ReferralPromoter.getInstance().ID())) {
                    ReferralNetwork.getInstance().savePromoTask(ReferralUtil.TASK_APP, ReferralPromoter.getInstance().ID(), profile.userId.substring(1), Util.getDeviceUdid(GlobalDataManager.getInstance().getApplicationContext()), null, null, null);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class Asker {
        public Asker(int second) {
            RegisterOrLoginDlg.this.timer = new Timer();
            RegisterOrLoginDlg.this.timer.schedule(new AskTask(), 0, (long) (second * 1000));
        }

        class AskTask extends TimerTask {
            AskTask() {
            }

            public void run() {
                RegisterOrLoginDlg.this.sendEasyRegisterCmd(this);
            }
        }
    }

    public void onDismiss(DialogInterface dialog) {
        getActivity().unregisterReceiver(this.smsSentReceiver);
        getActivity().unregisterReceiver(this.smsDeliveredReceiver);
        super.onDismiss(dialog);
    }

    public void closeDlg() {
        dismiss();
    }

    public void reRegister() {
        this.registerLoginLayout.setVisibility(0);
        this.progressBar.setVisibility(8);
    }
}
