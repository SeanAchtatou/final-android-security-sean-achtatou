package com.baixing.sharing.referral;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import com.baixing.data.GlobalDataManager;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Comparator;
import org.json.JSONException;
import org.json.JSONObject;

public class ReferralPromoter {
    private static final String PROMOTER_ID = "PROMOTER_ID";
    private static final String TAG = ReferralPromoter.class.getSimpleName();
    private static ReferralPromoter instance = null;

    public static ReferralPromoter getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new ReferralPromoter();
        return instance;
    }

    public String ID() {
        Context context = GlobalDataManager.getInstance().getApplicationContext();
        String promoter_id = getPromoterId(context);
        if (promoter_id != null) {
            return promoter_id;
        }
        String promoter_id2 = getPromoterIdByAssets(context);
        if (!TextUtils.isEmpty(promoter_id2)) {
            if (ReferralUtil.isValidUserId(promoter_id2)) {
                saveAppShareType(1, context);
            } else if (ReferralUtil.isValidQRCodeID(promoter_id2)) {
                saveAppShareType(2, context);
            }
            return savePromoterId(context, promoter_id2);
        }
        String promoter_id3 = getPromoterIdByBluetooth(context);
        if (TextUtils.isEmpty(promoter_id3)) {
            return savePromoterId(context, "");
        }
        saveAppShareType(3, context);
        return savePromoterId(context, promoter_id3);
    }

    private void saveAppShareType(int shareType, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(ReferralUtil.REFERRAL_STATUS, 0).edit();
        editor.putInt(ReferralUtil.SHARETYPE_KEY, shareType);
        editor.commit();
    }

    private String getPromoterId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(ReferralUtil.REFERRAL_STATUS, 0);
        if (preferences.contains(ReferralUtil.PROMOTER_KEY)) {
            return preferences.getString(ReferralUtil.PROMOTER_KEY, "");
        }
        return null;
    }

    private String getPromoterIdByBluetooth(Context context) {
        File[] bluetooth = Environment.getExternalStorageDirectory().listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.isDirectory() && pathname.getName().equalsIgnoreCase("bluetooth");
            }
        });
        if (bluetooth == null || bluetooth.length == 0) {
            bluetooth = getBluetoothDirInHTC();
        }
        if (bluetooth == null || bluetooth.length <= 0) {
            Log.e(TAG, "No Bluetooth Dir");
        } else {
            File[] files = bluetooth[0].listFiles(new FilenameFilter() {
                public boolean accept(File dir, String filename) {
                    return filename.startsWith("baixing-") && filename.endsWith(".apk");
                }
            });
            if (files == null || files.length <= 0) {
                Log.e(TAG, "No apk found");
            } else {
                if (files.length > 1) {
                    Arrays.sort(files, new Comparator<File>() {
                        public int compare(File lhs, File rhs) {
                            long d1 = lhs.lastModified();
                            long d2 = rhs.lastModified();
                            if (d1 == d2) {
                                return 0;
                            }
                            return d1 < d2 ? 1 : -1;
                        }
                    });
                }
                Log.d(TAG, files[0].getName());
                String[] strs = files[0].getName().split("-");
                if (strs.length >= 4) {
                    return strs[2];
                }
            }
        }
        return null;
    }

    private File[] getBluetoothDirInHTC() {
        File downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (downloads == null || downloads.listFiles() == null) {
            return null;
        }
        File[] arr$ = downloads.listFiles();
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            File file = arr$[i$];
            if (!file.isDirectory() || file.isHidden() || !file.getName().equalsIgnoreCase("bluetooth")) {
                i$++;
            } else {
                return new File[]{file};
            }
        }
        return null;
    }

    private String getPromoterIdByAssets(Context context) {
        try {
            InputStream is = context.getAssets().open(PROMOTER_ID);
            int length = is.available();
            byte[] buffer = new byte[length];
            return new String(buffer, 0, is.read(buffer, 0, length));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String savePromoterId(Context context, String mPromoterID) {
        JSONObject result;
        JSONObject error;
        String code;
        String parentUserId = mPromoterID;
        if (ReferralUtil.isValidUserId(mPromoterID)) {
            parentUserId = mPromoterID;
        } else if (ReferralUtil.isValidQRCodeID(mPromoterID)) {
            ApiParams params = new ApiParams();
            params.addParam("qrcodeId", mPromoterID);
            try {
                JSONObject obj = new JSONObject(BaseApiCommand.createCommand("Promo.getBoundUserId/", false, params).executeSync(GlobalDataManager.getInstance().getApplicationContext()));
                if (!(obj == null || (error = (result = obj.getJSONObject("result")).getJSONObject("error")) == null || (code = error.getString("code")) == null || !code.equals("0"))) {
                    parentUserId = result.getString("ownerId");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        SharedPreferences.Editor editor = context.getSharedPreferences(ReferralUtil.REFERRAL_STATUS, 0).edit();
        editor.putString(ReferralUtil.PROMOTER_KEY, parentUserId);
        editor.commit();
        return parentUserId;
    }
}
