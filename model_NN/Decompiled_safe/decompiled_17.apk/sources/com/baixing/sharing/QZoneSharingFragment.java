package com.baixing.sharing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.util.ViewUtil;
import com.tencent.tauth.Constants;
import com.tencent.tauth.Tencent;
import org.apache.commons.httpclient.cookie.Cookie2;
import org.json.JSONException;
import org.json.JSONObject;

public class QZoneSharingFragment extends BaseSharingFragment implements View.OnClickListener {
    public static final String EXTRA_LINK = "com.qzone.android.link";
    public static final String EXTRA_OPEN_ID = "com.qzone.android.openid";
    public static final String EXTRA_SUMMARY = "com.qzone.android.summary";
    public static final String EXTRA_TITLE = "com.qzone.android.title";
    /* access modifiers changed from: private */
    public ProgressDialog mPd;
    private Tencent mTencent;

    /* access modifiers changed from: private */
    public void doShare2QZone() {
        final boolean succeed = true;
        Bundle bundle = new Bundle();
        if (getArguments() != null) {
            bundle.putString(Constants.PARAM_TITLE, getArguments().getString(EXTRA_TITLE));
            bundle.putString(Constants.PARAM_URL, getArguments().getString(EXTRA_LINK));
            bundle.putString(Cookie2.COMMENT, this.mEdit != null ? this.mEdit.getText().toString() : "");
            bundle.putString(Constants.PARAM_SUMMARY, getArguments().getString(EXTRA_SUMMARY));
            bundle.putString("type", "4");
            if (this.mTencent == null) {
                this.mTencent = Tencent.createInstance(QZoneSharingManager.mAppid, getActivity().getApplicationContext());
                this.mTencent.setAccessToken(getArguments().getString(BaseSharingFragment.EXTRA_ACCESS_TOKEN), getArguments().getString(BaseSharingFragment.EXTRA_EXPIRES_IN));
                this.mTencent.setOpenId(getArguments().getString(EXTRA_OPEN_ID));
            }
            if (this.mTencent != null && this.mTencent.isSessionValid() && this.mTencent.getOpenId() != null) {
                this.mTencent.setOpenId(this.mTencent.getOpenId() + this.mTencent.getOpenId());
                JSONObject result = null;
                try {
                    result = this.mTencent.request(Constants.GRAPH_ADD_SHARE, bundle, "POST");
                } catch (Exception e) {
                }
                if (result == null) {
                    succeed = false;
                }
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        if (!succeed) {
                            ViewUtil.showToast(QZoneSharingFragment.this.getActivity(), "分享失败", false);
                        }
                        if (QZoneSharingFragment.this.mPd != null) {
                            QZoneSharingFragment.this.mPd.dismiss();
                        }
                    }
                });
                if (succeed) {
                    try {
                        final int code = result.getInt("ret");
                        final String msgShow = code == 0 ? "分享成功" : "分享失败";
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                if (code == 0) {
                                    boolean unused = QZoneSharingFragment.this.finishFragment();
                                }
                                ViewUtil.showToast(QZoneSharingFragment.this.getActivity(), msgShow, false);
                            }
                        });
                        if (code == 0) {
                            Context ctx = GlobalDataManager.getInstance().getApplicationContext();
                            if (ctx != null) {
                                ctx.sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_SHARE_SUCCEED));
                            }
                            SharingCenter.trackShareResult("qzone", true, null);
                            return;
                        }
                        SharingCenter.trackShareResult("qzone", false, "code:" + code + " msg:" + result.getString("msg"));
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    public void handleRightAction() {
        if (this.mEdit == null || this.mEdit.getText() == null || this.mEdit.getText().length() == 0) {
            ViewUtil.showToast(getActivity(), "内容不能为空", false);
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                QZoneSharingFragment.this.doShare2QZone();
            }
        }).start();
        this.mPd = ProgressDialog.show(getActivity(), "", "请稍候");
        this.mPd.setCancelable(true);
        this.mPd.setCanceledOnTouchOutside(true);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        super.initTitle(title);
        title.m_title = "QQ空间";
    }
}
