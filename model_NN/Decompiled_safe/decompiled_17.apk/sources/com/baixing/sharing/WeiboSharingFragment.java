package com.baixing.sharing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.sharing.weibo.Oauth2AccessToken;
import com.baixing.sharing.weibo.RequestListener;
import com.baixing.sharing.weibo.StatusesAPI;
import com.baixing.sharing.weibo.WeiboException;
import com.baixing.util.ViewUtil;
import java.io.IOException;

public class WeiboSharingFragment extends BaseSharingFragment implements View.OnClickListener {
    public static final int WEIBO_MAX_LENGTH = 140;
    private String mAccessToken = "";
    private String mExpires_in = "";
    /* access modifiers changed from: private */
    public ProgressDialog mPd;

    class ShareListener implements RequestListener {
        ShareListener() {
        }

        public void onComplete(String arg0) {
            if (WeiboSharingFragment.this.mPd != null) {
                WeiboSharingFragment.this.mPd.dismiss();
            }
            WeiboSharingFragment.this.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    boolean unused = WeiboSharingFragment.this.finishFragment();
                }
            });
            Context ctx = GlobalDataManager.getInstance().getApplicationContext();
            if (ctx != null) {
                ctx.sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_SHARE_SUCCEED));
            }
            ViewUtil.showToast(WeiboSharingFragment.this.getActivity(), "分享成功", false);
            if (WeiboSharingFragment.this.mPd != null) {
                WeiboSharingFragment.this.mPd.dismiss();
            }
            SharingCenter.trackShareResult("weibo", true, null);
        }

        public void onError(WeiboException arg0) {
            if (WeiboSharingFragment.this.mPd != null) {
                WeiboSharingFragment.this.mPd.dismiss();
            }
            ViewUtil.showToast(WeiboSharingFragment.this.getActivity(), arg0.getMessage(), true);
            if (WeiboSharingFragment.this.mPd != null) {
                WeiboSharingFragment.this.mPd.dismiss();
            }
            SharingCenter.trackShareResult("weibo", false, "code:" + arg0.getStatusCode() + " msg:" + arg0.getMessage());
        }

        public void onIOException(IOException arg0) {
            if (WeiboSharingFragment.this.mPd != null) {
                WeiboSharingFragment.this.mPd.dismiss();
            }
            ViewUtil.showToast(WeiboSharingFragment.this.getActivity(), arg0.getMessage(), true);
            if (WeiboSharingFragment.this.mPd != null) {
                WeiboSharingFragment.this.mPd.dismiss();
            }
            SharingCenter.trackShareResult("weibo", false, " msg:" + arg0.getMessage());
        }
    }

    private void doShare2Weibo() {
        StatusesAPI statusApi = new StatusesAPI(new Oauth2AccessToken(this.mAccessToken, this.mExpires_in));
        String content = this.mEdit != null ? this.mEdit.getText().toString() : "";
        if (this.mPicPath == null || this.mPicPath.length() == 0) {
            statusApi.update(content, "", "", new ShareListener());
        } else {
            statusApi.upload(content, this.mPicPath, "", "", new ShareListener());
        }
        this.mPd = ProgressDialog.show(getActivity(), "", "请稍候");
        this.mPd.setCancelable(true);
    }

    public void handleRightAction() {
        doShare2Weibo();
    }

    /* access modifiers changed from: protected */
    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.mAccessToken = bundle.getString(BaseSharingFragment.EXTRA_ACCESS_TOKEN);
            this.mExpires_in = bundle.getString(BaseSharingFragment.EXTRA_EXPIRES_IN);
        }
        return super.onInitializeView(inflater, container, savedInstanceState);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        super.initTitle(title);
        title.m_title = "新浪微博";
    }
}
