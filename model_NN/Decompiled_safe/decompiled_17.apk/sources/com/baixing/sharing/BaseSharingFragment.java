package com.baixing.sharing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.quanleimu.activity.R;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

public class BaseSharingFragment extends BaseFragment implements View.OnClickListener, Observer {
    public static final String EXTRA_ACCESS_TOKEN = "com.sharing.android.accesstoken";
    public static final String EXTRA_EXPIRES_IN = "com.sharing.android.expires";
    public static final String EXTRA_PIC_URI = "com.sharing.android.pic.uri";
    public static final String EXTRA_WEIBO_CONTENT = "com.sharing.android.content";
    public static final int WEIBO_MAX_LENGTH = 140;
    private String mContent = "";
    protected EditText mEdit;
    protected String mPicPath = "";
    /* access modifiers changed from: private */
    public FrameLayout mPiclayout;
    /* access modifiers changed from: private */
    public TextView mTextNum;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
    }

    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.ll_text_limit_unit) {
            new AlertDialog.Builder(getActivity()).setTitle("注意").setMessage("是否要删除这条微博？").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    BaseSharingFragment.this.mEdit.setText("");
                }
            }).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).create().show();
        } else if (viewId == R.id.ivDelPic) {
            new AlertDialog.Builder(getActivity()).setTitle("注意").setMessage("是否删除图片？").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    BaseSharingFragment.this.mPiclayout.setVisibility(8);
                    BaseSharingFragment.this.mPicPath = "";
                }
            }).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).create().show();
        }
    }

    /* access modifiers changed from: protected */
    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate((int) R.layout.share_mblog_view, (ViewGroup) null);
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.mPicPath = bundle.getString(EXTRA_PIC_URI);
            this.mContent = bundle.getString(EXTRA_WEIBO_CONTENT);
        }
        ((LinearLayout) layout.findViewById(R.id.ll_text_limit_unit)).setOnClickListener(this);
        this.mTextNum = (TextView) layout.findViewById(R.id.tv_text_limit);
        ((ImageView) layout.findViewById(R.id.ivDelPic)).setOnClickListener(this);
        this.mEdit = (EditText) layout.findViewById(R.id.etEdit);
        this.mEdit.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int len;
                int len2 = BaseSharingFragment.this.mEdit.getText().toString().length();
                View right = (BaseSharingFragment.this.getView() == null || BaseSharingFragment.this.getView().getRootView() == null) ? null : BaseSharingFragment.this.getView().getRootView().findViewById(R.id.right_action);
                if (len2 <= 140) {
                    len = 140 - len2;
                    if (right != null) {
                        right.setEnabled(true);
                    }
                } else {
                    len = len2 - 140;
                    BaseSharingFragment.this.mTextNum.setTextColor(-65536);
                    if (right != null) {
                        right.setEnabled(false);
                    }
                }
                BaseSharingFragment.this.mTextNum.setText(String.valueOf(len));
            }
        });
        this.mEdit.setText(this.mContent);
        this.mPiclayout = (FrameLayout) layout.findViewById(R.id.flPic);
        if (TextUtils.isEmpty(this.mPicPath)) {
            this.mPiclayout.setVisibility(8);
        } else {
            this.mPiclayout.setVisibility(0);
            if (new File(this.mPicPath).exists()) {
                ((ImageView) layout.findViewById(R.id.ivImage)).setImageBitmap(BitmapFactory.decodeFile(this.mPicPath));
            } else {
                this.mPiclayout.setVisibility(8);
            }
        }
        return layout;
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_leftActionHint = "返回";
        title.m_rightActionHint = "发布";
    }

    public void update(Observable observable, Object data) {
        if ((data instanceof BxMessageCenter.IBxNotification) && IBxNotificationNames.NOTIFICATION_LOGOUT.equals(((BxMessageCenter.IBxNotification) data).getName())) {
            getActivity().sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_MYAD_LOGOUT));
            finishFragment();
        }
    }

    public void onDestroy() {
        BxMessageCenter.defaultMessageCenter().removeObserver(this);
        super.onDestroy();
    }
}
