package com.baixing.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserProfile;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import org.json.JSONException;
import org.json.JSONObject;

public class EditUsernameDialogFragment extends DialogFragment {
    public ICallback callback;
    /* access modifiers changed from: private */
    public UserProfile userProfile;

    public interface ICallback {
        void onEditSucced(String str);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.userProfile = (UserProfile) Util.loadDataFromLocate(getActivity(), "userProfile", UserProfile.class);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = inflater.inflate((int) R.layout.dialog_edit_username, (ViewGroup) null);
        EditText editUsernameEt = (EditText) v.findViewById(R.id.dialog_edit_username_et);
        Drawable left = getResources().getDrawable(R.drawable.iconsearch);
        left.setBounds(0, 0, left.getIntrinsicWidth(), left.getIntrinsicHeight());
        editUsernameEt.setCompoundDrawables(left, null, null, null);
        editUsernameEt.setText(this.userProfile.nickName);
        editUsernameEt.setSelection(editUsernameEt.getText().length());
        builder.setView(v).setTitle("修改用户名").setPositiveButton("修改", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.EDITPROFILE_CANCEL).end();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface di) {
                dialog.getButton(-1).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        EditUsernameDialogFragment.this.updateUsername();
                    }
                });
            }
        });
        return dialog;
    }

    /* access modifiers changed from: private */
    public void updateUsername() {
        EditText editUsernameEt = (EditText) getDialog().findViewById(R.id.dialog_edit_username_et);
        if (editUsernameEt.getText().toString().trim().length() <= 0) {
            ViewUtil.showToast(getActivity(), "请输入用户名", false);
            return;
        }
        final String newUserName = editUsernameEt.getText().toString();
        ApiParams params = new ApiParams();
        params.addParam(BaseFragment.ARG_COMMON_TITLE, newUserName);
        params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        final ProgressDialog pr = ProgressDialog.show(getActivity(), getString(R.string.dialog_title_info), getString(R.string.dialog_message_waiting));
        pr.show();
        BaseApiCommand.createCommand("user.update/", false, params).execute(editUsernameEt.getContext(), new BaseApiCommand.Callback() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
             arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
             candidates:
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
            public void onNetworkFail(String apiName, ApiError error) {
                String errorMsg = "网络异常，请稍后再试";
                if (!(error == null || error.getMsg() == null)) {
                    errorMsg = error.getMsg();
                }
                pr.dismiss();
                ViewUtil.showToast(EditUsernameDialogFragment.this.getActivity(), errorMsg, false);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.EDITPROFILE_SAVE).append(TrackConfig.TrackMobile.Key.EDIT_PROFILE_STATUS, false).append(TrackConfig.TrackMobile.Key.EDIT_RPOFILE_FAIL_REASON, errorMsg).end();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
             arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
             candidates:
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
              com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
            public void onNetworkDone(String apiName, String responseData) {
                try {
                    JSONObject obj = new JSONObject(responseData).getJSONObject("result");
                    EditUsernameDialogFragment.this.userProfile.nickName = obj.getString(BaseFragment.ARG_COMMON_TITLE);
                    Activity activity = EditUsernameDialogFragment.this.getActivity();
                    if (activity != null) {
                        Util.saveDataToLocate(activity, "userProfile", EditUsernameDialogFragment.this.userProfile);
                    }
                    pr.dismiss();
                    EditUsernameDialogFragment.this.callback.onEditSucced(newUserName);
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.EDITPROFILE_SAVE).append(TrackConfig.TrackMobile.Key.EDIT_PROFILE_STATUS, true).end();
                    EditUsernameDialogFragment.this.dismiss();
                } catch (JSONException e) {
                    pr.dismiss();
                    ViewUtil.showToast(EditUsernameDialogFragment.this.getActivity(), "请求失败", false);
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.EDITPROFILE_SAVE).append(TrackConfig.TrackMobile.Key.EDIT_PROFILE_STATUS, false).append(TrackConfig.TrackMobile.Key.EDIT_RPOFILE_FAIL_REASON, "请求失败").end();
                }
            }
        });
    }
}
