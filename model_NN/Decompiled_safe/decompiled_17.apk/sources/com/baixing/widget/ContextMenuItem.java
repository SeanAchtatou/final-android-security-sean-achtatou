package com.baixing.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;

public class ContextMenuItem extends View {
    private int[] optionIds;
    private String[] options;
    private String title;

    public interface ContextHandler {
        void onItemSelect(int i);
    }

    public ContextMenuItem(Context context) {
        super(context);
    }

    public ContextMenuItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ContextMenuItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void updateOptionList(String optionTitle, String[] optionList, int[] ids) {
        this.title = optionTitle;
        this.options = optionList;
        this.optionIds = ids;
    }

    /* access modifiers changed from: protected */
    public void onCreateContextMenu(ContextMenu menu) {
        super.onCreateContextMenu(menu);
        if (this.title != null && this.options != null && this.options.length > 0) {
            menu.setHeaderTitle(this.title);
            for (int i = 0; i < this.options.length; i++) {
                menu.add(0, this.optionIds[i], 0, this.options[i]);
            }
        }
    }
}
