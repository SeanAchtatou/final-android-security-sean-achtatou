package com.baixing.widget;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.baixing.activity.BaseActivity;
import com.baixing.data.GlobalDataManager;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

@SuppressLint({"NewApi"})
public class CommentsDialog extends Dialog implements AdapterView.OnItemClickListener {
    private static String[] markets = {"http://play.google.com/store/apps/details?id=" + GlobalDataManager.getInstance().getApplicationContext().getPackageName(), "http://zhushou.360.cn/detail/index/soft_id/111600?recrefer=SE_D_%E7%99%BE%E5%A7%93%E7%BD%91", "http://www.wandoujia.com/apps/com.quanleimu.activity", "http://app.xiaomi.com/detail/3726", "http://www.nduoa.com/apk/detail/572166"};
    List<AppInfo> appResources = new ArrayList();

    private class AppInfo {
        public String activityName;
        public Drawable icon;
        public String packageName;
        public String showName;
        public Uri uri;

        public AppInfo(String sm, Drawable d, String p, Uri u) {
            this.icon = d;
            this.packageName = p;
            this.uri = u;
            this.showName = sm;
        }
    }

    private class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return CommentsDialog.this.appResources.size();
        }

        public Object getItem(int position) {
            return CommentsDialog.this.appResources.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View appView;
            if (convertView == null) {
                appView = LayoutInflater.from(this.mContext).inflate((int) R.layout.categorygriditem, (ViewGroup) null);
                appView.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.list_selector));
                int width = this.mContext.getResources().getDimensionPixelSize(17104896);
                appView.findViewById(R.id.itemicon).setLayoutParams(new LinearLayout.LayoutParams(width, width));
                ((ImageView) appView.findViewById(R.id.itemicon)).setScaleType(ImageView.ScaleType.FIT_CENTER);
            } else {
                appView = convertView;
            }
            Drawable drawable = CommentsDialog.this.appResources.get(position).icon;
            ((TextView) appView.findViewById(R.id.itemtext)).setText(CommentsDialog.this.appResources.get(position).showName);
            ((ImageView) appView.findViewById(R.id.itemicon)).setImageDrawable(drawable);
            return appView;
        }
    }

    public CommentsDialog(BaseActivity activity) {
        super(activity);
        setTitle("选择评论渠道");
        View v = LayoutInflater.from(getContext()).inflate((int) R.layout.comments, (ViewGroup) null);
        setContentView(v);
        setCanceledOnTouchOutside(true);
        String market = markets[new Random().nextInt(markets.length)];
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri[] data = {Uri.parse("market://details?id=" + activity.getPackageName()), Uri.parse(market)};
        for (int i = 0; i < data.length; i++) {
            intent.setData(data[i]);
            for (ResolveInfo info : activity.getPackageManager().queryIntentActivities(intent, 0)) {
                String packageName = info.activityInfo.applicationInfo.packageName;
                boolean exist = false;
                Iterator i$ = this.appResources.iterator();
                while (true) {
                    if (i$.hasNext()) {
                        if (i$.next().packageName.equals(packageName)) {
                            exist = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!exist) {
                    AppInfo ai = new AppInfo(info.activityInfo.applicationInfo.loadLabel(activity.getPackageManager()).toString(), info.activityInfo.applicationInfo.loadIcon(activity.getPackageManager()), packageName, data[i]);
                    if (i == 0) {
                        ai.activityName = info.activityInfo.name;
                    }
                    this.appResources.add(ai);
                }
            }
        }
        ((GridView) v.findViewById(R.id.gridComments)).setAdapter((ListAdapter) new ImageAdapter(activity));
        ((GridView) v.findViewById(R.id.gridComments)).setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        AppInfo ai = this.appResources.get(arg2);
        if (TextUtils.isEmpty(ai.activityName)) {
            Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage(ai.packageName);
            launchIntent.setData(ai.uri);
            launchIntent.setAction("android.intent.action.VIEW");
            getContext().startActivity(launchIntent);
        } else {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(this.appResources.get(arg2).uri);
            intent.setComponent(new ComponentName(ai.packageName, ai.activityName));
            getContext().startActivity(intent);
        }
        dismiss();
    }
}
