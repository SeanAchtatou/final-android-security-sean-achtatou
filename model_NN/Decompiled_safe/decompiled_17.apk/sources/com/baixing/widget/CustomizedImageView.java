package com.baixing.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class CustomizedImageView extends ImageView {
    protected int bitmapHeight = 0;
    protected int bitmapWidth = 0;

    public CustomizedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomizedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomizedImageView(Context context) {
        super(context);
    }

    public void setImageBitmap(Bitmap bitmap) {
        if (bitmap != null) {
            this.bitmapWidth = bitmap.getWidth();
            this.bitmapHeight = bitmap.getHeight();
        }
        super.setImageBitmap(bitmap);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.bitmapWidth != 0) {
            int width = View.MeasureSpec.getSize(widthMeasureSpec);
            setMeasuredDimension(width, (this.bitmapHeight * width) / this.bitmapWidth);
        }
    }
}
