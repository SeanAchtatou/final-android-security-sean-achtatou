package com.baixing.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Scroller;
import com.devspark.appmsg.AppMsg;
import com.quanleimu.activity.R;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

public class ViewFlow extends AdapterView<Adapter> {
    private static final int EFFECTIVE_SCROLL = 2;
    private static final int INVALID_SCREEN = -1;
    private static final int SNAP_VELOCITY = 500;
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_SCROLLING = 1;
    /* access modifiers changed from: private */
    public Adapter mAdapter;
    /* access modifiers changed from: private */
    public int mCurrentAdapterIndex;
    /* access modifiers changed from: private */
    public int mCurrentBufferIndex;
    /* access modifiers changed from: private */
    public int mCurrentScreen;
    private AdapterDataSetObserver mDataSetObserver;
    private boolean mFirstLayout;
    private FlowIndicator mIndicator;
    private float mLastMotionX;
    private int mLastOrientation;
    private int mLastScrollDirection;
    private EnumSet<LazyInit> mLazyInit;
    private LinkedList<View> mLoadedViews;
    private int mMaximumVelocity;
    private int mNextScreen;
    private Scroller mScroller;
    private int mSideBuffer;
    private int mTouchState;
    private VelocityTracker mVelocityTracker;
    private ViewLazyInitializeListener mViewInitializeListener;
    private ViewSwitchListener mViewSwitchListener;
    /* access modifiers changed from: private */
    public ViewTreeObserver.OnGlobalLayoutListener orientationChangeListener;
    /* access modifiers changed from: private */
    public long scrollDurationMs;
    private long scrollIntervalMs;
    private Timer timerScroll;

    enum LazyInit {
        LEFT,
        RIGHT
    }

    public interface ViewLazyInitializeListener {
        void onViewLazyInitialize(View view, int i);

        void onViewRecycled(View view);
    }

    public interface ViewSwitchListener {
        void onSwitched(View view, int i);
    }

    public ViewFlow(Context context) {
        super(context);
        this.mSideBuffer = 1;
        this.mTouchState = 0;
        this.mNextScreen = -1;
        this.mFirstLayout = true;
        this.mLazyInit = EnumSet.allOf(LazyInit.class);
        this.mLastOrientation = -1;
        this.timerScroll = null;
        this.scrollIntervalMs = 5000;
        this.scrollDurationMs = 200;
        this.orientationChangeListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                ViewFlow.this.getViewTreeObserver().removeGlobalOnLayoutListener(ViewFlow.this.orientationChangeListener);
                ViewFlow.this.setSelection(ViewFlow.this.mCurrentAdapterIndex);
            }
        };
        this.mSideBuffer = 1;
        init();
    }

    public ViewFlow(Context context, int sideBuffer) {
        super(context);
        this.mSideBuffer = 1;
        this.mTouchState = 0;
        this.mNextScreen = -1;
        this.mFirstLayout = true;
        this.mLazyInit = EnumSet.allOf(LazyInit.class);
        this.mLastOrientation = -1;
        this.timerScroll = null;
        this.scrollIntervalMs = 5000;
        this.scrollDurationMs = 200;
        this.orientationChangeListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                ViewFlow.this.getViewTreeObserver().removeGlobalOnLayoutListener(ViewFlow.this.orientationChangeListener);
                ViewFlow.this.setSelection(ViewFlow.this.mCurrentAdapterIndex);
            }
        };
        this.mSideBuffer = sideBuffer;
        init();
    }

    private class TimedScroll extends TimerTask {
        private boolean mCanceled;

        private TimedScroll() {
            this.mCanceled = false;
        }

        public boolean cancel() {
            super.cancel();
            this.mCanceled = true;
            return true;
        }

        public void run() {
            if (!this.mCanceled) {
                ViewFlow.this.post(new Runnable() {
                    public void run() {
                        if (ViewFlow.this.mCurrentAdapterIndex == ViewFlow.this.getViewsCount() - 1) {
                            ViewFlow.this.setSelection(0);
                        } else {
                            ViewFlow.this.snapToScreen(ViewFlow.this.mCurrentScreen + 1, (int) ViewFlow.this.scrollDurationMs);
                        }
                    }
                });
            }
        }
    }

    public ViewFlow(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mSideBuffer = 1;
        this.mTouchState = 0;
        this.mNextScreen = -1;
        this.mFirstLayout = true;
        this.mLazyInit = EnumSet.allOf(LazyInit.class);
        this.mLastOrientation = -1;
        this.timerScroll = null;
        this.scrollIntervalMs = 5000;
        this.scrollDurationMs = 200;
        this.orientationChangeListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                ViewFlow.this.getViewTreeObserver().removeGlobalOnLayoutListener(ViewFlow.this.orientationChangeListener);
                ViewFlow.this.setSelection(ViewFlow.this.mCurrentAdapterIndex);
            }
        };
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.ViewFlow);
        this.mSideBuffer = styledAttrs.getInt(0, 3);
        if (styledAttrs.getBoolean(1, false)) {
            this.scrollIntervalMs = (long) styledAttrs.getInteger(2, AppMsg.LENGTH_LONG);
            this.scrollDurationMs = (long) styledAttrs.getInteger(3, 200);
            this.timerScroll = new Timer();
            this.timerScroll.scheduleAtFixedRate(new TimedScroll(), this.scrollIntervalMs, this.scrollIntervalMs);
        }
        init();
    }

    private void init() {
        this.mLoadedViews = new LinkedList<>();
        this.mScroller = new Scroller(getContext());
        ViewConfiguration configuration = ViewConfiguration.get(getContext());
        configuration.getScaledTouchSlop();
        this.mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation != this.mLastOrientation) {
            this.mLastOrientation = newConfig.orientation;
            getViewTreeObserver().addOnGlobalLayoutListener(this.orientationChangeListener);
        }
    }

    public int getViewsCount() {
        return this.mAdapter.getCount();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
        int height = View.MeasureSpec.getSize(heightMeasureSpec);
        int item_height = 0;
        int item_width = 0;
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
            int height_temp = getChildAt(i).getMeasuredHeight();
            if (height_temp > item_height) {
                item_height = height_temp;
            }
            int width_temp = getChildAt(i).getMeasuredHeight();
            if (width_temp > item_width) {
                item_width = width_temp;
            }
        }
        if (this.mFirstLayout) {
            this.mScroller.startScroll(0, 0, this.mCurrentScreen * width, 0, 0);
            this.mFirstLayout = false;
        }
        if (width == 0 || widthMode == 0) {
            width = getPaddingLeft() + item_width + getPaddingRight();
        }
        if (height == 0 || heightMode == 0) {
            height = getPaddingTop() + item_height + getPaddingBottom();
        }
        setMeasuredDimension(width, height);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int childLeft = 0;
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                int childWidth = child.getMeasuredWidth();
                child.layout(childLeft, 0, childLeft + childWidth, child.getMeasuredHeight());
                childLeft += childWidth;
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        int availableToScroll;
        int i = 0;
        if (getChildCount() == 0) {
            return false;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(ev);
        int action = ev.getAction();
        float x = ev.getX();
        switch (action) {
            case 0:
                if (!this.mScroller.isFinished()) {
                    this.mScroller.abortAnimation();
                }
                this.mLastMotionX = x;
                if (!this.mScroller.isFinished()) {
                    i = 1;
                }
                this.mTouchState = i;
                break;
            case 1:
                if (this.mTouchState == 1) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int velocityX = (int) velocityTracker.getXVelocity();
                    if (velocityX > 500 && this.mCurrentAdapterIndex > 0) {
                        snapToScreen(this.mCurrentScreen - 1);
                    } else if (velocityX >= -500 || this.mCurrentAdapterIndex >= this.mAdapter.getCount() - 1) {
                        snapToDestination();
                    } else {
                        snapToScreen(this.mCurrentScreen + 1);
                    }
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                } else {
                    performItemClick(getChildAt(this.mCurrentScreen), this.mCurrentScreen, 0);
                }
                this.mTouchState = 0;
                break;
            case 2:
                int deltaX = (int) (this.mLastMotionX - x);
                if (1 != 0) {
                    this.mTouchState = 1;
                    if (this.mViewInitializeListener != null) {
                        initializeView((float) deltaX);
                    }
                }
                if (this.mTouchState == 1) {
                    this.mLastMotionX = x;
                    int scrollX = getScrollX();
                    if (deltaX < 0) {
                        if (scrollX > 0) {
                            scrollBy(Math.max(-scrollX, deltaX), 0);
                        }
                    } else if (deltaX > 0 && (availableToScroll = (getChildAt(getChildCount() - 1).getRight() - scrollX) - getWidth()) > 0) {
                        scrollBy(Math.min(availableToScroll, deltaX), 0);
                    }
                    return true;
                }
                break;
            case 3:
                this.mTouchState = 0;
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        boolean xMoved;
        int availableToScrollRight;
        int i = 0;
        if (getChildCount() == 0) {
            return false;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(ev);
        int action = ev.getAction();
        float x = ev.getX();
        switch (action) {
            case 0:
                if (!this.mScroller.isFinished()) {
                    this.mScroller.abortAnimation();
                }
                this.mLastMotionX = x;
                if (!this.mScroller.isFinished()) {
                    i = 1;
                }
                this.mTouchState = i;
                getScrollX();
                return true;
            case 1:
                if (this.mTouchState == 1) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int velocityX = (int) velocityTracker.getXVelocity();
                    if (velocityX > 500 && this.mCurrentAdapterIndex > 0) {
                        snapToScreen(this.mCurrentScreen - 1);
                    } else if (velocityX >= -500 || this.mCurrentAdapterIndex >= this.mAdapter.getCount() - 1) {
                        snapToDestination();
                    } else {
                        snapToScreen(this.mCurrentScreen + 1);
                    }
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                } else {
                    performItemClick(getChildAt(this.mCurrentScreen), this.mCurrentScreen, 0);
                }
                this.mTouchState = 0;
                return true;
            case 2:
                int deltaX = (int) (this.mLastMotionX - x);
                if (Math.abs(deltaX) > 2) {
                    xMoved = true;
                } else {
                    xMoved = false;
                }
                if (!xMoved) {
                    return true;
                }
                this.mTouchState = 1;
                if (this.mViewInitializeListener != null) {
                    initializeView((float) deltaX);
                }
                this.mLastMotionX = x;
                int scrollX = getScrollX();
                if (deltaX < 0) {
                    if (scrollX <= 0) {
                        return true;
                    }
                    scrollBy(Math.max(-scrollX, deltaX), 0);
                    return true;
                } else if (deltaX <= 0 || (availableToScrollRight = (getChildAt(getChildCount() - 1).getRight() - scrollX) - getWidth()) <= 0) {
                    return true;
                } else {
                    scrollBy(Math.min(availableToScrollRight, deltaX), 0);
                    return true;
                }
            case 3:
                if (this.mTouchState == 1) {
                    VelocityTracker velocityTracker2 = this.mVelocityTracker;
                    velocityTracker2.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int velocityX2 = (int) velocityTracker2.getXVelocity();
                    if (velocityX2 > 500 && this.mCurrentScreen > 0) {
                        snapToScreen(this.mCurrentScreen - 1);
                    } else if (velocityX2 >= -500 || this.mCurrentScreen >= getChildCount() - 1) {
                        snapToDestination();
                    } else {
                        snapToScreen(this.mCurrentScreen + 1);
                    }
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                }
                this.mTouchState = 0;
                return true;
            default:
                return true;
        }
    }

    private void initializeView(float direction) {
        if (direction > 0.0f) {
            if (this.mLazyInit.contains(LazyInit.RIGHT)) {
                this.mLazyInit.remove(LazyInit.RIGHT);
                if (this.mCurrentBufferIndex + 1 < this.mLoadedViews.size()) {
                    this.mViewInitializeListener.onViewLazyInitialize(this.mLoadedViews.get(this.mCurrentBufferIndex + 1), this.mCurrentAdapterIndex + 1);
                }
            }
        } else if (this.mLazyInit.contains(LazyInit.LEFT)) {
            this.mLazyInit.remove(LazyInit.LEFT);
            if (this.mCurrentBufferIndex > 0) {
                if (this.mCurrentBufferIndex > this.mLoadedViews.size()) {
                    Log.d("ViewFlow", "Fatal Error in initializeView: mCurrentBufferIndex[" + this.mCurrentBufferIndex + "] > mLoadedViews.size()[" + this.mLoadedViews.size() + "]!!!");
                }
                this.mViewInitializeListener.onViewLazyInitialize(this.mLoadedViews.get(this.mCurrentBufferIndex - 1), this.mCurrentAdapterIndex - 1);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int h, int v, int oldh, int oldv) {
        super.onScrollChanged(h, v, oldh, oldv);
        if (this.mIndicator != null) {
            this.mIndicator.onScrolled(h + ((this.mCurrentAdapterIndex - this.mCurrentBufferIndex) * getWidth()), v, oldh, oldv);
        }
    }

    private void snapToDestination() {
        int screenWidth = getWidth();
        snapToScreen((getScrollX() + (screenWidth / 2)) / screenWidth);
    }

    private void snapToScreen(int whichScreen) {
        this.mLastScrollDirection = whichScreen - this.mCurrentScreen;
        if (this.mScroller.isFinished()) {
            int whichScreen2 = Math.max(0, Math.min(whichScreen, getChildCount() - 1));
            this.mNextScreen = whichScreen2;
            int delta = (whichScreen2 * getWidth()) - getScrollX();
            this.mScroller.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);
            invalidate();
        }
    }

    /* access modifiers changed from: private */
    public void snapToScreen(int whichScreen, int durationMs) {
        this.mLastScrollDirection = whichScreen - this.mCurrentScreen;
        if (this.mScroller.isFinished()) {
            int whichScreen2 = Math.max(0, Math.min(whichScreen, getViewsCount() - 1));
            this.mNextScreen = whichScreen2;
            this.mScroller.startScroll(getScrollX(), 0, (whichScreen2 * getWidth()) - getScrollX(), 0, durationMs);
            invalidate();
        }
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
        } else if (this.mNextScreen != -1) {
            this.mCurrentScreen = Math.max(0, Math.min(this.mNextScreen, getChildCount() - 1));
            this.mNextScreen = -1;
            postViewSwitched(this.mLastScrollDirection);
        }
    }

    private void setVisibleView(int indexInBuffer, boolean uiThread) {
        this.mCurrentScreen = Math.max(0, Math.min(indexInBuffer, this.mLoadedViews.size()));
        int dx = (this.mCurrentScreen * getWidth()) - this.mScroller.getCurrX();
        this.mScroller.startScroll(this.mScroller.getCurrX(), this.mScroller.getCurrY(), dx, 0, 0);
        if (dx == 0) {
            onScrollChanged(this.mScroller.getCurrX() + dx, this.mScroller.getCurrY(), this.mScroller.getCurrX() + dx, this.mScroller.getCurrY());
        }
        if (uiThread) {
            invalidate();
        } else {
            postInvalidate();
        }
    }

    public void setOnViewSwitchListener(ViewSwitchListener l) {
        this.mViewSwitchListener = l;
    }

    public void setOnViewLazyInitializeListener(ViewLazyInitializeListener l) {
        this.mViewInitializeListener = l;
    }

    public Adapter getAdapter() {
        return this.mAdapter;
    }

    public void setAdapter(Adapter adapter) {
        setAdapter(adapter, 0);
    }

    public void setAdapter(Adapter adapter, int initialPosition) {
        if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
        }
        this.mAdapter = adapter;
        if (this.mAdapter != null) {
            this.mDataSetObserver = new AdapterDataSetObserver();
            this.mAdapter.registerDataSetObserver(this.mDataSetObserver);
        }
        if (this.mAdapter != null && this.mAdapter.getCount() != 0) {
            setSelection(initialPosition);
        }
    }

    public View getSelectedView() {
        if (this.mCurrentBufferIndex < this.mLoadedViews.size()) {
            return this.mLoadedViews.get(this.mCurrentBufferIndex);
        }
        return null;
    }

    public int getSelectedItemPosition() {
        return this.mCurrentAdapterIndex;
    }

    public void setFlowIndicator(FlowIndicator flowIndicator) {
        this.mIndicator = flowIndicator;
        this.mIndicator.setViewFlow(this);
    }

    /* access modifiers changed from: protected */
    public void recycleViews() {
        while (!this.mLoadedViews.isEmpty()) {
            recycleView(this.mLoadedViews.remove());
        }
    }

    /* access modifiers changed from: protected */
    public void recycleView(View v) {
        if (v != null) {
            v.setBackgroundDrawable(null);
            detachViewFromParent(v);
            removeViewInLayout(v);
            if (this.mViewInitializeListener != null) {
                this.mViewInitializeListener.onViewRecycled(v);
            }
        }
    }

    public void setSelection(int position) {
        this.mNextScreen = -1;
        this.mScroller.forceFinished(true);
        if (this.mAdapter != null) {
            int position2 = Math.min(Math.max(position, 0), this.mAdapter.getCount() - 1);
            recycleViews();
            View currentView = makeAndAddView(position2, true);
            this.mLoadedViews.addLast(currentView);
            if (this.mViewInitializeListener != null) {
                this.mViewInitializeListener.onViewLazyInitialize(currentView, position2);
            }
            for (int offset = 1; this.mSideBuffer - offset >= 0; offset++) {
                int leftIndex = position2 - offset;
                int rightIndex = position2 + offset;
                if (leftIndex >= 0) {
                    this.mLoadedViews.addFirst(makeAndAddView(leftIndex, false));
                }
                if (rightIndex < this.mAdapter.getCount()) {
                    this.mLoadedViews.addLast(makeAndAddView(rightIndex, true));
                }
            }
            this.mCurrentBufferIndex = this.mLoadedViews.indexOf(currentView);
            this.mCurrentAdapterIndex = position2;
            requestLayout();
            setVisibleView(this.mCurrentBufferIndex, false);
            if (this.mIndicator != null) {
                this.mIndicator.onSwitched(currentView, this.mCurrentAdapterIndex);
            }
            if (this.mViewSwitchListener != null) {
                this.mViewSwitchListener.onSwitched(currentView, this.mCurrentAdapterIndex);
            }
        }
    }

    /* access modifiers changed from: private */
    public void resetFocus() {
        logBuffer();
        recycleViews();
        removeAllViewsInLayout();
        this.mLazyInit.addAll(EnumSet.allOf(LazyInit.class));
        for (int i = Math.max(0, this.mCurrentAdapterIndex - this.mSideBuffer); i < Math.min(this.mAdapter.getCount(), this.mCurrentAdapterIndex + this.mSideBuffer + 1); i++) {
            this.mLoadedViews.addLast(makeAndAddView(i, true));
            if (i == this.mCurrentAdapterIndex) {
                this.mCurrentBufferIndex = this.mLoadedViews.size() - 1;
                if (this.mViewInitializeListener != null) {
                    this.mViewInitializeListener.onViewLazyInitialize(this.mLoadedViews.getLast(), this.mCurrentAdapterIndex);
                }
            }
        }
        logBuffer();
        requestLayout();
    }

    private void postViewSwitched(int direction) {
        if (direction != 0) {
            if (direction > 0) {
                this.mCurrentAdapterIndex++;
                this.mCurrentBufferIndex++;
                this.mLazyInit.remove(LazyInit.LEFT);
                this.mLazyInit.add(LazyInit.RIGHT);
                if (this.mCurrentBufferIndex >= this.mSideBuffer * 2) {
                    while (true) {
                        if (this.mCurrentBufferIndex > this.mSideBuffer && (this.mCurrentAdapterIndex - this.mCurrentBufferIndex) + (this.mSideBuffer * 2) < this.mAdapter.getCount()) {
                            if (this.mLoadedViews.size() <= 0) {
                                Log.d("haha", "hahaha,   no more elements   " + this.mCurrentBufferIndex);
                                break;
                            } else {
                                recycleView(this.mLoadedViews.removeFirst());
                                this.mCurrentBufferIndex--;
                            }
                        } else {
                            break;
                        }
                    }
                }
                for (int newBufferIndex = (this.mCurrentAdapterIndex + this.mLoadedViews.size()) - this.mCurrentBufferIndex; newBufferIndex < this.mAdapter.getCount() && this.mLoadedViews.size() < (this.mSideBuffer * 2) + 1; newBufferIndex++) {
                    View vLast = makeAndAddView(newBufferIndex, true);
                    if (vLast != null) {
                        this.mLoadedViews.addLast(vLast);
                    }
                }
            } else {
                this.mCurrentAdapterIndex--;
                this.mCurrentBufferIndex--;
                this.mLazyInit.add(LazyInit.LEFT);
                this.mLazyInit.remove(LazyInit.RIGHT);
                if (this.mCurrentBufferIndex <= 0) {
                    while (true) {
                        if ((this.mLoadedViews.size() - 1) - this.mCurrentBufferIndex > this.mSideBuffer) {
                            if (this.mLoadedViews.size() <= 0) {
                                Log.d("haha", "hahaha,   no more elements222   " + this.mCurrentBufferIndex);
                                break;
                            }
                            recycleView(this.mLoadedViews.removeLast());
                        } else {
                            break;
                        }
                    }
                }
                for (int newBufferIndex2 = (this.mCurrentAdapterIndex - this.mCurrentBufferIndex) - 1; newBufferIndex2 > -1 && this.mCurrentBufferIndex < this.mSideBuffer && this.mLoadedViews.size() < (this.mSideBuffer * 2) + 1; newBufferIndex2--) {
                    this.mLoadedViews.addFirst(makeAndAddView(newBufferIndex2, false));
                    this.mCurrentBufferIndex++;
                }
            }
            requestLayout();
            if (this.mCurrentBufferIndex >= this.mLoadedViews.size() || this.mCurrentBufferIndex < 0) {
                Log.d("ViewFlow", "fatal error, mCurrentBufferIndex out of range");
            }
            if (this.mCurrentAdapterIndex < this.mAdapter.getCount()) {
                setVisibleView(this.mCurrentBufferIndex, true);
                if (this.mCurrentBufferIndex < this.mLoadedViews.size() && this.mCurrentBufferIndex >= 0) {
                    if (this.mIndicator != null) {
                        this.mIndicator.onSwitched(this.mLoadedViews.get(this.mCurrentBufferIndex), this.mCurrentAdapterIndex);
                    }
                    if (this.mViewSwitchListener != null) {
                        this.mViewSwitchListener.onSwitched(this.mLoadedViews.get(this.mCurrentBufferIndex), this.mCurrentAdapterIndex);
                    }
                }
            }
            logBuffer();
        }
    }

    private View setupChild(View child, boolean addToEnd, boolean recycle) {
        int i = -1;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new AbsListView.LayoutParams(-1, -2, 0);
        }
        if (recycle) {
            if (!addToEnd) {
                i = 0;
            }
            attachViewToParent(child, i, p);
        } else {
            if (!addToEnd) {
                i = 0;
            }
            addViewInLayout(child, i, p, true);
        }
        return child;
    }

    private View makeAndAddView(int position, boolean addToEnd) {
        return makeAndAddView(position, addToEnd, null);
    }

    private View makeAndAddView(int position, boolean addToEnd, View convertView) {
        View view = this.mAdapter.getView(position, convertView, this);
        if (view == null) {
            return null;
        }
        return setupChild(view, addToEnd, view == convertView);
    }

    class AdapterDataSetObserver extends DataSetObserver {
        AdapterDataSetObserver() {
        }

        public void onChanged() {
            View v = ViewFlow.this.getChildAt(ViewFlow.this.mCurrentBufferIndex);
            if (v != null) {
                int index = 0;
                while (true) {
                    if (index >= ViewFlow.this.mAdapter.getCount()) {
                        break;
                    } else if (v.equals(ViewFlow.this.mAdapter.getItem(index))) {
                        int unused = ViewFlow.this.mCurrentAdapterIndex = index;
                        break;
                    } else {
                        index++;
                    }
                }
            }
            ViewFlow.this.resetFocus();
        }

        public void onInvalidated() {
        }
    }

    private void logBuffer() {
    }

    public void finalize() {
        recycleViews();
        try {
            super.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
