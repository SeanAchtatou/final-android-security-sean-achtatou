package com.baixing.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;

public class EditTitleDialogFragment extends DialogFragment {
    /* access modifiers changed from: private */
    public ICallback callback;
    /* access modifiers changed from: private */
    public String title;

    public interface ICallback {
        void onTitleChangeFinished(String str);
    }

    public void setCallback(ICallback callback2) {
        this.callback = callback2;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate((int) R.layout.dialog_edit_username, (ViewGroup) null);
        final EditText etTitle = (EditText) v.findViewById(R.id.dialog_edit_username_et);
        etTitle.setHint("标题");
        etTitle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(25)});
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.title = bundle.getString(Constants.PARAM_TITLE);
        }
        etTitle.setText(this.title);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("修改标题").setView(v).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (EditTitleDialogFragment.this.callback != null) {
                    EditTitleDialogFragment.this.callback.onTitleChangeFinished(etTitle.getText().toString());
                }
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface dlg) {
                dialog.getButton(-1).setEnabled(false);
            }
        });
        etTitle.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s) && !s.toString().equals(EditTitleDialogFragment.this.title)) {
                    dialog.getButton(-1).setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        return dialog;
    }
}
