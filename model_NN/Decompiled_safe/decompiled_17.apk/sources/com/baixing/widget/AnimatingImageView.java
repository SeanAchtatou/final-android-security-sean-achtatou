package com.baixing.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class AnimatingImageView extends ImageView {
    private View mForefrontView = null;

    public AnimatingImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public AnimatingImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimatingImageView(Context context) {
        super(context);
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        if (getVisibility() != 0) {
            setVisibility(0);
        }
        if (this.mForefrontView != null && bitmap != null) {
            this.mForefrontView.setVisibility(8);
        }
    }

    public void setImageResource(int resId) {
        super.setImageResource(resId);
        if (getVisibility() != 0) {
            setVisibility(0);
        }
        if (this.mForefrontView != null && resId != -1) {
            this.mForefrontView.setVisibility(8);
        }
    }

    public void setForefrontView(View view) {
        this.mForefrontView = view;
    }
}
