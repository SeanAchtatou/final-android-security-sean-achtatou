package com.baixing.widget;

import com.baixing.widget.ViewFlow;

public interface FlowIndicator extends ViewFlow.ViewSwitchListener {
    void onScrolled(int i, int i2, int i3, int i4);

    void setViewFlow(ViewFlow viewFlow);
}
