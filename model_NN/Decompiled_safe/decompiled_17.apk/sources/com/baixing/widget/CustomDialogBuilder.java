package com.baixing.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Category;
import com.baixing.entity.Filterss;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.view.fragment.MultiLevelSelectionFragment;
import com.baixing.view.fragment.PostParamsHolder;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CustomDialogBuilder {
    public static final String ARG_COMMON_REQ_CODE = "reqestCode";
    public static final int HOLDER_TAG = "secondCateAdapter_itemholder".hashCode();
    public static final int MSG_CATEGORY_SEL_BACK = 11;
    public static final int MSG_DIALOG_BACK_WITH_DATA = 12;
    /* access modifiers changed from: private */
    public static final String TAG = CustomDialogBuilder.class.getSimpleName();
    private final int MESSAGE_GET_METAOBJ = 1;
    /* access modifiers changed from: private */
    public Context context = null;
    private Handler delegateHandler;
    /* access modifiers changed from: private */
    public CustomDialog dialog;
    /* access modifiers changed from: private */
    public Handler handler = null;
    /* access modifiers changed from: private */
    public boolean hasNextLevel = false;
    /* access modifiers changed from: private */
    public boolean hasRangeSelection = false;
    /* access modifiers changed from: private */
    public String id = null;
    /* access modifiers changed from: private */
    public boolean isCategoryItem = false;
    /* access modifiers changed from: private */
    public List items = null;
    /* access modifiers changed from: private */
    public String json = null;
    /* access modifiers changed from: private */
    public Object lastChoise = null;
    private ProgressDialog pd;
    private int remainLevel = 0;
    private int requestCode;
    /* access modifiers changed from: private */
    public String selectedValue = null;
    /* access modifiers changed from: private */
    public String unit = "";

    public CustomDialogBuilder(Context context2, Handler delegateHandler2, Bundle bundle) {
        boolean z = false;
        this.context = context2;
        this.delegateHandler = delegateHandler2;
        this.remainLevel = bundle.getInt("maxLevel");
        this.hasNextLevel = this.remainLevel > 0 ? true : z;
        this.items = (List) bundle.getSerializable("items");
        Log.d(TAG, this.items.toString());
        this.requestCode = bundle.getInt("reqestCode");
        if (bundle.getInt("reqestCode") == 11) {
            this.isCategoryItem = true;
        }
        if (bundle.containsKey("selectedValue") && bundle.getString("selectedValue") != null) {
            this.selectedValue = bundle.getString("selectedValue");
        }
    }

    public void setHasRangeSelection(String unit2) {
        this.hasRangeSelection = true;
        this.unit = unit2;
        MultiLevelSelectionFragment.MultiLevelItem range = new MultiLevelSelectionFragment.MultiLevelItem();
        range.txt = "";
        range.id = "";
        this.items.add(range);
    }

    public void start() {
        this.dialog = getCustomDialog();
        this.dialog.show();
        configCustomDialog(this.dialog);
    }

    /* access modifiers changed from: private */
    public void configCustomDialog(CustomDialog cd) {
        ListView lv = cd.getListView();
        List<Map<String, Object>> firstLevelList = new ArrayList<>();
        if (this.isCategoryItem) {
            cd.setTitle("请选择分类");
            for (Category item : this.items) {
                Map<String, Object> map = new HashMap<>();
                map.put("tv", item.getName());
                map.put("tvEnglishName", item.getEnglishName());
                firstLevelList.add(map);
            }
        } else {
            cd.setTitle("请选择");
            Log.d(TAG, this.items.toString());
            for (MultiLevelSelectionFragment.MultiLevelItem item2 : this.items) {
                Map<String, Object> map2 = new HashMap<>();
                map2.put("tv", item2.toString());
                map2.put(LocaleUtil.INDONESIAN, item2.id);
                firstLevelList.add(map2);
            }
        }
        Log.d(TAG, firstLevelList.toString());
        configFirstLevel(cd, lv, firstLevelList);
    }

    /* access modifiers changed from: protected */
    public final void showProgress(int titleResid, int messageResid, boolean cancelable) {
        showProgress(this.context.getString(titleResid), this.context.getString(messageResid), cancelable);
    }

    /* access modifiers changed from: protected */
    public final void showProgress(String title, String message, boolean cancelable) {
        hideProgress();
        if (this.context != null) {
            this.pd = ProgressDialog.show(this.context, title, message);
            this.pd.setCancelable(cancelable);
            this.pd.setCanceledOnTouchOutside(cancelable);
        }
    }

    /* access modifiers changed from: protected */
    public final void hideProgress() {
        if (this.pd != null && this.pd.isShowing()) {
            this.pd.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void sendMessage(int what, Object data) {
        if (this.handler != null) {
            Message message = this.handler.obtainMessage();
            message.what = what;
            if (data != null) {
                message.obj = data;
            }
            this.handler.sendMessage(message);
        }
    }

    private void configFirstLevel(final CustomDialog cd, final ListView lv, final List list) {
        lv.setAdapter((ListAdapter) new FirstCateAdapter(list));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.baixing.widget.CustomDialogBuilder.showProgress(int, int, boolean):void
             arg types: [?, ?, int]
             candidates:
              com.baixing.widget.CustomDialogBuilder.showProgress(java.lang.String, java.lang.String, boolean):void
              com.baixing.widget.CustomDialogBuilder.showProgress(int, int, boolean):void */
            public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                Log.d(CustomDialogBuilder.TAG, "onItemClick");
                if (CustomDialogBuilder.this.hasNextLevel) {
                    Log.d(CustomDialogBuilder.TAG, "hasNextLevel true");
                    List<Map<String, Object>> secondLevelList = new ArrayList<>();
                    if (CustomDialogBuilder.this.isCategoryItem) {
                        Log.d(CustomDialogBuilder.TAG, "isCategoryItem " + CustomDialogBuilder.this.isCategoryItem);
                        cd.setTitle("请选择分类");
                        List<Category> allCates = GlobalDataManager.getInstance().getFirstLevelCategory();
                        Log.d(CustomDialogBuilder.TAG, allCates.toString());
                        if (allCates == null || allCates.size() <= pos) {
                            Log.d(CustomDialogBuilder.TAG, "Reload category");
                            GlobalDataManager.getInstance().loadCategorySync();
                            allCates = GlobalDataManager.getInstance().getFirstLevelCategory();
                            if (allCates == null || allCates.size() <= pos) {
                                Log.d(CustomDialogBuilder.TAG, "仁至义尽");
                                return;
                            }
                        }
                        Log.d(CustomDialogBuilder.TAG, "list.get(pos): " + list.get(pos).toString());
                        Category selectedCate = null;
                        String selText = (String) ((Map) list.get(pos)).get("tvEnglishName");
                        Log.d(CustomDialogBuilder.TAG, "selText: " + selText);
                        int i = 0;
                        while (true) {
                            if (i >= allCates.size()) {
                                break;
                            }
                            Log.d(CustomDialogBuilder.TAG, allCates.get(i).getEnglishName());
                            if (allCates.get(i).getEnglishName().equals(selText)) {
                                selectedCate = allCates.get(i);
                                break;
                            }
                            i++;
                        }
                        Map<String, Object> backMap = new HashMap<>();
                        backMap.put("tvCategoryName", "返回上一级");
                        backMap.put("tvCategoryEnglishName", "back");
                        secondLevelList.add(backMap);
                        Log.d("CustomDialogBuilder", selectedCate.toString());
                        for (Category cate : selectedCate.getChildren()) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("tvCategoryName", cate.getName());
                            map.put("tvCategoryEnglishName", cate.getEnglishName());
                            secondLevelList.add(map);
                        }
                        CustomDialogBuilder.this.configSecondLevel(cd, lv, secondLevelList);
                        return;
                    }
                    Handler unused = CustomDialogBuilder.this.handler = new Handler() {
                        public void handleMessage(Message msg) {
                            switch (msg.what) {
                                case 1:
                                    CustomDialogBuilder.this.hideProgress();
                                    if (CustomDialogBuilder.this.json != null) {
                                        LinkedHashMap<String, Filterss> areaList = JsonUtil.getAreas(CustomDialogBuilder.this.json);
                                        if (areaList != null) {
                                            Filterss area = areaList.get(((MultiLevelSelectionFragment.MultiLevelItem) msg.obj).id);
                                            List<MultiLevelSelectionFragment.MultiLevelItem> secondLevelItems = new ArrayList<>();
                                            if (area.getLabelsList() != null) {
                                                MultiLevelSelectionFragment.MultiLevelItem tBack = new MultiLevelSelectionFragment.MultiLevelItem();
                                                tBack.txt = "返回上一级";
                                                tBack.id = null;
                                                secondLevelItems.add(tBack);
                                                if (area.getLabelsList().size() > 1) {
                                                    MultiLevelSelectionFragment.MultiLevelItem tAll = new MultiLevelSelectionFragment.MultiLevelItem();
                                                    MultiLevelSelectionFragment.MultiLevelItem selectedItem = (MultiLevelSelectionFragment.MultiLevelItem) msg.obj;
                                                    tAll.txt = selectedItem.toString();
                                                    tAll.id = selectedItem.id;
                                                    secondLevelItems.add(tAll);
                                                }
                                                for (int i = 0; i < area.getLabelsList().size(); i++) {
                                                    MultiLevelSelectionFragment.MultiLevelItem t = new MultiLevelSelectionFragment.MultiLevelItem();
                                                    t.txt = area.getLabelsList().get(i).getLabel();
                                                    t.id = area.getValuesList().get(i).getValue();
                                                    secondLevelItems.add(t);
                                                }
                                                CustomDialogBuilder.this.configSecondLevel(cd, lv, secondLevelItems);
                                                return;
                                            }
                                            MultiLevelSelectionFragment.MultiLevelItem item = new MultiLevelSelectionFragment.MultiLevelItem();
                                            item.txt = area.getDisplayName();
                                            item.id = area.getName();
                                            Object unused = CustomDialogBuilder.this.lastChoise = item;
                                            CustomDialogBuilder.this.handleLastLevelChoice(cd);
                                            return;
                                        }
                                        return;
                                    }
                                    ViewUtil.showToast(CustomDialogBuilder.this.context, "网络连接异常", false);
                                    return;
                                default:
                                    return;
                            }
                        }
                    };
                    if (((String) ((Map) list.get(pos)).get("tv")).equals("全部")) {
                        Object unused2 = CustomDialogBuilder.this.lastChoise = (MultiLevelSelectionFragment.MultiLevelItem) CustomDialogBuilder.this.items.get(pos);
                        CustomDialogBuilder.this.handleLastLevelChoice(cd);
                        return;
                    }
                    String selId = ((MultiLevelSelectionFragment.MultiLevelItem) CustomDialogBuilder.this.items.get(pos)).id;
                    if (selId.equals(PostParamsHolder.INVALID_VALUE)) {
                        Object unused3 = CustomDialogBuilder.this.lastChoise = (MultiLevelSelectionFragment.MultiLevelItem) CustomDialogBuilder.this.items.get(pos);
                        CustomDialogBuilder.this.handleLastLevelChoice(cd);
                        return;
                    }
                    CustomDialogBuilder.this.showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_waiting, true);
                    String unused4 = CustomDialogBuilder.this.id = selId;
                    CustomDialogBuilder.this.sendGetMetaCmd(CustomDialogBuilder.this.id, ((MultiLevelSelectionFragment.MultiLevelItem) CustomDialogBuilder.this.items.get(pos)).toString());
                    return;
                }
                System.out.println("hasNextLevel false");
                Object unused5 = CustomDialogBuilder.this.lastChoise = (MultiLevelSelectionFragment.MultiLevelItem) CustomDialogBuilder.this.items.get(pos);
                CustomDialogBuilder.this.handleLastLevelChoice(cd);
            }
        });
    }

    /* access modifiers changed from: private */
    public void configSecondLevel(final CustomDialog cd, ListView lv, final List list) {
        lv.setAdapter((ListAdapter) new SecondCateAdapter(list));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                if (pos == 0) {
                    CustomDialogBuilder.this.configCustomDialog(cd);
                    return;
                }
                if (CustomDialogBuilder.this.isCategoryItem) {
                    Map<String, Object> map = (Map) list.get(pos);
                    Object unused = CustomDialogBuilder.this.lastChoise = map.get("tvCategoryEnglishName") + "," + map.get("tvCategoryName");
                } else {
                    Object unused2 = CustomDialogBuilder.this.lastChoise = (MultiLevelSelectionFragment.MultiLevelItem) list.get(pos);
                }
                CustomDialogBuilder.this.handleLastLevelChoice(cd);
            }
        });
    }

    /* access modifiers changed from: private */
    public void handleLastLevelChoice(CustomDialog cd) {
        cd.dismiss();
        if (this.delegateHandler != null) {
            Message message = this.delegateHandler.obtainMessage();
            message.what = 12;
            Bundle bundle = new Bundle();
            bundle.putSerializable("lastChoise", (Serializable) this.lastChoise);
            bundle.putInt("reqestCode", this.requestCode);
            message.obj = bundle;
            this.delegateHandler.sendMessage(message);
        }
    }

    private CustomDialog getCustomDialog() {
        return new CustomDialog(this.context);
    }

    /* access modifiers changed from: private */
    public void sendGetMetaCmd(String id2, String txt) {
        Pair<Long, String> areaData = Util.loadJsonAndTimestampFromLocate(this.context, "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName());
        final MultiLevelSelectionFragment.MultiLevelItem selectedItem = new MultiLevelSelectionFragment.MultiLevelItem();
        selectedItem.id = id2;
        selectedItem.txt = txt;
        if (areaData.second == null || ((String) areaData.second).length() <= 0 || ((Long) areaData.first).longValue() + TextUtil.FULL_DAY < System.currentTimeMillis() / 1000) {
            ApiParams areaParam = new ApiParams();
            areaParam.addParam("cityId", GlobalDataManager.getInstance().getCityId());
            BaseApiCommand.createCommand("City.areas/", true, areaParam).execute(this.context, new BaseApiCommand.Callback() {
                public void onNetworkFail(String apiName, ApiError error) {
                    CustomDialogBuilder.this.sendMessage(1, selectedItem);
                }

                public void onNetworkDone(String apiName, String responseData) {
                    String unused = CustomDialogBuilder.this.json = responseData;
                    Util.saveJsonAndTimestampToLocate(CustomDialogBuilder.this.context, "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName(), responseData, System.currentTimeMillis() / 1000);
                    CustomDialogBuilder.this.sendMessage(1, selectedItem);
                }
            });
            return;
        }
        this.json = (String) areaData.second;
        sendMessage(1, selectedItem);
    }

    class FirstCateAdapter extends BaseAdapter {
        private List list = null;

        public FirstCateAdapter(List list2) {
            this.list = list2;
        }

        public int getCount() {
            return this.list.size();
        }

        public Object getItem(int position) {
            return this.list.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (!CustomDialogBuilder.this.hasRangeSelection || position != CustomDialogBuilder.this.items.size() - 1) {
                View v = LayoutInflater.from(CustomDialogBuilder.this.context).inflate((int) R.layout.item_seccategory_simple2, (ViewGroup) null);
                TextView tv = (TextView) v.findViewById(R.id.tv);
                ImageView img = (ImageView) v.findViewById(R.id.img);
                if (tv != null) {
                    tv.setText((String) ((Map) this.list.get(position)).get("tv"));
                }
                if (!CustomDialogBuilder.this.isCategoryItem && img != null && ((Map) this.list.get(position)).get(LocaleUtil.INDONESIAN).equals(CustomDialogBuilder.this.selectedValue)) {
                    img.setBackgroundDrawable(CustomDialogBuilder.this.context.getResources().getDrawable(R.drawable.pic_radio_selected));
                }
                return v;
            }
            final View range = LayoutInflater.from(CustomDialogBuilder.this.context).inflate((int) R.layout.item_range_setting, (ViewGroup) null);
            ((TextView) range.findViewById(R.id.rangeUnit)).setText(CustomDialogBuilder.this.unit);
            range.findViewById(R.id.range_finish).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String left = ((TextView) range.findViewById(R.id.leftRange)).getText().toString();
                    String right = ((TextView) range.findViewById(R.id.rightRange)).getText().toString();
                    if (TextUtils.isEmpty(left) && TextUtils.isEmpty(right)) {
                        ViewUtil.showToast(GlobalDataManager.getInstance().getApplicationContext(), "至少填写一项范围", false);
                    } else if (TextUtils.isEmpty(left) || TextUtils.isEmpty(right) || Integer.valueOf(left).intValue() <= Integer.valueOf(right).intValue()) {
                        MultiLevelSelectionFragment.MultiLevelItem item = new MultiLevelSelectionFragment.MultiLevelItem();
                        item.id = (TextUtils.isEmpty(left) ? "*" : Integer.valueOf(left)) + " , " + (TextUtils.isEmpty(right) ? "*" : Integer.valueOf(right));
                        StringBuilder sb = new StringBuilder();
                        if (TextUtils.isEmpty(left)) {
                            left = "*";
                        }
                        StringBuilder append = sb.append(left).append("-");
                        if (TextUtils.isEmpty(right)) {
                            right = "*";
                        }
                        item.txt = append.append(right).append((Object) ((TextView) range.findViewById(R.id.rangeUnit)).getText()).toString();
                        Object unused = CustomDialogBuilder.this.lastChoise = item;
                        CustomDialogBuilder.this.handleLastLevelChoice(CustomDialogBuilder.this.dialog);
                    } else {
                        ViewUtil.showToast(GlobalDataManager.getInstance().getApplicationContext(), "范围不正确", false);
                    }
                }
            });
            return range;
        }

        class Holder {
            public TextView tv;

            Holder() {
            }
        }
    }

    class SecondCateAdapter extends BaseAdapter {
        private List list = null;

        public SecondCateAdapter(List list2) {
            this.list = list2;
        }

        public int getCount() {
            return this.list.size();
        }

        public Object getItem(int position) {
            return this.list.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v;
            TextView tv;
            String displayText;
            ImageView img = null;
            if (position == 0) {
                v = LayoutInflater.from(CustomDialogBuilder.this.context).inflate((int) R.layout.item_seccategory_simple, (ViewGroup) null);
                tv = (TextView) v.findViewById(R.id.tv);
            } else {
                v = LayoutInflater.from(CustomDialogBuilder.this.context).inflate((int) R.layout.item_seccategory_simple2, (ViewGroup) null);
                tv = (TextView) v.findViewById(R.id.tv);
                img = (ImageView) v.findViewById(R.id.img);
            }
            if (tv != null) {
                if (CustomDialogBuilder.this.isCategoryItem) {
                    displayText = (String) ((Map) this.list.get(position)).get("tvCategoryName");
                } else {
                    displayText = ((MultiLevelSelectionFragment.MultiLevelItem) this.list.get(position)).toString();
                }
                if (!CustomDialogBuilder.this.isCategoryItem && position == 1) {
                    displayText = "全部";
                }
                tv.setText(displayText);
            }
            if (img != null) {
                if (!CustomDialogBuilder.this.isCategoryItem) {
                    if (((MultiLevelSelectionFragment.MultiLevelItem) this.list.get(position)).id.equals(CustomDialogBuilder.this.selectedValue)) {
                        img.setBackgroundDrawable(CustomDialogBuilder.this.context.getResources().getDrawable(R.drawable.pic_radio_selected));
                    }
                } else if (((Map) this.list.get(position)).get("tvCategoryName").equals(CustomDialogBuilder.this.selectedValue)) {
                    img.setBackgroundDrawable(CustomDialogBuilder.this.context.getResources().getDrawable(R.drawable.pic_radio_selected));
                }
            }
            return v;
        }

        class Holder {
            public TextView tv;

            Holder() {
            }
        }
    }
}
