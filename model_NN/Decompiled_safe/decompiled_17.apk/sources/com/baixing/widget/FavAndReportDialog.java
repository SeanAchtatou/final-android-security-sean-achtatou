package com.baixing.widget;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import com.baixing.activity.BaseActivity;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.UserBean;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.FavoriteNetworkUtil;
import com.baixing.util.ViewUtil;
import com.baixing.view.vad.VadLogger;
import com.quanleimu.activity.R;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class FavAndReportDialog extends Dialog implements View.OnClickListener {
    public static final int MSG_PROSECUTE = 305459440;
    /* access modifiers changed from: private */
    public BaseActivity activity;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            ViewUtil.hideSimpleProgress(FavAndReportDialog.this.pd);
            switch (msg.what) {
                case FavoriteNetworkUtil.MSG_CANCEL_FAVORITE_SUCCESS:
                    FavAndReportDialog.this.removeAdFromFavorite();
                    ViewUtil.showToast(FavAndReportDialog.this.getContext(), ((FavoriteNetworkUtil.ReplyData) msg.obj).message, false);
                    return;
                case FavoriteNetworkUtil.MSG_CANCEL_FAVORITE_FAIL:
                    ViewUtil.showToast(FavAndReportDialog.this.activity, (String) msg.obj, false);
                    return;
                case FavoriteNetworkUtil.MSG_ADD_FAVORITE_SUCCESS:
                    FavAndReportDialog.this.addAdToFavorite();
                    ViewUtil.showToast(FavAndReportDialog.this.getContext(), ((FavoriteNetworkUtil.ReplyData) msg.obj).message, false);
                    return;
                case FavoriteNetworkUtil.MSG_ADD_FAVORITE_FAIL:
                    ViewUtil.showToast(FavAndReportDialog.this.activity, (String) msg.obj, false);
                    return;
                default:
                    return;
            }
        }
    };
    private Ad mAd;
    private Handler outHandler;
    /* access modifiers changed from: private */
    public ProgressDialog pd;

    public FavAndReportDialog(BaseActivity activity2, Ad ad, Handler handler2) {
        super(activity2);
        this.outHandler = handler2;
        this.activity = activity2;
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        View v = LayoutInflater.from(getContext()).inflate((int) R.layout.popup_fav_report, (ViewGroup) null);
        v.findViewById(R.id.favButton).setOnClickListener(this);
        v.findViewById(R.id.reportBtn).setOnClickListener(this);
        setContentView(v);
        if (GlobalDataManager.getInstance().isFav(ad)) {
            Drawable left = activity2.getResources().getDrawable(R.drawable.viewad_icon_fav);
            left.setBounds(0, 0, left.getIntrinsicWidth(), left.getIntrinsicHeight());
            ((Button) v.findViewById(R.id.favButton)).setCompoundDrawables(left, null, null, null);
            ((Button) v.findViewById(R.id.favButton)).setText("取消收藏");
        }
        getWindow().clearFlags(2);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        this.mAd = ad;
    }

    public void show(int x, int y) {
        WindowManager.LayoutParams wmlp = getWindow().getAttributes();
        wmlp.gravity = 51;
        wmlp.x = x;
        wmlp.y = y;
        if (!GlobalDataManager.getInstance().getAccountManager().isUserLogin()) {
            show();
            return;
        }
        new AsyncTask<Ad, Integer, Boolean>() {
            /* access modifiers changed from: protected */
            public Boolean doInBackground(Ad... ads) {
                ApiParams params = new ApiParams();
                params.addParam("adId", ads[0].getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
                params.addParam("mobile", GlobalDataManager.getInstance().getAccountManager().getCurrentUser().getPhone());
                try {
                    JSONObject json = new JSONObject(BaseApiCommand.createCommand("ad_reported", true, params).executeSync(FavAndReportDialog.this.getContext()));
                    if (json.getJSONObject("error").getInt("code") == 0) {
                        return Boolean.valueOf(json.getBoolean("reported"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return false;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean reported) {
                if (reported.booleanValue()) {
                    ((Button) FavAndReportDialog.this.findViewById(R.id.reportBtn)).setText("已举报");
                    ((Button) FavAndReportDialog.this.findViewById(R.id.reportBtn)).setClickable(false);
                } else {
                    ((Button) FavAndReportDialog.this.findViewById(R.id.reportBtn)).setText("举报");
                    ((Button) FavAndReportDialog.this.findViewById(R.id.reportBtn)).setClickable(true);
                }
                FavAndReportDialog.this.show();
            }
        }.execute(this.mAd);
    }

    /* access modifiers changed from: private */
    public void addAdToFavorite() {
        List<Ad> addFav = GlobalDataManager.getInstance().addFav(this.mAd);
        ((Button) findViewById(R.id.favButton)).setText("取消收藏");
        Drawable left = this.activity.getResources().getDrawable(R.drawable.viewad_icon_fav);
        left.setBounds(0, 0, left.getIntrinsicWidth(), left.getIntrinsicHeight());
        ((Button) findViewById(R.id.favButton)).setCompoundDrawables(left, null, null, null);
    }

    /* access modifiers changed from: private */
    public void removeAdFromFavorite() {
        GlobalDataManager.getInstance().removeFav(this.mAd);
        ((Button) findViewById(R.id.favButton)).setText("收藏");
        Drawable left = this.activity.getResources().getDrawable(R.drawable.viewad_icon_unfav);
        left.setBounds(0, 0, left.getIntrinsicWidth(), left.getIntrinsicHeight());
        ((Button) findViewById(R.id.favButton)).setCompoundDrawables(left, null, null, null);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.favButton) {
            VadLogger.trackLikeUnlike(this.mAd);
            UserBean user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
            boolean isValidUser = user != null && !TextUtils.isEmpty(user.getPhone());
            if (!GlobalDataManager.getInstance().isFav(this.mAd)) {
                if (isValidUser) {
                    ViewUtil.hideSimpleProgress(this.pd);
                    this.pd = ViewUtil.showSimpleProgress(this.activity);
                    FavoriteNetworkUtil.addFavorite(this.activity, this.mAd.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID), user, this.handler);
                } else {
                    addAdToFavorite();
                    ViewUtil.showToast(getContext(), "收藏成功", true);
                }
            } else if (isValidUser) {
                ViewUtil.hideSimpleProgress(this.pd);
                this.pd = ViewUtil.showSimpleProgress(this.activity);
                FavoriteNetworkUtil.cancelFavorite(this.activity, this.mAd.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID), user, this.handler);
            } else {
                removeAdFromFavorite();
                ViewUtil.showToast(getContext(), "取消收藏", true);
            }
            dismiss();
        } else if (v.getId() == R.id.reportBtn) {
            this.outHandler.sendEmptyMessage(MSG_PROSECUTE);
            dismiss();
        }
    }
}
