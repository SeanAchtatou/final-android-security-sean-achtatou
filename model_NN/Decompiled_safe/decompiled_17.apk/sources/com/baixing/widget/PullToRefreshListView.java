package com.baixing.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baixing.network.NetworkUtil;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.Util;

public class PullToRefreshListView extends ListView implements AbsListView.OnScrollListener, View.OnClickListener {
    private static final int DAY_MS = 86400000;
    private static final int GETTING_MORE = 6;
    private static final int HOUR_MS = 3600000;
    private static final int MINUTE_MS = 60000;
    private static final int PULL_TO_REFRESH = 2;
    private static final int REFRESHING = 4;
    private static final int RELEASE_TO_REFRESH = 3;
    private static final int SCROLLDOWN_TO_GETMORE = 5;
    private static final int SNAP_VELOCITY = 1000;
    private static final String TAG = "PullToRefreshListView";
    private static final int TAP_TO_REFRESH = 1;
    private boolean mAllowGetMore = true;
    private boolean mBounceHack;
    private int mCurrentScrollState;
    private int mDownY;
    private boolean mEnableHeader = true;
    private RotateAnimation mFlipAnimation;
    private LinearLayout mGapHeaderView = null;
    private boolean mGetMoreAllowPolicy = true;
    private OnGetmoreListener mGetMoreListener;
    private int mGetMoreState = 5;
    private ViewGroup mGetmoreView = null;
    private boolean mHasMore = true;
    private LayoutInflater mInflater;
    private int mLastMotionY;
    private long mLastUpdateTimeMs;
    private int mMaximumVelocity;
    private boolean mNeedBlankGapHeader = false;
    private ViewGroup mNoInfoView = null;
    private OnRefreshListener mOnRefreshListener;
    private AbsListView.OnScrollListener mOnScrollListener;
    private int mRefreshOriginalTopPadding;
    /* access modifiers changed from: private */
    public int mRefreshState;
    private RelativeLayout mRefreshView;
    private int mRefreshViewHeight;
    private ImageView mRefreshViewImage;
    private TextView mRefreshViewLastUpdated;
    private ProgressBar mRefreshViewProgress;
    private TextView mRefreshViewText;
    private RotateAnimation mReverseFlipAnimation;
    private boolean mTouchDown = false;
    private VelocityTracker mVelocityTracker;
    private boolean showFooter = true;

    public enum E_GETMORE {
        E_GETMORE_OK,
        E_GETMORE_NO_MORE
    }

    public interface OnGetmoreListener {
        void onGetMore();
    }

    public interface OnListHeightMeasurer {
        int onMeasureListHeight();
    }

    public interface OnRefreshListener {
        void onRefresh();
    }

    public PullToRefreshListView(Context context) {
        super(context);
        init(context);
    }

    public PullToRefreshListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.PullToRefreshListView);
        this.mGetMoreAllowPolicy = styledAttrs.getBoolean(0, true);
        this.mNeedBlankGapHeader = styledAttrs.getBoolean(1, false);
        init(context);
    }

    public PullToRefreshListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.PullToRefreshListView);
        this.mGetMoreAllowPolicy = styledAttrs.getBoolean(0, true);
        this.mNeedBlankGapHeader = styledAttrs.getBoolean(1, false);
        init(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.baixing.widget.PullToRefreshListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void init(Context context) {
        this.mFlipAnimation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.mFlipAnimation.setInterpolator(new LinearInterpolator());
        this.mFlipAnimation.setDuration(250);
        this.mFlipAnimation.setFillAfter(true);
        this.mReverseFlipAnimation = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.mReverseFlipAnimation.setInterpolator(new LinearInterpolator());
        this.mReverseFlipAnimation.setDuration(250);
        this.mReverseFlipAnimation.setFillAfter(true);
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mRefreshView = (RelativeLayout) this.mInflater.inflate((int) R.layout.pull_to_refresh_header, (ViewGroup) this, false);
        this.mRefreshViewText = (TextView) this.mRefreshView.findViewById(R.id.pull_to_refresh_text);
        this.mRefreshViewImage = (ImageView) this.mRefreshView.findViewById(R.id.pull_to_refresh_image);
        this.mRefreshViewProgress = (ProgressBar) this.mRefreshView.findViewById(R.id.pull_to_refresh_progress);
        this.mRefreshViewLastUpdated = (TextView) this.mRefreshView.findViewById(R.id.pull_to_refresh_updated_at);
        this.mRefreshViewImage.setMinimumHeight(50);
        this.mRefreshView.setOnClickListener(new OnClickRefreshListener());
        this.mRefreshOriginalTopPadding = this.mRefreshView.getPaddingTop();
        this.mRefreshState = 1;
        addHeaderView(this.mRefreshView);
        if (this.mNeedBlankGapHeader) {
            this.mGapHeaderView = (LinearLayout) this.mInflater.inflate((int) R.layout.pull_to_refresh_gap_header, (ViewGroup) this, false);
            addHeaderView(this.mGapHeaderView);
        }
        this.mAllowGetMore = true;
        if (this.mAllowGetMore) {
            this.mGetmoreView = (ViewGroup) this.mInflater.inflate((int) R.layout.pull_to_refresh_footer, (ViewGroup) this, false);
            addFooterView(this.mGetmoreView, null, true);
        }
        this.mNoInfoView = (ViewGroup) this.mInflater.inflate((int) R.layout.goodslist_empty_hint, (ViewGroup) null);
        super.setOnScrollListener(this);
        measureView(this.mRefreshView);
        this.mRefreshViewHeight = this.mRefreshView.getMeasuredHeight();
        this.mLastUpdateTimeMs = System.currentTimeMillis();
        resetHeader();
        ViewConfiguration configuration = ViewConfiguration.get(getContext());
        configuration.getScaledTouchSlop();
        this.mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
    }

    private void updateFooter(boolean hasMore) {
        if (this.showFooter) {
            if (this.mAllowGetMore) {
                this.mHasMore = hasMore;
                if (this.mHasMore) {
                    boolean showProgress = NetworkUtil.isWifiConnection(getContext());
                    updateFooterTip(!showProgress ? R.string.click_to_get_more : R.string.scrolldown_to_getmore, showProgress);
                } else {
                    updateFooterTip(R.string.scrolldown_to_getmore_nomore, false);
                }
            }
            ListAdapter adapter = findGoodListAdapter();
            if (this.mNoInfoView != null) {
                try {
                    removeFooterView(this.mNoInfoView);
                } catch (Throwable th) {
                    this.mNoInfoView.setVisibility(8);
                }
            }
            if (adapter == null || adapter.getCount() == 0) {
                this.mNoInfoView.setVisibility(0);
                addFooterView(this.mNoInfoView);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mGetMoreState == 6) {
            if (getAdapter() == null || !this.mAllowGetMore || getAdapter().getCount() != getLastVisiblePosition()) {
                this.mGetMoreState = 5;
            } else {
                onGetMore();
            }
        } else if (this.mRefreshState == 4) {
            if (getFirstVisiblePosition() == 0) {
                onRefresh();
            } else {
                this.mRefreshState = 2;
            }
        }
        resetHeader();
        updateFooter(this.mHasMore);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.mAllowGetMore = this.mGetMoreAllowPolicy & judgeListFull();
        if (!this.mAllowGetMore && this.mGetmoreView.getVisibility() != 8) {
            this.mGetmoreView.setVisibility(8);
            setFooterDividersEnabled(false);
        } else if (this.mAllowGetMore && this.mGetmoreView.getVisibility() != 0) {
            this.mGetmoreView.setVisibility(0);
            setFooterDividersEnabled(true);
        }
        super.onDraw(canvas);
    }

    public void fireRefresh() {
        checkLastUpdateTime();
        prepareForRefresh();
        super.setSelection(0);
        this.mNoInfoView.setVisibility(8);
        if (this.mOnRefreshListener != null) {
            this.mOnRefreshListener.onRefresh();
        }
    }

    public void setPullToRefreshEnabled(boolean enable) {
        if (this.mEnableHeader != enable) {
            if (enable) {
                this.mRefreshView.setVisibility(0);
                this.mRefreshViewText.setVisibility(0);
                if (this.mRefreshState == 4) {
                    this.mRefreshViewProgress.setVisibility(0);
                }
                if (this.mRefreshState == 2 && this.mRefreshState == 3) {
                    this.mRefreshViewLastUpdated.setVisibility(0);
                }
            } else {
                this.mRefreshView.setVisibility(8);
                this.mRefreshViewText.setVisibility(8);
                this.mRefreshViewImage.setVisibility(8);
                this.mRefreshViewProgress.setVisibility(8);
                this.mRefreshViewLastUpdated.setVisibility(8);
            }
            this.mEnableHeader = enable;
        }
    }

    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);
        setSelectionFromTop(1, 0);
        updateFooter(this.mHasMore);
    }

    public void setSelection(int selection) {
        if (this.mNeedBlankGapHeader) {
            super.setSelection((selection - getHeaderViewsCount()) + 1);
        } else {
            super.setSelection(selection - getHeaderViewsCount());
        }
    }

    public void setSelectionFromHeader(int selection) {
        setSelectionFromTop(selection, getHeaderViewsCount());
    }

    public void setOnScrollListener(AbsListView.OnScrollListener l) {
        this.mOnScrollListener = l;
    }

    public void setOnRefreshListener(OnRefreshListener onRefreshListener) {
        this.mOnRefreshListener = onRefreshListener;
    }

    public void setOnGetMoreListener(OnGetmoreListener listener) {
        this.mGetMoreListener = listener;
    }

    public void setOnListHeightMeasurer(OnListHeightMeasurer measurer) {
    }

    public void checkLastUpdateTime() {
        long time_diff = System.currentTimeMillis() - this.mLastUpdateTimeMs;
        long nDays = time_diff / Util.MILLSECONDS_OF_DAY;
        long time_diff2 = time_diff % Util.MILLSECONDS_OF_DAY;
        long nHours = time_diff2 / Util.MILLSECONDS_OF_HOUR;
        long time_diff3 = time_diff2 % Util.MILLSECONDS_OF_HOUR;
        long nMinutes = time_diff3 / Util.MILLSECONDS_OF_MINUTE;
        long time_diff4 = time_diff3 % Util.MILLSECONDS_OF_MINUTE;
        String strLastUpdate = "最后更新于:";
        if (nDays > 0) {
            strLastUpdate = strLastUpdate + nDays + "天";
        }
        if (nHours > 0) {
            strLastUpdate = strLastUpdate + nHours + "小时";
        }
        this.mRefreshViewLastUpdated.setText((strLastUpdate + nMinutes + "分钟") + "前");
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(event);
        int y = (int) event.getY();
        this.mBounceHack = false;
        switch (event.getAction()) {
            case 0:
                this.mLastMotionY = y;
                this.mTouchDown = true;
                this.mDownY = y;
                break;
            case 1:
                if (!isVerticalScrollBarEnabled()) {
                    setVerticalScrollBarEnabled(true);
                }
                if (getFirstVisiblePosition() >= getHeaderViewsCount() || !this.mEnableHeader || this.mRefreshState == 4) {
                    if (this.mAllowGetMore && getLastVisiblePosition() == getCount() - 1 && this.mGetMoreState != 6 && this.mGetmoreView.getTop() + ((this.mGetmoreView.getHeight() * 2) / 5) < getBottom()) {
                        this.mGetMoreState = 6;
                        onGetMore();
                    }
                } else if (this.mRefreshState == 3) {
                    this.mRefreshState = 4;
                    prepareForRefresh();
                    onRefresh();
                } else {
                    this.mRefreshState = 1;
                    resetHeader();
                    setSelection(0);
                }
                this.mTouchDown = false;
                break;
            case 2:
                if (this.mRefreshState != 4 && (this.mCurrentScrollState == 0 || !judgeListFull())) {
                    if (y - this.mDownY > 10) {
                        pullToRefresh(getFirstVisiblePosition());
                    } else {
                        this.mRefreshState = 1;
                        resetHeader();
                    }
                }
                applyHeaderPadding(event);
                this.mTouchDown = true;
                break;
        }
        return super.onTouchEvent(event);
    }

    private boolean judgeListFull() {
        int visibleCount = getChildCount();
        int allCount = 0;
        if (getAdapter() != null) {
            allCount = (getAdapter().getCount() - getHeaderViewsCount()) - getFooterViewsCount();
        }
        return visibleCount < allCount;
    }

    private void applyHeaderPadding(MotionEvent ev) {
        if (getScrollY() == 0) {
            int pointerCount = ev.getHistorySize();
            if (pointerCount > 0) {
                for (int p = 0; p < pointerCount; p++) {
                    if (isVerticalFadingEdgeEnabled()) {
                        setVerticalScrollBarEnabled(false);
                    }
                    int topPadding = (int) (((double) ((((int) ev.getHistoricalY(p)) - this.mLastMotionY) - this.mRefreshViewHeight)) / 1.7d);
                    if (topPadding >= 0) {
                        this.mRefreshView.setPadding(this.mRefreshView.getPaddingLeft(), topPadding, this.mRefreshView.getPaddingRight(), this.mRefreshView.getPaddingBottom());
                    }
                }
                return;
            }
            int topPadding2 = (int) (((double) ((ev.getY() - ((float) this.mLastMotionY)) - ((float) this.mRefreshViewHeight))) / 1.7d);
            if (topPadding2 >= 0) {
                this.mRefreshView.setPadding(this.mRefreshView.getPaddingLeft(), topPadding2, this.mRefreshView.getPaddingRight(), this.mRefreshView.getPaddingBottom());
            }
        }
    }

    private void resetHeaderPadding() {
        int header_top_gap = 0;
        if (this.mNeedBlankGapHeader) {
            header_top_gap = this.mGapHeaderView.getMeasuredHeight() + this.mGapHeaderView.getPaddingBottom() + this.mGapHeaderView.getPaddingTop();
        }
        this.mRefreshView.setPadding(this.mRefreshView.getPaddingLeft(), this.mRefreshOriginalTopPadding + header_top_gap, this.mRefreshView.getPaddingRight(), this.mRefreshView.getPaddingBottom());
    }

    private void resetHeader() {
        if (this.mRefreshState != 1) {
            this.mRefreshState = 1;
            openHeaderView();
            resetHeaderPadding();
            this.mRefreshViewImage.clearAnimation();
            return;
        }
        this.mRefreshView.setVisibility(8);
        this.mRefreshViewText.setVisibility(8);
        this.mRefreshViewImage.setVisibility(8);
        this.mRefreshViewProgress.setVisibility(8);
        this.mRefreshViewLastUpdated.setVisibility(8);
        this.mRefreshView.setPadding(0, 0, 0, 0);
    }

    /* access modifiers changed from: protected */
    public void openHeaderView() {
        this.mRefreshView.setVisibility(0);
        this.mRefreshViewText.setVisibility(0);
        if (this.mRefreshState == 2 || this.mRefreshState == 3) {
            if (this.mRefreshViewImage.getVisibility() != 0) {
                this.mRefreshViewImage.setVisibility(0);
            }
            if (this.mRefreshViewProgress.getVisibility() == 0) {
                this.mRefreshViewProgress.setVisibility(8);
            }
        } else if (this.mRefreshState == 4) {
            if (this.mRefreshViewProgress.getVisibility() != 0) {
                this.mRefreshViewProgress.setVisibility(0);
            }
            if (this.mRefreshViewImage.getVisibility() == 0) {
                this.mRefreshViewImage.setVisibility(8);
            }
        }
        this.mRefreshViewLastUpdated.setVisibility(0);
        this.mRefreshViewText.setText((int) R.string.pull_to_refresh_tap_label);
        this.mRefreshViewImage.setImageResource(R.drawable.ic_pulltorefresh_arrow);
    }

    private void measureView(View child) {
        int childHeightSpec;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(-1, -2);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        if (lpHeight > 0) {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.mEnableHeader && this.mCurrentScrollState == 1 && this.mRefreshState != 4 && judgeListFull()) {
            pullToRefresh(firstVisibleItem);
        } else if (this.mCurrentScrollState == 2) {
            if (this.mAllowGetMore && getLastVisiblePosition() == getCount() - 1 && this.mGetMoreState != 6 && this.mGetmoreView.getBottom() <= getBottom()) {
                this.mGetMoreState = 6;
                updateFooter(this.mHasMore);
                if (NetworkUtil.isWifiConnection(getContext())) {
                    this.mGetmoreView.setOnClickListener(null);
                    onGetMore();
                } else {
                    this.mGetmoreView.setOnClickListener(this);
                }
            } else if (this.mEnableHeader && firstVisibleItem == 0) {
                this.mVelocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                int velocityX = (int) this.mVelocityTracker.getXVelocity();
                if (this.mRefreshState != 4 && velocityX > 1000 && this.mRefreshView.getTop() >= 0) {
                    this.mRefreshState = 4;
                    openHeaderView();
                    checkLastUpdateTime();
                    prepareForRefresh();
                    onRefresh();
                } else if (this.mRefreshView.getTop() < 0) {
                    this.mRefreshViewText.setText((int) R.string.pull_to_refresh_pull_label);
                    this.mRefreshState = 2;
                }
                this.mBounceHack = true;
            }
        }
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }

    /* access modifiers changed from: protected */
    public void pullToRefresh(int firstVisibleItem) {
        boolean startAnimation = true;
        if (firstVisibleItem >= getFooterViewsCount()) {
            this.mRefreshViewImage.setVisibility(8);
            resetHeader();
        } else if (this.mRefreshView.getPaddingTop() > 20) {
            if (this.mRefreshState != 3) {
                this.mRefreshState = 3;
                openHeaderView();
                this.mRefreshViewText.setText((int) R.string.pull_to_refresh_release_label);
                this.mRefreshViewImage.clearAnimation();
                this.mRefreshViewImage.startAnimation(this.mFlipAnimation);
                checkLastUpdateTime();
            }
        } else if (this.mRefreshState != 2) {
            if (this.mRefreshState == 1) {
                startAnimation = false;
            }
            this.mRefreshState = 2;
            openHeaderView();
            this.mRefreshViewText.setText((int) R.string.pull_to_refresh_pull_label);
            if (startAnimation) {
                this.mRefreshViewImage.clearAnimation();
                this.mRefreshViewImage.startAnimation(this.mReverseFlipAnimation);
            }
            checkLastUpdateTime();
        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.mCurrentScrollState = scrollState;
        if (this.mCurrentScrollState == 0 && this.mBounceHack && !this.mTouchDown && this.mRefreshState == 2) {
            setSelection(0);
            this.mBounceHack = false;
        }
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScrollStateChanged(view, scrollState);
        }
    }

    public void prepareForRefresh() {
        this.mRefreshView.setVisibility(0);
        this.mRefreshViewText.setVisibility(0);
        this.mRefreshViewImage.setVisibility(8);
        this.mRefreshViewLastUpdated.setVisibility(0);
        resetHeaderPadding();
        this.mRefreshViewImage.setImageDrawable(null);
        this.mRefreshViewProgress.setVisibility(0);
        this.mRefreshViewText.setText((int) R.string.pull_to_refresh_refreshing_label);
        this.mRefreshState = 4;
    }

    public void onRefresh() {
        this.mNoInfoView.setVisibility(8);
        if (this.mOnRefreshListener != null) {
            this.mOnRefreshListener.onRefresh();
        }
    }

    public void onGetMore() {
        if (!this.mHasMore || this.mGetMoreListener == null) {
            this.mGetMoreState = 5;
        } else {
            this.mGetMoreListener.onGetMore();
        }
    }

    public void onFail() {
        if (this.mGetMoreState == 6) {
            this.mGetMoreState = 5;
        }
        if (this.mRefreshState == 4) {
            this.mRefreshState = 1;
            resetHeader();
        }
        invalidate();
    }

    public void onRefreshComplete() {
        this.mLastUpdateTimeMs = System.currentTimeMillis();
        this.mRefreshState = 1;
        resetHeader();
        if (this.mRefreshView.getBottom() > 0) {
            invalidateViews();
            if (this.mRefreshView.getTop() >= 0) {
                setSelection(0);
            }
        }
        updateFooter(true);
    }

    private ListAdapter findGoodListAdapter() {
        ListAdapter adapter = getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            return ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        }
        return adapter;
    }

    public void onGetMoreCompleted(E_GETMORE status) {
        boolean hasMore = true;
        switch (status) {
            case E_GETMORE_OK:
                invalidateViews();
                break;
            case E_GETMORE_NO_MORE:
                hasMore = false;
                break;
        }
        this.mGetMoreState = 5;
        updateFooter(hasMore);
    }

    private class OnClickRefreshListener implements View.OnClickListener {
        private OnClickRefreshListener() {
        }

        public void onClick(View v) {
            if (PullToRefreshListView.this.mRefreshState != 4) {
                PullToRefreshListView.this.prepareForRefresh();
                PullToRefreshListView.this.onRefresh();
            }
        }
    }

    private void updateFooterTip(int resId, boolean showProgress) {
        TextView text = (TextView) this.mGetmoreView.findViewById(R.id.pulldown_to_getmore);
        if (text != null) {
            text.setText(resId);
        }
        this.mGetmoreView.findViewById(R.id.loadingProgress).setVisibility(showProgress ? 0 : 8);
    }

    public void onClick(View v) {
        if (v == this.mGetmoreView) {
            updateFooterTip(R.string.scrolldown_to_getmore, true);
            this.mGetmoreView.setOnClickListener(null);
            onGetMore();
        }
    }

    public void setAdapter(ListAdapter adapter, boolean showFooter2) {
        super.setAdapter(adapter);
        this.showFooter = showFooter2;
    }
}
