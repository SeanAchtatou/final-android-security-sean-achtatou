package com.baixing.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.Util;

public class VerifyFailDialog extends DialogFragment {
    /* access modifiers changed from: private */
    public VerifyListener listener;

    public interface VerifyListener {
        void onReVerify(String str);

        void onSendVerifyCode(String str);
    }

    public VerifyFailDialog(VerifyListener listener2) {
        this.listener = listener2;
    }

    public Dialog onCreateDialog(Bundle savedInstanceBundle) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = inflater.inflate((int) R.layout.dialog_verify_fail, (ViewGroup) null);
        final EditText editPhoneEt = (EditText) v.findViewById(R.id.dialog_phone_et);
        builder.setView(v).setTitle("请输入验证码").setPositiveButton("重新验证", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int which) {
                if (VerifyFailDialog.this.listener != null) {
                    VerifyFailDialog.this.listener.onReVerify(editPhoneEt.getText().toString());
                }
            }
        }).setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (VerifyFailDialog.this.listener != null) {
                    VerifyFailDialog.this.listener.onSendVerifyCode(editPhoneEt.getText().toString());
                }
            }
        });
        AlertDialog altDlg = builder.create();
        altDlg.setCanceledOnTouchOutside(false);
        altDlg.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface dialog) {
                AlertDialog dlg = AlertDialog.class.cast(dialog);
                if (dlg != null) {
                    dlg.getButton(-1).setEnabled(false);
                }
                final DialogInterface dialogInterface = dialog;
                new CountDownTimer(Util.MILLSECONDS_OF_MINUTE, 1000) {
                    public void onTick(long millisUntilFinished) {
                        AlertDialog dlg = AlertDialog.class.cast(dialogInterface);
                        if (dlg != null) {
                            dlg.getButton(-1).setText("重新验证(" + String.valueOf(millisUntilFinished / 1000) + ")");
                        }
                    }

                    public void onFinish() {
                        AlertDialog dlg = AlertDialog.class.cast(dialogInterface);
                        if (dlg != null) {
                            dlg.getButton(-1).setText("重新验证");
                            dlg.getButton(-1).setEnabled(true);
                        }
                    }
                }.start();
            }
        });
        return altDlg;
    }
}
