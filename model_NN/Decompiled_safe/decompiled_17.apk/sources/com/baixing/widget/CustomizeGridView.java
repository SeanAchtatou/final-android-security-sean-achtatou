package com.baixing.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.baixing.imageCache.ImageLoaderManager;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.List;

public class CustomizeGridView extends LinearLayout implements View.OnClickListener {
    private int columnCount = 3;
    private List<GridInfo> gridItems = new ArrayList();
    private ItemClickListener itemClickListener;

    public static class GridInfo {
        public Bitmap img;
        public int number = 0;
        public String text;
    }

    public interface ItemClickListener {
        void onItemClick(GridInfo gridInfo, int i);
    }

    private static class GridHolder {
        ImageView imageBtn;
        public int index;
        public GridInfo info;
        public TextView text;

        private GridHolder() {
        }
    }

    public CustomizeGridView(Context context) {
        super(context);
    }

    public CustomizeGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setData(List<GridInfo> items, int columnCount2) {
        this.gridItems.clear();
        this.gridItems.addAll(items);
        construct();
    }

    public void setItemClickListener(ItemClickListener listener) {
        this.itemClickListener = listener;
    }

    private void construct() {
        int fixWidth = ((((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getWidth() - getPaddingLeft()) - getPaddingRight()) / this.columnCount;
        int lineCount = this.gridItems.size() / this.columnCount;
        if (this.gridItems.size() > this.columnCount * lineCount) {
            lineCount++;
        }
        removeAllViews();
        for (int i = 0; i < lineCount; i++) {
            LinearLayout line = new LinearLayout(getContext());
            line.setOrientation(0);
            for (int j = 0; j < this.columnCount; j++) {
                line.addView(getView((this.columnCount * i) + j), new LinearLayout.LayoutParams(fixWidth, fixWidth));
            }
            addView(line, new LinearLayout.LayoutParams(-1, -2));
        }
    }

    private void callback() {
    }

    public void updateLastView(String iconString, String nameString) {
        ViewGroup lastLine = (ViewGroup) getChildAt(getChildCount() - 1);
        View hotAppView = lastLine.getChildAt(lastLine.getChildCount() - 1);
        addFlag(hotAppView, R.drawable.icon_hot);
        ((LinearLayout) hotAppView.findViewById(R.id.id_griditem)).setVisibility(0);
        TextView textView = (TextView) hotAppView.findViewById(R.id.itemtext);
        final ImageView imageView = (ImageView) hotAppView.findViewById(R.id.itemicon);
        GlobalDataManager.getInstance().getImageLoaderMgr().loadImg(new ImageLoaderManager.DownloadCallback() {
            public void onFail(String url) {
                imageView.setImageResource(R.drawable.icon_category_app);
            }

            public void onSucced(String url, Bitmap bp) {
                imageView.setImageBitmap(bp);
            }
        }, iconString);
        if (nameString != null) {
            textView.setText(nameString);
        } else {
            textView.setText("热门应用");
        }
        hotAppView.setEnabled(true);
        invalidate();
    }

    public View getView(int index) {
        GridInfo info = null;
        View convertView = LayoutInflater.from(getContext()).inflate((int) R.layout.categorygriditem, (ViewGroup) null);
        GridHolder holder = new GridHolder();
        holder.imageBtn = (ImageView) convertView.findViewById(R.id.itemicon);
        holder.text = (TextView) convertView.findViewById(R.id.itemtext);
        convertView.setTag(holder);
        convertView.setOnClickListener(this);
        holder.index = index;
        if (index < this.gridItems.size()) {
            info = this.gridItems.get(index);
        }
        holder.info = info;
        if (info != null) {
            String text = info.text;
            if (info.number > 0) {
                text = String.format("%s(%d)", text, Integer.valueOf(info.number));
            }
            holder.text.setText(text);
            holder.imageBtn.setImageBitmap(info.img);
            convertView.setEnabled(true);
        } else {
            convertView.setEnabled(false);
        }
        if (index == 0) {
            convertView.setBackgroundResource(R.drawable.bg_grid_selector_left_top);
        } else if (index < this.columnCount) {
            convertView.setBackgroundResource(R.drawable.bg_grid_selector_top);
        } else if (index % this.columnCount == 0) {
            convertView.setBackgroundResource(R.drawable.bg_grid_selector_left);
        } else {
            if (index == this.gridItems.size() - 2) {
                addFlag(convertView, R.drawable.icon_body_flag);
            }
            if (index == this.gridItems.size() - 1) {
                ((LinearLayout) convertView.findViewById(R.id.id_griditem)).setVisibility(4);
                convertView.setEnabled(false);
            }
            convertView.setBackgroundResource(R.drawable.bg_grid_selector);
        }
        return convertView;
    }

    public void onClick(View v) {
        if (v != null && (v.getTag() instanceof GridHolder) && this.itemClickListener != null) {
            GridHolder holder = (GridHolder) v.getTag();
            this.itemClickListener.onItemClick(holder.info, holder.index);
        }
    }

    private void setIconAndText(View view, int resource, String name) {
        ((ImageView) view.findViewById(R.id.itemicon)).setImageResource(resource);
        ((TextView) view.findViewById(R.id.itemtext)).setText(name);
    }

    private void addFlag(View view, int resource) {
        ((ImageView) view.findViewById(R.id.item_flag)).setImageResource(resource);
    }
}
