package com.baixing.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import com.quanleimu.activity.R;

public class CustomDialog extends Dialog {
    private Button button = null;
    private Context context = null;
    private ListView listview_custom = null;

    public CustomDialog(Context context2) {
        super(context2, R.style.customize_dialog);
        this.context = context2;
    }

    public ListView getListView() {
        return this.listview_custom;
    }

    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.custom_shape);
        setContentView((int) R.layout.custom_dialog);
        this.listview_custom = (ListView) findViewById(R.id.listview_custom);
        this.button = (Button) findViewById(R.id.button_cancel);
        this.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CustomDialog.this.dismiss();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.d("TAG", "+++++++++++++++++++++++++++");
    }
}
