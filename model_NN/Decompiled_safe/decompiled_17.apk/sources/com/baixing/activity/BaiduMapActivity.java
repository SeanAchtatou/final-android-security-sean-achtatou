package com.baixing.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.CoordinateConvert;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.LocationListener;
import com.baidu.mapapi.MKGeneralListener;
import com.baidu.mapapi.MKLocationManager;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.MyLocationOverlay;
import com.baidu.mapapi.Overlay;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.BXLocation;
import com.baixing.network.NetworkCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Base64;
import com.baixing.util.LocationService;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.List;
import org.apache.commons.httpclient.HttpState;
import org.json.JSONObject;

public class BaiduMapActivity extends MapActivity implements LocationListener {
    public static String mStrKey = "736C4435847CB7D20DD1131064E35E8941C934F5";
    /* access modifiers changed from: private */
    public GeoPoint endGeoPoint;
    BMapManager mBMapMan = null;
    /* access modifiers changed from: private */
    public View popView;

    public static class MyGeneralListener implements MKGeneralListener {
        private Context context;
        boolean m_bKeyRight = true;

        MyGeneralListener(Context cxt) {
            this.context = cxt;
        }

        public void onGetNetworkState(int iError) {
            ViewUtil.showToast((BaiduMapActivity) this.context, "您的网络出错啦！", true);
        }

        public void onGetPermissionState(int iError) {
            if (iError == 300) {
                ViewUtil.showToast((BaiduMapActivity) this.context, "请在BMapApiDemoApp.java文件输入正确的授权Key！", true);
                this.m_bKeyRight = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mBMapMan != null) {
            this.mBMapMan.getLocationManager().removeUpdates(this);
            this.mBMapMan.destroy();
            this.mBMapMan = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mBMapMan != null) {
            this.mBMapMan.getLocationManager().removeUpdates(this);
            this.mBMapMan.stop();
        }
        super.onPause();
    }

    private void setTargetCoordinate(final Ad detail) {
        final String latV = detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_LAT);
        final String lonV = detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_LON);
        if (latV == null || latV.equals(HttpState.PREEMPTIVE_DEFAULT) || latV.equals("null") || latV.equals("") || latV.equals("0") || latV.equals("0.0") || lonV == null || lonV.equals(HttpState.PREEMPTIVE_DEFAULT) || lonV.equals("null") || lonV.equals("") || lonV.equals("0") || lonV.equals("0.0")) {
            new Thread(new Runnable() {
                public void run() {
                    if (GlobalDataManager.getInstance().getApplicationContext() != null) {
                        String address = GlobalDataManager.getInstance().cityName + detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_AREANAME);
                        if (!address.equals("")) {
                            BXLocation bxloc = LocationService.retreiveCoorFromGoogle(address);
                            BaiduMapActivity.this.applyToMap(String.valueOf((int) (((double) bxloc.fGeoCodedLat) * 1000000.0d)), String.valueOf((int) (((double) bxloc.fGeoCodedLon) * 1000000.0d)));
                        }
                    }
                }
            }).start();
        } else {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        JSONObject js = new JSONObject(NetworkCommand.doGet(BaiduMapActivity.this, String.format("http://api.map.baidu.com/ag/coord/convert?from=2&to=4&x=%s&y=%s", latV, lonV)));
                        Object errorCode = js.get("error");
                        if ((errorCode instanceof Integer) && ((Integer) errorCode).intValue() == 0) {
                            String x = new String(Base64.decode((String) js.get("x")), "UTF-8");
                            String y = new String(Base64.decode((String) js.get("y")), "UTF-8");
                            Double dx = Double.valueOf(x);
                            Double dy = Double.valueOf(y);
                            BaiduMapActivity.this.applyToMap(String.valueOf((int) (dx.doubleValue() * 1000000.0d)), String.valueOf((int) (dy.doubleValue() * 1000000.0d)));
                            return;
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    BaiduMapActivity.this.applyToMap(String.valueOf((int) (Double.valueOf(latV).doubleValue() * 1000000.0d)), String.valueOf((int) (Double.valueOf(lonV).doubleValue() * 1000000.0d)));
                }
            }).start();
        }
    }

    /* access modifiers changed from: private */
    public void applyToMap(final String x, final String y) {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    GeoPoint unused = BaiduMapActivity.this.endGeoPoint = new GeoPoint(Integer.parseInt(x), Integer.parseInt(y));
                    MapView mapView = (MapView) BaiduMapActivity.this.findViewById(R.id.bmapsView);
                    BMapManager manager = BaiduMapActivity.this.mBMapMan;
                    if (mapView != null && manager != null) {
                        MapController mapController = mapView.getController();
                        mapController.animateTo(BaiduMapActivity.this.endGeoPoint);
                        mapController.setZoom(15);
                        List<Overlay> overlays = mapView.getOverlays();
                        if (overlays != null) {
                            overlays.add(new MyPositionOverlays(BaiduMapActivity.this.endGeoPoint));
                        }
                        MKLocationManager locationManager = manager.getLocationManager();
                        locationManager.setLocationCoordinateType(0);
                        Location location = locationManager.getLocationInfo();
                        if (location != null) {
                            GeoPoint point = CoordinateConvert.bundleDecode(CoordinateConvert.fromWgs84ToBaidu(new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d))));
                            location.setLatitude((1.0d * ((double) point.getLatitudeE6())) / 1000000.0d);
                            location.setLongitude((1.0d * ((double) point.getLongitudeE6())) / 1000000.0d);
                            BaiduMapActivity.this.updateMyLocationOverlay(location);
                        } else {
                            locationManager.requestLocationUpdates(BaiduMapActivity.this);
                        }
                        manager.start();
                        if (BaiduMapActivity.this.popView != null) {
                            MapView.LayoutParams geoLP = (MapView.LayoutParams) BaiduMapActivity.this.popView.getLayoutParams();
                            geoLP.point = BaiduMapActivity.this.endGeoPoint;
                            mapView.updateViewLayout(BaiduMapActivity.this.popView, geoLP);
                            BaiduMapActivity.this.popView.setVisibility(0);
                        }
                    }
                } catch (Throwable th) {
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Ad position;
        String[] aryArea;
        super.onResume();
        if (this.mBMapMan != null) {
            this.mBMapMan.start();
        }
        MapView mapView = (MapView) findViewById(R.id.bmapsView);
        MapController mapController = mapView.getController();
        Location location = LocationService.getInstance().getLastKnownLocation();
        if (location != null) {
            GeoPoint point = CoordinateConvert.bundleDecode(CoordinateConvert.fromWgs84ToBaidu(new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d))));
            mapController.setCenter(new GeoPoint(point.getLatitudeE6(), point.getLongitudeE6()));
        }
        mapView.setBuiltInZoomControls(true);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && (position = (Ad) bundle.getSerializable("detail")) != null) {
            Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.VIEWADMAP).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, position.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, position.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).end();
            String areaname = position.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_AREANAME);
            if (!(areaname == null || (aryArea = areaname.split(",")) == null || aryArea.length <= 0)) {
                ((TextView) findViewById(R.id.tvTitle)).setText(aryArea[aryArea.length - 1]);
            }
            setTargetCoordinate(position);
            if (this.popView != null) {
                ((TextView) this.popView.findViewById(R.id.map_bubbleTitle)).setText(position.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE));
                ((TextView) this.popView.findViewById(R.id.map_bubbleText)).setText(position.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_AREANAME));
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        setTheme((int) R.style.lightTheme);
        super.onCreate(savedInstanceState);
        if (GlobalDataManager.context == null) {
            GlobalDataManager.context = new WeakReference<>(this);
        }
        setContentView((int) R.layout.baidumaplayout);
        findViewById(R.id.search_action).setVisibility(8);
        findViewById(R.id.right_action).setVisibility(8);
        if (savedInstanceState == null) {
            ((TextView) findViewById(R.id.tvTitle)).setText("位置");
        }
        if (this.mBMapMan == null) {
            this.mBMapMan = new BMapManager(GlobalDataManager.getInstance().getApplicationContext());
            this.mBMapMan.init(mStrKey, new MyGeneralListener(this));
        }
        findViewById(R.id.left_action).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaiduMapActivity.this.finish();
            }
        });
        findViewById(R.id.left_action).setPadding(0, 0, 0, 0);
        super.initMapActivity(this.mBMapMan);
        MapView mapView = (MapView) findViewById(R.id.bmapsView);
        if (this.popView == null) {
            this.popView = LayoutInflater.from(this).inflate((int) R.layout.map_bubble, (ViewGroup) null);
        }
        mapView.addView(this.popView, new MapView.LayoutParams(-2, -2, null, 81));
        this.popView.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void updateMyLocationOverlay(Location location) {
        if (location != null) {
            try {
                if (this.endGeoPoint != null) {
                    GeoPoint geoPoint = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
                    GeoPoint midPoint = new GeoPoint((this.endGeoPoint.getLatitudeE6() + geoPoint.getLatitudeE6()) / 2, (this.endGeoPoint.getLongitudeE6() + geoPoint.getLongitudeE6()) / 2);
                    int latSpan = Math.abs(this.endGeoPoint.getLatitudeE6() - geoPoint.getLatitudeE6());
                    int longSpan = Math.abs(this.endGeoPoint.getLongitudeE6() - geoPoint.getLongitudeE6());
                    MapView mapView = (MapView) findViewById(R.id.bmapsView);
                    if (mapView != null) {
                        MyLocationOverlay mylocationOverlay = new MyLocationOverlay(this, mapView);
                        mylocationOverlay.enableMyLocation();
                        mapView.getOverlays().add(mylocationOverlay);
                        mapView.getController().animateTo(midPoint);
                        mapView.getController().zoomToSpan(latSpan * 2, longSpan * 2);
                    }
                }
            } catch (Throwable th) {
            }
        }
    }

    class MyPositionOverlays extends Overlay {
        GeoPoint geoPoint;

        public MyPositionOverlays(GeoPoint geoPoint2) {
            this.geoPoint = geoPoint2;
        }

        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            super.draw(canvas, mapView, shadow);
            Point point = new Point();
            mapView.getProjection().toPixels(this.geoPoint, point);
            Paint paint = new Paint();
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inPurgeable = true;
            Bitmap bmp = BitmapFactory.decodeResource(BaiduMapActivity.this.getResources(), R.drawable.red, o);
            canvas.drawBitmap(bmp, (float) point.x, (float) point.y, paint);
            bmp.recycle();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
     candidates:
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
    public void onLocationChanged(Location location) {
        if (location != null) {
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.GPS).append(TrackConfig.TrackMobile.Key.GPS_RESULT, true).append(TrackConfig.TrackMobile.Key.GPS_GEO, String.format("(%f,%f)", Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()))).end();
        } else {
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.GPS).append(TrackConfig.TrackMobile.Key.GPS_RESULT, false).end();
        }
        if (location != null && isInChina(location)) {
            GeoPoint point = CoordinateConvert.bundleDecode(CoordinateConvert.fromWgs84ToBaidu(new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d))));
            location.setLatitude((((double) point.getLatitudeE6()) * 1.0d) / 1000000.0d);
            location.setLongitude((((double) point.getLongitudeE6()) * 1.0d) / 1000000.0d);
            if (isInChina(location)) {
                updateMyLocationOverlay(location);
            }
        }
        this.mBMapMan.getLocationManager().removeUpdates(this);
    }

    private boolean isInChina(Location location) {
        return location.getLatitude() >= 3.0d && location.getLatitude() <= 54.0d && location.getLongitude() >= 73.0d && location.getLongitude() <= 136.0d;
    }
}
