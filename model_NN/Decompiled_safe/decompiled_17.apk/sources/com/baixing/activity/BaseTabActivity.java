package com.baixing.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import com.baixing.data.GlobalDataManager;
import com.baixing.database.ChatMessageDatabase;
import com.baixing.entity.Ad;
import com.baixing.entity.UserBean;
import com.baixing.network.NetworkProfiler;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.sharing.referral.RegisterOrLoginDlg;
import com.baixing.tracking.Sender;
import com.baixing.tracking.Tracker;
import com.baixing.util.LocationService;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.view.AdViewHistory;
import com.baixing.view.CustomizeTabHost;
import com.baixing.view.fragment.ListingFragment;
import com.baixing.view.fragment.LoginFragment;
import com.baixing.view.fragment.PostGoodsFragment;
import com.quanleimu.activity.R;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.HttpState;

public class BaseTabActivity extends BaseActivity implements CustomizeTabHost.TabSelectListener, IExit {
    public static final String LIFE_TAG = MainActivity.class.getSimpleName();
    private static final String SHARE_PREFS_NAME = "baixing_shortcut_app";
    public static final int TAB_INDEX_CAT = 1;
    public static final int TAB_INDEX_HOME = 0;
    public static final int TAB_INDEX_PERSONAL = 2;
    public static final int TAB_INDEX_POST = 3;
    protected static int currentMainIndex = 1;
    protected static Map<Integer, Boolean> instanceList = new HashMap();
    protected static boolean isExitingApp = false;
    protected CustomizeTabHost globalTabCtrl;
    protected boolean isChangingTab = false;
    protected int originalAppHash = 0;

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (this.originalAppHash != 0) {
            savedInstanceState.putInt("appHash", this.originalAppHash);
        }
        if (this.globalTabCtrl != null) {
            this.globalTabCtrl.setCurrentFocusIndex(getTabIndex());
            savedInstanceState.putSerializable("tabCtrl", this.globalTabCtrl);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (instanceList.containsKey(Integer.valueOf(hashCode())) && instanceList.get(Integer.valueOf(hashCode())).booleanValue()) {
            instanceList.remove(Integer.valueOf(hashCode()));
            finish();
        } else if (this.originalAppHash != 0 && GlobalDataManager.isAppDestroy(this.originalAppHash)) {
            finish();
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Log.w(LIFE_TAG, hashCode() + " activity is created for class " + getClass().getName());
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.originalAppHash = savedInstanceState.getInt("appHash", 0);
        } else {
            this.originalAppHash = GlobalDataManager.getInstance().hashCode();
        }
        instanceList.put(Integer.valueOf(hashCode()), false);
        if (savedInstanceState != null) {
            this.globalTabCtrl = (CustomizeTabHost) savedInstanceState.get("tabCtrl");
        }
        if (this.globalTabCtrl == null) {
            String tabHomepage = getString(R.string.tab_homepage);
            String tabBrowse = getString(R.string.tab_browse);
            String tabUserCenter = getString(R.string.tab_user_center);
            String tabPost = getString(R.string.tab_post);
            this.globalTabCtrl = CustomizeTabHost.createTabHost(getTabIndex(), new String[]{tabHomepage, tabBrowse, tabUserCenter, tabPost}, new CustomizeTabHost.TabIconRes[]{new CustomizeTabHost.TabIconRes(R.drawable.icon_footer_home_on, R.drawable.icon_footer_home), new CustomizeTabHost.TabIconRes(R.drawable.icon_footer_category_on, R.drawable.icon_footer_category), new CustomizeTabHost.TabIconRes(R.drawable.icon_footer_profile_on, R.drawable.icon_footer_profile), new CustomizeTabHost.TabIconRes(R.drawable.icon_footer_post_on, R.drawable.icon_footer_post)});
        }
        this.globalTabCtrl.setCurrentFocusIndex(getTabIndex());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        BaseFragment f;
        super.onNewIntent(intent);
        if (intent.getBooleanExtra("changeTab", false)) {
            this.isChangingTab = true;
        } else {
            this.isChangingTab = false;
        }
        if (!handleNewIntent(intent) && (f = getCurrentFragment()) != null && intent.getBooleanExtra("extra.common.isThirdParty", false)) {
            f.onActivityResult(intent.getIntExtra("extra.image.reqcode", -1), intent.getIntExtra("extra.common.resultCode", -1), (Intent) intent.getExtras().get("extra.common.data"));
        }
    }

    /* access modifiers changed from: protected */
    public boolean handleNewIntent(Intent intent) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        instanceList.remove(Integer.valueOf(hashCode()));
        Log.w(LIFE_TAG, hashCode() + " activity is destroy " + getClass().getName());
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.isChangingTab = false;
        currentMainIndex = getTabIndex();
        this.globalTabCtrl.showTab(getTabIndex());
        SharedPreferences status = getSharedPreferences(ReferralUtil.REFERRAL_STATUS, 0);
        if (!status.getBoolean(ReferralUtil.DLGSHOWN_KEY, false)) {
            Log.v("registertest", HttpState.PREEMPTIVE_DEFAULT);
            RegisterOrLoginDlg rld = new RegisterOrLoginDlg();
            rld.setHandler(new Handler() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
                 arg types: [com.baixing.view.fragment.LoginFragment, android.os.Bundle, int]
                 candidates:
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case RegisterOrLoginDlg.REQ_LOGIN /*1010*/:
                            BaseTabActivity.this.pushFragment((BaseFragment) new LoginFragment(), new Bundle(), false);
                            return;
                        default:
                            return;
                    }
                }
            });
            rld.show(getSupportFragmentManager(), "EasyRegister");
            SharedPreferences.Editor editor = status.edit();
            editor.putBoolean(ReferralUtil.DLGSHOWN_KEY, true);
            editor.commit();
            return;
        }
        Log.v("registertest", "true");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public int getTabIndex() {
        return 0;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        handleBack();
        return true;
    }

    /* access modifiers changed from: protected */
    public void initTitleAction() {
    }

    public void handleBack() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(findViewById(R.id.contentLayout).getWindowToken(), 0);
        BaseFragment currentFragmet = getCurrentFragment();
        if (currentFragmet != null) {
            try {
                if (currentFragmet.handleBack()) {
                    return;
                }
                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    popFragment(currentFragmet);
                } else if (this.exitCtrl.requestExit()) {
                    exitMainActivity();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onClick(View v) {
        Log.d("tab", "onClick");
        switch (v.getId()) {
            case R.id.left_action /*2131165258*/:
                handleBack();
                break;
            case R.id.search_action /*2131165263*/:
                if (getCurrentFragment() != null) {
                    getCurrentFragment().handleSearch();
                    break;
                }
                break;
            case R.id.right_action /*2131165265*/:
                getCurrentFragment().handleRightAction();
                break;
        }
        super.onClick(v);
    }

    public void exitMainActivity() {
        LocationService.getInstance().stop();
        GlobalDataManager.getInstance().getNetworkCacheManager().deleteOldRecorders(259200);
        ChatMessageDatabase.prepareDB(this);
        ChatMessageDatabase.clearOldMessage(1000);
        try {
            Sender.getInstance().save();
            Tracker.getInstance().save();
        } catch (Exception e) {
        }
        Log.d("quanleimu", "exit");
        AdViewHistory.getInstance().clearHistory();
        List<Ad> favList = GlobalDataManager.getInstance().getListMyStore();
        UserBean user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (favList != null && (user == null || TextUtils.isEmpty(user.getPassword()))) {
            Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "listMyStore", favList);
        }
        for (Integer key : instanceList.keySet()) {
            instanceList.put(key, true);
        }
        instanceList.remove(Integer.valueOf(hashCode()));
        GlobalDataManager.resetApplication();
        NetworkProfiler.flush();
        finish();
    }

    public void beforeChange(int currentIndex, int nextIndex) {
    }

    public void deprecatSelect(int currentIndex) {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            FragmentTransaction ft = fm.beginTransaction();
            if (this.firstFragmentId == -1) {
                fm.popBackStack();
            } else {
                fm.popBackStack(this.firstFragmentId, 1);
            }
            ft.commit();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void afterChange(int newSelectIndex) {
        Intent intent = new Intent();
        switch (newSelectIndex) {
            case 0:
                intent.setClass(this, MainActivity.class);
                break;
            case 1:
                intent.setClass(this, CategoryActivity.class);
                break;
            case 2:
                intent.setClass(this, PersonalActivity.class);
                break;
            case 3:
                PerformanceTracker.stamp(PerformEvent.Event.E_Start_PostActivity);
                BaseFragment bf = getCurrentFragment();
                if (bf != null && (bf instanceof ListingFragment)) {
                    intent.putExtra(PostGoodsFragment.KEY_INIT_CATEGORY, ((ListingFragment) bf).getCategoryNames());
                }
                intent.setClass(this, PostActivity.class);
                break;
        }
        intent.addFlags(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
        intent.addFlags(4194304);
        intent.addFlags(65536);
        intent.putExtra("changeTab", true);
        this.isChangingTab = true;
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public final void onSetRootView(final View rootV) {
        rootV.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                int i = 8;
                BaseFragment currentF = BaseTabActivity.this.getCurrentFragment();
                if (rootV.getRootView().getHeight() - rootV.getHeight() > 100) {
                    BaseTabActivity.this.findViewById(R.id.common_tab_layout).setVisibility(8);
                    return;
                }
                View findViewById = BaseTabActivity.this.findViewById(R.id.common_tab_layout);
                if (currentF != null && currentF.hasGlobalTab()) {
                    i = 0;
                }
                findViewById.setVisibility(i);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStatckTop(BaseFragment f) {
        findViewById(R.id.tab_parent).setVisibility(f.hasGlobalTab() ? 0 : 8);
    }

    public void handleFragmentAction() {
        handleBack();
    }
}
