package com.baixing.activity;

import android.os.Handler;
import android.os.Message;
import com.baixing.data.AccountManager;
import com.baixing.data.GlobalDataManager;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.util.LocationService;
import com.baixing.util.MobileConfig;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.tencent.tauth.Constants;
import org.json.JSONException;
import org.json.JSONObject;

public class SplashJob {
    protected final int MSG_LOAD_ALLCATEGORY_LIST = 3;
    protected final int MSG_LOAD_CITY_LIST = 1;
    protected final int MSG_LOAD_HISTORY_STORED = 2;
    /* access modifiers changed from: private */
    public boolean isJobDone;
    private boolean isJobStarted;
    JobDoneListener jobListener;
    public String[] listRemark = new String[0];
    Handler myHandler = new Handler() {
        private int record1 = 0;
        private int record2 = 0;
        private int record3 = 0;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.record1 = 1;
                    break;
                case 2:
                    this.record2 = 1;
                    break;
                case 3:
                    this.record3 = 1;
                    break;
            }
            if (1 == this.record1 && 1 == this.record2 && 1 == this.record3) {
                boolean unused = SplashJob.this.isJobDone = true;
                PerformanceTracker.stamp(PerformEvent.Event.E_End_Init_Data);
                PerformanceTracker.stamp(PerformEvent.Event.E_Init_Image_Mgr);
                GlobalDataManager.getInstance().getImageManager();
                PerformanceTracker.stamp(PerformEvent.Event.E_Init_Image_Mgr_Done);
                SplashJob.this.jobListener.onJobDone();
            }
        }
    };
    /* access modifiers changed from: private */
    public BaseActivity parentActivity;

    public interface JobDoneListener {
        void onJobDone();
    }

    SplashJob(BaseActivity activity, JobDoneListener listener) {
        this.parentActivity = activity;
        this.jobListener = listener;
    }

    public void doSplashWork() {
        PerformanceTracker.stamp(PerformEvent.Event.E_DoSplash_Begin);
        if (!this.isJobStarted && !this.isJobDone) {
            this.isJobStarted = true;
            LocationService.getInstance().start(this.parentActivity, GlobalDataManager.getInstance().getLocationManager());
            try {
                if (!this.parentActivity.checkConnection()) {
                    ViewUtil.showToast(this.parentActivity, "网络连接异常", false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            new Thread(new LoadConfigTask()).start();
            PerformanceTracker.stamp(PerformEvent.Event.E_Call_LoadConfigTask);
        }
    }

    public boolean isJobDone() {
        return this.isJobDone;
    }

    class ReadCateListThread implements Runnable {
        ReadCateListThread() {
        }

        public void run() {
            PerformanceTracker.stamp(PerformEvent.Event.E_Begin_ReadCategory);
            GlobalDataManager.getInstance().loadCategorySync();
            SplashJob.this.myHandler.sendEmptyMessage(3);
            PerformanceTracker.stamp(PerformEvent.Event.E_End_ReadCategory);
        }
    }

    class LoadConfigTask implements Runnable {
        LoadConfigTask() {
        }

        public void run() {
            try {
                PerformanceTracker.stamp(PerformEvent.Event.E_SyncMobileConfig_Begin);
                MobileConfig.getInstance().syncMobileConfig();
            } catch (Throwable th) {
            } finally {
                PerformanceTracker.stamp(PerformEvent.Event.E_Begin_Init_Data);
                new Thread(new ReadCityListThread()).start();
                new Thread(new ReadInfoThread()).start();
                new Thread(new ReadCateListThread()).start();
                new Thread(new GetEventInfoThread()).start();
            }
        }
    }

    class ReadCityListThread implements Runnable {
        ReadCityListThread() {
        }

        public void run() {
            PerformanceTracker.stamp(PerformEvent.Event.E_Begin_ReadCity);
            GlobalDataManager.getInstance().loadCitySync();
            SplashJob.this.myHandler.sendEmptyMessage(1);
            PerformanceTracker.stamp(PerformEvent.Event.E_End_ReadCity);
        }
    }

    class ReadInfoThread implements Runnable {
        ReadInfoThread() {
        }

        public void run() {
            PerformanceTracker.stamp(PerformEvent.Event.E_Begin_ReadPersonalInfo);
            GlobalDataManager.getInstance().loadPersonalSync();
            SplashJob.this.myHandler.sendEmptyMessage(2);
            PerformanceTracker.stamp(PerformEvent.Event.E_End_ReadPersonalInfo);
        }
    }

    class GetEventInfoThread implements Runnable {
        GetEventInfoThread() {
        }

        public void run() {
            JSONObject result;
            JSONObject error;
            AccountManager am = GlobalDataManager.getInstance().getAccountManager();
            ApiParams params = new ApiParams();
            params.addParam(ApiParams.KEY_USERID, am.isUserLogin() ? am.getCurrentUser().getId().substring(1) : "");
            params.addParam("cityEnglishName", GlobalDataManager.getInstance().getCityEnglishName());
            try {
                JSONObject obj = new JSONObject(BaseApiCommand.createCommand("Promo.getEvent/", false, params).executeSync(SplashJob.this.parentActivity));
                if (obj != null && (error = (result = obj.getJSONObject("result")).getJSONObject("error")) != null) {
                    String code = error.getString("code");
                    if (code == null || !code.equals("0")) {
                        Util.deleteDataFromLocate(SplashJob.this.parentActivity.getApplicationContext(), ReferralUtil.LOCATE_EVENT_INFO);
                        Util.deleteDataFromLocate(SplashJob.this.parentActivity.getApplicationContext(), ReferralUtil.LOCATE_EVENT_URL);
                        return;
                    }
                    Util.saveDataToLocate(SplashJob.this.parentActivity.getApplicationContext(), ReferralUtil.LOCATE_EVENT_INFO, result.getString("content"));
                    Util.saveDataToLocate(SplashJob.this.parentActivity.getApplicationContext(), ReferralUtil.LOCATE_EVENT_URL, result.getString(Constants.PARAM_URL));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
