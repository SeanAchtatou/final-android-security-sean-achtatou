package com.baixing.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.BXLocation;
import com.baixing.entity.BXThumbnail;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.BitmapUtils;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.util.hardware.AccelerometerSensorDetector;
import com.baixing.util.hardware.OrientationSensorDetector;
import com.baixing.util.hardware.RotationDetector;
import com.baixing.util.post.ImageUploader;
import com.baixing.view.CameraPreview;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class CameraActivity extends Activity implements View.OnClickListener, SensorEventListener {
    private static final int MAX_IMG_COUNT = 6;
    private static final int MIN_PICTURE_TAKEN_GAP = 500;
    private static final int MSG_CANCEL_STORE_PIC = 8;
    private static final int MSG_INIT_CAME = 7;
    private static final int MSG_ORIENTATION_CHANGE = 2;
    private static final int MSG_PAUSE_ME = 6;
    private static final int MSG_PIC_TAKEN = 0;
    private static final int MSG_RESUME_ME = 9;
    private static final int MSG_SAVE_DONE = 1;
    private static final int MSG_TAKEPIC_DELAY = 10;
    private static final int MSG_UPDATE_THUMBNAILS = 3;
    private static final int MSG_UPLOADING_STATUS_UPDATE = 4;
    private static final int REQ_PICK_GALLERY = 1;
    private static final int STATE_DONE = 3;
    private static final int STATE_FAIL = 2;
    private static final int STATE_UPLOADING = 1;
    public static final String TAG = "CAMPREV";
    RotationDetector.Orien currentOrien = RotationDetector.Orien.DEFAULT;
    /* access modifiers changed from: private */
    public Vector<BXThumbnail> deleteList = new Vector<>();
    private OnDeleteListener deleteListener;
    private RotationDetector detector = new RotationDetector() {
        public RotationDetector.Orien updateSensorEvent(SensorEvent sensorEvent) {
            return RotationDetector.Orien.DEFAULT;
        }
    };
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public Vector<BXThumbnail> imageList = new Vector<>();
    /* access modifiers changed from: private */
    public boolean isCameraLock = false;
    boolean isFrontCam;
    /* access modifiers changed from: private */
    public boolean isInitialized = false;
    boolean isLandscapeMode;
    private long lastClickTime = 0;
    Camera mCamera;
    /* access modifiers changed from: private */
    public Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            boolean unused = CameraActivity.this.isCameraLock = false;
            if (!Util.isExternalStorageWriteable()) {
                ViewUtil.showToast(CameraActivity.this, "请检查SD卡状态", false);
                CameraActivity.this.handler.sendEmptyMessage(8);
                return;
            }
            CameraActivity.this.postAppendPhoto(data);
        }
    };
    /* access modifiers changed from: private */
    public CameraPreview mPreview;
    private Vector<BXThumbnail> originalList = new Vector<>();
    /* access modifiers changed from: private */
    public Sensor sensor;
    /* access modifiers changed from: private */
    public SensorManager sensorMgr;

    /* access modifiers changed from: private */
    public void updateCapState() {
        boolean enable;
        boolean z = true;
        if (this.imageList.size() < 6) {
            enable = true;
        } else {
            enable = false;
        }
        View findViewById = findViewById(R.id.cap);
        if (!enable || !this.isInitialized) {
            z = false;
        }
        findViewById.setEnabled(z);
        findViewById(R.id.pick_gallery).setEnabled(enable);
    }

    class InternalHandler extends Handler {
        InternalHandler() {
        }

        public void handleMessage(Message msg) {
            Camera cam;
            switch (msg.what) {
                case 0:
                    Camera cam2 = CameraActivity.this.mCamera;
                    if (CameraActivity.this.isInitialized && cam2 != null) {
                        Log.d(CameraActivity.TAG, "try start another preview!");
                        cam2.startPreview();
                    }
                    Log.d(CameraActivity.TAG, "start another preview succed!");
                    return;
                case 1:
                    BXThumbnail newPicPair = (BXThumbnail) msg.obj;
                    if (newPicPair != null) {
                        ImageUploader.getInstance().startUpload(newPicPair.getLocalPath(), newPicPair.getThumbnail(), null);
                        if (CameraActivity.this.appendResultImage(newPicPair)) {
                            CameraActivity.this.addImageUri(newPicPair);
                        }
                    } else {
                        ViewUtil.showToast(CameraActivity.this, "获取照片失败", false);
                    }
                    CameraActivity.this.updateCapState();
                    return;
                case 2:
                    View capV = CameraActivity.this.findViewById(R.id.cap);
                    if (CameraActivity.this.isInitialized) {
                        Camera.Parameters params = CameraActivity.this.mCamera.getParameters();
                        params.setRotation(CameraActivity.this.currentOrien.orientationDegree);
                        CameraActivity.this.mCamera.setParameters(params);
                    }
                    if (CameraActivity.this.isInitialized && capV != null && capV.isEnabled()) {
                        CameraActivity.this.autoFocusWhenOrienChange();
                        return;
                    }
                    return;
                case 3:
                    if (CameraActivity.this.imageList == null || CameraActivity.this.imageList.size() <= 0) {
                        ViewUtil.showToast(CameraActivity.this, CameraActivity.this.getString(R.string.tip_camera_before_post), true);
                        return;
                    }
                    Iterator i$ = CameraActivity.this.imageList.iterator();
                    while (i$.hasNext()) {
                        boolean unused = CameraActivity.this.appendResultImage((BXThumbnail) i$.next());
                    }
                    return;
                case 4:
                    Pair<Bitmap, ImageView> p = (Pair) msg.obj;
                    if (((ImageView) p.second).getParent() != null && ((View) ((ImageView) p.second).getParent()).getTag() != null) {
                        Log.w(CameraActivity.TAG, "start to update view " + ((ImageView) p.second).getParent().hashCode());
                        if (p.second != null) {
                            switch (msg.arg1) {
                                case 1:
                                    ((View) ((ImageView) p.second).getParent()).findViewById(R.id.loading_status).setVisibility(0);
                                    ((ImageView) p.second).setImageBitmap((Bitmap) p.first);
                                    return;
                                case 2:
                                    ((View) ((ImageView) p.second).getParent()).findViewById(R.id.loading_status).setVisibility(8);
                                    ((ImageView) p.second).setImageResource(R.drawable.icon_load_fail);
                                    return;
                                case 3:
                                    ((View) ((ImageView) p.second).getParent()).findViewById(R.id.loading_status).setVisibility(8);
                                    ((ImageView) p.second).setImageBitmap((Bitmap) p.first);
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case 5:
                default:
                    return;
                case 6:
                    boolean unused2 = CameraActivity.this.isInitialized = false;
                    CameraActivity.this.sensorMgr.unregisterListener(CameraActivity.this);
                    ViewGroup cameraP = (ViewGroup) CameraActivity.this.findViewById(R.id.camera_parent).findViewById(R.id.camera_root);
                    if (CameraActivity.this.mPreview != null) {
                        cameraP.removeView(CameraActivity.this.mPreview);
                    }
                    if (CameraActivity.this.mCamera != null) {
                        CameraActivity.this.mPreview.setCamera(null);
                        CameraActivity.this.mCamera.release();
                        CameraActivity.this.mCamera = null;
                        return;
                    }
                    return;
                case 7:
                    PerformanceTracker.stamp(PerformEvent.Event.E_Start_Init_Camera);
                    ViewGroup cameraP2 = (ViewGroup) CameraActivity.this.findViewById(R.id.camera_parent).findViewById(R.id.camera_root);
                    if (CameraActivity.this.mPreview != null) {
                        cameraP2.removeView(CameraActivity.this.mPreview);
                    }
                    CameraActivity.this.initializeCamera();
                    ((TextView) CameraActivity.this.findViewById(R.id.cam_not_available_tip)).setVisibility(CameraActivity.this.mCamera == null ? 0 : 8);
                    if (CameraActivity.this.mCamera != null) {
                        CameraPreview unused3 = CameraActivity.this.mPreview = new CameraPreview(CameraActivity.this);
                        CameraActivity.this.mPreview.setRotateDisplay(!CameraActivity.this.isLandscapeMode);
                        cameraP2.addView(CameraActivity.this.mPreview, -1, -1);
                        CameraActivity.this.mPreview.setCamera(CameraActivity.this.mCamera);
                    }
                    boolean unused4 = CameraActivity.this.isInitialized = true;
                    boolean unused5 = CameraActivity.this.isCameraLock = false;
                    Log.d(CameraActivity.TAG, "initialize camera done.");
                    CameraActivity.this.updateCapState();
                    PerformanceTracker.stamp(PerformEvent.Event.E_End_Init_Camera);
                    return;
                case 8:
                    CameraActivity.this.updateCapState();
                    Camera cam3 = CameraActivity.this.mCamera;
                    if (CameraActivity.this.isInitialized && cam3 != null) {
                        cam3.startPreview();
                        return;
                    }
                    return;
                case 9:
                    if (CameraActivity.this.sensor != null) {
                        CameraActivity.this.sensorMgr.registerListener(CameraActivity.this, CameraActivity.this.sensor, 3);
                        return;
                    }
                    return;
                case 10:
                    if (!((BooleanWrapper) msg.obj).isTrue && (cam = CameraActivity.this.mCamera) != null && !CameraActivity.this.isCameraLock) {
                        cam.cancelAutoFocus();
                        cam.takePicture(null, null, CameraActivity.this.mPicture);
                        return;
                    }
                    return;
            }
        }
    }

    private ArrayList<String> getLocalUrls() {
        ArrayList<String> list = new ArrayList<>();
        Iterator i$ = this.imageList.iterator();
        while (i$.hasNext()) {
            list.add(i$.next().getLocalPath());
        }
        return list;
    }

    /* access modifiers changed from: private */
    public void addImageUri(BXThumbnail p) {
        this.imageList.add(p);
        updateCapState();
        if (this.imageList.size() == 6) {
            finishTakenPic();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        cancelTakenPic();
        return true;
    }

    private void cancelTakenPic() {
        int resultCode = 0;
        onCancelEdit();
        if (!(getIntent() == null || getIntent().getExtras() == null)) {
            resultCode = getIntent().getExtras().getInt("extra.common.finishCode", 0);
        }
        Intent backIntent = (Intent) getIntent().getExtras().get("extra.common.intent");
        Bundle bundle = new Bundle();
        bundle.putBoolean("extra.common.isThirdParty", true);
        bundle.putInt("extra.image.reqcode", getIntent().getExtras().getInt("extra.image.reqcode"));
        bundle.putInt("extra.common.resultCode", resultCode);
        backIntent.putExtras(bundle);
        backIntent.addFlags(67108864);
        startActivity(backIntent);
        finish();
    }

    private void finishTakenPic() {
        Iterator i$ = this.deleteList.iterator();
        while (i$.hasNext()) {
            ImageUploader.getInstance().cancel(i$.next().getLocalPath());
        }
        Intent backIntent = (Intent) getIntent().getExtras().get("extra.common.intent");
        Bundle bundle = new Bundle();
        bundle.putBoolean("extra.common.isThirdParty", true);
        bundle.putInt("extra.image.reqcode", getIntent().getExtras().getInt("extra.image.reqcode"));
        bundle.putInt("extra.common.resultCode", -1);
        Intent data = new Intent();
        data.putStringArrayListExtra(CommonIntentAction.EXTRA_IMAGE_LIST, getLocalUrls());
        bundle.putParcelable("extra.common.data", data);
        backIntent.putExtras(bundle);
        backIntent.addFlags(67108864);
        startActivity(backIntent);
        finish();
    }

    /* access modifiers changed from: private */
    public void deleteImageUri(BXThumbnail t) {
        this.imageList.remove(t);
        this.deleteList.add(t);
        if (!this.originalList.contains(t.getLocalPath())) {
            ImageUploader.getInstance().cancel(t.getLocalPath());
        }
        updateCapState();
    }

    /* access modifiers changed from: private */
    public boolean appendResultImage(BXThumbnail thumbnail) {
        if (thumbnail == null) {
            return false;
        }
        ViewGroup vp = (ViewGroup) findViewById(R.id.result_parent);
        ViewGroup imageRoot = (ViewGroup) LayoutInflater.from(vp.getContext()).inflate((int) R.layout.single_image_layout, (ViewGroup) null);
        vp.addView(imageRoot);
        imageRoot.setTag(thumbnail.getLocalPath());
        final View deleteCmd = imageRoot.findViewById(R.id.delete_preview);
        deleteCmd.setVisibility(0);
        deleteCmd.setOnClickListener(this.deleteListener);
        deleteCmd.setTag(thumbnail);
        imageRoot.findViewById(R.id.delete_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteCmd.performClick();
            }
        });
        try {
            ImageView img = (ImageView) imageRoot.findViewById(R.id.result_image);
            if (thumbnail.getThumbnail() != null) {
                img.setImageBitmap(thumbnail.getThumbnail());
            }
            UploadingCallback cbk = new UploadingCallback(img);
            imageRoot.setTag(cbk);
            ImageUploader.getInstance().registerCallback(thumbnail.getLocalPath(), cbk);
            ((TextView) findViewById(R.id.right_btn_txt)).setText("完成");
            return true;
        } catch (Throwable th) {
            Log.d(TAG, "error when add image view " + imageRoot);
            return false;
        }
    }

    class UploadingCallback implements ImageUploader.Callback {
        private boolean disable = false;
        WeakReference<ImageView> viewRef;

        UploadingCallback(ImageView v) {
            this.viewRef = new WeakReference<>(v);
        }

        public void disable() {
            this.disable = true;
        }

        public void onUploadDone(String imagePath, String serverUrl, Bitmap thumbnail) {
            notifyHandler(3, imagePath, thumbnail);
        }

        public void onUploading(String imagePath, Bitmap thumbnail) {
            notifyHandler(1, imagePath, thumbnail);
        }

        public void onUploadFail(String imagePath, Bitmap thumbnail) {
            notifyHandler(2, imagePath, thumbnail);
        }

        private void notifyHandler(int status, String imagePath, Bitmap thumbnail) {
            Log.d(CameraActivity.TAG, "image status update to : " + status);
            if (thumbnail != null) {
                Iterator i$ = CameraActivity.this.imageList.iterator();
                while (i$.hasNext()) {
                    BXThumbnail t = (BXThumbnail) i$.next();
                    if (imagePath.equals(t.getLocalPath())) {
                        t.setThumbnail(thumbnail);
                    }
                }
            }
            if (!this.disable && this.viewRef.get() != null && CameraActivity.this.handler != null) {
                Message msg = CameraActivity.this.handler.obtainMessage();
                msg.what = 4;
                msg.arg1 = status;
                msg.obj = new Pair(thumbnail, this.viewRef.get());
                CameraActivity.this.handler.sendMessage(msg);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        PerformanceTracker.stamp(PerformEvent.Event.E_CameraActivity_OnCreate_Start);
        super.onCreate(savedInstanceState);
        this.sensorMgr = (SensorManager) getSystemService("sensor");
        this.sensor = this.sensorMgr.getDefaultSensor(1);
        if (this.sensor != null) {
            this.detector = new AccelerometerSensorDetector();
        } else {
            this.sensor = this.sensorMgr.getDefaultSensor(3);
            if (this.sensor != null) {
                this.detector = new OrientationSensorDetector();
            }
        }
        this.handler = new InternalHandler();
        String nextBtnLabel = getIntent().getStringExtra(CommonIntentAction.EXTRA_FINISH_ACTION_LABEL);
        if (Build.VERSION.SDK_INT < 14) {
            this.isLandscapeMode = true;
            getWindow().addFlags(1024);
            setContentView((int) R.layout.image_selector_land);
        } else {
            setRequestedOrientation(1);
            setContentView((int) R.layout.image_selector);
            if (nextBtnLabel != null) {
                nextBtnLabel = nextBtnLabel.replace("\n", "");
            }
        }
        findViewById(R.id.cap).setOnClickListener(this);
        findViewById(R.id.finish_cap).setOnClickListener(this);
        findViewById(R.id.cap).setEnabled(false);
        findViewById(R.id.cam_focus_area).setOnClickListener(this);
        if (nextBtnLabel != null) {
            ((TextView) findViewById(R.id.right_btn_txt)).setText(nextBtnLabel);
        }
        this.deleteListener = new OnDeleteListener();
        findViewById(R.id.pick_gallery).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent goIntent = new Intent("android.intent.action.GET_CONTENT");
                goIntent.addCategory("android.intent.category.OPENABLE");
                goIntent.setType(ThirdpartyTransitActivity.IMAGEUNSPECIFIED);
                CameraActivity.this.startActivityForResult(goIntent, 1);
            }
        });
        ImageUploader.getInstance().attachActivity(this);
        if (getIntent().hasExtra(CommonIntentAction.EXTRA_IMAGE_LIST)) {
            ArrayList<String> list = getIntent().getStringArrayListExtra(CommonIntentAction.EXTRA_IMAGE_LIST);
            getIntent().removeExtra(CommonIntentAction.EXTRA_IMAGE_LIST);
            this.imageList.clear();
            this.originalList.clear();
            Iterator i$ = list.iterator();
            while (i$.hasNext()) {
                this.originalList.add(BXThumbnail.createThumbnail(i$.next(), null));
            }
            this.imageList.addAll(this.originalList);
        }
        this.handler.sendEmptyMessageDelayed(3, 500);
        PerformanceTracker.stamp(PerformEvent.Event.E_CameraActivity_OnCreate_Leave);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<String> imgKeys = new ArrayList<>();
        if (this.imageList != null) {
            Iterator i$ = this.imageList.iterator();
            while (i$.hasNext()) {
                imgKeys.add(i$.next().getLocalPath());
            }
        }
        outState.putStringArrayList(CommonIntentAction.EXTRA_IMAGE_LIST, imgKeys);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        ArrayList<String> imgKeys = savedInstanceState.getStringArrayList(CommonIntentAction.EXTRA_IMAGE_LIST);
        if (imgKeys != null) {
            this.imageList.clear();
            this.originalList.clear();
            Iterator i$ = imgKeys.iterator();
            while (i$.hasNext()) {
                this.originalList.add(BXThumbnail.createThumbnail(i$.next(), null));
            }
            this.imageList.addAll(this.originalList);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "cam destroyed!");
    }

    @SuppressLint({"NewApi"})
    public void initializeCamera() {
        boolean z = true;
        Camera c = null;
        try {
            c = Camera.open();
            this.isFrontCam = false;
        } catch (Exception e) {
            Log.d("CAM", "fail to open cam " + e.getMessage());
        }
        if (c == null) {
            try {
                c = Camera.open(0);
                Camera.CameraInfo info = new Camera.CameraInfo();
                Camera.getCameraInfo(0, info);
                if (info.facing != 1) {
                    z = false;
                }
                this.isFrontCam = z;
            } catch (Throwable th) {
                Log.w(TAG, "Open camera failed.");
            }
        }
        this.mCamera = c;
        if (this.mCamera != null && getIntent().hasExtra("location")) {
            BXLocation loc = (BXLocation) getIntent().getSerializableExtra("location");
            this.mCamera.getParameters().setGpsLatitude((double) loc.fLat);
            this.mCamera.getParameters().setGpsLongitude((double) loc.fLon);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.handler.sendEmptyMessage(6);
        Log.d(TAG, "cam paused");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        PerformanceTracker.stamp(PerformEvent.Event.E_CameraActivity_onResume);
        String isEdit = getIntent().getBooleanExtra("isEdit", false) ? "1" : "0";
        String from = "others";
        if (getIntent().getIntExtra("extra.common.finishCode", 0) == 0) {
            from = "postForm";
        }
        GlobalDataManager.getInstance().setLastActiveActivity(getClass());
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.CAMERA).append(TrackConfig.TrackMobile.Key.ISEDIT, isEdit).append(TrackConfig.TrackMobile.Key.FROM, from).end();
        super.onResume();
        this.handler.sendEmptyMessage(7);
        this.handler.sendEmptyMessageDelayed(9, 500);
    }

    /* access modifiers changed from: private */
    public void autoFocusWhenOrienChange() {
        try {
            Camera cam = this.mCamera;
            if (cam != null) {
                cam.autoFocus(new Camera.AutoFocusCallback() {
                    public void onAutoFocus(boolean success, Camera camera) {
                    }
                });
            }
        } catch (Throwable th) {
            Log.d(TAG, "error occur when do autofocus.");
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cap /*2131165379*/:
                takePic();
                return;
            case R.id.finish_cap /*2131165381*/:
                finishTakenPic();
                return;
            case R.id.cam_focus_area /*2131165386*/:
                if (this.mCamera != null) {
                    this.mCamera.autoFocus(null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void onCancelEdit() {
        Iterator i$ = this.imageList.iterator();
        while (i$.hasNext()) {
            BXThumbnail t = i$.next();
            if (this.originalList.indexOf(t) == -1) {
                ImageUploader.getInstance().cancel(t.getLocalPath());
            }
        }
    }

    public void takePic() {
        boolean isProcessing = true;
        long currentTime = System.currentTimeMillis();
        Log.w(TAG, "click to take pic " + currentTime);
        if (this.isInitialized) {
            long gap = currentTime - this.lastClickTime;
            if (this.lastClickTime == 0 || gap <= 0 || gap >= 500) {
                isProcessing = false;
            }
            this.lastClickTime = currentTime;
            if (!isProcessing) {
                findViewById(R.id.cap).setEnabled(false);
                Log.d(TAG, "autofocus before take pitcure");
                final BooleanWrapper bWrapper = new BooleanWrapper();
                try {
                    if (!this.isCameraLock) {
                        this.isCameraLock = true;
                        this.mCamera.autoFocus(new Camera.AutoFocusCallback() {
                            public void onAutoFocus(boolean focused, Camera cam) {
                                if (!bWrapper.isTrue) {
                                    bWrapper.isTrue = true;
                                    Camera camera = CameraActivity.this.mCamera;
                                    if (camera != null) {
                                        CameraActivity.this.startGlint();
                                        Log.d(CameraActivity.TAG, "start invoke take picture");
                                        camera.takePicture(null, null, CameraActivity.this.mPicture);
                                        Log.d(CameraActivity.TAG, "take picture invoke return");
                                        camera.cancelAutoFocus();
                                    }
                                }
                            }
                        });
                        this.handler.sendMessageDelayed(this.handler.obtainMessage(10, bWrapper), 1500);
                    }
                } catch (Throwable t) {
                    this.isCameraLock = false;
                    Log.w(TAG, "auto focus exception : " + t.getMessage());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void startGlint() {
        final View v = findViewById(R.id.cam_glint_area);
        v.setVisibility(0);
        Animation glint = AnimationUtils.loadAnimation(this, R.anim.glint);
        glint.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                v.setVisibility(8);
            }
        });
        v.startAnimation(glint);
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        RotationDetector.Orien newOrien = this.detector.updateSensorEvent(sensorEvent);
        if (newOrien != null && this.currentOrien != newOrien) {
            Log.w(TAG, "orientation changed from " + this.currentOrien.des + " to " + newOrien.des);
            this.currentOrien = newOrien;
            this.handler.sendEmptyMessage(2);
        }
    }

    class OnDeleteListener implements View.OnClickListener {
        OnDeleteListener() {
        }

        public void onClick(View v) {
            ViewGroup vp = (ViewGroup) v.getParent();
            UploadingCallback callback = (UploadingCallback) vp.getTag();
            if (callback != null) {
                callback.disable();
                ImageUploader.getInstance().removeCallback(callback);
                vp.setTag(null);
                ((ImageView) vp.findViewById(R.id.result_image)).setImageResource(R.drawable.bg_transparent);
                vp.findViewById(R.id.loading_status).setVisibility(8);
                CameraActivity.this.deleteImageUri((BXThumbnail) v.getTag());
                v.setVisibility(4);
                ViewGroup imgContainer = (ViewGroup) vp.getParent();
                vp.setVisibility(8);
                imgContainer.destroyDrawingCache();
                Log.e(CameraActivity.TAG, "following view will be remove " + vp.hashCode());
                imgContainer.removeView(vp);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            postAppendData(data);
        }
    }

    /* access modifiers changed from: private */
    public String getRealPathFromURI(Uri contentUri) {
        return BitmapUtils.getRealPathFromURI(this, contentUri);
    }

    /* access modifiers changed from: private */
    public BXThumbnail findFromList(String path, List<BXThumbnail> list, boolean deleteIt) {
        BXThumbnail t = null;
        Iterator i$ = list.iterator();
        while (true) {
            if (!i$.hasNext()) {
                break;
            }
            BXThumbnail tt = i$.next();
            if (tt.getLocalPath().equals(path)) {
                t = tt;
                break;
            }
        }
        if (t != null && deleteIt) {
            list.remove(t);
        }
        return t;
    }

    private void postAppendData(Intent data) {
        new AsyncTask<Uri, Integer, BXThumbnail>() {
            /* access modifiers changed from: protected */
            public BXThumbnail doInBackground(Uri... params) {
                String path = CameraActivity.this.getRealPathFromURI(params[0]);
                BXThumbnail tb = CameraActivity.this.findFromList(path, CameraActivity.this.deleteList, true);
                if (tb == null) {
                    return BitmapUtils.copyAndCreateThrmbnail(path, CameraActivity.this);
                }
                return tb;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(BXThumbnail result) {
                CameraActivity.this.handler.sendMessage(CameraActivity.this.handler.obtainMessage(1, result));
                CameraActivity.this.showOneMoreMsg();
            }
        }.execute(data.getData());
    }

    /* access modifiers changed from: private */
    public void postAppendPhoto(byte[] cameraData) {
        this.handler.sendEmptyMessage(0);
        new AsyncTask<byte[], Integer, BXThumbnail>() {
            /* access modifiers changed from: protected */
            public BXThumbnail doInBackground(byte[]... params) {
                return BitmapUtils.saveAndCreateThumbnail(params[0], 0, CameraActivity.this, CameraActivity.this.isFrontCam);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(BXThumbnail result) {
                CameraActivity.this.handler.sendMessage(CameraActivity.this.handler.obtainMessage(1, result));
                CameraActivity.this.showOneMoreMsg();
            }
        }.execute(cameraData);
    }

    /* access modifiers changed from: private */
    public void showOneMoreMsg() {
        boolean full;
        if (5 == this.imageList.size()) {
            full = true;
        } else {
            full = false;
        }
        if (!full) {
            ViewUtil.showToast(this, "再来一张吧，你还能再添加" + ((6 - this.imageList.size()) - 1) + "张", false);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            if (this.mCamera != null) {
                this.mCamera.release();
            }
        } catch (Throwable th) {
            Log.d(TAG, "close camera failed when finalize this class.");
        }
        try {
            super.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        Log.d(TAG, "came finalize");
    }

    private class BooleanWrapper {
        boolean isTrue;

        private BooleanWrapper() {
        }
    }
}
