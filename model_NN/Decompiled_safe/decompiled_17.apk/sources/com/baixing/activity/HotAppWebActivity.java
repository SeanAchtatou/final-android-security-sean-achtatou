package com.baixing.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.tencent.tauth.Constants;

public class HotAppWebActivity extends Activity {
    String url = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.url = getIntent().getStringExtra(Constants.PARAM_URL);
        WebView webView = new WebView(this);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(this.url);
        setContentView(webView);
    }

    public void onResume() {
        super.onResume();
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.HOTAPP).append(TrackConfig.TrackMobile.Key.HOTAPP_URL, this.url).end();
    }
}
