package com.baixing.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.AdList;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.VadListLoader;
import com.baixing.view.fragment.MyAdFragment;
import com.baixing.view.fragment.PersonalProfileFragment;
import com.baixing.view.fragment.VadFragment;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;

public class PersonalActivity extends BaseTabActivity {
    private BroadcastReceiver personalReceiver;

    /* access modifiers changed from: protected */
    public int getTabIndex() {
        return 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.PersonalProfileFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.MyAdFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    private void jumpToPersonalPost(Intent intent) {
        Bundle extras;
        String action = intent.getAction();
        if (action != null && action.equals(CommonIntentAction.ACTION_BROADCAST_POST_FINISH) && (extras = intent.getExtras()) != null) {
            extras.putBoolean(CommonIntentAction.ACTION_BROADCAST_POST_FINISH, true);
            if (getSupportFragmentManager().getBackStackEntryCount() > 1 && !(getCurrentFragment() instanceof MyAdFragment)) {
                PerformanceTracker.stamp(PerformEvent.Event.E_JumpAfterPost_PushProfile);
                pushFragment((BaseFragment) new PersonalProfileFragment(), this.bundle, true);
            }
            PerformanceTracker.stamp(PerformEvent.Event.E_JumpAfterPost_PushMyAd);
            pushFragment((BaseFragment) new MyAdFragment(), extras, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.PersonalProfileFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public void onCreate(Bundle savedBundle) {
        PerformanceTracker.stamp(PerformEvent.Event.E_PersonalActivity_onCreate);
        super.onCreate(savedBundle);
        if (GlobalDataManager.context == null || GlobalDataManager.context.get() == null) {
            GlobalDataManager.context = new WeakReference<>(this);
        }
        setContentView((int) R.layout.main_post);
        onSetRootView(findViewById(R.id.root));
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            pushFragment((BaseFragment) new PersonalProfileFragment(), this.bundle, true);
        }
        this.globalTabCtrl.attachView(findViewById(R.id.common_tab_layout), this);
        initTitleAction();
        if (this.personalReceiver == null) {
            this.personalReceiver = new BroadcastReceiver() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
                 arg types: [com.baixing.view.fragment.PersonalProfileFragment, android.os.Bundle, int]
                 candidates:
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
                public void onReceive(Context context, Intent intent) {
                    BaseFragment fragment;
                    String action = intent.getAction();
                    if (action.equals(CommonIntentAction.ACTION_BROADCAST_EDIT_LOGOUT) || action.equals(CommonIntentAction.ACTION_BROADCAST_MYAD_LOGOUT)) {
                        PersonalActivity.this.pushFragment((BaseFragment) new PersonalProfileFragment(), PersonalActivity.this.bundle, true);
                    } else if (action.equals(CommonIntentAction.ACTION_BROADCAST_COMMON_AD_LOGOUT) && (fragment = PersonalActivity.this.getCurrentFragment()) != null && (fragment instanceof VadFragment)) {
                        PersonalActivity.this.pushFragment((BaseFragment) new PersonalProfileFragment(), PersonalActivity.this.bundle, true);
                    }
                }
            };
        }
    }

    public void onResume() {
        super.onResume();
        jumpToPersonalPost(getIntent());
        showDetailViewFromWX();
        sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_SHARE_BACK_TO_FRONT));
        IntentFilter intentFilter = new IntentFilter(CommonIntentAction.ACTION_BROADCAST_EDIT_LOGOUT);
        intentFilter.addAction(CommonIntentAction.ACTION_BROADCAST_MYAD_LOGOUT);
        intentFilter.addAction(CommonIntentAction.ACTION_BROADCAST_COMMON_AD_LOGOUT);
        registerReceiver(this.personalReceiver, intentFilter);
    }

    public void onPause() {
        super.onPause();
        if (this.personalReceiver != null) {
            unregisterReceiver(this.personalReceiver);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.VadFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    private void showDetailViewFromWX() {
        Bundle bundle;
        Intent intent = getIntent();
        if (intent != null && (bundle = intent.getExtras()) != null && bundle.getBoolean("isFromWX") && bundle.getString("detailFromWX") != null) {
            ApiParams param = new ApiParams();
            param.addParam("query", "id:" + bundle.getString("detailFromWX"));
            ProgressDialog pd = ProgressDialog.show(this, "", "请稍候");
            pd.setCancelable(false);
            pd.show();
            try {
                AdList gl = JsonUtil.getGoodsListFromJson(BaseApiCommand.createCommand("ad_list", true, param).executeSync(this));
                if (gl != null) {
                    VadListLoader glLoader = new VadListLoader(null, null, null, gl);
                    glLoader.setGoodsList(gl);
                    glLoader.setHasMore(false);
                    BaseFragment bf = getCurrentFragment();
                    if (bf != null && (bf instanceof VadFragment)) {
                        bf.finishFragment();
                    }
                    Bundle bundle2 = new Bundle();
                    bundle2.putSerializable("loader", glLoader);
                    bundle2.putInt("index", 0);
                    pushFragment((BaseFragment) new VadFragment(), bundle2, false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            pd.dismiss();
        }
    }
}
