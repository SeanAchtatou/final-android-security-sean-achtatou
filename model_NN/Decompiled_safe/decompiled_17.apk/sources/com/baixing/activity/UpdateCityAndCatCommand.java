package com.baixing.activity;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.CityList;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.MobileConfig;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;

public class UpdateCityAndCatCommand implements BaseApiCommand.Callback {
    private long cateUpdateTime;
    private long cityUpdateTime;
    private Context context;

    UpdateCityAndCatCommand(Context cxt) {
        this.context = cxt;
    }

    public void execute() {
        Log.d("citylist", "start update");
        PerformanceTracker.stamp(PerformEvent.Event.E_Begin_UpdateCityAndCat);
        Pair<Long, String> pair = Util.loadJsonAndTimestampFromLocate(this.context, "cityjson");
        long timestamp = ((Long) pair.first).longValue();
        String content = (String) pair.second;
        this.cityUpdateTime = MobileConfig.getInstance().getCityTimestamp();
        if (timestamp < this.cityUpdateTime || content == null || content.length() == 0) {
            Log.d("citylist", "lastupdate:" + timestamp + ";nowupdate:" + this.cityUpdateTime + ".so to update");
            BaseApiCommand.createCommand("City.all/", true, null).execute(this.context, this);
        } else {
            Log.d("citylist", "lastupdate:" + timestamp + ";nowupdate:" + this.cityUpdateTime + ".so not to update");
        }
        Pair<Long, String> firstCatePair = Util.loadJsonAndTimestampFromLocate(this.context, "saveFirstStepCate");
        String categoryContent = (String) firstCatePair.second;
        long timestamp2 = ((Long) firstCatePair.first).longValue();
        this.cateUpdateTime = MobileConfig.getInstance().getCategoryTimestamp();
        if (timestamp2 < this.cateUpdateTime || TextUtils.isEmpty(categoryContent)) {
            BaseApiCommand.createCommand("Category.all/", true, null).execute(this.context, this);
        }
    }

    public void onNetworkDone(String apiName, String responseData) {
        if ("City.all/".equals(apiName)) {
            if (!TextUtils.isEmpty(responseData)) {
                CityList cityList = JsonUtil.parseCityListFromJson(responseData);
                Util.saveJsonAndTimestampToLocate(this.context, "cityjson", responseData, this.cityUpdateTime);
                Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "city.3.6.0", "true");
                GlobalDataManager.getInstance().updateCityList(cityList);
            }
            PerformanceTracker.stamp(PerformEvent.Event.E_UpdateCity_Done);
        } else if ("Category.all/".equals(apiName)) {
            if (!TextUtils.isEmpty(responseData)) {
                Util.saveJsonAndTimestampToLocate(this.context, "saveFirstStepCate", responseData, this.cateUpdateTime);
                Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "category.3.6.0", "true");
            }
            PerformanceTracker.stamp(PerformEvent.Event.E_UpdateCat_Done);
        }
    }

    public void onNetworkFail(String apiName, ApiError error) {
        Log.e("QLM", "fail to update " + apiName);
        PerformanceTracker.stamp(PerformEvent.Event.E_UpdateCityAndCat_FAIL);
    }
}
