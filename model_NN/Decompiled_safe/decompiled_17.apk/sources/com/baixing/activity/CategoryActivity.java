package com.baixing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import com.baixing.data.GlobalDataManager;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.view.fragment.HomeFragment;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;

public class CategoryActivity extends BaseTabActivity {
    /* access modifiers changed from: protected */
    public int getTabIndex() {
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.HomeFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public void onCreate(Bundle savedBundle) {
        PerformanceTracker.stamp(PerformEvent.Event.E_PersonalActivity_onCreate);
        super.onCreate(savedBundle);
        if (GlobalDataManager.context == null || GlobalDataManager.context.get() == null) {
            GlobalDataManager.context = new WeakReference<>(this);
        }
        setContentView((int) R.layout.main_post);
        onSetRootView(findViewById(R.id.root));
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            pushFragment((BaseFragment) new HomeFragment(), this.bundle, true);
        }
        this.globalTabCtrl.attachView(findViewById(R.id.common_tab_layout), this);
        initTitleAction();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ("android.intent.action.MAIN".equals(intent.getAction()) && GlobalDataManager.getInstance().getLastActiveClass() != null) {
            Intent go = new Intent();
            go.setClassName(this, GlobalDataManager.getInstance().getLastActiveClass().getName());
            go.addFlags(4194304);
            startActivity(go);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (!this.isChangingTab) {
            Log.d("ddd", "onpause");
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.APP_PAUSE).end();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!this.isChangingTab) {
            Log.d("ddd", "onresume");
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.APP_RESUME).end();
        }
        this.bundle.putString(BaseFragment.ARG_COMMON_BACK_HINT, "");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        if (!this.isChangingTab) {
            Log.d("ddd", "onstart");
        }
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public boolean handleIntent() {
        if ((this.intent.getFlags() & 1048576) != 0) {
            return false;
        }
        setIntent(this.intent);
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        handleBack();
        return true;
    }
}
