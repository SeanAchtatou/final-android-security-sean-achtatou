package com.baixing.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.view.fragment.CityChangeFragment;
import com.baixing.view.fragment.FeedbackFragment;
import com.baixing.view.fragment.LoginFragment;
import com.baixing.view.fragment.SettingFragment;
import com.quanleimu.activity.R;
import java.util.Set;
import java.util.TreeSet;

public abstract class BaseFragment extends Fragment {
    public static final String ARG_COMMON_ANIMATION_EXIT = "exitAnimation";
    public static final String ARG_COMMON_ANIMATION_IN = "inAnimation";
    public static final String ARG_COMMON_BACK_HINT = "backPageName";
    public static final String ARG_COMMON_HAS_GLOBAL_TAB = "hasGlobalTabbar";
    public static final String ARG_COMMON_REQ_CODE = "reqestCode";
    public static final String ARG_COMMON_TITLE = "name";
    protected static int INVALID_REQUEST_CODE = -1;
    public static final int OPTION_CHANGE_CITY = 0;
    public static final int OPTION_EXIT = 5;
    public static final int OPTION_FEEDBACK = 2;
    public static final int OPTION_LOGIN = 3;
    public static final int OPTION_LOGOUT = 4;
    public static final int OPTION_SETTING = 1;
    public static final String[] OPTION_TITLES = {"切换城市", "设置", "反馈", "登录", "注销", "退出程序"};
    public static final String TAG = "QLM";
    private static final int[] baseOptions = {1, 2, 5, 3};
    private static Set<Integer> options = new TreeSet();
    public final int MSG_USER_LOGIN = 10001;
    public final int MSG_USER_LOGOUT = 10002;
    protected int defaultEnterAnim = R.anim.right_to_left_enter;
    protected int defaultExitAnim = R.anim.left_to_right_exit;
    protected int fragmentRequestCode = INVALID_REQUEST_CODE;
    protected Handler handler;
    private ProgressDialog pd;
    protected TrackConfig.TrackMobile.PV pv = TrackConfig.TrackMobile.PV.BASE;
    private View.OnClickListener titleActionListener;
    private TitleDef titleDef;

    /* access modifiers changed from: protected */
    public abstract View onInitializeView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    public Handler getHandler() {
        return this.handler;
    }

    public static final class TitleDef {
        public boolean hasGlobalSearch;
        public String m_leftActionHint;
        public int m_leftActionImage;
        public int m_rightActionBg;
        public String m_rightActionHint;
        public String m_title;
        public View m_titleControls;
        public boolean m_visible;
        public int rightCustomResourceId;

        private TitleDef() {
            this.m_visible = true;
            this.m_leftActionHint = null;
            this.m_leftActionImage = -1;
            this.m_title = null;
            this.m_titleControls = null;
            this.m_rightActionHint = null;
            this.m_rightActionBg = R.drawable.title_bg_selector;
            this.rightCustomResourceId = -1;
            this.hasGlobalSearch = false;
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
    }

    /* access modifiers changed from: protected */
    public final void sendMessage(int what, Object data) {
        if (this.handler != null) {
            Message message = this.handler.obtainMessage();
            message.what = what;
            if (data != null) {
                message.obj = data;
            }
            this.handler.sendMessage(message);
        }
    }

    /* access modifiers changed from: protected */
    public final void sendMessageDelay(int what, Object data, long delayMillis) {
        if (this.handler != null) {
            Message message = this.handler.obtainMessage();
            message.what = what;
            if (data != null) {
                message.obj = data;
            }
            this.handler.sendMessageDelayed(message, delayMillis);
        }
    }

    public int getEnterAnimation() {
        return getArguments() == null ? R.anim.right_to_left_enter : getArguments().getInt(ARG_COMMON_ANIMATION_IN, this.defaultEnterAnim);
    }

    public int getExitAnimation() {
        return getArguments() == null ? R.anim.left_to_right_exit : getArguments().getInt(ARG_COMMON_ANIMATION_EXIT, this.defaultExitAnim);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.titleActionListener = new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.left_action /*2131165258*/:
                        if (BaseFragment.this.getActivity() instanceof IExit) {
                            ((IExit) BaseFragment.this.getActivity()).handleFragmentAction();
                            return;
                        }
                        return;
                    case R.id.search_action /*2131165263*/:
                        BaseFragment.this.handleSearch();
                        return;
                    case R.id.right_action /*2131165265*/:
                        BaseFragment.this.handleRightAction();
                        return;
                    default:
                        return;
                }
            }
        };
        setHasOptionsMenu(true);
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                Activity activity = BaseFragment.this.getActivity();
                View rootView = BaseFragment.this.getView();
                if (rootView == null || activity == null) {
                    Log.e("QLM", BaseFragment.this.getName() + " cannot dispatch handle message because activity is null ? " + Boolean.valueOf(activity == null));
                } else {
                    BaseFragment.this.handleMessage(msg, activity, rootView);
                }
            }
        };
        if (getArguments() != null) {
            this.fragmentRequestCode = getArguments().getInt("reqestCode", INVALID_REQUEST_CODE);
        }
        if (savedInstanceState != null) {
        }
    }

    public String getName() {
        return getClass().getName() + hashCode();
    }

    /* access modifiers changed from: protected */
    public final TitleDef getTitleDef() {
        if (this.titleDef == null) {
            this.titleDef = new TitleDef();
            initTitle(this.titleDef);
        }
        return this.titleDef;
    }

    /* access modifiers changed from: protected */
    public final void reCreateTitle() {
        this.titleDef = null;
        getTitleDef();
    }

    /* access modifiers changed from: protected */
    public void initTitle(TitleDef title) {
    }

    public void onStart() {
        super.onStart();
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public int[] includedOptionMenus() {
        return new int[0];
    }

    public int[] excludedOptionMenus() {
        return new int[0];
    }

    private void initOptionMenu() {
        options.clear();
        for (int valueOf : baseOptions) {
            options.add(Integer.valueOf(valueOf));
        }
        int[] includeMenus = includedOptionMenus();
        for (int valueOf2 : includeMenus) {
            options.add(Integer.valueOf(valueOf2));
        }
        int[] excludeMenus = excludedOptionMenus();
        for (int valueOf3 : excludeMenus) {
            options.remove(Integer.valueOf(valueOf3));
        }
        if (GlobalDataManager.getInstance().getAccountManager().isUserLogin()) {
            if (options.remove(3)) {
                options.add(4);
            }
        } else if (options.remove(4)) {
            options.add(3);
        }
    }

    public void onPrepareOptionsMenu(Menu menu) {
        initOptionMenu();
        menu.clear();
        for (Integer intValue : options) {
            int option = intValue.intValue();
            menu.add(0, option, option, OPTION_TITLES[option]);
        }
        super.onPrepareOptionsMenu(menu);
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.MENU_SHOW).append(TrackConfig.TrackMobile.Key.FRAGMENT, getClass().toString()).end();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String action = "";
        switch (item.getItemId()) {
            case 0:
                pushFragment(new CityChangeFragment(), createArguments("切换城市", ""));
                action = "changecity";
                break;
            case 1:
                pushFragment(new SettingFragment(), createArguments("设置", ""));
                action = "setting";
                break;
            case 2:
                pushFragment(new FeedbackFragment(), createArguments("反馈信息", ""));
                action = "feedback";
                break;
            case 3:
                pushFragment(new LoginFragment(), createArguments("登录", ""));
                sendMessage(10001, null);
                action = "login";
                break;
            case 4:
                action = "logout";
                new AlertDialog.Builder(getActivity()).setTitle((int) R.string.dialog_confirm_logout).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Util.logout();
                        BaseFragment.this.sendMessage(10002, null);
                        ViewUtil.showToast(BaseFragment.this.getActivity(), "已退出", false);
                    }
                }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).create().show();
                break;
            case 5:
                ((BaseTabActivity) getActivity()).exitMainActivity();
                action = "exit";
                break;
        }
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.MENU_ACTION).append(TrackConfig.TrackMobile.Key.FRAGMENT, getClass().toString()).append(TrackConfig.TrackMobile.Key.MENU_ACTION_TYPE, action).end();
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void showAlert(String title, String message, final DialogAction positiveAction, final DialogAction negativeAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (title != null) {
            builder.setTitle(title);
        }
        if (message != null) {
            builder.setMessage(message);
        }
        if (positiveAction != null) {
            builder.setPositiveButton(positiveAction.actioLabel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    positiveAction.doAction();
                }
            });
        }
        if (negativeAction != null) {
            builder.setNegativeButton(negativeAction.actioLabel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    negativeAction.doAction();
                }
            });
        } else {
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
        }
        builder.create().show();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        refreshHeader(getView());
    }

    public final void notifyOnStackTop(boolean isBack) {
        onStackTop(isBack);
    }

    /* access modifiers changed from: protected */
    public int getFirstRunId() {
        return -1;
    }

    public void onStackTop(boolean isBack) {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("QLM", "fragment onActivityResult()");
    }

    public final void onDestroyView() {
        onViewDestory(getView());
        if (this.pd != null && this.pd.isShowing()) {
            this.pd.dismiss();
        }
        TitleDef title = getTitleDef();
        if (!(title == null || title.m_titleControls == null)) {
            ((ViewGroup) getView().findViewById(R.id.linearTop)).removeView(title.m_titleControls);
        }
        super.onDestroyView();
    }

    /* access modifiers changed from: protected */
    public void onViewDestory(View rootView) {
    }

    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = onInitializeView(inflater, container, savedInstanceState);
        refreshHeader(rootView);
        return rootView;
    }

    public void onDestroy() {
        this.handler = null;
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onPause() {
        super.onPause();
        hideSoftKeyboard();
    }

    /* access modifiers changed from: protected */
    public final void hideSoftKeyboard() {
        final View currentRoot = getView();
        if (currentRoot != null) {
            final InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService("input_method");
            currentRoot.postDelayed(new Runnable() {
                public void run() {
                    mgr.hideSoftInputFromWindow(currentRoot.getWindowToken(), 2);
                }
            }, 100);
        }
    }

    public void onResume() {
        super.onResume();
        getActivity().findViewById(R.id.contentLayout).setVisibility(0);
        ((BaseActivity) getActivity()).showFirstRun(this);
        Log.d("BaseFragment", "" + getClass().getName());
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public final void pushAndFinish(BaseFragment f, Bundle bundle) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.pushFragment(f, bundle, getName());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.activity.BaseFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* access modifiers changed from: protected */
    public void pushFragment(BaseFragment f, Bundle bundle) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.pushFragment(f, bundle, false);
        }
    }

    /* access modifiers changed from: protected */
    public final String getFragmentName() {
        return hashCode() + "";
    }

    /* access modifiers changed from: protected */
    public final boolean finishFragment() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            return activity.popFragment(this);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void finishFragment(int resultCode, Object result) {
        BaseActivity activity;
        BaseFragment current;
        if (finishFragment() && (activity = (BaseActivity) getActivity()) != null && (current = activity.getCurrentFragment()) != null) {
            current.onFragmentBackWithData(resultCode, result);
        }
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int requestCode, Object result) {
        Log.w("QLM", "fragment finish with data." + requestCode + "_" + result);
    }

    public boolean handleBack() {
        return false;
    }

    public void handleRightAction() {
    }

    public void handleSearch() {
    }

    /* access modifiers changed from: protected */
    public void onAddTitleControl(View titleControl) {
    }

    /* access modifiers changed from: protected */
    public void refreshHeader() {
        refreshHeader(getView());
    }

    private void refreshHeader(View rootView) {
        if (rootView == null || rootView.findViewById(R.id.linearTop) == null) {
            Log.e("QLM", "cannot refresh header because ui is not active now");
            return;
        }
        View left = rootView.findViewById(R.id.left_action);
        if (left != null) {
            left.setOnClickListener(this.titleActionListener);
        }
        View right = rootView.findViewById(R.id.right_action);
        if (right != null) {
            right.setOnClickListener(this.titleActionListener);
        }
        View search = rootView.findViewById(R.id.search_action);
        if (search != null) {
            search.setOnClickListener(this.titleActionListener);
        }
        TitleDef title = getTitleDef();
        RelativeLayout top = (RelativeLayout) rootView.findViewById(R.id.linearTop);
        top.setPadding(0, 0, 0, 0);
        if (title == null || !title.m_visible) {
            top.setVisibility(8);
            return;
        }
        top.setVisibility(0);
        LinearLayout llTitleControls = (LinearLayout) top.findViewById(R.id.linearTitleControls);
        TextView tTitle = (TextView) rootView.findViewById(R.id.tvTitle);
        if (title.m_titleControls != null) {
            llTitleControls.setVisibility(0);
            tTitle.setVisibility(8);
            if ((llTitleControls.getChildCount() == 0 ? null : llTitleControls.getChildAt(0)) != title.m_titleControls) {
                ViewParent p = title.m_titleControls.getParent();
                if (p != null) {
                    ((ViewGroup) p).removeView(title.m_titleControls);
                }
                llTitleControls.addView(title.m_titleControls);
                onAddTitleControl(title.m_titleControls);
            }
        } else {
            llTitleControls.setVisibility(8);
            tTitle.setVisibility(0);
            tTitle.setText(title.m_title);
        }
        left.setPadding(0, 0, 0, 0);
        if (title.m_leftActionHint == null || title.m_leftActionHint.equals("")) {
            left.setVisibility(8);
            rootView.findViewById(R.id.left_line).setVisibility(8);
        } else {
            left.setVisibility(0);
            rootView.findViewById(R.id.left_line).setVisibility(0);
            ImageView img = (ImageView) rootView.findViewById(R.id.back_icon);
            if (title.m_leftActionImage != -1) {
                img.setImageResource(title.m_leftActionImage);
            } else {
                img.setImageBitmap(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.icon_back));
            }
        }
        search.setPadding(0, 0, 0, 0);
        if (title.hasGlobalSearch) {
            search.setVisibility(0);
        } else {
            search.setVisibility(8);
        }
        if (right != null && title.m_rightActionHint != null && !"".equals(title.m_rightActionHint)) {
            right.setVisibility(0);
            right.setBackgroundResource(title.m_rightActionBg);
            ((TextView) rootView.findViewById(R.id.right_btn_txt)).setText(title.m_rightActionHint);
        } else if (right != null) {
            right.setVisibility(8);
        }
        right.setPadding(0, 0, 0, 0);
    }

    /* access modifiers changed from: protected */
    public final Bundle createArguments(String title, String backhint) {
        Bundle bundle = new Bundle();
        if (title != null) {
            bundle.putString(ARG_COMMON_TITLE, title);
        }
        if (backhint != null) {
            bundle.putString(ARG_COMMON_BACK_HINT, backhint);
        }
        if (getArguments() != null) {
            bundle.putBoolean(ARG_COMMON_HAS_GLOBAL_TAB, hasGlobalTab());
        }
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [?, ?, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    /* access modifiers changed from: protected */
    public final void showSimpleProgress() {
        showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_waiting, true);
    }

    /* access modifiers changed from: protected */
    public final void showProgress(int titleResid, int messageResid, boolean cancelable) {
        showProgress(getString(titleResid), getString(messageResid), cancelable);
    }

    /* access modifiers changed from: protected */
    public final void showProgress(String title, String message, boolean cancelable) {
        hideProgress();
        if (getActivity() != null) {
            this.pd = ProgressDialog.show(getActivity(), title, message);
            this.pd.setCancelable(cancelable);
            this.pd.setCanceledOnTouchOutside(cancelable);
        }
    }

    /* access modifiers changed from: protected */
    public final void showProgress(String title, String message, DialogInterface.OnCancelListener cancelListener) {
        hideProgress();
        if (getActivity() != null) {
            this.pd = ProgressDialog.show(getActivity(), title, message);
            this.pd.setCancelable(cancelListener != null);
            if (cancelListener != null) {
                this.pd.setOnCancelListener(cancelListener);
                this.pd.setCanceledOnTouchOutside(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void hideProgress() {
        if (this.pd != null && this.pd.isShowing()) {
            this.pd.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public final Context getAppContext() {
        return GlobalDataManager.getInstance().getApplicationContext();
    }

    /* access modifiers changed from: protected */
    public void logCreateView(Bundle bundle) {
    }

    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    private boolean isUiActive() {
        return getActivity() != null;
    }

    public boolean hasGlobalTab() {
        if (getArguments() == null || !getArguments().containsKey(ARG_COMMON_HAS_GLOBAL_TAB)) {
            return true;
        }
        return getArguments().getBoolean(ARG_COMMON_HAS_GLOBAL_TAB);
    }

    protected abstract class DialogAction {
        String actioLabel;

        public abstract void doAction();

        public DialogAction(String label) {
            this.actioLabel = label;
        }

        public DialogAction(int labelId) {
            this.actioLabel = BaseFragment.this.getString(labelId);
        }
    }
}
