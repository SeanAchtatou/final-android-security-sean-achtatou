package com.baixing.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.CityDetail;
import com.baixing.util.LocationService;
import com.baixing.util.ViewUtil;
import com.baixing.view.fragment.FirstRunFragment;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.umeng.analytics.MobclickAgent;
import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends FragmentActivity implements View.OnClickListener {
    public static final int INVALID_FIRSTFRAGMENT_ID = -1;
    public static final String PREF_FIRSTRUN = "firstRunFlag";
    public static final String TAG = "QLM";
    protected Bundle bundle = null;
    protected ExitController exitCtrl = new ExitController();
    protected int firstFragmentId = -1;
    protected Intent intent = null;
    protected LinearLayout loadingLayout;
    protected ProgressDialog pd;
    protected ProgressBar progressBar;
    private boolean savedInstance = false;
    private int stackSize;
    protected TextView tvAddMore;
    protected View v = null;

    protected interface BUNDLE_KEYS {
        public static final String KEY_FIRST_FRAGMENT_ID = "firstFragmentId";
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent2) {
        this.savedInstance = false;
        setIntent(intent2);
        super.onNewIntent(intent2);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d("baseactivity", "save");
        LocationService.getInstance().stop();
        savedInstanceState.putString("cityEnglishName", GlobalDataManager.getInstance().getCityEnglishName());
        savedInstanceState.putString("cityName", GlobalDataManager.getInstance().getCityName());
        savedInstanceState.putString("cityId", GlobalDataManager.getInstance().getCityId());
        ArrayList<String> strDetails = new ArrayList<>();
        for (int i = 0; i < GlobalDataManager.getInstance().getListCityDetails().size(); i++) {
            CityDetail detail = GlobalDataManager.getInstance().getListCityDetails().get(i);
            strDetails.add("englishName=" + detail.getEnglishName() + ",id=" + detail.getId() + ",name=" + detail.getName() + ",sheng=" + detail.getSheng());
        }
        savedInstanceState.putStringArrayList("cityDetails", strDetails);
        savedInstanceState.putInt(BUNDLE_KEYS.KEY_FIRST_FRAGMENT_ID, this.firstFragmentId);
        this.savedInstance = true;
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.savedInstance = false;
        super.onRestoreInstanceState(savedInstanceState);
        GlobalDataManager.getInstance().setCityEnglishName(savedInstanceState.getString("cityEnglishName"));
        GlobalDataManager.getInstance().setCityName(savedInstanceState.getString("cityName"));
        GlobalDataManager.getInstance().setCityId(savedInstanceState.getString("cityId"));
        ArrayList<String> listDetails = savedInstanceState.getStringArrayList("cityDetails");
        List<CityDetail> cityDetails = new ArrayList<>();
        for (int i = 0; i < listDetails.size(); i++) {
            String[] strDetails = listDetails.get(i).split(",");
            CityDetail detail = new CityDetail();
            for (String split : strDetails) {
                String[] subItems = split.split("=");
                if (subItems[0].equals("englishName")) {
                    detail.setEnglishName(subItems[1]);
                } else if (subItems[0].equals(LocaleUtil.INDONESIAN)) {
                    detail.setId(subItems[1]);
                } else if (subItems[0].equals(BaseFragment.ARG_COMMON_TITLE)) {
                    detail.setName(subItems[1]);
                } else if (subItems[0].equals("sheng")) {
                    detail.setSheng(subItems[1]);
                }
            }
            cityDetails.add(detail);
        }
        GlobalDataManager.getInstance().setListCityDetails(cityDetails);
        this.firstFragmentId = savedInstanceState.getInt(BUNDLE_KEYS.KEY_FIRST_FRAGMENT_ID, -1);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        GlobalDataManager.getInstance().setLastActiveActivity(getClass());
        MobclickAgent.onResume(this);
        this.savedInstance = false;
    }

    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    /* access modifiers changed from: protected */
    public final void notifyStackTop() {
        BaseFragment f;
        if (!this.savedInstance && (f = getCurrentFragment()) != null) {
            int newStackSize = getSupportFragmentManager().getBackStackEntryCount();
            Log.e("QLM", "notify stack top " + f.getClass().getName() + "#" + f.hashCode());
            try {
                f.notifyOnStackTop(newStackSize < this.stackSize);
            } catch (Throwable th) {
            } finally {
                this.stackSize = newStackSize;
            }
            View cover = findViewById(R.id.splash_cover);
            if (cover != null) {
                cover.setVisibility(8);
            }
            onStatckTop(f);
        }
    }

    /* access modifiers changed from: protected */
    public void onStatckTop(BaseFragment f) {
    }

    public final void showFirstRun(BaseFragment f) {
        if (!this.savedInstance && f.getFirstRunId() != -1) {
            String key = f.getClass().getName() + GlobalDataManager.getInstance().getVersion();
            SharedPreferences share = getSharedPreferences(PREF_FIRSTRUN, 0);
            if (!share.getBoolean(key, false)) {
                SharedPreferences.Editor edit = share.edit();
                edit.putBoolean(key, true);
                edit.commit();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                FirstRunFragment.create(key, f.getFirstRunId()).show(ft, "dialog");
            }
        }
    }

    public final void onHideFirstRun(String key) {
        SharedPreferences.Editor edit = getSharedPreferences(PREF_FIRSTRUN, 0).edit();
        edit.putBoolean(key, true);
        edit.commit();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                BaseActivity.this.notifyStackTop();
            }
        });
        MobclickAgent.onError(this);
        this.intent = getIntent();
        if (this.intent == null) {
            this.intent = new Intent();
        }
        this.intent.setFlags(67108864);
        this.bundle = this.intent.getExtras();
        if (this.bundle == null) {
            this.bundle = new Bundle();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("QLM", "activity on activity result.");
        BaseFragment fragment = getCurrentFragment();
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v2) {
        v2.getId();
    }

    public boolean checkConnection() throws Exception {
        boolean a;
        NetworkInfo.State wifi = null;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService("connectivity");
        if (connManager == null) {
            return false;
        }
        NetworkInfo mobileInfo = connManager.getNetworkInfo(0);
        NetworkInfo wifiInfo = connManager.getNetworkInfo(1);
        NetworkInfo.State mobile = mobileInfo == null ? null : mobileInfo.getState();
        if (wifiInfo != null) {
            wifi = wifiInfo.getState();
        }
        if ((mobile == null || !mobile.toString().equals("CONNECTED")) && (wifi == null || !wifi.toString().equals("CONNECTED"))) {
            a = false;
        } else {
            a = true;
        }
        return a;
    }

    public void pdShow(Context context) {
        this.pd = ProgressDialog.show(context, getString(R.string.dialog_title_info), getString(R.string.dialog_message_waiting));
        this.pd.setCancelable(true);
    }

    public void pdDismiss(Context context) {
        this.pd.dismiss();
    }

    public final void pushFragment(BaseFragment fragment, Bundle bundle2, String popTo) {
        if (!this.savedInstance) {
            if (bundle2 != null) {
                fragment.setArguments(bundle2);
            }
            FragmentManager fm = getSupportFragmentManager();
            boolean isFirstFragment = fm.getBackStackEntryCount() == 1;
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(fragment.getEnterAnimation(), R.anim.zoom_exit, R.anim.zoom_enter, fragment.getExitAnimation());
            if (!"".equals(popTo)) {
                if (popTo == null) {
                    popTo = null;
                }
                fm.popBackStack(popTo, 1);
            }
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.replace(R.id.contentLayout, fragment);
            ft.addToBackStack(fragment.getName());
            int id = ft.commit();
            if (isFirstFragment) {
                this.firstFragmentId = id;
                Log.e("QLM", "first fragment added to stack id is " + id);
                return;
            }
            Log.w("QLM", "fragment added to stack id is " + id);
        }
    }

    public final void pushFragment(BaseFragment f, Bundle bundle2, boolean clearStack) {
        pushFragment(f, bundle2, clearStack ? null : "");
    }

    public final boolean popFragment(BaseFragment f) {
        if (this.savedInstance) {
            return false;
        }
        FragmentManager fm = getSupportFragmentManager();
        int entryCount = fm.getBackStackEntryCount();
        FragmentTransaction ft = fm.beginTransaction();
        boolean popSucceed = fm.popBackStackImmediate();
        ft.commit();
        if (!popSucceed || entryCount > 1) {
            return popSucceed;
        }
        onFragmentEmpty();
        return popSucceed;
    }

    /* access modifiers changed from: protected */
    public void onFragmentEmpty() {
        finish();
    }

    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    public BaseFragment getCurrentFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.contentLayout);
    }

    public class ExitController {
        private static final int TIME_GAP = 5000;
        private long lastBackEventTime;

        public ExitController() {
        }

        public boolean requestExit() {
            long currentTime = System.currentTimeMillis();
            if (this.lastBackEventTime == 0 || currentTime <= this.lastBackEventTime || currentTime - this.lastBackEventTime >= 5000) {
                this.lastBackEventTime = currentTime;
                ViewUtil.showToast(BaseActivity.this, "再按一次退出程序", false);
                return false;
            }
            this.lastBackEventTime = 0;
            return true;
        }
    }
}
