package com.baixing.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.inputmethod.InputMethodManager;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.view.fragment.PostGoodsFragment;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;

public class PostActivity extends BaseTabActivity {
    private BroadcastReceiver postReceiver;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.PostGoodsFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public void onCreate(Bundle savedBundle) {
        Bundle extra;
        PerformanceTracker.stamp(PerformEvent.Event.E_PostActivity_OnCreate_Begin);
        super.onCreate(savedBundle);
        if (GlobalDataManager.context == null || GlobalDataManager.context.get() == null) {
            GlobalDataManager.context = new WeakReference<>(this);
        }
        setContentView((int) R.layout.main_post);
        onSetRootView(findViewById(R.id.root));
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            Intent intent = getIntent();
            Bundle bundle = new Bundle();
            if (!(intent == null || !intent.hasExtra(PostGoodsFragment.KEY_INIT_CATEGORY) || (extra = intent.getExtras()) == null)) {
                bundle.putString(PostGoodsFragment.KEY_INIT_CATEGORY, extra.getString(PostGoodsFragment.KEY_INIT_CATEGORY));
            }
            pushFragment((BaseFragment) new PostGoodsFragment(), bundle, false);
        }
        this.globalTabCtrl.attachView(findViewById(R.id.common_tab_layout), this);
        initTitleAction();
        PerformanceTracker.stamp(PerformEvent.Event.E_PostActivity_OnCreate_Leave);
    }

    /* access modifiers changed from: protected */
    public void onFragmentEmpty() {
        afterChange(0);
        finish();
    }

    private void registerBroadcast() {
        IntentFilter intentFilter = new IntentFilter(CommonIntentAction.ACTION_BROADCAST_POST_FINISH);
        if (this.postReceiver == null) {
            this.postReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(CommonIntentAction.ACTION_BROADCAST_POST_FINISH)) {
                        Bundle bundle = intent.getExtras();
                        PerformanceTracker.stamp(PerformEvent.Event.E_GetPostSuccessBroadcast);
                        Intent personalIntent = new Intent();
                        personalIntent.setClass(PostActivity.this, PersonalActivity.class);
                        personalIntent.addFlags(AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY);
                        personalIntent.addFlags(4194304);
                        personalIntent.addFlags(65536);
                        personalIntent.setAction(CommonIntentAction.ACTION_BROADCAST_POST_FINISH);
                        personalIntent.putExtras(bundle);
                        PostActivity.this.startActivity(personalIntent);
                        BaseFragment current = PostActivity.this.getCurrentFragment();
                        if (current != null && (current instanceof PostGoodsFragment)) {
                            current.finishFragment();
                        }
                    }
                }
            };
        }
        registerReceiver(this.postReceiver, intentFilter);
    }

    public void onResume() {
        super.onResume();
        registerBroadcast();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Bundle extra;
        super.onNewIntent(intent);
        BaseFragment bf = getCurrentFragment();
        if (bf != null && (bf instanceof PostGoodsFragment) && intent != null && intent.hasExtra(PostGoodsFragment.KEY_INIT_CATEGORY) && (extra = intent.getExtras()) != null) {
            ((PostGoodsFragment) bf).updateNewCategoryLayout(extra.getString(PostGoodsFragment.KEY_INIT_CATEGORY));
        }
    }

    public void handleBack() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(findViewById(R.id.contentLayout).getWindowToken(), 0);
        BaseFragment currentFragmet = getCurrentFragment();
        if (currentFragmet != null) {
            try {
                if (currentFragmet.handleBack()) {
                    return;
                }
                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    popFragment(currentFragmet);
                } else {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onPause() {
        super.onPause();
        if (this.postReceiver != null) {
            unregisterReceiver(this.postReceiver);
        }
    }

    /* access modifiers changed from: protected */
    public int getTabIndex() {
        return 3;
    }
}
