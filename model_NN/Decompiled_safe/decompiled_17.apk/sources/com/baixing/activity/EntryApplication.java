package com.baixing.activity;

import android.app.Application;
import android.os.Environment;
import com.baixing.broadcast.push.PushDispatcher;
import com.baixing.data.GlobalDataManager;
import com.baixing.network.NetworkProfiler;
import com.baixing.network.api.ApiConfiguration;
import com.baixing.util.ErrorHandler;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import com.xiaomi.mipush.MiPushCallback;
import com.xiaomi.mipush.MiPushService;
import java.io.File;
import java.lang.ref.WeakReference;

public class EntryApplication extends Application {
    public static boolean pushViewed;

    public void onCreate() {
        super.onCreate();
        GlobalDataManager.context = new WeakReference<>(getApplicationContext());
        ErrorHandler.getInstance().initContext(getApplicationContext());
        GlobalDataManager mangerInstance = GlobalDataManager.getInstance();
        ApiConfiguration.config("www.baixing.com", GlobalDataManager.getInstance().getNetworkCacheManager(), "api_androidbaixing", "c6dd9d408c0bcbeda381d42955e08a3f");
        MiPushService.initialize(mangerInstance.getApplicationContext(), new MiPushCallback(new PushDispatcher(mangerInstance.getApplicationContext())));
        pushViewed = true;
        if (Util.isLoggable()) {
            NetworkProfiler.endable(new File(Environment.getExternalStorageDirectory(), TextUtil.getShortTimeDesc(System.currentTimeMillis()) + "_bxnt.txt").getAbsolutePath());
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public void onTerminate() {
        super.onTerminate();
    }
}
