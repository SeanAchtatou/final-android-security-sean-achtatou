package com.baixing.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import com.baixing.broadcast.CommonIntentAction;
import java.io.File;

public class ThirdpartyTransitActivity extends Activity {
    public static final String IMAGEUNSPECIFIED = "image/*";
    public static final String Key_Data = "data";
    public static final String Key_RequestCode = "requestCode";
    public static final String Key_RequestResult = "requestResult";
    public static final String ThirdpartyKey = "thirdparty";
    private boolean isCreate = false;
    private ETHIRDPARTYTYPE tptype = null;

    enum ETHIRDPARTYTYPE {
        ETHIRDPARTYTYPE_ALBAM,
        ETHIRDPARTYTYPE_PHOTO
    }

    public void onCreate(Bundle bundle) {
        Log.e("QLM", "third party create");
        getWindow().setBackgroundDrawable(null);
        this.isCreate = bundle == null;
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Log.e("QLM", "third party onNewIntent()");
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.e("QLM", "third party onResume()");
        super.onResume();
        if (this.isCreate) {
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            Intent goIntent = null;
            if (CommonIntentAction.ACTION_IMAGE_CAPTURE.equals(intent.getAction())) {
                goIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                if (intent.hasExtra(CommonIntentAction.EXTRA_IMAGE_SAEV_PATH)) {
                    goIntent.putExtra("output", Uri.fromFile(new File(Environment.getExternalStorageDirectory(), intent.getStringExtra(CommonIntentAction.EXTRA_IMAGE_SAEV_PATH))));
                }
            } else if (CommonIntentAction.ACTION_IMAGE_SELECT.equals(intent.getAction())) {
                goIntent = new Intent("android.intent.action.GET_CONTENT");
                goIntent.addCategory("android.intent.category.OPENABLE");
                goIntent.setType(IMAGEUNSPECIFIED);
            }
            if (goIntent != null) {
                startActivityForResult(goIntent, intent.getIntExtra("extra.image.reqcode", -1));
            }
            this.isCreate = false;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent backIntent = (Intent) getIntent().getExtras().get("extra.common.intent");
        Bundle bundle = new Bundle();
        bundle.putBoolean("extra.common.isThirdParty", true);
        bundle.putInt("extra.image.reqcode", requestCode);
        bundle.putInt("extra.common.resultCode", resultCode);
        bundle.putParcelable("extra.common.data", data);
        backIntent.putExtras(bundle);
        startActivity(backIntent);
        finish();
    }
}
