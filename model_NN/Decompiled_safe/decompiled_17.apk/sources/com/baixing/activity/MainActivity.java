package com.baixing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import com.baixing.activity.SplashJob;
import com.baixing.broadcast.push.PageJumper;
import com.baixing.data.GlobalDataManager;
import com.baixing.tracking.Sender;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.LocationService;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.view.fragment.HomePageFragment;
import com.quanleimu.activity.R;
import com.umeng.update.UmengUpdateAgent;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends BaseTabActivity implements SplashJob.JobDoneListener {
    public static final String WX_APP_ID = "wx862b30c868401dbc";
    private List<Runnable> pendingTask;
    private List<Runnable> resumeTask = new ArrayList();
    private SplashJob splashJob;

    /* access modifiers changed from: protected */
    public int getTabIndex() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ("android.intent.action.MAIN".equals(intent.getAction()) && GlobalDataManager.getInstance().getLastActiveClass() != null) {
            Intent go = new Intent();
            go.setClassName(this, GlobalDataManager.getInstance().getLastActiveClass().getName());
            go.addFlags(4194304);
            startActivity(go);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (!this.isChangingTab) {
            Log.d("ddd", "onpause");
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.APP_PAUSE).end();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!this.isChangingTab) {
            Log.d("ddd", "onresume");
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.APP_RESUME).end();
        }
        this.bundle.putString(BaseFragment.ARG_COMMON_BACK_HINT, "");
        super.onResume();
        if (getCurrentFragment() == null && this.splashJob == null) {
            this.splashJob = new SplashJob(this, this);
        }
        if (this.splashJob == null || this.splashJob.isJobDone()) {
            jumpToPage();
            responseOnResume();
        } else {
            this.splashJob.doSplashWork();
        }
        for (Runnable task : this.resumeTask) {
            task.run();
        }
        this.resumeTask.clear();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (!this.isChangingTab) {
            Log.d("ddd", "onstop");
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.APP_STOP).end();
            Tracker.getInstance().save();
            Sender.getInstance().notifySendMutex();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LocationService.getInstance().stop();
        super.onDestroy();
    }

    private void responseOnResume() {
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.d("quanleimu", "onSaveInstanceState");
        try {
            Sender.getInstance().save();
            Tracker.getInstance().save();
        } catch (Exception e) {
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d("quanleimu", "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.HomePageFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public void onJobDone() {
        PerformanceTracker.stamp(PerformEvent.Event.E_Handle_Jobdone);
        initTitleAction();
        if (!GlobalDataManager.update) {
            GlobalDataManager.update = true;
        }
        PerformanceTracker.stamp(PerformEvent.Event.E_Start_HomepageFragment);
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            pushFragment((BaseFragment) new HomePageFragment(), this.bundle, true);
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.APP_START).append(TrackConfig.TrackMobile.Key.CITY, GlobalDataManager.getInstance().getCityEnglishName()).end();
            Tracker.getInstance().save();
            Sender.getInstance().notifySendMutex();
        } else {
            notifyStackTop();
        }
        responseOnResume();
        this.splashJob = null;
        if (this.pendingTask != null && this.pendingTask.size() > 0) {
            for (Runnable task : this.pendingTask) {
                task.run();
            }
        }
        new Thread(new Runnable() {
            public void run() {
                new UpdateCityAndCatCommand(MainActivity.this).execute();
            }
        }).start();
        jumpToPage();
        PerformanceTracker.stamp(PerformEvent.Event.E_Handle_Jobdone_End);
    }

    private void jumpToPage() {
        Intent intent = getIntent();
        if (intent != null && intent.getBooleanExtra("pagejump", false)) {
            Bundle data = intent.getExtras();
            PageJumper.jumpToPage(this, data.getString("page"), data.getString(ThirdpartyTransitActivity.Key_Data));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        PerformanceTracker.stamp(PerformEvent.Event.E_MainActivity_Begin_Create);
        super.onCreate(savedInstanceState);
        GlobalDataManager.context = new WeakReference<>(this);
        this.pendingTask = new ArrayList();
        setContentView((int) R.layout.main_activity);
        onSetRootView(findViewById(R.id.root));
        findViewById(R.id.splash_cover).setVisibility(savedInstanceState == null ? 0 : 8);
        this.globalTabCtrl.attachView(findViewById(R.id.common_tab_layout), this);
        this.splashJob = new SplashJob(this, this);
        byte[] checkFlgData = Util.loadData(this, "umeng_update_check_flg");
        String checkFlg = checkFlgData == null ? null : new String(checkFlgData);
        String todayFlg = String.valueOf(new Date().getDate());
        if (checkFlg == null || !checkFlg.equals(todayFlg)) {
            UmengUpdateAgent.update(this);
            Util.saveDataToFile(this, null, "umeng_update_check_flg", todayFlg.getBytes());
        }
        PerformanceTracker.stamp(PerformEvent.Event.E_MainActivity_End_Create);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        if (!this.isChangingTab) {
            Log.d("ddd", "onstart");
        }
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public boolean handleIntent() {
        if ((this.intent.getFlags() & 1048576) != 0) {
            return false;
        }
        setIntent(this.intent);
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        handleBack();
        return true;
    }
}
