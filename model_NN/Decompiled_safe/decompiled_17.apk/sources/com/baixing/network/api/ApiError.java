package com.baixing.network.api;

public class ApiError {
    private String errorCode;
    private String msg;
    private String serverResponse;

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode2) {
        this.errorCode = errorCode2;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg2) {
        this.msg = msg2;
    }

    public String getServerResponse() {
        return this.serverResponse;
    }

    public void setServerResponse(String serverResponse2) {
        this.serverResponse = serverResponse2;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("errorCode:").append(this.errorCode).append(" msg:").append(this.msg).append(" serverResponse:").append(this.serverResponse);
        return builder.toString();
    }
}
