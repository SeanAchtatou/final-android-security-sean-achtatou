package com.baixing.network.api;

import android.content.Context;
import com.baixing.network.impl.GetRequest;
import com.baixing.network.impl.HttpNetworkConnector;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;

public class FileDownloadCommand {
    private String targetUrl;

    public FileDownloadCommand(String url) {
        this.targetUrl = url;
    }

    public boolean doDownload(Context context, File targetFile) {
        return ((Boolean) HttpNetworkConnector.connect().sendHttpRequestSync(context, new GetRequest(this.targetUrl, new HashMap(), false), new FileDownloadHandler(targetFile))).booleanValue();
    }

    class FileDownloadHandler implements HttpNetworkConnector.IResponseHandler<Boolean> {
        private OutputStream os;
        private File targetFile;

        FileDownloadHandler(File f) {
            this.targetFile = f;
        }

        public Boolean networkError(int respCode, String serverMessage) {
            return Boolean.FALSE;
        }

        public Boolean handleException(Exception ex) {
            return Boolean.FALSE;
        }

        public void handlePartialData(byte[] partOfResponseData, int len) {
            try {
                if (this.os == null) {
                    this.os = new FileOutputStream(this.targetFile);
                }
                this.os.write(partOfResponseData, 0, len);
            } catch (Throwable th) {
            }
        }

        public Boolean handleResponseEnd(String charset) {
            try {
                if (this.os != null) {
                    this.os.flush();
                    this.os.close();
                    return Boolean.TRUE;
                }
                Boolean bool = Boolean.FALSE;
                this.os = null;
                return bool;
            } catch (Throwable th) {
            } finally {
                this.os = null;
            }
        }
    }
}
