package com.baixing.network.api;

import com.baixing.network.NetworkUtil;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ApiParams implements Serializable {
    public static final String KEY_ACCESSTOKEN = "access_token";
    public static final String KEY_APIKEY = "api_key";
    public static final String KEY_APPID = "appId";
    public static final String KEY_CHANNEL = "channel";
    public static final String KEY_CITY = "city";
    public static final String KEY_TIMESTAMP = "timestamp";
    public static final String KEY_UDID = "udid";
    public static final String KEY_USERID = "userId";
    public static final String KEY_USERTOKEN = "BAPI-USER-TOKEN";
    public static final String KEY_VERSION = "version";
    private static final long serialVersionUID = 6811845003931804312L;
    private Map<String, Map<String, String>> aryParams = new HashMap();
    private Map<String, String> params = new HashMap();
    public boolean useCache = false;
    private String userToken = "";
    public boolean zipRequest = false;

    public void addParam(String key, String value) {
        addParameter(key, value);
    }

    public boolean hasParam(String key) {
        return this.params.containsKey(key);
    }

    public void addAll(Map<String, String> all) {
        this.params.putAll(all);
    }

    private void addParameter(String key, Object value) {
        if (value != null) {
            this.params.put(key, value.toString());
        }
    }

    public void addParam(String key, int value) {
        this.params.put(key, value + "");
    }

    public void addParam(String key, double value) {
        this.params.put(key, value + "");
    }

    public void addParam(String key, long value) {
        this.params.put(key, value + "");
    }

    public void addParam(String key, float value) {
        this.params.put(key, value + "");
    }

    public void addParam(String key, short value) {
        this.params.put(key, ((int) value) + "");
    }

    public void addParam(String key, boolean value) {
        addParameter(key, Boolean.valueOf(value));
    }

    public void addParam(String key, Map<String, String> param) {
        this.aryParams.put(key, param);
    }

    public String getParam(String key) {
        return this.params.get(key);
    }

    public void removeParam(String key) {
        this.params.remove(key);
    }

    public Map<String, String> getParams() {
        return this.params;
    }

    public Map<String, Map<String, String>> getAryParams() {
        return this.aryParams;
    }

    public void setParams(Map<String, String> params2) {
        this.params = params2;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        Map<String, String> map = this.params;
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                sb.append(((String) entry.getKey()) + ":" + ((String) entry.getValue()));
                sb.append(10);
            }
        }
        return sb.toString();
    }

    public String toUrlParams() {
        StringBuffer buf = new StringBuffer();
        for (String key : this.params.keySet()) {
            buf.append(key).append("=").append(URLEncoder.encode(this.params.get(key))).append("&");
        }
        if (buf.length() > 0) {
            buf.deleteCharAt(buf.length() - 1);
        }
        return buf.toString();
    }

    public void setAuthToken(String token) {
        this.userToken = token;
    }

    public String getAuthToken() {
        return this.userToken;
    }

    public static String generateUsertoken(String password) {
        return NetworkUtil.getMD5(password + BaseApiCommand.API_SECRET);
    }

    public static Map<String, String> generateApiAuthHeader(String url, byte[] rawPostData) {
        URL u = null;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Map<String, String> toRet = new HashMap<>();
        if (u != null) {
            toRet.put("BAPI-APP-KEY", BaseApiCommand.API_KEY);
            byte[] combined = new byte[(u.getPath().length() + rawPostData.length + BaseApiCommand.API_SECRET.length())];
            System.arraycopy(u.getPath().getBytes(), 0, combined, 0, u.getPath().length());
            System.arraycopy(rawPostData, 0, combined, u.getPath().length(), rawPostData.length);
            System.arraycopy(BaseApiCommand.API_SECRET.getBytes(), 0, combined, u.getPath().length() + rawPostData.length, BaseApiCommand.API_SECRET.length());
            toRet.put("BAPI-HASH", NetworkUtil.getMD5(combined));
        }
        return toRet;
    }
}
