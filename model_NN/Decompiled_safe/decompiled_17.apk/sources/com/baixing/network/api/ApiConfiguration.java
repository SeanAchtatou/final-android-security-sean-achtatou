package com.baixing.network.api;

import com.baixing.network.ICacheProxy;
import com.baixing.network.impl.HttpNetworkConnector;

public class ApiConfiguration {
    public static final void config(String apiHost, ICacheProxy proxy, String apiKey, String apiSecret) {
        BaseApiCommand.API_KEY = apiKey;
        BaseApiCommand.API_SECRET = apiSecret;
        setHost(apiHost);
        HttpNetworkConnector.cacheProxy = proxy;
    }

    public static final void setHost(String apiHost) {
        if (apiHost != null) {
            BaseApiCommand.HOST = apiHost;
            FileUploadCommand.HOST = apiHost;
        }
    }

    public static final String getHost() {
        return BaseApiCommand.HOST;
    }
}
