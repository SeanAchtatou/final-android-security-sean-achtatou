package com.baixing.network.api;

import android.util.Log;
import android.util.Pair;
import com.baixing.network.impl.HttpNetworkConnector;
import java.io.UnsupportedEncodingException;
import org.apache.http.util.ByteArrayBuffer;

public class PlainRespHandler implements HttpNetworkConnector.IResponseHandler<Pair<Boolean, String>> {
    private ByteArrayBuffer buf;

    public Pair<Boolean, String> networkError(int respCode, String serverMessage) {
        return createResponse(false, serverMessage);
    }

    public Pair<Boolean, String> handleException(Exception ex) {
        return createResponse(false, ex.getMessage());
    }

    public void handlePartialData(byte[] partOfResponseData, int len) {
        if (this.buf == null) {
            this.buf = new ByteArrayBuffer(4096);
        }
        if (len > 0) {
            this.buf.append(partOfResponseData, 0, len);
        }
    }

    public Pair<Boolean, String> handleResponseEnd(String charset) {
        String response = "";
        try {
            response = this.buf == null ? "" : new String(this.buf.toByteArray(), charset);
        } catch (UnsupportedEncodingException e) {
            Log.e("PlainResponseHandler", "fail to decode response " + e.getMessage());
        }
        return createResponse(true, response);
    }

    private Pair<Boolean, String> createResponse(boolean succed, String responseData) {
        Boolean valueOf = Boolean.valueOf(succed);
        if (responseData == null) {
            responseData = "";
        }
        Pair p = new Pair(valueOf, responseData);
        if (this.buf != null) {
            this.buf.clear();
        }
        return p;
    }
}
