package com.baixing.network.api;

import android.content.Context;
import android.text.TextUtils;
import com.upyun.api.Uploader;
import com.upyun.api.utils.UpYunException;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class FileUploadCommand {
    static String HOST = "www.baixing.com";
    private String filePath;

    public static class ImageConfig {
        public String policy;
        public String server;
        public String signature;
    }

    private FileUploadCommand(String filepath) {
        this.filePath = filepath;
    }

    public static FileUploadCommand create(String filePath2) {
        return new FileUploadCommand(filePath2);
    }

    private String getUrl() {
        return "http://" + HOST + "/image_upload/";
    }

    public String doUpload(Context context, ImageConfig config) {
        try {
            String policy = TextUtils.isEmpty(config.policy) ? "eyJidWNrZXQiOiJieGltZyIsInNhdmUta2V5IjoiL3tmaWxlbWQ1fXsuc3VmZml4fSIsImNvbnRlbnQtc2VjcmV0Ijoib3JpZ2luYWwiLCJjb250ZW50LWxlbmd0aC1yYW5nZSI6IjAsIDEwNDg1NzYwIiwieC1nbWtlcmwtdHlwZSI6ImZpeF9tYXgiLCJ4LWdta2VybC12YWx1ZSI6IjY0MCIsIngtZ21rZXJsLW5vaWNjIjp0cnVlLCJ4LWdta2VybC1xdWFsaXR5IjoiNzUiLCJ4LWdta2VybC1yb3RhdGUiOiJhdXRvIiwieC1nbWtlcmwtZXhpZi1zd2l0Y2giOnRydWUsIm5vdGlmeS11cmwiOiJodHRwOi8vaW1nLmJhaXhpbmcubmV0L25vdGlmeS9ieGltZy5waHAiLCJleHBpcmF0aW9uIjoxMzg0ODM3NzkxfQ==" : config.policy;
            String signature = TextUtils.isEmpty(config.signature) ? "ee5d5fd1508f37e4f15723aae12c41e0" : config.signature;
            String server = TextUtils.isEmpty(config.server) ? "http://v0.api.upyun.com/bximg" : config.server;
            String url = Uploader.upload(policy, signature, server.substring(server.indexOf("upyun.com/") + "upyun.com/".length()), this.filePath);
            if (url.startsWith(CookieSpec.PATH_DELIM)) {
                url = url.substring(1);
            }
            return url;
        } catch (UpYunException e) {
            e.printStackTrace();
            return "";
        }
    }
}
