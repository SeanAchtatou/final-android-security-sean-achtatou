package com.baixing.network.api;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import com.baixing.network.impl.GetRequest;
import com.baixing.network.impl.HttpNetworkConnector;
import com.baixing.network.impl.IHttpRequest;
import com.baixing.network.impl.IRequestStatusListener;
import com.baixing.network.impl.PostRequest;
import com.baixing.network.impl.ZippedPostRequest;
import java.util.Map;

public final class BaseApiCommand implements IRequestStatusListener {
    static String API_KEY = "";
    static String API_SECRET = "";
    static String HOST = "www.baixing.com";
    private static final String TAG = "ApiCommand";
    private static ApiParams commonParams = new ApiParams();
    private String apiName;
    private ApiParams apiParams;
    private Callback callback;
    private boolean isGet;
    private IHttpRequest request;

    public interface Callback {
        void onNetworkDone(String str, String str2);

        void onNetworkFail(String str, ApiError apiError);
    }

    protected BaseApiCommand(String apiName2, ApiParams params, boolean isGetRequest) {
        this.apiName = apiName2;
        this.apiParams = params;
        this.isGet = isGetRequest;
        if (this.apiParams == null) {
            this.apiParams = new ApiParams();
        }
    }

    public static BaseApiCommand createCommand(String apiName2, boolean isGet2, ApiParams params) {
        return new BaseApiCommand(apiName2, params, isGet2);
    }

    private static String getApiUri(String apiName2) {
        return "http://" + HOST + "/api/mobile/" + apiName2;
    }

    public static void addCommonParams(String key, String value) {
        commonParams.addParam(key, value);
    }

    public void execute(Context context, Callback callback2) {
        HttpNetworkConnector connector = preSending();
        if (connector != null) {
            this.callback = callback2;
            connector.sendHttpRequest(context, this.request, new PlainRespHandler(), this);
        }
    }

    public String executeSync(Context context) {
        Pair<Boolean, String> result;
        HttpNetworkConnector connector = preSending();
        if (connector == null || (result = (Pair) connector.sendHttpRequestSync(context, this.request, new PlainRespHandler())) == null) {
            return null;
        }
        return decodeUnicode((String) result.second);
    }

    private HttpNetworkConnector preSending() {
        if (this.request != null && !this.request.isCanceled()) {
            return null;
        }
        Map<String, String> params = mergeParams();
        String url = getApiUri(this.apiName);
        this.request = this.isGet ? new GetRequest(url, params, this.apiParams.useCache) : this.apiParams.zipRequest ? new ZippedPostRequest(url, params, this.apiParams.getAryParams()) : new PostRequest(url, params, this.apiParams.getAryParams());
        if (!TextUtils.isEmpty(this.apiParams.getAuthToken())) {
            this.request.addHeader(ApiParams.KEY_USERTOKEN, this.apiParams.getAuthToken());
        }
        return HttpNetworkConnector.connect();
    }

    private Map<String, String> mergeParams() {
        ApiParams allParams = new ApiParams();
        Map<String, String> common = commonParams.getParams();
        allParams.addAll(this.apiParams.getParams());
        for (String key : common.keySet()) {
            if (!allParams.hasParam(key)) {
                allParams.addParam(key, common.get(key));
            }
        }
        return allParams.getParams();
    }

    public void cancel() {
        if (this.request != null) {
            this.request.cancel();
            this.request = null;
        }
    }

    public void onConnectionStart() {
    }

    public void onReceiveData(long cursor, long total) {
    }

    public void onProcessingData() {
    }

    public void onCancel() {
        this.request.cancel();
    }

    public void onRequestDone(Object response) {
        if (this.request == null || !this.request.isCanceled()) {
            this.request = null;
            if (response == null) {
                fail("unknown error, response is null");
                return;
            }
            Pair<Boolean, String> result = (Pair) response;
            String decodedResponse = decodeUnicode((String) result.second);
            ApiError error = parseForError(decodedResponse);
            if (!((Boolean) result.first).booleanValue()) {
                this.callback.onNetworkFail(this.apiName, error);
            } else if (error == null) {
                this.callback.onNetworkDone(this.apiName, decodedResponse);
            } else {
                this.callback.onNetworkFail(this.apiName, error);
            }
        }
    }

    private void fail(String message) {
        ApiError error = new ApiError();
        error.setMsg(message);
        this.callback.onNetworkFail(this.apiName, error);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.baixing.network.api.ApiError parseForError(java.lang.String r10) {
        /*
            r9 = this;
            r5 = 0
            java.lang.String r6 = r10.trim()
            java.lang.String r7 = "{\"error\""
            boolean r6 = r6.startsWith(r7)
            if (r6 != 0) goto L_0x000f
            r1 = r5
        L_0x000e:
            return r1
        L_0x000f:
            r3 = 0
            r1 = 0
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0042 }
            r4.<init>(r10)     // Catch:{ JSONException -> 0x0042 }
            com.baixing.network.api.ApiError r2 = new com.baixing.network.api.ApiError     // Catch:{ JSONException -> 0x005c }
            r2.<init>()     // Catch:{ JSONException -> 0x005c }
            java.lang.String r6 = "error"
            java.lang.String r6 = r4.getString(r6)     // Catch:{ JSONException -> 0x005f }
            r2.setErrorCode(r6)     // Catch:{ JSONException -> 0x005f }
            java.lang.String r6 = "message"
            java.lang.String r6 = r4.getString(r6)     // Catch:{ JSONException -> 0x005f }
            r2.setMsg(r6)     // Catch:{ JSONException -> 0x005f }
            r2.setServerResponse(r10)     // Catch:{ JSONException -> 0x005f }
            r1 = r2
            r3 = r4
        L_0x0032:
            if (r1 == 0) goto L_0x000e
            java.lang.String r6 = "0"
            java.lang.String r7 = r1.getErrorCode()
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x000e
            r1 = r5
            goto L_0x000e
        L_0x0042:
            r0 = move-exception
        L_0x0043:
            java.lang.String r6 = "ApiCommand"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "fail to parse response "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r10)
            java.lang.String r7 = r7.toString()
            android.util.Log.w(r6, r7)
            goto L_0x0032
        L_0x005c:
            r0 = move-exception
            r3 = r4
            goto L_0x0043
        L_0x005f:
            r0 = move-exception
            r1 = r2
            r3 = r4
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.network.api.BaseApiCommand.parseForError(java.lang.String):com.baixing.network.api.ApiError");
    }

    public static final String decodeUnicode(String source) {
        return source;
    }
}
