package com.baixing.network.impl;

public interface IRequestStatusListener {
    void onCancel();

    void onConnectionStart();

    void onProcessingData();

    void onReceiveData(long j, long j2);

    void onRequestDone(Object obj);
}
