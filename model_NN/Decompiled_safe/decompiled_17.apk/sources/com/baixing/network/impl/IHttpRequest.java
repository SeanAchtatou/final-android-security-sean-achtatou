package com.baixing.network.impl;

import android.util.Pair;
import java.io.OutputStream;
import java.util.List;

public interface IHttpRequest {

    public enum CACHE_POLICY {
        CACHE_PREF_CACHE,
        CACHE_ONLY_NEW,
        CACHE_NOT_CACHEABLE
    }

    void addHeader(String str, String str2);

    void cancel();

    CACHE_POLICY getCachePolicy();

    String getContentType();

    List<Pair<String, String>> getHeaders();

    String getRawPostData();

    String getUrl();

    boolean isCanceled();

    boolean isGetRequest();

    boolean isZipRequest();

    void removeHeader(String str);

    int writeContent(OutputStream outputStream);
}
