package com.baixing.network.impl;

import android.util.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class BaseHttpRequest implements IHttpRequest {
    public static final String TAG = "BaseHttpRequest";
    protected String baseUrl;
    private Map<String, String> headers = new HashMap();
    private boolean isCancel;

    protected BaseHttpRequest(String url) {
        this.baseUrl = url;
    }

    public String getRawPostData() {
        return "";
    }

    public void addHeader(String name, String value) {
        this.headers.put(name, value);
    }

    public String getUrl() {
        return this.baseUrl;
    }

    public boolean isZipRequest() {
        return false;
    }

    public void removeHeader(String name) {
        this.headers.remove(name);
    }

    public List<Pair<String, String>> getHeaders() {
        List<Pair<String, String>> list = new ArrayList<>();
        for (String name : this.headers.keySet()) {
            list.add(new Pair(name, this.headers.get(name)));
        }
        return list;
    }

    public void cancel() {
        this.isCancel = true;
    }

    public boolean isCanceled() {
        return this.isCancel;
    }
}
