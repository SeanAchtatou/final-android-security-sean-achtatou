package com.baixing.network.impl;

import android.util.Log;
import com.baixing.network.impl.IHttpRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public class PostRequest extends RestHttpRequest {
    public PostRequest(String baseUrl, Map<String, String> parameters, Map<String, Map<String, String>> aryParams) {
        super(baseUrl, parameters, aryParams);
    }

    public IHttpRequest.CACHE_POLICY getCachePolicy() {
        return IHttpRequest.CACHE_POLICY.CACHE_NOT_CACHEABLE;
    }

    public boolean isGetRequest() {
        return false;
    }

    public int writeContent(OutputStream out) {
        String params = getRawPostData();
        if (params == null) {
            return 0;
        }
        try {
            byte[] buf = params.getBytes();
            out.write(buf);
            out.flush();
            return buf.length;
        } catch (IOException e) {
            Log.d(BaseHttpRequest.TAG, "exception when write data to outputstream." + e.getMessage());
            return 0;
        }
    }
}
