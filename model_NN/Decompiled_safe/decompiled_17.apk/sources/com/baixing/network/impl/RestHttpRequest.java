package com.baixing.network.impl;

import android.text.TextUtils;
import android.util.Log;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class RestHttpRequest extends BaseHttpRequest {
    public static final String DEFAULT_CHARSET = "UTF-8";
    protected Map<String, Map<String, String>> aryParams;
    protected String charset;
    protected Map<String, String> parameters;

    public /* bridge */ /* synthetic */ void addHeader(String x0, String x1) {
        super.addHeader(x0, x1);
    }

    public /* bridge */ /* synthetic */ void cancel() {
        super.cancel();
    }

    public /* bridge */ /* synthetic */ List getHeaders() {
        return super.getHeaders();
    }

    public /* bridge */ /* synthetic */ String getUrl() {
        return super.getUrl();
    }

    public /* bridge */ /* synthetic */ boolean isCanceled() {
        return super.isCanceled();
    }

    public /* bridge */ /* synthetic */ boolean isZipRequest() {
        return super.isZipRequest();
    }

    public /* bridge */ /* synthetic */ void removeHeader(String x0) {
        super.removeHeader(x0);
    }

    protected RestHttpRequest(String baseUrl, Map<String, String> parameters2) {
        this(baseUrl, parameters2, "UTF-8");
    }

    protected RestHttpRequest(String baseUrl, Map<String, String> parameters2, Map<String, Map<String, String>> aryParams2) {
        this(baseUrl, parameters2, "UTF-8");
        this.aryParams = aryParams2;
    }

    protected RestHttpRequest(String baseUrl, Map<String, String> parameters2, String charset2) {
        super(baseUrl);
        this.parameters = parameters2;
        this.charset = charset2;
    }

    public String getRawPostData() {
        if (isGetRequest()) {
            return "";
        }
        JSONObject obj = new JSONObject();
        for (Map.Entry<String, String> entry : this.parameters.entrySet()) {
            boolean valueIsJsonObj = false;
            boolean valueIsJsonAry = false;
            if (((String) entry.getValue()).startsWith("[") && ((String) entry.getValue()).endsWith("]")) {
                try {
                    new JSONArray((String) entry.getValue());
                    valueIsJsonAry = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (((String) entry.getValue()).startsWith("{") && ((String) entry.getValue()).endsWith("}")) {
                try {
                    new JSONObject((String) entry.getValue());
                    valueIsJsonObj = true;
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
            if (valueIsJsonObj) {
                try {
                    obj.put((String) entry.getKey(), new JSONObject((String) entry.getValue()));
                } catch (JSONException e3) {
                    e3.printStackTrace();
                }
            } else if (valueIsJsonAry) {
                obj.put((String) entry.getKey(), new JSONArray((String) entry.getValue()));
            } else {
                obj.put((String) entry.getKey(), entry.getValue());
            }
        }
        if (this.aryParams != null) {
            for (Map.Entry<String, Map<String, String>> entry2 : this.aryParams.entrySet()) {
                try {
                    obj.put((String) entry2.getKey(), new JSONObject((Map) entry2.getValue()));
                } catch (JSONException e4) {
                    e4.printStackTrace();
                }
            }
        }
        return obj.toString();
    }

    /* access modifiers changed from: protected */
    public String getFormatParams() {
        Map<String, String> params = this.parameters;
        if (params == null || params.isEmpty()) {
            return null;
        }
        StringBuilder query = new StringBuilder();
        boolean hasParam = false;
        List<String> keyList = new ArrayList<>(params.keySet());
        Collections.sort(keyList);
        for (String key : keyList) {
            String name = key;
            String value = params.get(key);
            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(value)) {
                if (hasParam) {
                    query.append("&");
                } else {
                    hasParam = true;
                }
                String encodedName = "";
                String encodedValue = "";
                try {
                    encodedName = URLEncoder.encode(name);
                    encodedValue = URLEncoder.encode(value);
                } catch (Throwable th) {
                    Log.w(BaseHttpRequest.TAG, "fail to encoded parameter [key, value]:[" + name + "," + value + "] with charset " + this.charset);
                }
                query.append(encodedName).append("=").append(encodedValue);
            }
        }
        return query.toString();
    }

    public static String urlEncode(String str) {
        return str.replaceAll(":", "%3A").replaceAll(" ", "%20").replaceAll("\\(", "%28").replaceAll("\\)", "%29").replaceAll(CookieSpec.PATH_DELIM, "%2F").replaceAll("\\+", "%20").replaceAll("\\*", "%2A").replaceAll("\\,", "%2C");
    }

    public String getContentType() {
        return PostMethod.FORM_URL_ENCODED_CONTENT_TYPE;
    }
}
