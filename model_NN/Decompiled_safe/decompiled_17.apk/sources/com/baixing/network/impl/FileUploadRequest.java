package com.baixing.network.impl;

import com.baixing.network.impl.IHttpRequest;
import com.baixing.util.TraceUtil;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;
import org.apache.commons.httpclient.methods.multipart.FilePart;

public class FileUploadRequest extends BaseHttpRequest {
    public static final String BOUNDARY = "---------------------------19861025304733";
    private String filePath;

    public /* bridge */ /* synthetic */ void addHeader(String x0, String x1) {
        super.addHeader(x0, x1);
    }

    public /* bridge */ /* synthetic */ void cancel() {
        super.cancel();
    }

    public /* bridge */ /* synthetic */ List getHeaders() {
        return super.getHeaders();
    }

    public /* bridge */ /* synthetic */ String getRawPostData() {
        return super.getRawPostData();
    }

    public /* bridge */ /* synthetic */ String getUrl() {
        return super.getUrl();
    }

    public /* bridge */ /* synthetic */ boolean isCanceled() {
        return super.isCanceled();
    }

    public /* bridge */ /* synthetic */ boolean isZipRequest() {
        return super.isZipRequest();
    }

    public /* bridge */ /* synthetic */ void removeHeader(String x0) {
        super.removeHeader(x0);
    }

    public FileUploadRequest(String url, String filePath2) {
        super(url);
        this.filePath = filePath2;
    }

    private static String getMimeType(byte[] fileContentBuf) {
        return getMimeType(getFileSuffix(fileContentBuf));
    }

    private static String getMimeType(String suffix) {
        if ("JPG".equals(suffix)) {
            return "image/jpeg";
        }
        if ("GIF".equals(suffix)) {
            return "image/gif";
        }
        if ("PNG".equals(suffix)) {
            return "image/png";
        }
        if ("BMP".equals(suffix)) {
            return "image/bmp";
        }
        return FilePart.DEFAULT_CONTENT_TYPE;
    }

    private static String getFileSuffix(byte[] bytes) {
        if (bytes == null || bytes.length < 10) {
            return null;
        }
        if (bytes[0] == 71 && bytes[1] == 73 && bytes[2] == 70) {
            return "GIF";
        }
        if (bytes[1] == 80 && bytes[2] == 78 && bytes[3] == 71) {
            return "PNG";
        }
        if (bytes[6] == 74 && bytes[7] == 70 && bytes[8] == 73 && bytes[9] == 70) {
            return "JPG";
        }
        if (bytes[0] == 66 && bytes[1] == 77) {
            return "BMP";
        }
        return null;
    }

    public String getContentType() {
        return "multipart/form-data; boundary=---------------------------19861025304733";
    }

    public boolean isGetRequest() {
        return false;
    }

    public int writeContent(OutputStream out) {
        int count;
        StringBuffer boundary = new StringBuffer().append("--").append(BOUNDARY).append(TraceUtil.LINE);
        boolean firstTime = true;
        int contentLen = 0;
        try {
            FileInputStream ins = new FileInputStream(this.filePath);
            byte[] buffer = new byte[4096];
            do {
                count = ins.read(buffer);
                if (firstTime) {
                    String fileSuffix = getFileSuffix(buffer);
                    boundary = boundary.append("Content-Disposition: form-data; name=\"file\"; filename=\"iphonefile." + fileSuffix + "\"\r\n").append("Content-Type: " + getMimeType(fileSuffix) + "\r\n\r\n");
                    byte[] boundaryStart = boundary.toString().getBytes();
                    out.write(boundaryStart);
                    firstTime = false;
                    contentLen += boundaryStart.length;
                }
                if (count > 0) {
                    out.write(buffer, 0, count);
                    contentLen += count;
                    continue;
                }
            } while (count > 0);
            ins.close();
            byte[] boundaryEnd = "\r\n-----------------------------19861025304733--\r\n".getBytes();
            out.write(boundaryEnd);
            return contentLen + boundaryEnd.length;
        } catch (Throwable th) {
            return 0;
        }
    }

    public IHttpRequest.CACHE_POLICY getCachePolicy() {
        return IHttpRequest.CACHE_POLICY.CACHE_NOT_CACHEABLE;
    }
}
