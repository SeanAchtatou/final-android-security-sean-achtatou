package com.baixing.network.impl;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.httpclient.methods.multipart.FilePart;

public class ZippedPostRequest extends PostRequest {
    public ZippedPostRequest(String baseUrl, Map<String, String> parameters, Map<String, Map<String, String>> aryParams) {
        super(baseUrl, parameters, aryParams);
        this.charset = "ISO-8859-1";
    }

    public String getContentType() {
        return FilePart.DEFAULT_CONTENT_TYPE;
    }

    public int writeContent(OutputStream out) {
        String params = getRawPostData();
        if (params == null) {
            return 0;
        }
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            GZIPOutputStream gzip = new GZIPOutputStream(bo);
            gzip.write(params.getBytes());
            gzip.close();
            byte[] content = bo.toByteArray();
            out.write(content);
            out.flush();
            return content.length;
        } catch (IOException e) {
            Log.d(BaseHttpRequest.TAG, "exception when write data to outputstream." + e.getMessage());
            return 0;
        }
    }

    public boolean isZipRequest() {
        return true;
    }
}
