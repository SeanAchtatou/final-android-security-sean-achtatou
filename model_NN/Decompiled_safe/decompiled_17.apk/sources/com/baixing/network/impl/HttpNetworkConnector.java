package com.baixing.network.impl;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.baixing.network.ICacheProxy;
import com.baixing.network.NetworkProfiler;
import com.baixing.network.api.ApiParams;
import com.baixing.network.impl.IHttpRequest;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HttpNetworkConnector {
    public static final String DEFAULT_CHARSET = "UTF-8";
    public static final long DEFAULT_CONNECTION_TIMEOUT = 30000;
    public static final long DEFAULT_READ_TIMEOUT = 30000;
    public static final int READ_BUFFER_SIZE = 4096;
    private static final String USER_AGENT = "BaixingMobileApi";
    public static ICacheProxy cacheProxy;
    long connectionTimeout = 30000;
    long readTimeout = 30000;
    /* access modifiers changed from: private */
    public Object syncRequestResponse;

    public interface IResponseHandler<T> {
        T handleException(Exception exc);

        void handlePartialData(byte[] bArr, int i);

        T handleResponseEnd(String str);

        T networkError(int i, String str);
    }

    private HttpNetworkConnector() {
    }

    public static HttpNetworkConnector connect() {
        return new HttpNetworkConnector();
    }

    public void sendHttpRequest(Context context, IHttpRequest request, IResponseHandler responseHandler, IRequestStatusListener statusListener) {
        final Context context2 = context;
        final IHttpRequest iHttpRequest = request;
        final IResponseHandler iResponseHandler = responseHandler;
        final IRequestStatusListener iRequestStatusListener = statusListener;
        new Thread(new Runnable() {
            public void run() {
                HttpNetworkConnector.this.doSend(context2, iHttpRequest, iResponseHandler, iRequestStatusListener);
            }
        }).start();
    }

    public Object sendHttpRequestSync(Context context, IHttpRequest request, IResponseHandler responseHandler) {
        this.syncRequestResponse = null;
        doSend(context, request, responseHandler, new IRequestStatusListener() {
            public void onConnectionStart() {
            }

            public void onReceiveData(long cursor, long total) {
            }

            public void onProcessingData() {
            }

            public void onCancel() {
            }

            public void onRequestDone(Object response) {
                Object unused = HttpNetworkConnector.this.syncRequestResponse = response;
            }
        });
        return this.syncRequestResponse;
    }

    private static String dealCacheKey(String url) {
        if (TextUtils.isEmpty(url)) {
            return "";
        }
        String ret = new String(url);
        Matcher m = Pattern.compile("access_token=\\w+").matcher(url);
        if (m.find()) {
            ret = ret.replace(m.group(), "");
        }
        Matcher m2 = Pattern.compile("timestamp=\\w+").matcher(ret);
        if (m2.find()) {
            ret = ret.replace(m2.group(), "");
        }
        return ret.replaceAll("\\?\\&", "\\?").replaceAll("\\&\\&", "\\&");
    }

    /* access modifiers changed from: private */
    public void doSend(Context context, IHttpRequest request, IResponseHandler responseHandler, IRequestStatusListener listener) {
        Object businessResponse;
        String cacheData;
        HttpURLConnection connection = null;
        String targetUrl = request.getUrl();
        if (!(request.getCachePolicy() != IHttpRequest.CACHE_POLICY.CACHE_PREF_CACHE || cacheProxy == null || (cacheData = cacheProxy.onLoad(dealCacheKey(targetUrl))) == null)) {
            try {
                if (!request.isCanceled()) {
                    byte[] data = cacheData.getBytes("UTF-8");
                    responseHandler.handlePartialData(data, data.length);
                    listener.onRequestDone(responseHandler.handleResponseEnd("UTF-8"));
                    return;
                }
                return;
            } catch (Throwable th) {
            }
        }
        int requestSize = 0;
        int responseSize = 0;
        try {
            NetworkProfiler.startUrl(targetUrl);
            Log.d("targetUrl", targetUrl);
            connection = getConnection(context, new URL(targetUrl), request.isGetRequest(), request.getContentType());
            if (listener != null) {
                listener.onConnectionStart();
            }
            List<Pair<String, String>> headers = request.getHeaders();
            if (headers != null) {
                for (Pair<String, String> header : headers) {
                    connection.addRequestProperty((String) header.first, (String) header.second);
                }
            }
            byte[] rawPostData = new byte[0];
            if (!request.isGetRequest()) {
                rawPostData = request.getRawPostData().getBytes();
                if (request.isZipRequest()) {
                    ByteArrayOutputStream bo = new ByteArrayOutputStream();
                    GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(bo);
                    gZIPOutputStream.write(rawPostData);
                    gZIPOutputStream.close();
                    rawPostData = bo.toByteArray();
                }
            }
            for (Map.Entry<String, String> entry : ApiParams.generateApiAuthHeader(targetUrl, rawPostData).entrySet()) {
                connection.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            if (!request.isGetRequest()) {
                connection.setConnectTimeout((int) this.connectionTimeout);
                connection.setReadTimeout((int) this.readTimeout);
                requestSize = 0 + request.writeContent(connection.getOutputStream());
            } else {
                requestSize = 0 + targetUrl.length();
            }
            String charset = getResponseCharset(connection.getContentType());
            String header2 = connection.getHeaderField("Content-Encoding");
            boolean isGzip = false;
            if (header2 != null && header2.toLowerCase().contains("gzip")) {
                isGzip = true;
            }
            InputStream es = connection.getErrorStream();
            if (es == null) {
                InputStream input = connection.getInputStream();
                if (isGzip) {
                    input = new GZIPInputStream(input);
                }
                long cursor = 0;
                long totalLen = (long) getResponseLen(connection.getHeaderField("Content-Length"), input.available());
                if (!request.isCanceled()) {
                    if (listener != null) {
                        listener.onReceiveData(0, totalLen);
                    }
                    responseSize = (int) (((long) 0) + totalLen);
                    ByteArrayOutputStream cacheOs = request.getCachePolicy() != IHttpRequest.CACHE_POLICY.CACHE_NOT_CACHEABLE ? new ByteArrayOutputStream() : null;
                    byte[] buffer = new byte[4096];
                    int totalReadCount = 0;
                    int count = input.read(buffer);
                    while (count > 0) {
                        totalReadCount += count;
                        responseHandler.handlePartialData(buffer, count);
                        if (cacheOs != null) {
                            cacheOs.write(buffer, 0, count);
                        }
                        cursor += (long) count;
                        if (!request.isCanceled()) {
                            if (listener != null) {
                                listener.onReceiveData(cursor, totalLen);
                            }
                            count = input.read(buffer);
                        } else {
                            if (connection != null) {
                                connection.disconnect();
                            }
                            NetworkProfiler.endUrl(targetUrl, requestSize, responseSize, "canceled:" + request.isCanceled());
                            return;
                        }
                    }
                    if (totalLen < ((long) totalReadCount)) {
                        responseSize = totalReadCount;
                    }
                    if (!request.isCanceled()) {
                        if (listener != null) {
                            listener.onProcessingData();
                        }
                        businessResponse = responseHandler.handleResponseEnd(charset);
                        if (!(cacheOs == null || cacheProxy == null)) {
                            cacheProxy.onSave(dealCacheKey(targetUrl), cacheOs.toString());
                        }
                    } else {
                        if (connection != null) {
                            connection.disconnect();
                        }
                        NetworkProfiler.endUrl(targetUrl, requestSize, responseSize, "canceled:" + request.isCanceled());
                        return;
                    }
                } else {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    NetworkProfiler.endUrl(targetUrl, requestSize, 0, "canceled:" + request.isCanceled());
                    return;
                }
            } else {
                if (isGzip) {
                    es = new GZIPInputStream(es);
                }
                String msg = getStreamAsString(es, charset);
                businessResponse = TextUtils.isEmpty(msg) ? responseHandler.networkError(connection.getResponseCode(), connection.getResponseMessage()) : responseHandler.networkError(connection.getResponseCode(), msg);
            }
            if (connection != null) {
                connection.disconnect();
            }
            NetworkProfiler.endUrl(targetUrl, requestSize, responseSize, "canceled:" + request.isCanceled());
        } catch (MalformedURLException e) {
            businessResponse = responseHandler.handleException(new Exception("请求URL出错, " + e.getMessage()));
            if (connection != null) {
                connection.disconnect();
            }
            NetworkProfiler.endUrl(targetUrl, requestSize, responseSize, "canceled:" + request.isCanceled());
        } catch (Throwable th2) {
            if (connection != null) {
                connection.disconnect();
            }
            NetworkProfiler.endUrl(targetUrl, requestSize, responseSize, "canceled:" + request.isCanceled());
            throw th2;
        }
        if (!request.isCanceled() && listener != null) {
            listener.onRequestDone(businessResponse);
        }
    }

    private static HttpURLConnection getConnection(Context context, URL url, boolean isGet, String ctype) throws IOException {
        HttpURLConnection conn;
        HttpsURLConnection connHttps;
        Cursor mCursor;
        Proxy proxy = null;
        if (!((WifiManager) context.getSystemService("wifi")).isWifiEnabled() && (mCursor = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null)) != null && mCursor.moveToFirst()) {
            String proxyStr = mCursor.getString(mCursor.getColumnIndex("proxy"));
            if (proxyStr != null && proxyStr.trim().length() > 0) {
                proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyStr, 80));
            }
            mCursor.close();
        }
        if ("https".equals(url.getProtocol())) {
            try {
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
                if (proxy == null) {
                    connHttps = (HttpsURLConnection) url.openConnection();
                } else {
                    connHttps = (HttpsURLConnection) url.openConnection(proxy);
                }
                connHttps.setSSLSocketFactory(ctx.getSocketFactory());
                connHttps.setHostnameVerifier(new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
                conn = connHttps;
            } catch (Exception e) {
                throw new IOException(e.getMessage());
            }
        } else if (proxy == null) {
            conn = (HttpURLConnection) url.openConnection();
        } else {
            conn = (HttpURLConnection) url.openConnection(proxy);
        }
        conn.setRequestMethod(isGet ? "GET" : "POST");
        conn.setDoInput(true);
        if (!isGet) {
            conn.setDoOutput(true);
        }
        conn.setRequestProperty("Accept", "text/xml,text/javascript,text/html");
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setRequestProperty("Content-Type", ctype);
        conn.setRequestProperty("Accept-Language", "zh-CN, en-us, en");
        conn.setRequestProperty("Accept-Encoding", "gzip");
        return conn;
    }

    private static class DefaultTrustManager implements X509TrustManager {
        private DefaultTrustManager() {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }
    }

    private static int getResponseLen(String contentLen, int defaultValue) {
        try {
            return Integer.valueOf(contentLen).intValue();
        } catch (Throwable th) {
            return defaultValue;
        }
    }

    private static String getResponseCharset(String ctype) {
        if (TextUtils.isEmpty(ctype)) {
            return "UTF-8";
        }
        for (String param : ctype.split(";")) {
            String param2 = param.trim();
            if (param2.startsWith("charset")) {
                String[] pair = param2.split("=", 2);
                if (pair.length != 2 || TextUtils.isEmpty(pair[1])) {
                    return "UTF-8";
                }
                return pair[1].trim();
            }
        }
        return "UTF-8";
    }

    private static String getStreamAsString(InputStream stream, String charset) throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset));
            StringWriter writer = new StringWriter();
            char[] chars = new char[256];
            while (true) {
                int count = reader.read(chars);
                if (count <= 0) {
                    break;
                }
                writer.write(chars, 0, count);
            }
            return writer.toString();
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }
}
