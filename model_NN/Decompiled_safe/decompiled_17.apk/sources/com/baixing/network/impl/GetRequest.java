package com.baixing.network.impl;

import android.text.TextUtils;
import com.baixing.network.impl.IHttpRequest;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class GetRequest extends RestHttpRequest {
    private boolean useCache;

    public GetRequest(String baseUrl, Map<String, String> parameters, boolean useCache2) {
        super(baseUrl, parameters);
        this.useCache = useCache2;
    }

    public String getUrl() {
        String query = getFormatParams();
        if (TextUtils.isEmpty(query)) {
            return this.baseUrl;
        }
        String strUrl = this.baseUrl;
        try {
            if (TextUtils.isEmpty(new URL(strUrl).getQuery())) {
                if (strUrl.endsWith("?")) {
                    return strUrl + query;
                }
                return strUrl + "?" + query;
            } else if (strUrl.endsWith("&")) {
                return strUrl + query;
            } else {
                return strUrl + "&" + query;
            }
        } catch (MalformedURLException e) {
            return this.baseUrl;
        }
    }

    public IHttpRequest.CACHE_POLICY getCachePolicy() {
        return this.useCache ? IHttpRequest.CACHE_POLICY.CACHE_PREF_CACHE : IHttpRequest.CACHE_POLICY.CACHE_ONLY_NEW;
    }

    public int writeContent(OutputStream out) {
        return 0;
    }

    public boolean isGetRequest() {
        return true;
    }
}
