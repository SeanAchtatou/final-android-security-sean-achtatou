package com.baixing.network;

import com.baixing.util.TraceUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NetworkProfiler {
    public static final int BUFFER_SIZE = 100;
    public static final String TAB = "    ";
    private static boolean enable = false;
    /* access modifiers changed from: private */
    public static String outputPath;
    private static Map<String, ProfileRecord> recordMapper = new HashMap();
    /* access modifiers changed from: private */
    public static ArrayList<ProfileRecord> records = new ArrayList<>();

    static class ProfileRecord {
        /* access modifiers changed from: private */
        public long endTime;
        /* access modifiers changed from: private */
        public String[] extralInfo;
        private boolean isDeprecated = false;
        /* access modifiers changed from: private */
        public int requestSize;
        /* access modifiers changed from: private */
        public int responseSize;
        private long startTime;
        /* access modifiers changed from: private */
        public String url;

        ProfileRecord(long startTime2, boolean deprecate) {
            this.startTime = startTime2;
            this.isDeprecated = deprecate;
        }

        public String toString() {
            StringBuffer buf = new StringBuffer();
            buf.append(this.isDeprecated ? this.url + "_deprecated" : this.url).append(NetworkProfiler.TAB).append(this.startTime).append(NetworkProfiler.TAB).append(this.endTime).append(NetworkProfiler.TAB).append(this.requestSize).append(NetworkProfiler.TAB).append(this.responseSize).append(NetworkProfiler.TAB);
            if (this.extralInfo != null) {
                for (String extra : this.extralInfo) {
                    buf.append(extra).append(",");
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                buf.append(NetworkProfiler.TAB);
            }
            buf.append(TraceUtil.LINE);
            return buf.toString();
        }
    }

    public static final void endable(String outputPath2) {
        enable = true;
        outputPath = outputPath2;
    }

    public static final void disable() {
        enable = false;
        records.clear();
        recordMapper.clear();
    }

    public static void flush() {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
             arg types: [java.io.File, int]
             candidates:
              ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
              ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
            public void run() {
                try {
                    ArrayList<ProfileRecord> list = new ArrayList<>(NetworkProfiler.records);
                    NetworkProfiler.records.clear();
                    FileOutputStream fos = new FileOutputStream(new File(NetworkProfiler.outputPath), true);
                    NetworkProfiler.writeTo(list, fos);
                    fos.close();
                } catch (Throwable th) {
                }
            }
        }).start();
    }

    public static final void startUrl(String url) {
        if (enable) {
            recordMapper.put(url, new ProfileRecord(System.currentTimeMillis(), recordMapper.containsKey(url)));
        }
    }

    public static final void endUrl(String url, int requestSize, int responseSize, String... extralInfo) {
        if (enable) {
            ProfileRecord r = recordMapper.remove(url);
            if (r != null) {
                long unused = r.endTime = System.currentTimeMillis();
                int unused2 = r.requestSize = requestSize;
                int unused3 = r.responseSize = responseSize;
                String[] unused4 = r.extralInfo = extralInfo;
                String unused5 = r.url = url;
                records.add(r);
            }
            if (records.size() > 100) {
                flush();
            }
        }
    }

    public static void writeTo(OutputStream os, boolean clear) {
        writeTo(records, os);
        if (clear) {
            records.clear();
        }
    }

    /* access modifiers changed from: private */
    public static void writeTo(List<ProfileRecord> list, OutputStream os) {
        for (ProfileRecord r : list) {
            try {
                os.write(r.toString().getBytes());
            } catch (IOException e) {
            }
        }
    }
}
