package com.baixing.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.security.MessageDigest;

public class NetworkUtil {
    public static String getMD5(String targetStr) {
        return getMD5(targetStr.getBytes());
    }

    public static String getMD5(byte[] source) {
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            byte[] tmp = md.digest();
            char[] str = new char[32];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = tmp[i];
                int k2 = k + 1;
                str[k] = hexDigits[(byte0 >>> 4) & 15];
                k = k2 + 1;
                str[k2] = hexDigits[byte0 & 15];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isWifiConnection(Context context) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetInfo;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || (activeNetInfo = connectivityManager.getActiveNetworkInfo()) == null || activeNetInfo.getType() != 1) {
            return false;
        }
        return true;
    }

    public static boolean isNetworkActive(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            return true;
        }
        return false;
    }

    public static long getTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }
}
