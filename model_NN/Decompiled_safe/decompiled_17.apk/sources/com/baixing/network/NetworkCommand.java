package com.baixing.network;

import android.content.Context;
import android.util.Pair;
import com.baixing.network.api.PlainRespHandler;
import com.baixing.network.impl.GetRequest;
import com.baixing.network.impl.HttpNetworkConnector;
import com.baixing.network.impl.PostRequest;

public class NetworkCommand {
    private boolean isGet;
    private String requestUrl;

    protected NetworkCommand(String url, boolean isGet2) {
        this.requestUrl = url;
        this.isGet = isGet2;
    }

    public static String doGet(Context cxt, String url) {
        return new NetworkCommand(url, true).execute(cxt);
    }

    public static String doPost(Context cxt, String url) {
        return new NetworkCommand(url, true).execute(cxt);
    }

    private String execute(Context context) {
        HttpNetworkConnector connector = HttpNetworkConnector.connect();
        if (connector == null) {
            return null;
        }
        Pair<Boolean, String> result = (Pair) connector.sendHttpRequestSync(context, this.isGet ? new GetRequest(this.requestUrl, null, false) : new PostRequest(this.requestUrl, null, null), new PlainRespHandler());
        if (result == null) {
            return null;
        }
        return (String) result.second;
    }
}
