package com.baixing.imageCache;

public final class R {

    public static final class drawable {
        public static final int ic_launcher = 2130837597;
    }

    public static final class string {
        public static final int app_name = 2131230721;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }
}
