package com.baixing.imageCache;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;
import android.util.Pair;
import android.view.WindowManager;
import com.baixing.imageCache.Utils;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpException;

public class ImageCacheManager {
    private Context context;
    private DiskLruCache imageDiskLruCache = null;
    private LruCache<String, Pair<Integer, WeakReference<Bitmap>>> imageLruCache;
    /* access modifiers changed from: private */
    public List<WeakReference<Bitmap>> trashList = new ArrayList();

    public ImageCacheManager(Context ctx) {
        long diskCacheSize;
        this.context = ctx;
        int memClass = ((ActivityManager) this.context.getSystemService("activity")).getMemoryClass();
        File fileCacheDir = DiskLruCache.getDiskCacheDir(this.context, "");
        long capacity_halfFreeSpace = Utils.getUsableSpace(fileCacheDir) / 2;
        if (20971520 < capacity_halfFreeSpace) {
            diskCacheSize = 20971520;
        } else {
            diskCacheSize = capacity_halfFreeSpace;
        }
        this.imageDiskLruCache = DiskLruCache.openCache(this.context, fileCacheDir, diskCacheSize);
        this.imageLruCache = new LruCache<String, Pair<Integer, WeakReference<Bitmap>>>((1048576 * memClass) / 8) {
            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void entryRemoved(boolean x0, Object x1, Object x2, Object x3) {
                entryRemoved(x0, (String) x1, (Pair<Integer, WeakReference<Bitmap>>) ((Pair) x2), (Pair<Integer, WeakReference<Bitmap>>) ((Pair) x3));
            }

            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ int sizeOf(Object x0, Object x1) {
                return sizeOf((String) x0, (Pair<Integer, WeakReference<Bitmap>>) ((Pair) x1));
            }

            /* access modifiers changed from: protected */
            public int sizeOf(String key, Pair<Integer, WeakReference<Bitmap>> value) {
                if (value == null) {
                    return 0;
                }
                return ((Integer) value.first).intValue();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.util.LruCache.entryRemoved(boolean, java.lang.Object, java.lang.Object, java.lang.Object):void
             arg types: [boolean, java.lang.String, android.util.Pair<java.lang.Integer, java.lang.ref.WeakReference<android.graphics.Bitmap>>, android.util.Pair<java.lang.Integer, java.lang.ref.WeakReference<android.graphics.Bitmap>>]
             candidates:
              com.baixing.imageCache.ImageCacheManager.1.entryRemoved(boolean, java.lang.String, android.util.Pair<java.lang.Integer, java.lang.ref.WeakReference<android.graphics.Bitmap>>, android.util.Pair<java.lang.Integer, java.lang.ref.WeakReference<android.graphics.Bitmap>>):void
              android.support.v4.util.LruCache.entryRemoved(boolean, java.lang.Object, java.lang.Object, java.lang.Object):void */
            /* access modifiers changed from: protected */
            public void entryRemoved(boolean evicted, String key, Pair<Integer, WeakReference<Bitmap>> oldValue, Pair<Integer, WeakReference<Bitmap>> newValue) {
                super.entryRemoved(evicted, (Object) key, (Object) oldValue, (Object) newValue);
            }
        };
    }

    public void enableSampleSize(boolean b) {
        Utils.enableSampleSize(b);
    }

    public boolean contains(String url) {
        boolean z;
        synchronized (this) {
            z = this.imageLruCache.get(url) != null;
        }
        return z;
    }

    public Bitmap loadBitmapFromResource(int resId) {
        WeakReference<Bitmap> cached = getFromCache(String.valueOf(resId));
        if (cached != null && cached.get() != null) {
            return cached.get();
        }
        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inPurgeable = true;
        option.inInputShareable = true;
        option.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap ret = BitmapFactory.decodeResource(this.context.getResources(), resId, option);
        if (ret != null) {
            saveBitmapToCache(String.valueOf(resId), new WeakReference(ret));
        }
        return ret;
    }

    public WeakReference<Bitmap> getFromCache(String url) {
        WeakReference<Bitmap> bitmap = getFromMapCache(url);
        if (!((bitmap != null && bitmap.get() != null) || (bitmap = getFromFileCache(url)) == null || bitmap.get() == null)) {
            saveBitmapToCache(url, bitmap);
        }
        return bitmap;
    }

    private WeakReference<Bitmap> getFromFileCache(String url) {
        if (this.imageDiskLruCache != null) {
            return new WeakReference<>(this.imageDiskLruCache.get(Utils.getMD5(url)));
        }
        return null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.ref.WeakReference<android.graphics.Bitmap>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.ref.WeakReference<android.graphics.Bitmap> getFromMapCache(java.lang.String r5) {
        /*
            r4 = this;
            r1 = 0
            monitor-enter(r4)
            android.support.v4.util.LruCache<java.lang.String, android.util.Pair<java.lang.Integer, java.lang.ref.WeakReference<android.graphics.Bitmap>>> r3 = r4.imageLruCache     // Catch:{ all -> 0x0014 }
            java.lang.Object r2 = r3.get(r5)     // Catch:{ all -> 0x0014 }
            android.util.Pair r2 = (android.util.Pair) r2     // Catch:{ all -> 0x0014 }
            if (r2 == 0) goto L_0x0012
            java.lang.Object r3 = r2.second     // Catch:{ all -> 0x0014 }
            r0 = r3
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0     // Catch:{ all -> 0x0014 }
            r1 = r0
        L_0x0012:
            monitor-exit(r4)     // Catch:{ all -> 0x0014 }
            return r1
        L_0x0014:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0014 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.ImageCacheManager.getFromMapCache(java.lang.String):java.lang.ref.WeakReference");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.ref.WeakReference} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void forceRecycle(java.lang.String r7, boolean r8) {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x000a
            java.lang.String r3 = ""
            boolean r3 = r7.equals(r3)
            if (r3 == 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            r1 = 0
            monitor-enter(r6)
            android.support.v4.util.LruCache<java.lang.String, android.util.Pair<java.lang.Integer, java.lang.ref.WeakReference<android.graphics.Bitmap>>> r3 = r6.imageLruCache     // Catch:{ all -> 0x0055 }
            java.lang.Object r2 = r3.remove(r7)     // Catch:{ all -> 0x0055 }
            android.util.Pair r2 = (android.util.Pair) r2     // Catch:{ all -> 0x0055 }
            if (r2 == 0) goto L_0x001d
            java.lang.Object r3 = r2.second     // Catch:{ all -> 0x0055 }
            r0 = r3
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0     // Catch:{ all -> 0x0055 }
            r1 = r0
        L_0x001d:
            monitor-exit(r6)     // Catch:{ all -> 0x0055 }
            if (r1 == 0) goto L_0x000a
            java.lang.String r3 = "recycle"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "hahaha remove unuesd bitmap~~~~~~~~~~~~~~~    "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r5 = ", recycle right now ? "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r8)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
            if (r8 == 0) goto L_0x0058
            java.lang.Object r3 = r1.get()
            if (r3 == 0) goto L_0x0053
            java.lang.Object r3 = r1.get()
            android.graphics.Bitmap r3 = (android.graphics.Bitmap) r3
            r3.recycle()
        L_0x0053:
            r1 = 0
            goto L_0x000a
        L_0x0055:
            r3 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0055 }
            throw r3
        L_0x0058:
            java.util.List<java.lang.ref.WeakReference<android.graphics.Bitmap>> r4 = r6.trashList
            monitor-enter(r4)
            java.util.List<java.lang.ref.WeakReference<android.graphics.Bitmap>> r3 = r6.trashList     // Catch:{ all -> 0x0062 }
            r3.add(r1)     // Catch:{ all -> 0x0062 }
            monitor-exit(r4)     // Catch:{ all -> 0x0062 }
            goto L_0x0053
        L_0x0062:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0062 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.ImageCacheManager.forceRecycle(java.lang.String, boolean):void");
    }

    public void postRecycle() {
        new Thread(new Runnable() {
            /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
                java.lang.Thread.sleep(2000);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:21:0x0066, code lost:
                r1 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:22:0x0067, code lost:
                r1.printStackTrace();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
                if (r4.size() <= 0) goto L_0x002b;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r8 = this;
                    r3 = 0
                    com.baixing.imageCache.ImageCacheManager r5 = com.baixing.imageCache.ImageCacheManager.this
                    java.util.List r6 = r5.trashList
                    monitor-enter(r6)
                    java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x0063 }
                    r4.<init>()     // Catch:{ all -> 0x0063 }
                    com.baixing.imageCache.ImageCacheManager r5 = com.baixing.imageCache.ImageCacheManager.this     // Catch:{ all -> 0x006f }
                    java.util.List r5 = r5.trashList     // Catch:{ all -> 0x006f }
                    r4.addAll(r5)     // Catch:{ all -> 0x006f }
                    com.baixing.imageCache.ImageCacheManager r5 = com.baixing.imageCache.ImageCacheManager.this     // Catch:{ all -> 0x006f }
                    java.util.List r5 = r5.trashList     // Catch:{ all -> 0x006f }
                    r5.clear()     // Catch:{ all -> 0x006f }
                    monitor-exit(r6)     // Catch:{ all -> 0x006f }
                    int r5 = r4.size()     // Catch:{ InterruptedException -> 0x0066 }
                    if (r5 <= 0) goto L_0x002b
                    r5 = 2000(0x7d0, double:9.88E-321)
                    java.lang.Thread.sleep(r5)     // Catch:{ InterruptedException -> 0x0066 }
                L_0x002b:
                    java.util.Iterator r2 = r4.iterator()
                L_0x002f:
                    boolean r5 = r2.hasNext()
                    if (r5 == 0) goto L_0x006b
                    java.lang.Object r0 = r2.next()
                    java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0
                    java.lang.String r5 = "recycle"
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder
                    r6.<init>()
                    java.lang.String r7 = "exe delay recycle bitmap "
                    java.lang.StringBuilder r6 = r6.append(r7)
                    java.lang.StringBuilder r6 = r6.append(r0)
                    java.lang.String r6 = r6.toString()
                    android.util.Log.d(r5, r6)
                    java.lang.Object r5 = r0.get()
                    if (r5 == 0) goto L_0x002f
                    java.lang.Object r5 = r0.get()
                    android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5
                    r5.recycle()
                    goto L_0x002f
                L_0x0063:
                    r5 = move-exception
                L_0x0064:
                    monitor-exit(r6)     // Catch:{ all -> 0x0063 }
                    throw r5
                L_0x0066:
                    r1 = move-exception
                    r1.printStackTrace()
                    goto L_0x002b
                L_0x006b:
                    java.lang.System.gc()
                    return
                L_0x006f:
                    r5 = move-exception
                    r3 = r4
                    goto L_0x0064
                */
                throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.ImageCacheManager.AnonymousClass2.run():void");
            }
        }).start();
    }

    public void forceRecycle() {
        synchronized (this) {
            this.imageLruCache.evictAll();
        }
    }

    public void saveBitmapToCache(String url, WeakReference<Bitmap> bitmap) {
        if (bitmap != null) {
            try {
                if (bitmap.get() != null) {
                    synchronized (this) {
                        this.imageLruCache.put(url, new Pair(Integer.valueOf(bitmap.get().getHeight() * bitmap.get().getRowBytes()), bitmap));
                    }
                }
            } catch (Throwable th) {
            }
        }
    }

    public WeakReference<Bitmap> safeGetFromNetwork(String url) throws HttpException {
        WeakReference<Bitmap> bitmap = new WeakReference<>(downloadImg(url));
        saveBitmapToCache(url, bitmap);
        return bitmap;
    }

    public void putImageToDisk(String url, Bitmap bmp) {
        this.imageDiskLruCache.put(Utils.getMD5(url), bmp);
    }

    private static Bitmap decodeSampledBitmapFromFile(Context ctx, String file) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (Utils.useSampleSize()) {
            Utils._Rect rc = new Utils._Rect();
            rc.width = 200;
            rc.height = 200;
            WindowManager wm = (WindowManager) ctx.getSystemService("window");
            rc.width = wm.getDefaultDisplay().getWidth() / 2;
            rc.height = wm.getDefaultDisplay().getHeight() / 2;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file, options);
            options.inSampleSize = Utils.calculateInSampleSize(ctx, options, rc.width, rc.height);
        } else {
            options.inSampleSize = 1;
        }
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        return BitmapFactory.decodeFile(file, options);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003b, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap downloadImg(java.lang.String r8) throws org.apache.http.HttpException {
        /*
            r7 = this;
            r0 = 0
            java.lang.String r2 = com.baixing.imageCache.Utils.getMD5(r8)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            com.baixing.imageCache.DiskLruCache r5 = r7.imageDiskLruCache     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            java.lang.String r4 = r5.createFilePath(r2)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            com.baixing.network.api.FileDownloadCommand r1 = new com.baixing.network.api.FileDownloadCommand     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            r1.<init>(r8)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            android.content.Context r5 = r7.context     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            r6.<init>(r4)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            boolean r3 = r1.doDownload(r5, r6)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            if (r3 == 0) goto L_0x002b
            r5 = 1
            r7.enableSampleSize(r5)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            android.content.Context r5 = r7.context     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            android.graphics.Bitmap r0 = decodeSampledBitmapFromFile(r5, r4)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            r5 = 0
            r7.enableSampleSize(r5)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
        L_0x002b:
            com.baixing.imageCache.DiskLruCache r5 = r7.imageDiskLruCache     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            if (r5 == 0) goto L_0x0036
            if (r0 == 0) goto L_0x0036
            com.baixing.imageCache.DiskLruCache r5 = r7.imageDiskLruCache     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
            r5.put(r2, r0, r4)     // Catch:{ Exception -> 0x003a, all -> 0x0038 }
        L_0x0036:
            r5 = r0
        L_0x0037:
            return r5
        L_0x0038:
            r5 = move-exception
            throw r5
        L_0x003a:
            r5 = move-exception
            r5 = 0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.ImageCacheManager.downloadImg(java.lang.String):android.graphics.Bitmap");
    }

    public void clearall() {
        String[] str = this.context.fileList();
        for (String deleteFile : str) {
            this.context.deleteFile(deleteFile);
        }
    }

    public String getFileInDiskCache(String url) {
        if (this.imageDiskLruCache != null) {
            return this.imageDiskLruCache.getFilePath(Utils.getMD5(url));
        }
        return "";
    }
}
