package com.baixing.imageCache;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.io.File;
import java.security.MessageDigest;

class Utils {
    private static boolean useSampleSize = false;

    Utils() {
    }

    public static class _Rect {
        public int height;
        public int width;

        public _Rect() {
            this.width = 0;
            this.height = 0;
            this.width = 0;
            this.height = 0;
        }
    }

    @SuppressLint({"NewApi"})
    public static long getUsableSpace(File path) {
        try {
            StatFs stats = new StatFs(path.getPath());
            return ((long) stats.getBlockSize()) * ((long) stats.getAvailableBlocks());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static boolean useSampleSize() {
        return useSampleSize;
    }

    public static void enableSampleSize(boolean b) {
        useSampleSize = b;
    }

    public static int calculateInSampleSize(Context ctx, BitmapFactory.Options options, int reqWidth, int reqHeight) {
        if (reqWidth <= 0 && reqHeight <= 0 && ctx != null) {
            DisplayMetrics metrics = new DisplayMetrics();
            ((WindowManager) ctx.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
            reqWidth = metrics.widthPixels;
            reqHeight = metrics.heightPixels;
        }
        int height = options.outHeight;
        int width = options.outWidth;
        if (height <= reqHeight && width <= reqWidth) {
            return 1;
        }
        if (width > height) {
            return Math.round(((float) height) / ((float) reqHeight));
        }
        return Math.round(((float) width) / ((float) reqWidth));
    }

    @SuppressLint({"NewApi"})
    public static boolean isExternalStorageRemovable() {
        return true;
    }

    @SuppressLint({"NewApi"})
    public static File getExternalCacheDir(Context context) {
        return new File(Environment.getExternalStorageDirectory().getPath() + ("/Android/data/" + context.getPackageName() + "/cache/"));
    }

    public static boolean isPathValidForDiskCache(File path) {
        return path.exists() && path.canWrite();
    }

    public static String getMD5(String strMD5) {
        byte[] source = strMD5.getBytes();
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            byte[] tmp = md.digest();
            char[] str = new char[32];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = tmp[i];
                int k2 = k + 1;
                str[k] = hexDigits[(byte0 >>> 4) & 15];
                k = k2 + 1;
                str[k2] = hexDigits[byte0 & 15];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
