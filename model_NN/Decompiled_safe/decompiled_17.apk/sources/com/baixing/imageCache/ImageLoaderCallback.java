package com.baixing.imageCache;

import android.graphics.Bitmap;

interface ImageLoaderCallback {
    void fail(String str);

    Object getObject();

    void refresh(String str, Bitmap bitmap);
}
