package com.baixing.imageCache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.WindowManager;
import com.baixing.imageCache.Utils;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

class DiskLruCache {
    private static final String CACHE_FILENAME_PREFIX = "__cache_";
    private static final long DUMP_TO_FILE_ELAPSE_MS = 60000;
    private static final int INITIAL_CAPACITY = 256;
    static final int IO_BUFFER_SIZE = 8192;
    private static final float LOAD_FACTOR = 0.75f;
    private static final int MAX_REMOVALS = 256;
    private static final String TAG = "DiskLruCache";
    private static final FilenameFilter cacheFileFilter = new FilenameFilter() {
        public boolean accept(File dir, String filename) {
            return filename.startsWith(DiskLruCache.CACHE_FILENAME_PREFIX);
        }
    };
    private int cacheByteSize = 0;
    private int cacheSize = 0;
    private final File mCacheDir;
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private int mCompressQuality = 70;
    private Context mCtx;
    private Map<String, String> mLinkedHashMap = null;
    private boolean mNeedDumpToFile = false;
    private long maxCacheByteSize = 5242880;
    private final int maxCacheItemSize = 16777215;
    private Timer timer = new Timer();

    public static DiskLruCache openCache(Context context, File cacheDir, long maxFreeByteSize) {
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        if (!cacheDir.isDirectory() || !cacheDir.canWrite() || Utils.getUsableSpace(cacheDir) <= maxFreeByteSize) {
            return null;
        }
        DiskLruCache instance = new DiskLruCache(cacheDir, maxFreeByteSize);
        instance.mCtx = context;
        return instance;
    }

    private class TimedIndexDumping extends TimerTask {
        private boolean mCanceled;

        private TimedIndexDumping() {
            this.mCanceled = false;
        }

        public boolean cancel() {
            super.cancel();
            this.mCanceled = true;
            return true;
        }

        public void run() {
            if (!this.mCanceled) {
                DiskLruCache.this.dumpCacheIndexToFile();
            }
        }
    }

    private DiskLruCache(File cacheDir, long maxFreeByteSize) {
        this.mCacheDir = cacheDir;
        this.maxCacheByteSize = maxFreeByteSize;
        rebuildFromExistingFiles(this.mCacheDir);
        this.timer.scheduleAtFixedRate(new TimedIndexDumping(), 60000, 60000);
    }

    public void put(String key, Bitmap data) {
        synchronized (this.mLinkedHashMap) {
            try {
                String file = createFilePath(this.mCacheDir, key);
                if (writeBitmapToFile(data, file)) {
                    Log.d("bitmap", "bitmap, diskLruCache:put:  " + key);
                    put(key, file);
                    flushCache();
                }
            } catch (FileNotFoundException e) {
                Log.e(TAG, "Error in put: " + e.getMessage());
                Log.d("bitmap", "bitmap, diskLruCache:put:  error" + key + e.getMessage());
            } catch (IOException e2) {
                Log.e(TAG, "Error in put: " + e2.getMessage());
                Log.d("bitmap", "bitmap, diskLruCache:put:  error io" + key + e2.getMessage());
            }
        }
        return;
    }

    public void put(String key, Bitmap data, String file) {
        synchronized (this.mLinkedHashMap) {
            Log.d("bitmap", "bitmap, diskLruCache:put:  " + key);
            put(key, file);
            flushCache();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0032 A[SYNTHETIC, Splitter:B:16:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0037 A[Catch:{ IOException -> 0x005c, all -> 0x0061 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0067 A[SYNTHETIC, Splitter:B:43:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x006c A[Catch:{ IOException -> 0x0073 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:47:0x0072=Splitter:B:47:0x0072, B:20:0x003d=Splitter:B:20:0x003d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void put(java.lang.String r12, java.io.InputStream r13) {
        /*
            r11 = this;
            java.util.Map<java.lang.String, java.lang.String> r9 = r11.mLinkedHashMap
            monitor-enter(r9)
            r0 = 0
            r2 = 0
            java.io.File r8 = r11.mCacheDir     // Catch:{ Exception -> 0x0083, all -> 0x0064 }
            java.lang.String r6 = createFilePath(r8, r12)     // Catch:{ Exception -> 0x0083, all -> 0x0064 }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0083, all -> 0x0064 }
            r1.<init>(r13)     // Catch:{ Exception -> 0x0083, all -> 0x0064 }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0085, all -> 0x007c }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0085, all -> 0x007c }
            r8.<init>(r6)     // Catch:{ Exception -> 0x0085, all -> 0x007c }
            r10 = 8192(0x2000, float:1.14794E-41)
            r3.<init>(r8, r10)     // Catch:{ Exception -> 0x0085, all -> 0x007c }
            r8 = 8192(0x2000, float:1.14794E-41)
            byte[] r4 = new byte[r8]     // Catch:{ Exception -> 0x002d, all -> 0x007f }
            r7 = 0
        L_0x0021:
            int r7 = r1.read(r4)     // Catch:{ Exception -> 0x002d, all -> 0x007f }
            r8 = -1
            if (r7 == r8) goto L_0x003f
            r8 = 0
            r3.write(r4, r8, r7)     // Catch:{ Exception -> 0x002d, all -> 0x007f }
            goto L_0x0021
        L_0x002d:
            r8 = move-exception
            r2 = r3
            r0 = r1
        L_0x0030:
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ IOException -> 0x005c }
        L_0x0035:
            if (r2 == 0) goto L_0x003d
            r2.flush()     // Catch:{ IOException -> 0x005c }
            r2.close()     // Catch:{ IOException -> 0x005c }
        L_0x003d:
            monitor-exit(r9)     // Catch:{ all -> 0x0061 }
            return
        L_0x003f:
            r11.put(r12, r6)     // Catch:{ Exception -> 0x002d, all -> 0x007f }
            r11.flushCache()     // Catch:{ Exception -> 0x002d, all -> 0x007f }
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x0055 }
        L_0x004a:
            if (r3 == 0) goto L_0x0052
            r3.flush()     // Catch:{ IOException -> 0x0055 }
            r3.close()     // Catch:{ IOException -> 0x0055 }
        L_0x0052:
            r2 = r3
            r0 = r1
            goto L_0x003d
        L_0x0055:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x0078 }
            r2 = r3
            r0 = r1
            goto L_0x003d
        L_0x005c:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x0061 }
            goto L_0x003d
        L_0x0061:
            r8 = move-exception
        L_0x0062:
            monitor-exit(r9)     // Catch:{ all -> 0x0061 }
            throw r8
        L_0x0064:
            r8 = move-exception
        L_0x0065:
            if (r0 == 0) goto L_0x006a
            r0.close()     // Catch:{ IOException -> 0x0073 }
        L_0x006a:
            if (r2 == 0) goto L_0x0072
            r2.flush()     // Catch:{ IOException -> 0x0073 }
            r2.close()     // Catch:{ IOException -> 0x0073 }
        L_0x0072:
            throw r8     // Catch:{ all -> 0x0061 }
        L_0x0073:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x0061 }
            goto L_0x0072
        L_0x0078:
            r8 = move-exception
            r2 = r3
            r0 = r1
            goto L_0x0062
        L_0x007c:
            r8 = move-exception
            r0 = r1
            goto L_0x0065
        L_0x007f:
            r8 = move-exception
            r2 = r3
            r0 = r1
            goto L_0x0065
        L_0x0083:
            r8 = move-exception
            goto L_0x0030
        L_0x0085:
            r8 = move-exception
            r0 = r1
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.DiskLruCache.put(java.lang.String, java.io.InputStream):void");
    }

    private void put(String key, String file) {
        this.mLinkedHashMap.put(key, file);
        this.cacheSize = this.mLinkedHashMap.size();
        this.cacheByteSize = (int) (((long) this.cacheByteSize) + new File(file).length());
        this.mNeedDumpToFile = true;
    }

    private void flushCache() {
        int count = 0;
        Set<Map.Entry<String, String>> entrySet = this.mLinkedHashMap.entrySet();
        int size = this.mLinkedHashMap.size();
        while (count < 256) {
            if ((size > 100 || this.cacheSize > 16777215 || ((long) this.cacheByteSize) > this.maxCacheByteSize) && entrySet.iterator().hasNext()) {
                Map.Entry<String, String> eldestEntry = entrySet.iterator().next();
                File eldestFile = new File((String) eldestEntry.getValue());
                long eldestFileSize = eldestFile.length();
                this.mLinkedHashMap.remove(eldestEntry.getKey());
                eldestFile.delete();
                this.cacheSize = this.mLinkedHashMap.size();
                this.cacheByteSize = (int) (((long) this.cacheByteSize) - eldestFileSize);
                count++;
                size = this.mLinkedHashMap.size();
            } else {
                return;
            }
        }
    }

    private static Bitmap decodeSampledBitmapFromFile(Context ctx, String fileName) {
        if (fileName == null || !new File(fileName).exists()) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (Utils.useSampleSize()) {
            Utils._Rect rc = new Utils._Rect();
            rc.width = 200;
            rc.height = 200;
            WindowManager wm = (WindowManager) ctx.getSystemService("window");
            rc.width = wm.getDefaultDisplay().getWidth() / 2;
            rc.height = wm.getDefaultDisplay().getHeight() / 2;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fileName, options);
            options.inSampleSize = Utils.calculateInSampleSize(ctx, options, rc.width, rc.height);
        } else {
            options.inSampleSize = 1;
        }
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        return BitmapFactory.decodeFile(fileName, options);
    }

    public Bitmap decodeBitmapFromFile(Context ctx, String fileName) {
        try {
            return decodeSampledBitmapFromFile(ctx, fileName);
        } catch (Throwable th) {
            System.gc();
            return null;
        }
    }

    public String getFilePath(String key) {
        return this.mLinkedHashMap.get(key);
    }

    public Bitmap get(String key) {
        Bitmap bitmap;
        synchronized (this.mLinkedHashMap) {
            String file = this.mLinkedHashMap.get(key);
            if (file != null) {
                this.mNeedDumpToFile = true;
                bitmap = decodeBitmapFromFile(this.mCtx, file);
            } else {
                String existingFile = createFilePath(this.mCacheDir, key);
                if (new File(existingFile).exists()) {
                    put(key, existingFile);
                    bitmap = decodeBitmapFromFile(this.mCtx, file);
                } else {
                    bitmap = null;
                }
            }
        }
        return bitmap;
    }

    public boolean containsKey(String key) {
        if (this.mLinkedHashMap.containsKey(key)) {
            return true;
        }
        String existingFile = createFilePath(this.mCacheDir, key);
        if (!new File(existingFile).exists()) {
            return false;
        }
        put(key, existingFile);
        return true;
    }

    public void clearCache() {
        clearCache(this.mCacheDir);
    }

    public static void clearCache(Context context, String uniqueName) {
        clearCache(getDiskCacheDir(context, uniqueName));
    }

    private static void clearCache(File cacheDir) {
        File[] files = cacheDir.listFiles(cacheFileFilter);
        for (File delete : files) {
            delete.delete();
        }
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        File diskCacheDir = new File(((Environment.getExternalStorageState().equals("mounted") || !Utils.isExternalStorageRemovable()) ? Utils.getExternalCacheDir(context).getPath() : context.getCacheDir().getPath()) + File.separator + uniqueName);
        if (!diskCacheDir.exists()) {
            diskCacheDir.mkdirs();
        }
        if (!Utils.isPathValidForDiskCache(diskCacheDir) || Utils.getUsableSpace(diskCacheDir) < 0) {
            return new File(context.getCacheDir().getPath() + File.separator + uniqueName);
        }
        return diskCacheDir;
    }

    public static String createFilePath(File cacheDir, String key) {
        try {
            return new File(cacheDir.getAbsolutePath() + File.separator + CACHE_FILENAME_PREFIX + URLEncoder.encode(key.replace("*", ""), "UTF-8")).getAbsolutePath();
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "createFilePath - " + e);
            return null;
        }
    }

    public String createFilePath(String key) {
        return createFilePath(this.mCacheDir, key);
    }

    public void setCompressParams(Bitmap.CompressFormat compressFormat, int quality) {
        this.mCompressFormat = compressFormat;
        this.mCompressQuality = quality;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035 A[SYNTHETIC, Splitter:B:18:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0044 A[SYNTHETIC, Splitter:B:26:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0050 A[SYNTHETIC, Splitter:B:32:0x0050] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x0030=Splitter:B:15:0x0030, B:23:0x003f=Splitter:B:23:0x003f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dumpCacheIndexToFile() {
        /*
            r5 = this;
            boolean r4 = r5.mNeedDumpToFile
            if (r4 == 0) goto L_0x0028
            java.io.File r1 = new java.io.File
            java.lang.String r4 = "__mapIndex_dump_file__.bin"
            java.lang.String r4 = r5.createFilePath(r4)
            r1.<init>(r4)
            r2 = 0
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ FileNotFoundException -> 0x002f, IOException -> 0x003e }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002f, IOException -> 0x003e }
            r4.<init>(r1)     // Catch:{ FileNotFoundException -> 0x002f, IOException -> 0x003e }
            r3.<init>(r4)     // Catch:{ FileNotFoundException -> 0x002f, IOException -> 0x003e }
            java.util.Map<java.lang.String, java.lang.String> r4 = r5.mLinkedHashMap     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            r3.writeObject(r4)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            if (r3 == 0) goto L_0x0062
            r3.close()     // Catch:{ IOException -> 0x0029 }
            r2 = r3
        L_0x0025:
            r4 = 0
            r5.mNeedDumpToFile = r4
        L_0x0028:
            return
        L_0x0029:
            r0 = move-exception
            r0.printStackTrace()
            r2 = r3
            goto L_0x0025
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            r0.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ IOException -> 0x0039 }
            goto L_0x0025
        L_0x0039:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0025
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            r0.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0025
        L_0x0048:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0025
        L_0x004d:
            r4 = move-exception
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r4
        L_0x0054:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x0059:
            r4 = move-exception
            r2 = r3
            goto L_0x004e
        L_0x005c:
            r0 = move-exception
            r2 = r3
            goto L_0x003f
        L_0x005f:
            r0 = move-exception
            r2 = r3
            goto L_0x0030
        L_0x0062:
            r2 = r3
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.DiskLruCache.dumpCacheIndexToFile():void");
    }

    private boolean writeBitmapToFile(Bitmap bitmap, String file) throws IOException, FileNotFoundException {
        OutputStream out = null;
        try {
            OutputStream out2 = new BufferedOutputStream(new FileOutputStream(file), 8192);
            try {
                boolean compress = bitmap.compress(this.mCompressFormat, this.mCompressQuality, out2);
                if (out2 != null) {
                    out2.close();
                }
                return compress;
            } catch (Throwable th) {
                th = th;
                out = out2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (out != null) {
                out.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x007d A[SYNTHETIC, Splitter:B:22:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00df A[SYNTHETIC, Splitter:B:46:0x00df] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0106 A[SYNTHETIC, Splitter:B:57:0x0106] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x012b A[SYNTHETIC, Splitter:B:66:0x012b] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0134  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:54:0x0101=Splitter:B:54:0x0101, B:19:0x0078=Splitter:B:19:0x0078, B:43:0x00da=Splitter:B:43:0x00da} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean rebuildFromExistingFiles(java.io.File r18) {
        /*
            r17 = this;
            java.io.File r9 = new java.io.File
            java.lang.String r12 = "__mapIndex_dump_file__.bin"
            r0 = r17
            java.lang.String r12 = r0.createFilePath(r12)
            r9.<init>(r12)
            r10 = 0
            java.io.ObjectInputStream r11 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x0156, IOException -> 0x00d9, ClassNotFoundException -> 0x0100 }
            java.io.FileInputStream r12 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0156, IOException -> 0x00d9, ClassNotFoundException -> 0x0100 }
            r12.<init>(r9)     // Catch:{ FileNotFoundException -> 0x0156, IOException -> 0x00d9, ClassNotFoundException -> 0x0100 }
            r11.<init>(r12)     // Catch:{ FileNotFoundException -> 0x0156, IOException -> 0x00d9, ClassNotFoundException -> 0x0100 }
            java.lang.Object r12 = r11.readObject()     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            java.util.Map r12 = (java.util.Map) r12     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r0 = r17
            r0.mLinkedHashMap = r12     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            if (r12 == 0) goto L_0x00b0
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            java.util.Set r12 = r12.keySet()     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            java.lang.Object[] r7 = r12.toArray()     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            if (r7 == 0) goto L_0x009d
            r1 = r7
            int r8 = r1.length     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r5 = 0
        L_0x0039:
            if (r5 >= r8) goto L_0x009d
            r6 = r1[r5]     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            java.lang.String r13 = r6.toString()     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            java.lang.Object r4 = r12.get(r13)     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            if (r4 == 0) goto L_0x0067
            java.io.File r3 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r3.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            boolean r12 = r3.exists()     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            if (r12 == 0) goto L_0x006a
            r0 = r17
            int r12 = r0.cacheByteSize     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            long r12 = (long) r12     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            long r14 = r3.length()     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            long r12 = r12 + r14
            int r12 = (int) r12     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r0 = r17
            r0.cacheByteSize = r12     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
        L_0x0067:
            int r5 = r5 + 1
            goto L_0x0039
        L_0x006a:
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            java.lang.String r13 = r6.toString()     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r12.remove(r13)     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            goto L_0x0067
        L_0x0076:
            r2 = move-exception
            r10 = r11
        L_0x0078:
            r2.printStackTrace()     // Catch:{ all -> 0x0128 }
            if (r10 == 0) goto L_0x0080
            r10.close()     // Catch:{ IOException -> 0x00d4 }
        L_0x0080:
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap
            if (r12 != 0) goto L_0x0098
            java.util.LinkedHashMap r12 = new java.util.LinkedHashMap
            r13 = 256(0x100, float:3.59E-43)
            r14 = 1061158912(0x3f400000, float:0.75)
            r15 = 1
            r12.<init>(r13, r14, r15)
            java.util.Map r12 = java.util.Collections.synchronizedMap(r12)
            r0 = r17
            r0.mLinkedHashMap = r12
        L_0x0098:
            r17.flushCache()
            r12 = 1
            return r12
        L_0x009d:
            r0 = r17
            long r12 = r0.maxCacheByteSize     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r0 = r17
            int r14 = r0.cacheByteSize     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            long r14 = (long) r14     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            long r12 = r12 + r14
            r0 = r17
            r0.maxCacheByteSize = r12     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
            r12 = 1
            r0 = r17
            r0.mNeedDumpToFile = r12     // Catch:{ FileNotFoundException -> 0x0076, IOException -> 0x0153, ClassNotFoundException -> 0x0150, all -> 0x014d }
        L_0x00b0:
            if (r11 == 0) goto L_0x00b5
            r11.close()     // Catch:{ IOException -> 0x00cf }
        L_0x00b5:
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap
            if (r12 != 0) goto L_0x0159
            java.util.LinkedHashMap r12 = new java.util.LinkedHashMap
            r13 = 256(0x100, float:3.59E-43)
            r14 = 1061158912(0x3f400000, float:0.75)
            r15 = 1
            r12.<init>(r13, r14, r15)
            java.util.Map r12 = java.util.Collections.synchronizedMap(r12)
            r0 = r17
            r0.mLinkedHashMap = r12
            r10 = r11
            goto L_0x0098
        L_0x00cf:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00b5
        L_0x00d4:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0080
        L_0x00d9:
            r2 = move-exception
        L_0x00da:
            r2.printStackTrace()     // Catch:{ all -> 0x0128 }
            if (r10 == 0) goto L_0x00e2
            r10.close()     // Catch:{ IOException -> 0x00fb }
        L_0x00e2:
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap
            if (r12 != 0) goto L_0x0098
            java.util.LinkedHashMap r12 = new java.util.LinkedHashMap
            r13 = 256(0x100, float:3.59E-43)
            r14 = 1061158912(0x3f400000, float:0.75)
            r15 = 1
            r12.<init>(r13, r14, r15)
            java.util.Map r12 = java.util.Collections.synchronizedMap(r12)
            r0 = r17
            r0.mLinkedHashMap = r12
            goto L_0x0098
        L_0x00fb:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00e2
        L_0x0100:
            r2 = move-exception
        L_0x0101:
            r2.printStackTrace()     // Catch:{ all -> 0x0128 }
            if (r10 == 0) goto L_0x0109
            r10.close()     // Catch:{ IOException -> 0x0123 }
        L_0x0109:
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r12 = r0.mLinkedHashMap
            if (r12 != 0) goto L_0x0098
            java.util.LinkedHashMap r12 = new java.util.LinkedHashMap
            r13 = 256(0x100, float:3.59E-43)
            r14 = 1061158912(0x3f400000, float:0.75)
            r15 = 1
            r12.<init>(r13, r14, r15)
            java.util.Map r12 = java.util.Collections.synchronizedMap(r12)
            r0 = r17
            r0.mLinkedHashMap = r12
            goto L_0x0098
        L_0x0123:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0109
        L_0x0128:
            r12 = move-exception
        L_0x0129:
            if (r10 == 0) goto L_0x012e
            r10.close()     // Catch:{ IOException -> 0x0148 }
        L_0x012e:
            r0 = r17
            java.util.Map<java.lang.String, java.lang.String> r13 = r0.mLinkedHashMap
            if (r13 != 0) goto L_0x0147
            java.util.LinkedHashMap r13 = new java.util.LinkedHashMap
            r14 = 256(0x100, float:3.59E-43)
            r15 = 1061158912(0x3f400000, float:0.75)
            r16 = 1
            r13.<init>(r14, r15, r16)
            java.util.Map r13 = java.util.Collections.synchronizedMap(r13)
            r0 = r17
            r0.mLinkedHashMap = r13
        L_0x0147:
            throw r12
        L_0x0148:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x012e
        L_0x014d:
            r12 = move-exception
            r10 = r11
            goto L_0x0129
        L_0x0150:
            r2 = move-exception
            r10 = r11
            goto L_0x0101
        L_0x0153:
            r2 = move-exception
            r10 = r11
            goto L_0x00da
        L_0x0156:
            r2 = move-exception
            goto L_0x0078
        L_0x0159:
            r10 = r11
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.DiskLruCache.rebuildFromExistingFiles(java.io.File):boolean");
    }

    public void finalize() {
        this.timer.cancel();
        dumpCacheIndexToFile();
        try {
            super.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
