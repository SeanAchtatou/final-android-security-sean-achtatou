package com.baixing.imageCache;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

class CallbackManager {
    private ConcurrentHashMap<String, List<ImageLoaderCallback>> callbackMap = new ConcurrentHashMap<>();

    public void remove(String url) {
        this.callbackMap.remove(url);
    }

    public boolean remove(String url, Object object) {
        List<ImageLoaderCallback> callbackList = this.callbackMap.get(url);
        if (callbackList == null) {
            return true;
        }
        Iterator i$ = callbackList.iterator();
        while (true) {
            if (i$.hasNext()) {
                ImageLoaderCallback callback = (ImageLoaderCallback) i$.next();
                if (callback != null && callback.getObject() == object) {
                    callbackList.remove(callback);
                    break;
                }
            } else {
                break;
            }
        }
        if (callbackList.size() != 0) {
            return false;
        }
        this.callbackMap.remove(callbackList);
        return true;
    }

    public void put(String url, ImageLoaderCallback callback) {
        if (this.callbackMap.get(url) == null) {
            this.callbackMap.put(url, new ArrayList());
        }
        this.callbackMap.get(url).add(callback);
    }

    public void callback(String url, Bitmap bitmap) {
        List<ImageLoaderCallback> callbacks = this.callbackMap.get(url);
        if (callbacks != null) {
            for (ImageLoaderCallback callback : callbacks) {
                if (callback != null) {
                    callback.refresh(url, bitmap);
                }
            }
            callbacks.clear();
            this.callbackMap.remove(url);
        }
    }

    public void fail(String url) {
        List<ImageLoaderCallback> callbacks = this.callbackMap.get(url);
        if (callbacks != null) {
            for (ImageLoaderCallback callback : callbacks) {
                if (callback != null) {
                    callback.fail(url);
                }
            }
        }
    }
}
