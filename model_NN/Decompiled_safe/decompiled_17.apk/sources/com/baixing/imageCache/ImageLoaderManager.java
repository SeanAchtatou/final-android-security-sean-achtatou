package com.baixing.imageCache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import java.lang.Thread;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class ImageLoaderManager {
    public static final String EXTRA_IMG = "extra_img";
    public static final String EXTRA_IMG_URL = "extra_img_url";
    public static final int MESSAGE_FAIL = 2;
    private static final int MESSAGE_ID = 1;
    static ImageLoaderManager instance;
    HashMap<Integer, ArrayList<Integer>> bmpReferenceMap = new HashMap<>();
    /* access modifiers changed from: private */
    public ImageCacheManager cacheManager;
    /* access modifiers changed from: private */
    public CallbackManager callbackManager = new CallbackManager();
    private DiskIOImageThread diskIOImgThread = new DiskIOImageThread();
    private DownloadImageThread[] downloadImgThread = {new DownloadImageThread(), new DownloadImageThread(), new DownloadImageThread()};
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Bundle bundle = msg.getData();
                    ImageLoaderManager.this.callbackManager.callback(bundle.getString(ImageLoaderManager.EXTRA_IMG_URL), (Bitmap) bundle.getParcelable(ImageLoaderManager.EXTRA_IMG));
                    return;
                case 2:
                    ImageLoaderManager.this.callbackManager.fail(msg.getData().getString(ImageLoaderManager.EXTRA_IMG_URL));
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Vector<String> urlDequeDiskIO = new Vector<>();
    /* access modifiers changed from: private */
    public Vector<String> urlDequeDownload = new Vector<>();

    public interface DownloadCallback {
        void onFail(String str);

        void onSucced(String str, Bitmap bitmap);
    }

    public ImageLoaderManager(ImageCacheManager cacheManager2) {
        this.cacheManager = cacheManager2;
    }

    private WeakReference<Bitmap> get(String url, ImageLoaderCallback callback) {
        WeakReference<Bitmap> bitmap = this.cacheManager.getFromCache(url);
        if (bitmap == null || bitmap.get() == null) {
            this.callbackManager.put(url, callback);
            startFetchingTread(url);
        }
        return bitmap;
    }

    public void AdjustPriority(ArrayList<String> urls) {
        while (urls.size() > 0) {
            String url = urls.remove(urls.size() - 1);
            if (this.urlDequeDiskIO.remove(url)) {
                this.urlDequeDiskIO.add(0, url);
            }
            if (this.urlDequeDownload.remove(url)) {
                this.urlDequeDownload.add(0, url);
            }
        }
    }

    public void Cancel(List<String> urls) {
        for (int i = 0; i < urls.size(); i++) {
            String url = urls.get(i);
            this.urlDequeDiskIO.remove(url);
            this.urlDequeDownload.remove(url);
            this.callbackManager.remove(url);
        }
    }

    public void Cancel(String url, Object object) {
        if (this.callbackManager.remove(url, object)) {
            this.urlDequeDiskIO.remove(url);
            this.urlDequeDownload.remove(url);
        }
    }

    /* access modifiers changed from: protected */
    public void putToDownloadDeque(String url) {
        if (!this.urlDequeDownload.contains(url)) {
            this.urlDequeDownload.add(url);
        }
    }

    private void startFetchingTread(String url) {
        putUrlToUrlQueue(url);
        Thread.State state = this.diskIOImgThread.getState();
        if (state == Thread.State.NEW) {
            this.diskIOImgThread.start();
        } else if (state == Thread.State.TERMINATED) {
            this.diskIOImgThread = new DiskIOImageThread();
            this.diskIOImgThread.start();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void startDownloadingTread() {
        for (int i = 0; i < this.downloadImgThread.length; i++) {
            Thread.State state = this.downloadImgThread[i].getState();
            if (state == Thread.State.NEW) {
                this.downloadImgThread[i].start();
            } else if (state == Thread.State.TERMINATED) {
                this.downloadImgThread[i] = new DownloadImageThread();
                this.downloadImgThread[i].start();
            }
        }
    }

    private void putUrlToUrlQueue(String url) {
        if (!this.urlDequeDiskIO.contains(url) && !this.urlDequeDownload.contains(url)) {
            this.urlDequeDiskIO.add(url);
        }
    }

    private class DiskIOImageThread extends Thread {
        private boolean isRun;
        private String mCurrentUrl;

        private DiskIOImageThread() {
            this.isRun = true;
            this.mCurrentUrl = null;
        }

        public void shutDown() {
            this.isRun = false;
        }

        public void run() {
            while (this.isRun && ImageLoaderManager.this.urlDequeDiskIO.size() > 0) {
                try {
                    this.mCurrentUrl = (String) ImageLoaderManager.this.urlDequeDiskIO.remove(0);
                    if (this.mCurrentUrl != null) {
                        WeakReference<Bitmap> bitmap = ImageLoaderManager.this.cacheManager.getFromCache(this.mCurrentUrl);
                        if (bitmap == null || bitmap.get() == null) {
                            ImageLoaderManager.this.putToDownloadDeque(this.mCurrentUrl);
                            ImageLoaderManager.this.startDownloadingTread();
                        } else {
                            Message msg = ImageLoaderManager.this.handler.obtainMessage(1);
                            Bundle bundle = msg.getData();
                            bundle.putSerializable(ImageLoaderManager.EXTRA_IMG_URL, this.mCurrentUrl);
                            bundle.putParcelable(ImageLoaderManager.EXTRA_IMG, bitmap.get());
                            ImageLoaderManager.this.handler.sendMessage(msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                } finally {
                    shutDown();
                }
            }
        }
    }

    private class DownloadImageThread extends Thread {
        private boolean isRun;

        private DownloadImageThread() {
            this.isRun = true;
        }

        public void shutDown() {
            this.isRun = false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
            if (r5 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
            if (r5.trim().startsWith("http") == false) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x003b, code lost:
            r1 = com.baixing.imageCache.ImageLoaderManager.access$400(r9.this$0).safeGetFromNetwork(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0045, code lost:
            if (r1 == null) goto L_0x0082;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x004b, code lost:
            if (r1.get() == null) goto L_0x0082;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
            r4 = r9.this$0.handler.obtainMessage(1);
            r2 = r4.getData();
            r2.putSerializable(com.baixing.imageCache.ImageLoaderManager.EXTRA_IMG_URL, r5);
            r2.putParcelable(com.baixing.imageCache.ImageLoaderManager.EXTRA_IMG, r1.get());
            r9.this$0.handler.sendMessage(r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            r4 = r9.this$0.handler.obtainMessage(2);
            r4.getData().putSerializable(com.baixing.imageCache.ImageLoaderManager.EXTRA_IMG_URL, r5);
            r9.this$0.handler.sendMessage(r4);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r9 = this;
            L_0x0000:
                boolean r6 = r9.isRun     // Catch:{ Exception -> 0x0072 }
                if (r6 == 0) goto L_0x0019
                r5 = 0
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ Exception -> 0x0072 }
                java.util.Vector r7 = r6.urlDequeDownload     // Catch:{ Exception -> 0x0072 }
                monitor-enter(r7)     // Catch:{ Exception -> 0x0072 }
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ all -> 0x007a }
                java.util.Vector r6 = r6.urlDequeDownload     // Catch:{ all -> 0x007a }
                int r6 = r6.size()     // Catch:{ all -> 0x007a }
                if (r6 > 0) goto L_0x001d
                monitor-exit(r7)     // Catch:{ all -> 0x007a }
            L_0x0019:
                r9.shutDown()
            L_0x001c:
                return
            L_0x001d:
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ all -> 0x007a }
                java.util.Vector r6 = r6.urlDequeDownload     // Catch:{ all -> 0x007a }
                r8 = 0
                java.lang.Object r6 = r6.remove(r8)     // Catch:{ all -> 0x007a }
                r0 = r6
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x007a }
                r5 = r0
                monitor-exit(r7)     // Catch:{ all -> 0x007a }
                if (r5 == 0) goto L_0x0000
                java.lang.String r6 = r5.trim()     // Catch:{ Exception -> 0x0072 }
                java.lang.String r7 = "http"
                boolean r6 = r6.startsWith(r7)     // Catch:{ Exception -> 0x0072 }
                if (r6 == 0) goto L_0x0000
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ Exception -> 0x0072 }
                com.baixing.imageCache.ImageCacheManager r6 = r6.cacheManager     // Catch:{ Exception -> 0x0072 }
                java.lang.ref.WeakReference r1 = r6.safeGetFromNetwork(r5)     // Catch:{ Exception -> 0x0072 }
                if (r1 == 0) goto L_0x0082
                java.lang.Object r6 = r1.get()     // Catch:{ Exception -> 0x0072 }
                if (r6 == 0) goto L_0x0082
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ Exception -> 0x0072 }
                android.os.Handler r6 = r6.handler     // Catch:{ Exception -> 0x0072 }
                r7 = 1
                android.os.Message r4 = r6.obtainMessage(r7)     // Catch:{ Exception -> 0x0072 }
                android.os.Bundle r2 = r4.getData()     // Catch:{ Exception -> 0x0072 }
                java.lang.String r6 = "extra_img_url"
                r2.putSerializable(r6, r5)     // Catch:{ Exception -> 0x0072 }
                java.lang.String r7 = "extra_img"
                java.lang.Object r6 = r1.get()     // Catch:{ Exception -> 0x0072 }
                android.os.Parcelable r6 = (android.os.Parcelable) r6     // Catch:{ Exception -> 0x0072 }
                r2.putParcelable(r7, r6)     // Catch:{ Exception -> 0x0072 }
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ Exception -> 0x0072 }
                android.os.Handler r6 = r6.handler     // Catch:{ Exception -> 0x0072 }
                r6.sendMessage(r4)     // Catch:{ Exception -> 0x0072 }
                goto L_0x0000
            L_0x0072:
                r3 = move-exception
                r3.printStackTrace()     // Catch:{ all -> 0x007d }
                r9.shutDown()
                goto L_0x001c
            L_0x007a:
                r6 = move-exception
                monitor-exit(r7)     // Catch:{ all -> 0x007a }
                throw r6     // Catch:{ Exception -> 0x0072 }
            L_0x007d:
                r6 = move-exception
                r9.shutDown()
                throw r6
            L_0x0082:
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ Exception -> 0x0072 }
                android.os.Handler r6 = r6.handler     // Catch:{ Exception -> 0x0072 }
                r7 = 2
                android.os.Message r4 = r6.obtainMessage(r7)     // Catch:{ Exception -> 0x0072 }
                android.os.Bundle r2 = r4.getData()     // Catch:{ Exception -> 0x0072 }
                java.lang.String r6 = "extra_img_url"
                r2.putSerializable(r6, r5)     // Catch:{ Exception -> 0x0072 }
                com.baixing.imageCache.ImageLoaderManager r6 = com.baixing.imageCache.ImageLoaderManager.this     // Catch:{ Exception -> 0x0072 }
                android.os.Handler r6 = r6.handler     // Catch:{ Exception -> 0x0072 }
                r6.sendMessage(r4)     // Catch:{ Exception -> 0x0072 }
                goto L_0x0000
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baixing.imageCache.ImageLoaderManager.DownloadImageThread.run():void");
        }
    }

    /* access modifiers changed from: private */
    public WeakReference<Bitmap> getBitmapInMemory(String url) {
        if (url == null || url.equals("")) {
            return null;
        }
        return this.cacheManager.getFromCache(url);
    }

    public void loadImg(final DownloadCallback downloadCallback, String url) {
        ImageLoaderCallback loaderCallback = new ImageLoaderCallback() {
            public void refresh(String url, Bitmap bitmap) {
                downloadCallback.onSucced(url, bitmap);
            }

            public Object getObject() {
                return null;
            }

            public void fail(String url) {
                downloadCallback.onFail(url);
            }
        };
        WeakReference<Bitmap> bp = get(url, loaderCallback);
        if (bp != null && bp.get() != null) {
            loaderCallback.refresh(url, bp.get());
        }
    }

    public void showImg(final View view, final String url, final String preUrl, Context con, WeakReference<Bitmap> bmp) {
        view.setTag(url);
        WeakReference<Bitmap> bitmap = get(url, getCallback(url, preUrl, view, bmp));
        if (bitmap != null && bitmap.get() != null) {
            new AsyncTask<Bitmap, Boolean, Bitmap>() {
                /* access modifiers changed from: protected */
                public Bitmap doInBackground(Bitmap... bitmaps) {
                    return bitmaps[0];
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Bitmap bitmap_) {
                    WeakReference<Bitmap> bmp;
                    Drawable curDrawable;
                    Bitmap curBmp;
                    synchronized (this) {
                        if (((String) view.getTag()).equals(url) && !bitmap_.isRecycled()) {
                            if (!(url.equals(preUrl) || (bmp = ImageLoaderManager.this.getBitmapInMemory(preUrl)) == null || bmp.get() == null)) {
                                if (view instanceof ImageView) {
                                    curDrawable = ((ImageView) view).getDrawable();
                                } else {
                                    curDrawable = view.getBackground();
                                }
                                if (curDrawable != null && (curDrawable instanceof BitmapDrawable) && (curBmp = ((BitmapDrawable) curDrawable).getBitmap()) != null && curBmp.hashCode() == bmp.hashCode() && ImageLoaderManager.this.decreaseBitmapReferenceCount(bmp.hashCode(), view.hashCode()) <= 0) {
                                    ImageLoaderManager.this.cacheManager.forceRecycle(preUrl, true);
                                }
                            }
                            if (view instanceof ImageView) {
                                ((ImageView) view).setImageBitmap(bitmap_);
                            } else {
                                view.setBackgroundDrawable(new BitmapDrawable(bitmap_));
                            }
                            int unused = ImageLoaderManager.this.increaseBitmapReferenceCount(bitmap_.hashCode(), view.hashCode());
                        }
                    }
                }
            }.execute(bitmap.get());
        }
    }

    public void showImg(View view, String url, String preUrl, Context con) {
        showImg(view, url, preUrl, con, null);
    }

    /* access modifiers changed from: private */
    public int decreaseBitmapReferenceCount(int bmpHashCode, int viewHashCode) {
        ArrayList<Integer> value;
        if (!this.bmpReferenceMap.containsKey(Integer.valueOf(bmpHashCode)) || (value = this.bmpReferenceMap.get(Integer.valueOf(bmpHashCode))) == null) {
            return -1;
        }
        int i = 0;
        while (true) {
            if (i >= value.size()) {
                break;
            } else if (((Integer) value.get(i)).intValue() == viewHashCode) {
                value.remove(i);
                break;
            } else {
                i++;
            }
        }
        return value.size();
    }

    /* access modifiers changed from: private */
    public int increaseBitmapReferenceCount(int bmpHashCode, int viewHashCode) {
        if (this.bmpReferenceMap.containsKey(Integer.valueOf(bmpHashCode))) {
            ArrayList<Integer> value = this.bmpReferenceMap.get(Integer.valueOf(bmpHashCode));
            if (value != null) {
                value.add(Integer.valueOf(viewHashCode));
            }
            if (value == null) {
                return -1;
            }
            return value.size();
        }
        ArrayList<Integer> value2 = new ArrayList<>();
        value2.add(Integer.valueOf(viewHashCode));
        this.bmpReferenceMap.put(Integer.valueOf(bmpHashCode), value2);
        return 1;
    }

    private ImageLoaderCallback getCallback(String url, final String preUrl, final View view, final WeakReference<Bitmap> defaultBmp) {
        return new ImageLoaderCallback() {
            private boolean inFailStatus = false;

            public void refresh(String url, Bitmap bitmap) {
                WeakReference<Bitmap> bmp;
                Drawable curDrawable;
                Bitmap curBmp;
                if (bitmap != null) {
                    synchronized (this) {
                        if (url.equals(view.getTag().toString())) {
                            if (!bitmap.isRecycled()) {
                                if (!url.equals(preUrl) && (bmp = ImageLoaderManager.this.getBitmapInMemory(preUrl)) != null) {
                                    if (view instanceof ImageView) {
                                        curDrawable = ((ImageView) view).getDrawable();
                                    } else {
                                        curDrawable = view.getBackground();
                                    }
                                    if (curDrawable != null && (curDrawable instanceof BitmapDrawable) && (curBmp = ((BitmapDrawable) curDrawable).getBitmap()) != null && curBmp.hashCode() == bmp.hashCode() && ImageLoaderManager.this.decreaseBitmapReferenceCount(bmp.hashCode(), view.hashCode()) <= 0) {
                                        ImageLoaderManager.this.cacheManager.forceRecycle(preUrl, true);
                                    }
                                }
                                if (view instanceof ImageView) {
                                    ((ImageView) view).setImageBitmap(bitmap);
                                } else {
                                    view.setBackgroundDrawable(new BitmapDrawable(bitmap));
                                }
                                int unused = ImageLoaderManager.this.increaseBitmapReferenceCount(bitmap.hashCode(), view.hashCode());
                            }
                            this.inFailStatus = false;
                        }
                    }
                }
            }

            public Object getObject() {
                return view;
            }

            public void fail(String url) {
                if (url.equals(view.getTag().toString()) && defaultBmp != null && defaultBmp.get() != null && !this.inFailStatus) {
                    if (view instanceof ImageView) {
                        ((ImageView) view).setImageBitmap((Bitmap) defaultBmp.get());
                        view.invalidate();
                    } else {
                        view.setBackgroundDrawable(new BitmapDrawable((Bitmap) defaultBmp.get()));
                    }
                    this.inFailStatus = true;
                }
            }
        };
    }
}
