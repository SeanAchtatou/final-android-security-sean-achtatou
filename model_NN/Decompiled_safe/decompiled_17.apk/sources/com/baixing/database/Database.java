package com.baixing.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public abstract class Database {
    protected static SQLiteDatabase database;
    protected static SQLiteDatabase databaseRO;
    protected Context context;

    Database(Context ctx) {
        if (database == null) {
            try {
                DatabaseOpenHelper helper = new DatabaseOpenHelper(ctx);
                this.context = ctx;
                database = helper.getWritableDatabase();
                databaseRO = helper.getReadableDatabase();
            } catch (SQLiteException e) {
            }
        }
    }
}
