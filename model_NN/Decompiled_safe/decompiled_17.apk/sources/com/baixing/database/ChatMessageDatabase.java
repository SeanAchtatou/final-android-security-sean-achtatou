package com.baixing.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.baixing.entity.ChatMessage;
import com.baixing.entity.compare.MsgTimeComparator;
import com.baixing.network.api.ApiParams;
import com.tencent.tauth.Constants;
import java.util.Collections;
import java.util.List;

public class ChatMessageDatabase extends Database {
    ChatMessageDatabase(Context ctx) {
        super(ctx);
    }

    public static void prepareDB(Context ctx) {
        if (database == null) {
            new ChatMessageDatabase(ctx);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054 A[Catch:{ Throwable -> 0x005b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.baixing.entity.ChatMessage> queryMessageBySession(java.lang.String r13) {
        /*
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            android.database.sqlite.SQLiteDatabase r0 = com.baixing.database.ChatMessageDatabase.databaseRO
            if (r0 != 0) goto L_0x000a
        L_0x0009:
            return r11
        L_0x000a:
            android.database.sqlite.SQLiteDatabase r0 = com.baixing.database.ChatMessageDatabase.databaseRO     // Catch:{ Throwable -> 0x005b }
            java.lang.String r1 = "chatMsg"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x005b }
            r3 = 0
            java.lang.String r4 = "msgJson"
            r2[r3] = r4     // Catch:{ Throwable -> 0x005b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x005b }
            r3.<init>()     // Catch:{ Throwable -> 0x005b }
            java.lang.String r4 = "sessionId='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x005b }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ Throwable -> 0x005b }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x005b }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x005b }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Throwable -> 0x005b }
            if (r9 == 0) goto L_0x0052
            boolean r0 = r9.moveToFirst()     // Catch:{ Throwable -> 0x005b }
            if (r0 == 0) goto L_0x0052
        L_0x0040:
            r0 = 0
            java.lang.String r12 = r9.getString(r0)     // Catch:{ Throwable -> 0x005b }
            com.baixing.entity.ChatMessage r0 = com.baixing.entity.ChatMessage.fromJson(r12)     // Catch:{ Throwable -> 0x0060 }
            r11.add(r0)     // Catch:{ Throwable -> 0x0060 }
        L_0x004c:
            boolean r0 = r9.moveToNext()     // Catch:{ Throwable -> 0x005b }
            if (r0 != 0) goto L_0x0040
        L_0x0052:
            if (r9 == 0) goto L_0x0009
            r9.deactivate()     // Catch:{ Throwable -> 0x005b }
            r9.close()     // Catch:{ Throwable -> 0x005b }
            goto L_0x0009
        L_0x005b:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0009
        L_0x0060:
            r0 = move-exception
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.database.ChatMessageDatabase.queryMessageBySession(java.lang.String):java.util.List");
    }

    public static ChatMessage getLastMessage(String sid) {
        List<ChatMessage> msglist = queryMessageBySession(sid);
        if (msglist.size() == 0) {
            return null;
        }
        Collections.sort(msglist, new MsgTimeComparator());
        return msglist.get(msglist.size() - 1);
    }

    public static void updateReadStatus(String msgId, boolean readStatus) {
        if (database != null) {
            try {
                if (hasMessage(msgId)) {
                    database.execSQL("update chatMsg set readstatus = " + Integer.valueOf(readStatus ? 1 : 0));
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public static int getUnreadCount(String sid, String uid) {
        if (databaseRO == null) {
            return 0;
        }
        String where = "( readstatus=0 OR readstatus is NULL )";
        if (sid != null) {
            where = where + " AND sessionId='" + sid + "'";
        }
        if (uid != null) {
            where = where + " AND receiver='" + uid + "'";
        }
        Cursor cur = null;
        try {
            cur = databaseRO.query(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, new String[]{"readstatus"}, where, null, null, null, null);
            if (cur != null) {
                int count = cur.getCount();
            }
            if (cur != null) {
                cur.deactivate();
                cur.close();
            }
            return 0;
        } finally {
            if (cur != null) {
                cur.deactivate();
                cur.close();
            }
        }
    }

    public static String getSessionId(String from, String to, String adId) {
        String sid = null;
        if (databaseRO != null) {
            sid = null;
            String where = "((sender='" + from + "' AND receiver='" + to + "')" + " OR " + "(receiver='" + from + "' AND sender='" + to + "')" + ")";
            if (adId != null && adId.trim().length() > 0) {
                where = where + " AND adId='" + adId + "'";
            }
            try {
                Cursor cur = databaseRO.query(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, new String[]{"sessionId"}, where, null, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    sid = cur.getString(0);
                }
                if (cur != null) {
                    cur.deactivate();
                    cur.close();
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return sid;
    }

    public static boolean hasMessage(String msgId) {
        boolean exists = false;
        if (databaseRO != null) {
            exists = false;
            try {
                Cursor cur = databaseRO.query(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, new String[]{"msgId"}, "msgId='" + msgId + "'", null, null, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    exists = true;
                }
                if (cur != null) {
                    cur.deactivate();
                    cur.close();
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return exists;
    }

    public static ChatMessage queryMessageByMsgId(String msgId) {
        if (databaseRO == null) {
            return null;
        }
        Cursor cur = null;
        try {
            cur = databaseRO.query(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, new String[]{"msgJson"}, "msgId='" + msgId + "'", null, null, null, null, null);
            if (cur == null || !cur.moveToFirst()) {
                if (cur != null) {
                    cur.deactivate();
                    cur.close();
                }
                return null;
            }
            ChatMessage fromJson = ChatMessage.fromJson(cur.getString(0));
        } finally {
            if (cur != null) {
                cur.deactivate();
                cur.close();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.database.ChatMessageDatabase.storeMessage(com.baixing.entity.ChatMessage, boolean):void
     arg types: [com.baixing.entity.ChatMessage, int]
     candidates:
      com.baixing.database.ChatMessageDatabase.storeMessage(java.util.List<com.baixing.entity.ChatMessage>, boolean):void
      com.baixing.database.ChatMessageDatabase.storeMessage(com.baixing.entity.ChatMessage, boolean):void */
    public static void storeMessage(ChatMessage msg) {
        storeMessage(msg, false);
    }

    public static void storeMessage(ChatMessage msg, boolean markRead) {
        if (database != null) {
            ContentValues values = new ContentValues();
            values.put("msgId", msg.getId());
            values.put("adId", msg.getAdId());
            values.put("sender", msg.getFrom());
            values.put(Constants.PARAM_RECEIVER, msg.getTo());
            values.put("msgJson", msg.toJson());
            values.put("sessionId", msg.getSession());
            values.put(ApiParams.KEY_TIMESTAMP, Long.valueOf(msg.getTimestamp()));
            try {
                if (hasMessage(msg.getId())) {
                    values.put("readstatus", "1");
                    database.update(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, values, "msgId='" + msg.getId() + "'", null);
                    return;
                }
                values.put("readstatus", markRead ? "1" : "0");
                database.insert(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, null, values);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public static void storeMessage(List<ChatMessage> messages, boolean markRead) {
        if (database != null) {
            database.beginTransaction();
            for (ChatMessage msg : messages) {
                storeMessage(msg, markRead);
            }
            database.setTransactionSuccessful();
            database.endTransaction();
        }
    }

    public static void deleteMsgOlderthan(long olderThan) {
        if (database != null) {
            try {
                database.delete(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, "timestamp < " + olderThan, null);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public static void deleteMsgBySession(String sid) {
        if (database != null) {
            try {
                database.delete(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, "sessionId='" + sid + "'", null);
            } catch (Throwable th) {
            }
        }
    }

    public static void clearOldMessage(long keepCount) {
        if (databaseRO != null) {
            long time = 0;
            try {
                Cursor cur = databaseRO.query(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, new String[]{ApiParams.KEY_TIMESTAMP}, null, null, null, null, ApiParams.KEY_TIMESTAMP, null);
                if (((long) cur.getCount()) > keepCount) {
                    for (int i = 0; ((long) i) < keepCount; i++) {
                        cur.moveToNext();
                        time = cur.getLong(0);
                    }
                }
                if (cur != null) {
                    cur.deactivate();
                    cur.close();
                }
            } catch (Throwable th) {
            }
            if (time != 0) {
                deleteMsgOlderthan(time);
            }
        }
    }

    public static void clearDatabase() {
        if (database != null) {
            try {
                database.delete(DatabaseOpenHelper.CHAT_MESSAGE_TABLE, null, null);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public static void storeMessage(String msg) {
        ChatMessage msgObj = ChatMessage.fromJson(msg);
        if (msgObj.getId() != null) {
            storeMessage(msgObj);
        }
    }
}
