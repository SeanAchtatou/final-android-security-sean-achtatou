package com.baixing.util;

import android.text.TextUtils;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Locale;
import org.apache.commons.httpclient.HttpStatus;

public class BXHanzi2Pinyin {
    private static final Collator COLLATOR = Collator.getInstance(Locale.CHINA);
    private static final String FIRST_PINYIN_UNIHAN = "阿";
    private static final char FIRST_UNIHAN = '㐀';
    private static final String LAST_PINYIN_UNIHAN = "蓙";
    private static final byte[][] PINYINS;
    private static final String TAG = "HanziToPinyin";
    private static final char[] UNIHANS;
    private static BXHanzi2Pinyin sInstance;
    private final boolean mHasChinaCollator;

    static {
        char[] cArr = new char[HttpStatus.SC_NOT_ACCEPTABLE];
        // fill-array-data instruction
        cArr[0] = 21526;
        cArr[1] = 21710;
        cArr[2] = 23433;
        cArr[3] = -32594;
        cArr[4] = 20985;
        cArr[5] = 20843;
        cArr[6] = 25520;
        cArr[7] = 25203;
        cArr[8] = -28506;
        cArr[9] = 21241;
        cArr[10] = -27070;
        cArr[11] = 22868;
        cArr[12] = 20283;
        cArr[13] = 30336;
        cArr[14] = 30765;
        cArr[15] = 28780;
        cArr[16] = 24971;
        cArr[17] = 27715;
        cArr[18] = 20907;
        cArr[19] = 30326;
        cArr[20] = 23788;
        cArr[21] = 22163;
        cArr[22] = 20594;
        cArr[23] = 21442;
        cArr[24] = 20179;
        cArr[25] = 25761;
        cArr[26] = 20874;
        cArr[27] = 23934;
        cArr[28] = 22092;
        cArr[29] = 25184;
        cArr[30] = 25286;
        cArr[31] = -28737;
        cArr[32] = 20261;
        cArr[33] = 25220;
        cArr[34] = -28826;
        cArr[35] = 25275;
        cArr[36] = -27081;
        cArr[37] = 21507;
        cArr[38] = 20805;
        cArr[39] = 25277;
        cArr[40] = 20986;
        cArr[41] = 25611;
        cArr[42] = 24027;
        cArr[43] = 20997;
        cArr[44] = 21561;
        cArr[45] = 26110;
        cArr[46] = -29164;
        cArr[47] = 21618;
        cArr[48] = 20174;
        cArr[49] = 20945;
        cArr[50] = 31895;
        cArr[51] = 27718;
        cArr[52] = 23828;
        cArr[53] = -28504;
        cArr[54] = 25619;
        cArr[55] = 21649;
        cArr[56] = 21574;
        cArr[57] = 20025;
        cArr[58] = 24403;
        cArr[59] = 20992;
        cArr[60] = 24692;
        cArr[61] = 25189;
        cArr[62] = 28783;
        cArr[63] = 20222;
        cArr[64] = 22002;
        cArr[65] = 25921;
        cArr[66] = 20993;
        cArr[67] = 29241;
        cArr[68] = 19969;
        cArr[69] = 19999;
        cArr[70] = 19996;
        cArr[71] = 21562;
        cArr[72] = 21090;
        cArr[73] = -32751;
        cArr[74] = 22422;
        cArr[75] = 21544;
        cArr[76] = 22810;
        cArr[77] = 22968;
        cArr[78] = 22848;
        cArr[79] = -26715;
        cArr[80] = -32756;
        cArr[81] = 21457;
        cArr[82] = 24070;
        cArr[83] = 21274;
        cArr[84] = -26402;
        cArr[85] = 20998;
        cArr[86] = 20016;
        cArr[87] = -30331;
        cArr[88] = 20175;
        cArr[89] = 22458;
        cArr[90] = 32017;
        cArr[91] = 22827;
        cArr[92] = 29476;
        cArr[93] = 26094;
        cArr[94] = 20357;
        cArr[95] = 24178;
        cArr[96] = 20872;
        cArr[97] = 30347;
        cArr[98] = 25096;
        cArr[99] = 32473;
        cArr[100] = 26681;
        cArr[101] = 25583;
        cArr[102] = 21948;
        cArr[103] = 22000;
        cArr[104] = 24037;
        cArr[105] = 21246;
        cArr[106] = 20272;
        cArr[107] = -25031;
        cArr[108] = 20054;
        cArr[109] = 20851;
        cArr[110] = 20809;
        cArr[111] = 24402;
        cArr[112] = 20008;
        cArr[113] = -29900;
        cArr[114] = 21593;
        cArr[115] = 22926;
        cArr[116] = 21645;
        cArr[117] = 20292;
        cArr[118] = 22831;
        cArr[119] = -31968;
        cArr[120] = -29757;
        cArr[121] = -24878;
        cArr[122] = 25323;
        cArr[123] = 20136;
        cArr[124] = 21503;
        cArr[125] = -24767;
        cArr[126] = 20046;
        cArr[127] = -32079;
        cArr[128] = 24576;
        cArr[129] = 27426;
        cArr[130] = 24031;
        cArr[131] = 28784;
        cArr[132] = 26127;
        cArr[133] = 21529;
        cArr[134] = 19980;
        cArr[135] = 21152;
        cArr[136] = 25099;
        cArr[137] = 27743;
        cArr[138] = -32131;
        cArr[139] = -27082;
        cArr[140] = 24062;
        cArr[141] = 22357;
        cArr[142] = 20866;
        cArr[143] = 20009;
        cArr[144] = 20965;
        cArr[145] = 23010;
        cArr[146] = 22104;
        cArr[147] = 20891;
        cArr[148] = 21652;
        cArr[149] = 24320;
        cArr[150] = -27762;
        cArr[151] = 24572;
        cArr[152] = 23611;
        cArr[153] = 21308;
        cArr[154] = -32626;
        cArr[155] = 21157;
        cArr[156] = 31354;
        cArr[157] = 25248;
        cArr[158] = 25181;
        cArr[159] = 22840;
        cArr[160] = -31569;
        cArr[161] = 23485;
        cArr[162] = 21281;
        cArr[163] = 20111;
        cArr[164] = 22372;
        cArr[165] = 25193;
        cArr[166] = 25289;
        cArr[167] = 20358;
        cArr[168] = 20848;
        cArr[169] = 21879;
        cArr[170] = 25438;
        cArr[171] = 20162;
        cArr[172] = -26889;
        cArr[173] = -32457;
        cArr[174] = 26865;
        cArr[175] = 26974;
        cArr[176] = 21774;
        cArr[177] = 20457;
        cArr[178] = 23294;
        cArr[179] = -32145;
        cArr[180] = -29059;
        cArr[181] = 22483;
        cArr[182] = 21432;
        cArr[183] = 25294;
        cArr[184] = 28316;
        cArr[185] = -24679;
        cArr[186] = 23044;
        cArr[187] = 22108;
        cArr[188] = 23402;
        cArr[189] = 25249;
        cArr[190] = -26575;
        cArr[191] = 22920;
        cArr[192] = 22475;
        cArr[193] = -26465;
        cArr[194] = 29284;
        cArr[195] = 29483;
        cArr[196] = 21573;
        cArr[197] = -27160;
        cArr[198] = 27667;
        cArr[199] = 21674;
        cArr[200] = 23424;
        cArr[201] = 21941;
        cArr[202] = 20060;
        cArr[203] = 27665;
        cArr[204] = 21517;
        cArr[205] = -29652;
        cArr[206] = 25720;
        cArr[207] = 29279;
        cArr[208] = 27597;
        cArr[209] = 25295;
        cArr[210] = -32439;
        cArr[211] = 22241;
        cArr[212] = 22228;
        cArr[213] = 23404;
        cArr[214] = -29769;
        cArr[215] = 23070;
        cArr[216] = 23273;
        cArr[217] = -32515;
        cArr[218] = -27984;
        cArr[219] = 25288;
        cArr[220] = 23064;
        cArr[221] = -25057;
        cArr[222] = 25423;
        cArr[223] = 22236;
        cArr[224] = 23425;
        cArr[225] = 22942;
        cArr[226] = 20892;
        cArr[227] = 32698;
        cArr[228] = 22900;
        cArr[229] = 22907;
        cArr[230] = -24895;
        cArr[231] = -28467;
        cArr[232] = 22114;
        cArr[233] = -29772;
        cArr[234] = 22929;
        cArr[235] = 25293;
        cArr[236] = 30469;
        cArr[237] = 27768;
        cArr[238] = 25243;
        cArr[239] = 21624;
        cArr[240] = 21943;
        cArr[241] = 21257;
        cArr[242] = 20086;
        cArr[243] = 29255;
        cArr[244] = 21117;
        cArr[245] = 27669;
        cArr[246] = 23000;
        cArr[247] = 20050;
        cArr[248] = -27509;
        cArr[249] = 21078;
        cArr[250] = 20166;
        cArr[251] = 19971;
        cArr[252] = 25488;
        cArr[253] = 21315;
        cArr[254] = 21595;
        cArr[255] = 24708;
        cArr[256] = 20999;
        cArr[257] = 20146;
        cArr[258] = -26799;
        cArr[259] = 23430;
        cArr[260] = 29911;
        cArr[261] = 21306;
        cArr[262] = 23761;
        cArr[263] = 28820;
        cArr[264] = 22795;
        cArr[265] = 21605;
        cArr[266] = 31331;
        cArr[267] = -31909;
        cArr[268] = 24825;
        cArr[269] = 20154;
        cArr[270] = 25172;
        cArr[271] = 26085;
        cArr[272] = 25102;
        cArr[273] = 21433;
        cArr[274] = -28518;
        cArr[275] = 22567;
        cArr[276] = 26741;
        cArr[277] = -27152;
        cArr[278] = 25404;
        cArr[279] = 20200;
        cArr[280] = 27618;
        cArr[281] = 19977;
        cArr[282] = 26706;
        cArr[283] = 25531;
        cArr[284] = -32142;
        cArr[285] = 26862;
        cArr[286] = 20711;
        cArr[287] = 26432;
        cArr[288] = 31579;
        cArr[289] = 23665;
        cArr[290] = 20260;
        cArr[291] = 24368;
        cArr[292] = 22882;
        cArr[293] = 30003;
        cArr[294] = 21319;
        cArr[295] = 23608;
        cArr[296] = 21454;
        cArr[297] = 20070;
        cArr[298] = 21047;
        cArr[299] = -30608;
        cArr[300] = -27159;
        cArr[301] = 21452;
        cArr[302] = -29695;
        cArr[303] = 21550;
        cArr[304] = -29708;
        cArr[305] = 21430;
        cArr[306] = 24554;
        cArr[307] = 20929;
        cArr[308] = -32049;
        cArr[309] = 29435;
        cArr[310] = 22794;
        cArr[311] = 23385;
        cArr[312] = 21766;
        cArr[313] = 20182;
        cArr[314] = 23393;
        cArr[315] = 22349;
        cArr[316] = 27748;
        cArr[317] = 22834;
        cArr[318] = 24529;
        cArr[319] = -32337;
        cArr[320] = 21076;
        cArr[321] = 22825;
        cArr[322] = 26091;
        cArr[323] = 24599;
        cArr[324] = 21381;
        cArr[325] = 28853;
        cArr[326] = 20599;
        cArr[327] = 20984;
        cArr[328] = 28237;
        cArr[329] = 25512;
        cArr[330] = 21534;
        cArr[331] = -29780;
        cArr[332] = 21176;
        cArr[333] = 27498;
        cArr[334] = 24367;
        cArr[335] = 23587;
        cArr[336] = 21361;
        cArr[337] = 26167;
        cArr[338] = 32705;
        cArr[339] = 25373;
        cArr[340] = 20044;
        cArr[341] = 22805;
        cArr[342] = 21623;
        cArr[343] = 20185;
        cArr[344] = 20065;
        cArr[345] = 28785;
        cArr[346] = 20123;
        cArr[347] = 24515;
        cArr[348] = 20852;
        cArr[349] = 20982;
        cArr[350] = 20241;
        cArr[351] = 25100;
        cArr[352] = 21509;
        cArr[353] = 30134;
        cArr[354] = 28709;
        cArr[355] = 20011;
        cArr[356] = 21693;
        cArr[357] = 22830;
        cArr[358] = 24186;
        cArr[359] = 20539;
        cArr[360] = 19968;
        cArr[361] = 20058;
        cArr[362] = 24212;
        cArr[363] = 21815;
        cArr[364] = 20323;
        cArr[365] = 20248;
        cArr[366] = 32417;
        cArr[367] = 22246;
        cArr[368] = 26352;
        cArr[369] = -31616;
        cArr[370] = 24064;
        cArr[371] = 28797;
        cArr[372] = 20802;
        cArr[373] = 29250;
        cArr[374] = 20654;
        cArr[375] = 21867;
        cArr[376] = -25503;
        cArr[377] = 24590;
        cArr[378] = 26365;
        cArr[379] = 21522;
        cArr[380] = 25434;
        cArr[381] = 27838;
        cArr[382] = 24352;
        cArr[383] = 20299;
        cArr[384] = -30969;
        cArr[385] = -29410;
        cArr[386] = -24850;
        cArr[387] = 20043;
        cArr[388] = 20013;
        cArr[389] = 24030;
        cArr[390] = 26417;
        cArr[391] = 25235;
        cArr[392] = 25341;
        cArr[393] = 19987;
        cArr[394] = 22918;
        cArr[395] = -26951;
        cArr[396] = 23442;
        cArr[397] = 21331;
        cArr[398] = 20180;
        cArr[399] = 23447;
        cArr[400] = -28487;
        cArr[401] = 31199;
        cArr[402] = 21143;
        cArr[403] = 22010;
        cArr[404] = 23562;
        cArr[405] = 26152;
        UNIHANS = cArr;
        byte[][] bArr = new byte[HttpStatus.SC_NOT_ACCEPTABLE][];
        bArr[0] = new byte[]{65, 0, 0, 0, 0, 0};
        bArr[1] = new byte[]{65, 73, 0, 0, 0, 0};
        bArr[2] = new byte[]{65, 78, 0, 0, 0, 0};
        bArr[3] = new byte[]{65, 78, 71, 0, 0, 0};
        bArr[4] = new byte[]{65, 79, 0, 0, 0, 0};
        bArr[5] = new byte[]{66, 65, 0, 0, 0, 0};
        bArr[6] = new byte[]{66, 65, 73, 0, 0, 0};
        bArr[7] = new byte[]{66, 65, 78, 0, 0, 0};
        bArr[8] = new byte[]{66, 65, 78, 71, 0, 0};
        bArr[9] = new byte[]{66, 65, 79, 0, 0, 0};
        bArr[10] = new byte[]{66, 69, 73, 0, 0, 0};
        bArr[11] = new byte[]{66, 69, 78, 0, 0, 0};
        bArr[12] = new byte[]{66, 69, 78, 71, 0, 0};
        bArr[13] = new byte[]{66, 73, 0, 0, 0, 0};
        bArr[14] = new byte[]{66, 73, 65, 78, 0, 0};
        bArr[15] = new byte[]{66, 73, 65, 79, 0, 0};
        bArr[16] = new byte[]{66, 73, 69, 0, 0, 0};
        bArr[17] = new byte[]{66, 73, 78, 0, 0, 0};
        bArr[18] = new byte[]{66, 73, 78, 71, 0, 0};
        bArr[19] = new byte[]{66, 79, 0, 0, 0, 0};
        bArr[20] = new byte[]{66, 85, 0, 0, 0, 0};
        bArr[21] = new byte[]{67, 65, 0, 0, 0, 0};
        bArr[22] = new byte[]{67, 65, 73, 0, 0, 0};
        bArr[23] = new byte[]{67, 65, 78, 0, 0, 0};
        bArr[24] = new byte[]{67, 65, 78, 71, 0, 0};
        bArr[25] = new byte[]{67, 65, 79, 0, 0, 0};
        bArr[26] = new byte[]{67, 69, 0, 0, 0, 0};
        bArr[27] = new byte[]{67, 69, 78, 0, 0, 0};
        bArr[28] = new byte[]{67, 69, 78, 71, 0, 0};
        bArr[29] = new byte[]{67, 72, 65, 0, 0, 0};
        bArr[30] = new byte[]{67, 72, 65, 73, 0, 0};
        bArr[31] = new byte[]{67, 72, 65, 78, 0, 0};
        bArr[32] = new byte[]{67, 72, 65, 78, 71, 0};
        bArr[33] = new byte[]{67, 72, 65, 79, 0, 0};
        bArr[34] = new byte[]{67, 72, 69, 0, 0, 0};
        bArr[35] = new byte[]{67, 72, 69, 78, 0, 0};
        bArr[36] = new byte[]{67, 72, 69, 78, 71, 0};
        bArr[37] = new byte[]{67, 72, 73, 0, 0, 0};
        bArr[38] = new byte[]{67, 72, 79, 78, 71, 0};
        bArr[39] = new byte[]{67, 72, 79, 85, 0, 0};
        bArr[40] = new byte[]{67, 72, 85, 0, 0, 0};
        bArr[41] = new byte[]{67, 72, 85, 65, 73, 0};
        bArr[42] = new byte[]{67, 72, 85, 65, 78, 0};
        bArr[43] = new byte[]{67, 72, 85, 65, 78, 71};
        bArr[44] = new byte[]{67, 72, 85, 73, 0, 0};
        bArr[45] = new byte[]{67, 72, 85, 78, 0, 0};
        bArr[46] = new byte[]{67, 72, 85, 79, 0, 0};
        bArr[47] = new byte[]{67, 73, 0, 0, 0, 0};
        bArr[48] = new byte[]{67, 79, 78, 71, 0, 0};
        bArr[49] = new byte[]{67, 79, 85, 0, 0, 0};
        bArr[50] = new byte[]{67, 85, 0, 0, 0, 0};
        bArr[51] = new byte[]{67, 85, 65, 78, 0, 0};
        bArr[52] = new byte[]{67, 85, 73, 0, 0, 0};
        bArr[53] = new byte[]{67, 85, 78, 0, 0, 0};
        bArr[54] = new byte[]{67, 85, 79, 0, 0, 0};
        bArr[55] = new byte[]{68, 65, 0, 0, 0, 0};
        bArr[56] = new byte[]{68, 65, 73, 0, 0, 0};
        bArr[57] = new byte[]{68, 65, 78, 0, 0, 0};
        bArr[58] = new byte[]{68, 65, 78, 71, 0, 0};
        bArr[59] = new byte[]{68, 65, 79, 0, 0, 0};
        bArr[60] = new byte[]{68, 69, 0, 0, 0, 0};
        bArr[61] = new byte[]{68, 69, 78, 0, 0, 0};
        bArr[62] = new byte[]{68, 69, 78, 71, 0, 0};
        bArr[63] = new byte[]{68, 73, 0, 0, 0, 0};
        bArr[64] = new byte[]{68, 73, 65, 0, 0, 0};
        bArr[65] = new byte[]{68, 73, 65, 78, 0, 0};
        bArr[66] = new byte[]{68, 73, 65, 79, 0, 0};
        bArr[67] = new byte[]{68, 73, 69, 0, 0, 0};
        bArr[68] = new byte[]{68, 73, 78, 71, 0, 0};
        bArr[69] = new byte[]{68, 73, 85, 0, 0, 0};
        bArr[70] = new byte[]{68, 79, 78, 71, 0, 0};
        bArr[71] = new byte[]{68, 79, 85, 0, 0, 0};
        bArr[72] = new byte[]{68, 85, 0, 0, 0, 0};
        bArr[73] = new byte[]{68, 85, 65, 78, 0, 0};
        bArr[74] = new byte[]{68, 85, 73, 0, 0, 0};
        bArr[75] = new byte[]{68, 85, 78, 0, 0, 0};
        bArr[76] = new byte[]{68, 85, 79, 0, 0, 0};
        bArr[77] = new byte[]{69, 0, 0, 0, 0, 0};
        bArr[78] = new byte[]{69, 78, 0, 0, 0, 0};
        bArr[79] = new byte[]{69, 78, 71, 0, 0, 0};
        bArr[80] = new byte[]{69, 82, 0, 0, 0, 0};
        bArr[81] = new byte[]{70, 65, 0, 0, 0, 0};
        bArr[82] = new byte[]{70, 65, 78, 0, 0, 0};
        bArr[83] = new byte[]{70, 65, 78, 71, 0, 0};
        bArr[84] = new byte[]{70, 69, 73, 0, 0, 0};
        bArr[85] = new byte[]{70, 69, 78, 0, 0, 0};
        bArr[86] = new byte[]{70, 69, 78, 71, 0, 0};
        bArr[87] = new byte[]{70, 73, 65, 79, 0, 0};
        bArr[88] = new byte[]{70, 79, 0, 0, 0, 0};
        bArr[89] = new byte[]{70, 85, 0, 0, 0, 0};
        bArr[90] = new byte[]{70, 79, 85, 0, 0, 0};
        bArr[91] = new byte[]{70, 85, 0, 0, 0, 0};
        bArr[92] = new byte[]{71, 85, 73, 0, 0, 0};
        bArr[93] = new byte[]{71, 65, 0, 0, 0, 0};
        bArr[94] = new byte[]{71, 65, 73, 0, 0, 0};
        bArr[95] = new byte[]{71, 65, 78, 0, 0, 0};
        bArr[96] = new byte[]{71, 65, 78, 71, 0, 0};
        bArr[97] = new byte[]{71, 65, 79, 0, 0, 0};
        bArr[98] = new byte[]{71, 69, 0, 0, 0, 0};
        bArr[99] = new byte[]{71, 69, 73, 0, 0, 0};
        bArr[100] = new byte[]{71, 69, 78, 0, 0, 0};
        bArr[101] = new byte[]{71, 69, 78, 71, 0, 0};
        bArr[102] = new byte[]{74, 73, 69, 0, 0, 0};
        bArr[103] = new byte[]{71, 69, 0, 0, 0, 0};
        bArr[104] = new byte[]{71, 79, 78, 71, 0, 0};
        bArr[105] = new byte[]{71, 79, 85, 0, 0, 0};
        bArr[106] = new byte[]{71, 85, 0, 0, 0, 0};
        bArr[107] = new byte[]{71, 85, 65, 0, 0, 0};
        bArr[108] = new byte[]{71, 85, 65, 73, 0, 0};
        bArr[109] = new byte[]{71, 85, 65, 78, 0, 0};
        bArr[110] = new byte[]{71, 85, 65, 78, 71, 0};
        bArr[111] = new byte[]{71, 85, 73, 0, 0, 0};
        bArr[112] = new byte[]{71, 85, 78, 0, 0, 0};
        bArr[113] = new byte[]{71, 85, 65, 78, 0, 0};
        bArr[114] = new byte[]{71, 85, 79, 0, 0, 0};
        bArr[115] = new byte[]{72, 65, 0, 0, 0, 0};
        bArr[116] = new byte[]{72, 65, 73, 0, 0, 0};
        bArr[117] = new byte[]{72, 65, 78, 0, 0, 0};
        bArr[118] = new byte[]{72, 65, 78, 71, 0, 0};
        bArr[119] = new byte[]{72, 65, 79, 0, 0, 0};
        bArr[120] = new byte[]{72, 69, 0, 0, 0, 0};
        bArr[121] = new byte[]{72, 69, 73, 0, 0, 0};
        bArr[122] = new byte[]{72, 69, 78, 0, 0, 0};
        bArr[123] = new byte[]{72, 69, 78, 71, 0, 0};
        bArr[124] = new byte[]{72, 79, 78, 71, 0, 0};
        bArr[125] = new byte[]{72, 79, 85, 0, 0, 0};
        bArr[126] = new byte[]{72, 85, 0, 0, 0, 0};
        bArr[127] = new byte[]{72, 85, 65, 0, 0, 0};
        bArr[128] = new byte[]{72, 85, 65, 73, 0, 0};
        bArr[129] = new byte[]{72, 85, 65, 78, 0, 0};
        bArr[130] = new byte[]{72, 85, 65, 78, 71, 0};
        bArr[131] = new byte[]{72, 85, 73, 0, 0, 0};
        bArr[132] = new byte[]{72, 85, 78, 0, 0, 0};
        bArr[133] = new byte[]{72, 85, 79, 0, 0, 0};
        bArr[134] = new byte[]{74, 73, 0, 0, 0, 0};
        bArr[135] = new byte[]{74, 73, 65, 0, 0, 0};
        bArr[136] = new byte[]{74, 73, 65, 78, 0, 0};
        bArr[137] = new byte[]{74, 73, 65, 78, 71, 0};
        bArr[138] = new byte[]{74, 73, 65, 79, 0, 0};
        bArr[139] = new byte[]{74, 73, 69, 0, 0, 0};
        bArr[140] = new byte[]{74, 73, 78, 0, 0, 0};
        bArr[141] = new byte[]{74, 73, 78, 71, 0, 0};
        bArr[142] = new byte[]{74, 73, 79, 78, 71, 0};
        bArr[143] = new byte[]{74, 73, 85, 0, 0, 0};
        bArr[144] = new byte[]{74, 85, 0, 0, 0, 0};
        bArr[145] = new byte[]{74, 85, 65, 78, 0, 0};
        bArr[146] = new byte[]{74, 85, 69, 0, 0, 0};
        bArr[147] = new byte[]{74, 85, 78, 0, 0, 0};
        bArr[148] = new byte[]{75, 65, 0, 0, 0, 0};
        bArr[149] = new byte[]{75, 65, 73, 0, 0, 0};
        bArr[150] = new byte[]{75, 65, 78, 0, 0, 0};
        bArr[151] = new byte[]{75, 65, 78, 71, 0, 0};
        bArr[152] = new byte[]{75, 65, 79, 0, 0, 0};
        bArr[153] = new byte[]{75, 69, 0, 0, 0, 0};
        bArr[154] = new byte[]{75, 69, 78, 0, 0, 0};
        bArr[155] = new byte[]{75, 69, 78, 71, 0, 0};
        bArr[156] = new byte[]{75, 79, 78, 71, 0, 0};
        bArr[157] = new byte[]{75, 79, 85, 0, 0, 0};
        bArr[158] = new byte[]{75, 85, 0, 0, 0, 0};
        bArr[159] = new byte[]{75, 85, 65, 0, 0, 0};
        bArr[160] = new byte[]{75, 85, 65, 73, 0, 0};
        bArr[161] = new byte[]{75, 85, 65, 78, 0, 0};
        bArr[162] = new byte[]{75, 85, 65, 78, 71, 0};
        bArr[163] = new byte[]{75, 85, 73, 0, 0, 0};
        bArr[164] = new byte[]{75, 85, 78, 0, 0, 0};
        bArr[165] = new byte[]{75, 85, 79, 0, 0, 0};
        bArr[166] = new byte[]{76, 65, 0, 0, 0, 0};
        bArr[167] = new byte[]{76, 65, 73, 0, 0, 0};
        bArr[168] = new byte[]{76, 65, 78, 0, 0, 0};
        bArr[169] = new byte[]{76, 65, 78, 71, 0, 0};
        bArr[170] = new byte[]{76, 65, 79, 0, 0, 0};
        bArr[171] = new byte[]{76, 69, 0, 0, 0, 0};
        bArr[172] = new byte[]{76, 69, 73, 0, 0, 0};
        bArr[173] = new byte[]{76, 73, 0, 0, 0, 0};
        bArr[174] = new byte[]{76, 73, 78, 71, 0, 0};
        bArr[175] = new byte[]{76, 69, 78, 71, 0, 0};
        bArr[176] = new byte[]{76, 73, 0, 0, 0, 0};
        bArr[177] = new byte[]{76, 73, 65, 0, 0, 0};
        bArr[178] = new byte[]{76, 73, 65, 78, 0, 0};
        bArr[179] = new byte[]{76, 73, 65, 78, 71, 0};
        bArr[180] = new byte[]{76, 73, 65, 79, 0, 0};
        bArr[181] = new byte[]{76, 73, 69, 0, 0, 0};
        bArr[182] = new byte[]{76, 73, 78, 0, 0, 0};
        bArr[183] = new byte[]{76, 73, 78, 71, 0, 0};
        bArr[184] = new byte[]{76, 73, 85, 0, 0, 0};
        bArr[185] = new byte[]{76, 79, 78, 71, 0, 0};
        bArr[186] = new byte[]{76, 79, 85, 0, 0, 0};
        bArr[187] = new byte[]{76, 85, 0, 0, 0, 0};
        bArr[188] = new byte[]{76, 85, 65, 78, 0, 0};
        bArr[189] = new byte[]{76, 85, 78, 0, 0, 0};
        bArr[190] = new byte[]{76, 85, 79, 0, 0, 0};
        bArr[191] = new byte[]{77, 65, 0, 0, 0, 0};
        bArr[192] = new byte[]{77, 65, 73, 0, 0, 0};
        bArr[193] = new byte[]{77, 65, 78, 0, 0, 0};
        bArr[194] = new byte[]{77, 65, 78, 71, 0, 0};
        bArr[195] = new byte[]{77, 65, 79, 0, 0, 0};
        bArr[196] = new byte[]{77, 69, 73, 0, 0, 0};
        bArr[197] = new byte[]{77, 69, 78, 0, 0, 0};
        bArr[198] = new byte[]{77, 69, 78, 71, 0, 0};
        bArr[199] = new byte[]{77, 73, 0, 0, 0, 0};
        bArr[200] = new byte[]{77, 73, 65, 78, 0, 0};
        bArr[201] = new byte[]{77, 73, 65, 79, 0, 0};
        bArr[202] = new byte[]{77, 73, 69, 0, 0, 0};
        bArr[203] = new byte[]{77, 73, 78, 0, 0, 0};
        bArr[204] = new byte[]{77, 73, 78, 71, 0, 0};
        bArr[205] = new byte[]{77, 73, 85, 0, 0, 0};
        bArr[206] = new byte[]{77, 79, 0, 0, 0, 0};
        bArr[207] = new byte[]{77, 79, 85, 0, 0, 0};
        bArr[208] = new byte[]{77, 85, 0, 0, 0, 0};
        bArr[209] = new byte[]{78, 65, 0, 0, 0, 0};
        bArr[210] = new byte[]{78, 65, 73, 0, 0, 0};
        bArr[211] = new byte[]{78, 65, 78, 0, 0, 0};
        bArr[212] = new byte[]{78, 65, 78, 71, 0, 0};
        bArr[213] = new byte[]{78, 65, 79, 0, 0, 0};
        bArr[214] = new byte[]{78, 69, 0, 0, 0, 0};
        bArr[215] = new byte[]{78, 69, 73, 0, 0, 0};
        bArr[216] = new byte[]{78, 69, 78, 0, 0, 0};
        bArr[217] = new byte[]{78, 69, 78, 71, 0, 0};
        bArr[218] = new byte[]{78, 73, 0, 0, 0, 0};
        bArr[219] = new byte[]{78, 73, 65, 78, 0, 0};
        bArr[220] = new byte[]{78, 73, 65, 78, 71, 0};
        bArr[221] = new byte[]{78, 73, 65, 79, 0, 0};
        bArr[222] = new byte[]{78, 73, 69, 0, 0, 0};
        bArr[223] = new byte[]{78, 73, 78, 0, 0, 0};
        bArr[224] = new byte[]{78, 73, 78, 71, 0, 0};
        bArr[225] = new byte[]{78, 73, 85, 0, 0, 0};
        bArr[226] = new byte[]{78, 79, 78, 71, 0, 0};
        bArr[227] = new byte[]{78, 79, 85, 0, 0, 0};
        bArr[228] = new byte[]{78, 85, 0, 0, 0, 0};
        bArr[229] = new byte[]{78, 85, 65, 78, 0, 0};
        bArr[230] = new byte[]{78, 85, 78, 0, 0, 0};
        bArr[231] = new byte[]{78, 85, 79, 0, 0, 0};
        bArr[232] = new byte[]{79, 0, 0, 0, 0, 0};
        bArr[233] = new byte[]{79, 85, 0, 0, 0, 0};
        bArr[234] = new byte[]{80, 65, 0, 0, 0, 0};
        bArr[235] = new byte[]{80, 65, 73, 0, 0, 0};
        bArr[236] = new byte[]{80, 65, 78, 0, 0, 0};
        bArr[237] = new byte[]{80, 65, 78, 71, 0, 0};
        bArr[238] = new byte[]{80, 65, 79, 0, 0, 0};
        bArr[239] = new byte[]{80, 69, 73, 0, 0, 0};
        bArr[240] = new byte[]{80, 69, 78, 0, 0, 0};
        bArr[241] = new byte[]{80, 69, 78, 71, 0, 0};
        bArr[242] = new byte[]{80, 73, 0, 0, 0, 0};
        bArr[243] = new byte[]{80, 73, 65, 78, 0, 0};
        bArr[244] = new byte[]{80, 73, 65, 79, 0, 0};
        bArr[245] = new byte[]{80, 73, 69, 0, 0, 0};
        bArr[246] = new byte[]{80, 73, 78, 0, 0, 0};
        bArr[247] = new byte[]{80, 73, 78, 71, 0, 0};
        bArr[248] = new byte[]{80, 79, 0, 0, 0, 0};
        bArr[249] = new byte[]{80, 79, 85, 0, 0, 0};
        bArr[250] = new byte[]{80, 85, 0, 0, 0, 0};
        bArr[251] = new byte[]{81, 73, 0, 0, 0, 0};
        bArr[252] = new byte[]{81, 73, 65, 0, 0, 0};
        bArr[253] = new byte[]{81, 73, 65, 78, 0, 0};
        bArr[254] = new byte[]{81, 73, 65, 78, 71, 0};
        bArr[255] = new byte[]{81, 73, 65, 79, 0, 0};
        bArr[256] = new byte[]{81, 73, 69, 0, 0, 0};
        bArr[257] = new byte[]{81, 73, 78, 0, 0, 0};
        bArr[258] = new byte[]{81, 73, 78, 71, 0, 0};
        bArr[259] = new byte[]{81, 73, 79, 78, 71, 0};
        bArr[260] = new byte[]{81, 73, 85, 0, 0, 0};
        bArr[261] = new byte[]{81, 85, 0, 0, 0, 0};
        bArr[262] = new byte[]{81, 85, 65, 78, 0, 0};
        bArr[263] = new byte[]{81, 85, 69, 0, 0, 0};
        bArr[264] = new byte[]{81, 85, 78, 0, 0, 0};
        bArr[265] = new byte[]{82, 65, 78, 0, 0, 0};
        bArr[266] = new byte[]{82, 65, 78, 71, 0, 0};
        bArr[267] = new byte[]{82, 65, 79, 0, 0, 0};
        bArr[268] = new byte[]{82, 69, 0, 0, 0, 0};
        bArr[269] = new byte[]{82, 69, 78, 0, 0, 0};
        bArr[270] = new byte[]{82, 69, 78, 71, 0, 0};
        bArr[271] = new byte[]{82, 73, 0, 0, 0, 0};
        bArr[272] = new byte[]{82, 79, 78, 71, 0, 0};
        bArr[273] = new byte[]{82, 79, 85, 0, 0, 0};
        bArr[274] = new byte[]{82, 85, 0, 0, 0, 0};
        bArr[275] = new byte[]{82, 85, 65, 78, 0, 0};
        bArr[276] = new byte[]{82, 85, 73, 0, 0, 0};
        bArr[277] = new byte[]{82, 85, 78, 0, 0, 0};
        bArr[278] = new byte[]{82, 85, 79, 0, 0, 0};
        bArr[279] = new byte[]{83, 65, 0, 0, 0, 0};
        bArr[280] = new byte[]{83, 65, 73, 0, 0, 0};
        bArr[281] = new byte[]{83, 65, 78, 0, 0, 0};
        bArr[282] = new byte[]{83, 65, 78, 71, 0, 0};
        bArr[283] = new byte[]{83, 65, 79, 0, 0, 0};
        bArr[284] = new byte[]{83, 69, 0, 0, 0, 0};
        bArr[285] = new byte[]{83, 69, 78, 0, 0, 0};
        bArr[286] = new byte[]{83, 69, 78, 71, 0, 0};
        bArr[287] = new byte[]{83, 72, 65, 0, 0, 0};
        bArr[288] = new byte[]{83, 72, 65, 73, 0, 0};
        bArr[289] = new byte[]{83, 72, 65, 78, 0, 0};
        bArr[290] = new byte[]{83, 72, 65, 78, 71, 0};
        bArr[291] = new byte[]{83, 72, 65, 79, 0, 0};
        bArr[292] = new byte[]{83, 72, 69, 0, 0, 0};
        bArr[293] = new byte[]{83, 72, 69, 78, 0, 0};
        bArr[294] = new byte[]{83, 72, 69, 78, 71, 0};
        bArr[295] = new byte[]{83, 72, 73, 0, 0, 0};
        bArr[296] = new byte[]{83, 72, 79, 85, 0, 0};
        bArr[297] = new byte[]{83, 72, 85, 0, 0, 0};
        bArr[298] = new byte[]{83, 72, 85, 65, 0, 0};
        bArr[299] = new byte[]{83, 72, 85, 65, 73, 0};
        bArr[300] = new byte[]{83, 72, 85, 65, 78, 0};
        bArr[301] = new byte[]{83, 72, 85, 65, 78, 71};
        bArr[302] = new byte[]{83, 72, 85, 73, 0, 0};
        bArr[303] = new byte[]{83, 72, 85, 78, 0, 0};
        bArr[304] = new byte[]{83, 72, 85, 79, 0, 0};
        bArr[305] = new byte[]{83, 73, 0, 0, 0, 0};
        bArr[306] = new byte[]{83, 79, 78, 71, 0, 0};
        bArr[307] = new byte[]{83, 79, 85, 0, 0, 0};
        bArr[308] = new byte[]{83, 85, 0, 0, 0, 0};
        bArr[309] = new byte[]{83, 85, 65, 78, 0, 0};
        bArr[310] = new byte[]{83, 85, 73, 0, 0, 0};
        bArr[311] = new byte[]{83, 85, 78, 0, 0, 0};
        bArr[312] = new byte[]{83, 85, 79, 0, 0, 0};
        bArr[313] = new byte[]{84, 65, 0, 0, 0, 0};
        bArr[314] = new byte[]{84, 65, 73, 0, 0, 0};
        bArr[315] = new byte[]{84, 65, 78, 0, 0, 0};
        bArr[316] = new byte[]{84, 65, 78, 71, 0, 0};
        bArr[317] = new byte[]{84, 65, 79, 0, 0, 0};
        bArr[318] = new byte[]{84, 69, 0, 0, 0, 0};
        bArr[319] = new byte[]{84, 69, 78, 71, 0, 0};
        bArr[320] = new byte[]{84, 73, 0, 0, 0, 0};
        bArr[321] = new byte[]{84, 73, 65, 78, 0, 0};
        bArr[322] = new byte[]{84, 73, 65, 79, 0, 0};
        bArr[323] = new byte[]{84, 73, 69, 0, 0, 0};
        bArr[324] = new byte[]{84, 73, 78, 71, 0, 0};
        bArr[325] = new byte[]{84, 79, 78, 71, 0, 0};
        bArr[326] = new byte[]{84, 79, 85, 0, 0, 0};
        bArr[327] = new byte[]{84, 85, 0, 0, 0, 0};
        bArr[328] = new byte[]{84, 85, 65, 78, 0, 0};
        bArr[329] = new byte[]{84, 85, 73, 0, 0, 0};
        bArr[330] = new byte[]{84, 85, 78, 0, 0, 0};
        bArr[331] = new byte[]{84, 85, 79, 0, 0, 0};
        bArr[332] = new byte[]{87, 65, 0, 0, 0, 0};
        bArr[333] = new byte[]{87, 65, 73, 0, 0, 0};
        bArr[334] = new byte[]{87, 65, 78, 0, 0, 0};
        bArr[335] = new byte[]{87, 65, 78, 71, 0, 0};
        bArr[336] = new byte[]{87, 69, 73, 0, 0, 0};
        bArr[337] = new byte[]{87, 69, 78, 0, 0, 0};
        bArr[338] = new byte[]{87, 69, 78, 71, 0, 0};
        bArr[339] = new byte[]{87, 79, 0, 0, 0, 0};
        bArr[340] = new byte[]{87, 85, 0, 0, 0, 0};
        bArr[341] = new byte[]{88, 73, 0, 0, 0, 0};
        bArr[342] = new byte[]{88, 73, 65, 0, 0, 0};
        bArr[343] = new byte[]{88, 73, 65, 78, 0, 0};
        bArr[344] = new byte[]{88, 73, 65, 78, 71, 0};
        bArr[345] = new byte[]{88, 73, 65, 79, 0, 0};
        bArr[346] = new byte[]{88, 73, 69, 0, 0, 0};
        bArr[347] = new byte[]{88, 73, 78, 0, 0, 0};
        bArr[348] = new byte[]{88, 73, 78, 71, 0, 0};
        bArr[349] = new byte[]{88, 73, 79, 78, 71, 0};
        bArr[350] = new byte[]{88, 73, 85, 0, 0, 0};
        bArr[351] = new byte[]{88, 85, 0, 0, 0, 0};
        bArr[352] = new byte[]{88, 85, 65, 78, 0, 0};
        bArr[353] = new byte[]{88, 85, 69, 0, 0, 0};
        bArr[354] = new byte[]{88, 85, 78, 0, 0, 0};
        bArr[355] = new byte[]{89, 65, 0, 0, 0, 0};
        bArr[356] = new byte[]{89, 65, 78, 0, 0, 0};
        bArr[357] = new byte[]{89, 65, 78, 71, 0, 0};
        bArr[358] = new byte[]{89, 65, 79, 0, 0, 0};
        bArr[359] = new byte[]{89, 69, 0, 0, 0, 0};
        bArr[360] = new byte[]{89, 73, 0, 0, 0, 0};
        bArr[361] = new byte[]{89, 73, 78, 0, 0, 0};
        bArr[362] = new byte[]{89, 73, 78, 71, 0, 0};
        bArr[363] = new byte[]{89, 79, 0, 0, 0, 0};
        bArr[364] = new byte[]{89, 79, 78, 71, 0, 0};
        bArr[365] = new byte[]{89, 79, 85, 0, 0, 0};
        bArr[366] = new byte[]{89, 85, 0, 0, 0, 0};
        bArr[367] = new byte[]{89, 85, 65, 78, 0, 0};
        bArr[368] = new byte[]{89, 85, 69, 0, 0, 0};
        bArr[369] = new byte[]{89, 85, 78, 0, 0, 0};
        bArr[370] = new byte[]{90, 65, 0, 0, 0, 0};
        bArr[371] = new byte[]{90, 65, 73, 0, 0, 0};
        bArr[372] = new byte[]{90, 65, 78, 0, 0, 0};
        bArr[373] = new byte[]{90, 65, 78, 71, 0, 0};
        bArr[374] = new byte[]{90, 65, 79, 0, 0, 0};
        bArr[375] = new byte[]{90, 69, 0, 0, 0, 0};
        bArr[376] = new byte[]{90, 69, 73, 0, 0, 0};
        bArr[377] = new byte[]{90, 69, 78, 0, 0, 0};
        bArr[378] = new byte[]{90, 69, 78, 71, 0, 0};
        bArr[379] = new byte[]{90, 72, 65, 0, 0, 0};
        bArr[380] = new byte[]{90, 72, 65, 73, 0, 0};
        bArr[381] = new byte[]{90, 72, 65, 78, 0, 0};
        bArr[382] = new byte[]{90, 72, 65, 78, 71, 0};
        bArr[383] = new byte[]{90, 72, 65, 79, 0, 0};
        bArr[384] = new byte[]{90, 72, 69, 0, 0, 0};
        bArr[385] = new byte[]{90, 72, 69, 78, 0, 0};
        bArr[386] = new byte[]{90, 72, 69, 78, 71, 0};
        bArr[387] = new byte[]{90, 72, 73, 0, 0, 0};
        bArr[388] = new byte[]{90, 72, 79, 78, 71, 0};
        bArr[389] = new byte[]{90, 72, 79, 85, 0, 0};
        bArr[390] = new byte[]{90, 72, 85, 0, 0, 0};
        bArr[391] = new byte[]{90, 72, 85, 65, 0, 0};
        bArr[392] = new byte[]{90, 72, 85, 65, 73, 0};
        bArr[393] = new byte[]{90, 72, 85, 65, 78, 0};
        bArr[394] = new byte[]{90, 72, 85, 65, 78, 71};
        bArr[395] = new byte[]{90, 72, 85, 73, 0, 0};
        bArr[396] = new byte[]{90, 72, 85, 78, 0, 0};
        bArr[397] = new byte[]{90, 72, 85, 79, 0, 0};
        bArr[398] = new byte[]{90, 73, 0, 0, 0, 0};
        bArr[399] = new byte[]{90, 79, 78, 71, 0, 0};
        bArr[400] = new byte[]{90, 79, 85, 0, 0, 0};
        bArr[401] = new byte[]{90, 85, 0, 0, 0, 0};
        bArr[402] = new byte[]{90, 85, 65, 78, 0, 0};
        bArr[403] = new byte[]{90, 85, 73, 0, 0, 0};
        bArr[404] = new byte[]{90, 85, 78, 0, 0, 0};
        bArr[405] = new byte[]{90, 85, 79, 0, 0, 0};
        PINYINS = bArr;
    }

    public static class Token {
        public static final int LATIN = 1;
        public static final int PINYIN = 2;
        public static final String SEPARATOR = " ";
        public static final int UNKNOWN = 3;
        public String source;
        public String target;
        public int type;

        public Token() {
        }

        public Token(int type2, String source2, String target2) {
            this.type = type2;
            this.source = source2;
            this.target = target2;
        }
    }

    protected BXHanzi2Pinyin(boolean hasChinaCollator) {
        this.mHasChinaCollator = hasChinaCollator;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        android.util.Log.w(com.baixing.util.BXHanzi2Pinyin.TAG, "There is no Chinese collator, HanziToPinyin is disabled");
        com.baixing.util.BXHanzi2Pinyin.sInstance = new com.baixing.util.BXHanzi2Pinyin(false);
        r2 = com.baixing.util.BXHanzi2Pinyin.sInstance;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.baixing.util.BXHanzi2Pinyin getInstance() {
        /*
            java.lang.Class<com.baixing.util.BXHanzi2Pinyin> r3 = com.baixing.util.BXHanzi2Pinyin.class
            monitor-enter(r3)
            com.baixing.util.BXHanzi2Pinyin r2 = com.baixing.util.BXHanzi2Pinyin.sInstance     // Catch:{ all -> 0x0029 }
            if (r2 == 0) goto L_0x000b
            com.baixing.util.BXHanzi2Pinyin r2 = com.baixing.util.BXHanzi2Pinyin.sInstance     // Catch:{ all -> 0x0029 }
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
        L_0x000a:
            return r2
        L_0x000b:
            java.util.Locale[] r1 = java.text.Collator.getAvailableLocales()     // Catch:{ all -> 0x0029 }
            r0 = 0
        L_0x0010:
            int r2 = r1.length     // Catch:{ all -> 0x0029 }
            if (r0 >= r2) goto L_0x002f
            r2 = r1[r0]     // Catch:{ all -> 0x0029 }
            java.util.Locale r4 = java.util.Locale.CHINA     // Catch:{ all -> 0x0029 }
            boolean r2 = r2.equals(r4)     // Catch:{ all -> 0x0029 }
            if (r2 == 0) goto L_0x002c
            com.baixing.util.BXHanzi2Pinyin r2 = new com.baixing.util.BXHanzi2Pinyin     // Catch:{ all -> 0x0029 }
            r4 = 1
            r2.<init>(r4)     // Catch:{ all -> 0x0029 }
            com.baixing.util.BXHanzi2Pinyin.sInstance = r2     // Catch:{ all -> 0x0029 }
            com.baixing.util.BXHanzi2Pinyin r2 = com.baixing.util.BXHanzi2Pinyin.sInstance     // Catch:{ all -> 0x0029 }
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
            goto L_0x000a
        L_0x0029:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
            throw r2
        L_0x002c:
            int r0 = r0 + 1
            goto L_0x0010
        L_0x002f:
            java.lang.String r2 = "HanziToPinyin"
            java.lang.String r4 = "There is no Chinese collator, HanziToPinyin is disabled"
            android.util.Log.w(r2, r4)     // Catch:{ all -> 0x0029 }
            com.baixing.util.BXHanzi2Pinyin r2 = new com.baixing.util.BXHanzi2Pinyin     // Catch:{ all -> 0x0029 }
            r4 = 0
            r2.<init>(r4)     // Catch:{ all -> 0x0029 }
            com.baixing.util.BXHanzi2Pinyin.sInstance = r2     // Catch:{ all -> 0x0029 }
            com.baixing.util.BXHanzi2Pinyin r2 = com.baixing.util.BXHanzi2Pinyin.sInstance     // Catch:{ all -> 0x0029 }
            monitor-exit(r3)     // Catch:{ all -> 0x0029 }
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.BXHanzi2Pinyin.getInstance():com.baixing.util.BXHanzi2Pinyin");
    }

    private Token getToken(char character) {
        Token token = new Token();
        String letter = Character.toString(character);
        token.source = letter;
        int offset = -1;
        if (character < 256) {
            token.type = 1;
            token.target = letter;
        } else if (character < 13312) {
            token.type = 3;
            token.target = letter;
        } else {
            int cmp = COLLATOR.compare(letter, FIRST_PINYIN_UNIHAN);
            if (cmp < 0) {
                token.type = 3;
                token.target = letter;
            } else {
                if (cmp == 0) {
                    token.type = 2;
                    offset = 0;
                } else {
                    cmp = COLLATOR.compare(letter, LAST_PINYIN_UNIHAN);
                    if (cmp > 0) {
                        token.type = 3;
                        token.target = letter;
                    } else if (cmp == 0) {
                        token.type = 2;
                        offset = UNIHANS.length - 1;
                    }
                }
                token.type = 2;
                if (offset < 0) {
                    int begin = 0;
                    int end = UNIHANS.length - 1;
                    while (begin <= end) {
                        offset = (begin + end) / 2;
                        cmp = COLLATOR.compare(letter, Character.toString(UNIHANS[offset]));
                        if (cmp == 0) {
                            break;
                        } else if (cmp > 0) {
                            begin = offset + 1;
                        } else {
                            end = offset - 1;
                        }
                    }
                }
                if (cmp < 0) {
                    offset--;
                }
                StringBuilder pinyin = new StringBuilder();
                int j = 0;
                while (j < PINYINS[offset].length && PINYINS[offset][j] != 0) {
                    pinyin.append((char) PINYINS[offset][j]);
                    j++;
                }
                token.target = pinyin.toString();
            }
        }
        return token;
    }

    public ArrayList<Token> get(String input) {
        ArrayList<Token> tokens = new ArrayList<>();
        if (this.mHasChinaCollator && !TextUtils.isEmpty(input)) {
            int inputLength = input.length();
            StringBuilder sb = new StringBuilder();
            int tokenType = 1;
            for (int i = 0; i < inputLength; i++) {
                char character = input.charAt(i);
                if (character == ' ') {
                    if (sb.length() > 0) {
                        addToken(sb, tokens, tokenType);
                    }
                } else if (character < 256) {
                    if (tokenType != 1 && sb.length() > 0) {
                        addToken(sb, tokens, tokenType);
                    }
                    tokenType = 1;
                    sb.append(character);
                } else if (character < 13312) {
                    if (tokenType != 3 && sb.length() > 0) {
                        addToken(sb, tokens, tokenType);
                    }
                    tokenType = 3;
                    sb.append(character);
                } else {
                    Token t = getToken(character);
                    if (t.type == 2) {
                        if (sb.length() > 0) {
                            addToken(sb, tokens, tokenType);
                        }
                        tokens.add(t);
                        tokenType = 2;
                    } else {
                        if (tokenType != t.type && sb.length() > 0) {
                            addToken(sb, tokens, tokenType);
                        }
                        tokenType = t.type;
                        sb.append(character);
                    }
                }
            }
            if (sb.length() > 0) {
                addToken(sb, tokens, tokenType);
            }
        }
        return tokens;
    }

    private void addToken(StringBuilder sb, ArrayList<Token> tokens, int tokenType) {
        String str = sb.toString();
        tokens.add(new Token(tokenType, str, str));
        sb.setLength(0);
    }

    public static String hanziToPinyin(String hanzi) {
        String s = "";
        ArrayList<Token> tokens = getInstance().get(hanzi);
        for (int i = 0; i < tokens.size(); i++) {
            s = s + tokens.get(i).target;
        }
        return s.toLowerCase();
    }
}
