package com.baixing.util;

import android.app.ProgressDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginUtil implements View.OnClickListener {
    /* access modifiers changed from: private */
    public LoginListener listener;
    /* access modifiers changed from: private */
    public ProgressDialog pd;
    private View view;

    public interface LoginListener {
        void onForgetClicked();

        void onLoginFail(String str);

        void onLoginSucceed(String str);

        void onRegisterClicked();
    }

    public LoginUtil(View v, LoginListener listener2) {
        this.view = v;
        this.listener = listener2;
        if (this.view != null) {
            View login = this.view.findViewById(R.id.btn_login);
            if (login != null) {
                login.setOnClickListener(this);
            }
            Button forgetBtn = (Button) this.view.findViewById(R.id.loginForgetPwdBtn);
            if (forgetBtn != null) {
                forgetBtn.setOnClickListener(this);
            }
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_login) {
            String account = ((TextView) this.view.findViewById(R.id.et_account)).getText().toString();
            String password = ((TextView) this.view.findViewById(R.id.et_password)).getText().toString();
            if (check(account, password)) {
                ((InputMethodManager) this.view.getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.view.getWindowToken(), 0);
                this.pd = ProgressDialog.show(this.view.getContext(), "提示", "请稍候...");
                this.pd.setCancelable(true);
                this.pd.show();
                doLogin(account, password);
            }
        } else if (v.getId() == R.id.loginForgetPwdBtn && this.listener != null) {
            this.listener.onForgetClicked();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
     candidates:
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
    private boolean check(String account, String password) {
        if (account == null || account.trim().equals("")) {
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LOGIN_SUBMIT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, "account is empty!").end();
            ViewUtil.showToast(this.view.getContext(), "账号不能为空！", false);
            return false;
        } else if (password != null && !password.trim().equals("")) {
            return true;
        } else {
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LOGIN_SUBMIT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, "password is empty!").end();
            ViewUtil.showToast(this.view.getContext(), "密码不能为空！", false);
            return false;
        }
    }

    public static void parseLoginResponse(String json_response, LoginListener listener2) {
        try {
            JSONObject jsonResult = new JSONObject(json_response).getJSONObject("result");
            JSONObject jsonUser = jsonResult.getJSONObject("user");
            String id = jsonUser.getString(LocaleUtil.INDONESIAN);
            String token = jsonResult.getString("token");
            if (!id.equals("")) {
                UserBean user = new UserBean();
                user.setId(id);
                user.setPhone(jsonUser.getString("mobile"));
                Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "user", user);
                GlobalDataManager.getInstance().setPhoneNumber(user.getPhone());
                GlobalDataManager.getInstance().setLoginUserToken(token);
                Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "loginToken", token);
                BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_LOGIN, user);
                if (listener2 != null) {
                    listener2.onLoginSucceed("登陆成功");
                }
            } else if (listener2 != null) {
                listener2.onLoginFail("登陆失败");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (listener2 != null) {
                listener2.onLoginFail("登陆失败");
            }
        }
    }

    private void doLogin(String mobile, String password) {
        ApiParams params = new ApiParams();
        params.addParam("identity", mobile);
        params.addParam("type", "mobile");
        params.addParam("password", password.trim());
        params.addParam("expire", "60");
        BaseApiCommand.createCommand("user.login/", true, params).execute(GlobalDataManager.getInstance().getApplicationContext(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                if (LoginUtil.this.pd != null) {
                    LoginUtil.this.pd.dismiss();
                }
                if (LoginUtil.this.listener != null) {
                    LoginUtil.this.listener.onLoginFail((error == null || TextUtils.isEmpty(error.getMsg())) ? "登录未成功，请稍后重试！" : error.getMsg());
                }
            }

            public void onNetworkDone(String apiName, String responseData) {
                if (responseData != null) {
                    LoginUtil.parseLoginResponse(responseData, LoginUtil.this.listener);
                    if (LoginUtil.this.pd != null) {
                        LoginUtil.this.pd.dismiss();
                    }
                    FavoriteNetworkUtil.syncFavorites(GlobalDataManager.getInstance().getApplicationContext(), GlobalDataManager.getInstance().getAccountManager().getCurrentUser());
                }
            }
        });
    }
}
