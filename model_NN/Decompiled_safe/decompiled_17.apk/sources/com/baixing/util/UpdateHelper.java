package com.baixing.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.baixing.data.GlobalDataManager;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.message.RMsgInfoDB;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateHelper {
    /* access modifiers changed from: private */
    public static UpdateHelper ourInstance = new UpdateHelper();
    private final int MSG_DOWNLOAD_APP = 1;
    private final int MSG_HAS_NEW_VERSION = 4;
    private final int MSG_INSTALL_APP = 3;
    private final int MSG_NETWORK_ERROR = 0;
    Context activity = null;
    /* access modifiers changed from: private */
    public String apkUrl = null;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (UpdateHelper.this.activity != null) {
                UpdateHelper.ourInstance.handleMessage(msg);
            }
        }
    };
    ProgressDialog pd = null;
    /* access modifiers changed from: private */
    public String serverVersion = null;

    public static UpdateHelper getInstance() {
        return ourInstance;
    }

    private UpdateHelper() {
    }

    /* access modifiers changed from: private */
    public void sendMessage(int msgCode, Object obj) {
        this.handler.sendMessage(this.handler.obtainMessage(msgCode, obj));
    }

    /* access modifiers changed from: private */
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                ViewUtil.showToast(this.activity, msg.obj.toString(), false);
                break;
            case 1:
                updateAppDownload();
                break;
            case 3:
                updateAppInstall();
                break;
            case 4:
                new AlertDialog.Builder(this.activity).setTitle("检查更新").setMessage("当前版本: " + GlobalDataManager.getInstance().getVersion() + "\n发现新版本: " + this.serverVersion + "\n是否更新？").setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        UpdateHelper.this.sendMessage(1, UpdateHelper.this.apkUrl);
                    }
                }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).create().show();
                break;
        }
        if (this.pd != null) {
            this.pd.hide();
        }
    }

    public void checkNewVersion(Context currentActivity) {
        this.activity = currentActivity;
        ApiParams params = new ApiParams();
        params.addParam("clientVersion", GlobalDataManager.getInstance().getVersion());
        this.pd = ProgressDialog.show(this.activity, "提示", "请稍候...");
        this.pd.show();
        BaseApiCommand.createCommand("check_version", true, params).execute(GlobalDataManager.getInstance().getApplicationContext(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                UpdateHelper.this.sendMessage(0, "网络异常");
            }

            public void onNetworkDone(String apiName, String responseData) {
                try {
                    JSONObject respond = new JSONObject(responseData);
                    JSONObject error = respond.getJSONObject("error");
                    String unused = UpdateHelper.this.serverVersion = respond.getString("serverVersion");
                    String unused2 = UpdateHelper.this.apkUrl = respond.getString("apkUrl");
                    if (!"0".equals(error.getString("code"))) {
                        UpdateHelper.this.sendMessage(0, error.getString(RMsgInfoDB.TABLE));
                    } else if (respond.getBoolean("hasNew")) {
                        UpdateHelper.this.sendMessage(4, null);
                    } else {
                        UpdateHelper.this.sendMessage(0, "已经安装最新版本");
                    }
                } catch (JSONException e) {
                    UpdateHelper.this.sendMessage(0, "网络异常");
                }
            }
        });
    }

    private void updateAppDownload() {
        Intent updateIntent = new Intent(this.activity, BXUpdateService.class);
        updateIntent.putExtra("titleId", (int) R.string.app_name);
        updateIntent.putExtra("apkUrl", this.apkUrl);
        this.activity.startService(updateIntent);
    }

    private void updateAppInstall() {
    }
}
