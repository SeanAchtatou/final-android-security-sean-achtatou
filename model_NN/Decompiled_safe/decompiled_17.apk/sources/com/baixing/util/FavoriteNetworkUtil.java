package com.baixing.util;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.AdList;
import com.baixing.entity.UserBean;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;

public class FavoriteNetworkUtil {
    public static final int MSG_ADD_FAVORITE_FAIL = 1431633924;
    public static final int MSG_ADD_FAVORITE_SUCCESS = 1431633923;
    public static final int MSG_CANCEL_FAVORITE_FAIL = 1431633922;
    public static final int MSG_CANCEL_FAVORITE_SUCCESS = 1431633921;

    public static class ReplyData {
        public String id;
        public String message;

        public ReplyData(String id2, String message2) {
            this.id = id2;
            this.message = message2;
        }
    }

    public static void addFavorite(Context ctx, final String ids, UserBean user, final Handler outHandler) {
        ApiParams params = new ApiParams();
        params.addParam("adIds", ids);
        params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        BaseApiCommand.createCommand("favorite.addIds/", true, params).execute(ctx, new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                Message msg = Message.obtain();
                msg.what = FavoriteNetworkUtil.MSG_ADD_FAVORITE_FAIL;
                msg.obj = "收藏失败，请稍后重试";
                if (outHandler != null) {
                    outHandler.sendMessage(msg);
                }
            }

            public void onNetworkDone(String apiName, String responseData) {
                Message msg = Message.obtain();
                msg.obj = new ReplyData(ids, "收藏成功");
                msg.what = FavoriteNetworkUtil.MSG_ADD_FAVORITE_SUCCESS;
                if (outHandler != null) {
                    outHandler.sendMessage(msg);
                }
            }
        });
    }

    public static void cancelFavorite(Context ctx, final String id, UserBean user, final Handler outHandler) {
        ApiParams params = new ApiParams();
        params.addParam("adId", id);
        params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        BaseApiCommand.createCommand("favorite.remove/", true, params).execute(ctx, new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                Message msg = Message.obtain();
                msg.what = FavoriteNetworkUtil.MSG_CANCEL_FAVORITE_FAIL;
                msg.obj = "取消收藏失败，请稍后重试";
                outHandler.sendMessage(msg);
            }

            public void onNetworkDone(String apiName, String responseData) {
                Message msg = Message.obtain();
                msg.obj = new ReplyData(id, "取消收藏成功");
                msg.what = FavoriteNetworkUtil.MSG_CANCEL_FAVORITE_SUCCESS;
                outHandler.sendMessage(msg);
            }
        });
    }

    public static void retreiveFavorites(Context ctx, UserBean user) {
        ApiParams params = new ApiParams();
        params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        BaseApiCommand.createCommand("Favorite.get/", false, params).execute(ctx, new BaseApiCommand.Callback() {
            public void onNetworkDone(String apiName, String responseData) {
                AdList gl = JsonUtil.getGoodsListFromJson(responseData);
                if (gl != null) {
                    GlobalDataManager.getInstance().updateFav(gl.getData());
                }
            }

            public void onNetworkFail(String apiName, ApiError error) {
            }
        });
    }

    public static void syncFavorites(final Context ctx, UserBean user) {
        Ad[] listStore = (Ad[]) Util.loadDataFromLocate(ctx, "listMyStore", Ad[].class);
        if (listStore != null && listStore.length != 0 && user != null && !TextUtils.isEmpty(user.getPhone())) {
            String ids = "";
            for (int i = 0; i < listStore.length; i++) {
                ids = ids + listStore[i].getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID);
                if (i != listStore.length - 1) {
                    ids = ids + ",";
                }
            }
            addFavorite(ctx, ids, user, new Handler(ctx.getMainLooper()) {
                public void handleMessage(Message msg) {
                    if (msg.what == 1431633923) {
                        Util.deleteDataFromLocate(ctx, "listMyStore");
                    }
                }
            });
        }
    }
}
