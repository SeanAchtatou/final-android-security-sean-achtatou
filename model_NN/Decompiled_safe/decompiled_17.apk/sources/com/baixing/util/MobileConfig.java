package com.baixing.util;

import android.content.Context;
import android.util.Log;
import com.baixing.data.GlobalDataManager;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.network.api.FileUploadCommand;
import com.baixing.util.PerformEvent;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

public class MobileConfig {
    private static MobileConfig instance = null;
    /* access modifiers changed from: private */
    public JSONObject json = null;

    public static MobileConfig getInstance() {
        if (instance == null) {
            instance = new MobileConfig();
        }
        return instance;
    }

    public void reset() {
        Context context = GlobalDataManager.getInstance().getApplicationContext();
        String jsonString = (String) Util.loadJsonAndTimestampFromLocate(context, "mobile_config").second;
        if (jsonString == null || jsonString.length() == 0) {
            try {
                InputStream input = context.getAssets().open("mobile_config.txt");
                byte[] b = new byte[input.available()];
                input.read(b);
                jsonString = new String(b);
            } catch (IOException e) {
                jsonString = null;
            }
        }
        if (jsonString != null && jsonString.length() > 0) {
            try {
                this.json = new JSONObject(jsonString);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    private MobileConfig() {
        reset();
    }

    public boolean isEnableTracker() {
        try {
            return this.json.getBoolean("trackFlag");
        } catch (JSONException e) {
            return true;
        }
    }

    public boolean isUseUmengUpdate() {
        try {
            return this.json.getBoolean("umengUpdateFlag");
        } catch (JSONException e) {
            return false;
        }
    }

    public long getCityTimestamp() {
        try {
            return this.json.getLong("cityTimestamp");
        } catch (JSONException e) {
            return 0;
        }
    }

    public long getCategoryTimestamp() {
        try {
            return this.json.getLong("categoryTimestamp");
        } catch (JSONException e) {
            return 0;
        }
    }

    public boolean hasNewVersion() {
        try {
            if (Version.compare(this.json.getString("serverVersion"), GlobalDataManager.getInstance().getVersion()) == 1) {
                return true;
            }
            return false;
        } catch (JSONException e) {
            return false;
        }
    }

    public FileUploadCommand.ImageConfig getImageConfig() {
        FileUploadCommand.ImageConfig ic = new FileUploadCommand.ImageConfig();
        try {
            JSONObject img = this.json.getJSONObject("imageConfig");
            ic.server = img.getString("server");
            JSONObject param = img.getJSONObject("params");
            ic.policy = param.getString("policy");
            ic.signature = param.getString("signature");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ic;
    }

    public void syncMobileConfig() {
        new Thread(new UpdateMobileConfigThread()).start();
    }

    class UpdateMobileConfigThread implements Runnable {
        UpdateMobileConfigThread() {
        }

        public void run() {
            ApiParams params = new ApiParams();
            params.addParam("type", "android");
            if ((System.currentTimeMillis() / 1000) - ((Long) Util.loadJsonAndTimestampFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "mobile_config").first).longValue() <= TextUtil.FULL_HOUR) {
                PerformanceTracker.stamp(PerformEvent.Event.E_LoadConfig_Less_24);
                return;
            }
            try {
                String content = BaseApiCommand.createCommand("Mobile.config/", true, params).executeSync(GlobalDataManager.getInstance().getApplicationContext());
                if (content != null && content.length() > 0) {
                    JSONObject unused = MobileConfig.this.json = new JSONObject(content).getJSONObject("result");
                    Util.saveJsonAndTimestampToLocate(GlobalDataManager.getInstance().getApplicationContext(), "mobile_config", MobileConfig.this.json.toString(), System.currentTimeMillis() / 1000);
                    MobileConfig.getInstance().reset();
                }
            } catch (Throwable t) {
                Log.e("QLM", "get mobile config faild. caused by " + t.getMessage());
            } finally {
                BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_CONFIGURATION_UPDATE, null);
            }
            PerformanceTracker.stamp(PerformEvent.Event.E_Leave_MobileConfigThread);
        }
    }
}
