package com.baixing.util;

import com.baixing.util.PerformEvent;
import java.util.ArrayList;

public class PerformanceTracker {
    private static final int MAX_RECORDS = 50;
    private static PerformanceTracker instance;
    private ArrayList<String> records = new ArrayList<>();

    private PerformanceTracker() {
    }

    public static PerformanceTracker getInstance() {
        if (instance == null) {
            instance = new PerformanceTracker();
        }
        return instance;
    }

    private void record(String record) {
    }

    public static void flush() {
    }

    public static void stamp(PerformEvent.Event event) {
    }
}
