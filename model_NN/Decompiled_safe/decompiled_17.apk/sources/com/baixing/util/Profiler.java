package com.baixing.util;

import android.util.Log;
import java.util.Enumeration;
import java.util.Hashtable;

public class Profiler {
    private static Hashtable<String, TimeRange> eventMapper = new Hashtable<>();

    static class TimeRange {
        /* access modifiers changed from: private */
        public long end;
        /* access modifiers changed from: private */
        public long start;

        TimeRange() {
        }

        public long duration() {
            return this.end - this.start;
        }
    }

    public static void markStart(String tag) {
        if (!eventMapper.containsKey(tag)) {
            eventMapper.put(tag, new TimeRange());
        }
        long unused = eventMapper.get(tag).start = System.currentTimeMillis();
    }

    public static void markEnd(String tag) {
        if (eventMapper.containsKey(tag)) {
            long unused = eventMapper.get(tag).end = System.currentTimeMillis();
        }
    }

    public static void clear() {
        eventMapper.clear();
    }

    public static void remove(String event) {
        eventMapper.remove(event);
    }

    public static String dump() {
        StringBuffer buf = new StringBuffer();
        Enumeration<String> keys = eventMapper.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            buf.append(key).append(":").append(eventMapper.get(key).duration()).append(" | ");
        }
        Log.d("PROGILE", buf.toString());
        return buf.toString();
    }
}
