package com.baixing.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import com.baixing.entity.BXThumbnail;
import com.google.zxing.pdf417.PDF417Common;
import com.tencent.mm.sdk.platformtools.Util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.httpclient.HttpState;

public class BitmapUtils {
    public static final int DEFAULT_THUMBNAIL_HEIGHT = 200;
    public static final int DEFAULT_THUMBNAIL_WIDTH = 200;
    public static final String TAG = "BitmapUtils";

    private BitmapUtils() {
    }

    public static void disableConnectionReuseIfNecessary() {
        if (hasHttpConnectionBug()) {
            System.setProperty("http.keepAlive", HttpState.PREEMPTIVE_DEFAULT);
        }
    }

    @SuppressLint({"NewApi"})
    public static int getBitmapSize(Bitmap bitmap) {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    public static int getMemoryClass(Context context) {
        return ((ActivityManager) context.getSystemService("activity")).getMemoryClass();
    }

    public static boolean hasHttpConnectionBug() {
        return Build.VERSION.SDK_INT < 8;
    }

    public static boolean hasExternalCacheDir() {
        return Build.VERSION.SDK_INT >= 8;
    }

    public static Bitmap createThumbnail(String path, int thunbWidth, int thumbHeight) {
        Bitmap srcBmp = getBitmap(path);
        if (srcBmp == null) {
            return null;
        }
        Bitmap thumbnail = Bitmap.createScaledBitmap(srcBmp, thunbWidth, thumbHeight, true);
        if (thumbnail == srcBmp) {
            return thumbnail;
        }
        srcBmp.recycle();
        return thumbnail;
    }

    private static Bitmap getBitmap(String path) {
        if (path != null) {
            try {
                BitmapFactory.Options bfo = new BitmapFactory.Options();
                bfo.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(path, bfo);
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inPurgeable = true;
                o.inSampleSize = getClosestResampleSize(bfo.outWidth, bfo.outHeight, 600);
                return BitmapFactory.decodeFile(path, o);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static int getClosestResampleSize(int cx, int cy, int maxDim) {
        int resample;
        int max = Math.max(cx, cy);
        int resample2 = 1;
        while (true) {
            if (resample2 >= Integer.MAX_VALUE) {
                break;
            } else if (resample2 * maxDim > max) {
                resample2--;
                break;
            } else {
                resample2++;
            }
        }
        if (resample2 <= 1) {
            return 1;
        }
        if (resample2 < 4) {
            resample = 2;
        } else if (resample2 < 8) {
            resample = 4;
        } else {
            resample = 8;
        }
        return resample;
    }

    public static String getRealPathFromURI(Activity context, Uri contentUri) {
        Cursor cursor = context.managedQuery(contentUri, new String[]{"_data"}, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private static File getOutputMediaFile() {
        File dirF = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/bx/");
        dirF.mkdirs();
        return new File(dirF, "bx_" + System.currentTimeMillis() + Util.PHOTO_DEFAULT_EXT);
    }

    public static final BXThumbnail copyAndCreateThrmbnail(String sourceFile, Context context) {
        String savedPath = getOutputMediaFile().getAbsolutePath();
        if (savedPath == null) {
            return null;
        }
        Bitmap source = null;
        try {
            FileOutputStream fos = new FileOutputStream(savedPath);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPurgeable = true;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(sourceFile, options);
            options.inJustDecodeBounds = false;
            options.inSampleSize = getClosestResampleSize(options.outWidth, options.outHeight, 600);
            source = BitmapFactory.decodeFile(sourceFile, options);
            source.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
            if (source != null) {
            }
        } catch (Throwable th) {
            if (source != null) {
            }
            throw th;
        }
        if (source == null) {
            return null;
        }
        try {
            Bitmap bp = Bitmap.createScaledBitmap(source, 200, 200, true);
            source.recycle();
            return BXThumbnail.createThumbnail(savedPath, bp);
        } catch (Throwable th2) {
            return BXThumbnail.createThumbnail(savedPath, source);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static final BXThumbnail saveAndCreateThumbnail(byte[] data, int rotation, Context context, boolean isMirror) {
        String savedPath = getOutputMediaFile().getAbsolutePath();
        if (savedPath == null) {
            return null;
        }
        Bitmap source = null;
        try {
            FileOutputStream fos = new FileOutputStream(savedPath);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPurgeable = true;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, options);
            options.inJustDecodeBounds = false;
            options.inSampleSize = getClosestResampleSize(options.outWidth, options.outHeight, 600);
            source = BitmapFactory.decodeByteArray(data, 0, data.length, options);
            source.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e2) {
            Log.d(TAG, "Error accessing file: " + e2.getMessage());
        } catch (Throwable t) {
            Log.d(TAG, "other exception when processing image." + t.getMessage());
        }
        if (source == null) {
            return null;
        }
        new BitmapFactory.Options().inPurgeable = true;
        try {
            Matrix m = new Matrix();
            if (isMirror) {
                m.setValues(new float[]{-1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f});
            }
            m.postRotate((float) rotation);
            m.postScale(200.0f / ((float) source.getWidth()), 200.0f / ((float) source.getHeight()));
            BXThumbnail createThumbnail = BXThumbnail.createThumbnail(savedPath, Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), m, false));
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(savedPath);
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            if (exif == null) {
                return createThumbnail;
            }
            exif.setAttribute("Orientation", getExifOrientation(rotation));
            try {
                exif.saveAttributes();
                return createThumbnail;
            } catch (IOException e4) {
                return createThumbnail;
            }
        } catch (Throwable th) {
            ExifInterface exif2 = null;
            try {
                exif2 = new ExifInterface(savedPath);
            } catch (IOException e5) {
                e5.printStackTrace();
            }
            if (exif2 != null) {
                exif2.setAttribute("Orientation", getExifOrientation(rotation));
                try {
                    exif2.saveAttributes();
                } catch (IOException e6) {
                }
            }
            throw th;
        }
    }

    private static String getExifOrientation(int degree) {
        switch (degree) {
            case 0:
                return "1";
            case PDF417Common.MAX_ROWS_IN_BARCODE /*90*/:
                return "6";
            case 180:
                return "3";
            case 270:
                return "8";
            default:
                return "0";
        }
    }
}
