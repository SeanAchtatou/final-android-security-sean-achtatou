package com.baixing.util;

public class Version {
    public static int compare(String v1, String v2) {
        int k1;
        int k2;
        if (v1.equals(v2)) {
            return 0;
        }
        if (!v1.matches("[0-9]+(\\.[0-9]+)*") || !v2.matches("[0-9]+(\\.[0-9]+)*")) {
            return -2;
        }
        String[] vArr1 = v1.split("\\.");
        String[] vArr2 = v2.split("\\.");
        for (int i = 0; i < Math.max(vArr1.length, vArr2.length); i++) {
            if (vArr1.length > i) {
                k1 = Integer.valueOf(vArr1[i]).intValue();
            } else {
                k1 = 0;
            }
            if (vArr2.length > i) {
                k2 = Integer.valueOf(vArr2[i]).intValue();
            } else {
                k2 = 0;
            }
            if (k1 != k2) {
                return k1 < k2 ? -1 : 1;
            }
        }
        return 0;
    }
}
