package com.baixing.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Pair;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tencent.mm.algorithm.Base64;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class Util {
    private static String DEVICE_ID = "";
    private static String PREF_DEVICE_ID = "bx_share_deviceID";
    private static String PREF_KEY_DEVICE_ID = "pref_key_device";
    public static final String TAG = "QLM";
    private static String[] keys;
    private static ObjectMapper mapper = new ObjectMapper();
    public static String qq_access_key = "";
    public static String qq_access_secret = "";
    private static String[] values;

    public static boolean isLoggable() {
        return new File(Environment.getExternalStorageDirectory() + "/baixing_debug_log_crl.dat").exists();
    }

    private static String getSdCardRoot() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        return path.endsWith(CookieSpec.PATH_DELIM) ? path : path + CookieSpec.PATH_DELIM;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x007b A[SYNTHETIC, Splitter:B:24:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0090 A[SYNTHETIC, Splitter:B:34:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a0 A[SYNTHETIC, Splitter:B:41:0x00a0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String saveDataToSdCard(java.lang.String r9, java.lang.String r10, byte[] r11, boolean r12) {
        /*
            r5 = 0
            r2 = 0
            java.lang.String r7 = android.os.Environment.getExternalStorageState()
            if (r7 == 0) goto L_0x006a
            java.lang.String r6 = getSdCardRoot()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.io.File r4 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            r4.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.String r8 = "/"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.StringBuilder r7 = r7.append(r10)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.String r8 = ".txt"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            r1.<init>(r7)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            boolean r7 = r4.exists()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            if (r7 != 0) goto L_0x0051
            r4.mkdir()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
        L_0x0051:
            boolean r7 = r1.exists()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            if (r7 != 0) goto L_0x005a
            r1.createNewFile()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
        L_0x005a:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            r3.<init>(r1, r12)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x0088 }
            r3.write(r11)     // Catch:{ FileNotFoundException -> 0x00b3, IOException -> 0x00b0, all -> 0x00ad }
            if (r3 == 0) goto L_0x0067
            r3.close()     // Catch:{ IOException -> 0x006b }
        L_0x0067:
            java.lang.String r5 = "保存成功"
            r2 = r3
        L_0x006a:
            return r5
        L_0x006b:
            r0 = move-exception
            java.lang.String r5 = "没有数据"
            r0.printStackTrace()
            r2 = r3
            goto L_0x006a
        L_0x0073:
            r0 = move-exception
        L_0x0074:
            java.lang.String r5 = "没有找到文件"
            r0.printStackTrace()     // Catch:{ all -> 0x009d }
            if (r2 == 0) goto L_0x007e
            r2.close()     // Catch:{ IOException -> 0x0081 }
        L_0x007e:
            java.lang.String r5 = "保存成功"
            goto L_0x006a
        L_0x0081:
            r0 = move-exception
            java.lang.String r5 = "没有数据"
            r0.printStackTrace()
            goto L_0x006a
        L_0x0088:
            r0 = move-exception
        L_0x0089:
            java.lang.String r5 = "没有数据"
            r0.printStackTrace()     // Catch:{ all -> 0x009d }
            if (r2 == 0) goto L_0x0093
            r2.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0093:
            java.lang.String r5 = "保存成功"
            goto L_0x006a
        L_0x0096:
            r0 = move-exception
            java.lang.String r5 = "没有数据"
            r0.printStackTrace()
            goto L_0x006a
        L_0x009d:
            r7 = move-exception
        L_0x009e:
            if (r2 == 0) goto L_0x00a3
            r2.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x00a3:
            java.lang.String r5 = "保存成功"
        L_0x00a5:
            throw r7
        L_0x00a6:
            r0 = move-exception
            java.lang.String r5 = "没有数据"
            r0.printStackTrace()
            goto L_0x00a5
        L_0x00ad:
            r7 = move-exception
            r2 = r3
            goto L_0x009e
        L_0x00b0:
            r0 = move-exception
            r2 = r3
            goto L_0x0089
        L_0x00b3:
            r0 = move-exception
            r2 = r3
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.Util.saveDataToSdCard(java.lang.String, java.lang.String, byte[], boolean):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0050 A[SYNTHETIC, Splitter:B:17:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0061 A[SYNTHETIC, Splitter:B:26:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006e A[SYNTHETIC, Splitter:B:32:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] loadDataFromSdCard(java.lang.String r6, java.lang.String r7) {
        /*
            r0 = 0
            r2 = 0
            java.lang.String r4 = android.os.Environment.getExternalStorageState()
            if (r4 == 0) goto L_0x0041
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.String r5 = getSdCardRoot()     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.String r5 = ".txt"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            r3.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0049, IOException -> 0x005a }
            int r4 = r3.available()     // Catch:{ FileNotFoundException -> 0x007e, IOException -> 0x007b, all -> 0x0078 }
            byte[] r0 = new byte[r4]     // Catch:{ FileNotFoundException -> 0x007e, IOException -> 0x007b, all -> 0x0078 }
            r3.read(r0)     // Catch:{ FileNotFoundException -> 0x007e, IOException -> 0x007b, all -> 0x0078 }
            if (r3 == 0) goto L_0x0040
            r3.close()     // Catch:{ Exception -> 0x0042 }
        L_0x0040:
            r2 = r3
        L_0x0041:
            return r0
        L_0x0042:
            r1 = move-exception
            r0 = 0
            r1.printStackTrace()
            r2 = r3
            goto L_0x0041
        L_0x0049:
            r1 = move-exception
        L_0x004a:
            r0 = 0
            r1.printStackTrace()     // Catch:{ all -> 0x006b }
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ Exception -> 0x0054 }
            goto L_0x0041
        L_0x0054:
            r1 = move-exception
            r0 = 0
            r1.printStackTrace()
            goto L_0x0041
        L_0x005a:
            r1 = move-exception
        L_0x005b:
            r0 = 0
            r1.printStackTrace()     // Catch:{ all -> 0x006b }
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ Exception -> 0x0065 }
            goto L_0x0041
        L_0x0065:
            r1 = move-exception
            r0 = 0
            r1.printStackTrace()
            goto L_0x0041
        L_0x006b:
            r4 = move-exception
        L_0x006c:
            if (r2 == 0) goto L_0x0071
            r2.close()     // Catch:{ Exception -> 0x0072 }
        L_0x0071:
            throw r4
        L_0x0072:
            r1 = move-exception
            r0 = 0
            r1.printStackTrace()
            goto L_0x0071
        L_0x0078:
            r4 = move-exception
            r2 = r3
            goto L_0x006c
        L_0x007b:
            r1 = move-exception
            r2 = r3
            goto L_0x005b
        L_0x007e:
            r1 = move-exception
            r2 = r3
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.Util.loadDataFromSdCard(java.lang.String, java.lang.String):byte[]");
    }

    public static void saveDataToLocateDelay(final Context context, final String file, final Object obj) {
        new Thread(new Runnable() {
            public void run() {
                Util.saveDataToLocate(context, file, obj);
            }
        }).start();
    }

    public static String saveDataToLocate(Context context, String file, Object object) {
        if (!(file == null || file.equals("") || file.charAt(0) == '_')) {
            file = "_" + file;
        }
        if (object == null) {
            context.deleteFile(file);
            return "保存成功";
        }
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput(file, 0);
            mapper.writer().writeValue(fos, object);
            if (fos == null) {
                return "保存成功";
            }
            try {
                fos.close();
                return "保存成功";
            } catch (IOException e) {
                e.printStackTrace();
                return "保存成功";
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            if (fos == null) {
                return "没有找到文件";
            }
            try {
                fos.close();
                return "没有找到文件";
            } catch (IOException e3) {
                e3.printStackTrace();
                return "没有找到文件";
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            if (fos == null) {
                return "没有数据";
            }
            try {
                fos.close();
                return "没有数据";
            } catch (IOException e5) {
                e5.printStackTrace();
                return "没有数据";
            }
        } catch (NullPointerException e6) {
            e6.printStackTrace();
            if (fos == null) {
                return "null pointer";
            }
            try {
                fos.close();
                return "null pointer";
            } catch (IOException e7) {
                e7.printStackTrace();
                return "null pointer";
            }
        } catch (Throwable th) {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e8) {
                    e8.printStackTrace();
                }
            }
            throw th;
        }
    }

    public static List<String> listFiles(Context context, String dir) {
        String dirPath;
        List<String> list = new ArrayList<>();
        String dirPath2 = context.getFilesDir().getAbsolutePath();
        if (dir.startsWith(File.separator)) {
            dirPath = dirPath2 + dir;
        } else {
            dirPath = dirPath2 + File.separator + dir;
        }
        File dirF = new File(dirPath);
        if (dirF.exists() && dirF.isDirectory()) {
            for (File f : dirF.listFiles()) {
                list.add(f.getAbsolutePath());
            }
        }
        return list;
    }

    public static byte[] loadData(Context context, String fileName) {
        if (context == null || fileName == null) {
            return null;
        }
        try {
            return loadBytes(context.openFileInput(fileName));
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    private static byte[] loadBytes(FileInputStream ins) {
        if (ins == null) {
            return null;
        }
        try {
            byte[] data = new byte[ins.available()];
            ins.read(data);
            if (ins == null) {
                return data;
            }
            try {
                ins.close();
                return data;
            } catch (Throwable th) {
                return data;
            }
        } catch (Throwable th2) {
        }
        throw th;
        return null;
    }

    public static byte[] loadData(String absolutePath) {
        if (absolutePath == null) {
            return null;
        }
        File f = new File(absolutePath);
        if (!f.exists() || f.isDirectory()) {
            return null;
        }
        try {
            return loadBytes(new FileInputStream(f));
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0038 A[SYNTHETIC, Splitter:B:23:0x0038] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object loadSerializable(java.lang.String r5) {
        /*
            r3 = 0
            if (r5 != 0) goto L_0x0004
        L_0x0003:
            return r3
        L_0x0004:
            java.io.File r0 = new java.io.File
            r0.<init>(r5)
            boolean r4 = r0.exists()
            if (r4 == 0) goto L_0x0003
            boolean r4 = r0.isDirectory()
            if (r4 != 0) goto L_0x0003
            r1 = 0
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Throwable -> 0x002c, all -> 0x0035 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x002c, all -> 0x0035 }
            r4.<init>(r0)     // Catch:{ Throwable -> 0x002c, all -> 0x0035 }
            r2.<init>(r4)     // Catch:{ Throwable -> 0x002c, all -> 0x0035 }
            java.lang.Object r3 = r2.readObject()     // Catch:{ Throwable -> 0x0041, all -> 0x003e }
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ Throwable -> 0x002a }
            goto L_0x0003
        L_0x002a:
            r4 = move-exception
            goto L_0x0003
        L_0x002c:
            r4 = move-exception
        L_0x002d:
            if (r1 == 0) goto L_0x0003
            r1.close()     // Catch:{ Throwable -> 0x0033 }
            goto L_0x0003
        L_0x0033:
            r4 = move-exception
            goto L_0x0003
        L_0x0035:
            r3 = move-exception
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Throwable -> 0x003c }
        L_0x003b:
            throw r3
        L_0x003c:
            r4 = move-exception
            goto L_0x003b
        L_0x003e:
            r3 = move-exception
            r1 = r2
            goto L_0x0036
        L_0x0041:
            r4 = move-exception
            r1 = r2
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.Util.loadSerializable(java.lang.String):java.lang.Object");
    }

    public static String saveDataToFile(Context context, String dir, String file, byte[] data) {
        return saveDataToFile(context, dir, file, data, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ac A[SYNTHETIC, Splitter:B:31:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b8 A[SYNTHETIC, Splitter:B:36:0x00b8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String saveDataToFile(android.content.Context r8, java.lang.String r9, java.lang.String r10, byte[] r11, boolean r12) {
        /*
            r5 = 0
            if (r10 == 0) goto L_0x000a
            if (r11 == 0) goto L_0x000a
            int r6 = r11.length
            if (r6 == 0) goto L_0x000a
            if (r8 != 0) goto L_0x000c
        L_0x000a:
            r2 = r5
        L_0x000b:
            return r2
        L_0x000c:
            java.io.File r6 = r8.getFilesDir()
            java.lang.String r1 = r6.getAbsolutePath()
            if (r9 == 0) goto L_0x0057
            java.lang.String r6 = java.io.File.separator
            boolean r6 = r9.startsWith(r6)
            if (r6 == 0) goto L_0x003f
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.StringBuilder r6 = r6.append(r9)
            java.lang.String r1 = r6.toString()
        L_0x002f:
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            r0.mkdirs()
            boolean r6 = r0.exists()
            if (r6 != 0) goto L_0x0057
            r2 = r5
            goto L_0x000b
        L_0x003f:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.String r7 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r9)
            java.lang.String r1 = r6.toString()
            goto L_0x002f
        L_0x0057:
            java.lang.String r6 = java.io.File.separator
            boolean r6 = r1.endsWith(r6)
            if (r6 == 0) goto L_0x0091
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r2 = r6.toString()
        L_0x0070:
            r3 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            java.io.File r6 = new java.io.File     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            r6.<init>(r2)     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            r4.<init>(r6, r12)     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            r4.write(r11)     // Catch:{ Throwable -> 0x00c6, all -> 0x00c1 }
            r4.flush()     // Catch:{ Throwable -> 0x00c6, all -> 0x00c1 }
            r4.close()     // Catch:{ Throwable -> 0x00c6, all -> 0x00c1 }
            if (r4 == 0) goto L_0x000b
            r4.flush()     // Catch:{ Throwable -> 0x008e }
            r4.close()     // Catch:{ Throwable -> 0x008e }
            goto L_0x000b
        L_0x008e:
            r5 = move-exception
            goto L_0x000b
        L_0x0091:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.String r7 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r2 = r6.toString()
            goto L_0x0070
        L_0x00a9:
            r6 = move-exception
        L_0x00aa:
            if (r3 == 0) goto L_0x00b2
            r3.flush()     // Catch:{ Throwable -> 0x00c4 }
            r3.close()     // Catch:{ Throwable -> 0x00c4 }
        L_0x00b2:
            r2 = r5
            goto L_0x000b
        L_0x00b5:
            r5 = move-exception
        L_0x00b6:
            if (r3 == 0) goto L_0x00be
            r3.flush()     // Catch:{ Throwable -> 0x00bf }
            r3.close()     // Catch:{ Throwable -> 0x00bf }
        L_0x00be:
            throw r5
        L_0x00bf:
            r6 = move-exception
            goto L_0x00be
        L_0x00c1:
            r5 = move-exception
            r3 = r4
            goto L_0x00b6
        L_0x00c4:
            r6 = move-exception
            goto L_0x00b2
        L_0x00c6:
            r6 = move-exception
            r3 = r4
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.Util.saveDataToFile(android.content.Context, java.lang.String, java.lang.String, byte[], boolean):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ac A[SYNTHETIC, Splitter:B:27:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b8 A[SYNTHETIC, Splitter:B:32:0x00b8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String saveSerializableToPath(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.io.Serializable r11) {
        /*
            r5 = 0
            if (r10 == 0) goto L_0x0007
            if (r11 == 0) goto L_0x0007
            if (r8 != 0) goto L_0x0009
        L_0x0007:
            r2 = r5
        L_0x0008:
            return r2
        L_0x0009:
            java.io.File r6 = r8.getFilesDir()
            java.lang.String r1 = r6.getAbsolutePath()
            java.lang.String r6 = java.io.File.separator
            boolean r6 = r9.startsWith(r6)
            if (r6 == 0) goto L_0x003a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.StringBuilder r6 = r6.append(r9)
            java.lang.String r1 = r6.toString()
        L_0x002a:
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            r0.mkdirs()
            boolean r6 = r0.exists()
            if (r6 != 0) goto L_0x0052
            r2 = r5
            goto L_0x0008
        L_0x003a:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.String r7 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r9)
            java.lang.String r1 = r6.toString()
            goto L_0x002a
        L_0x0052:
            java.lang.String r6 = java.io.File.separator
            boolean r6 = r1.endsWith(r6)
            if (r6 == 0) goto L_0x0091
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r2 = r6.toString()
        L_0x006b:
            r3 = 0
            java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            java.io.File r7 = new java.io.File     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            r7.<init>(r2)     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            r6.<init>(r7)     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            r4.<init>(r6)     // Catch:{ Throwable -> 0x00a9, all -> 0x00b5 }
            r4.writeObject(r11)     // Catch:{ Throwable -> 0x00c6, all -> 0x00c1 }
            r4.flush()     // Catch:{ Throwable -> 0x00c6, all -> 0x00c1 }
            r4.close()     // Catch:{ Throwable -> 0x00c6, all -> 0x00c1 }
            if (r4 == 0) goto L_0x0008
            r4.flush()     // Catch:{ Throwable -> 0x008e }
            r4.close()     // Catch:{ Throwable -> 0x008e }
            goto L_0x0008
        L_0x008e:
            r5 = move-exception
            goto L_0x0008
        L_0x0091:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.String r7 = java.io.File.separator
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r2 = r6.toString()
            goto L_0x006b
        L_0x00a9:
            r6 = move-exception
        L_0x00aa:
            if (r3 == 0) goto L_0x00b2
            r3.flush()     // Catch:{ Throwable -> 0x00c4 }
            r3.close()     // Catch:{ Throwable -> 0x00c4 }
        L_0x00b2:
            r2 = r5
            goto L_0x0008
        L_0x00b5:
            r5 = move-exception
        L_0x00b6:
            if (r3 == 0) goto L_0x00be
            r3.flush()     // Catch:{ Throwable -> 0x00bf }
            r3.close()     // Catch:{ Throwable -> 0x00bf }
        L_0x00be:
            throw r5
        L_0x00bf:
            r6 = move-exception
            goto L_0x00be
        L_0x00c1:
            r5 = move-exception
            r3 = r4
            goto L_0x00b6
        L_0x00c4:
            r6 = move-exception
            goto L_0x00b2
        L_0x00c6:
            r6 = move-exception
            r3 = r4
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.Util.saveSerializableToPath(android.content.Context, java.lang.String, java.lang.String, java.io.Serializable):java.lang.String");
    }

    public static void clearFile(String absolutePath) {
        new File(absolutePath).delete();
    }

    public static String saveJsonAndTimestampToLocate(Context context, String file, String json, long timestamp) {
        if (json == null) {
            return "data invalid";
        }
        String json2 = json.trim();
        if ((!json2.startsWith("[") || !json2.endsWith("]")) && (!json2.startsWith("{") || !json2.endsWith("}"))) {
            return "data invalid";
        }
        saveDataToFile(context, null, file, String.format("%d,%s", Long.valueOf(timestamp), json2).getBytes());
        return "保存成功";
    }

    public static Pair<Long, String> loadJsonAndTimestampFromLocate(Context context, String filename) {
        int index;
        byte[] bytes = loadData(context, filename);
        String s = bytes == null ? null : new String(bytes);
        if (!(s == null || s.length() <= 0 || (index = s.indexOf(44)) == -1)) {
            try {
                return new Pair<>(Long.valueOf(Long.parseLong(s.substring(0, index))), s.substring(index + 1));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return new Pair<>(0L, "");
    }

    public static Pair<Long, String> loadDataAndTimestampFromAssets(Context context, String filename) {
        int index;
        InputStream is = null;
        Pair<Long, String> pair = new Pair<>(0L, "");
        try {
            InputStream is2 = context.getAssets().open(filename);
            byte[] b = new byte[is2.available()];
            is2.read(b);
            String s = new String(b);
            if (!(s == null || s.length() <= 0 || (index = s.indexOf(44)) == -1)) {
                pair = new Pair<>(Long.valueOf(Long.parseLong(s.substring(0, index))), s.substring(index + 1));
            }
            if (is2 != null) {
                try {
                    is2.close();
                } catch (IOException e) {
                }
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e3) {
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
        return pair;
    }

    private static ObjectMapper getDefaultMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        return mapper;
    }

    public static void deleteDataFromLocate(Context context, String file) {
        if (!(file == null || file.equals("") || file.charAt(0) == '_')) {
            file = "_" + file;
        }
        context.deleteFile(file);
    }

    public static Object loadDataFromLocate(Context context, String file, Class clsName) {
        if (!(file == null || file.equals("") || file.charAt(0) == '_')) {
            file = "_" + file;
        }
        FileInputStream fis = null;
        try {
            fis = context.openFileInput(file);
            Object obj = mapper.reader(clsName).readValue(fis);
            if (fis == null) {
                return obj;
            }
            try {
                fis.close();
                return obj;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException e2) {
            if (fis == null) {
                return null;
            }
            try {
                fis.close();
                return null;
            } catch (Exception e3) {
                e3.printStackTrace();
                return null;
            }
        } catch (IOException e4) {
            if (fis == null) {
                return null;
            }
            try {
                fis.close();
                return null;
            } catch (Exception e5) {
                e5.printStackTrace();
                return null;
            }
        } catch (Throwable th) {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e6) {
                    e6.printStackTrace();
                }
            }
            throw th;
        }
    }

    public String[] getKeys() {
        return keys;
    }

    public static void setKeys(Object... args) {
        keys = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            keys[i] = (String) args[i];
        }
    }

    public static String[] getValues() {
        return values;
    }

    public static void setValues(Object... args) {
        values = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            values[i] = (String) args[i];
        }
    }

    public static boolean isExternalStorageWriteable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return "mounted".equals(state) || "mounted_ro".equals(state);
    }

    public static int getWidthByContext(Context context) {
        return ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int getHeightByContext(Context context) {
        return ((Activity) context).getWindowManager().getDefaultDisplay().getHeight();
    }

    public static String GetAddr(double latitude, double longitude) {
        String addr = "";
        try {
            try {
                URLConnection httpsConn = new URL(String.format("http://ditu.google.cn/maps/geo?output=csv&key=abcdef&q=%s,%s", Double.valueOf(latitude), Double.valueOf(longitude))).openConnection();
                if (httpsConn != null) {
                    InputStreamReader insr = new InputStreamReader(httpsConn.getInputStream(), "UTF-8");
                    String data = new BufferedReader(insr).readLine();
                    if (data != null) {
                        String[] retList = data.split(",");
                        if (retList.length <= 2 || !"200".equals(retList[0])) {
                            addr = "";
                        } else {
                            addr = retList[2].replace(CookieSpec.PATH_DELIM, "");
                        }
                    }
                    insr.close();
                }
                return addr;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String extractUrlWithoutSecret(String url) {
        int index2;
        int index22;
        int index23;
        if (url == null || url.equals("")) {
            return null;
        }
        int index1 = url.indexOf("access_token=");
        if (index1 >= 0 && (index23 = url.indexOf("&", index1)) > index1) {
            url = url.replace(url.substring(index1, index23 + 1), "");
        }
        int index12 = url.indexOf("timestamp=");
        if (index12 >= 0 && (index22 = url.indexOf("&", index12)) > index12) {
            url = url.replace(url.substring(index12, index22 + 1), "");
        }
        int index13 = url.indexOf("adIds=");
        if (index13 < 0 || (index2 = url.indexOf("&", index13)) <= index13) {
            return url;
        }
        return url.replace(url.substring(index13, index2 + 1), "");
    }

    public static void logout() {
        List<Ad> ads = GlobalDataManager.getInstance().getListMyStore();
        if (ads != null) {
            ads.clear();
        }
        deleteDataFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "user");
        deleteDataFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "userProfile");
        deleteDataFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "loginToken");
        GlobalDataManager.getInstance().getAccountManager().logout();
        BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_LOGOUT, null);
        GlobalDataManager.getInstance().loadPersonalSync();
    }

    public static boolean isPushAlreadyThere(Context ctx, String pushCode) {
        byte[] objCode;
        boolean z = true;
        if (ctx == null) {
            return true;
        }
        if (pushCode == null || pushCode.equals("") || (objCode = loadData(ctx, "pushCode")) == null) {
            return false;
        }
        try {
            if (Integer.valueOf(pushCode).intValue() > Integer.valueOf(new String(objCode)).intValue()) {
                z = false;
            }
            return z;
        } catch (Throwable e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getVersion(Context ctx) {
        try {
            return ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
            return "";
        }
    }

    public static String getDeviceUdid(Context context) {
        String uuid;
        if (DEVICE_ID != null && DEVICE_ID.length() > 0) {
            return DEVICE_ID;
        }
        SharedPreferences pref = context.getSharedPreferences(PREF_DEVICE_ID, 0);
        if (pref != null && pref.contains(PREF_KEY_DEVICE_ID)) {
            DEVICE_ID = pref.getString(PREF_KEY_DEVICE_ID, null);
        }
        if (DEVICE_ID != null && DEVICE_ID.length() > 0) {
            return DEVICE_ID;
        }
        try {
            DEVICE_ID = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if ("9774d56d682e549c".equals(DEVICE_ID) || DEVICE_ID == null || "null".equalsIgnoreCase(DEVICE_ID.trim())) {
                String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
                if (deviceId == null || "null".equalsIgnoreCase(deviceId.trim())) {
                    uuid = UUID.randomUUID().toString();
                } else {
                    uuid = UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")).toString();
                }
                DEVICE_ID = uuid;
                if (pref == null) {
                    return uuid;
                }
                pref.edit().putString(PREF_KEY_DEVICE_ID, DEVICE_ID).commit();
                return uuid;
            }
            if (pref != null) {
                pref.edit().putString(PREF_KEY_DEVICE_ID, DEVICE_ID).commit();
            }
            return DEVICE_ID;
        } catch (Throwable th) {
            if (pref != null) {
                pref.edit().putString(PREF_KEY_DEVICE_ID, DEVICE_ID).commit();
            }
            throw th;
        }
    }

    public static String getDevicePhoneNumber() {
        TelephonyManager mTelephonyMgr = (TelephonyManager) GlobalDataManager.getInstance().getApplicationContext().getSystemService("phone");
        if (mTelephonyMgr == null) {
            return null;
        }
        String number = mTelephonyMgr.getLine1Number();
        if (number == null || number.length() < 11) {
            return number;
        }
        return number.substring(number.length() - 11);
    }

    public static boolean isValidMobile(String mobile) {
        Matcher matcher = Pattern.compile("^(1(3|4|5|8))\\d{9}$").matcher(mobile);
        System.out.println(matcher.matches() + "---");
        return matcher.matches();
    }

    private static byte[] decript(byte[] encryptedData, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher c = Cipher.getInstance("AES/ECB/ZeroBytePadding");
        c.init(2, new SecretKeySpec(key, "AES"));
        return c.doFinal(encryptedData);
    }

    public static String getDecryptedPassword(String encryptedPwd) {
        try {
            return new String(decript(Base64.decode(encryptedPwd), "c6dd9d408c0bcbeda381d42955e08a3f".substring(0, 16).getBytes("utf-8")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
