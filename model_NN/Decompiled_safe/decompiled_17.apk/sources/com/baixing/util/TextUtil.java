package com.baixing.util;

import android.content.Context;
import android.content.res.Resources;
import com.quanleimu.activity.R;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TextUtil {
    public static final long FULL_DAY = 86400;
    public static final long FULL_HOUR = 3600;
    public static final long FULL_MINITE = 60;
    public static final long FULL_MONTH = 2592000;
    public static final long FULL_YEAR = 31104000;
    private static SimpleDateFormat shortDateFormat = new SimpleDateFormat("yyyy.MM.dd");
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

    public static final String getTimeDesc(long time) {
        return simpleDateFormat.format(new Date(time));
    }

    public static final String getShortTimeDesc(long time) {
        return shortDateFormat.format(new Date(time));
    }

    public static final String timeTillNow(long time, Context context) {
        long timeInterval = (System.currentTimeMillis() - time) / 1000;
        Resources res = context.getResources();
        String agoLabel = res.getString(R.string.common_time_ago);
        if (timeInterval <= 10) {
            return res.getString(R.string.common_time_justnow);
        }
        if (timeInterval > 10 && timeInterval < 60) {
            return timeInterval + res.getString(R.string.common_time_second) + agoLabel;
        }
        if (timeInterval > 60 && timeInterval < FULL_HOUR) {
            return (timeInterval / 60) + res.getString(R.string.common_time_minute) + agoLabel;
        }
        if (timeInterval >= FULL_HOUR && timeInterval < FULL_DAY) {
            return (timeInterval / FULL_HOUR) + res.getString(R.string.common_time_hour) + agoLabel;
        }
        if (timeInterval >= FULL_DAY && timeInterval < FULL_MONTH) {
            return (timeInterval / FULL_DAY) + res.getString(R.string.common_time_day) + agoLabel;
        }
        if (timeInterval >= FULL_MONTH && timeInterval < FULL_YEAR) {
            return (timeInterval / FULL_MONTH) + res.getString(R.string.common_time_month) + agoLabel;
        }
        if (timeInterval >= FULL_YEAR) {
            return res.getString(R.string.common_time_one_year_ago);
        }
        return res.getString(R.string.common_time_unknow);
    }

    public static boolean isNumberSequence(String target) {
        if (target == null || target.trim().length() == 0) {
            return false;
        }
        try {
            Long.parseLong(target.trim());
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static String replaceAll(String string, String expr, String substitute) {
        if (string != null) {
            return string.replaceAll(expr, substitute);
        }
        return string;
    }

    public static String filterString(String source, char[] filterList) {
        if (source == null || " ".equals(source)) {
            return source;
        }
        StringBuffer sb = new StringBuffer();
        int i = 0;
        while (i < source.length()) {
            if (contains(filterList, source.charAt(i))) {
                i++;
            } else {
                sb.append(source.charAt(i));
                i++;
            }
        }
        return sb.toString();
    }

    private static boolean contains(char[] list, char c) {
        for (char l : list) {
            if (l == c) {
                return true;
            }
        }
        return false;
    }

    public static String decimalFormat(double data) {
        return new DecimalFormat("##.0").format(data);
    }

    public static String decodeUnicode(String source) {
        if (source == null || " ".equals(source)) {
            return source;
        }
        StringBuffer sb = new StringBuffer();
        int i = 0;
        while (i < source.length()) {
            if (source.charAt(i) != '\\') {
                sb.append(source.charAt(i));
                i++;
            } else if (source.charAt(i + 1) == 'u') {
                sb.append((char) Integer.parseInt(source.substring(i + 2, i + 6), 16));
                i += 6;
            } else {
                sb.append(source.charAt(i));
                int i2 = i + 1;
                sb.append(source.charAt(i2));
                i = i2 + 1;
            }
        }
        return sb.toString();
    }

    public static String filterXml(String message) {
        String[] from = {"&amp;", "&lt;", "&gt;", "&quot;", "&apos;"};
        String[] to = {"&", "<", ">", "\"", "'"};
        String result = message;
        for (int i = 0; i < from.length; i++) {
            result = result.replaceAll(from[i], to[i]);
        }
        return result;
    }
}
