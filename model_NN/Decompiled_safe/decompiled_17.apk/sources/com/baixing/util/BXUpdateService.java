package com.baixing.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.baixing.activity.MainActivity;
import com.quanleimu.activity.R;
import java.io.File;

public class BXUpdateService extends Service {
    private static final int DOWNLOAD_COMPLETE = 0;
    private static final int DOWNLOAD_FAIL = 1;
    /* access modifiers changed from: private */
    public String apkUrl = "";
    /* access modifiers changed from: private */
    public boolean hasSdCard = false;
    private int titleId = 0;
    /* access modifiers changed from: private */
    public File updateDir = null;
    /* access modifiers changed from: private */
    public File updateFile = null;
    /* access modifiers changed from: private */
    public Handler updateHandler = new Handler() {
        public void handleMessage(Message msg) {
            String apkStr;
            switch (msg.what) {
                case 0:
                    if (BXUpdateService.this.hasSdCard) {
                        apkStr = BXUpdateService.this.updateFile.getPath();
                    } else {
                        apkStr = "/data/data/com.quanleimu.activity/files/baixing_in_tmp.apk";
                    }
                    Uri uri = Uri.fromFile(new File(apkStr));
                    Intent installIntent = new Intent("android.intent.action.VIEW", uri);
                    installIntent.addFlags(1);
                    installIntent.addFlags(268435456);
                    installIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                    BXUpdateService.this.startActivity(installIntent);
                    BXUpdateService.this.updateNotificationManager.cancel(0);
                    BXUpdateService.this.stopService(BXUpdateService.this.updateIntent);
                    return;
                case 1:
                    BXUpdateService.this.updateNotification.setLatestEventInfo(BXUpdateService.this, "下载失败，请重试", "请检查您的网络连接", BXUpdateService.this.updatePendingIntent);
                    BXUpdateService.this.updateNotificationManager.notify(0, BXUpdateService.this.updateNotification);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Intent updateIntent = null;
    /* access modifiers changed from: private */
    public Notification updateNotification = null;
    /* access modifiers changed from: private */
    public NotificationManager updateNotificationManager = null;
    /* access modifiers changed from: private */
    public PendingIntent updatePendingIntent = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.titleId = intent.getIntExtra("titleId", 0);
        this.apkUrl = intent.getStringExtra("apkUrl");
        this.hasSdCard = "mounted".equals(Environment.getExternalStorageState());
        if (this.hasSdCard) {
            this.updateDir = new File(Environment.getExternalStorageDirectory(), "/tmp/baixing_tmp/");
        } else {
            this.updateDir = new File(getCacheDir().getPath(), "/tmp/baixing_tmp/");
        }
        this.updateFile = new File(this.updateDir.getPath(), "baixing_app_tmp.apk");
        this.updateNotificationManager = (NotificationManager) getSystemService("notification");
        this.updateNotification = new Notification();
        this.updateIntent = new Intent(this, MainActivity.class);
        this.updatePendingIntent = PendingIntent.getActivity(this, 0, this.updateIntent, 0);
        this.updateNotification.icon = R.drawable.app_icon;
        this.updateNotification.tickerText = "开始下载";
        this.updateNotification.setLatestEventInfo(this, "百姓网客户端", "0%", this.updatePendingIntent);
        this.updateNotificationManager.notify(0, this.updateNotification);
        new Thread(new updateRunnable()).start();
        return super.onStartCommand(intent, flags, startId);
    }

    class updateRunnable implements Runnable {
        Message message = BXUpdateService.this.updateHandler.obtainMessage();

        updateRunnable() {
        }

        public void run() {
            this.message.what = 0;
            try {
                if (!BXUpdateService.this.updateDir.exists()) {
                    BXUpdateService.this.updateDir.mkdirs();
                }
                if (BXUpdateService.this.downloadUpdateFile(BXUpdateService.this.apkUrl, BXUpdateService.this.updateFile) > 0) {
                    BXUpdateService.this.updateHandler.sendMessage(this.message);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                this.message.what = 1;
                BXUpdateService.this.updateHandler.sendMessage(this.message);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r17v1, types: [java.net.URLConnection] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long downloadUpdateFile(java.lang.String r24, java.io.File r25) throws java.lang.Exception {
        /*
            r23 = this;
            r6 = 0
            r5 = 0
            r12 = 0
            r15 = 0
            r9 = 0
            r10 = 0
            r7 = 0
            java.net.URL r16 = new java.net.URL     // Catch:{ all -> 0x0058 }
            r0 = r16
            r1 = r24
            r0.<init>(r1)     // Catch:{ all -> 0x0058 }
            java.net.URLConnection r17 = r16.openConnection()     // Catch:{ all -> 0x0058 }
            r0 = r17
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0058 }
            r9 = r0
            java.lang.String r17 = "User-Agent"
            java.lang.String r18 = "baixing java HttpClient"
            r0 = r17
            r1 = r18
            r9.setRequestProperty(r0, r1)     // Catch:{ all -> 0x0058 }
            java.lang.String r17 = "Accept-Encoding"
            java.lang.String r18 = "identity"
            r0 = r17
            r1 = r18
            r9.setRequestProperty(r0, r1)     // Catch:{ all -> 0x0058 }
            r17 = 60000(0xea60, float:8.4078E-41)
            r0 = r17
            r9.setConnectTimeout(r0)     // Catch:{ all -> 0x0058 }
            r17 = 600000(0x927c0, float:8.40779E-40)
            r0 = r17
            r9.setReadTimeout(r0)     // Catch:{ all -> 0x0058 }
            int r15 = r9.getContentLength()     // Catch:{ all -> 0x0058 }
            int r17 = r9.getResponseCode()     // Catch:{ all -> 0x0058 }
            r18 = 404(0x194, float:5.66E-43)
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x006c
            java.lang.Exception r17 = new java.lang.Exception     // Catch:{ all -> 0x0058 }
            java.lang.String r18 = "fail!"
            r17.<init>(r18)     // Catch:{ all -> 0x0058 }
            throw r17     // Catch:{ all -> 0x0058 }
        L_0x0058:
            r17 = move-exception
            if (r9 == 0) goto L_0x005e
            r9.disconnect()
        L_0x005e:
            if (r10 == 0) goto L_0x0063
            r10.close()
        L_0x0063:
            if (r7 == 0) goto L_0x006b
            r7.flush()
            r7.close()
        L_0x006b:
            throw r17
        L_0x006c:
            java.io.InputStream r10 = r9.getInputStream()     // Catch:{ all -> 0x0058 }
            r0 = r23
            boolean r0 = r0.hasSdCard     // Catch:{ all -> 0x0058 }
            r17 = r0
            if (r17 == 0) goto L_0x0124
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ all -> 0x0058 }
            r17 = 0
            r0 = r25
            r1 = r17
            r8.<init>(r0, r1)     // Catch:{ all -> 0x0058 }
            r7 = r8
        L_0x0084:
            r17 = 4096(0x1000, float:5.74E-42)
            r0 = r17
            byte[] r4 = new byte[r0]     // Catch:{ all -> 0x0058 }
            r11 = 0
            r14 = 0
        L_0x008c:
            int r11 = r10.read(r4)     // Catch:{ all -> 0x0058 }
            if (r11 <= 0) goto L_0x0134
            r17 = 0
            r0 = r17
            r7.write(r4, r0, r11)     // Catch:{ all -> 0x0058 }
            long r0 = (long) r11     // Catch:{ all -> 0x0058 }
            r17 = r0
            long r12 = r12 + r17
            if (r6 == 0) goto L_0x00bb
            double r0 = (double) r12     // Catch:{ all -> 0x0058 }
            r17 = r0
            double r0 = (double) r15     // Catch:{ all -> 0x0058 }
            r19 = r0
            r21 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r19 = r19 * r21
            double r17 = r17 / r19
            r19 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r17 = r17 * r19
            r19 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r17 = r17 - r19
            double r0 = (double) r6     // Catch:{ all -> 0x0058 }
            r19 = r0
            int r17 = (r17 > r19 ? 1 : (r17 == r19 ? 0 : -1))
            if (r17 <= 0) goto L_0x008c
        L_0x00bb:
            int r6 = r6 + 1
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ all -> 0x0058 }
            r17.<init>()     // Catch:{ all -> 0x0058 }
            int r0 = (int) r12     // Catch:{ all -> 0x0058 }
            r18 = r0
            int r18 = r18 * 100
            int r18 = r18 / r15
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0058 }
            java.lang.String r18 = "% "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0058 }
            r18 = 1000(0x3e8, double:4.94E-321)
            long r18 = r12 / r18
            r0 = r18
            int r0 = (int) r0     // Catch:{ all -> 0x0058 }
            r18 = r0
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0058 }
            java.lang.String r18 = "KB/"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0058 }
            int r0 = r15 / 1000
            r18 = r0
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0058 }
            java.lang.String r18 = "KB"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0058 }
            java.lang.String r14 = r17.toString()     // Catch:{ all -> 0x0058 }
            r0 = r23
            android.app.Notification r0 = r0.updateNotification     // Catch:{ all -> 0x0058 }
            r17 = r0
            java.lang.String r18 = "正在下载新版客户端"
            r0 = r23
            android.app.PendingIntent r0 = r0.updatePendingIntent     // Catch:{ all -> 0x0058 }
            r19 = r0
            r0 = r17
            r1 = r23
            r2 = r18
            r3 = r19
            r0.setLatestEventInfo(r1, r2, r14, r3)     // Catch:{ all -> 0x0058 }
            r0 = r23
            android.app.NotificationManager r0 = r0.updateNotificationManager     // Catch:{ all -> 0x0058 }
            r17 = r0
            r18 = 0
            r0 = r23
            android.app.Notification r0 = r0.updateNotification     // Catch:{ all -> 0x0058 }
            r19 = r0
            r17.notify(r18, r19)     // Catch:{ all -> 0x0058 }
            goto L_0x008c
        L_0x0124:
            java.lang.String r17 = "baixing_in_tmp.apk"
            r18 = 1
            r0 = r23
            r1 = r17
            r2 = r18
            java.io.FileOutputStream r7 = r0.openFileOutput(r1, r2)     // Catch:{ all -> 0x0058 }
            goto L_0x0084
        L_0x0134:
            if (r9 == 0) goto L_0x0139
            r9.disconnect()
        L_0x0139:
            if (r10 == 0) goto L_0x013e
            r10.close()
        L_0x013e:
            if (r7 == 0) goto L_0x0146
            r7.flush()
            r7.close()
        L_0x0146:
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.BXUpdateService.downloadUpdateFile(java.lang.String, java.io.File):long");
    }
}
