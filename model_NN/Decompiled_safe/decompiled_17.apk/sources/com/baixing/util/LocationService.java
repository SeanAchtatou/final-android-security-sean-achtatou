package com.baixing.util;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.LocationListener;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKLocationManager;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.BXLocation;
import com.baixing.network.NetworkCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class LocationService {
    private static LocationService s_instance;
    private BMapManager bMapManager;
    /* access modifiers changed from: private */
    public Location lastKnownLocation;
    private BXLocationListener locationListener;
    private MKLocationManager mkLocationManager;

    public interface BXLocationServiceListener {
        void onLocationUpdated(Location location);
    }

    public interface BXRgcListener {
        void onRgcUpdated(BXLocation bXLocation);
    }

    public static LocationService getInstance() {
        if (s_instance == null) {
            s_instance = new LocationService();
        }
        return s_instance;
    }

    class BXSearchListener implements MKSearchListener {
        private float lat = 0.0f;
        private float lon = 0.0f;
        BXRgcListener rgcCallback = null;

        BXSearchListener(BXRgcListener listener) {
            this.rgcCallback = listener;
        }

        BXSearchListener(BXRgcListener listener, float lat2, float lon2) {
            this.rgcCallback = listener;
            this.lat = lat2;
            this.lon = lon2;
        }

        public void onGetAddrResult(MKAddrInfo result, int iError) {
            String str = null;
            Log.d("location", "location, onGetAddrResult reached");
            BXLocation location = null;
            if (iError == 0) {
                location = new BXLocation(false);
                location.address = result.strBusiness;
                location.detailAddress = result.strAddr;
                location.cityName = result.addressComponents == null ? null : result.addressComponents.city;
                location.subCityName = result.addressComponents == null ? null : result.addressComponents.district;
                if (result.addressComponents != null) {
                    str = result.addressComponents.province;
                }
                location.adminArea = str;
                if (location.cityName != null && location.cityName.length() > 0) {
                    location.geocoded = true;
                }
                location.fGeoCodedLat = (float) (((double) (((float) result.geoPt.getLatitudeE6()) * 1.0f)) / 1000000.0d);
                location.fGeoCodedLon = (float) (((double) (((float) result.geoPt.getLongitudeE6()) * 1.0f)) / 1000000.0d);
                location.fLat = this.lat == 0.0f ? location.fGeoCodedLat : this.lat;
                location.fLon = this.lon == 0.0f ? location.fGeoCodedLon : this.lon;
            }
            if (this.rgcCallback != null) {
                this.rgcCallback.onRgcUpdated(location);
            }
        }

        public void onGetDrivingRouteResult(MKDrivingRouteResult result, int iError) {
            Log.d("location", "location, onGetDrivingRouteResult reached");
        }

        public void onGetPoiResult(MKPoiResult result, int type, int iError) {
            Log.d("location", "location, onGetPoiResult reached");
        }

        public void onGetTransitRouteResult(MKTransitRouteResult result, int iError) {
            Log.d("location", "location, onGetTransitRouteResult reached");
        }

        public void onGetWalkingRouteResult(MKWalkingRouteResult result, int iError) {
            Log.d("location", "location, onGetWalkingRouteResult reached");
        }

        public void onGetBusDetailResult(MKBusLineResult result, int iError) {
            Log.d("location", "location, onGetAddrResult reached");
        }

        public void onGetSuggestionResult(MKSuggestionResult result, int iError) {
            Log.d("location", "location, onGetSuggestionResult reached");
        }
    }

    public boolean geocode(String addr, String city, BXRgcListener callback) {
        if (this.bMapManager == null) {
            return false;
        }
        MKSearch searcher = new MKSearch();
        searcher.init(this.bMapManager, new BXSearchListener(callback));
        this.bMapManager.start();
        if (searcher.geocode(addr, city) == 0) {
            return true;
        }
        return false;
    }

    public boolean reverseGeocode(float lat, float lon, BXRgcListener callback) {
        if (this.bMapManager == null) {
            return false;
        }
        MKSearch searcher = new MKSearch();
        searcher.init(this.bMapManager, new BXSearchListener(callback, lat, lon));
        this.bMapManager.start();
        if (searcher.reverseGeocode(new GeoPoint((int) (((double) lat) * 1000000.0d), (int) (((double) lon) * 1000000.0d))) == 0) {
            return true;
        }
        return false;
    }

    public Location getLastKnownLocation() {
        return this.lastKnownLocation;
    }

    public void start(Context context, BXLocationServiceListener listener) {
        init(context, listener);
    }

    public void stop() {
        if (this.mkLocationManager != null) {
            this.mkLocationManager.removeUpdates(this.locationListener);
        }
        if (this.bMapManager != null) {
            this.bMapManager.stop();
        }
        this.bMapManager = null;
        this.mkLocationManager = null;
    }

    class BXLocationListener implements LocationListener {
        private List<BXLocationServiceListener> userListeners = new ArrayList();

        public BXLocationListener(BXLocationServiceListener userListener) {
            if (userListener != null) {
                this.userListeners.add(userListener);
            }
        }

        public void addListener(BXLocationServiceListener listener) {
            if (listener != null) {
                this.userListeners.remove(listener);
                this.userListeners.add(listener);
            }
        }

        public void removeListener(BXLocationServiceListener listener) {
            if (listener != null) {
                this.userListeners.remove(listener);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
         arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
         candidates:
          com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
          com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
          com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
          com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
          com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
          com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
          com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
        public void onLocationChanged(Location location) {
            if (location != null) {
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.GPS).append(TrackConfig.TrackMobile.Key.GPS_RESULT, true).append(TrackConfig.TrackMobile.Key.GPS_GEO, String.format("(%f,%f)", Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()))).end();
            } else {
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.GPS).append(TrackConfig.TrackMobile.Key.GPS_RESULT, false).end();
            }
            if (location != null) {
                Location unused = LocationService.this.lastKnownLocation = location;
                for (BXLocationServiceListener listener : this.userListeners) {
                    listener.onLocationUpdated(location);
                }
            }
        }
    }

    public void addLocationListener(Context context, BXLocationServiceListener userListener) {
        if (this.bMapManager == null || this.mkLocationManager == null || this.locationListener == null) {
            start(context, userListener);
        } else {
            this.locationListener.addListener(userListener);
        }
    }

    public void removeLocationListener(BXLocationServiceListener userListener) {
        if (this.bMapManager != null && this.mkLocationManager != null && this.locationListener != null) {
            this.locationListener.removeListener(userListener);
        }
    }

    private void init(Context context, BXLocationServiceListener userListener) {
        if (!(this.mkLocationManager == null || this.locationListener == null)) {
            this.mkLocationManager.removeUpdates(this.locationListener);
        }
        this.locationListener = new BXLocationListener(userListener);
        if (this.bMapManager == null) {
            try {
                this.bMapManager = new BMapManager(context);
                this.bMapManager.init("736C4435847CB7D20DD1131064E35E8941C934F5", null);
            } catch (Throwable th) {
                this.bMapManager = null;
                return;
            }
        } else {
            this.bMapManager.stop();
        }
        this.mkLocationManager = this.bMapManager.getLocationManager();
        this.mkLocationManager.setLocationCoordinateType(0);
        this.mkLocationManager.requestLocationUpdates(this.locationListener);
        this.bMapManager.start();
    }

    public static BXLocation retreiveCoorFromGoogle(String addr) {
        JSONArray aryResults;
        JSONObject result;
        JSONArray types;
        JSONObject jsonLocation;
        BXLocation ret = new BXLocation();
        if (addr != null && !addr.equals("")) {
            try {
                JSONObject jsonObj = new JSONObject(NetworkCommand.doGet(GlobalDataManager.getInstance().getApplicationContext(), String.format("http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false&language=\"zh-CN\"", URLEncoder.encode(addr))));
                if (!(jsonObj == null || !jsonObj.getString("status").equals("OK") || (aryResults = jsonObj.getJSONArray("results")) == null || (result = aryResults.getJSONObject(0)) == null)) {
                    ret.detailAddress = result.getString("formatted_address");
                    JSONObject geo = result.getJSONObject("geometry");
                    if (!(geo == null || (jsonLocation = geo.getJSONObject("location")) == null)) {
                        String lat = jsonLocation.getString("lat");
                        String lng = jsonLocation.getString("lng");
                        ret.fGeoCodedLat = Float.valueOf(lat).floatValue();
                        ret.fGeoCodedLon = Float.valueOf(lng).floatValue();
                        ret.geocoded = true;
                        ret.fLat = ret.fGeoCodedLat;
                        ret.fLon = ret.fGeoCodedLon;
                    }
                    JSONArray addresses = result.getJSONArray("address_components");
                    if (addresses != null) {
                        for (int i = 0; i < addresses.length(); i++) {
                            JSONObject add = addresses.getJSONObject(i);
                            if (!(add == null || (types = add.getJSONArray("types")) == null)) {
                                String value = add.getString("long_name");
                                if (types.getString(0).equals("sublocality")) {
                                    ret.subCityName = value;
                                } else if (types.getString(0).equals("locality")) {
                                    ret.cityName = value;
                                } else if (types.getString(0).equals("administrative_area_level_1")) {
                                    ret.adminArea = value;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
}
