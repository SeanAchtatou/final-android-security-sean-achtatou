package com.baixing.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.AdList;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.devspark.appmsg.AppMsg;
import java.io.Serializable;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class VadListLoader implements Serializable {
    /* access modifiers changed from: private */
    public static int DEFAULT_ROWS = 30;
    public static final int MSG_EXCEPTION = -1;
    public static final int MSG_FINISH_GET_FIRST = 0;
    public static final int MSG_FINISH_GET_MORE = 1;
    public static final int MSG_FIRST_FAIL = 268435455;
    public static final int MSG_NO_MORE = 2;
    private static final long serialVersionUID = -4876054768912906374L;
    /* access modifiers changed from: private */
    public transient Callback callback;
    /* access modifiers changed from: private */
    public transient GetListCommand currentCommand = null;
    private transient HasMoreListener hasMoreListener = null;
    private int mCurIndex = 0;
    private String mFields = "";
    private AdList mGoodsList = null;
    private boolean mHasMore = true;
    /* access modifiers changed from: private */
    public boolean mIsFirst = true;
    /* access modifiers changed from: private */
    public String mLastJson = null;
    private SEARCH_POLICY mPolicy = SEARCH_POLICY.SEARCH_LISTING;
    /* access modifiers changed from: private */
    public int mRows = DEFAULT_ROWS;
    private boolean mRt = true;
    /* access modifiers changed from: private */
    public E_LISTDATA_STATUS mStatusListdataExisting = E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE;
    private E_LISTDATA_STATUS mStatusListdataRequesting = E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE;
    /* access modifiers changed from: private */
    public ApiParams params = null;

    public interface Callback {
        void onRequestComplete(int i, Object obj);
    }

    public enum E_LISTDATA_STATUS {
        E_LISTDATA_STATUS_OFFLINE,
        E_LISTDATA_STATUS_ONLINE
    }

    public interface HasMoreListener {
        void onHasMoreStatusChanged();
    }

    public void reset() {
        this.mGoodsList = null;
        this.mLastJson = null;
        this.callback = null;
    }

    public VadListLoader(ApiParams params2, Callback callback2, String fields, AdList goodsList) {
        this.params = params2;
        this.callback = callback2;
        if (fields != null) {
            this.mFields = fields;
        }
        if (goodsList != null) {
            this.mGoodsList = goodsList;
        }
    }

    public void setCallback(Callback callback2) {
        this.callback = callback2;
    }

    public void setParams(ApiParams params2) {
        this.params = params2;
    }

    public void setRows(int rows) {
        this.mRows = rows;
    }

    public int getRows() {
        return this.mRows;
    }

    public String getLastJson() {
        return this.mLastJson;
    }

    public void setGoodsList(AdList list) {
        this.mGoodsList = list;
    }

    public AdList getGoodsList() {
        if (this.mGoodsList == null) {
            this.mGoodsList = new AdList();
        }
        return this.mGoodsList;
    }

    public void setHasMore(boolean hasMore) {
        if (this.mHasMore != hasMore) {
            this.mHasMore = hasMore;
            if (this.hasMoreListener != null) {
                this.hasMoreListener.onHasMoreStatusChanged();
            }
        }
    }

    public boolean hasMore() {
        return this.mHasMore;
    }

    public int getSelection() {
        return this.mCurIndex;
    }

    public void setSelection(int selection) {
        this.mCurIndex = selection;
    }

    public void cancelFetching() {
        if (this.currentCommand != null) {
            this.currentCommand.cancel();
        }
    }

    public void setHasMoreListener(HasMoreListener listener) {
        this.hasMoreListener = listener;
    }

    public E_LISTDATA_STATUS getDataStatus() {
        return this.mStatusListdataExisting;
    }

    public E_LISTDATA_STATUS getRequestDataStatus() {
        return this.mStatusListdataRequesting;
    }

    public enum SEARCH_POLICY {
        SEARCH_LISTING("ad"),
        SEARCH_USER_LIST("ad.userAds/"),
        SEARCH_NEARBY("ad"),
        SEARCH_AROUND("extra"),
        SEARCH_FAVORITES("Favorite.get/"),
        SEARCH_RECOMMEND("Mobile.adRecommend/");
        
        public String command;

        private SEARCH_POLICY(String command2) {
            this.command = command2;
        }
    }

    public void setSearchType(SEARCH_POLICY policy) {
        this.mPolicy = policy;
    }

    public SEARCH_POLICY getSearchType() {
        return this.mPolicy;
    }

    public void startFetching(Context context, boolean isFirst, boolean useCache) {
        cancelFetching();
        this.mStatusListdataRequesting = useCache ? E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE : E_LISTDATA_STATUS.E_LISTDATA_STATUS_ONLINE;
        this.mIsFirst = isFirst;
        this.currentCommand = new GetListCommand(context, this.mPolicy, useCache);
        this.currentCommand.run();
    }

    public void startFetching(Context context, boolean isFirst, int msgGotFirst, int msgGotMore, int msgNoMore, boolean useCache) {
        cancelFetching();
        this.mStatusListdataRequesting = useCache ? E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE : E_LISTDATA_STATUS.E_LISTDATA_STATUS_ONLINE;
        this.mIsFirst = isFirst;
        this.currentCommand = new GetListCommand(context, msgGotFirst, msgGotMore, msgNoMore, useCache, this.mPolicy);
        this.currentCommand.run();
    }

    public void setRuntime(boolean rt) {
        this.mRt = rt;
    }

    class GetListCommand implements Runnable, BaseApiCommand.Callback {
        private Context context;
        private boolean mCancel = false;
        private int msgFirst = 0;
        private int msgMore = 1;
        private int msgNoMore = 2;
        private SEARCH_POLICY policy = SEARCH_POLICY.SEARCH_LISTING;
        private boolean useCache;

        GetListCommand(Context cxt, SEARCH_POLICY policy2, boolean useCache2) {
            this.useCache = useCache2;
            this.policy = policy2;
            this.context = cxt;
        }

        GetListCommand(Context cxt, int errFirst, int errMore, int errNoMore, boolean useCache2, SEARCH_POLICY policy2) {
            this.msgFirst = errFirst;
            this.msgMore = errMore;
            this.msgNoMore = errNoMore;
            this.useCache = useCache2;
            this.policy = policy2;
            this.context = cxt;
        }

        public void cancel() {
            this.mCancel = true;
        }

        private void exit() {
            GetListCommand unused = VadListLoader.this.currentCommand = null;
        }

        private boolean wantedExists(ApiParams list) {
            Log.d("wantedExist", list.toString());
            return list.getParam("wanted") != null;
        }

        public void start() {
            run();
        }

        private int skipCount() {
            AdList list = VadListLoader.this.getGoodsList();
            if (list == null || list.getData() == null) {
                return 0;
            }
            return list.getData().size();
        }

        public void run() {
            if (this.mCancel) {
                exit();
                return;
            }
            ApiParams list = new ApiParams();
            list.useCache = this.useCache;
            if (VadListLoader.this.params != null) {
                list.setParams(VadListLoader.this.params.getParams());
            }
            list.addParam("from", "" + (VadListLoader.this.mIsFirst ? 0 : skipCount()));
            if (VadListLoader.this.mRows <= 0) {
                int unused = VadListLoader.this.mRows = VadListLoader.DEFAULT_ROWS;
            }
            if (!this.policy.command.equals(SEARCH_POLICY.SEARCH_FAVORITES.command)) {
                list.addParam("size", VadListLoader.this.mRows);
            } else {
                list.addParam("limit", VadListLoader.this.mRows);
            }
            if (this.mCancel) {
                exit();
                return;
            }
            String method = this.policy.command;
            if (this.policy.command.equals(SEARCH_POLICY.SEARCH_FAVORITES.command) || this.policy.command.equals(SEARCH_POLICY.SEARCH_USER_LIST.command)) {
                Log.d("usr", this.policy.command);
                list.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
            } else if (this.policy.equals(SEARCH_POLICY.SEARCH_NEARBY)) {
                method = list.getParam("categoryEnglishName") + CookieSpec.PATH_DELIM + method;
                list.addParam("coordinate[distance]", (int) AppMsg.LENGTH_SHORT);
                list.removeParam("area");
                list.removeParam("categoryEnglishName");
            } else if (this.policy.equals(SEARCH_POLICY.SEARCH_LISTING) || this.policy.equals(SEARCH_POLICY.SEARCH_AROUND)) {
                method = list.getParam("categoryEnglishName") + CookieSpec.PATH_DELIM + method;
                list.removeParam("categoryEnglishName");
            }
            Log.d("loader", "api:" + method);
            Log.d("loader", "param:" + VadListLoader.this.params.toString());
            Log.d("loader", list.toString());
            BaseApiCommand.createCommand(method, true, list).execute(this.context, this);
        }

        public void onNetworkDone(String apiName, String responseData) {
            E_LISTDATA_STATUS e_listdata_status;
            String unused = VadListLoader.this.mLastJson = responseData;
            Log.d("loader", "response:" + responseData);
            if (VadListLoader.this.mLastJson != null && !VadListLoader.this.mLastJson.equals("")) {
                if (VadListLoader.this.mIsFirst) {
                    boolean unused2 = VadListLoader.this.mIsFirst = false;
                    if (VadListLoader.this.callback != null) {
                        VadListLoader.this.callback.onRequestComplete(this.msgFirst, VadListLoader.this.mLastJson);
                    }
                } else if (VadListLoader.this.callback != null) {
                    VadListLoader.this.callback.onRequestComplete(this.msgMore, VadListLoader.this.mLastJson);
                }
                VadListLoader vadListLoader = VadListLoader.this;
                if (this.useCache) {
                    e_listdata_status = E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE;
                } else {
                    e_listdata_status = E_LISTDATA_STATUS.E_LISTDATA_STATUS_ONLINE;
                }
                E_LISTDATA_STATUS unused3 = vadListLoader.mStatusListdataExisting = e_listdata_status;
            } else if (!VadListLoader.this.mIsFirst) {
                if (VadListLoader.this.callback != null) {
                    VadListLoader.this.callback.onRequestComplete(this.msgNoMore, null);
                }
            } else if (VadListLoader.this.callback != null) {
                VadListLoader.this.callback.onRequestComplete(VadListLoader.MSG_FIRST_FAIL, null);
            }
        }

        public void onNetworkFail(String apiName, ApiError error) {
            if (!this.mCancel && VadListLoader.this.callback != null) {
                if (error == null || TextUtils.isEmpty(error.getMsg())) {
                    VadListLoader.this.callback.onRequestComplete(-10, null);
                } else {
                    VadListLoader.this.callback.onRequestComplete(-3, error.getMsg());
                }
            }
        }
    }
}
