package com.baixing.util.hardware;

import android.hardware.SensorEvent;
import com.baixing.util.hardware.RotationDetector;

public class OrientationSensorDetector extends RotationDetector {
    public RotationDetector.Orien updateSensorEvent(SensorEvent sensorEvent) {
        return updateOrien(sensorEvent.values[1], sensorEvent.values[2]);
    }

    private RotationDetector.Orien updateOrien(float pitch, float roll) {
        if ((pitch < -45.0f && pitch > -135.0f) || (roll >= -15.0f && roll <= 15.0f && pitch >= -45.0f && pitch <= 0.0f)) {
            return RotationDetector.Orien.TOP_UP;
        }
        if (pitch > 45.0f && pitch < 135.0f) {
            return RotationDetector.Orien.BOTTOM_UP;
        }
        if (roll > 45.0f) {
            return RotationDetector.Orien.RIGHT_UP;
        }
        if (roll < -45.0f || (roll > -45.0f && roll < 0.0f && pitch > -15.0f && pitch < 15.0f)) {
            return RotationDetector.Orien.LEFT_UP;
        }
        return RotationDetector.Orien.DEFAULT;
    }
}
