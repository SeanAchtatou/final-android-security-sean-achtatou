package com.baixing.util.hardware;

import android.hardware.SensorEvent;
import com.baixing.util.hardware.RotationDetector;

public class AccelerometerSensorDetector extends RotationDetector {
    public RotationDetector.Orien updateSensorEvent(SensorEvent sensorEvent) {
        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];
        float f = sensorEvent.values[2];
        return Math.abs(x) > Math.abs(y) ? x > 0.0f ? RotationDetector.Orien.RIGHT_UP : RotationDetector.Orien.LEFT_UP : y > 0.0f ? RotationDetector.Orien.TOP_UP : RotationDetector.Orien.BOTTOM_UP;
    }
}
