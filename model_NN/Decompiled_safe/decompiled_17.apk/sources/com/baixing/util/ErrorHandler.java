package com.baixing.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class ErrorHandler extends Handler {
    public static final int ERROR_COMMON_FAILURE = -3;
    public static final int ERROR_COMMON_WARNING = -2;
    public static final int ERROR_NETWORK_UNAVAILABLE = -10;
    public static final int ERROR_OK = 0;
    public static final int ERROR_SERVICE_UNAVAILABLE = -9;
    private static ErrorHandler m_instance = null;
    private final String MSG_KEY = "popup_message";
    private Context context = null;

    public static ErrorHandler getInstance() {
        if (m_instance == null) {
            m_instance = new ErrorHandler();
        }
        return m_instance;
    }

    public void initContext(Context context_) {
        this.context = context_;
    }

    public void handleError(int errorCode, String msgContent) {
        Message msg = obtainMessage(errorCode);
        if (msgContent != null) {
            Bundle bundle = new Bundle();
            bundle.putString("popup_message", msgContent);
            msg.setData(bundle);
        }
        sendMessage(msg);
    }

    public void handleMessage(Message msg) {
        if (msg.obj != null && (msg.obj instanceof ProgressDialog)) {
            ((ProgressDialog) msg.obj).dismiss();
        }
        String strToast = null;
        if (!(msg.getData() == null || msg.getData().getString("popup_message") == null)) {
            strToast = msg.getData().getString("popup_message");
        }
        if (strToast == null) {
            switch (msg.what) {
                case ERROR_NETWORK_UNAVAILABLE /*-10*/:
                    strToast = "网络连接失败，请检查设置！";
                    break;
                case -9:
                    strToast = "服务当前不可用，请稍后重试！";
                    break;
                case -3:
                    strToast = "操作失败，请检查后重试!";
                    break;
                case -2:
                    strToast = "请注意！";
                    break;
                case 0:
                    strToast = "操作已成功！";
                    break;
            }
        }
        if (!(strToast == null || strToast.length() == 0)) {
            ViewUtil.showToast(this.context, strToast, false);
        }
        if (msg.getCallback() != null) {
            msg.getCallback().run();
        }
        super.handleMessage(msg);
    }
}
