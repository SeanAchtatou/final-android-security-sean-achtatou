package com.baixing.util.post;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Pair;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.BXLocation;
import com.baixing.entity.Filterss;
import com.baixing.entity.PostGoodsBean;
import com.baixing.entity.UserBean;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class PostNetworkService implements BaseApiCommand.Callback {
    private static PostNetworkService instance;
    private String category;
    private String city;
    private Handler handler;
    private String mobile;
    private PostParams postParams;
    private String verifyCode;

    public static class PostResultData {
        public int error;
        public String id;
        public boolean isRegisteredUser;
        public String message;
    }

    class PostParams {
        public String apiName;
        public ApiParams params;

        PostParams() {
        }
    }

    public static PostNetworkService getInstance() {
        if (instance == null) {
            instance = new PostNetworkService();
        }
        return instance;
    }

    public void setHandler(Handler handler2) {
        this.handler = handler2;
    }

    private PostNetworkService() {
    }

    public void retreiveMetaAsync(String cityEnglishName, String categoryEnglishName) {
        this.city = cityEnglishName;
        this.category = categoryEnglishName;
        ApiParams param = new ApiParams();
        param.addParam("categoryId", categoryEnglishName);
        BaseApiCommand.createCommand("Meta.post/", true, param).execute(GlobalDataManager.getInstance().getApplicationContext(), this);
    }

    public void doRegisterAndVerify(String mobile2) {
        UserBean curUser = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (curUser == null || curUser.getPhone() == null) {
            String checkMobile = mobile2;
            String deviceNumber = Util.getDevicePhoneNumber();
            if (deviceNumber != null && deviceNumber.length() == 11) {
                checkMobile = deviceNumber;
            }
            sendMessage(PostCommonValues.MSG_POST_NEED_LOGIN, checkMobile);
            return;
        }
        doPost();
    }

    private void doPost() {
        if (this.postParams != null) {
            if (GlobalDataManager.getInstance().getAccountManager().getCurrentUser() != null) {
                this.postParams.params.removeParam("mobile");
                this.postParams.params.removeParam("userToken");
            }
            ApiParams apiParam = new ApiParams();
            if (this.postParams.params.hasParam("adId")) {
                apiParam.addParam("adId", this.postParams.params.getParam("adId"));
                this.postParams.params.removeParam("adId");
            } else {
                apiParam.addParam("cityEnglishName", this.postParams.params.getParam("cityEnglishName"));
                apiParam.addParam("categoryEnglishName", this.postParams.params.getParam("categoryEnglishName"));
            }
            this.postParams.params.removeParam("cityEnglishName");
            this.postParams.params.removeParam("categoryEnglishName");
            apiParam.addParam("params", this.postParams.params.getParams());
            apiParam.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
            apiParam.addParam("uuid", Util.getDeviceUdid(GlobalDataManager.getInstance().getApplicationContext()));
            BaseApiCommand.createCommand(this.postParams.apiName, false, apiParam).execute(GlobalDataManager.getInstance().getApplicationContext(), this);
        }
    }

    public static String getUploadedUrl(String url) {
        int pos;
        int end;
        if (Pattern.compile("^[a-zA-Z0-9]+.jpg$").matcher(url).matches()) {
            return url;
        }
        if (!url.startsWith("http://") || (end = url.indexOf((String) com.tencent.mm.sdk.platformtools.Util.PHOTO_DEFAULT_EXT)) <= (pos = url.indexOf("baixing.net/")) || pos < 0) {
            return "";
        }
        return url.substring("baixing.net/".length() + pos, com.tencent.mm.sdk.platformtools.Util.PHOTO_DEFAULT_EXT.length() + end);
    }

    public void savePostData(HashMap<String, String> params, HashMap<String, String> mustParams, LinkedHashMap<String, PostGoodsBean> beans, List<String> bmpUrls, BXLocation address, boolean editMode) {
        String apiName = editMode ? "ad.update/" : "ad.insert/";
        UserBean user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        boolean registered = (user == null || user.getPhone() == null || user.getPhone().equals("")) ? false : true;
        ApiParams apiParam = new ApiParams();
        if (mustParams != null) {
            apiParam.addAll(mustParams);
        }
        apiParam.addParam(PostCommonValues.STRING_AREA, getDistrictByLocation(address));
        if (registered) {
        }
        if (address != null) {
            apiParam.addParam("lat", String.valueOf(address.fGeoCodedLat));
            apiParam.addParam("lng", String.valueOf(address.fGeoCodedLon));
        }
        Set<String> keys = params.keySet();
        if (keys != null) {
            for (String key : keys) {
                String value = params.get(key);
                if (!(value == null || value.length() <= 0 || beans.get(key) == null)) {
                    apiParam.addParam(beans.get(key).getName(), value);
                }
            }
        }
        String images = "";
        if (bmpUrls != null) {
            for (int i = 0; i < bmpUrls.size(); i++) {
                String uploadUrl = getUploadedUrl(bmpUrls.get(i));
                if (!TextUtils.isEmpty(uploadUrl)) {
                    images = images + " " + uploadUrl + "#up";
                }
            }
            if (images != null && images.length() > 0 && images.charAt(0) == ',') {
                images = images.substring(1);
            }
            if (images != null && images.length() > 0) {
                apiParam.addParam("images", images);
            }
        }
        this.postParams = new PostParams();
        this.postParams.apiName = apiName;
        this.postParams.params = new ApiParams();
        this.postParams.params.addAll(apiParam.getParams());
    }

    private void sendMessage(int what, Object obj) {
        if (this.handler != null) {
            Message msg = Message.obtain();
            msg.what = what;
            msg.obj = obj;
            this.handler.sendMessage(msg);
        }
    }

    private String getDistrictByLocation(BXLocation location) {
        String areaJson;
        String district = GlobalDataManager.getInstance().getCityId();
        Pair<Long, String> areaPair = Util.loadJsonAndTimestampFromLocate(GlobalDataManager.getInstance().getApplicationContext(), "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName());
        if (areaPair.second == null || ((String) areaPair.second).length() <= 0 || ((Long) areaPair.first).longValue() + TextUtil.FULL_DAY < System.currentTimeMillis() / 1000) {
            ApiParams areaParam = new ApiParams();
            areaParam.addParam("cityId", GlobalDataManager.getInstance().getCityId());
            areaJson = BaseApiCommand.createCommand("City.areas/", true, areaParam).executeSync(GlobalDataManager.getInstance().getApplicationContext());
            if (areaJson != null && areaJson.length() > 0) {
                Util.saveJsonAndTimestampToLocate(GlobalDataManager.getInstance().getApplicationContext(), "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName(), areaJson, System.currentTimeMillis() / 1000);
            }
        } else {
            areaJson = (String) areaPair.second;
        }
        if (location == null || location.subCityName == null || areaJson == null || areaJson.length() <= 0) {
            return district;
        }
        Filterss areaList = JsonUtil.getTopAreas(areaJson);
        for (int i = 0; i < areaList.getLabelsList().size(); i++) {
            if (location.subCityName.startsWith(areaList.getLabelsList().get(i).getLabel())) {
                return areaList.getValuesList().get(i).getValue();
            }
        }
        return district;
    }

    private void handleGetMetaMsgBack(String rawData) {
        if (rawData != null) {
            LinkedHashMap<String, PostGoodsBean> pl = JsonUtil.getPostGoodsBean(rawData);
            if (pl == null || pl.size() == 0) {
                sendMessage(-10, null);
                return;
            }
            Util.saveJsonAndTimestampToLocate(GlobalDataManager.getInstance().getApplicationContext(), this.category + this.city, rawData, System.currentTimeMillis() / 1000);
            sendMessage(PostCommonValues.MSG_GET_META_SUCCEED, pl);
            return;
        }
        sendMessage(PostCommonValues.MSG_GET_META_FAIL, null);
    }

    public void onNetworkDone(String apiName, String responseData) {
        if (apiName.equals("Meta.post/")) {
            handleGetMetaMsgBack(responseData);
            return;
        }
        PostResultData data = new PostResultData();
        data.message = "发布成功";
        data.error = 0;
        try {
            data.id = new JSONObject(responseData).getJSONObject("result").getString(LocaleUtil.INDONESIAN);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendMessage(PostCommonValues.MSG_POST_SUCCEED, data);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:12:0x0031 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.baixing.util.post.PostNetworkService$PostResultData} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onNetworkFail(java.lang.String r4, com.baixing.network.api.ApiError r5) {
        /*
            r3 = this;
            r0 = 0
            if (r5 == 0) goto L_0x0026
            com.baixing.util.post.PostNetworkService$PostResultData r0 = new com.baixing.util.post.PostNetworkService$PostResultData
            r0.<init>()
            java.lang.String r2 = r5.getErrorCode()
            if (r2 == 0) goto L_0x001c
            java.lang.String r2 = r5.getErrorCode()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r2 = r2.intValue()
            r0.error = r2
        L_0x001c:
            java.lang.String r2 = r5.getMsg()
            if (r2 != 0) goto L_0x0039
            java.lang.String r2 = "网络错误"
        L_0x0024:
            r0.message = r2
        L_0x0026:
            java.lang.String r2 = "Meta.post/"
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x003e
            r1 = -65263(0xffffffffffff0111, float:NaN)
        L_0x0031:
            if (r5 != 0) goto L_0x0035
            java.lang.String r0 = "网络错误"
        L_0x0035:
            r3.sendMessage(r1, r0)
            return
        L_0x0039:
            java.lang.String r2 = r5.getMsg()
            goto L_0x0024
        L_0x003e:
            r1 = -61678(0xffffffffffff0f12, float:NaN)
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.post.PostNetworkService.onNetworkFail(java.lang.String, com.baixing.network.api.ApiError):void");
    }

    public void onOutActionDone(int action, String data) {
        if (action == 286330624) {
            sendMessage(PostCommonValues.MSG_CHECK_QUOTA_AFTER_LOGIN, null);
        } else if (action == 286330626) {
            doPost();
        }
    }
}
