package com.baixing.util.post;

public class PostCommonValues {
    public static final int ACTION_POST_CHECK_QUOTA_OK = 286330626;
    public static final int ACTION_POST_NEED_LOGIN_DONE = 286330624;
    public static final int ACTION_POST_NEED_REVERIIFY = 286330625;
    public static final int HASH_CONTROL = "control".hashCode();
    public static final int HASH_POST_BEAN = "postBean".hashCode();
    public static final int MSG_ACCOUNT_CHECK_FAIL = -61671;
    public static final int MSG_CHECK_QUOTA_AFTER_LOGIN = -61663;
    public static final int MSG_GEOCODING_FETCHED = 65552;
    public static final int MSG_GET_META_FAIL = -65263;
    public static final int MSG_GET_META_SUCCEED = -65519;
    public static final int MSG_GPS_LOC_FETCHED = 66064;
    public static final int MSG_POST_EDIT_SUCCEED = -61664;
    public static final int MSG_POST_EXCEPTION = -61677;
    public static final int MSG_POST_FAIL = -61678;
    public static final int MSG_POST_LOGIN_DONE = -61675;
    public static final int MSG_POST_NEED_LOGIN = -61676;
    public static final int MSG_POST_NEED_REGISTER = -61672;
    public static final int MSG_POST_SUCCEED = -61679;
    public static final int MSG_VERIFY_FAIL = -61674;
    public static final String STRING_AREA = "地区";
    public static final String STRING_DESCRIPTION = "content";
    public static final String STRING_DETAIL_POSITION = "具体地点";
    public static final String[] fixedItemDisplayNames = {"", "描述", "价格", "联系电话", STRING_DETAIL_POSITION};
    public static final String[] fixedItemNames = {"images", "content", "价格", "contact", STRING_DETAIL_POSITION};
    public static final String[] hiddenItemNames = {"wanted"};
}
