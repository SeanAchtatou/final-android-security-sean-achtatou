package com.baixing.util.post;

import android.os.Handler;
import android.os.Message;
import com.baidu.mapapi.CoordinateConvert;
import com.baidu.mapapi.GeoPoint;
import com.baixing.data.GlobalDataManager;
import com.baixing.data.LocationManager;
import com.baixing.entity.BXLocation;
import com.baixing.util.LocationService;

public class PostLocationService implements LocationService.BXRgcListener, LocationManager.onLocationFetchedListener {
    private boolean gettingLocationFromBaidu = false;
    /* access modifiers changed from: private */
    public Handler handler;
    private boolean inreverse = false;

    public PostLocationService(Handler handler2) {
        this.handler = handler2;
    }

    public void start() {
        this.inreverse = false;
        GlobalDataManager.getInstance().getLocationManager().addLocationListener(this);
    }

    public void stop() {
        GlobalDataManager.getInstance().getLocationManager().removeLocationListener(this);
    }

    public boolean retreiveLocation(final String city, final String addr) {
        new Thread(new Runnable() {
            public void run() {
                BXLocation loc = LocationService.retreiveCoorFromGoogle(city + addr);
                Message msg = Message.obtain();
                msg.what = PostCommonValues.MSG_GEOCODING_FETCHED;
                msg.obj = loc;
                PostLocationService.this.handler.sendMessage(msg);
            }
        }).start();
        return true;
    }

    public void onRgcUpdated(BXLocation location) {
        if (this.gettingLocationFromBaidu) {
            if (this.inreverse || location == null || (location.subCityName != null && !location.subCityName.equals(""))) {
                Message msg = Message.obtain();
                msg.what = PostCommonValues.MSG_GEOCODING_FETCHED;
                msg.obj = location;
                this.handler.sendMessage(msg);
            } else {
                GeoPoint point = CoordinateConvert.bundleDecode(CoordinateConvert.fromWgs84ToBaidu(new GeoPoint((int) (((double) location.fLat) * 1000000.0d), (int) (((double) location.fLon) * 1000000.0d))));
                LocationService.getInstance().reverseGeocode((float) ((((double) point.getLatitudeE6()) * 1.0d) / 1000000.0d), (float) ((((double) point.getLongitudeE6()) * 1.0d) / 1000000.0d), this);
                this.inreverse = true;
            }
            this.gettingLocationFromBaidu = false;
        }
    }

    public void onLocationFetched(BXLocation location) {
    }

    public void onGeocodedLocationFetched(BXLocation location) {
        if (location != null) {
            Message msg = Message.obtain();
            msg.what = PostCommonValues.MSG_GPS_LOC_FETCHED;
            msg.obj = location;
            this.handler.sendMessage(msg);
        }
    }
}
