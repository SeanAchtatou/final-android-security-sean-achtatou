package com.baixing.util.post;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.baixing.data.GlobalDataManager;
import com.baixing.imageCache.ImageLoaderManager;
import com.baixing.network.api.FileUploadCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.BitmapUtils;
import com.baixing.util.MobileConfig;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageUploader implements ImageLoaderManager.DownloadCallback {
    private static ImageUploader instance;
    /* access modifiers changed from: private */
    public Activity activity;
    private List<StateImage> imageList = new ArrayList();

    public interface Callback {
        void onUploadDone(String str, String str2, Bitmap bitmap);

        void onUploadFail(String str, Bitmap bitmap);

        void onUploading(String str, Bitmap bitmap);
    }

    private enum ImageState {
        SYNC,
        FAIL,
        UPLOADING,
        DOWNLOADING
    }

    private class StateImage {
        public List<Callback> callbacks;
        public String imagePath;
        public String serverUrl;
        public ImageState state;
        public Bitmap thumbnail;

        private StateImage() {
            this.state = ImageState.FAIL;
            this.callbacks = new ArrayList();
        }
    }

    private ImageUploader() {
    }

    public static ImageUploader getInstance() {
        if (instance == null) {
            instance = new ImageUploader();
        }
        return instance;
    }

    public void attachActivity(Activity activity2) {
        this.activity = activity2;
    }

    private StateImage findImage(String imagePath) {
        synchronized (this.imageList) {
            for (StateImage img : this.imageList) {
                if (img.imagePath.equals(imagePath)) {
                    return img;
                }
            }
            return null;
        }
    }

    public void removeCallback(Callback callback) {
        if (callback != null) {
            synchronized (this.imageList) {
                for (StateImage img : this.imageList) {
                    int i = 0;
                    while (i < img.callbacks.size()) {
                        if (callback.equals(img.callbacks.get(i))) {
                            img.callbacks.remove(i);
                        } else {
                            i++;
                        }
                    }
                }
            }
        }
    }

    public boolean hasPendingJob() {
        for (StateImage img : this.imageList) {
            if (img.state != ImageState.SYNC && img.state != ImageState.FAIL) {
                return true;
            }
        }
        return false;
    }

    public List<String> getServerUrlList() {
        ArrayList<String> array = new ArrayList<>();
        for (StateImage img : this.imageList) {
            if (img.state == ImageState.SYNC) {
                array.add(img.serverUrl);
            }
        }
        return array;
    }

    public void clearAll() {
        for (StateImage img : this.imageList) {
            img.callbacks.clear();
        }
        this.imageList.clear();
    }

    public void addDownloadImage(String thumbnailUrl, String url, Callback callback) {
        if (findImage(thumbnailUrl) == null) {
            synchronized (this.imageList) {
                try {
                    StateImage image = new StateImage();
                    try {
                        image.imagePath = thumbnailUrl;
                        image.serverUrl = url;
                        image.state = ImageState.DOWNLOADING;
                        if (callback != null) {
                            image.callbacks.add(callback);
                        }
                        this.imageList.add(image);
                        GlobalDataManager.getInstance().getImageLoaderMgr().loadImg(this, thumbnailUrl);
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public void startUpload(String imagePath, Bitmap thumbnail, Callback callback) {
        if (findImage(imagePath) == null) {
            synchronized (this.imageList) {
                try {
                    StateImage image = new StateImage();
                    try {
                        image.imagePath = imagePath;
                        image.thumbnail = thumbnail;
                        if (callback != null) {
                            image.callbacks.add(callback);
                        }
                        this.imageList.add(image);
                        appendJob(image);
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        }
    }

    public Bitmap getThumbnail(String imagePath) {
        StateImage img = findImage(imagePath);
        if (img == null) {
            return null;
        }
        return img.thumbnail;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0022, code lost:
        if (r2 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0024, code lost:
        appendJob(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void registerCallback(java.lang.String r6, com.baixing.util.post.ImageUploader.Callback r7) {
        /*
            r5 = this;
            r2 = 0
            r0 = 0
            java.util.List<com.baixing.util.post.ImageUploader$StateImage> r4 = r5.imageList
            monitor-enter(r4)
            com.baixing.util.post.ImageUploader$StateImage r0 = r5.findImage(r6)     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x0028
            r2 = 1
            com.baixing.util.post.ImageUploader$StateImage r1 = new com.baixing.util.post.ImageUploader$StateImage     // Catch:{ all -> 0x003b }
            r3 = 0
            r1.<init>()     // Catch:{ all -> 0x003b }
            r1.imagePath = r6     // Catch:{ all -> 0x003e }
            if (r7 == 0) goto L_0x001b
            java.util.List<com.baixing.util.post.ImageUploader$Callback> r3 = r1.callbacks     // Catch:{ all -> 0x003e }
            r3.add(r7)     // Catch:{ all -> 0x003e }
        L_0x001b:
            java.util.List<com.baixing.util.post.ImageUploader$StateImage> r3 = r5.imageList     // Catch:{ all -> 0x003e }
            r3.add(r1)     // Catch:{ all -> 0x003e }
            r0 = r1
        L_0x0021:
            monitor-exit(r4)     // Catch:{ all -> 0x003b }
            if (r2 == 0) goto L_0x0027
            r5.appendJob(r0)
        L_0x0027:
            return
        L_0x0028:
            if (r7 == 0) goto L_0x0037
            java.util.List<com.baixing.util.post.ImageUploader$Callback> r3 = r0.callbacks     // Catch:{ all -> 0x003b }
            boolean r3 = r3.contains(r7)     // Catch:{ all -> 0x003b }
            if (r3 != 0) goto L_0x0037
            java.util.List<com.baixing.util.post.ImageUploader$Callback> r3 = r0.callbacks     // Catch:{ all -> 0x003b }
            r3.add(r7)     // Catch:{ all -> 0x003b }
        L_0x0037:
            r5.notifyState(r7, r0)     // Catch:{ all -> 0x003b }
            goto L_0x0021
        L_0x003b:
            r3 = move-exception
        L_0x003c:
            monitor-exit(r4)     // Catch:{ all -> 0x003b }
            throw r3
        L_0x003e:
            r3 = move-exception
            r0 = r1
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.util.post.ImageUploader.registerCallback(java.lang.String, com.baixing.util.post.ImageUploader$Callback):void");
    }

    private void appendJob(final StateImage img) {
        new Thread(new Runnable() {
            public void run() {
                if (img.thumbnail == null) {
                    img.thumbnail = BitmapUtils.createThumbnail(img.imagePath, 200, 200);
                }
                img.state = ImageState.UPLOADING;
                ImageUploader.this.notifyState(img);
                if (img.imagePath != null) {
                    String url = null;
                    String failReason = null;
                    try {
                        long startTime = System.currentTimeMillis();
                        String result = FileUploadCommand.create(img.imagePath).doUpload(ImageUploader.this.activity, MobileConfig.getInstance().getImageConfig());
                        long timeInMill = System.currentTimeMillis() - startTime;
                        url = result;
                        if (TextUtils.isEmpty(url)) {
                            failReason = "url of json string in response is null";
                        }
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.POST_IMAGEUPLOAD).append(TrackConfig.TrackMobile.Key.RESULT, url != null ? TrackConfig.TrackMobile.Value.YES : TrackConfig.TrackMobile.Value.NO).append(TrackConfig.TrackMobile.Key.FAIL_REASON, failReason).append(TrackConfig.TrackMobile.Key.SIZEINBYTES, new File(img.imagePath).length()).append(TrackConfig.TrackMobile.Key.UPLOADSECONDS, ((double) timeInMill) / 1000.0d).end();
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.POST_IMAGEUPLOAD).append(TrackConfig.TrackMobile.Key.RESULT, 0 != 0 ? TrackConfig.TrackMobile.Value.YES : TrackConfig.TrackMobile.Value.NO).append(TrackConfig.TrackMobile.Key.FAIL_REASON, (String) null).append(TrackConfig.TrackMobile.Key.SIZEINBYTES, new File(img.imagePath).length()).append(TrackConfig.TrackMobile.Key.UPLOADSECONDS, ((double) 0) / 1000.0d).end();
                        throw th2;
                    }
                    if (!TextUtils.isEmpty(url)) {
                        img.state = ImageState.SYNC;
                        img.serverUrl = url;
                        ImageUploader.this.notifyState(img);
                        return;
                    }
                    img.state = ImageState.FAIL;
                    ImageUploader.this.notifyFail(img);
                    return;
                }
                img.state = ImageState.FAIL;
                ImageUploader.this.notifyFail(img);
            }
        }).start();
    }

    private void notifyState(Callback callback, StateImage img) {
        switch (img.state) {
            case UPLOADING:
            case DOWNLOADING:
                callback.onUploading(img.imagePath, img.thumbnail);
                return;
            case SYNC:
                callback.onUploadDone(img.imagePath, img.serverUrl, img.thumbnail);
                return;
            case FAIL:
                callback.onUploadFail(img.imagePath, img.thumbnail);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void notifyState(StateImage img) {
        for (Callback callback : img.callbacks) {
            if (callback != null) {
                notifyState(callback, img);
            }
        }
    }

    public void cancel(String imagePath) {
        StateImage img;
        synchronized (this.imageList) {
            img = findImage(imagePath);
            if (img != null) {
                this.imageList.remove(img);
            }
        }
        if (img != null) {
            notifyFail(img);
        }
    }

    /* access modifiers changed from: private */
    public void notifyFail(StateImage img) {
        for (Callback callback : img.callbacks) {
            if (callback != null) {
                callback.onUploadFail(img.imagePath, img.thumbnail);
            }
        }
    }

    public void onFail(String url) {
        StateImage img = findImage(url);
        if (img != null) {
            img.state = ImageState.FAIL;
            notifyFail(img);
        }
    }

    public void onSucced(String url, Bitmap bp) {
        StateImage img = findImage(url);
        if (img != null) {
            img.state = ImageState.SYNC;
            img.thumbnail = bp.copy(bp.getConfig(), true);
            notifyState(img);
        }
    }
}
