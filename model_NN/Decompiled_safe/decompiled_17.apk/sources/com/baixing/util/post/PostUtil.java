package com.baixing.util.post;

import android.content.Context;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baixing.entity.Ad;
import com.baixing.entity.BXLocation;
import com.baixing.entity.PostGoodsBean;
import com.baixing.view.fragment.MultiLevelSelectionFragment;
import com.baixing.view.fragment.PostParamsHolder;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.util.List;

public class PostUtil {
    public static String getDetailValue(PostGoodsBean bean, Ad detail, String detailKey) {
        int pos;
        String _sValue;
        if (bean == null || detail == null || detailKey == null || detailKey.equals("")) {
            return "";
        }
        String value = detail.getValueByKey(detailKey);
        String detailValue = "";
        if (bean.getControlType().equals("input") || bean.getControlType().equals("textarea")) {
            String detailValue2 = detail.getValueByKey(detailKey);
            if (detailValue2 == null || bean.getUnit().equals("") || (pos = detailValue2.lastIndexOf(bean.getUnit())) == -1) {
                return detailValue2;
            }
            return detailValue2.substring(0, pos);
        } else if (!bean.getControlType().equals("select") && !bean.getControlType().equals("checkbox")) {
            return detailValue;
        } else {
            List<String> beanLs = bean.getLabels();
            if (beanLs != null) {
                int t = 0;
                while (true) {
                    if (t >= beanLs.size()) {
                        break;
                    }
                    if (bean.getControlType().equals("checkbox") && bean.getLabels() != null && bean.getLabels().size() > 1 && value.contains(beanLs.get(t))) {
                        detailValue = detailValue + (detailValue.equals("") ? "" : ",") + bean.getValues().get(t);
                    } else if (beanLs.get(t).equals(value)) {
                        detailValue = bean.getValues().get(t);
                        break;
                    }
                    t++;
                }
            }
            if (!detailValue.equals("") || (_sValue = detail.getValueByKey(detailKey + "_s")) == null || _sValue.equals("")) {
                return detailValue;
            }
            return _sValue;
        }
    }

    public static void extractInputData(ViewGroup vg, PostParamsHolder params) {
        CheckBox box;
        if (vg != null) {
            String title = ((TextView) vg.getRootView().findViewById(R.id.tv_title_post)).getText().toString();
            params.put(Constants.PARAM_TITLE, title, title);
            for (int i = 0; i < vg.getChildCount(); i++) {
                PostGoodsBean postGoodsBean = (PostGoodsBean) vg.getChildAt(i).getTag(PostCommonValues.HASH_POST_BEAN);
                if (postGoodsBean != null) {
                    if (postGoodsBean.getControlType().equals("input") || postGoodsBean.getControlType().equals("textarea")) {
                        EditText et = (EditText) vg.getChildAt(i).getTag(PostCommonValues.HASH_CONTROL);
                        if (!(et == null || et.getText() == null)) {
                            params.put(postGoodsBean.getName(), et.getText().toString(), et.getText().toString());
                        }
                    } else if (postGoodsBean.getControlType().equals("checkbox") && postGoodsBean.getValues().size() == 1 && (box = (CheckBox) vg.getChildAt(i).getTag(PostCommonValues.HASH_CONTROL)) != null) {
                        if (box.isChecked()) {
                            params.put(postGoodsBean.getName(), postGoodsBean.getValues().get(0), postGoodsBean.getValues().get(0));
                        } else {
                            params.remove(postGoodsBean.getName());
                        }
                    }
                }
            }
        }
    }

    public static boolean fetchResultFromViewBack(int message, Object obj, ViewGroup vg, PostParamsHolder params) {
        if (vg == null) {
            return false;
        }
        boolean match = false;
        for (int i = 0; i < vg.getChildCount(); i++) {
            View v = vg.getChildAt(i);
            PostGoodsBean bean = (PostGoodsBean) v.getTag(PostCommonValues.HASH_POST_BEAN);
            if (bean != null && bean.getName().hashCode() == message) {
                TextView tv = (TextView) v.getTag(PostCommonValues.HASH_CONTROL);
                if (obj instanceof Integer) {
                    String txt = bean.getLabels().get(((Integer) obj).intValue());
                    String txtValue = bean.getValues().get(((Integer) obj).intValue());
                    if (tv != null) {
                        tv.setText(txt);
                    }
                    match = true;
                    params.put(bean.getName(), txt, txtValue);
                } else if (obj instanceof String) {
                    String[] checks = ((String) obj).split(",");
                    String value = "";
                    String txt2 = "";
                    for (int t = 0; t < checks.length; t++) {
                        if (!checks[t].equals("")) {
                            txt2 = txt2 + "," + bean.getLabels().get(Integer.parseInt(checks[t]));
                            value = value + "," + bean.getValues().get(Integer.parseInt(checks[t]));
                        }
                    }
                    if (txt2.length() > 0) {
                        txt2 = txt2.substring(1);
                    }
                    if (value.length() > 0) {
                        value = value.substring(1);
                    }
                    if (tv != null) {
                        tv.setText(txt2);
                    }
                    match = true;
                    params.put(bean.getName(), txt2, value);
                } else if (obj instanceof MultiLevelSelectionFragment.MultiLevelItem) {
                    if (tv != null) {
                        tv.setText(((MultiLevelSelectionFragment.MultiLevelItem) obj).txt);
                    }
                    match = true;
                    params.put(bean.getName(), ((MultiLevelSelectionFragment.MultiLevelItem) obj).txt, ((MultiLevelSelectionFragment.MultiLevelItem) obj).id);
                }
            }
        }
        return match;
    }

    public static class BorderChangeListener implements View.OnFocusChangeListener {
        private Context context;
        private View rootView;

        public BorderChangeListener(Context context2, View root) {
            this.context = context2;
            this.rootView = root;
        }

        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                this.rootView.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.post_box_focus));
            } else {
                this.rootView.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.post_box));
            }
        }
    }

    public static ViewGroup createItemByPostBean(PostGoodsBean postBean, Context context) {
        View v;
        if (postBean.getControlType().equals("input")) {
            LayoutInflater inflater = LayoutInflater.from(context);
            if (postBean.getName().equals(PostCommonValues.STRING_DETAIL_POSITION)) {
                v = inflater.inflate((int) R.layout.item_post_location, (ViewGroup) null);
            } else {
                v = inflater.inflate((int) R.layout.item_post_edit, (ViewGroup) null);
            }
            ((TextView) v.findViewById(R.id.postshow)).setText(postBean.getDisplayName());
            EditText text = (EditText) v.findViewById(R.id.postinput);
            text.setOnFocusChangeListener(new BorderChangeListener(context, v));
            int maxLen = postBean.getMaxlength();
            if (maxLen > 0) {
                text.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLen)});
            }
            v.setTag(PostCommonValues.HASH_POST_BEAN, postBean);
            v.setTag(PostCommonValues.HASH_CONTROL, text);
            if (postBean.getNumeric() != 0) {
                text.setInputType(12290);
            }
            if (postBean.getName().equals("contact")) {
                text.setInputType(3);
            }
            if (postBean.getUnit() != null && !postBean.getUnit().equals("")) {
                ((TextView) v.findViewById(R.id.postunit)).setText(postBean.getUnit());
            }
            return (ViewGroup) v;
        } else if (postBean.getControlType().equals("select")) {
            View v2 = LayoutInflater.from(context).inflate((int) R.layout.item_post_select, (ViewGroup) null);
            ((TextView) v2.findViewById(R.id.postshow)).setText(postBean.getDisplayName());
            v2.setTag(PostCommonValues.HASH_POST_BEAN, postBean);
            v2.setTag(PostCommonValues.HASH_CONTROL, v2.findViewById(R.id.posthint));
            return (ViewGroup) v2;
        } else if (postBean.getControlType().equals("checkbox")) {
            LayoutInflater inflater2 = LayoutInflater.from(context);
            if (postBean.getLabels().size() > 1) {
                View v3 = inflater2.inflate((int) R.layout.item_post_select, (ViewGroup) null);
                ((TextView) v3.findViewById(R.id.postshow)).setText(postBean.getDisplayName());
                v3.setTag(PostCommonValues.HASH_POST_BEAN, postBean);
                v3.setTag(PostCommonValues.HASH_CONTROL, v3.findViewById(R.id.posthint));
                return (ViewGroup) v3;
            }
            View v4 = inflater2.inflate((int) R.layout.item_text_checkbox, (ViewGroup) null);
            v4.findViewById(R.id.divider).setVisibility(8);
            ((TextView) v4.findViewById(R.id.checktext)).setText(postBean.getDisplayName());
            v4.findViewById(R.id.checkitem).setTag(postBean.getDisplayName());
            v4.setTag(PostCommonValues.HASH_POST_BEAN, postBean);
            v4.setTag(PostCommonValues.HASH_CONTROL, v4.findViewById(R.id.checkitem));
            return (ViewGroup) v4;
        } else if (!postBean.getControlType().equals("textarea")) {
            return null;
        } else {
            View v5 = LayoutInflater.from(context).inflate((int) R.layout.item_post_description, (ViewGroup) null);
            ((TextView) v5.findViewById(R.id.postdescriptionshow)).setText(postBean.getDisplayName());
            EditText descriptionEt = (EditText) v5.findViewById(R.id.postdescriptioninput);
            descriptionEt.setOnFocusChangeListener(new BorderChangeListener(context, v5));
            int maxLen2 = postBean.getMaxlength();
            if (maxLen2 > 0) {
                descriptionEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLen2)});
            }
            v5.setTag(PostCommonValues.HASH_POST_BEAN, postBean);
            v5.setTag(PostCommonValues.HASH_CONTROL, descriptionEt);
            return (ViewGroup) v5;
        }
    }

    public static boolean inArray(String item, String[] array) {
        for (String equals : array) {
            if (item.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    public static void adjustMarginBottomAndHeight(View view) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new LinearLayout.LayoutParams(-1, -1);
        }
        layoutParams.bottomMargin = view.getContext().getResources().getDimensionPixelOffset(R.dimen.post_marginbottom);
        layoutParams.height = view.getResources().getDimensionPixelOffset(R.dimen.post_item_height);
        view.setLayoutParams(layoutParams);
    }

    public static String getLocationSummary(BXLocation location) {
        String address = (location.detailAddress == null || location.detailAddress.equals("")) ? (location.subCityName == null || location.subCityName.equals("")) ? "" : location.subCityName : location.detailAddress;
        if (address == null || address.length() == 0) {
            return "";
        }
        if (location.adminArea != null && location.adminArea.length() > 0) {
            address = address.replaceFirst(location.adminArea, "");
        }
        if (location.cityName != null && location.cityName.length() > 0) {
            address = address.replaceFirst(location.cityName, "");
        }
        return address;
    }
}
