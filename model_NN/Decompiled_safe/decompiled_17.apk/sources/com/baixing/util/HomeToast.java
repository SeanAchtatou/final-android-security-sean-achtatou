package com.baixing.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import com.devspark.appmsg.AppMsg;
import com.quanleimu.activity.R;

public class HomeToast {
    public static void show(Context context, LayoutInflater inflater, String tip, boolean isLongTime) {
        AppMsg appMsg = AppMsg.makeText((Activity) context, tip, new AppMsg.Style(isLongTime ? AppMsg.LENGTH_LONG : AppMsg.LENGTH_SHORT, R.color.toast_info_bg));
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2, 4);
        layoutParams.topMargin = (int) context.getResources().getDimension(R.dimen.title_height);
        appMsg.setLayoutParams(layoutParams);
        appMsg.show();
    }
}
