package com.baixing.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.baixing.activity.BaiduMapActivity;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.EntryApplication;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.entity.Ad;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.post.PostCommonValues;
import com.baixing.view.fragment.FeedbackFragment;
import com.baixing.widget.CommentsDialog;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import org.apache.commons.httpclient.HttpState;
import org.json.JSONException;
import org.json.JSONObject;

public class ViewUtil {
    private static boolean commentsDlgShowed = false;

    public static void postShortToastMessage(final View parent, final String message, long delay) {
        long delayTime = 0;
        if (parent != null && message != null) {
            if (delay > 0) {
                delayTime = delay;
            }
            parent.postDelayed(new Runnable() {
                public void run() {
                    Toast t = Toast.makeText(parent.getContext(), message, 0);
                    t.setGravity(17, 0, 0);
                    t.show();
                }
            }, delayTime);
        }
    }

    public static void postShortToastMessage(final View parent, final int resId, long delay) {
        long delayTime = 0;
        if (parent != null) {
            if (delay > 0) {
                delayTime = delay;
            }
            parent.postDelayed(new Runnable() {
                public void run() {
                    Toast t = Toast.makeText(parent.getContext(), resId, 0);
                    t.setGravity(17, 0, 0);
                    t.show();
                }
            }, delayTime);
        }
    }

    public static void removeNotification(Context context, int notificationid) {
        ((PowerManager) context.getSystemService("power")).newWakeLock(26, "NOTIFY_WAKEUP").acquire(1000);
        ((NotificationManager) context.getSystemService("notification")).cancel(notificationid);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void putOrUpdateNotification(Context context, int notificationId, String notificationType, String title, String msg, Bundle extras, boolean isPersistant) {
        String contentTitle;
        Intent notificationIntent;
        ((PowerManager) context.getSystemService("power")).newWakeLock(26, "NOTIFY_WAKEUP").acquire(1000);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService("notification");
        String tickerText = msg;
        if (title == null) {
            contentTitle = "";
        } else {
            contentTitle = title;
        }
        String contentText = msg;
        Notification notification = new Notification(R.drawable.push_icon, tickerText, System.currentTimeMillis());
        notification.defaults |= 1;
        notification.defaults |= 2;
        if (isPersistant) {
            notification.flags |= 32;
        } else {
            notification.flags |= 16;
        }
        if (notificationType == null) {
            notificationIntent = new Intent();
        } else {
            notificationIntent = new Intent(notificationType);
        }
        if (notificationType.equals("android.intent.action.VIEW") && extras != null) {
            try {
                notificationIntent.setData(Uri.parse(new JSONObject(extras.getString(ThirdpartyTransitActivity.Key_Data)).getString(Constants.PARAM_URL)));
                notification.setLatestEventInfo(context, contentTitle, contentText, PendingIntent.getActivity(context, 0, notificationIntent, 0));
                mNotificationManager.notify(notificationId, notification);
                return;
            } catch (JSONException e) {
                Log.e("", "viewutil jsonexception");
                e.printStackTrace();
            }
        }
        if (extras != null) {
            notificationIntent.putExtras(extras);
        }
        if (notificationId == 3001) {
            notificationIntent.putExtra("pagejump", true);
            EntryApplication.pushViewed = false;
        }
        notification.setLatestEventInfo(context, contentTitle, contentText, PendingIntent.getBroadcast(context, 0, notificationIntent, 134217728));
        mNotificationManager.notify(notificationId, notification);
    }

    public static void startMapForAds(Context context, Ad ad) {
        if (ad == null) {
            Toast.makeText(context, "无信息无法显示地图", 1).show();
            return;
        }
        BaseActivity baseActivity = (BaseActivity) context;
        if (baseActivity == null) {
            Toast.makeText(context, "显示地图失败", 0).show();
        } else if (Build.VERSION.SDK_INT > 16) {
            String latV = ad.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_LAT);
            String lonV = ad.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_LON);
            String query = null;
            if (latV == null || latV.equals(HttpState.PREEMPTIVE_DEFAULT) || latV.equals("") || latV.equals("0") || lonV == null || lonV.equals(HttpState.PREEMPTIVE_DEFAULT) || lonV.equals("") || lonV.equals("0")) {
                String area = ad.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_AREANAME);
                String address = ad.getValueByKey(PostCommonValues.STRING_DETAIL_POSITION);
                if (address != null && address.trim().length() > 0) {
                    query = address.trim();
                } else if (area != null && area.trim().length() > 0) {
                    query = area.trim();
                }
            } else {
                query = latV + "," + lonV;
            }
            if (query != null) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("http://maps.google.com/?q=" + query));
                context.startActivity(intent);
            }
        } else {
            Bundle bundle = new Bundle();
            bundle.putSerializable("detail", ad);
            baseActivity.getIntent().putExtras(bundle);
            baseActivity.getIntent().setClass(baseActivity, BaiduMapActivity.class);
            baseActivity.startActivity(baseActivity.getIntent());
        }
    }

    public static Bitmap createThumbnail(Bitmap srcBmp, int thumbHeight) {
        return Bitmap.createScaledBitmap(srcBmp, (int) (((float) thumbHeight) * Float.valueOf(Float.valueOf((float) srcBmp.getWidth()).floatValue() / Float.valueOf((float) srcBmp.getHeight()).floatValue()).floatValue()), thumbHeight, true);
    }

    public static ProgressDialog showSimpleProgress(Context context) {
        ProgressDialog pd = ProgressDialog.show(context, context.getString(R.string.dialog_title_info), context.getString(R.string.dialog_message_waiting));
        pd.setCancelable(true);
        pd.setCanceledOnTouchOutside(true);
        return pd;
    }

    public static void hideSimpleProgress(ProgressDialog pd) {
        if (pd != null) {
            pd.dismiss();
        }
    }

    public static void showToast(final Context context, final String msg, final boolean longDuration) {
        if (context != null && msg != null) {
            Runnable task = new Runnable() {
                public void run() {
                    int i;
                    Context context = context;
                    String str = msg;
                    if (longDuration) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    Toast t = Toast.makeText(context, str, i);
                    t.setGravity(17, 0, 0);
                    t.show();
                }
            };
            if (context instanceof Activity) {
                ((Activity) context).runOnUiThread(task);
            } else {
                task.run();
            }
        }
    }

    public static void showCommentsPromptDialog(final BaseActivity activity) {
        if (!commentsDlgShowed) {
            commentsDlgShowed = true;
            new AlertDialog.Builder(activity).setTitle("百姓网好用么？").setMessage("感谢您使用了这么久百姓网，不知道您的感受如何？您的反馈是我们不断为您改进的动力").setPositiveButton("很爽，赞一下", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    new CommentsDialog(activity).show();
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REVIEW_ACTION).append(TrackConfig.TrackMobile.Key.ACTION, "appMarket").end();
                }
            }).setNegativeButton("不爽，我要告状", new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
                 arg types: [com.baixing.view.fragment.FeedbackFragment, android.os.Bundle, int]
                 candidates:
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    Bundle arg = new Bundle();
                    arg.putString(BaseFragment.ARG_COMMON_TITLE, "反馈信息");
                    activity.pushFragment((BaseFragment) new FeedbackFragment(), arg, false);
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REVIEW_ACTION).append(TrackConfig.TrackMobile.Key.ACTION, "feedback").end();
                }
            }).create().show();
        }
    }
}
