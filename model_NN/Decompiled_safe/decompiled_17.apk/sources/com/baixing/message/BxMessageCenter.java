package com.baixing.message;

import java.util.Hashtable;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class BxMessageCenter {
    private static BxMessageCenter defaultCenter;
    private Map<String, Observable> observMapper = new Hashtable();

    public interface IBxNotification {
        Map getAdditionalInfo();

        String getName();

        Object getObject();
    }

    private BxMessageCenter() {
    }

    public static BxMessageCenter defaultMessageCenter() {
        if (defaultCenter == null) {
            defaultCenter = new BxMessageCenter();
        }
        return defaultCenter;
    }

    public void registerObserver(Observer observer, String notificationName) {
        Observable observ = this.observMapper.get(notificationName);
        if (observ == null) {
            observ = new InnerObservable();
            this.observMapper.put(notificationName, observ);
        }
        observ.addObserver(observer);
    }

    public void removeObserver(Observer observer) {
        for (Observable deleteObserver : this.observMapper.values()) {
            deleteObserver.deleteObserver(observer);
        }
    }

    public void removeObserver(Observer observer, String notificationName) {
        Observable observ = this.observMapper.get(notificationName);
        if (observ != null) {
            observ.deleteObserver(observer);
        }
    }

    public void postNotification(String name, Object obj) {
        postNotification(name, obj, null);
    }

    public void postNotification(String name, Object obj, Map additionalInfo) {
        postNotification(new DefaultNotification(name, obj, additionalInfo));
    }

    public void postNotification(IBxNotification notification) {
        Observable observ;
        if (notification != null && (observ = this.observMapper.get(notification.getName())) != null) {
            observ.notifyObservers(notification);
        }
    }

    private class InnerObservable extends Observable {
        private InnerObservable() {
        }

        public void notifyObservers() {
            setChanged();
            super.notifyObservers();
            clearChanged();
        }

        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
            clearChanged();
        }
    }

    private class DefaultNotification implements IBxNotification {
        private Map additionalInfo;
        private String notificationName;
        private Object obj;

        public DefaultNotification(String name, Object obj2, Map additonal) {
            this.notificationName = name;
            this.obj = obj2;
            this.additionalInfo = additonal;
        }

        public String getName() {
            return this.notificationName;
        }

        public Object getObject() {
            return this.obj;
        }

        public Map getAdditionalInfo() {
            return this.additionalInfo;
        }
    }
}
