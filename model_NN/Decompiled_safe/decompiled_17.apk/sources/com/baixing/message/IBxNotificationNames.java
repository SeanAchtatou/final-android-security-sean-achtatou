package com.baixing.message;

public interface IBxNotificationNames {
    public static final String NOTIFICATION_CONFIGURATION_UPDATE = "note.config.change";
    public static final String NOTIFICATION_FAV_ADDED = "note.addFav";
    public static final String NOTIFICATION_FAV_REMOVE = "note.remove";
    public static final String NOTIFICATION_LOGIN = "note.login";
    public static final String NOTIFICATION_LOGOUT = "note.logout";
    public static final String NOTIFICATION_NETWORK_CHANGE = "note.networkchange";
    public static final String NOTIFICATION_NEW_PASSWORD = "note.newpassword";
    public static final String NOTIFICATION_PROFILE_UPDATE = "note.profileupdate";
    public static final String NOTIFICATION_USER_CREATE = "note.user.created";
}
