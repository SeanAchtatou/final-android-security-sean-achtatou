package com.baixing.jsonutil;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AnalysisJson {
    public String result = "result";
    public String returncode = "returncode";
    public String returnmessage = "returnmessage";
    public String total = "total";

    public int getReturncode(String json) {
        try {
            try {
                return new JSONObject(json).getInt(this.returncode);
            } catch (JSONException e) {
                return -1;
            }
        } catch (JSONException e2) {
            return -1;
        }
    }

    public String getReturnmessage(String json) {
        try {
            try {
                return new JSONObject(json).getString(this.returnmessage);
            } catch (JSONException e) {
                return "";
            }
        } catch (JSONException e2) {
            return "";
        }
    }

    public int getTotal(String json) {
        try {
            try {
                return new JSONObject(json).getInt(this.total);
            } catch (JSONException e) {
                return -1;
            }
        } catch (JSONException e2) {
            return -1;
        }
    }

    public JSONArray getResult(String json) {
        try {
            try {
                return new JSONArray(new JSONObject(json).getString(this.result));
            } catch (JSONException e) {
                return null;
            }
        } catch (JSONException e2) {
            return null;
        }
    }

    public ArrayList<HashMap<String, String>> getAllData(String json, String[] valueKey) {
        ArrayList<HashMap<String, String>> listMap = new ArrayList<>();
        JSONArray array = getResult(json);
        if (array == null || array.length() == 0) {
            return null;
        }
        for (int i = 0; i < array.length(); i++) {
            try {
                HashMap<String, String> map = getMoreData(array.getString(i), valueKey);
                if (map != null) {
                    listMap.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return listMap;
    }

    public HashMap<String, String> getData(String json, String[] valueKey) {
        ArrayList<HashMap<String, String>> listMap = new ArrayList<>();
        JSONArray array = getResult(json);
        int i = 0;
        while (i < array.length()) {
            try {
                HashMap<String, String> map = getMoreData(array.getString(i), valueKey);
                if (map != null) {
                    listMap.add(map);
                }
                i++;
            } catch (JSONException e) {
            }
        }
        if (listMap.size() > 0) {
            return (HashMap) listMap.get(0);
        }
        return null;
    }

    public HashMap<String, String> getMoreData(String json, String[] valueKey) {
        String data;
        try {
            JSONObject obj = new JSONObject(json);
            try {
                HashMap<String, String> map = new HashMap<>();
                for (int i = 0; i < valueKey.length; i++) {
                    try {
                        data = obj.getString(valueKey[i]);
                        if (data == null || data.equals("")) {
                            data = "";
                        }
                    } catch (Exception e) {
                        try {
                            data = obj.getInt(valueKey[i]) + "";
                        } catch (Exception e2) {
                            try {
                                data = obj.getBoolean(valueKey[i]) + "";
                            } catch (Exception e3) {
                            }
                        }
                    }
                    map.put(valueKey[i], data);
                }
                return map;
            } catch (JSONException e4) {
                return null;
            }
        } catch (JSONException e5) {
            return null;
        }
    }

    public static String getShopMallTel(String json) {
        String a;
        try {
            JSONArray array = new JSONArray(json);
            if (array == null || array.length() == 0) {
                return "";
            }
            for (int i = 0; i < array.length(); i++) {
                try {
                    a = array.optJSONObject(i).getString("PhoneNumber");
                } catch (Exception e) {
                    a = "";
                }
                if (a != null) {
                    if (!a.equals("")) {
                        return a;
                    }
                }
            }
            return "";
        } catch (JSONException e1) {
            e1.printStackTrace();
            return "";
        }
    }

    public static String getShopMallImage(String json) {
        String a;
        try {
            JSONArray array = new JSONArray(json);
            if (array == null || array.length() == 0) {
                return "";
            }
            for (int i = 0; i < array.length(); i++) {
                try {
                    a = array.optJSONObject(i).getString("ImageUrl");
                } catch (Exception e) {
                    a = "";
                }
                if (a != null) {
                    if (!a.equals("")) {
                        return a;
                    }
                }
            }
            return "";
        } catch (JSONException e1) {
            e1.printStackTrace();
            return "";
        }
    }

    public static String getShopMallBrands(String json) {
        String a;
        String shopmallBrand = "";
        try {
            JSONArray array = new JSONArray(json);
            if (array == null || array.length() == 0) {
                return "";
            }
            for (int i = 0; i < array.length(); i++) {
                try {
                    a = array.optJSONObject(i).getString("BrandName");
                } catch (Exception e) {
                    a = "";
                }
                if (a != null) {
                    if (!a.equals("")) {
                        if (shopmallBrand.equals("")) {
                            shopmallBrand = a;
                        } else {
                            shopmallBrand = shopmallBrand + "," + a;
                        }
                    }
                }
            }
            return shopmallBrand;
        } catch (JSONException e1) {
            e1.printStackTrace();
            return shopmallBrand;
        }
    }
}
