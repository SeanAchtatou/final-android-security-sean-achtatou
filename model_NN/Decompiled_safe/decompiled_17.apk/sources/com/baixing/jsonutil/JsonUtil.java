package com.baixing.jsonutil;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.entity.Ad;
import com.baixing.entity.AdList;
import com.baixing.entity.Category;
import com.baixing.entity.ChatMessage;
import com.baixing.entity.CityDetail;
import com.baixing.entity.CityList;
import com.baixing.entity.Filters;
import com.baixing.entity.Filterss;
import com.baixing.entity.HotData;
import com.baixing.entity.HotList;
import com.baixing.entity.ImageList;
import com.baixing.entity.PostGoodsBean;
import com.baixing.entity.Quota;
import com.baixing.entity.labels;
import com.baixing.entity.values;
import com.baixing.network.api.ApiParams;
import com.baixing.util.post.PostCommonValues;
import com.devspark.appmsg.AppMsg;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.tencent.mm.sdk.message.RMsgInfoDB;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {
    public static CityList parseCityListFromJackson(String jsonData) {
        if (jsonData == null || jsonData.length() == 0) {
            return null;
        }
        CityList cityList = new CityList();
        List<CityDetail> lists = new ArrayList<>();
        try {
            JsonParser parser = new JsonFactory().createJsonParser(jsonData);
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                JsonToken nextToken = parser.nextToken();
                String fieldname = parser.getCurrentName();
                if (fieldname != null && fieldname.equals("result")) {
                    for (JsonToken jt = parser.nextToken(); jt != JsonToken.END_ARRAY; jt = parser.nextToken()) {
                        CityDetail cityDetail = new CityDetail();
                        JsonToken jt2 = parser.nextToken();
                        boolean hasCities = false;
                        while (jt2 != JsonToken.END_OBJECT) {
                            String key = parser.getCurrentName();
                            if (key == null) {
                                jt2 = parser.nextToken();
                            } else {
                                JsonToken jt3 = parser.nextToken();
                                String value = parser.getText();
                                if (key.equals(LocaleUtil.INDONESIAN)) {
                                    cityDetail.setId(value);
                                } else if (key.equals(BaseFragment.ARG_COMMON_TITLE)) {
                                    cityDetail.setName(value);
                                } else if (key.equals("englishName")) {
                                    cityDetail.setEnglishName(value);
                                } else if (key.equals("cities")) {
                                    hasCities = true;
                                    JsonToken jt4 = parser.nextToken();
                                    while (jt4 != JsonToken.END_ARRAY) {
                                        CityDetail childCityDetail = new CityDetail();
                                        JsonToken jt5 = parser.nextToken();
                                        while (jt5 != JsonToken.END_OBJECT) {
                                            String childKey = parser.getCurrentName();
                                            if (childKey == null) {
                                                jt5 = parser.nextToken();
                                            } else {
                                                JsonToken jt6 = parser.nextToken();
                                                String childVal = parser.getText();
                                                if (childKey.equals(LocaleUtil.INDONESIAN)) {
                                                    childCityDetail.setId(childVal);
                                                } else if (childKey.equals(BaseFragment.ARG_COMMON_TITLE)) {
                                                    childCityDetail.setName(childVal);
                                                } else if (childKey.equals("englishName")) {
                                                    childCityDetail.setEnglishName(childVal);
                                                }
                                                jt5 = parser.nextToken();
                                            }
                                        }
                                        childCityDetail.setSheng(cityDetail.getName());
                                        lists.add(childCityDetail);
                                        jt4 = parser.nextToken();
                                        Log.d("citylist", "city:" + childCityDetail.toString());
                                    }
                                }
                                jt2 = parser.nextToken();
                            }
                        }
                        if (!hasCities) {
                            cityDetail.setSheng("直辖市");
                            lists.add(cityDetail);
                            Log.d("citylist", "city:" + cityDetail.toString());
                        }
                    }
                }
            }
        } catch (JsonParseException e) {
        } catch (IOException e2) {
        } catch (Throwable t) {
            Log.d(JsonFactory.FORMAT_NAME_JSON, "unexpected json parse issue " + t);
        }
        cityList.setListDetails(lists);
        return cityList;
    }

    public static CityList parseCityListFromJson(String jsonData) {
        return parseCityListFromJackson(jsonData);
    }

    public static List<ChatMessage> parseChatMessagesByJackson(String msg) {
        JsonFactory factory = new JsonFactory();
        List<ChatMessage> list = new ArrayList<>();
        try {
            JsonParser parser = factory.createJsonParser(msg);
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String fieldname = parser.getCurrentName();
                if (fieldname != null && fieldname.equals(ThirdpartyTransitActivity.Key_Data)) {
                    JsonToken nextToken = parser.nextToken();
                    JsonToken jt = parser.nextToken();
                    while (jt != JsonToken.END_ARRAY) {
                        JsonToken jt2 = parser.nextToken();
                        ChatMessage chat = new ChatMessage();
                        while (jt2 != JsonToken.END_OBJECT) {
                            String fname = parser.getCurrentName();
                            if (fname == null) {
                                parser.nextToken();
                            } else {
                                jt2 = parser.nextToken();
                                String text = parser.getText();
                                if (fname.equals("u_id_from")) {
                                    chat.setFrom(text);
                                    jt2 = parser.nextToken();
                                } else if (fname.equals("u_id_to")) {
                                    chat.setTo(text);
                                    jt2 = parser.nextToken();
                                } else if (fname.equals(RMsgInfoDB.TABLE)) {
                                    chat.setMessage(text);
                                    jt2 = parser.nextToken();
                                } else if (fname.equals(ApiParams.KEY_TIMESTAMP)) {
                                    chat.setTimestamp(Long.valueOf(text).longValue());
                                    jt2 = parser.nextToken();
                                } else if (fname.equals("ad_id")) {
                                    chat.setAdId(text);
                                    jt2 = parser.nextToken();
                                } else if (fname.equals("session_id")) {
                                    chat.setSession(text);
                                    jt2 = parser.nextToken();
                                } else if (fname.equals(LocaleUtil.INDONESIAN)) {
                                    chat.setId(text);
                                    jt2 = parser.nextToken();
                                }
                            }
                        }
                        jt = parser.nextToken();
                        list.add(chat);
                    }
                }
            }
        } catch (JsonParseException | IOException e) {
        } catch (Throwable t) {
            Log.d(JsonFactory.FORMAT_NAME_JSON, "unexpected json parse issue " + t);
        }
        return list;
    }

    public static List<ChatMessage> parseChatMessages(JSONArray msgs) {
        List<ChatMessage> list = new ArrayList<>();
        int count = msgs.length();
        for (int i = 0; i < count; i++) {
            try {
                list.add(ChatMessage.fromJson(msgs.getJSONObject(i)));
            } catch (Throwable th) {
            }
        }
        return list;
    }

    public static List<HotList> parseCityHotFromJson(String jsonData) {
        List<HotList> listHot = new ArrayList<>();
        JSONArray jsonA = new JSONArray(jsonData);
        int i = 0;
        while (i < jsonA.length()) {
            HotList hotList = new HotList();
            JSONObject jsonObj = jsonA.getJSONObject(i);
            try {
                hotList.setImgUrl(jsonObj.getString("imgUrl"));
            } catch (Exception e1) {
                hotList.setImgUrl("");
                e1.printStackTrace();
            }
            try {
                hotList.setType(Integer.valueOf(jsonObj.getInt("type")).intValue());
            } catch (Exception e12) {
                hotList.setType(-1);
                e12.printStackTrace();
            }
            HotData hotData = new HotData();
            JSONObject jsonHotData = jsonObj.getJSONObject(ThirdpartyTransitActivity.Key_Data);
            try {
                if (jsonHotData.has("keyword")) {
                    hotData.setKeyword(jsonHotData.getString("keyword"));
                }
            } catch (Exception e) {
                hotData.setKeyword("");
                e.printStackTrace();
            }
            try {
                if (jsonHotData.has(Constants.PARAM_TITLE)) {
                    hotData.setTitle(jsonHotData.getString(Constants.PARAM_TITLE));
                }
            } catch (Exception e2) {
                hotData.setTitle("");
                e2.printStackTrace();
            }
            try {
                if (jsonHotData.has("weburl")) {
                    hotData.setWeburl(jsonHotData.getString("weburl"));
                }
            } catch (Exception e3) {
                hotData.setWeburl("");
                e3.printStackTrace();
            }
            try {
                hotList.setHotData(hotData);
                listHot.add(hotList);
                i++;
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
        }
        return listHot;
    }

    public static AdList getGoodsListFromJsonByJackson(String jsonData) {
        JsonToken jt;
        String value;
        JsonFactory factory = new JsonFactory();
        AdList goodsList = new AdList();
        List<Ad> list = new ArrayList<>();
        try {
            JsonParser parser = factory.createJsonParser(jsonData);
            Log.d("resData", jsonData);
            JsonToken nextToken = parser.nextToken();
            JsonToken jt2 = parser.nextToken();
            if (parser.getCurrentName().equals("result")) {
                JsonToken jt3 = parser.nextToken();
                while (jt3 != JsonToken.END_ARRAY && (jt = parser.nextToken()) != JsonToken.END_ARRAY) {
                    Ad detail = new Ad();
                    while (jt != JsonToken.END_OBJECT) {
                        String fname = parser.getCurrentName();
                        if (fname == null) {
                            jt = parser.nextToken();
                        } else if (fname.equals("images")) {
                            JsonToken jt4 = parser.nextToken();
                            ImageList il = new ImageList();
                            for (JsonToken jt5 = parser.nextToken(); jt5 != JsonToken.END_ARRAY; jt5 = parser.nextToken()) {
                                for (JsonToken jt6 = parser.nextToken(); jt6 != JsonToken.END_OBJECT; jt6 = parser.nextToken()) {
                                    String imgType = parser.getCurrentName();
                                    Log.d("MyAd", "imgType:" + imgType);
                                    JsonToken jt7 = parser.nextToken();
                                    String imgUrl = parser.getText();
                                    Log.d("MyAd", "imgUrl:" + imgUrl);
                                    if (imgUrl != null && imgUrl.length() > 0) {
                                        if (imgType.equals("big")) {
                                            il.setBig(imgUrl);
                                        } else if (imgType.equals("small")) {
                                            il.setSmall(imgUrl);
                                        } else if (imgType.equals("square_180")) {
                                            il.setSquare(imgUrl);
                                        }
                                    }
                                }
                            }
                            detail.setImageList(il);
                            Log.d("MyAd", "imglist:" + il.toString());
                            jt = parser.nextToken();
                        } else if (fname.equals("metaData")) {
                            JsonToken jt8 = parser.nextToken();
                            ArrayList<String> metas = new ArrayList<>();
                            for (JsonToken jt9 = parser.nextToken(); jt9 != JsonToken.END_OBJECT; jt9 = parser.nextToken()) {
                                String text = parser.getCurrentName();
                                JsonToken jt10 = parser.nextToken();
                                JsonToken jt11 = parser.nextToken();
                                while (jt11 != JsonToken.END_OBJECT) {
                                    String meta = parser.getCurrentName();
                                    JsonToken jt12 = parser.nextToken();
                                    if (meta != "label") {
                                        jt11 = parser.nextToken();
                                    } else {
                                        String data = parser.getText();
                                        Log.d("MyAd", "data:" + data);
                                        if (data != null && data.length() > 0) {
                                            text = text + " " + data;
                                        }
                                        jt11 = parser.nextToken();
                                    }
                                }
                                metas.add(text);
                            }
                            detail.setMetaData(metas);
                            jt = parser.nextToken();
                        } else if (fname.equals("user")) {
                            JsonToken jt13 = parser.nextToken();
                            JsonToken jt14 = parser.nextToken();
                            while (jt14 != JsonToken.END_OBJECT) {
                                String key = parser.getCurrentName();
                                if (key == null) {
                                    jt14 = parser.nextToken();
                                } else {
                                    JsonToken jt15 = parser.nextToken();
                                    if (key.equals(LocaleUtil.INDONESIAN)) {
                                        String value2 = parser.getText();
                                        if (value2 != null && value2.length() > 0) {
                                            detail.setValueByKey(ApiParams.KEY_USERID, value2.substring(1));
                                        }
                                    } else if (key.equals(BaseFragment.ARG_COMMON_TITLE)) {
                                        String value3 = parser.getText();
                                        if (value3 != null && value3.length() > 0) {
                                            detail.setValueByKey("userNick", value3);
                                        }
                                    } else if (key.equals("createdTime")) {
                                        String value4 = parser.getText();
                                        if (value4 != null && value4.length() > 0) {
                                            detail.setValueByKey("userCreatedTime", value4);
                                        }
                                    } else if (key.equals("mobile") && (value = parser.getText()) != null && value.length() > 0) {
                                        detail.setValueByKey(key, value);
                                    }
                                    jt14 = parser.nextToken();
                                }
                            }
                            jt = parser.nextToken();
                        } else if (fname.equals("areaNames")) {
                            JsonToken jt16 = parser.nextToken();
                            StringBuffer valueBuffer = new StringBuffer();
                            for (JsonToken jt17 = parser.nextToken(); jt17 != JsonToken.END_ARRAY; jt17 = parser.nextToken()) {
                                String val = parser.getText();
                                if (val != null && val.length() > 0) {
                                    valueBuffer.append(val);
                                    valueBuffer.append(",");
                                }
                            }
                            if (valueBuffer.length() > 0) {
                                detail.setValueByKey(fname, valueBuffer.substring(0, valueBuffer.length() - 1));
                            }
                            jt = parser.nextToken();
                        } else if (fname == "area") {
                            while (jt != JsonToken.END_OBJECT) {
                                jt = parser.nextToken();
                            }
                            jt = parser.nextToken();
                        } else {
                            JsonToken jt18 = parser.nextToken();
                            detail.setValueByKey(fname, parser.getText());
                            jt = parser.nextToken();
                        }
                    }
                    jt3 = parser.nextToken();
                    list.add(detail);
                }
            }
            parser.close();
        } catch (JsonParseException e) {
        } catch (IOException e2) {
        } catch (Throwable th) {
        }
        goodsList.setData(list);
        return goodsList;
    }

    public static AdList getGoodsListFromJson(String jsonData) {
        return getGoodsListFromJsonByJackson(jsonData);
    }

    public static Filterss getTopAreas(String jsonData) {
        Filterss areas = new Filterss();
        try {
            JSONArray jsonArray = new JSONObject(jsonData).getJSONArray("result");
            areas.setName("area");
            areas.setDisplayName(PostCommonValues.STRING_AREA);
            areas.setControlType("select");
            areas.setNumeric("0");
            areas.setRequired(true);
            areas.setLevelCount(2);
            List<values> valueList = new ArrayList<>();
            List<labels> labelList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject area = jsonArray.getJSONObject(i);
                values areaValues = new values();
                areaValues.setValue(area.getString(LocaleUtil.INDONESIAN));
                valueList.add(areaValues);
                labels areaLabels = new labels();
                areaLabels.setLabel(area.getString(BaseFragment.ARG_COMMON_TITLE));
                labelList.add(areaLabels);
            }
            areas.setValuesList(valueList);
            areas.setLabelsList(labelList);
            return areas;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static LinkedHashMap<String, Filterss> getAreas(String jsonData) {
        LinkedHashMap<String, Filterss> areas = new LinkedHashMap<>();
        try {
            JSONArray jsonArray = new JSONObject(jsonData).getJSONArray("result");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Filterss area = new Filterss();
                area.setControlType("select");
                area.setName(jsonObject.getString(LocaleUtil.INDONESIAN));
                area.setDisplayName(jsonObject.getString(BaseFragment.ARG_COMMON_TITLE));
                area.setNumeric("0");
                if (!jsonObject.isNull("children")) {
                    List<values> vList = new ArrayList<>();
                    List<labels> lList = new ArrayList<>();
                    JSONArray areaArray = jsonObject.getJSONArray("children");
                    if (areaArray != null && areaArray.length() > 0) {
                        for (int j = 0; j < areaArray.length(); j++) {
                            JSONObject areaObject = areaArray.getJSONObject(j);
                            values areaVal = new values();
                            areaVal.setValue(areaObject.getString(LocaleUtil.INDONESIAN));
                            vList.add(areaVal);
                            labels areaLab = new labels();
                            areaLab.setLabel(areaObject.getString(BaseFragment.ARG_COMMON_TITLE));
                            lList.add(areaLab);
                        }
                    }
                    area.setValuesList(vList);
                    area.setLabelsList(lList);
                }
                areas.put(area.getName(), area);
            }
            return areas;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Filters getFilters(String jsonData) {
        JSONArray selectDataArray;
        Filters filters = new Filters();
        List<Filterss> list = new ArrayList<>();
        JSONArray jsonArray = new JSONObject(jsonData).getJSONArray("result");
        int i = 0;
        while (i < jsonArray.length()) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            boolean isRange = false;
            Filterss mfil = new Filterss();
            mfil.setLevelCount(1);
            try {
                mfil.setName(jsonObject.getString(BaseFragment.ARG_COMMON_TITLE));
            } catch (Exception e) {
                mfil.setName("");
            }
            try {
                mfil.setDisplayName(jsonObject.getString("displayName"));
            } catch (Exception e2) {
                mfil.setDisplayName("");
            }
            try {
                String controlType = jsonObject.getString("controlType");
                mfil.setControlType(controlType);
                if (controlType.equals("FilterRangeSelect")) {
                    isRange = true;
                }
            } catch (Exception e3) {
                mfil.setControlType("");
            }
            try {
                ArrayList arrayList = new ArrayList();
                List<labels> llist = new ArrayList<>();
                try {
                    selectDataArray = jsonObject.getJSONArray("selectData");
                } catch (Exception e4) {
                    selectDataArray = null;
                }
                if (selectDataArray != null) {
                    for (int j = 0; j < selectDataArray.length(); j++) {
                        JSONObject vlObj = selectDataArray.getJSONObject(j);
                        if (!TextUtils.isEmpty(vlObj.getString("value"))) {
                            values mvalues = new values();
                            labels mlabels = new labels();
                            try {
                                mlabels.setLabel(vlObj.getString("label"));
                                if (isRange) {
                                    JSONArray rangePair = vlObj.getJSONArray("value");
                                    mvalues.setValue(rangePair.getString(0) + "," + (TextUtils.isEmpty(rangePair.getString(1)) ? "*" : rangePair.getString(1)));
                                    if (!"0".equals(rangePair.getString(0)) && !TextUtils.isEmpty(rangePair.getString(1)) && !mlabels.getLabel().endsWith(rangePair.getString(1)) && TextUtils.isEmpty(mfil.getUnit())) {
                                        mfil.setUnit(mlabels.getLabel().split(rangePair.getString(1))[1]);
                                    }
                                } else {
                                    mvalues.setValue(vlObj.getString("value"));
                                }
                            } catch (Exception e5) {
                                mvalues.setValue("");
                                mlabels.setLabel("");
                            }
                            arrayList.add(mvalues);
                            llist.add(mlabels);
                        }
                    }
                    mfil.setValuesList(arrayList);
                    mfil.setLabelsList(llist);
                }
                try {
                    mfil.setRequired(jsonObject.getBoolean("required"));
                } catch (Exception e6) {
                    mfil.setRequired(false);
                    e6.printStackTrace();
                }
                list.add(mfil);
                i++;
            } catch (JSONException e7) {
                e7.printStackTrace();
            }
        }
        filters.setFilterssList(list);
        return filters;
    }

    public static LinkedHashMap<String, PostGoodsBean> getPostGoodsBean(String jsonData) {
        JSONArray selectDataArray;
        String value;
        String label;
        LinkedHashMap<String, PostGoodsBean> postList = new LinkedHashMap<>();
        PostGoodsBean postTitle = new PostGoodsBean();
        postTitle.setName(Constants.PARAM_TITLE);
        postTitle.setDisplayName("标题");
        postTitle.setControlType("input");
        postTitle.setNumeric(0);
        postTitle.setRequired("required");
        postTitle.setMaxlength(25);
        postTitle.setUnit("");
        postList.put(postTitle.getName(), postTitle);
        PostGoodsBean postDescription = new PostGoodsBean();
        postDescription.setName("content");
        postDescription.setDisplayName("描述");
        postDescription.setControlType("textarea");
        postDescription.setNumeric(0);
        postDescription.setRequired("");
        postDescription.setMaxlength(AppMsg.LENGTH_LONG);
        postDescription.setUnit("");
        postList.put(postDescription.getName(), postDescription);
        PostGoodsBean postLocation = new PostGoodsBean();
        postLocation.setName(PostCommonValues.STRING_DETAIL_POSITION);
        postLocation.setDisplayName("所在地点");
        postLocation.setControlType("input");
        postLocation.setNumeric(0);
        postLocation.setRequired("");
        postLocation.setMaxlength(60);
        postLocation.setUnit("");
        postList.put(postLocation.getName(), postLocation);
        PostGoodsBean postContact = new PostGoodsBean();
        postContact.setName("contact");
        postContact.setDisplayName("联系电话");
        postContact.setControlType("input");
        postContact.setNumeric(0);
        postContact.setRequired("required");
        postContact.setMaxlength(30);
        postContact.setUnit("");
        postList.put(postContact.getName(), postContact);
        JSONArray jsonArray = new JSONObject(jsonData).getJSONArray("result");
        int i = 0;
        while (i < jsonArray.length()) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            PostGoodsBean postGoods = new PostGoodsBean();
            try {
                postGoods.setMaxlength(jsonObject.getInt("MaxLength"));
            } catch (Throwable th) {
                postGoods.setMaxlength(-1);
            }
            try {
                String unit = jsonObject.getString("unit");
                if (unit == null || unit.equalsIgnoreCase("null")) {
                    unit = "";
                }
                postGoods.setUnit(unit);
            } catch (Exception e) {
                postGoods.setUnit("");
            }
            try {
                postGoods.setControlType(jsonObject.getString("controlType"));
            } catch (Exception e2) {
                postGoods.setControlType("");
            }
            try {
                postGoods.setNumeric(jsonObject.getBoolean("numeric") ? 1 : 0);
            } catch (Exception e3) {
                postGoods.setNumeric(0);
            }
            try {
                postGoods.setRequired(jsonObject.getBoolean("required") ? "required" : "");
            } catch (Exception e4) {
                postGoods.setRequired("");
            }
            try {
                postGoods.setDisplayName(jsonObject.getString("displayName"));
            } catch (Exception e5) {
                postGoods.setDisplayName("");
            }
            try {
                postGoods.setName(jsonObject.getString(BaseFragment.ARG_COMMON_TITLE));
            } catch (Exception e6) {
                postGoods.setName("");
            }
            postGoods.setLevelCount(1);
            try {
                String metaId = jsonObject.getString("metaId");
                if (metaId == null || metaId.equalsIgnoreCase("null")) {
                    metaId = "";
                }
                postGoods.setMetaId(metaId);
            } catch (Exception e7) {
                postGoods.setMetaId("");
            }
            try {
                postGoods.setMaxValue(jsonObject.getInt("maxValue"));
            } catch (Exception e8) {
                postGoods.setMaxValue(0);
            }
            try {
                postGoods.setMaxSelectNumber(jsonObject.getInt("maxSelectNumber"));
            } catch (Exception e9) {
                postGoods.setMaxSelectNumber(0);
            }
            try {
                List<String> labelList = new ArrayList<>();
                ArrayList arrayList = new ArrayList();
                try {
                    selectDataArray = jsonObject.getJSONArray("selectData");
                } catch (Exception e10) {
                    selectDataArray = null;
                }
                if (selectDataArray != null) {
                    if (selectDataArray.length() != 0) {
                        for (int j = 0; j < selectDataArray.length(); j++) {
                            try {
                                label = selectDataArray.getJSONObject(j).getString("label");
                                value = selectDataArray.getJSONObject(j).getString("value");
                            } catch (Exception e11) {
                                value = "";
                                label = value;
                            }
                            labelList.add(label);
                            arrayList.add(value);
                        }
                        postGoods.setLabels(labelList);
                        postGoods.setValues(arrayList);
                        postList.put(postGoods.getName(), postGoods);
                        i++;
                    }
                }
                postList.put(postGoods.getName(), postGoods);
                i++;
            } catch (JSONException e12) {
                e12.printStackTrace();
            }
        }
        return postList;
    }

    public static List<Pair<Category, Integer>> parseAdSearchCategoryCountResult(String jsonData) {
        List<Pair<Category, Integer>> categoryCountList = new ArrayList<>(8);
        try {
            JSONObject data = new JSONObject(jsonData).getJSONObject("result");
            JSONArray cateListArray = data.getJSONArray("category");
            JSONArray countArray = data.getJSONArray("count");
            if (!(cateListArray.length() == 0 || countArray.length() == 0)) {
                int length = cateListArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject cate = cateListArray.getJSONObject(i);
                    String englishName = cate.getString(LocaleUtil.INDONESIAN);
                    String name = cate.getString(BaseFragment.ARG_COMMON_TITLE);
                    int count = countArray.getInt(i);
                    Category secondCate = new Category();
                    secondCate.setEnglishName(englishName);
                    secondCate.setName(name);
                    categoryCountList.add(new Pair(secondCate, Integer.valueOf(count)));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return categoryCountList;
    }

    public static Category loadCategoryTree(String jsonData) {
        Category root = new Category();
        root.setName("所有类目");
        root.setEnglishName("root");
        try {
            JsonParser parser = new JsonFactory().createJsonParser(jsonData);
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String currentName = parser.getCurrentName();
                if (currentName != null && currentName.equals("result")) {
                    JsonToken jt = parser.nextToken();
                    while (jt != JsonToken.END_ARRAY) {
                        if (jt != JsonToken.START_OBJECT) {
                            jt = parser.nextToken();
                        } else {
                            jt = parser.nextToken();
                            Category firstStepCate = new Category();
                            firstStepCate.setParentEnglishName(root.getEnglishName());
                            while (jt != JsonToken.END_OBJECT) {
                                String flName = parser.getCurrentName();
                                if (flName.equals(BaseFragment.ARG_COMMON_TITLE)) {
                                    JsonToken jt2 = parser.nextToken();
                                    firstStepCate.setName(parser.getText());
                                } else if (flName.equals(LocaleUtil.INDONESIAN)) {
                                    JsonToken jt3 = parser.nextToken();
                                    firstStepCate.setEnglishName(parser.getText());
                                } else if (flName.equals("children")) {
                                    JsonToken jt4 = parser.nextToken();
                                    while (jt4 != JsonToken.END_ARRAY) {
                                        if (jt4 != JsonToken.START_OBJECT) {
                                            jt4 = parser.nextToken();
                                        } else {
                                            Category secStepCate = new Category();
                                            secStepCate.setParentEnglishName(firstStepCate.getEnglishName());
                                            for (JsonToken jt5 = parser.nextToken(); jt5 != JsonToken.END_OBJECT; jt5 = parser.nextToken()) {
                                                String secName = parser.getCurrentName();
                                                JsonToken jt6 = parser.nextToken();
                                                String text = parser.getText();
                                                if (secName.equals(BaseFragment.ARG_COMMON_TITLE)) {
                                                    secStepCate.setName(text);
                                                } else if (secName.equals(LocaleUtil.INDONESIAN)) {
                                                    secStepCate.setEnglishName(text);
                                                }
                                            }
                                            jt4 = parser.nextToken();
                                            firstStepCate.addChild(secStepCate);
                                        }
                                    }
                                }
                                jt = parser.nextToken();
                            }
                            root.addChild(firstStepCate);
                        }
                    }
                }
            }
            return root;
        } catch (JsonParseException e) {
            Log.d("BX_JSON", "invalid category json.");
            return null;
        } catch (IOException e2) {
            Log.d("BX_JSON", "IOException when parse category.");
            return null;
        }
    }

    public static Quota parseQuota(String jsonData) {
        try {
            JSONObject quotaObj = new JSONObject(jsonData).getJSONObject("result");
            JSONObject refreshObj = quotaObj.getJSONObject("refresh");
            JSONObject postObj = quotaObj.getJSONObject("post");
            return new Quota(refreshObj.getInt("total"), refreshObj.getInt("last"), refreshObj.getDouble("price"), postObj.getInt("total"), postObj.getInt("last"), postObj.getDouble("price"), quotaObj.getDouble("balance"), quotaObj.getBoolean("multiCity"));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
