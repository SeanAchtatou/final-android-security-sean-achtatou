package com.baixing.broadcast.push;

import android.content.Context;
import android.os.Bundle;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.broadcast.NotificationIds;
import com.baixing.util.ViewUtil;
import org.json.JSONObject;

public class UrlHandler extends PushHandler {
    UrlHandler(Context context) {
        super(context);
    }

    public boolean acceptMessage(String type) {
        return type.equals("jumpurl");
    }

    public void processMessage(String message) {
        try {
            JSONObject json = new JSONObject(message);
            JSONObject data = json.getJSONObject("d");
            String title = json.getString("t");
            String content = data.getString("content");
            Bundle bundle = new Bundle();
            bundle.putString(ThirdpartyTransitActivity.Key_Data, json.getString("d"));
            ViewUtil.putOrUpdateNotification(this.cxt, NotificationIds.NOTIFICATION_ID_JUMPURL, "android.intent.action.VIEW", title, content, bundle, false);
        } catch (Throwable th) {
        }
    }
}
