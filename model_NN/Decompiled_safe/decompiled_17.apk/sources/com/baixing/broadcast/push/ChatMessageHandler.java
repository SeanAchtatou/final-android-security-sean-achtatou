package com.baixing.broadcast.push;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.database.ChatMessageDatabase;
import com.baixing.entity.ChatMessage;
import com.baixing.util.ViewUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class ChatMessageHandler extends PushHandler {
    ChatMessageHandler(Context context) {
        super(context);
    }

    public boolean acceptMessage(String type) {
        return "bxmessage".equals(type) && isAuthenticated();
    }

    public void processMessage(String message) {
        try {
            JSONObject jsonObj = new JSONObject(message);
            ChatMessage msg = ChatMessage.fromJson(jsonObj.getString(ThirdpartyTransitActivity.Key_Data));
            ChatMessageDatabase.prepareDB(this.cxt);
            ChatMessageDatabase.storeMessage(msg);
            Intent intent = new Intent(CommonIntentAction.ACTION_BROADCAST_NEW_MSG);
            intent.putExtra(CommonIntentAction.EXTRA_MSG_MESSAGE, msg);
            this.cxt.sendBroadcast(intent);
            String titleText = jsonObj.has("text") ? jsonObj.getString("text") : "私信提醒";
            Bundle bundle = new Bundle();
            bundle.putBoolean("isTalking", true);
            bundle.putSerializable(CommonIntentAction.EXTRA_MSG_MESSAGE, msg);
            ViewUtil.putOrUpdateNotification(this.cxt, 1001, CommonIntentAction.ACTION_NOTIFICATION_MESSAGE, titleText, msg.getMessage(), bundle, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
