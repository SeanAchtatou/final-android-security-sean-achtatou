package com.baixing.broadcast.push;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.broadcast.NotificationIds;
import com.baixing.data.GlobalDataManager;
import com.baixing.util.Version;
import com.baixing.util.ViewUtil;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class PushUpdateHandler extends PushHandler {
    private static final String TAG = PushUpdateHandler.class.getSimpleName();

    PushUpdateHandler(Context context) {
        super(context);
    }

    public boolean acceptMessage(String type) {
        return "bxupdate".equals(type);
    }

    public void processMessage(String message) {
        Log.e(TAG, "here:" + message);
        try {
            JSONObject json = new JSONObject(message);
            JSONObject data = json.getJSONObject("d");
            String title = json.getString("t");
            String serverVersion = data.getString("serverVersion");
            String apkUrl = data.getString("apkUrl");
            Log.e(TAG, "here1:" + data + " : " + apkUrl);
            if (Pattern.compile("http(s)?://\\w+").matcher(apkUrl).find() && Version.compare(serverVersion, GlobalDataManager.getInstance().getVersion()) == 1) {
                if (title == null || title == "") {
                    title = "百姓网有新版本啦~";
                }
                String content = "赶紧去更新";
                if (data.has("content")) {
                    content = data.getString("content");
                }
                Bundle extral = new Bundle();
                extral.putString("apkUrl", apkUrl);
                ViewUtil.putOrUpdateNotification(this.cxt, NotificationIds.NOTIFICATION_ID_UPGRADE, CommonIntentAction.ACTION_NOTIFICATION_UPGRADE, title, content, extral, false);
            }
        } catch (Exception ex) {
            Log.e(TAG, "here2:" + ex.toString());
        }
    }
}
