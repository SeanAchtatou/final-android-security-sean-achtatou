package com.baixing.broadcast.push;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.BaseTabActivity;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.AdList;
import com.baixing.entity.Category;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.VadListLoader;
import com.baixing.util.ViewUtil;
import com.baixing.view.fragment.HomePageFragment;
import com.baixing.view.fragment.ListingFragment;
import com.baixing.view.fragment.MyAdFragment;
import com.baixing.view.fragment.SecondCateFragment;
import com.baixing.view.fragment.VadFragment;
import com.baixing.view.fragment.WebViewFragment;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class PageJumper {
    private static final String PAGE_CATEGORY = "category";
    private static final String PAGE_HOME = "home";
    private static final String PAGE_LISTING = "listing";
    private static final String PAGE_MY = "my";
    private static final String PAGE_OPENURL = "openurl";
    private static final String PAGE_VIEWAD = "viewad";

    public static boolean isValidPage(String page) {
        return page.equals(PAGE_CATEGORY) || page.equals(PAGE_LISTING) || page.equals(PAGE_HOME) || page.equals(PAGE_MY) || page.equals(PAGE_VIEWAD) || page.equals(PAGE_OPENURL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.HomePageFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.WebViewFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.ListingFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.SecondCateFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.VadFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.MyAdFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public static void jumpToPage(BaseTabActivity currentActivity, String pageName, String data) {
        List<Category> cates;
        List<Category> allCates;
        Fragment currentFrag = currentActivity.getCurrentFragment();
        if (pageName.equals(PAGE_HOME) || pageName.equals(PAGE_OPENURL)) {
            if (currentFrag == null || !(currentFrag instanceof HomePageFragment)) {
                currentActivity.pushFragment((BaseFragment) new HomePageFragment(), new Bundle(), true);
            }
            if (pageName.equals(PAGE_OPENURL)) {
                try {
                    String url = new JSONObject(data).getString(Constants.PARAM_URL);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.PARAM_URL, url);
                    currentActivity.pushFragment((BaseFragment) new WebViewFragment(), bundle, false);
                } catch (JSONException e) {
                }
            }
        } else if (pageName.equals(PAGE_LISTING)) {
            try {
                String categoryEnglishName = new JSONObject(data).getString("englishname");
                if (categoryEnglishName != null && categoryEnglishName.length() != 0 && (allCates = GlobalDataManager.getInstance().getFirstLevelCategory()) != null) {
                    for (int i = 0; i < allCates.size(); i++) {
                        List<Category> subCates = allCates.get(i).getChildren();
                        for (int j = 0; j < subCates.size(); j++) {
                            if (subCates.get(j).getEnglishName().equals(categoryEnglishName)) {
                                String name = subCates.get(j).getName();
                                Bundle arg = new Bundle();
                                arg.putString(BaseFragment.ARG_COMMON_TITLE, name);
                                arg.putString(BaseFragment.ARG_COMMON_BACK_HINT, "返回");
                                arg.putString("categoryEnglishName", categoryEnglishName);
                                currentActivity.pushFragment((BaseFragment) new ListingFragment(), arg, false);
                                return;
                            }
                        }
                    }
                }
            } catch (JSONException e2) {
                ViewUtil.showToast(currentActivity, "数据错误", false);
                e2.printStackTrace();
            }
        } else if (pageName.equals(PAGE_CATEGORY)) {
            try {
                String categoryEnglishName2 = new JSONObject(data).getString("englishname");
                if (categoryEnglishName2 != null && categoryEnglishName2.length() != 0 && (cates = GlobalDataManager.getInstance().getFirstLevelCategory()) != null) {
                    for (int i2 = 0; i2 < cates.size(); i2++) {
                        if (cates.get(i2).getEnglishName().equals(categoryEnglishName2)) {
                            Bundle bundle2 = new Bundle();
                            bundle2.putSerializable("cates", cates.get(i2));
                            bundle2.putBoolean("isPost", false);
                            currentActivity.pushFragment((BaseFragment) new SecondCateFragment(), bundle2, false);
                            return;
                        }
                    }
                }
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        } else if (pageName.equals(PAGE_VIEWAD)) {
            try {
                JSONObject jSONObject = new JSONObject(data);
                ApiParams param = new ApiParams();
                param.addParam("adIds", jSONObject.getString(LocaleUtil.INDONESIAN));
                AdList gl = JsonUtil.getGoodsListFromJson(BaseApiCommand.createCommand("ad.ads/", true, param).executeSync(currentActivity));
                if (gl != null && gl.getData() != null && gl.getData().size() > 0) {
                    VadListLoader glLoader = new VadListLoader(null, null, null, gl);
                    glLoader.setGoodsList(gl);
                    glLoader.setHasMore(false);
                    Bundle bundle22 = new Bundle();
                    bundle22.putSerializable("loader", glLoader);
                    bundle22.putInt("index", 0);
                    currentActivity.pushFragment((BaseFragment) new VadFragment(), bundle22, false);
                }
            } catch (JSONException e4) {
                e4.printStackTrace();
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        } else if (pageName.equals(PAGE_MY)) {
            Bundle bundle3 = new Bundle();
            bundle3.putInt(MyAdFragment.TYPE_KEY, 0);
            currentActivity.pushFragment((BaseFragment) new MyAdFragment(), bundle3, false);
        } else {
            if (pageName.equals(PAGE_OPENURL)) {
            }
        }
    }
}
