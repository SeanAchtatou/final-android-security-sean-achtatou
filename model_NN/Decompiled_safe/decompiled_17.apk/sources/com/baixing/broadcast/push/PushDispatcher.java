package com.baixing.broadcast.push;

import android.content.Context;

public class PushDispatcher {
    private static final String TAG = PushDispatcher.class.getSimpleName();
    private Context context;
    private PushHandler[] handlers;

    public PushDispatcher(Context context2) {
        this.context = context2;
        this.handlers = new PushHandler[]{new BXInfoHandler(context2), new PushUpdateHandler(context2), new UrlHandler(context2)};
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void dispatch(java.lang.String r17) {
        /*
            r16 = this;
            java.lang.String r1 = "push"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "push"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r17
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            r0 = r16
            android.content.Context r1 = r0.context
            java.lang.String r2 = "showDebugPush"
            java.lang.Class<java.lang.Boolean> r3 = java.lang.Boolean.class
            java.lang.Object r13 = com.baixing.util.Util.loadDataFromLocate(r1, r2, r3)
            java.lang.Boolean r13 = (java.lang.Boolean) r13
            if (r13 == 0) goto L_0x0045
            boolean r1 = r13.booleanValue()
            if (r1 == 0) goto L_0x0045
            r0 = r16
            android.content.Context r1 = r0.context
            r2 = 6001(0x1771, float:8.409E-42)
            java.lang.String r3 = "com.baixing.action.notify.debug"
            java.lang.String r4 = "收到 push"
            android.os.Bundle r6 = new android.os.Bundle
            r6.<init>()
            r7 = 0
            r5 = r17
            com.baixing.util.ViewUtil.putOrUpdateNotification(r1, r2, r3, r4, r5, r6, r7)
        L_0x0045:
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0098 }
            r0 = r17
            r11.<init>(r0)     // Catch:{ Throwable -> 0x0098 }
            if (r11 == 0) goto L_0x0056
            java.lang.String r1 = "a"
            boolean r1 = r11.has(r1)     // Catch:{ Throwable -> 0x0098 }
            if (r1 != 0) goto L_0x0057
        L_0x0056:
            return
        L_0x0057:
            java.lang.String r1 = "a"
            java.lang.String r15 = r11.getString(r1)     // Catch:{ Throwable -> 0x0098 }
            r0 = r16
            com.baixing.broadcast.push.PushHandler[] r8 = r0.handlers     // Catch:{ Throwable -> 0x0098 }
            int r12 = r8.length     // Catch:{ Throwable -> 0x0098 }
            r10 = 0
        L_0x0063:
            if (r10 >= r12) goto L_0x0056
            r9 = r8[r10]     // Catch:{ Throwable -> 0x0098 }
            boolean r1 = r9.acceptMessage(r15)     // Catch:{ Throwable -> 0x0098 }
            if (r1 == 0) goto L_0x008a
            java.lang.String r1 = com.baixing.broadcast.push.PushDispatcher.TAG     // Catch:{ Throwable -> 0x0098 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0098 }
            r2.<init>()     // Catch:{ Throwable -> 0x0098 }
            java.lang.String r3 = "type = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0098 }
            java.lang.StringBuilder r2 = r2.append(r15)     // Catch:{ Throwable -> 0x0098 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0098 }
            android.util.Log.d(r1, r2)     // Catch:{ Throwable -> 0x0098 }
            r0 = r17
            r9.processMessage(r0)     // Catch:{ Throwable -> 0x008d }
        L_0x008a:
            int r10 = r10 + 1
            goto L_0x0063
        L_0x008d:
            r14 = move-exception
            java.lang.String r1 = com.baixing.broadcast.push.PushDispatcher.TAG     // Catch:{ Throwable -> 0x0098 }
            java.lang.String r2 = r14.toString()     // Catch:{ Throwable -> 0x0098 }
            android.util.Log.e(r1, r2)     // Catch:{ Throwable -> 0x0098 }
            goto L_0x008a
        L_0x0098:
            r1 = move-exception
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.broadcast.push.PushDispatcher.dispatch(java.lang.String):void");
    }
}
