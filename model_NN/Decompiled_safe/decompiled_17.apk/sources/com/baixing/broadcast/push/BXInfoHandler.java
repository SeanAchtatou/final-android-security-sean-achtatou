package com.baixing.broadcast.push;

import android.content.Context;
import android.os.Bundle;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.broadcast.NotificationIds;
import com.baixing.util.ViewUtil;
import org.json.JSONObject;

public class BXInfoHandler extends PushHandler {
    private static final String TAG = BXInfoHandler.class.getSimpleName();

    BXInfoHandler(Context context) {
        super(context);
    }

    public boolean acceptMessage(String type) {
        return PageJumper.isValidPage(type);
    }

    public void processMessage(String message) {
        try {
            JSONObject json = new JSONObject(message);
            String action = json.getString("a");
            String title = json.getString("t");
            String data = json.getString("d");
            String content = json.getJSONObject("d").getString("content");
            Bundle bundle = new Bundle();
            bundle.putString("page", action);
            bundle.putString(ThirdpartyTransitActivity.Key_Data, data);
            ViewUtil.putOrUpdateNotification(this.cxt, NotificationIds.NOTIFICATION_ID_BXINFO, CommonIntentAction.ACTION_NOTIFICATION_BXINFO, title, content, bundle, false);
        } catch (Throwable th) {
        }
    }
}
