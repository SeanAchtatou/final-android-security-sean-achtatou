package com.baixing.broadcast.push;

import android.app.ActivityManager;
import android.content.Context;
import com.baixing.entity.UserBean;
import com.baixing.util.Util;

public abstract class PushHandler {
    protected Context cxt;

    public abstract boolean acceptMessage(String str);

    public abstract void processMessage(String str);

    protected PushHandler(Context context) {
        this.cxt = context;
    }

    /* access modifiers changed from: protected */
    public boolean isUIActive(String expActivity) {
        if (((ActivityManager) this.cxt.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName().startsWith(expActivity)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isAuthenticated() {
        UserBean user = (UserBean) Util.loadDataFromLocate(this.cxt, "user", UserBean.class);
        if (user == null || user.getId() == null) {
            return false;
        }
        return true;
    }
}
