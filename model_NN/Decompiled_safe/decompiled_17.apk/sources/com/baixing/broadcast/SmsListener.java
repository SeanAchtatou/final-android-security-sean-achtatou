package com.baixing.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsListener extends BroadcastReceiver {
    private SharedPreferences preferences;

    public void onReceive(Context context, Intent intent) {
        Bundle bundle;
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED") && (bundle = intent.getExtras()) != null) {
            try {
                Object[] pdus = (Object[]) bundle.get("pdus");
                SmsMessage[] msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String originatingAddress = msgs[i].getOriginatingAddress();
                    String msgBody = msgs[i].getMessageBody();
                    Intent newIntent = new Intent(CommonIntentAction.ACTION_BROADCAST_SMS);
                    newIntent.putExtra("msg", msgBody);
                    context.sendBroadcast(newIntent);
                }
            } catch (Exception e) {
            }
        }
    }
}
