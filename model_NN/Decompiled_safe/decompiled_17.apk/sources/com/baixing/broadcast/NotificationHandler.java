package com.baixing.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.baixing.activity.MainActivity;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.BXUpdateService;

public class NotificationHandler extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (CommonIntentAction.ACTION_NOTIFICATION_MESSAGE.equals(intent.getAction())) {
            startApp(context, intent);
        } else if (CommonIntentAction.ACTION_NOTIFICATION_BXINFO.equals(intent.getAction())) {
            startApp(context, intent);
        } else if (CommonIntentAction.ACTION_NOTIFICATION_UPGRADE.equals(intent.getAction())) {
            startService(context, intent);
            startApp(context, intent);
        }
    }

    private void startService(Context context, Intent outerIntent) {
        Intent service = new Intent(context, BXUpdateService.class);
        service.putExtras(outerIntent);
        service.setFlags(268435456);
        context.startService(service);
    }

    private void startApp(Context context, Intent outerIntent) {
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.PUSH_STARTAPP).end();
        Intent goMain = new Intent(context, MainActivity.class);
        goMain.putExtras(outerIntent);
        goMain.addFlags(1409286144);
        goMain.putExtras(outerIntent);
        context.startActivity(goMain);
    }
}
