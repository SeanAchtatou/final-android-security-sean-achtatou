package com.baixing.broadcast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import com.baixing.activity.MainActivity;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.Util;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import org.json.JSONObject;

public class BXNotificationService extends Service implements BaseApiCommand.Callback {
    private static final int HELLO_ID = 287454020;
    private static final int MSG_CHECK_UPDATE = 1;
    private static final int MSG_PUSH_RETURN = 2;
    /* access modifiers changed from: private */
    public String json = null;
    Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            JSONObject errorjs;
            switch (msg.what) {
                case 1:
                    BXNotificationService.this.doGetPushInfo();
                    BXNotificationService.this.myHandler.sendEmptyMessageDelayed(1, 7200000);
                    break;
                case 2:
                    Log.d("notification", "notification result:  " + BXNotificationService.this.json);
                    if (BXNotificationService.this.json != null && !BXNotificationService.this.json.toString().equals("null")) {
                        String time = null;
                        String ticket = null;
                        String title = null;
                        String content = null;
                        try {
                            JSONObject jsonObject = new JSONObject(BXNotificationService.this.json);
                            if (!jsonObject.has("error") || (errorjs = jsonObject.getJSONObject("error")) == null || !errorjs.has("code") || errorjs.getInt("code") == 0) {
                                if (jsonObject.has("pushCode")) {
                                    time = jsonObject.getString("pushCode");
                                }
                                if (jsonObject.has("ticket")) {
                                    ticket = jsonObject.getString("ticket");
                                }
                                if (jsonObject.has(Constants.PARAM_TITLE)) {
                                    title = jsonObject.getString(Constants.PARAM_TITLE);
                                }
                                if (jsonObject.has("content")) {
                                    content = jsonObject.getString("content");
                                }
                                if (!Util.isPushAlreadyThere(BXNotificationService.this, time)) {
                                    Log.d("task", "task  increase get_notification");
                                    BXNotificationService.this.showNotification(ticket, title, content);
                                    Util.saveDataToFile(BXNotificationService.this, null, "pushCode", time.getBytes());
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private BroadcastReceiver networkStateReceiver;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void showNotification(String ticket, String title, String content) {
        String tickerText;
        String contentTitle;
        String contentText;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService("notification");
        if (ticket == null || ticket.equals("")) {
            tickerText = "百姓网客户端有新版本更新";
        } else {
            tickerText = ticket;
        }
        if (title == null || title.equals("")) {
            contentTitle = "百姓网有新版本啦";
        } else {
            contentTitle = title;
        }
        if (content == null || content.equals("")) {
            contentText = "赶紧去更新";
        } else {
            contentText = content;
        }
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.putExtra("fromNotification", true);
        notificationIntent.addFlags(536870912);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new Notification(R.drawable.push_icon, tickerText, System.currentTimeMillis());
        notification.defaults |= 1;
        notification.defaults |= 2;
        notification.flags |= 16;
        notification.setLatestEventInfo(this, contentTitle, contentText, contentIntent);
        mNotificationManager.notify(HELLO_ID, notification);
    }

    /* access modifiers changed from: private */
    public void doGetPushInfo() {
        ApiParams list = new ApiParams();
        list.useCache = false;
        UserBean user = (UserBean) Util.loadDataFromLocate(this, "user", UserBean.class);
        list.addParam("userid", user == null ? "" : URLEncoder.encode(user.getId()));
        byte[] timeObj = Util.loadData(this, "pushCode");
        if (timeObj != null) {
            list.addParam("pushCode", URLEncoder.encode(new String(timeObj)));
        }
        BaseApiCommand.createCommand("pushNotification", true, list).execute(this, this);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (GlobalDataManager.context == null) {
            GlobalDataManager.context = new WeakReference<>(this);
        }
        this.networkStateReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                    BXNotificationService.this.myHandler.removeMessages(1);
                    if (BXNotificationService.this.isInternetConnected()) {
                        BXNotificationService.this.myHandler.sendEmptyMessage(1);
                    }
                }
            }
        };
        registerReceiver(this.networkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    public void onDestroy() {
        this.myHandler.removeMessages(1);
        unregisterReceiver(this.networkStateReceiver);
    }

    public void onStart(Intent intent, int startid) {
        if (isInternetConnected()) {
            this.myHandler.sendEmptyMessage(1);
        }
    }

    /* access modifiers changed from: private */
    public boolean isInternetConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService("connectivity");
        if (cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }

    public void onNetworkDone(String apiName, String responseData) {
        this.json = responseData;
        this.myHandler.sendEmptyMessage(2);
    }

    public void onNetworkFail(String apiName, ApiError error) {
        this.myHandler.sendEmptyMessage(-10);
    }
}
