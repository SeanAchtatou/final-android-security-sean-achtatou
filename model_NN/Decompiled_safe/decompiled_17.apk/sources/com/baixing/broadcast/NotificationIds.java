package com.baixing.broadcast;

public interface NotificationIds {
    public static final int NOTIFICATION_ID_BXINFO = 3001;
    public static final int NOTIFICATION_ID_CHAT_MESSAGE = 1001;
    public static final int NOTIFICATION_ID_DEBUG = 6001;
    public static final int NOTIFICATION_ID_JUMPURL = 5001;
    public static final int NOTIFICATION_ID_UPGRADE = 4001;
    public static final int NOTIFICATION_XMPP_CONNECTION_STATUS = 2001;
}
