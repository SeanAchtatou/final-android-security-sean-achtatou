package com.baixing.tracking;

import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiParams;
import com.baixing.util.MobileConfig;
import com.tencent.tauth.Constants;
import java.util.Observable;
import java.util.Observer;

public class TrackConfig implements Observer {
    private static TrackConfig instance = null;
    private boolean logEnabled = true;

    public static TrackConfig getInstance() {
        if (instance == null) {
            instance = new TrackConfig();
        }
        return instance;
    }

    private TrackConfig() {
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_CONFIGURATION_UPDATE);
    }

    public boolean getLoggingFlag() {
        return this.logEnabled;
    }

    public interface TrackMobile {

        public enum Key implements TrackMobile {
            HOTAPP_URL(Constants.PARAM_URL, Constants.PARAM_URL),
            TRACKTYPE("tracktype", "tracktype"),
            TIMESTAMP(ApiParams.KEY_TIMESTAMP, ApiParams.KEY_TIMESTAMP),
            TIMESTAMP_MS("timestamp_ms", "timestamp_ms"),
            URL(Constants.PARAM_URL, "页面URL"),
            FIRSTCATENAME("firstCateName", "一级类目名"),
            SECONDCATENAME("secondCateName", "二级类目名"),
            SEARCHKEYWORD("searchKeyword", "搜索关键字"),
            LISTINGFILTERKEYWORD("listingFilterKeyword", "listing页面筛选关键字"),
            LISTINGFILTER("listingFilter", "listing页面筛选条件"),
            ADID("adId", "adId"),
            USERID(ApiParams.KEY_USERID, ApiParams.KEY_USERID),
            ISLOGIN("isLogin", "已登录/未登录"),
            ADSCOUNT("adsCount", "信息条数"),
            ADSENDERID("adSenderId", "ad的发布者的userId"),
            ADSTATUS("adStatus", "正常/审核未通过"),
            EVENT("event", "事件名"),
            CITY(ApiParams.KEY_CITY, "城市"),
            BLOCK("block", "区块（GPS定位、热门城市、其他城市、搜索）"),
            GPS_RESULT("GPS_result", "GPS定位城市结果成功失败"),
            GPS_GEO("GPS_geo", "GPS定位的经纬度结果"),
            CATEGORYCOUNT("categoryCount", "类目数"),
            MAXCATE_ADSCOUNT("maxCate_adsCount", "最大类目条数"),
            FILTER("filter", "筛选条件"),
            FILTERNAME("filterName", "筛选名"),
            FILTERVALUE("filterValue", "筛选值"),
            RESULTCATESCOUNT("resultCatesCount", "结果类目数"),
            TOTAL_ADSCOUNT("total_adsCount", "总信息数"),
            SELECTEDROWINDEX("selectedRowIndex", "点进去看的所在行（从0开始）"),
            DIALOG("dialogCount", "对话数"),
            DIALOG_BUYER("dialog_buyer", "作为买家对话数"),
            DIALOG_SELLER("dialog_seller", "作为卖家对话数"),
            POSTSTATUS("postStatus", "状态（客户端验证失败，server端机器规则失败，成功）"),
            POSTFAILREASON("postFailReason", "发布失败原因"),
            POSTPICSCOUNT("postPicsCount", "图片数"),
            POSTDESCRIPTIONTEXTCOUNT("postDescriptionTextCount", "描述文字数"),
            POSTDESCRIPTIONLINECOUNT("postDescriptionLineCount", "描述文字行数"),
            POSTCONTACTTEXTCOUNT("postContactTextCount", "联系方式字数"),
            POSTDETAILPOSITIONAUTO("postDetailPositionAuto", "具体地点是否自动定位"),
            POSTEDSECONDS("postedSeconds", "已发布秒数"),
            POSTENTRY("postEntry", "发布入口点"),
            LOGIN_RESULT_STATUS("loginResultStatus", "Login结果（成功、出错）"),
            LOGIN_RESULT_FAIL_REASON("loginResultFailReason", "login出错原因"),
            EDIT_PROFILE_STATUS("editProfileStatus", "修改用户信息成功失败状态"),
            EDIT_RPOFILE_FAIL_REASON("editProfileFileReason", "修改用户信息失败原因"),
            REGISTER_RESULT_STATUS("registerResultStatus", "注册结果（成功、出错）"),
            REGISTER_RESULT_FAIL_REASON("registerResultFailReason", "注册失败原因"),
            FORGETPASSWORD_SENDCODE_RESULT_STATUS("forgetPasswordSendCodeResultStatus", "发送验证码成功/失败状态"),
            FORGETPASSWORD_SENDCODE_RESULT_FAIL_REASON("forgetPasswordSendCodeResultFailReason", "发送验证码失败原因"),
            FORGETPASSWORD_RESETPASSWORD_RESULT_STATUS("forgetPasswordResetPasswordResultStatus", "重设密码成功/失败状态"),
            FORGETPASSWORD_RESETPASSWORD_RESULT_FAIL_REASON("forgetPasswordResetPasswordResultFailReason", "重设密码失败原因"),
            MENU_SHOW_PAGEURL("menuShowInPageURL", "菜单出现的页面URL"),
            MENU_ACTION_TYPE("menuActionType", "菜单动作类型"),
            MENU_ACTION_TYPE_CHANGE_CITY("menuActionType_changeCity", "菜单动作切换城市"),
            FRAGMENT("fragment", "fragment"),
            RESULT("result", "结果"),
            FAIL_REASON("failReason", "失败原因"),
            ACTION("action", "inputing动作"),
            STATUS("status", "信息状态"),
            SIZEINBYTES("size", "字节数大小"),
            UPLOADSECONDS("uploadTime", "上传时间秒数"),
            SHARE_FROM("shareFrom", "分享发起页"),
            SHARE_WEIXIN_SCENE("shareWeixinScene", "微信分享场景"),
            SHARE_CHANNEL("shareChannel", "分享渠道"),
            ISEDIT("isEdit", "拍照是否编辑"),
            FROM("from", "拍照来源"),
            RULENAME("ruleName", "版规名称"),
            RECENTCATEGORY_COUNT("count", "最近使用类目数量"),
            RECENTCATEGORY_NAMES("secondCateNames", "最近使用类目的名称"),
            CURRENTCITY("currentCity", "当前城市"),
            GEOCITY("geoCity", "定位所在城市"),
            ACCEPT("accept", "是否接受切换城市"),
            TITLE(Constants.PARAM_TITLE, "页面标题"),
            WEBVIEW_URL("webview_url", "页面URL");
            
            private String description;
            private String name;

            private Key(String keyName, String keyDescription) {
                this.name = keyName;
                this.description = keyDescription;
            }

            public String getName() {
                return this.name;
            }

            public String getDescription() {
                return this.description;
            }
        }

        public enum Value implements TrackMobile {
            VALID("1", "正常"),
            APPROVING("0", "待定"),
            FAV("1", "fav"),
            CANCEL("0", "cancel"),
            CALL("1", "CALL"),
            COPY("0", "COPY"),
            YES("1", "1"),
            NO("0", "0");
            
            private String description;
            private String value;

            private Value(String valueName, String valueDescription) {
                this.value = valueName;
                this.description = valueDescription;
            }

            public String getValue() {
                return this.value;
            }

            public String getDescription() {
                return this.description;
            }
        }

        public enum PV implements TrackMobile {
            BASE("/base", "没有定义pv的fragment走这里"),
            SELECTCITY("/selectCity", "切换城市"),
            HOME("/home", "分类"),
            LISTHOME("/listhome", "首页"),
            CATEGORIES("/categories", "一级类目页"),
            SEARCH("/search", "header搜索页"),
            SEARCHRESULTCATEGORY("/searchResultCategory", "header搜索类目结果页"),
            SEARCHRESULT("/searchResult", "header搜索结果页"),
            LISTING("/listing", "Listing页"),
            LISTINGFILTER("/listingFilter", "更多筛选页"),
            VIEWAD("/viewAd", "Viewad页"),
            VIEWADMAP("/viewAdMap", "Viewad地图页"),
            VIEWADPIC("/viewAdPic", "图"),
            USER("/user", "用户相关信息"),
            POST("/post", "发布界面"),
            EDITPOST("/editPost", "编辑界面"),
            CAMERA("/post/camera", "拍照"),
            MY("/my", "我的百姓网"),
            MYADS_SENT("/myAds_sent", "已发布信息"),
            MYVIEWAD("/myViewad", "自己查看的viewad"),
            FAVADS("/favAds", "收藏"),
            SETTINGS("/settings", "设置"),
            FEEDBACK("/feedback", "反馈"),
            LOGIN("/login", "登录"),
            REGISTER("/register", "注册"),
            FORGETPASSWORD("/forgetPassword", "忘记密码"),
            RESETPASSWORD("/resetPassword", "修改密码"),
            BINDFORWARD("/3rdAuth", "绑定转发"),
            SHAREAPP("/shareApp", "分享"),
            WEBVIEW("/webview", "webview"),
            HOTAPP("/hotApp", "热门应用");
            
            private String description;
            private String name;

            private PV(String url, String description2) {
                this.name = url;
                this.description = description2;
            }

            public String getName() {
                return this.name;
            }

            public String getDescription() {
                return this.description;
            }
        }

        public enum BxEvent implements TrackMobile {
            CITY_SELECT("City_Select", "City_Select"),
            City_postSelect("City_postSelect", "提示切换城市"),
            CITY_SEARCH("City_Search", "City_Search"),
            HEADERSEARCHRESULT("HeaderSearchResult", "HeaderSearchResult"),
            BROWSEMODENOIMAGE("BrowseModeNoImage", "无图模式切换的结果"),
            LISTING("Listing", "Listing"),
            LISTING_SELECTEDROWINDEX("Listing_SelectedRowIndex", "Listing_SelectedRowIndex"),
            LISTING_MORE("Listing_More", "Listing_More"),
            LISTING_TOPFILTEROPEN("Listing_TopFilterOpen", "Listing_TopFilterOpen"),
            LISTING_TOPFILTERSUBMIT("Listing_TopFilterSubmit", "Listing_TopFilterSubmit"),
            VIEWAD_MOBILECALLCLICK("Viewad_MobileCallClick", "点击拨打按钮"),
            VIEWAD_FAV("Viewad_Fav", "Viewad_Fav"),
            VIEWAD_UNFAV("Viewad_Unfav", "Viewad_Unfav"),
            VIEWAD_SMS("Viewad_SMS", "Viewad_SMS"),
            VIEWAD_HINTFAV("Viewad_HintFav", "拨打后提示收藏"),
            VIEWAD_HINTFAVRESULT("Viewad_HintFavResult", "拨打后提示收藏的结果"),
            VIEWAD_SHOWMAP("Viewad_ShowMap", "点击查看地图"),
            BUZZLIST("BuzzList", "BuzzList"),
            POST_POSTBTNCONTENTCLICKED("Post_PostBtnContentClicked", "Post_PostBtnContentClicked"),
            POST_POSTWITHLOGIN("Post_PostWithLogin", "Post_登录Post"),
            POST_POSTWITHOUTLOGIN("Post_PostWithoutLogin", "Post_未登录Post"),
            POST_POSTRESULT("Post_PostResult", "Post_PostResult"),
            POST_GPSFAIL("Post_GpsFail", "Post_GPS失败"),
            POST_INPUTING("Post_Inputing", "post页面编辑项目"),
            POST_IMAGEUPLOAD("Post_ImgUpload", "照片上传"),
            POST_RULE_ALERT_SHOW("Post_RuleAlert_Show", "遇到前置版规提示"),
            POST_RULE_ALERT_ACTION("Post_RuleAlert_Action", "处理前置版规提示"),
            EDITPOST_POSTBTNCONTENTCLICKED("EditPost_PostBtnContentClicked", "EditPost_PostBtnContentClicked"),
            EDITPOST_POSTWITHLOGIN("EditPost_PostWithLogin", "EditPost_登录Post"),
            EDITPOST_POSTWITHOUTLOGIN("EditPost_PostWithoutLogin", "EditPost_未登录Post"),
            EDITPOST_POSTRESULT("EditPost_PostResult", "EditPost_PostResult"),
            EDITPOST_GPSFAIL("EditPost_GpsFail", "EditPost_GPS失败"),
            EDITPOST_INPUTING("EditPost_Inputing", "editpost页面编辑项目"),
            SENT_RESULT("Sent_Result", "Sent_Result"),
            SENT_MANAGE("Sent_Manage", "Sent_Manage"),
            SENT_REFRESH("Sent_Refresh", "Sent_Refresh"),
            SENT_EDIT("Sent_Edit", "Sent_Edit"),
            SENT_DELETE("Sent_Delete", "Sent_Delete"),
            SENT_APPEAL("Sent_Appeal", "Sent_Appeal"),
            MYVIEWAD_EDIT("MyViewad_Edit", "MyViewad_Edit"),
            MYVIEWAD_REFRESH("MyViewad_Refresh", "MyViewad_Refresh"),
            MYVIEWAD_SHARE("MyViewad_Share", "MyViewad_Share"),
            MYVIEWAD_DELETE("MyViewad_Delete", "MyViewad_Delete"),
            MYVIEWAD_APPEAL("MyViewad_Appeal", "MyViewad_Appeal"),
            FAV_MANAGE("Fav_Manage", "Fav_Manage"),
            FAV_DELETE("Fav_Delete", "Fav_Delete"),
            SETTINGS_CHECKUPDATE("Settings_CheckUpdate", "Settings_CheckUpdate"),
            SETTINGS_ABOUT("Settings_About", "Settings_About"),
            SETTINGS_FEEDBACK("Settings_Feedback", "Settings_Feedback"),
            SETTINGS_COMMENTSUS("Settings_Rate", "Settings_Rate"),
            SETTINGS_PICMODE("settingsPicMode", "settingsPicMode"),
            SETTINGS_LOGOUT("Settings_Logout", "Settings_Logout"),
            SETTINGS_LOGOUT_CONFIRM("Settings_Logout_Confirm", "Settings_Logout_Confirm"),
            SETTINGS_LOGOUT_CANCEL("Settings_Logout_Cancel", "Settings_Logout_Cancel"),
            SETTINGS_LOGIN("Settings_Login", "Settings_Login"),
            EDITPROFILE_SAVE("EditProfile_Save", "EditProfile_Save"),
            EDITPROFILE_CANCEL("EditProfile_Cancel", "EditProfile_Cancel"),
            LOGIN_BACK("Login_Back", "Login_Back"),
            LOGIN_REGISTER("Login_Register", "Login_Register"),
            LOGIN_SUBMIT("Login_Submit", "Login_Submit"),
            LOGIN_FORGETPASSWORD("Login_ForgetPassword", "Login_ForgetPassword"),
            REGISTER_BACK("Register_Back", "Register_Back"),
            REGISTER_REGISTER("Register_Register", "Register_Register"),
            REGISTER_SUBMIT("Register_Submit", "Register_Submit"),
            FORGETPASSWORD_SENDCODE_RESULT("ForgetPassword_sendCode_Result", "ForgetPassword_sendCode_Result"),
            FORGETPASSWORD_RESETPASSWORD_RESULT("ForgetPassword_resetPassword_Result", "ForgetPassword_resetPassword_Result"),
            MENU_SHOW("Menu_Show", "Menu_Show"),
            MENU_CANCEL("Menu_Cancel", "Menu_Cancel"),
            MENU_ACTION("Menu_Action", "Menu_Action"),
            APP_START("App_Start", "App_Start"),
            APP_STOP("App_Stop", "App_Stop"),
            APP_PAUSE("App_Pause", "App_Pause"),
            APP_RESUME("App_Resume", "App_Resume"),
            PUSH_STARTAPP("Push_StartApp", "Push_StartApp"),
            SHARE("Share", "Share"),
            SHARE_START("Share_Start", "Share_Start"),
            GPS("GPS", "GPS"),
            LISTING_FILTERSUBMIT("Listing_FilterSubmit", "Listing_FilterSubmit"),
            RECENTCATEGORY_CHOW("RecentCategory_Chow", "RecentCategory_Chow"),
            RECENTCATEGORY_CLICK("RecentCategory_Click", "RecentCategory_Click"),
            REVIEW_ACTION("Review_Action", "Review_Action"),
            VIEW_BANNER("View_Banner", "View_Banner"),
            SHARE_BY_BLUETOOTH("Share_By_Bluetooth", "Share_By_Bluetooth"),
            APP_SHARE_DETAIL("App_Share_Detail", "App_Share_Detail"),
            JOIN_QIUSHOU("Join_Qiushou", "Join_Qiushou"),
            QUICK_REGISTER_LOGIN("Quick_Register_Login", "Quick_Register_Login"),
            INDEX_START_LOGIN("Index_Start_Login", "Index_Start_Login");
            
            private String description;
            private String name;

            private BxEvent(String name2, String description2) {
                this.name = name2;
                this.description = description2;
            }

            public String getName() {
                return this.name;
            }

            public String getDescription() {
                return this.description;
            }
        }
    }

    public void update(Observable observable, Object data) {
        if (data instanceof BxMessageCenter.IBxNotification) {
            if (((BxMessageCenter.IBxNotification) data).getName().equals(IBxNotificationNames.NOTIFICATION_CONFIGURATION_UPDATE)) {
            }
            this.logEnabled = MobileConfig.getInstance().isEnableTracker();
        }
    }
}
