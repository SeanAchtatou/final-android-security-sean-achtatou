package com.baixing.tracking;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import com.baixing.data.GlobalDataManager;
import com.baixing.tracking.TrackConfig;
import com.baixing.util.TraceUtil;
import com.baixing.util.Util;
import java.io.File;
import java.util.HashMap;
import org.json.JSONArray;

public class Tracker {
    private static Tracker instance = null;
    private Context context;
    private JSONArray dataArray;
    private int size;
    private int threshold;

    public static Tracker getInstance() {
        if (instance == null) {
            instance = new Tracker();
        }
        return instance;
    }

    private Tracker() {
        this.context = null;
        this.dataArray = null;
        this.context = GlobalDataManager.getInstance().getApplicationContext();
        this.dataArray = new JSONArray();
        this.size = 0;
        this.threshold = 100;
    }

    public LogData pv(TrackConfig.TrackMobile.PV url) {
        LogData data = new LogData(new HashMap());
        data.append(TrackConfig.TrackMobile.Key.TRACKTYPE, "pageview");
        long timestamp_ms = System.currentTimeMillis();
        data.append(TrackConfig.TrackMobile.Key.TIMESTAMP, String.valueOf(timestamp_ms / 1000));
        data.append(TrackConfig.TrackMobile.Key.TIMESTAMP_MS, String.valueOf(timestamp_ms));
        data.append(TrackConfig.TrackMobile.Key.URL, url.getName());
        data.append(TrackConfig.TrackMobile.Key.USERID, GlobalDataManager.getInstance().getAccountManager().getMyId(GlobalDataManager.getInstance().getApplicationContext()));
        return data;
    }

    public LogData event(TrackConfig.TrackMobile.BxEvent event) {
        LogData data = new LogData(new HashMap());
        data.append(TrackConfig.TrackMobile.Key.TRACKTYPE, "event");
        long timestamp_ms = System.currentTimeMillis();
        data.append(TrackConfig.TrackMobile.Key.TIMESTAMP, String.valueOf(timestamp_ms / 1000));
        data.append(TrackConfig.TrackMobile.Key.TIMESTAMP_MS, String.valueOf(timestamp_ms));
        data.append(TrackConfig.TrackMobile.Key.EVENT, event.getName());
        data.append(TrackConfig.TrackMobile.Key.USERID, GlobalDataManager.getInstance().getAccountManager().getMyId(GlobalDataManager.getInstance().getApplicationContext()));
        return data;
    }

    public void addLog(LogData log) {
        if (new File(Environment.getExternalStorageDirectory() + "/baixing_debug_log_crl.dat").exists() && log != null) {
            try {
                synchronized (this) {
                    Util.saveDataToSdCard("baixing", "tracker_addlog", (log.toJsonObj().toString() + TraceUtil.LINE).getBytes(), true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.d("sendlistfunction", "tracker addLog");
        this.dataArray.put(log.toJsonObj());
        this.size++;
        if (this.size > this.threshold) {
            try {
                clear();
            } catch (Exception e2) {
            }
        }
    }

    public void save() {
        Log.d("sendlistfunction", "tracker save");
        if (this.context != null && this.dataArray.length() > 0) {
            try {
                Util.saveDataToFile(this.context, "sender_dir", "tracker" + System.currentTimeMillis() + ".log", this.dataArray.toString().getBytes());
                clear();
            } catch (Exception e) {
            }
        }
    }

    public void clear() {
        this.dataArray = new JSONArray();
        this.size = 0;
    }
}
