package com.baixing.tracking;

import com.baixing.tracking.TrackConfig;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class LogData {
    private HashMap<String, String> map;

    public LogData(HashMap<String, String> map2) {
        this.map = map2;
    }

    public HashMap<String, String> getMap() {
        return this.map;
    }

    public LogData append(TrackConfig.TrackMobile.Key key, TrackConfig.TrackMobile.Value value) {
        this.map.put(key.getName(), value.getValue());
        return this;
    }

    public LogData append(TrackConfig.TrackMobile.Key key, int value) {
        this.map.put(key.getName(), value + "");
        return this;
    }

    public LogData append(TrackConfig.TrackMobile.Key key, boolean value) {
        this.map.put(key.getName(), value ? "1" : "0");
        return this;
    }

    public LogData append(TrackConfig.TrackMobile.Key key, long value) {
        this.map.put(key.getName(), value + "");
        return this;
    }

    public LogData append(TrackConfig.TrackMobile.Key key, String value) {
        String keyName = key.getName();
        if (value != null) {
            this.map.put(keyName, value);
        }
        return this;
    }

    public LogData append(TrackConfig.TrackMobile.Key key, float value) {
        String valueString;
        if (value == ((float) ((int) value))) {
            valueString = ((int) value) + "";
        } else {
            valueString = value + "";
        }
        this.map.put(key.getName(), valueString);
        return this;
    }

    public LogData append(TrackConfig.TrackMobile.Key key, double value) {
        String valueString;
        if (value == ((double) ((int) value))) {
            valueString = ((int) value) + "";
        } else {
            valueString = value + "";
        }
        this.map.put(key.getName(), valueString);
        return this;
    }

    public LogData append(HashMap aMap) {
        this.map.putAll(aMap);
        return this;
    }

    public void end() {
        Tracker.getInstance().addLog(this);
    }

    public JSONObject toJsonObj() {
        JSONObject jobj = new JSONObject();
        try {
            for (Map.Entry<String, String> entry : this.map.entrySet()) {
                jobj.put((String) entry.getKey(), entry.getValue());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jobj;
    }
}
