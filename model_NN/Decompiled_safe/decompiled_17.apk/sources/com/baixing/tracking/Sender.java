package com.baixing.tracking;

import android.content.Context;
import android.util.Log;
import com.baixing.data.GlobalDataManager;
import com.baixing.network.NetworkUtil;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.GzipUtil;
import com.baixing.util.TraceUtil;
import com.baixing.util.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Sender implements Runnable {
    static final String SENDER_DIR = "sender_dir";
    static final String SENDER_FILE_SUFFIX = ".log";
    private static String apiName = "mobile.trackdata/";
    private static Sender instance = null;
    private Context context;
    private long dataSize;
    private List<String> queue;
    private Object sendMutex;

    public static Sender getInstance() {
        if (instance == null) {
            instance = new Sender();
        }
        return instance;
    }

    private Sender() {
        this.context = null;
        this.queue = null;
        this.sendMutex = new Object();
        this.dataSize = 0;
        this.context = GlobalDataManager.getInstance().getApplicationContext();
        this.queue = new ArrayList();
        startThread();
    }

    private void startThread() {
        new Thread(this).start();
    }

    public void notifySendMutex() {
        Log.d("sendlistfunction", "notifySendMutex");
        synchronized (this.sendMutex) {
            this.sendMutex.notifyAll();
        }
    }

    public void notifyNetworkReady() {
        notifySendMutex();
    }

    public void addToQueue(String dataString) {
        synchronized (this.queue) {
            this.queue.add(dataString);
        }
        notifySendMutex();
    }

    public List<String> getQueue() {
        return this.queue;
    }

    private boolean hasDataToSend() {
        int size;
        synchronized (this.queue) {
            size = this.queue.size();
        }
        return size > 0 || loadRecord() != null;
    }

    private boolean isSendingReady() {
        return NetworkUtil.isNetworkActive(this.context);
    }

    private boolean checkQueueFull() {
        boolean isQueueFull;
        synchronized (this.queue) {
            isQueueFull = this.queue.size() > 3;
        }
        return isQueueFull;
    }

    public void save() {
        Log.d("sendlistfunction", "sender save");
        List<String> newQueue = new ArrayList<>();
        synchronized (this.queue) {
            newQueue.addAll(this.queue);
            this.queue.clear();
        }
        if (newQueue.size() > 0) {
            JSONArray compositeArray = new JSONArray();
            for (String itemString : newQueue) {
                JSONArray itemArray = null;
                try {
                    itemArray = new JSONArray(itemString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (itemArray != null && itemArray.length() > 0) {
                    for (int i = 0; i < itemArray.length(); i++) {
                        try {
                            compositeArray.put(itemArray.get(i));
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
            if (compositeArray.length() > 0) {
                saveListToFile(compositeArray.toString());
            }
            notifySendMutex();
            newQueue.clear();
        }
    }

    private void saveListToFile(String data) {
        if (this.context != null) {
            String fileName = System.currentTimeMillis() + SENDER_FILE_SUFFIX;
            try {
                Log.d("sendlist", "saveListToFile->" + fileName);
                Util.saveDataToFile(this.context, SENDER_DIR, fileName, data.getBytes());
            } catch (Exception e) {
            }
        }
    }

    private String loadRecord() {
        List<String> list = null;
        if (this.context != null) {
            try {
                list = Util.listFiles(this.context, SENDER_DIR);
            } catch (Exception e) {
                return null;
            }
        }
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    private static boolean executeSyncPostTask(Context cxt, String apiName2, String jsonStr) {
        boolean value;
        ApiParams params = new ApiParams();
        params.zipRequest = true;
        params.addParam("json", jsonStr);
        Map<String, String> common = new HashMap<>();
        common.put(ApiParams.KEY_UDID, Util.getDeviceUdid(cxt));
        common.put(ApiParams.KEY_USERID, GlobalDataManager.getInstance().getAccountManager().getMyId(cxt));
        common.put("version", GlobalDataManager.getInstance().getVersion());
        common.put("channel", GlobalDataManager.getInstance().getChannelId());
        common.put(ApiParams.KEY_CITY, GlobalDataManager.getInstance().getCityEnglishName());
        common.put(ApiParams.KEY_APIKEY, "api_androidbaixing");
        common.put(ApiParams.KEY_APPID, cxt.getPackageName());
        params.addParam("commonJson", common);
        try {
            Log.d("sender", "try sending");
            String result = BaseApiCommand.createCommand(apiName2, false, params).executeSync(cxt);
            Log.d("response", result);
            JSONObject obj = new JSONObject(result);
            if (obj.has("result")) {
                value = obj.getBoolean("result");
            } else {
                value = false;
            }
            if (value) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean sendList(String jsonStr) {
        Log.d("sendlistfunction", jsonStr);
        boolean succed = executeSyncPostTask(this.context, apiName, jsonStr);
        if (succed) {
            try {
                this.dataSize += (long) GzipUtil.compress(jsonStr).getBytes().length;
                Log.d("datasize", "datasize:" + ((this.dataSize / 1024) / 1024 != 0 ? ((((double) this.dataSize) / 1024.0d) / 1024.0d) + "MB" : this.dataSize / 1024 != 0 ? (((double) this.dataSize) / 1024.0d) + "KB" : this.dataSize + "B"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (succed && Util.isLoggable()) {
            try {
                JSONArray array = new JSONArray(jsonStr);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject log = array.getJSONObject(i);
                    if (log != null) {
                        Util.saveDataToSdCard("baixing", "sender_sendlistlog", (log.toString() + TraceUtil.LINE).getBytes(), true);
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return succed;
    }

    public void run() {
        Log.d("sender", "run");
        while (TrackConfig.getInstance().getLoggingFlag()) {
            String list = null;
            synchronized (this.queue) {
                if (this.queue.size() > 0) {
                    list = this.queue.remove(0);
                }
            }
            Log.d("sendlistfunction", "big while");
            if (list != null) {
                Log.d("sender", "has memory data");
                boolean succed = sendList(list);
                Log.d("sender", "after sendList");
                if (!succed) {
                    Log.d("sender", "saveListToFile");
                    saveListToFile(list);
                }
            } else {
                String recordPath = loadRecord();
                if (recordPath != null) {
                    String singleRecordList = new String(Util.loadData(recordPath));
                    Log.d("sendlist", "file send->" + recordPath);
                    if (singleRecordList != null && sendList(singleRecordList)) {
                        Util.clearFile(recordPath);
                    }
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            Log.d("sender", "check if there's more data to send");
            boolean hasMoreData = hasDataToSend();
            boolean isQueueFull = checkQueueFull();
            while (true) {
                if (isSendingReady() || isQueueFull) {
                    if (hasMoreData) {
                    }
                }
                Log.d("sendlist", "into small while");
                try {
                    Log.d("sendlistfunction", "small while,wait");
                    synchronized (this.sendMutex) {
                        this.sendMutex.wait(300000);
                    }
                    hasMoreData = hasDataToSend();
                    isQueueFull = checkQueueFull();
                    Log.d("sender", "hasMoredata:" + hasMoreData);
                    Log.d("sendlistfunction", "wake up");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
