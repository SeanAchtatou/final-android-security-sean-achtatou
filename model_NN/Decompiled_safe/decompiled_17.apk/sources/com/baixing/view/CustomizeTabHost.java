package com.baixing.view;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.baixing.data.GlobalDataManager;
import com.baixing.imageCache.ImageCacheManager;
import com.quanleimu.activity.R;
import java.io.Serializable;

public final class CustomizeTabHost implements Serializable {
    private static final long serialVersionUID = -7127389061287304936L;
    private int currentFocusIndex;
    private transient ViewGroup tabBarRoot;
    private transient TabSelectListener tabChangeListener;
    private int tabCount;
    private TabIconRes[] tabIconsRes;
    private String[] tabLabels;

    public interface TabSelectListener {
        void afterChange(int i);

        void beforeChange(int i, int i2);

        void deprecatSelect(int i);
    }

    public static final class TabIconRes implements Serializable {
        public static final TabIconRes NO_ICON = new TabIconRes(-1, -1);
        private static final long serialVersionUID = 6132394089130310112L;
        private int selectIconRes;
        private int unselectIconRes;

        public TabIconRes(int select, int unselect) {
            this.selectIconRes = select;
            this.unselectIconRes = unselect;
        }

        public int getRes(boolean isSelect) {
            return isSelect ? this.selectIconRes : this.unselectIconRes;
        }
    }

    private CustomizeTabHost() {
    }

    public static CustomizeTabHost createTabHost(int focusIndex, String[] tabString, TabIconRes[] tabIcons) {
        CustomizeTabHost tabHost = new CustomizeTabHost();
        tabHost.tabCount = tabString.length;
        tabHost.currentFocusIndex = focusIndex;
        tabHost.tabLabels = tabString;
        tabHost.tabIconsRes = tabIcons;
        return tabHost;
    }

    public void attachView(View v, TabSelectListener listener) {
        setTabSelectListener(listener);
        this.tabBarRoot = (ViewGroup) v;
        initTabButton(v, this.tabCount);
        showTab(this.currentFocusIndex);
        Resources res = v.getContext().getResources();
        for (int i = 0; i < this.tabLabels.length; i++) {
            setTabText(i, this.tabLabels[i]);
            setTabIcon(i, this.tabIconsRes[i]);
            if (i == this.currentFocusIndex) {
                getTabItem(i).setBackgroundColor(res.getColor(R.color.tab_bg_select));
            } else {
                getTabItem(i).setBackgroundResource(R.drawable.bg_camera_header);
            }
        }
    }

    public int getCurrentIndex() {
        return this.currentFocusIndex;
    }

    private void setTabText(int index, CharSequence text) {
        ((TextView) getTabItem(index).findViewById(R.id.tab_text)).setText(text);
    }

    private void setTabIcon(int index, TabIconRes res) {
        boolean z;
        TextView tv = (TextView) getTabItem(index).findViewById(R.id.tab_text);
        if (res == TabIconRes.NO_ICON) {
            tv.setCompoundDrawables(null, null, null, null);
            return;
        }
        ImageCacheManager imageManager = GlobalDataManager.getInstance().getImageManager();
        if (index == this.currentFocusIndex) {
            z = true;
        } else {
            z = false;
        }
        Drawable top = new BitmapDrawable(imageManager.loadBitmapFromResource(res.getRes(z)));
        top.setBounds(0, 0, GlobalDataManager.getInstance().getApplicationContext().getResources().getDimensionPixelSize(R.dimen.tab_icon_width), GlobalDataManager.getInstance().getApplicationContext().getResources().getDimensionPixelSize(R.dimen.tab_icon_height));
        tv.setCompoundDrawables(null, top, null, null);
    }

    public void setTabSelectListener(TabSelectListener listener) {
        this.tabChangeListener = listener;
    }

    private void initTabButton(View rootView, int num) {
        for (int i = 0; i < num; i++) {
            getTabItem(i).setTag(Integer.valueOf(i));
        }
        for (int i2 = num; i2 < 4; i2++) {
            getTabItem(i2).setVisibility(8);
        }
        initTabSelectAction(num);
    }

    private View getTabItem(int index) {
        return this.tabBarRoot.getChildAt(index);
    }

    private void initTabSelectAction(int count) {
        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View v) {
                CustomizeTabHost.this.switchTab(((Integer) v.getTag()).intValue());
            }
        };
        for (int i = 0; i < count; i++) {
            getTabItem(i).setOnClickListener(listener);
        }
    }

    /* access modifiers changed from: private */
    public void switchTab(int newIndex) {
        if (newIndex != this.currentFocusIndex) {
            if (this.tabChangeListener != null) {
                this.tabChangeListener.beforeChange(this.currentFocusIndex, newIndex);
            }
            showTab(newIndex);
            this.currentFocusIndex = newIndex;
            if (this.tabChangeListener != null) {
                this.tabChangeListener.afterChange(this.currentFocusIndex);
            }
        } else if (this.tabChangeListener != null) {
            this.tabChangeListener.deprecatSelect(this.currentFocusIndex);
        }
    }

    public void setCurrentFocusIndex(int index) {
        this.currentFocusIndex = index;
    }

    public int getTabCount() {
        return this.tabCount;
    }

    public void showTab(int index) {
        setCurrentFocusIndex(index);
        Resources res = this.tabBarRoot.getResources();
        int i = 0;
        while (i < this.tabCount) {
            View tabItem = getTabItem(i);
            tabItem.findViewById(R.id.tab_arrow).setVisibility(i == index ? 0 : 4);
            ((TextView) tabItem.findViewById(R.id.tab_text)).setTextColor(res.getColor(i == index ? R.color.tab_font_foucs : R.color.tab_font));
            setTabIcon(i, this.tabIconsRes[i]);
            if (i == this.currentFocusIndex) {
                tabItem.setBackgroundColor(res.getColor(R.color.tab_bg_select));
            } else {
                tabItem.setBackgroundResource(R.drawable.bg_camera_header);
            }
            i++;
        }
    }
}
