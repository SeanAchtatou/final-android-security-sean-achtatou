package com.baixing.view.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.baixing.activity.BaseActivity;

public class FirstRunFragment extends DialogFragment {
    public static FirstRunFragment create(String target, int layoutId) {
        FirstRunFragment f = new FirstRunFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("layout", layoutId);
        bundle.putString("target", target);
        f.setArguments(bundle);
        return f;
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        ((BaseActivity) getActivity()).onHideFirstRun(getArguments().getString("target"));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(0, 16973840);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(getArguments().getInt("layout"), (ViewGroup) null);
        v.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FirstRunFragment.this.dismissAllowingStateLoss();
            }
        });
        return v;
    }

    public boolean hasGlobalTab() {
        return false;
    }
}
