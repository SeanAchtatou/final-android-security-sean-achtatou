package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.sharing.referral.ReferralNetwork;
import com.baixing.sharing.referral.ReferralPromoter;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.LoginUtil;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;

public class LoginFragment extends BaseFragment implements LoginUtil.LoginListener {
    public static final String KEY_RETURN_CODE = "login_return_code";
    private static final int MSG_FORGETPASSWORDVIEW = 4;
    private static final int MSG_LOGINFAIL = 1;
    private static final int MSG_LOGINSUCCESS = 2;
    public static final int MSG_LOGIN_SUCCESS = 305463295;
    private static final int MSG_NEWREGISTERVIEW = 3;
    public String backPageName = "back";
    public String categoryEnglishName = "";
    private LoginUtil loginHelper;
    private boolean needShowDlg = false;
    private boolean paused = false;

    public void onLoginFail(String message) {
        sendMessage(1, message);
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LOGIN_SUBMIT).append(TrackConfig.TrackMobile.Key.LOGIN_RESULT_STATUS, "0").append(TrackConfig.TrackMobile.Key.LOGIN_RESULT_FAIL_REASON, message).end();
        Log.d("loginfragment", "onLoginFail");
    }

    public void onLoginSucceed(String message) {
        sendMessage(2, message);
        Log.d("loginfragment", "onLoginSucceed");
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LOGIN_SUBMIT).append(TrackConfig.TrackMobile.Key.LOGIN_RESULT_STATUS, "1").end();
    }

    public void onRegisterClicked() {
        sendMessage(3, null);
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LOGIN_REGISTER).end();
    }

    public void onForgetClicked() {
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LOGIN_FORGETPASSWORD).end();
        sendMessage(4, null);
    }

    public boolean handleBack() {
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LOGIN_BACK).end();
        return super.handleBack();
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_leftActionHint = "返回";
        title.m_visible = true;
        title.m_rightActionHint = "注册";
        title.m_title = "登录";
    }

    public int[] excludedOptionMenus() {
        return new int[]{3};
    }

    public void handleRightAction() {
        super.handleRightAction();
        onRegisterClicked();
    }

    public void onPause() {
        super.onPause();
        this.paused = true;
    }

    public void onResume() {
        super.onResume();
        this.pv = TrackConfig.TrackMobile.PV.LOGIN;
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.LOGIN).end();
        this.paused = false;
    }

    public void onStackTop(boolean isBack) {
        super.onStackTop(isBack);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.logout();
        this.backPageName = getArguments().getString(BaseFragment.ARG_COMMON_BACK_HINT);
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout llLoginRoot = (RelativeLayout) inflater.inflate((int) R.layout.login, (ViewGroup) null);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("defaultNumber")) {
            String number = bundle.getString("defaultNumber");
            if (Util.isValidMobile(number)) {
                ((TextView) llLoginRoot.findViewById(R.id.et_account)).setText(number);
            }
        }
        this.loginHelper = new LoginUtil(llLoginRoot, this);
        return llLoginRoot;
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int resultCode, Object result) {
        super.onFragmentBackWithData(resultCode, result);
        if (resultCode == 101 || resultCode == -983039) {
            finishFragment(resultCode, result);
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        hideProgress();
        switch (msg.what) {
            case 1:
                String msgToShow = "登录未成功，请稍后重试！";
                if (msg.obj != null && (msg.obj instanceof String) && !TextUtils.isEmpty((String) msg.obj)) {
                    msgToShow = (String) msg.obj;
                }
                ViewUtil.showToast(activity, msgToShow, false);
                return;
            case 2:
                if (!TextUtils.isEmpty(ReferralPromoter.getInstance().ID())) {
                    Log.d("QLM", ReferralPromoter.getInstance().ID());
                    ReferralNetwork.getInstance().savePromoTask(ReferralUtil.TASK_APP, ReferralPromoter.getInstance().ID(), GlobalDataManager.getInstance().getAccountManager().getCurrentUser().getId().substring(1), Util.getDeviceUdid(GlobalDataManager.getInstance().getApplicationContext()), null, null, null);
                }
                if (msg.obj == null || !(msg.obj instanceof String)) {
                    ViewUtil.showToast(activity, "登陆成功", false);
                } else {
                    ViewUtil.showToast(activity, (String) msg.obj, false);
                }
                if (getArguments() == null || !getArguments().containsKey(KEY_RETURN_CODE)) {
                    finishFragment(MSG_LOGIN_SUCCESS, null);
                    return;
                } else {
                    finishFragment(getArguments().getInt(KEY_RETURN_CODE), null);
                    return;
                }
            case 3:
                pushFragment(new RegisterFragment(), createArguments(null, null));
                return;
            case 4:
                Bundle bundle = createArguments("找回密码", null);
                bundle.putString(ForgetPassFragment.Forget_Type, "forget");
                String phone = ((TextView) getView().findViewById(R.id.et_account)).getText().toString();
                if (phone != null && phone.length() > 0 && Util.isValidMobile(phone)) {
                    bundle.putString("defaultNumber", phone);
                }
                pushFragment(new ForgetPassFragment(), bundle);
                return;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            default:
                return;
            case 10:
                hideProgress();
                ViewUtil.showToast(getActivity(), "网络连接失败，请检查设置！", true);
                return;
        }
    }

    public boolean hasGlobalTab() {
        return false;
    }
}
