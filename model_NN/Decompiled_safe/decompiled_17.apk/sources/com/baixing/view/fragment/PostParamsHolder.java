package com.baixing.view.fragment;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class PostParamsHolder implements Serializable {
    public static final String INVALID_VALUE = "INVALID_VALUE_KEY";
    private static final long serialVersionUID = 2515425925056832882L;
    private LinkedHashMap<String, ValuePair> map = new LinkedHashMap<>();

    static class ValuePair implements Serializable {
        private static final long serialVersionUID = -6486097669248525239L;
        String uiValue;
        String value;

        ValuePair(String ui, String v) {
            this.uiValue = ui;
            this.value = v;
        }
    }

    public int size() {
        return this.map.size();
    }

    public void clear() {
        this.map.clear();
    }

    public void put(String key, String uiValue, String data) {
        this.map.put(key, new ValuePair(uiValue, data));
    }

    public void remove(String key) {
        this.map.remove(key);
    }

    public boolean containsKey(String key) {
        return this.map.containsKey(key);
    }

    public Iterator<String> keyIterator() {
        return this.map.keySet().iterator();
    }

    public String getData(String key) {
        if (this.map.containsKey(key)) {
            return this.map.get(key).value;
        }
        return null;
    }

    public String getUiData(String key) {
        if (this.map.containsKey(key)) {
            return this.map.get(key).uiValue;
        }
        return null;
    }

    public void merge(PostParamsHolder params) {
        if (params != null && params != this) {
            this.map.putAll(params.map);
        }
    }

    public Map<String, String> toParams() {
        Map<String, String> params = new HashMap<>();
        for (String key : this.map.keySet()) {
            if (!INVALID_VALUE.equals(this.map.get(key).value) && !"".equals(key)) {
                String value = this.map.get(key).value;
                if (value.contains(",")) {
                    String[] vals = value.split(",");
                    params.put(key + "[0]", vals[0]);
                    params.put(key + "[1]", "*".equals(vals[1]) ? "" : vals[1]);
                } else {
                    params.put(key, this.map.get(key).value);
                }
            }
        }
        return params;
    }

    public String toUrlString() {
        StringBuffer result = new StringBuffer();
        for (String key : this.map.keySet()) {
            if (!INVALID_VALUE.equals(this.map.get(key).value) && !"".equals(key)) {
                result.append(" AND ").append(key).append(":").append(this.map.get(key).value);
            }
        }
        if (result.length() > 4) {
            result.replace(0, 4, "");
        }
        return result.toString();
    }
}
