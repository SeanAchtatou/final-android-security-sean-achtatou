package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterFragment extends BaseFragment {
    public static final int MSG_REGISTER_SUCCESS = 101;
    /* access modifiers changed from: private */
    public EditText accoutnEt;
    /* access modifiers changed from: private */
    public EditText passwordEt;

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String deviceNum;
        View v = inflater.inflate((int) R.layout.register, (ViewGroup) null);
        this.accoutnEt = (EditText) v.findViewById(R.id.accountEt);
        Bundle bundle = getArguments();
        boolean defaultNum = false;
        if (bundle != null && bundle.containsKey("defaultNumber")) {
            String number = bundle.getString("defaultNumber");
            if (Util.isValidMobile(number)) {
                this.accoutnEt.setText(number);
                defaultNum = true;
            }
        }
        if (!defaultNum && (deviceNum = Util.getDevicePhoneNumber()) != null && Util.isValidMobile(deviceNum)) {
            this.accoutnEt.setText(deviceNum);
        }
        this.passwordEt = (EditText) v.findViewById(R.id.passwordEt);
        ((Button) v.findViewById(R.id.registerBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (RegisterFragment.this.check()) {
                    RegisterFragment.this.doRegister(RegisterFragment.this.accoutnEt.getText().toString(), RegisterFragment.this.passwordEt.getText().toString(), ((TextView) RegisterFragment.this.getView().findViewById(R.id.et_verifyCode)).getText().toString());
                }
            }
        });
        ((Button) v.findViewById(R.id.btn_getVerifyCode)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (TextUtils.isEmpty(RegisterFragment.this.accoutnEt.getText())) {
                    ViewUtil.showToast(RegisterFragment.this.getAppContext(), "请填写手机号码", false);
                } else {
                    RegisterFragment.this.getVerifyCode(RegisterFragment.this.accoutnEt.getText().toString());
                }
            }
        });
        return v;
    }

    /* access modifiers changed from: private */
    public void doRegister(String mobile, String pwd, String verifyCode) {
        showSimpleProgress();
        ApiParams param = new ApiParams();
        param.addParam("mobile", mobile);
        param.addParam("mobile_code", verifyCode);
        param.addParam("password", pwd);
        BaseApiCommand.createCommand("user.register/", true, param).execute(getAppContext(), new BaseApiCommand.Callback() {
            public void onNetworkDone(String apiName, String responseData) {
                RegisterFragment.this.hideProgress();
                RegisterFragment.this.sendMessage(1, responseData);
            }

            public void onNetworkFail(String apiName, ApiError error) {
                RegisterFragment.this.hideProgress();
                RegisterFragment.this.sendMessage(2, error == null ? "注册失败" : TextUtils.isEmpty(error.getMsg()) ? "注册失败" : error.getMsg());
            }
        });
    }

    /* access modifiers changed from: private */
    public void getVerifyCode(String mobile) {
        ApiParams param = new ApiParams();
        param.addParam("mobile", mobile);
        showSimpleProgress();
        BaseApiCommand.createCommand("user.sendMobileCode/", true, param).execute(getAppContext(), new BaseApiCommand.Callback() {
            public void onNetworkDone(String apiName, String responseData) {
                RegisterFragment.this.hideProgress();
                ViewUtil.showToast(RegisterFragment.this.getAppContext(), "验证码已发送", false);
            }

            public void onNetworkFail(String apiName, ApiError error) {
                RegisterFragment.this.hideProgress();
                ViewUtil.showToast(RegisterFragment.this.getAppContext(), "发送失败，请重试", false);
            }
        });
    }

    public boolean handleBack() {
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REGISTER_BACK).end();
        return super.handleBack();
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_title = "注册账号";
        title.m_visible = true;
        title.m_leftActionHint = "登录";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
     candidates:
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
    /* access modifiers changed from: private */
    public boolean check() {
        String msgToShow = null;
        boolean ret = true;
        try {
            if (this.accoutnEt.getText().toString().trim().equals("")) {
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REGISTER_SUBMIT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, "account is empty!").end();
                msgToShow = "账号不能为空！";
                ret = false;
            } else if (this.passwordEt.getText().toString().trim().equals("")) {
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REGISTER_SUBMIT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, "password is empty!").end();
                msgToShow = "密码不能为空！";
                ret = false;
            } else if (((TextView) getView().findViewById(R.id.et_verifyCode)).getText().toString().trim().equals("")) {
                msgToShow = "验证码不能为空！";
                ret = false;
            }
            return ret;
        } finally {
            if (msgToShow != null) {
                ViewUtil.showToast(getActivity(), msgToShow, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
     candidates:
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        String str;
        hideProgress();
        switch (msg.what) {
            case 1:
                try {
                    JSONObject jsonResult = new JSONObject((String) msg.obj).getJSONObject("result");
                    JSONObject jsonUser = jsonResult.getJSONObject("user");
                    String token = jsonResult.getString("token");
                    String id = jsonUser.getString(LocaleUtil.INDONESIAN);
                    if (!id.equals("")) {
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REGISTER_SUBMIT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, true).end();
                        UserBean user = new UserBean();
                        user.setId(id);
                        user.setPhone(this.accoutnEt.getText().toString());
                        user.setPassword(this.passwordEt.getText().toString());
                        GlobalDataManager.getInstance().setPhoneNumber(user.getPhone());
                        Util.saveDataToLocate(activity, "user", user);
                        GlobalDataManager.getInstance().setLoginUserToken(token);
                        Util.saveDataToLocate(GlobalDataManager.getInstance().getApplicationContext(), "loginToken", token);
                        BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_LOGIN, user);
                        finishFragment(101, null);
                        return;
                    }
                    return;
                } catch (JSONException e) {
                    e.printStackTrace();
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REGISTER_SUBMIT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, e.getMessage()).end();
                    return;
                }
            case 2:
                if (TextUtils.isEmpty((String) msg.obj)) {
                    str = "注册未成功，请稍后重试！";
                } else {
                    str = (String) msg.obj;
                }
                ViewUtil.showToast(activity, str, true);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.REGISTER_SUBMIT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, (String) msg.obj).end();
                return;
            default:
                return;
        }
    }

    public void onResume() {
        super.onResume();
        this.pv = TrackConfig.TrackMobile.PV.REGISTER;
        Tracker.getInstance().pv(this.pv).end();
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int requestCode, Object result) {
        finishFragment(requestCode, result);
    }
}
