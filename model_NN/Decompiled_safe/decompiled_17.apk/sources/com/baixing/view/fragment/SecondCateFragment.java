package com.baixing.view.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Category;
import com.baixing.network.NetworkUtil;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecondCateFragment extends BaseFragment implements AdapterView.OnItemClickListener {
    private Category cate = null;
    private boolean isPost = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.isPost = getArguments().getBoolean("isPost", false);
        this.cate = (Category) getArguments().getSerializable("cates");
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate((int) R.layout.secondcate, (ViewGroup) null);
        v.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        GridView gridView = (GridView) v.findViewById(R.id.gridSecCategory);
        List<Map<String, Object>> list = new ArrayList<>();
        for (Category cate2 : this.cate.getChildren()) {
            Map<String, Object> map = new HashMap<>();
            map.put("tvCategoryName", cate2.getName());
            list.add(map);
        }
        gridView.setAdapter((ListAdapter) new SimpleAdapter(getActivity(), list, R.layout.item_seccategory, new String[]{"tvCategoryName"}, new int[]{R.id.tvCategoryName}));
        gridView.setOnItemClickListener(this);
        return v;
    }

    public void onResume() {
        super.onResume();
        if (!this.isPost) {
            this.pv = TrackConfig.TrackMobile.PV.CATEGORIES;
            Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.CATEGORIES).append(TrackConfig.TrackMobile.Key.FIRSTCATENAME, this.cate.getEnglishName()).end();
        }
    }

    public void onStackTop(boolean isBack) {
        super.onStackTop(isBack);
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int requestCode, Object result) {
        if (-268435440 == requestCode) {
            finishFragment(requestCode, result);
        }
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        if (this.cate != null && this.cate.getChildren() != null && this.cate.getChildren().size() > arg2) {
            PerformanceTracker.stamp(PerformEvent.Event.E_Category_Clicked);
            Category secCate = this.cate.getChildren().get(arg2);
            if (!this.isPost) {
                final Bundle bundle = createArguments(secCate.getName(), "返回");
                bundle.putString("categoryEnglishName", secCate.getEnglishName());
                bundle.putString("siftresult", "");
                if (this.fragmentRequestCode != INVALID_REQUEST_CODE) {
                    finishFragment(this.fragmentRequestCode, secCate.getEnglishName() + "," + secCate.getName());
                    return;
                }
                bundle.putString("categoryName", secCate.getName());
                GlobalDataManager.getInstance().updateLastUsedCategory(secCate.getName(), secCate.getEnglishName());
                if (GlobalDataManager.isTextMode() || !GlobalDataManager.needNotifySwitchMode() || NetworkUtil.isWifiConnection(arg0.getContext())) {
                    PerformanceTracker.stamp(PerformEvent.Event.E_Start_ListingFragment);
                    pushFragment(new ListingFragment(), bundle);
                    return;
                }
                new AlertDialog.Builder(getActivity()).setTitle((int) R.string.dialog_title_info).setMessage((int) R.string.label_warning_flow_optimize).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.BROWSEMODENOIMAGE).append(TrackConfig.TrackMobile.Key.RESULT, TrackConfig.TrackMobile.Value.NO).end();
                        GlobalDataManager.setTextMode(false);
                        PerformanceTracker.stamp(PerformEvent.Event.E_Start_ListingFragment);
                        SecondCateFragment.this.pushFragment(new ListingFragment(), bundle);
                    }
                }).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.BROWSEMODENOIMAGE).append(TrackConfig.TrackMobile.Key.RESULT, TrackConfig.TrackMobile.Value.YES).end();
                        GlobalDataManager.setTextMode(true);
                        ViewUtil.postShortToastMessage(SecondCateFragment.this.getView(), (int) R.string.label_warning_switch_succed, 100);
                        PerformanceTracker.stamp(PerformEvent.Event.E_Start_ListingFragment);
                        SecondCateFragment.this.pushFragment(new ListingFragment(), bundle);
                    }
                }).create().show();
                return;
            }
            String names = secCate.getEnglishName() + "," + secCate.getName();
            if (this.fragmentRequestCode != INVALID_REQUEST_CODE) {
                finishFragment(this.fragmentRequestCode, names);
                return;
            }
            Bundle bundle2 = createArguments(null, null);
            bundle2.putSerializable(PostGoodsFragment.KEY_INIT_CATEGORY, names);
            pushFragment(new PostGoodsFragment(), bundle2);
        }
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = this.cate.getName();
        title.m_leftActionHint = "返回";
        if (this.isPost) {
            title.hasGlobalSearch = false;
            title.m_rightActionHint = null;
            return;
        }
        title.hasGlobalSearch = true;
    }

    public void handleSearch() {
        Bundle bundle = new Bundle();
        bundle.putString("categoryEnglishName", this.cate.getEnglishName());
        bundle.putString("categoryName", this.cate.getName());
        pushFragment(new SearchFragment(), bundle);
    }
}
