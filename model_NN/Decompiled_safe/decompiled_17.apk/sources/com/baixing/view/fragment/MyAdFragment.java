package com.baixing.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.baidu.mapapi.MKEvent;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.PersonalActivity;
import com.baixing.adapter.VadListAdapter;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.AdList;
import com.baixing.entity.UserBean;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.NetworkUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.LogData;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.ErrorHandler;
import com.baixing.util.FavoriteNetworkUtil;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.util.VadListLoader;
import com.baixing.util.ViewUtil;
import com.baixing.widget.PullToRefreshListView;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MyAdFragment extends BaseFragment implements PullToRefreshListView.OnRefreshListener, VadListLoader.Callback, Observer {
    public static final String TYPE_KEY = "PersonalPostFragment_type_key";
    static final int TYPE_MYFAVORITES = 3;
    static final int TYPE_MYPOST = 0;
    private final int MCMESSAGE_DELETE = 5;
    private final int MSG_ASK_REFRESH = 13;
    private final int MSG_DELETE_POST_FAIL = 7;
    private final int MSG_DELETE_POST_SUCCESS = 6;
    private final int MSG_ITEM_OPERATE = 10;
    private final int MSG_MYFAVS = 4;
    private final int MSG_MYPOST = 1;
    private final int MSG_REFRESH_FAIL = 12;
    private final int MSG_RESTORE_POST_FAIL = 9;
    private final int MSG_SHOW_BIND_DIALOG = 11;
    private final int MSG_UPDATE_LIST = 14;
    /* access modifiers changed from: private */
    public VadListAdapter adapter = null;
    private int currentType = 0;
    /* access modifiers changed from: private */
    public VadListLoader glLoader = null;
    private boolean isOnResume = false;
    private boolean isRefreshing = false;
    private List<Ad> listMyPost = null;
    /* access modifiers changed from: private */
    public PullToRefreshListView lvGoodsList;
    private boolean needReloadData = false;
    private boolean showShareDlg = false;
    /* access modifiers changed from: private */
    public UserBean user;

    public void onCreate(Bundle savedInstanceState) {
        Intent intent;
        String action;
        PerformanceTracker.stamp(PerformEvent.Event.E_MyAd_OnCreate);
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(TYPE_KEY)) {
            this.currentType = arguments.getInt(TYPE_KEY, 0);
        }
        Activity activity = getActivity();
        if (!(activity == null || (intent = activity.getIntent()) == null || (action = intent.getAction()) == null || !action.equals(CommonIntentAction.ACTION_BROADCAST_POST_FINISH))) {
            this.showShareDlg = true;
            intent.setAction("");
        }
        this.user = (UserBean) Util.loadDataFromLocate(getActivity(), "user", UserBean.class);
        this.listMyPost = GlobalDataManager.getInstance().getListMyPost();
        filterOutAd(this.listMyPost, this.user);
        this.glLoader = new VadListLoader(null, this, null, null);
        this.glLoader.setHasMore(false);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGIN);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
    }

    public void onDestroy() {
        super.onDestroy();
        BxMessageCenter.defaultMessageCenter().removeObserver(this);
    }

    private void filterOutAd(List<Ad> list, UserBean user2) {
        if (list != null && user2 != null) {
            int i = 0;
            while (i < this.listMyPost.size()) {
                if (!list.get(i).getValueByKey(ApiParams.KEY_USERID).equals(user2.getId())) {
                    list.remove(i);
                } else {
                    i++;
                }
            }
        }
    }

    public void onStackTop(boolean isBack) {
        if (!isBack || this.needReloadData) {
            PerformanceTracker.stamp(PerformEvent.Event.E_MyAd_FireRefresh);
            this.lvGoodsList.fireRefresh();
            this.needReloadData = false;
        }
        if (this.glLoader.getGoodsList().getData() != null && this.glLoader.getGoodsList().getData().size() > 0) {
            this.lvGoodsList.setSelectionFromHeader(this.glLoader.getSelection());
        }
    }

    /* access modifiers changed from: private */
    public boolean isMyPostView() {
        return this.currentType == 0;
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Intent intent;
        View v = inflater.inflate((int) R.layout.personalcenterview, (ViewGroup) null);
        v.findViewById(R.id.linearType).setVisibility(8);
        try {
            if (!NetworkUtil.isNetworkActive(v.getContext())) {
                ErrorHandler.getInstance().handleError(-10, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.lvGoodsList = (PullToRefreshListView) v.findViewById(R.id.lvGoodsList);
        this.adapter = new VadListAdapter(getActivity(), isMyPostView() ? this.listMyPost : GlobalDataManager.getInstance().getListMyStore(), null);
        this.adapter.setHasDelBtn(true);
        this.adapter.setOperateMessage(this.handler, 10);
        this.lvGoodsList.setAdapter((ListAdapter) this.adapter);
        AdList gl = new AdList();
        gl.setData(isMyPostView() ? this.listMyPost == null ? new ArrayList() : this.listMyPost : GlobalDataManager.getInstance().getListMyStore());
        this.glLoader.setGoodsList(gl);
        this.glLoader.setSearchType(isMyPostView() ? VadListLoader.SEARCH_POLICY.SEARCH_USER_LIST : VadListLoader.SEARCH_POLICY.SEARCH_FAVORITES);
        this.lvGoodsList.setOnRefreshListener(this);
        Bundle bundle = null;
        Activity act = getActivity();
        if (!(act == null || (intent = act.getIntent()) == null)) {
            bundle = intent.getExtras();
        }
        if (bundle != null && bundle.containsKey("forceUpdate")) {
            if (bundle.getBoolean("forceUpdate")) {
                this.needReloadData = true;
            }
            bundle.remove("forceUpdate");
        }
        return v;
    }

    public void onPause() {
        super.onPause();
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
            this.lvGoodsList.invalidateViews();
        }
        for (int i = 0; i < this.lvGoodsList.getChildCount(); i++) {
            ImageView imageView = (ImageView) this.lvGoodsList.getChildAt(i).findViewById(R.id.ivInfo);
            if (!(imageView == null || imageView.getTag() == null || imageView.getTag().toString().length() <= 0)) {
                GlobalDataManager.getInstance().getImageLoaderMgr().Cancel(imageView.getTag().toString(), imageView);
            }
        }
    }

    private void setSharedStatus() {
        String sharedIds = (String) Util.loadDataFromLocate(getActivity(), CommonIntentAction.EXTRA_MSG_SHARED_AD_ID, String.class);
        if (sharedIds != null && sharedIds.length() != 0 && this.glLoader.getGoodsList().getData() != null) {
            String[] ids = sharedIds.split(",");
            for (int i = 0; i < ids.length; i++) {
                int j = 0;
                while (true) {
                    if (j >= this.glLoader.getGoodsList().getData().size()) {
                        break;
                    } else if (ids[i].equals(this.glLoader.getGoodsList().getData().get(j).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                        this.glLoader.getGoodsList().getData().get(j).setValueByKey("shared", "1");
                        break;
                    } else {
                        j++;
                    }
                }
            }
        }
    }

    public void onResume() {
        int i;
        PerformanceTracker.stamp(PerformEvent.Event.E_MyAdShowup);
        super.onResume();
        if (isMyPostView()) {
            LogData pv = Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.MYADS_SENT);
            TrackConfig.TrackMobile.Key key = TrackConfig.TrackMobile.Key.ADSCOUNT;
            if (this.listMyPost != null) {
                i = this.listMyPost.size();
            } else {
                i = 0;
            }
            pv.append(key, i).end();
        } else {
            Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.FAVADS).append(TrackConfig.TrackMobile.Key.ADSCOUNT, this.glLoader.getGoodsList().getData() != null ? this.glLoader.getGoodsList().getData().size() : 0).end();
        }
        rebuildPage(getView(), false);
        for (int i2 = 0; i2 < this.lvGoodsList.getChildCount(); i2++) {
            ImageView imageView = (ImageView) this.lvGoodsList.getChildAt(i2).findViewById(R.id.ivInfo);
            if (!(imageView == null || imageView.getTag() == null || imageView.getTag().toString().length() <= 0)) {
                GlobalDataManager.getInstance().getImageLoaderMgr().showImg(imageView, imageView.getTag().toString(), null, getActivity());
            }
        }
        setSharedStatus();
        this.glLoader.setHasMoreListener(null);
        this.glLoader.setCallback(this);
        this.adapter.setList(this.glLoader.getGoodsList().getData());
        this.lvGoodsList.setSelectionFromHeader(this.glLoader.getSelection());
    }

    /* access modifiers changed from: private */
    public boolean checkAndHandleDeletedAd(int index) {
        final Ad ad = this.glLoader.getGoodsList().getData().get(index);
        if (!ad.getValueByKey("status").equals("3")) {
            return false;
        }
        new AlertDialog.Builder(getActivity()).setTitle("该信息已被删除").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).setNegativeButton("取消收藏", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                MyAdFragment.this.showSimpleProgress();
                FavoriteNetworkUtil.cancelFavorite(MyAdFragment.this.getActivity(), ad.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID), MyAdFragment.this.user, MyAdFragment.this.getHandler());
            }
        }).create().show();
        return true;
    }

    private void rebuildPage(View rootView, boolean onResult) {
        if (this.glLoader != null) {
            this.glLoader.setCallback(this);
        }
        rootView.findViewById(R.id.linearListView);
        if (isMyPostView()) {
            AdList gl = new AdList();
            gl.setData(this.listMyPost);
            this.glLoader.setGoodsList(gl);
            this.adapter.setList(this.listMyPost);
            this.adapter.notifyDataSetChanged();
            this.lvGoodsList.invalidateViews();
            if (this.isOnResume) {
                this.isOnResume = false;
            }
        } else {
            this.adapter.setList(this.glLoader.getGoodsList().getData());
            this.adapter.notifyDataSetChanged();
            this.lvGoodsList.invalidateViews();
        }
        this.lvGoodsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int index = arg2 - MyAdFragment.this.lvGoodsList.getHeaderViewsCount();
                if (index >= 0 && !MyAdFragment.this.checkAndHandleDeletedAd(index)) {
                    Bundle bundle = MyAdFragment.this.createArguments(null, null);
                    bundle.putSerializable("loader", MyAdFragment.this.glLoader);
                    bundle.putInt("index", index);
                    MyAdFragment.this.pushFragment(new VadFragment(), bundle);
                }
            }
        });
        this.lvGoodsList.invalidateViews();
        this.lvGoodsList.setOnRefreshListener(this);
        this.lvGoodsList.setSelectionFromHeader(this.glLoader.getSelection());
    }

    private void doShare() {
        String lastPost;
        Bundle bundle = getArguments();
        if (isMyPostView() && this.listMyPost != null && this.listMyPost.size() > 0 && this.showShareDlg && (lastPost = bundle.getString("lastPost")) != null && lastPost.length() > 0) {
            String lastPost2 = lastPost.split(",")[0];
            for (int i = 0; i < this.listMyPost.size(); i++) {
                if (this.listMyPost.get(i).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(lastPost2)) {
                    new SharingFragment(this.listMyPost.get(i), "postSuccess").show(getFragmentManager(), (String) null);
                    this.showShareDlg = false;
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case ErrorHandler.ERROR_NETWORK_UNAVAILABLE:
            case -3:
                this.isRefreshing = false;
                hideProgress();
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_RESULT).append(TrackConfig.TrackMobile.Key.ADSCOUNT, 0).end();
                if (msg.what != -3 || msg.obj == null) {
                    ErrorHandler.getInstance().handleError(-10, null);
                } else {
                    ViewUtil.showToast(getActivity(), (String) msg.obj, false);
                }
                this.lvGoodsList.onRefreshComplete();
                this.lvGoodsList.onFail();
                return;
            case -1:
            case VadListLoader.MSG_FIRST_FAIL:
                hideProgress();
                this.isRefreshing = false;
                this.lvGoodsList.onRefreshComplete();
                return;
            case 1:
            case 4:
                PerformanceTracker.stamp(PerformEvent.Event.E_MyPost_Got);
                this.isRefreshing = false;
                hideProgress();
                AdList gl = JsonUtil.getGoodsListFromJson(this.glLoader.getLastJson());
                Log.d("MyAds", "gl:" + gl.toString());
                if (msg.what == 1) {
                    if (gl == null || gl.getData() == null) {
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_RESULT).append(TrackConfig.TrackMobile.Key.ADSCOUNT, 0).end();
                    } else {
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_RESULT).append(TrackConfig.TrackMobile.Key.ADSCOUNT, gl.getData().size()).end();
                    }
                }
                if (gl == null || gl.getData().size() == 0) {
                    if (msg.what != 1) {
                        GlobalDataManager.getInstance().updateFav(new ArrayList());
                    } else if (this.listMyPost != null) {
                        this.listMyPost.clear();
                    }
                    this.glLoader.setGoodsList(new AdList());
                } else if (msg.what == 1) {
                    this.listMyPost = gl.getData();
                    if (this.listMyPost != null) {
                        for (int i = this.listMyPost.size() - 1; i >= 0; i--) {
                            if (!this.listMyPost.get(i).getValueByKey("status").equals("0") && !this.listMyPost.get(i).getValueByKey("status").equals("4") && !this.listMyPost.get(i).getValueByKey("status").equals("20")) {
                                this.listMyPost.remove(i);
                            }
                        }
                    }
                    AdList gl2 = new AdList();
                    gl2.setData(this.listMyPost);
                    this.glLoader.setGoodsList(gl2);
                } else {
                    GlobalDataManager.getInstance().updateFav(gl.getData());
                    FavoriteNetworkUtil.syncFavorites(getActivity(), GlobalDataManager.getInstance().getAccountManager().getCurrentUser());
                    this.glLoader.setGoodsList(gl);
                }
                if (msg.what == 1) {
                    GlobalDataManager.getInstance().setListMyPost(this.listMyPost);
                    setSharedStatus();
                }
                rebuildPage(rootView, true);
                this.lvGoodsList.onRefreshComplete();
                if (msg.what == 1) {
                    doShare();
                }
                PerformanceTracker.stamp(PerformEvent.Event.E_MyPost_Got_Handled);
                return;
            case 5:
                String id = this.glLoader.getGoodsList().getData().get(msg.arg2).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID);
                showSimpleProgress();
                sendDeteleCmd(id, 0);
                return;
            case 6:
                hideProgress();
                Object deletedId = msg.obj;
                List<Ad> refList = null;
                if (msg.arg1 == 0) {
                    refList = this.listMyPost;
                }
                if (refList != null) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < refList.size()) {
                            if (refList.get(i2).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals((String) deletedId)) {
                                refList.remove(i2);
                            } else {
                                i2++;
                            }
                        }
                    }
                    if (msg.arg1 == -1) {
                        GlobalDataManager.getInstance().setListMyPost(this.listMyPost);
                    }
                    this.adapter.setList(refList);
                    this.adapter.notifyDataSetChanged();
                    this.lvGoodsList.invalidateViews();
                    ViewUtil.showToast(activity, "删除成功！", false);
                    this.adapter.setUiHold(false);
                    ViewUtil.showCommentsPromptDialog((BaseActivity) getActivity());
                    return;
                }
                return;
            case 7:
                hideProgress();
                ViewUtil.showToast(activity, "删除失败,请稍后重试！", false);
                return;
            case 9:
                hideProgress();
                ViewUtil.showToast(activity, "恢复失败,请稍后重试！", false);
                return;
            case 10:
                showItemOperateMenu(msg);
                return;
            case 12:
                hideProgress();
                ViewUtil.showToast(getActivity(), (String) msg.obj, false);
                return;
            case 13:
                hideProgress();
                final Pair<String, String> p = (Pair) msg.obj;
                new AlertDialog.Builder(getActivity()).setTitle("提醒").setMessage((CharSequence) p.first).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MyAdFragment.this.showSimpleProgress();
                        MyAdFragment.this.doRefresh(1, (String) p.second);
                        dialog.dismiss();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
                return;
            case MKEvent.MKEVENT_MAP_MOVE_FINISH:
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                    return;
                }
                return;
            case FavoriteNetworkUtil.MSG_CANCEL_FAVORITE_SUCCESS:
                hideProgress();
                FavoriteNetworkUtil.ReplyData data = (FavoriteNetworkUtil.ReplyData) msg.obj;
                List<Ad> refList2 = GlobalDataManager.getInstance().getListMyStore();
                if (refList2 != null) {
                    int i3 = 0;
                    while (true) {
                        if (i3 < refList2.size()) {
                            if (refList2.get(i3).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(data.id)) {
                                refList2.remove(i3);
                            } else {
                                i3++;
                            }
                        }
                    }
                    GlobalDataManager.getInstance().updateFav(refList2);
                    this.adapter.setList(refList2);
                    this.adapter.notifyDataSetChanged();
                    this.lvGoodsList.invalidateViews();
                    ViewUtil.showToast(activity, data.message, false);
                    this.adapter.setUiHold(false);
                    return;
                }
                return;
            case FavoriteNetworkUtil.MSG_CANCEL_FAVORITE_FAIL:
                hideProgress();
                ViewUtil.showToast(activity, (String) msg.obj, false);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.PersonalProfileFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public boolean handleBack() {
        Bundle bundle = createArguments(null, null);
        if (!(getActivity() instanceof PersonalActivity)) {
            return false;
        }
        ((BaseActivity) getActivity()).pushFragment((BaseFragment) new PersonalProfileFragment(), bundle, true);
        return true;
    }

    private String getPostCateEnglishName() {
        Bundle bundle = getArguments();
        if (bundle == null || !bundle.containsKey("isEditPost")) {
            return "";
        }
        return bundle.getString("cateEnglishName");
    }

    /* access modifiers changed from: private */
    public boolean isValidMessage(Ad detail) {
        return !detail.getValueByKey("status").equals("4") && !detail.getValueByKey("status").equals("20");
    }

    /* access modifiers changed from: private */
    public void postDelete(final LogData event, final String adId, long postedSeconds) {
        new AlertDialog.Builder(getActivity()).setTitle("提醒").setMessage("是否确定删除").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MyAdFragment.this.showSimpleProgress();
                MyAdFragment.this.sendDeteleCmd(adId, 0);
                event.end();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    private void showItemOperateMenu(Message msg) {
        int r_array_item_operate;
        final Ad detail = this.glLoader.getGoodsList().getData().get(msg.arg2);
        final String adId = detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID);
        String tmpInsertedTime = detail.data.get("insertedTime");
        long tmpPostedSeconds = -1;
        if (tmpInsertedTime != null) {
            tmpPostedSeconds = (new Date().getTime() / 1000) - Long.valueOf(tmpInsertedTime).longValue();
        }
        final long postedSeconds = tmpPostedSeconds;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("操作");
        if (!isMyPostView()) {
            r_array_item_operate = R.array.item_operate_favorite;
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.FAV_MANAGE).end();
        } else if (isValidMessage(detail)) {
            r_array_item_operate = R.array.item_operate_mypost;
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_MANAGE).append(TrackConfig.TrackMobile.Key.STATUS, TrackConfig.TrackMobile.Value.VALID).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds).end();
        } else {
            r_array_item_operate = R.array.item_operate_inverify;
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_MANAGE).append(TrackConfig.TrackMobile.Key.STATUS, TrackConfig.TrackMobile.Value.APPROVING).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds).end();
        }
        builder.setItems(r_array_item_operate, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int clickedIndex) {
                if (MyAdFragment.this.isMyPostView()) {
                    if (MyAdFragment.this.isValidMessage(detail)) {
                        switch (clickedIndex) {
                            case 0:
                                new SharingFragment(detail, "myAdList").show(MyAdFragment.this.getFragmentManager(), (String) null);
                                return;
                            case 1:
                                MyAdFragment.this.doRefresh(0, adId);
                                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_REFRESH).append(TrackConfig.TrackMobile.Key.STATUS, TrackConfig.TrackMobile.Value.VALID).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds).end();
                                return;
                            case 2:
                                Bundle args = MyAdFragment.this.createArguments(null, null);
                                args.putSerializable("goodsDetail", detail);
                                args.putString(PostGoodsFragment.KEY_INIT_CATEGORY, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME));
                                MyAdFragment.this.pushFragment(new EditAdFragment(), args);
                                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_EDIT).append(TrackConfig.TrackMobile.Key.STATUS, TrackConfig.TrackMobile.Value.VALID).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds).end();
                                return;
                            case 3:
                                MyAdFragment.this.postDelete(Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_DELETE).append(TrackConfig.TrackMobile.Key.STATUS, TrackConfig.TrackMobile.Value.VALID).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds), adId, postedSeconds);
                                return;
                            default:
                                return;
                        }
                    } else {
                        switch (clickedIndex) {
                            case 0:
                                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_APPEAL).append(TrackConfig.TrackMobile.Key.STATUS, TrackConfig.TrackMobile.Value.APPROVING).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds).end();
                                Bundle bundle = MyAdFragment.this.createArguments("申诉", null);
                                bundle.putInt("type", 1);
                                bundle.putString("adId", adId);
                                MyAdFragment.this.pushFragment(new FeedbackFragment(), bundle);
                                return;
                            case 1:
                                MyAdFragment.this.postDelete(Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SENT_DELETE).append(TrackConfig.TrackMobile.Key.STATUS, TrackConfig.TrackMobile.Value.APPROVING).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds), adId, postedSeconds);
                                return;
                            default:
                                return;
                        }
                    }
                } else if (clickedIndex == 0) {
                    if (GlobalDataManager.getInstance().getAccountManager().isUserLogin()) {
                        MyAdFragment.this.showSimpleProgress();
                        FavoriteNetworkUtil.cancelFavorite(MyAdFragment.this.getActivity(), adId, MyAdFragment.this.user, MyAdFragment.this.getHandler());
                    } else {
                        GlobalDataManager.getInstance().removeFav(detail);
                        MyAdFragment.this.adapter.setList(GlobalDataManager.getInstance().getListMyStore());
                        MyAdFragment.this.adapter.notifyDataSetChanged();
                        MyAdFragment.this.lvGoodsList.invalidateViews();
                    }
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.FAV_DELETE).end();
                }
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void doRefresh(int pay, String adId) {
        ApiParams params = new ApiParams();
        params.addParam(LocaleUtil.INDONESIAN, adId);
        params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        showSimpleProgress();
        BaseApiCommand.createCommand("ad.freeRefresh/", true, params).execute(getActivity(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                MyAdFragment.this.sendMessage(12, error == null ? "刷新失败，请稍后重试！" : error.getMsg());
            }

            public void onNetworkDone(String apiName, String responseData) {
                MyAdFragment.this.sendMessage(12, "刷新成功");
            }
        });
    }

    /* access modifiers changed from: private */
    public void sendDeteleCmd(final String id, final int currentType2) {
        ApiParams params = new ApiParams();
        params.addParam(LocaleUtil.INDONESIAN, id);
        params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        BaseApiCommand.createCommand("ad.delete/", true, params).execute(getActivity(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                MyAdFragment.this.hideProgress();
                MyAdFragment.this.sendMessage(7, null);
            }

            public void onNetworkDone(String apiName, String responseData) {
                MyAdFragment.this.hideProgress();
                Message msg = Message.obtain();
                msg.obj = id;
                msg.arg1 = currentType2;
                msg.what = 6;
                MyAdFragment.this.handler.sendMessage(msg);
            }
        });
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_leftActionHint = "返回";
        if (isMyPostView()) {
            title.m_title = "已发布信息";
        } else {
            title.m_title = "收藏信息";
        }
        title.m_visible = true;
    }

    public void onRefresh() {
        if (!this.isRefreshing) {
            if (this.user == null || TextUtils.isEmpty(this.user.getPhone())) {
                this.lvGoodsList.onRefreshComplete();
                return;
            }
            ApiParams params = new ApiParams();
            if (isMyPostView()) {
                Bundle bundle = null;
                if (!(getActivity() == null || getActivity().getIntent() == null)) {
                    bundle = getActivity().getIntent().getExtras();
                }
                if (!(bundle == null || bundle.getString("lastPost") == null)) {
                    params.addParam("newAdIds", bundle.getString("lastPost"));
                }
                params.addParam("status", "3");
            }
            this.glLoader.setRows(1000);
            this.glLoader.setParams(params);
            int msg = isMyPostView() ? 1 : 4;
            PerformanceTracker.stamp(PerformEvent.Event.E_MyAdStartFetching);
            this.glLoader.startFetching(getAppContext(), true, msg, msg, msg, !NetworkUtil.isNetworkActive(getAppContext()));
            this.isRefreshing = true;
        }
    }

    public void onFragmentBackWithData(int message, Object obj) {
        if (65536 == message) {
            if (obj != null && this.listMyPost != null) {
                boolean updateUi = false;
                int i = 0;
                while (true) {
                    if (i >= this.listMyPost.size()) {
                        break;
                    } else if (this.listMyPost.get(i).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals((String) obj)) {
                        this.listMyPost.remove(i);
                        updateUi = true;
                        break;
                    } else {
                        i++;
                    }
                }
                if (updateUi) {
                    this.adapter.setList(this.listMyPost);
                    this.adapter.notifyDataSetChanged();
                    this.lvGoodsList.invalidateViews();
                }
            }
        } else if (65537 == message) {
            if (this.glLoader.getGoodsList() != null && this.glLoader.getGoodsList().getData() != null && this.glLoader.getGoodsList().getData().size() > 0) {
                if (GlobalDataManager.getInstance().getListMyPost() == null || GlobalDataManager.getInstance().getListMyPost().size() != this.glLoader.getGoodsList().getData().size()) {
                    AdList gl = new AdList();
                    gl.setData(GlobalDataManager.getInstance().getListMyPost());
                    this.glLoader.setGoodsList(gl);
                    this.adapter.setList(GlobalDataManager.getInstance().getListMyPost());
                    this.adapter.notifyDataSetChanged();
                    this.lvGoodsList.invalidateViews();
                }
            }
        } else if (-268435440 == message && this.lvGoodsList != null) {
            this.lvGoodsList.fireRefresh();
        }
    }

    public void update(Observable observable, Object data) {
        if (data instanceof BxMessageCenter.IBxNotification) {
            BxMessageCenter.IBxNotification note = (BxMessageCenter.IBxNotification) data;
            if (IBxNotificationNames.NOTIFICATION_LOGIN.equals(note.getName()) || IBxNotificationNames.NOTIFICATION_LOGOUT.equals(note.getName())) {
                this.user = (UserBean) note.getObject();
                if (isMyPostView()) {
                    filterOutAd(this.listMyPost, this.user);
                }
                this.needReloadData = true;
                this.adapter.notifyDataSetChanged();
            }
        }
    }

    public void onRequestComplete(int respCode, Object data) {
        Log.d("MyAd", "res:" + respCode + ";data:" + data.toString());
        sendMessage(respCode, data);
    }
}
