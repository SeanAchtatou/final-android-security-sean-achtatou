package com.baixing.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.baixing.activity.BaseFragment;
import com.baixing.adapter.CheckableAdapter;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.List;

public class OtherPropertiesFragment extends BaseFragment {
    /* access modifiers changed from: private */
    public List<CheckableAdapter.CheckableItem> others = null;
    private String selected = "";
    /* access modifiers changed from: private */
    public boolean singleSelection = false;
    private String title = "请选择要填写的项目";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        this.singleSelection = bundle.getBoolean("singleSelection", false);
        List<String> properties = (List) bundle.getSerializable("properties");
        this.selected = bundle.getString("selected");
        this.others = new ArrayList();
        for (int i = 0; i < properties.size(); i++) {
            CheckableAdapter.CheckableItem item = new CheckableAdapter.CheckableItem();
            item.checked = false;
            item.txt = (String) properties.get(i);
            this.others.add(item);
        }
        if (this.selected != null) {
            String[] selecteds = this.selected.split(",");
            for (int i2 = 0; i2 < selecteds.length; i2++) {
                for (int j = 0; j < this.others.size(); j++) {
                    CheckableAdapter.CheckableItem item2 = this.others.get(j);
                    if (selecteds[i2].equals(item2.txt)) {
                        item2.checked = true;
                        this.others.set(j, item2);
                    }
                }
            }
        }
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate((int) R.layout.post_othersview, (ViewGroup) null);
        ListView lv = (ListView) v.findViewById(R.id.post_other_list);
        lv.setDivider(null);
        final CheckableAdapter adapter = new CheckableAdapter(getActivity(), this.others, 536870911, false);
        lv.setAdapter((ListAdapter) adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long id) {
                CheckableAdapter.CheckableItem item = (CheckableAdapter.CheckableItem) OtherPropertiesFragment.this.others.get(position);
                item.checked = !item.checked;
                OtherPropertiesFragment.this.others.set(position, item);
                adapter.setList(OtherPropertiesFragment.this.others);
                if (OtherPropertiesFragment.this.singleSelection) {
                    OtherPropertiesFragment.this.finishFragment(OtherPropertiesFragment.this.fragmentRequestCode, Integer.valueOf(position));
                }
            }
        });
        return v;
    }

    public void handleRightAction() {
        String lists = "";
        for (int i = 0; i < this.others.size(); i++) {
            if (this.others.get(i).checked) {
                lists = lists + "," + i;
            }
        }
        if (lists.length() > 0) {
            lists = lists.substring(1);
        }
        finishFragment(this.fragmentRequestCode, lists);
    }

    public void initTitle(BaseFragment.TitleDef title2) {
        title2.m_visible = true;
        title2.m_title = this.title;
        title2.m_leftActionHint = "发布";
        if (!this.singleSelection) {
            title2.m_rightActionHint = "完成";
        }
    }
}
