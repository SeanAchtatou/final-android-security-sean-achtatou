package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.sharing.referral.ReferralNetwork;
import com.baixing.sharing.referral.ReferralPromoter;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.LoginUtil;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import java.util.Observable;
import java.util.Observer;
import org.json.JSONException;
import org.json.JSONObject;

public class ForgetPassFragment extends BaseFragment implements Observer {
    public static final String Forget_Type = "forget_type";
    public static final int MSG_FORGET_PWD_SUCCEED = -983039;
    private final int MSG_NETWORK_ERROR = 0;
    private final int MSG_POST_ERROR = 1;
    private final int MSG_POST_FINISH = 3;
    private CountDownTimer countTimer;
    /* access modifiers changed from: private */
    public boolean isForgetType = false;
    private EditText mobileEt;
    /* access modifiers changed from: private */
    public EditText newPwdEt;
    private Button postBtn;

    public void onResume() {
        this.pv = this.isForgetType ? TrackConfig.TrackMobile.PV.FORGETPASSWORD : TrackConfig.TrackMobile.PV.RESETPASSWORD;
        Tracker.getInstance().pv(this.pv).end();
        super.onResume();
    }

    public void update(Observable observable, Object data) {
        if ((data instanceof BxMessageCenter.IBxNotification) && IBxNotificationNames.NOTIFICATION_LOGOUT.equals(((BxMessageCenter.IBxNotification) data).getName())) {
            finishFragment();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(Forget_Type) && !bundle.getString(Forget_Type).equals("forget")) {
            BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
        }
    }

    public void onDestory() {
        super.onDestroy();
        this.countTimer.cancel();
        BxMessageCenter.defaultMessageCenter().removeObserver(this);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        Bundle bundle = getArguments();
        if (bundle == null || !bundle.containsKey(BaseFragment.ARG_COMMON_TITLE)) {
            title.m_title = "找回密码";
        } else {
            title.m_title = bundle.getString(BaseFragment.ARG_COMMON_TITLE);
        }
        title.m_leftActionHint = "返回";
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String deviceNum;
        View rootV = inflater.inflate((int) R.layout.forget_password, (ViewGroup) null);
        this.mobileEt = (EditText) rootV.findViewById(R.id.forgetPwdMobileEt);
        this.postBtn = (Button) rootV.findViewById(R.id.forgetPwdPostBtn);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(Forget_Type)) {
            if (bundle.getString(Forget_Type).equals("forget")) {
                rootV.findViewById(R.id.forgetPwdNewPwdEt).setVisibility(8);
                rootV.findViewById(R.id.forgetPwdOldPwdEt).setVisibility(8);
                this.postBtn.setText("找回密码");
                this.isForgetType = true;
                boolean defaultNum = false;
                if (bundle.containsKey("defaultNumber")) {
                    this.mobileEt.setText(bundle.getString("defaultNumber"));
                    defaultNum = true;
                }
                if (!defaultNum && (deviceNum = Util.getDevicePhoneNumber()) != null && Util.isValidMobile(deviceNum)) {
                    this.mobileEt.setText(deviceNum);
                }
                rootV.findViewById(R.id.btn_getVerifyCode).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (ForgetPassFragment.this.checkMobile()) {
                            ForgetPassFragment.this.getVerifyCode();
                        }
                    }
                });
            } else {
                this.mobileEt.setVisibility(8);
                rootV.findViewById(R.id.rl_verify_code).setVisibility(8);
                this.newPwdEt = (EditText) rootV.findViewById(R.id.forgetPwdNewPwdEt);
                this.postBtn.setText("修改密码");
                this.isForgetType = false;
            }
        }
        this.postBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!ForgetPassFragment.this.isForgetType || ForgetPassFragment.this.checkAllInputs()) {
                    ForgetPassFragment.this.showProgress(R.string.dialog_title_info, R.string.dialog_message_waiting, false);
                    if (ForgetPassFragment.this.isForgetType) {
                        ForgetPassFragment.this.doReset();
                    } else {
                        ForgetPassFragment.this.doUpdate();
                    }
                }
            }
        });
        return rootV;
    }

    /* access modifiers changed from: private */
    public void getVerifyCode() {
        String mobile = this.mobileEt.getText().toString();
        ApiParams param = new ApiParams();
        param.addParam("mobile", mobile);
        showSimpleProgress();
        BaseApiCommand.createCommand("user.sendMobileCode/", true, param).execute(getAppContext(), new BaseApiCommand.Callback() {
            public void onNetworkDone(String apiName, String responseData) {
                ForgetPassFragment.this.hideProgress();
                ViewUtil.showToast(ForgetPassFragment.this.getAppContext(), "验证码已发送", false);
            }

            public void onNetworkFail(String apiName, ApiError error) {
                ForgetPassFragment.this.hideProgress();
                ViewUtil.showToast(ForgetPassFragment.this.getAppContext(), "发送失败，请重试", false);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
     candidates:
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
    /* access modifiers changed from: private */
    public boolean checkMobile() {
        String mobile = this.mobileEt.getText().toString();
        if (mobile != null && mobile.length() == 11) {
            return true;
        }
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.FORGETPASSWORD_SENDCODE_RESULT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, "mobile number not 11 bits!").end();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
     candidates:
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
    /* access modifiers changed from: private */
    public boolean checkAllInputs() {
        String tip = null;
        if (this.isForgetType) {
            if (!checkMobile()) {
                tip = "请输入手机号";
            } else if (TextUtils.isEmpty(((TextView) getView().findViewById(R.id.et_verifyCode)).getText().toString())) {
                tip = "请获取验证码";
            }
        } else if (this.newPwdEt.getText().toString().length() <= 0) {
            tip = "请输入新密码";
        } else if (TextUtils.isEmpty(((TextView) getView().findViewById(R.id.forgetPwdOldPwdEt)).getText().toString())) {
            tip = "请输入旧密码";
        }
        if (tip == null) {
            return true;
        }
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.FORGETPASSWORD_RESETPASSWORD_RESULT).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.REGISTER_RESULT_FAIL_REASON, tip).end();
        ViewUtil.showToast(getActivity(), tip, false);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
     candidates:
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        super.handleMessage(msg, activity, rootView);
        String showMsg = "不可读的信息…";
        if (msg.obj != null && (msg.obj instanceof String)) {
            showMsg = msg.obj.toString();
        }
        switch (msg.what) {
            case 0:
                hideProgress();
                ViewUtil.showToast(getActivity(), showMsg, false);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.FORGETPASSWORD_SENDCODE_RESULT).append(TrackConfig.TrackMobile.Key.FORGETPASSWORD_SENDCODE_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.FORGETPASSWORD_SENDCODE_RESULT_FAIL_REASON, (String) msg.obj).end();
                return;
            case 1:
                hideProgress();
                ViewUtil.showToast(getActivity(), showMsg, false);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.FORGETPASSWORD_RESETPASSWORD_RESULT).append(TrackConfig.TrackMobile.Key.FORGETPASSWORD_RESETPASSWORD_RESULT_STATUS, false).append(TrackConfig.TrackMobile.Key.FORGETPASSWORD_RESETPASSWORD_RESULT_FAIL_REASON, (String) msg.obj).end();
                return;
            case 2:
            default:
                return;
            case 3:
                if (!TextUtils.isEmpty(ReferralPromoter.getInstance().ID())) {
                    Log.d("QLM", ReferralPromoter.getInstance().ID());
                    ReferralNetwork.getInstance().savePromoTask(ReferralUtil.TASK_APP, ReferralPromoter.getInstance().ID(), GlobalDataManager.getInstance().getAccountManager().getCurrentUser().getId().substring(1), Util.getDeviceUdid(GlobalDataManager.getInstance().getApplicationContext()), null, null, null);
                }
                hideProgress();
                ViewUtil.showToast(getActivity(), showMsg, false);
                finishFragment(MSG_FORGET_PWD_SUCCEED, null);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.FORGETPASSWORD_RESETPASSWORD_RESULT).append(TrackConfig.TrackMobile.Key.FORGETPASSWORD_RESETPASSWORD_RESULT_STATUS, true).end();
                return;
        }
    }

    public boolean hasGlobalTab() {
        return false;
    }

    /* access modifiers changed from: private */
    public void doReset() {
        ApiParams params = new ApiParams();
        params.addParam("type", "mobile_code");
        params.addParam("identity", ((TextView) getView().findViewById(R.id.forgetPwdMobileEt)).getText().toString());
        params.addParam("password", ((TextView) getView().findViewById(R.id.et_verifyCode)).getText().toString());
        BaseApiCommand.createCommand("user.login/", true, params).execute(getActivity(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                ForgetPassFragment.this.hideProgress();
                ForgetPassFragment.this.sendMessage(0, (error == null || TextUtils.isEmpty(error.getMsg())) ? "请求失败" : error.getMsg());
            }

            public void onNetworkDone(String apiName, String responseData) {
                ForgetPassFragment.this.hideProgress();
                LoginUtil.parseLoginResponse(responseData, null);
                ForgetPassFragment.this.sendMessage(3, "重置密码成功");
            }
        });
    }

    /* access modifiers changed from: private */
    public void doUpdate() {
        ApiParams params = new ApiParams();
        UserBean user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (user != null && user.getPhone() != null) {
            params.addParam("old_password", ((TextView) getView().findViewById(R.id.forgetPwdOldPwdEt)).getText().toString());
            params.addParam("new_password", this.newPwdEt.getText().toString());
            params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
            BaseApiCommand.createCommand("user.updatePassword/", false, params).execute(getActivity(), new BaseApiCommand.Callback() {
                public void onNetworkFail(String apiName, ApiError error) {
                    ForgetPassFragment.this.hideProgress();
                    ForgetPassFragment.this.sendMessage(0, (error == null || TextUtils.isEmpty(error.getMsg())) ? "请求失败" : error.getMsg());
                }

                public void onNetworkDone(String apiName, String responseData) {
                    ForgetPassFragment.this.hideProgress();
                    try {
                        String newToken = new JSONObject(responseData).getJSONObject("result").getString("token");
                        GlobalDataManager.getInstance().setLoginUserToken(newToken);
                        Util.saveDataToLocate(ForgetPassFragment.this.getAppContext(), "loginToken", newToken);
                        ForgetPassFragment.this.sendMessage(3, "修改密码成功");
                        BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_NEW_PASSWORD, ForgetPassFragment.this.newPwdEt.getText().toString());
                    } catch (JSONException e) {
                        ForgetPassFragment.this.sendMessage(1, "网络异常");
                    }
                }
            });
        }
    }
}
