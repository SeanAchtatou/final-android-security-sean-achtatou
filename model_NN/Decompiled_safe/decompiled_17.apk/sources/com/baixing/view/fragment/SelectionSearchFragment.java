package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.baixing.activity.BaseFragment;
import com.baixing.adapter.BXAlphabetSortableAdapter;
import com.baixing.adapter.CheckableAdapter;
import com.baixing.adapter.CommonItemAdapter;
import com.quanleimu.activity.R;
import java.util.List;

public class SelectionSearchFragment extends BaseFragment implements View.OnClickListener {
    private static final int MSG_DOFILTER = 1;
    public static final int MSG_SELECTIONVIEW_BACK = 17;
    /* access modifiers changed from: private */
    public BXAlphabetSortableAdapter adapter;
    private Button btnCancel;
    /* access modifiers changed from: private */
    public EditText etSearch;
    private ListView lvSearchResult;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<? extends Object> selections = (List) getArguments().getSerializable("selections");
        if (getArguments().getBoolean("hasNextLevel", false)) {
            this.adapter = new CommonItemAdapter(getActivity(), selections, 10, false);
        } else {
            this.adapter = new CheckableAdapter(getActivity(), selections, 10, false);
        }
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate((int) R.layout.selectionsearch, (ViewGroup) null);
        this.btnCancel = (Button) v.findViewById(R.id.btnCancel);
        this.btnCancel.setText("取消");
        this.etSearch = (EditText) v.findViewById(R.id.etSearch);
        this.etSearch.setFocusableInTouchMode(true);
        this.etSearch.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SelectionSearchFragment.this.sendMessage(1, null);
            }
        });
        this.lvSearchResult = (ListView) v.findViewById(R.id.lvSearchHistory);
        this.lvSearchResult.setDivider(null);
        this.lvSearchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (!(SelectionSearchFragment.this.adapter.getItem(arg2) instanceof BXAlphabetSortableAdapter.BXHeader)) {
                    SelectionSearchFragment.this.finishFragment(17, ((BXAlphabetSortableAdapter.BXPinyinSortItem) SelectionSearchFragment.this.adapter.getItem(arg2)).obj);
                }
            }
        });
        this.lvSearchResult.setAdapter((ListAdapter) this.adapter);
        this.btnCancel.setOnClickListener(this);
        return v;
    }

    public void onPause() {
        ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.etSearch.getWindowToken(), 0);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.etSearch.postDelayed(new Runnable() {
            public void run() {
                SelectionSearchFragment.this.etSearch.requestFocus();
                ((InputMethodManager) SelectionSearchFragment.this.getActivity().getSystemService("input_method")).showSoftInput(SelectionSearchFragment.this.etSearch, 2);
            }
        }, 100);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = false;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel /*2131165551*/:
                finishFragment();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 1:
                this.adapter.doFilter(this.etSearch.getText().toString());
                return;
            default:
                return;
        }
    }
}
