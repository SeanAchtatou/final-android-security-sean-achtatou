package com.baixing.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.PostGoodsBean;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.TextUtil;
import com.baixing.util.post.ImageUploader;
import com.baixing.util.post.PostCommonValues;
import com.baixing.util.post.PostUtil;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class EditAdFragment extends PostGoodsFragment implements Observer {
    private Ad goodsDetail;

    public void onCreate(Bundle savedInstanceState) {
        this.editMode = true;
        super.onCreate(savedInstanceState);
        this.goodsDetail = (Ad) getArguments().getSerializable("goodsDetail");
        GlobalDataManager.getInstance().setAddress(this.goodsDetail.getValueByKey(PostCommonValues.STRING_DETAIL_POSITION));
        GlobalDataManager.getInstance().setPhoneNumber(this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CONTACT));
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
    }

    public void onDestroy() {
        super.onDestroy();
        BxMessageCenter.defaultMessageCenter().removeObserver(this);
    }

    public void onResume() {
        this.pv = TrackConfig.TrackMobile.PV.EDITPOST;
        Tracker.getInstance().pv(this.pv).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).append(TrackConfig.TrackMobile.Key.ADID, this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).end();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public String getCityEnglishName() {
        if (this.goodsDetail == null || this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CITYENGLISHNAME).length() <= 0) {
            return super.getCityEnglishName();
        }
        return this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CITYENGLISHNAME);
    }

    private void loadIamgeUrl(ArrayList<String> smalls, ArrayList<String> bigs) {
        String[] bList = this.goodsDetail.getImageList().getBigArray();
        String[] sList = this.goodsDetail.getImageList().getSquareArray();
        if (sList != null && sList.length > 0) {
            for (int i = 0; i < sList.length; i++) {
                smalls.add(sList[i]);
                bigs.add(bList[i]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void mergeParams(HashMap<String, String> list) {
        if (this.goodsDetail != null) {
            list.put("adId", this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
        }
        super.mergeParams(list);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        if (this.goodsDetail != null) {
            title.m_leftActionHint = "返回";
        }
        super.initTitle(title);
    }

    /* access modifiers changed from: protected */
    public String getAdContact() {
        return this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CONTACT);
    }

    /* access modifiers changed from: protected */
    public void buildPostLayout(HashMap<String, PostGoodsBean> pl) {
        super.buildPostLayout(pl);
        String description = this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_DESCRIPTION);
        String title = this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE);
        if (!(description == null || title == null || (description.startsWith(title) && (description.length() > 25 || description.length() == title.length())))) {
            ((EditText) this.layout_txt.findViewById(R.id.description_input)).removeTextChangedListener(this.textWatcher);
        }
        editPostUI();
        ((TextView) getView().findViewById(R.id.tv_title_post)).setText(title);
        super.updateImageInfo(this.layout_txt);
    }

    private void editPostUI() {
        String displayValue;
        if (this.goodsDetail != null) {
            for (int i = 0; i < this.layout_txt.getChildCount(); i++) {
                View v = this.layout_txt.getChildAt(i);
                PostGoodsBean bean = (PostGoodsBean) v.getTag(PostCommonValues.HASH_POST_BEAN);
                if (!(bean == null || (displayValue = this.goodsDetail.getValueByKey(bean.getName())) == null || displayValue.equals(""))) {
                    String detailValue = PostUtil.getDetailValue(bean, this.goodsDetail, bean.getName());
                    if (!TextUtils.isEmpty(bean.getUnit()) && displayValue.endsWith(bean.getUnit())) {
                        displayValue = displayValue.substring(0, displayValue.lastIndexOf(bean.getUnit()));
                    }
                    View control = (View) v.getTag(PostCommonValues.HASH_CONTROL);
                    if (control instanceof CheckBox) {
                        if (displayValue.contains(((CheckBox) control).getText())) {
                            ((CheckBox) control).setChecked(true);
                        } else {
                            ((CheckBox) control).setChecked(false);
                        }
                    } else if (control instanceof TextView) {
                        ((TextView) control).setText(displayValue);
                    }
                    this.params.put(bean.getName(), displayValue, detailValue);
                }
            }
            if (this.goodsDetail.getImageList() != null) {
                ArrayList<String> listUrl = new ArrayList<>();
                if (this.photoList.size() == 0) {
                    ArrayList<String> list = new ArrayList<>();
                    loadIamgeUrl(listUrl, list);
                    if (list.size() > 0) {
                        this.photoList.addAll(listUrl);
                        for (int i2 = 0; i2 < listUrl.size(); i2++) {
                            ImageUploader.getInstance().addDownloadImage((String) listUrl.get(i2), (String) list.get(i2), null);
                        }
                    }
                }
                String big = this.goodsDetail.getImageList().getBig();
                if (big != null && big.length() > 0) {
                    String[] cbig = TextUtil.filterString(big, new char[]{'\\', '\"'}).split(",");
                    for (String add : cbig) {
                        this.bmpUrls.add(add);
                    }
                }
            }
            String btnAddr = ((Button) getView().findViewById(R.id.btn_address)).getText().toString();
            if (btnAddr == null || btnAddr.length() == 0) {
                ((Button) getView().findViewById(R.id.btn_address)).setText(this.goodsDetail.getValueByKey(PostCommonValues.STRING_DETAIL_POSITION));
                setPhoneAndAddrLeftIcon();
            }
            String btnCall = ((Button) getView().findViewById(R.id.btn_contact)).getText().toString();
            if (btnCall == null || btnCall.length() == 0) {
                ((Button) getView().findViewById(R.id.btn_contact)).setText(this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CONTACT));
                setPhoneAndAddrLeftIcon();
            }
        }
    }

    public void update(Observable observable, Object data) {
        if ((data instanceof BxMessageCenter.IBxNotification) && IBxNotificationNames.NOTIFICATION_LOGOUT.equals(((BxMessageCenter.IBxNotification) data).getName())) {
            getActivity().sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_EDIT_LOGOUT));
            finishFragment();
        }
    }
}
