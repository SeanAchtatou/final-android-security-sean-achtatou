package com.baixing.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.adapter.CommonItemAdapter;
import com.baixing.data.GlobalDataManager;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.List;

public class KeywordSelectFragment extends BaseFragment {
    /* access modifiers changed from: private */
    public EditText etSearch;
    /* access modifiers changed from: private */
    public List<String> listRemark = new ArrayList();
    /* access modifiers changed from: private */
    public ListView lvSearchHistory;
    /* access modifiers changed from: private */
    public TextView tvClear;

    public KeywordSelectFragment() {
        this.defaultEnterAnim = R.anim.zoom_enter;
        this.defaultExitAnim = R.anim.zoom_exit;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.listRemark = GlobalDataManager.getInstance().getListRemark();
    }

    /* access modifiers changed from: protected */
    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_titleControls = LayoutInflater.from(getActivity()).inflate((int) R.layout.title_search, (ViewGroup) null);
        this.etSearch = (EditText) title.m_titleControls.findViewById(R.id.etSearch);
        title.m_leftActionHint = "返回";
        title.m_rightActionHint = "搜索";
        this.etSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66 || event.getAction() != 1) {
                    return false;
                }
                String searchContent = KeywordSelectFragment.this.etSearch.getText().toString().trim();
                if (searchContent.equals("")) {
                    ViewUtil.showToast(KeywordSelectFragment.this.getActivity(), "搜索内容不能为空", false);
                    return true;
                }
                KeywordSelectFragment.this.finishFragment(KeywordSelectFragment.this.fragmentRequestCode, searchContent);
                return true;
            }
        });
        this.etSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (KeywordSelectFragment.this.listRemark != null && KeywordSelectFragment.this.listRemark.size() != 0) {
                    CommonItemAdapter adapter = new CommonItemAdapter(KeywordSelectFragment.this.getActivity(), KeywordSelectFragment.this.listRemark, 536870911, false);
                    adapter.setHasArrow(false);
                    KeywordSelectFragment.this.lvSearchHistory.setAdapter((ListAdapter) adapter);
                }
            }
        });
    }

    public boolean handleBack() {
        finishFragment(this.fragmentRequestCode, null);
        return true;
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootV = (ViewGroup) inflater.inflate((int) R.layout.keyword_selector, (ViewGroup) null);
        this.lvSearchHistory = (ListView) rootV.findViewById(R.id.lvSearchHistory);
        this.tvClear = (TextView) inflater.inflate((int) R.layout.button_clear, (ViewGroup) null);
        this.tvClear.setHeight(getResources().getDimensionPixelSize(R.dimen.tab_height));
        this.lvSearchHistory.addFooterView(this.tvClear);
        this.lvSearchHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (arg2 <= KeywordSelectFragment.this.listRemark.size() - 1) {
                    String searchContent = (String) KeywordSelectFragment.this.listRemark.get(arg2);
                    KeywordSelectFragment.this.etSearch.setText(searchContent);
                    KeywordSelectFragment.this.etSearch.setSelection(searchContent.length(), searchContent.length());
                    KeywordSelectFragment.this.finishFragment(KeywordSelectFragment.this.fragmentRequestCode, searchContent);
                    return;
                }
                KeywordSelectFragment.this.listRemark.clear();
                GlobalDataManager.getInstance().updateRemark((String[]) null);
                KeywordSelectFragment.this.lvSearchHistory.setVisibility(8);
                KeywordSelectFragment.this.tvClear.setVisibility(8);
            }
        });
        this.tvClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeywordSelectFragment.this.listRemark.clear();
                GlobalDataManager.getInstance().updateRemark((String[]) KeywordSelectFragment.this.listRemark.toArray(new String[KeywordSelectFragment.this.listRemark.size()]));
                KeywordSelectFragment.this.lvSearchHistory.setVisibility(8);
                v.setVisibility(8);
                Util.saveDataToLocate(KeywordSelectFragment.this.getActivity(), "listRemark", KeywordSelectFragment.this.listRemark);
                ViewUtil.postShortToastMessage(KeywordSelectFragment.this.getView(), (int) R.string.tip_history_cleared, 0);
            }
        });
        this.listRemark = GlobalDataManager.getInstance().getListRemark();
        return rootV;
    }

    private void showSearchHistory() {
        if (this.listRemark == null || this.listRemark.size() == 0) {
            this.tvClear.setVisibility(8);
            return;
        }
        this.lvSearchHistory.setVisibility(0);
        this.tvClear.setVisibility(0);
        CommonItemAdapter adapter = new CommonItemAdapter(getActivity(), this.listRemark, 536870911, false);
        adapter.setHasArrow(false);
        this.lvSearchHistory.setAdapter((ListAdapter) adapter);
    }

    public void onStackTop(boolean isBack) {
        Log.d("ooo", "keywordselector->onstacktop");
        showSearchHistory();
        this.etSearch.postDelayed(new Runnable() {
            public void run() {
                if (KeywordSelectFragment.this.etSearch != null) {
                    KeywordSelectFragment.this.etSearch.requestFocus();
                    ((InputMethodManager) KeywordSelectFragment.this.etSearch.getContext().getSystemService("input_method")).showSoftInput(KeywordSelectFragment.this.etSearch, 1);
                }
            }
        }, 100);
    }

    public void onPause() {
        ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.etSearch.getWindowToken(), 0);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.SEARCH).end();
        Log.d("ooo", "keywordselector->onresume");
        this.etSearch.postDelayed(new Runnable() {
            public void run() {
                KeywordSelectFragment.this.etSearch.requestFocus();
            }
        }, 300);
    }

    private void doSearch() {
        Log.d("ooo", "keywordselector->dosearch");
        String searchContent = this.etSearch.getText().toString().trim();
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.HEADERSEARCHRESULT).append(TrackConfig.TrackMobile.Key.SEARCHKEYWORD, searchContent).end();
        if (searchContent.equals("")) {
            ViewUtil.showToast(getActivity(), "搜索内容不能为空", false);
            return;
        }
        addToListRemark(searchContent);
        finishFragment(this.fragmentRequestCode, searchContent);
    }

    public void handleRightAction() {
        doSearch();
    }

    private void addToListRemark(String searchContent) {
        if (this.listRemark == null || this.listRemark.size() == 0) {
            this.listRemark = new ArrayList();
        }
        this.listRemark.remove(searchContent);
        this.listRemark.add(0, searchContent);
        GlobalDataManager.getInstance().updateRemark(this.listRemark);
        Util.saveDataToLocate(getActivity(), "listRemark", this.listRemark);
    }
}
