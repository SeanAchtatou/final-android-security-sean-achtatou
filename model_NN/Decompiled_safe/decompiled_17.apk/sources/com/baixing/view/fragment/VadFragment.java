package com.baixing.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.adapter.VadImageAdapter;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.AdList;
import com.baixing.entity.UserBean;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.util.ErrorHandler;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import com.baixing.util.VadListLoader;
import com.baixing.util.ViewUtil;
import com.baixing.view.AdViewHistory;
import com.baixing.view.vad.VadLogger;
import com.baixing.view.vad.VadPageController;
import com.baixing.widget.ContextMenuItem;
import com.baixing.widget.FavAndReportDialog;
import com.lambergar.verticalviewpager.VerticalViewPager;
import com.quanleimu.activity.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.mm.sdk.platformtools.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.json.JSONException;
import org.json.JSONObject;

public class VadFragment extends BaseFragment implements View.OnTouchListener, View.OnClickListener, AdapterView.OnItemSelectedListener, VadListLoader.HasMoreListener, VadListLoader.Callback, VadPageController.ActionCallback, BaseApiCommand.Callback, Observer {
    public static final int MSG_ADINVERIFY_DELETED = 65536;
    private static final int MSG_DELETE = 7;
    private static final int MSG_FINISH_FRAGMENT = 10;
    private static final int MSG_LOAD_AD_EVENT = 8;
    private static final int MSG_LOGIN_TO_PROSECUTE = 11;
    public static final int MSG_MYPOST_DELETED = 65537;
    private static final int MSG_NETWORK_FAIL = 9;
    private static final int MSG_REFRESH = 5;
    private static final int MSG_REFRESH_CONFIRM = 4;
    private static final int MSG_UPDATE = 6;
    public Ad detail = new Ad();
    private boolean galleryReturned = true;
    private boolean keepSilent = false;
    private IListHolder mHolder = null;
    private VadListLoader mListLoader;
    private VadPageController pageController;
    List<View> pages = new ArrayList();

    public interface IListHolder {
        boolean onResult(int i, VadListLoader vadListLoader);

        void startFecthingMore();
    }

    enum REQUEST_TYPE {
        REQUEST_TYPE_REFRESH(5, "ad.freeRefresh/"),
        REQUEST_TYPE_UPDATE(6, "ad_list"),
        REQUEST_TYPE_DELETE(7, "ad.delete/");
        
        public String apiName;
        public int reqCode;

        private REQUEST_TYPE(int requestCode, String apiName2) {
            this.reqCode = requestCode;
            this.apiName = apiName2;
        }
    }

    public void onDestroy() {
        this.keepSilent = true;
        BxMessageCenter.defaultMessageCenter().removeObserver(this);
        super.onDestroy();
    }

    public boolean handleBack() {
        this.keepSilent = false;
        if (getView().findViewById(R.id.guide_swipe).getVisibility() != 0) {
            return false;
        }
        getView().findViewById(R.id.guide_swipe).setVisibility(8);
        return true;
    }

    public void onPause() {
        this.keepSilent = true;
        super.onPause();
        this.pages.clear();
    }

    public void onResume() {
        VadLogger.trackPageView(this.detail, getAppContext());
        this.keepSilent = false;
        super.onResume();
        getView().findViewById(R.id.svDetail).post(new Runnable() {
            public void run() {
                VadFragment.this.getView().findViewById(R.id.svDetail).requestLayout();
            }
        });
    }

    private boolean isMyAd() {
        if (this.detail == null) {
            return false;
        }
        return GlobalDataManager.getInstance().isMyAd(this.detail);
    }

    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case 0:
            case 2:
                if (!(getView() == null || getView().findViewById(R.id.svDetail) == null)) {
                    ((VerticalViewPager) getView().findViewById(R.id.svDetail)).requestDisallowInterceptTouchEvent(true);
                    break;
                }
            case 1:
            case 4:
                if (!(getView() == null || getView().findViewById(R.id.svDetail) == null)) {
                    ((VerticalViewPager) getView().findViewById(R.id.svDetail)).requestDisallowInterceptTouchEvent(false);
                    break;
                }
        }
        return this.keepSilent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mListLoader = (VadListLoader) getArguments().getSerializable("loader");
        int index = getArguments().getInt("index", 0);
        if (this.mListLoader != null && this.mListLoader.getGoodsList() != null && this.mListLoader.getGoodsList().getData() != null && this.mListLoader.getGoodsList().getData().size() > index) {
            this.detail = this.mListLoader.getGoodsList().getData().get(index);
            if (savedInstanceState != null) {
                this.mListLoader.setHasMoreListener(this);
            }
            BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
        }
    }

    public void onStackTop(boolean isBack) {
        if (isVadPreview() && this.detail != null && this.detail.isValidMessage()) {
            new SharingFragment(this.detail, "myViewad").show(getFragmentManager(), (String) null);
            getArguments().putBoolean("autoShared", true);
        }
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = null;
        if (!(this.detail == null || this.mListLoader == null)) {
            if ((this.mListLoader.getGoodsList() == null || this.mListLoader.getGoodsList().getData() == null || this.mListLoader.getGoodsList().getData().size() == 0) && getActivity() != null) {
                getActivity().sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_MYAD_LOGOUT));
            }
            int originalSelect = getArguments().getInt("index", 0);
            this.keepSilent = false;
            v = inflater.inflate((int) R.layout.gooddetailview, (ViewGroup) null);
            this.pageController = new VadPageController(v, this.detail, this, originalSelect);
            this.mListLoader.setSelection(originalSelect);
            this.mListLoader.setCallback(this);
            if (TextUtils.isEmpty((String) Util.loadDataFromLocate(getAppContext(), "firstInVad", String.class))) {
                v.findViewById(R.id.guide_swipe).setVisibility(0);
                v.findViewById(R.id.guide_swipe).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        VadFragment.this.getView().findViewById(R.id.guide_swipe).setVisibility(8);
                    }
                });
                Util.saveDataToLocate(getAppContext(), "firstInVad", "true");
            } else {
                v.findViewById(R.id.guide_swipe).setVisibility(8);
            }
        }
        return v;
    }

    private void notifyPageDataChange(boolean hasMore) {
        if (!this.keepSilent) {
            this.pageController.resetLoadingPage(hasMore);
        }
    }

    private void updateContactBar(View rootView, boolean forceHide) {
        int i;
        AdViewHistory.getInstance().markRead(this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
        if (!this.detail.isValidMessage() && !forceHide) {
            String tips = this.detail.getValueByKey("tips");
            if (tips == null || tips.equals("")) {
                tips = "该信息不符合《百姓网公约》";
            }
            AlertDialog dialog = new AlertDialog.Builder(getActivity()).setTitle(R.string.dialog_title_info).setMessage(tips).setNegativeButton((int) R.string.delete, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    VadFragment.this.postDelete(true, new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            boolean unused = VadFragment.this.finishFragment();
                        }
                    });
                }
            }).setPositiveButton((int) R.string.appeal, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    VadLogger.trackMofifyEvent(VadFragment.this.detail, TrackConfig.TrackMobile.BxEvent.MYVIEWAD_APPEAL);
                    Bundle bundle = VadFragment.this.createArguments("申诉", null);
                    bundle.putInt("type", 1);
                    bundle.putString("adId", VadFragment.this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
                    VadFragment.this.pushAndFinish(new FeedbackFragment(), bundle);
                }
            }).create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    boolean unused = VadFragment.this.finishFragment();
                }
            });
            dialog.show();
        }
        LinearLayout rl_phone = (LinearLayout) rootView.findViewById(R.id.phonelayout);
        if (forceHide) {
            rl_phone.setVisibility(8);
        } else if (isMyAd() || !this.detail.isValidMessage()) {
            rootView.findViewById(R.id.phone_parent).setVisibility(8);
            rootView.findViewById(R.id.vad_tool_bar).setVisibility(0);
            rootView.findViewById(R.id.vad_btn_edit).setOnClickListener(this);
            rootView.findViewById(R.id.vad_btn_refresh).setOnClickListener(this);
            rootView.findViewById(R.id.vad_btn_delete).setOnClickListener(this);
            rootView.findViewById(R.id.vad_btn_forward).setOnClickListener(this);
            if (!this.detail.isValidMessage()) {
                rootView.findViewById(R.id.vad_btn_edit).setVisibility(8);
                rootView.findViewById(R.id.vad_btn_refresh).setVisibility(8);
            }
        } else {
            rootView.findViewById(R.id.phone_parent).setVisibility(0);
            rootView.findViewById(R.id.vad_tool_bar).setVisibility(8);
            String contactS = this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CONTACT);
            String mobileArea = this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_MOBILE_AREA);
            ViewGroup btnBuzz = (ViewGroup) rootView.findViewById(R.id.vad_buzz_btn);
            ImageView btnImg = (ImageView) btnBuzz.findViewById(R.id.vad_buzz_btn_img);
            boolean buzzEnable = Util.isValidMobile(contactS);
            btnBuzz.setEnabled(buzzEnable);
            if (!buzzEnable) {
                btnImg.setImageBitmap(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.icon_sms_disable));
            }
            rootView.findViewById(R.id.vad_buzz_btn).setOnClickListener(this);
            rl_phone.setVisibility(0);
            boolean callEnable = TextUtil.isNumberSequence(contactS);
            rootView.findViewById(R.id.vad_call_btn).setEnabled(callEnable);
            rootView.findViewById(R.id.vad_call_btn).setOnClickListener(callEnable ? this : null);
            rootView.findViewById(R.id.icon_call).setBackgroundResource(callEnable ? R.drawable.icon_call : R.drawable.icon_call_disable);
            TextView txtCall = (TextView) rootView.findViewById(R.id.txt_call);
            String text = "立即拨打" + contactS;
            if ((mobileArea == null || mobileArea.length() <= 0 || GlobalDataManager.getInstance().getCityName().equals(mobileArea)) && (mobileArea == null || "".equals(mobileArea.trim()))) {
                ((ContextMenuItem) rootView.findViewById(R.id.vad_call_nonmobile)).updateOptionList("", getResources().getStringArray(R.array.item_call_nonmobile), new int[]{R.id.vad_buzz_btn, R.id.vad_buzz_btn_img});
            }
            if (!callEnable) {
                text = "无联系方式";
            }
            txtCall.setText(text);
            Resources resources = getResources();
            if (callEnable) {
                i = R.color.vad_call_btn_text;
            } else {
                i = R.color.common_button_disable;
            }
            txtCall.setTextColor(resources.getColor(i));
        }
    }

    private boolean handleRightBtnIfInVerify() {
        if (this.detail.getValueByKey("status").equals("0")) {
            return false;
        }
        showSimpleProgress();
        executeModify(REQUEST_TYPE.REQUEST_TYPE_DELETE, 0);
        return true;
    }

    private boolean isVadPreview() {
        return getArguments() != null && getArguments().getBoolean("isVadPreview", false) && !getArguments().getBoolean("autoShared", false);
    }

    public void handleRightAction() {
        finishFragment();
    }

    private void handleStoreBtnClicked() {
        if (!handleRightBtnIfInVerify()) {
            View parent = getView().findViewById(R.id.vad_title_fav_parent);
            int[] location = {0, 0};
            parent.getLocationInWindow(location);
            int width = parent.getWidth();
            int height = parent.getHeight();
            FavAndReportDialog menu = new FavAndReportDialog((BaseActivity) getActivity(), this.detail, this.handler);
            menu.show(((location[0] + width) - menu.getWindow().getAttributes().width) - 5, height - 1);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.vad_buzz_btn /*2131165348*/:
                startContact(true);
                return;
            case R.id.vad_call_btn /*2131165350*/:
                VadLogger.trackContactEvent(TrackConfig.TrackMobile.BxEvent.VIEWAD_MOBILECALLCLICK, this.detail);
                String mobileArea = this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_MOBILE_AREA);
                if (mobileArea == null || "".equals(mobileArea.trim())) {
                    getView().findViewById(R.id.vad_call_nonmobile).performLongClick();
                    return;
                } else {
                    startContact(false);
                    return;
                }
            case R.id.vad_title_fav_parent /*2131165612*/:
                handleStoreBtnClicked();
                return;
            case R.id.vad_btn_edit /*2131165616*/:
                Bundle args = createArguments(null, null);
                args.putSerializable("goodsDetail", this.detail);
                args.putString(PostGoodsFragment.KEY_INIT_CATEGORY, this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME));
                pushFragment(new EditAdFragment(), args);
                VadLogger.trackMofifyEvent(this.detail, TrackConfig.TrackMobile.BxEvent.MYVIEWAD_EDIT);
                return;
            case R.id.vad_btn_refresh /*2131165618*/:
                showSimpleProgress();
                executeModify(REQUEST_TYPE.REQUEST_TYPE_REFRESH, 0);
                VadLogger.trackMofifyEvent(this.detail, TrackConfig.TrackMobile.BxEvent.MYVIEWAD_REFRESH);
                return;
            case R.id.vad_btn_delete /*2131165619*/:
                postDelete(true, null);
                return;
            case R.id.vad_btn_forward /*2131165620*/:
                VadLogger.trackMofifyEvent(this.detail, TrackConfig.TrackMobile.BxEvent.MYVIEWAD_SHARE);
                new SharingFragment(this.detail, "myViewad").show(getFragmentManager(), (String) null);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void postDelete(boolean cancelable, DialogInterface.OnCancelListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("提醒").setMessage("是否确定删除").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VadFragment.this.showSimpleProgress();
                VadFragment.this.executeModify(REQUEST_TYPE.REQUEST_TYPE_DELETE, 0);
                VadLogger.trackMofifyEvent(VadFragment.this.detail, TrackConfig.TrackMobile.BxEvent.MYVIEWAD_DELETE);
            }
        });
        if (cancelable) {
            builder = builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        AlertDialog dialog = builder.create();
        dialog.show();
        if (listener != null) {
            dialog.setOnCancelListener(listener);
        }
        dialog.show();
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int requestCode, Object result) {
        if (-268435440 == requestCode) {
            finishFragment(requestCode, result);
        } else if (BigGalleryFragment.MSG_GALLERY_BACK == requestCode) {
            this.galleryReturned = true;
        } else if (-61664 == requestCode) {
            if (result != null) {
                Ad newDetail = (Ad) result;
                this.detail = newDetail;
                try {
                    List<Ad> dataList = this.mListLoader.getGoodsList().getData();
                    int i = 0;
                    while (true) {
                        if (i >= dataList.size()) {
                            break;
                        } else if (dataList.get(i).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(newDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                            dataList.add(i, newDetail);
                            dataList.remove(i + 1);
                            break;
                        } else {
                            i++;
                        }
                    }
                } catch (Throwable t) {
                    Log.d("QLM", "error when update ad in adlist. " + t.getMessage());
                } finally {
                    notifyPageDataChange(false);
                }
            }
        } else if (11 == requestCode) {
            new AsyncTask<Ad, Integer, Boolean>() {
                /* access modifiers changed from: protected */
                public Boolean doInBackground(Ad... ads) {
                    ApiParams params = new ApiParams();
                    params.addParam("adId", ads[0].getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
                    params.addParam("mobile", GlobalDataManager.getInstance().getAccountManager().getCurrentUser().getPhone());
                    try {
                        JSONObject json = new JSONObject(BaseApiCommand.createCommand("ad_reported", true, params).executeSync(VadFragment.this.getAppContext()));
                        if (json.getJSONObject("error").getInt("code") == 0) {
                            return Boolean.valueOf(json.getBoolean("reported"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return false;
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Boolean reported) {
                    if (reported.booleanValue()) {
                        ViewUtil.showToast(VadFragment.this.getAppContext(), "您已举报过该信息", false);
                    } else {
                        VadFragment.this.showProsecute();
                    }
                }
            }.execute(this.detail);
        }
    }

    private void popRefresh(String message) {
        new AlertDialog.Builder(getActivity()).setTitle("提醒").setMessage(message).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VadFragment.this.showSimpleProgress();
                VadFragment.this.executeModify(REQUEST_TYPE.REQUEST_TYPE_REFRESH, 1);
                dialog.dismiss();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 5:
                hideProgress();
                ViewUtil.showToast(getActivity(), "刷新成功", false);
                return;
            case 6:
                hideProgress();
                List<Ad> goodsDetails = JsonUtil.getGoodsListFromJson((String) msg.obj).getData();
                if (goodsDetails != null && goodsDetails.size() > 0) {
                    int i = 0;
                    while (true) {
                        if (i < goodsDetails.size()) {
                            if (goodsDetails.get(i).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                                this.detail = goodsDetails.get(i);
                            } else {
                                i++;
                            }
                        }
                    }
                    List<Ad> listMyPost = GlobalDataManager.getInstance().getListMyPost();
                    if (listMyPost != null) {
                        for (int i2 = 0; i2 < listMyPost.size(); i2++) {
                            if (listMyPost.get(i2).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                                listMyPost.set(i2, this.detail);
                                return;
                            }
                        }
                        return;
                    }
                    return;
                }
                return;
            case 7:
                hideProgress();
                if (this.detail.getValueByKey("status").equals("0")) {
                    List<Ad> listMyPost2 = GlobalDataManager.getInstance().getListMyPost();
                    if (listMyPost2 != null) {
                        int i3 = 0;
                        while (true) {
                            if (i3 < listMyPost2.size()) {
                                if (listMyPost2.get(i3).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID).equals(this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID))) {
                                    listMyPost2.remove(i3);
                                } else {
                                    i3++;
                                }
                            }
                        }
                    }
                    finishFragment(MSG_MYPOST_DELETED, null);
                } else {
                    finishFragment(65536, this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
                }
                if (getArguments() == null || !getArguments().getBoolean("isVadPreview", false)) {
                    ViewUtil.showCommentsPromptDialog((BaseActivity) getActivity());
                    return;
                }
                return;
            case 8:
                Pair<Integer, Object> data = (Pair) msg.obj;
                processEvent(((Integer) data.first).intValue(), data.second);
                return;
            case 9:
                ViewUtil.showToast(getActivity(), (String) msg.obj, true);
                return;
            case 10:
                finishFragment();
                return;
            case FavAndReportDialog.MSG_PROSECUTE /*305459440*/:
                UserBean user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
                Bundle arg = createArguments(null, null);
                arg.putInt(LoginFragment.KEY_RETURN_CODE, 11);
                if (user == null || TextUtils.isEmpty(user.getPhone())) {
                    pushFragment(new LoginFragment(), arg);
                    return;
                } else {
                    showProsecute();
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void showProsecute() {
        Bundle bundle = new Bundle();
        bundle.putInt("type", 0);
        bundle.putString("adId", this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
        bundle.putString(BaseFragment.ARG_COMMON_TITLE, "举报");
        pushFragment(new FeedbackFragment(), bundle);
    }

    /* access modifiers changed from: private */
    public void executeModify(REQUEST_TYPE request, int pay) {
        ApiParams params = new ApiParams();
        params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        switch (request) {
            case REQUEST_TYPE_DELETE:
                params.addParam(LocaleUtil.INDONESIAN, this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
                break;
            case REQUEST_TYPE_REFRESH:
                params.addParam(LocaleUtil.INDONESIAN, this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
                break;
            case REQUEST_TYPE_UPDATE:
                params.addParam("query", "id:" + this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
                break;
        }
        BaseApiCommand.createCommand(request.apiName, false, params).execute(getActivity(), this);
    }

    private void updateTitle(BaseFragment.TitleDef title) {
        if (this.detail != null) {
            if (!isMyAd()) {
                ((TextView) getTitleDef().m_titleControls.findViewById(R.id.vad_viewed_time)).setText(this.detail.getValueByKey("count") + "次查看");
            } else {
                title.m_title = this.detail.getValueByKey("count") + "次查看";
                View root = getView();
                if (root != null) {
                    ((TextView) root.findViewById(R.id.tvTitle)).setText(title.m_title);
                }
            }
            Log.d("detail", this.detail.getImageList().toString());
        }
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_leftActionHint = isVadPreview() ? "" : "返回";
        title.m_rightActionHint = isVadPreview() ? "完成" : "";
        if (!(this.mListLoader == null || this.mListLoader.getGoodsList() == null || this.mListLoader.getGoodsList().getData() == null)) {
            title.m_title = (this.mListLoader.getSelection() + 1) + CookieSpec.PATH_DELIM + this.mListLoader.getGoodsList().getData().size();
        }
        title.m_visible = true;
        title.m_leftActionImage = (getArguments() == null || !"close".equalsIgnoreCase(getArguments().getString(BaseFragment.ARG_COMMON_BACK_HINT))) ? R.drawable.icon_back : R.drawable.icon_close;
        if (!isMyAd()) {
            title.m_titleControls = LayoutInflater.from(getActivity()).inflate((int) R.layout.vad_title, (ViewGroup) null);
            title.m_titleControls.findViewById(R.id.vad_title_fav_parent).setOnClickListener(this);
        }
        updateTitle(title);
        updateTitleBar(title);
    }

    private void updateTitleBar(BaseFragment.TitleDef title) {
        if (isMyAd() || !this.detail.isValidMessage()) {
            if (title.m_titleControls != null) {
                title.m_titleControls.findViewById(R.id.vad_title_fav_parent).setVisibility(4);
            }
        } else if (title.m_titleControls != null) {
            title.m_titleControls.findViewById(R.id.vad_title_fav_parent).setVisibility(0);
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getAdapter() instanceof VadImageAdapter) {
            List<String> listUrl = ((VadImageAdapter) parent.getAdapter()).getImages();
            ArrayList<String> urls = new ArrayList<>();
            urls.add(listUrl.get(position));
            int index = 0;
            while (true) {
                if (index + position < listUrl.size() || position - index >= 0) {
                    if (index + position < listUrl.size()) {
                        urls.add(listUrl.get(index + position));
                    }
                    if (position - index >= 0) {
                        urls.add(listUrl.get(position - index));
                    }
                    index++;
                } else {
                    GlobalDataManager.getInstance().getImageLoaderMgr().AdjustPriority(urls);
                    return;
                }
            }
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void onHasMoreStatusChanged() {
    }

    public void setListHolder(IListHolder holder) {
        this.mHolder = holder;
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.vad_buzz_btn /*2131165348*/:
                startContact(false);
                return true;
            case R.id.vad_buzz_btn_img /*2131165349*/:
                ((ClipboardManager) getActivity().getSystemService("clipboard")).setText(this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CONTACT));
                ViewUtil.postShortToastMessage(getView(), (int) R.string.tip_clipd_contact, 0);
                return true;
            default:
                return super.onContextItemSelected(menuItem);
        }
    }

    private boolean shouldShowCommentsDialog() {
        String count = (String) Util.loadDataFromLocate(getAppContext(), "contactCount1", String.class);
        if (TextUtils.isEmpty(count) || !count.equals("yes")) {
            return false;
        }
        return true;
    }

    private void increaseContactCount() {
        String count = (String) Util.loadDataFromLocate(getAppContext(), "contactCount1", String.class);
        if (TextUtils.isEmpty(count)) {
            Util.saveDataToLocate(getAppContext(), "contactCount1", "1");
        } else if (!count.equals("no") && !count.equals("yes")) {
            Integer ic = Integer.valueOf(Integer.valueOf(count).intValue() + 1);
            if (ic.intValue() == 3) {
                Util.saveDataToLocate(getAppContext(), "contactCount1", "yes");
            } else {
                Util.saveDataToLocate(getAppContext(), "contactCount1", String.valueOf(ic));
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            increaseContactCount();
            if (shouldShowCommentsDialog()) {
                ViewUtil.showCommentsPromptDialog((BaseActivity) getActivity());
                Util.saveDataToLocate(getAppContext(), "contactCount1", "no");
                return;
            }
            handleStoreBtnClicked();
        }
    }

    private void startContact(boolean sms) {
        String str;
        if (sms) {
            VadLogger.trackContactEvent(TrackConfig.TrackMobile.BxEvent.VIEWAD_SMS, this.detail);
        }
        String contact = this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CONTACT);
        if (contact != null) {
            String str2 = sms ? "android.intent.action.SENDTO" : "android.intent.action.DIAL";
            StringBuilder sb = new StringBuilder();
            if (sms) {
                str = "smsto:";
            } else {
                str = "tel:";
            }
            Intent intent = new Intent(str2, Uri.parse(sb.append(str).append(contact).toString()));
            if (sms) {
                intent.putExtra("sms_body", "你好，我在百姓网看到你发的\"" + this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE) + "\"，");
            }
            List<ResolveInfo> ls = getActivity().getPackageManager().queryIntentActivities(intent, 65536);
            if (ls == null || ls.size() <= 0) {
                ViewUtil.postShortToastMessage(getView(), sms ? R.string.warning_no_sms_app_install : R.string.warning_no_phone_app_install, 0);
            } else {
                startActivityForResult(intent, 100);
            }
        }
    }

    public void showMap() {
        VadLogger.trackShowMapEvent(this.detail);
        if (this.keepSilent) {
            ViewUtil.showToast(getActivity(), "当前无法显示地图", false);
        } else {
            ViewUtil.startMapForAds(getActivity(), this.detail);
        }
    }

    public boolean hasGlobalTab() {
        return false;
    }

    public void onRequestComplete(int respCode, Object data) {
        sendMessage(8, Pair.create(Integer.valueOf(respCode), data));
    }

    private void processEvent(int respCode, Object data) {
        if (this.mHolder != null) {
            if (this.mHolder.onResult(respCode, this.mListLoader)) {
                this.pageController.loadMoreSucced();
            } else {
                onNoMore();
            }
            if (respCode == -10) {
                this.pageController.loadMoreFail();
                return;
            }
            return;
        }
        switch (respCode) {
            case ErrorHandler.ERROR_NETWORK_UNAVAILABLE:
                ErrorHandler.getInstance().handleError(-10, null);
                this.pageController.loadMoreFail();
                return;
            case 0:
                AdList goodsList = JsonUtil.getGoodsListFromJson(this.mListLoader.getLastJson());
                this.mListLoader.setGoodsList(goodsList);
                if (goodsList == null || goodsList.getData().size() == 0) {
                    ErrorHandler.getInstance().handleError(-3, "没有符合的结果，请稍后并重试！");
                }
                this.mListLoader.setHasMore(true);
                notifyPageDataChange(true);
                return;
            case 1:
                AdList goodsList1 = JsonUtil.getGoodsListFromJson(this.mListLoader.getLastJson());
                if (goodsList1 == null || goodsList1.getData().size() == 0) {
                    ErrorHandler.getInstance().handleError(-2, "后面没有啦！");
                    onNoMore();
                    this.mListLoader.setHasMore(false);
                    notifyPageDataChange(false);
                    return;
                }
                List<Ad> listCommonGoods = goodsList1.getData();
                for (int i = 0; i < listCommonGoods.size(); i++) {
                    this.mListLoader.getGoodsList().getData().add(listCommonGoods.get(i));
                }
                this.mListLoader.setHasMore(true);
                notifyPageDataChange(true);
                this.pageController.loadMoreSucced();
                return;
            case 2:
                onNoMore();
                this.mListLoader.setHasMore(false);
                notifyPageDataChange(false);
                return;
            default:
                return;
        }
    }

    private void onNoMore() {
        if (getView() != null) {
            ViewUtil.showToast(getActivity(), "后面没有啦！", false);
        }
    }

    public int totalPages() {
        if (this.mListLoader == null || this.mListLoader.getGoodsList() == null || this.mListLoader.getGoodsList().getData() == null) {
            return 0;
        }
        return this.mListLoader.getGoodsList().getData().size();
    }

    public Ad getAd(int pos) {
        if (this.mListLoader == null || this.mListLoader.getGoodsList() == null || this.mListLoader.getGoodsList().getData() == null || this.mListLoader.getGoodsList().getData().size() <= pos) {
            return null;
        }
        return this.mListLoader.getGoodsList().getData().get(pos);
    }

    public void onLoadMore() {
        if (this.mHolder != null) {
            this.mHolder.startFecthingMore();
            return;
        }
        ApiParams params = new ApiParams();
        params.addParam("categoryEnglishName", this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME));
        this.mListLoader.setParams(params);
        this.mListLoader.startFetching(getAppContext(), false, VadListLoader.E_LISTDATA_STATUS.E_LISTDATA_STATUS_ONLINE != this.mListLoader.getDataStatus());
    }

    public void onPageSwitchTo(int pos) {
        this.keepSilent = false;
        VadLogger.trackPageView(this.detail, getAppContext());
        if (pos != totalPages()) {
            this.detail = this.mListLoader.getGoodsList().getData().get(pos);
            this.mListLoader.setSelection(pos);
            updateTitle(getTitleDef());
            updateTitleBar(getTitleDef());
            updateContactBar(getView(), false);
            return;
        }
        updateTitleBar(getTitleDef());
        updateContactBar(getView(), true);
    }

    public void onRequestBigPic(int pos, Ad detail2) {
        if (this.galleryReturned) {
            Bundle bundle = createArguments(null, null);
            bundle.putInt("postIndex", pos);
            bundle.putSerializable("goodsDetail", detail2);
            this.galleryReturned = false;
            pushFragment(new BigGalleryFragment(), bundle);
        }
    }

    public void onRequestMap() {
        showMap();
    }

    public void onRequestUserAd(int userId, String userNick) {
        Bundle args = createArguments(null, null);
        args.putInt(ApiParams.KEY_USERID, userId);
        args.putString("userNick", userNick);
        args.putString("secondCategoryName", this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME));
        args.putString("adId", this.detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
        pushFragment(new UserAdFragment(), args);
    }

    public void onPageInitDone(VerticalViewPager pager, int pageIndex) {
        VerticalViewPager vp = pager != null ? pager : (VerticalViewPager) getActivity().findViewById(R.id.svDetail);
        if (vp != null && pageIndex == vp.getCurrentItem()) {
            updateTitleBar(getTitleDef());
            updateContactBar(vp.getRootView(), false);
        }
    }

    public boolean hasMore() {
        if (this.mListLoader == null) {
            return false;
        }
        return this.mListLoader.hasMore();
    }

    public void onNetworkDone(String apiName, String responseData) {
        sendMessage("ad.freeRefresh/".equals(apiName) ? 5 : "ad.delete/".equals(apiName) ? 7 : 6, responseData);
    }

    public void onNetworkFail(String apiName, ApiError error) {
        hideProgress();
        sendMessage(9, error == null ? "请求失败，请稍后重试" : error.getMsg());
        if ("ad.delete/".equals(apiName) && this.detail != null && !this.detail.isValidMessage()) {
            sendMessage(10, null);
        }
    }

    public void update(Observable observable, Object data) {
        if ((data instanceof BxMessageCenter.IBxNotification) && IBxNotificationNames.NOTIFICATION_LOGOUT.equals(((BxMessageCenter.IBxNotification) data).getName())) {
            View edit = getView() == null ? null : getView().findViewById(R.id.vad_tool_bar);
            if (edit == null) {
                return;
            }
            if (edit.getVisibility() == 0) {
                finishFragment();
                getActivity().sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_MYAD_LOGOUT));
                return;
            }
            getActivity().sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_COMMON_AD_LOGOUT));
        }
    }
}
