package com.baixing.view.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.BXLocation;
import com.baixing.entity.BXThumbnail;
import com.baixing.entity.PostGoodsBean;
import com.baixing.entity.Quota;
import com.baixing.entity.UserBean;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.sharing.referral.ReferralPost;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.util.post.ImageUploader;
import com.baixing.util.post.PostCommonValues;
import com.baixing.util.post.PostLocationService;
import com.baixing.util.post.PostNetworkService;
import com.baixing.util.post.PostUtil;
import com.baixing.view.fragment.MultiLevelSelectionFragment;
import com.baixing.widget.CustomDialogBuilder;
import com.baixing.widget.EditTitleDialogFragment;
import com.baixing.widget.VerifyFailDialog;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class PostGoodsFragment extends BaseFragment implements View.OnClickListener, ImageUploader.Callback {
    private static final String FILE_LAST_CATEGORY = "lastCategory";
    private static final int IMG_STATE_FAIL = 3;
    private static final int IMG_STATE_UPLOADED = 2;
    private static final int IMG_STATE_UPLOADING = 1;
    static final String KEY_CATE_ENGLISHNAME = "cateEnglishName";
    public static final String KEY_INIT_CATEGORY = "cateNames";
    static final String KEY_IS_EDITPOST = "isEditPost";
    static final String KEY_LAST_POST_CONTACT_USER = "lastPostContactIsRegisteredUser";
    private static final int MSG_CATEGORY_SEL_BACK = 11;
    private static final int MSG_DIALOG_BACK_WITH_DATA = 12;
    private static final int MSG_GEOCODING_TIMEOUT = 65553;
    private static final int MSG_GET_AD_FAIL = 15;
    private static final int MSG_GET_AD_SUCCED = 16;
    private static final int MSG_GET_QUOTA_AFTER_CATEGORY_FINISH = 17;
    private static final int MSG_GET_QUOTA_AFTER_LOGIN_FINISH = 18;
    public static final int MSG_HELP_SEND_END = 20;
    public static final int MSG_HELP_SEND_START = 19;
    private static final int MSG_IMAGE_STATE_CHANGE = 14;
    static final int MSG_POST_SUCCEED = -268435440;
    private static final int MSG_UPDATE_IMAGE_LIST = 13;
    private static final int NONE = 0;
    protected List<String> bmpUrls = new ArrayList();
    protected String categoryEnglishName = "";
    private String categoryName = "";
    protected String cityEnglishName = "";
    /* access modifiers changed from: private */
    public BXLocation detailLocation = null;
    private boolean doingAccountCheck = false;
    protected boolean editMode = false;
    private EditText etContact = null;
    private EditText etDescription = null;
    private boolean finishRightNow = false;
    private boolean isActive = false;
    protected boolean isNewPost = true;
    private long lastClickPostTime = 0;
    protected LinearLayout layout_txt;
    private boolean needShowDlg = false;
    protected PostParamsHolder params = new PostParamsHolder();
    private boolean paused = false;
    protected ArrayList<String> photoList = new ArrayList<>();
    private PostLocationService postLBS;
    private LinkedHashMap<String, PostGoodsBean> postList = new LinkedHashMap<>();
    /* access modifiers changed from: private */
    public PostNetworkService postNS;
    protected TextWatcher textWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            ((TextView) PostGoodsFragment.this.getView().findViewById(R.id.tv_title_post)).setText(s);
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != 0) {
            if (resultCode == 1) {
                this.finishRightNow = true;
                return;
            }
            if (resultCode == -1) {
                this.photoList.clear();
                if (data.getExtras().containsKey(CommonIntentAction.EXTRA_IMAGE_LIST)) {
                    this.photoList.addAll(data.getStringArrayListExtra(CommonIntentAction.EXTRA_IMAGE_LIST));
                }
            }
            this.handler.sendEmptyMessage(MSG_UPDATE_IMAGE_LIST);
        }
    }

    private void initWithCategoryNames(String categoryNames) {
        if (categoryNames == null || categoryNames.length() == 0) {
            categoryNames = (String) Util.loadDataFromLocate(getActivity(), FILE_LAST_CATEGORY, String.class);
        }
        if (categoryNames != null && !categoryNames.equals("")) {
            String[] names = categoryNames.split(",");
            if (names.length == 2) {
                this.categoryEnglishName = names[0];
                this.categoryName = names[1];
            } else if (names.length == 1) {
                this.categoryEnglishName = names[0];
            }
            Util.saveDataToLocate(getActivity(), FILE_LAST_CATEGORY, categoryNames);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        UserBean user;
        boolean z = false;
        PerformanceTracker.stamp(PerformEvent.Event.E_PGFrag_OnCreate_Start);
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            Log.d("QLM", savedInstanceState.toString());
            this.isNewPost = false;
        } else {
            if (!this.editMode) {
                z = true;
            }
            this.isNewPost = z;
        }
        Log.d("QLM", "isNewPost: " + this.isNewPost + ", editMode: " + this.editMode);
        initWithCategoryNames(getArguments().getString(KEY_INIT_CATEGORY));
        if (savedInstanceState != null) {
            this.postList.putAll((HashMap) savedInstanceState.getSerializable("postList"));
            this.params = (PostParamsHolder) savedInstanceState.getSerializable("params");
            this.photoList.addAll((List) savedInstanceState.getSerializable("listUrl"));
        }
        String appPhone = GlobalDataManager.getInstance().getPhoneNumber();
        if (!this.editMode && ((appPhone == null || appPhone.length() == 0) && (user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser()) != null && user.getPhone() != null && !user.getPhone().equals(""))) {
            GlobalDataManager.getInstance().setPhoneNumber(user.getPhone());
        }
        this.postLBS = new PostLocationService(this.handler);
        this.postNS = PostNetworkService.getInstance();
        this.postNS.setHandler(this.handler);
        this.cityEnglishName = GlobalDataManager.getInstance().getCityEnglishName();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        PostUtil.extractInputData(this.layout_txt, this.params);
        outState.putSerializable("params", this.params);
        outState.putSerializable("postList", this.postList);
        outState.putSerializable("listUrl", this.photoList);
    }

    /* access modifiers changed from: private */
    public void doClearUpImages() {
        this.photoList.clear();
        ImageUploader.getInstance().clearAll();
    }

    public boolean handleBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("退出发布？");
        builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PostGoodsFragment.this.doClearUpImages();
                boolean unused = PostGoodsFragment.this.finishFragment();
            }
        });
        builder.create().show();
        return true;
    }

    public void onResume() {
        super.onResume();
        if (this.finishRightNow) {
            this.finishRightNow = false;
            getView().post(new Runnable() {
                public void run() {
                    PostGoodsFragment.this.doClearUpImages();
                    boolean unused = PostGoodsFragment.this.finishFragment();
                }
            });
            return;
        }
        this.isActive = true;
        this.postLBS.start();
        if (!this.editMode && !this.isNewPost && !this.finishRightNow) {
            this.pv = TrackConfig.TrackMobile.PV.POST;
            Tracker.getInstance().pv(this.pv).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).end();
        }
        if (this.isNewPost) {
            this.isNewPost = false;
        }
        this.paused = false;
        if (this.needShowDlg) {
            showVerifyDlg();
            this.needShowDlg = false;
        }
        if (getView() != null) {
            getView().post(new Runnable() {
                public void run() {
                    int titleWidth = PostGoodsFragment.this.getView().findViewById(R.id.linearTop).getWidth();
                    int leftWidth = PostGoodsFragment.this.getView().findViewById(R.id.left_action).getWidth();
                    int maxWidth = ((titleWidth - (leftWidth * 2)) - PostGoodsFragment.this.getView().findViewById(R.id.imageView1).getWidth()) - (PostGoodsFragment.this.getView().findViewById(R.id.ll_post_title).getPaddingRight() * 4);
                    if (maxWidth > 0) {
                        ((TextView) PostGoodsFragment.this.getView().findViewById(R.id.tv_title_post)).setMaxWidth(maxWidth);
                    }
                }
            });
        }
    }

    public void onPause() {
        this.postLBS.stop();
        PostUtil.extractInputData(this.layout_txt, this.params);
        setPhoneAndAddress();
        this.isActive = false;
        super.onPause();
        this.paused = true;
    }

    public void onDestroy() {
        if (this.layout_txt != null) {
            for (int i = 0; i < this.layout_txt.getChildCount(); i++) {
                View child = this.layout_txt.getChildAt(i);
                if (child != null) {
                    child.setTag(PostCommonValues.HASH_CONTROL, null);
                }
            }
            View categoryItem = this.layout_txt.findViewById(R.id.categoryItem);
            if (categoryItem != null) {
                categoryItem.setTag(PostCommonValues.HASH_CONTROL, null);
            }
            View description = this.layout_txt.findViewById(R.id.img_description);
            if (description != null) {
                description.setTag(PostCommonValues.HASH_CONTROL, null);
            }
        }
        super.onDestroy();
    }

    public void onStackTop(boolean isBack) {
        if (isBack) {
            final ScrollView scroll = (ScrollView) getView().findViewById(R.id.goodscontent);
            scroll.post(new Runnable() {
                public void run() {
                    scroll.fullScroll(130);
                }
            });
        }
        if (this.isNewPost) {
            startImgSelDlg(1, "跳过\n拍照");
        }
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showPost();
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup v = (ViewGroup) inflater.inflate((int) R.layout.postgoodsview, (ViewGroup) null);
        this.layout_txt = (LinearLayout) v.findViewById(R.id.layout_txt);
        v.findViewById(R.id.quotaText).setVisibility(8);
        Button button = (Button) v.findViewById(R.id.iv_post_finish);
        button.setOnClickListener(this);
        if (!this.editMode) {
            button.setText("立即免费发布");
        } else {
            button.setText("立即更新信息");
        }
        return v;
    }

    /* access modifiers changed from: protected */
    public void startImgSelDlg(int cancelResultCode, String finishActionLabel) {
        PerformanceTracker.stamp(PerformEvent.Event.E_Send_Camera_Bootup);
        Intent backIntent = new Intent();
        backIntent.setClass(getActivity(), getActivity().getClass());
        Intent goIntent = new Intent();
        goIntent.putExtra("extra.common.intent", backIntent);
        goIntent.setAction(CommonIntentAction.ACTION_IMAGE_CAPTURE);
        goIntent.putExtra("extra.image.reqcode", 1);
        goIntent.putStringArrayListExtra(CommonIntentAction.EXTRA_IMAGE_LIST, this.photoList);
        goIntent.putExtra(CommonIntentAction.EXTRA_FINISH_ACTION_LABEL, finishActionLabel);
        goIntent.putExtra("extra.common.finishCode", cancelResultCode);
        goIntent.putExtra("isEdit", this.editMode);
        getActivity().startActivity(goIntent);
    }

    private void deployDefaultLayout() {
        addCategoryItem();
        HashMap<String, PostGoodsBean> pl = new HashMap<>();
        for (int i = 1; i < PostCommonValues.fixedItemNames.length; i++) {
            PostGoodsBean bean = new PostGoodsBean();
            bean.setControlType("input");
            bean.setDisplayName(PostCommonValues.fixedItemDisplayNames[i]);
            bean.setName(PostCommonValues.fixedItemNames[i]);
            bean.setUnit("");
            if (PostCommonValues.fixedItemNames[i].equals("价格")) {
                bean.setNumeric(1);
                bean.setUnit("元");
            }
            pl.put(PostCommonValues.fixedItemNames[i], bean);
        }
        buildPostLayout(pl);
    }

    public void updateNewCategoryLayout(String cateNames) {
        if (cateNames != null) {
            String[] names = cateNames.split(",");
            if (names == null || !this.categoryEnglishName.equals(names[0])) {
                initWithCategoryNames(cateNames);
                resetData(false);
                Util.saveDataToLocate(getActivity(), FILE_LAST_CATEGORY, cateNames);
                showPost();
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getCityEnglishName() {
        return this.cityEnglishName;
    }

    private void showGettingMetaProgress(boolean show) {
        View progress;
        for (int i = 0; i < this.layout_txt.getChildCount(); i++) {
            View v = this.layout_txt.getChildAt(i);
            if (!(v == null || (progress = v.findViewById(R.id.metaLoadingBar)) == null)) {
                if (show) {
                    progress.setVisibility(0);
                    v.findViewById(R.id.post_next).setVisibility(8);
                } else {
                    progress.setVisibility(8);
                    v.findViewById(R.id.post_next).setVisibility(0);
                }
            }
        }
    }

    private void showPost() {
        if (this.categoryEnglishName == null || this.categoryEnglishName.length() == 0) {
            deployDefaultLayout();
            return;
        }
        String cityEnglishName2 = getCityEnglishName();
        Pair<Long, String> pair = Util.loadJsonAndTimestampFromLocate(getActivity(), this.categoryEnglishName + cityEnglishName2);
        String json = (String) pair.second;
        if (json == null || json.length() <= 0 || ((Long) pair.first).longValue() + TextUtil.FULL_DAY < System.currentTimeMillis() / 1000) {
            showGettingMetaProgress(true);
            this.postNS.retreiveMetaAsync(cityEnglishName2, this.categoryEnglishName);
            return;
        }
        if (this.postList == null || this.postList.size() == 0) {
            this.postList = JsonUtil.getPostGoodsBean(json);
        }
        addCategoryItem();
        buildPostLayout(this.postList);
        loadCachedData();
        checkQuota(17);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_post_image /*2131165198*/:
                Tracker.getInstance().event(!this.editMode ? TrackConfig.TrackMobile.BxEvent.POST_INPUTING : TrackConfig.TrackMobile.BxEvent.EDITPOST_INPUTING).append(TrackConfig.TrackMobile.Key.ACTION, "image").end();
                startImgSelDlg(0, "完成");
                return;
            case R.id.postinputlayout /*2131165431*/:
                final View et2 = v.findViewById(R.id.postinput);
                if (et2 != null) {
                    et2.postDelayed(new Runnable() {
                        public void run() {
                            if (et2 != null) {
                                et2.requestFocus();
                                ((InputMethodManager) et2.getContext().getSystemService("input_method")).showSoftInput(et2, 1);
                            }
                        }
                    }, 100);
                    return;
                }
                return;
            case R.id.img_description /*2131165501*/:
                final View et = v.findViewById(R.id.description_input);
                if (et != null) {
                    et.postDelayed(new Runnable() {
                        public void run() {
                            if (et != null) {
                                Tracker.getInstance().event(!PostGoodsFragment.this.editMode ? TrackConfig.TrackMobile.BxEvent.POST_INPUTING : TrackConfig.TrackMobile.BxEvent.EDITPOST_INPUTING).append(TrackConfig.TrackMobile.Key.ACTION, "content").end();
                                et.requestFocus();
                                ((InputMethodManager) et.getContext().getSystemService("input_method")).showSoftInput(et, 1);
                            }
                        }
                    }, 100);
                    return;
                }
                return;
            case R.id.iv_post_finish /*2131165506*/:
                Tracker.getInstance().event(!this.editMode ? TrackConfig.TrackMobile.BxEvent.POST_POSTBTNCONTENTCLICKED : TrackConfig.TrackMobile.BxEvent.EDITPOST_POSTBTNCONTENTCLICKED).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).end();
                if (Math.abs(System.currentTimeMillis() - this.lastClickPostTime) > 500) {
                    postAction();
                    this.lastClickPostTime = System.currentTimeMillis();
                    return;
                }
                return;
            case R.id.delete_btn /*2131165583*/:
                final String img = (String) v.getTag();
                showAlert(null, "是否删除该照片", new BaseFragment.DialogAction(R.string.yes) {
                    public void doAction() {
                        ImageUploader.getInstance().cancel(img);
                        if (PostGoodsFragment.this.photoList.remove(img)) {
                            ViewGroup parent = (ViewGroup) PostGoodsFragment.this.getView().findViewById(R.id.image_list_parent);
                            View v = PostGoodsFragment.this.findImageViewByTag(img);
                            if (v != null) {
                                parent.removeView(v);
                            }
                            PostGoodsFragment.this.showAddImageButton(parent, LayoutInflater.from(v.getContext()), true);
                        }
                    }
                }, null);
                return;
            default:
                return;
        }
    }

    private void setPhoneAndAddress() {
        String phone = this.params.getData("contact");
        if (phone != null && phone.length() > 0 && !this.editMode) {
            GlobalDataManager.getInstance().setPhoneNumber(phone);
        }
        String address = this.params.getData(PostCommonValues.STRING_DETAIL_POSITION);
        if (address != null && address.length() > 0) {
            GlobalDataManager.getInstance().setAddress(address);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [?, ?, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    private void postAction() {
        boolean contactAndAddrVisible;
        String contentContact;
        if (this.postList == null || this.postList.size() == 0) {
            if (this.categoryEnglishName != null && this.categoryEnglishName.length() > 0) {
                return;
            }
            if (this.categoryName != null && this.categoryName.length() > 0) {
                return;
            }
        }
        PostUtil.extractInputData(this.layout_txt, this.params);
        if (getView().findViewById(R.id.ll_contactAndAddress).getVisibility() == 0) {
            contactAndAddrVisible = true;
        } else {
            contactAndAddrVisible = false;
        }
        String contentAdr = contactAndAddrVisible ? ((Button) getView().findViewById(R.id.btn_address)).getText().toString() : this.params.getData(PostCommonValues.STRING_DETAIL_POSITION);
        if (contactAndAddrVisible) {
            contentContact = ((Button) getView().findViewById(R.id.btn_contact)).getText().toString();
        } else {
            contentContact = this.params.getData("contact");
        }
        if (contentAdr != null && contentAdr.length() > 0) {
            GlobalDataManager.getInstance().setAddress(contentAdr);
        }
        if (contentContact != null && contentContact.length() > 0) {
            GlobalDataManager.getInstance().setPhoneNumber(contentContact);
        }
        if (getView().findViewById(R.id.ll_contactAndAddress).getVisibility() == 0) {
            this.params.put("contact", GlobalDataManager.getInstance().getPhoneNumber(), GlobalDataManager.getInstance().getPhoneNumber());
            this.params.put(PostCommonValues.STRING_DETAIL_POSITION, GlobalDataManager.getInstance().getAddress(), GlobalDataManager.getInstance().getAddress());
        } else {
            setPhoneAndAddress();
        }
        if (checkInputComplete()) {
            this.params.put(Constants.PARAM_TITLE, ((TextView) getView().findViewById(R.id.tv_title_post)).getText().toString(), ((TextView) getView().findViewById(R.id.tv_title_post)).getText().toString());
            PerformanceTracker.stamp(PerformEvent.Event.E_Start_PostAction);
            String detailLocationValue = this.params.getUiData(PostCommonValues.STRING_DETAIL_POSITION);
            if (this.detailLocation == null || !(detailLocationValue == null || detailLocationValue.length() == 0)) {
                Log.d("post", "pos2");
                sendMessageDelay(MSG_GEOCODING_TIMEOUT, null, 5000);
                showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_waiting, false);
                PerformanceTracker.stamp(PerformEvent.Event.E_PostAction_GetLocation_Start);
                this.postLBS.retreiveLocation(GlobalDataManager.getInstance().cityName, getFilledLocation());
                return;
            }
            Log.d("post", "pos1");
            showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_waiting, false);
            PerformanceTracker.stamp(PerformEvent.Event.E_PostAction_Direct_Start);
            postAd(this.detailLocation);
        }
    }

    private boolean checkInputComplete() {
        if (this.categoryEnglishName == null || this.categoryEnglishName.equals("")) {
            ViewUtil.showToast(getActivity(), "请选择分类", false);
            popupCategorySelectionDialog();
            return false;
        }
        for (int i = 0; i < this.postList.size(); i++) {
            PostGoodsBean postGoodsBean = this.postList.get((String) this.postList.keySet().toArray()[i]);
            if ((postGoodsBean.getName().equals("content") || (postGoodsBean.getRequired().endsWith("required") && !PostUtil.inArray(postGoodsBean.getName(), PostCommonValues.hiddenItemNames) && !postGoodsBean.getName().equals(Constants.PARAM_TITLE) && !postGoodsBean.getName().equals(PostCommonValues.STRING_AREA))) && ((!this.params.containsKey(postGoodsBean.getName()) || this.params.getData(postGoodsBean.getName()).equals("") || (postGoodsBean.getUnit() != null && this.params.getData(postGoodsBean.getName()).equals(postGoodsBean.getUnit()))) && !postGoodsBean.getName().equals("images"))) {
                postResultFail("please entering " + postGoodsBean.getDisplayName() + "!");
                ViewUtil.showToast(getActivity(), "请填写" + postGoodsBean.getDisplayName() + "!", false);
                changeFocusAfterPostError(postGoodsBean.getDisplayName());
                return false;
            }
        }
        if (!ImageUploader.getInstance().hasPendingJob()) {
            return true;
        }
        ViewUtil.showToast(getActivity(), "图片上传中", false);
        return false;
    }

    private String getFilledLocation() {
        return ((Button) getView().findViewById(R.id.btn_address)).getText().toString();
    }

    /* access modifiers changed from: protected */
    public void mergeParams(HashMap<String, String> hashMap) {
    }

    /* access modifiers changed from: protected */
    public void postAd(BXLocation location) {
        HashMap<String, String> list = new HashMap<>();
        list.put("categoryEnglishName", this.categoryEnglishName);
        list.put("cityEnglishName", this.cityEnglishName);
        HashMap<String, String> mapParams = new HashMap<>();
        Iterator<String> ite = this.params.keyIterator();
        while (ite.hasNext()) {
            String key = ite.next();
            mapParams.put(key, this.params.getData(key));
        }
        mergeParams(list);
        this.bmpUrls.clear();
        this.bmpUrls.addAll(ImageUploader.getInstance().getServerUrlList());
        String phone = (String) mapParams.get("contact");
        UserBean curUser = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (!ReferralUtil.isPromoter() || phone.equals(curUser.getPhone())) {
            PerformanceTracker.stamp(PerformEvent.Event.E_Post_Request_Sent);
            this.postNS.savePostData(mapParams, list, this.postList, this.bmpUrls, location, this.editMode);
            if (!(curUser == null || curUser.getPhone() == null || curUser.getPhone().length() <= 0)) {
                phone = curUser.getPhone();
            }
            this.postNS.doRegisterAndVerify(phone);
            return;
        }
        list.put("api_referral_id", curUser.getId().substring(1));
        list.put("api_compaign", "offline_promote");
        this.postNS.savePostData(mapParams, list, this.postList, this.bmpUrls, location, this.editMode);
        ReferralPost.Config(getFragmentManager(), this.postNS, this.handler);
        ReferralPost.getInstance().postNewAd(phone);
    }

    private int getLineCount() {
        if (this.etDescription != null) {
            return this.etDescription.getLineCount();
        }
        return 1;
    }

    private int getDescLength() {
        if (this.etDescription != null) {
            return this.etDescription.getText().length();
        }
        return 0;
    }

    private int getContactLength() {
        if (this.etContact != null) {
            return this.etContact.getText().length();
        }
        return 0;
    }

    private int getImgCount() {
        int imgCount = 0;
        for (int i = 0; i < this.bmpUrls.size(); i++) {
            if (!TextUtils.isEmpty(PostNetworkService.getUploadedUrl(this.bmpUrls.get(i)))) {
                imgCount++;
            }
        }
        return imgCount;
    }

    private void postResultSuccess() {
        TrackConfig.TrackMobile.BxEvent event = this.editMode ? TrackConfig.TrackMobile.BxEvent.EDITPOST_POSTRESULT : TrackConfig.TrackMobile.BxEvent.POST_POSTRESULT;
        boolean autoPosition = false;
        if (this.detailLocation != null && PostUtil.getLocationSummary(this.detailLocation).equals(this.params.getData(PostCommonValues.STRING_DETAIL_POSITION))) {
            autoPosition = true;
        }
        Tracker.getInstance().event(event).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).append(TrackConfig.TrackMobile.Key.POSTSTATUS, 1).append(TrackConfig.TrackMobile.Key.POSTPICSCOUNT, getImgCount()).append(TrackConfig.TrackMobile.Key.POSTDESCRIPTIONLINECOUNT, getLineCount()).append(TrackConfig.TrackMobile.Key.POSTDESCRIPTIONTEXTCOUNT, getDescLength()).append(TrackConfig.TrackMobile.Key.POSTCONTACTTEXTCOUNT, getContactLength()).append(TrackConfig.TrackMobile.Key.POSTDETAILPOSITIONAUTO, autoPosition).end();
    }

    private void postResultFail(String errorMsg) {
        Tracker.getInstance().event(this.editMode ? TrackConfig.TrackMobile.BxEvent.EDITPOST_POSTRESULT : TrackConfig.TrackMobile.BxEvent.POST_POSTRESULT).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).append(TrackConfig.TrackMobile.Key.POSTSTATUS, 0).append(TrackConfig.TrackMobile.Key.POSTFAILREASON, errorMsg).append(TrackConfig.TrackMobile.Key.POSTPICSCOUNT, getImgCount()).append(TrackConfig.TrackMobile.Key.POSTDESCRIPTIONLINECOUNT, getLineCount()).append(TrackConfig.TrackMobile.Key.POSTDESCRIPTIONTEXTCOUNT, getDescLength()).append(TrackConfig.TrackMobile.Key.POSTCONTACTTEXTCOUNT, getContactLength()).end();
    }

    private void loadCachedData() {
        if (this.params.size() != 0) {
            Iterator<String> it = this.params.keyIterator();
            while (it.hasNext()) {
                String name = it.next();
                for (int i = 0; i < this.layout_txt.getChildCount(); i++) {
                    View v = this.layout_txt.getChildAt(i);
                    PostGoodsBean bean = (PostGoodsBean) v.getTag(PostCommonValues.HASH_POST_BEAN);
                    if (bean != null && bean.getName().equals(name)) {
                        View control = (View) v.getTag(PostCommonValues.HASH_CONTROL);
                        String displayValue = this.params.getUiData(name);
                        if (control instanceof CheckBox) {
                            if (displayValue.contains(((CheckBox) control).getText())) {
                                ((CheckBox) control).setChecked(true);
                            } else {
                                ((CheckBox) control).setChecked(false);
                            }
                        } else if (control instanceof TextView) {
                            ((TextView) control).setText(displayValue);
                        }
                    }
                }
            }
        }
    }

    private void clearCategoryParameters() {
        Iterator<String> ite = this.params.keyIterator();
        while (ite.hasNext()) {
            String key = ite.next();
            if (!PostUtil.inArray(key, PostCommonValues.fixedItemNames) && !key.equals(Constants.PARAM_TITLE)) {
                this.params.remove(key);
                ite = this.params.keyIterator();
            }
        }
    }

    private void resetData(boolean clearImgs) {
        if (this.layout_txt != null) {
            View imgView = this.layout_txt.findViewById(R.id.image_list);
            View desView = this.layout_txt.findViewById(R.id.img_description);
            View catView = this.layout_txt.findViewById(R.id.categoryItem);
            this.layout_txt.removeAllViews();
            this.layout_txt.addView(imgView);
            this.layout_txt.addView(desView);
            if (catView != null) {
                this.layout_txt.addView(catView);
            }
        }
        this.postList.clear();
        if (Util.loadDataFromLocate(getActivity(), FILE_LAST_CATEGORY, String.class) != null) {
            clearCategoryParameters();
            if (clearImgs) {
                doClearUpImages();
                this.bmpUrls.clear();
                this.params.remove("content");
                this.params.remove("价格");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [?, ?, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    private void handleBackWithData(int message, Object obj) {
        if (message == 11 && obj != null) {
            String[] names = ((String) obj).split(",");
            if (names.length == 2) {
                if (!names[0].equals(this.categoryEnglishName)) {
                    this.categoryEnglishName = names[0];
                    this.categoryName = names[1];
                } else {
                    return;
                }
            } else if (names.length == 1) {
                if (!names[0].equals(this.categoryEnglishName)) {
                    this.categoryEnglishName = names[0];
                } else {
                    return;
                }
            }
            resetData(false);
            Util.saveDataToLocate(getActivity(), FILE_LAST_CATEGORY, obj);
            showPost();
        } else if (message == -61184) {
            if (this.layout_txt.findViewById(R.id.ll_contactAndAddress).getVisibility() == 0) {
                ((Button) getView().findViewById(R.id.btn_address)).setText(GlobalDataManager.getInstance().getAddress());
                ((Button) getView().findViewById(R.id.btn_contact)).setText(GlobalDataManager.getInstance().getPhoneNumber());
            }
        } else if ((message == 101 || message == 305463295 || message == -983039) && this.doingAccountCheck) {
            this.doingAccountCheck = false;
            Log.d("post", "pos3");
            showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_waiting, false);
            this.postNS.onOutActionDone(PostCommonValues.ACTION_POST_NEED_LOGIN_DONE, "");
        }
        PostUtil.fetchResultFromViewBack(message, obj, this.layout_txt, this.params);
    }

    public void onFragmentBackWithData(int message, Object obj) {
        handleBackWithData(message, obj);
    }

    /* access modifiers changed from: protected */
    public String getAdContact() {
        return "";
    }

    private void appendBeanToLayout(PostGoodsBean postBean) {
        UserBean user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (postBean.getName().equals("contact") && ((postBean.getValues() == null || postBean.getValues().isEmpty()) && user != null && user.getPhone() != null && user.getPhone().length() > 0)) {
            List<String> valueList = new ArrayList<>(1);
            valueList.add(user.getPhone());
            postBean.setValues(valueList);
            postBean.setLabels(valueList);
        }
        ViewGroup layout = createItemByPostBean(postBean);
        if (!(layout == null || layout.findViewById(R.id.postinputlayout) == null)) {
            layout.setClickable(true);
            layout.setOnClickListener(this);
        }
        if (postBean.getName().equals("content") && layout != null) {
            this.etDescription = (EditText) layout.getTag(PostCommonValues.HASH_CONTROL);
        } else if (postBean.getName().equals("价格")) {
            ((TextView) layout.findViewById(R.id.postinput)).setHint("越便宜成交越快");
        } else if (postBean.getName().equals("faburen")) {
            List<String> labels = postBean.getLabels();
            List<String> values = postBean.getValues();
            if (labels != null) {
                for (int i = 0; i < labels.size(); i++) {
                    if (labels.get(i).equals("个人")) {
                        ((TextView) layout.findViewById(R.id.posthint)).setText(labels.get(i));
                        this.params.put(postBean.getName(), labels.get(i), values.get(i));
                    }
                }
            }
        }
        if (layout != null) {
            this.layout_txt.addView(layout);
        }
    }

    /* access modifiers changed from: private */
    public void popupCategorySelectionDialog() {
        Bundle bundle = createArguments(null, null);
        bundle.putSerializable("items", (Serializable) GlobalDataManager.getInstance().getFirstLevelCategory());
        bundle.putInt("maxLevel", 1);
        bundle.putInt("reqestCode", 11);
        if (!(this.categoryEnglishName == null || this.categoryEnglishName.equals("") || this.categoryName == null)) {
            bundle.putString("selectedValue", this.categoryName);
        }
        PostUtil.extractInputData(this.layout_txt, this.params);
        new CustomDialogBuilder(getActivity(), getHandler(), bundle).start();
    }

    private void addCategoryItem() {
        if (this.layout_txt == null || this.layout_txt.findViewById(R.id.arrow_down) == null) {
            View categoryItem = this.layout_txt.findViewById(R.id.categoryItem);
            if (this.editMode) {
                this.layout_txt.removeView(categoryItem);
                return;
            }
            categoryItem.setTag(PostCommonValues.HASH_CONTROL, categoryItem.findViewById(R.id.posthint));
            ((TextView) categoryItem.findViewById(R.id.postshow)).setText("分类");
            categoryItem.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.POST_INPUTING).append(TrackConfig.TrackMobile.Key.ACTION, "类目").end();
                    PostGoodsFragment.this.popupCategorySelectionDialog();
                }
            });
            if (this.categoryEnglishName == null || this.categoryEnglishName.equals("") || this.categoryName == null) {
                ((TextView) categoryItem.findViewById(R.id.posthint)).setText("请选择分类");
            } else {
                ((TextView) categoryItem.findViewById(R.id.posthint)).setText(this.categoryName);
            }
            PostUtil.adjustMarginBottomAndHeight(categoryItem);
        }
    }

    private void buildFixedPostLayout(HashMap<String, PostGoodsBean> pl) {
        boolean contactAndPhoneVisible;
        PostGoodsBean bean;
        if (pl != null && pl.size() != 0) {
            HashMap<String, PostGoodsBean> pm = new HashMap<>();
            Object[] postListKeySetArray = pl.keySet().toArray();
            for (int i = 0; i < pl.size(); i++) {
                int j = 0;
                while (true) {
                    if (j >= PostCommonValues.fixedItemNames.length) {
                        break;
                    }
                    PostGoodsBean bean2 = pl.get(postListKeySetArray[i]);
                    if (bean2.getName().equals(PostCommonValues.fixedItemNames[j])) {
                        pm.put(PostCommonValues.fixedItemNames[j], bean2);
                        break;
                    }
                    j++;
                }
            }
            if (pm.containsKey("content") && (bean = (PostGoodsBean) pm.get("content")) != null) {
                View v = this.layout_txt.findViewById(R.id.img_description);
                EditText text = (EditText) v.findViewById(R.id.description_input);
                text.setText("");
                text.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() != 0) {
                            return false;
                        }
                        Tracker.getInstance().event(!PostGoodsFragment.this.editMode ? TrackConfig.TrackMobile.BxEvent.POST_INPUTING : TrackConfig.TrackMobile.BxEvent.EDITPOST_INPUTING).append(TrackConfig.TrackMobile.Key.ACTION, "content").end();
                        return false;
                    }
                });
                text.setOnFocusChangeListener(new PostUtil.BorderChangeListener(getActivity(), v));
                if (this.params.containsKey(Constants.PARAM_TITLE)) {
                    if (shouldAddTextWatcher(this.params.getData(Constants.PARAM_TITLE), this.params.getData("content"))) {
                        text.addTextChangedListener(this.textWatcher);
                    }
                    ((TextView) v.getRootView().findViewById(R.id.tv_title_post)).setText(this.params.getData(Constants.PARAM_TITLE));
                } else {
                    text.addTextChangedListener(this.textWatcher);
                }
                text.setHint("请输入" + bean.getDisplayName());
                v.setTag(PostCommonValues.HASH_POST_BEAN, bean);
                v.setTag(PostCommonValues.HASH_CONTROL, text);
                v.setOnClickListener(this);
                updateImageInfo(this.layout_txt);
            }
            if (TextUtils.isEmpty(GlobalDataManager.getInstance().getPhoneNumber()) || TextUtils.isEmpty(GlobalDataManager.getInstance().getAddress())) {
                contactAndPhoneVisible = false;
            } else {
                contactAndPhoneVisible = true;
            }
            boolean isPromoter = ReferralUtil.isPromoter();
            for (int i2 = 0; i2 < PostCommonValues.fixedItemNames.length; i2++) {
                if (!pm.containsKey(PostCommonValues.fixedItemNames[i2]) || PostCommonValues.fixedItemNames[i2].equals("content")) {
                    if (!pm.containsKey(PostCommonValues.fixedItemNames[i2])) {
                        this.params.remove(PostCommonValues.fixedItemNames[i2]);
                    }
                } else if (!contactAndPhoneVisible || ((!PostCommonValues.fixedItemNames[i2].equals("contact") && !PostCommonValues.fixedItemNames[i2].equals(PostCommonValues.STRING_DETAIL_POSITION)) || isPromoter)) {
                    appendBeanToLayout((PostGoodsBean) pm.get(PostCommonValues.fixedItemNames[i2]));
                }
            }
            getView().findViewById(R.id.ll_contactAndAddress).setVisibility(0);
            if (!contactAndPhoneVisible || isPromoter) {
                getView().findViewById(R.id.ll_contactAndAddress).setVisibility(8);
                return;
            }
            getView().findViewById(R.id.ll_contactAndAddress).setVisibility(0);
            setPhoneAndAddrLayout();
        }
    }

    /* access modifiers changed from: protected */
    public void setPhoneAndAddrLeftIcon() {
        int resId;
        int resId2;
        Button pBtn = getView() == null ? null : (Button) getView().findViewById(R.id.btn_contact);
        Button aBtn = getView() == null ? null : (Button) getView().findViewById(R.id.btn_address);
        if (aBtn != null && pBtn != null) {
            String text = pBtn.getText().toString();
            if (text == null || text.length() <= 0) {
                resId = R.drawable.icon_post_call_disable;
            } else {
                resId = R.drawable.icon_post_call;
            }
            BitmapDrawable bd = new BitmapDrawable(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(resId));
            bd.setBounds(0, 0, 45, 45);
            pBtn.setCompoundDrawables(bd, null, null, null);
            String text2 = aBtn.getText().toString();
            if (text2 == null || text2.length() <= 0) {
                resId2 = R.drawable.icon_location_disable;
            } else {
                resId2 = R.drawable.icon_location;
            }
            BitmapDrawable bd2 = new BitmapDrawable(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(resId2));
            bd2.setBounds(0, 0, 45, 45);
            aBtn.setCompoundDrawables(bd2, null, null, null);
        }
    }

    private void setPhoneAndAddrLayout() {
        String phone = GlobalDataManager.getInstance().getPhoneNumber();
        if (phone == null || phone.length() == 0) {
            ((Button) getView().findViewById(R.id.btn_contact)).setHint("联系方式");
        } else {
            ((Button) getView().findViewById(R.id.btn_contact)).setText(phone);
        }
        String address = GlobalDataManager.getInstance().getAddress();
        if (address != null && address.length() != 0) {
            ((Button) getView().findViewById(R.id.btn_address)).setText(address);
        } else if (this.detailLocation != null) {
            setDetailLocationControl(this.detailLocation);
        } else {
            ((Button) getView().findViewById(R.id.btn_address)).setHint("交易地点");
        }
        setPhoneAndAddrLeftIcon();
        getView().findViewById(R.id.btn_contact).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle temp = PostGoodsFragment.this.createArguments("填写联系方式", "");
                temp.putString("edittype", "contact");
                temp.putString("defaultValue", ((Button) PostGoodsFragment.this.getView().findViewById(R.id.btn_contact)).getText().toString());
                PostGoodsFragment.this.pushFragment(new ContactAndAddressDetailFragment(), temp);
            }
        });
        getView().findViewById(R.id.btn_address).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle temp = PostGoodsFragment.this.createArguments("填写交易地点", "");
                temp.putString("edittype", "address");
                temp.putString("defaultValue", ((Button) PostGoodsFragment.this.getView().findViewById(R.id.btn_address)).getText().toString());
                if (PostGoodsFragment.this.detailLocation != null) {
                    temp.putSerializable("location", PostGoodsFragment.this.detailLocation);
                }
                PostGoodsFragment.this.pushFragment(new ContactAndAddressDetailFragment(), temp);
            }
        });
    }

    private void addHiddenItemsToParams() {
        if (this.postList != null && !this.postList.isEmpty()) {
            for (String key : this.postList.keySet()) {
                PostGoodsBean bean = this.postList.get(key);
                int i = 0;
                while (true) {
                    if (i >= PostCommonValues.hiddenItemNames.length) {
                        break;
                    } else if (bean.getName().equals(PostCommonValues.hiddenItemNames[i])) {
                        String defaultValue = bean.getDefaultValue();
                        if (defaultValue == null || defaultValue.length() <= 0) {
                            this.params.put(bean.getName(), bean.getLabels().get(0), bean.getValues().get(0));
                        } else {
                            this.params.put(bean.getName(), defaultValue, defaultValue);
                        }
                    } else {
                        i++;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void buildPostLayout(HashMap<String, PostGoodsBean> pl) {
        getView().findViewById(R.id.goodscontent).setVisibility(0);
        getView().findViewById(R.id.networkErrorView).setVisibility(8);
        reCreateTitle();
        refreshHeader();
        if (pl != null && pl.size() != 0) {
            buildFixedPostLayout(pl);
            addHiddenItemsToParams();
            Object[] postListKeySetArray = pl.keySet().toArray();
            for (int i = 0; i < pl.size(); i++) {
                PostGoodsBean postBean = pl.get((String) postListKeySetArray[i]);
                if (!PostUtil.inArray(postBean.getName(), PostCommonValues.fixedItemNames) && !postBean.getName().equals(Constants.PARAM_TITLE) && !PostUtil.inArray(postBean.getName(), PostCommonValues.hiddenItemNames) && !postBean.getName().equals(PostCommonValues.STRING_AREA) && postBean.getRequired().contains("required")) {
                    appendBeanToLayout(postBean);
                }
            }
            showInputMethod();
        }
    }

    private View searchEditText(View parent, int resourceId) {
        View v = parent.findViewById(resourceId);
        if (v == null || !(v instanceof EditText) || (((EditText) v).getText() != null && ((EditText) v).getText().length() != 0)) {
            return null;
        }
        return v;
    }

    /* access modifiers changed from: private */
    public View getEmptyEditText() {
        View edit = null;
        for (int i = 0; i < this.layout_txt.getChildCount(); i++) {
            View child = this.layout_txt.getChildAt(i);
            if (child != null) {
                edit = searchEditText(child, R.id.description_input);
                if (edit != null || (edit = searchEditText(child, R.id.postinput)) != null) {
                    break;
                }
                edit = null;
            }
        }
        return edit;
    }

    private void showInputMethod() {
        final View root = getView();
        if (root != null) {
            root.postDelayed(new Runnable() {
                public void run() {
                    View ed = PostGoodsFragment.this.getEmptyEditText();
                    if (ed != null) {
                        ed.requestFocus();
                        ((InputMethodManager) root.getContext().getSystemService("input_method")).showSoftInput(ed, 1);
                    }
                }
            }, 200);
        }
    }

    /* access modifiers changed from: protected */
    public final void updateImageInfo(View rootView) {
        ViewGroup list;
        if (rootView != null && (list = (ViewGroup) rootView.findViewById(R.id.image_list_parent)) != null) {
            list.removeAllViews();
            LayoutInflater inflator = LayoutInflater.from(rootView.getContext());
            Iterator i$ = this.photoList.iterator();
            while (i$.hasNext()) {
                String img = i$.next();
                View imgParent = inflator.inflate((int) R.layout.post_image, (ViewGroup) null);
                imgParent.setTag(img);
                imgParent.setOnClickListener(this);
                imgParent.setId(R.id.delete_btn);
                int margin = (int) getResources().getDimension(R.dimen.post_img_margin);
                int wh = (int) getResources().getDimension(R.dimen.post_img_size);
                ViewGroup.MarginLayoutParams layParams = new ViewGroup.MarginLayoutParams(wh + margin, (margin * 2) + wh);
                layParams.setMargins(0, margin, margin, margin);
                list.addView(imgParent, layParams);
                ImageUploader.getInstance().registerCallback(img, this);
            }
            if (this.photoList == null || this.photoList.size() < 6) {
                showAddImageButton(list, inflator, false);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showAddImageButton(ViewGroup parent, LayoutInflater inflator, boolean scroolNow) {
        long j = 0;
        try {
            View addBtn = parent.getChildAt(parent.getChildCount() - 1);
            if (addBtn == null || addBtn.getId() != R.id.add_post_image) {
                View addBtn2 = inflator.inflate((int) R.layout.post_image, (ViewGroup) null);
                ((ImageView) addBtn2.findViewById(R.id.result_image)).setImageResource(R.drawable.btn_add_picture);
                addBtn2.setOnClickListener(this);
                addBtn2.setId(R.id.add_post_image);
                int margin = (int) getResources().getDimension(R.dimen.post_img_margin);
                int wh = (int) getResources().getDimension(R.dimen.post_img_size);
                ViewGroup.MarginLayoutParams layParams = new ViewGroup.MarginLayoutParams(wh, (margin * 2) + wh);
                layParams.setMargins(0, margin, 0, margin);
                parent.addView(addBtn2, layParams);
                if (!scroolNow) {
                    j = 300;
                }
                return;
            }
            final HorizontalScrollView hs = (HorizontalScrollView) parent.getParent();
            hs.postDelayed(new Runnable() {
                public void run() {
                    hs.scrollBy(1000, 0);
                }
            }, scroolNow ? j : 300);
        } finally {
            final HorizontalScrollView hs2 = (HorizontalScrollView) parent.getParent();
            AnonymousClass15 r10 = new Runnable() {
                public void run() {
                    hs2.scrollBy(1000, 0);
                }
            };
            if (!scroolNow) {
                j = 300;
            }
            hs2.postDelayed(r10, j);
        }
    }

    private boolean checkQuota(final int msg) {
        if (this.doingAccountCheck || this.editMode) {
            return false;
        }
        UserBean user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        if (user == null || TextUtils.isEmpty(user.getId()) || !GlobalDataManager.getInstance().getAccountManager().isUserLogin()) {
            return false;
        }
        ApiParams param = new ApiParams();
        param.addParam("cityEnglishName", this.cityEnglishName);
        param.addParam("categoryId", this.categoryEnglishName);
        param.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
        param.useCache = false;
        BaseApiCommand.createCommand("Business.getInfo/", true, param).execute(getAppContext(), new BaseApiCommand.Callback() {
            public void onNetworkDone(String apiName, String responseData) {
                PostGoodsFragment.this.sendMessage(msg, JsonUtil.parseQuota(responseData));
            }

            public void onNetworkFail(String apiName, ApiError error) {
                PostGoodsFragment.this.sendMessage(msg, null);
            }
        });
        return true;
    }

    private void handleOutOfQuota(final Quota qt) {
        final boolean cityExceed = qt.isMultiCity();
        AlertDialog.Builder bd = new AlertDialog.Builder(getActivity());
        bd.setTitle("").setMessage(qt.getMessage()).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.POST_RULE_ALERT_ACTION).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, PostGoodsFragment.this.categoryEnglishName).append(TrackConfig.TrackMobile.Key.RULENAME, qt.getType()).append(TrackConfig.TrackMobile.Key.MENU_ACTION_TYPE, "取消").end();
                dialog.dismiss();
                PostGoodsFragment.this.hideSoftKeyboard();
                Button button = (Button) PostGoodsFragment.this.layout_txt.getRootView().findViewById(R.id.iv_post_finish);
                if (button != null) {
                    button.setEnabled(false);
                }
                if (cityExceed) {
                    PostGoodsFragment.this.getActivity().finish();
                } else if (qt.getType().equals("类目发布超限")) {
                    PostGoodsFragment.this.popupCategorySelectionDialog();
                }
            }
        });
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.POST_RULE_ALERT_SHOW).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).append(TrackConfig.TrackMobile.Key.RULENAME, qt.getType()).end();
        bd.create().show();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v136, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r30, android.app.Activity r31, android.view.View r32) {
        /*
            r29 = this;
            r0 = r30
            int r0 = r0.what
            r26 = r0
            r27 = 66064(0x10210, float:9.2575E-41)
            r0 = r26
            r1 = r27
            if (r0 == r1) goto L_0x0012
            r29.hideProgress()
        L_0x0012:
            r0 = r30
            int r0 = r0.what
            r26 = r0
            switch(r26) {
                case -65519: goto L_0x01c1;
                case -65263: goto L_0x0342;
                case -61679: goto L_0x03c3;
                case -61678: goto L_0x04b5;
                case -61677: goto L_0x0535;
                case -61676: goto L_0x0633;
                case -61674: goto L_0x06b5;
                case -61672: goto L_0x0674;
                case -61671: goto L_0x06ba;
                case -61663: goto L_0x0215;
                case -9: goto L_0x0572;
                case 12: goto L_0x0194;
                case 13: goto L_0x01b5;
                case 14: goto L_0x0118;
                case 15: goto L_0x010d;
                case 16: goto L_0x0032;
                case 17: goto L_0x02ae;
                case 18: goto L_0x0231;
                case 19: goto L_0x001c;
                case 20: goto L_0x002e;
                case 65552: goto L_0x0582;
                case 65553: goto L_0x0582;
                case 66064: goto L_0x05ed;
                default: goto L_0x001b;
            }
        L_0x001b:
            return
        L_0x001c:
            java.lang.String r26 = "请稍候"
            java.lang.String r27 = "正在校验发送信息"
            r28 = 1
            r0 = r29
            r1 = r26
            r2 = r27
            r3 = r28
            r0.showProgress(r1, r2, r3)
            goto L_0x001b
        L_0x002e:
            r29.hideProgress()
            goto L_0x001b
        L_0x0032:
            r29.hideSoftKeyboard()
            r29.hideProgress()
            r0 = r30
            java.lang.Object r0 = r0.obj     // Catch:{ Exception -> 0x00e7 }
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26     // Catch:{ Exception -> 0x00e7 }
            com.baixing.entity.AdList r13 = com.baixing.jsonutil.JsonUtil.getGoodsListFromJson(r26)     // Catch:{ Exception -> 0x00e7 }
            if (r13 == 0) goto L_0x0105
            java.util.List r26 = r13.getData()     // Catch:{ Exception -> 0x00e7 }
            if (r26 == 0) goto L_0x0105
            java.util.List r26 = r13.getData()     // Catch:{ Exception -> 0x00e7 }
            int r26 = r26.size()     // Catch:{ Exception -> 0x00e7 }
            if (r26 <= 0) goto L_0x0105
            com.baixing.data.GlobalDataManager r27 = com.baixing.data.GlobalDataManager.getInstance()     // Catch:{ Exception -> 0x00e7 }
            java.util.List r26 = r13.getData()     // Catch:{ Exception -> 0x00e7 }
            r28 = 0
            r0 = r26
            r1 = r28
            java.lang.Object r26 = r0.get(r1)     // Catch:{ Exception -> 0x00e7 }
            com.baixing.entity.Ad r26 = (com.baixing.entity.Ad) r26     // Catch:{ Exception -> 0x00e7 }
            r0 = r27
            r1 = r26
            r0.updateMyAd(r1)     // Catch:{ Exception -> 0x00e7 }
            com.baixing.util.VadListLoader r14 = new com.baixing.util.VadListLoader     // Catch:{ Exception -> 0x00e7 }
            r26 = 0
            r27 = 0
            r28 = 0
            r0 = r26
            r1 = r27
            r2 = r28
            r14.<init>(r0, r1, r2, r13)     // Catch:{ Exception -> 0x00e7 }
            r14.setGoodsList(r13)     // Catch:{ Exception -> 0x00e7 }
            r26 = 0
            r0 = r26
            r14.setHasMore(r0)     // Catch:{ Exception -> 0x00e7 }
            r26 = 0
            r27 = 0
            r0 = r29
            r1 = r26
            r2 = r27
            android.os.Bundle r8 = r0.createArguments(r1, r2)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r26 = "loader"
            r0 = r26
            r8.putSerializable(r0, r14)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r26 = "index"
            r27 = 0
            r0 = r26
            r1 = r27
            r8.putInt(r0, r1)     // Catch:{ Exception -> 0x00e7 }
            r0 = r29
            boolean r0 = r0.editMode     // Catch:{ Exception -> 0x00e7 }
            r26 = r0
            if (r26 != 0) goto L_0x00ed
            java.lang.String r26 = "isVadPreview"
            java.lang.Boolean r27 = java.lang.Boolean.TRUE     // Catch:{ Exception -> 0x00e7 }
            boolean r27 = r27.booleanValue()     // Catch:{ Exception -> 0x00e7 }
            r0 = r26
            r1 = r27
            r8.putBoolean(r0, r1)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r26 = "inAnimation"
            r27 = 0
            r0 = r26
            r1 = r27
            r8.putInt(r0, r1)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r26 = "exitAnimation"
            r27 = 0
            r0 = r26
            r1 = r27
            r8.putInt(r0, r1)     // Catch:{ Exception -> 0x00e7 }
            com.baixing.view.fragment.VadFragment r26 = new com.baixing.view.fragment.VadFragment     // Catch:{ Exception -> 0x00e7 }
            r26.<init>()     // Catch:{ Exception -> 0x00e7 }
            r0 = r29
            r1 = r26
            r0.pushAndFinish(r1, r8)     // Catch:{ Exception -> 0x00e7 }
            goto L_0x001b
        L_0x00e7:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x001b
        L_0x00ed:
            r26 = -61664(0xffffffffffff0f20, float:NaN)
            java.util.List r27 = r13.getData()     // Catch:{ Exception -> 0x00e7 }
            r28 = 0
            java.lang.Object r27 = r27.get(r28)     // Catch:{ Exception -> 0x00e7 }
            r0 = r29
            r1 = r26
            r2 = r27
            r0.finishFragment(r1, r2)     // Catch:{ Exception -> 0x00e7 }
            goto L_0x001b
        L_0x0105:
            r29.hideProgress()     // Catch:{ Exception -> 0x00e7 }
            r29.finishFragment()     // Catch:{ Exception -> 0x00e7 }
            goto L_0x001b
        L_0x010d:
            r29.hideProgress()
            r29.hideSoftKeyboard()
            r29.finishFragment()
            goto L_0x001b
        L_0x0118:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r16 = r0
            com.baixing.entity.BXThumbnail r16 = (com.baixing.entity.BXThumbnail) r16
            r0 = r30
            int r0 = r0.arg1
            r23 = r0
            java.lang.String r26 = r16.getLocalPath()
            r0 = r29
            r1 = r26
            android.view.View r17 = r0.findImageViewByTag(r1)
            android.view.ViewGroup r17 = (android.view.ViewGroup) r17
            if (r17 == 0) goto L_0x001b
            r26 = 2131165490(0x7f070132, float:1.7945199E38)
            r0 = r17
            r1 = r26
            android.view.View r18 = r0.findViewById(r1)
            android.widget.ImageView r18 = (android.widget.ImageView) r18
            r26 = 2131165491(0x7f070133, float:1.79452E38)
            r0 = r17
            r1 = r26
            android.view.View r20 = r0.findViewById(r1)
            r26 = 1
            r0 = r23
            r1 = r26
            if (r0 == r1) goto L_0x015e
            r26 = 2
            r0 = r23
            r1 = r26
            if (r0 != r1) goto L_0x017f
        L_0x015e:
            android.graphics.Bitmap r26 = r16.getThumbnail()
            r0 = r18
            r1 = r26
            r0.setImageBitmap(r1)
            r26 = 1
            r0 = r23
            r1 = r26
            if (r0 != r1) goto L_0x017c
            r26 = 0
        L_0x0173:
            r0 = r20
            r1 = r26
            r0.setVisibility(r1)
            goto L_0x001b
        L_0x017c:
            r26 = 4
            goto L_0x0173
        L_0x017f:
            r26 = 2130837633(0x7f020081, float:1.7280226E38)
            r0 = r18
            r1 = r26
            r0.setImageResource(r1)
            r26 = 8
            r0 = r20
            r1 = r26
            r0.setVisibility(r1)
            goto L_0x001b
        L_0x0194:
            r0 = r30
            java.lang.Object r7 = r0.obj
            android.os.Bundle r7 = (android.os.Bundle) r7
            java.lang.String r26 = "reqestCode"
            r0 = r26
            int r26 = r7.getInt(r0)
            java.lang.String r27 = "lastChoise"
            r0 = r27
            java.io.Serializable r27 = r7.getSerializable(r0)
            r0 = r29
            r1 = r26
            r2 = r27
            r0.handleBackWithData(r1, r2)
            goto L_0x001b
        L_0x01b5:
            r0 = r29
            r1 = r32
            r0.updateImageInfo(r1)
            r29.showInputMethod()
            goto L_0x001b
        L_0x01c1:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.util.LinkedHashMap r26 = (java.util.LinkedHashMap) r26
            r0 = r26
            r1 = r29
            r1.postList = r0
            r29.addCategoryItem()
            r0 = r29
            java.util.LinkedHashMap<java.lang.String, com.baixing.entity.PostGoodsBean> r0 = r0.postList
            r26 = r0
            r0 = r29
            r1 = r26
            r0.buildPostLayout(r1)
            r29.loadCachedData()
            r26 = 17
            r0 = r29
            r1 = r26
            boolean r26 = r0.checkQuota(r1)
            if (r26 != 0) goto L_0x001b
            r26 = 0
            r0 = r29
            r1 = r26
            r0.showGettingMetaProgress(r1)
            r0 = r29
            android.widget.LinearLayout r0 = r0.layout_txt
            r26 = r0
            android.view.View r26 = r26.getRootView()
            r27 = 2131165506(0x7f070142, float:1.7945231E38)
            android.view.View r9 = r26.findViewById(r27)
            android.widget.Button r9 = (android.widget.Button) r9
            if (r9 == 0) goto L_0x001b
            r26 = 1
            r0 = r26
            r9.setEnabled(r0)
            goto L_0x001b
        L_0x0215:
            r26 = 18
            r0 = r29
            r1 = r26
            boolean r26 = r0.checkQuota(r1)
            if (r26 != 0) goto L_0x001b
            r0 = r29
            com.baixing.util.post.PostNetworkService r0 = r0.postNS
            r26 = r0
            r27 = 286330626(0x11110f02, float:1.1443108E-28)
            java.lang.String r28 = ""
            r26.onOutActionDone(r27, r28)
            goto L_0x001b
        L_0x0231:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x029e
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof com.baixing.entity.Quota
            r26 = r0
            if (r26 == 0) goto L_0x029e
            r0 = r29
            android.widget.LinearLayout r0 = r0.layout_txt
            r26 = r0
            android.view.View r26 = r26.getRootView()
            r27 = 2131165497(0x7f070139, float:1.7945213E38)
            android.view.View r26 = r26.findViewById(r27)
            android.widget.TextView r26 = (android.widget.TextView) r26
            r0 = r30
            java.lang.Object r0 = r0.obj
            r27 = r0
            com.baixing.entity.Quota r27 = (com.baixing.entity.Quota) r27
            java.lang.String r27 = r27.getMessage()
            r26.setText(r27)
            r0 = r29
            android.widget.LinearLayout r0 = r0.layout_txt
            r26 = r0
            android.view.View r26 = r26.getRootView()
            r27 = 2131165497(0x7f070139, float:1.7945213E38)
            android.view.View r26 = r26.findViewById(r27)
            r27 = 0
            r26.setVisibility(r27)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.entity.Quota r26 = (com.baixing.entity.Quota) r26
            boolean r26 = r26.isOutOfQuota()
            if (r26 == 0) goto L_0x029e
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.entity.Quota r26 = (com.baixing.entity.Quota) r26
            r0 = r29
            r1 = r26
            r0.handleOutOfQuota(r1)
            goto L_0x001b
        L_0x029e:
            r0 = r29
            com.baixing.util.post.PostNetworkService r0 = r0.postNS
            r26 = r0
            r27 = 286330626(0x11110f02, float:1.1443108E-28)
            java.lang.String r28 = ""
            r26.onOutActionDone(r27, r28)
            goto L_0x001b
        L_0x02ae:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof com.baixing.entity.Quota
            r26 = r0
            if (r26 == 0) goto L_0x032d
            r0 = r29
            android.widget.LinearLayout r0 = r0.layout_txt
            r26 = r0
            android.view.View r26 = r26.getRootView()
            r27 = 2131165506(0x7f070142, float:1.7945231E38)
            android.view.View r9 = r26.findViewById(r27)
            android.widget.Button r9 = (android.widget.Button) r9
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.entity.Quota r26 = (com.baixing.entity.Quota) r26
            boolean r26 = r26.isOutOfQuota()
            if (r26 == 0) goto L_0x0338
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.entity.Quota r26 = (com.baixing.entity.Quota) r26
            r0 = r29
            r1 = r26
            r0.handleOutOfQuota(r1)
            if (r9 == 0) goto L_0x02f5
            r26 = 0
            r0 = r26
            r9.setEnabled(r0)
        L_0x02f5:
            r0 = r29
            android.widget.LinearLayout r0 = r0.layout_txt
            r26 = r0
            android.view.View r26 = r26.getRootView()
            r27 = 2131165497(0x7f070139, float:1.7945213E38)
            android.view.View r26 = r26.findViewById(r27)
            android.widget.TextView r26 = (android.widget.TextView) r26
            r0 = r30
            java.lang.Object r0 = r0.obj
            r27 = r0
            com.baixing.entity.Quota r27 = (com.baixing.entity.Quota) r27
            java.lang.String r27 = r27.getMessage()
            r26.setText(r27)
            r0 = r29
            android.widget.LinearLayout r0 = r0.layout_txt
            r26 = r0
            android.view.View r26 = r26.getRootView()
            r27 = 2131165497(0x7f070139, float:1.7945213E38)
            android.view.View r26 = r26.findViewById(r27)
            r27 = 0
            r26.setVisibility(r27)
        L_0x032d:
            r26 = 0
            r0 = r29
            r1 = r26
            r0.showGettingMetaProgress(r1)
            goto L_0x001b
        L_0x0338:
            if (r9 == 0) goto L_0x02f5
            r26 = 1
            r0 = r26
            r9.setEnabled(r0)
            goto L_0x02f5
        L_0x0342:
            r29.hideProgress()
            r0 = r29
            android.widget.LinearLayout r0 = r0.layout_txt
            r26 = r0
            android.view.View r26 = r26.getRootView()
            r27 = 2131165506(0x7f070142, float:1.7945231E38)
            android.view.View r9 = r26.findViewById(r27)
            android.widget.Button r9 = (android.widget.Button) r9
            if (r9 == 0) goto L_0x0361
            r26 = 0
            r0 = r26
            r9.setEnabled(r0)
        L_0x0361:
            r29.addCategoryItem()
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x03a1
            java.lang.String r21 = ""
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof com.baixing.util.post.PostNetworkService.PostResultData
            r26 = r0
            if (r26 == 0) goto L_0x03ac
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.util.post.PostNetworkService$PostResultData r26 = (com.baixing.util.post.PostNetworkService.PostResultData) r26
            r0 = r26
            java.lang.String r0 = r0.message
            r21 = r0
        L_0x038a:
            java.lang.String r26 = ""
            r0 = r21
            r1 = r26
            boolean r26 = r0.equals(r1)
            if (r26 != 0) goto L_0x03a1
            r26 = 0
            r0 = r31
            r1 = r21
            r2 = r26
            com.baixing.util.ViewUtil.showToast(r0, r1, r2)
        L_0x03a1:
            r26 = 0
            r0 = r29
            r1 = r26
            r0.showGettingMetaProgress(r1)
            goto L_0x001b
        L_0x03ac:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof java.lang.String
            r26 = r0
            if (r26 == 0) goto L_0x038a
            r0 = r30
            java.lang.Object r0 = r0.obj
            r21 = r0
            java.lang.String r21 = (java.lang.String) r21
            goto L_0x038a
        L_0x03c3:
            com.baixing.util.PerformEvent$Event r26 = com.baixing.util.PerformEvent.Event.E_POST_SUCCEEDED
            com.baixing.util.PerformanceTracker.stamp(r26)
            r29.hideProgress()
            r29.doClearUpImages()
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.util.post.PostNetworkService$PostResultData r26 = (com.baixing.util.post.PostNetworkService.PostResultData) r26
            r0 = r26
            java.lang.String r15 = r0.id
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.util.post.PostNetworkService$PostResultData r26 = (com.baixing.util.post.PostNetworkService.PostResultData) r26
            r0 = r26
            java.lang.String r0 = r0.message
            r22 = r0
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.util.post.PostNetworkService$PostResultData r26 = (com.baixing.util.post.PostNetworkService.PostResultData) r26
            r0 = r26
            int r10 = r0.error
            java.lang.String r26 = ""
            r0 = r26
            boolean r26 = r15.equals(r0)
            if (r26 != 0) goto L_0x0495
            if (r10 != 0) goto L_0x0495
            r29.postResultSuccess()
            r26 = 0
            r27 = 0
            r0 = r29
            r1 = r26
            r2 = r27
            android.os.Bundle r6 = r0.createArguments(r1, r2)
            java.lang.String r26 = "forceUpdate"
            r27 = 1
            r0 = r26
            r1 = r27
            r6.putInt(r0, r1)
            r0 = r29
            boolean r0 = r0.editMode
            r26 = r0
            if (r26 == 0) goto L_0x0434
            r0 = r29
            boolean r0 = r0.editMode
            r26 = r0
            if (r26 == 0) goto L_0x045e
            r0 = r29
            boolean r0 = r0.isActive
            r26 = r0
            if (r26 == 0) goto L_0x045e
        L_0x0434:
            r0 = r29
            boolean r0 = r0.editMode
            r26 = r0
            if (r26 != 0) goto L_0x0492
            r26 = 1
        L_0x043e:
            r0 = r29
            r1 = r26
            r0.resetData(r1)
            android.support.v4.app.FragmentActivity r26 = r29.getActivity()
            java.lang.String r27 = "lastCategory"
            com.baixing.util.Util.deleteDataFromLocate(r26, r27)
            java.lang.String r26 = ""
            r0 = r26
            r1 = r29
            r1.categoryEnglishName = r0
            java.lang.String r26 = ""
            r0 = r26
            r1 = r29
            r1.categoryName = r0
        L_0x045e:
            r0 = r29
            r0.handlePostFinish(r15)
            r0 = r29
            boolean r0 = r0.editMode
            r26 = r0
            if (r26 != 0) goto L_0x001b
            android.support.v4.app.FragmentActivity r4 = r29.getActivity()
            if (r4 == 0) goto L_0x048d
            android.content.Intent r19 = r4.getIntent()
            if (r19 == 0) goto L_0x048d
            java.lang.String r26 = "forceUpdate"
            r27 = 1
            r0 = r19
            r1 = r26
            r2 = r27
            r0.putExtra(r1, r2)
            java.lang.String r26 = "lastPost"
            r0 = r19
            r1 = r26
            r0.putExtra(r1, r15)
        L_0x048d:
            r29.doClearUpImages()
            goto L_0x001b
        L_0x0492:
            r26 = 0
            goto L_0x043e
        L_0x0495:
            r0 = r29
            r1 = r22
            r0.postResultFail(r1)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x001b
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.util.post.PostNetworkService$PostResultData r26 = (com.baixing.util.post.PostNetworkService.PostResultData) r26
            r0 = r29
            r1 = r26
            r0.handlePostFail(r1)
            goto L_0x001b
        L_0x04b5:
            r29.hideProgress()
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x001b
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof java.lang.String
            r26 = r0
            if (r26 == 0) goto L_0x0501
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26
            r27 = 0
            r0 = r31
            r1 = r26
            r2 = r27
            com.baixing.util.ViewUtil.showToast(r0, r1, r2)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26
            r0 = r29
            r1 = r26
            r0.changeFocusAfterPostError(r1)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26
            r0 = r29
            r1 = r26
            r0.postResultFail(r1)
            goto L_0x001b
        L_0x0501:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof com.baixing.util.post.PostNetworkService.PostResultData
            r26 = r0
            if (r26 == 0) goto L_0x001b
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.util.post.PostNetworkService$PostResultData r26 = (com.baixing.util.post.PostNetworkService.PostResultData) r26
            r0 = r29
            r1 = r26
            r0.handlePostFail(r1)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.util.post.PostNetworkService$PostResultData r26 = (com.baixing.util.post.PostNetworkService.PostResultData) r26
            r0 = r26
            java.lang.String r0 = r0.message
            r26 = r0
            r0 = r29
            r1 = r26
            r0.postResultFail(r1)
            goto L_0x001b
        L_0x0535:
            r29.hideProgress()
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x0563
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof java.lang.String
            r26 = r0
            if (r26 == 0) goto L_0x0563
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26
            r27 = 0
            r0 = r31
            r1 = r26
            r2 = r27
            com.baixing.util.ViewUtil.showToast(r0, r1, r2)
            goto L_0x001b
        L_0x0563:
            java.lang.String r26 = "网络连接异常"
            r27 = 0
            r0 = r31
            r1 = r26
            r2 = r27
            com.baixing.util.ViewUtil.showToast(r0, r1, r2)
            goto L_0x001b
        L_0x0572:
            r29.hideProgress()
            com.baixing.util.ErrorHandler r26 = com.baixing.util.ErrorHandler.getInstance()
            r0 = r26
            r1 = r30
            r0.handleMessage(r1)
            goto L_0x001b
        L_0x0582:
            r0 = r30
            int r0 = r0.what
            r26 = r0
            r27 = 65553(0x10011, float:9.186E-41)
            r0 = r26
            r1 = r27
            if (r0 != r1) goto L_0x05e1
            com.baixing.util.PerformEvent$Event r12 = com.baixing.util.PerformEvent.Event.E_GeoCoding_Timeout
        L_0x0593:
            com.baixing.util.PerformanceTracker.stamp(r12)
            java.lang.String r26 = "post"
            java.lang.String r27 = "pos4"
            android.util.Log.d(r26, r27)
            boolean r26 = com.baixing.sharing.referral.ReferralUtil.isPromoter()
            if (r26 != 0) goto L_0x05b6
            r26 = 2131230765(0x7f08002d, float:1.8077592E38)
            r27 = 2131230770(0x7f080032, float:1.8077602E38)
            r28 = 0
            r0 = r29
            r1 = r26
            r2 = r27
            r3 = r28
            r0.showProgress(r1, r2, r3)
        L_0x05b6:
            r0 = r29
            android.os.Handler r0 = r0.handler
            r26 = r0
            r27 = 65553(0x10011, float:9.186E-41)
            r26.removeMessages(r27)
            r0 = r29
            android.os.Handler r0 = r0.handler
            r26 = r0
            r27 = 65552(0x10010, float:9.1858E-41)
            r26.removeMessages(r27)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 != 0) goto L_0x05e4
            r26 = 0
        L_0x05d8:
            r0 = r29
            r1 = r26
            r0.postAd(r1)
            goto L_0x001b
        L_0x05e1:
            com.baixing.util.PerformEvent$Event r12 = com.baixing.util.PerformEvent.Event.E_GeoCoding_Fetched
            goto L_0x0593
        L_0x05e4:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.entity.BXLocation r26 = (com.baixing.entity.BXLocation) r26
            goto L_0x05d8
        L_0x05ed:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            com.baixing.entity.BXLocation r26 = (com.baixing.entity.BXLocation) r26
            r0 = r26
            r1 = r29
            r1.detailLocation = r0
            android.view.View r26 = r29.getView()
            if (r26 != 0) goto L_0x0623
            r5 = 0
        L_0x0602:
            if (r5 == 0) goto L_0x001b
            java.lang.CharSequence r26 = r5.getText()
            if (r26 == 0) goto L_0x0614
            java.lang.CharSequence r26 = r5.getText()
            int r26 = r26.length()
            if (r26 != 0) goto L_0x001b
        L_0x0614:
            r0 = r29
            com.baixing.entity.BXLocation r0 = r0.detailLocation
            r26 = r0
            r0 = r29
            r1 = r26
            r0.setDetailLocationControl(r1)
            goto L_0x001b
        L_0x0623:
            android.view.View r26 = r29.getView()
            r27 = 2131165505(0x7f070141, float:1.794523E38)
            android.view.View r26 = r26.findViewById(r27)
            android.widget.Button r26 = (android.widget.Button) r26
            r5 = r26
            goto L_0x0602
        L_0x0633:
            java.lang.String r26 = "登录"
            java.lang.String r27 = ""
            r0 = r29
            r1 = r26
            r2 = r27
            android.os.Bundle r24 = r0.createArguments(r1, r2)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x065c
            java.lang.String r27 = "defaultNumber"
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26
            r0 = r24
            r1 = r27
            r2 = r26
            r0.putString(r1, r2)
        L_0x065c:
            com.baixing.view.fragment.LoginFragment r26 = new com.baixing.view.fragment.LoginFragment
            r26.<init>()
            r0 = r29
            r1 = r26
            r2 = r24
            r0.pushFragment(r1, r2)
            r26 = 1
            r0 = r26
            r1 = r29
            r1.doingAccountCheck = r0
            goto L_0x001b
        L_0x0674:
            java.lang.String r26 = ""
            java.lang.String r27 = ""
            r0 = r29
            r1 = r26
            r2 = r27
            android.os.Bundle r25 = r0.createArguments(r1, r2)
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x069d
            java.lang.String r27 = "defaultNumber"
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26
            r0 = r25
            r1 = r27
            r2 = r26
            r0.putString(r1, r2)
        L_0x069d:
            com.baixing.view.fragment.RegisterFragment r26 = new com.baixing.view.fragment.RegisterFragment
            r26.<init>()
            r0 = r29
            r1 = r26
            r2 = r25
            r0.pushFragment(r1, r2)
            r26 = 1
            r0 = r26
            r1 = r29
            r1.doingAccountCheck = r0
            goto L_0x001b
        L_0x06b5:
            r29.showVerifyDlg()
            goto L_0x001b
        L_0x06ba:
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            if (r26 == 0) goto L_0x001b
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            r0 = r26
            boolean r0 = r0 instanceof java.lang.String
            r26 = r0
            if (r26 == 0) goto L_0x001b
            r0 = r30
            java.lang.Object r0 = r0.obj
            r26 = r0
            java.lang.String r26 = (java.lang.String) r26
            r27 = 0
            r0 = r31
            r1 = r26
            r2 = r27
            com.baixing.util.ViewUtil.showToast(r0, r1, r2)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.view.fragment.PostGoodsFragment.handleMessage(android.os.Message, android.app.Activity, android.view.View):void");
    }

    private void showVerifyDlg() {
        if (this.paused) {
            this.needShowDlg = true;
            return;
        }
        new VerifyFailDialog(new VerifyFailDialog.VerifyListener() {
            public void onReVerify(String mobile) {
                Log.d("post", "pos5");
                PostGoodsFragment.this.showProgress(R.string.dialog_title_info, R.string.dialog_message_waiting, false);
                PostGoodsFragment.this.postNS.onOutActionDone(PostCommonValues.ACTION_POST_NEED_REVERIIFY, null);
            }

            public void onSendVerifyCode(String code) {
                Log.d("post", "pos6");
                PostGoodsFragment.this.showProgress(R.string.dialog_title_info, R.string.dialog_message_waiting, false);
                PostGoodsFragment.this.postNS.onOutActionDone(PostCommonValues.ACTION_POST_NEED_REVERIIFY, code);
            }
        }).show(getFragmentManager(), (String) null);
        this.needShowDlg = false;
    }

    private void changeFocusAfterPostError(String errMsg) {
        Set<String> keys;
        PostGoodsBean tag;
        if (this.postList != null && (keys = this.postList.keySet()) != null) {
            for (String key : keys) {
                if (errMsg.contains(this.postList.get(key).getDisplayName())) {
                    int j = 0;
                    while (j < this.layout_txt.getChildCount()) {
                        final View child = this.layout_txt.getChildAt(j);
                        if (child == null || (tag = (PostGoodsBean) child.getTag(PostCommonValues.HASH_POST_BEAN)) == null || !tag.getName().equals(this.postList.get(key).getName())) {
                            j++;
                        } else {
                            View et = child.findViewById(R.id.postinput);
                            if (et == null) {
                                et = child.findViewById(R.id.description_input);
                            }
                            if (et != null) {
                                final View inputView = et;
                                inputView.postDelayed(new Runnable() {
                                    public void run() {
                                        inputView.requestFocus();
                                        ((InputMethodManager) inputView.getContext().getSystemService("input_method")).showSoftInput(inputView, 1);
                                    }
                                }, 100);
                                return;
                            }
                            child.postDelayed(new Runnable() {
                                public void run() {
                                    child.performClick();
                                }
                            }, 100);
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }

    private void handlePostFail(final PostNetworkService.PostResultData result) {
        if (result != null) {
            if (result.error == 505) {
                AlertDialog.Builder bd = new AlertDialog.Builder(getActivity());
                bd.setTitle("").setMessage(result.message).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        PostGoodsFragment.this.handlePostFinish(result.id);
                    }
                });
                bd.create().show();
            } else if (result.message != null && !result.message.equals("")) {
                ViewUtil.showToast(getActivity(), result.message, false);
            }
        }
    }

    private void setInputContent() {
        View control;
        if (this.layout_txt != null) {
            for (int i = 0; i < this.layout_txt.getChildCount(); i++) {
                View v = this.layout_txt.getChildAt(i);
                PostGoodsBean bean = (PostGoodsBean) v.getTag(PostCommonValues.HASH_POST_BEAN);
                if (!(bean == null || (control = (View) v.getTag(PostCommonValues.HASH_CONTROL)) == null || !(control instanceof TextView) || this.params == null || !this.params.containsKey(bean.getName()))) {
                    String value = this.params.getUiData(bean.getName());
                    if (value == null) {
                        value = this.params.getUiData(bean.getName());
                    }
                    if (bean.getName().equals("contact")) {
                        if (this.editMode) {
                            ((TextView) control).setText(getAdContact());
                        } else {
                            String phone = GlobalDataManager.getInstance().getPhoneNumber();
                            if (phone != null && phone.length() > 0) {
                                ((TextView) control).setText(phone);
                            }
                        }
                    }
                    ((TextView) control).setText(value);
                }
            }
        }
    }

    public void onStart() {
        super.onStart();
        setInputContent();
    }

    private static boolean shouldAddTextWatcher(String title, String description) {
        String subDescription;
        if (TextUtils.isEmpty(title)) {
            return true;
        }
        if (TextUtils.isEmpty(description)) {
            return false;
        }
        if (description.length() > 25) {
            subDescription = description.substring(0, 25);
        } else {
            subDescription = description;
        }
        return title.equals(subDescription);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_leftActionHint = "返回";
        title.m_leftActionImage = R.drawable.icon_close;
        title.m_titleControls = LayoutInflater.from(getActivity()).inflate((int) R.layout.title_post, (ViewGroup) null);
        title.m_titleControls.findViewById(R.id.ll_post_title).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditTitleDialogFragment dlgEdit = new EditTitleDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.PARAM_TITLE, ((TextView) PostGoodsFragment.this.getView().findViewById(R.id.tv_title_post)).getText().toString());
                dlgEdit.setCallback(new EditTitleDialogFragment.ICallback() {
                    public void onTitleChangeFinished(String newTitle) {
                        String currentTitle = ((TextView) PostGoodsFragment.this.getView().findViewById(R.id.tv_title_post)).getText().toString();
                        if (!TextUtils.isEmpty(newTitle) && !newTitle.equals(currentTitle)) {
                            ((EditText) PostGoodsFragment.this.getView().findViewById(R.id.description_input)).removeTextChangedListener(PostGoodsFragment.this.textWatcher);
                            ((TextView) PostGoodsFragment.this.getView().findViewById(R.id.tv_title_post)).setText(newTitle);
                        }
                    }
                });
                dlgEdit.setArguments(bundle);
                dlgEdit.show(PostGoodsFragment.this.getFragmentManager(), (String) null);
            }
        });
    }

    private ViewGroup createItemByPostBean(PostGoodsBean postBean) {
        final ViewGroup layout = PostUtil.createItemByPostBean(postBean, getActivity());
        if (layout == null) {
            return null;
        }
        if (postBean.getControlType().equals("select") || postBean.getControlType().equals("checkbox")) {
            final String actionName = ((PostGoodsBean) layout.getTag(PostCommonValues.HASH_POST_BEAN)).getDisplayName();
            layout.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
                 arg types: [com.baixing.view.fragment.OtherPropertiesFragment, android.os.Bundle, int]
                 candidates:
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
                  com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
                public void onClick(View v) {
                    Tracker.getInstance().event(!PostGoodsFragment.this.editMode ? TrackConfig.TrackMobile.BxEvent.POST_INPUTING : TrackConfig.TrackMobile.BxEvent.EDITPOST_INPUTING).append(TrackConfig.TrackMobile.Key.ACTION, actionName).end();
                    PostGoodsBean postBean = (PostGoodsBean) v.getTag(PostCommonValues.HASH_POST_BEAN);
                    if (postBean.getControlType().equals("select") || postBean.getControlType().equals("tableSelect")) {
                        if (postBean.getLevelCount() > 0) {
                            ArrayList<MultiLevelSelectionFragment.MultiLevelItem> items = new ArrayList<>();
                            for (int i = 0; i < postBean.getLabels().size(); i++) {
                                MultiLevelSelectionFragment.MultiLevelItem t = new MultiLevelSelectionFragment.MultiLevelItem();
                                t.txt = postBean.getLabels().get(i);
                                t.id = postBean.getValues().get(i);
                                items.add(t);
                            }
                            Bundle bundle = PostGoodsFragment.this.createArguments(null, null);
                            bundle.putInt("reqestCode", postBean.getName().hashCode());
                            bundle.putSerializable("items", items);
                            bundle.putInt("maxLevel", postBean.getLevelCount() - 1);
                            String selectedValue = PostGoodsFragment.this.params.getData(postBean.getName());
                            if (selectedValue != null) {
                                bundle.putString("selectedValue", selectedValue);
                            }
                            PostUtil.extractInputData(PostGoodsFragment.this.layout_txt, PostGoodsFragment.this.params);
                            new CustomDialogBuilder(PostGoodsFragment.this.getActivity(), PostGoodsFragment.this.getHandler(), bundle).start();
                            return;
                        }
                        Bundle bundle2 = PostGoodsFragment.this.createArguments(postBean.getDisplayName(), null);
                        bundle2.putInt("reqestCode", postBean.getName().hashCode());
                        bundle2.putBoolean("singleSelection", false);
                        bundle2.putSerializable("properties", (ArrayList) postBean.getLabels());
                        TextView txview = (TextView) v.getTag(PostCommonValues.HASH_CONTROL);
                        if (txview != null) {
                            bundle2.putString("selected", txview.getText().toString());
                        }
                        ((BaseActivity) PostGoodsFragment.this.getActivity()).pushFragment((BaseFragment) new OtherPropertiesFragment(), bundle2, false);
                    } else if (!postBean.getControlType().equals("checkbox")) {
                    } else {
                        if (postBean.getLabels().size() > 1) {
                            Bundle bundle3 = PostGoodsFragment.this.createArguments(postBean.getDisplayName(), null);
                            bundle3.putInt("reqestCode", postBean.getName().hashCode());
                            bundle3.putBoolean("singleSelection", false);
                            bundle3.putSerializable("properties", (ArrayList) postBean.getLabels());
                            TextView txview2 = (TextView) v.getTag(PostCommonValues.HASH_CONTROL);
                            if (txview2 != null) {
                                bundle3.putString("selected", txview2.getText().toString());
                            }
                            ((BaseActivity) PostGoodsFragment.this.getActivity()).pushFragment((BaseFragment) new OtherPropertiesFragment(), bundle3, false);
                            return;
                        }
                        View checkV = v.findViewById(R.id.checkitem);
                        if (checkV != null && (checkV instanceof CheckBox)) {
                            ((CheckBox) checkV).setChecked(!((CheckBox) checkV).isChecked());
                        }
                    }
                }
            });
        } else {
            final String actionName2 = ((PostGoodsBean) layout.getTag(PostCommonValues.HASH_POST_BEAN)).getDisplayName();
            ((View) layout.getTag(PostCommonValues.HASH_CONTROL)).setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() != 0) {
                        return false;
                    }
                    Tracker.getInstance().event(!PostGoodsFragment.this.editMode ? TrackConfig.TrackMobile.BxEvent.POST_INPUTING : TrackConfig.TrackMobile.BxEvent.EDITPOST_INPUTING).append(TrackConfig.TrackMobile.Key.ACTION, actionName2).end();
                    return false;
                }
            });
            layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    View ctrl = (View) v.getTag(PostCommonValues.HASH_CONTROL);
                    ctrl.requestFocus();
                    ((InputMethodManager) ctrl.getContext().getSystemService("input_method")).showSoftInput(ctrl, 1);
                }
            });
            if (postBean.getName().equals(PostCommonValues.STRING_DETAIL_POSITION)) {
                layout.findViewById(R.id.location).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (PostGoodsFragment.this.detailLocation != null) {
                            ((TextView) layout.findViewById(R.id.postinput)).setText(PostUtil.getLocationSummary(PostGoodsFragment.this.detailLocation));
                        }
                    }
                });
            }
        }
        PostUtil.adjustMarginBottomAndHeight(layout);
        return layout;
    }

    private void setDetailLocationControl(BXLocation location) {
        if (location != null) {
            Button addrBtn = getView() == null ? null : (Button) getView().findViewById(R.id.btn_address);
            if (addrBtn != null) {
                String address = (location.detailAddress == null || location.detailAddress.equals("")) ? (location.subCityName == null || location.subCityName.equals("")) ? "" : location.subCityName : location.detailAddress;
                if (address != null && address.length() != 0) {
                    if (location.adminArea != null && location.adminArea.length() > 0) {
                        address = address.replaceFirst(location.adminArea, "");
                    }
                    if (location.cityName != null && location.cityName.length() > 0) {
                        address = address.replaceFirst(location.cityName, "");
                    }
                    addrBtn.setText(address);
                    setPhoneAndAddrLeftIcon();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public void handlePostFinish(String adId) {
        showProgress("", "正在获取您发布信息的状态，请耐心等候", false);
        ApiParams param = new ApiParams();
        param.addParam("adIds", adId);
        BaseApiCommand.createCommand("ad.ads/", true, param).execute(getActivity(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                PostGoodsFragment.this.sendMessage(15, "");
            }

            public void onNetworkDone(String apiName, String responseData) {
                PostGoodsFragment.this.sendMessage(16, responseData);
            }
        });
        if (ReferralUtil.isPromoter()) {
            Intent postIntent = new Intent();
            postIntent.setAction(CommonIntentAction.ACTION_SENT_POST);
            postIntent.putExtra("adId", adId);
            ReferralPost.getInstance().doAction(postIntent);
        }
    }

    public boolean hasGlobalTab() {
        return false;
    }

    public void onUploadDone(String imagePath, String serverUrl, Bitmap thumbnail) {
        this.handler.sendMessage(this.handler.obtainMessage(14, 2, 0, BXThumbnail.createThumbnail(imagePath, thumbnail)));
    }

    public void onUploading(String imagePath, Bitmap thumbnail) {
        this.handler.sendMessage(this.handler.obtainMessage(14, 1, 0, BXThumbnail.createThumbnail(imagePath, thumbnail)));
    }

    public void onUploadFail(String imagePath, Bitmap thumbnail) {
        this.handler.sendMessage(this.handler.obtainMessage(14, 3, 0, BXThumbnail.createThumbnail(imagePath, thumbnail)));
    }

    /* access modifiers changed from: private */
    public View findImageViewByTag(String imagePath) {
        ViewGroup root = (ViewGroup) getView().findViewById(R.id.image_list_parent);
        if (root == null) {
            return null;
        }
        int c = root.getChildCount();
        for (int i = 0; i < c; i++) {
            View child = root.getChildAt(i);
            if (imagePath.equals(child.getTag())) {
                return child;
            }
        }
        return null;
    }
}
