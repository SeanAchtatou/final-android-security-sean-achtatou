package com.baixing.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.quanleimu.activity.R;

public class AboutUsFragment extends BaseFragment {
    public TextView rlVersion;
    public TextView tvTitle;

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = "关于我们";
        title.m_leftActionHint = "返回";
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout relAbout = (RelativeLayout) inflater.inflate((int) R.layout.aboutus, (ViewGroup) null);
        this.rlVersion = (TextView) relAbout.findViewById(R.id.rlVersion);
        try {
            String pt = (String) GlobalDataManager.getInstance().getApplicationContext().getPackageManager().getApplicationInfo(getActivity().getPackageName(), 128).metaData.get("publishTime");
            this.rlVersion.setText("版本信息：v" + getVersionName() + (pt == null ? "" : " " + pt));
        } catch (Exception e) {
            this.rlVersion.setText("版本信息：v1.01 ");
        }
        return relAbout;
    }

    private String getVersionName() throws Exception {
        return GlobalDataManager.getInstance().getApplicationContext().getPackageManager().getPackageInfo(GlobalDataManager.getInstance().getApplicationContext().getPackageName(), 0).versionName;
    }

    public boolean hasGlobalTab() {
        return false;
    }
}
