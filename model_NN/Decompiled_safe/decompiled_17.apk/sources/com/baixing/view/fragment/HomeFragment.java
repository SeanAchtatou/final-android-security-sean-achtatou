package com.baixing.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.BaseTabActivity;
import com.baixing.activity.HotAppWebActivity;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Category;
import com.baixing.entity.Filterss;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.sharing.referral.ReferralEntrance;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.ViewUtil;
import com.baixing.widget.CustomizeGridView;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class HomeFragment extends BaseFragment implements CustomizeGridView.ItemClickListener {
    public static final int INDEX_DELETED = 2;
    public static final int INDEX_FAVORITE = 3;
    public static final int INDEX_HISTORY = 5;
    public static final int INDEX_LIMITED = 1;
    public static final int INDEX_MESSAGE = 4;
    public static final int INDEX_POSTED = 0;
    public static final int INDEX_SETTING = 6;
    private static final String KEY_HOT_APP = "hotApp";
    public static final int MSG_EDIT_USERNAME_SUCCESS = 100;
    public static final int MSG_GETPERSONALPROFILE = 99;
    public static final int MSG_SHOW_PROGRESS = 102;
    public static final int MSG_SHOW_TOAST = 101;
    public static final int MSG_UPDATE_FILTER = 1000;
    public static final String NAME = "HomeFragment";
    private List<Category> allCates = null;
    public int deletedNum = 0;
    public int favoriteNum = 0;
    /* access modifiers changed from: private */
    public CustomizeGridView gv;
    public int historyNum = 0;
    /* access modifiers changed from: private */
    public String iconUrl = "http://s.baixing.net/img/event/sidebar_qr.png";
    public int limitedNum = 0;
    private List<Filterss> listFilterss;
    /* access modifiers changed from: private */
    public String name = "热门应用";
    public int postNum = 0;
    public int unreadMessageNum = 0;
    /* access modifiers changed from: private */
    public String urlString;

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = "分类查找";
    }

    public int[] includedOptionMenus() {
        return new int[]{0};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void
     arg types: [com.baixing.view.fragment.SecondCateFragment, android.os.Bundle, int]
     candidates:
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, java.lang.String):void
      com.baixing.activity.BaseActivity.pushFragment(com.baixing.activity.BaseFragment, android.os.Bundle, boolean):void */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        if (extras.getBoolean("isInPush", false)) {
            ((BaseTabActivity) getActivity()).pushFragment((BaseFragment) new SecondCateFragment(), extras, false);
            return;
        }
        if (savedInstanceState != null) {
            this.urlString = savedInstanceState.getString(Constants.PARAM_URL);
        }
        this.allCates = GlobalDataManager.getInstance().getFirstLevelCategory();
        if (this.allCates == null || this.allCates.size() == 0) {
            Log.d("QLM", "Reload category");
            GlobalDataManager.getInstance().loadCategorySync();
            this.allCates = GlobalDataManager.getInstance().getFirstLevelCategory();
            if (this.allCates == null || this.allCates.size() == 0) {
                Log.e("QLM", "Failed to reload category");
            }
        }
    }

    public boolean hasGlobalTab() {
        return true;
    }

    private void connectHotApp() {
        ApiParams params1 = new ApiParams();
        params1.addParam("key", KEY_HOT_APP);
        BaseApiCommand.createCommand("Mobile.featureConfig/", true, params1).execute(getActivity(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                Log.d("homefragment", "homefragment net error");
            }

            public void onNetworkDone(String apiName, String responseData) {
                try {
                    Log.d("homefragment", "homefragment in try");
                    JSONObject result = new JSONObject(responseData).getJSONObject("result");
                    if (result.getInt("showHotApp") == 1) {
                        String unused = HomeFragment.this.urlString = result.getString(Constants.PARAM_URL);
                        String unused2 = HomeFragment.this.iconUrl = result.getString("icon");
                        String unused3 = HomeFragment.this.name = result.getString(BaseFragment.ARG_COMMON_TITLE);
                        HomeFragment.this.getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                HomeFragment.this.gv.updateLastView(HomeFragment.this.iconUrl, HomeFragment.this.name);
                            }
                        });
                    }
                } catch (JSONException e) {
                    Log.d("homefragment", "homefragment in exception");
                }
            }
        });
    }

    private void addHotApp(List<CustomizeGridView.GridInfo> gitems) {
        CustomizeGridView.GridInfo gridInfo = new CustomizeGridView.GridInfo();
        gridInfo.img = GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.icon_category_app);
        gridInfo.text = "热门应用";
        gitems.add(gridInfo);
    }

    private void setViewContent() {
        if (getView() != null) {
            int[] icons = {R.drawable.icon_category_wupinjiaoyi, R.drawable.icon_category_car, R.drawable.icon_category_house, R.drawable.icon_category_quanzhi, R.drawable.icon_category_jianzhi, R.drawable.icon_category_vita, R.drawable.icon_category_friend, R.drawable.icon_category_pet, R.drawable.icon_category_service, R.drawable.icon_category_education};
            List<CustomizeGridView.GridInfo> gitems = new ArrayList<>();
            for (int i = 0; i < icons.length; i++) {
                CustomizeGridView.GridInfo gi = new CustomizeGridView.GridInfo();
                gi.img = GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(icons[i]);
                gi.text = this.allCates.get(i).getName();
                gitems.add(gi);
            }
            ReferralEntrance.getInstance().addAppShareGrid(getActivity(), gitems);
            addHotApp(gitems);
            this.gv = (CustomizeGridView) getView().findViewById(R.id.gridcategory);
            this.gv.setData(gitems, 3);
            this.gv.setItemClickListener(this);
            if (this.urlString == null || this.urlString.equals("")) {
                connectHotApp();
            } else {
                this.gv.updateLastView(this.iconUrl, this.name);
            }
            List<String> lastUsedCategories = GlobalDataManager.getInstance().getLastUsedCategory();
            if (lastUsedCategories == null || lastUsedCategories.size() == 0) {
                getView().findViewById(R.id.ll_everUsed).setVisibility(8);
                return;
            }
            String recentNames = "";
            for (String cate : lastUsedCategories) {
                recentNames = recentNames + cate.split(",")[1] + " ";
            }
            final String finalNames = recentNames.trim();
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.RECENTCATEGORY_CHOW).append(TrackConfig.TrackMobile.Key.RECENTCATEGORY_COUNT, lastUsedCategories.size()).append(TrackConfig.TrackMobile.Key.RECENTCATEGORY_NAMES, finalNames).end();
            LinearLayout llCategory = (LinearLayout) getView().findViewById(R.id.ll_categories);
            for (int i2 = 0; i2 < lastUsedCategories.size(); i2++) {
                ((Button) llCategory.getChildAt(i2)).setText(lastUsedCategories.get(i2).split(",")[0]);
                llCategory.getChildAt(i2).setTag(lastUsedCategories.get(i2));
                llCategory.getChildAt(i2).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        String[] category;
                        String value = (String) v.getTag();
                        if (value != null && (category = value.split(",")) != null && category.length == 2) {
                            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.RECENTCATEGORY_CLICK).append(TrackConfig.TrackMobile.Key.RECENTCATEGORY_COUNT, finalNames.split(" ").length).append(TrackConfig.TrackMobile.Key.RECENTCATEGORY_NAMES, finalNames).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, category[1]).end();
                            Bundle bundle = HomeFragment.this.createArguments(category[0], "返回");
                            bundle.putString("categoryEnglishName", category[1]);
                            bundle.putString("siftresult", "");
                            bundle.putString("categoryName", category[0]);
                            HomeFragment.this.pushFragment(new ListingFragment(), bundle);
                        }
                    }
                });
            }
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.PARAM_URL, this.urlString);
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        logCreateView(savedInstanceState);
        View v = inflater.inflate((int) R.layout.homepageview, (ViewGroup) null);
        v.findViewById(R.id.linearLayout1).setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        return v;
    }

    public void onStackTop(boolean isBack) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [?, ?, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 1:
            default:
                return;
            case 2:
                hideProgress();
                return;
            case 4:
                hideProgress();
                ViewUtil.showToast(getActivity(), "网络连接失败，请检查设置！", false);
                return;
            case 101:
                hideProgress();
                ViewUtil.showToast(getActivity(), msg.obj.toString(), false);
                return;
            case 102:
                showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_updating, true);
                return;
            case 10001:
                getView().findViewById(R.id.userInfoLayout).setVisibility(0);
                return;
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onResume() {
        PerformanceTracker.stamp(PerformEvent.Event.E_HomeFragment_Showup);
        super.onResume();
        synchronized (this) {
            setViewContent();
        }
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.HOME).end();
        PerformanceTracker.stamp(PerformEvent.Event.E_HomeFragment_Showup_done);
        PerformanceTracker.flush();
    }

    public void onPause() {
        super.onPause();
    }

    public void onItemClick(CustomizeGridView.GridInfo info, int index) {
        if (this.allCates != null && this.allCates.size() != 0) {
            if (index > this.allCates.size()) {
                Log.d("homefragment", "homefragment onitemclick");
                Intent intent = new Intent();
                intent.putExtra(Constants.PARAM_URL, this.urlString);
                intent.setClass(getActivity(), HotAppWebActivity.class);
                startActivity(intent);
            } else if (index >= this.allCates.size()) {
                ReferralEntrance.getInstance().pushFragment(this, info);
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt("reqestCode", this.fragmentRequestCode);
                bundle.putSerializable("cates", this.allCates.get(index));
                bundle.putBoolean("isPost", false);
                pushFragment(new SecondCateFragment(), bundle);
            }
        }
    }

    public int getEnterAnimation() {
        return 0;
    }

    public int getExitAnimation() {
        return 0;
    }
}
