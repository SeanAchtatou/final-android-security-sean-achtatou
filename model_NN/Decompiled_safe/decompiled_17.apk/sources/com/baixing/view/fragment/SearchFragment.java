package com.baixing.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Category;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.NetworkUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import java.util.List;

public class SearchFragment extends BaseFragment implements BaseApiCommand.Callback {
    private static final int MSG_SEARCH_RESULT = 1;
    private static final int MSG_START_SERACH = 2;
    private static final int NETWOTK_REQ_SEARACH_CAT = 100;
    private static final int REQ_GETKEYWORD = 1;
    private LinearLayout.LayoutParams FFlayoutParams = new LinearLayout.LayoutParams(-1, -1);
    private LinearLayout.LayoutParams WClayoutParams = new LinearLayout.LayoutParams(-2, -2);
    List<Pair<Category, Integer>> categoryResultCountList;
    /* access modifiers changed from: private */
    public View loadingView;
    private ListView lvSearchResultList;
    private View noResultView;
    private Runnable searachJob;
    public String searchContent = "";
    private View searchResult;

    /* access modifiers changed from: protected */
    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = this.searchContent == null ? "" : this.searchContent;
        title.m_leftActionHint = "返回";
        title.hasGlobalSearch = true;
    }

    public SearchFragment() {
        this.defaultEnterAnim = R.anim.zoom_enter;
        this.defaultExitAnim = R.anim.zoom_exit;
    }

    public void handleSearch() {
        Bundle args = createArguments(null, null);
        args.putInt("reqestCode", 1);
        pushFragment(new KeywordSelectFragment(), args);
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootV = inflater.inflate((int) R.layout.search, (ViewGroup) null);
        rootV.setLayoutParams(this.FFlayoutParams);
        this.lvSearchResultList = (ListView) rootV.findViewById(R.id.lvSearchResultList);
        this.searchResult = rootV.findViewById(R.id.searchResult);
        this.noResultView = rootV.findViewById(R.id.noResultView);
        this.loadingView = rootV.findViewById(R.id.searching_parent);
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(0);
        layout.setGravity(17);
        LinearLayout loadingLayout = new LinearLayout(getActivity());
        loadingLayout.addView(layout, this.WClayoutParams);
        loadingLayout.setGravity(17);
        this.lvSearchResultList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                final Bundle bundle = new Bundle();
                bundle.putString("searchContent", SearchFragment.this.searchContent);
                bundle.putString("actType", "search");
                bundle.putString(BaseFragment.ARG_COMMON_TITLE, "");
                bundle.putString("categoryEnglishName", ((Category) arg1.getTag()).getEnglishName());
                if (GlobalDataManager.isTextMode() || !GlobalDataManager.needNotifySwitchMode() || NetworkUtil.isWifiConnection(arg0.getContext())) {
                    SearchFragment.this.pushFragment(new ListingFragment(), bundle);
                } else {
                    new AlertDialog.Builder(SearchFragment.this.getActivity()).setTitle((int) R.string.dialog_title_info).setMessage((int) R.string.label_warning_flow_optimize).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GlobalDataManager.setTextMode(false);
                            SearchFragment.this.pushFragment(new ListingFragment(), bundle);
                        }
                    }).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GlobalDataManager.setTextMode(true);
                            ViewUtil.postShortToastMessage(SearchFragment.this.getView(), (int) R.string.label_warning_switch_succed, 100);
                            SearchFragment.this.pushFragment(new ListingFragment(), bundle);
                        }
                    }).create().show();
                }
            }
        });
        return rootV;
    }

    public void onStackTop(boolean isBack) {
        if (isBack) {
            showSearchResult(false);
        } else if (this.searchContent == null || this.searchContent.length() == 0) {
            handleSearch();
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int requestCode, Object result) {
        if (requestCode != 1) {
            return;
        }
        if (result == null && (this.categoryResultCountList == null || this.categoryResultCountList.size() == 0)) {
            finishFragment();
        } else if (result == null) {
            showSearchResult(false);
        } else {
            this.categoryResultCountList = null;
            this.searchContent = (String) result;
            getTitleDef().m_title = this.searchContent;
            refreshHeader();
            showSearchResult(true);
        }
    }

    private void showSearchResult(boolean search) {
        this.pv = TrackConfig.TrackMobile.PV.SEARCHRESULTCATEGORY;
        hideSoftKeyboard();
        if (search) {
            sendMessage(2, null);
            searchCategory();
            return;
        }
        this.loadingView.setVisibility(8);
        sendMessage(1, null);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 1:
                int totalAdsCount = 0;
                int maxAdsCount = 0;
                this.loadingView.setVisibility(8);
                if (this.categoryResultCountList == null || this.categoryResultCountList.isEmpty()) {
                    this.noResultView.setVisibility(0);
                    this.searchResult.setVisibility(8);
                } else {
                    this.searchResult.setVisibility(0);
                    this.noResultView.setVisibility(8);
                    this.lvSearchResultList.setAdapter((ListAdapter) new ResultListAdapter(activity, R.layout.item_categorysearch, this.categoryResultCountList));
                    int resultCatesCount = this.categoryResultCountList.size();
                    for (Pair<Category, Integer> pair : this.categoryResultCountList) {
                        totalAdsCount += ((Integer) pair.second).intValue();
                        maxAdsCount = Math.max(maxAdsCount, ((Integer) pair.second).intValue());
                    }
                    Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.SEARCHRESULTCATEGORY).append(TrackConfig.TrackMobile.Key.SEARCHKEYWORD, this.searchContent).end();
                }
                hideProgress();
                return;
            case 2:
                this.noResultView.setVisibility(8);
                this.loadingView.setVisibility(0);
                return;
            default:
                return;
        }
    }

    private void searchCategory() {
        ApiParams params = new ApiParams();
        params.addParam("keywords", this.searchContent);
        params.addParam("cityId", GlobalDataManager.getInstance().getCityId());
        BaseApiCommand.createCommand("Ad.search/", true, params).execute(getActivity(), this);
    }

    class ResultListAdapter extends ArrayAdapter<Pair<Category, Integer>> {
        List<Pair<Category, Integer>> objects;

        public ResultListAdapter(Context context, int textViewResourceId, List<Pair<Category, Integer>> objects2) {
            super(context, textViewResourceId, objects2);
            this.objects = objects2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(SearchFragment.this.getActivity()).inflate((int) R.layout.item_categorysearch, (ViewGroup) null);
            }
            Pair<Category, Integer> pair = this.objects.get(position);
            convertView.setTag(pair.first);
            ((TextView) convertView.findViewById(R.id.tvCategoryName)).setText(((Category) pair.first).getName());
            ((TextView) convertView.findViewById(R.id.tvCategoryCount)).setText(String.format("(%d)", Integer.valueOf(((Integer) pair.second).intValue())));
            return convertView;
        }
    }

    public int getEnterAnimation() {
        return getArguments() == null ? R.anim.zoom_enter : getArguments().getInt(BaseFragment.ARG_COMMON_ANIMATION_IN, R.anim.zoom_enter);
    }

    public int getExitAnimation() {
        return getArguments() == null ? R.anim.zoom_exit : getArguments().getInt(BaseFragment.ARG_COMMON_ANIMATION_EXIT, R.anim.zoom_exit);
    }

    private void handleCategoryResult(String responseData) {
        this.categoryResultCountList = JsonUtil.parseAdSearchCategoryCountResult(responseData);
        sendMessage(1, this.categoryResultCountList);
    }

    private void handleCategorySearchFail(ApiError error) {
        this.categoryResultCountList = null;
        this.loadingView.post(new Runnable() {
            public void run() {
                if (SearchFragment.this.loadingView != null) {
                    SearchFragment.this.loadingView.setVisibility(8);
                }
                if (SearchFragment.this.loadingView.getContext() != null) {
                    ViewUtil.showToast(SearchFragment.this.loadingView.getContext(), "网络请求失败,请稍后重试", false);
                }
            }
        });
    }

    public void onNetworkDone(String apiName, String responseData) {
        handleCategoryResult(responseData);
    }

    public void onNetworkFail(String apiName, ApiError error) {
        handleCategorySearchFail(error);
    }

    public void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle.getBoolean("HomePage")) {
            String search = bundle.getString("SearchContent");
            if (search == null && (this.categoryResultCountList == null || this.categoryResultCountList.size() == 0)) {
                finishFragment();
            } else if (search == null) {
                showSearchResult(false);
            } else {
                this.categoryResultCountList = null;
                this.searchContent = search;
                getTitleDef().m_title = this.searchContent;
                refreshHeader();
                showSearchResult(true);
            }
        }
        super.onCreate(savedInstanceState);
    }
}
