package com.baixing.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.BXLocation;
import com.baixing.util.ViewUtil;
import com.baixing.util.post.PostUtil;
import com.quanleimu.activity.R;

public class ContactAndAddressDetailFragment extends BaseFragment {
    public static final int MSG_RET_CODE = -61184;
    private boolean isContact = false;

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_leftActionHint = "返回";
        title.m_rightActionHint = "完成";
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(BaseFragment.ARG_COMMON_TITLE)) {
            title.m_title = bundle.getString(BaseFragment.ARG_COMMON_TITLE);
        }
    }

    public void onResume() {
        super.onResume();
        final View input = this.isContact ? getView().findViewById(R.id.contact_edit) : getView().findViewById(R.id.postinput);
        if (input != null) {
            input.postDelayed(new Runnable() {
                public void run() {
                    input.requestFocus();
                    ((InputMethodManager) ContactAndAddressDetailFragment.this.getActivity().getSystemService("input_method")).showSoftInput(input, 1);
                }
            }, 100);
        }
    }

    /* access modifiers changed from: protected */
    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final LinearLayout llEdit = (LinearLayout) inflater.inflate((int) R.layout.post_contact_addr_edit, (ViewGroup) null);
        final Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("edittype")) {
            if (bundle.getString("edittype").equals("contact")) {
                this.isContact = true;
                llEdit.findViewById(R.id.edit_post_location).setVisibility(8);
                String value = GlobalDataManager.getInstance().getPhoneNumber();
                if (value == null || (value.length() == 0 && bundle.containsKey("defaultValue"))) {
                    value = bundle.getString("defaultValue");
                }
                ((TextView) llEdit.findViewById(R.id.contact_edit)).setText(value);
            } else if (bundle.getString("edittype").equals("address")) {
                this.isContact = false;
                llEdit.findViewById(R.id.ll_contact).setVisibility(8);
                String adr = GlobalDataManager.getInstance().getAddress();
                if ((adr == null || adr.length() == 0) && bundle.containsKey("defaultValue")) {
                    adr = bundle.getString("defaultValue");
                }
                ((TextView) llEdit.findViewById(R.id.postinput)).setText(adr);
                llEdit.findViewById(R.id.location).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (bundle.containsKey("location")) {
                            ((TextView) llEdit.findViewById(R.id.postinput)).setText(PostUtil.getLocationSummary((BXLocation) bundle.getSerializable("location")));
                            return;
                        }
                        llEdit.postDelayed(new Runnable() {
                            public void run() {
                                ViewUtil.showToast(ContactAndAddressDetailFragment.this.getActivity(), "无法获得当前位置", false);
                            }
                        }, 0);
                    }
                });
            }
        }
        return llEdit;
    }

    private boolean saveContent(boolean finish) {
        if (this.isContact) {
            String text = ((TextView) getView().findViewById(R.id.contact_edit)).getText().toString();
            if (text == null || text.length() == 0 || text.trim().length() == 0) {
                if (finish) {
                    ViewUtil.showToast(getAppContext(), "联系方式不能为空", false);
                }
                return false;
            }
            GlobalDataManager.getInstance().setPhoneNumber(((TextView) getView().findViewById(R.id.contact_edit)).getText().toString());
        } else {
            String text2 = ((TextView) getView().findViewById(R.id.postinput)).getText().toString();
            if (text2 == null || text2.length() == 0) {
                if (finish) {
                    ViewUtil.showToast(getAppContext(), "地址不能为空", false);
                }
                return false;
            }
            GlobalDataManager.getInstance().setAddress(((TextView) getView().findViewById(R.id.postinput)).getText().toString());
        }
        return true;
    }

    public boolean handleBack() {
        saveContent(false);
        return false;
    }

    public void handleRightAction() {
        if (saveContent(true)) {
            finishFragment();
        }
    }
}
