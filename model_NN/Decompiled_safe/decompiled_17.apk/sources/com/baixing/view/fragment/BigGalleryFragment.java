package com.baixing.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.TextUtil;
import com.baixing.util.ViewUtil;
import com.baixing.widget.ViewFlow;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class BigGalleryFragment extends BaseFragment implements ViewFlow.ViewSwitchListener, MediaScannerConnection.MediaScannerConnectionClient {
    static int MSG_GALLERY_BACK = -65535;
    private static final int MSG_HIDE_TITLE = 1;
    private static final int MSG_SHOW_TITLE = 2;
    private static String mediaPath = (Environment.getExternalStorageDirectory().getPath() + "/quanleimu/favorites/百姓网收藏图片/");
    private boolean exit = false;
    private Ad goodsDetail;
    private List<String> listUrl = new ArrayList();
    private int postIndex = -1;
    private MediaScannerConnection scannerConnection = null;
    private Vector<String> unScannedFiles = new Vector<>();

    public void onPause() {
        super.onPause();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        this.postIndex = bundle.getInt("postIndex");
        this.goodsDetail = (Ad) bundle.getSerializable("goodsDetail");
    }

    public boolean handleBack() {
        this.exit = true;
        finishFragment(MSG_GALLERY_BACK, null);
        return true;
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate((int) R.layout.biggallery, (ViewGroup) null);
        try {
            if (this.goodsDetail.getImageList().getBig() == null || this.goodsDetail.getImageList().getBig().equals("")) {
                getTitleDef().m_title = "0/0";
                ViewUtil.showToast(getActivity(), getString(R.string.dialog_message_image_load_error), false);
                return v;
            }
            String b = TextUtil.filterString(this.goodsDetail.getImageList().getBig(), new char[]{'\\', '\"'});
            if (b.contains(",")) {
                String[] c = b.split(",");
                for (String add : c) {
                    this.listUrl.add(add);
                }
            } else {
                this.listUrl.add(b);
            }
            new BitmapFactory.Options().inPurgeable = true;
            ViewFlow vfCoupon = (ViewFlow) v.findViewById(R.id.vfCoupon);
            GalleryImageAdapter adapter = new GalleryImageAdapter(getActivity(), this.listUrl);
            vfCoupon.setOnViewLazyInitializeListener(adapter);
            vfCoupon.setOnViewSwitchListener(this);
            vfCoupon.setAdapter(adapter, this.postIndex);
            return v;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = (this.postIndex + 1) + CookieSpec.PATH_DELIM + this.listUrl.size();
        title.m_leftActionHint = "返回";
        title.m_rightActionHint = "保存";
    }

    public void onViewDestory(View rootView) {
        ((ViewFlow) rootView.findViewById(R.id.vfCoupon)).finalize();
        GlobalDataManager.getInstance().getImageLoaderMgr().Cancel(this.listUrl);
        if (this.listUrl != null) {
            for (int i = 0; i < this.listUrl.size(); i++) {
                String url = this.listUrl.get(i);
                if (url != null && !url.equals("")) {
                    GlobalDataManager.getInstance().getImageManager().forceRecycle(url, true);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 1:
                getTitleDef().m_visible = false;
                refreshHeader();
                return;
            case 2:
                getTitleDef().m_visible = true;
                refreshHeader();
                sendMessageDelay(1, null, 5000);
                return;
            default:
                return;
        }
    }

    public void onDestroy() {
        this.goodsDetail = null;
        this.listUrl = null;
        super.onDestroy();
    }

    public void onResume() {
        super.onResume();
        this.pv = TrackConfig.TrackMobile.PV.VIEWADPIC;
        Tracker.getInstance().pv(this.pv).append(TrackConfig.TrackMobile.Key.ADID, this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.goodsDetail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).end();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0201 A[SYNTHETIC, Splitter:B:58:0x0201] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0206 A[SYNTHETIC, Splitter:B:61:0x0206] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleRightAction() {
        /*
            r21 = this;
            r14 = 0
            r0 = r21
            java.util.List<java.lang.String> r0 = r0.listUrl
            r18 = r0
            if (r18 == 0) goto L_0x0039
            r0 = r21
            int r0 = r0.postIndex
            r18 = r0
            if (r18 < 0) goto L_0x0039
            r0 = r21
            java.util.List<java.lang.String> r0 = r0.listUrl
            r18 = r0
            int r18 = r18.size()
            r0 = r21
            int r0 = r0.postIndex
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 <= r1) goto L_0x0039
            r0 = r21
            java.util.List<java.lang.String> r0 = r0.listUrl
            r18 = r0
            r0 = r21
            int r0 = r0.postIndex
            r19 = r0
            java.lang.Object r14 = r18.get(r19)
            java.lang.String r14 = (java.lang.String) r14
        L_0x0039:
            if (r14 != 0) goto L_0x003c
        L_0x003b:
            return
        L_0x003c:
            com.baixing.data.GlobalDataManager r18 = com.baixing.data.GlobalDataManager.getInstance()
            com.baixing.imageCache.ImageCacheManager r18 = r18.getImageManager()
            r0 = r18
            java.lang.String r6 = r0.getFileInDiskCache(r14)
            if (r6 == 0) goto L_0x003b
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            r0 = r21
            com.baixing.entity.Ad r0 = r0.goodsDetail
            r19 = r0
            com.baixing.entity.Ad$EDATAKEYS r20 = com.baixing.entity.Ad.EDATAKEYS.EDATAKEYS_TITLE
            java.lang.String r19 = r19.getValueByKey(r20)
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r21
            int r0 = r0.postIndex
            r19 = r0
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r15 = r18.toString()
            java.lang.String r18 = "/"
            r0 = r18
            int r9 = r6.lastIndexOf(r0)
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            int r19 = r9 + 1
            r0 = r19
            java.lang.String r19 = r6.substring(r0)
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = ".png"
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r5 = r18.toString()
            android.content.ContentValues r17 = new android.content.ContentValues
            r18 = 8
            r17.<init>(r18)
            java.text.SimpleDateFormat r18 = new java.text.SimpleDateFormat
            java.lang.String r19 = "MM月dd日 HH:mm:ss"
            java.util.Locale r20 = java.util.Locale.SIMPLIFIED_CHINESE
            r18.<init>(r19, r20)
            long r19 = java.lang.System.currentTimeMillis()
            java.lang.Long r19 = java.lang.Long.valueOf(r19)
            java.lang.String r18 = r18.format(r19)
            java.lang.String r12 = r18.toString()
            java.lang.String r18 = "title"
            r0 = r17
            r1 = r18
            r0.put(r1, r12)
            java.lang.String r18 = "_display_name"
            r0 = r17
            r1 = r18
            r0.put(r1, r12)
            java.lang.String r18 = "description"
            r0 = r17
            r1 = r18
            r0.put(r1, r15)
            java.lang.String r18 = "datetaken"
            long r19 = java.lang.System.currentTimeMillis()
            java.lang.Long r19 = java.lang.Long.valueOf(r19)
            r17.put(r18, r19)
            java.lang.String r18 = "mime_type"
            java.lang.String r19 = "image/png"
            r17.put(r18, r19)
            java.lang.String r18 = "orientation"
            r19 = 0
            java.lang.Integer r19 = java.lang.Integer.valueOf(r19)
            r17.put(r18, r19)
            java.lang.String r18 = "isprivate"
            r19 = 1
            java.lang.Integer r19 = java.lang.Integer.valueOf(r19)
            r17.put(r18, r19)
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = com.baixing.view.fragment.BigGalleryFragment.mediaPath
            java.lang.StringBuilder r18 = r18.append(r19)
            int r19 = r5.hashCode()
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = ".png"
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r10 = r18.toString()
            java.lang.String r18 = "_data"
            r0 = r17
            r1 = r18
            r0.put(r1, r10)
            r16 = 0
            android.support.v4.app.FragmentActivity r18 = r21.getActivity()     // Catch:{ Exception -> 0x018a }
            android.content.ContentResolver r18 = r18.getContentResolver()     // Catch:{ Exception -> 0x018a }
            android.net.Uri r19 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI     // Catch:{ Exception -> 0x018a }
            r0 = r18
            r1 = r19
            r2 = r17
            android.net.Uri r16 = r0.insert(r1, r2)     // Catch:{ Exception -> 0x018a }
            r13 = 0
            r7 = 0
            android.support.v4.app.FragmentActivity r18 = r21.getActivity()     // Catch:{ Exception -> 0x0228 }
            android.content.ContentResolver r18 = r18.getContentResolver()     // Catch:{ Exception -> 0x0228 }
            r0 = r18
            r1 = r16
            java.io.OutputStream r13 = r0.openOutputStream(r1)     // Catch:{ Exception -> 0x0228 }
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0228 }
            java.io.File r18 = new java.io.File     // Catch:{ Exception -> 0x0228 }
            r0 = r18
            r0.<init>(r6)     // Catch:{ Exception -> 0x0228 }
            r0 = r18
            r8.<init>(r0)     // Catch:{ Exception -> 0x0228 }
            r18 = 1024(0x400, float:1.435E-42)
            r0 = r18
            byte[] r3 = new byte[r0]     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r11 = 0
        L_0x015a:
            int r11 = r8.read(r3)     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            if (r11 <= 0) goto L_0x0198
            r18 = 0
            r0 = r18
            r13.write(r3, r0, r11)     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            goto L_0x015a
        L_0x0168:
            r4 = move-exception
            r7 = r8
        L_0x016a:
            r4.printStackTrace()     // Catch:{ all -> 0x0226 }
            android.support.v4.app.FragmentActivity r18 = r21.getActivity()     // Catch:{ all -> 0x0226 }
            java.lang.String r19 = "保存失败,请检查SD卡是否可用>_<"
            r20 = 1
            com.baixing.util.ViewUtil.showToast(r18, r19, r20)     // Catch:{ all -> 0x0226 }
            if (r13 == 0) goto L_0x017d
            r13.close()     // Catch:{ IOException -> 0x0216 }
        L_0x017d:
            if (r7 == 0) goto L_0x003b
            r7.close()     // Catch:{ IOException -> 0x0184 }
            goto L_0x003b
        L_0x0184:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x003b
        L_0x018a:
            r4 = move-exception
            android.support.v4.app.FragmentActivity r18 = r21.getActivity()
            java.lang.String r19 = "保存失败,请检查SD卡是否可用>_<"
            r20 = 1
            com.baixing.util.ViewUtil.showToast(r18, r19, r20)
            goto L_0x003b
        L_0x0198:
            r0 = r21
            android.media.MediaScannerConnection r0 = r0.scannerConnection     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r18 = r0
            if (r18 != 0) goto L_0x01b5
            android.media.MediaScannerConnection r18 = new android.media.MediaScannerConnection     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            android.support.v4.app.FragmentActivity r19 = r21.getActivity()     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r0 = r18
            r1 = r19
            r2 = r21
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r0 = r18
            r1 = r21
            r1.scannerConnection = r0     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
        L_0x01b5:
            r0 = r21
            android.media.MediaScannerConnection r0 = r0.scannerConnection     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r18 = r0
            boolean r18 = r18.isConnected()     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            if (r18 != 0) goto L_0x01ed
            r0 = r21
            java.util.Vector<java.lang.String> r0 = r0.unScannedFiles     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r18 = r0
            r0 = r18
            r0.add(r10)     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r0 = r21
            android.media.MediaScannerConnection r0 = r0.scannerConnection     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r18 = r0
            r18.connect()     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
        L_0x01d5:
            android.support.v4.app.FragmentActivity r18 = r21.getActivity()     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            java.lang.String r19 = "图片已保存到相册"
            r20 = 1
            com.baixing.util.ViewUtil.showToast(r18, r19, r20)     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            if (r13 == 0) goto L_0x01e5
            r13.close()     // Catch:{ IOException -> 0x020a }
        L_0x01e5:
            if (r8 == 0) goto L_0x022b
            r8.close()     // Catch:{ IOException -> 0x020f }
            r7 = r8
            goto L_0x003b
        L_0x01ed:
            r0 = r21
            android.media.MediaScannerConnection r0 = r0.scannerConnection     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            r18 = r0
            java.lang.String r19 = "image/png"
            r0 = r18
            r1 = r19
            r0.scanFile(r10, r1)     // Catch:{ Exception -> 0x0168, all -> 0x01fd }
            goto L_0x01d5
        L_0x01fd:
            r18 = move-exception
            r7 = r8
        L_0x01ff:
            if (r13 == 0) goto L_0x0204
            r13.close()     // Catch:{ IOException -> 0x021c }
        L_0x0204:
            if (r7 == 0) goto L_0x0209
            r7.close()     // Catch:{ IOException -> 0x0221 }
        L_0x0209:
            throw r18
        L_0x020a:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x01e5
        L_0x020f:
            r4 = move-exception
            r4.printStackTrace()
            r7 = r8
            goto L_0x003b
        L_0x0216:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x017d
        L_0x021c:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0204
        L_0x0221:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0209
        L_0x0226:
            r18 = move-exception
            goto L_0x01ff
        L_0x0228:
            r4 = move-exception
            goto L_0x016a
        L_0x022b:
            r7 = r8
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baixing.view.fragment.BigGalleryFragment.handleRightAction():void");
    }

    private class GalleryImageAdapter extends BaseAdapter implements ViewFlow.ViewLazyInitializeListener {
        private Bitmap failBk;
        private List<String> imageUrls;
        private int position;

        private GalleryImageAdapter(Context c, List<String> imageUrls2) {
            this.position = 0;
            this.imageUrls = imageUrls2;
        }

        public int getCount() {
            return this.imageUrls.size();
        }

        public Object getItem(int position2) {
            return Integer.valueOf(this.position);
        }

        public long getItemId(int position2) {
            return (long) this.position;
        }

        public View getView(int position2, View convertView, ViewGroup parent) {
            View view = null;
            ViewGroup itemRoot = (ViewGroup) convertView;
            if (itemRoot == null) {
                if (BigGalleryFragment.this.getActivity() == null) {
                    return null;
                }
                itemRoot = (ViewGroup) LayoutInflater.from(BigGalleryFragment.this.getActivity()).inflate((int) R.layout.item_big_gallery, (ViewGroup) null);
            }
            if (itemRoot != null) {
                view = itemRoot.findViewById(R.id.real_img);
            }
            ImageView imageView = (ImageView) view;
            itemRoot.setLayoutParams(new Gallery.LayoutParams(-2, -1));
            if (imageView.getTag() == null || !imageView.getTag().equals(this.imageUrls.get(position2))) {
                if (this.failBk == null) {
                    this.failBk = GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.home_bg_thumb_2x);
                }
                imageView.setTag(this.imageUrls.get(position2));
                GlobalDataManager.getInstance().getImageLoaderMgr().showImg(imageView, this.imageUrls.get(position2), (String) imageView.getTag(), BigGalleryFragment.this.getAppContext(), new WeakReference(this.failBk));
            }
            return itemRoot;
        }

        public void onViewLazyInitialize(View view, int position2) {
        }

        public void onViewRecycled(View view) {
        }
    }

    public void onSwitched(View view, int position) {
        if (this.listUrl != null) {
            this.postIndex = position;
            if (!this.exit) {
                getTitleDef().m_title = (position + 1) + CookieSpec.PATH_DELIM + this.listUrl.size();
                refreshHeader();
                ArrayList<String> urls = new ArrayList<>();
                urls.add(this.listUrl.get(position));
                int index = 0;
                while (true) {
                    if (index + position < this.listUrl.size() || position - index >= 0) {
                        if (index + position < this.listUrl.size()) {
                            urls.add(this.listUrl.get(index + position));
                        }
                        if (position - index >= 0) {
                            urls.add(this.listUrl.get(position - index));
                        }
                        index++;
                    } else {
                        GlobalDataManager.getInstance().getImageLoaderMgr().AdjustPriority(urls);
                        return;
                    }
                }
            }
        }
    }

    public void onMediaScannerConnected() {
        while (this.unScannedFiles.size() > 0) {
            String mediaFileName = this.unScannedFiles.remove(0);
            if (mediaFileName != null && mediaFileName.length() > 0) {
                this.scannerConnection.scanFile(mediaFileName, "image/png");
            }
        }
    }

    public void onScanCompleted(String path, Uri uri) {
    }

    public boolean hasGlobalTab() {
        return false;
    }
}
