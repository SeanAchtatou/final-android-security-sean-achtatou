package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.baixing.activity.BaseFragment;
import com.baixing.adapter.BXAlphabetSortableAdapter;
import com.baixing.adapter.CheckableAdapter;
import com.baixing.adapter.CommonItemAdapter;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.PostGoodsBean;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.quanleimu.activity.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MultiLevelSelectionFragment extends BaseFragment {
    private static final String[] munisipalities = {"北京", "上海", "重庆", "天津"};
    private final int MESSAGE_GET_METAOBJ = 1;
    ListAdapter adapter = null;
    /* access modifiers changed from: private */
    public String id = null;
    /* access modifiers changed from: private */
    public List<MultiLevelItem> items = null;
    /* access modifiers changed from: private */
    public String json = null;
    private ListView listView = null;
    /* access modifiers changed from: private */
    public int remainLevel = 0;
    /* access modifiers changed from: private */
    public String selectedValue = null;
    /* access modifiers changed from: private */
    public String title = "请选择";

    public static class MultiLevelItem implements Serializable {
        private static final long serialVersionUID = 6707309053604383782L;
        public String id;
        public String txt;

        public String toString() {
            return this.txt;
        }
    }

    public void initTitle(BaseFragment.TitleDef title2) {
        title2.m_visible = true;
        title2.m_title = this.title;
        title2.m_leftActionHint = "返回";
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [?, ?, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    public void onStackTop(boolean isBack) {
        boolean z = true;
        if (this.items == null || this.items.size() == 0) {
            showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_waiting, true);
            new Thread(new GetMetaDataThread(this.id)).start();
            return;
        }
        if (this.remainLevel <= 0) {
            z = false;
        }
        initContent(z);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle.containsKey(BaseFragment.ARG_COMMON_TITLE)) {
            this.title = bundle.getString(BaseFragment.ARG_COMMON_TITLE);
        }
        if (bundle.containsKey("selectedValue")) {
            this.selectedValue = bundle.getString("selectedValue");
        }
        if (bundle.containsKey("items")) {
            this.items = (List) bundle.getSerializable("items");
        }
        this.remainLevel = bundle.getInt("maxLevel");
        if (bundle.containsKey("metaId")) {
            this.id = bundle.getString("metaId");
        }
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate((int) R.layout.post_othersview, (ViewGroup) null);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        LinkedHashMap<String, PostGoodsBean> beans;
        switch (msg.what) {
            case 1:
                hideProgress();
                if (this.json != null && (beans = JsonUtil.getPostGoodsBean(this.json)) != null) {
                    PostGoodsBean bean = beans.get((String) beans.keySet().toArray()[0]);
                    if (this.items == null || this.items.size() == 0) {
                        this.items = new ArrayList();
                        if (bean.getLabels() != null) {
                            if (bean.getLabels().size() > 1) {
                                MultiLevelItem tAll = new MultiLevelItem();
                                tAll.txt = "全部";
                                tAll.id = null;
                                this.items.add(tAll);
                            }
                            for (int i = 0; i < bean.getLabels().size(); i++) {
                                MultiLevelItem t = new MultiLevelItem();
                                t.txt = bean.getLabels().get(i);
                                t.id = bean.getValues().get(i);
                                this.items.add(t);
                            }
                        } else {
                            MultiLevelItem nItem = new MultiLevelItem();
                            nItem.id = this.id;
                            nItem.txt = this.title;
                            finishFragment(this.fragmentRequestCode, nItem);
                            return;
                        }
                    }
                    initContent(this.remainLevel > 0);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public boolean isMunisipality(String name) {
        for (String m : munisipalities) {
            if (m.equals(name)) {
                return true;
            }
        }
        return false;
    }

    private void initContent(final boolean hasNextLevel) {
        ListView lv = (ListView) getView().findViewById(R.id.post_other_list);
        if (lv != null) {
            this.listView = lv;
        }
        lv.setDivider(null);
        final ArrayList<CheckableAdapter.CheckableItem> checkList = new ArrayList<>();
        if (!hasNextLevel) {
            for (int i = 0; i < this.items.size(); i++) {
                CheckableAdapter.CheckableItem t = new CheckableAdapter.CheckableItem();
                t.txt = this.items.get(i).txt;
                t.id = this.items.get(i).id;
                String itemId = t.id != null ? t.id : this.id;
                t.checked = itemId != null ? itemId.equals(this.selectedValue) : false;
                checkList.add(t);
            }
            this.adapter = new CheckableAdapter(getActivity(), checkList, 10, true);
        } else {
            this.adapter = new CommonItemAdapter(getActivity(), this.items, 10, true);
        }
        lv.setAdapter(this.adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long id) {
                boolean z = true;
                Object item = MultiLevelSelectionFragment.this.adapter.getItem(position);
                if (!(MultiLevelSelectionFragment.this.adapter.getItem(position) instanceof BXAlphabetSortableAdapter.BXHeader)) {
                    if (position == 0 && MultiLevelSelectionFragment.this.items.size() > 10 && !(MultiLevelSelectionFragment.this.adapter.getItem(0) instanceof BXAlphabetSortableAdapter.BXHeader) && !(MultiLevelSelectionFragment.this.adapter.getItem(0) instanceof BXAlphabetSortableAdapter.BXPinyinSortItem)) {
                        Bundle bundle = MultiLevelSelectionFragment.this.createArguments(null, null);
                        bundle.putBoolean("hasNextLevel", hasNextLevel);
                        bundle.putSerializable("selections", hasNextLevel ? (ArrayList) MultiLevelSelectionFragment.this.items : checkList);
                        MultiLevelSelectionFragment.this.pushFragment(new SelectionSearchFragment(), bundle);
                    } else if ((position == 1 || position == 0 || position == 2) && MultiLevelSelectionFragment.this.adapter.getItem(position).toString().equals("全部")) {
                        MultiLevelItem nItem = new MultiLevelItem();
                        nItem.id = MultiLevelSelectionFragment.this.id;
                        nItem.txt = (nItem.id == null || nItem.id.equals("")) ? "全部" : MultiLevelSelectionFragment.this.title;
                        MultiLevelSelectionFragment.this.finishFragment(MultiLevelSelectionFragment.this.fragmentRequestCode, nItem);
                    } else if (hasNextLevel) {
                        MultiLevelItem item2 = MultiLevelSelectionFragment.this.adapter.getItem(position) instanceof BXAlphabetSortableAdapter.BXPinyinSortItem ? (MultiLevelItem) ((BXAlphabetSortableAdapter.BXPinyinSortItem) MultiLevelSelectionFragment.this.adapter.getItem(position)).obj : (MultiLevelItem) MultiLevelSelectionFragment.this.adapter.getItem(position);
                        if (MultiLevelSelectionFragment.this.isMunisipality(item2.txt)) {
                            MultiLevelSelectionFragment.this.finishFragment(MultiLevelSelectionFragment.this.fragmentRequestCode, item2);
                            return;
                        }
                        Bundle bundle2 = MultiLevelSelectionFragment.this.createArguments(item2.txt, null);
                        bundle2.putInt("reqestCode", MultiLevelSelectionFragment.this.fragmentRequestCode);
                        bundle2.putInt("maxLevel", MultiLevelSelectionFragment.this.remainLevel - 1);
                        bundle2.putString("metaId", item2.id);
                        bundle2.putString("selectedValue", MultiLevelSelectionFragment.this.selectedValue);
                        MultiLevelSelectionFragment.this.pushFragment(new MultiLevelSelectionFragment(), bundle2);
                    } else {
                        CheckableAdapter.CheckableItem item3 = MultiLevelSelectionFragment.this.adapter.getItem(position) instanceof BXAlphabetSortableAdapter.BXPinyinSortItem ? (CheckableAdapter.CheckableItem) ((BXAlphabetSortableAdapter.BXPinyinSortItem) MultiLevelSelectionFragment.this.adapter.getItem(position)).obj : (CheckableAdapter.CheckableItem) MultiLevelSelectionFragment.this.adapter.getItem(position);
                        CheckableAdapter checkableAdapter = (CheckableAdapter) MultiLevelSelectionFragment.this.adapter;
                        if (item3.checked) {
                            z = false;
                        }
                        checkableAdapter.setItemCheckStatus(position, z);
                        MultiLevelItem mItem = new MultiLevelItem();
                        mItem.id = item3.id;
                        mItem.txt = item3.txt;
                        MultiLevelSelectionFragment.this.finishFragment(MultiLevelSelectionFragment.this.fragmentRequestCode, mItem);
                    }
                }
            }
        });
    }

    class GetMetaDataThread implements Runnable {
        private String id;

        public GetMetaDataThread(String id2) {
            this.id = id2;
        }

        public void run() {
            ApiParams params = new ApiParams();
            params.addParam("objIds", this.id);
            try {
                String unused = MultiLevelSelectionFragment.this.json = BaseApiCommand.createCommand("metaobject", true, params).executeSync(GlobalDataManager.getInstance().getApplicationContext());
            } catch (Exception e) {
                Log.d("QLM", "fail to get meta object, caused by " + e.getMessage());
            }
            MultiLevelSelectionFragment.this.sendMessage(1, null);
        }
    }

    public void onFragmentBackWithData(int message, Object obj) {
        if (message != 17) {
            finishFragment(this.fragmentRequestCode, obj);
        } else if ((this.adapter instanceof CommonItemAdapter) && (obj instanceof MultiLevelItem)) {
            Bundle bundle = createArguments(((MultiLevelItem) obj).txt, null);
            bundle.putInt("reqestCode", this.fragmentRequestCode);
            bundle.putInt("maxLevel", this.remainLevel - 1);
            bundle.putString("metaId", ((MultiLevelItem) obj).id);
            pushFragment(new MultiLevelSelectionFragment(), bundle);
        } else if ((this.adapter instanceof CheckableAdapter) && (obj instanceof CheckableAdapter.CheckableItem)) {
            MultiLevelItem mItem = new MultiLevelItem();
            mItem.id = ((CheckableAdapter.CheckableItem) obj).id;
            mItem.txt = ((CheckableAdapter.CheckableItem) obj).txt;
            finishFragment(this.fragmentRequestCode, mItem);
        }
    }

    public boolean hasGlobalTab() {
        return false;
    }
}
