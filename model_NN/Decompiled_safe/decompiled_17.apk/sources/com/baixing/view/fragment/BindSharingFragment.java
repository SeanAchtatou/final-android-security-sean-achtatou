package com.baixing.view.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.sharing.BaseSharingManager;
import com.baixing.sharing.QZoneSharingManager;
import com.baixing.sharing.WeiboSSOSharingManager;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.quanleimu.activity.R;

class BindSharingFragment extends BaseFragment implements View.OnClickListener {
    private BroadcastReceiver authReceiver = null;
    private BaseSharingManager sharingMgr = null;

    enum BindType {
        BindType_Weibo,
        BindType_QZone
    }

    BindSharingFragment() {
    }

    /* access modifiers changed from: protected */
    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout layout = (LinearLayout) inflater.inflate((int) R.layout.bind_sharing, (ViewGroup) null);
        layout.findViewById(R.id.bindWeibo).setOnClickListener(this);
        layout.findViewById(R.id.bindQQ).setOnClickListener(this);
        return layout;
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = "绑定转发帐号";
        title.m_leftActionHint = "返回";
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        setBindingStatus();
    }

    /* access modifiers changed from: private */
    public void setBindingStatus() {
        View view = getView();
        View rootView = view == null ? null : view.getRootView();
        if (rootView != null) {
            if (isWeiboBinded()) {
                ((TextView) rootView.findViewById(R.id.weiboBindStatus)).setText("已绑定");
            } else {
                ((TextView) rootView.findViewById(R.id.weiboBindStatus)).setText("尚未绑定，点此绑定");
            }
            if (isQZoneBinded()) {
                ((TextView) rootView.findViewById(R.id.qqBindStatus)).setText("已绑定");
            } else {
                ((TextView) rootView.findViewById(R.id.qqBindStatus)).setText("尚未绑定，点此绑定");
            }
        }
    }

    private boolean isWeiboBinded() {
        return ((WeiboSSOSharingManager.WeiboAccessTokenWrapper) Util.loadDataFromLocate(getActivity(), WeiboSSOSharingManager.STRING_WEIBO_ACCESS_TOKEN, WeiboSSOSharingManager.WeiboAccessTokenWrapper.class)) != null;
    }

    private boolean isQZoneBinded() {
        String accessToken = (String) Util.loadDataFromLocate(getActivity(), QZoneSharingManager.STRING_ACCESS_TOKEN, String.class);
        return accessToken != null && accessToken.length() > 0;
    }

    public void onResume() {
        super.onResume();
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.BINDFORWARD).end();
        setBindingStatus();
    }

    public void onPause() {
        if (this.authReceiver != null) {
            getActivity().unregisterReceiver(this.authReceiver);
            this.authReceiver = null;
        }
        super.onPause();
    }

    private void showUnBindConfirmDialog(final BindType type) {
        new AlertDialog.Builder(getActivity()).setMessage("是否解除绑定？").setPositiveButton("是", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (type == BindType.BindType_Weibo) {
                    Util.deleteDataFromLocate(BindSharingFragment.this.getActivity(), WeiboSSOSharingManager.STRING_WEIBO_ACCESS_TOKEN);
                    BindSharingFragment.this.setBindingStatus();
                } else if (type == BindType.BindType_QZone) {
                    Util.deleteDataFromLocate(BindSharingFragment.this.getActivity(), QZoneSharingManager.STRING_ACCESS_TOKEN);
                    Util.deleteDataFromLocate(BindSharingFragment.this.getActivity(), QZoneSharingManager.STRING_OPENID);
                    BindSharingFragment.this.setBindingStatus();
                }
            }
        }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    public void onClick(View v) {
        if (v.getId() == R.id.bindWeibo) {
            if (isWeiboBinded()) {
                showUnBindConfirmDialog(BindType.BindType_Weibo);
                return;
            }
            if (this.sharingMgr != null) {
                this.sharingMgr.release();
            }
            this.sharingMgr = new WeiboSSOSharingManager((BaseActivity) getActivity());
            this.sharingMgr.auth();
        } else if (v.getId() != R.id.bindQQ) {
        } else {
            if (isQZoneBinded()) {
                showUnBindConfirmDialog(BindType.BindType_QZone);
                return;
            }
            if (this.sharingMgr != null) {
                this.sharingMgr.release();
            }
            this.sharingMgr = new QZoneSharingManager(getActivity());
            this.sharingMgr.auth();
            if (this.authReceiver == null) {
                this.authReceiver = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        BindSharingFragment.this.setBindingStatus();
                    }
                };
            }
            getActivity().registerReceiver(this.authReceiver, new IntentFilter(CommonIntentAction.ACTION_BROADCAST_QZONE_AUTH_SUCCESS));
        }
    }
}
