package com.baixing.view.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.baixing.activity.BaseActivity;
import com.baixing.entity.Ad;
import com.baixing.sharing.SharingCenter;

class SharingFragment extends DialogFragment {
    /* access modifiers changed from: private */
    public Ad mAd;

    public SharingFragment(Ad ad, String shareFrom) {
        this.mAd = ad;
        SharingCenter.shareFrom = shareFrom;
        SharingCenter.adId = this.mAd.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID);
        SharingCenter.categoryName = this.mAd.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME);
    }

    public SharingFragment() {
    }

    public Dialog onCreateDialog(Bundle savedInstanceBundle) {
        final boolean wxInstalled = SharingCenter.isWeixinInstalled(getActivity());
        String[] namesInstalled = {"转发到微信朋友圈", "转发到微信好友", "转发到QQ空间", "转发到新浪微博"};
        String[] namesNotInstalled = {"转发到QQ空间", "转发到新浪微博"};
        AlertDialog.Builder title = new AlertDialog.Builder(getActivity()).setTitle("发布成功！请朋友帮忙转发");
        if (!wxInstalled) {
            namesInstalled = namesNotInstalled;
        }
        return title.setItems(namesInstalled, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SharingFragment.this.dismiss();
                switch (which) {
                    case 0:
                        if (wxInstalled) {
                            SharingCenter.share2Weixin((BaseActivity) SharingFragment.this.getActivity(), SharingFragment.this.mAd, true);
                            return;
                        } else {
                            SharingCenter.share2QZone((BaseActivity) SharingFragment.this.getActivity(), SharingFragment.this.mAd);
                            return;
                        }
                    case 1:
                        if (wxInstalled) {
                            SharingCenter.share2Weixin((BaseActivity) SharingFragment.this.getActivity(), SharingFragment.this.mAd, false);
                            return;
                        } else {
                            SharingCenter.share2Weibo((BaseActivity) SharingFragment.this.getActivity(), SharingFragment.this.mAd);
                            return;
                        }
                    case 2:
                        SharingCenter.share2QZone((BaseActivity) SharingFragment.this.getActivity(), SharingFragment.this.mAd);
                        return;
                    case 3:
                        SharingCenter.share2Weibo((BaseActivity) SharingFragment.this.getActivity(), SharingFragment.this.mAd);
                        return;
                    default:
                        return;
                }
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
    }
}
