package com.baixing.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import com.baixing.activity.BaseFragment;
import com.baixing.adapter.VadListAdapter;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.entity.AdList;
import com.baixing.entity.AdSeperator;
import com.baixing.entity.BXLocation;
import com.baixing.entity.CityDetail;
import com.baixing.entity.Filterss;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.ErrorHandler;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import com.baixing.util.VadListLoader;
import com.baixing.util.ViewUtil;
import com.baixing.view.AdViewHistory;
import com.baixing.view.FilterUtil;
import com.baixing.view.fragment.MultiLevelSelectionFragment;
import com.baixing.widget.PullToRefreshListView;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.httpclient.HttpStatus;

public class ListingFragment extends BaseFragment implements AbsListView.OnScrollListener, PullToRefreshListView.OnRefreshListener, PullToRefreshListView.OnGetmoreListener, BaseApiCommand.Callback, VadListLoader.Callback {
    public static final int MSG_UPDATE_AREAS = 1001;
    public static final int MSG_UPDATE_FILTER = 1000;
    private static final int REQ_SIFT = 1;
    private String actType = null;
    private Filterss areaFilterss = null;
    boolean areasUpdated = false;
    /* access modifiers changed from: private */
    public String categoryEnglishName = "";
    /* access modifiers changed from: private */
    public BXLocation curLocation = null;
    /* access modifiers changed from: private */
    public PostParamsHolder filterParamHolder;
    boolean filterUpdated = false;
    /* access modifiers changed from: private */
    public VadListLoader goodsListLoader = null;
    private List<Filterss> listFilterss = null;
    /* access modifiers changed from: private */
    public PullToRefreshListView lvGoodsList;
    private boolean mRefreshUsingLocal = false;
    private List<Filterss> otherFilterss = null;
    private ProgressBar progressBar;
    private String searchContent = "";

    /* access modifiers changed from: protected */
    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_leftActionHint = "返回";
        title.m_title = getArguments().getString("categoryName");
        title.m_rightActionHint = "筛选";
    }

    public VadListAdapter findGoodListAdapter() {
        ListAdapter adapter = this.lvGoodsList == null ? null : this.lvGoodsList.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            return (VadListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        }
        return (VadListAdapter) adapter;
    }

    public void handleRightAction() {
        if (this.categoryEnglishName == null || this.categoryEnglishName.equals("")) {
            pushAndFinish(new SearchFragment(), createArguments(null, null));
            return;
        }
        Bundle args = createArguments(null, getArguments().getString(BaseFragment.ARG_COMMON_BACK_HINT));
        args.putAll(getArguments());
        args.putInt("reqestCode", 1);
        args.putString("searchType", "goodslist");
        args.putString("categoryEnglishName", this.categoryEnglishName);
        pushFragment(new FilterFragment(), args);
    }

    public String getCategoryNames() {
        return this.categoryEnglishName + "," + getArguments().getString("categoryName");
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int requestCode, Object result) {
        if (requestCode == 1 && (result instanceof PostParamsHolder)) {
            this.filterParamHolder.clear();
            this.filterParamHolder.merge((PostParamsHolder) result);
            if (this.listFilterss != null && this.listFilterss.size() > 1) {
                showFilterBar(getView(), this.listFilterss);
            }
            resetSearch(this.curLocation);
            this.lvGoodsList.fireRefresh();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        PerformanceTracker.stamp(PerformEvent.Event.E_ListingFrag_begin);
        super.onCreate(savedInstanceState);
        this.categoryEnglishName = getArguments().getString("categoryEnglishName");
        this.searchContent = getArguments().getString("searchContent");
        this.actType = getArguments().getString("actType");
        this.filterParamHolder = (PostParamsHolder) getArguments().getSerializable("filterResult");
        if (this.filterParamHolder == null) {
            this.filterParamHolder = new PostParamsHolder();
            getArguments().putSerializable("filterResult", this.filterParamHolder);
            if (this.searchContent != null) {
                this.filterParamHolder.put("", this.searchContent, this.searchContent);
            }
        }
        this.goodsListLoader = new VadListLoader(getSearchParams(), this, null, new AdList());
        this.goodsListLoader.setRuntime(true);
        PerformanceTracker.stamp(PerformEvent.Event.E_ListingFrag_create_end);
    }

    public void onResume() {
        PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Showup);
        super.onResume();
        if (this.actType == null || !this.actType.equals("search")) {
            this.pv = TrackConfig.TrackMobile.PV.LISTING;
            Tracker.getInstance().pv(this.pv).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).end();
        } else {
            this.pv = TrackConfig.TrackMobile.PV.SEARCHRESULT;
            Tracker.getInstance().pv(this.pv).append(TrackConfig.TrackMobile.Key.SEARCHKEYWORD, this.searchContent).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).end();
        }
        this.goodsListLoader.setCallback(this);
    }

    public void onStackTop(boolean isBack) {
        super.onStackTop(isBack);
        if (this.goodsListLoader.getGoodsList().getData() == null || this.goodsListLoader.getGoodsList().getData().size() <= 0) {
            this.lvGoodsList.setAdapter((ListAdapter) new VadListAdapter(getActivity(), new ArrayList(), AdViewHistory.getInstance()));
            if (!isBack) {
                this.mRefreshUsingLocal = true;
                PerformanceTracker.stamp(PerformEvent.Event.E_FireRefresh_OnShowup);
                this.lvGoodsList.fireRefresh();
            }
        } else {
            VadListAdapter adapter = new VadListAdapter(getActivity(), this.goodsListLoader.getGoodsList().getData(), AdViewHistory.getInstance());
            this.lvGoodsList.setAdapter((ListAdapter) adapter);
            updateData(adapter, this.goodsListLoader.getGoodsList().getData());
            this.lvGoodsList.setSelectionFromHeader(this.goodsListLoader.getSelection());
        }
        if (this.listFilterss == null || this.listFilterss.size() <= 1) {
            boolean metaNeeded = true;
            boolean areaNeeded = true;
            if (this.otherFilterss == null) {
                Pair<Long, String> pair = Util.loadJsonAndTimestampFromLocate(getActivity(), "saveFilterss" + this.categoryEnglishName + GlobalDataManager.getInstance().getCityEnglishName());
                String json = (String) pair.second;
                if (json != null && json.length() > 0 && ((Long) pair.first).longValue() + TextUtil.FULL_DAY >= System.currentTimeMillis() / 1000) {
                    this.otherFilterss = JsonUtil.getFilters(json).getFilterssList();
                    metaNeeded = false;
                }
            }
            if (this.areaFilterss == null) {
                Pair<Long, String> pair2 = Util.loadJsonAndTimestampFromLocate(getActivity(), "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName());
                String json2 = (String) pair2.second;
                if (json2 != null && json2.length() > 0 && ((Long) pair2.first).longValue() + TextUtil.FULL_DAY >= System.currentTimeMillis() / 1000) {
                    this.areaFilterss = JsonUtil.getTopAreas(json2);
                    areaNeeded = false;
                }
            }
            if (this.otherFilterss == null || this.areaFilterss == null) {
                executeGetAdsCommand(metaNeeded, areaNeeded);
                return;
            }
            this.listFilterss = new ArrayList();
            this.listFilterss.add(this.areaFilterss);
            this.listFilterss.addAll(this.otherFilterss);
            if (this.listFilterss.size() > 1) {
                showFilterBar(getView().findViewById(R.id.filter_bar_root), this.listFilterss);
                return;
            }
            return;
        }
        showFilterBar(getView().findViewById(R.id.filter_bar_root), this.listFilterss);
    }

    private void executeGetAdsCommand(boolean metaNeeded, boolean areaNeeded) {
        String cityId = GlobalDataManager.getInstance().getCityId();
        if (metaNeeded) {
            ApiParams params = new ApiParams();
            params.addParam("categoryId", this.categoryEnglishName);
            params.addParam("cityId", cityId);
            BaseApiCommand.createCommand("Meta.filter/", true, params).execute(getActivity(), this);
        }
        if (areaNeeded) {
            ApiParams areaParam = new ApiParams();
            areaParam.addParam("cityId", cityId);
            BaseApiCommand.createCommand("City.areas/", true, areaParam).execute(getActivity(), this);
        }
    }

    private ApiParams getSearchParams() {
        ApiParams basicParams = new ApiParams();
        basicParams.addAll(this.filterParamHolder.toParams());
        String keyword = this.filterParamHolder.getData("");
        if (keyword != null && keyword.length() > 0) {
            basicParams.addParam("query", keyword);
        }
        if (isSerchNearBy() && this.curLocation != null) {
            if (this.goodsListLoader.getSearchType() == VadListLoader.SEARCH_POLICY.SEARCH_NEARBY) {
                basicParams.addParam("coordinate[lat]", "" + this.curLocation.fLat);
                basicParams.addParam("coordinate[lng]", "" + this.curLocation.fLon);
            } else {
                basicParams.addParam("lat", "" + this.curLocation.fLat);
                basicParams.addParam("lng", "" + this.curLocation.fLon);
            }
        }
        if (!basicParams.hasParam("area")) {
            basicParams.addParam("area", GlobalDataManager.getInstance().getCityId());
        }
        basicParams.addParam("categoryEnglishName", this.categoryEnglishName);
        return basicParams;
    }

    private boolean isSerchNearBy() {
        return PostParamsHolder.INVALID_VALUE.equals(this.filterParamHolder.getData("area"));
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String categoryName;
        logCreateView(savedInstanceState);
        PerformanceTracker.stamp(PerformEvent.Event.E_InitListingFragView_Begin);
        View v = inflater.inflate((int) R.layout.goodslist, (ViewGroup) null);
        this.lvGoodsList = (PullToRefreshListView) v.findViewById(R.id.lvGoodsList);
        this.lvGoodsList.setOnRefreshListener(this);
        this.lvGoodsList.setOnGetMoreListener(this);
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(0);
        this.progressBar = new ProgressBar(getActivity(), null, 16842873);
        this.progressBar.setVisibility(8);
        layout.addView(this.progressBar, new LinearLayout.LayoutParams(-2, -2));
        layout.setGravity(17);
        this.lvGoodsList.setOnScrollListener(this);
        this.curLocation = GlobalDataManager.getInstance().getLocationManager().getCurrentPosition(true);
        if (this.curLocation == null && isSerchNearBy()) {
            this.filterParamHolder.remove("area");
        }
        this.goodsListLoader.setParams(getSearchParams());
        if (this.curLocation != null && isSerchNearBy()) {
            this.goodsListLoader.setSearchType(VadListLoader.SEARCH_POLICY.SEARCH_NEARBY);
            this.goodsListLoader.setRuntime(true);
        }
        this.lvGoodsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                int index = (int) arg3;
                if (index >= 0 && index <= ListingFragment.this.goodsListLoader.getGoodsList().getData().size() - 1) {
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LISTING_SELECTEDROWINDEX).append(TrackConfig.TrackMobile.Key.SELECTEDROWINDEX, index).end();
                    if (!(ListingFragment.this.goodsListLoader.getGoodsList().getData().get(index) instanceof AdSeperator)) {
                        Bundle bundle = ListingFragment.this.createArguments(null, null);
                        bundle.putSerializable("loader", ListingFragment.this.goodsListLoader);
                        bundle.putInt("index", index);
                        ListingFragment.this.pushFragment(new VadFragment(), bundle);
                    }
                }
            }
        });
        String categoryName2 = getArguments().getString("categoryName");
        if (!((categoryName2 != null && !categoryName2.equals("")) || this.categoryEnglishName == null || (categoryName = GlobalDataManager.getInstance().queryCategoryDisplayName(this.categoryEnglishName)) == null)) {
            getArguments().putString("categoryName", categoryName);
        }
        PerformanceTracker.stamp(PerformEvent.Event.E_InitListingFragView_End);
        return v;
    }

    public void onDestroy() {
        this.lvGoodsList = null;
        AdList goodsList = this.goodsListLoader.getGoodsList();
        this.goodsListLoader.reset();
        this.goodsListLoader = null;
        super.onDestroy();
    }

    public void onPause() {
        this.lvGoodsList.setOnScrollListener(null);
        super.onPause();
        for (int i = 0; i < this.lvGoodsList.getChildCount(); i++) {
            ImageView imageView = (ImageView) this.lvGoodsList.getChildAt(i).findViewById(R.id.ivInfo);
            if (!(imageView == null || imageView.getTag() == null || imageView.getTag().toString().length() <= 0)) {
                GlobalDataManager.getInstance().getImageLoaderMgr().Cancel(imageView.getTag().toString(), imageView);
            }
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        View curIv;
        if (scrollState == 0) {
            ArrayList<String> urls = new ArrayList<>();
            for (int index = 0; index < view.getChildCount(); index++) {
                View curView = view.getChildAt(index);
                if (!(curView == null || (curIv = curView.findViewById(R.id.ivInfo)) == null || curIv.getTag() == null)) {
                    urls.add(curIv.getTag().toString());
                }
            }
            GlobalDataManager.getInstance().getImageLoaderMgr().AdjustPriority(urls);
        }
    }

    public void onGetMore() {
        boolean z;
        this.goodsListLoader.setParams(getSearchParams());
        VadListLoader vadListLoader = this.goodsListLoader;
        Context appContext = getAppContext();
        if (VadListLoader.E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE == this.goodsListLoader.getDataStatus()) {
            z = true;
        } else {
            z = false;
        }
        vadListLoader.startFetching(appContext, false, z);
    }

    public void onRefresh() {
        PerformanceTracker.stamp(PerformEvent.Event.E_Listing_StartFetching);
        this.goodsListLoader.setParams(getSearchParams());
        this.goodsListLoader.startFetching(getAppContext(), true, this.mRefreshUsingLocal);
        this.mRefreshUsingLocal = false;
    }

    private void updateData(VadListAdapter adapter, List<Ad> data) {
        if (isSerchNearBy()) {
            adapter.setList(data, FilterUtil.createDistanceGroupAndResortAds(data, this.curLocation, new int[]{HttpStatus.SC_INTERNAL_SERVER_ERROR, 1500}));
        } else {
            adapter.setList(data, FilterUtil.createFilterGroup(this.listFilterss, this.filterParamHolder, data));
        }
    }

    private static String getAroundCityName(List<Ad> ads) {
        String ret = "";
        String preCityEnglishName = GlobalDataManager.getInstance().getCityEnglishName();
        String cityCombine = preCityEnglishName;
        List<CityDetail> cities = GlobalDataManager.getInstance().getListCityDetails();
        for (int i = 0; i < ads.size(); i++) {
            String curCityEnglishName = ads.get(i).getValueByKey("cityEnglishName");
            if (!curCityEnglishName.equals(preCityEnglishName) && !cityCombine.contains(curCityEnglishName)) {
                preCityEnglishName = ads.get(i).getValueByKey("cityEnglishName");
                cityCombine = cityCombine + " " + preCityEnglishName;
                int j = 0;
                while (true) {
                    if (j >= cities.size()) {
                        break;
                    } else if (cities.get(j).englishName.equals(preCityEnglishName)) {
                        ret = ret + cities.get(j).name + " ";
                        break;
                    } else {
                        j++;
                    }
                }
            }
        }
        return ret;
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        boolean z;
        String cities;
        switch (msg.what) {
            case ErrorHandler.ERROR_NETWORK_UNAVAILABLE:
            case -3:
                if (this.goodsListLoader != null) {
                    this.progressBar.setVisibility(8);
                    if (msg.what != -3 || msg.obj == null) {
                        ErrorHandler.getInstance().handleError(-10, null);
                    } else {
                        ViewUtil.showToast(getActivity(), (String) msg.obj, false);
                    }
                    this.lvGoodsList.onFail();
                    hideProgress();
                    return;
                }
                return;
            case 0:
                PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Got_First);
                if (this.goodsListLoader != null) {
                    PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Start_ParseJson);
                    AdList goodsList = JsonUtil.getGoodsListFromJson(this.goodsListLoader.getLastJson());
                    PerformanceTracker.stamp(PerformEvent.Event.E_Listing_End_ParseJson);
                    boolean prevSearchAround = this.goodsListLoader.getSearchType() == VadListLoader.SEARCH_POLICY.SEARCH_AROUND;
                    if (!prevSearchAround) {
                        this.goodsListLoader.setGoodsList(goodsList);
                    } else if (!(goodsList == null || goodsList.getData() == null || goodsList.getData().size() <= 0)) {
                        AdSeperator seperator = new AdSeperator();
                        String cities2 = getAroundCityName(goodsList.getData());
                        if (!TextUtils.isEmpty(cities2)) {
                            cities = "附近地区:" + cities2;
                        } else {
                            cities = "附近地区";
                        }
                        seperator.setValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE, cities);
                        AdList preList = this.goodsListLoader.getGoodsList();
                        preList.getData().add(seperator);
                        preList.getData().addAll(goodsList.getData());
                        this.goodsListLoader.setGoodsList(preList);
                    }
                    boolean needAroundAds = false;
                    int newRows = 0;
                    if (this.goodsListLoader.getGoodsList() == null || this.goodsListLoader.getGoodsList().getData() == null || this.goodsListLoader.getGoodsList().getData().size() == 0) {
                        if (prevSearchAround || isSerchNearBy()) {
                            ErrorHandler.getInstance().handleError(-3, "没有符合的结果，请更改条件并重试！");
                        } else {
                            needAroundAds = true;
                            newRows = this.goodsListLoader.getRows();
                        }
                        VadListAdapter adapter = findGoodListAdapter();
                        if (adapter != null) {
                            adapter.setList(new ArrayList());
                            adapter.updateGroups(null);
                            adapter.notifyDataSetChanged();
                        }
                    } else if (isSerchNearBy() || prevSearchAround || goodsList.getData().size() >= this.goodsListLoader.getRows()) {
                        VadListAdapter adapter2 = new VadListAdapter(getActivity(), this.goodsListLoader.getGoodsList().getData(), AdViewHistory.getInstance());
                        updateData(adapter2, this.goodsListLoader.getGoodsList().getData());
                        this.lvGoodsList.setAdapter((ListAdapter) adapter2);
                        this.goodsListLoader.setHasMore(true);
                    } else {
                        needAroundAds = true;
                        newRows = this.goodsListLoader.getRows() - goodsList.getData().size();
                    }
                    if (needAroundAds) {
                        this.goodsListLoader.setSearchType(VadListLoader.SEARCH_POLICY.SEARCH_AROUND);
                        this.goodsListLoader.setRows(newRows);
                        this.goodsListLoader.setParams(getSearchParams());
                        this.goodsListLoader.startFetching(getAppContext(), true, this.mRefreshUsingLocal);
                        return;
                    }
                    this.lvGoodsList.onRefreshComplete();
                    if (VadListLoader.E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE != this.goodsListLoader.getDataStatus()) {
                        HashMap<String, String> tmp = new HashMap<>();
                        Iterator<String> ite = this.filterParamHolder.keyIterator();
                        while (ite.hasNext()) {
                            String key = ite.next();
                            tmp.put(key, this.filterParamHolder.getData(key));
                        }
                        if (tmp.containsKey("")) {
                            tmp.put(TrackConfig.TrackMobile.Key.LISTINGFILTERKEYWORD.getName(), tmp.get(""));
                            tmp.remove("");
                        }
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LISTING).append(TrackConfig.TrackMobile.Key.SEARCHKEYWORD, this.searchContent).append(tmp).append(TrackConfig.TrackMobile.Key.TOTAL_ADSCOUNT, this.goodsListLoader.getGoodsList().getData().size()).end();
                    }
                    hideProgress();
                    PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Got_First_Leave);
                    PerformanceTracker.flush();
                    if (this.goodsListLoader.getSearchType() == VadListLoader.SEARCH_POLICY.SEARCH_AROUND) {
                        this.goodsListLoader.setSearchType(isSerchNearBy() ? VadListLoader.SEARCH_POLICY.SEARCH_NEARBY : VadListLoader.SEARCH_POLICY.SEARCH_LISTING);
                        this.goodsListLoader.setRows(-1);
                        return;
                    }
                    return;
                }
                return;
            case 1:
                if (this.goodsListLoader != null) {
                    this.progressBar.setVisibility(8);
                    AdList moreGoodsList = JsonUtil.getGoodsListFromJson(this.goodsListLoader.getLastJson());
                    if (moreGoodsList == null || moreGoodsList.getData() == null || moreGoodsList.getData().size() == 0) {
                        this.lvGoodsList.onGetMoreCompleted(PullToRefreshListView.E_GETMORE.E_GETMORE_NO_MORE);
                        this.goodsListLoader.setHasMore(false);
                    } else {
                        List<Ad> listCommonGoods = moreGoodsList.getData();
                        if (this.goodsListLoader.getGoodsList().getData() == null) {
                            this.goodsListLoader.getGoodsList().setData(listCommonGoods);
                        } else {
                            this.goodsListLoader.getGoodsList().getData().addAll(listCommonGoods);
                        }
                        VadListAdapter adapter3 = findGoodListAdapter();
                        if (adapter3 != null) {
                            updateData(adapter3, this.goodsListLoader.getGoodsList().getData());
                            adapter3.notifyDataSetChanged();
                        }
                        this.lvGoodsList.onGetMoreCompleted(PullToRefreshListView.E_GETMORE.E_GETMORE_OK);
                        this.goodsListLoader.setHasMore(true);
                    }
                    HashMap<String, String> tmp2 = new HashMap<>();
                    Iterator<String> ite2 = this.filterParamHolder.keyIterator();
                    while (ite2.hasNext()) {
                        String key2 = ite2.next();
                        tmp2.put(key2, this.filterParamHolder.getData(key2));
                    }
                    if (tmp2.containsKey("")) {
                        tmp2.put(TrackConfig.TrackMobile.Key.LISTINGFILTERKEYWORD.getName(), tmp2.get(""));
                        tmp2.remove("");
                    }
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LISTING_MORE).append(TrackConfig.TrackMobile.Key.SEARCHKEYWORD, this.searchContent).append(tmp2).append(TrackConfig.TrackMobile.Key.TOTAL_ADSCOUNT, this.goodsListLoader.getGoodsList().getData().size()).end();
                    hideProgress();
                    return;
                }
                return;
            case 2:
                if (this.goodsListLoader != null) {
                    this.progressBar.setVisibility(8);
                    this.lvGoodsList.onGetMoreCompleted(PullToRefreshListView.E_GETMORE.E_GETMORE_NO_MORE);
                    this.goodsListLoader.setHasMore(false);
                    hideProgress();
                    return;
                }
                return;
            case 1000:
            case 1001:
                boolean z2 = this.areaFilterss != null;
                if (this.otherFilterss != null) {
                    z = true;
                } else {
                    z = false;
                }
                if ((z2 && z) && this.otherFilterss.size() > 0) {
                    this.listFilterss = new ArrayList();
                    this.listFilterss.add(this.areaFilterss);
                    this.listFilterss.addAll(this.otherFilterss);
                    showFilterBar(rootView, this.listFilterss);
                    return;
                }
                return;
            case VadListLoader.MSG_FIRST_FAIL:
                if (this.goodsListLoader != null) {
                    if (VadListLoader.E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE == this.goodsListLoader.getRequestDataStatus()) {
                        this.goodsListLoader.startFetching(getAppContext(), true, false);
                        return;
                    }
                    ViewUtil.showToast(getActivity(), "没有符合条件的结果，请重新输入！", true);
                    hideProgress();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public boolean isCurrentCity() {
        String geoCity = GlobalDataManager.getInstance().getLocationManager().getCurrentCity();
        return geoCity != null && geoCity.contains(GlobalDataManager.getInstance().getCityName());
    }

    public void showFilterBar(View root, List<Filterss> fss) {
        int i = 0;
        View[] actionViews = findAllFilterView();
        if (actionViews != null) {
            View.OnClickListener listener = new View.OnClickListener() {
                public void onClick(View v) {
                    final Filterss fss = (Filterss) v.getTag();
                    boolean isLocation = fss.getName().equals("area");
                    MultiLevelSelectionFragment.MultiLevelItem cItem = new MultiLevelSelectionFragment.MultiLevelItem();
                    cItem.id = PostParamsHolder.INVALID_VALUE;
                    cItem.txt = "附近500米";
                    MultiLevelSelectionFragment.MultiLevelItem[] cs = (!isLocation || ListingFragment.this.curLocation == null || !ListingFragment.this.isCurrentCity()) ? null : new MultiLevelSelectionFragment.MultiLevelItem[]{cItem};
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LISTING_TOPFILTEROPEN).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, ListingFragment.this.categoryEnglishName).append(TrackConfig.TrackMobile.Key.FILTERNAME, fss.getDisplayName()).end();
                    FilterUtil.startSelect(ListingFragment.this.getActivity(), cs, fss, new FilterUtil.FilterSelectListener() {
                        public void onItemSelect(MultiLevelSelectionFragment.MultiLevelItem item) {
                            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LISTING_TOPFILTERSUBMIT).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, ListingFragment.this.categoryEnglishName).append(TrackConfig.TrackMobile.Key.FILTERNAME, fss.getDisplayName()).append(TrackConfig.TrackMobile.Key.FILTERVALUE, item.txt).end();
                            FilterUtil.updateFilter(ListingFragment.this.filterParamHolder, item, fss.getName());
                            if (ListingFragment.this.filterParamHolder.containsKey(fss.getName())) {
                                FilterUtil.updateFilterLabel(ListingFragment.this.findAllFilterView(), item.txt, fss);
                            } else {
                                FilterUtil.updateFilterLabel(ListingFragment.this.findAllFilterView(), fss.getDisplayName(), fss);
                            }
                            ListingFragment.this.resetSearch(ListingFragment.this.curLocation);
                            ListingFragment.this.lvGoodsList.fireRefresh();
                        }

                        public void onCancel() {
                        }
                    });
                }
            };
            for (View v : actionViews) {
                v.setOnClickListener(listener);
            }
            View fItemParent = getView().findViewById(R.id.filter_parent);
            if (fss != null) {
                FilterUtil.loadFilterBar(fss, this.filterParamHolder, actionViews);
                fItemParent.setVisibility(0);
            } else {
                for (View view : actionViews) {
                    view.setVisibility(8);
                }
                fItemParent.setVisibility(8);
            }
            View filterParent = getView() == null ? null : getView().findViewById(R.id.filter_bar_root);
            if (filterParent != null) {
                if (fss == null) {
                    i = 8;
                }
                filterParent.setVisibility(i);
            }
        }
    }

    /* access modifiers changed from: private */
    public View[] findAllFilterView() {
        View filterParent = getView() == null ? null : getView().findViewById(R.id.filter_bar_root);
        if (filterParent == null) {
            return null;
        }
        return new View[]{filterParent.findViewById(R.id.filter_item_1), filterParent.findViewById(R.id.filter_item_2), filterParent.findViewById(R.id.filter_item_3)};
    }

    /* access modifiers changed from: private */
    public void resetSearch(BXLocation location) {
        boolean isNeryBy = isSerchNearBy();
        this.goodsListLoader.cancelFetching();
        if (!isNeryBy) {
            this.goodsListLoader.setSearchType(VadListLoader.SEARCH_POLICY.SEARCH_LISTING);
            this.goodsListLoader.setParams(getSearchParams());
            this.goodsListLoader.setRuntime(true);
            return;
        }
        this.goodsListLoader.setSearchType(VadListLoader.SEARCH_POLICY.SEARCH_NEARBY);
        this.goodsListLoader.setParams(getSearchParams());
        this.goodsListLoader.setRuntime(true);
    }

    public void onRequestComplete(int respCode, Object data) {
        sendMessage(respCode, data);
    }

    public void onNetworkDone(String apiName, String responseData) {
        if (responseData == null) {
            return;
        }
        if ("Meta.filter/".equals(apiName)) {
            Util.saveJsonAndTimestampToLocate(getAppContext(), "saveFilterss" + this.categoryEnglishName + GlobalDataManager.getInstance().getCityEnglishName(), responseData, System.currentTimeMillis() / 1000);
            this.otherFilterss = JsonUtil.getFilters(responseData).getFilterssList();
            sendMessage(1000, null);
        } else if ("City.areas/".equals(apiName)) {
            Util.saveJsonAndTimestampToLocate(getAppContext(), "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName(), responseData, System.currentTimeMillis() / 1000);
            this.areaFilterss = JsonUtil.getTopAreas(responseData);
            sendMessage(1001, null);
        }
    }

    public void onNetworkFail(String apiName, ApiError error) {
    }
}
