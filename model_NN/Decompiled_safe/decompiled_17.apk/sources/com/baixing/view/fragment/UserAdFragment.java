package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.baixing.activity.BaseFragment;
import com.baixing.adapter.VadListAdapter;
import com.baixing.entity.Ad;
import com.baixing.entity.AdList;
import com.baixing.entity.UserProfile;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.NetworkUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.ErrorHandler;
import com.baixing.util.VadListLoader;
import com.baixing.util.ViewUtil;
import com.baixing.view.AdViewHistory;
import com.baixing.widget.PullToRefreshListView;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.List;

public class UserAdFragment extends BaseFragment implements PullToRefreshListView.OnRefreshListener, VadListLoader.Callback {
    private static final int MSG_LIST_UPDATE = 2;
    private static final int MSG_UPDATE_PROFILE = 1;
    private VadListAdapter adapter;
    private PostParamsHolder filterParamHolder = new PostParamsHolder();
    /* access modifiers changed from: private */
    public VadListLoader listLoader;
    private List<Ad> userAdList = new ArrayList();
    private int userId;
    /* access modifiers changed from: private */
    public UserProfile userProfile;

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 1:
                if (this.userProfile == null) {
                    ViewUtil.postShortToastMessage(rootView, (int) R.string.warning_fail_to_get_profile, 30);
                }
                reCreateTitle();
                refreshHeader();
                return;
            case 2:
                rebuildPage(true);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initTitle(BaseFragment.TitleDef title) {
        title.m_leftActionHint = "返回";
        title.m_title = getArguments().getString("userNick");
    }

    public boolean hasGlobalTab() {
        return false;
    }

    private PullToRefreshListView findAdListView() {
        if (getView() == null) {
            return null;
        }
        return (PullToRefreshListView) getView().findViewById(R.id.lvGoodsList);
    }

    public void onCreate(Bundle savedBundle) {
        super.onCreate(savedBundle);
        this.userId = getArguments().getInt(ApiParams.KEY_USERID);
        String idStr = this.userId + "";
        this.filterParamHolder.put(ApiParams.KEY_USERID, idStr, idStr);
    }

    /* access modifiers changed from: protected */
    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        boolean z = true;
        View v = inflater.inflate((int) R.layout.user_adlist, (ViewGroup) null);
        try {
            if (!NetworkUtil.isNetworkActive(v.getContext())) {
                ErrorHandler.getInstance().handleError(-10, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        PullToRefreshListView lvGoodsList = (PullToRefreshListView) v.findViewById(R.id.lvGoodsList);
        this.adapter = new VadListAdapter(getActivity(), this.userAdList, null);
        this.adapter.setHasDelBtn(true);
        VadListAdapter vadListAdapter = this.adapter;
        if (this.userAdList == null || this.userAdList.size() <= 0) {
            z = false;
        }
        lvGoodsList.setAdapter(vadListAdapter, z);
        AdList gl = new AdList();
        gl.setData(this.userAdList == null ? new ArrayList() : this.userAdList);
        this.listLoader = new VadListLoader(null, this, null, null);
        this.listLoader.setHasMore(false);
        this.listLoader.setGoodsList(gl);
        lvGoodsList.setOnRefreshListener(this);
        lvGoodsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                int index = (int) arg3;
                if (index >= 0 && index <= UserAdFragment.this.listLoader.getGoodsList().getData().size() - 1) {
                    Bundle bundle = UserAdFragment.this.createArguments(null, null);
                    bundle.putSerializable("loader", UserAdFragment.this.listLoader);
                    bundle.putInt("index", index);
                    UserAdFragment.this.pushFragment(new VadFragment(), bundle);
                }
            }
        });
        return v;
    }

    public void onResume() {
        this.pv = TrackConfig.TrackMobile.PV.USER;
        Tracker.getInstance().pv(this.pv).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, getArguments().getString("secondCategoryName")).append(TrackConfig.TrackMobile.Key.ADID, getArguments().getString("adId")).append(TrackConfig.TrackMobile.Key.ADSENDERID, this.userId).end();
        super.onResume();
    }

    private void updateData(VadListAdapter adapter2, List<Ad> list) {
        VadListAdapter.GroupItem g = new VadListAdapter.GroupItem();
        g.resultCount = list.size();
        g.filterHint = "共发布" + list.size() + "条信息";
        g.isCountVisible = false;
        ArrayList<VadListAdapter.GroupItem> gList = new ArrayList<>();
        gList.add(g);
        adapter2.setList(list, gList);
    }

    public void onStackTop(boolean isBack) {
        rebuildPage(false);
    }

    private void rebuildPage(boolean isNetworkFinish) {
        if (this.listLoader != null) {
            this.listLoader.setCallback(this);
        }
        PullToRefreshListView listView = findAdListView();
        if (this.listLoader.getGoodsList().getData() == null || this.listLoader.getGoodsList().getData().size() <= 0) {
            this.adapter = new VadListAdapter(getActivity(), new ArrayList(), AdViewHistory.getInstance());
            listView.setAdapter(this.adapter, isNetworkFinish);
            if (!isNetworkFinish) {
                listView.fireRefresh();
            }
        } else {
            this.adapter = new VadListAdapter(getActivity(), this.listLoader.getGoodsList().getData(), AdViewHistory.getInstance());
            listView.setAdapter(this.adapter, true);
            updateData(this.adapter, this.listLoader.getGoodsList().getData());
            listView.setSelectionFromHeader(this.listLoader.getSelection());
        }
        if (isNetworkFinish) {
            listView.onRefreshComplete();
        }
    }

    private void getProfile() {
        ApiParams params = new ApiParams();
        params.addParam(ApiParams.KEY_USERID, this.userId);
        BaseApiCommand.createCommand("user_profile", true, params).execute(getActivity(), new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                UserProfile unused = UserAdFragment.this.userProfile = null;
                UserAdFragment.this.handler.sendEmptyMessage(1);
            }

            public void onNetworkDone(String apiName, String responseData) {
                UserProfile unused = UserAdFragment.this.userProfile = UserProfile.from(responseData);
                UserAdFragment.this.handler.sendEmptyMessage(1);
            }
        });
    }

    public void onRequestComplete(int respCode, Object data) {
        AdList adList = JsonUtil.getGoodsListFromJson(this.listLoader.getLastJson());
        if (adList == null || adList.getData().size() == 0) {
            if (this.userAdList != null) {
                this.userAdList.clear();
            }
            this.listLoader.setGoodsList(new AdList());
        } else {
            this.userAdList = adList.getData();
            if (this.userAdList != null) {
                int i = 0;
                while (i < this.userAdList.size() - 1) {
                    Ad ad = this.userAdList.get(i);
                    if (ad.getValueByKey("status").equals("0") || ad.getValueByKey("status").equals("4") || ad.getValueByKey("status").equals("20")) {
                        i++;
                    } else {
                        this.userAdList.remove(i);
                    }
                }
                AdList gl2 = new AdList();
                gl2.setData(this.userAdList);
                this.listLoader.setGoodsList(gl2);
            }
        }
        sendMessage(2, null);
    }

    public void onRefresh() {
        ApiParams params = new ApiParams();
        params.addParam("query", this.filterParamHolder.toUrlString());
        this.listLoader.setRows(30);
        this.listLoader.setParams(params);
        this.listLoader.startFetching(getAppContext(), true, 2, 2, 2, !NetworkUtil.isNetworkActive(getAppContext()));
    }
}
