package com.baixing.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.adapter.VadListAdapter;
import com.baixing.data.GlobalDataManager;
import com.baixing.data.LocationManager;
import com.baixing.entity.Ad;
import com.baixing.entity.AdList;
import com.baixing.entity.AdSeperator;
import com.baixing.entity.BXLocation;
import com.baixing.entity.CityDetail;
import com.baixing.imageCache.ImageLoaderManager;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.NetworkUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.ErrorHandler;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.util.VadListLoader;
import com.baixing.util.ViewUtil;
import com.baixing.view.AdViewHistory;
import com.baixing.widget.PullToRefreshListView;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HomePageFragment extends BaseFragment implements BaseApiCommand.Callback, AbsListView.OnScrollListener, PullToRefreshListView.OnRefreshListener, PullToRefreshListView.OnGetmoreListener, VadListLoader.Callback, LocationManager.onLocationFetchedListener {
    public static final String NAME = "HomePageFragment";
    private static final int REQ_BANNER = 1024;
    private static final int REQ_GETKEYWORD = 1;
    static boolean switchCityPrompted = false;
    private int bannerCount = 0;
    private ImageView bannerImage;
    /* access modifiers changed from: private */
    public JSONObject bannerItem;
    private JSONArray bannerJson;
    private LinearLayout bannerLine;
    /* access modifiers changed from: private */
    public JSONArray bannerList;
    private LinearLayout bannerView;
    private JSONArray banners;
    /* access modifiers changed from: private */
    public List<View> childBannerList = new ArrayList();
    private TextView city;
    private final String defaultBanner = "";
    /* access modifiers changed from: private */
    public EditText etSearch;
    /* access modifiers changed from: private */
    public VadListLoader goodsListLoader = null;
    /* access modifiers changed from: private */
    public boolean mRefreshUsingLocal = false;
    private ProgressBar progressBar;
    /* access modifiers changed from: private */
    public PullToRefreshListView recommendList;

    public interface Callback {
        void onRequestComplete(int i, Object obj);
    }

    /* access modifiers changed from: protected */
    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_titleControls = LayoutInflater.from(getActivity()).inflate((int) R.layout.title_home_page, (ViewGroup) null);
        this.etSearch = (EditText) title.m_titleControls.findViewById(R.id.etSearch);
        title.m_rightActionHint = "搜索";
        this.etSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66 || event.getAction() != 1) {
                    return false;
                }
                if (HomePageFragment.this.etSearch.getText().toString().trim().equals("")) {
                    ViewUtil.showToast(HomePageFragment.this.getActivity(), "搜索内容不能为空", false);
                    return true;
                }
                HomePageFragment.this.startSearch();
                return true;
            }
        });
        this.city = (TextView) title.m_titleControls.findViewById(R.id.homegage_city);
        this.city.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                HomePageFragment.this.pushFragment(new CityChangeFragment(), HomePageFragment.this.createArguments("切换城市", "首页"));
                GlobalDataManager.getInstance().getLocationManager().removeLocationListener(HomePageFragment.this);
            }
        });
    }

    public VadListAdapter findGoodListAdapter() {
        ListAdapter adapter = this.recommendList == null ? null : this.recommendList.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            return (VadListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        }
        return (VadListAdapter) adapter;
    }

    public void handleRightAction() {
        startSearch();
        super.handleRightAction();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.network.api.ApiParams.addParam(java.lang.String, double):void
     arg types: [java.lang.String, int]
     candidates:
      com.baixing.network.api.ApiParams.addParam(java.lang.String, float):void
      com.baixing.network.api.ApiParams.addParam(java.lang.String, int):void
      com.baixing.network.api.ApiParams.addParam(java.lang.String, long):void
      com.baixing.network.api.ApiParams.addParam(java.lang.String, java.lang.String):void
      com.baixing.network.api.ApiParams.addParam(java.lang.String, java.util.Map<java.lang.String, java.lang.String>):void
      com.baixing.network.api.ApiParams.addParam(java.lang.String, short):void
      com.baixing.network.api.ApiParams.addParam(java.lang.String, boolean):void
      com.baixing.network.api.ApiParams.addParam(java.lang.String, double):void */
    /* access modifiers changed from: private */
    public ApiParams getSearchParams() {
        ApiParams basicParams = new ApiParams();
        basicParams.addParam(ApiParams.KEY_CITY, GlobalDataManager.getInstance().getCityEnglishName());
        basicParams.addParam(ApiParams.KEY_UDID, Util.getDeviceUdid(getAppContext()));
        basicParams.addParam("lat", 0.0d);
        basicParams.addParam("lng", 0.0d);
        return basicParams;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!switchCityPrompted) {
            GlobalDataManager.getInstance().getLocationManager().addLocationListener(this);
        }
        this.goodsListLoader = new VadListLoader(getSearchParams(), this, null, new AdList());
        this.goodsListLoader.setSearchType(VadListLoader.SEARCH_POLICY.SEARCH_RECOMMEND);
        this.goodsListLoader.setRuntime(true);
        PerformanceTracker.stamp(PerformEvent.Event.E_ListingFrag_create_end);
    }

    /* access modifiers changed from: protected */
    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("home", "initializeview");
        Bundle args = getArguments();
        View v = inflater.inflate((int) R.layout.homepage, (ViewGroup) null);
        this.recommendList = (PullToRefreshListView) v.findViewById(R.id.recommendList);
        this.recommendList.setOnRefreshListener(this);
        this.recommendList.setOnGetMoreListener(this);
        ApiParams param = new ApiParams();
        param.addParam("key", "hotlist");
        BaseApiCommand.createCommand("mobile.featureConfig/", true, param).execute(getActivity(), this);
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        this.bannerView = (LinearLayout) layoutInflater.inflate((int) R.layout.homepage_banner, (ViewGroup) getActivity().findViewById(R.id.banner_view));
        if (args.containsKey("banner") && args.getString("banner") != null) {
            this.bannerView = showBanner(args.getString("banner"), this.bannerView);
        }
        this.recommendList.addHeaderView(this.bannerView);
        this.recommendList.addHeaderView(layoutInflater.inflate((int) R.layout.homepage_header, (ViewGroup) getActivity().findViewById(R.id.homepage_headerview)));
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(0);
        this.progressBar = new ProgressBar(getActivity(), null, 16842873);
        this.progressBar.setVisibility(8);
        layout.addView(this.progressBar, new LinearLayout.LayoutParams(-2, -2));
        layout.setGravity(17);
        this.recommendList.setOnScrollListener(this);
        this.recommendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                int index = (int) arg3;
                if (index >= 0 && index <= HomePageFragment.this.goodsListLoader.getGoodsList().getData().size() - 1) {
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LISTING_SELECTEDROWINDEX).append(TrackConfig.TrackMobile.Key.SELECTEDROWINDEX, index).end();
                    if (!(HomePageFragment.this.goodsListLoader.getGoodsList().getData().get(index) instanceof AdSeperator)) {
                        Bundle bundle = HomePageFragment.this.createArguments(null, null);
                        bundle.putSerializable("loader", HomePageFragment.this.goodsListLoader);
                        bundle.putInt("index", index);
                        HomePageFragment.this.pushFragment(new VadFragment(), bundle);
                    }
                }
            }
        });
        PerformanceTracker.stamp(PerformEvent.Event.E_InitListingFragView_End);
        return v;
    }

    public void onStackTop(boolean isBack) {
        String cityName = GlobalDataManager.getInstance().getCityName();
        if (cityName == null || "".equals(cityName)) {
            pushFragment(new CityChangeFragment(), createArguments("切换城市", "首页"));
        } else {
            if (!cityName.equals(this.city.getText())) {
                this.goodsListLoader.setGoodsList(null);
            }
            this.city.setText(cityName);
            Log.d("homepage", GlobalDataManager.getInstance().getCityId());
            refreshHeader();
            this.goodsListLoader.setParams(getSearchParams());
            this.goodsListLoader.setSearchType(VadListLoader.SEARCH_POLICY.SEARCH_RECOMMEND);
            this.goodsListLoader.setRuntime(true);
        }
        if (this.goodsListLoader.getGoodsList().getData() == null || this.goodsListLoader.getGoodsList().getData().size() <= 0) {
            this.recommendList.setAdapter((ListAdapter) new VadListAdapter(getActivity(), new ArrayList(), AdViewHistory.getInstance()));
            PerformanceTracker.stamp(PerformEvent.Event.E_FireRefresh_OnShowup);
            this.mRefreshUsingLocal = true;
            this.recommendList.fireRefresh();
            return;
        }
        VadListAdapter adapter = new VadListAdapter(getActivity(), this.goodsListLoader.getGoodsList().getData(), AdViewHistory.getInstance());
        this.recommendList.setAdapter((ListAdapter) adapter);
        updateData(adapter, this.goodsListLoader.getGoodsList().getData());
        this.recommendList.setSelectionFromHeader(this.goodsListLoader.getSelection());
    }

    private void updateData(VadListAdapter adapter, List<Ad> data) {
        adapter.setList(data, null);
    }

    public LinearLayout showBanner(String jsonString, LinearLayout parentView) {
        Log.d("banner", jsonString);
        if (jsonString != null) {
            try {
                this.bannerJson = new JSONObject(jsonString).getJSONArray("result");
                Log.d("banner", this.bannerJson.toString());
                if (!(this.bannerJson == null || this.bannerJson.length() == 0)) {
                    int lines = this.bannerJson.length();
                    parentView.removeAllViews();
                    Log.d("home", "lines:" + lines);
                    this.bannerList = new JSONArray();
                    int banner_margin = (int) getResources().getDimension(R.dimen.banner_marginTop);
                    for (int i = 0; i < lines; i++) {
                        this.banners = this.bannerJson.getJSONArray(i);
                        this.bannerLine = new LinearLayout(getActivity());
                        this.bannerLine.setOrientation(0);
                        LinearLayout.LayoutParams lineparams = new LinearLayout.LayoutParams(-1, -2);
                        lineparams.gravity = 17;
                        lineparams.setMargins(banner_margin, banner_margin, banner_margin, 0);
                        this.bannerLine.setLayoutParams(lineparams);
                        for (int j = 0; j < this.banners.length(); j++) {
                            this.bannerItem = this.banners.getJSONObject(j);
                            this.bannerList.put(this.bannerItem);
                            this.bannerImage = new ImageView(getActivity());
                            this.bannerImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                            this.bannerImage.setTag(this.bannerItem.getString("imgUrl"));
                            this.bannerImage.setAdjustViewBounds(true);
                            this.childBannerList.add(this.bannerImage);
                            LinearLayout.LayoutParams imgParams = new LinearLayout.LayoutParams(-2, -2);
                            imgParams.weight = 1.0f;
                            if (this.banners.length() >= 2 && j > 0) {
                                imgParams.setMargins(banner_margin, 0, 0, 0);
                            }
                            this.bannerImage.setLayoutParams(imgParams);
                            GlobalDataManager.getInstance().getImageLoaderMgr().loadImg(new ImageLoaderManager.DownloadCallback() {
                                public void onFail(String url) {
                                    for (int i = 0; i < HomePageFragment.this.childBannerList.size(); i++) {
                                        if ((((View) HomePageFragment.this.childBannerList.get(i)).getTag() instanceof String) && ((String) ((View) HomePageFragment.this.childBannerList.get(i)).getTag()).equals(url)) {
                                            ((ImageView) HomePageFragment.this.childBannerList.get(i)).setImageResource(R.drawable.loading_210_black);
                                        }
                                    }
                                }

                                public void onSucced(String url, Bitmap bp) {
                                    for (int i = 0; i < HomePageFragment.this.childBannerList.size(); i++) {
                                        if ((((View) HomePageFragment.this.childBannerList.get(i)).getTag() instanceof String) && ((String) ((View) HomePageFragment.this.childBannerList.get(i)).getTag()).equals(url)) {
                                            ((ImageView) HomePageFragment.this.childBannerList.get(i)).setImageBitmap(bp);
                                        }
                                    }
                                }
                            }, this.bannerItem.getString("imgUrl"));
                            this.bannerImage.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View arg0) {
                                    Bundle bundle;
                                    new Bundle();
                                    int i = 0;
                                    while (i < HomePageFragment.this.bannerList.length()) {
                                        try {
                                            JSONObject unused = HomePageFragment.this.bannerItem = HomePageFragment.this.bannerList.getJSONObject(i);
                                            if (HomePageFragment.this.bannerItem.getString("imgUrl") == arg0.getTag()) {
                                                switch (HomePageFragment.this.bannerItem.getInt("type")) {
                                                    case 0:
                                                        bundle = new Bundle();
                                                        try {
                                                            String title = HomePageFragment.this.bannerItem.getJSONObject(ThirdpartyTransitActivity.Key_Data).getString(Constants.PARAM_TITLE);
                                                            String url = HomePageFragment.this.bannerItem.getJSONObject(ThirdpartyTransitActivity.Key_Data).getString(Constants.PARAM_URL);
                                                            bundle.putString(Constants.PARAM_TITLE, title);
                                                            bundle.putString(Constants.PARAM_URL, url);
                                                            bundle.putBoolean("isGet", true);
                                                            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.VIEW_BANNER).append(TrackConfig.TrackMobile.Key.URL, url).append(TrackConfig.TrackMobile.Key.TITLE, title).end();
                                                            HomePageFragment.this.pushFragment(new WebViewFragment(), bundle);
                                                            continue;
                                                        } catch (JSONException e) {
                                                            e = e;
                                                            e.printStackTrace();
                                                            return;
                                                        }
                                                    case 1:
                                                        bundle = new Bundle();
                                                        String searchContent = HomePageFragment.this.bannerItem.getJSONObject(ThirdpartyTransitActivity.Key_Data).getString("keyword");
                                                        String categoryName = HomePageFragment.this.bannerItem.getJSONObject(ThirdpartyTransitActivity.Key_Data).getString("categoryEnglishName");
                                                        bundle.putString("searchContent", searchContent);
                                                        bundle.putString("actType", "search");
                                                        bundle.putString(BaseFragment.ARG_COMMON_TITLE, "");
                                                        bundle.putString("categoryEnglishName", categoryName);
                                                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.VIEW_BANNER).append(TrackConfig.TrackMobile.Key.SEARCHKEYWORD, searchContent).end();
                                                        HomePageFragment.this.pushFragment(new ListingFragment(), bundle);
                                                        continue;
                                                }
                                            }
                                            i++;
                                        } catch (JSONException e2) {
                                            e = e2;
                                            e.printStackTrace();
                                            return;
                                        }
                                    }
                                }
                            });
                            this.bannerLine.addView(this.bannerImage);
                        }
                        parentView.addView(this.bannerLine);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return parentView;
    }

    public void onGetMore() {
        boolean z;
        VadListLoader vadListLoader = this.goodsListLoader;
        Context appContext = getAppContext();
        if (VadListLoader.E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE == this.goodsListLoader.getDataStatus()) {
            z = true;
        } else {
            z = false;
        }
        vadListLoader.startFetching(appContext, false, z);
    }

    public void onRefresh() {
        PerformanceTracker.stamp(PerformEvent.Event.E_Listing_StartFetching);
        this.goodsListLoader.startFetching(getAppContext(), true, this.mRefreshUsingLocal);
        this.mRefreshUsingLocal = false;
        if (this.bannerView.getChildCount() == 0) {
            ApiParams param = new ApiParams();
            param.addParam("key", "hotlist");
            BaseApiCommand.createCommand("mobile.featureConfig/", true, param).execute(getActivity(), this);
        }
    }

    public void onNetworkDone(String apiName, String responseData) {
        sendMessage(1024, responseData);
        Log.d("bannerRes", responseData);
    }

    public void onNetworkFail(String apiName, ApiError error) {
    }

    public void onRequestComplete(int respCode, Object data) {
        sendMessage(respCode, data);
    }

    public void onResume() {
        PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Showup);
        super.onResume();
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.LISTHOME).end();
        this.goodsListLoader.setCallback(this);
    }

    public void startSearch() {
        if (!NetworkUtil.isNetworkActive(getActivity())) {
            ViewUtil.showToast(getActivity(), "请检查网络连接情况", false);
            return;
        }
        String searchContent = this.etSearch.getText().toString().trim();
        if (searchContent.equals("")) {
            ViewUtil.showToast(getActivity(), "搜索内容不能为空", false);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putBoolean("HomePage", true);
        bundle.putString("SearchContent", searchContent);
        pushFragment(new SearchFragment(), bundle);
    }

    public void onDestroy() {
        this.recommendList = null;
        this.goodsListLoader.reset();
        this.goodsListLoader = null;
        GlobalDataManager.getInstance().getLocationManager().removeLocationListener(this);
        super.onDestroy();
    }

    public void onPause() {
        this.recommendList.setOnScrollListener(null);
        super.onPause();
        for (int i = 0; i < this.recommendList.getChildCount(); i++) {
            ImageView imageView = (ImageView) this.recommendList.getChildAt(i).findViewById(R.id.ivInfo);
            if (!(imageView == null || imageView.getTag() == null || imageView.getTag().toString().length() <= 0)) {
                GlobalDataManager.getInstance().getImageLoaderMgr().Cancel(imageView.getTag().toString(), imageView);
            }
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        View curIv;
        if (scrollState == 0) {
            ArrayList<String> urls = new ArrayList<>();
            for (int index = 0; index < view.getChildCount(); index++) {
                View curView = view.getChildAt(index);
                if (!(curView == null || (curIv = curView.findViewById(R.id.ivInfo)) == null || curIv.getTag() == null)) {
                    urls.add(curIv.getTag().toString());
                }
            }
            GlobalDataManager.getInstance().getImageLoaderMgr().AdjustPriority(urls);
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case ErrorHandler.ERROR_NETWORK_UNAVAILABLE:
            case -3:
                if (this.goodsListLoader != null) {
                    this.progressBar.setVisibility(8);
                    if (msg.what != -3 || msg.obj == null) {
                        ErrorHandler.getInstance().handleError(-10, null);
                    } else {
                        ViewUtil.showToast(getActivity(), (String) msg.obj, false);
                    }
                    this.recommendList.onFail();
                    hideProgress();
                    return;
                }
                return;
            case 0:
                PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Got_First);
                if (this.goodsListLoader != null) {
                    PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Start_ParseJson);
                    AdList goodsList = JsonUtil.getGoodsListFromJson(this.goodsListLoader.getLastJson());
                    PerformanceTracker.stamp(PerformEvent.Event.E_Listing_End_ParseJson);
                    this.goodsListLoader.setGoodsList(goodsList);
                    if (this.goodsListLoader.getGoodsList() == null || this.goodsListLoader.getGoodsList().getData() == null || this.goodsListLoader.getGoodsList().getData().size() == 0) {
                        VadListAdapter adapter = findGoodListAdapter();
                        if (adapter != null) {
                            adapter.setList(new ArrayList());
                            adapter.updateGroups(null);
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        VadListAdapter adapter2 = new VadListAdapter(getActivity(), this.goodsListLoader.getGoodsList().getData(), AdViewHistory.getInstance());
                        updateData(adapter2, this.goodsListLoader.getGoodsList().getData());
                        this.recommendList.setAdapter((ListAdapter) adapter2);
                        this.goodsListLoader.setHasMore(true);
                    }
                    this.recommendList.onRefreshComplete();
                    if (VadListLoader.E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE == this.goodsListLoader.getDataStatus()) {
                        this.recommendList.fireRefresh();
                    }
                    hideProgress();
                    PerformanceTracker.stamp(PerformEvent.Event.E_Listing_Got_First_Leave);
                    PerformanceTracker.flush();
                    return;
                }
                return;
            case 1:
                if (this.goodsListLoader != null) {
                    this.progressBar.setVisibility(8);
                    AdList moreGoodsList = JsonUtil.getGoodsListFromJson(this.goodsListLoader.getLastJson());
                    if (moreGoodsList == null || moreGoodsList.getData() == null || moreGoodsList.getData().size() == 0) {
                        this.recommendList.onGetMoreCompleted(PullToRefreshListView.E_GETMORE.E_GETMORE_NO_MORE);
                        this.goodsListLoader.setHasMore(false);
                    } else {
                        List<Ad> listCommonGoods = moreGoodsList.getData();
                        if (this.goodsListLoader.getGoodsList().getData() == null) {
                            this.goodsListLoader.getGoodsList().setData(listCommonGoods);
                        } else {
                            this.goodsListLoader.getGoodsList().getData().addAll(listCommonGoods);
                        }
                        VadListAdapter adapter3 = findGoodListAdapter();
                        if (adapter3 != null) {
                            updateData(adapter3, this.goodsListLoader.getGoodsList().getData());
                            adapter3.notifyDataSetChanged();
                        }
                        this.recommendList.onGetMoreCompleted(PullToRefreshListView.E_GETMORE.E_GETMORE_OK);
                        this.goodsListLoader.setHasMore(true);
                    }
                    hideProgress();
                    return;
                }
                return;
            case 2:
                if (this.goodsListLoader != null) {
                    this.progressBar.setVisibility(8);
                    this.recommendList.onGetMoreCompleted(PullToRefreshListView.E_GETMORE.E_GETMORE_NO_MORE);
                    this.goodsListLoader.setHasMore(false);
                    hideProgress();
                    return;
                }
                return;
            case 1024:
                showBanner(msg.obj.toString(), this.bannerView);
                return;
            case VadListLoader.MSG_FIRST_FAIL:
                if (this.goodsListLoader == null) {
                    return;
                }
                if (VadListLoader.E_LISTDATA_STATUS.E_LISTDATA_STATUS_OFFLINE == this.goodsListLoader.getRequestDataStatus()) {
                    this.goodsListLoader.startFetching(getAppContext(), true, false);
                    return;
                }
                ViewUtil.showToast(getActivity(), "没有推荐结果，抱歉！", true);
                hideProgress();
                return;
            default:
                return;
        }
    }

    public void onGeocodedLocationFetched(BXLocation location) {
        if (!switchCityPrompted) {
            final String currentCity = GlobalDataManager.getInstance().getCityName();
            final String gpsCity = GlobalDataManager.getInstance().getLocationManager().getCurrentCity();
            if (!TextUtils.isEmpty(currentCity) && !TextUtils.isEmpty(gpsCity) && !currentCity.equals(gpsCity) && !gpsCity.startsWith(currentCity)) {
                new AlertDialog.Builder(getActivity()).setTitle("是否切换城市").setMessage("猜您在" + gpsCity + "，是否切换到所在城市？").setPositiveButton("切换", new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
                     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
                     candidates:
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (CityDetail city : GlobalDataManager.getInstance().getListCityDetails()) {
                            if (gpsCity.startsWith(city.name)) {
                                GlobalDataManager.getInstance().setCityEnglishName(city.englishName);
                                GlobalDataManager.getInstance().setCityName(city.name);
                                GlobalDataManager.getInstance().setCityId(city.id);
                                Util.saveDataToFile(HomePageFragment.this.getActivity(), null, "cityName", city.getName().getBytes());
                                ((TextView) HomePageFragment.this.getTitleDef().m_titleControls.findViewById(R.id.homegage_city)).setText(city.name);
                                HomePageFragment.this.goodsListLoader.setParams(HomePageFragment.this.getSearchParams());
                                HomePageFragment.this.recommendList.setAdapter((ListAdapter) new VadListAdapter(HomePageFragment.this.getActivity(), new ArrayList(), AdViewHistory.getInstance()));
                                PerformanceTracker.stamp(PerformEvent.Event.E_FireRefresh_OnShowup);
                                boolean unused = HomePageFragment.this.mRefreshUsingLocal = true;
                                HomePageFragment.this.recommendList.fireRefresh();
                                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.City_postSelect).append(TrackConfig.TrackMobile.Key.CURRENTCITY, currentCity).append(TrackConfig.TrackMobile.Key.GEOCITY, gpsCity).append(TrackConfig.TrackMobile.Key.ACCEPT, true);
                                return;
                            }
                        }
                    }
                }).setNegativeButton("不了", new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData
                     arg types: [com.baixing.tracking.TrackConfig$TrackMobile$Key, int]
                     candidates:
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, double):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, float):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, int):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, long):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, com.baixing.tracking.TrackConfig$TrackMobile$Value):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, java.lang.String):com.baixing.tracking.LogData
                      com.baixing.tracking.LogData.append(com.baixing.tracking.TrackConfig$TrackMobile$Key, boolean):com.baixing.tracking.LogData */
                    public void onClick(DialogInterface dialog, int id) {
                        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.City_postSelect).append(TrackConfig.TrackMobile.Key.CURRENTCITY, currentCity).append(TrackConfig.TrackMobile.Key.GEOCITY, gpsCity).append(TrackConfig.TrackMobile.Key.ACCEPT, false);
                    }
                }).create().show();
                switchCityPrompted = true;
            }
        }
    }

    public void onLocationFetched(BXLocation location) {
    }
}
