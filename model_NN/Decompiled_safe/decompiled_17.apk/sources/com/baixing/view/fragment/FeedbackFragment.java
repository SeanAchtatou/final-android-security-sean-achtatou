package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import java.util.Observable;
import java.util.Observer;
import org.json.JSONException;
import org.json.JSONObject;

public class FeedbackFragment extends BaseFragment implements Observer {
    private String adId = "";
    private String content = "";
    /* access modifiers changed from: private */
    public EditText etOpinion;
    private String mobile = "";
    private int opinionType = -1;
    private String result;
    private UserBean user;

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_leftActionHint = "返回";
        title.m_rightActionHint = "确定";
        title.m_title = (getArguments() == null || !getArguments().containsKey(BaseFragment.ARG_COMMON_TITLE)) ? "反馈信息" : getArguments().getString(BaseFragment.ARG_COMMON_TITLE);
    }

    public int[] excludedOptionMenus() {
        return new int[]{2};
    }

    public void onResume() {
        super.onResume();
        this.pv = TrackConfig.TrackMobile.PV.FEEDBACK;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bunle = getArguments() != null ? getArguments() : new Bundle();
        if (bunle.containsKey("type")) {
            this.opinionType = bunle.getInt("type");
        }
        if (bunle.containsKey("adId")) {
            this.adId = bunle.getString("adId");
        }
        if (this.opinionType == 0 || this.opinionType == 1) {
            BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
        }
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate((int) R.layout.opinionback, (ViewGroup) null);
        this.user = (UserBean) Util.loadDataFromLocate(getActivity(), "user", UserBean.class);
        if (this.user != null) {
            this.mobile = this.user.getPhone();
        }
        this.etOpinion = (EditText) rootView.findViewById(R.id.etOpinion);
        this.etOpinion.findFocus();
        if (-1 != this.opinionType) {
            rootView.findViewById(R.id.et_contact).setVisibility(8);
        } else if (this.mobile != null && !this.mobile.equals("")) {
            ((TextView) rootView.findViewById(R.id.et_contact)).setText(this.mobile);
        }
        if (this.opinionType == 0) {
            this.etOpinion.setHint("请留下举报原因");
        } else if (1 == this.opinionType) {
            this.etOpinion.setHint("请留下申诉原因");
        }
        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.etOpinion.postDelayed(new Runnable() {
            public void run() {
                if (FeedbackFragment.this.etOpinion != null) {
                    FeedbackFragment.this.etOpinion.requestFocus();
                    ((InputMethodManager) FeedbackFragment.this.etOpinion.getContext().getSystemService("input_method")).showSoftInput(FeedbackFragment.this.etOpinion, 1);
                }
            }
        }, 100);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        hideProgress();
        switch (msg.what) {
            case 0:
                String message = "";
                if (msg.obj != null) {
                    try {
                        message = new JSONObject((String) msg.obj).getString("result");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (TextUtils.isEmpty(message)) {
                    message = "提交成功！";
                }
                ViewUtil.showToast(activity, message, false);
                finishFragment();
                return;
            case 1:
                ViewUtil.showToast(activity, "提交失败！", false);
                return;
            default:
                return;
        }
    }

    private void doAction() {
        String apiName = -1 == this.opinionType ? "feedback.feedback/" : this.opinionType == 0 ? "feedback.report/" : "feedback.appeal/";
        ApiParams params = new ApiParams();
        if (this.opinionType == -1) {
            params.addParam("mobile", this.mobile);
            params.addParam("content", this.content);
        } else {
            params.setAuthToken(GlobalDataManager.getInstance().getLoginUserToken());
            params.addParam("content", this.content);
            params.addParam("adId", this.adId);
        }
        BaseApiCommand.createCommand(apiName, false, params).execute(GlobalDataManager.getInstance().getApplicationContext(), new BaseApiCommand.Callback() {
            public void onNetworkDone(String apiName, String responseData) {
                FeedbackFragment.this.sendMessage(0, responseData);
            }

            public void onNetworkFail(String apiName, ApiError error) {
                FeedbackFragment.this.sendMessage(1, null);
            }
        });
    }

    public void handleRightAction() {
        this.content = this.etOpinion.getText().toString().trim();
        String contact = ((TextView) getView().findViewById(R.id.et_contact)).getText().toString().trim();
        if (this.content.equals("")) {
            ViewUtil.showToast(getActivity(), "内容不能为空", false);
            return;
        }
        if (contact != null && !contact.equals("")) {
            this.content += "    联系方式: " + contact;
        }
        showSimpleProgress();
        doAction();
    }

    public boolean hasGlobalTab() {
        return false;
    }

    public void update(Observable observable, Object data) {
        if ((data instanceof BxMessageCenter.IBxNotification) && IBxNotificationNames.NOTIFICATION_LOGOUT.equals(((BxMessageCenter.IBxNotification) data).getName())) {
            finishFragment();
        }
    }
}
