package com.baixing.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.baixing.activity.BaseFragment;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.network.api.ApiConfiguration;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;

public class DebugFragment extends BaseFragment implements View.OnClickListener, BaseApiCommand.Callback {
    private final int MSG_pushTestFail = 102;
    private final int MSG_pushTestSuccess = 101;
    /* access modifiers changed from: private */
    public Button hostBtn;
    private EditText pushActionEt;
    private EditText pushDataEt;
    private EditText pushTitleEt;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = "Debug";
    }

    /* access modifiers changed from: protected */
    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout debugLayout = (RelativeLayout) inflater.inflate((int) R.layout.debug_layout, (ViewGroup) null);
        this.hostBtn = (Button) debugLayout.findViewById(R.id.hostBtn);
        this.hostBtn.setOnClickListener(this);
        this.hostBtn.setText(ApiConfiguration.getHost());
        this.pushActionEt = (EditText) debugLayout.findViewById(R.id.pushActionEt);
        this.pushTitleEt = (EditText) debugLayout.findViewById(R.id.pushTitleEt);
        this.pushDataEt = (EditText) debugLayout.findViewById(R.id.pushDataEt);
        ((Button) debugLayout.findViewById(R.id.pushTestBtn)).setOnClickListener(this);
        ToggleButton showPushBtn = (ToggleButton) debugLayout.findViewById(R.id.showPushBtn);
        Boolean showDebugPush = (Boolean) Util.loadDataFromLocate(getAppContext(), "showDebugPush", Boolean.class);
        if (showDebugPush != null) {
            showPushBtn.setChecked(showDebugPush.booleanValue());
        }
        showPushBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Util.saveDataToLocate(DebugFragment.this.getAppContext(), "showDebugPush", new Boolean(isChecked));
            }
        });
        ((TextView) debugLayout.findViewById(R.id.infoTv)).setText("udid:" + Util.getDeviceUdid(getAppContext()));
        return debugLayout;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.hostBtn /*2131165271*/:
                hostBtnClicked();
                return;
            case R.id.pushTestBtn /*2131165272*/:
                pushTestBtnClicked();
                return;
            default:
                return;
        }
    }

    private void hostBtnClicked() {
        final CharSequence[] hosts = {"www.baixing.com", "www.xumengyi.baixing.com", "www.liuchong.baixing.com", "www.zengming.baixing.com", "www.zhongjiawu.com"};
        new AlertDialog.Builder(getActivity()).setTitle("选择 api").setItems(hosts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ApiConfiguration.setHost(hosts[which].toString());
                DebugFragment.this.hostBtn.setText(ApiConfiguration.getHost());
                dialog.dismiss();
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        }).create().show();
    }

    private void pushTestBtnClicked() {
        ApiParams params = new ApiParams();
        params.addParam("type", "push");
        params.addParam("action", this.pushActionEt.getText().toString());
        params.addParam(Constants.PARAM_TITLE, this.pushTitleEt.getText().toString());
        params.addParam(ThirdpartyTransitActivity.Key_Data, "{" + this.pushDataEt.getText().toString() + "}");
        BaseApiCommand.createCommand("debug", false, params).execute(getActivity(), this);
    }

    public void onNetworkDone(String apiName, String responseData) {
        sendMessage(101, responseData);
    }

    public void onNetworkFail(String apiName, ApiError error) {
        sendMessage(102, error);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 101:
                ViewUtil.showToast(getActivity(), msg.obj.toString(), false);
                break;
            case 102:
                break;
            default:
                return;
        }
        new AlertDialog.Builder(getActivity()).setTitle("提示").setMessage(decode2(msg.obj.toString())).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public static String decode2(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        char[] chars = s.toCharArray();
        int i = 0;
        while (i < chars.length) {
            char c = chars[i];
            if (c == '\\' && chars[i + 1] == 'u') {
                char cc = 0;
                int j = 0;
                while (true) {
                    if (j >= 4) {
                        break;
                    }
                    char ch = Character.toLowerCase(chars[i + 2 + j]);
                    if (('0' > ch || ch > '9') && ('a' > ch || ch > 'f')) {
                        cc = 0;
                    } else {
                        cc = (char) ((Character.digit(ch, 16) << ((3 - j) * 4)) | cc);
                        j++;
                    }
                }
                cc = 0;
                if (cc > 0) {
                    i += 5;
                    sb.append(cc);
                    i++;
                }
            }
            sb.append(c);
            i++;
        }
        return sb.toString();
    }
}
