package com.baixing.view.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.baixing.activity.BaseFragment;
import com.baixing.sharing.referral.AppShareFragment;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import org.apache.http.util.EncodingUtils;

public class WebViewFragment extends BaseFragment {
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (!Thread.currentThread().isInterrupted()) {
                switch (msg.what) {
                    case 0:
                        WebViewFragment.this.showProgress("请稍候", "数据载入中", true);
                        break;
                    case 1:
                        WebViewFragment.this.hideProgress();
                        break;
                }
            }
            super.handleMessage(msg);
        }
    };

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_leftActionHint = "返回";
        title.m_title = getArguments().getString(Constants.PARAM_TITLE);
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.WEBVIEW).append(TrackConfig.TrackMobile.Key.WEBVIEW_URL, getArguments().getString(Constants.PARAM_URL)).end();
        if (isJumpable(getArguments().getString(Constants.PARAM_URL))) {
            title.m_visible = true;
            title.m_title = null;
            title.m_leftActionHint = null;
            title.m_titleControls = LayoutInflater.from(getActivity()).inflate((int) R.layout.title_event, (ViewGroup) null);
            title.m_titleControls.findViewById(R.id.right_btn).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.JOIN_QIUSHOU).end();
                    WebViewFragment.this.pushFragment(new AppShareFragment(), WebViewFragment.this.createArguments("分享", "活动说明"));
                }
            });
            title.m_titleControls.findViewById(R.id.left_action).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    boolean unused = WebViewFragment.this.finishFragment();
                }
            });
        }
    }

    private boolean isJumpable(String url) {
        return url.contains("PromoEvent.php");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (RelativeLayout) inflater.inflate((int) R.layout.webview, (ViewGroup) null);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.e("webviewfragment", "argument:" + getArguments());
        if (getArguments() != null && getArguments().containsKey(Constants.PARAM_URL)) {
            WebView wv = (WebView) getView().findViewById(R.id.webview);
            wv.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (!WebViewFragment.this.getArguments().getBoolean("isGet", false)) {
                        return false;
                    }
                    view.loadUrl(url);
                    return true;
                }
            });
            this.handler.sendEmptyMessage(0);
            wv.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    if (progress == 100) {
                        WebViewFragment.this.getTitleDef().m_title = view.getTitle();
                        WebViewFragment.this.refreshHeader();
                        WebViewFragment.this.handler.sendEmptyMessage(1);
                    }
                    super.onProgressChanged(view, progress);
                }
            });
            wv.getSettings().setJavaScriptEnabled(true);
            if (getArguments().getBoolean("isGet", true)) {
                wv.loadUrl(getArguments().getString(Constants.PARAM_URL));
                return;
            }
            Log.e("webviewfragment", "url:" + getArguments().getString(Constants.PARAM_URL));
            String[] urlpost = getArguments().getString(Constants.PARAM_URL).split("\\?");
            wv.postUrl(urlpost[0], EncodingUtils.getBytes(urlpost[1], "BASE64"));
        }
    }

    public void onResume() {
        super.onResume();
    }
}
