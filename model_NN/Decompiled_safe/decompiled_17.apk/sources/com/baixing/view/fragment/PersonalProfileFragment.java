package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.entity.UserProfile;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.sharing.referral.ReferralFragment;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.LoginUtil;
import com.baixing.util.PerformEvent;
import com.baixing.util.PerformanceTracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.widget.EditUsernameDialogFragment;
import com.quanleimu.activity.R;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

public class PersonalProfileFragment extends BaseFragment implements View.OnClickListener, LoginUtil.LoginListener, Observer {
    private static final int MSG_ANONYMOUS_USER = 9;
    public static final int MSG_CHECK_PROMOTER = 200;
    public static final int MSG_EDIT_USERNAME_SUCCESS = 100;
    private static final int MSG_FORGETPASSWORDVIEW = 8;
    private static final int MSG_GETPERSONALADS = 1;
    private static final int MSG_GETPERSONALLOCATION = 3;
    private static final int MSG_GETPERSONALPROFILE = 2;
    private static final int MSG_GETPERSONALSESSIONS = 4;
    private static final int MSG_LOGINFAIL = 6;
    private static final int MSG_LOGINSUCCESS = 5;
    private static final int MSG_LOGIN_STATUS_CHANGE = 10;
    private static final int MSG_NEWREGISTERVIEW = 7;
    public static final int MSG_SHOW_PROGRESS = 102;
    public static final int MSG_SHOW_TOAST = 101;
    private static final int REQ_EDIT_PROFILE = 1;
    private static final int REQ_REGISTER = 2;
    private EditUsernameDialogFragment editUserDlg;
    private UserProfile up = null;
    /* access modifiers changed from: private */
    public UserBean user = null;

    public void onLoginFail(String message) {
        sendMessage(6, message);
    }

    public void onLoginSucceed(String message) {
        sendMessage(5, message);
    }

    public void onRegisterClicked() {
        sendMessage(7, null);
    }

    public void onForgetClicked() {
        sendMessage(8, null);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = "用户中心";
    }

    public boolean hasGlobalTab() {
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        PerformanceTracker.stamp(PerformEvent.Event.E_Profile_OnCreate);
        super.onCreate(savedInstanceState);
        getArguments();
        this.user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGIN);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_PROFILE_UPDATE);
        ReferralUtil.getInstance().setHandler(this.handler);
        if (savedInstanceState != null) {
            Log.e("QLM", "check if arguments is auto saved ? restore:" + getArguments());
        }
    }

    public void onStackTop(boolean isBack) {
        String cityName = GlobalDataManager.getInstance().getCityName();
        if (cityName == null || "".equals(cityName)) {
            pushFragment(new CityChangeFragment(), createArguments("切换城市", "首页"));
        } else {
            ((TextView) getTitleDef().m_titleControls.findViewById(R.id.title_label_city)).setText(GlobalDataManager.getInstance().getCityName());
        }
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int i = 0;
        if (savedInstanceState != null) {
            Log.d("QLM", "recreate view from saved data." + getClass().getName());
        }
        View v = inflater.inflate((int) R.layout.personalentryview, (ViewGroup) null);
        v.findViewById(R.id.rl_login).setOnClickListener(this);
        v.findViewById(R.id.rl_wosent).setOnClickListener(this);
        v.findViewById(R.id.rl_wofav).setOnClickListener(this);
        v.findViewById(R.id.rl_setting).setOnClickListener(this);
        v.findViewById(R.id.rl_login).setVisibility(GlobalDataManager.getInstance().getAccountManager().isUserLogin() ? 8 : 0);
        View findViewById = v.findViewById(R.id.rl_referral);
        if (!ReferralUtil.isPromoter()) {
            i = 8;
        }
        findViewById.setVisibility(i);
        v.findViewById(R.id.rl_referral).setOnClickListener(this);
        if (this.up != null) {
            fillProfile(this.up, v);
        } else {
            reloadUser(v);
        }
        return v;
    }

    private void reloadUser(View v) {
        if (this.user == null || this.user.getPhone() == null || this.user.getPhone().equals("")) {
            sendMessage(9, null);
        } else {
            new Thread(new Runnable() {
                public void run() {
                    UserProfile profile = (UserProfile) Util.loadDataFromLocate(PersonalProfileFragment.this.getActivity(), "userProfile", UserProfile.class);
                    if (profile != null) {
                        PersonalProfileFragment.this.sendMessage(2, profile);
                    } else {
                        new Thread(new GetPersonalProfileThread()).start();
                    }
                }
            }).start();
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        PerformanceTracker.stamp(PerformEvent.Event.E_Profile_ShowUp);
        super.onResume();
        this.pv = TrackConfig.TrackMobile.PV.MY;
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.MY).append(TrackConfig.TrackMobile.Key.ISLOGIN, GlobalDataManager.getInstance().getAccountManager().isUserLogin()).append(TrackConfig.TrackMobile.Key.USERID, this.user != null ? this.user.getId() : null).end();
    }

    /* access modifiers changed from: protected */
    public void onFragmentBackWithData(int requestCode, Object result) {
        if (requestCode != 1 || result == null) {
            if (!(2 == requestCode && result == null)) {
            }
            return;
        }
        forceUpdate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void
     arg types: [?, ?, int]
     candidates:
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, android.content.DialogInterface$OnCancelListener):void
      com.baixing.activity.BaseFragment.showProgress(java.lang.String, java.lang.String, boolean):void
      com.baixing.activity.BaseFragment.showProgress(int, int, boolean):void */
    private void forceUpdate() {
        showProgress((int) R.string.dialog_title_info, (int) R.string.dialog_message_data_loading, true);
        new Thread(new GetPersonalProfileThread()).start();
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        int i = 0;
        switch (msg.what) {
            case 1:
            case 3:
            case 4:
            default:
                return;
            case 2:
                this.up = (UserProfile) msg.obj;
                if (this.up != null) {
                    fillProfile(this.up, rootView);
                    return;
                }
                return;
            case 6:
                String msgToShow = "登录未成功，请稍后重试！";
                if (msg.obj != null && (msg.obj instanceof String)) {
                    msgToShow = (String) msg.obj;
                }
                ViewUtil.showToast(activity, msgToShow, false);
                return;
            case 7:
                Bundle bundle = createArguments(null, null);
                bundle.putInt("reqestCode", 2);
                pushFragment(new RegisterFragment(), bundle);
                return;
            case 8:
                pushFragment(new ForgetPassFragment(), createArguments(null, null));
                return;
            case 9:
                fillProfile(null, rootView);
                return;
            case 10:
                Boolean login = (Boolean) msg.obj;
                View findViewById = rootView.findViewById(R.id.rl_login);
                if (login.booleanValue()) {
                    i = 8;
                }
                findViewById.setVisibility(i);
                if (!login.booleanValue()) {
                    rootView.findViewById(R.id.rl_referral).setVisibility(8);
                    return;
                }
                return;
            case 100:
                hideProgress();
                this.editUserDlg.dismiss();
                reloadUser(getView());
                return;
            case 101:
                hideProgress();
                ViewUtil.showToast(activity, msg.obj.toString(), false);
                return;
            case 102:
                showSimpleProgress();
                return;
            case 200:
                if (ReferralUtil.isPromoter()) {
                    rootView.findViewById(R.id.rl_referral).setVisibility(0);
                    return;
                } else {
                    rootView.findViewById(R.id.rl_referral).setVisibility(8);
                    return;
                }
            case 10002:
                getView().findViewById(R.id.userInfoLayout).setVisibility(8);
                return;
        }
    }

    private class GetPersonalProfileThread implements Runnable {
        private GetPersonalProfileThread() {
        }

        public void run() {
            if (PersonalProfileFragment.this.user != null) {
                try {
                    String upJson = BaseApiCommand.createCommand(PersonalProfileFragment.this.user.getId(), true, null).executeSync(GlobalDataManager.getInstance().getApplicationContext());
                    if (!TextUtils.isEmpty(upJson)) {
                        UserProfile profile = UserProfile.from(upJson);
                        if (PersonalProfileFragment.this.getActivity() != null) {
                            Util.saveDataToLocate(PersonalProfileFragment.this.getActivity(), "userProfile", profile);
                            PersonalProfileFragment.this.sendMessage(2, profile);
                        }
                    }
                } catch (Throwable t) {
                    Log.d("QLM", "request user profile failed, caused by " + t.getMessage());
                }
                PersonalProfileFragment.this.hideProgress();
            }
        }
    }

    private void fillProfile(UserProfile up2, View userInfoView) {
        View activity = userInfoView;
        if (up2 == null) {
            activity.findViewById(R.id.userInfoLayout).setVisibility(8);
            return;
        }
        if (up2.nickName != null) {
            ((TextView) activity.findViewById(R.id.userInfoNickname)).setText(up2.nickName);
        } else {
            ((TextView) activity.findViewById(R.id.userInfoNickname)).setText("");
        }
        if (up2.createTime != null && !up2.equals("")) {
            try {
                Date date = new Date(Long.parseLong(up2.createTime) * 1000);
                ((TextView) activity.findViewById(R.id.userInfoJoinDays)).setText(new SimpleDateFormat("yyyy年MM月", Locale.SIMPLIFIED_CHINESE).format(date) + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        activity.findViewById(R.id.userInfoLayout).setVisibility(0);
        activity.findViewById(R.id.userInfo_editUsername_btn).setOnClickListener(this);
    }

    private void pushPersonalPostFragment(int type) {
        Bundle bundle = createArguments(null, null);
        bundle.putInt(MyAdFragment.TYPE_KEY, type);
        pushFragment(new MyAdFragment(), bundle);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_login /*2131165449*/:
                pushFragment(new LoginFragment(), createArguments("登录", ""));
                return;
            case R.id.rl_wosent /*2131165475*/:
                pushPersonalPostFragment(0);
                return;
            case R.id.rl_wofav /*2131165476*/:
                Bundle bundle = createArguments(null, null);
                bundle.putInt(MyAdFragment.TYPE_KEY, 3);
                pushFragment(new MyAdFragment(), bundle);
                return;
            case R.id.rl_setting /*2131165478*/:
                pushFragment(new SettingFragment(), null);
                return;
            case R.id.rl_referral /*2131165480*/:
                pushFragment(new ReferralFragment(), null);
                return;
            default:
                return;
        }
    }

    public void handleSearch() {
        pushFragment(new SearchFragment(), getArguments());
    }

    public int getEnterAnimation() {
        return 0;
    }

    public void onDestroy() {
        super.onDestroy();
        BxMessageCenter.defaultMessageCenter().removeObserver(this);
    }

    public int getExitAnimation() {
        return 0;
    }

    public void update(Observable observable, Object data) {
        if (data instanceof BxMessageCenter.IBxNotification) {
            BxMessageCenter.IBxNotification note = (BxMessageCenter.IBxNotification) data;
            if (IBxNotificationNames.NOTIFICATION_LOGIN.equals(note.getName()) || IBxNotificationNames.NOTIFICATION_LOGOUT.equals(note.getName())) {
                this.user = (UserBean) note.getObject();
                this.up = null;
                reloadUser(getView());
                sendMessage(10, IBxNotificationNames.NOTIFICATION_LOGIN.equals(note.getName()) ? Boolean.TRUE : Boolean.FALSE);
            } else if (IBxNotificationNames.NOTIFICATION_PROFILE_UPDATE.equals(note.getName())) {
                this.up = (UserProfile) note.getObject();
            }
        }
    }
}
