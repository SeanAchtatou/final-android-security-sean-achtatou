package com.baixing.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Filterss;
import com.baixing.entity.values;
import com.baixing.jsonutil.JsonUtil;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.TextUtil;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.view.fragment.MultiLevelSelectionFragment;
import com.baixing.widget.CustomDialogBuilder;
import com.quanleimu.activity.R;
import com.tencent.tauth.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FilterFragment extends BaseFragment implements View.OnClickListener, BaseApiCommand.Callback {
    public static final int MSG_DIALOG_BACK_WITH_DATA = 12;
    private static final int MSG_LOAD_DATA_FAILD = 2;
    private static final int MSG_LOAD_DATA_SUCCED = 1;
    private static final int MSG_MULTISEL_BACK = 0;
    private static final int MSG_UPDATE_KEYWORD = 3;
    private String areaJson = null;
    public String categoryEnglishName = "";
    private EditText ed_sift;
    private Map<String, EditText> editors = new HashMap();
    public String filterJson = null;
    public List<Filterss> listFilterss = new ArrayList();
    /* access modifiers changed from: private */
    public PostParamsHolder parametersHolder;
    private boolean progressHidden = false;
    private Map<Integer, TextView> selector = new HashMap();
    public int temp;

    public void onStackTop(boolean isBack) {
        super.onStackTop(isBack);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.categoryEnglishName = getArguments().getString("categoryEnglishName");
        PostParamsHolder params = (PostParamsHolder) getArguments().getSerializable("filterResult");
        getArguments().remove("filterResult");
        this.parametersHolder = (PostParamsHolder) getArguments().getSerializable("savedFilter");
        if (this.parametersHolder == null) {
            this.parametersHolder = new PostParamsHolder();
        }
        if (params != null) {
            this.parametersHolder.merge(params);
        }
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate((int) R.layout.sifttest, (ViewGroup) null);
        this.ed_sift = (EditText) v.findViewById(R.id.edsift);
        this.ed_sift.clearFocus();
        v.findViewById(R.id.filter_confirm).setOnClickListener(this);
        v.findViewById(R.id.filter_clear).setOnClickListener(this);
        return v;
    }

    public void onResume() {
        super.onResume();
        this.pv = TrackConfig.TrackMobile.PV.LISTINGFILTER;
        Tracker.getInstance().pv(this.pv).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).end();
        Pair<Long, String> filterPair = Util.loadJsonAndTimestampFromLocate(getActivity(), "saveFilterss" + this.categoryEnglishName + GlobalDataManager.getInstance().getCityEnglishName());
        this.filterJson = (String) filterPair.second;
        long filterTime = ((Long) filterPair.first).longValue();
        Pair<Long, String> areaPair = Util.loadJsonAndTimestampFromLocate(getActivity(), "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName());
        this.areaJson = (String) areaPair.second;
        long areaTime = ((Long) areaPair.first).longValue();
        boolean filterNeeded = this.filterJson == null || this.filterJson.length() == 0 || TextUtil.FULL_DAY + filterTime < System.currentTimeMillis() / 1000;
        boolean areasNeeded = this.areaJson == null || this.areaJson.length() == 0 || TextUtil.FULL_DAY + areaTime < System.currentTimeMillis() / 1000;
        if (filterNeeded || areasNeeded) {
            showSimpleProgress();
            this.progressHidden = false;
            executeGetFilterCmd(filterNeeded, areasNeeded);
            return;
        }
        loadSiftFrame(getView());
    }

    public void handleRightAction() {
        Iterator<String> ite = this.parametersHolder.keyIterator();
        HashMap<String, String> data = new HashMap<>();
        while (ite.hasNext()) {
            String key = ite.next();
            data.put(key, this.parametersHolder.getData(key));
        }
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.LISTING_FILTERSUBMIT).append(data).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, this.categoryEnglishName).end();
        finishFragment(this.fragmentRequestCode, this.parametersHolder);
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = "更多筛选";
        title.m_leftActionHint = "返回";
        title.m_rightActionHint = "确定";
    }

    public void onPause() {
        super.onPause();
        collectValue(getArguments());
    }

    private void collectValue(Bundle bundle) {
        String uiValue;
        String str = this.ed_sift.getText().toString().trim();
        for (int i = 0; i < this.editors.size(); i++) {
            String key = this.editors.keySet().toArray()[i].toString();
            String textInput = this.editors.get(key).getText().toString();
            if (this.editors.get(key).getTag() != null) {
                uiValue = textInput + this.editors.get(key).getTag();
            } else {
                uiValue = textInput;
            }
            if (textInput.length() > 0) {
                this.parametersHolder.put(key, uiValue, textInput);
            } else {
                this.parametersHolder.remove(key);
            }
        }
        if (!str.equals("")) {
            this.parametersHolder.put("", str, str);
        } else {
            this.parametersHolder.remove("");
        }
    }

    public void onFragmentBackWithData(int message, Object obj) {
        handleBackWithData(message, obj);
    }

    private void handleBackWithData(int message, Object obj) {
        if (message == 0 && (obj instanceof MultiLevelSelectionFragment.MultiLevelItem)) {
            this.selector.get(Integer.valueOf(this.temp)).setText(((MultiLevelSelectionFragment.MultiLevelItem) obj).txt);
            if (((MultiLevelSelectionFragment.MultiLevelItem) obj).id != null && !((MultiLevelSelectionFragment.MultiLevelItem) obj).id.equals("")) {
                this.parametersHolder.put(this.listFilterss.get(this.temp).getName(), ((MultiLevelSelectionFragment.MultiLevelItem) obj).txt, ((MultiLevelSelectionFragment.MultiLevelItem) obj).id);
            } else if (this.temp < this.listFilterss.size() && this.listFilterss.get(this.temp).toString().length() > 0) {
                this.parametersHolder.remove(this.listFilterss.get(this.temp).getName());
            }
        }
    }

    private void executeGetFilterCmd(boolean filterNeeded, boolean areasNeeded) {
        if (filterNeeded) {
            ApiParams params = new ApiParams();
            params.addParam("categoryId", this.categoryEnglishName);
            params.addParam("cityId", GlobalDataManager.getInstance().getCityEnglishName());
            BaseApiCommand.createCommand("Meta.filter/", true, params).execute(GlobalDataManager.getInstance().getApplicationContext(), this);
        }
        if (areasNeeded) {
            ApiParams areaParam = new ApiParams();
            areaParam.addParam("cityId", GlobalDataManager.getInstance().getCityId());
            BaseApiCommand.createCommand("City.areas/", true, areaParam).execute(GlobalDataManager.getInstance().getApplicationContext(), this);
        }
    }

    private void loadSiftFrame(View rootView) {
        View v;
        this.listFilterss = JsonUtil.getFilters(this.filterJson).getFilterssList();
        this.listFilterss.add(JsonUtil.getTopAreas(this.areaJson));
        LinearLayout ll_meta = (LinearLayout) rootView.findViewById(R.id.meta);
        LayoutInflater inflater = LayoutInflater.from(rootView.getContext());
        ll_meta.removeAllViews();
        if (this.listFilterss == null || this.listFilterss.size() == 0) {
            View v2 = new View(ll_meta.getContext());
            v2.setLayoutParams(new LinearLayout.LayoutParams(-1, 0));
            ll_meta.addView(v2);
        } else {
            for (int i = 0; i < this.listFilterss.size(); i++) {
                if (this.listFilterss.get(i) != null) {
                    String paramsKey = this.listFilterss.get(i).getName();
                    if (this.listFilterss.get(i).getControlType().equals("select")) {
                        v = inflater.inflate(R.layout.item_filter_select, (ViewGroup) null);
                        ((TextView) v.findViewById(R.id.filter_label)).setText(this.listFilterss.get(i).getDisplayName());
                        TextView tvmeta = (TextView) v.findViewById(R.id.filter_value);
                        tvmeta.setTag(paramsKey);
                        if (this.parametersHolder.containsKey(paramsKey)) {
                            String preValue = this.parametersHolder.getData(paramsKey);
                            String preLabel = this.parametersHolder.getUiData(paramsKey);
                            boolean valid = false;
                            if (preLabel != null) {
                                tvmeta.setText(preLabel);
                                valid = true;
                            }
                            if (!valid) {
                                List<values> values = this.listFilterss.get(i).getValuesList();
                                int z = 0;
                                while (true) {
                                    if (z >= values.size()) {
                                        break;
                                    } else if (values.get(z).getValue().equals(preValue)) {
                                        tvmeta.setText(this.listFilterss.get(i).getLabelsList().get(z).getLabel());
                                        valid = true;
                                        break;
                                    } else {
                                        z++;
                                    }
                                }
                                if (!valid) {
                                    tvmeta.setText("请选择");
                                }
                            }
                        } else {
                            tvmeta.setText("请选择");
                        }
                        this.selector.put(Integer.valueOf(i), tvmeta);
                        v.setTag(Integer.valueOf(i));
                        v.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                FilterFragment.this.temp = Integer.parseInt(v.getTag().toString());
                                Bundle bundle = FilterFragment.this.createArguments(null, null);
                                bundle.putAll(FilterFragment.this.getArguments());
                                bundle.putInt("temp", FilterFragment.this.temp);
                                bundle.putString(Constants.PARAM_TITLE, FilterFragment.this.listFilterss.get(FilterFragment.this.temp).getDisplayName());
                                bundle.putString("back", "筛选");
                                Filterss fss = FilterFragment.this.listFilterss.get(FilterFragment.this.temp);
                                if (fss.getLevelCount() > 0) {
                                    bundle.putString("selectedValue", FilterFragment.this.parametersHolder.getData(fss.getName()));
                                    ArrayList<MultiLevelSelectionFragment.MultiLevelItem> items = new ArrayList<>();
                                    MultiLevelSelectionFragment.MultiLevelItem head = new MultiLevelSelectionFragment.MultiLevelItem();
                                    head.txt = "全部";
                                    head.id = "";
                                    items.add(head);
                                    for (int i = 0; i < fss.getLabelsList().size(); i++) {
                                        MultiLevelSelectionFragment.MultiLevelItem t = new MultiLevelSelectionFragment.MultiLevelItem();
                                        t.txt = fss.getLabelsList().get(i).getLabel();
                                        t.id = fss.getValuesList().get(i).getValue();
                                        items.add(t);
                                    }
                                    bundle.putInt("reqestCode", 0);
                                    bundle.putSerializable("items", items);
                                    bundle.putInt("maxLevel", fss.getLevelCount() - 1);
                                    CustomDialogBuilder cdb = new CustomDialogBuilder(FilterFragment.this.getActivity(), FilterFragment.this.getHandler(), bundle);
                                    if (fss.getName().contains("价格")) {
                                        cdb.setHasRangeSelection(fss.getUnit());
                                    }
                                    cdb.start();
                                }
                            }
                        });
                    } else {
                        v = inflater.inflate(R.layout.item_filter_edit, (ViewGroup) null);
                        ((TextView) v.findViewById(R.id.filter_label)).setText(this.listFilterss.get(i).getDisplayName());
                        EditText tvmeta2 = (EditText) v.findViewById(R.id.filter_input);
                        TextView unitmeta = (TextView) v.findViewById(R.id.filter_unit);
                        String unitS = this.listFilterss.get(i).getUnit();
                        if (unitS != null) {
                            unitmeta.setText(unitS);
                            tvmeta2.setTag(unitS);
                        }
                        this.editors.put(this.listFilterss.get(i).getName(), tvmeta2);
                        String preValue2 = this.parametersHolder.getData(paramsKey);
                        if (preValue2 != null) {
                            tvmeta2.setText(preValue2);
                        }
                    }
                    View border = new View(rootView.getContext());
                    border.setLayoutParams(new LinearLayout.LayoutParams(-1, getResources().getDimensionPixelSize(R.dimen.filter_gap), 1.0f));
                    ll_meta.addView(v);
                    ll_meta.addView(border);
                }
            }
        }
        String keyWords = this.parametersHolder.getData("");
        if (keyWords != null) {
            sendMessage(3, keyWords);
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 1:
                if (this.filterJson != null && this.filterJson.length() > 0 && this.areaJson != null && this.areaJson.length() > 0) {
                    hideProgress();
                    if (rootView != null) {
                        loadSiftFrame(rootView);
                        return;
                    }
                    return;
                }
                return;
            case 2:
                if (!this.progressHidden) {
                    hideProgress();
                    this.progressHidden = true;
                    ViewUtil.showToast(activity, "服务当前不可用，请稍后重试！", false);
                    return;
                }
                return;
            case 3:
                ((TextView) rootView.findViewById(R.id.edsift)).setText((String) msg.obj);
                return;
            case 12:
                Bundle bundle = (Bundle) msg.obj;
                handleBackWithData(bundle.getInt("reqestCode"), bundle.getSerializable("lastChoise"));
                return;
            default:
                return;
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.filter_clear) {
            this.ed_sift.setText("");
            this.parametersHolder.remove("");
            for (TextView selectValue : this.selector.values()) {
                selectValue.setText("请选择");
                this.parametersHolder.remove((String) selectValue.getTag());
            }
            for (String key : this.editors.keySet()) {
                this.editors.get(key).setText("");
                this.parametersHolder.remove(key);
            }
        } else if (v.getId() == R.id.filter_confirm) {
            handleRightAction();
        }
    }

    public int getEnterAnimation() {
        return R.anim.zoom_enter;
    }

    public int getExitAnimation() {
        return R.anim.zoom_exit;
    }

    public boolean hasGlobalTab() {
        return false;
    }

    public void onNetworkDone(String apiName, String responseData) {
        if (apiName.equals("Meta.filter/")) {
            this.filterJson = responseData;
            Util.saveJsonAndTimestampToLocate(getAppContext(), "saveFilterss" + this.categoryEnglishName + GlobalDataManager.getInstance().getCityEnglishName(), responseData, System.currentTimeMillis() / 1000);
        }
        if (apiName.equals("City.areas/")) {
            this.areaJson = responseData;
            Util.saveJsonAndTimestampToLocate(getAppContext(), "saveAreas" + GlobalDataManager.getInstance().getCityEnglishName(), responseData, System.currentTimeMillis() / 1000);
        }
        sendMessage(1, null);
    }

    public void onNetworkFail(String apiName, ApiError error) {
        sendMessage(2, null);
    }
}
