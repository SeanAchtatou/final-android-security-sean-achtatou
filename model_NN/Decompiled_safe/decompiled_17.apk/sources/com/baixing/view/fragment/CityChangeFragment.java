package com.baixing.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.data.LocationManager;
import com.baixing.entity.BXLocation;
import com.baixing.entity.CityDetail;
import com.baixing.jsonutil.LocateJsonData;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class CityChangeFragment extends BaseFragment implements LocationManager.onLocationFetchedListener, View.OnClickListener {
    private final int MSG_LOCATING_TIMEOUT = -252645136;
    protected View activeView;
    public String backPageName = "返回";
    public String cityEnglishName = "";
    public String cityName = "";
    public ImageView ivGPSChoose;
    public LinearLayout linearListInfo;
    public LinearLayout linearProvinces;
    public List<String> listCityName = new ArrayList();
    public List<CityDetail> listHotCity = new ArrayList();
    private boolean located = false;
    public ScrollView parentView;
    /* access modifiers changed from: private */
    public RelativeLayout relativeProvinces;
    public EditText searchField;
    protected Stack<chooseStage> stackStage = new Stack<>();
    public String title = "选择城市";

    protected class chooseStage {
        public String backString;
        public View effectiveView;
        public String titleString;

        protected chooseStage() {
        }
    }

    public void initTitle(BaseFragment.TitleDef title2) {
        title2.m_visible = true;
        String cityName2 = GlobalDataManager.getInstance().cityName;
        if (cityName2 != null && cityName2.length() > 0) {
            title2.m_leftActionHint = "返回";
        }
        title2.m_title = "选择城市";
    }

    public int[] excludedOptionMenus() {
        return new int[]{0};
    }

    public boolean handleBack() {
        if (this.stackStage.size() != 0) {
            this.parentView.removeView(this.activeView);
            chooseStage stage = this.stackStage.pop();
            this.activeView = stage.effectiveView;
            this.parentView.addView(this.activeView);
            this.backPageName = stage.backString;
            this.title = stage.titleString;
            BaseFragment.TitleDef title2 = getTitleDef();
            title2.m_leftActionHint = "返回";
            title2.m_title = this.title;
            refreshHeader();
            if (this.stackStage.size() == 0) {
                this.searchField.setVisibility(0);
            }
            return true;
        } else if (this.cityName != null && !"".equals(this.cityName)) {
            return false;
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle((int) R.string.dialog_title_info).setMessage((int) R.string.dialog_message_confirm_exit).setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    CityChangeFragment.this.getActivity().finish();
                }
            });
            builder.create().show();
            return true;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate((int) R.layout.citychange, (ViewGroup) null);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.listHotCity = LocateJsonData.hotCityList();
        GlobalDataManager.getInstance().setListHotCity(this.listHotCity);
        this.cityName = GlobalDataManager.getInstance().getCityName();
        this.parentView = (ScrollView) rootView.findViewById(R.id.llParentView);
        this.linearListInfo = (LinearLayout) rootView.findViewById(R.id.linearList);
        this.activeView = this.linearListInfo;
        ((TextView) rootView.findViewById(R.id.tvGPSCityName)).setText("定位中...");
        this.ivGPSChoose = (ImageView) rootView.findViewById(R.id.ivGPSChoose);
        this.ivGPSChoose.setVisibility(4);
        LinearLayout linearHotCities = (LinearLayout) rootView.findViewById(R.id.linearHotCities);
        for (int i = 0; i < this.listHotCity.size(); i++) {
            CityDetail city = this.listHotCity.get(i);
            View v = inflater.inflate((int) R.layout.item_citychange, (ViewGroup) null);
            ImageView ivChoose = (ImageView) v.findViewById(R.id.ivChoose);
            ivChoose.setImageBitmap(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.gou));
            ((TextView) v.findViewById(R.id.tvCateName)).setText(city.getName());
            ivChoose.setVisibility(4);
            v.setTag(new Pair(city, "hotcity"));
            v.setOnClickListener(this);
            if (i == this.listHotCity.size() - 1) {
                v.findViewById(R.id.citychange_border).setVisibility(8);
            }
            linearHotCities.addView(v);
        }
        for (int i2 = 0; i2 < this.listHotCity.size(); i2++) {
            if (this.cityName.equals(this.listHotCity.get(i2).getName())) {
                linearHotCities.getChildAt(i2).findViewById(R.id.ivChoose).setVisibility(0);
            } else {
                linearHotCities.getChildAt(i2).findViewById(R.id.ivChoose).setVisibility(8);
            }
        }
        ((RelativeLayout) rootView.findViewById(R.id.linear2Other)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CityChangeFragment.this.searchField.setVisibility(8);
                CityChangeFragment.this.parentView.scrollTo(0, 0);
                chooseStage stage = new chooseStage();
                stage.effectiveView = CityChangeFragment.this.activeView;
                stage.titleString = CityChangeFragment.this.title;
                stage.backString = CityChangeFragment.this.backPageName;
                CityChangeFragment.this.stackStage.push(stage);
                CityChangeFragment.this.parentView.removeView(CityChangeFragment.this.activeView);
                CityChangeFragment.this.title = "选择省份";
                BaseFragment.TitleDef titleDef = CityChangeFragment.this.getTitleDef();
                titleDef.m_leftActionHint = CityChangeFragment.this.backPageName;
                titleDef.m_title = CityChangeFragment.this.title;
                CityChangeFragment.this.refreshHeader();
                if (CityChangeFragment.this.linearProvinces == null) {
                    LayoutInflater inflater = LayoutInflater.from(CityChangeFragment.this.getActivity());
                    RelativeLayout unused = CityChangeFragment.this.relativeProvinces = (RelativeLayout) inflater.inflate((int) R.layout.citylist, (ViewGroup) null);
                    CityChangeFragment.this.linearProvinces = (LinearLayout) CityChangeFragment.this.relativeProvinces.findViewById(R.id.llcitylist);
                    CityChangeFragment.initShengMap();
                    HashMap<String, List<CityDetail>> shengMap = GlobalDataManager.getInstance().getShengMap();
                    String[] shengArray = (String[]) shengMap.keySet().toArray(new String[0]);
                    for (int i = 0; i < shengMap.size(); i++) {
                        View vTemp = inflater.inflate((int) R.layout.item_citychange, (ViewGroup) null);
                        ((ImageView) vTemp.findViewById(R.id.ivChoose)).setImageBitmap(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.arrow));
                        ((TextView) vTemp.findViewById(R.id.tvCateName)).setText(shengArray[i]);
                        vTemp.setTag(shengArray[i]);
                        vTemp.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                CityChangeFragment.this.parentView.scrollTo(0, 0);
                                chooseStage stage = new chooseStage();
                                stage.effectiveView = CityChangeFragment.this.activeView;
                                stage.titleString = CityChangeFragment.this.title;
                                stage.backString = CityChangeFragment.this.backPageName;
                                CityChangeFragment.this.stackStage.push(stage);
                                CityChangeFragment.this.parentView.removeView(CityChangeFragment.this.activeView);
                                CityChangeFragment.this.title = "选择城市";
                                BaseFragment.TitleDef title = CityChangeFragment.this.getTitleDef();
                                title.m_leftActionHint = CityChangeFragment.this.backPageName;
                                title.m_title = CityChangeFragment.this.title;
                                CityChangeFragment.this.refreshHeader();
                                LayoutInflater inflater = LayoutInflater.from(CityChangeFragment.this.getActivity());
                                RelativeLayout relativeCitys = (RelativeLayout) inflater.inflate((int) R.layout.citylist, (ViewGroup) null);
                                LinearLayout linearCities = (LinearLayout) relativeCitys.findViewById(R.id.llcitylist);
                                List<CityDetail> list2Sheng = GlobalDataManager.getInstance().getShengMap().get(v.getTag().toString());
                                for (int i = 0; i < list2Sheng.size(); i++) {
                                    CityDetail city = (CityDetail) list2Sheng.get(i);
                                    View vCity = inflater.inflate((int) R.layout.item_citychange, (ViewGroup) null);
                                    ((TextView) vCity.findViewById(R.id.tvCateName)).setText(city.getName());
                                    ((ImageView) vCity.findViewById(R.id.ivChoose)).setVisibility(8);
                                    vCity.setTag(new Pair(city, "othercity"));
                                    vCity.setOnClickListener(CityChangeFragment.this);
                                    linearCities.addView(vCity);
                                }
                                CityChangeFragment.this.activeView = relativeCitys;
                                CityChangeFragment.this.parentView.addView(CityChangeFragment.this.activeView);
                            }
                        });
                        CityChangeFragment.this.linearProvinces.addView(vTemp);
                    }
                }
                CityChangeFragment.this.activeView = CityChangeFragment.this.relativeProvinces;
                CityChangeFragment.this.parentView.addView(CityChangeFragment.this.activeView);
            }
        });
        this.searchField = (EditText) rootView.findViewById(R.id.etSearchCity);
        this.parentView.findViewById(R.id.filteredList).setVisibility(8);
        this.searchField.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                ViewGroup filteredList = (ViewGroup) CityChangeFragment.this.parentView.findViewById(R.id.filteredList);
                View unfilteredList = CityChangeFragment.this.parentView.findViewById(R.id.unfilteredList);
                String filterKeyword = s.toString().trim();
                filteredList.removeAllViews();
                if (filterKeyword.length() == 0) {
                    unfilteredList.setVisibility(0);
                    filteredList.setVisibility(8);
                    return;
                }
                unfilteredList.setVisibility(8);
                filteredList.setVisibility(0);
                LayoutInflater inflater = LayoutInflater.from(CityChangeFragment.this.getActivity());
                List<CityDetail> filteredCityDetails = CityChangeFragment.this.getFilteredCityDetails(filterKeyword);
                for (int i = 0; i < filteredCityDetails.size(); i++) {
                    CityDetail city = (CityDetail) filteredCityDetails.get(i);
                    View v = inflater.inflate((int) R.layout.item_citychange, (ViewGroup) null);
                    ImageView ivChoose = (ImageView) v.findViewById(R.id.ivChoose);
                    ivChoose.setImageBitmap(GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.arrow));
                    ((TextView) v.findViewById(R.id.tvCateName)).setText(city.getName());
                    ivChoose.setVisibility(4);
                    v.setTag(new Pair(city, "search"));
                    v.setOnClickListener(CityChangeFragment.this);
                    filteredList.addView(v);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        return rootView;
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        View rView;
        TextView tvGPSCityName;
        hideProgress();
        if (msg.what == -252645136 && !this.located && (rView = getView()) != null && (tvGPSCityName = (TextView) rView.findViewById(R.id.tvGPSCityName)) != null) {
            tvGPSCityName.setText("定位失败");
        }
    }

    public void onStackTop(boolean isBack) {
        GlobalDataManager.getInstance().getLocationManager().addLocationListener(this);
        sendMessageDelay(-252645136, null, 3000);
    }

    public void onResume() {
        this.pv = TrackConfig.TrackMobile.PV.SELECTCITY;
        Tracker.getInstance().pv(this.pv).end();
        super.onResume();
    }

    public void onPause() {
        GlobalDataManager.getInstance().getLocationManager().removeLocationListener(this);
        super.onPause();
    }

    public void onLocationFetched(BXLocation location) {
    }

    public void onGeocodedLocationFetched(BXLocation location) {
        View rootView = getView();
        if (rootView != null) {
            TextView tvGPSCityName = (TextView) rootView.findViewById(R.id.tvGPSCityName);
            if (location == null || !location.geocoded) {
                tvGPSCityName.setText("定位失败");
                this.located = false;
                return;
            }
            this.located = true;
            if (tvGPSCityName != null) {
                RelativeLayout linearGpsCity = (RelativeLayout) rootView.findViewById(R.id.linearGpsCityItem);
                String subCity = "";
                String cityName2 = "";
                CityDetail cityDetail = null;
                Iterator i$ = GlobalDataManager.getInstance().getListCityDetails().iterator();
                while (true) {
                    if (!i$.hasNext()) {
                        break;
                    }
                    CityDetail city = i$.next();
                    if (city.getName() != null) {
                        if (!location.cityName.contains(city.getName())) {
                            if (location.subCityName != null && location.subCityName.contains(city.getName())) {
                                subCity = city.getName();
                                cityDetail = city;
                                break;
                            }
                        } else {
                            cityName2 = city.getName();
                            cityDetail = city;
                        }
                    }
                }
                if (cityDetail != null) {
                    linearGpsCity.setTag(new Pair(cityDetail, "gpscity"));
                    linearGpsCity.setOnClickListener(this);
                    if (!subCity.equals("")) {
                        tvGPSCityName.setText(subCity);
                    } else if (!cityName2.equals("")) {
                        tvGPSCityName.setText(cityName2);
                    } else {
                        tvGPSCityName.setText(cityDetail.name);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void initShengMap() {
        if (GlobalDataManager.getInstance().getShengMap() == null || GlobalDataManager.getInstance().getShengMap().size() == 0) {
            List<String> listShengName = new ArrayList<>();
            HashMap<String, List<CityDetail>> shengMap = new HashMap<>();
            List<CityDetail> cityDetails = GlobalDataManager.getInstance().getListCityDetails();
            for (int i = 0; i < cityDetails.size(); i++) {
                String sheng = cityDetails.get(i).getSheng();
                if (!sheng.equals("直辖市")) {
                    if (listShengName == null || listShengName.size() == 0) {
                        listShengName.add(sheng);
                    } else if (!listShengName.contains(sheng)) {
                        listShengName.add(sheng);
                    }
                }
            }
            for (int j = 0; j < listShengName.size(); j++) {
                List<CityDetail> listCD = new ArrayList<>();
                for (int i2 = 0; i2 < cityDetails.size(); i2++) {
                    if (cityDetails.get(i2).getSheng().equals(listShengName.get(j))) {
                        listCD.add(cityDetails.get(i2));
                    }
                }
                shengMap.put(listShengName.get(j), listCD);
            }
            GlobalDataManager.getInstance().setShengMap(shengMap);
        }
    }

    private static boolean checkContainsShortPinyin(String englishName, String shortPinyin) {
        int shortPos = 0;
        for (int i = 0; i < englishName.length(); i++) {
            if (englishName.charAt(i) == shortPinyin.charAt(shortPos) && (shortPos = shortPos + 1) >= shortPinyin.length()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public List<CityDetail> getFilteredCityDetails(String filterKeyword) {
        List<CityDetail> allCities = GlobalDataManager.getInstance().getListCityDetails();
        List<CityDetail> filteredCities = new ArrayList<>(16);
        List<CityDetail> shortFilteredCities = new ArrayList<>(8);
        for (CityDetail city : allCities) {
            if (city.name.startsWith(filterKeyword) || city.englishName.startsWith(filterKeyword)) {
                filteredCities.add(city);
            } else if (filterKeyword.length() < 6 && checkContainsShortPinyin(city.englishName, filterKeyword)) {
                shortFilteredCities.add(city);
            }
        }
        filteredCities.addAll(shortFilteredCities);
        return filteredCities;
    }

    public void onClick(View v) {
        Pair<CityDetail, String> pair = (Pair) v.getTag();
        CityDetail city = (CityDetail) pair.first;
        String block = (String) pair.second;
        if (city.getClass().equals(CityDetail.class)) {
            GlobalDataManager.getInstance().setCityEnglishName(city.getEnglishName());
            GlobalDataManager.getInstance().setCityName(city.getName());
            GlobalDataManager.getInstance().setCityId(city.getId());
            Util.saveDataToFile(getActivity(), null, "cityName", city.getName().getBytes());
            finishFragment();
            Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.CITY_SELECT).append(TrackConfig.TrackMobile.Key.CITY, city.getEnglishName()).append(TrackConfig.TrackMobile.Key.BLOCK, block).append(TrackConfig.TrackMobile.Key.GPS_RESULT, this.located).append(TrackConfig.TrackMobile.Key.SEARCHKEYWORD, this.searchField.getText().toString().trim()).end();
        }
    }

    public boolean hasGlobalTab() {
        return false;
    }
}
