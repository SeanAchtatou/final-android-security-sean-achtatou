package com.baixing.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baixing.activity.BaseActivity;
import com.baixing.activity.BaseFragment;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.UserBean;
import com.baixing.entity.UserProfile;
import com.baixing.message.BxMessageCenter;
import com.baixing.message.IBxNotificationNames;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import com.baixing.util.Util;
import com.baixing.util.ViewUtil;
import com.baixing.widget.CommentsDialog;
import com.baixing.widget.EditUsernameDialogFragment;
import com.quanleimu.activity.R;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import java.util.Observable;
import java.util.Observer;

public class SettingFragment extends BaseFragment implements View.OnClickListener, EditUsernameDialogFragment.ICallback, Observer {
    public static final int MSG_PROFILE_UPDATE = 1;
    /* access modifiers changed from: private */
    public long debugShowFlag = 0;
    /* access modifiers changed from: private */
    public long debugShowFlagTime = 0;
    /* access modifiers changed from: private */
    public UserProfile profile;
    /* access modifiers changed from: private */
    public UserBean user;

    static /* synthetic */ long access$108(SettingFragment x0) {
        long j = x0.debugShowFlag;
        x0.debugShowFlag = 1 + j;
        return j;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BxMessageCenter.defaultMessageCenter().registerObserver(this, IBxNotificationNames.NOTIFICATION_LOGOUT);
    }

    public View onInitializeView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View setmain = inflater.inflate((int) R.layout.setmain, (ViewGroup) null);
        ((RelativeLayout) setmain.findViewById(R.id.setFlowOptimize)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.setBindID)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.setCheckUpdate)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.setAbout)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.setFeedback)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.commentsUs)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.bindSharingAccount)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.setChangeUserName)).setOnClickListener(this);
        ((RelativeLayout) setmain.findViewById(R.id.resetPassword)).setOnClickListener(this);
        ((Button) setmain.findViewById(R.id.debugBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long nowTime = System.currentTimeMillis();
                if (nowTime - SettingFragment.this.debugShowFlagTime > 1000) {
                    long unused = SettingFragment.this.debugShowFlagTime = nowTime;
                    long unused2 = SettingFragment.this.debugShowFlag = 0;
                    return;
                }
                SettingFragment.access$108(SettingFragment.this);
                if (SettingFragment.this.debugShowFlag > 1) {
                    SettingFragment.this.pushFragment(new DebugFragment(), null);
                }
            }
        });
        refreshUI(setmain);
        boolean isLogin = GlobalDataManager.getInstance().getAccountManager().isUserLogin();
        if (this.profile == null && isLogin) {
            loadProfile();
        }
        return setmain;
    }

    private void loadProfile() {
        new Thread(new Runnable() {
            public void run() {
                UserProfile profile;
                UserProfile profile2 = (UserProfile) Util.loadDataFromLocate(SettingFragment.this.getActivity(), "userProfile", UserProfile.class);
                if (profile2 == null) {
                    String upString = BaseApiCommand.createCommand(SettingFragment.this.user.getId(), true, null).executeSync(SettingFragment.this.getAppContext());
                    if (!TextUtils.isEmpty(upString) && (profile = UserProfile.from(upString)) != null) {
                        Util.saveDataToLocate(SettingFragment.this.getActivity(), "userProfile", profile);
                        UserProfile unused = SettingFragment.this.profile = profile;
                        SettingFragment.this.sendMessageDelay(1, null, 100);
                        return;
                    }
                    return;
                }
                UserProfile unused2 = SettingFragment.this.profile = profile2;
                SettingFragment.this.sendMessageDelay(1, null, 100);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void refreshUI(View rootView) {
        char c;
        int i;
        int i2 = 0;
        if (rootView != null) {
            this.user = GlobalDataManager.getInstance().getAccountManager().getCurrentUser();
            TextView bindIdTextView = (TextView) rootView.findViewById(R.id.setBindIdtextView);
            Button btnLogout = (Button) rootView.findViewById(R.id.btn_logout);
            if (this.user == null || this.user.getPhone() == null || this.user.getPhone().equals("")) {
                bindIdTextView.setText((int) R.string.label_login);
                btnLogout.setVisibility(8);
            } else {
                rootView.findViewById(R.id.setBindID).setVisibility(8);
                btnLogout.setVisibility(0);
                btnLogout.setOnClickListener(this);
            }
            TextView flowOptimizeTw = (TextView) rootView.findViewById(R.id.setFlowOptimizeTw);
            String[] stringArray = getResources().getStringArray(R.array.item_flow_optimize);
            if (GlobalDataManager.isTextMode()) {
                c = 1;
            } else {
                c = 0;
            }
            flowOptimizeTw.setText(stringArray[c]);
            if (this.profile == null || !GlobalDataManager.getInstance().getAccountManager().isUserLogin()) {
                rootView.findViewById(R.id.setChangeUserName).setVisibility(8);
            } else {
                rootView.findViewById(R.id.setChangeUserName).setVisibility(0);
                ((TextView) rootView.findViewById(R.id.userNameTxt)).setText(this.profile.nickName);
            }
            boolean isLogin = GlobalDataManager.getInstance().getAccountManager().isUserLogin();
            RelativeLayout relativeLayout = (RelativeLayout) rootView.findViewById(R.id.resetPassword);
            if (isLogin) {
                i = 0;
            } else {
                i = 8;
            }
            relativeLayout.setVisibility(i);
            RelativeLayout relativeLayout2 = (RelativeLayout) rootView.findViewById(R.id.bindSharingAccount);
            if (!isLogin) {
                i2 = 8;
            }
            relativeLayout2.setVisibility(i2);
        }
    }

    public void onResume() {
        super.onResume();
        this.pv = TrackConfig.TrackMobile.PV.SETTINGS;
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.SETTINGS).end();
        refreshUI(getView());
    }

    public void initTitle(BaseFragment.TitleDef title) {
        title.m_visible = true;
        title.m_title = "设置";
        title.m_leftActionHint = "完成";
    }

    public int[] excludedOptionMenus() {
        return new int[]{1};
    }

    private void logoutAction() {
        new AlertDialog.Builder(getActivity()).setTitle((int) R.string.dialog_confirm_logout).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Util.logout();
                UserProfile unused = SettingFragment.this.profile = null;
                SettingFragment.this.refreshUI(SettingFragment.this.getView());
                ViewUtil.showToast(SettingFragment.this.getActivity(), "已退出", false);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_LOGOUT_CONFIRM).end();
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_LOGOUT_CANCEL).end();
            }
        }).create().show();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setChangeUserName /*2131165554*/:
                EditUsernameDialogFragment editUserDlg = new EditUsernameDialogFragment();
                editUserDlg.callback = this;
                editUserDlg.show(getFragmentManager(), (String) null);
                return;
            case R.id.userNameTxt /*2131165555*/:
            case R.id.txt_changePass /*2131165557*/:
            case R.id.setBindIdtextView /*2131165560*/:
            case R.id.setFlowOptimizeTw /*2131165562*/:
            default:
                ViewUtil.showToast(getActivity(), "no action", false);
                return;
            case R.id.resetPassword /*2131165556*/:
                Bundle b = createArguments("修改密码", null);
                b.putString(ForgetPassFragment.Forget_Type, "edit");
                pushFragment(new ForgetPassFragment(), b);
                return;
            case R.id.bindSharingAccount /*2131165558*/:
                pushFragment(new BindSharingFragment(), createArguments("绑定转发帐号", null));
                return;
            case R.id.setBindID /*2131165559*/:
            case R.id.btn_logout /*2131165567*/:
                if (this.user == null || this.user.getPhone() == null || this.user.getPhone().equals("")) {
                    pushFragment(new LoginFragment(), createArguments(null, "用户中心"));
                    Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_LOGIN).end();
                    return;
                }
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_LOGOUT).end();
                logoutAction();
                return;
            case R.id.setFlowOptimize /*2131165561*/:
                showFlowOptimizeDialog();
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_PICMODE).end();
                return;
            case R.id.setCheckUpdate /*2131165563*/:
                UmengUpdateAgent.update(GlobalDataManager.getInstance().getApplicationContext());
                UmengUpdateAgent.setUpdateAutoPopup(false);
                UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
                    public void onUpdateReturned(int updateStatus, UpdateResponse updateInfo) {
                        if (SettingFragment.this.getActivity() != null) {
                            String msgToShow = null;
                            switch (updateStatus) {
                                case 0:
                                    UmengUpdateAgent.showUpdateDialog(SettingFragment.this.getActivity(), updateInfo);
                                    break;
                                case 1:
                                    msgToShow = "你所使用的就是最新版本";
                                    break;
                                case 2:
                                    msgToShow = "为了节省您的流量，请在wifi下更新";
                                    break;
                                case 3:
                                    msgToShow = "网络超时，请检查网络";
                                    break;
                            }
                            if (msgToShow != null) {
                                ViewUtil.showToast(SettingFragment.this.getActivity(), msgToShow, false);
                            }
                        }
                    }
                });
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_CHECKUPDATE).end();
                return;
            case R.id.setAbout /*2131165564*/:
                pushFragment(new AboutUsFragment(), null);
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_ABOUT).end();
                return;
            case R.id.commentsUs /*2131165565*/:
                new CommentsDialog((BaseActivity) getActivity()).show();
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_COMMENTSUS).end();
                return;
            case R.id.setFeedback /*2131165566*/:
                pushFragment(new FeedbackFragment(), createArguments("反馈信息", null));
                Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.SETTINGS_FEEDBACK).end();
                return;
        }
    }

    private void showFlowOptimizeDialog() {
        new AlertDialog.Builder(getActivity()).setTitle((int) R.string.label_flow_optimize).setSingleChoiceItems((int) R.array.item_flow_optimize, GlobalDataManager.isTextMode() ? 1 : 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                boolean z = true;
                if (i != 1) {
                    z = false;
                }
                GlobalDataManager.setTextMode(z);
                SettingFragment.this.refreshUI(SettingFragment.this.getView());
                dialog.dismiss();
                ViewUtil.showToast(SettingFragment.this.getActivity(), "已切换至" + SettingFragment.this.getResources().getStringArray(R.array.item_flow_optimize)[i], false);
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public boolean hasGlobalTab() {
        return false;
    }

    public void onEditSucced(String newUserName) {
        if (this.profile != null) {
            this.profile.nickName = newUserName;
        }
        BxMessageCenter.defaultMessageCenter().postNotification(IBxNotificationNames.NOTIFICATION_PROFILE_UPDATE, this.profile);
        sendMessage(1, null);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message msg, Activity activity, View rootView) {
        switch (msg.what) {
            case 1:
                refreshUI(rootView);
                return;
            default:
                super.handleMessage(msg, activity, rootView);
                return;
        }
    }

    public void update(Observable observable, Object data) {
        if ((data instanceof BxMessageCenter.IBxNotification) && IBxNotificationNames.NOTIFICATION_LOGOUT.equals(((BxMessageCenter.IBxNotification) data).getName()) && getView() != null && getView().findViewById(R.id.setChangeUserName).getVisibility() == 0) {
            finishFragment();
        }
    }

    public void onDestroy() {
        BxMessageCenter.defaultMessageCenter().removeObserver(this);
        super.onDestroy();
    }
}
