package com.baixing.view;

import java.util.HashMap;

public class AdViewHistory {
    private static final Integer R = new Integer(1);
    private static AdViewHistory instance;
    private HashMap<String, Integer> mapper = new HashMap<>();

    private AdViewHistory() {
    }

    public static AdViewHistory getInstance() {
        if (instance == null) {
            instance = new AdViewHistory();
        }
        return instance;
    }

    public void markRead(String vadId) {
        this.mapper.put(vadId, R);
    }

    public boolean isReaded(String vadId) {
        return this.mapper.containsKey(vadId);
    }

    public void clearHistory() {
        this.mapper.clear();
    }
}
