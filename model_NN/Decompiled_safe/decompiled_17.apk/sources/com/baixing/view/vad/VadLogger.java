package com.baixing.view.vad;

import android.content.Context;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.tracking.LogData;
import com.baixing.tracking.TrackConfig;
import com.baixing.tracking.Tracker;
import java.util.Date;

public class VadLogger {
    public static final void trackPageView(Ad detail, Context context) {
        if ((detail == null || !GlobalDataManager.getInstance().isMyAd(detail)) && detail.isValidMessage()) {
            Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.VIEWAD).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).end();
            return;
        }
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.MYVIEWAD).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).append(TrackConfig.TrackMobile.Key.ADSENDERID, GlobalDataManager.getInstance().getAccountManager().getMyId(context)).append(TrackConfig.TrackMobile.Key.ADSTATUS, detail.getValueByKey("status")).end();
    }

    public static final void event(TrackConfig.TrackMobile.BxEvent event, TrackConfig.TrackMobile.Key key, TrackConfig.TrackMobile.Value value) {
        LogData log = Tracker.getInstance().event(event);
        if (!(key == null || value == null)) {
            log.append(key, value);
        }
        log.end();
    }

    public static final void trackMofifyEvent(Ad detail, TrackConfig.TrackMobile.BxEvent event) {
        String tmpCateName = detail.data.get("categoryEnglishName");
        String secondCategoryName = tmpCateName != null ? tmpCateName : "empty categoryEnglishName";
        String tmpInsertedTime = detail.data.get("insertedTime");
        long postedSeconds = -1;
        if (tmpInsertedTime != null) {
            postedSeconds = (new Date().getTime() / 1000) - Long.valueOf(tmpInsertedTime).longValue();
        }
        Tracker.getInstance().event(event).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, secondCategoryName).append(TrackConfig.TrackMobile.Key.POSTEDSECONDS, postedSeconds).end();
    }

    public static final void trackLikeUnlike(Ad detail) {
        if (detail != null) {
            Tracker.getInstance().event(GlobalDataManager.getInstance().isFav(detail) ? TrackConfig.TrackMobile.BxEvent.VIEWAD_UNFAV : TrackConfig.TrackMobile.BxEvent.VIEWAD_FAV).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).end();
        }
    }

    public static final void trackViewMap(Ad detail) {
        Tracker.getInstance().pv(TrackConfig.TrackMobile.PV.VIEWADMAP).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).end();
    }

    public static final void trackContactEvent(TrackConfig.TrackMobile.BxEvent event, Ad detail) {
        Tracker.getInstance().event(event).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).end();
    }

    public static final void trackShowMapEvent(Ad detail) {
        Tracker.getInstance().event(TrackConfig.TrackMobile.BxEvent.VIEWAD_SHOWMAP).append(TrackConfig.TrackMobile.Key.SECONDCATENAME, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME)).append(TrackConfig.TrackMobile.Key.ADID, detail.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID)).end();
    }
}
