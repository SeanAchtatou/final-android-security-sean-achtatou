package com.baixing.view.vad;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.baixing.adapter.VadImageAdapter;
import com.baixing.data.GlobalDataManager;
import com.baixing.entity.Ad;
import com.baixing.network.api.ApiParams;
import com.baixing.util.TextUtil;
import com.baixing.util.post.PostCommonValues;
import com.baixing.widget.HorizontalListView;
import com.lambergar.verticalviewpager.PagerAdapter;
import com.lambergar.verticalviewpager.VerticalViewPager;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class VadPageController implements View.OnTouchListener, VadImageAdapter.IImageProvider, View.OnClickListener {
    private static final String[] INVISIBLE_META = {"价格", "地点", PostCommonValues.STRING_AREA, "查看", "来自", PostCommonValues.STRING_DETAIL_POSITION, "发布人"};
    /* access modifiers changed from: private */
    public ActionCallback callback;
    /* access modifiers changed from: private */
    public Ad detail;
    private Bitmap failBk;
    private HashMap<String, List<Integer>> imageMap = new HashMap<>();
    /* access modifiers changed from: private */
    public WeakReference<View> loadingMorePage;
    private int originalIndex;
    private List<View> pages = new ArrayList();
    private WeakReference<View> viewRoot;

    public interface ActionCallback {
        Ad getAd(int i);

        boolean hasMore();

        void onLoadMore();

        void onPageInitDone(VerticalViewPager verticalViewPager, int i);

        void onPageSwitchTo(int i);

        void onRequestBigPic(int i, Ad ad);

        void onRequestMap();

        void onRequestUserAd(int i, String str);

        int totalPages();
    }

    public VadPageController(View rootView, Ad currentAd, ActionCallback actionCallback, int originalSelect) {
        this.viewRoot = new WeakReference<>(rootView);
        this.detail = currentAd;
        this.callback = actionCallback;
        this.originalIndex = originalSelect;
        initPage();
    }

    private void initPage() {
        View v = this.viewRoot.get();
        if (v != null) {
            VerticalViewPager vp = (VerticalViewPager) v.findViewById(R.id.svDetail);
            vp.setAdapter(new PagerAdapter() {
                public Object instantiateItem(View arg0, int position) {
                    View detail = VadPageController.this.getNewPage(LayoutInflater.from(arg0.getContext()), position);
                    ((VerticalViewPager) arg0).addView(detail, 0);
                    if (position == VadPageController.this.callback.totalPages()) {
                        VadPageController.this.updateLoadingPage(detail, true, false);
                        WeakReference unused = VadPageController.this.loadingMorePage = new WeakReference(detail);
                        VadPageController.this.callback.onLoadMore();
                    } else {
                        VadPageController.this.initContent(detail, VadPageController.this.callback.getAd(position), position, (VerticalViewPager) arg0, false);
                    }
                    return detail;
                }

                public void destroyItem(View arg0, int index, Object arg2) {
                    List<String> listUrl;
                    ((VerticalViewPager) arg0).removeView((View) arg2);
                    Integer pos = (Integer) ((View) arg2).getTag();
                    if (pos.intValue() < VadPageController.this.callback.totalPages() && (listUrl = VadPageController.getImageUrls(VadPageController.this.callback.getAd(pos.intValue()))) != null && listUrl.size() > 0) {
                        GlobalDataManager.getInstance().getImageLoaderMgr().Cancel(listUrl);
                        for (int i = 0; i < listUrl.size(); i++) {
                            VadPageController.this.decreaseImageCount((String) listUrl.get(i), pos.intValue());
                        }
                    }
                    VadPageController.this.removePage(pos.intValue());
                }

                public boolean isViewFromObject(View arg0, Object arg1) {
                    return arg0 == arg1;
                }

                public int getCount() {
                    return (VadPageController.this.callback.hasMore() ? 1 : 0) + VadPageController.this.callback.totalPages();
                }
            });
            vp.setCurrentItem(this.originalIndex);
            vp.setOnPageChangeListener(new VerticalViewPager.OnPageChangeListener() {
                private int currentPage = 0;

                public void onPageSelected(int pos) {
                    this.currentPage = pos;
                    if (pos != VadPageController.this.callback.totalPages()) {
                        Ad unused = VadPageController.this.detail = VadPageController.this.callback.getAd(pos);
                    }
                    VadPageController.this.callback.onPageSwitchTo(pos);
                }

                public void onPageScrolled(int arg0, float arg1, int arg2) {
                    this.currentPage = arg0;
                }

                public void onPageScrollStateChanged(int arg0) {
                    if (arg0 == 0) {
                        List<String> listUrl = VadPageController.getImageUrls(VadPageController.this.detail);
                        if (listUrl == null || listUrl.size() == 0) {
                            ViewGroup currentVG = (ViewGroup) VadPageController.this.getPage(this.currentPage);
                            if (currentVG != null) {
                                View noimage = currentVG.findViewById(R.id.vad_no_img_tip);
                                if (noimage != null) {
                                    noimage.setVisibility(0);
                                }
                                View detail = currentVG.findViewById(R.id.glDetail);
                                if (detail != null) {
                                    detail.setVisibility(8);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        ViewGroup currentVG2 = (ViewGroup) VadPageController.this.getPage(this.currentPage);
                        if (currentVG2 != null) {
                            HorizontalListView glDetail = (HorizontalListView) currentVG2.findViewById(R.id.glDetail);
                            VadImageAdapter adapter = (VadImageAdapter) glDetail.getAdapter();
                            if (adapter != null) {
                                List<String> curLists = adapter.getImages();
                                boolean sameList = true;
                                if (curLists != null && curLists.size() == listUrl.size()) {
                                    int i = 0;
                                    while (true) {
                                        if (i >= curLists.size()) {
                                            break;
                                        }
                                        String cstr = curLists.get(i);
                                        String lstr = (String) listUrl.get(i);
                                        if (cstr != null && cstr.length() > 0 && lstr != null && lstr.length() > 0 && !cstr.equals(lstr)) {
                                            sameList = false;
                                            break;
                                        }
                                        i++;
                                    }
                                } else {
                                    sameList = false;
                                }
                                if (!sameList) {
                                    adapter.setContent(listUrl);
                                    adapter.notifyDataSetChanged();
                                }
                            } else {
                                glDetail.setAdapter((ListAdapter) new VadImageAdapter(currentVG2.getContext(), listUrl, this.currentPage, VadPageController.this));
                            }
                            glDetail.setOnTouchListener(VadPageController.this);
                            glDetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                                    VadPageController.this.callback.onRequestBigPic(arg2, VadPageController.this.detail);
                                }
                            });
                        }
                    }
                }
            });
            vp.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent event) {
                    return false;
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public View getPage(int index) {
        for (int i = 0; i < this.pages.size(); i++) {
            if (this.pages.get(i).getTag() != null && ((Integer) this.pages.get(i).getTag()).intValue() == index) {
                return this.pages.get(i);
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public View getNewPage(LayoutInflater layoutInflater, int index) {
        int i = 0;
        while (true) {
            if (i >= this.pages.size()) {
                break;
            } else if (this.pages.get(i).getTag() == null) {
                this.pages.get(i).setTag(Integer.valueOf(index));
                ViewParent parent = this.pages.get(i).getParent();
                if (parent != null) {
                    if (parent instanceof ViewGroup) {
                        ((ViewGroup) parent).removeView(this.pages.get(i));
                    }
                }
                return this.pages.get(i);
            } else {
                i++;
            }
        }
        View detail2 = layoutInflater.inflate((int) R.layout.gooddetailcontent, (ViewGroup) null);
        detail2.setTag(Integer.valueOf(index));
        this.pages.add(detail2);
        return detail2;
    }

    /* access modifiers changed from: private */
    public void removePage(int index) {
        for (int i = 0; i < this.pages.size(); i++) {
            if (!(this.pages.get(i) == null || this.pages.get(i).getTag() == null || ((Integer) this.pages.get(i).getTag()).intValue() != index)) {
                ((HorizontalListView) this.pages.get(i).findViewById(R.id.glDetail)).setAdapter((ListAdapter) null);
                this.pages.get(i).setTag(null);
                if (this.pages.get(i) instanceof ScrollView) {
                    ((ScrollView) this.pages.get(i)).scrollTo(0, 0);
                }
            }
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        View detailV = this.viewRoot.get() == null ? null : this.viewRoot.get().findViewById(R.id.svDetail);
        switch (event.getAction()) {
            case 0:
            case 2:
                if (detailV != null) {
                    ((VerticalViewPager) detailV).requestDisallowInterceptTouchEvent(true);
                    break;
                }
                break;
            case 1:
            case 4:
                if (detailV != null) {
                    ((VerticalViewPager) detailV).requestDisallowInterceptTouchEvent(false);
                    break;
                }
                break;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void initContent(View contentView, Ad detail2, int pageIndex, VerticalViewPager pager, boolean useRoot) {
        SpannableString spannableString;
        if (detail2 != null) {
            if (useRoot) {
                contentView = contentView.getRootView();
            }
            RelativeLayout relativeLayout = (RelativeLayout) contentView.findViewById(R.id.llgl);
            List<String> listUrl = getImageUrls(detail2);
            RelativeLayout llgl = (RelativeLayout) contentView.findViewById(R.id.llgl);
            int cur = pager != null ? pager.getCurrentItem() : -1;
            if (listUrl == null || listUrl.size() == 0) {
                if (pageIndex == this.originalIndex || pageIndex == cur) {
                    llgl.findViewById(R.id.vad_no_img_tip).setVisibility(0);
                } else {
                    llgl.findViewById(R.id.vad_no_img_tip).setVisibility(8);
                }
                llgl.findViewById(R.id.glDetail).setVisibility(8);
            } else {
                llgl.findViewById(R.id.vad_no_img_tip).setVisibility(8);
                llgl.findViewById(R.id.glDetail).setVisibility(0);
                HorizontalListView glDetail = (HorizontalListView) contentView.findViewById(R.id.glDetail);
                if (pageIndex == this.originalIndex || pageIndex == cur) {
                    glDetail.setAdapter((ListAdapter) new VadImageAdapter(llgl.getContext(), listUrl, pageIndex, this));
                    glDetail.setOnTouchListener(this);
                    final Ad ad = detail2;
                    glDetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                            VadPageController.this.callback.onRequestBigPic(arg2, ad);
                        }
                    });
                }
            }
            TextView txt_tittle = (TextView) contentView.findViewById(R.id.goods_tittle);
            TextView txt_message1 = (TextView) contentView.findViewById(R.id.sendmess1);
            TextView txt_user = (TextView) contentView.findViewById(R.id.user_info);
            setMetaObject(contentView, detail2);
            String title = detail2.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE);
            String description = detail2.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_DESCRIPTION);
            String userNick = TextUtils.isEmpty(detail2.getValueByKey("userNick")) ? "匿名" : detail2.getValueByKey("userNick");
            String userInfo = "发布人：" + userNick;
            String faburen = detail2.getValueByKey("发布人");
            if (!TextUtils.isEmpty(faburen)) {
                userInfo = userInfo + "(" + faburen + ")";
            }
            if ((title == null || title.length() == 0) && description != null) {
                title = description.length() > 40 ? description.substring(0, 40) : description;
            }
            String dateV = detail2.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_DATE);
            if (dateV != null) {
                try {
                    dateV = TextUtil.timeTillNow(Long.parseLong(dateV) * 1000, GlobalDataManager.getInstance().getApplicationContext());
                } catch (Throwable th) {
                    dateV = "";
                }
            }
            if (!TextUtils.isEmpty(dateV)) {
                Resources res = GlobalDataManager.getInstance().getApplicationContext().getResources();
                float fontSize = res.getDimension(R.dimen.font_small) / res.getDisplayMetrics().scaledDensity;
                spannableString = new SpannableString(title + " " + dateV);
                spannableString.setSpan(new AbsoluteSizeSpan((int) fontSize, true), title.length(), spannableString.length(), 18);
                spannableString.setSpan(new ForegroundColorSpan(res.getColor(R.color.vad_meta_label)), title.length(), spannableString.length(), 18);
            } else {
                spannableString = new SpannableString(title);
            }
            txt_message1.setText(appendExtralMetaInfo(detail2, appendPostFromInfo(detail2, description + "\n打电话给我时，请一定说明在百姓网看到的，谢谢！")));
            txt_tittle.setText(spannableString);
            txt_user.setText(userInfo);
            ((TextView) contentView.findViewById(R.id.ad_id)).setText("信息编号：" + detail2.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_ID));
            final Ad ad2 = detail2;
            final String str = userNick;
            txt_user.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int userId = -1;
                    try {
                        userId = Integer.parseInt(ad2.getValueByKey(ApiParams.KEY_USERID));
                    } catch (Throwable th) {
                    }
                    if (userId != -1) {
                        VadPageController.this.callback.onRequestUserAd(userId, str);
                    }
                }
            });
            this.callback.onPageInitDone(pager, pageIndex);
        }
    }

    public void loadMoreFail() {
        final View page = this.loadingMorePage == null ? null : this.loadingMorePage.get();
        if (page != null) {
            page.findViewById(R.id.retry_load_more).setOnClickListener(this);
            page.postDelayed(new Runnable() {
                public void run() {
                    VadPageController.this.updateLoadingPage(page, false, true);
                }
            }, 10);
        }
    }

    public void loadMoreSucced() {
        final View page = this.loadingMorePage == null ? null : this.loadingMorePage.get();
        if (page != null) {
            page.postDelayed(new Runnable() {
                public void run() {
                    VadPageController.this.updateLoadingPage(page, false, false);
                    Integer tag = (Integer) page.getTag();
                    if (tag != null) {
                        VadPageController.this.initContent(page, VadPageController.this.callback.getAd(tag.intValue()), tag.intValue(), null, false);
                    }
                }
            }, 10);
        }
    }

    /* access modifiers changed from: private */
    public void updateLoadingPage(View page, boolean loading, boolean retry) {
        int i;
        int i2;
        int i3 = 0;
        View findViewById = page.findViewById(R.id.loading_more_progress_parent);
        if (loading) {
            i = 0;
        } else {
            i = 8;
        }
        findViewById.setVisibility(i);
        View findViewById2 = page.findViewById(R.id.retry_more_parent);
        if (retry) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        findViewById2.setVisibility(i2);
        View findViewById3 = page.findViewById(R.id.llDetail);
        if (loading || retry) {
            i3 = 8;
        }
        findViewById3.setVisibility(i3);
    }

    public void resetLoadingPage(boolean hasMore) {
        View page;
        PagerAdapter adapter = getContentPageAdapter();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        View rootView = this.viewRoot.get();
        if (rootView != null) {
            if (this.loadingMorePage == null) {
                page = null;
            } else {
                page = this.loadingMorePage.get();
            }
            VerticalViewPager vp = (VerticalViewPager) rootView.findViewById(R.id.svDetail);
            if (vp != null) {
                for (int i = 0; i < vp.getChildCount(); i++) {
                    Ad detaiObj = this.callback.getAd(((Integer) vp.getChildAt(i).getTag()).intValue());
                    if (detaiObj != null) {
                        initContent(vp.getChildAt(i), detaiObj, Integer.valueOf(((Integer) vp.getChildAt(i).getTag()).intValue()).intValue(), vp, false);
                    }
                }
            }
            if (!hasMore && page != null && vp != null) {
                vp.removeView(page);
            }
        }
    }

    /* access modifiers changed from: private */
    public static List<String> getImageUrls(Ad goodDetail) {
        List<String> listUrl = null;
        if (goodDetail.getImageList() != null) {
            listUrl = new ArrayList<>();
            String b = goodDetail.getImageList().getSquare();
            if (b == null) {
                return listUrl;
            }
            String[] c = TextUtil.filterString(b, new char[]{'\\', '\"'}).split(",");
            for (String add : c) {
                listUrl.add(add);
            }
        }
        return listUrl;
    }

    private boolean isVisible(String metaKey) {
        for (String key : INVISIBLE_META) {
            if (metaKey.startsWith(key)) {
                return false;
            }
        }
        return true;
    }

    private String appendExtralMetaInfo(Ad detail2, String description) {
        int splitIndex;
        if (detail2 == null) {
            return description;
        }
        StringBuffer extralInfo = new StringBuffer();
        Iterator i$ = detail2.getMetaData().iterator();
        while (i$.hasNext()) {
            String meta = i$.next();
            if (isVisible(meta) && (splitIndex = meta.indexOf(" ")) != -1) {
                extralInfo.append(meta.substring(splitIndex).trim()).append("，");
            }
        }
        if (extralInfo.length() <= 0) {
            return description;
        }
        extralInfo.deleteCharAt(extralInfo.length() - 1);
        return extralInfo.append("\n\n").append(description).toString();
    }

    private String appendPostFromInfo(Ad detail2, String description) {
        if (detail2 == null) {
            return description;
        }
        String postFrom = detail2.getValueByKey("postMethod");
        if ("api_mobile_android".equals(postFrom) || "api_androidbaixing".equals(postFrom)) {
            return description + "\n来自android客户端";
        }
        if ("baixing_ios".equalsIgnoreCase(postFrom) || "api_iosbaixing".equals(postFrom)) {
            return description + "\n来自iPhone客户端";
        }
        return description;
    }

    public void onShowView(ImageView imageView, String url, String previousUrl, int index) {
        if (this.failBk == null) {
            this.failBk = GlobalDataManager.getInstance().getImageManager().loadBitmapFromResource(R.drawable.home_bg_thumb_2x);
        }
        GlobalDataManager.getInstance().getImageLoaderMgr().showImg(imageView, url, previousUrl, imageView.getContext(), new WeakReference(this.failBk));
        increaseImageCount(url, index);
    }

    private void increaseImageCount(String url, int pos) {
        if (url != null) {
            if (this.imageMap.containsKey(url)) {
                List<Integer> values = this.imageMap.get(url);
                int i = 0;
                while (i < values.size()) {
                    if (((Integer) values.get(i)).intValue() != pos) {
                        i++;
                    } else {
                        return;
                    }
                }
                values.add(Integer.valueOf(pos));
                this.imageMap.put(url, values);
                return;
            }
            List<Integer> value = new ArrayList<>();
            value.add(Integer.valueOf(pos));
            this.imageMap.put(url, value);
        }
    }

    /* access modifiers changed from: private */
    public void decreaseImageCount(String url, int pos) {
        if (url != null && this.imageMap.containsKey(url)) {
            List<Integer> values = this.imageMap.get(url);
            int i = 0;
            while (true) {
                if (i >= values.size()) {
                    break;
                } else if (((Integer) values.get(i)).intValue() == pos) {
                    values.remove(i);
                    break;
                } else {
                    i++;
                }
            }
            if (values.size() == 0) {
                GlobalDataManager.getInstance().getImageManager().forceRecycle(url, true);
                this.imageMap.remove(url);
                return;
            }
            this.imageMap.put(url, values);
        }
    }

    private void setMetaObject(View currentPage, Ad detail2) {
        LinearLayout ll_meta = (LinearLayout) currentPage.findViewById(R.id.meta);
        if (ll_meta != null) {
            ll_meta.removeAllViews();
            LayoutInflater inflater = LayoutInflater.from(currentPage.getContext());
            String price = detail2.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_PRICE);
            if (price != null && !"".equals(price)) {
                View item = createMetaView(inflater, R.drawable.vad_icon_price, "价格:", price, null);
                ll_meta.addView(item);
                ((TextView) item.findViewById(R.id.tvmeta)).setTextColor(currentPage.getResources().getColor(R.color.vad_meta_price));
            }
            String area = detail2.getValueByKey(Ad.EDATAKEYS.EDATAKEYS_AREANAME);
            String address = detail2.getValueByKey(PostCommonValues.STRING_DETAIL_POSITION);
            if (address != null && address.trim().length() > 0) {
                area = address;
            }
            ll_meta.addView(createMetaView(inflater, R.drawable.vad_icon_location, "地区:", area, new View.OnClickListener() {
                public void onClick(View v) {
                    VadPageController.this.callback.onRequestMap();
                }
            }));
        }
    }

    private View createMetaView(LayoutInflater inflater, int iconRes, String label, String value, View.OnClickListener clickListener) {
        View v = inflater.inflate((int) R.layout.item_meta, (ViewGroup) null);
        TextView tvmetatxt = (TextView) v.findViewById(R.id.tvmetatxt);
        TextView tvmeta = (TextView) v.findViewById(R.id.tvmeta);
        ImageView icon = (ImageView) v.findViewById(R.id.meta_icon);
        if (!(iconRes == -1 || icon == null)) {
            icon.setImageResource(iconRes);
            icon.setVisibility(0);
            tvmetatxt.setVisibility(8);
        }
        tvmetatxt.setText(label);
        tvmeta.setText(value);
        if (clickListener != null) {
            v.findViewById(R.id.action_indicator_img).setVisibility(0);
            v.setOnClickListener(clickListener);
        } else {
            v.findViewById(R.id.action_indicator_img).setVisibility(4);
        }
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) v.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new LinearLayout.LayoutParams(-1, -2);
        }
        layoutParams.height = (int) v.getResources().getDimension(R.dimen.vad_meta_item_height);
        v.setLayoutParams(layoutParams);
        return v;
    }

    private PagerAdapter getContentPageAdapter() {
        VerticalViewPager vp;
        View root = this.viewRoot.get();
        if (root == null || (vp = (VerticalViewPager) root.findViewById(R.id.svDetail)) == null) {
            return null;
        }
        return vp.getAdapter();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry_load_more /*2131165330*/:
                retryLoadMore();
                return;
            default:
                return;
        }
    }

    private void retryLoadMore() {
        View page = this.loadingMorePage == null ? null : this.loadingMorePage.get();
        if (page != null) {
            updateLoadingPage(page, true, false);
        }
        this.callback.onLoadMore();
    }
}
