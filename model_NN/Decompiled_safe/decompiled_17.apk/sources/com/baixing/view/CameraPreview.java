package com.baixing.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.List;

@SuppressLint({"NewApi"})
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    public static final String TAG = "CameraPreview";
    private Camera mCamera;
    private SurfaceHolder mHolder = getHolder();
    private boolean rotateDisplay;

    public CameraPreview(Context context) {
        super(context);
        this.mHolder.addCallback(this);
        this.mHolder.setType(3);
    }

    public void setRotateDisplay(boolean rotateDisplay2) {
        this.rotateDisplay = rotateDisplay2;
    }

    public void setCamera(Camera camera) {
        this.mCamera = camera;
        if (this.mCamera != null) {
            requestLayout();
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (this.mHolder.getSurface() != null) {
            try {
                this.mCamera.stopPreview();
            } catch (Exception e) {
            }
            boolean isNewSdk = Build.VERSION.SDK_INT >= 14;
            try {
                this.mCamera.setPreviewDisplay(this.mHolder);
                if (this.rotateDisplay) {
                    try {
                        this.mCamera.setDisplayOrientation(90);
                    } catch (Throwable th) {
                    }
                }
                Camera.Parameters param = this.mCamera.getParameters();
                param.setPictureFormat(256);
                initParam(param, isNewSdk);
                Camera.Size size = param.getPreviewSize();
                if (size == null) {
                    size = getOptimalPreviewSize(param.getSupportedPreviewSizes(), w, h);
                }
                if (size != null) {
                    param.setPreviewSize(size.width, size.height);
                }
                this.mCamera.setParameters(param);
                this.mCamera.startPreview();
            } catch (Exception e2) {
                Log.d(TAG, "Error starting camera preview: " + e2.getMessage());
            }
        }
    }

    private void initParam(Camera.Parameters params, boolean isNewSdk) {
        params.setPreviewFormat(params.getSupportedPreviewFormats().get(0).intValue());
        int min = params.getMinExposureCompensation();
        int max = params.getMaxExposureCompensation();
        if (!(min == 0 && max == 0)) {
            params.setExposureCompensation((params.getMinExposureCompensation() + params.getMaxExposureCompensation()) / 2);
        }
        if (isNewSdk) {
            List<int[]> r = params.getSupportedPreviewFpsRange();
            params.setPreviewFpsRange(r.get(0)[0], r.get(0)[1]);
        }
    }

    private static Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        double targetRatio = ((double) w) / ((double) h);
        if (sizes == null) {
            return null;
        }
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        for (Camera.Size size : sizes) {
            if (Math.abs((((double) size.width) / ((double) size.height)) - targetRatio) <= 0.1d && ((double) Math.abs(size.height - targetHeight)) < minDiff) {
                optimalSize = size;
                minDiff = (double) Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize != null) {
            return optimalSize;
        }
        double minDiff2 = Double.MAX_VALUE;
        for (Camera.Size size2 : sizes) {
            if (((double) Math.abs(size2.height - targetHeight)) < minDiff2) {
                optimalSize = size2;
                minDiff2 = (double) Math.abs(size2.height - targetHeight);
            }
        }
        return optimalSize;
    }
}
