package com.baixing.view;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.baixing.view.CustomizeTabHost;
import com.quanleimu.activity.R;
import java.lang.ref.WeakReference;

public class CustomizePagerManager implements CustomizeTabHost.TabSelectListener {
    /* access modifiers changed from: private */
    public transient PageProvider pProvider;
    /* access modifiers changed from: private */
    public transient PageSelectListener pSelectListener;
    private WeakReference<ViewPager> pagerWrapper;
    /* access modifiers changed from: private */
    public CustomizeTabHost tabHost;

    public interface PageProvider {
        View onCreateView(Context context, int i);
    }

    public interface PageSelectListener {
        void onPageSelect(int i);
    }

    private CustomizePagerManager() {
    }

    public static CustomizePagerManager createManager(String[] tabTitles, int[][] tabIcons, int selectIndex) {
        CustomizePagerManager instance = new CustomizePagerManager();
        CustomizeTabHost.TabIconRes[] tabIconDef = new CustomizeTabHost.TabIconRes[tabTitles.length];
        int i = 0;
        while (i < tabTitles.length) {
            tabIconDef[i] = i < tabIcons.length ? new CustomizeTabHost.TabIconRes(tabIcons[i][1], tabIcons[i][0]) : CustomizeTabHost.TabIconRes.NO_ICON;
            i++;
        }
        instance.tabHost = CustomizeTabHost.createTabHost(selectIndex, tabTitles, tabIconDef);
        return instance;
    }

    public void attachView(View rootView, PageProvider pageProvider, PageSelectListener selectListener) {
        this.pProvider = pageProvider;
        this.pSelectListener = selectListener;
        this.tabHost.attachView(rootView.findViewById(R.id.common_tab_layout), this);
        ViewPager pager = (ViewPager) rootView.findViewById(R.id.tab_content);
        this.pagerWrapper = new WeakReference<>(pager);
        pager.setAdapter(new PagerAdapter() {
            public Object instantiateItem(View arg0, int position) {
                View page = CustomizePagerManager.this.pProvider.onCreateView(arg0.getContext(), position);
                ((ViewPager) arg0).addView(page, 0);
                return page;
            }

            public void destroyItem(View arg0, int index, Object arg2) {
            }

            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            public int getCount() {
                return CustomizePagerManager.this.tabHost.getTabCount();
            }
        });
        pager.setCurrentItem(this.tabHost.getCurrentIndex());
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int arg0) {
                CustomizePagerManager.this.tabHost.showTab(arg0);
                CustomizePagerManager.this.pSelectListener.onPageSelect(arg0);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    public void beforeChange(int currentIndex, int nextIndex) {
    }

    public void afterChange(int newSelectIndex) {
        ViewPager p = this.pagerWrapper.get();
        if (p != null) {
            p.setCurrentItem(newSelectIndex);
        }
    }

    public void deprecatSelect(int currentIndex) {
    }
}
