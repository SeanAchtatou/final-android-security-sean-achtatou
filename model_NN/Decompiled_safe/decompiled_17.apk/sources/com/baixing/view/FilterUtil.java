package com.baixing.view;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.baixing.adapter.VadListAdapter;
import com.baixing.entity.Ad;
import com.baixing.entity.BXLocation;
import com.baixing.entity.Filterss;
import com.baixing.entity.values;
import com.baixing.view.fragment.MultiLevelSelectionFragment;
import com.baixing.view.fragment.PostParamsHolder;
import com.baixing.widget.CustomDialogBuilder;
import com.quanleimu.activity.R;
import java.util.ArrayList;
import java.util.List;

public class FilterUtil {

    public interface FilterSelectListener {
        void onCancel();

        void onItemSelect(MultiLevelSelectionFragment.MultiLevelItem multiLevelItem);
    }

    public static List<VadListAdapter.GroupItem> createFilterGroup(List<Filterss> fss, PostParamsHolder paramsHolder, List<Ad> list) {
        if (list == null || list.size() == 0 || fss == null) {
            return null;
        }
        StringBuffer buf = new StringBuffer();
        if (paramsHolder.containsKey("")) {
            buf.append(paramsHolder.getData("")).append("+");
        }
        int skiped = 0;
        if (fss != null) {
            for (int i = 0; i < fss.size(); i++) {
                Filterss f = fss.get(i);
                if (f.getControlType().equals("select")) {
                    if (skiped < 3) {
                        skiped++;
                    } else if (paramsHolder.containsKey(f.getName())) {
                        buf.append(paramsHolder.getUiData(f.getName())).append("+");
                    }
                } else if (paramsHolder.containsKey(f.getName())) {
                    buf.append(paramsHolder.getUiData(f.getName())).append("+");
                }
            }
        }
        List<VadListAdapter.GroupItem> groups = new ArrayList<>();
        if (buf.length() <= 0) {
            return groups;
        }
        buf.deleteCharAt(buf.length() - 1);
        VadListAdapter.GroupItem g = new VadListAdapter.GroupItem();
        g.filterHint = "\"" + buf.toString() + "\"的搜索结果";
        g.resultCount = list.size();
        groups.add(g);
        return groups;
    }

    public static List<VadListAdapter.GroupItem> createDistanceGroupAndResortAds(List<Ad> ls, BXLocation currentLocation, int[] conditions) {
        ArrayList arrayList = new ArrayList();
        List<VadListAdapter.GroupItem> groups = new ArrayList<>();
        for (int i = 0; i < conditions.length; i++) {
            int count = 0;
            for (int j = 0; j < ls.size(); j++) {
                float[] results = {0.0f, 0.0f, 0.0f};
                String lat = ls.get(j).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_LAT);
                String lon = ls.get(j).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_LON);
                double latD = 0.0d;
                double lonD = 0.0d;
                try {
                    latD = Double.parseDouble(lat);
                    lonD = Double.parseDouble(lon);
                } catch (Throwable th) {
                    Log.d("GetGoodView", "ad nearby lacks lat & lon");
                }
                if (!(latD == 0.0d || lonD == 0.0d || currentLocation == null)) {
                    Location.distanceBetween(latD, lonD, (double) currentLocation.fLat, (double) currentLocation.fLon, results);
                }
                if (results[0] <= ((float) conditions[i])) {
                    Log.d("LIST", "find first item distance > " + conditions[i] + " " + ls.get(j).getValueByKey(Ad.EDATAKEYS.EDATAKEYS_TITLE));
                    arrayList.add(ls.get(j));
                    ls.remove(j);
                    count++;
                }
            }
            if (count > 0) {
                VadListAdapter.GroupItem item = new VadListAdapter.GroupItem();
                item.resultCount = count;
                item.filterHint = "附近" + getDisplayDistance(conditions[i]) + "的信息";
                groups.add(item);
                Log.d("LIST", "group:" + item.filterHint + "(" + item.resultCount + " of " + ls.size());
            }
        }
        if (ls.size() > 0) {
            VadListAdapter.GroupItem item2 = new VadListAdapter.GroupItem();
            item2.resultCount = ls.size();
            item2.filterHint = getDisplayDistance(conditions[conditions.length - 1]) + "以外的信息";
            groups.add(item2);
            Log.d("LIST", "group:" + item2.filterHint + "(" + item2.resultCount + " of " + ls.size());
        }
        arrayList.addAll(ls);
        ls.clear();
        ls.addAll(arrayList);
        return groups;
    }

    private static String getDisplayDistance(int distance) {
        String unit = "米";
        String number = distance + "";
        if (distance > 1000) {
            unit = "公里";
            int kilo_number = distance / 1000;
            number = "" + kilo_number + "." + ((distance - (kilo_number * 1000)) / 100);
        }
        return number + unit;
    }

    public static void startSelect(Context context, MultiLevelSelectionFragment.MultiLevelItem[] customizeItems, Filterss fss, final FilterSelectListener listener) {
        String str = "选择" + fss.getDisplayName();
        int skipCount = customizeItems == null ? 0 : customizeItems.length;
        ArrayList<MultiLevelSelectionFragment.MultiLevelItem> items = new ArrayList<>();
        MultiLevelSelectionFragment.MultiLevelItem head = new MultiLevelSelectionFragment.MultiLevelItem();
        head.txt = "全部";
        head.id = "";
        items.add(head);
        if (skipCount != 0) {
            for (int i = 0; i < skipCount; i++) {
                items.add(customizeItems[i]);
            }
        }
        for (int i2 = 0; i2 < fss.getLabelsList().size(); i2++) {
            MultiLevelSelectionFragment.MultiLevelItem t = new MultiLevelSelectionFragment.MultiLevelItem();
            t.txt = fss.getLabelsList().get(i2).getLabel();
            t.id = fss.getValuesList().get(i2).getValue();
            items.add(t);
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("items", items);
        bundle.putInt("maxLevel", fss.getLevelCount() - 1);
        CustomDialogBuilder cdb = new CustomDialogBuilder(context, new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 12 && msg.obj != null && (msg.obj instanceof Bundle)) {
                    listener.onItemSelect((MultiLevelSelectionFragment.MultiLevelItem) ((Bundle) msg.obj).getSerializable("lastChoise"));
                }
            }
        }, bundle);
        if (fss.getName().contains("价格")) {
            cdb.setHasRangeSelection(fss.getUnit());
        }
        cdb.start();
    }

    private static CharSequence[] getItemList(List<MultiLevelSelectionFragment.MultiLevelItem> items) {
        CharSequence[] list = new CharSequence[items.size()];
        int i = 0;
        for (MultiLevelSelectionFragment.MultiLevelItem item : items) {
            list[i] = item.txt;
            i++;
        }
        return list;
    }

    public static void updateFilter(PostParamsHolder holder, MultiLevelSelectionFragment.MultiLevelItem item, String name) {
        if (item != null && name != null) {
            if (item.id == null || item.id.equals("")) {
                holder.remove(name);
            } else {
                holder.put(name, item.txt, item.id);
            }
        }
    }

    public static void loadFilterBar(List<Filterss> filters, PostParamsHolder paramsHolder, View[] actionItems) {
        int MAX = actionItems.length;
        int added = 0;
        for (int i = 0; i < filters.size() && added < MAX; i++) {
            Filterss f = filters.get(i);
            if (f.getControlType().equals("select")) {
                View item = actionItems[added];
                item.setTag(f);
                item.setVisibility(0);
                initFilterLable((TextView) item.findViewById(R.id.filter_name), paramsHolder, f);
                added++;
            }
        }
        while (added < MAX) {
            actionItems[added].setVisibility(8);
            added++;
        }
    }

    public static void updateFilterLabel(View[] actionItems, String label, Filterss f) {
        if (actionItems != null) {
            View[] arr$ = actionItems;
            int len$ = arr$.length;
            int i$ = 0;
            while (i$ < len$) {
                View v = arr$[i$];
                Object tag = v.getTag();
                if (!(tag instanceof Filterss) || !f.getName().equals(((Filterss) tag).getName())) {
                    i$++;
                } else {
                    ((TextView) v.findViewById(R.id.filter_name)).setText(label);
                    return;
                }
            }
        }
    }

    private static void initFilterLable(TextView text, PostParamsHolder holder, Filterss filter) {
        if (holder == null || !holder.containsKey(filter.getName())) {
            text.setText(filter.getDisplayName());
            return;
        }
        String preValue = holder.getData(filter.getName());
        boolean valid = false;
        String label = holder.getUiData(filter.getName());
        if (label != null) {
            text.setText(label);
            valid = true;
        }
        if (!valid) {
            List<values> values = filter.getValuesList();
            int z = 0;
            while (true) {
                if (z >= values.size()) {
                    break;
                } else if (values.get(z).getValue().equals(preValue)) {
                    text.setText(filter.getLabelsList().get(z).getLabel());
                    valid = true;
                    break;
                } else {
                    z++;
                }
            }
            if (!valid) {
                text.setText(filter.getDisplayName());
            }
        }
    }
}
