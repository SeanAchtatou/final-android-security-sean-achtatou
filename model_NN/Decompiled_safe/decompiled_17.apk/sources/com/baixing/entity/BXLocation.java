package com.baixing.entity;

import java.io.Serializable;

public class BXLocation implements Serializable {
    private static final long serialVersionUID = 0;
    public String address;
    public String adminArea;
    public String cityName;
    public String detailAddress;
    public float fGeoCodedLat;
    public float fGeoCodedLon;
    public float fLat;
    public float fLon;
    public boolean geocoded;
    public String postCode;
    public String subCityName;

    public BXLocation(BXLocation l) {
        this.fLat = 0.0f;
        this.fLon = 0.0f;
        this.geocoded = false;
        this.adminArea = "";
        this.cityName = "";
        this.subCityName = "";
        this.address = "";
        this.detailAddress = "";
        this.postCode = "";
        this.fGeoCodedLat = 0.0f;
        this.fGeoCodedLon = 0.0f;
        this.fLat = l.fLat;
        this.fLon = l.fLon;
        this.geocoded = l.geocoded;
        this.adminArea = l.adminArea;
        this.cityName = l.cityName;
        this.subCityName = l.subCityName;
        this.address = l.address;
        this.detailAddress = l.detailAddress;
        this.postCode = l.postCode;
        this.fGeoCodedLat = l.fGeoCodedLat;
        this.fGeoCodedLon = l.fGeoCodedLon;
    }

    public BXLocation(boolean bDefault) {
        this.fLat = 0.0f;
        this.fLon = 0.0f;
        this.geocoded = false;
        this.adminArea = "";
        this.cityName = "";
        this.subCityName = "";
        this.address = "";
        this.detailAddress = "";
        this.postCode = "";
        this.fGeoCodedLat = 0.0f;
        this.fGeoCodedLon = 0.0f;
        if (bDefault) {
            this.fLat = 31.198486f;
            this.fLon = 121.43502f;
            this.geocoded = true;
            this.fGeoCodedLat = 31.198486f;
            this.fGeoCodedLon = 121.43502f;
            this.adminArea = "上海市";
            this.cityName = "上海";
            this.subCityName = "徐汇区";
            this.address = "广元西路55号";
            this.detailAddress = "上海市徐汇区广元西路55号";
            this.postCode = "200030";
        }
    }

    public BXLocation() {
        this(false);
    }
}
