package com.baixing.entity;

import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.network.api.ApiParams;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChatSession implements Serializable {
    private static final long serialVersionUID = -2300264543803637316L;
    private String adId;
    private String adTitle;
    private int count;
    private String imageUrl;
    private String lastMsg;
    private String oppositeId;
    private String oppositeNick;
    private String sessionId;
    private String timeStamp;

    public static List<ChatSession> fromJson(String json) {
        JSONArray sessions;
        if (json == null || json.equals("")) {
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);
            if (obj != null && obj.has(ThirdpartyTransitActivity.Key_Data) && (sessions = obj.getJSONArray(ThirdpartyTransitActivity.Key_Data)) != null && sessions.length() > 0) {
                List<ChatSession> sessionList = new ArrayList<>();
                for (int i = 0; i < sessions.length(); i++) {
                    JSONObject session = sessions.getJSONObject(i);
                    if (session != null) {
                        ChatSession sc = new ChatSession();
                        if (session.has("session_id")) {
                            sc.setSessionId(session.getString("session_id"));
                        }
                        if (session.has("u_id_other")) {
                            sc.setOppositeId(session.getString("u_id_other"));
                        }
                        if (session.has("u_nick_other")) {
                            sc.setOppositeNick(session.getString("u_nick_other"));
                        }
                        if (session.has("ad_id")) {
                            sc.setAdId(session.getString("ad_id"));
                        }
                        if (session.has("ad_title")) {
                            sc.setAdTitle(session.getString("ad_title"));
                        }
                        if (session.has("count")) {
                            sc.setCount(session.getInt("count"));
                        }
                        if (session.has("lastmessage")) {
                            sc.setLastMsg(session.getString("lastmessage"));
                        }
                        if (session.has(ApiParams.KEY_TIMESTAMP)) {
                            sc.setTimeStamp(String.valueOf(session.getLong(ApiParams.KEY_TIMESTAMP)));
                        }
                        if (session.has("u_image_other")) {
                            sc.setImageUrl(session.getString("u_image_other"));
                        }
                        sessionList.add(sc);
                    }
                }
                return sessionList;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String url) {
        this.imageUrl = url;
    }

    public String getLastMsg() {
        return this.lastMsg;
    }

    public void setLastMsg(String msg) {
        this.lastMsg = msg;
    }

    public String getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(String time) {
        this.timeStamp = time;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String id) {
        this.sessionId = id;
    }

    public String getOppositeId() {
        return this.oppositeId;
    }

    public void setOppositeId(String id) {
        this.oppositeId = id;
    }

    public String getOppositeNick() {
        return this.oppositeNick;
    }

    public void setOppositeNick(String nick) {
        this.oppositeNick = nick;
    }

    public String getAdId() {
        return this.adId;
    }

    public void setAdId(String id) {
        this.adId = id;
    }

    public void setAdTitle(String title) {
        this.adTitle = title;
    }

    public String getAdTitle() {
        return this.adTitle;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }
}
