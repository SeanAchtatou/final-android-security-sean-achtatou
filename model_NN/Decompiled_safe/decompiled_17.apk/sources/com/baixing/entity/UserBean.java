package com.baixing.entity;

import java.io.Serializable;

public class UserBean implements Serializable {
    private static final long serialVersionUID = 1;
    private String id;
    private String password;
    private String phone;

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String toString() {
        return "UserBean [id=" + this.id + ", phone=" + this.phone + ", password=" + this.password + "]";
    }
}
