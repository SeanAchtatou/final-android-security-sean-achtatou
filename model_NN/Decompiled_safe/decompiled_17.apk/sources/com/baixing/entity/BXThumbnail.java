package com.baixing.entity;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class BXThumbnail implements Parcelable {
    public static final Parcelable.Creator<BXThumbnail> CREATOR = new Parcelable.Creator<BXThumbnail>() {
        public BXThumbnail createFromParcel(Parcel in) {
            return new BXThumbnail(in);
        }

        public BXThumbnail[] newArray(int size) {
            return new BXThumbnail[size];
        }
    };
    private String localPath;
    private Bitmap thumbnail;

    private BXThumbnail() {
    }

    private BXThumbnail(Parcel in) {
        this.localPath = in.readString();
        this.thumbnail = (Bitmap) in.readParcelable(null);
    }

    private BXThumbnail(String path, Bitmap thumbnail2) {
        this.localPath = path;
        this.thumbnail = thumbnail2;
    }

    public static BXThumbnail createThumbnail(String path, Bitmap bitmap) {
        return new BXThumbnail(path, bitmap);
    }

    public boolean equals(Object obj) {
        if (obj instanceof BXThumbnail) {
            return ((BXThumbnail) obj).getLocalPath().equals(getLocalPath());
        }
        return false;
    }

    public String getLocalPath() {
        return this.localPath;
    }

    public void setLocalPath(String localPath2) {
        this.localPath = localPath2;
    }

    public Bitmap getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail2) {
        this.thumbnail = thumbnail2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.localPath);
        dest.writeParcelable(this.thumbnail, flags);
    }
}
