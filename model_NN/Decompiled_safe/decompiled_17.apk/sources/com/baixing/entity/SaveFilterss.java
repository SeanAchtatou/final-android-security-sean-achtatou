package com.baixing.entity;

import java.io.Serializable;
import java.util.List;

public class SaveFilterss implements Serializable {
    private static final long serialVersionUID = 1;
    private List<Filterss> list;
    private Long time;

    public List<Filterss> getList() {
        return this.list;
    }

    public void setList(List<Filterss> list2) {
        this.list = list2;
    }

    public Long getTime() {
        return this.time;
    }

    public void setTime(Long time2) {
        this.time = time2;
    }

    public String toString() {
        return "SaveFilterss [list=" + this.list + ", time=" + this.time + "]";
    }
}
