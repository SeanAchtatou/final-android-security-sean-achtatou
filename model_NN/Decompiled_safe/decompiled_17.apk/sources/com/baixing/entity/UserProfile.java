package com.baixing.entity;

import com.baixing.activity.BaseFragment;
import com.baixing.util.post.PostCommonValues;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class UserProfile implements Cloneable, Serializable {
    private static final long serialVersionUID = -5763571391927393475L;
    public String bigImage = "";
    public String createTime = "";
    public String detailLocation = "";
    public String gender = "";
    public String hometown = "";
    public String location = "";
    public String mobile = "";
    public String nickName = "";
    public String qq = "";
    public String resize180Image = "";
    public String smallImage = "";
    public String squareImage = "";
    public String type = "";
    public String userId = "";

    public static UserProfile from(String jsonData) {
        JSONObject images;
        UserProfile up = new UserProfile();
        if (jsonData != null && !jsonData.equals("")) {
            try {
                JSONObject jsonObj = new JSONObject(jsonData).getJSONObject("result");
                if (jsonObj.has(LocaleUtil.INDONESIAN)) {
                    up.userId = jsonObj.getString(LocaleUtil.INDONESIAN);
                }
                if (jsonObj.has("mobile")) {
                    up.mobile = jsonObj.getString("mobile");
                }
                if (jsonObj.has(BaseFragment.ARG_COMMON_TITLE)) {
                    up.nickName = jsonObj.getString(BaseFragment.ARG_COMMON_TITLE);
                }
                if (jsonObj.has("gender")) {
                    up.gender = jsonObj.getString("gender");
                }
                if (jsonObj.has("家乡")) {
                    up.hometown = jsonObj.getString("家乡");
                }
                if (jsonObj.has("所在地")) {
                    up.location = jsonObj.getString("所在地");
                }
                if (jsonObj.has(PostCommonValues.STRING_DETAIL_POSITION)) {
                    up.detailLocation = jsonObj.getString(PostCommonValues.STRING_DETAIL_POSITION);
                }
                if (jsonObj.has("qq")) {
                    up.qq = jsonObj.getString("qq");
                }
                if (jsonObj.has("createdTime")) {
                    up.createTime = jsonObj.getString("createdTime");
                }
                if (jsonObj.has("type")) {
                    up.type = jsonObj.getString("type");
                }
                if (jsonObj.has("images") && (images = jsonObj.getJSONObject("images")) != null) {
                    up.squareImage = images.getString("square");
                    up.bigImage = images.getString("big");
                    up.smallImage = images.getString("small");
                    up.resize180Image = images.getString("resize180");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return up;
    }
}
