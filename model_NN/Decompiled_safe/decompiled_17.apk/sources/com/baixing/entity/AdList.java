package com.baixing.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AdList implements Serializable {
    private static final long serialVersionUID = -2158869923050057462L;
    private List<Ad> data;

    public AdList() {
    }

    public AdList(List<Ad> data__) {
        this.data = data__;
    }

    public List<Ad> getData() {
        return this.data;
    }

    public void setData(List<Ad> data2) {
        this.data = data2;
    }

    public Object clone() {
        List<Ad> temp = new ArrayList<>();
        temp.addAll(this.data);
        return new AdList(temp);
    }
}
