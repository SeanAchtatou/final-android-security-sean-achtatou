package com.baixing.entity;

public class CityDetail {
    public String englishName;
    public String id;
    public String name;
    public String sheng;

    public String getEnglishName() {
        return this.englishName;
    }

    public void setEnglishName(String englishName2) {
        this.englishName = englishName2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getSheng() {
        return this.sheng;
    }

    public void setSheng(String sheng2) {
        this.sheng = sheng2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String toString() {
        return "CityDetail [id=" + this.id + ", englishName=" + this.englishName + ", name=" + this.name + ", sheng=" + this.sheng + "]";
    }
}
