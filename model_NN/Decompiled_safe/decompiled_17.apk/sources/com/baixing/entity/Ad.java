package com.baixing.entity;

import android.text.TextUtils;
import com.baidu.mapapi.MKEvent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Ad implements Serializable {
    private static final long serialVersionUID = 1;
    public HashMap<String, String> data = new HashMap<>();
    public double distance = 0.0d;
    public ImageList imageList = new ImageList();
    public ArrayList<String> metaData = new ArrayList<>();

    public enum EDATAKEYS {
        EDATAKEYS_TITLE(Constants.PARAM_TITLE),
        EDATAKEYS_DESCRIPTION("content"),
        EDATAKEYS_LAT("lat"),
        EDATAKEYS_LON("lng"),
        EDATAKEYS_DATE("createdTime"),
        EDATAKEYS_POST_TIME("insertedTime"),
        EDATAKEYS_ID(LocaleUtil.INDONESIAN),
        EDATAKEYS_CATEGORYENGLISHNAME("categoryId"),
        EDATAKEYS_CITYENGLISHNAME("cityEnglishName"),
        EDATAKEYS_AREANAME("areaNames"),
        EDATAKEYS_MOBILE("mobile"),
        EDATAKEYS_MOBILE_AREA("mobileArea"),
        EDATAKEYS_WANTED("wanted"),
        EDATAKEYS_CONTACT("contact"),
        EDATAKEYS_LINK("link"),
        EDATAKEYS_PRICE("价格");
        
        public String metaKey;

        private EDATAKEYS(String key) {
            this.metaKey = key;
        }
    }

    @JsonIgnore
    public Set<String> getKeys() {
        if (this.data == null) {
            return null;
        }
        return this.data.keySet();
    }

    /* renamed from: com.baixing.entity.Ad$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$baixing$entity$Ad$EDATAKEYS = new int[EDATAKEYS.values().length];

        static {
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_TITLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_DESCRIPTION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_LAT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_LON.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_DATE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_POST_TIME.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_ID.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_CATEGORYENGLISHNAME.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_CITYENGLISHNAME.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_AREANAME.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_MOBILE.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_WANTED.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_CONTACT.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_LINK.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$baixing$entity$Ad$EDATAKEYS[EDATAKEYS.EDATAKEYS_PRICE.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
        }
    }

    @JsonIgnore
    private String getStringByEnum(EDATAKEYS e) {
        switch (AnonymousClass1.$SwitchMap$com$baixing$entity$Ad$EDATAKEYS[e.ordinal()]) {
            case 1:
                return Constants.PARAM_TITLE;
            case 2:
                return "content";
            case 3:
                return "lat";
            case 4:
                return "lng";
            case 5:
                return "createdTime";
            case 6:
                return "insertedTime";
            case 7:
                return LocaleUtil.INDONESIAN;
            case 8:
                return "categoryEnglishName";
            case 9:
                return "cityEnglishName";
            case 10:
                return "areaNames";
            case 11:
                return "mobile";
            case 12:
                return "wanted";
            case 13:
                return "contact";
            case MKEvent.MKEVENT_MAP_MOVE_FINISH:
                return "link";
            case 15:
                return "价格";
            default:
                return "";
        }
    }

    @JsonIgnore
    public String getValueByKey(EDATAKEYS e) {
        return getValueByKey(e.metaKey);
    }

    @JsonIgnore
    public String getValueByKey(String key) {
        if (key.equals("")) {
            return "";
        }
        if (this.data.containsKey(key)) {
            return this.data.get(key);
        }
        return getMetaValueByKey(key);
    }

    @JsonIgnore
    public void setValueByKey(EDATAKEYS e, String value) {
        String key = getStringByEnum(e);
        if (!key.equals("") && !TextUtils.isEmpty(value) && !value.equals("null")) {
            this.data.put(key, value);
        }
    }

    @JsonIgnore
    public void setValueByKey(String key, String value) {
        if (key != null && !key.equals("") && value != null && !value.equals("") && !value.equals("null")) {
            this.data.put(key, value.replaceAll("&amp;", "&").replace("&#039;", "'").replaceAll("&quot;", "\""));
        }
    }

    @JsonIgnore
    private String getMetaValueByKey(String key) {
        for (int i = 0; i < this.metaData.size(); i++) {
            String[] meta = this.metaData.get(i).split(" ");
            if (meta.length >= 2 && meta[0].equals(key)) {
                return meta[1];
            }
        }
        return "";
    }

    public ArrayList<String> getMetaData() {
        return this.metaData;
    }

    public void setMetaData(ArrayList<String> metaData2) {
        this.metaData = metaData2;
    }

    public ImageList getImageList() {
        return this.imageList;
    }

    public void setImageList(ImageList imageList2) {
        this.imageList = imageList2;
    }

    public double getDistance() {
        return this.distance;
    }

    public void setDistance(double distance2) {
        this.distance = distance2;
    }

    public boolean equals(Object o) {
        if (o instanceof Ad) {
            return getValueByKey(EDATAKEYS.EDATAKEYS_ID).equals(((Ad) o).getValueByKey(EDATAKEYS.EDATAKEYS_ID));
        }
        return false;
    }

    @JsonIgnore
    public boolean isValidMessage() {
        return !getValueByKey("status").equals("4") && !getValueByKey("status").equals("20");
    }
}
