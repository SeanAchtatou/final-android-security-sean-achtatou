package com.baixing.entity;

import java.io.Serializable;
import java.util.List;

public class Filterss implements Serializable {
    private static final long serialVersionUID = 1;
    private String controlType;
    private String displayName;
    private List<labels> labelsList;
    private int levelCount = 0;
    private String name;
    private String numeric;
    private boolean required;
    private String unit;
    private List<values> valuesList;

    public void setLevelCount(int levelCount2) {
        this.levelCount = levelCount2;
    }

    public int getLevelCount() {
        return this.levelCount;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName2) {
        this.displayName = displayName2;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit2) {
        this.unit = unit2;
    }

    public String getControlType() {
        return this.controlType;
    }

    public void setControlType(String controlType2) {
        if (controlType2.equals("FilterSelect") || controlType2.equals("FilterRangeSelect")) {
            this.controlType = "select";
        } else {
            this.controlType = controlType2;
        }
    }

    public List<values> getValuesList() {
        return this.valuesList;
    }

    public void setValuesList(List<values> valuesList2) {
        this.valuesList = valuesList2;
    }

    public List<labels> getLabelsList() {
        return this.labelsList;
    }

    public void setLabelsList(List<labels> labelsList2) {
        this.labelsList = labelsList2;
    }

    public String getNumeric() {
        return this.numeric;
    }

    public void setNumeric(String numeric2) {
        this.numeric = numeric2;
    }

    public boolean getRequired() {
        return this.required;
    }

    public void setRequired(boolean required2) {
        this.required = required2;
    }

    public String toString() {
        return "Filterss [name=" + this.name + ", displayName=" + this.displayName + ", unit=" + this.unit + ", controlType=" + this.controlType + ", valuesList=" + this.valuesList + ", labelsList=" + this.labelsList + ", numeric=" + this.numeric + ", required=" + this.required + "]";
    }
}
