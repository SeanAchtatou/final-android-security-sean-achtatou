package com.baixing.entity.compare;

import com.baixing.entity.ChatMessage;
import java.util.Comparator;

public class MsgTimeComparator implements Comparator<ChatMessage> {
    public int compare(ChatMessage lhs, ChatMessage rhs) {
        if (lhs.getTimestamp() < rhs.getTimestamp()) {
            return -1;
        }
        if (lhs.getTimestamp() > rhs.getTimestamp()) {
            return 1;
        }
        return 0;
    }
}
