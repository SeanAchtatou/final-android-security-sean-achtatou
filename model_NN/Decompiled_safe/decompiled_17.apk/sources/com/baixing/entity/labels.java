package com.baixing.entity;

import java.io.Serializable;

public class labels implements Serializable {
    private static final long serialVersionUID = 1;
    public String label;

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label2) {
        this.label = label2;
    }

    public String toString() {
        return this.label;
    }
}
