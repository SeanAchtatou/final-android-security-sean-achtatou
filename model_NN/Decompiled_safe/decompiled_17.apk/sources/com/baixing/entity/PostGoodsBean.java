package com.baixing.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class PostGoodsBean implements Serializable {
    private static final long serialVersionUID = 1;
    private String controlType;
    private String defaultValue;
    private String displayName;
    private List<String> labels;
    private int levelCount;
    private HashMap<String, String> lvmap;
    private int maxSelectNumber;
    private int maxValue;
    private int maxlength;
    private String metaId;
    private String name;
    private int numeric;
    private String required;
    private String subMeta;
    private String unit;
    private List<String> values;

    public void setLevelCount(int count) {
        this.levelCount = count;
    }

    public int getLevelCount() {
        return this.levelCount;
    }

    public void setSubMeta(String sm) {
        this.subMeta = sm;
    }

    public String getSubMeta() {
        return this.subMeta;
    }

    public int getMaxlength() {
        return this.maxlength;
    }

    public void setMaxlength(int maxlength2) {
        this.maxlength = maxlength2;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit2) {
        this.unit = unit2;
    }

    public String getControlType() {
        return this.controlType;
    }

    public void setControlType(String controlType2) {
        if (controlType2.equals("GapValueInput") || controlType2.equals("Input")) {
            controlType2 = "input";
        } else if (controlType2.equals("Select") || controlType2.equals("TagSelect") || controlType2.equals("TreeSelect") || controlType2.equals("Radio") || controlType2.equals("TagRadio")) {
            controlType2 = "select";
        } else if (controlType2.equals("CheckBox") || controlType2.equals("TagCheckbox")) {
            controlType2 = "checkbox";
        }
        this.controlType = controlType2;
    }

    public int getNumeric() {
        return this.numeric;
    }

    public void setNumeric(int numeric2) {
        this.numeric = numeric2;
    }

    public String getRequired() {
        return this.required;
    }

    public void setRequired(String required2) {
        this.required = required2;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName2) {
        this.displayName = displayName2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public List<String> getLabels() {
        return this.labels;
    }

    public void setLabels(List<String> labels2) {
        this.labels = labels2;
    }

    public List<String> getValues() {
        return this.values;
    }

    public void setValues(List<String> values2) {
        this.values = values2;
    }

    public String getDefaultValue() {
        return this.defaultValue;
    }

    public void setDefaultValue(String defaultValue2) {
        this.defaultValue = defaultValue2;
    }

    public HashMap<String, String> getLvmap() {
        this.lvmap = new LinkedHashMap();
        if (!(this.labels == null || this.labels.size() == 0)) {
            for (int i = 0; i < this.labels.size(); i++) {
                this.lvmap.put(this.labels.get(i), this.values.get(i));
            }
        }
        return this.lvmap;
    }

    public int getMaxValue() {
        return this.maxValue;
    }

    public void setMaxValue(int maxValue2) {
        this.maxValue = maxValue2;
    }

    public int getMaxSelectNumber() {
        return this.maxSelectNumber;
    }

    public void setMaxSelectNumber(int maxSelectNumber2) {
        this.maxSelectNumber = maxSelectNumber2;
    }

    public String getMetaId() {
        return this.metaId;
    }

    public void setMetaId(String metaId2) {
        this.metaId = metaId2;
    }

    public String toString() {
        return "PostGoodsBean [unit=" + this.unit + ", controlType=" + this.controlType + ", numeric=" + this.numeric + ", required=" + this.required + ", displayName=" + this.displayName + ", name=" + this.name + ", subMeta=" + this.subMeta + ", labels=" + this.labels + ", values=" + this.values + ", lvmap=" + getLvmap() + "]";
    }
}
