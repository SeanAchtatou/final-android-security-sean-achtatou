package com.baixing.entity;

public class HotData {
    public String keyword;
    public String title;
    public String weburl;

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword2) {
        this.keyword = keyword2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getWeburl() {
        return this.weburl;
    }

    public void setWeburl(String weburl2) {
        this.weburl = weburl2;
    }

    public String toString() {
        return "HotData [keyword=" + this.keyword + ", title=" + this.title + ", weburl=" + this.weburl + "]";
    }
}
