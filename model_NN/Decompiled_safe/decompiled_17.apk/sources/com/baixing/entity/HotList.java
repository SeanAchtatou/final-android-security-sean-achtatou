package com.baixing.entity;

import java.io.Serializable;

public class HotList implements Serializable {
    private static final long serialVersionUID = 1494862624344532411L;
    public HotData hotData = new HotData();
    public String imgUrl = "";
    public int type = -1;

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String imgUrl2) {
        this.imgUrl = imgUrl2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public HotData getHotData() {
        return this.hotData;
    }

    public void setHotData(HotData hotData2) {
        this.hotData = hotData2;
    }

    public String toString() {
        return "HotList [imgUrl=" + this.imgUrl + ", type=" + this.type + ", hotData=" + this.hotData + "]";
    }
}
