package com.baixing.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {
    private static final long serialVersionUID = 1;
    private List<Category> children = new ArrayList();
    private String englishName;
    private String level;
    private String name;
    private Category parent;
    private String parentEnglishName;

    public String getName() {
        return this.name;
    }

    public String getLevel() {
        return this.level;
    }

    public void setLevel(String level2) {
        this.level = level2;
    }

    public String getParentEnglishName() {
        return this.parentEnglishName;
    }

    public void setParentEnglishName(String parentEnglishName2) {
        this.parentEnglishName = parentEnglishName2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getEnglishName() {
        return this.englishName;
    }

    public void setEnglishName(String englishName2) {
        this.englishName = englishName2;
    }

    public List<Category> getChildren() {
        return this.children;
    }

    public void setChildren(List<Category> children2) {
        if (children2 == null) {
            this.children.clear();
        }
        for (Category cat : children2) {
            addChild(cat);
        }
    }

    public void addChild(Category cat) {
        if (cat != null) {
            this.children.add(cat);
            cat.setParent(cat);
            cat.setParentEnglishName(this.englishName);
        }
    }

    public Category getParent() {
        return this.parent;
    }

    public void setParent(Category parent2) {
        this.parent = parent2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public Category findCategoryByEnglishName(String eName) {
        if (eName.equals(this.englishName)) {
            return this;
        }
        for (Category child : this.children) {
            Category cat = child.findCategoryByEnglishName(eName);
            if (cat != null) {
                return cat;
            }
        }
        return null;
    }

    public Category findChildByName(String name2) {
        for (Category child : this.children) {
            if (child.getName().equals(name2)) {
                return child;
            }
        }
        return null;
    }

    public String toString() {
        return this.name;
    }
}
