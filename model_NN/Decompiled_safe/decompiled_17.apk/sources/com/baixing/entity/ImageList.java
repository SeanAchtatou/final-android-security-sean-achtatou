package com.baixing.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;

public class ImageList implements Serializable {
    private static final long serialVersionUID = 1;
    private String big;
    private String small;
    private String square;

    public String getSquare() {
        return this.square;
    }

    @JsonIgnore
    public String[] getSquareArray() {
        return parseForArray(this.square);
    }

    public void setSquare(String square2) {
        if (this.square == null) {
            this.square = square2;
        } else {
            this.square += "," + square2;
        }
    }

    public String getSmall() {
        return this.small;
    }

    @JsonIgnore
    public String[] getSmallArray() {
        return parseForArray(this.small);
    }

    public void setSmall(String small2) {
        if (this.small == null) {
            this.small = small2;
        } else {
            this.small += "," + small2;
        }
    }

    public String getBig() {
        return this.big;
    }

    @JsonIgnore
    public String[] getBigArray() {
        return parseForArray(this.big);
    }

    private String[] parseForArray(String str) {
        if (str != null) {
            return str.split(",");
        }
        return null;
    }

    public void setBig(String big2) {
        if (this.big == null) {
            this.big = big2;
        } else {
            this.big += "," + big2;
        }
    }

    public String toString() {
        return "ImageList [square=" + this.square + ", small=" + this.small + ", big=" + this.big + "]";
    }
}
