package com.baixing.entity;

public class Quota {
    private double balance;
    private boolean multiCity;
    private double postPrice;
    private int postTotal;
    private int postUnused;
    private double refreshPrice;
    private int refreshTotal;
    private int refreshUnused;

    public Quota(int refreshTotal2, int refreshUnused2, double refreshPrice2, int postTotal2, int postUnused2, double postPrice2, double balance2, boolean multiCity2) {
        this.refreshTotal = refreshTotal2;
        this.refreshUnused = refreshUnused2;
        this.refreshPrice = refreshPrice2;
        this.postTotal = postTotal2;
        this.postUnused = postUnused2;
        this.postPrice = postPrice2;
        this.balance = balance2;
        this.multiCity = multiCity2;
    }

    public int getRefreshLimit() {
        return this.refreshTotal;
    }

    public int getRefreshUnused() {
        return this.refreshUnused;
    }

    public double getRefreshPrice() {
        return this.refreshPrice;
    }

    public int getPostLimit() {
        return this.postTotal;
    }

    public int getPostUnused() {
        return this.postUnused;
    }

    public double getPostPrice() {
        return this.postPrice;
    }

    public double getBalance() {
        return this.balance;
    }

    public boolean isMultiCity() {
        return this.multiCity;
    }

    public boolean isRefreshExceed() {
        return this.refreshUnused == 0;
    }

    public boolean isPostExceed() {
        return this.postUnused == 0;
    }

    public boolean isOutOfQuota() {
        return this.multiCity || this.postUnused == 0;
    }

    public String getMessage() {
        if (this.multiCity) {
            return "城市发布超限";
        }
        if (this.postUnused != 0) {
            return "在当前类目还可免费发布" + this.postUnused + "条（共" + this.postTotal + "条）";
        }
        if (this.refreshUnused == 0) {
            return "当前类目的免费发布和免费刷新额度均已用完，请付费发布或刷新。";
        }
        return "当前类目的免费发布额度已用完（共" + this.postTotal + "条），可免费刷新（还剩" + this.refreshUnused + "次）已发布信息或付费发布";
    }

    public String getType() {
        if (this.multiCity) {
            return "城市发布超限";
        }
        if (this.postUnused == 0) {
            return "类目发布超限";
        }
        return "正常发布";
    }
}
