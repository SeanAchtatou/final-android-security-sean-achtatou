package com.baixing.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CityList implements Serializable {
    private static final long serialVersionUID = 1;
    List<CityDetail> listDetails = new ArrayList();

    public List<CityDetail> getListDetails() {
        return this.listDetails;
    }

    public void setListDetails(List<CityDetail> listDetails2) {
        this.listDetails = listDetails2;
    }
}
