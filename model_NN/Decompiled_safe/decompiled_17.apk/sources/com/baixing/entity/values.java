package com.baixing.entity;

import java.io.Serializable;

public class values implements Serializable {
    private static final long serialVersionUID = 1;
    public String value;

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public String toString() {
        return "values [value=" + this.value + "]";
    }
}
