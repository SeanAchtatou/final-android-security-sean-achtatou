package com.baixing.entity;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.message.RMsgInfoDB;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class ChatMessage implements Serializable, Cloneable {
    private static final long serialVersionUID = 2495827266364235867L;
    private String adId;
    private String from;
    private String id;
    private String message;
    private String session;
    private long timestamp;
    private String to;

    public static ChatMessage fromJson(String json) {
        try {
            return fromJson(new JSONObject(json));
        } catch (JSONException e) {
            e.printStackTrace();
            return new ChatMessage();
        }
    }

    public static ChatMessage fromJson(JSONObject obj) {
        ChatMessage chatMsg = new ChatMessage();
        try {
            if (obj.has("ad_id")) {
                chatMsg.setAdId(obj.getString("ad_id"));
            }
            chatMsg.setFrom(obj.getString("u_id_from"));
            chatMsg.setTo(obj.getString("u_id_to"));
            chatMsg.setId(obj.getString(LocaleUtil.INDONESIAN));
            chatMsg.setTimestamp(obj.getLong(ApiParams.KEY_TIMESTAMP));
            chatMsg.setMessage(obj.getString(RMsgInfoDB.TABLE));
            chatMsg.setSession(obj.getString("session_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return chatMsg;
    }

    public String toJson() {
        JSONObject obj = new JSONObject();
        try {
            if (this.adId != null) {
                obj.put("ad_id", getAdId());
            }
            obj.put("u_id_from", getFrom());
            obj.put("u_id_to", getTo());
            obj.put(LocaleUtil.INDONESIAN, getId());
            obj.put(ApiParams.KEY_TIMESTAMP, "" + getTimestamp());
            obj.put(RMsgInfoDB.TABLE, getMessage());
            obj.put("session_id", getSession());
        } catch (Throwable th) {
        }
        return obj.toString();
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from2) {
        this.from = from2;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String to2) {
        this.to = to2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getAdId() {
        return this.adId;
    }

    public void setAdId(String adId2) {
        this.adId = adId2;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp2) {
        this.timestamp = timestamp2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getSession() {
        return this.session;
    }

    public void setSession(String session2) {
        this.session = session2;
    }
}
