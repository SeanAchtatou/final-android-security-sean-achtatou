package com.baixing.qrcode.scan.decoding;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;
import com.baixing.qrcode.scan.R;
import com.baixing.qrcode.scan.ScanActivity;
import com.baixing.qrcode.scan.camera.CameraManager;
import com.baixing.qrcode.scan.view.ViewfinderResultPointCallback;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import java.util.Vector;

public final class CaptureActivityHandler extends Handler {
    private static final String TAG = CaptureActivityHandler.class.getSimpleName();
    private final ScanActivity activity;
    private final DecodeThread decodeThread;
    private State state = State.SUCCESS;

    private enum State {
        PREVIEW,
        SUCCESS,
        DONE
    }

    public CaptureActivityHandler(ScanActivity activity2, Vector<BarcodeFormat> decodeFormats, String characterSet) {
        this.activity = activity2;
        this.decodeThread = new DecodeThread(activity2, decodeFormats, characterSet, new ViewfinderResultPointCallback(activity2.getViewfinderView()));
        this.decodeThread.start();
        CameraManager.get().startPreview();
        restartPreviewAndDecode();
    }

    public void handleMessage(Message message) {
        if (message.what == R.id.lib_qrcode_auto_focus) {
            if (this.state == State.PREVIEW) {
                CameraManager.get().requestAutoFocus(this, R.id.lib_qrcode_auto_focus);
            }
        } else if (message.what == R.id.lib_qrcode_restart_preview) {
            Log.d(TAG, "Got restart preview message");
            restartPreviewAndDecode();
        } else if (message.what == R.id.lib_qrcode_decode_succeeded) {
            Log.d(TAG, "Got decode succeeded message");
            this.state = State.SUCCESS;
            Bundle bundle = message.getData();
            this.activity.handleDecode((Result) message.obj, bundle == null ? null : (Bitmap) bundle.getParcelable(DecodeThread.BARCODE_BITMAP));
        } else if (message.what == R.id.lib_qrcode_decode_failed) {
            this.state = State.PREVIEW;
            CameraManager.get().requestPreviewFrame(this.decodeThread.getHandler(), R.id.lib_qrcode_decode);
        } else if (message.what == R.id.lib_qrcode_return_scan_result) {
            Log.d(TAG, "Got return scan result message");
            this.activity.setResult(-1, (Intent) message.obj);
            this.activity.finish();
        } else if (message.what == R.id.lib_qrcode_launch_product_query) {
            Log.d(TAG, "Got product query message");
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse((String) message.obj));
            intent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
            this.activity.startActivity(intent);
        }
    }

    public void quitSynchronously() {
        this.state = State.DONE;
        CameraManager.get().stopPreview();
        Message.obtain(this.decodeThread.getHandler(), R.id.lib_qrcode_quit).sendToTarget();
        try {
            this.decodeThread.join();
        } catch (InterruptedException e) {
        }
        removeMessages(R.id.lib_qrcode_decode_succeeded);
        removeMessages(R.id.lib_qrcode_decode_failed);
    }

    private void restartPreviewAndDecode() {
        if (this.state == State.SUCCESS) {
            this.state = State.PREVIEW;
            CameraManager.get().requestPreviewFrame(this.decodeThread.getHandler(), R.id.lib_qrcode_decode);
            CameraManager.get().requestAutoFocus(this, R.id.lib_qrcode_auto_focus);
            this.activity.drawViewfinder();
        }
    }
}
