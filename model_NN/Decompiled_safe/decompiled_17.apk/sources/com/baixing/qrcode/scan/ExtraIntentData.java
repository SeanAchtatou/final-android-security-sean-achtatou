package com.baixing.qrcode.scan;

public class ExtraIntentData {
    public static final String EXTRA_COMMON_DATA = "extra.common.data";
    public static final String EXTRA_COMMON_FINISH_CODE = "extra.common.finishCode";
    public static final String EXTRA_COMMON_INTENT = "extra.common.intent";
    public static final String EXTRA_COMMON_IS_THIRD_PARTY = "extra.common.isThirdParty";
    public static final String EXTRA_COMMON_REQUST_CODE = "extra.image.reqcode";
    public static final String EXTRA_COMMON_RESULT_CODE = "extra.common.resultCode";
}
