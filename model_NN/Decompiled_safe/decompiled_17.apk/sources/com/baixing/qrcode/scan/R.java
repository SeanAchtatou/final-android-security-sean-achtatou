package com.baixing.qrcode.scan;

public final class R {

    public static final class color {
        public static final int lib_qrcode_possible_result_points = 2131099648;
        public static final int lib_qrcode_result_view = 2131099649;
        public static final int lib_qrcode_transparent = 2131099650;
        public static final int lib_qrcode_viewfinder_frame = 2131099651;
        public static final int lib_qrcode_viewfinder_laser = 2131099652;
        public static final int lib_qrcode_viewfinder_mask = 2131099653;
    }

    public static final class id {
        public static final int lib_qrcode_auto_focus = 2131165184;
        public static final int lib_qrcode_decode = 2131165185;
        public static final int lib_qrcode_decode_failed = 2131165186;
        public static final int lib_qrcode_decode_succeeded = 2131165187;
        public static final int lib_qrcode_encode_failed = 2131165188;
        public static final int lib_qrcode_encode_succeeded = 2131165189;
        public static final int lib_qrcode_launch_product_query = 2131165190;
        public static final int lib_qrcode_quit = 2131165191;
        public static final int lib_qrcode_restart_preview = 2131165192;
        public static final int lib_qrcode_return_scan_result = 2131165193;
        public static final int lib_qrcode_search_book_contents_failed = 2131165194;
        public static final int lib_qrcode_search_book_contents_succeeded = 2131165195;
        public static final int preview_view = 2131165215;
        public static final int viewfinder_view = 2131165216;
    }

    public static final class layout {
        public static final int activity_capture = 2130903041;
    }

    public static final class raw {
        public static final int lib_qrcode_beep = 2131034112;
    }

    public static final class string {
        public static final int lib_qrcode_app_name = 2131230720;
    }
}
