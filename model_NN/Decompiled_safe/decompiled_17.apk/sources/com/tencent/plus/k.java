package com.tencent.plus;

import android.graphics.Rect;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class k implements ViewTreeObserver.OnGlobalLayoutListener {
    final /* synthetic */ ImageActivity a;

    k(ImageActivity imageActivity) {
        this.a = imageActivity;
    }

    public void onGlobalLayout() {
        this.a.a.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        Rect unused = this.a.p = this.a.h.a();
        this.a.e.a(this.a.p);
    }
}
