package com.tencent.plus;

import android.content.Intent;
import android.util.Log;
import com.tencent.open.HttpStatusException;
import com.tencent.open.NetworkUnavailableException;
import com.tencent.tauth.IRequestListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class i implements IRequestListener {
    final /* synthetic */ ImageActivity a;

    i(ImageActivity imageActivity) {
        this.a = imageActivity;
    }

    public void onMalformedURLException(MalformedURLException malformedURLException, Object obj) {
        malformedURLException.printStackTrace();
        a(0, null);
    }

    public void onJSONException(JSONException jSONException, Object obj) {
        jSONException.printStackTrace();
        a(-1, null);
    }

    public void onIOException(IOException iOException, Object obj) {
        iOException.printStackTrace();
        a(-2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.plus.ImageActivity.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, android.graphics.Rect):android.graphics.Rect
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
      com.tencent.plus.ImageActivity.a(java.lang.String, int):void
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, boolean):boolean
      com.tencent.plus.ImageActivity.a(java.lang.String, long):void */
    public void onComplete(JSONObject jSONObject, Object obj) {
        int i;
        this.a.d.post(new n(this));
        try {
            i = jSONObject.getInt("ret");
        } catch (JSONException e) {
            e.printStackTrace();
            i = -1;
        }
        if (i == 0) {
            this.a.a("设置成功", 0);
            this.a.a("10658", 0L);
            ImageActivity imageActivity = this.a;
            if (this.a.c != null && !"".equals(this.a.c)) {
                Intent intent = new Intent();
                intent.setClassName(imageActivity, this.a.c);
                if (imageActivity.getPackageManager().resolveActivity(intent, 0) != null) {
                    imageActivity.startActivity(intent);
                }
            }
            imageActivity.finish();
            return;
        }
        a(i, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.plus.ImageActivity.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, android.graphics.Rect):android.graphics.Rect
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
      com.tencent.plus.ImageActivity.a(java.lang.String, int):void
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, boolean):boolean
      com.tencent.plus.ImageActivity.a(java.lang.String, long):void */
    private void a(int i, String str) {
        this.a.d.post(new m(this));
        if (str != null) {
            this.a.a(str, 1);
        } else if (i == -2) {
            this.a.a("网络有问题呢，检查一下网络再重试吧：）", 1);
        } else {
            this.a.a("设置出错了，请重新登录再尝试下呢：）", 1);
        }
        this.a.a("10660", 0L);
        Log.i("ImageActivity", "setAvatar failure, errorCode: " + i);
    }

    public void onConnectTimeoutException(ConnectTimeoutException connectTimeoutException, Object obj) {
        a(-2, null);
    }

    public void onSocketTimeoutException(SocketTimeoutException socketTimeoutException, Object obj) {
        a(-2, null);
    }

    public void onUnknowException(Exception exc, Object obj) {
        a(1, null);
    }

    public void onNetworkUnavailableException(NetworkUnavailableException networkUnavailableException, Object obj) {
        a(-2, null);
    }

    public void onHttpStatusException(HttpStatusException httpStatusException, Object obj) {
        a(-2, null);
    }
}
