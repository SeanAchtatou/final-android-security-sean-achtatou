package com.tencent.plus;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IRequestListener;
import com.tencent.tauth.Tencent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ProGuard */
public class ImageActivity extends Activity {
    RelativeLayout a;
    private Tencent b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public Handler d;
    /* access modifiers changed from: private */
    public TouchView e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public MaskView h;
    private TextView i;
    /* access modifiers changed from: private */
    public ProgressBar j;
    /* access modifiers changed from: private */
    public int k = 0;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public long m = 0;
    private int n = 640;
    private int o = 640;
    /* access modifiers changed from: private */
    public Rect p = new Rect();
    private String q;
    private Bitmap r;
    private View.OnClickListener s = new j(this);
    private View.OnClickListener t = new g(this);
    private IRequestListener u = new i(this);
    private IRequestListener v = new h(this);

    private Bitmap a(String str) {
        int i2 = 1;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Uri parse = Uri.parse(str);
        InputStream openInputStream = getContentResolver().openInputStream(parse);
        BitmapFactory.decodeStream(openInputStream, null, options);
        openInputStream.close();
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        while (i3 * i4 > 4194304) {
            i3 /= 2;
            i4 /= 2;
            i2 *= 2;
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = i2;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(parse), null, options);
    }

    /* access modifiers changed from: private */
    public Drawable b(String str) {
        IOException e2;
        Drawable drawable;
        try {
            InputStream open = getAssets().open(str);
            drawable = Drawable.createFromStream(open, str);
            try {
                open.close();
            } catch (IOException e3) {
                e2 = e3;
                e2.printStackTrace();
                return drawable;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            drawable = null;
            e2 = iOException;
        }
        return drawable;
    }

    private View a() {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        ViewGroup.LayoutParams layoutParams2 = new ViewGroup.LayoutParams(-1, -1);
        ViewGroup.LayoutParams layoutParams3 = new ViewGroup.LayoutParams(-2, -2);
        this.a = new RelativeLayout(this);
        this.a.setLayoutParams(layoutParams);
        this.a.setBackgroundColor(-16777216);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(layoutParams3);
        this.a.addView(relativeLayout);
        this.e = new TouchView(this);
        this.e.setLayoutParams(layoutParams2);
        this.e.setScaleType(ImageView.ScaleType.MATRIX);
        relativeLayout.addView(this.e);
        this.h = new MaskView(this);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(layoutParams2);
        layoutParams4.addRule(14, -1);
        layoutParams4.addRule(15, -1);
        this.h.setLayoutParams(layoutParams4);
        relativeLayout.addView(this.h);
        LinearLayout linearLayout = new LinearLayout(this);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, DensityUtil.a(this, 80.0f));
        layoutParams5.addRule(14, -1);
        linearLayout.setLayoutParams(layoutParams5);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        this.a.addView(linearLayout);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(DensityUtil.a(this, 24.0f), DensityUtil.a(this, 24.0f)));
        imageView.setImageDrawable(b("com.tencent.plus.logo.png"));
        linearLayout.addView(imageView);
        this.i = new TextView(this);
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(layoutParams3);
        layoutParams6.leftMargin = DensityUtil.a(this, 7.0f);
        this.i.setLayoutParams(layoutParams6);
        this.i.setEllipsize(TextUtils.TruncateAt.END);
        this.i.setSingleLine();
        this.i.setTextColor(-1);
        this.i.setTextSize(24.0f);
        this.i.setVisibility(8);
        linearLayout.addView(this.i);
        RelativeLayout relativeLayout2 = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-1, DensityUtil.a(this, 60.0f));
        layoutParams7.addRule(12, -1);
        layoutParams7.addRule(9, -1);
        relativeLayout2.setLayoutParams(layoutParams7);
        relativeLayout2.setBackgroundDrawable(b("com.tencent.plus.bar.png"));
        int a2 = DensityUtil.a(this, 10.0f);
        relativeLayout2.setPadding(a2, a2, a2, 0);
        this.a.addView(relativeLayout2);
        b bVar = new b(this, this);
        int a3 = DensityUtil.a(this, 14.0f);
        int a4 = DensityUtil.a(this, 7.0f);
        this.g = new Button(this);
        this.g.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.a(this, 78.0f), DensityUtil.a(this, 45.0f)));
        this.g.setText("取消");
        this.g.setTextColor(-1);
        this.g.setTextSize(18.0f);
        this.g.setPadding(a3, a4, a3, a4);
        bVar.b(this.g);
        relativeLayout2.addView(this.g);
        this.f = new Button(this);
        RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(DensityUtil.a(this, 78.0f), DensityUtil.a(this, 45.0f));
        layoutParams8.addRule(11, -1);
        this.f.setLayoutParams(layoutParams8);
        this.f.setTextColor(-1);
        this.f.setTextSize(18.0f);
        this.f.setPadding(a3, a4, a3, a4);
        this.f.setText("选取");
        bVar.a(this.f);
        relativeLayout2.addView(this.f);
        TextView textView = new TextView(this);
        RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(layoutParams3);
        layoutParams9.addRule(13, -1);
        textView.setLayoutParams(layoutParams9);
        textView.setText("移动和缩放");
        textView.setPadding(0, DensityUtil.a(this, 3.0f), 0, 0);
        textView.setTextSize(18.0f);
        textView.setTextColor(-1);
        relativeLayout2.addView(textView);
        this.j = new ProgressBar(this);
        RelativeLayout.LayoutParams layoutParams10 = new RelativeLayout.LayoutParams(layoutParams3);
        layoutParams10.addRule(14, -1);
        layoutParams10.addRule(15, -1);
        this.j.setLayoutParams(layoutParams10);
        this.j.setVisibility(8);
        this.a.addView(this.j);
        return this.a;
    }

    private void b() {
        try {
            this.r = a(this.q);
            if (this.r == null) {
                throw new IOException("cannot read picture: '" + this.q + "'!");
            }
            this.e.setImageBitmap(this.r);
            this.f.setOnClickListener(this.s);
            this.g.setOnClickListener(this.t);
            this.a.getViewTreeObserver().addOnGlobalLayoutListener(new k(this));
        } catch (IOException e2) {
            e2.printStackTrace();
            b("图片读取失败，请检查该图片是否有效", 1);
            finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.plus.ImageActivity.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, android.graphics.Rect):android.graphics.Rect
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
      com.tencent.plus.ImageActivity.a(java.lang.String, int):void
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, boolean):boolean
      com.tencent.plus.ImageActivity.a(java.lang.String, long):void */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        setContentView(a());
        this.d = new Handler();
        Bundle extras = getIntent().getExtras();
        this.q = extras.getString(Constants.PARAM_AVATAR_URI);
        this.c = extras.getString(Constants.PARAM_AVATAR_RETURN_ACTIVITY);
        String string = extras.getString(Constants.PARAM_APP_ID);
        String string2 = extras.getString("access_token");
        long j2 = extras.getLong("expires_in");
        String string3 = extras.getString(Constants.PARAM_OPEN_ID);
        this.b = Tencent.createInstance(string, getApplicationContext());
        this.b.setAccessToken(string2, (j2 - System.currentTimeMillis()) + "");
        this.b.setOpenId(string3);
        b();
        d();
        this.m = System.currentTimeMillis();
        a("10653", 0L);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.e.setImageBitmap(null);
        if (this.r != null && !this.r.isRecycled()) {
            this.r.recycle();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: private */
    public void c() {
        Matrix imageMatrix = this.e.getImageMatrix();
        float[] fArr = new float[9];
        imageMatrix.getValues(fArr);
        float f2 = fArr[2];
        float f3 = fArr[5];
        float f4 = fArr[0];
        float width = ((float) this.n) / ((float) this.p.width());
        int i2 = (int) ((((float) this.p.left) - f2) / f4);
        int i3 = (int) ((((float) this.p.top) - f3) / f4);
        Matrix matrix = new Matrix();
        matrix.set(imageMatrix);
        matrix.postScale(width, width);
        int i4 = (int) (650.0f / f4);
        Bitmap createBitmap = Bitmap.createBitmap(this.r, i2, i3, Math.min(this.r.getWidth() - i2, i4), Math.min(this.r.getHeight() - i3, i4), matrix, true);
        Bitmap createBitmap2 = Bitmap.createBitmap(createBitmap, 0, 0, this.n, this.o);
        createBitmap.recycle();
        a(createBitmap2);
    }

    private void a(Bitmap bitmap) {
        Bundle bundle = new Bundle();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        bitmap.recycle();
        bundle.putByteArray(Constants.PARAM_AVATAR_URI, byteArray);
        this.b.requestAsync(Constants.GRAPH_SET_AVATAR, bundle, "POST", this.u, null);
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2) {
        this.d.post(new f(this, str, i2));
    }

    /* access modifiers changed from: private */
    public void b(String str, int i2) {
        Toast makeText = Toast.makeText(this, str, 1);
        LinearLayout linearLayout = (LinearLayout) makeText.getView();
        ((TextView) linearLayout.getChildAt(0)).setPadding(8, 0, 0, 0);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(DensityUtil.a(this, 16.0f), DensityUtil.a(this, 16.0f)));
        if (i2 == 0) {
            imageView.setImageDrawable(b("com.tencent.plus.ic_success.png"));
        } else {
            imageView.setImageDrawable(b("com.tencent.plus.ic_error.png"));
        }
        linearLayout.addView(imageView, 0);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        makeText.setView(linearLayout);
        makeText.setGravity(17, 0, 0);
        makeText.show();
    }

    /* access modifiers changed from: private */
    public void d() {
        this.k++;
        this.b.requestAsync(Constants.GRAPH_SIMPLE_USER_INFO, null, "GET", this.v, null);
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        String d2 = d(str);
        if (!"".equals(d2)) {
            this.i.setText(d2);
            this.i.setVisibility(0);
        }
    }

    private String d(String str) {
        return str.replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&quot;", "\"").replaceAll("&#39;", "'").replaceAll("&amp;", "&");
    }

    public void a(String str, long j2) {
        Bundle bundle = new Bundle();
        bundle.putString("strValue", this.b.getAppId());
        bundle.putString("nValue", str);
        bundle.putString("qver", Constants.SDK_VERSION);
        if (j2 != 0) {
            bundle.putLong("elt", j2);
        }
        new e(this, getApplicationContext(), bundle).start();
    }
}
