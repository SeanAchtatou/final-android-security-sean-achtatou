package com.tencent.plus;

import android.content.Context;

/* compiled from: ProGuard */
public class DensityUtil {
    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }
}
