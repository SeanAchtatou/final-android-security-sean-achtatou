package com.tencent.plus;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.Button;

/* compiled from: ProGuard */
class b extends View {
    final /* synthetic */ ImageActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(ImageActivity imageActivity, Context context) {
        super(context);
        this.a = imageActivity;
    }

    public void a(Button button) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable a2 = this.a.b("com.tencent.plus.blue_normal.png");
        Drawable a3 = this.a.b("com.tencent.plus.blue_down.png");
        Drawable a4 = this.a.b("com.tencent.plus.blue_disable.png");
        stateListDrawable.addState(View.PRESSED_ENABLED_STATE_SET, a3);
        stateListDrawable.addState(View.ENABLED_FOCUSED_STATE_SET, a2);
        stateListDrawable.addState(View.ENABLED_STATE_SET, a2);
        stateListDrawable.addState(View.FOCUSED_STATE_SET, a2);
        stateListDrawable.addState(View.EMPTY_STATE_SET, a4);
        button.setBackgroundDrawable(stateListDrawable);
    }

    public void b(Button button) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable a2 = this.a.b("com.tencent.plus.gray_normal.png");
        Drawable a3 = this.a.b("com.tencent.plus.gray_down.png");
        Drawable a4 = this.a.b("com.tencent.plus.gray_disable.png");
        stateListDrawable.addState(View.PRESSED_ENABLED_STATE_SET, a3);
        stateListDrawable.addState(View.ENABLED_FOCUSED_STATE_SET, a2);
        stateListDrawable.addState(View.ENABLED_STATE_SET, a2);
        stateListDrawable.addState(View.FOCUSED_STATE_SET, a2);
        stateListDrawable.addState(View.EMPTY_STATE_SET, a4);
        button.setBackgroundDrawable(stateListDrawable);
    }
}
