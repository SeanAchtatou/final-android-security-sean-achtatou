package com.tencent.plus;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/* compiled from: ProGuard */
public class MaskView extends View {
    private static String a = "MaskView";
    private Rect b;
    private Paint c;

    public MaskView(Context context) {
        super(context);
        b();
    }

    public MaskView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    private void b() {
        this.c = new Paint();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect a2 = a();
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        Log.d(a, "width:" + a2.left);
        Log.d(a, "height:" + a2.top);
        this.c.setStyle(Paint.Style.FILL);
        this.c.setColor(Color.argb(100, 0, 0, 0));
        canvas.drawRect(0.0f, 0.0f, (float) measuredWidth, (float) a2.top, this.c);
        canvas.drawRect(0.0f, (float) a2.bottom, (float) measuredWidth, (float) measuredHeight, this.c);
        canvas.drawRect(0.0f, (float) a2.top, (float) a2.left, (float) a2.bottom, this.c);
        canvas.drawRect((float) a2.right, (float) a2.top, (float) measuredWidth, (float) a2.bottom, this.c);
        canvas.drawColor(Color.argb(100, 0, 0, 0));
        this.c.setStyle(Paint.Style.STROKE);
        this.c.setColor(-1);
        canvas.drawRect((float) a2.left, (float) a2.top, (float) (a2.right - 1), (float) a2.bottom, this.c);
    }

    public Rect a() {
        if (this.b == null) {
            this.b = new Rect();
            int measuredWidth = getMeasuredWidth();
            int measuredHeight = getMeasuredHeight();
            int min = Math.min(Math.min((measuredHeight - 60) - 80, measuredWidth), 640);
            int i = (measuredWidth - min) / 2;
            int i2 = (measuredHeight - min) / 2;
            this.b.set(i, i2, i + min, min + i2);
        }
        return this.b;
    }
}
