package com.tencent.plus;

import android.graphics.Color;
import android.view.View;

/* compiled from: ProGuard */
class j implements View.OnClickListener {
    final /* synthetic */ ImageActivity a;

    j(ImageActivity imageActivity) {
        this.a = imageActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.plus.ImageActivity.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, android.graphics.Rect):android.graphics.Rect
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
      com.tencent.plus.ImageActivity.a(java.lang.String, int):void
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, boolean):boolean
      com.tencent.plus.ImageActivity.a(java.lang.String, long):void */
    public void onClick(View view) {
        this.a.j.setVisibility(0);
        this.a.g.setEnabled(false);
        this.a.g.setTextColor(Color.rgb(21, 21, 21));
        this.a.f.setEnabled(false);
        this.a.f.setTextColor(Color.rgb(36, 94, 134));
        new Thread(new l(this)).start();
        if (this.a.l) {
            this.a.a("10657", 0L);
            return;
        }
        this.a.a("10655", System.currentTimeMillis() - this.a.m);
        if (this.a.e.b) {
            this.a.a("10654", 0L);
        }
    }
}
