package com.tencent.tauth.bean;

/* compiled from: ProGuard */
public class UserInfo {
    private String mIcon_100;
    private String mIcon_30;
    private String mIcon_50;
    private String mNickName;

    public UserInfo(String str, String str2, String str3, String str4) {
        this.mNickName = str;
        this.mIcon_30 = str2;
        this.mIcon_50 = str3;
        this.mIcon_100 = str4;
    }

    public String getNickName() {
        return this.mNickName;
    }

    public void setNickName(String str) {
        this.mNickName = str;
    }

    public String getIcon_30() {
        return this.mIcon_30;
    }

    public void setIcon_30(String str) {
        this.mIcon_30 = str;
    }

    public String getIcon_50() {
        return this.mIcon_50;
    }

    public void setIcon_50(String str) {
        this.mIcon_50 = str;
    }

    public String getIcon_100() {
        return this.mIcon_100;
    }

    public void setIcon_100(String str) {
        this.mIcon_100 = str;
    }

    public String toString() {
        return "nickname: " + this.mNickName + "\nicon_30: " + this.mIcon_30 + "\nicon_50: " + this.mIcon_50 + "\nicon_100: " + this.mIcon_100 + "\n";
    }
}
