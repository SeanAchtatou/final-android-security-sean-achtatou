package com.tencent.tauth.bean;

/* compiled from: ProGuard */
public class Album {
    private String mAlbumid;
    private int mClassid;
    private long mCreatetime;
    private String mDesc;
    private String mName;
    private long mPicnum;
    private int mPriv;

    public Album(String str, int i, long j, String str2, String str3, long j2, int i2) {
        this.mAlbumid = str;
        this.mClassid = i;
        this.mCreatetime = j;
        this.mDesc = str2;
        this.mName = str3;
        this.mPicnum = j2;
        this.mPriv = i2;
    }

    public String getAlbumid() {
        return this.mAlbumid;
    }

    public void setAlbumid(String str) {
        this.mAlbumid = str;
    }

    public int getClassid() {
        return this.mClassid;
    }

    public void setClassid(int i) {
        this.mClassid = i;
    }

    public long getCreatetime() {
        return this.mCreatetime;
    }

    public void setCreatetime(long j) {
        this.mCreatetime = j;
    }

    public String getDesc() {
        return this.mDesc;
    }

    public void setDesc(String str) {
        this.mDesc = str;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String str) {
        this.mName = str;
    }

    public long getPicnum() {
        return this.mPicnum;
    }

    public void setPicnum(long j) {
        this.mPicnum = j;
    }

    public int getPriv() {
        return this.mPriv;
    }

    public void setPriv(int i) {
        this.mPriv = i;
    }

    public String toString() {
        return "albumid: " + this.mAlbumid + "\nclassid: " + this.mClassid + "\ncreatetime: " + this.mCreatetime + "\ndesc: " + this.mDesc + "\nname: " + this.mName + "\npicnum: " + this.mPicnum + "\npriv: " + this.mPriv + "\n";
    }
}
