package com.tencent.tauth.bean;

/* compiled from: ProGuard */
public class OpenId {
    private String mClientId;
    private String mOpenId;

    public OpenId(String str, String str2) {
        this.mOpenId = str;
        this.mClientId = str2;
    }

    public String getOpenId() {
        return this.mOpenId;
    }

    public void setOpenId(String str) {
        this.mOpenId = str;
    }

    public String getClientId() {
        return this.mClientId;
    }

    public void setClientId(String str) {
        this.mClientId = str;
    }

    public String toString() {
        return this.mOpenId;
    }
}
