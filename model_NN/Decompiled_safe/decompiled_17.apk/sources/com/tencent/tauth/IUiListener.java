package com.tencent.tauth;

import org.json.JSONObject;

/* compiled from: ProGuard */
public interface IUiListener {
    void onCancel();

    void onComplete(JSONObject jSONObject);

    void onError(UiError uiError);
}
