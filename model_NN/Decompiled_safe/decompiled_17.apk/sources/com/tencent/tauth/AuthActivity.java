package com.tencent.tauth;

import android.app.Activity;
import android.os.Bundle;
import com.tencent.open.BrowserAuth;
import com.tencent.open.Util;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class AuthActivity extends Activity {
    private static final String TAG = "AuthActivity";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String uri = getIntent().getData().toString();
        Bundle a = Util.a(uri.substring(uri.indexOf("#") + 1));
        BrowserAuth a2 = BrowserAuth.a();
        String string = a.getString("serial");
        BrowserAuth.Auth a3 = a2.a(string);
        if (a3 != null) {
            if (uri.indexOf("://cancel") != -1) {
                a3.a.onCancel();
                a3.b.dismiss();
            } else {
                a.putString("access_token", a2.a(a.getString("access_token"), a3.c));
                JSONObject a4 = Util.a(new JSONObject(), Util.a(a));
                String optString = a4.optString("cb");
                if (!"".equals(optString)) {
                    a3.b.a(optString, a4.toString());
                } else {
                    a3.a.onComplete(a4);
                    a3.b.dismiss();
                }
            }
            a2.b(string);
        }
        finish();
    }
}
