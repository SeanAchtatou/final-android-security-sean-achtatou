package com.tencent.tauth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieSyncManager;
import com.tencent.open.OpenApi;
import com.tencent.open.OpenUi;
import com.tencent.open.TContext;
import com.tencent.plus.ImageActivity;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class Tencent {
    private OpenApi mOpenApi = new OpenApi(this.mTContext);
    private OpenUi mOpenUi = new OpenUi(this.mTContext);
    private TContext mTContext;

    private Tencent(String str, Context context) {
        this.mTContext = new TContext(str, context);
    }

    public static Tencent createInstance(String str, Context context) {
        try {
            context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.tauth.AuthActivity"), 0);
            return new Tencent(str, context);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Tencent", ("没有在AndroidManifest.xml中检测到com.tencent.open.AuthActivity,请加上com.tencent.open.AuthActivity,并配置<data android:scheme=\"tencent" + str + "\" />,详细信息请查看官网文档.") + "\n配置示例如下: \n<activity\n\t android:name=\"com.tencent.tauth.AuthActivity\"\n\t android:noHistory=\"true\"\n\t android:launchMode=\"singleTask\"\n<intent-filter>\n    <action android:name=\"android.intent.action.VIEW\" />\n\t <category android:name=\"android.intent.category.DEFAULT\" />\n    <category android:name=\"android.intent.category.BROWSABLE\" />\n    <data android:scheme=\"tencent" + str + "\" />\n" + "</intent-filter>\n" + "</activity>");
            return null;
        }
    }

    public boolean isSessionValid() {
        return this.mTContext.a();
    }

    public String getAccessToken() {
        return this.mTContext.b();
    }

    public void setAccessToken(String str, String str2) {
        this.mTContext.a(str, str2);
    }

    public String getOpenId() {
        return this.mTContext.c();
    }

    public void setOpenId(String str) {
        this.mTContext.a(str);
    }

    public String getAppId() {
        return this.mTContext.d();
    }

    public int login(Activity activity, String str, IUiListener iUiListener) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PARAM_SCOPE, str);
        return this.mOpenUi.a(activity, Constants.ACTION_LOGIN, bundle, iUiListener);
    }

    public void logout(Context context) {
        CookieSyncManager.createInstance(context);
        setAccessToken(null, null);
        setOpenId(null);
    }

    public int invite(Activity activity, Bundle bundle, IUiListener iUiListener) {
        return this.mOpenUi.a(activity, Constants.ACTION_INVITE, bundle, iUiListener);
    }

    public int story(Activity activity, Bundle bundle, IUiListener iUiListener) {
        return this.mOpenUi.a(activity, Constants.ACTION_STORY, bundle, iUiListener);
    }

    public int pay(Activity activity, IUiListener iUiListener) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PARAM_SCOPE, "");
        return this.mOpenUi.a(activity, Constants.ACTION_PAY, bundle, iUiListener);
    }

    public JSONObject request(String str, Bundle bundle, String str2) {
        return this.mOpenApi.a(this.mTContext.f(), str, bundle, str2);
    }

    public void requestAsync(String str, Bundle bundle, String str2, IRequestListener iRequestListener, Object obj) {
        this.mOpenApi.a(this.mTContext.f(), str, bundle, str2, iRequestListener, obj);
    }

    public boolean onActivityResult(int i, int i2, Intent intent) {
        return this.mOpenUi.a(i, i2, intent);
    }

    public String getSDKVersion() {
        return OpenApi.a();
    }

    public void setAvatar(Activity activity, Bundle bundle) {
        String string = bundle.getString(Constants.PARAM_AVATAR_URI);
        String string2 = bundle.getString(Constants.PARAM_AVATAR_RETURN_ACTIVITY);
        Bundle bundle2 = new Bundle();
        bundle2.putString(Constants.PARAM_AVATAR_URI, string);
        bundle2.putString(Constants.PARAM_AVATAR_RETURN_ACTIVITY, string2);
        bundle2.putString(Constants.PARAM_APP_ID, this.mTContext.d());
        bundle2.putString("access_token", this.mTContext.b());
        bundle2.putLong("expires_in", this.mTContext.e());
        bundle2.putString(Constants.PARAM_OPEN_ID, this.mTContext.c());
        Intent intent = new Intent(activity, ImageActivity.class);
        intent.putExtras(bundle2);
        activity.startActivity(intent);
    }
}
