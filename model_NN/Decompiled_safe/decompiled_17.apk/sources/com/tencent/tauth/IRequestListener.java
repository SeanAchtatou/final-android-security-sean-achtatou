package com.tencent.tauth;

import com.tencent.open.HttpStatusException;
import com.tencent.open.NetworkUnavailableException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public interface IRequestListener {
    void onComplete(JSONObject jSONObject, Object obj);

    void onConnectTimeoutException(ConnectTimeoutException connectTimeoutException, Object obj);

    void onHttpStatusException(HttpStatusException httpStatusException, Object obj);

    void onIOException(IOException iOException, Object obj);

    void onJSONException(JSONException jSONException, Object obj);

    void onMalformedURLException(MalformedURLException malformedURLException, Object obj);

    void onNetworkUnavailableException(NetworkUnavailableException networkUnavailableException, Object obj);

    void onSocketTimeoutException(SocketTimeoutException socketTimeoutException, Object obj);

    void onUnknowException(Exception exc, Object obj);
}
