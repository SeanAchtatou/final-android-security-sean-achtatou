package com.tencent.open;

import android.webkit.CookieSyncManager;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class f implements IUiListener {
    final /* synthetic */ OpenUi a;
    private IUiListener b;
    private boolean c;
    private boolean d;

    public f(OpenUi openUi, IUiListener iUiListener, boolean z, boolean z2) {
        this.a = openUi;
        this.b = iUiListener;
        this.c = z;
        this.d = z2;
    }

    public void onComplete(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("access_token");
            String string2 = jSONObject.getString("expires_in");
            String string3 = jSONObject.getString(Constants.PARAM_OPEN_ID);
            if (!(string == null || this.a.a == null || string3 == null)) {
                this.a.a.a(string, string2);
                this.a.a.a(string3);
            }
            if (this.c) {
                CookieSyncManager.getInstance().sync();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.b.onComplete(jSONObject);
    }

    public void onError(UiError uiError) {
        this.b.onError(uiError);
    }

    public void onCancel() {
        this.b.onCancel();
    }
}
