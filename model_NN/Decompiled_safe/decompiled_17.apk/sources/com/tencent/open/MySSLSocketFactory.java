package com.tencent.open;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.SecureRandom;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.scheme.HostNameResolver;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/* compiled from: ProGuard */
public class MySSLSocketFactory extends SSLSocketFactory {
    private final SSLContext a = SSLContext.getInstance("TLS");
    private final javax.net.ssl.SSLSocketFactory b;
    private final HostNameResolver c;

    public MySSLSocketFactory() {
        super("TLS", null, null, null, null, null);
        this.a.init(null, new TrustManager[]{new c(this)}, new SecureRandom());
        this.b = this.a.getSocketFactory();
        this.c = null;
    }

    public Socket createSocket() {
        return (SSLSocket) this.b.createSocket();
    }

    public Socket connectSocket(Socket socket, String str, int i, InetAddress inetAddress, int i2, HttpParams httpParams) {
        InetSocketAddress inetSocketAddress;
        if (str == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (httpParams == null) {
            throw new IllegalArgumentException("Parameters may not be null.");
        } else {
            SSLSocket sSLSocket = (SSLSocket) (socket != null ? socket : createSocket());
            if (inetAddress != null || i2 > 0) {
                if (i2 < 0) {
                    i2 = 0;
                }
                sSLSocket.bind(new InetSocketAddress(inetAddress, i2));
            }
            int connectionTimeout = HttpConnectionParams.getConnectionTimeout(httpParams);
            int soTimeout = HttpConnectionParams.getSoTimeout(httpParams);
            if (this.c != null) {
                inetSocketAddress = new InetSocketAddress(this.c.resolve(str), i);
            } else {
                inetSocketAddress = new InetSocketAddress(str, i);
            }
            sSLSocket.connect(inetSocketAddress, connectionTimeout);
            sSLSocket.setSoTimeout(soTimeout);
            return sSLSocket;
        }
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        if (i == -1) {
            return (SSLSocket) this.b.createSocket(socket, str, 443, z);
        }
        return (SSLSocket) this.b.createSocket(socket, str, i, z);
    }
}
