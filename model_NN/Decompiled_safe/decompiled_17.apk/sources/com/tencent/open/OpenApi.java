package com.tencent.open;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IRequestListener;
import java.util.Random;

/* compiled from: ProGuard */
public class OpenApi {
    private static Random b = new Random();
    private TContext a;

    public OpenApi(TContext tContext) {
        this.a = tContext;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008a, code lost:
        r12 = -4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008c, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008d, code lost:
        r14 = r13;
        r13 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a6, code lost:
        r17 = r12;
        r12 = r9;
        r13 = r8;
        r8 = r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b5, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b6, code lost:
        r14 = r13;
        r13 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b8, code lost:
        r13.printStackTrace();
        r10 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c4, code lost:
        if (r4 >= 3) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00c6, code lost:
        r9 = -8;
        r8 = r14;
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00cd, code lost:
        a(r20, r5, r6, 0, 0, -8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d4, code lost:
        throw r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d5, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d6, code lost:
        r13 = r3;
        r13.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r12 = java.lang.Integer.parseInt(r13.getMessage().replace("http status code error:", ""));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00f6, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f7, code lost:
        r3.printStackTrace();
        r12 = -9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00fd, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00fe, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0101, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0102, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0103, code lost:
        r13 = r3;
        r13.printStackTrace();
        a(r20, r5, r6, 0, 0, -3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0113, code lost:
        throw r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0114, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0115, code lost:
        r13 = r3;
        r13.printStackTrace();
        a(r20, r5, r6, 0, 0, -2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0125, code lost:
        throw r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0126, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0127, code lost:
        r13 = r3;
        r13.printStackTrace();
        a(r20, r5, r6, 0, 0, -4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0137, code lost:
        throw r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0138, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0139, code lost:
        r13 = r9;
        r14 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x013d, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x013e, code lost:
        r13 = r9;
        r14 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008c A[ExcHandler: ConnectTimeoutException (r8v16 'e' org.apache.http.conn.ConnectTimeoutException A[CUSTOM_DECLARE]), Splitter:B:8:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b5 A[ExcHandler: SocketTimeoutException (r8v15 'e' java.net.SocketTimeoutException A[CUSTOM_DECLARE]), Splitter:B:8:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d5 A[ExcHandler: HttpStatusException (r3v11 'e' com.tencent.open.HttpStatusException A[CUSTOM_DECLARE]), Splitter:B:5:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00fd A[ExcHandler: NetworkUnavailableException (r3v10 'e' com.tencent.open.NetworkUnavailableException A[CUSTOM_DECLARE]), Splitter:B:5:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0102 A[ExcHandler: MalformedURLException (r3v8 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:5:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0114 A[ExcHandler: IOException (r3v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:5:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0126 A[ExcHandler: JSONException (r3v4 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:5:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00cd A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00a6 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject a(android.content.Context r20, java.lang.String r21, android.os.Bundle r22, java.lang.String r23) {
        /*
            r19 = this;
            r0 = r19
            r1 = r21
            r2 = r22
            android.os.Bundle r15 = r0.a(r1, r2)
            java.lang.String r3 = r21.toLowerCase()
            java.lang.String r4 = "http"
            boolean r3 = r3.startsWith(r4)
            if (r3 != 0) goto L_0x0142
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "https://openmobile.qq.com/"
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r21
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "https://openmobile.qq.com/"
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r21
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = r4.toString()
        L_0x0040:
            r8 = 0
            long r6 = android.os.SystemClock.elapsedRealtime()
            r4 = 0
        L_0x0046:
            int r4 = r4 + 1
            r0 = r20
            r1 = r23
            com.tencent.open.Util$Statistic r10 = com.tencent.open.Util.a(r0, r3, r1, r15)     // Catch:{ ConnectTimeoutException -> 0x013d, SocketTimeoutException -> 0x0138, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            java.lang.String r9 = r10.a     // Catch:{ ConnectTimeoutException -> 0x013d, SocketTimeoutException -> 0x0138, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            org.json.JSONObject r13 = com.tencent.open.Util.c(r9)     // Catch:{ ConnectTimeoutException -> 0x013d, SocketTimeoutException -> 0x0138, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            java.lang.String r8 = "oauth2.0/m_me"
            r0 = r21
            boolean r8 = r8.equals(r0)     // Catch:{ ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            if (r8 == 0) goto L_0x0077
            if (r13 == 0) goto L_0x0077
            java.lang.String r8 = "openid"
            java.lang.String r8 = r13.getString(r8)     // Catch:{ ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            if (r8 == 0) goto L_0x0077
            r0 = r19
            com.tencent.open.TContext r9 = r0.a     // Catch:{ ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            if (r9 == 0) goto L_0x0077
            r0 = r19
            com.tencent.open.TContext r9 = r0.a     // Catch:{ ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            r9.a(r8)     // Catch:{ ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
        L_0x0077:
            java.lang.String r8 = "ret"
            int r12 = r13.getInt(r8)     // Catch:{ JSONException -> 0x0089, ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114 }
        L_0x007d:
            long r8 = r10.b     // Catch:{ ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
            long r10 = r10.c     // Catch:{ ConnectTimeoutException -> 0x008c, SocketTimeoutException -> 0x00b5, HttpStatusException -> 0x00d5, NetworkUnavailableException -> 0x00fd, MalformedURLException -> 0x0102, IOException -> 0x0114, JSONException -> 0x0126 }
        L_0x0081:
            r3 = r19
            r4 = r20
            r3.a(r4, r5, r6, r8, r10, r12)
            return r13
        L_0x0089:
            r8 = move-exception
            r12 = -4
            goto L_0x007d
        L_0x008c:
            r8 = move-exception
            r14 = r13
            r13 = r8
        L_0x008f:
            r13.printStackTrace()
            r12 = -7
            r8 = 0
            r10 = 0
            r16 = 3
            r0 = r16
            if (r4 >= r0) goto L_0x00ad
            r17 = r8
            r9 = r12
            r8 = r14
            r12 = r17
        L_0x00a3:
            r14 = 3
            if (r4 < r14) goto L_0x0046
            r17 = r12
            r12 = r9
            r13 = r8
            r8 = r17
            goto L_0x0081
        L_0x00ad:
            r3 = r19
            r4 = r20
            r3.a(r4, r5, r6, r8, r10, r12)
            throw r13
        L_0x00b5:
            r8 = move-exception
            r14 = r13
            r13 = r8
        L_0x00b8:
            r13.printStackTrace()
            r12 = -8
            r8 = 0
            r10 = 0
            r16 = 3
            r0 = r16
            if (r4 >= r0) goto L_0x00cd
            r17 = r8
            r9 = r12
            r8 = r14
            r12 = r17
            goto L_0x00a3
        L_0x00cd:
            r3 = r19
            r4 = r20
            r3.a(r4, r5, r6, r8, r10, r12)
            throw r13
        L_0x00d5:
            r3 = move-exception
            r13 = r3
            r13.printStackTrace()
            java.lang.String r3 = r13.getMessage()
            java.lang.String r4 = "http status code error:"
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r4, r8)     // Catch:{ Exception -> 0x00f6 }
            int r12 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x00f6 }
        L_0x00ea:
            r8 = 0
            r10 = 0
            r3 = r19
            r4 = r20
            r3.a(r4, r5, r6, r8, r10, r12)
            throw r13
        L_0x00f6:
            r3 = move-exception
            r3.printStackTrace()
            r12 = -9
            goto L_0x00ea
        L_0x00fd:
            r3 = move-exception
            r3.printStackTrace()
            throw r3
        L_0x0102:
            r3 = move-exception
            r13 = r3
            r13.printStackTrace()
            r12 = -3
            r8 = 0
            r10 = 0
            r3 = r19
            r4 = r20
            r3.a(r4, r5, r6, r8, r10, r12)
            throw r13
        L_0x0114:
            r3 = move-exception
            r13 = r3
            r13.printStackTrace()
            r12 = -2
            r8 = 0
            r10 = 0
            r3 = r19
            r4 = r20
            r3.a(r4, r5, r6, r8, r10, r12)
            throw r13
        L_0x0126:
            r3 = move-exception
            r13 = r3
            r13.printStackTrace()
            r12 = -4
            r8 = 0
            r10 = 0
            r3 = r19
            r4 = r20
            r3.a(r4, r5, r6, r8, r10, r12)
            throw r13
        L_0x0138:
            r9 = move-exception
            r13 = r9
            r14 = r8
            goto L_0x00b8
        L_0x013d:
            r9 = move-exception
            r13 = r9
            r14 = r8
            goto L_0x008f
        L_0x0142:
            r5 = r21
            r3 = r21
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.OpenApi.a(android.content.Context, java.lang.String, android.os.Bundle, java.lang.String):org.json.JSONObject");
    }

    public void a(Context context, String str, long j, long j2, long j3, int i) {
        if (b.nextInt(100) < 10) {
            Bundle bundle = new Bundle();
            bundle.putString(Constants.PARAM_APP_ID, "1000002");
            bundle.putString("releaseversion", Constants.SDK_VERSION);
            bundle.putString("commandid", str);
            bundle.putString("apn", a(context));
            bundle.putString("resultcode", "" + i);
            bundle.putString("device", Build.DEVICE);
            bundle.putString("tmcost", "" + (SystemClock.elapsedRealtime() - j));
            bundle.putString("reqsize", "" + j2);
            bundle.putString("rspsize", "" + j3);
            bundle.putString("frequency", "10");
            bundle.putString("qua", "V1_AND_OpenSDK_1.4_159_RDM_B");
            new b(this, context, bundle).start();
        }
    }

    private Bundle a(String str, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString("format", "json");
        if (c()) {
            bundle.putString("access_token", b());
        }
        if (!Constants.GRAPH_OPEN_ID.equals(str) && this.a != null) {
            bundle.putString(Constants.PARAM_CONSUMER_KEY, this.a.d());
            if (this.a.c() != null) {
                bundle.putString(Constants.PARAM_OPEN_ID, this.a.c());
            }
        }
        return bundle;
    }

    public void a(Context context, String str, Bundle bundle, String str2, IRequestListener iRequestListener, Object obj) {
        new d(this, context, str, bundle, str2, iRequestListener, obj).start();
    }

    private String b() {
        if (this.a != null) {
            return this.a.b();
        }
        return null;
    }

    private boolean c() {
        return this.a != null && this.a.a();
    }

    public static String a() {
        return Constants.SDK_VERSION_STRING;
    }

    public String a(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return "unknown";
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
                return "unknown";
            }
            if (activeNetworkInfo.getTypeName().toUpperCase().equals("WIFI")) {
                return "wifi";
            }
            String lowerCase = activeNetworkInfo.getExtraInfo().toLowerCase();
            if (lowerCase == null || lowerCase.length() == 0) {
                return "unknown";
            }
            return lowerCase;
        } catch (Exception e) {
            e.printStackTrace();
            return "unknown";
        }
    }
}
