package com.tencent.open;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.tencent.tauth.IUiListener;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class RedoDialog extends Dialog {
    static final FrameLayout.LayoutParams a = new FrameLayout.LayoutParams(-1, -1);
    public JSONObject b;
    public String c;
    public String d;
    private String e;
    /* access modifiers changed from: private */
    public IUiListener f;
    /* access modifiers changed from: private */
    public ProgressDialog g;
    /* access modifiers changed from: private */
    public WebView h;
    /* access modifiers changed from: private */
    public FrameLayout i;

    public void dismiss() {
        this.f.onCancel();
        super.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.g = new ProgressDialog(getContext());
        this.g.requestWindowFeature(1);
        this.g.setMessage("Loading...");
        requestWindowFeature(1);
        this.i = new FrameLayout(getContext());
        a(0);
        addContentView(this.i, new ViewGroup.LayoutParams(-1, -1));
    }

    private void a(int i2) {
        LinearLayout linearLayout = new LinearLayout(getContext());
        this.h = new WebView(getContext());
        this.h.setVerticalScrollBarEnabled(false);
        this.h.setHorizontalScrollBarEnabled(false);
        this.h.setWebViewClient(new e(this));
        this.h.getSettings().setJavaScriptEnabled(true);
        this.h.addJavascriptInterface(new j(this), "sdk_js_if");
        this.h.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.h.loadUrl(this.e);
        this.h.setLayoutParams(a);
        this.h.setVisibility(4);
        this.h.getSettings().setSavePassword(false);
        linearLayout.setPadding(0, 0, 0, 0);
        linearLayout.addView(this.h);
        this.i.addView(linearLayout, -1, -1);
    }
}
