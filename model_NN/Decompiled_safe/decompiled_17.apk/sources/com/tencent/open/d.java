package com.tencent.open;

import android.content.Context;
import android.os.Bundle;
import com.tencent.tauth.IRequestListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class d extends Thread {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;
    final /* synthetic */ Bundle c;
    final /* synthetic */ String d;
    final /* synthetic */ IRequestListener e;
    final /* synthetic */ Object f;
    final /* synthetic */ OpenApi g;

    d(OpenApi openApi, Context context, String str, Bundle bundle, String str2, IRequestListener iRequestListener, Object obj) {
        this.g = openApi;
        this.a = context;
        this.b = str;
        this.c = bundle;
        this.d = str2;
        this.e = iRequestListener;
        this.f = obj;
    }

    public void run() {
        try {
            JSONObject a2 = this.g.a(this.a, this.b, this.c, this.d);
            if (this.e != null) {
                this.e.onComplete(a2, this.f);
            }
        } catch (MalformedURLException e2) {
            if (this.e != null) {
                this.e.onMalformedURLException(e2, this.f);
            }
        } catch (ConnectTimeoutException e3) {
            if (this.e != null) {
                this.e.onConnectTimeoutException(e3, this.f);
            }
        } catch (SocketTimeoutException e4) {
            if (this.e != null) {
                this.e.onSocketTimeoutException(e4, this.f);
            }
        } catch (NetworkUnavailableException e5) {
            if (this.e != null) {
                this.e.onNetworkUnavailableException(e5, this.f);
            }
        } catch (HttpStatusException e6) {
            if (this.e != null) {
                this.e.onHttpStatusException(e6, this.f);
            }
        } catch (IOException e7) {
            if (this.e != null) {
                this.e.onIOException(e7, this.f);
            }
        } catch (JSONException e8) {
            if (this.e != null) {
                this.e.onJSONException(e8, this.f);
            }
        } catch (Exception e9) {
            if (this.e != null) {
                this.e.onUnknowException(e9, this.f);
            }
        }
    }
}
