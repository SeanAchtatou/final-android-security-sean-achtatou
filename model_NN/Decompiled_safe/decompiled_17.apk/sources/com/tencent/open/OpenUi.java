package com.tencent.open;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseArray;
import android.webkit.CookieSyncManager;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;

/* compiled from: ProGuard */
public class OpenUi {
    /* access modifiers changed from: private */
    public TContext a;
    private int b = Constants.CODE_REQUEST_MIN;
    private SparseArray c = new SparseArray();

    public OpenUi(TContext tContext) {
        this.a = tContext;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.open.OpenUi.a(android.content.Context, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int
     arg types: [android.app.Activity, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener]
     candidates:
      com.tencent.open.OpenUi.a(android.app.Activity, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int
      com.tencent.open.OpenUi.a(android.content.Context, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int */
    public int a(Activity activity, String str, Bundle bundle, IUiListener iUiListener) {
        if (b(activity, str, bundle, iUiListener)) {
            return 1;
        }
        return a((Context) activity, str, bundle, iUiListener);
    }

    public boolean b(Activity activity, String str, Bundle bundle, IUiListener iUiListener) {
        Bundle b2 = b(str, bundle);
        Intent a2 = a(activity, str);
        if (a2 == null) {
            return false;
        }
        a2.putExtra(Constants.KEY_ACTION, str);
        a2.putExtra(Constants.KEY_PARAMS, b2);
        try {
            int intExtra = a2.getIntExtra("key_request_code", 0);
            activity.startActivityForResult(a2, intExtra);
            if (Constants.ACTION_LOGIN.equals(str)) {
                iUiListener = new f(this, iUiListener, false, false);
            } else if (Constants.ACTION_PAY.equals(str)) {
                iUiListener = new f(this, iUiListener, false, true);
            }
            this.c.put(intExtra, new g(activity, str, bundle, iUiListener));
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public boolean a(int i, int i2, Intent intent) {
        if (i >= 5656 && i <= 6656) {
            g gVar = (g) this.c.get(i);
            this.c.remove(i);
            if (!(gVar == null || gVar.a == null)) {
                if (i2 == -1) {
                    int intExtra = intent.getIntExtra(Constants.KEY_ERROR_CODE, 0);
                    if (intExtra == 0) {
                        String stringExtra = intent.getStringExtra(Constants.KEY_RESPONSE);
                        try {
                            gVar.a.onComplete(Util.c(stringExtra));
                        } catch (JSONException e) {
                            gVar.a.onError(new UiError(-4, Constants.MSG_JSON_ERROR, stringExtra));
                        }
                    } else {
                        gVar.a.onError(new UiError(intExtra, intent.getStringExtra(Constants.KEY_ERROR_MSG), intent.getStringExtra(Constants.KEY_ERROR_DETAIL)));
                    }
                } else if (i2 == 0) {
                    gVar.a.onCancel();
                }
                return true;
            }
        }
        return false;
    }

    private Intent a(Context context, String str) {
        Intent intent = new Intent();
        intent.setClassName("com.qzone", "com.tencent.open.agent.AgentActivity");
        intent.putExtra("key_request_code", a());
        if (a(context, intent)) {
            return intent;
        }
        return null;
    }

    private boolean a(Context context, Intent intent) {
        if (context.getPackageManager().resolveActivity(intent, 0) == null) {
            return false;
        }
        return true;
    }

    private int a() {
        g gVar;
        do {
            this.b++;
            if (this.b == 6656) {
                this.b = Constants.CODE_REQUEST_MIN;
            }
            gVar = (g) this.c.get(this.b);
            this.c.remove(this.b);
            if (!(gVar == null || gVar.a == null)) {
                gVar.a.onCancel();
                continue;
            }
        } while (gVar != null);
        return this.b;
    }

    public int a(Context context, String str, Bundle bundle, IUiListener iUiListener) {
        CookieSyncManager.createInstance(context);
        String c2 = c(str, a(str, bundle));
        if (Constants.ACTION_LOGIN.equals(str)) {
            iUiListener = new f(this, iUiListener, true, false);
        } else if (Constants.ACTION_PAY.equals(str)) {
            iUiListener = new f(this, iUiListener, true, true);
        }
        new TDialog(context, c2, iUiListener).show();
        return 2;
    }

    private Bundle a(String str, Bundle bundle) {
        Bundle bundle2 = new Bundle(bundle);
        if (Constants.ACTION_LOGIN.equals(str) || Constants.ACTION_PAY.equals(str)) {
            if (this.a != null) {
                bundle2.putString(Constants.PARAM_CLIENT_ID, this.a.d());
            }
        } else if (this.a != null) {
            bundle2.putString(Constants.PARAM_CONSUMER_KEY, this.a.d());
            if (this.a.a()) {
                bundle2.putString("access_token", this.a.b());
            }
            String c2 = this.a.c();
            if (c2 != null) {
                bundle2.putString(Constants.PARAM_OPEN_ID, c2);
            }
        }
        return bundle2;
    }

    private Bundle b(String str, Bundle bundle) {
        Bundle bundle2 = new Bundle(bundle);
        if (Constants.ACTION_LOGIN.equals(str)) {
            if (this.a != null) {
                bundle2.putString(Constants.PARAM_CLIENT_ID, this.a.d());
            }
        } else if (!Constants.ACTION_PAY.equals(str)) {
            if (this.a != null) {
                bundle2.putString(Constants.PARAM_APP_ID, this.a.d());
                if (this.a.a()) {
                    bundle2.putString(Constants.PARAM_KEY_STR, this.a.b());
                    bundle2.putString(Constants.PARAM_KEY_TYPE, "0x80");
                }
                String c2 = this.a.c();
                if (c2 != null) {
                    bundle2.putString(Constants.PARAM_HOPEN_ID, c2);
                }
            }
            bundle2.putString(Constants.PARAM_PLATFORM, "androidqz");
        } else if (this.a != null) {
            bundle2.putString(Constants.PARAM_CONSUMER_KEY, this.a.d());
            String c3 = this.a.c();
            if (c3 != null) {
                bundle2.putString(Constants.PARAM_HOPEN_ID, c3);
            } else {
                bundle2.putString(Constants.PARAM_HOPEN_ID, "");
            }
        }
        return bundle2;
    }

    private String c(String str, Bundle bundle) {
        bundle.putString("display", "mobile");
        StringBuilder sb = new StringBuilder();
        if (Constants.ACTION_LOGIN.equals(str) || Constants.ACTION_PAY.equals(str)) {
            bundle.putString("response_type", "token");
            bundle.putString("redirect_uri", "auth://tauth.qq.com/");
            bundle.putString("status_userip", Util.a());
            bundle.putString("status_os", Build.VERSION.RELEASE);
            bundle.putString("status_machine", Build.MODEL);
            bundle.putString("status_version", Build.VERSION.SDK);
            bundle.putString("cancel_display", "1");
            bundle.putString("switch", "1");
            bundle.putString("sdkv", Constants.SDK_VERSION);
            bundle.putString("sdkp", "a");
            sb.append("https://openmobile.qq.com/oauth2.0/m_authorize?");
            sb.append(Util.a(bundle));
        } else if (Constants.ACTION_STORY.equals(str)) {
            sb.append("http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory_v1.3.html?");
            bundle.putString("sdkv", Constants.SDK_VERSION);
            sb.append(Util.a(bundle));
        } else if (Constants.ACTION_INVITE.equals(str)) {
            sb.append("http://qzs.qq.com/open/mobile/invite/sdk_invite.html?");
            bundle.putString("sdkv", Constants.SDK_VERSION);
            sb.append(Util.a(bundle));
        }
        return sb.toString();
    }
}
