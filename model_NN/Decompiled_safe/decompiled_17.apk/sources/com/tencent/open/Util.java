package com.tencent.open;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.baixing.util.TraceUtil;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;
import org.apache.commons.httpclient.HttpState;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class Util {
    private static boolean a = true;

    public static String a(Bundle bundle, String str) {
        if (bundle == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (obj instanceof String) {
                sb.append("Content-Disposition: form-data; name=\"" + next + "\"" + TraceUtil.LINE + TraceUtil.LINE + ((String) obj));
                sb.append("\r\n--" + str + TraceUtil.LINE);
            }
        }
        return sb.toString();
    }

    public static String a(Bundle bundle) {
        if (bundle == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if ((obj instanceof String) || (obj instanceof String[])) {
                if (obj instanceof String[]) {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=");
                    String[] stringArray = bundle.getStringArray(next);
                    for (int i = 0; i < stringArray.length; i++) {
                        if (i == 0) {
                            sb.append(URLEncoder.encode(stringArray[i]));
                        } else {
                            sb.append(URLEncoder.encode("," + stringArray[i]));
                        }
                    }
                } else {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=" + URLEncoder.encode(bundle.getString(next)));
                }
                z = z;
            }
        }
        return sb.toString();
    }

    public static Bundle a(String str) {
        Bundle bundle = new Bundle();
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                if (split2.length == 2) {
                    bundle.putString(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
                }
            }
        }
        return bundle;
    }

    public static JSONObject a(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                if (split2.length == 2) {
                    try {
                        jSONObject.put(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return jSONObject;
    }

    public static JSONObject b(String str) {
        try {
            URL url = new URL(str.replace("auth://", "http://"));
            JSONObject a2 = a((JSONObject) null, url.getQuery());
            a(a2, url.getRef());
            return a2;
        } catch (MalformedURLException e) {
            return new JSONObject();
        }
    }

    /* compiled from: ProGuard */
    public class Statistic {
        public String a;
        public long b;
        public long c;

        public Statistic(String str, int i) {
            this.a = str;
            this.b = (long) i;
            if (this.a != null) {
                this.c = (long) this.a.length();
            }
        }
    }

    public static Statistic a(Context context, String str, String str2, Bundle bundle) {
        HttpUriRequest httpUriRequest;
        int i;
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || ((activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable())) {
            HttpClient b = b(context);
            int length = str.length();
            if (str2.equals("GET")) {
                String a2 = a(bundle);
                i = a2.length() + length;
                if (!str.endsWith("?")) {
                    str = str + "?";
                }
                httpUriRequest = new HttpGet(str + a2);
                httpUriRequest.addHeader("Accept-Encoding", "gzip");
            } else if (str2.equals("POST")) {
                HttpUriRequest httpPost = new HttpPost(str);
                httpPost.addHeader("Accept-Encoding", "gzip");
                Bundle bundle2 = new Bundle();
                for (String next : bundle.keySet()) {
                    Object obj = bundle.get(next);
                    if (obj instanceof byte[]) {
                        bundle2.putByteArray(next, (byte[]) obj);
                    }
                }
                if (!bundle.containsKey("method")) {
                    bundle.putString("method", str2);
                }
                httpPost.setHeader("Content-Type", "multipart/form-data;boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
                httpPost.setHeader("Connection", "Keep-Alive");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byteArrayOutputStream.write("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
                byteArrayOutputStream.write(a(bundle, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f").getBytes());
                if (!bundle2.isEmpty()) {
                    int size = bundle2.size();
                    int i2 = 0;
                    Iterator<String> it = bundle2.keySet().iterator();
                    while (true) {
                        int i3 = i2;
                        if (!it.hasNext()) {
                            break;
                        }
                        String next2 = it.next();
                        byteArrayOutputStream.write(("Content-Disposition: form-data; name=\"" + next2 + "\"; filename=\"" + next2 + "\"" + TraceUtil.LINE).getBytes());
                        byteArrayOutputStream.write("Content-Type: content/unknown\r\n\r\n".getBytes());
                        byteArrayOutputStream.write(bundle2.getByteArray(next2));
                        if (i3 < size - 1) {
                            byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
                        }
                        i2 = i3 + 1;
                    }
                }
                byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f--\r\n".getBytes());
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                i = byteArray.length + length;
                byteArrayOutputStream.close();
                httpPost.setEntity(new ByteArrayEntity(byteArray));
                httpUriRequest = httpPost;
            } else {
                httpUriRequest = null;
                i = length;
            }
            HttpResponse execute = b.execute(httpUriRequest);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return new Statistic(a(execute), i);
            }
            throw new HttpStatusException("http status code error:" + statusCode);
        }
        throw new NetworkUnavailableException("network unavailable");
    }

    private static String a(HttpResponse httpResponse) {
        InputStream inputStream;
        InputStream content = httpResponse.getEntity().getContent();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
        if (firstHeader == null || firstHeader.getValue().toLowerCase().indexOf("gzip") <= -1) {
            inputStream = content;
        } else {
            inputStream = new GZIPInputStream(content);
        }
        byte[] bArr = new byte[512];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return new String(byteArrayOutputStream.toByteArray());
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    private static HttpClient b(Context context) {
        MySSLSocketFactory mySSLSocketFactory;
        try {
            mySSLSocketFactory = new MySSLSocketFactory();
        } catch (KeyManagementException e) {
            e.printStackTrace();
            mySSLSocketFactory = null;
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            mySSLSocketFactory = null;
        } catch (KeyStoreException e3) {
            e3.printStackTrace();
            mySSLSocketFactory = null;
        } catch (UnrecoverableKeyException e4) {
            e4.printStackTrace();
            mySSLSocketFactory = null;
        }
        mySSLSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUserAgent(basicHttpParams, "AndroidSDK_" + Build.VERSION.SDK + "_" + Build.DEVICE + "_" + Build.VERSION.RELEASE);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", mySSLSocketFactory, 443));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        NetworkProxy a2 = a(context);
        if (a2 != null) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(a2.a, a2.b));
        }
        return defaultHttpClient;
    }

    public static JSONObject c(String str) {
        if (str.equals(HttpState.PREEMPTIVE_DEFAULT)) {
            str = "{value : false}";
        }
        if (str.equals("true")) {
            str = "{value : true}";
        }
        if (str.contains("allback(")) {
            str = str.replaceFirst("[\\s\\S]*allback\\(([\\s\\S]*)\\);[^\\)]*\\z", "$1").trim();
        }
        return new JSONObject(str);
    }

    public static void a(String str, String str2) {
        if (a) {
            Log.d(str, str2);
        }
    }

    public static String a() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            a("Tencent-Util", e.toString());
        }
        return "";
    }

    private static String c(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            return System.getProperty("http.proxyHost");
        }
        if (context == null) {
            return Proxy.getDefaultHost();
        }
        String host = Proxy.getHost(context);
        if (d(host)) {
            return Proxy.getDefaultHost();
        }
        return host;
    }

    public static NetworkProxy a(Context context) {
        if (context == null) {
            return null;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return null;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        if (activeNetworkInfo.getType() == 0) {
            String c = c(context);
            int d = d(context);
            if (!d(c) && d >= 0) {
                return new NetworkProxy(c, d, null);
            }
        }
        return null;
    }

    /* compiled from: ProGuard */
    public class NetworkProxy {
        public final String a;
        public final int b;

        /* synthetic */ NetworkProxy(String str, int i, h hVar) {
            this(str, i);
        }

        private NetworkProxy(String str, int i) {
            this.a = str;
            this.b = i;
        }
    }

    private static int d(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            String property = System.getProperty("http.proxyPort");
            if (d(property)) {
                return -1;
            }
            try {
                return Integer.parseInt(property);
            } catch (NumberFormatException e) {
                return -1;
            }
        } else if (context == null) {
            return Proxy.getDefaultPort();
        } else {
            int port = Proxy.getPort(context);
            if (port < 0) {
                return Proxy.getDefaultPort();
            }
            return port;
        }
    }

    private static boolean d(String str) {
        return str == null || str.length() == 0;
    }
}
