package com.tencent.open;

import android.graphics.Bitmap;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class e extends WebViewClient {
    final /* synthetic */ RedoDialog a;

    private e(RedoDialog redoDialog) {
        this.a = redoDialog;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Util.a("TDialog", "Redirect URL: " + str);
        if (!str.startsWith("auth://tauth.qq.com/")) {
            return false;
        }
        JSONObject b = Util.b(str);
        try {
            if (b.getString("error") == null) {
                b.getString("error_type");
            }
            this.a.c = b.getString("access_token");
            this.a.d = b.getString("expires_in");
            if (this.a.c == null || this.a.d == null) {
                this.a.f.onComplete(this.a.b);
                this.a.dismiss();
            }
        } catch (JSONException e) {
            this.a.f.onComplete(this.a.b);
            this.a.dismiss();
        }
        return true;
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        this.a.f.onError(new UiError(i, str, str2));
        this.a.dismiss();
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        Util.a("TDialog", "Webview loading URL: " + str);
        super.onPageStarted(webView, str, bitmap);
        this.a.g.show();
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.a.g.dismiss();
        this.a.i.setBackgroundColor(0);
        this.a.h.setVisibility(0);
    }

    public void onReceivedHttpAuthRequest(WebView webView, HttpAuthHandler httpAuthHandler, String str, String str2) {
        super.onReceivedHttpAuthRequest(webView, httpAuthHandler, str, str2);
    }
}
