package com.tencent.open;

import android.app.Activity;
import android.os.Bundle;
import com.tencent.tauth.IUiListener;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
class g {
    public IUiListener a;
    public Bundle b;
    public String c;
    public WeakReference d;

    public g(Activity activity, String str, Bundle bundle, IUiListener iUiListener) {
        this.d = new WeakReference(activity);
        this.c = str;
        this.b = bundle;
        this.a = iUiListener;
    }
}
