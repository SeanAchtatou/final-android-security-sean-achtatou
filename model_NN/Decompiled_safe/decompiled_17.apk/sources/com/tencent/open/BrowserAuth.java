package com.tencent.open;

import com.tencent.tauth.IUiListener;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: ProGuard */
public class BrowserAuth {
    static final /* synthetic */ boolean c;
    private static int d = 0;
    public Map a = Collections.synchronizedMap(new WeakHashMap());
    public final String b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /* compiled from: ProGuard */
    public class Auth {
        public IUiListener a;
        public TDialog b;
        public String c;
    }

    static {
        boolean z;
        if (!BrowserAuth.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        c = z;
    }

    public static BrowserAuth a() {
        return a.a;
    }

    public Auth a(String str) {
        try {
            if (!this.a.containsKey(str)) {
                return null;
            }
            return (Auth) this.a.get(str);
        } catch (NullPointerException e) {
            return null;
        }
    }

    public String a(Auth auth) {
        d++;
        try {
            this.a.put("" + d, auth);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return "" + d;
    }

    public void b(String str) {
        this.a.remove(str);
    }

    public String b() {
        int ceil = (int) Math.ceil((Math.random() * 20.0d) + 3.0d);
        char[] charArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        int length = charArray.length;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < ceil; i++) {
            stringBuffer.append(charArray[(int) (Math.random() * ((double) length))]);
        }
        return stringBuffer.toString();
    }

    public String a(String str, String str2) {
        return b(str, str2);
    }

    private String b(String str, String str2) {
        if (c || str.length() % 2 == 0) {
            StringBuilder sb = new StringBuilder();
            int length = str2.length();
            int length2 = str.length() / 2;
            int i = 0;
            for (int i2 = 0; i2 < length2; i2++) {
                sb.append((char) (Integer.parseInt(str.substring(i2 * 2, (i2 * 2) + 2), 16) ^ str2.charAt(i)));
                i = (i + 1) % length;
            }
            return sb.toString();
        }
        throw new AssertionError();
    }
}
