package com.tencent.open;

import android.content.Context;

/* compiled from: ProGuard */
public class TContext {
    private String a;
    private String b;
    private long c = -1;
    private String d;
    private Context e;

    public TContext(String str, Context context) {
        this.a = str;
        a(context);
    }

    public boolean a() {
        if (this.b == null || System.currentTimeMillis() >= this.c) {
            return false;
        }
        return true;
    }

    public String b() {
        return this.b;
    }

    public void a(String str, String str2) {
        this.c = 0;
        this.b = null;
        if (str2 == null) {
            str2 = "0";
        }
        this.c = System.currentTimeMillis() + (Long.parseLong(str2) * 1000);
        this.b = str;
    }

    public String c() {
        return this.d;
    }

    public void a(String str) {
        this.d = str;
    }

    public String d() {
        return this.a;
    }

    public long e() {
        return this.c;
    }

    public Context f() {
        return this.e;
    }

    public void a(Context context) {
        this.e = context;
    }
}
