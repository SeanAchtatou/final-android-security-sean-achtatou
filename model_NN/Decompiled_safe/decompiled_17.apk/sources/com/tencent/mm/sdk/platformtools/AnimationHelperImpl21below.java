package com.tencent.mm.sdk.platformtools;

import android.view.View;
import android.view.animation.Animation;
import com.tencent.mm.sdk.platformtools.BackwardSupportUtil;

class AnimationHelperImpl21below implements BackwardSupportUtil.AnimationHelper.IHelper {
    AnimationHelperImpl21below() {
    }

    public void cancelAnimation(View view, Animation animation) {
        if (view != null) {
            view.setAnimation(null);
        }
    }
}
