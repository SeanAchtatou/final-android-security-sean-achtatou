package com.tencent.mm.sdk.platformtools;

import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class KVConfig {
    private static boolean aa = false;

    private static void a(Map<String, String> map) {
        if (map == null || map.size() <= 0) {
            Log.v("MicroMsg.SDK.KVConfig", "empty values");
            return;
        }
        for (Map.Entry next : map.entrySet()) {
            Log.v("MicroMsg.SDK.KVConfig", "key=" + ((String) next.getKey()) + " value=" + ((String) next.getValue()));
        }
    }

    private static void a(Map<String, String> map, String str, Node node, int i) {
        if (node.getNodeName().equals("#text")) {
            map.put(str, node.getNodeValue());
        } else if (node.getNodeName().equals("#cdata-section")) {
            map.put(str, node.getNodeValue());
        } else {
            String str2 = str + "." + node.getNodeName() + (i > 0 ? Integer.valueOf(i) : "");
            map.put(str2, node.getNodeValue());
            NamedNodeMap attributes = node.getAttributes();
            if (attributes != null) {
                for (int i2 = 0; i2 < attributes.getLength(); i2++) {
                    Node item = attributes.item(i2);
                    map.put(str2 + ".$" + item.getNodeName(), item.getNodeValue());
                }
            }
            HashMap hashMap = new HashMap();
            NodeList childNodes = node.getChildNodes();
            for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                Node item2 = childNodes.item(i3);
                int nullAsNil = Util.nullAsNil((Integer) hashMap.get(item2.getNodeName()));
                a(map, str2, item2, nullAsNil);
                hashMap.put(item2.getNodeName(), Integer.valueOf(nullAsNil + 1));
            }
        }
    }

    public static Map<String, String> parseIni(String str) {
        String[] split;
        if (str == null || str.length() <= 0) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (String str2 : str.split("\n")) {
            if (str2 != null && str2.length() > 0 && (split = str2.trim().split("=", 2)) != null && split.length >= 2) {
                String str3 = split[0];
                String str4 = split[1];
                if (str3 != null && str3.length() > 0 && str3.matches("^[a-zA-Z0-9_]*")) {
                    hashMap.put(str3, str4);
                }
            }
        }
        if (!aa) {
            return hashMap;
        }
        a(hashMap);
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0076, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0077, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007c, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0080, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0081, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d8, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0076 A[ExcHandler: SAXException (r1v7 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:15:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007b A[ExcHandler: IOException (r1v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:15:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0080 A[ExcHandler: Exception (r1v5 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:15:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0085  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> parseXml(java.lang.String r8, java.lang.String r9, java.lang.String r10) {
        /*
            r7 = 1
            r6 = 0
            r0 = 0
            r1 = 60
            int r1 = r8.indexOf(r1)
            if (r1 >= 0) goto L_0x0013
            java.lang.String r1 = "MicroMsg.SDK.KVConfig"
            java.lang.String r2 = "text not in xml format"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
        L_0x0012:
            return r0
        L_0x0013:
            if (r1 <= 0) goto L_0x0028
            java.lang.String r2 = "MicroMsg.SDK.KVConfig"
            java.lang.String r3 = "fix xml header from + %d"
            java.lang.Object[] r4 = new java.lang.Object[r7]
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)
            r4[r6] = r5
            com.tencent.mm.sdk.platformtools.Log.w(r2, r3, r4)
            java.lang.String r8 = r8.substring(r1)
        L_0x0028:
            if (r8 == 0) goto L_0x0012
            int r1 = r8.length()
            if (r1 <= 0) goto L_0x0012
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            javax.xml.parsers.DocumentBuilderFactory r2 = javax.xml.parsers.DocumentBuilderFactory.newInstance()
            javax.xml.parsers.DocumentBuilder r2 = r2.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x0047 }
            if (r2 != 0) goto L_0x004c
            java.lang.String r1 = "MicroMsg.SDK.KVConfig"
            java.lang.String r2 = "new Document Builder failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x0012
        L_0x0047:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0012
        L_0x004c:
            org.xml.sax.InputSource r3 = new org.xml.sax.InputSource     // Catch:{ DOMException -> 0x0070, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ DOMException -> 0x0070, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
            byte[] r5 = r8.getBytes()     // Catch:{ DOMException -> 0x0070, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
            r4.<init>(r5)     // Catch:{ DOMException -> 0x0070, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
            r3.<init>(r4)     // Catch:{ DOMException -> 0x0070, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
            if (r10 == 0) goto L_0x005f
            r3.setEncoding(r10)     // Catch:{ DOMException -> 0x0070, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
        L_0x005f:
            org.w3c.dom.Document r3 = r2.parse(r3)     // Catch:{ DOMException -> 0x0070, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
            r3.normalize()     // Catch:{ DOMException -> 0x00d8, SAXException -> 0x0076, IOException -> 0x007b, Exception -> 0x0080 }
        L_0x0066:
            if (r3 != 0) goto L_0x0085
            java.lang.String r1 = "MicroMsg.SDK.KVConfig"
            java.lang.String r2 = "new Document failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x0012
        L_0x0070:
            r2 = move-exception
            r3 = r0
        L_0x0072:
            r2.printStackTrace()
            goto L_0x0066
        L_0x0076:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0012
        L_0x007b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0012
        L_0x0080:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0012
        L_0x0085:
            org.w3c.dom.Element r2 = r3.getDocumentElement()
            if (r2 != 0) goto L_0x0093
            java.lang.String r1 = "MicroMsg.SDK.KVConfig"
            java.lang.String r2 = "getDocumentElement failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x0012
        L_0x0093:
            if (r9 == 0) goto L_0x00ae
            java.lang.String r3 = r2.getNodeName()
            boolean r3 = r9.equals(r3)
            if (r3 == 0) goto L_0x00ae
            java.lang.String r0 = ""
            a(r1, r0, r2, r6)
        L_0x00a4:
            boolean r0 = com.tencent.mm.sdk.platformtools.KVConfig.aa
            if (r0 == 0) goto L_0x00ab
            a(r1)
        L_0x00ab:
            r0 = r1
            goto L_0x0012
        L_0x00ae:
            org.w3c.dom.NodeList r2 = r2.getElementsByTagName(r9)
            int r3 = r2.getLength()
            if (r3 > 0) goto L_0x00c1
            java.lang.String r1 = "MicroMsg.SDK.KVConfig"
            java.lang.String r2 = "parse item null"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x0012
        L_0x00c1:
            int r0 = r2.getLength()
            if (r0 <= r7) goto L_0x00ce
            java.lang.String r0 = "MicroMsg.SDK.KVConfig"
            java.lang.String r3 = "parse items more than one"
            com.tencent.mm.sdk.platformtools.Log.w(r0, r3)
        L_0x00ce:
            java.lang.String r0 = ""
            org.w3c.dom.Node r2 = r2.item(r6)
            a(r1, r0, r2, r6)
            goto L_0x00a4
        L_0x00d8:
            r2 = move-exception
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mm.sdk.platformtools.KVConfig.parseXml(java.lang.String, java.lang.String, java.lang.String):java.util.Map");
    }
}
