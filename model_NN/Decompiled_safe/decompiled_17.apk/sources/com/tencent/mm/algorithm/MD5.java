package com.tencent.mm.algorithm;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

public final class MD5 {
    private MD5() {
    }

    public static String getMD5(File file) {
        return getMD5(file, 102400);
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x003c A[SYNTHETIC, Splitter:B:28:0x003c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getMD5(java.io.File r7, int r8) {
        /*
            r1 = 0
            if (r7 == 0) goto L_0x000b
            if (r8 <= 0) goto L_0x000b
            boolean r0 = r7.exists()
            if (r0 != 0) goto L_0x000d
        L_0x000b:
            r0 = r1
        L_0x000c:
            return r0
        L_0x000d:
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x002f, all -> 0x0038 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x002f, all -> 0x0038 }
            long r3 = (long) r8
            long r5 = r7.length()     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x002a
            long r3 = (long) r8     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
        L_0x001c:
            int r0 = (int) r3     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
            java.lang.String r0 = getMD5(r2, r0)     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
            r2.close()     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
            r2.close()     // Catch:{ IOException -> 0x0028 }
            goto L_0x000c
        L_0x0028:
            r1 = move-exception
            goto L_0x000c
        L_0x002a:
            long r3 = r7.length()     // Catch:{ Exception -> 0x0046, all -> 0x0044 }
            goto L_0x001c
        L_0x002f:
            r0 = move-exception
            r0 = r1
        L_0x0031:
            if (r0 == 0) goto L_0x0036
            r0.close()     // Catch:{ IOException -> 0x0040 }
        L_0x0036:
            r0 = r1
            goto L_0x000c
        L_0x0038:
            r0 = move-exception
            r2 = r1
        L_0x003a:
            if (r2 == 0) goto L_0x003f
            r2.close()     // Catch:{ IOException -> 0x0042 }
        L_0x003f:
            throw r0
        L_0x0040:
            r0 = move-exception
            goto L_0x0036
        L_0x0042:
            r1 = move-exception
            goto L_0x003f
        L_0x0044:
            r0 = move-exception
            goto L_0x003a
        L_0x0046:
            r0 = move-exception
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mm.algorithm.MD5.getMD5(java.io.File, int):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0028 A[SYNTHETIC, Splitter:B:18:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0031 A[SYNTHETIC, Splitter:B:24:0x0031] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getMD5(java.io.File r3, int r4, int r5) {
        /*
            r1 = 0
            if (r3 == 0) goto L_0x000d
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x000d
            if (r4 < 0) goto L_0x000d
            if (r5 > 0) goto L_0x000f
        L_0x000d:
            r0 = r1
        L_0x000e:
            return r0
        L_0x000f:
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0024, all -> 0x002d }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0024, all -> 0x002d }
            r0 = 102400(0x19000, float:1.43493E-40)
            java.lang.String r0 = getMD5(r2, r0, r4, r5)     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            r2.close()     // Catch:{ Exception -> 0x003b, all -> 0x0039 }
            r2.close()     // Catch:{ IOException -> 0x0022 }
            goto L_0x000e
        L_0x0022:
            r1 = move-exception
            goto L_0x000e
        L_0x0024:
            r0 = move-exception
            r0 = r1
        L_0x0026:
            if (r0 == 0) goto L_0x002b
            r0.close()     // Catch:{ IOException -> 0x0035 }
        L_0x002b:
            r0 = r1
            goto L_0x000e
        L_0x002d:
            r0 = move-exception
            r2 = r1
        L_0x002f:
            if (r2 == 0) goto L_0x0034
            r2.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0034:
            throw r0
        L_0x0035:
            r0 = move-exception
            goto L_0x002b
        L_0x0037:
            r1 = move-exception
            goto L_0x0034
        L_0x0039:
            r0 = move-exception
            goto L_0x002f
        L_0x003b:
            r0 = move-exception
            r0 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mm.algorithm.MD5.getMD5(java.io.File, int, int):java.lang.String");
    }

    public static final String getMD5(FileInputStream fileInputStream, int i) {
        if (fileInputStream == null || i <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            StringBuilder sb = new StringBuilder(32);
            byte[] bArr = new byte[i];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                instance.update(bArr, 0, read);
            }
            byte[] digest = instance.digest();
            for (byte b : digest) {
                sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static final String getMD5(FileInputStream fileInputStream, int i, int i2, int i3) {
        if (fileInputStream == null || i <= 0 || i2 < 0 || i3 <= 0) {
            return null;
        }
        try {
            if (fileInputStream.skip((long) i2) < ((long) i2)) {
                return null;
            }
            MessageDigest instance = MessageDigest.getInstance("MD5");
            StringBuilder sb = new StringBuilder(32);
            byte[] bArr = new byte[i];
            int i4 = 0;
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read == -1 || i4 >= i3) {
                    byte[] digest = instance.digest();
                } else if (i4 + read <= i3) {
                    instance.update(bArr, 0, read);
                    i4 += read;
                } else {
                    instance.update(bArr, 0, i3 - i4);
                    i4 = i3;
                }
            }
            byte[] digest2 = instance.digest();
            for (byte b : digest2) {
                sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static String getMD5(String str) {
        if (str == null) {
            return null;
        }
        File file = new File(str);
        if (file.exists()) {
            return getMD5(file, 102400);
        }
        return null;
    }

    public static String getMD5(String str, int i, int i2) {
        if (str == null) {
            return null;
        }
        File file = new File(str);
        if (file.exists()) {
            return getMD5(file, i, i2);
        }
        return null;
    }

    public static final String getMessageDigest(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            char[] cArr2 = new char[(r4 * 2)];
            int i = 0;
            for (byte b : instance.digest()) {
                int i2 = i + 1;
                cArr2[i] = cArr[(b >>> 4) & 15];
                i = i2 + 1;
                cArr2[i2] = cArr[b & 15];
            }
            return new String(cArr2);
        } catch (Exception e) {
            return null;
        }
    }

    public static final byte[] getRawDigest(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return instance.digest();
        } catch (Exception e) {
            return null;
        }
    }
}
