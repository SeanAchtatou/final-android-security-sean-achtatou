package com.umeng.common.b;

import java.io.UnsupportedEncodingException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class f {
    public static int a;

    public static String a(byte[] bArr, String str) throws UnsupportedEncodingException, DataFormatException {
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        Inflater inflater = new Inflater();
        byte[] bArr2 = new byte[100];
        inflater.setInput(bArr, 0, bArr.length);
        StringBuilder sb = new StringBuilder();
        while (!inflater.needsInput()) {
            sb.append(new String(bArr2, 0, inflater.inflate(bArr2), str));
        }
        inflater.end();
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.String r6, java.lang.String r7) throws java.io.IOException {
        /*
            r0 = 0
            r4 = 0
            boolean r1 = com.umeng.common.b.g.c(r6)
            if (r1 == 0) goto L_0x0009
        L_0x0008:
            return r0
        L_0x0009:
            java.util.zip.Deflater r2 = new java.util.zip.Deflater
            r2.<init>()
            byte[] r1 = r6.getBytes(r7)
            r2.setInput(r1)
            r2.finish()
            r1 = 8192(0x2000, float:1.14794E-41)
            byte[] r3 = new byte[r1]
            com.umeng.common.b.f.a = r4
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x004b }
            r1.<init>()     // Catch:{ all -> 0x004b }
        L_0x0023:
            boolean r0 = r2.finished()     // Catch:{ all -> 0x0037 }
            if (r0 != 0) goto L_0x003e
            int r0 = r2.deflate(r3)     // Catch:{ all -> 0x0037 }
            int r4 = com.umeng.common.b.f.a     // Catch:{ all -> 0x0037 }
            int r4 = r4 + r0
            com.umeng.common.b.f.a = r4     // Catch:{ all -> 0x0037 }
            r4 = 0
            r1.write(r3, r4, r0)     // Catch:{ all -> 0x0037 }
            goto L_0x0023
        L_0x0037:
            r0 = move-exception
        L_0x0038:
            if (r1 == 0) goto L_0x003d
            r1.close()
        L_0x003d:
            throw r0
        L_0x003e:
            r2.end()     // Catch:{ all -> 0x0037 }
            if (r1 == 0) goto L_0x0046
            r1.close()
        L_0x0046:
            byte[] r0 = r1.toByteArray()
            goto L_0x0008
        L_0x004b:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.b.f.a(java.lang.String, java.lang.String):byte[]");
    }
}
