package com.umeng.common.b;

import java.math.BigInteger;

public class c extends d {
    static final byte[] a = {13, 10};
    private static final int m = 6;
    private static final int n = 3;
    private static final int o = 4;
    private static final byte[] p = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] q = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] r = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private static final int s = 63;
    private final byte[] t;
    private final byte[] u;
    private final byte[] v;
    private final int w;
    private final int x;
    private int y;

    public c() {
        this(0);
    }

    public c(int i) {
        this(i, a);
    }

    public c(int i, byte[] bArr) {
        this(i, bArr, false);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(int i, byte[] bArr, boolean z) {
        super(3, 4, i, bArr == null ? 0 : bArr.length);
        this.u = r;
        if (bArr == null) {
            this.x = 4;
            this.v = null;
        } else if (n(bArr)) {
            throw new IllegalArgumentException("lineSeparator must not contain base64 characters: [" + a.f(bArr) + "]");
        } else if (i > 0) {
            this.x = bArr.length + 4;
            this.v = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.v, 0, bArr.length);
        } else {
            this.x = 4;
            this.v = null;
        }
        this.w = this.x - 1;
        this.t = z ? q : p;
    }

    public c(boolean z) {
        this(76, a, z);
    }

    public static boolean a(byte b) {
        return b == 61 || (b >= 0 && b < r.length && r[b] != -1);
    }

    public static boolean a(String str) {
        return b(a.f(str));
    }

    public static boolean a(byte[] bArr) {
        return b(bArr);
    }

    public static byte[] a(BigInteger bigInteger) {
        if (bigInteger != null) {
            return a(b(bigInteger), false);
        }
        throw new NullPointerException("encodeInteger called with null parameter");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.common.b.c.a(byte[], boolean, boolean):byte[]
     arg types: [byte[], boolean, int]
     candidates:
      com.umeng.common.b.c.a(byte[], int, int):void
      com.umeng.common.b.d.a(byte[], int, int):void
      com.umeng.common.b.c.a(byte[], boolean, boolean):byte[] */
    public static byte[] a(byte[] bArr, boolean z) {
        return a(bArr, z, false);
    }

    public static byte[] a(byte[] bArr, boolean z, boolean z2) {
        return a(bArr, z, z2, Integer.MAX_VALUE);
    }

    public static byte[] a(byte[] bArr, boolean z, boolean z2, int i) {
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        c cVar = z ? new c(z2) : new c(0, a, z2);
        long o2 = cVar.o(bArr);
        if (o2 <= ((long) i)) {
            return cVar.l(bArr);
        }
        throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + o2 + ") than the specified maximum size of " + i);
    }

    public static boolean b(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            if (!a(bArr[i]) && !c(bArr[i])) {
                return false;
            }
        }
        return true;
    }

    public static byte[] b(String str) {
        return new c().c(str);
    }

    static byte[] b(BigInteger bigInteger) {
        int bitLength = ((bigInteger.bitLength() + 7) >> 3) << 3;
        byte[] byteArray = bigInteger.toByteArray();
        if (bigInteger.bitLength() % 8 != 0 && (bigInteger.bitLength() / 8) + 1 == bitLength / 8) {
            return byteArray;
        }
        int i = 0;
        int length = byteArray.length;
        if (bigInteger.bitLength() % 8 == 0) {
            i = 1;
            length--;
        }
        byte[] bArr = new byte[(bitLength / 8)];
        System.arraycopy(byteArray, i, bArr, (bitLength / 8) - length, length);
        return bArr;
    }

    public static byte[] c(byte[] bArr) {
        return a(bArr, false);
    }

    public static String d(byte[] bArr) {
        return a.f(a(bArr, false));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.common.b.c.a(byte[], boolean, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.umeng.common.b.c.a(byte[], int, int):void
      com.umeng.common.b.d.a(byte[], int, int):void
      com.umeng.common.b.c.a(byte[], boolean, boolean):byte[] */
    public static byte[] e(byte[] bArr) {
        return a(bArr, false, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.common.b.c.a(byte[], boolean, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.umeng.common.b.c.a(byte[], int, int):void
      com.umeng.common.b.d.a(byte[], int, int):void
      com.umeng.common.b.c.a(byte[], boolean, boolean):byte[] */
    public static String f(byte[] bArr) {
        return a.f(a(bArr, false, true));
    }

    public static byte[] g(byte[] bArr) {
        return a(bArr, true);
    }

    public static byte[] h(byte[] bArr) {
        return new c().k(bArr);
    }

    public static BigInteger i(byte[] bArr) {
        return new BigInteger(1, h(bArr));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v34, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v35, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(byte[] r8, int r9, int r10) {
        /*
            r7 = this;
            r6 = 61
            r2 = 0
            boolean r0 = r7.j
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r10 >= 0) goto L_0x00d8
            r0 = 1
            r7.j = r0
            int r0 = r7.l
            if (r0 != 0) goto L_0x0015
            int r0 = r7.g
            if (r0 == 0) goto L_0x0007
        L_0x0015:
            int r0 = r7.x
            r7.a(r0)
            int r0 = r7.i
            int r1 = r7.l
            switch(r1) {
                case 1: goto L_0x0047;
                case 2: goto L_0x008a;
                default: goto L_0x0021;
            }
        L_0x0021:
            int r1 = r7.k
            int r3 = r7.i
            int r0 = r3 - r0
            int r0 = r0 + r1
            r7.k = r0
            int r0 = r7.g
            if (r0 <= 0) goto L_0x0007
            int r0 = r7.k
            if (r0 <= 0) goto L_0x0007
            byte[] r0 = r7.v
            byte[] r1 = r7.h
            int r3 = r7.i
            byte[] r4 = r7.v
            int r4 = r4.length
            java.lang.System.arraycopy(r0, r2, r1, r3, r4)
            int r0 = r7.i
            byte[] r1 = r7.v
            int r1 = r1.length
            int r0 = r0 + r1
            r7.i = r0
            goto L_0x0007
        L_0x0047:
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            byte[] r4 = r7.t
            int r5 = r7.y
            int r5 = r5 >> 2
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            byte[] r4 = r7.t
            int r5 = r7.y
            int r5 = r5 << 4
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.t
            byte[] r3 = com.umeng.common.b.c.p
            if (r1 != r3) goto L_0x0021
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            r1[r3] = r6
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            r1[r3] = r6
            goto L_0x0021
        L_0x008a:
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            byte[] r4 = r7.t
            int r5 = r7.y
            int r5 = r5 >> 10
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            byte[] r4 = r7.t
            int r5 = r7.y
            int r5 = r5 >> 4
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            byte[] r4 = r7.t
            int r5 = r7.y
            int r5 = r5 << 2
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.t
            byte[] r3 = com.umeng.common.b.c.p
            if (r1 != r3) goto L_0x0021
            byte[] r1 = r7.h
            int r3 = r7.i
            int r4 = r3 + 1
            r7.i = r4
            r1[r3] = r6
            goto L_0x0021
        L_0x00d8:
            r1 = r2
        L_0x00d9:
            if (r1 >= r10) goto L_0x0007
            int r0 = r7.x
            r7.a(r0)
            int r0 = r7.l
            int r0 = r0 + 1
            int r0 = r0 % 3
            r7.l = r0
            int r3 = r9 + 1
            byte r0 = r8[r9]
            if (r0 >= 0) goto L_0x00f0
            int r0 = r0 + 256
        L_0x00f0:
            int r4 = r7.y
            int r4 = r4 << 8
            int r0 = r0 + r4
            r7.y = r0
            int r0 = r7.l
            if (r0 != 0) goto L_0x016f
            byte[] r0 = r7.h
            int r4 = r7.i
            int r5 = r4 + 1
            r7.i = r5
            byte[] r5 = r7.t
            int r6 = r7.y
            int r6 = r6 >> 18
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            byte[] r0 = r7.h
            int r4 = r7.i
            int r5 = r4 + 1
            r7.i = r5
            byte[] r5 = r7.t
            int r6 = r7.y
            int r6 = r6 >> 12
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            byte[] r0 = r7.h
            int r4 = r7.i
            int r5 = r4 + 1
            r7.i = r5
            byte[] r5 = r7.t
            int r6 = r7.y
            int r6 = r6 >> 6
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            byte[] r0 = r7.h
            int r4 = r7.i
            int r5 = r4 + 1
            r7.i = r5
            byte[] r5 = r7.t
            int r6 = r7.y
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            int r0 = r7.k
            int r0 = r0 + 4
            r7.k = r0
            int r0 = r7.g
            if (r0 <= 0) goto L_0x016f
            int r0 = r7.g
            int r4 = r7.k
            if (r0 > r4) goto L_0x016f
            byte[] r0 = r7.v
            byte[] r4 = r7.h
            int r5 = r7.i
            byte[] r6 = r7.v
            int r6 = r6.length
            java.lang.System.arraycopy(r0, r2, r4, r5, r6)
            int r0 = r7.i
            byte[] r4 = r7.v
            int r4 = r4.length
            int r0 = r0 + r4
            r7.i = r0
            r7.k = r2
        L_0x016f:
            int r0 = r1 + 1
            r1 = r0
            r9 = r3
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.b.c.a(byte[], int, int):void");
    }

    public boolean a() {
        return this.t == q;
    }

    /* access modifiers changed from: package-private */
    public void b(byte[] bArr, int i, int i2) {
        byte b;
        if (!this.j) {
            if (i2 < 0) {
                this.j = true;
            }
            int i3 = 0;
            while (true) {
                if (i3 >= i2) {
                    break;
                }
                a(this.w);
                int i4 = i + 1;
                byte b2 = bArr[i];
                if (b2 == 61) {
                    this.j = true;
                    break;
                }
                if (b2 >= 0 && b2 < r.length && (b = r[b2]) >= 0) {
                    this.l = (this.l + 1) % 4;
                    this.y = b + (this.y << 6);
                    if (this.l == 0) {
                        byte[] bArr2 = this.h;
                        int i5 = this.i;
                        this.i = i5 + 1;
                        bArr2[i5] = (byte) ((this.y >> 16) & 255);
                        byte[] bArr3 = this.h;
                        int i6 = this.i;
                        this.i = i6 + 1;
                        bArr3[i6] = (byte) ((this.y >> 8) & 255);
                        byte[] bArr4 = this.h;
                        int i7 = this.i;
                        this.i = i7 + 1;
                        bArr4[i7] = (byte) (this.y & 255);
                    }
                }
                i3++;
                i = i4;
            }
            if (this.j && this.l != 0) {
                a(this.w);
                switch (this.l) {
                    case 2:
                        this.y >>= 4;
                        byte[] bArr5 = this.h;
                        int i8 = this.i;
                        this.i = i8 + 1;
                        bArr5[i8] = (byte) (this.y & 255);
                        return;
                    case 3:
                        this.y >>= 2;
                        byte[] bArr6 = this.h;
                        int i9 = this.i;
                        this.i = i9 + 1;
                        bArr6[i9] = (byte) ((this.y >> 8) & 255);
                        byte[] bArr7 = this.h;
                        int i10 = this.i;
                        this.i = i10 + 1;
                        bArr7[i10] = (byte) (this.y & 255);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean b(byte b) {
        return b >= 0 && b < this.u.length && this.u[b] != -1;
    }
}
