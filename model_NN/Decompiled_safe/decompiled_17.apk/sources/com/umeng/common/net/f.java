package com.umeng.common.net;

import android.os.AsyncTask;
import com.umeng.common.net.h;

public class f extends j {

    public interface a {
        void a();

        void a(h.a aVar);
    }

    private class b extends AsyncTask<Integer, Integer, h.a> {
        private g b;
        private a c;

        public b(g gVar, a aVar) {
            this.b = gVar;
            this.c = aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public h.a doInBackground(Integer... numArr) {
            return f.this.a(this.b);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(h.a aVar) {
            if (this.c != null) {
                this.c.a(aVar);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (this.c != null) {
                this.c.a();
            }
        }
    }

    public h.a a(g gVar) {
        h hVar = (h) a(gVar, h.class);
        return hVar == null ? h.a.FAIL : hVar.a;
    }

    public void a(g gVar, a aVar) {
        new b(gVar, aVar).execute(new Integer[0]);
    }
}
