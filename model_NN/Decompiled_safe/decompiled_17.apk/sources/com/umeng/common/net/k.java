package com.umeng.common.net;

import org.json.JSONObject;

public abstract class k {
    protected static String a = "POST";
    protected static String b = "GET";
    protected String c;

    public k(String str) {
        this.c = str;
    }

    public abstract JSONObject a();

    public void a(String str) {
        this.c = str;
    }

    public abstract String b();

    /* access modifiers changed from: protected */
    public String c() {
        return a;
    }

    public String d() {
        return this.c;
    }
}
