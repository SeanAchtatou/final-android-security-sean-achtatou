package com.umeng.common.net;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.widget.RemoteViews;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import com.umeng.common.net.a;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class DownloadingService extends Service {
    static final int a = 3;
    static final int b = 4;
    static final int c = 5;
    static final int d = 0;
    static final int e = 1;
    static final int f = 100;
    static final String g = "filename";
    /* access modifiers changed from: private */
    public static final String i = DownloadingService.class.getName();
    /* access modifiers changed from: private */
    public static Map<a.C0001a, Messenger> l = new HashMap();
    final Messenger h = new Messenger(new b());
    /* access modifiers changed from: private */
    public NotificationManager j;
    /* access modifiers changed from: private */
    public String[] k;

    class a extends Thread {
        private static final long j = 30000;
        /* access modifiers changed from: private */
        public Context b;
        private String c;
        private Notification d;
        /* access modifiers changed from: private */
        public int e;
        private int f = 0;
        private int g = -1;
        private int h = -1;
        /* access modifiers changed from: private */
        public a.C0001a i;
        private Handler k = new d(this);

        public a(Context context, a.C0001a aVar) {
            try {
                this.b = context;
                this.i = aVar;
                if (com.umeng.common.b.b()) {
                    this.c = Environment.getExternalStorageDirectory().getCanonicalPath();
                    new File(this.c).mkdirs();
                } else {
                    this.c = this.b.getFilesDir().getAbsolutePath();
                }
                this.c += "/download/.um/apk";
                new File(this.c).mkdirs();
                this.d = new Notification(17301633, com.umeng.common.a.j, 1);
                RemoteViews remoteViews = new RemoteViews(this.b.getPackageName(), com.umeng.common.a.b.a(this.b));
                remoteViews.setProgressBar(com.umeng.common.a.a.c(this.b), 100, 0, false);
                remoteViews.setTextViewText(com.umeng.common.a.a.b(this.b), "0%");
                remoteViews.setTextViewText(com.umeng.common.a.a.d(this.b), com.umeng.common.a.j + this.i.b);
                remoteViews.setTextViewText(com.umeng.common.a.a.a(this.b), "");
                remoteViews.setImageViewResource(com.umeng.common.a.a.e(this.b), 17301633);
                this.d.contentView = remoteViews;
                this.d.contentIntent = PendingIntent.getActivity(this.b, 0, new Intent(), 134217728);
                this.e = (int) System.currentTimeMillis();
                if (this.e < 0) {
                    this.e = -this.e;
                }
                DownloadingService.this.j.notify(this.e, this.d);
            } catch (Exception e2) {
                Log.c(DownloadingService.i, e2.getMessage(), e2);
                DownloadingService.this.j.cancel(this.e);
            }
        }

        private void a(Exception exc) {
            Log.b(DownloadingService.i, "can not install. " + exc.getMessage());
            this.d.contentView.setTextViewText(com.umeng.common.a.a.d(this.b), this.i.b + com.umeng.common.a.i);
            DownloadingService.this.j.notify(this.e, this.d);
            DownloadingService.this.j.cancel(this.e);
            DownloadingService.l.remove(this.i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        /* JADX WARNING: Code restructure failed: missing block: B:155:0x042c, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:157:?, code lost:
            com.umeng.common.net.DownloadingService.b().remove(r13.i);
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:158:0x0439, code lost:
            if (r2 != null) goto L_0x043b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:160:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:161:0x043e, code lost:
            if (r1 != null) goto L_0x0440;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:163:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:164:0x0445, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:165:0x0446, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:166:0x044b, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:168:?, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:169:0x044f, code lost:
            if (r1 != null) goto L_0x0451;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:171:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:172:0x0456, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:173:0x0457, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:174:0x045c, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:175:0x045d, code lost:
            if (r1 != null) goto L_0x045f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:177:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:178:0x0462, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:179:0x0463, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:180:0x0464, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:225:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:226:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:227:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:228:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:234:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:235:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x0268, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x0269, code lost:
            r3 = r2;
            r2 = r1;
            r1 = r0;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:155:0x042c A[ExcHandler: RemoteException (r0v1 'e' android.os.RemoteException A[CUSTOM_DECLARE]), PHI: r1 
          PHI: (r1v3 java.io.FileOutputStream) = (r1v0 java.io.FileOutputStream), (r1v0 java.io.FileOutputStream), (r1v19 java.io.FileOutputStream), (r1v19 java.io.FileOutputStream), (r1v19 java.io.FileOutputStream), (r1v19 java.io.FileOutputStream), (r1v19 java.io.FileOutputStream), (r1v19 java.io.FileOutputStream), (r1v19 java.io.FileOutputStream) binds: [B:1:0x0002, B:34:0x017e, B:6:0x0032, B:84:0x02d8, B:39:0x019c, B:40:?, B:41:0x020f, B:45:0x0231, B:15:0x00b8] A[DONT_GENERATE, DONT_INLINE], Splitter:B:6:0x0032] */
        /* JADX WARNING: Removed duplicated region for block: B:214:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x02a5 A[SYNTHETIC, Splitter:B:60:0x02a5] */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x02aa A[SYNTHETIC, Splitter:B:63:0x02aa] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a(boolean r14) {
            /*
                r13 = this;
                r2 = 0
                r1 = 0
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                r0.<init>()     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.lang.String r3 = r3.c     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.lang.String r3 = com.umeng.common.b.g.a(r3)     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.lang.String r3 = ".apk.tmp"
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.lang.String r3 = r0.toString()     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                boolean r0 = com.umeng.common.b.b()     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                if (r0 == 0) goto L_0x017e
                java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.lang.String r4 = r13.c     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                r0.<init>(r4, r3)     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                r4 = 1
                r3.<init>(r0, r4)     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                r5 = r0
                r1 = r3
            L_0x0032:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r3 = "saveAPK: url = %1$15s\t|\tfilename = %2$15s"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r6 = 0
                com.umeng.common.net.a$a r7 = r13.i     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r7 = r7.c     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r4[r6] = r7     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r6 = 1
                java.lang.String r7 = r5.getAbsolutePath()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r4[r6] = r7     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                com.umeng.common.Log.c(r0, r3)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r3 = r3.c     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r0.<init>(r3)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r3 = "GET"
                r0.setRequestMethod(r3)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r3 = "Accept-Encoding"
                java.lang.String r4 = "identity"
                r0.setRequestProperty(r3, r4)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r3 = "Connection"
                java.lang.String r4 = "keep-alive"
                r0.addRequestProperty(r3, r4)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r3 = 5000(0x1388, float:7.006E-42)
                r0.setConnectTimeout(r3)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r3 = 10000(0x2710, float:1.4013E-41)
                r0.setReadTimeout(r3)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                boolean r3 = r5.exists()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                if (r3 == 0) goto L_0x00ae
                long r3 = r5.length()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r6 = 0
                int r3 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
                if (r3 <= 0) goto L_0x00ae
                java.lang.String r3 = "Range"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r4.<init>()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r6 = "bytes="
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                long r6 = r5.length()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r6 = "-"
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                r0.setRequestProperty(r3, r4)     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
            L_0x00ae:
                r0.connect()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                java.io.InputStream r2 = r0.getInputStream()     // Catch:{ IOException -> 0x049a, RemoteException -> 0x042c }
                if (r14 != 0) goto L_0x00d9
                r3 = 0
                r13.g = r3     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r0 = r0.getContentLength()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r13.h = r0     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r3 = "getContentLength: %1$15s"
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r6 = 0
                int r7 = r13.h     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r4[r6] = r7     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.Log.c(r0, r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
            L_0x00d9:
                r0 = 4096(0x1000, float:5.74E-42)
                byte[] r6 = new byte[r0]     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0 = 0
                r7 = 50
                r3 = 1
                java.lang.String r4 = com.umeng.common.net.DownloadingService.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r8.<init>()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r9 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = r9.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = "saveAPK getContentLength "
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r9 = r13.h     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.Log.c(r4, r8)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.content.Context r4 = r13.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.c r4 = com.umeng.common.net.c.a(r4)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r8 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r8 = r8.a     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r9 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = r9.c     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r4.a(r8, r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
            L_0x011a:
                int r4 = r2.read(r6)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                if (r4 <= 0) goto L_0x04a3
                r8 = 0
                r1.write(r6, r8, r4)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r8 = r13.g     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r4 = r4 + r8
                r13.g = r4     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r4 = r0 + 1
                int r0 = r0 % r7
                if (r0 != 0) goto L_0x04a0
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                boolean r0 = com.umeng.common.b.m(r0)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                if (r0 != 0) goto L_0x019c
                r0 = 0
            L_0x0137:
                r2.close()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r1.close()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                if (r0 != 0) goto L_0x02d8
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.app.NotificationManager r0 = r0.j     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r3 = r13.e     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.cancel(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r3 = "Download Fail"
                com.umeng.common.Log.b(r0, r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.Object r0 = r0.get(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r3 = 0
                r4 = 5
                r5 = 0
                r6 = 0
                android.os.Message r3 = android.os.Message.obtain(r3, r4, r5, r6)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.send(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.remove(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                if (r2 == 0) goto L_0x0178
                r2.close()     // Catch:{ IOException -> 0x02bb }
            L_0x0178:
                if (r1 == 0) goto L_0x017d
                r1.close()     // Catch:{ IOException -> 0x02b5 }
            L_0x017d:
                return
            L_0x017e:
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.io.File r0 = r0.getFilesDir()     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                r13.c = r0     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                r4 = 32771(0x8003, float:4.5922E-41)
                java.io.FileOutputStream r1 = r0.openFileOutput(r3, r4)     // Catch:{ IOException -> 0x048e, RemoteException -> 0x042c }
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0494, RemoteException -> 0x042c }
                java.io.File r0 = r0.getFileStreamPath(r3)     // Catch:{ IOException -> 0x0494, RemoteException -> 0x042c }
                r5 = r0
                goto L_0x0032
            L_0x019c:
                int r0 = r13.g     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                float r0 = (float) r0     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r8 = 1120403456(0x42c80000, float:100.0)
                float r0 = r0 * r8
                int r8 = r13.h     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                float r8 = (float) r8     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                float r0 = r0 / r8
                int r8 = (int) r0     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.app.Notification r0 = r13.d     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.widget.RemoteViews r0 = r0.contentView     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.content.Context r9 = r13.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r9 = com.umeng.common.a.a.c(r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r10 = 100
                r11 = 0
                r0.setProgressBar(r9, r10, r8, r11)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.app.Notification r0 = r13.d     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.widget.RemoteViews r0 = r0.contentView     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.content.Context r9 = r13.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r9 = com.umeng.common.a.a.b(r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r10.<init>()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r11 = java.lang.String.valueOf(r8)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r11 = "%"
                java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.setTextViewText(r9, r10)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.app.NotificationManager r0 = r0.j     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r9 = r13.e     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.app.Notification r10 = r13.d     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.notify(r9, r10)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = "%3$10s Notification: mNotificationId = %1$15s\t|\tprogress = %2$15s"
                r10 = 3
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r11 = 0
                int r12 = r13.e     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r10[r11] = r12     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r11 = 1
                java.lang.Integer r12 = java.lang.Integer.valueOf(r8)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r10[r11] = r12     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r11 = 2
                com.umeng.common.net.a$a r12 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r12 = r12.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r10[r11] = r12     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = java.lang.String.format(r9, r10)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.Log.c(r0, r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ DeadObjectException -> 0x0245 }
                com.umeng.common.net.a$a r9 = r13.i     // Catch:{ DeadObjectException -> 0x0245 }
                java.lang.Object r0 = r0.get(r9)     // Catch:{ DeadObjectException -> 0x0245 }
                if (r0 == 0) goto L_0x0231
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ DeadObjectException -> 0x0245 }
                com.umeng.common.net.a$a r9 = r13.i     // Catch:{ DeadObjectException -> 0x0245 }
                java.lang.Object r0 = r0.get(r9)     // Catch:{ DeadObjectException -> 0x0245 }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ DeadObjectException -> 0x0245 }
                r9 = 0
                r10 = 3
                r11 = 0
                android.os.Message r9 = android.os.Message.obtain(r9, r10, r8, r11)     // Catch:{ DeadObjectException -> 0x0245 }
                r0.send(r9)     // Catch:{ DeadObjectException -> 0x0245 }
            L_0x0231:
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.c r0 = com.umeng.common.net.c.a(r0)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r9 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = r9.a     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r10 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r10 = r10.c     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.a(r9, r10, r8)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0 = r4
                goto L_0x011a
            L_0x0245:
                r0 = move-exception
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = "Service Client for downloading %1$15s is dead. Removing messenger from the service"
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r11 = 0
                com.umeng.common.net.a$a r12 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r12 = r12.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r10[r11] = r12     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r9 = java.lang.String.format(r9, r10)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.Log.b(r0, r9)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r9 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r10 = 0
                r0.put(r9, r10)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                goto L_0x0231
            L_0x0268:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
            L_0x026c:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ all -> 0x03c9 }
                java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x03c9 }
                com.umeng.common.Log.c(r0, r4, r1)     // Catch:{ all -> 0x03c9 }
                int r0 = r13.f     // Catch:{ all -> 0x03c9 }
                int r0 = r0 + 1
                r13.f = r0     // Catch:{ all -> 0x03c9 }
                r4 = 3
                if (r0 <= r4) goto L_0x03d7
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ RemoteException -> 0x03ba }
                java.lang.String r4 = "Download Fail"
                com.umeng.common.Log.b(r0, r4)     // Catch:{ RemoteException -> 0x03ba }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ RemoteException -> 0x03ba }
                com.umeng.common.net.a$a r4 = r13.i     // Catch:{ RemoteException -> 0x03ba }
                java.lang.Object r0 = r0.get(r4)     // Catch:{ RemoteException -> 0x03ba }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ RemoteException -> 0x03ba }
                r4 = 0
                r5 = 5
                r6 = 0
                r7 = 0
                android.os.Message r4 = android.os.Message.obtain(r4, r5, r6, r7)     // Catch:{ RemoteException -> 0x03ba }
                r0.send(r4)     // Catch:{ RemoteException -> 0x03ba }
            L_0x02a0:
                r13.a(r1)     // Catch:{ all -> 0x03c9 }
            L_0x02a3:
                if (r3 == 0) goto L_0x02a8
                r3.close()     // Catch:{ IOException -> 0x040f }
            L_0x02a8:
                if (r2 == 0) goto L_0x017d
                r2.close()     // Catch:{ IOException -> 0x02af }
                goto L_0x017d
            L_0x02af:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x02b5:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x02bb:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x02cc }
                if (r1 == 0) goto L_0x017d
                r1.close()     // Catch:{ IOException -> 0x02c6 }
                goto L_0x017d
            L_0x02c6:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x02cc:
                r0 = move-exception
                if (r1 == 0) goto L_0x02d2
                r1.close()     // Catch:{ IOException -> 0x02d3 }
            L_0x02d2:
                throw r0
            L_0x02d3:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x02d2
            L_0x02d8:
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String[] r0 = r0.k     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                if (r0 == 0) goto L_0x02e9
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String[] r0 = r0.k     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.DownloadingService.b(r0)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
            L_0x02e9:
                android.app.Notification r0 = r13.d     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.widget.RemoteViews r0 = r0.contentView     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.content.Context r3 = r13.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                int r3 = com.umeng.common.a.a.b(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r4.<init>()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r6 = 100
                java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r6 = "%"
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.setTextViewText(r3, r4)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.c r0 = com.umeng.common.net.c.a(r0)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r3 = r3.a     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r4 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r4 = r4.c     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r6 = 100
                r0.a(r3, r4, r6)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r3 = r5.getParent()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r4 = r5.getName()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r6 = ".tmp"
                java.lang.String r7 = ""
                java.lang.String r4 = r4.replace(r6, r7)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.<init>(r3, r4)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r5.renameTo(r0)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.os.Bundle r3 = new android.os.Bundle     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r3.<init>()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.String r4 = "filename"
                r3.putString(r4, r0)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.os.Message r0 = android.os.Message.obtain()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r4 = 5
                r0.what = r4     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r4 = 1
                r0.arg1 = r4     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.setData(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.os.Handler r4 = r13.k     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r4.sendMessage(r0)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.os.Message r4 = android.os.Message.obtain()     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0 = 5
                r4.what = r0     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0 = 1
                r4.arg1 = r0     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r4.setData(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.Object r0 = r0.get(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                if (r0 == 0) goto L_0x0382
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                java.lang.Object r0 = r0.get(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.send(r4)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
            L_0x0382:
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                com.umeng.common.net.a$a r3 = r13.i     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                r0.remove(r3)     // Catch:{ IOException -> 0x0268, RemoteException -> 0x042c }
                if (r2 == 0) goto L_0x0390
                r2.close()     // Catch:{ IOException -> 0x039d }
            L_0x0390:
                if (r1 == 0) goto L_0x017d
                r1.close()     // Catch:{ IOException -> 0x0397 }
                goto L_0x017d
            L_0x0397:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x039d:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x03ae }
                if (r1 == 0) goto L_0x017d
                r1.close()     // Catch:{ IOException -> 0x03a8 }
                goto L_0x017d
            L_0x03a8:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x03ae:
                r0 = move-exception
                if (r1 == 0) goto L_0x03b4
                r1.close()     // Catch:{ IOException -> 0x03b5 }
            L_0x03b4:
                throw r0
            L_0x03b5:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x03b4
            L_0x03ba:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x03c9 }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.l     // Catch:{ all -> 0x03c9 }
                com.umeng.common.net.a$a r4 = r13.i     // Catch:{ all -> 0x03c9 }
                r0.remove(r4)     // Catch:{ all -> 0x03c9 }
                goto L_0x02a0
            L_0x03c9:
                r0 = move-exception
                r1 = r2
                r2 = r3
            L_0x03cc:
                if (r2 == 0) goto L_0x03d1
                r2.close()     // Catch:{ IOException -> 0x046e }
            L_0x03d1:
                if (r1 == 0) goto L_0x03d6
                r1.close()     // Catch:{ IOException -> 0x0468 }
            L_0x03d6:
                throw r0
            L_0x03d7:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.i     // Catch:{ all -> 0x03c9 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c9 }
                r1.<init>()     // Catch:{ all -> 0x03c9 }
                java.lang.String r4 = "wait for repeating Test network repeat count="
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x03c9 }
                int r4 = r13.f     // Catch:{ all -> 0x03c9 }
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x03c9 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x03c9 }
                com.umeng.common.Log.c(r0, r1)     // Catch:{ all -> 0x03c9 }
                r0 = 30000(0x7530, double:1.4822E-319)
                java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0403 }
                int r0 = r13.h     // Catch:{ InterruptedException -> 0x0403 }
                r1 = 1
                if (r0 >= r1) goto L_0x0409
                r0 = 0
                r13.a(r0)     // Catch:{ InterruptedException -> 0x0403 }
                goto L_0x02a3
            L_0x0403:
                r0 = move-exception
                r13.a(r0)     // Catch:{ all -> 0x03c9 }
                goto L_0x02a3
            L_0x0409:
                r0 = 1
                r13.a(r0)     // Catch:{ InterruptedException -> 0x0403 }
                goto L_0x02a3
            L_0x040f:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0420 }
                if (r2 == 0) goto L_0x017d
                r2.close()     // Catch:{ IOException -> 0x041a }
                goto L_0x017d
            L_0x041a:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x0420:
                r0 = move-exception
                if (r2 == 0) goto L_0x0426
                r2.close()     // Catch:{ IOException -> 0x0427 }
            L_0x0426:
                throw r0
            L_0x0427:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0426
            L_0x042c:
                r0 = move-exception
                java.util.Map r3 = com.umeng.common.net.DownloadingService.l     // Catch:{ all -> 0x048b }
                com.umeng.common.net.a$a r4 = r13.i     // Catch:{ all -> 0x048b }
                r3.remove(r4)     // Catch:{ all -> 0x048b }
                r0.printStackTrace()     // Catch:{ all -> 0x048b }
                if (r2 == 0) goto L_0x043e
                r2.close()     // Catch:{ IOException -> 0x044b }
            L_0x043e:
                if (r1 == 0) goto L_0x017d
                r1.close()     // Catch:{ IOException -> 0x0445 }
                goto L_0x017d
            L_0x0445:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x044b:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x045c }
                if (r1 == 0) goto L_0x017d
                r1.close()     // Catch:{ IOException -> 0x0456 }
                goto L_0x017d
            L_0x0456:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x017d
            L_0x045c:
                r0 = move-exception
                if (r1 == 0) goto L_0x0462
                r1.close()     // Catch:{ IOException -> 0x0463 }
            L_0x0462:
                throw r0
            L_0x0463:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0462
            L_0x0468:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x03d6
            L_0x046e:
                r2 = move-exception
                r2.printStackTrace()     // Catch:{ all -> 0x047f }
                if (r1 == 0) goto L_0x03d6
                r1.close()     // Catch:{ IOException -> 0x0479 }
                goto L_0x03d6
            L_0x0479:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x03d6
            L_0x047f:
                r0 = move-exception
                if (r1 == 0) goto L_0x0485
                r1.close()     // Catch:{ IOException -> 0x0486 }
            L_0x0485:
                throw r0
            L_0x0486:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0485
            L_0x048b:
                r0 = move-exception
                goto L_0x03cc
            L_0x048e:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x026c
            L_0x0494:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x026c
            L_0x049a:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x026c
            L_0x04a0:
                r0 = r4
                goto L_0x011a
            L_0x04a3:
                r0 = r3
                goto L_0x0137
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.net.DownloadingService.a.a(boolean):void");
        }

        public void run() {
            this.f = 0;
            try {
                a(false);
                DownloadingService.l.remove(this.i);
                if (DownloadingService.l.size() <= 0) {
                    DownloadingService.this.stopSelf();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    class b extends Handler {
        b() {
        }

        public void handleMessage(Message message) {
            Log.c(DownloadingService.i, "IncomingHandler(msg.what:" + message.what + " msg.arg1:" + message.arg1 + " msg.arg2:" + message.arg2 + " msg.replyTo:" + message.replyTo);
            switch (message.what) {
                case 4:
                    Bundle data = message.getData();
                    Log.c(DownloadingService.i, "IncomingHandler(msg.getData():" + data);
                    a.C0001a a2 = a.C0001a.a(data);
                    if (DownloadingService.c(a2)) {
                        Log.a(DownloadingService.i, a2.b + " is already in downloading list. ");
                        return;
                    }
                    DownloadingService.l.put(a2, message.replyTo);
                    DownloadingService.this.b(a2);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(a.C0001a aVar) {
        Log.c(i, "startDownload([mComponentName:" + aVar.a + " mTitle:" + aVar.b + " mUrl:" + aVar.c + "])");
        new a(getApplicationContext(), aVar).start();
    }

    /* access modifiers changed from: private */
    public static final void b(String[] strArr) {
        int nextInt = new Random().nextInt(1000);
        if (strArr == null) {
            android.util.Log.i(i, nextInt + "service report: urls is null");
            return;
        }
        int length = strArr.length;
        int i2 = 0;
        while (i2 < length) {
            String str = strArr[i2];
            String a2 = g.a();
            String str2 = str + "&data=" + a2.split(" ")[0] + "&time=" + a2.split(" ")[1];
            try {
                android.util.Log.i(i, nextInt + ": service report:\tget: " + str2);
                HttpGet httpGet = new HttpGet(str2);
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
                HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(httpGet);
                android.util.Log.i(i, nextInt + ": service report:status code:  " + execute.getStatusLine().getStatusCode());
                if (execute.getStatusLine().getStatusCode() != 200) {
                    i2++;
                } else {
                    return;
                }
            } catch (ClientProtocolException e2) {
                android.util.Log.d(i, nextInt + ": service report:\tClientProtocolException,Failed to send message." + str2, e2);
            } catch (IOException e3) {
                android.util.Log.d(i, nextInt + ": service report:\tIOException,Failed to send message." + str2, e3);
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.importance == 100 && next.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static boolean c(a.C0001a aVar) {
        if (l == null) {
            return false;
        }
        for (a.C0001a aVar2 : l.keySet()) {
            if (aVar2.c.equals(aVar.c)) {
                return true;
            }
        }
        return false;
    }

    public IBinder onBind(Intent intent) {
        Log.c(i, "onBind ");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            this.k = extras.getStringArray("reporturls");
        }
        return this.h.getBinder();
    }

    public void onCreate() {
        super.onCreate();
        Log.c(i, "onCreate ");
        this.j = (NotificationManager) getSystemService("notification");
    }

    public void onDestroy() {
        try {
            c.a(getApplicationContext()).a(259200);
            c.a(getApplicationContext()).finalize();
        } catch (Exception e2) {
            Log.b(i, e2.getMessage());
        }
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        Log.c(i, "onStart ");
        super.onStart(intent, i2);
    }
}
