package com.umeng.common.net;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import com.umeng.common.net.a;

class b implements ServiceConnection {
    final /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d(a.b, "ServiceConnection.onServiceConnected");
        Messenger unused = this.a.e = new Messenger(iBinder);
        if (this.a.d != null) {
            this.a.d.a();
        }
        try {
            Message obtain = Message.obtain((Handler) null, 4);
            obtain.setData(new a.C0001a(this.a.f, this.a.g, this.a.h).a());
            obtain.replyTo = this.a.a;
            this.a.e.send(obtain);
        } catch (RemoteException e) {
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        Log.d(a.b, "ServiceConnection.onServiceDisconnected");
        Messenger unused = this.a.e = null;
    }
}
