package com.umeng.update;

import android.content.Context;
import android.content.DialogInterface;

final class d implements DialogInterface.OnClickListener {
    final /* synthetic */ Context a;
    final /* synthetic */ UpdateResponse b;

    d(Context context, UpdateResponse updateResponse) {
        this.a = context;
        this.b = updateResponse;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        UmengUpdateAgent.j.a(this.a, this.b.path);
    }
}
