package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import java.net.URLEncoder;
import org.json.JSONObject;

class f {
    f() {
    }

    static SharedPreferences a(Context context) {
        return context.getSharedPreferences("mobclick_agent_user_" + context.getPackageName(), 0);
    }

    static SharedPreferences b(Context context) {
        return context.getSharedPreferences("mobclick_agent_online_setting_" + context.getPackageName(), 0);
    }

    static SharedPreferences c(Context context) {
        return context.getSharedPreferences("mobclick_agent_header_" + context.getPackageName(), 0);
    }

    static SharedPreferences d(Context context) {
        return context.getSharedPreferences("mobclick_agent_update_" + context.getPackageName(), 0);
    }

    static SharedPreferences e(Context context) {
        return context.getSharedPreferences("mobclick_agent_state_" + context.getPackageName(), 0);
    }

    static String f(Context context) {
        return "mobclick_agent_header_" + context.getPackageName();
    }

    static String g(Context context) {
        return "mobclick_agent_cached_" + context.getPackageName();
    }

    static JSONObject h(Context context) {
        JSONObject jSONObject = new JSONObject();
        SharedPreferences a = a(context);
        try {
            if (a.getInt("gender", -1) != -1) {
                jSONObject.put("sex", a.getInt("gender", -1));
            }
            if (a.getInt("age", -1) != -1) {
                jSONObject.put("age", a.getInt("age", -1));
            }
            if (!"".equals(a.getString("user_id", ""))) {
                jSONObject.put(LocaleUtil.INDONESIAN, a.getString("user_id", ""));
            }
            if (!"".equals(a.getString("id_source", ""))) {
                jSONObject.put(Constants.PARAM_URL, URLEncoder.encode(a.getString("id_source", "")));
            }
            if (jSONObject.length() > 0) {
                return jSONObject;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
