package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.Vector;

class g {
    private static final int b = 4;
    private Vector<Long> a;
    private String c;
    private Context d;

    public g(Context context, String str) {
        this.a = new Vector<>(4);
        this.d = context;
        this.c = str;
    }

    public g(Context context, String str, int i) {
        this.c = str;
        this.d = context;
        if (i < 0) {
            this.a = new Vector<>(4);
        } else {
            this.a = new Vector<>(i);
        }
    }

    public static g a(Context context, String str) {
        String string = f.e(context).getString(str, null);
        g gVar = new g(context, str);
        if (!TextUtils.isEmpty(string)) {
            for (String trim : string.split(",")) {
                String trim2 = trim.trim();
                if (!TextUtils.isEmpty(trim2)) {
                    Long.valueOf(-1);
                    try {
                        gVar.a(Long.valueOf(Long.parseLong(trim2)));
                    } catch (Exception e) {
                    }
                }
            }
        }
        return gVar;
    }

    public Long a() {
        int size = this.a.size();
        if (size <= 0) {
            return -1L;
        }
        return this.a.remove(size - 1);
    }

    public void a(Long l) {
        while (this.a.size() >= 4) {
            this.a.remove(0);
        }
        this.a.add(l);
    }

    public int b() {
        return this.a.size();
    }

    public void c() {
        String gVar = toString();
        SharedPreferences.Editor edit = f.e(this.d).edit();
        if (TextUtils.isEmpty(gVar)) {
            edit.remove(this.c).commit();
        } else {
            edit.putString(this.c, gVar).commit();
        }
    }

    public String toString() {
        int size = this.a.size();
        if (size <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(4);
        for (int i = 0; i < size; i++) {
            stringBuffer.append(this.a.get(i));
            if (i != size - 1) {
                stringBuffer.append(",");
            }
        }
        this.a.clear();
        return stringBuffer.toString();
    }
}
