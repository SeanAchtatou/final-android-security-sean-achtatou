package com.umeng.analytics;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.common.Log;
import com.umeng.common.b;
import com.umeng.common.b.g;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import org.json.JSONArray;
import org.json.JSONObject;

class a implements Thread.UncaughtExceptionHandler {
    private static final String c = "com_umeng__crash.cache";
    private Thread.UncaughtExceptionHandler a;
    private Context b;

    private void a(Throwable th) {
        if (th == null) {
            Log.e("MobclickAgent", "Exception is null in handleException");
        } else {
            a(this.b, th);
        }
    }

    public void a(Context context) {
        if (Thread.getDefaultUncaughtExceptionHandler() == this) {
            Log.a("MobclickAgent", "can't call onError more than once!");
            return;
        }
        this.b = context;
        this.a = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        if (context != null && !TextUtils.isEmpty(str)) {
            try {
                String a2 = g.a();
                String str2 = a2.split(" ")[0];
                String str3 = a2.split(" ")[1];
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("date", str2);
                jSONObject.put("time", str3);
                jSONObject.put("context", str);
                jSONObject.put("type", "error");
                jSONObject.put("version", b.d(context));
                JSONArray b2 = b(context);
                if (b2 == null) {
                    b2 = new JSONArray();
                }
                b2.put(jSONObject);
                FileOutputStream openFileOutput = context.openFileOutput(c, 0);
                openFileOutput.write(b2.toString().getBytes());
                openFileOutput.flush();
                openFileOutput.close();
            } catch (Exception e) {
                Log.b("MobclickAgent", "an error occured while writing report file...", e);
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Throwable th) {
        if (context != null && th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(printWriter);
            }
            String obj = stringWriter.toString();
            printWriter.close();
            a(context, obj);
        }
    }

    /* access modifiers changed from: package-private */
    public JSONArray b(Context context) {
        JSONArray jSONArray = null;
        if (context != null) {
            File file = new File(context.getFilesDir(), c);
            try {
                if (file.exists()) {
                    FileInputStream openFileInput = context.openFileInput(c);
                    StringBuffer stringBuffer = new StringBuffer();
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = openFileInput.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        stringBuffer.append(new String(bArr, 0, read));
                    }
                    openFileInput.close();
                    jSONArray = stringBuffer.length() != 0 ? new JSONArray(stringBuffer.toString()) : null;
                    try {
                        file.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return jSONArray;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        a(th);
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
