package com.xiaomi.channel.commonutils.logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class b {
    private static int a = 2;
    private static LoggerInterface b = new a();
    private static HashMap<Integer, Long> c = new HashMap<>();
    private static HashMap<Integer, String> d = new HashMap<>();
    private static final Integer e = -1;
    private static AtomicInteger f = new AtomicInteger(1);

    public static void a(int i) {
        if (i < 0 || i > 5) {
            a(2, "set log level as " + i);
        }
        a = i;
    }

    public static void a(int i, String str) {
        if (i >= a) {
            b.log(str);
        }
    }

    public static void a(int i, String str, Throwable th) {
        if (i >= a) {
            b.log(str, th);
        }
    }

    public static void a(int i, Throwable th) {
        if (i >= a) {
            b.log("", th);
        }
    }

    public static void a(LoggerInterface loggerInterface) {
        b = loggerInterface;
    }

    public static void a(String str) {
        a(2, str);
    }

    public static void a(String str, Throwable th) {
        a(4, str, th);
    }

    public static void a(Throwable th) {
        a(4, th);
    }

    public static void b(String str) {
        a(1, str);
    }

    public static void c(String str) {
        a(4, str);
    }

    public static void d(String str) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        printWriter.println(str);
        printWriter.println(String.format("Current thread id (%s); thread name (%s)", Long.valueOf(Thread.currentThread().getId()), Thread.currentThread().getName()));
        new Throwable("Call stack").printStackTrace(printWriter);
        b(stringWriter.toString());
    }
}
