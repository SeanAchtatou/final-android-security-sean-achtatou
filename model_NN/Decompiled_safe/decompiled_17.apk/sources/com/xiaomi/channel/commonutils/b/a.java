package com.xiaomi.channel.commonutils.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.tauth.Constants;
import java.io.BufferedReader;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.http.NameValuePair;

public class a {
    public static final Pattern a = Pattern.compile("([^\\s;]+)(.*)");
    public static final Pattern b = Pattern.compile("(.*?charset\\s*=[^a-zA-Z0-9]*)([-a-zA-Z0-9]+)(.*)", 2);
    public static final Pattern c = Pattern.compile("(\\<\\?xml\\s+.*?encoding\\s*=[^a-zA-Z0-9]*)([-a-zA-Z0-9]+)(.*)", 2);

    /* renamed from: com.xiaomi.channel.commonutils.b.a$a  reason: collision with other inner class name */
    public static final class C0004a extends FilterInputStream {
        private boolean a;

        public C0004a(InputStream inputStream) {
            super(inputStream);
        }

        public int read(byte[] bArr, int i, int i2) {
            int read;
            if (!this.a && (read = super.read(bArr, i, i2)) != -1) {
                return read;
            }
            this.a = true;
            return -1;
        }
    }

    public static class b {
        public int a;
        public Map<String, String> b;

        public String toString() {
            return String.format("resCode = %1$d, headers = %2$s", Integer.valueOf(this.a), this.b.toString());
        }
    }

    public static int a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return -1;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return -1;
        }
        return activeNetworkInfo.getType();
    }

    public static String a(Context context, String str, List<NameValuePair> list) {
        return a(context, str, list, null, null, null);
    }

    public static String a(Context context, String str, List<NameValuePair> list, b bVar, String str2, String str3) {
        int i = 0;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException(Constants.PARAM_URL);
        }
        HttpURLConnection a2 = a(context, new URL(str));
        a2.setConnectTimeout(10000);
        a2.setReadTimeout(15000);
        a2.setRequestMethod("POST");
        if (!TextUtils.isEmpty(str2)) {
            a2.setRequestProperty("User-Agent", str2);
        }
        if (str3 != null) {
            a2.setRequestProperty("Cookie", str3);
        }
        String a3 = a(list);
        if (a3 == null) {
            throw new IllegalArgumentException("nameValuePairs");
        }
        a2.setDoOutput(true);
        byte[] bytes = a3.getBytes();
        a2.getOutputStream().write(bytes, 0, bytes.length);
        a2.getOutputStream().flush();
        a2.getOutputStream().close();
        int responseCode = a2.getResponseCode();
        Log.d("com.xiaomi.common.Network", "Http POST Response Code: " + responseCode);
        if (bVar != null) {
            bVar.a = responseCode;
            if (bVar.b == null) {
                bVar.b = new HashMap();
            }
            while (true) {
                String headerFieldKey = a2.getHeaderFieldKey(i);
                String headerField = a2.getHeaderField(i);
                if (headerFieldKey == null && headerField == null) {
                    break;
                }
                bVar.b.put(headerFieldKey, headerField);
                i = i + 1 + 1;
            }
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new C0004a(a2.getInputStream())));
        StringBuffer stringBuffer = new StringBuffer();
        String property = System.getProperty("line.separator");
        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
            stringBuffer.append(readLine);
            stringBuffer.append(property);
        }
        String stringBuffer2 = stringBuffer.toString();
        bufferedReader.close();
        return stringBuffer2;
    }

    public static String a(URL url) {
        StringBuilder sb = new StringBuilder();
        sb.append(url.getProtocol()).append("://").append("10.0.0.172").append(url.getPath());
        if (!TextUtils.isEmpty(url.getQuery())) {
            sb.append("?").append(url.getQuery());
        }
        return sb.toString();
    }

    public static String a(List<NameValuePair> list) {
        StringBuffer stringBuffer = new StringBuffer();
        for (NameValuePair next : list) {
            try {
                if (next.getValue() != null) {
                    stringBuffer.append(URLEncoder.encode(next.getName(), "UTF-8"));
                    stringBuffer.append("=");
                    stringBuffer.append(URLEncoder.encode(next.getValue(), "UTF-8"));
                    stringBuffer.append("&");
                }
            } catch (UnsupportedEncodingException e) {
                Log.d("com.xiaomi.common.Network", "Failed to convert from param list to string: " + e.toString());
                Log.d("com.xiaomi.common.Network", "pair: " + next.toString());
                return null;
            }
        }
        return (stringBuffer.length() > 0 ? stringBuffer.deleteCharAt(stringBuffer.length() - 1) : stringBuffer).toString();
    }

    public static HttpURLConnection a(Context context, URL url) {
        if (c(context)) {
            return (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80)));
        }
        if (!b(context)) {
            return (HttpURLConnection) url.openConnection();
        }
        String host = url.getHost();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(a(url)).openConnection();
        httpURLConnection.addRequestProperty("X-Online-Host", host);
        return httpURLConnection;
    }

    public static boolean b(Context context) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (!"CN".equalsIgnoreCase(((TelephonyManager) context.getSystemService("phone")).getSimCountryIso()) || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) {
            return false;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        if (TextUtils.isEmpty(extraInfo) || extraInfo.length() < 3 || extraInfo.contains("ctwap")) {
            return false;
        }
        return extraInfo.regionMatches(true, extraInfo.length() - 3, "wap", 0, 3);
    }

    public static boolean c(Context context) {
        if (!"CN".equalsIgnoreCase(((TelephonyManager) context.getSystemService("phone")).getSimCountryIso())) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        if (TextUtils.isEmpty(extraInfo) || extraInfo.length() < 3) {
            return false;
        }
        return extraInfo.contains("ctwap");
    }

    public static boolean d(Context context) {
        return a(context) >= 0;
    }

    public static boolean e(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) {
            return false;
        }
        return 1 == activeNetworkInfo.getType();
    }

    public static String f(Context context) {
        NetworkInfo activeNetworkInfo;
        if (e(context)) {
            return "wifi";
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) ? "" : activeNetworkInfo.getExtraInfo();
    }
}
