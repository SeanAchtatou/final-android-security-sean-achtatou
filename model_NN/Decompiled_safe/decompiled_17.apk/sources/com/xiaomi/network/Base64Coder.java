package com.xiaomi.network;

public class Base64Coder {
    private static final String a = System.getProperty("line.separator");
    private static char[] b = new char[64];
    private static byte[] c = new byte[128];

    static {
        int i = 0;
        char c2 = 'A';
        while (c2 <= 'Z') {
            b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        int i2 = i + 1;
        b[i] = '+';
        int i3 = i2 + 1;
        b[i2] = '/';
        for (int i4 = 0; i4 < c.length; i4++) {
            c[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            c[b[i5]] = (byte) i5;
        }
    }

    private Base64Coder() {
    }

    public static char[] a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static char[] a(byte[] bArr, int i, int i2) {
        byte b2;
        int i3;
        byte b3;
        int i4 = ((i2 * 4) + 2) / 3;
        char[] cArr = new char[(((i2 + 2) / 3) * 4)];
        int i5 = i + i2;
        int i6 = 0;
        while (i < i5) {
            int i7 = i + 1;
            byte b4 = bArr[i] & 255;
            if (i7 < i5) {
                b2 = bArr[i7] & 255;
                i7++;
            } else {
                b2 = 0;
            }
            if (i7 < i5) {
                i3 = i7 + 1;
                b3 = bArr[i7] & 255;
            } else {
                i3 = i7;
                b3 = 0;
            }
            int i8 = b4 >>> 2;
            int i9 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i10 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i11 = i6 + 1;
            cArr[i6] = b[i8];
            int i12 = i11 + 1;
            cArr[i11] = b[i9];
            cArr[i12] = i12 < i4 ? b[i10] : '=';
            int i13 = i12 + 1;
            cArr[i13] = i13 < i4 ? b[b5] : '=';
            i6 = i13 + 1;
            i = i3;
        }
        return cArr;
    }
}
