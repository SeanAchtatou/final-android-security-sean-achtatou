package com.xiaomi.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.baixing.util.TraceUtil;
import com.tencent.tauth.Constants;
import java.io.BufferedReader;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.http.NameValuePair;

public class Network {
    public static final Pattern a = Pattern.compile("([^\\s;]+)(.*)");
    public static final Pattern b = Pattern.compile("(.*?charset\\s*=[^a-zA-Z0-9]*)([-a-zA-Z0-9]+)(.*)", 2);
    public static final Pattern c = Pattern.compile("(\\<\\?xml\\s+.*?encoding\\s*=[^a-zA-Z0-9]*)([-a-zA-Z0-9]+)(.*)", 2);

    public static final class DoneHandlerInputStream extends FilterInputStream {
        private boolean a;

        public DoneHandlerInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public int read(byte[] bArr, int i, int i2) {
            int read;
            if (!this.a && (read = super.read(bArr, i, i2)) != -1) {
                return read;
            }
            this.a = true;
            return -1;
        }
    }

    public static class HttpHeaderInfo {
        public int a;
        public Map<String, String> b;
    }

    public interface PostDownloadHandler {
    }

    public static InputStream a(Context context, URL url, String str, String str2) {
        return a(context, url, str, str2, (Map<String, String>) null, (HttpHeaderInfo) null);
    }

    public static InputStream a(Context context, URL url, String str, String str2, Map<String, String> map, HttpHeaderInfo httpHeaderInfo) {
        if (context == null) {
            throw new IllegalArgumentException("context");
        } else if (url == null) {
            throw new IllegalArgumentException(Constants.PARAM_URL);
        } else {
            HttpURLConnection.setFollowRedirects(true);
            HttpURLConnection b2 = b(context, url);
            b2.setConnectTimeout(10000);
            b2.setReadTimeout(15000);
            if (!TextUtils.isEmpty(str)) {
                b2.setRequestProperty("User-Agent", str);
            }
            if (str2 != null) {
                b2.setRequestProperty("Cookie", str2);
            }
            if (map != null) {
                for (String next : map.keySet()) {
                    b2.setRequestProperty(next, map.get(next));
                }
            }
            if (httpHeaderInfo != null && (url.getProtocol().equals("http") || url.getProtocol().equals("https"))) {
                httpHeaderInfo.a = b2.getResponseCode();
                if (httpHeaderInfo.b == null) {
                    httpHeaderInfo.b = new HashMap();
                }
                int i = 0;
                while (true) {
                    String headerFieldKey = b2.getHeaderFieldKey(i);
                    String headerField = b2.getHeaderField(i);
                    if (headerFieldKey == null && headerField == null) {
                        break;
                    }
                    if (!TextUtils.isEmpty(headerFieldKey) && !TextUtils.isEmpty(headerField)) {
                        httpHeaderInfo.b.put(headerFieldKey.toLowerCase(), headerField);
                    }
                    i++;
                }
            }
            return new DoneHandlerInputStream(b2.getInputStream());
        }
    }

    public static String a(Context context, String str, List<NameValuePair> list) {
        return a(context, str, list, (Map<String, String>) null, (String) null, (String) null);
    }

    public static String a(Context context, String str, List<NameValuePair> list, Map<String, String> map, String str2, String str3) {
        int i = 0;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException(Constants.PARAM_URL);
        }
        HttpURLConnection b2 = b(context, new URL(str));
        b2.setConnectTimeout(10000);
        b2.setReadTimeout(15000);
        b2.setRequestMethod("POST");
        if (!TextUtils.isEmpty(str2)) {
            b2.setRequestProperty("User-Agent", str2);
        }
        if (str3 != null) {
            b2.setRequestProperty("Cookie", str3);
        }
        String a2 = a(list);
        if (a2 == null) {
            throw new IllegalArgumentException("nameValuePairs");
        }
        b2.setDoOutput(true);
        byte[] bytes = a2.getBytes();
        b2.getOutputStream().write(bytes, 0, bytes.length);
        b2.getOutputStream().flush();
        b2.getOutputStream().close();
        Log.d("com.xiaomi.common.Network", "Http POST Response Code: " + b2.getResponseCode());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new DoneHandlerInputStream(b2.getInputStream())));
        StringBuffer stringBuffer = new StringBuffer();
        String property = System.getProperty("line.separator");
        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
            stringBuffer.append(readLine);
            stringBuffer.append(property);
        }
        String stringBuffer2 = stringBuffer.toString();
        bufferedReader.close();
        if (map != null) {
            while (true) {
                String headerFieldKey = b2.getHeaderFieldKey(i);
                String headerField = b2.getHeaderField(i);
                if (headerFieldKey == null && headerField == null) {
                    break;
                }
                map.put(headerFieldKey, headerField);
                i = i + 1 + 1;
            }
        }
        return stringBuffer2;
    }

    public static String a(Context context, URL url) {
        return a(context, url, null, "UTF-8", null);
    }

    public static String a(Context context, URL url, String str, String str2, String str3) {
        InputStream inputStream = null;
        try {
            inputStream = a(context, url, str, str3);
            StringBuilder sb = new StringBuilder(1024);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, str2), 1024);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append(TraceUtil.LINE);
            }
            if (inputStream != null) {
                try {
                } catch (IOException e) {
                    Log.e("com.xiaomi.common.Network", "Failed to close responseStream" + e.toString());
                }
            }
            return sb.toString();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    Log.e("com.xiaomi.common.Network", "Failed to close responseStream" + e2.toString());
                }
            }
        }
    }

    public static String a(URL url) {
        StringBuilder sb = new StringBuilder();
        sb.append(url.getProtocol()).append("://").append("10.0.0.172").append(url.getPath());
        if (!TextUtils.isEmpty(url.getQuery())) {
            sb.append("?").append(url.getQuery());
        }
        return sb.toString();
    }

    public static String a(List<NameValuePair> list) {
        StringBuffer stringBuffer = new StringBuffer();
        for (NameValuePair next : list) {
            try {
                if (next.getValue() != null) {
                    stringBuffer.append(URLEncoder.encode(next.getName(), "UTF-8"));
                    stringBuffer.append("=");
                    stringBuffer.append(URLEncoder.encode(next.getValue(), "UTF-8"));
                    stringBuffer.append("&");
                }
            } catch (UnsupportedEncodingException e) {
                Log.d("com.xiaomi.common.Network", "Failed to convert from param list to string: " + e.toString());
                Log.d("com.xiaomi.common.Network", "pair: " + next.toString());
                return null;
            }
        }
        return (stringBuffer.length() > 0 ? stringBuffer.deleteCharAt(stringBuffer.length() - 1) : stringBuffer).toString();
    }

    public static boolean a(Context context) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (!"CN".equalsIgnoreCase(((TelephonyManager) context.getSystemService("phone")).getSimCountryIso()) || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) {
            return false;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        if (TextUtils.isEmpty(extraInfo) || extraInfo.length() < 3 || extraInfo.contains("ctwap")) {
            return false;
        }
        return extraInfo.regionMatches(true, extraInfo.length() - 3, "wap", 0, 3);
    }

    public static HttpURLConnection b(Context context, URL url) {
        if (b(context)) {
            return (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80)));
        }
        if (!a(context)) {
            return (HttpURLConnection) url.openConnection();
        }
        String host = url.getHost();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(a(url)).openConnection();
        httpURLConnection.addRequestProperty("X-Online-Host", host);
        return httpURLConnection;
    }

    public static boolean b(Context context) {
        if (!"CN".equalsIgnoreCase(((TelephonyManager) context.getSystemService("phone")).getSimCountryIso())) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        if (TextUtils.isEmpty(extraInfo) || extraInfo.length() < 3) {
            return false;
        }
        return extraInfo.contains("ctwap");
    }
}
