package com.xiaomi.network;

import android.text.TextUtils;
import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.Util;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class Fallback {
    protected String a;
    protected String b;
    protected String c;
    protected String d;
    protected String e;
    protected String f;
    protected String g;
    protected String h;
    private long i;
    private ArrayList<WeightedHost> j = new ArrayList<>();
    private String k;
    private double l = 0.1d;
    private String m = "s.mi1.cc";
    private long n = Util.MILLSECONDS_OF_DAY;

    public Fallback(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("the host is empty");
        }
        this.i = System.currentTimeMillis();
        this.j.add(new WeightedHost(str, -1));
        this.a = HostManager.getInstance().getActiveNetworkLabel();
        this.b = str;
    }

    public Fallback a(JSONObject jSONObject) {
        synchronized (this) {
            this.a = jSONObject.optString("net");
            this.n = jSONObject.getLong("ttl");
            this.l = jSONObject.getDouble("pct");
            this.i = jSONObject.getLong("ts");
            this.d = jSONObject.optString(ApiParams.KEY_CITY);
            this.c = jSONObject.optString("prv");
            this.g = jSONObject.optString("cty");
            this.e = jSONObject.optString("isp");
            this.f = jSONObject.optString("ip");
            this.b = jSONObject.optString("host");
            this.h = jSONObject.optString("xf");
            JSONArray jSONArray = jSONObject.getJSONArray("fbs");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                this.j.add(new WeightedHost().a(jSONArray.getJSONObject(i2)));
            }
        }
        return this;
    }

    public ArrayList<String> a(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("the url is empty.");
        }
        URL url = new URL(str);
        if (TextUtils.equals(url.getHost(), this.b)) {
            ArrayList<String> arrayList = new ArrayList<>();
            Iterator<String> it = c().iterator();
            while (it.hasNext()) {
                arrayList.add(new URL(url.getProtocol(), it.next(), url.getPort(), url.getFile()).toString());
            }
            return arrayList;
        }
        throw new IllegalArgumentException("the url is not supported by the fallback");
    }

    public void a(double d2) {
        this.l = d2;
    }

    public void a(long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("the duration is invalid " + j2);
        }
        this.n = j2;
    }

    public void a(WeightedHost weightedHost) {
        synchronized (this) {
            this.j.add(weightedHost);
        }
    }

    public void a(String str, int i2, long j2, long j3, Exception exc) {
        a(str, new AccessHistory(i2, j2, j3, exc));
    }

    public void a(String str, long j2, long j3) {
        a(str, 1, j2, j3, null);
    }

    public void a(String str, long j2, long j3, Exception exc) {
        a(str, -1, j2, j3, exc);
    }

    public void a(String str, AccessHistory accessHistory) {
        synchronized (this) {
            Iterator<WeightedHost> it = this.j.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                WeightedHost next = it.next();
                if (TextUtils.equals(str, next.a)) {
                    next.a(accessHistory);
                    break;
                }
            }
        }
    }

    public void a(String[] strArr) {
        synchronized (this) {
            for (int size = this.j.size() - 1; size >= 0; size--) {
                int length = strArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    if (TextUtils.equals(this.j.get(size).a, strArr[i2])) {
                        this.j.remove(size);
                        break;
                    }
                    i2++;
                }
            }
            Iterator<WeightedHost> it = this.j.iterator();
            int i3 = 0;
            while (it.hasNext()) {
                WeightedHost next = it.next();
                i3 = next.b > i3 ? next.b : i3;
            }
            for (int i4 = 0; i4 < strArr.length; i4++) {
                a(new WeightedHost(strArr[i4], (strArr.length + i3) - i4));
            }
        }
    }

    public boolean a() {
        return TextUtils.equals(this.a, HostManager.getInstance().getActiveNetworkLabel());
    }

    public boolean a(Fallback fallback) {
        return TextUtils.equals(this.a, fallback.a);
    }

    public void b(String str) {
        synchronized (this) {
            this.j.add(new WeightedHost(str));
        }
    }

    public boolean b() {
        return System.currentTimeMillis() - this.i < this.n;
    }

    public ArrayList<String> c() {
        ArrayList<String> arrayList;
        synchronized (this) {
            WeightedHost[] weightedHostArr = new WeightedHost[this.j.size()];
            this.j.toArray(weightedHostArr);
            Arrays.sort(weightedHostArr);
            arrayList = new ArrayList<>();
            for (WeightedHost weightedHost : weightedHostArr) {
                arrayList.add(weightedHost.a);
            }
        }
        return arrayList;
    }

    public void c(String str) {
        this.m = str;
    }

    public String d() {
        String str;
        synchronized (this) {
            if (!TextUtils.isEmpty(this.k)) {
                str = this.k;
            } else if (TextUtils.isEmpty(this.e)) {
                str = "hardcode_isp";
            } else {
                this.k = HostManager.join(new String[]{this.e, this.c, this.d, this.g, this.f}, "_");
                str = this.k;
            }
        }
        return str;
    }

    public ArrayList<WeightedHost> e() {
        return this.j;
    }

    public double f() {
        if (this.l < 1.0E-5d) {
            return 0.1d;
        }
        return this.l;
    }

    public JSONObject g() {
        JSONObject jSONObject;
        synchronized (this) {
            jSONObject = new JSONObject();
            jSONObject.put("net", this.a);
            jSONObject.put("ttl", this.n);
            jSONObject.put("pct", this.l);
            jSONObject.put("ts", this.i);
            jSONObject.put(ApiParams.KEY_CITY, this.d);
            jSONObject.put("prv", this.c);
            jSONObject.put("cty", this.g);
            jSONObject.put("isp", this.e);
            jSONObject.put("ip", this.f);
            jSONObject.put("host", this.b);
            jSONObject.put("xf", this.h);
            JSONArray jSONArray = new JSONArray();
            Iterator<WeightedHost> it = this.j.iterator();
            while (it.hasNext()) {
                jSONArray.put(it.next().b());
            }
            jSONObject.put("fbs", jSONArray);
        }
        return jSONObject;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append("\n");
        sb.append(d());
        Iterator<WeightedHost> it = this.j.iterator();
        while (it.hasNext()) {
            sb.append("\n");
            sb.append(it.next().toString());
        }
        sb.append("\n");
        return sb.toString();
    }
}
