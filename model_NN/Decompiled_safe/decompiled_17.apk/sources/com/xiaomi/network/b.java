package com.xiaomi.network;

import android.util.Log;
import java.util.TimerTask;
import org.apache.thrift.TException;

class b extends TimerTask {
    final /* synthetic */ HostManager a;

    b(HostManager hostManager) {
        this.a = hostManager;
    }

    public void run() {
        try {
            this.a.uploadHostStat();
        } catch (TException e) {
            Log.e("HostManager", "send stat data failed.", e);
        }
        boolean unused = this.a.mTaskPending = false;
    }
}
