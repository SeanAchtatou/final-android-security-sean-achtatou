package com.xiaomi.network;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.Util;
import com.xiaomi.a.a.a.a;
import com.xiaomi.a.a.a.a.b;
import com.xiaomi.a.a.a.a.c;
import com.xiaomi.a.a.a.a.d;
import com.xiaomi.a.a.a.a.e;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TCompactProtocol;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HostManager {
    private static HostManagerFactory factory;
    private static boolean hostLoaded = false;
    private static Map<String, ArrayList<String>> mReservedHosts = new HashMap();
    private static HostManager sInstance;
    private final int HOST_STAT_DELAY = 60000;
    private final long MAX_REQUEST_FAILURE_CNT = 15;
    private String currentISP = "isp_prov_city_country_ip";
    private long lastRemoteRequestTimestamp = 0;
    protected Map<String, Fallbacks> mHostsMapping = new HashMap();
    private Random mRandomGenerator = new Random();
    /* access modifiers changed from: private */
    public boolean mTaskPending = false;
    private Timer mTimer = new Timer("HostManager Upload Timer");
    private long remoteRequestFailureCount = 0;
    private Context sAppContext;
    private HostFilter sHostFilter;
    private HttpGet sHttpGetter;
    private String sUserId = "0";

    public interface HostManagerFactory {
        HostManager a(Context context, HostFilter hostFilter, HttpGet httpGet, String str);
    }

    public interface HttpGet {
        String a(String str);
    }

    protected HostManager(Context context, HostFilter hostFilter, HttpGet httpGet, String str) {
        this.sAppContext = context.getApplicationContext();
        this.sHttpGetter = httpGet;
        if (hostFilter == null) {
            this.sHostFilter = new a(this);
        } else {
            this.sHostFilter = hostFilter;
        }
        this.sUserId = str;
        this.mRandomGenerator = new Random();
    }

    private void fromJSON(String str) {
        synchronized (this.mHostsMapping) {
            this.mHostsMapping.clear();
            JSONArray jSONArray = new JSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                Fallbacks fromJSON = new Fallbacks().fromJSON(jSONArray.getJSONObject(i));
                this.mHostsMapping.put(fromJSON.getHost(), fromJSON);
            }
        }
    }

    public static byte[] getBytes(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return str.getBytes();
        }
    }

    public static HostManager getInstance() {
        HostManager hostManager;
        synchronized (HostManager.class) {
            try {
                if (sInstance == null) {
                    throw new IllegalStateException("the host manager is not initialized yet.");
                }
                hostManager = sInstance;
            } catch (Throwable th) {
                Class<HostManager> cls = HostManager.class;
                throw th;
            }
        }
        return hostManager;
    }

    private Fallback getLocalFallback(String str) {
        Fallbacks fallbacks;
        Fallback fallback;
        synchronized (this.mHostsMapping) {
            checkHostMapping();
            fallbacks = this.mHostsMapping.get(str);
        }
        if (fallbacks == null || (fallback = fallbacks.getFallback()) == null) {
            return null;
        }
        return fallback;
    }

    public static String getMd5Digest(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(getBytes(str));
            return String.format("%1$032X", new BigInteger(1, instance.digest()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String getPackageName() {
        try {
            PackageInfo packageInfo = this.sAppContext.getPackageManager().getPackageInfo(this.sAppContext.getPackageName(), 16384);
            if (packageInfo != null) {
                return packageInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return "0";
    }

    private String getRemoteFallbackJSON(ArrayList<String> arrayList, String str) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        ArrayList<NameValuePair> arrayList3 = new ArrayList<>();
        arrayList3.add(new BasicNameValuePair("type", str));
        arrayList3.add(new BasicNameValuePair("uuid", this.sUserId));
        arrayList3.add(new BasicNameValuePair("list", join(arrayList, ",")));
        Fallback localFallback = getLocalFallback("resolver.gslb.mi-idc.com");
        String format = String.format("http://%1$s/gslb/gslb/getbucket.asp?ver=3.0", "resolver.gslb.mi-idc.com");
        if (localFallback == null) {
            arrayList2.add(format);
        } else {
            arrayList2 = localFallback.a(format);
        }
        Iterator<String> it = arrayList2.iterator();
        if (!it.hasNext()) {
            return null;
        }
        Uri.Builder buildUpon = Uri.parse(it.next()).buildUpon();
        for (NameValuePair nameValuePair : arrayList3) {
            buildUpon.appendQueryParameter(nameValuePair.getName(), nameValuePair.getValue());
        }
        return this.sHttpGetter == null ? Network.a(this.sAppContext, new URL(buildUpon.toString())) : this.sHttpGetter.a(buildUpon.toString());
    }

    public static void init(Context context, HostFilter hostFilter, HttpGet httpGet, String str) {
        synchronized (HostManager.class) {
            try {
                if (sInstance == null) {
                    if (factory == null) {
                        sInstance = new HostManager(context, hostFilter, httpGet, str);
                    } else {
                        sInstance = factory.a(context, hostFilter, httpGet, str);
                    }
                }
            } catch (Throwable th) {
                Class<HostManager> cls = HostManager.class;
                throw th;
            }
        }
    }

    public static <T> String join(Collection collection, String str) {
        if (collection == null || collection.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public static String join(String[] strArr, String str) {
        if (strArr == null || strArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(strArr[0]);
        for (int i = 1; i < strArr.length; i++) {
            sb.append(str);
            sb.append(strArr[i]);
        }
        return sb.toString();
    }

    private String processNetwork(String str) {
        return TextUtils.isEmpty(str) ? "unknown" : str.startsWith("WIFI") ? "WIFI" : str;
    }

    private Fallback requestRemoteFallback(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(str);
        return requestRemoteFallbacks(arrayList).get(0);
    }

    private ArrayList<Fallback> requestRemoteFallbacks(ArrayList<String> arrayList) {
        purge();
        synchronized (this.mHostsMapping) {
            checkHostMapping();
            for (String next : this.mHostsMapping.keySet()) {
                if (!arrayList.contains(next)) {
                    arrayList.add(next);
                }
            }
        }
        synchronized (mReservedHosts) {
            for (String next2 : mReservedHosts.keySet()) {
                if (!arrayList.contains(next2)) {
                    arrayList.add(next2);
                }
            }
        }
        if (!arrayList.contains("resolver.gslb.mi-idc.com")) {
            arrayList.add("resolver.gslb.mi-idc.com");
        }
        ArrayList<Fallback> arrayList2 = new ArrayList<>(arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList2.add(null);
        }
        try {
            String str = isWIFIConnected() ? "wifi" : "wap";
            String remoteFallbackJSON = getRemoteFallbackJSON(arrayList, str);
            if (!TextUtils.isEmpty(remoteFallbackJSON)) {
                JSONObject jSONObject = new JSONObject(remoteFallbackJSON);
                if ("OK".equalsIgnoreCase(jSONObject.getString("S"))) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("R");
                    String string = jSONObject2.getString("province");
                    String string2 = jSONObject2.getString(ApiParams.KEY_CITY);
                    String string3 = jSONObject2.getString("isp");
                    String string4 = jSONObject2.getString("ip");
                    String string5 = jSONObject2.getString("country");
                    JSONObject jSONObject3 = jSONObject2.getJSONObject(str);
                    int i2 = 0;
                    while (true) {
                        int i3 = i2;
                        if (i3 >= arrayList.size()) {
                            break;
                        }
                        String str2 = arrayList.get(i3);
                        JSONArray jSONArray = jSONObject3.getJSONArray(str2);
                        Fallback fallback = new Fallback(str2);
                        for (int i4 = 0; i4 < jSONArray.length(); i4++) {
                            String string6 = jSONArray.getString(i4);
                            if (!TextUtils.isEmpty(string6)) {
                                fallback.a(new WeightedHost(string6, jSONArray.length() - i4));
                            }
                        }
                        arrayList2.set(i3, fallback);
                        fallback.g = string5;
                        fallback.c = string;
                        fallback.e = string3;
                        fallback.f = string4;
                        fallback.d = string2;
                        if (jSONObject2.has("stat-percent")) {
                            fallback.a(jSONObject2.getDouble("stat-percent"));
                        }
                        if (jSONObject2.has("stat-domain")) {
                            fallback.c(jSONObject2.getString("stat-domain"));
                        }
                        if (jSONObject2.has("ttl")) {
                            fallback.a(1000 * ((long) jSONObject2.getInt("ttl")));
                        }
                        setCurrentISP(fallback.d());
                        i2 = i3 + 1;
                    }
                }
            }
        } catch (JSONException e) {
            Log.e("com.xiaomi.network", "failed to get bucket", e);
        } catch (IOException e2) {
            Log.e("com.xiaomi.network", "failed to get bucket", e2);
        }
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 < arrayList.size()) {
                Fallback fallback2 = arrayList2.get(i6);
                if (fallback2 != null) {
                    updateFallbacks(arrayList.get(i6), fallback2);
                }
                i5 = i6 + 1;
            } else {
                persist();
                return arrayList2;
            }
        }
    }

    private JSONArray toJSON() {
        JSONArray jSONArray;
        synchronized (this.mHostsMapping) {
            jSONArray = new JSONArray();
            for (Fallbacks json : this.mHostsMapping.values()) {
                jSONArray.put(json.toJSON());
            }
        }
        return jSONArray;
    }

    /* access modifiers changed from: private */
    public void uploadHostStat() {
        try {
            for (b a : generateHostStats()) {
                c cVar = new c();
                cVar.a("httpapi");
                cVar.a(a);
                cVar.a(new a());
                String str = new String(Base64Coder.a(new TSerializer(new TCompactProtocol.Factory()).a(cVar)));
                ArrayList arrayList = new ArrayList();
                Fallback fallbacksByHost = getFallbacksByHost("f3.mi-stat.gslb.mi-idc.com");
                double d = 0.1d;
                if (fallbacksByHost != null) {
                    fallbacksByHost.c();
                    d = fallbacksByHost.f();
                } else {
                    arrayList.add("f3.mi-stat.gslb.mi-idc.com");
                }
                if (((double) this.mRandomGenerator.nextInt(10000)) < d * 10000.0d) {
                    try {
                        uploadHostStat("f3.mi-stat.gslb.mi-idc.com", str);
                    } catch (IOException e) {
                        Log.e("HostManager", null, e);
                    }
                }
            }
        } catch (JSONException e2) {
        }
    }

    private void uploadHostStat(String str, String str2) {
        ArrayList arrayList = new ArrayList();
        String valueOf = String.valueOf(System.nanoTime());
        String valueOf2 = String.valueOf(System.currentTimeMillis());
        arrayList.add(new BasicNameValuePair("n", valueOf));
        arrayList.add(new BasicNameValuePair("d", str2));
        arrayList.add(new BasicNameValuePair("t", valueOf2));
        arrayList.add(new BasicNameValuePair("s", getMd5Digest(valueOf + str2 + valueOf2 + "56C6A520%$C99119A0&^229(!@2746C7")));
        Network.a(this.sAppContext, String.format("http://%1$s/diagnoses/v1/report", str), arrayList);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004b A[SYNTHETIC, Splitter:B:20:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x007f A[SYNTHETIC, Splitter:B:48:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0089 A[SYNTHETIC, Splitter:B:54:0x0089] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:32:0x006a=Splitter:B:32:0x006a, B:56:0x008c=Splitter:B:56:0x008c, B:22:0x004e=Splitter:B:22:0x004e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean checkHostMapping() {
        /*
            r7 = this;
            r0 = 1
            r1 = 0
            java.util.Map<java.lang.String, com.xiaomi.network.Fallbacks> r3 = r7.mHostsMapping
            monitor-enter(r3)
            boolean r2 = com.xiaomi.network.HostManager.hostLoaded     // Catch:{ all -> 0x006c }
            if (r2 != 0) goto L_0x008d
            r2 = 1
            com.xiaomi.network.HostManager.hostLoaded = r2     // Catch:{ all -> 0x006c }
            java.util.Map<java.lang.String, com.xiaomi.network.Fallbacks> r2 = r7.mHostsMapping     // Catch:{ all -> 0x006c }
            r2.clear()     // Catch:{ all -> 0x006c }
            java.io.File r4 = new java.io.File     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            android.content.Context r2 = r7.sAppContext     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            java.io.File r2 = r2.getFilesDir()     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            java.lang.String r5 = r7.getProcessName()     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            r4.<init>(r2, r5)     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            boolean r2 = r4.isFile()     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            if (r2 == 0) goto L_0x006f
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            r6.<init>(r4)     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            r5.<init>(r6)     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            r2.<init>(r5)     // Catch:{ IOException -> 0x0095, Throwable -> 0x009b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
            r1.<init>()     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
        L_0x003a:
            java.lang.String r4 = r2.readLine()     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
            if (r4 == 0) goto L_0x0051
            r1.append(r4)     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
            goto L_0x003a
        L_0x0044:
            r0 = move-exception
            r1 = r2
        L_0x0046:
            r0.printStackTrace()     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x004e
            r1.close()     // Catch:{ IOException -> 0x0091 }
        L_0x004e:
            monitor-exit(r3)     // Catch:{ all -> 0x006c }
            r0 = 0
        L_0x0050:
            return r0
        L_0x0051:
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
            boolean r4 = android.text.TextUtils.isEmpty(r1)     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
            if (r4 != 0) goto L_0x0070
            r7.fromJSON(r1)     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
            java.lang.String r1 = "HostManager"
            java.lang.String r4 = "loading the new hosts succeed"
            android.util.Log.v(r1, r4)     // Catch:{ IOException -> 0x0044, Throwable -> 0x0078, all -> 0x0085 }
            if (r2 == 0) goto L_0x006a
            r2.close()     // Catch:{ IOException -> 0x008f }
        L_0x006a:
            monitor-exit(r3)     // Catch:{ all -> 0x006c }
            goto L_0x0050
        L_0x006c:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x006c }
            throw r0
        L_0x006f:
            r2 = r1
        L_0x0070:
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ IOException -> 0x0076 }
            goto L_0x004e
        L_0x0076:
            r0 = move-exception
            goto L_0x004e
        L_0x0078:
            r0 = move-exception
            r1 = r2
        L_0x007a:
            r0.printStackTrace()     // Catch:{ all -> 0x0099 }
            if (r1 == 0) goto L_0x004e
            r1.close()     // Catch:{ IOException -> 0x0083 }
            goto L_0x004e
        L_0x0083:
            r0 = move-exception
            goto L_0x004e
        L_0x0085:
            r0 = move-exception
            r1 = r2
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()     // Catch:{ IOException -> 0x0093 }
        L_0x008c:
            throw r0     // Catch:{ all -> 0x006c }
        L_0x008d:
            monitor-exit(r3)     // Catch:{ all -> 0x006c }
            goto L_0x0050
        L_0x008f:
            r1 = move-exception
            goto L_0x006a
        L_0x0091:
            r0 = move-exception
            goto L_0x004e
        L_0x0093:
            r1 = move-exception
            goto L_0x008c
        L_0x0095:
            r0 = move-exception
            goto L_0x0046
        L_0x0097:
            r0 = move-exception
            goto L_0x0087
        L_0x0099:
            r0 = move-exception
            goto L_0x0087
        L_0x009b:
            r0 = move-exception
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.network.HostManager.checkHostMapping():boolean");
    }

    public void fireHostEvent() {
        if (!this.mTaskPending) {
            this.mTaskPending = true;
            this.mTimer.schedule(new b(this), (long) Util.MILLSECONDS_OF_MINUTE);
        }
    }

    public ArrayList<b> generateHostStats() {
        ArrayList<b> arrayList;
        b bVar;
        int i;
        int i2;
        long j;
        int i3;
        synchronized (this.mHostsMapping) {
            HashMap hashMap = new HashMap();
            for (String str : this.mHostsMapping.keySet()) {
                Fallbacks fallbacks = this.mHostsMapping.get(str);
                if (fallbacks != null) {
                    Iterator<Fallback> it = fallbacks.getFallbacks().iterator();
                    while (it.hasNext()) {
                        Fallback next = it.next();
                        b bVar2 = (b) hashMap.get(next.d());
                        if (bVar2 == null) {
                            b bVar3 = new b();
                            bVar3.a("httpapi");
                            bVar3.e(next.f);
                            bVar3.d(processNetwork(next.a));
                            bVar3.b(this.sUserId);
                            bVar3.c(getPackageName());
                            e eVar = new e();
                            eVar.c(next.d);
                            eVar.a(next.g);
                            eVar.b(next.c);
                            eVar.d(next.e);
                            bVar3.a(eVar);
                            hashMap.put(next.d(), bVar3);
                            bVar = bVar3;
                        } else {
                            bVar = bVar2;
                        }
                        com.xiaomi.a.a.a.a.a aVar = new com.xiaomi.a.a.a.a.a();
                        aVar.a(next.b);
                        ArrayList arrayList2 = new ArrayList();
                        Iterator<WeightedHost> it2 = next.e().iterator();
                        while (it2.hasNext()) {
                            WeightedHost next2 = it2.next();
                            ArrayList<AccessHistory> a = next2.a();
                            if (!a.isEmpty()) {
                                d dVar = new d();
                                dVar.a(next2.a);
                                int i4 = 0;
                                int i5 = 0;
                                long j2 = 0;
                                int i6 = 0;
                                HashMap hashMap2 = new HashMap();
                                Iterator<AccessHistory> it3 = a.iterator();
                                while (it3.hasNext()) {
                                    AccessHistory next3 = it3.next();
                                    if (next3.a() > 0) {
                                        int i7 = i4 + 1;
                                        i = i5;
                                        j = j2 + next3.b();
                                        i3 = (int) (((long) i6) + next3.d());
                                        i2 = i7;
                                    } else {
                                        String e = next3.e();
                                        if (!TextUtils.isEmpty(e)) {
                                            hashMap2.put(e, Integer.valueOf(hashMap2.containsKey(e) ? ((Integer) hashMap2.get(e)).intValue() + 1 : 1));
                                        }
                                        int i8 = i4;
                                        i = i5 + 1;
                                        i2 = i8;
                                        j = j2;
                                        i3 = i6;
                                    }
                                    i6 = i3;
                                    j2 = j;
                                    i5 = i;
                                    i4 = i2;
                                }
                                dVar.a(hashMap2);
                                dVar.b(i4);
                                dVar.a(i5);
                                dVar.a(j2);
                                dVar.c(i6);
                                arrayList2.add(dVar);
                            }
                        }
                        if (!arrayList2.isEmpty()) {
                            aVar.a(arrayList2);
                            bVar.a(aVar);
                        }
                    }
                    continue;
                }
            }
            arrayList = new ArrayList<>();
            for (b bVar4 : hashMap.values()) {
                if (bVar4.g() > 0) {
                    arrayList.add(bVar4);
                }
            }
        }
        return arrayList;
    }

    public String getActiveNetworkLabel() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.sAppContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        if (activeNetworkInfo.getType() != 1) {
            return activeNetworkInfo.getTypeName() + "-" + activeNetworkInfo.getSubtypeName();
        }
        WifiManager wifiManager = (WifiManager) this.sAppContext.getSystemService("wifi");
        if (wifiManager == null || wifiManager.getConnectionInfo() == null) {
            return null;
        }
        return "WIFI-" + wifiManager.getConnectionInfo().getSSID();
    }

    public Fallback getFallbacksByHost(String str) {
        Fallback localFallback;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("the host is empty");
        }
        if (!this.sHostFilter.a(str)) {
            localFallback = null;
        } else {
            localFallback = getLocalFallback(str);
            if (localFallback == null) {
                if (System.currentTimeMillis() - this.lastRemoteRequestTimestamp > 1000 * 60 * this.remoteRequestFailureCount) {
                    this.lastRemoteRequestTimestamp = System.currentTimeMillis();
                    Fallback requestRemoteFallback = requestRemoteFallback(str);
                    if (requestRemoteFallback != null) {
                        this.remoteRequestFailureCount = 0;
                        return requestRemoteFallback;
                    } else if (this.remoteRequestFailureCount < 15) {
                        this.remoteRequestFailureCount = 1 + this.remoteRequestFailureCount;
                    }
                }
                ArrayList arrayList = mReservedHosts.get(str);
                synchronized (mReservedHosts) {
                    if (arrayList == null) {
                        return null;
                    }
                    Fallback fallback = new Fallback(str);
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        fallback.b((String) it.next());
                    }
                    return fallback;
                }
            }
        }
        return localFallback;
    }

    /* access modifiers changed from: protected */
    public String getProcessName() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.sAppContext.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == Process.myPid()) {
                    return next.processName;
                }
            }
        }
        return "com.xiaomi";
    }

    public boolean isWIFIConnected() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) this.sAppContext.getSystemService("connectivity");
        if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null) {
            return false;
        }
        return 1 == activeNetworkInfo.getType();
    }

    public void persist() {
        purge();
        synchronized (this.mHostsMapping) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.sAppContext.openFileOutput(getProcessName(), 0)));
                bufferedWriter.write(toJSON().toString());
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return;
    }

    public void purge() {
        synchronized (this.mHostsMapping) {
            for (Fallbacks purge : this.mHostsMapping.values()) {
                purge.purge();
            }
            boolean z = false;
            while (!z) {
                Iterator<String> it = this.mHostsMapping.keySet().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z = true;
                        break;
                    }
                    String next = it.next();
                    if (this.mHostsMapping.get(next).getFallbacks().isEmpty()) {
                        this.mHostsMapping.remove(next);
                        z = false;
                        break;
                    }
                }
            }
        }
    }

    public void refreshFallbacks() {
        ArrayList arrayList;
        synchronized (this.mHostsMapping) {
            checkHostMapping();
            arrayList = new ArrayList(this.mHostsMapping.keySet());
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                Fallbacks fallbacks = this.mHostsMapping.get(arrayList.get(size));
                if (!(fallbacks == null || fallbacks.getFallback() == null)) {
                    arrayList.remove(size);
                }
            }
        }
        ArrayList<Fallback> requestRemoteFallbacks = requestRemoteFallbacks(arrayList);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                if (requestRemoteFallbacks.get(i2) != null) {
                    updateFallbacks((String) arrayList.get(i2), requestRemoteFallbacks.get(i2));
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void setCurrentISP(String str) {
        this.currentISP = str;
    }

    public void updateFallbacks(String str, Fallback fallback) {
        if (TextUtils.isEmpty(str) || fallback == null) {
            throw new IllegalArgumentException("the argument is invalid " + str + ", " + fallback);
        } else if (this.sHostFilter.a(str)) {
            synchronized (this.mHostsMapping) {
                checkHostMapping();
                if (this.mHostsMapping.containsKey(str)) {
                    this.mHostsMapping.get(str).addFallback(fallback);
                } else {
                    Fallbacks fallbacks = new Fallbacks(str);
                    fallbacks.addFallback(fallback);
                    this.mHostsMapping.put(str, fallbacks);
                }
            }
        }
    }
}
