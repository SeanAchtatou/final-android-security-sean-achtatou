package com.xiaomi.network;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONObject;

public class WeightedHost implements Comparable<WeightedHost> {
    public String a;
    protected int b;
    private final LinkedList<AccessHistory> c;
    private long d;

    public WeightedHost() {
        this(null, 0);
    }

    public WeightedHost(String str) {
        this(str, 0);
    }

    public WeightedHost(String str, int i) {
        this.c = new LinkedList<>();
        this.d = 0;
        this.a = str;
        this.b = i;
    }

    /* renamed from: a */
    public int compareTo(WeightedHost weightedHost) {
        if (weightedHost == null) {
            return 1;
        }
        return weightedHost.b - this.b;
    }

    public WeightedHost a(JSONObject jSONObject) {
        synchronized (this) {
            this.d = jSONObject.getLong("tt");
            this.b = jSONObject.getInt("wt");
            this.a = jSONObject.getString("host");
            JSONArray jSONArray = jSONObject.getJSONArray("ah");
            for (int i = 0; i < jSONArray.length(); i++) {
                this.c.add(new AccessHistory().a(jSONArray.getJSONObject(i)));
            }
        }
        return this;
    }

    public ArrayList<AccessHistory> a() {
        ArrayList<AccessHistory> arrayList;
        synchronized (this) {
            arrayList = new ArrayList<>();
            Iterator<AccessHistory> it = this.c.iterator();
            while (it.hasNext()) {
                AccessHistory next = it.next();
                if (next.c() > this.d) {
                    arrayList.add(next);
                }
            }
            this.d = System.currentTimeMillis();
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(AccessHistory accessHistory) {
        synchronized (this) {
            if (accessHistory != null) {
                HostManager.getInstance().fireHostEvent();
                this.c.add(accessHistory);
                int a2 = accessHistory.a();
                if (a2 > 0) {
                    this.b += accessHistory.a();
                } else {
                    int i = 0;
                    int size = this.c.size() - 1;
                    while (size >= 0 && this.c.get(size).a() < 0) {
                        i++;
                        size--;
                    }
                    this.b += a2 * i;
                }
                if (this.c.size() > 30) {
                    this.b -= this.c.remove().a();
                }
            }
        }
    }

    public JSONObject b() {
        JSONObject jSONObject;
        synchronized (this) {
            jSONObject = new JSONObject();
            jSONObject.put("tt", this.d);
            jSONObject.put("wt", this.b);
            jSONObject.put("host", this.a);
            JSONArray jSONArray = new JSONArray();
            Iterator<AccessHistory> it = this.c.iterator();
            while (it.hasNext()) {
                jSONArray.put(it.next().f());
            }
            jSONObject.put("ah", jSONArray);
        }
        return jSONObject;
    }

    public String toString() {
        return this.a + ":" + this.b;
    }
}
