package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.ListMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TList;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class f implements Serializable, Cloneable, TBase<f, a> {
    public static final Map<a, FieldMetaData> l;
    private static final TStruct m = new TStruct("XmPushActionCommandResult");
    private static final TField n = new TField("debug", (byte) 11, 1);
    private static final TField o = new TField("target", (byte) 12, 2);
    private static final TField p = new TField(LocaleUtil.INDONESIAN, (byte) 11, 3);
    private static final TField q = new TField(ApiParams.KEY_APPID, (byte) 11, 4);
    private static final TField r = new TField("cmdName", (byte) 11, 5);
    private static final TField s = new TField("request", (byte) 12, 6);
    private static final TField t = new TField("errorCode", (byte) 10, 7);
    private static final TField u = new TField("reason", (byte) 11, 8);
    private static final TField v = new TField("packageName", (byte) 11, 9);
    private static final TField w = new TField("cmdArgs", (byte) 15, 10);
    private static final TField x = new TField("category", (byte) 11, 12);
    public String a;
    public c b;
    public String c;
    public String d;
    public String e;
    public e f;
    public long g;
    public String h;
    public String i;
    public List<String> j;
    public String k;
    private BitSet y = new BitSet(1);

    public enum a implements TFieldIdEnum {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, LocaleUtil.INDONESIAN),
        APP_ID(4, ApiParams.KEY_APPID),
        CMD_NAME(5, "cmdName"),
        REQUEST(6, "request"),
        ERROR_CODE(7, "errorCode"),
        REASON(8, "reason"),
        PACKAGE_NAME(9, "packageName"),
        CMD_ARGS(10, "cmdArgs"),
        CATEGORY(12, "category");
        
        private static final Map<String, a> l = new HashMap();
        private final short m;
        private final String n;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                l.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.m = s;
            this.n = str;
        }

        public String a() {
            return this.n;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.f$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new FieldMetaData("debug", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new FieldMetaData("target", (byte) 2, new StructMetaData((byte) 12, c.class)));
        enumMap.put((Object) a.ID, (Object) new FieldMetaData(LocaleUtil.INDONESIAN, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new FieldMetaData(ApiParams.KEY_APPID, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.CMD_NAME, (Object) new FieldMetaData("cmdName", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.REQUEST, (Object) new FieldMetaData("request", (byte) 2, new StructMetaData((byte) 12, e.class)));
        enumMap.put((Object) a.ERROR_CODE, (Object) new FieldMetaData("errorCode", (byte) 1, new FieldValueMetaData((byte) 10)));
        enumMap.put((Object) a.REASON, (Object) new FieldMetaData("reason", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new FieldMetaData("packageName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.CMD_ARGS, (Object) new FieldMetaData("cmdArgs", (byte) 2, new ListMetaData((byte) 15, new FieldValueMetaData((byte) 11))));
        enumMap.put((Object) a.CATEGORY, (Object) new FieldMetaData("category", (byte) 2, new FieldValueMetaData((byte) 11)));
        l = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(f.class, l);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                if (!h()) {
                    throw new TProtocolException("Required field 'errorCode' was not found in serialized data! Struct: " + toString());
                }
                o();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = new c();
                        this.b.a(tProtocol);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.w();
                        break;
                    }
                case 6:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = new e();
                        this.f.a(tProtocol);
                        break;
                    }
                case 7:
                    if (i2.b != 10) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.g = tProtocol.u();
                        a(true);
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.h = tProtocol.w();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.i = tProtocol.w();
                        break;
                    }
                case 10:
                    if (i2.b != 15) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        TList m2 = tProtocol.m();
                        this.j = new ArrayList(m2.b);
                        for (int i3 = 0; i3 < m2.b; i3++) {
                            this.j.add(tProtocol.w());
                        }
                        tProtocol.n();
                        break;
                    }
                case 11:
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
                case 12:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.k = tProtocol.w();
                        break;
                    }
            }
            tProtocol.j();
        }
    }

    public void a(boolean z) {
        this.y.set(0, z);
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(f fVar) {
        if (fVar != null) {
            boolean a2 = a();
            boolean a3 = fVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(fVar.a))) {
                boolean b2 = b();
                boolean b3 = fVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.a(fVar.b))) {
                    boolean c2 = c();
                    boolean c3 = fVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(fVar.c))) {
                        boolean d2 = d();
                        boolean d3 = fVar.d();
                        if ((!d2 && !d3) || (d2 && d3 && this.d.equals(fVar.d))) {
                            boolean f2 = f();
                            boolean f3 = fVar.f();
                            if ((!f2 && !f3) || (f2 && f3 && this.e.equals(fVar.e))) {
                                boolean g2 = g();
                                boolean g3 = fVar.g();
                                if (((!g2 && !g3) || (g2 && g3 && this.f.a(fVar.f))) && this.g == fVar.g) {
                                    boolean i2 = i();
                                    boolean i3 = fVar.i();
                                    if ((!i2 && !i3) || (i2 && i3 && this.h.equals(fVar.h))) {
                                        boolean j2 = j();
                                        boolean j3 = fVar.j();
                                        if ((!j2 && !j3) || (j2 && j3 && this.i.equals(fVar.i))) {
                                            boolean l2 = l();
                                            boolean l3 = fVar.l();
                                            if ((!l2 && !l3) || (l2 && l3 && this.j.equals(fVar.j))) {
                                                boolean n2 = n();
                                                boolean n3 = fVar.n();
                                                return (!n2 && !n3) || (n2 && n3 && this.k.equals(fVar.k));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(f fVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        int a12;
        if (!getClass().equals(fVar.getClass())) {
            return getClass().getName().compareTo(fVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(fVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a12 = TBaseHelper.a(this.a, fVar.a)) != 0) {
            return a12;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(fVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a11 = TBaseHelper.a(this.b, fVar.b)) != 0) {
            return a11;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(fVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a10 = TBaseHelper.a(this.c, fVar.c)) != 0) {
            return a10;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(fVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a9 = TBaseHelper.a(this.d, fVar.d)) != 0) {
            return a9;
        }
        int compareTo5 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(fVar.f()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (f() && (a8 = TBaseHelper.a(this.e, fVar.e)) != 0) {
            return a8;
        }
        int compareTo6 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(fVar.g()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (g() && (a7 = TBaseHelper.a(this.f, fVar.f)) != 0) {
            return a7;
        }
        int compareTo7 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(fVar.h()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (h() && (a6 = TBaseHelper.a(this.g, fVar.g)) != 0) {
            return a6;
        }
        int compareTo8 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(fVar.i()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (i() && (a5 = TBaseHelper.a(this.h, fVar.h)) != 0) {
            return a5;
        }
        int compareTo9 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(fVar.j()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (j() && (a4 = TBaseHelper.a(this.i, fVar.i)) != 0) {
            return a4;
        }
        int compareTo10 = Boolean.valueOf(l()).compareTo(Boolean.valueOf(fVar.l()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (l() && (a3 = TBaseHelper.a(this.j, fVar.j)) != 0) {
            return a3;
        }
        int compareTo11 = Boolean.valueOf(n()).compareTo(Boolean.valueOf(fVar.n()));
        if (compareTo11 != 0) {
            return compareTo11;
        }
        if (!n() || (a2 = TBaseHelper.a(this.k, fVar.k)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        o();
        tProtocol.a(m);
        if (this.a != null && a()) {
            tProtocol.a(n);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(o);
            this.b.b(tProtocol);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(p);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null) {
            tProtocol.a(q);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null) {
            tProtocol.a(r);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        if (this.f != null && g()) {
            tProtocol.a(s);
            this.f.b(tProtocol);
            tProtocol.b();
        }
        tProtocol.a(t);
        tProtocol.a(this.g);
        tProtocol.b();
        if (this.h != null && i()) {
            tProtocol.a(u);
            tProtocol.a(this.h);
            tProtocol.b();
        }
        if (this.i != null && j()) {
            tProtocol.a(v);
            tProtocol.a(this.i);
            tProtocol.b();
        }
        if (this.j != null && l()) {
            tProtocol.a(w);
            tProtocol.a(new TList((byte) 11, this.j.size()));
            for (String a2 : this.j) {
                tProtocol.a(a2);
            }
            tProtocol.e();
            tProtocol.b();
        }
        if (this.k != null && n()) {
            tProtocol.a(x);
            tProtocol.a(this.k);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public String e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof f)) {
            return a((f) obj);
        }
        return false;
    }

    public boolean f() {
        return this.e != null;
    }

    public boolean g() {
        return this.f != null;
    }

    public boolean h() {
        return this.y.get(0);
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.h != null;
    }

    public boolean j() {
        return this.i != null;
    }

    public List<String> k() {
        return this.j;
    }

    public boolean l() {
        return this.j != null;
    }

    public String m() {
        return this.k;
    }

    public boolean n() {
        return this.k != null;
    }

    public void o() {
        if (this.c == null) {
            throw new TProtocolException("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new TProtocolException("Required field 'cmdName' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionCommandResult(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("cmdName:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        if (g()) {
            sb.append(", ");
            sb.append("request:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("errorCode:");
        sb.append(this.g);
        if (i()) {
            sb.append(", ");
            sb.append("reason:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("cmdArgs:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (n()) {
            sb.append(", ");
            sb.append("category:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
