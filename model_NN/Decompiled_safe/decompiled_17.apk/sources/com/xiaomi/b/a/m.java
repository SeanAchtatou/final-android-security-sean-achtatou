package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.message.RMsgInfoDB;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.MapMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TMap;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class m implements Serializable, Cloneable, TBase<m, a> {
    public static final Map<a, FieldMetaData> l;
    private static final TStruct m = new TStruct("XmPushActionSendMessage");
    private static final TField n = new TField("debug", (byte) 11, 1);
    private static final TField o = new TField("target", (byte) 12, 2);
    private static final TField p = new TField(LocaleUtil.INDONESIAN, (byte) 11, 3);
    private static final TField q = new TField(ApiParams.KEY_APPID, (byte) 11, 4);
    private static final TField r = new TField("packageName", (byte) 11, 5);
    private static final TField s = new TField("topic", (byte) 11, 6);
    private static final TField t = new TField("aliasName", (byte) 11, 7);
    private static final TField u = new TField(RMsgInfoDB.TABLE, (byte) 12, 8);
    private static final TField v = new TField("needAck", (byte) 2, 9);
    private static final TField w = new TField("params", (byte) 13, 10);
    private static final TField x = new TField("category", (byte) 11, 11);
    public String a;
    public c b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public b h;
    public boolean i = true;
    public Map<String, String> j;
    public String k;
    private BitSet y = new BitSet(1);

    public enum a implements TFieldIdEnum {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, LocaleUtil.INDONESIAN),
        APP_ID(4, ApiParams.KEY_APPID),
        PACKAGE_NAME(5, "packageName"),
        TOPIC(6, "topic"),
        ALIAS_NAME(7, "aliasName"),
        MESSAGE(8, RMsgInfoDB.TABLE),
        NEED_ACK(9, "needAck"),
        PARAMS(10, "params"),
        CATEGORY(11, "category");
        
        private static final Map<String, a> l = new HashMap();
        private final short m;
        private final String n;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                l.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.m = s;
            this.n = str;
        }

        public String a() {
            return this.n;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.m$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new FieldMetaData("debug", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new FieldMetaData("target", (byte) 2, new StructMetaData((byte) 12, c.class)));
        enumMap.put((Object) a.ID, (Object) new FieldMetaData(LocaleUtil.INDONESIAN, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new FieldMetaData(ApiParams.KEY_APPID, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new FieldMetaData("packageName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TOPIC, (Object) new FieldMetaData("topic", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.ALIAS_NAME, (Object) new FieldMetaData("aliasName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.MESSAGE, (Object) new FieldMetaData(RMsgInfoDB.TABLE, (byte) 2, new StructMetaData((byte) 12, b.class)));
        enumMap.put((Object) a.NEED_ACK, (Object) new FieldMetaData("needAck", (byte) 2, new FieldValueMetaData((byte) 2)));
        enumMap.put((Object) a.PARAMS, (Object) new FieldMetaData("params", (byte) 2, new MapMetaData((byte) 13, new FieldValueMetaData((byte) 11), new FieldValueMetaData((byte) 11))));
        enumMap.put((Object) a.CATEGORY, (Object) new FieldMetaData("category", (byte) 2, new FieldValueMetaData((byte) 11)));
        l = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(m.class, l);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                r();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = new c();
                        this.b.a(tProtocol);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.w();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = tProtocol.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.g = tProtocol.w();
                        break;
                    }
                case 8:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.h = new b();
                        this.h.a(tProtocol);
                        break;
                    }
                case 9:
                    if (i2.b != 2) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.i = tProtocol.q();
                        a(true);
                        break;
                    }
                case 10:
                    if (i2.b != 13) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        TMap k2 = tProtocol.k();
                        this.j = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.j.put(tProtocol.w(), tProtocol.w());
                        }
                        tProtocol.l();
                        break;
                    }
                case 11:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.k = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public void a(boolean z) {
        this.y.set(0, z);
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(m mVar) {
        if (mVar != null) {
            boolean a2 = a();
            boolean a3 = mVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(mVar.a))) {
                boolean b2 = b();
                boolean b3 = mVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.a(mVar.b))) {
                    boolean d2 = d();
                    boolean d3 = mVar.d();
                    if ((!d2 && !d3) || (d2 && d3 && this.c.equals(mVar.c))) {
                        boolean f2 = f();
                        boolean f3 = mVar.f();
                        if ((!f2 && !f3) || (f2 && f3 && this.d.equals(mVar.d))) {
                            boolean g2 = g();
                            boolean g3 = mVar.g();
                            if ((!g2 && !g3) || (g2 && g3 && this.e.equals(mVar.e))) {
                                boolean i2 = i();
                                boolean i3 = mVar.i();
                                if ((!i2 && !i3) || (i2 && i3 && this.f.equals(mVar.f))) {
                                    boolean k2 = k();
                                    boolean k3 = mVar.k();
                                    if ((!k2 && !k3) || (k2 && k3 && this.g.equals(mVar.g))) {
                                        boolean m2 = m();
                                        boolean m3 = mVar.m();
                                        if ((!m2 && !m3) || (m2 && m3 && this.h.a(mVar.h))) {
                                            boolean n2 = n();
                                            boolean n3 = mVar.n();
                                            if ((!n2 && !n3) || (n2 && n3 && this.i == mVar.i)) {
                                                boolean o2 = o();
                                                boolean o3 = mVar.o();
                                                if ((!o2 && !o3) || (o2 && o3 && this.j.equals(mVar.j))) {
                                                    boolean q2 = q();
                                                    boolean q3 = mVar.q();
                                                    return (!q2 && !q3) || (q2 && q3 && this.k.equals(mVar.k));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(m mVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        int a12;
        if (!getClass().equals(mVar.getClass())) {
            return getClass().getName().compareTo(mVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(mVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a12 = TBaseHelper.a(this.a, mVar.a)) != 0) {
            return a12;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(mVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a11 = TBaseHelper.a(this.b, mVar.b)) != 0) {
            return a11;
        }
        int compareTo3 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(mVar.d()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (d() && (a10 = TBaseHelper.a(this.c, mVar.c)) != 0) {
            return a10;
        }
        int compareTo4 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(mVar.f()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (f() && (a9 = TBaseHelper.a(this.d, mVar.d)) != 0) {
            return a9;
        }
        int compareTo5 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(mVar.g()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (g() && (a8 = TBaseHelper.a(this.e, mVar.e)) != 0) {
            return a8;
        }
        int compareTo6 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(mVar.i()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (i() && (a7 = TBaseHelper.a(this.f, mVar.f)) != 0) {
            return a7;
        }
        int compareTo7 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(mVar.k()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (k() && (a6 = TBaseHelper.a(this.g, mVar.g)) != 0) {
            return a6;
        }
        int compareTo8 = Boolean.valueOf(m()).compareTo(Boolean.valueOf(mVar.m()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (m() && (a5 = TBaseHelper.a(this.h, mVar.h)) != 0) {
            return a5;
        }
        int compareTo9 = Boolean.valueOf(n()).compareTo(Boolean.valueOf(mVar.n()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (n() && (a4 = TBaseHelper.a(this.i, mVar.i)) != 0) {
            return a4;
        }
        int compareTo10 = Boolean.valueOf(o()).compareTo(Boolean.valueOf(mVar.o()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (o() && (a3 = TBaseHelper.a(this.j, mVar.j)) != 0) {
            return a3;
        }
        int compareTo11 = Boolean.valueOf(q()).compareTo(Boolean.valueOf(mVar.q()));
        if (compareTo11 != 0) {
            return compareTo11;
        }
        if (!q() || (a2 = TBaseHelper.a(this.k, mVar.k)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        r();
        tProtocol.a(m);
        if (this.a != null && a()) {
            tProtocol.a(n);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(o);
            this.b.b(tProtocol);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(p);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null) {
            tProtocol.a(q);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null && g()) {
            tProtocol.a(r);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        if (this.f != null && i()) {
            tProtocol.a(s);
            tProtocol.a(this.f);
            tProtocol.b();
        }
        if (this.g != null && k()) {
            tProtocol.a(t);
            tProtocol.a(this.g);
            tProtocol.b();
        }
        if (this.h != null && m()) {
            tProtocol.a(u);
            this.h.b(tProtocol);
            tProtocol.b();
        }
        if (n()) {
            tProtocol.a(v);
            tProtocol.a(this.i);
            tProtocol.b();
        }
        if (this.j != null && o()) {
            tProtocol.a(w);
            tProtocol.a(new TMap((byte) 11, (byte) 11, this.j.size()));
            for (Map.Entry next : this.j.entrySet()) {
                tProtocol.a((String) next.getKey());
                tProtocol.a((String) next.getValue());
            }
            tProtocol.d();
            tProtocol.b();
        }
        if (this.k != null && q()) {
            tProtocol.a(x);
            tProtocol.a(this.k);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public String c() {
        return this.c;
    }

    public boolean d() {
        return this.c != null;
    }

    public String e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof m)) {
            return a((m) obj);
        }
        return false;
    }

    public boolean f() {
        return this.d != null;
    }

    public boolean g() {
        return this.e != null;
    }

    public String h() {
        return this.f;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.f != null;
    }

    public String j() {
        return this.g;
    }

    public boolean k() {
        return this.g != null;
    }

    public b l() {
        return this.h;
    }

    public boolean m() {
        return this.h != null;
    }

    public boolean n() {
        return this.y.get(0);
    }

    public boolean o() {
        return this.j != null;
    }

    public String p() {
        return this.k;
    }

    public boolean q() {
        return this.k != null;
    }

    public void r() {
        if (this.c == null) {
            throw new TProtocolException("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'appId' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionSendMessage(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (g()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("topic:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("aliasName:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (m()) {
            sb.append(", ");
            sb.append("message:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (n()) {
            sb.append(", ");
            sb.append("needAck:");
            sb.append(this.i);
        }
        if (o()) {
            sb.append(", ");
            sb.append("params:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (q()) {
            sb.append(", ");
            sb.append("category:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
