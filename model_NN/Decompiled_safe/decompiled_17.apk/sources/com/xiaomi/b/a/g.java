package com.xiaomi.b.a;

import com.tencent.tauth.Constants;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.EnumMetaData;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class g implements Serializable, Cloneable, TBase<g, a> {
    public static final Map<a, FieldMetaData> h;
    private static final TStruct i = new TStruct("XmPushActionContainer");
    private static final TField j = new TField("action", (byte) 8, 1);
    private static final TField k = new TField("encryptAction", (byte) 2, 2);
    private static final TField l = new TField("isRequest", (byte) 2, 3);
    private static final TField m = new TField("pushAction", (byte) 11, 4);
    private static final TField n = new TField(Constants.PARAM_APP_ID, (byte) 11, 5);
    private static final TField o = new TField("packageName", (byte) 11, 6);
    private static final TField p = new TField("target", (byte) 12, 7);
    public a a;
    public boolean b = true;
    public boolean c = true;
    public ByteBuffer d;
    public String e;
    public String f;
    public c g;
    private BitSet q = new BitSet(2);

    public enum a implements TFieldIdEnum {
        ACTION(1, "action"),
        ENCRYPT_ACTION(2, "encryptAction"),
        IS_REQUEST(3, "isRequest"),
        PUSH_ACTION(4, "pushAction"),
        APPID(5, Constants.PARAM_APP_ID),
        PACKAGE_NAME(6, "packageName"),
        TARGET(7, "target");
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.g$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.ACTION, (Object) new FieldMetaData("action", (byte) 1, new EnumMetaData((byte) 16, a.class)));
        enumMap.put((Object) a.ENCRYPT_ACTION, (Object) new FieldMetaData("encryptAction", (byte) 1, new FieldValueMetaData((byte) 2)));
        enumMap.put((Object) a.IS_REQUEST, (Object) new FieldMetaData("isRequest", (byte) 1, new FieldValueMetaData((byte) 2)));
        enumMap.put((Object) a.PUSH_ACTION, (Object) new FieldMetaData("pushAction", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APPID, (Object) new FieldMetaData(Constants.PARAM_APP_ID, (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new FieldMetaData("packageName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new FieldMetaData("target", (byte) 1, new StructMetaData((byte) 12, c.class)));
        h = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(g.class, h);
    }

    public a a() {
        return this.a;
    }

    public g a(a aVar) {
        this.a = aVar;
        return this;
    }

    public g a(c cVar) {
        this.g = cVar;
        return this;
    }

    public g a(String str) {
        this.e = str;
        return this;
    }

    public g a(ByteBuffer byteBuffer) {
        this.d = byteBuffer;
        return this;
    }

    public g a(boolean z) {
        this.b = z;
        b(true);
        return this;
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                if (!d()) {
                    throw new TProtocolException("Required field 'encryptAction' was not found in serialized data! Struct: " + toString());
                } else if (!e()) {
                    throw new TProtocolException("Required field 'isRequest' was not found in serialized data! Struct: " + toString());
                } else {
                    l();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 8) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.a = a.a(tProtocol.t());
                            break;
                        }
                    case 2:
                        if (i2.b != 2) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.b = tProtocol.q();
                            b(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 2) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.c = tProtocol.q();
                            d(true);
                            break;
                        }
                    case 4:
                        if (i2.b != 11) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.d = tProtocol.x();
                            break;
                        }
                    case 5:
                        if (i2.b != 11) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.e = tProtocol.w();
                            break;
                        }
                    case 6:
                        if (i2.b != 11) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.f = tProtocol.w();
                            break;
                        }
                    case 7:
                        if (i2.b != 12) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.g = new c();
                            this.g.a(tProtocol);
                            break;
                        }
                    default:
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                }
                tProtocol.j();
            }
        }
    }

    public boolean a(g gVar) {
        if (gVar != null) {
            boolean b2 = b();
            boolean b3 = gVar.b();
            if (((!b2 && !b3) || (b2 && b3 && this.a.equals(gVar.a))) && this.b == gVar.b && this.c == gVar.c) {
                boolean g2 = g();
                boolean g3 = gVar.g();
                if ((!g2 && !g3) || (g2 && g3 && this.d.equals(gVar.d))) {
                    boolean h2 = h();
                    boolean h3 = gVar.h();
                    if ((!h2 && !h3) || (h2 && h3 && this.e.equals(gVar.e))) {
                        boolean j2 = j();
                        boolean j3 = gVar.j();
                        if ((!j2 && !j3) || (j2 && j3 && this.f.equals(gVar.f))) {
                            boolean k2 = k();
                            boolean k3 = gVar.k();
                            return (!k2 && !k3) || (k2 && k3 && this.g.a(gVar.g));
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(g gVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        if (!getClass().equals(gVar.getClass())) {
            return getClass().getName().compareTo(gVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(b()).compareTo(Boolean.valueOf(gVar.b()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (b() && (a8 = TBaseHelper.a(this.a, gVar.a)) != 0) {
            return a8;
        }
        int compareTo2 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(gVar.d()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (d() && (a7 = TBaseHelper.a(this.b, gVar.b)) != 0) {
            return a7;
        }
        int compareTo3 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(gVar.e()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (e() && (a6 = TBaseHelper.a(this.c, gVar.c)) != 0) {
            return a6;
        }
        int compareTo4 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(gVar.g()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (g() && (a5 = TBaseHelper.a(this.d, gVar.d)) != 0) {
            return a5;
        }
        int compareTo5 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(gVar.h()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (h() && (a4 = TBaseHelper.a(this.e, gVar.e)) != 0) {
            return a4;
        }
        int compareTo6 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(gVar.j()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (j() && (a3 = TBaseHelper.a(this.f, gVar.f)) != 0) {
            return a3;
        }
        int compareTo7 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(gVar.k()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (!k() || (a2 = TBaseHelper.a(this.g, gVar.g)) == 0) {
            return 0;
        }
        return a2;
    }

    public g b(String str) {
        this.f = str;
        return this;
    }

    public void b(TProtocol tProtocol) {
        l();
        tProtocol.a(i);
        if (this.a != null) {
            tProtocol.a(j);
            tProtocol.a(this.a.a());
            tProtocol.b();
        }
        tProtocol.a(k);
        tProtocol.a(this.b);
        tProtocol.b();
        tProtocol.a(l);
        tProtocol.a(this.c);
        tProtocol.b();
        if (this.d != null) {
            tProtocol.a(m);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null && h()) {
            tProtocol.a(n);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        if (this.f != null && j()) {
            tProtocol.a(o);
            tProtocol.a(this.f);
            tProtocol.b();
        }
        if (this.g != null) {
            tProtocol.a(p);
            this.g.b(tProtocol);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public void b(boolean z) {
        this.q.set(0, z);
    }

    public boolean b() {
        return this.a != null;
    }

    public g c(boolean z) {
        this.c = z;
        d(true);
        return this;
    }

    public boolean c() {
        return this.b;
    }

    public void d(boolean z) {
        this.q.set(1, z);
    }

    public boolean d() {
        return this.q.get(0);
    }

    public boolean e() {
        return this.q.get(1);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof g)) {
            return a((g) obj);
        }
        return false;
    }

    public byte[] f() {
        a(TBaseHelper.c(this.d));
        return this.d.array();
    }

    public boolean g() {
        return this.d != null;
    }

    public boolean h() {
        return this.e != null;
    }

    public int hashCode() {
        return 0;
    }

    public String i() {
        return this.f;
    }

    public boolean j() {
        return this.f != null;
    }

    public boolean k() {
        return this.g != null;
    }

    public void l() {
        if (this.a == null) {
            throw new TProtocolException("Required field 'action' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'pushAction' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new TProtocolException("Required field 'target' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("XmPushActionContainer(");
        sb.append("action:");
        if (this.a == null) {
            sb.append("null");
        } else {
            sb.append(this.a);
        }
        sb.append(", ");
        sb.append("encryptAction:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("isRequest:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("pushAction:");
        if (this.d == null) {
            sb.append("null");
        } else {
            TBaseHelper.a(this.d, sb);
        }
        if (h()) {
            sb.append(", ");
            sb.append("appid:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("target:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        sb.append(")");
        return sb.toString();
    }
}
