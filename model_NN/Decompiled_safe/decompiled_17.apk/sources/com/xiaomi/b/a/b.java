package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class b implements Serializable, Cloneable, TBase<b, a> {
    public static final Map<a, FieldMetaData> i;
    private static final TStruct j = new TStruct("PushMessage");
    private static final TField k = new TField("to", (byte) 12, 1);
    private static final TField l = new TField(LocaleUtil.INDONESIAN, (byte) 11, 2);
    private static final TField m = new TField(ApiParams.KEY_APPID, (byte) 11, 3);
    private static final TField n = new TField("payload", (byte) 11, 4);
    private static final TField o = new TField("createAt", (byte) 10, 5);
    private static final TField p = new TField("ttl", (byte) 10, 6);
    private static final TField q = new TField("collapseKey", (byte) 11, 7);
    private static final TField r = new TField("packageName", (byte) 11, 8);
    public c a;
    public String b;
    public String c;
    public String d;
    public long e;
    public long f;
    public String g;
    public String h;
    private BitSet s = new BitSet(2);

    public enum a implements TFieldIdEnum {
        TO(1, "to"),
        ID(2, LocaleUtil.INDONESIAN),
        APP_ID(3, ApiParams.KEY_APPID),
        PAYLOAD(4, "payload"),
        CREATE_AT(5, "createAt"),
        TTL(6, "ttl"),
        COLLAPSE_KEY(7, "collapseKey"),
        PACKAGE_NAME(8, "packageName");
        
        private static final Map<String, a> i = new HashMap();
        private final short j;
        private final String k;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                i.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.j = s;
            this.k = str;
        }

        public String a() {
            return this.k;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.b$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.TO, (Object) new FieldMetaData("to", (byte) 2, new StructMetaData((byte) 12, c.class)));
        enumMap.put((Object) a.ID, (Object) new FieldMetaData(LocaleUtil.INDONESIAN, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new FieldMetaData(ApiParams.KEY_APPID, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PAYLOAD, (Object) new FieldMetaData("payload", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.CREATE_AT, (Object) new FieldMetaData("createAt", (byte) 2, new FieldValueMetaData((byte) 10)));
        enumMap.put((Object) a.TTL, (Object) new FieldMetaData("ttl", (byte) 2, new FieldValueMetaData((byte) 10)));
        enumMap.put((Object) a.COLLAPSE_KEY, (Object) new FieldMetaData("collapseKey", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new FieldMetaData("packageName", (byte) 2, new FieldValueMetaData((byte) 11)));
        i = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(b.class, i);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                l();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = new c();
                        this.a.a(tProtocol);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = tProtocol.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 10) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.u();
                        a(true);
                        break;
                    }
                case 6:
                    if (i2.b != 10) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = tProtocol.u();
                        b(true);
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.g = tProtocol.w();
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.h = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public void a(boolean z) {
        this.s.set(0, z);
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(b bVar) {
        if (bVar != null) {
            boolean a2 = a();
            boolean a3 = bVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.a(bVar.a))) {
                boolean c2 = c();
                boolean c3 = bVar.c();
                if ((!c2 && !c3) || (c2 && c3 && this.b.equals(bVar.b))) {
                    boolean d2 = d();
                    boolean d3 = bVar.d();
                    if ((!d2 && !d3) || (d2 && d3 && this.c.equals(bVar.c))) {
                        boolean f2 = f();
                        boolean f3 = bVar.f();
                        if ((!f2 && !f3) || (f2 && f3 && this.d.equals(bVar.d))) {
                            boolean h2 = h();
                            boolean h3 = bVar.h();
                            if ((!h2 && !h3) || (h2 && h3 && this.e == bVar.e)) {
                                boolean i2 = i();
                                boolean i3 = bVar.i();
                                if ((!i2 && !i3) || (i2 && i3 && this.f == bVar.f)) {
                                    boolean j2 = j();
                                    boolean j3 = bVar.j();
                                    if ((!j2 && !j3) || (j2 && j3 && this.g.equals(bVar.g))) {
                                        boolean k2 = k();
                                        boolean k3 = bVar.k();
                                        return (!k2 && !k3) || (k2 && k3 && this.h.equals(bVar.h));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(b bVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        if (!getClass().equals(bVar.getClass())) {
            return getClass().getName().compareTo(bVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(bVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a9 = TBaseHelper.a(this.a, bVar.a)) != 0) {
            return a9;
        }
        int compareTo2 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(bVar.c()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (c() && (a8 = TBaseHelper.a(this.b, bVar.b)) != 0) {
            return a8;
        }
        int compareTo3 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(bVar.d()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (d() && (a7 = TBaseHelper.a(this.c, bVar.c)) != 0) {
            return a7;
        }
        int compareTo4 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(bVar.f()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (f() && (a6 = TBaseHelper.a(this.d, bVar.d)) != 0) {
            return a6;
        }
        int compareTo5 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(bVar.h()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (h() && (a5 = TBaseHelper.a(this.e, bVar.e)) != 0) {
            return a5;
        }
        int compareTo6 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(bVar.i()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (i() && (a4 = TBaseHelper.a(this.f, bVar.f)) != 0) {
            return a4;
        }
        int compareTo7 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(bVar.j()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (j() && (a3 = TBaseHelper.a(this.g, bVar.g)) != 0) {
            return a3;
        }
        int compareTo8 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(bVar.k()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (!k() || (a2 = TBaseHelper.a(this.h, bVar.h)) == 0) {
            return 0;
        }
        return a2;
    }

    public String b() {
        return this.b;
    }

    public void b(TProtocol tProtocol) {
        l();
        tProtocol.a(j);
        if (this.a != null && a()) {
            tProtocol.a(k);
            this.a.b(tProtocol);
            tProtocol.b();
        }
        if (this.b != null) {
            tProtocol.a(l);
            tProtocol.a(this.b);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(m);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null) {
            tProtocol.a(n);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (h()) {
            tProtocol.a(o);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        if (i()) {
            tProtocol.a(p);
            tProtocol.a(this.f);
            tProtocol.b();
        }
        if (this.g != null && j()) {
            tProtocol.a(q);
            tProtocol.a(this.g);
            tProtocol.b();
        }
        if (this.h != null && k()) {
            tProtocol.a(r);
            tProtocol.a(this.h);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public void b(boolean z) {
        this.s.set(1, z);
    }

    public boolean c() {
        return this.b != null;
    }

    public boolean d() {
        return this.c != null;
    }

    public String e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof b)) {
            return a((b) obj);
        }
        return false;
    }

    public boolean f() {
        return this.d != null;
    }

    public long g() {
        return this.e;
    }

    public boolean h() {
        return this.s.get(0);
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.s.get(1);
    }

    public boolean j() {
        return this.g != null;
    }

    public boolean k() {
        return this.h != null;
    }

    public void l() {
        if (this.b == null) {
            throw new TProtocolException("Required field 'id' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new TProtocolException("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'payload' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PushMessage(");
        boolean z = true;
        if (a()) {
            sb.append("to:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z = false;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("payload:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (h()) {
            sb.append(", ");
            sb.append("createAt:");
            sb.append(this.e);
        }
        if (i()) {
            sb.append(", ");
            sb.append("ttl:");
            sb.append(this.f);
        }
        if (j()) {
            sb.append(", ");
            sb.append("collapseKey:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
