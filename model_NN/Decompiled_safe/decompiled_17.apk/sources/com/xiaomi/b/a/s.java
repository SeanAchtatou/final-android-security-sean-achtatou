package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class s implements Serializable, Cloneable, TBase<s, a> {
    public static final Map<a, FieldMetaData> k;
    private static final TStruct l = new TStruct("XmPushActionUnSubscriptionResult");
    private static final TField m = new TField("debug", (byte) 11, 1);
    private static final TField n = new TField("target", (byte) 12, 2);
    private static final TField o = new TField(LocaleUtil.INDONESIAN, (byte) 11, 3);
    private static final TField p = new TField(ApiParams.KEY_APPID, (byte) 11, 4);
    private static final TField q = new TField("request", (byte) 12, 5);
    private static final TField r = new TField("errorCode", (byte) 10, 6);
    private static final TField s = new TField("reason", (byte) 11, 7);
    private static final TField t = new TField("topic", (byte) 11, 8);
    private static final TField u = new TField("packageName", (byte) 11, 9);
    private static final TField v = new TField("category", (byte) 11, 10);
    public String a;
    public c b;
    public String c;
    public String d;
    public r e;
    public long f;
    public String g;
    public String h;
    public String i;
    public String j;
    private BitSet w = new BitSet(1);

    public enum a implements TFieldIdEnum {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, LocaleUtil.INDONESIAN),
        APP_ID(4, ApiParams.KEY_APPID),
        REQUEST(5, "request"),
        ERROR_CODE(6, "errorCode"),
        REASON(7, "reason"),
        TOPIC(8, "topic"),
        PACKAGE_NAME(9, "packageName"),
        CATEGORY(10, "category");
        
        private static final Map<String, a> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                k.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.s$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new FieldMetaData("debug", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new FieldMetaData("target", (byte) 2, new StructMetaData((byte) 12, c.class)));
        enumMap.put((Object) a.ID, (Object) new FieldMetaData(LocaleUtil.INDONESIAN, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new FieldMetaData(ApiParams.KEY_APPID, (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.REQUEST, (Object) new FieldMetaData("request", (byte) 2, new StructMetaData((byte) 12, r.class)));
        enumMap.put((Object) a.ERROR_CODE, (Object) new FieldMetaData("errorCode", (byte) 2, new FieldValueMetaData((byte) 10)));
        enumMap.put((Object) a.REASON, (Object) new FieldMetaData("reason", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TOPIC, (Object) new FieldMetaData("topic", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new FieldMetaData("packageName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.CATEGORY, (Object) new FieldMetaData("category", (byte) 2, new FieldValueMetaData((byte) 11)));
        k = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(s.class, k);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                m();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = new c();
                        this.b.a(tProtocol);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = new r();
                        this.e.a(tProtocol);
                        break;
                    }
                case 6:
                    if (i2.b != 10) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = tProtocol.u();
                        a(true);
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.g = tProtocol.w();
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.h = tProtocol.w();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.i = tProtocol.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.j = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public void a(boolean z) {
        this.w.set(0, z);
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(s sVar) {
        if (sVar != null) {
            boolean a2 = a();
            boolean a3 = sVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(sVar.a))) {
                boolean b2 = b();
                boolean b3 = sVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.a(sVar.b))) {
                    boolean c2 = c();
                    boolean c3 = sVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(sVar.c))) {
                        boolean d2 = d();
                        boolean d3 = sVar.d();
                        if ((!d2 && !d3) || (d2 && d3 && this.d.equals(sVar.d))) {
                            boolean e2 = e();
                            boolean e3 = sVar.e();
                            if ((!e2 && !e3) || (e2 && e3 && this.e.a(sVar.e))) {
                                boolean f2 = f();
                                boolean f3 = sVar.f();
                                if ((!f2 && !f3) || (f2 && f3 && this.f == sVar.f)) {
                                    boolean g2 = g();
                                    boolean g3 = sVar.g();
                                    if ((!g2 && !g3) || (g2 && g3 && this.g.equals(sVar.g))) {
                                        boolean i2 = i();
                                        boolean i3 = sVar.i();
                                        if ((!i2 && !i3) || (i2 && i3 && this.h.equals(sVar.h))) {
                                            boolean j2 = j();
                                            boolean j3 = sVar.j();
                                            if ((!j2 && !j3) || (j2 && j3 && this.i.equals(sVar.i))) {
                                                boolean l2 = l();
                                                boolean l3 = sVar.l();
                                                return (!l2 && !l3) || (l2 && l3 && this.j.equals(sVar.j));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(s sVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        if (!getClass().equals(sVar.getClass())) {
            return getClass().getName().compareTo(sVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(sVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a11 = TBaseHelper.a(this.a, sVar.a)) != 0) {
            return a11;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(sVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a10 = TBaseHelper.a(this.b, sVar.b)) != 0) {
            return a10;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(sVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a9 = TBaseHelper.a(this.c, sVar.c)) != 0) {
            return a9;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(sVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a8 = TBaseHelper.a(this.d, sVar.d)) != 0) {
            return a8;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(sVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a7 = TBaseHelper.a(this.e, sVar.e)) != 0) {
            return a7;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(sVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a6 = TBaseHelper.a(this.f, sVar.f)) != 0) {
            return a6;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(sVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (g() && (a5 = TBaseHelper.a(this.g, sVar.g)) != 0) {
            return a5;
        }
        int compareTo8 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(sVar.i()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (i() && (a4 = TBaseHelper.a(this.h, sVar.h)) != 0) {
            return a4;
        }
        int compareTo9 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(sVar.j()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (j() && (a3 = TBaseHelper.a(this.i, sVar.i)) != 0) {
            return a3;
        }
        int compareTo10 = Boolean.valueOf(l()).compareTo(Boolean.valueOf(sVar.l()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (!l() || (a2 = TBaseHelper.a(this.j, sVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        m();
        tProtocol.a(l);
        if (this.a != null && a()) {
            tProtocol.a(m);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(n);
            this.b.b(tProtocol);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(o);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null && d()) {
            tProtocol.a(p);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null && e()) {
            tProtocol.a(q);
            this.e.b(tProtocol);
            tProtocol.b();
        }
        if (f()) {
            tProtocol.a(r);
            tProtocol.a(this.f);
            tProtocol.b();
        }
        if (this.g != null && g()) {
            tProtocol.a(s);
            tProtocol.a(this.g);
            tProtocol.b();
        }
        if (this.h != null && i()) {
            tProtocol.a(t);
            tProtocol.a(this.h);
            tProtocol.b();
        }
        if (this.i != null && j()) {
            tProtocol.a(u);
            tProtocol.a(this.i);
            tProtocol.b();
        }
        if (this.j != null && l()) {
            tProtocol.a(v);
            tProtocol.a(this.j);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof s)) {
            return a((s) obj);
        }
        return false;
    }

    public boolean f() {
        return this.w.get(0);
    }

    public boolean g() {
        return this.g != null;
    }

    public String h() {
        return this.h;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.h != null;
    }

    public boolean j() {
        return this.i != null;
    }

    public String k() {
        return this.j;
    }

    public boolean l() {
        return this.j != null;
    }

    public void m() {
        if (this.c == null) {
            throw new TProtocolException("Required field 'id' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionUnSubscriptionResult(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        if (d()) {
            sb.append(", ");
            sb.append("appId:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("request:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("errorCode:");
            sb.append(this.f);
        }
        if (g()) {
            sb.append(", ");
            sb.append("reason:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("topic:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("category:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
