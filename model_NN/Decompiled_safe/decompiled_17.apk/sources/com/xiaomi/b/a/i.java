package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class i implements Serializable, Cloneable, TBase<i, a> {
    public static final Map<a, FieldMetaData> k;
    private static final TStruct l = new TStruct("XmPushActionRegistration");
    private static final TField m = new TField("debug", (byte) 11, 1);
    private static final TField n = new TField("target", (byte) 12, 2);
    private static final TField o = new TField(LocaleUtil.INDONESIAN, (byte) 11, 3);
    private static final TField p = new TField(ApiParams.KEY_APPID, (byte) 11, 4);
    private static final TField q = new TField("appVersion", (byte) 11, 5);
    private static final TField r = new TField("packageName", (byte) 11, 6);
    private static final TField s = new TField("token", (byte) 11, 7);
    private static final TField t = new TField("deviceId", (byte) 11, 8);
    private static final TField u = new TField("aliasName", (byte) 11, 9);
    private static final TField v = new TField("sdkVersion", (byte) 11, 10);
    public String a;
    public c b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;

    public enum a implements TFieldIdEnum {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, LocaleUtil.INDONESIAN),
        APP_ID(4, ApiParams.KEY_APPID),
        APP_VERSION(5, "appVersion"),
        PACKAGE_NAME(6, "packageName"),
        TOKEN(7, "token"),
        DEVICE_ID(8, "deviceId"),
        ALIAS_NAME(9, "aliasName"),
        SDK_VERSION(10, "sdkVersion");
        
        private static final Map<String, a> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                k.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.i$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new FieldMetaData("debug", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new FieldMetaData("target", (byte) 2, new StructMetaData((byte) 12, c.class)));
        enumMap.put((Object) a.ID, (Object) new FieldMetaData(LocaleUtil.INDONESIAN, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new FieldMetaData(ApiParams.KEY_APPID, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_VERSION, (Object) new FieldMetaData("appVersion", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new FieldMetaData("packageName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TOKEN, (Object) new FieldMetaData("token", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.DEVICE_ID, (Object) new FieldMetaData("deviceId", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.ALIAS_NAME, (Object) new FieldMetaData("aliasName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.SDK_VERSION, (Object) new FieldMetaData("sdkVersion", (byte) 2, new FieldValueMetaData((byte) 11)));
        k = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(i.class, k);
    }

    public i a(String str) {
        this.c = str;
        return this;
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                m();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = new c();
                        this.b.a(tProtocol);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.w();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = tProtocol.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.g = tProtocol.w();
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.h = tProtocol.w();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.i = tProtocol.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.j = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(i iVar) {
        if (iVar != null) {
            boolean a2 = a();
            boolean a3 = iVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(iVar.a))) {
                boolean b2 = b();
                boolean b3 = iVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.a(iVar.b))) {
                    boolean c2 = c();
                    boolean c3 = iVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(iVar.c))) {
                        boolean e2 = e();
                        boolean e3 = iVar.e();
                        if ((!e2 && !e3) || (e2 && e3 && this.d.equals(iVar.d))) {
                            boolean f2 = f();
                            boolean f3 = iVar.f();
                            if ((!f2 && !f3) || (f2 && f3 && this.e.equals(iVar.e))) {
                                boolean g2 = g();
                                boolean g3 = iVar.g();
                                if ((!g2 && !g3) || (g2 && g3 && this.f.equals(iVar.f))) {
                                    boolean i2 = i();
                                    boolean i3 = iVar.i();
                                    if ((!i2 && !i3) || (i2 && i3 && this.g.equals(iVar.g))) {
                                        boolean j2 = j();
                                        boolean j3 = iVar.j();
                                        if ((!j2 && !j3) || (j2 && j3 && this.h.equals(iVar.h))) {
                                            boolean k2 = k();
                                            boolean k3 = iVar.k();
                                            if ((!k2 && !k3) || (k2 && k3 && this.i.equals(iVar.i))) {
                                                boolean l2 = l();
                                                boolean l3 = iVar.l();
                                                return (!l2 && !l3) || (l2 && l3 && this.j.equals(iVar.j));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(i iVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        if (!getClass().equals(iVar.getClass())) {
            return getClass().getName().compareTo(iVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(iVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a11 = TBaseHelper.a(this.a, iVar.a)) != 0) {
            return a11;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(iVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a10 = TBaseHelper.a(this.b, iVar.b)) != 0) {
            return a10;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(iVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a9 = TBaseHelper.a(this.c, iVar.c)) != 0) {
            return a9;
        }
        int compareTo4 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(iVar.e()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (e() && (a8 = TBaseHelper.a(this.d, iVar.d)) != 0) {
            return a8;
        }
        int compareTo5 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(iVar.f()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (f() && (a7 = TBaseHelper.a(this.e, iVar.e)) != 0) {
            return a7;
        }
        int compareTo6 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(iVar.g()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (g() && (a6 = TBaseHelper.a(this.f, iVar.f)) != 0) {
            return a6;
        }
        int compareTo7 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(iVar.i()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (i() && (a5 = TBaseHelper.a(this.g, iVar.g)) != 0) {
            return a5;
        }
        int compareTo8 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(iVar.j()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (j() && (a4 = TBaseHelper.a(this.h, iVar.h)) != 0) {
            return a4;
        }
        int compareTo9 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(iVar.k()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (k() && (a3 = TBaseHelper.a(this.i, iVar.i)) != 0) {
            return a3;
        }
        int compareTo10 = Boolean.valueOf(l()).compareTo(Boolean.valueOf(iVar.l()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (!l() || (a2 = TBaseHelper.a(this.j, iVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public i b(String str) {
        this.d = str;
        return this;
    }

    public void b(TProtocol tProtocol) {
        m();
        tProtocol.a(l);
        if (this.a != null && a()) {
            tProtocol.a(m);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(n);
            this.b.b(tProtocol);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(o);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null) {
            tProtocol.a(p);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null && f()) {
            tProtocol.a(q);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        if (this.f != null && g()) {
            tProtocol.a(r);
            tProtocol.a(this.f);
            tProtocol.b();
        }
        if (this.g != null) {
            tProtocol.a(s);
            tProtocol.a(this.g);
            tProtocol.b();
        }
        if (this.h != null && j()) {
            tProtocol.a(t);
            tProtocol.a(this.h);
            tProtocol.b();
        }
        if (this.i != null && k()) {
            tProtocol.a(u);
            tProtocol.a(this.i);
            tProtocol.b();
        }
        if (this.j != null && l()) {
            tProtocol.a(v);
            tProtocol.a(this.j);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public i c(String str) {
        this.e = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public i d(String str) {
        this.f = str;
        return this;
    }

    public String d() {
        return this.d;
    }

    public i e(String str) {
        this.g = str;
        return this;
    }

    public boolean e() {
        return this.d != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof i)) {
            return a((i) obj);
        }
        return false;
    }

    public boolean f() {
        return this.e != null;
    }

    public boolean g() {
        return this.f != null;
    }

    public String h() {
        return this.g;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.g != null;
    }

    public boolean j() {
        return this.h != null;
    }

    public boolean k() {
        return this.i != null;
    }

    public boolean l() {
        return this.j != null;
    }

    public void m() {
        if (this.c == null) {
            throw new TProtocolException("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new TProtocolException("Required field 'token' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionRegistration(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (f()) {
            sb.append(", ");
            sb.append("appVersion:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("token:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (j()) {
            sb.append(", ");
            sb.append("deviceId:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("aliasName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("sdkVersion:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
