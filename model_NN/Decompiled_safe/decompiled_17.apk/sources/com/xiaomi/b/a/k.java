package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.MapMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TMap;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class k implements Serializable, Cloneable, TBase<k, a> {
    public static final Map<a, FieldMetaData> g;
    private static final TStruct h = new TStruct("XmPushActionSendFeedback");
    private static final TField i = new TField("debug", (byte) 11, 1);
    private static final TField j = new TField("target", (byte) 12, 2);
    private static final TField k = new TField(LocaleUtil.INDONESIAN, (byte) 11, 3);
    private static final TField l = new TField(ApiParams.KEY_APPID, (byte) 11, 4);
    private static final TField m = new TField("feedbacks", (byte) 13, 5);
    private static final TField n = new TField("category", (byte) 11, 6);
    public String a;
    public c b;
    public String c;
    public String d;
    public Map<String, String> e;
    public String f;

    public enum a implements TFieldIdEnum {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, LocaleUtil.INDONESIAN),
        APP_ID(4, ApiParams.KEY_APPID),
        FEEDBACKS(5, "feedbacks"),
        CATEGORY(6, "category");
        
        private static final Map<String, a> g = new HashMap();
        private final short h;
        private final String i;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                g.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.h = s;
            this.i = str;
        }

        public String a() {
            return this.i;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.k$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new FieldMetaData("debug", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new FieldMetaData("target", (byte) 2, new StructMetaData((byte) 12, c.class)));
        enumMap.put((Object) a.ID, (Object) new FieldMetaData(LocaleUtil.INDONESIAN, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new FieldMetaData(ApiParams.KEY_APPID, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.FEEDBACKS, (Object) new FieldMetaData("feedbacks", (byte) 2, new MapMetaData((byte) 13, new FieldValueMetaData((byte) 11), new FieldValueMetaData((byte) 11))));
        enumMap.put((Object) a.CATEGORY, (Object) new FieldMetaData("category", (byte) 2, new FieldValueMetaData((byte) 11)));
        g = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(k.class, g);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                g();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = new c();
                        this.b.a(tProtocol);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 13) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        TMap k2 = tProtocol.k();
                        this.e = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.e.put(tProtocol.w(), tProtocol.w());
                        }
                        tProtocol.l();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(k kVar) {
        if (kVar != null) {
            boolean a2 = a();
            boolean a3 = kVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(kVar.a))) {
                boolean b2 = b();
                boolean b3 = kVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.a(kVar.b))) {
                    boolean c2 = c();
                    boolean c3 = kVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(kVar.c))) {
                        boolean d2 = d();
                        boolean d3 = kVar.d();
                        if ((!d2 && !d3) || (d2 && d3 && this.d.equals(kVar.d))) {
                            boolean e2 = e();
                            boolean e3 = kVar.e();
                            if ((!e2 && !e3) || (e2 && e3 && this.e.equals(kVar.e))) {
                                boolean f2 = f();
                                boolean f3 = kVar.f();
                                return (!f2 && !f3) || (f2 && f3 && this.f.equals(kVar.f));
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(k kVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        if (!getClass().equals(kVar.getClass())) {
            return getClass().getName().compareTo(kVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(kVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a7 = TBaseHelper.a(this.a, kVar.a)) != 0) {
            return a7;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(kVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a6 = TBaseHelper.a(this.b, kVar.b)) != 0) {
            return a6;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(kVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a5 = TBaseHelper.a(this.c, kVar.c)) != 0) {
            return a5;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(kVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a4 = TBaseHelper.a(this.d, kVar.d)) != 0) {
            return a4;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(kVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a3 = TBaseHelper.a(this.e, kVar.e)) != 0) {
            return a3;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(kVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (!f() || (a2 = TBaseHelper.a(this.f, kVar.f)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        g();
        tProtocol.a(h);
        if (this.a != null && a()) {
            tProtocol.a(i);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(j);
            this.b.b(tProtocol);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(k);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null) {
            tProtocol.a(l);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null && e()) {
            tProtocol.a(m);
            tProtocol.a(new TMap((byte) 11, (byte) 11, this.e.size()));
            for (Map.Entry next : this.e.entrySet()) {
                tProtocol.a((String) next.getKey());
                tProtocol.a((String) next.getValue());
            }
            tProtocol.d();
            tProtocol.b();
        }
        if (this.f != null && f()) {
            tProtocol.a(n);
            tProtocol.a(this.f);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof k)) {
            return a((k) obj);
        }
        return false;
    }

    public boolean f() {
        return this.f != null;
    }

    public void g() {
        if (this.c == null) {
            throw new TProtocolException("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'appId' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionSendFeedback(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (e()) {
            sb.append(", ");
            sb.append("feedbacks:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("category:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
