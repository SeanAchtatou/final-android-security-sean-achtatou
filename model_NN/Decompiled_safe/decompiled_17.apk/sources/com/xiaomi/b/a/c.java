package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class c implements Serializable, Cloneable, TBase<c, a> {
    public static final Map<a, FieldMetaData> f;
    private static final TStruct g = new TStruct("Target");
    private static final TField h = new TField("channelId", (byte) 10, 1);
    private static final TField i = new TField(ApiParams.KEY_USERID, (byte) 11, 2);
    private static final TField j = new TField("server", (byte) 11, 3);
    private static final TField k = new TField("resource", (byte) 11, 4);
    private static final TField l = new TField("isPreview", (byte) 2, 5);
    public long a = 5;
    public String b;
    public String c = "xiaomi.com";
    public String d = "";
    public boolean e = false;
    private BitSet m = new BitSet(2);

    public enum a implements TFieldIdEnum {
        CHANNEL_ID(1, "channelId"),
        USER_ID(2, ApiParams.KEY_USERID),
        SERVER(3, "server"),
        RESOURCE(4, "resource"),
        IS_PREVIEW(5, "isPreview");
        
        private static final Map<String, a> f = new HashMap();
        private final short g;
        private final String h;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                f.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.g = s;
            this.h = str;
        }

        public String a() {
            return this.h;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.c$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CHANNEL_ID, (Object) new FieldMetaData("channelId", (byte) 1, new FieldValueMetaData((byte) 10)));
        enumMap.put((Object) a.USER_ID, (Object) new FieldMetaData(ApiParams.KEY_USERID, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.SERVER, (Object) new FieldMetaData("server", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.RESOURCE, (Object) new FieldMetaData("resource", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.IS_PREVIEW, (Object) new FieldMetaData("isPreview", (byte) 2, new FieldValueMetaData((byte) 2)));
        f = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(c.class, f);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                if (!a()) {
                    throw new TProtocolException("Required field 'channelId' was not found in serialized data! Struct: " + toString());
                }
                f();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 10) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.u();
                        a(true);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = tProtocol.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 2) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.q();
                        b(true);
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public void a(boolean z) {
        this.m.set(0, z);
    }

    public boolean a() {
        return this.m.get(0);
    }

    public boolean a(c cVar) {
        if (cVar != null && this.a == cVar.a) {
            boolean b2 = b();
            boolean b3 = cVar.b();
            if ((!b2 && !b3) || (b2 && b3 && this.b.equals(cVar.b))) {
                boolean c2 = c();
                boolean c3 = cVar.c();
                if ((!c2 && !c3) || (c2 && c3 && this.c.equals(cVar.c))) {
                    boolean d2 = d();
                    boolean d3 = cVar.d();
                    if ((!d2 && !d3) || (d2 && d3 && this.d.equals(cVar.d))) {
                        boolean e2 = e();
                        boolean e3 = cVar.e();
                        return (!e2 && !e3) || (e2 && e3 && this.e == cVar.e);
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(c cVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        if (!getClass().equals(cVar.getClass())) {
            return getClass().getName().compareTo(cVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(cVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a6 = TBaseHelper.a(this.a, cVar.a)) != 0) {
            return a6;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(cVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a5 = TBaseHelper.a(this.b, cVar.b)) != 0) {
            return a5;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(cVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a4 = TBaseHelper.a(this.c, cVar.c)) != 0) {
            return a4;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(cVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a3 = TBaseHelper.a(this.d, cVar.d)) != 0) {
            return a3;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(cVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (!e() || (a2 = TBaseHelper.a(this.e, cVar.e)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        f();
        tProtocol.a(g);
        tProtocol.a(h);
        tProtocol.a(this.a);
        tProtocol.b();
        if (this.b != null) {
            tProtocol.a(i);
            tProtocol.a(this.b);
            tProtocol.b();
        }
        if (this.c != null && c()) {
            tProtocol.a(j);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null && d()) {
            tProtocol.a(k);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (e()) {
            tProtocol.a(l);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public void b(boolean z) {
        this.m.set(1, z);
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.m.get(1);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof c)) {
            return a((c) obj);
        }
        return false;
    }

    public void f() {
        if (this.b == null) {
            throw new TProtocolException("Required field 'userId' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Target(");
        sb.append("channelId:");
        sb.append(this.a);
        sb.append(", ");
        sb.append("userId:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        if (c()) {
            sb.append(", ");
            sb.append("server:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("resource:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("isPreview:");
            sb.append(this.e);
        }
        sb.append(")");
        return sb.toString();
    }
}
