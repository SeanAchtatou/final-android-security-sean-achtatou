package com.xiaomi.b.a;

import com.baixing.network.api.ApiParams;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class r implements Serializable, Cloneable, TBase<r, a> {
    public static final Map<a, FieldMetaData> h;
    private static final TStruct i = new TStruct("XmPushActionUnSubscription");
    private static final TField j = new TField("debug", (byte) 11, 1);
    private static final TField k = new TField("target", (byte) 12, 2);
    private static final TField l = new TField(LocaleUtil.INDONESIAN, (byte) 11, 3);
    private static final TField m = new TField(ApiParams.KEY_APPID, (byte) 11, 4);
    private static final TField n = new TField("topic", (byte) 11, 5);
    private static final TField o = new TField("packageName", (byte) 11, 6);
    private static final TField p = new TField("category", (byte) 11, 7);
    public String a;
    public c b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;

    public enum a implements TFieldIdEnum {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, LocaleUtil.INDONESIAN),
        APP_ID(4, ApiParams.KEY_APPID),
        TOPIC(5, "topic"),
        PACKAGE_NAME(6, "packageName"),
        CATEGORY(7, "category");
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.r$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new FieldMetaData("debug", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new FieldMetaData("target", (byte) 2, new StructMetaData((byte) 12, c.class)));
        enumMap.put((Object) a.ID, (Object) new FieldMetaData(LocaleUtil.INDONESIAN, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new FieldMetaData(ApiParams.KEY_APPID, (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.TOPIC, (Object) new FieldMetaData("topic", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new FieldMetaData("packageName", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.CATEGORY, (Object) new FieldMetaData("category", (byte) 2, new FieldValueMetaData((byte) 11)));
        h = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(r.class, h);
    }

    public r a(String str) {
        this.c = str;
        return this;
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                h();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = new c();
                        this.b.a(tProtocol);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.w();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = tProtocol.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.g = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(r rVar) {
        if (rVar != null) {
            boolean a2 = a();
            boolean a3 = rVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(rVar.a))) {
                boolean b2 = b();
                boolean b3 = rVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.a(rVar.b))) {
                    boolean c2 = c();
                    boolean c3 = rVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(rVar.c))) {
                        boolean d2 = d();
                        boolean d3 = rVar.d();
                        if ((!d2 && !d3) || (d2 && d3 && this.d.equals(rVar.d))) {
                            boolean e2 = e();
                            boolean e3 = rVar.e();
                            if ((!e2 && !e3) || (e2 && e3 && this.e.equals(rVar.e))) {
                                boolean f2 = f();
                                boolean f3 = rVar.f();
                                if ((!f2 && !f3) || (f2 && f3 && this.f.equals(rVar.f))) {
                                    boolean g2 = g();
                                    boolean g3 = rVar.g();
                                    return (!g2 && !g3) || (g2 && g3 && this.g.equals(rVar.g));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(r rVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        if (!getClass().equals(rVar.getClass())) {
            return getClass().getName().compareTo(rVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(rVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a8 = TBaseHelper.a(this.a, rVar.a)) != 0) {
            return a8;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(rVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a7 = TBaseHelper.a(this.b, rVar.b)) != 0) {
            return a7;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(rVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a6 = TBaseHelper.a(this.c, rVar.c)) != 0) {
            return a6;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(rVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a5 = TBaseHelper.a(this.d, rVar.d)) != 0) {
            return a5;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(rVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a4 = TBaseHelper.a(this.e, rVar.e)) != 0) {
            return a4;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(rVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a3 = TBaseHelper.a(this.f, rVar.f)) != 0) {
            return a3;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(rVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (!g() || (a2 = TBaseHelper.a(this.g, rVar.g)) == 0) {
            return 0;
        }
        return a2;
    }

    public r b(String str) {
        this.d = str;
        return this;
    }

    public void b(TProtocol tProtocol) {
        h();
        tProtocol.a(i);
        if (this.a != null && a()) {
            tProtocol.a(j);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(k);
            this.b.b(tProtocol);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(l);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null) {
            tProtocol.a(m);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null) {
            tProtocol.a(n);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        if (this.f != null && f()) {
            tProtocol.a(o);
            tProtocol.a(this.f);
            tProtocol.b();
        }
        if (this.g != null && g()) {
            tProtocol.a(p);
            tProtocol.a(this.g);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public r c(String str) {
        this.e = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public r d(String str) {
        this.f = str;
        return this;
    }

    public boolean d() {
        return this.d != null;
    }

    public r e(String str) {
        this.g = str;
        return this;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof r)) {
            return a((r) obj);
        }
        return false;
    }

    public boolean f() {
        return this.f != null;
    }

    public boolean g() {
        return this.g != null;
    }

    public void h() {
        if (this.c == null) {
            throw new TProtocolException("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new TProtocolException("Required field 'topic' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionUnSubscription(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("topic:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        if (f()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("category:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
