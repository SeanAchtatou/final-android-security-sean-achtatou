package com.xiaomi.b.a;

import com.xiaomi.channel.commonutils.logger.b;
import org.apache.thrift.TBase;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;

public class t {
    public static <T extends TBase<T, ?>> void a(T t, byte[] bArr) {
        if (bArr == null) {
            throw new TException("the message byte is empty.");
        }
        new TDeserializer().a(t, bArr);
    }

    public static <T extends TBase<T, ?>> byte[] a(T t) {
        if (t == null) {
            return null;
        }
        try {
            return new TSerializer(new TBinaryProtocol.Factory()).a(t);
        } catch (TException e) {
            b.a("convertThriftObjectToBytes catch TException.", e);
            return null;
        }
    }
}
