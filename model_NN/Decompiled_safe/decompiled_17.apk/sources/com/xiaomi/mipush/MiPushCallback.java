package com.xiaomi.mipush;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.baixing.broadcast.push.PushDispatcher;
import com.baixing.data.GlobalDataManager;
import com.baixing.network.api.ApiError;
import com.baixing.network.api.ApiParams;
import com.baixing.network.api.BaseApiCommand;
import com.baixing.sharing.referral.ReferralNetwork;
import com.baixing.sharing.referral.ReferralPromoter;
import com.baixing.sharing.referral.ReferralUtil;
import com.baixing.util.Util;
import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class MiPushCallback extends MiPushClient.MiPushClientCallback {
    /* access modifiers changed from: private */
    public static final String TAG = MiPushCallback.class.getSimpleName();
    private Context context;
    private PushDispatcher pushDispatcher;

    public MiPushCallback(PushDispatcher dispatcher) {
        this.pushDispatcher = dispatcher;
    }

    public String getCategory() {
        return super.getCategory();
    }

    public void setCategory(String category) {
        super.setCategory(category);
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public void onCommandResult(String command, long resultCode, String reason, List<String> list) {
    }

    public void onInitializeResult(long resultCode, String reason, String regID) {
        if (resultCode == ((long) ErrorCode.SUCCESS)) {
            Log.d(TAG, "regID = " + regID);
            register(regID);
            MiPushService.setAlias(this.context, Util.getDeviceUdid(this.context));
            MiPushService.subscribe(this.context, MiPushService.TOPIC_BROADCAST);
            MiPushService.subscribe(this.context, GlobalDataManager.getInstance().getVersion());
            MiPushService.subscribe(this.context, GlobalDataManager.getInstance().getChannelId());
            return;
        }
        Log.e(TAG, reason);
    }

    public void onReceiveMessage(String content, String topic, String alias) {
        Log.d(TAG, "content = " + content);
        try {
            new JSONObject(content);
            Log.d(TAG, "call dispatcher");
            this.pushDispatcher.dispatch(content);
        } catch (JSONException e) {
            Log.w(TAG, "content is not json data");
        }
    }

    public void onSubscribeResult(long resultCode, String reason, String topic) {
        if (resultCode == ((long) ErrorCode.SUCCESS)) {
            Log.d(TAG, "subscribed topic: " + topic);
        } else {
            Log.e(TAG, reason);
        }
    }

    public void onUnsubscribeResult(long resultCode, String reason, String topic) {
        if (resultCode == ((long) ErrorCode.SUCCESS)) {
            Log.d(TAG, "unsubscribed topic: " + topic);
        } else {
            Log.e(TAG, reason);
        }
    }

    public void register(String regID) {
        Log.d(TAG, "registerDevice: " + regID);
        ApiParams params = new ApiParams();
        params.addParam("appBundleId", this.context.getPackageName());
        params.addParam("appVersion", GlobalDataManager.getInstance().getVersion());
        params.addParam("deviceToken", regID);
        params.addParam("deviceUniqueIdentifier", Util.getDeviceUdid(this.context));
        Log.d(TAG, "params: " + params.toString());
        BaseApiCommand.createCommand("mobile.updateToken/", true, params).execute(this.context, new BaseApiCommand.Callback() {
            public void onNetworkFail(String apiName, ApiError error) {
                Log.e(MiPushCallback.TAG, error == null ? "" : error.toString());
            }

            public void onNetworkDone(String apiName, String responseData) {
                Log.d(MiPushCallback.TAG, "updatetoken succeed " + responseData);
                if (!TextUtils.isEmpty(ReferralPromoter.getInstance().ID())) {
                    ReferralNetwork.getInstance().savePromoTask(ReferralUtil.TASK_APP, ReferralPromoter.getInstance().ID(), null, Util.getDeviceUdid(GlobalDataManager.getInstance().getApplicationContext()), null, null, null);
                }
            }
        });
    }
}
