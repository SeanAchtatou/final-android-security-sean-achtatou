package com.xiaomi.mipush.sdk;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.xiaomi.b.a.a;
import com.xiaomi.b.a.d;
import com.xiaomi.b.a.f;
import com.xiaomi.b.a.g;
import com.xiaomi.b.a.h;
import com.xiaomi.b.a.j;
import com.xiaomi.b.a.m;
import com.xiaomi.b.a.o;
import com.xiaomi.b.a.q;
import com.xiaomi.b.a.s;
import com.xiaomi.b.a.t;
import com.xiaomi.channel.commonutils.c.c;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.apache.thrift.TBase;
import org.apache.thrift.TException;

public class PushMessageHandler extends IntentService {
    private static List<MiPushClient.MiPushClientCallback> a = new ArrayList();
    private Queue<String> b;

    public PushMessageHandler() {
        super("mipush message handler");
    }

    private void a(Context context, a aVar, TBase tBase) {
        if (a.isEmpty()) {
            b.c("receive a message before application calling initialize");
            return;
        }
        b.a("processing a message, action=" + aVar);
        switch (aVar) {
            case Registration:
                j jVar = (j) tBase;
                if (jVar.f == 0) {
                    a.a(context).c(jVar.h, jVar.i);
                }
                a(jVar.f, jVar.g, jVar.h);
                return;
            case UnRegistration:
                if (((q) tBase).f == 0) {
                    a.a(context).f();
                    return;
                }
                return;
            case SendMessage:
                if (a.a(context).j()) {
                    b.a("receive a message in pause state. drop it");
                    return;
                }
                m mVar = (m) tBase;
                com.xiaomi.b.a.b l = mVar.l();
                if (l == null) {
                    b.c("receive an empty message without push content, drop it");
                    return;
                }
                if (a(l.b())) {
                    b.a("drop a duplicate message, msgid=" + l.b());
                } else {
                    a(mVar.p(), l.e(), mVar.j(), mVar.h());
                }
                d dVar = new d();
                dVar.b(mVar.e());
                dVar.a(mVar.c());
                dVar.a(l.g());
                if (!TextUtils.isEmpty(mVar.h())) {
                    dVar.c(mVar.h());
                }
                if (!TextUtils.isEmpty(mVar.j())) {
                    dVar.d(mVar.j());
                }
                c.a(context).a(dVar, a.AckMessage);
                return;
            case Subscription:
                o oVar = (o) tBase;
                a(oVar.k(), oVar.f, oVar.g, oVar.h());
                return;
            case UnSubscription:
                s sVar = (s) tBase;
                b(sVar.k(), sVar.f, sVar.g, sVar.h());
                return;
            case Command:
                f fVar = (f) tBase;
                String e = fVar.e();
                List<String> k = fVar.k();
                a(fVar.m(), e, fVar.g, fVar.h, k);
                if (TextUtils.equals(e, MiPushClient.COMMAND_SET_ACCEPT_TIME)) {
                    if ("00:00-00:00".equals((k == null || k.isEmpty()) ? null : k.get(0))) {
                        a.a(context).a(true);
                        return;
                    } else {
                        a.a(context).a(false);
                        return;
                    }
                } else {
                    return;
                }
            case Notification:
                if ("registration id expired".equalsIgnoreCase(((h) tBase).e)) {
                    MiPushClient.reInitialize(context);
                    return;
                }
                return;
            default:
                return;
        }
    }

    protected static void a(MiPushClient.MiPushClientCallback miPushClientCallback) {
        synchronized (a) {
            if (!a.contains(miPushClientCallback)) {
                a.add(miPushClientCallback);
            }
        }
    }

    private boolean a(String str) {
        SharedPreferences h = a.a(getApplicationContext()).h();
        if (this.b == null) {
            String[] split = h.getString("pref_msg_ids", "").split(",");
            this.b = new LinkedList();
            for (String add : split) {
                this.b.add(add);
            }
        }
        if (this.b.contains(str)) {
            return true;
        }
        this.b.add(str);
        if (this.b.size() > 10) {
            this.b.poll();
        }
        String a2 = c.a(this.b, ",");
        SharedPreferences.Editor edit = h.edit();
        edit.putString("pref_msg_ids", a2);
        edit.commit();
        return false;
    }

    public void a(long j, String str, String str2) {
        synchronized (a) {
            for (MiPushClient.MiPushClientCallback onInitializeResult : a) {
                onInitializeResult.onInitializeResult(j, str, str2);
            }
        }
    }

    public void a(String str, long j, String str2, String str3) {
        synchronized (a) {
            for (MiPushClient.MiPushClientCallback next : a) {
                if (TextUtils.equals(str, next.getCategory())) {
                    next.onSubscribeResult(j, str2, str3);
                }
            }
        }
    }

    public void a(String str, String str2, long j, String str3, List<String> list) {
        synchronized (a) {
            for (MiPushClient.MiPushClientCallback next : a) {
                if (TextUtils.equals(str, next.getCategory())) {
                    next.onCommandResult(str2, j, str3, list);
                }
            }
        }
    }

    public void a(String str, String str2, String str3, String str4) {
        synchronized (a) {
            for (MiPushClient.MiPushClientCallback next : a) {
                if (TextUtils.equals(str, next.getCategory())) {
                    next.onReceiveMessage(str2, str3, str4);
                }
            }
        }
    }

    public void b(String str, long j, String str2, String str3) {
        synchronized (a) {
            for (MiPushClient.MiPushClientCallback next : a) {
                if (TextUtils.equals(str, next.getCategory())) {
                    next.onUnsubscribeResult(j, str2, str3);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        b.a("receive an intent from server, action=" + action);
        if ("com.xiaomi.mipush.RECEIVE_MESSAGE".equals(action)) {
            byte[] byteArrayExtra = intent.getByteArrayExtra("mipush_payload");
            if (byteArrayExtra == null) {
                b.c("receiving an empty message, drop");
                return;
            }
            g gVar = new g();
            try {
                t.a(gVar, byteArrayExtra);
                a a2 = a.a(getApplicationContext());
                if (!a2.g() && gVar.a != a.Registration) {
                    b.b("receive message without registration. need unregister or re-register!");
                } else if (!a2.g() || !a2.k()) {
                    try {
                        TBase a3 = b.a(getApplicationContext(), gVar);
                        if (a3 == null) {
                            b.c("receiving an un-recognized message. " + gVar.a);
                            return;
                        }
                        b.b("receive a message." + a3);
                        a(getApplicationContext(), gVar.a(), a3);
                    } catch (TException e) {
                        b.a(e);
                        b.c("receive a message which action string is not valid. is the reg expired?");
                    }
                } else if (gVar.a == a.UnRegistration) {
                    a2.f();
                } else {
                    MiPushClient.unregisterPush(getApplicationContext());
                }
            } catch (TException e2) {
                b.a(e2);
            }
        } else if ("com.xiaomi.mipush.ERROR".equals(action)) {
            b.c("receive a error message. code = " + intent.getIntExtra("mipush_error_code", 0) + ", msg= " + intent.getStringExtra("mipush_error_msg"));
        }
    }
}
