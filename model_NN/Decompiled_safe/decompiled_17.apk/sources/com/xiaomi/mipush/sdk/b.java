package com.xiaomi.mipush.sdk;

import android.content.Context;
import com.xiaomi.b.a.a;
import com.xiaomi.b.a.c;
import com.xiaomi.b.a.d;
import com.xiaomi.b.a.f;
import com.xiaomi.b.a.g;
import com.xiaomi.b.a.h;
import com.xiaomi.b.a.j;
import com.xiaomi.b.a.l;
import com.xiaomi.b.a.m;
import com.xiaomi.b.a.o;
import com.xiaomi.b.a.q;
import com.xiaomi.b.a.s;
import com.xiaomi.b.a.t;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.thrift.TBase;
import org.apache.thrift.TException;

public class b {
    private static final byte[] a = {100, 23, 84, 114, 72, 0, 4, 97, 73, 97, 2, 52, 84, 102, 18, 32};

    protected static <T extends TBase<T, ?>> g a(Context context, T t, a aVar) {
        byte[] a2 = t.a(t);
        g gVar = new g();
        boolean z = !aVar.equals(a.Registration);
        if (z) {
            byte[] a3 = com.xiaomi.channel.commonutils.c.a.a(a.a(context).e());
            com.xiaomi.channel.commonutils.logger.b.b(Arrays.toString(a3));
            try {
                a2 = b(a3, a2);
            } catch (Exception e) {
                throw new RuntimeException("aes decode error.", e);
            }
        }
        c cVar = new c();
        cVar.a = 5;
        cVar.b = "fakeid";
        gVar.a(cVar);
        gVar.a(ByteBuffer.wrap(a2));
        gVar.a(aVar);
        gVar.c(true);
        gVar.b(context.getPackageName());
        gVar.a(z);
        gVar.a(a.a(context).b());
        return gVar;
    }

    private static Cipher a(byte[] bArr, int i) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(a);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(i, secretKeySpec, ivParameterSpec);
        return instance;
    }

    protected static TBase a(Context context, g gVar) {
        byte[] f;
        if (gVar.c()) {
            try {
                f = a(com.xiaomi.channel.commonutils.c.a.a(a.a(context).e()), gVar.f());
            } catch (Exception e) {
                throw new TException("the aes decrypt failed.", e);
            }
        } else {
            f = gVar.f();
        }
        TBase a2 = a(gVar.a());
        if (a2 != null) {
            t.a(a2, f);
        }
        return a2;
    }

    private static TBase a(a aVar) {
        switch (aVar) {
            case Registration:
                return new j();
            case UnRegistration:
                return new q();
            case Subscription:
                return new o();
            case UnSubscription:
                return new s();
            case SendMessage:
                return new m();
            case AckMessage:
                return new d();
            case SetConfig:
                return new f();
            case ReportFeedback:
                return new l();
            case Notification:
                return new h();
            case Command:
                return new f();
            default:
                return null;
        }
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        return a(bArr, 2).doFinal(bArr2);
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        return a(bArr, 1).doFinal(bArr2);
    }
}
