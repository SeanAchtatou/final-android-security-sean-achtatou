package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.b.a.e;
import com.xiaomi.b.a.i;
import com.xiaomi.b.a.n;
import com.xiaomi.b.a.p;
import com.xiaomi.b.a.r;
import com.xiaomi.channel.commonutils.a.a;
import com.xiaomi.channel.commonutils.c.c;
import com.xiaomi.channel.commonutils.logger.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

public abstract class MiPushClient {
    public static final String COMMAND_SET_ACCEPT_TIME = "accept-time";
    public static final String COMMAND_SET_ALIAS = "set-alias";
    public static final String COMMAND_UNSET_ALIAS = "unset-alias";
    private static long sCurMsgId = System.currentTimeMillis();

    public static abstract class MiPushClientCallback {
        private String category;

        /* access modifiers changed from: protected */
        public String getCategory() {
            return this.category;
        }

        public abstract void onCommandResult(String str, long j, String str2, List<String> list);

        public abstract void onInitializeResult(long j, String str, String str2);

        public abstract void onReceiveMessage(String str, String str2, String str3);

        public abstract void onSubscribeResult(long j, String str, String str2);

        public abstract void onUnsubscribeResult(long j, String str, String str2);

        /* access modifiers changed from: protected */
        public void setCategory(String str) {
            this.category = str;
        }
    }

    static {
        if (a.b || a.e || a.c || a.g) {
            b.a(0);
        }
    }

    private static void checkNotNull(Object obj, String str) {
        if (obj == null) {
            throw new IllegalArgumentException("param " + str + " is not nullable");
        }
    }

    protected static String generatePacketID() {
        String str;
        synchronized (MiPushClient.class) {
            try {
                str = c.a(4) + sCurMsgId;
                sCurMsgId = 1 + sCurMsgId;
            } catch (Throwable th) {
                Class<MiPushClient> cls = MiPushClient.class;
                throw th;
            }
        }
        return str;
    }

    public static void initialize(Context context, String str, String str2, MiPushClientCallback miPushClientCallback) {
        checkNotNull(context, "context");
        checkNotNull(str, "appID");
        checkNotNull(str2, "appToken");
        checkNotNull(miPushClientCallback, "callback");
        PushMessageHandler.a(miPushClientCallback);
        if (!a.a(context).a(str, str2) || a.a(context).k()) {
            a.a(context).f();
            a.a(context).b(str, str2);
            i iVar = new i();
            iVar.a(generatePacketID());
            iVar.b(str);
            iVar.e(str2);
            iVar.d(context.getPackageName());
            iVar.c(a.a(context, context.getPackageName()));
            c.a(context).a(iVar);
            return;
        }
        miPushClientCallback.onInitializeResult(0, null, a.a(context).d());
        c.a(context).a();
    }

    public static void pausePush(Context context, String str) {
        setAcceptTime(context, 0, 0, 0, 0, str);
    }

    static void reInitialize(Context context) {
        if (a.a(context).g()) {
            String b = a.a(context).b();
            String c = a.a(context).c();
            a.a(context).f();
            a.a(context).b(b, c);
            i iVar = new i();
            iVar.a(generatePacketID());
            iVar.b(b);
            iVar.e(c);
            iVar.d(context.getPackageName());
            iVar.c(a.a(context, context.getPackageName()));
            c.a(context).a(iVar);
        }
    }

    public static void resumePush(Context context, String str) {
        setAcceptTime(context, 0, 0, 23, 59, str);
    }

    public static void setAcceptTime(Context context, int i, int i2, int i3, int i4, String str) {
        if (i < 0 || i >= 24 || i3 < 0 || i3 >= 24 || i2 < 0 || i2 >= 60 || i4 < 0 || i4 >= 60) {
            throw new IllegalArgumentException("the input parameter is not valid.");
        }
        long rawOffset = (long) (((TimeZone.getTimeZone("GMT+08").getRawOffset() - TimeZone.getDefault().getRawOffset()) / 1000) / 60);
        long j = (1440 + (((long) ((i * 60) + i2)) + rawOffset)) % 1440;
        long j2 = ((rawOffset + ((long) ((i3 * 60) + i4))) + 1440) % 1440;
        ArrayList arrayList = new ArrayList();
        arrayList.add(String.format("%1$02d:%2$02d", Long.valueOf(j / 60), Long.valueOf(j % 60)));
        arrayList.add(String.format("%1$02d:%2$02d", Long.valueOf(j2 / 60), Long.valueOf(j2 % 60)));
        setCommand(context, COMMAND_SET_ACCEPT_TIME, arrayList, str);
    }

    public static void setAlias(Context context, String str, String str2) {
        setCommand(context, COMMAND_SET_ALIAS, str, str2);
    }

    protected static void setCommand(Context context, String str, String str2, String str3) {
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
        }
        setCommand(context, str, arrayList, str3);
    }

    protected static void setCommand(Context context, String str, ArrayList<String> arrayList, String str2) {
        a.a(context).a();
        e eVar = new e();
        eVar.a(generatePacketID());
        eVar.b(a.a(context).b());
        eVar.c(str);
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            eVar.d(it.next());
        }
        eVar.f(str2);
        eVar.e(context.getPackageName());
        c.a(context).a(eVar, com.xiaomi.b.a.a.Command);
    }

    public static void subscribe(Context context, String str, String str2) {
        a.a(context).a();
        n nVar = new n();
        nVar.a(generatePacketID());
        nVar.b(a.a(context).b());
        nVar.c(str);
        nVar.d(context.getPackageName());
        nVar.e(str2);
        c.a(context).a(nVar, com.xiaomi.b.a.a.Subscription);
    }

    public static void unregisterPush(Context context) {
        a.a(context).a();
        p pVar = new p();
        pVar.a(generatePacketID());
        pVar.b(a.a(context).b());
        pVar.c(a.a(context).d());
        pVar.e(a.a(context).c());
        pVar.d(context.getPackageName());
        c.a(context).a(pVar);
        a.a(context).i();
    }

    public static void unsetAlias(Context context, String str, String str2) {
        setCommand(context, COMMAND_UNSET_ALIAS, str, str2);
    }

    public static void unsubscribe(Context context, String str, String str2) {
        a.a(context).a();
        r rVar = new r();
        rVar.a(generatePacketID());
        rVar.b(a.a(context).b());
        rVar.c(str);
        rVar.d(context.getPackageName());
        rVar.e(str2);
        c.a(context).a(rVar, com.xiaomi.b.a.a.UnSubscription);
    }
}
