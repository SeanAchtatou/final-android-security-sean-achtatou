package com.xiaomi.mipush.sdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.xiaomi.b.a.i;
import com.xiaomi.b.a.p;
import com.xiaomi.b.a.t;
import com.xiaomi.channel.commonutils.a.a;
import com.xiaomi.push.service.XMPushService;
import org.apache.thrift.TBase;

public class c {
    private static c b;
    private boolean a = false;
    private Context c;
    private String d;

    private c(Context context) {
        this.c = context.getApplicationContext();
        this.d = com.xiaomi.channel.commonutils.c.c.a(6);
        this.a = b();
    }

    public static c a(Context context) {
        if (b == null) {
            b = new c(context);
        }
        return b;
    }

    private boolean b() {
        try {
            PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo("com.xiaomi.xmsf", 4);
            return packageInfo != null && packageInfo.versionCode >= 105;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private Intent c() {
        if (!this.a || a.a()) {
            Intent intent = new Intent(this.c, XMPushService.class);
            intent.putExtra("mipush_app_package", this.c.getPackageName());
            return intent;
        }
        Intent intent2 = new Intent();
        intent2.setPackage("com.xiaomi.xmsf");
        intent2.setClassName("com.xiaomi.xmsf", "com.xiaomi.xmsf.push.service.XMPushService");
        intent2.putExtra("mipush_app_package", this.c.getPackageName());
        d();
        return intent2;
    }

    private void d() {
        this.c.getPackageManager().setComponentEnabledSetting(new ComponentName(this.c, XMPushService.class), 2, 1);
    }

    public void a() {
        this.c.startService(c());
    }

    public final void a(i iVar) {
        Intent c2 = c();
        byte[] a2 = t.a(b.a(this.c, iVar, com.xiaomi.b.a.a.Registration));
        c2.setAction("com.xiaomi.mipush.REGISTER_APP");
        c2.putExtra("mipush_app_id", a.a(this.c).b());
        c2.putExtra("mipush_payload", a2);
        c2.putExtra("mipush_session", this.d);
        this.c.startService(c2);
    }

    public final void a(p pVar) {
        Intent c2 = c();
        byte[] a2 = t.a(b.a(this.c, pVar, com.xiaomi.b.a.a.UnRegistration));
        c2.setAction("com.xiaomi.mipush.UNREGISTER_APP");
        c2.putExtra("mipush_app_id", a.a(this.c).b());
        c2.putExtra("mipush_payload", a2);
        this.c.startService(c2);
    }

    public final <T extends TBase<T, ?>> void a(T t, com.xiaomi.b.a.a aVar) {
        Intent c2 = c();
        byte[] a2 = t.a(b.a(this.c, t, aVar));
        c2.setAction("com.xiaomi.mipush.SEND_MESSAGE");
        c2.putExtra("mipush_payload", a2);
        this.c.startService(c2);
    }
}
