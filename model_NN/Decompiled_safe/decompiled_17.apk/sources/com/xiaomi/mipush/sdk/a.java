package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.baixing.network.api.ApiParams;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.g;

public class a {
    private static a a;
    /* access modifiers changed from: private */
    public Context b;
    private C0005a c;

    /* renamed from: com.xiaomi.mipush.sdk.a$a  reason: collision with other inner class name */
    private class C0005a {
        public String a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public boolean g;
        public boolean h;

        private C0005a() {
            this.g = true;
            this.h = false;
        }

        private String d() {
            return a.a(a.this.b, a.this.b.getPackageName());
        }

        public void a(String str, String str2) {
            this.a = str;
            this.b = str2;
            SharedPreferences.Editor edit = a.this.h().edit();
            edit.putString(ApiParams.KEY_APPID, this.a);
            edit.putString("appToken", str2);
            edit.commit();
        }

        public void a(boolean z) {
            this.h = z;
        }

        public boolean a() {
            return c(this.a, this.b);
        }

        public void b() {
            a.this.h().edit().clear().commit();
            this.a = null;
            this.b = null;
            this.c = null;
            this.d = null;
            this.f = null;
            this.e = null;
            this.g = false;
            this.h = false;
        }

        public void b(String str, String str2) {
            this.c = str;
            this.d = str2;
            this.f = g.c(a.this.b);
            this.e = d();
            this.g = true;
            SharedPreferences.Editor edit = a.this.h().edit();
            edit.putString("regId", str);
            edit.putString("regSec", str2);
            edit.putString("devId", this.f);
            edit.putString("vName", d());
            edit.putBoolean("valid", true);
            edit.commit();
        }

        public void c() {
            this.g = false;
            a.this.h().edit().putBoolean("valid", this.g).commit();
        }

        public boolean c(String str, String str2) {
            return TextUtils.equals(this.a, str) && TextUtils.equals(this.b, str2) && !TextUtils.isEmpty(this.c) && !TextUtils.isEmpty(this.d) && TextUtils.equals(this.e, d()) && TextUtils.equals(this.f, g.c(a.this.b));
        }
    }

    private a(Context context) {
        this.b = context;
        l();
    }

    public static a a(Context context) {
        if (a == null) {
            a = new a(context);
        }
        return a;
    }

    public static String a(Context context, String str) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(str, 16384);
        } catch (PackageManager.NameNotFoundException e) {
            b.a(e);
            packageInfo = null;
        }
        return packageInfo != null ? packageInfo.versionName : "1.0";
    }

    private void l() {
        this.c = new C0005a();
        SharedPreferences h = h();
        this.c.a = h.getString(ApiParams.KEY_APPID, null);
        this.c.b = h.getString("appToken", null);
        this.c.c = h.getString("regId", null);
        this.c.d = h.getString("regSec", null);
        this.c.f = h.getString("devId", null);
        if (!TextUtils.isEmpty(this.c.f) && this.c.f.startsWith("a-")) {
            this.c.f = g.c(this.b);
            h.edit().putString("devId", this.c.f).commit();
        }
        this.c.e = h.getString("vName", null);
        this.c.g = h.getBoolean("valid", true);
        this.c.h = h.getBoolean("paused", false);
    }

    public void a() {
        if (!this.c.a()) {
            throw new IllegalStateException("Don't send message before initialization succeeded!");
        }
    }

    public void a(boolean z) {
        this.c.a(z);
        h().edit().putBoolean("paused", z).commit();
    }

    public boolean a(String str, String str2) {
        return this.c.c(str, str2);
    }

    public String b() {
        return this.c.a;
    }

    public void b(String str, String str2) {
        this.c.a(str, str2);
    }

    public String c() {
        return this.c.b;
    }

    public void c(String str, String str2) {
        this.c.b(str, str2);
    }

    public String d() {
        return this.c.c;
    }

    public String e() {
        return this.c.d;
    }

    public void f() {
        this.c.b();
    }

    public boolean g() {
        return this.c.a();
    }

    public SharedPreferences h() {
        return this.b.getSharedPreferences("mipush", 0);
    }

    public void i() {
        this.c.c();
    }

    public boolean j() {
        return this.c.h;
    }

    public boolean k() {
        return !this.c.g;
    }
}
