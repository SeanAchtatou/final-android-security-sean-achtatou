package com.xiaomi.mipush;

import android.content.Context;
import android.util.Log;
import com.xiaomi.channel.commonutils.logger.LoggerInterface;
import com.xiaomi.mipush.sdk.Logger;
import com.xiaomi.mipush.sdk.MiPushClient;

public class MiPushService {
    private static final String MiPush_APP_ID = "1005249";
    private static final String MiPush_APP_TOKEN = "830100583249";
    /* access modifiers changed from: private */
    public static final String TAG = MiPushService.class.getSimpleName();
    public static final String TOPIC_BROADCAST = "topic_all";
    private static Context context;

    static {
        Logger.setLogger(new LoggerInterface() {
            public void setTag(String tag) {
            }

            public void log(String content, Throwable t) {
                Log.d(MiPushService.TAG, content, t);
            }

            public void log(String content) {
                Log.d(MiPushService.TAG, content);
            }
        });
    }

    public static void initialize(Context context2, MiPushCallback callback) {
        Log.d(TAG, "initializing");
        context = context2;
        callback.setCategory(null);
        callback.setContext(context2);
        MiPushClient.initialize(context2, MiPush_APP_ID, MiPush_APP_TOKEN, callback);
    }

    public static void subscribe(Context context2, String topic) {
        if (topic != null) {
            Log.d(TAG, "subscribe topic: " + topic);
            MiPushClient.subscribe(context2, topic, null);
        }
    }

    public static void unsubscribe(Context context2, String topic) {
        if (topic != null) {
            Log.d(TAG, "unsubscribe topic: " + topic);
            MiPushClient.unsubscribe(context2, topic, null);
        }
    }

    public static void setAlias(Context context2, String alias) {
        if (alias != null) {
            Log.d(TAG, "set alias: " + alias);
            MiPushClient.setAlias(context2, alias, null);
        }
    }
}
