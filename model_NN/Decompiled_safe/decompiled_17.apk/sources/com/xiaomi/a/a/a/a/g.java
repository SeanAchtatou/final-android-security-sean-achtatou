package com.xiaomi.a.a.a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.ListMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TList;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class g implements Serializable, Cloneable, TBase<g, a> {
    public static final Map<a, FieldMetaData> c;
    private static final TStruct d = new TStruct("PassportHostInfo");
    private static final TField e = new TField("host", (byte) 11, 1);
    private static final TField f = new TField("land_node_info", (byte) 15, 2);
    public String a;
    public List<h> b;

    public enum a implements TFieldIdEnum {
        HOST(1, "host"),
        LAND_NODE_INFO(2, "land_node_info");
        
        private static final Map<String, a> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                c.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public String a() {
            return this.e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a.g$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.HOST, (Object) new FieldMetaData("host", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.LAND_NODE_INFO, (Object) new FieldMetaData("land_node_info", (byte) 1, new ListMetaData((byte) 15, new StructMetaData((byte) 12, h.class))));
        c = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(g.class, c);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i = tProtocol.i();
            if (i.b == 0) {
                tProtocol.h();
                c();
                return;
            }
            switch (i.c) {
                case 1:
                    if (i.b != 11) {
                        TProtocolUtil.a(tProtocol, i.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i.b != 15) {
                        TProtocolUtil.a(tProtocol, i.b);
                        break;
                    } else {
                        TList m = tProtocol.m();
                        this.b = new ArrayList(m.b);
                        for (int i2 = 0; i2 < m.b; i2++) {
                            h hVar = new h();
                            hVar.a(tProtocol);
                            this.b.add(hVar);
                        }
                        tProtocol.n();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(g gVar) {
        if (gVar != null) {
            boolean a2 = a();
            boolean a3 = gVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(gVar.a))) {
                boolean b2 = b();
                boolean b3 = gVar.b();
                return (!b2 && !b3) || (b2 && b3 && this.b.equals(gVar.b));
            }
        }
    }

    /* renamed from: b */
    public int compareTo(g gVar) {
        int a2;
        int a3;
        if (!getClass().equals(gVar.getClass())) {
            return getClass().getName().compareTo(gVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(gVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a3 = TBaseHelper.a(this.a, gVar.a)) != 0) {
            return a3;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(gVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (!b() || (a2 = TBaseHelper.a(this.b, gVar.b)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        c();
        tProtocol.a(d);
        if (this.a != null) {
            tProtocol.a(e);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null) {
            tProtocol.a(f);
            tProtocol.a(new TList((byte) 12, this.b.size()));
            for (h b2 : this.b) {
                b2.b(tProtocol);
            }
            tProtocol.e();
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public void c() {
        if (this.a == null) {
            throw new TProtocolException("Required field 'host' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new TProtocolException("Required field 'land_node_info' was not present! Struct: " + toString());
        }
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof g)) {
            return a((g) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PassportHostInfo(");
        sb.append("host:");
        if (this.a == null) {
            sb.append("null");
        } else {
            sb.append(this.a);
        }
        sb.append(", ");
        sb.append("land_node_info:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(")");
        return sb.toString();
    }
}
