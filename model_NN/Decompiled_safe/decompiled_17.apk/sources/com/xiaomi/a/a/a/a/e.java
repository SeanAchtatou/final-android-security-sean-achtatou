package com.xiaomi.a.a.a.a;

import com.baixing.network.api.ApiParams;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class e implements Serializable, Cloneable, TBase<e, a> {
    public static final Map<a, FieldMetaData> e;
    private static final TStruct f = new TStruct("Location");
    private static final TField g = new TField("contry", (byte) 11, 1);
    private static final TField h = new TField("province", (byte) 11, 2);
    private static final TField i = new TField(ApiParams.KEY_CITY, (byte) 11, 3);
    private static final TField j = new TField("isp", (byte) 11, 4);
    public String a;
    public String b;
    public String c;
    public String d;

    public enum a implements TFieldIdEnum {
        CONTRY(1, "contry"),
        PROVINCE(2, "province"),
        CITY(3, ApiParams.KEY_CITY),
        ISP(4, "isp");
        
        private static final Map<String, a> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                e.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public String a() {
            return this.g;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a.e$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CONTRY, (Object) new FieldMetaData("contry", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.PROVINCE, (Object) new FieldMetaData("province", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.CITY, (Object) new FieldMetaData(ApiParams.KEY_CITY, (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.ISP, (Object) new FieldMetaData("isp", (byte) 2, new FieldValueMetaData((byte) 11)));
        e = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(e.class, e);
    }

    public e a(String str) {
        this.a = str;
        return this;
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                e();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = tProtocol.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(e eVar) {
        if (eVar != null) {
            boolean a2 = a();
            boolean a3 = eVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(eVar.a))) {
                boolean b2 = b();
                boolean b3 = eVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.equals(eVar.b))) {
                    boolean c2 = c();
                    boolean c3 = eVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(eVar.c))) {
                        boolean d2 = d();
                        boolean d3 = eVar.d();
                        return (!d2 && !d3) || (d2 && d3 && this.d.equals(eVar.d));
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(e eVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        if (!getClass().equals(eVar.getClass())) {
            return getClass().getName().compareTo(eVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(eVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a5 = TBaseHelper.a(this.a, eVar.a)) != 0) {
            return a5;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(eVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a4 = TBaseHelper.a(this.b, eVar.b)) != 0) {
            return a4;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(eVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a3 = TBaseHelper.a(this.c, eVar.c)) != 0) {
            return a3;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(eVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (!d() || (a2 = TBaseHelper.a(this.d, eVar.d)) == 0) {
            return 0;
        }
        return a2;
    }

    public e b(String str) {
        this.b = str;
        return this;
    }

    public void b(TProtocol tProtocol) {
        e();
        tProtocol.a(f);
        if (this.a != null && a()) {
            tProtocol.a(g);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(h);
            tProtocol.a(this.b);
            tProtocol.b();
        }
        if (this.c != null && c()) {
            tProtocol.a(i);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null && d()) {
            tProtocol.a(j);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public e c(String str) {
        this.c = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public e d(String str) {
        this.d = str;
        return this;
    }

    public boolean d() {
        return this.d != null;
    }

    public void e() {
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof e)) {
            return a((e) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("Location(");
        boolean z2 = true;
        if (a()) {
            sb.append("contry:");
            if (this.a == null) {
                sb.append("null");
            } else {
                sb.append(this.a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("province:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
            z2 = false;
        }
        if (c()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("city:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        } else {
            z = z2;
        }
        if (d()) {
            if (!z) {
                sb.append(", ");
            }
            sb.append("isp:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
