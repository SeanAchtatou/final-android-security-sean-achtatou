package com.xiaomi.a.a.a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.ListMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TList;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class a implements Serializable, Cloneable, TBase<a, C0003a> {
    public static final Map<C0003a, FieldMetaData> c;
    private static final TStruct d = new TStruct("HostInfo");
    private static final TField e = new TField("host", (byte) 11, 1);
    private static final TField f = new TField("land_node_info", (byte) 15, 2);
    public String a;
    public List<d> b;

    /* renamed from: com.xiaomi.a.a.a.a.a$a  reason: collision with other inner class name */
    public enum C0003a implements TFieldIdEnum {
        HOST(1, "host"),
        LAND_NODE_INFO(2, "land_node_info");
        
        private static final Map<String, C0003a> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(C0003a.class).iterator();
            while (it.hasNext()) {
                C0003a aVar = (C0003a) it.next();
                c.put(aVar.a(), aVar);
            }
        }

        private C0003a(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public String a() {
            return this.e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a.a$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(C0003a.class);
        enumMap.put((Object) C0003a.HOST, (Object) new FieldMetaData("host", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) C0003a.LAND_NODE_INFO, (Object) new FieldMetaData("land_node_info", (byte) 1, new ListMetaData((byte) 15, new StructMetaData((byte) 12, d.class))));
        c = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(a.class, c);
    }

    public a a(String str) {
        this.a = str;
        return this;
    }

    public a a(List<d> list) {
        this.b = list;
        return this;
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i = tProtocol.i();
            if (i.b == 0) {
                tProtocol.h();
                c();
                return;
            }
            switch (i.c) {
                case 1:
                    if (i.b != 11) {
                        TProtocolUtil.a(tProtocol, i.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i.b != 15) {
                        TProtocolUtil.a(tProtocol, i.b);
                        break;
                    } else {
                        TList m = tProtocol.m();
                        this.b = new ArrayList(m.b);
                        for (int i2 = 0; i2 < m.b; i2++) {
                            d dVar = new d();
                            dVar.a(tProtocol);
                            this.b.add(dVar);
                        }
                        tProtocol.n();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(a aVar) {
        if (aVar != null) {
            boolean a2 = a();
            boolean a3 = aVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(aVar.a))) {
                boolean b2 = b();
                boolean b3 = aVar.b();
                return (!b2 && !b3) || (b2 && b3 && this.b.equals(aVar.b));
            }
        }
    }

    /* renamed from: b */
    public int compareTo(a aVar) {
        int a2;
        int a3;
        if (!getClass().equals(aVar.getClass())) {
            return getClass().getName().compareTo(aVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(aVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a3 = TBaseHelper.a(this.a, aVar.a)) != 0) {
            return a3;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(aVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (!b() || (a2 = TBaseHelper.a(this.b, aVar.b)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        c();
        tProtocol.a(d);
        if (this.a != null) {
            tProtocol.a(e);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null) {
            tProtocol.a(f);
            tProtocol.a(new TList((byte) 12, this.b.size()));
            for (d b2 : this.b) {
                b2.b(tProtocol);
            }
            tProtocol.e();
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public void c() {
        if (this.a == null) {
            throw new TProtocolException("Required field 'host' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new TProtocolException("Required field 'land_node_info' was not present! Struct: " + toString());
        }
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof a)) {
            return a((a) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HostInfo(");
        sb.append("host:");
        if (this.a == null) {
            sb.append("null");
        } else {
            sb.append(this.a);
        }
        sb.append(", ");
        sb.append("land_node_info:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(")");
        return sb.toString();
    }
}
