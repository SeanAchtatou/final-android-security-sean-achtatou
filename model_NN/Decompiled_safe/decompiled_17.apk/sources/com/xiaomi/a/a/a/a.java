package com.xiaomi.a.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class a implements Serializable, Cloneable, TBase<a, C0002a> {
    public static final Map<C0002a, FieldMetaData> f;
    private static final TStruct g = new TStruct("Common");
    private static final TField h = new TField("uuid", (byte) 10, 1);
    private static final TField i = new TField("time", (byte) 11, 2);
    private static final TField j = new TField("clientIp", (byte) 11, 3);
    private static final TField k = new TField("serverIp", (byte) 11, 4);
    private static final TField l = new TField("serverHost", (byte) 11, 5);
    public long a = 0;
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";
    private BitSet m = new BitSet(1);

    /* renamed from: com.xiaomi.a.a.a.a$a  reason: collision with other inner class name */
    public enum C0002a implements TFieldIdEnum {
        UUID(1, "uuid"),
        TIME(2, "time"),
        CLIENT_IP(3, "clientIp"),
        SERVER_IP(4, "serverIp"),
        SERVER_HOST(5, "serverHost");
        
        private static final Map<String, C0002a> f = new HashMap();
        private final short g;
        private final String h;

        static {
            Iterator it = EnumSet.allOf(C0002a.class).iterator();
            while (it.hasNext()) {
                C0002a aVar = (C0002a) it.next();
                f.put(aVar.a(), aVar);
            }
        }

        private C0002a(short s, String str) {
            this.g = s;
            this.h = str;
        }

        public String a() {
            return this.h;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(C0002a.class);
        enumMap.put((Object) C0002a.UUID, (Object) new FieldMetaData("uuid", (byte) 2, new FieldValueMetaData((byte) 10)));
        enumMap.put((Object) C0002a.TIME, (Object) new FieldMetaData("time", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) C0002a.CLIENT_IP, (Object) new FieldMetaData("clientIp", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) C0002a.SERVER_IP, (Object) new FieldMetaData("serverIp", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) C0002a.SERVER_HOST, (Object) new FieldMetaData("serverHost", (byte) 2, new FieldValueMetaData((byte) 11)));
        f = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(a.class, f);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                f();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 10) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.u();
                        a(true);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = tProtocol.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.w();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public void a(boolean z) {
        this.m.set(0, z);
    }

    public boolean a() {
        return this.m.get(0);
    }

    public boolean a(a aVar) {
        if (aVar != null) {
            boolean a2 = a();
            boolean a3 = aVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a == aVar.a)) {
                boolean b2 = b();
                boolean b3 = aVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.equals(aVar.b))) {
                    boolean c2 = c();
                    boolean c3 = aVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(aVar.c))) {
                        boolean d2 = d();
                        boolean d3 = aVar.d();
                        if ((!d2 && !d3) || (d2 && d3 && this.d.equals(aVar.d))) {
                            boolean e2 = e();
                            boolean e3 = aVar.e();
                            return (!e2 && !e3) || (e2 && e3 && this.e.equals(aVar.e));
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(a aVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        if (!getClass().equals(aVar.getClass())) {
            return getClass().getName().compareTo(aVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(aVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a6 = TBaseHelper.a(this.a, aVar.a)) != 0) {
            return a6;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(aVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a5 = TBaseHelper.a(this.b, aVar.b)) != 0) {
            return a5;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(aVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a4 = TBaseHelper.a(this.c, aVar.c)) != 0) {
            return a4;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(aVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a3 = TBaseHelper.a(this.d, aVar.d)) != 0) {
            return a3;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(aVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (!e() || (a2 = TBaseHelper.a(this.e, aVar.e)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        f();
        tProtocol.a(g);
        if (a()) {
            tProtocol.a(h);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null && b()) {
            tProtocol.a(i);
            tProtocol.a(this.b);
            tProtocol.b();
        }
        if (this.c != null && c()) {
            tProtocol.a(j);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null && d()) {
            tProtocol.a(k);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null && e()) {
            tProtocol.a(l);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof a)) {
            return a((a) obj);
        }
        return false;
    }

    public void f() {
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("Common(");
        boolean z2 = true;
        if (a()) {
            sb.append("uuid:");
            sb.append(this.a);
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("time:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
            z2 = false;
        }
        if (c()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("clientIp:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z2 = false;
        }
        if (d()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("serverIp:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        } else {
            z = z2;
        }
        if (e()) {
            if (!z) {
                sb.append(", ");
            }
            sb.append("serverHost:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
