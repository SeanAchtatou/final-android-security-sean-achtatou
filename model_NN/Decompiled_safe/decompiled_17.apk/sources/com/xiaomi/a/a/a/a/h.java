package com.xiaomi.a.a.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class h implements Serializable, Cloneable, TBase<h, a> {
    public static final Map<a, FieldMetaData> d;
    private static final TStruct e = new TStruct("PassportLandNodeInfo");
    private static final TField f = new TField("ip", (byte) 8, 1);
    private static final TField g = new TField("eid", (byte) 8, 2);
    private static final TField h = new TField("rt", (byte) 8, 3);
    public int a;
    public int b;
    public int c;
    private BitSet i = new BitSet(3);

    public enum a implements TFieldIdEnum {
        IP(1, "ip"),
        EID(2, "eid"),
        RT(3, "rt");
        
        private static final Map<String, a> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                d.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a.h$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.IP, (Object) new FieldMetaData("ip", (byte) 1, new FieldValueMetaData((byte) 8)));
        enumMap.put((Object) a.EID, (Object) new FieldMetaData("eid", (byte) 1, new FieldValueMetaData((byte) 8)));
        enumMap.put((Object) a.RT, (Object) new FieldMetaData("rt", (byte) 1, new FieldValueMetaData((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(h.class, d);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                if (!a()) {
                    throw new TProtocolException("Required field 'ip' was not found in serialized data! Struct: " + toString());
                } else if (!b()) {
                    throw new TProtocolException("Required field 'eid' was not found in serialized data! Struct: " + toString());
                } else if (!c()) {
                    throw new TProtocolException("Required field 'rt' was not found in serialized data! Struct: " + toString());
                } else {
                    d();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 8) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.a = tProtocol.t();
                            a(true);
                            break;
                        }
                    case 2:
                        if (i2.b != 8) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.b = tProtocol.t();
                            b(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 8) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.c = tProtocol.t();
                            c(true);
                            break;
                        }
                    default:
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                }
                tProtocol.j();
            }
        }
    }

    public void a(boolean z) {
        this.i.set(0, z);
    }

    public boolean a() {
        return this.i.get(0);
    }

    public boolean a(h hVar) {
        return hVar != null && this.a == hVar.a && this.b == hVar.b && this.c == hVar.c;
    }

    /* renamed from: b */
    public int compareTo(h hVar) {
        int a2;
        int a3;
        int a4;
        if (!getClass().equals(hVar.getClass())) {
            return getClass().getName().compareTo(hVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(hVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a4 = TBaseHelper.a(this.a, hVar.a)) != 0) {
            return a4;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(hVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a3 = TBaseHelper.a(this.b, hVar.b)) != 0) {
            return a3;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(hVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (!c() || (a2 = TBaseHelper.a(this.c, hVar.c)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        d();
        tProtocol.a(e);
        tProtocol.a(f);
        tProtocol.a(this.a);
        tProtocol.b();
        tProtocol.a(g);
        tProtocol.a(this.b);
        tProtocol.b();
        tProtocol.a(h);
        tProtocol.a(this.c);
        tProtocol.b();
        tProtocol.c();
        tProtocol.a();
    }

    public void b(boolean z) {
        this.i.set(1, z);
    }

    public boolean b() {
        return this.i.get(1);
    }

    public void c(boolean z) {
        this.i.set(2, z);
    }

    public boolean c() {
        return this.i.get(2);
    }

    public void d() {
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof h)) {
            return a((h) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        return "PassportLandNodeInfo(" + "ip:" + this.a + ", " + "eid:" + this.b + ", " + "rt:" + this.c + ")";
    }
}
