package com.xiaomi.a.a.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.MapMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TMap;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class d implements Serializable, Cloneable, TBase<d, a> {
    public static final Map<a, FieldMetaData> h;
    private static final TStruct i = new TStruct("LandNodeInfo");
    private static final TField j = new TField("ip", (byte) 11, 1);
    private static final TField k = new TField("failed_count", (byte) 8, 2);
    private static final TField l = new TField("success_count", (byte) 8, 3);
    private static final TField m = new TField("duration", (byte) 10, 4);
    private static final TField n = new TField("size", (byte) 8, 5);
    private static final TField o = new TField("exp_info", (byte) 13, 6);
    private static final TField p = new TField("http_info", (byte) 13, 7);
    public String a;
    public int b;
    public int c;
    public long d;
    public int e;
    public Map<String, Integer> f;
    public Map<Integer, Integer> g;
    private BitSet q = new BitSet(4);

    public enum a implements TFieldIdEnum {
        IP(1, "ip"),
        FAILED_COUNT(2, "failed_count"),
        SUCCESS_COUNT(3, "success_count"),
        DURATION(4, "duration"),
        SIZE(5, "size"),
        EXP_INFO(6, "exp_info"),
        HTTP_INFO(7, "http_info");
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a.d$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.IP, (Object) new FieldMetaData("ip", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.FAILED_COUNT, (Object) new FieldMetaData("failed_count", (byte) 1, new FieldValueMetaData((byte) 8)));
        enumMap.put((Object) a.SUCCESS_COUNT, (Object) new FieldMetaData("success_count", (byte) 1, new FieldValueMetaData((byte) 8)));
        enumMap.put((Object) a.DURATION, (Object) new FieldMetaData("duration", (byte) 1, new FieldValueMetaData((byte) 10)));
        enumMap.put((Object) a.SIZE, (Object) new FieldMetaData("size", (byte) 1, new FieldValueMetaData((byte) 8)));
        enumMap.put((Object) a.EXP_INFO, (Object) new FieldMetaData("exp_info", (byte) 2, new MapMetaData((byte) 13, new FieldValueMetaData((byte) 11), new FieldValueMetaData((byte) 8))));
        enumMap.put((Object) a.HTTP_INFO, (Object) new FieldMetaData("http_info", (byte) 2, new MapMetaData((byte) 13, new FieldValueMetaData((byte) 8), new FieldValueMetaData((byte) 8))));
        h = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(d.class, h);
    }

    public d a(int i2) {
        this.b = i2;
        a(true);
        return this;
    }

    public d a(long j2) {
        this.d = j2;
        c(true);
        return this;
    }

    public d a(String str) {
        this.a = str;
        return this;
    }

    public d a(Map<String, Integer> map) {
        this.f = map;
        return this;
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                if (!b()) {
                    throw new TProtocolException("Required field 'failed_count' was not found in serialized data! Struct: " + toString());
                } else if (!c()) {
                    throw new TProtocolException("Required field 'success_count' was not found in serialized data! Struct: " + toString());
                } else if (!d()) {
                    throw new TProtocolException("Required field 'duration' was not found in serialized data! Struct: " + toString());
                } else if (!e()) {
                    throw new TProtocolException("Required field 'size' was not found in serialized data! Struct: " + toString());
                } else {
                    h();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 11) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.a = tProtocol.w();
                            break;
                        }
                    case 2:
                        if (i2.b != 8) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.b = tProtocol.t();
                            a(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 8) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.c = tProtocol.t();
                            b(true);
                            break;
                        }
                    case 4:
                        if (i2.b != 10) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.d = tProtocol.u();
                            c(true);
                            break;
                        }
                    case 5:
                        if (i2.b != 8) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            this.e = tProtocol.t();
                            d(true);
                            break;
                        }
                    case 6:
                        if (i2.b != 13) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            TMap k2 = tProtocol.k();
                            this.f = new HashMap(k2.c * 2);
                            for (int i3 = 0; i3 < k2.c; i3++) {
                                this.f.put(tProtocol.w(), Integer.valueOf(tProtocol.t()));
                            }
                            tProtocol.l();
                            break;
                        }
                    case 7:
                        if (i2.b != 13) {
                            TProtocolUtil.a(tProtocol, i2.b);
                            break;
                        } else {
                            TMap k3 = tProtocol.k();
                            this.g = new HashMap(k3.c * 2);
                            for (int i4 = 0; i4 < k3.c; i4++) {
                                this.g.put(Integer.valueOf(tProtocol.t()), Integer.valueOf(tProtocol.t()));
                            }
                            tProtocol.l();
                            break;
                        }
                    default:
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                }
                tProtocol.j();
            }
        }
    }

    public void a(boolean z) {
        this.q.set(0, z);
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(d dVar) {
        if (dVar != null) {
            boolean a2 = a();
            boolean a3 = dVar.a();
            if (((!a2 && !a3) || (a2 && a3 && this.a.equals(dVar.a))) && this.b == dVar.b && this.c == dVar.c && this.d == dVar.d && this.e == dVar.e) {
                boolean f2 = f();
                boolean f3 = dVar.f();
                if ((!f2 && !f3) || (f2 && f3 && this.f.equals(dVar.f))) {
                    boolean g2 = g();
                    boolean g3 = dVar.g();
                    return (!g2 && !g3) || (g2 && g3 && this.g.equals(dVar.g));
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(d dVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        if (!getClass().equals(dVar.getClass())) {
            return getClass().getName().compareTo(dVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(dVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a8 = TBaseHelper.a(this.a, dVar.a)) != 0) {
            return a8;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(dVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a7 = TBaseHelper.a(this.b, dVar.b)) != 0) {
            return a7;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(dVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a6 = TBaseHelper.a(this.c, dVar.c)) != 0) {
            return a6;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(dVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a5 = TBaseHelper.a(this.d, dVar.d)) != 0) {
            return a5;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(dVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a4 = TBaseHelper.a(this.e, dVar.e)) != 0) {
            return a4;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(dVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a3 = TBaseHelper.a(this.f, dVar.f)) != 0) {
            return a3;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(dVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (!g() || (a2 = TBaseHelper.a(this.g, dVar.g)) == 0) {
            return 0;
        }
        return a2;
    }

    public d b(int i2) {
        this.c = i2;
        b(true);
        return this;
    }

    public void b(TProtocol tProtocol) {
        h();
        tProtocol.a(i);
        if (this.a != null) {
            tProtocol.a(j);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        tProtocol.a(k);
        tProtocol.a(this.b);
        tProtocol.b();
        tProtocol.a(l);
        tProtocol.a(this.c);
        tProtocol.b();
        tProtocol.a(m);
        tProtocol.a(this.d);
        tProtocol.b();
        tProtocol.a(n);
        tProtocol.a(this.e);
        tProtocol.b();
        if (this.f != null && f()) {
            tProtocol.a(o);
            tProtocol.a(new TMap((byte) 11, (byte) 8, this.f.size()));
            for (Map.Entry next : this.f.entrySet()) {
                tProtocol.a((String) next.getKey());
                tProtocol.a(((Integer) next.getValue()).intValue());
            }
            tProtocol.d();
            tProtocol.b();
        }
        if (this.g != null && g()) {
            tProtocol.a(p);
            tProtocol.a(new TMap((byte) 8, (byte) 8, this.g.size()));
            for (Map.Entry next2 : this.g.entrySet()) {
                tProtocol.a(((Integer) next2.getKey()).intValue());
                tProtocol.a(((Integer) next2.getValue()).intValue());
            }
            tProtocol.d();
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public void b(boolean z) {
        this.q.set(1, z);
    }

    public boolean b() {
        return this.q.get(0);
    }

    public d c(int i2) {
        this.e = i2;
        d(true);
        return this;
    }

    public void c(boolean z) {
        this.q.set(2, z);
    }

    public boolean c() {
        return this.q.get(1);
    }

    public void d(boolean z) {
        this.q.set(3, z);
    }

    public boolean d() {
        return this.q.get(2);
    }

    public boolean e() {
        return this.q.get(3);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof d)) {
            return a((d) obj);
        }
        return false;
    }

    public boolean f() {
        return this.f != null;
    }

    public boolean g() {
        return this.g != null;
    }

    public void h() {
        if (this.a == null) {
            throw new TProtocolException("Required field 'ip' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("LandNodeInfo(");
        sb.append("ip:");
        if (this.a == null) {
            sb.append("null");
        } else {
            sb.append(this.a);
        }
        sb.append(", ");
        sb.append("failed_count:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("success_count:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.d);
        sb.append(", ");
        sb.append("size:");
        sb.append(this.e);
        if (f()) {
            sb.append(", ");
            sb.append("exp_info:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("http_info:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
