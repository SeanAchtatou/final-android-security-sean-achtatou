package com.xiaomi.a.a.a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TStruct;

public class c implements Serializable, Cloneable, TBase<c, a> {
    public static final Map<a, FieldMetaData> e;
    private static final TStruct f = new TStruct("HttpLog");
    private static final TField g = new TField("common", (byte) 12, 1);
    private static final TField h = new TField("category", (byte) 11, 2);
    private static final TField i = new TField("httpApi", (byte) 12, 3);
    private static final TField j = new TField("passport", (byte) 12, 4);
    public com.xiaomi.a.a.a.a a;
    public String b = "";
    public b c;
    public f d;

    public enum a implements TFieldIdEnum {
        COMMON(1, "common"),
        CATEGORY(2, "category"),
        HTTP_API(3, "httpApi"),
        PASSPORT(4, "passport");
        
        private static final Map<String, a> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                e.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public String a() {
            return this.g;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a.c$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.COMMON, (Object) new FieldMetaData("common", (byte) 1, new StructMetaData((byte) 12, com.xiaomi.a.a.a.a.class)));
        enumMap.put((Object) a.CATEGORY, (Object) new FieldMetaData("category", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.HTTP_API, (Object) new FieldMetaData("httpApi", (byte) 2, new StructMetaData((byte) 12, b.class)));
        enumMap.put((Object) a.PASSPORT, (Object) new FieldMetaData("passport", (byte) 2, new StructMetaData((byte) 12, f.class)));
        e = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(c.class, e);
    }

    public c a(b bVar) {
        this.c = bVar;
        return this;
    }

    public c a(com.xiaomi.a.a.a.a aVar) {
        this.a = aVar;
        return this;
    }

    public c a(String str) {
        this.b = str;
        return this;
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                e();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = new com.xiaomi.a.a.a.a();
                        this.a.a(tProtocol);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = tProtocol.w();
                        break;
                    }
                case 3:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = new b();
                        this.c.a(tProtocol);
                        break;
                    }
                case 4:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = new f();
                        this.d.a(tProtocol);
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(c cVar) {
        if (cVar != null) {
            boolean a2 = a();
            boolean a3 = cVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.a(cVar.a))) {
                boolean b2 = b();
                boolean b3 = cVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.equals(cVar.b))) {
                    boolean c2 = c();
                    boolean c3 = cVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.a(cVar.c))) {
                        boolean d2 = d();
                        boolean d3 = cVar.d();
                        return (!d2 && !d3) || (d2 && d3 && this.d.a(cVar.d));
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(c cVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        if (!getClass().equals(cVar.getClass())) {
            return getClass().getName().compareTo(cVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(cVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a5 = TBaseHelper.a(this.a, cVar.a)) != 0) {
            return a5;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(cVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a4 = TBaseHelper.a(this.b, cVar.b)) != 0) {
            return a4;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(cVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a3 = TBaseHelper.a(this.c, cVar.c)) != 0) {
            return a3;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(cVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (!d() || (a2 = TBaseHelper.a(this.d, cVar.d)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(TProtocol tProtocol) {
        e();
        tProtocol.a(f);
        if (this.a != null) {
            tProtocol.a(g);
            this.a.b(tProtocol);
            tProtocol.b();
        }
        if (this.b != null) {
            tProtocol.a(h);
            tProtocol.a(this.b);
            tProtocol.b();
        }
        if (this.c != null && c()) {
            tProtocol.a(i);
            this.c.b(tProtocol);
            tProtocol.b();
        }
        if (this.d != null && d()) {
            tProtocol.a(j);
            this.d.b(tProtocol);
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public void e() {
        if (this.a == null) {
            throw new TProtocolException("Required field 'common' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new TProtocolException("Required field 'category' was not present! Struct: " + toString());
        }
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof c)) {
            return a((c) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HttpLog(");
        sb.append("common:");
        if (this.a == null) {
            sb.append("null");
        } else {
            sb.append(this.a);
        }
        sb.append(", ");
        sb.append("category:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        if (c()) {
            sb.append(", ");
            sb.append("httpApi:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("passport:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
