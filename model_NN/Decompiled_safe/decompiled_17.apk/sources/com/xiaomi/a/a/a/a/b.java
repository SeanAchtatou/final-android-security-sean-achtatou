package com.xiaomi.a.a.a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.thrift.TBase;
import org.apache.thrift.TBaseHelper;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.meta_data.FieldMetaData;
import org.apache.thrift.meta_data.FieldValueMetaData;
import org.apache.thrift.meta_data.SetMetaData;
import org.apache.thrift.meta_data.StructMetaData;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TSet;
import org.apache.thrift.protocol.TStruct;

public class b implements Serializable, Cloneable, TBase<b, a> {
    public static final Map<a, FieldMetaData> h;
    private static final TStruct i = new TStruct("HttpApi");
    private static final TField j = new TField("category", (byte) 11, 1);
    private static final TField k = new TField("uuid", (byte) 11, 2);
    private static final TField l = new TField("version", (byte) 11, 3);
    private static final TField m = new TField("network", (byte) 11, 4);
    private static final TField n = new TField("client_ip", (byte) 11, 5);
    private static final TField o = new TField("location", (byte) 12, 6);
    private static final TField p = new TField("host_info", (byte) 14, 7);
    public String a = "";
    public String b;
    public String c;
    public String d;
    public String e;
    public e f;
    public Set<a> g;

    public enum a implements TFieldIdEnum {
        CATEGORY(1, "category"),
        UUID(2, "uuid"),
        VERSION(3, "version"),
        NETWORK(4, "network"),
        CLIENT_IP(5, "client_ip"),
        LOCATION(6, "location"),
        HOST_INFO(7, "host_info");
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.a.a.a.a.b$a, org.apache.thrift.meta_data.FieldMetaData]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CATEGORY, (Object) new FieldMetaData("category", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.UUID, (Object) new FieldMetaData("uuid", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.VERSION, (Object) new FieldMetaData("version", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.NETWORK, (Object) new FieldMetaData("network", (byte) 1, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.CLIENT_IP, (Object) new FieldMetaData("client_ip", (byte) 2, new FieldValueMetaData((byte) 11)));
        enumMap.put((Object) a.LOCATION, (Object) new FieldMetaData("location", (byte) 2, new StructMetaData((byte) 12, e.class)));
        enumMap.put((Object) a.HOST_INFO, (Object) new FieldMetaData("host_info", (byte) 2, new SetMetaData((byte) 14, new StructMetaData((byte) 12, a.class))));
        h = Collections.unmodifiableMap(enumMap);
        FieldMetaData.a(b.class, h);
    }

    public b a(e eVar) {
        this.f = eVar;
        return this;
    }

    public b a(String str) {
        this.a = str;
        return this;
    }

    public void a(a aVar) {
        if (this.g == null) {
            this.g = new HashSet();
        }
        this.g.add(aVar);
    }

    public void a(TProtocol tProtocol) {
        tProtocol.g();
        while (true) {
            TField i2 = tProtocol.i();
            if (i2.b == 0) {
                tProtocol.h();
                i();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.a = tProtocol.w();
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.b = tProtocol.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.c = tProtocol.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.d = tProtocol.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.e = tProtocol.w();
                        break;
                    }
                case 6:
                    if (i2.b != 12) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        this.f = new e();
                        this.f.a(tProtocol);
                        break;
                    }
                case 7:
                    if (i2.b != 14) {
                        TProtocolUtil.a(tProtocol, i2.b);
                        break;
                    } else {
                        TSet o2 = tProtocol.o();
                        this.g = new HashSet(o2.b * 2);
                        for (int i3 = 0; i3 < o2.b; i3++) {
                            a aVar = new a();
                            aVar.a(tProtocol);
                            this.g.add(aVar);
                        }
                        tProtocol.p();
                        break;
                    }
                default:
                    TProtocolUtil.a(tProtocol, i2.b);
                    break;
            }
            tProtocol.j();
        }
    }

    public boolean a() {
        return this.a != null;
    }

    public boolean a(b bVar) {
        if (bVar != null) {
            boolean a2 = a();
            boolean a3 = bVar.a();
            if ((!a2 && !a3) || (a2 && a3 && this.a.equals(bVar.a))) {
                boolean b2 = b();
                boolean b3 = bVar.b();
                if ((!b2 && !b3) || (b2 && b3 && this.b.equals(bVar.b))) {
                    boolean c2 = c();
                    boolean c3 = bVar.c();
                    if ((!c2 && !c3) || (c2 && c3 && this.c.equals(bVar.c))) {
                        boolean d2 = d();
                        boolean d3 = bVar.d();
                        if ((!d2 && !d3) || (d2 && d3 && this.d.equals(bVar.d))) {
                            boolean e2 = e();
                            boolean e3 = bVar.e();
                            if ((!e2 && !e3) || (e2 && e3 && this.e.equals(bVar.e))) {
                                boolean f2 = f();
                                boolean f3 = bVar.f();
                                if ((!f2 && !f3) || (f2 && f3 && this.f.a(bVar.f))) {
                                    boolean h2 = h();
                                    boolean h3 = bVar.h();
                                    return (!h2 && !h3) || (h2 && h3 && this.g.equals(bVar.g));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public int compareTo(b bVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        if (!getClass().equals(bVar.getClass())) {
            return getClass().getName().compareTo(bVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(bVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a8 = TBaseHelper.a(this.a, bVar.a)) != 0) {
            return a8;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(bVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a7 = TBaseHelper.a(this.b, bVar.b)) != 0) {
            return a7;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(bVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a6 = TBaseHelper.a(this.c, bVar.c)) != 0) {
            return a6;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(bVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a5 = TBaseHelper.a(this.d, bVar.d)) != 0) {
            return a5;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(bVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a4 = TBaseHelper.a(this.e, bVar.e)) != 0) {
            return a4;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(bVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a3 = TBaseHelper.a(this.f, bVar.f)) != 0) {
            return a3;
        }
        int compareTo7 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(bVar.h()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (!h() || (a2 = TBaseHelper.a(this.g, bVar.g)) == 0) {
            return 0;
        }
        return a2;
    }

    public b b(String str) {
        this.b = str;
        return this;
    }

    public void b(TProtocol tProtocol) {
        i();
        tProtocol.a(i);
        if (this.a != null) {
            tProtocol.a(j);
            tProtocol.a(this.a);
            tProtocol.b();
        }
        if (this.b != null) {
            tProtocol.a(k);
            tProtocol.a(this.b);
            tProtocol.b();
        }
        if (this.c != null) {
            tProtocol.a(l);
            tProtocol.a(this.c);
            tProtocol.b();
        }
        if (this.d != null) {
            tProtocol.a(m);
            tProtocol.a(this.d);
            tProtocol.b();
        }
        if (this.e != null && e()) {
            tProtocol.a(n);
            tProtocol.a(this.e);
            tProtocol.b();
        }
        if (this.f != null && f()) {
            tProtocol.a(o);
            this.f.b(tProtocol);
            tProtocol.b();
        }
        if (this.g != null && h()) {
            tProtocol.a(p);
            tProtocol.a(new TSet((byte) 12, this.g.size()));
            for (a b2 : this.g) {
                b2.b(tProtocol);
            }
            tProtocol.f();
            tProtocol.b();
        }
        tProtocol.c();
        tProtocol.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public b c(String str) {
        this.c = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public b d(String str) {
        this.d = str;
        return this;
    }

    public boolean d() {
        return this.d != null;
    }

    public b e(String str) {
        this.e = str;
        return this;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof b)) {
            return a((b) obj);
        }
        return false;
    }

    public boolean f() {
        return this.f != null;
    }

    public int g() {
        if (this.g == null) {
            return 0;
        }
        return this.g.size();
    }

    public boolean h() {
        return this.g != null;
    }

    public int hashCode() {
        return 0;
    }

    public void i() {
        if (this.a == null) {
            throw new TProtocolException("Required field 'category' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new TProtocolException("Required field 'uuid' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new TProtocolException("Required field 'version' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new TProtocolException("Required field 'network' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HttpApi(");
        sb.append("category:");
        if (this.a == null) {
            sb.append("null");
        } else {
            sb.append(this.a);
        }
        sb.append(", ");
        sb.append("uuid:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(", ");
        sb.append("version:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("network:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (e()) {
            sb.append(", ");
            sb.append("client_ip:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("location:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("host_info:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
