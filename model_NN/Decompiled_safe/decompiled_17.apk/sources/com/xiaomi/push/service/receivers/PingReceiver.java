package com.xiaomi.push.service.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.o;

public class PingReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        b.b(intent.getPackage() + " is the package name");
        if (!o.k.equals(intent.getAction())) {
            b.a("cancel the old ping timer");
            ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, new Intent(context, PingReceiver.class), 0));
        } else if (TextUtils.equals(context.getPackageName(), intent.getPackage())) {
            b.b("Ping XMChannelService on timer");
            Intent intent2 = new Intent(context, XMPushService.class);
            intent2.setAction("com.xiaomi.push.timer");
            context.startService(intent2);
        }
    }
}
