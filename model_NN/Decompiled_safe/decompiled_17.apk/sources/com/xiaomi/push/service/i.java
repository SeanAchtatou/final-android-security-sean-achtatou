package com.xiaomi.push.service;

import android.content.Context;
import android.content.Intent;
import java.util.HashMap;
import java.util.Map;

public class i {
    private static final Map<String, byte[]> a = new HashMap();

    public static Map<String, byte[]> a() {
        return a;
    }

    public static void a(Context context, int i, String str) {
        for (String next : a.keySet()) {
            a(context, next, a.get(next), i, str);
        }
        a.clear();
    }

    public static void a(Context context, String str, byte[] bArr, int i, String str2) {
        Intent intent = new Intent("com.xiaomi.mipush.ERROR");
        intent.setPackage(str);
        intent.putExtra("mipush_payload", bArr);
        intent.putExtra("mipush_error_code", i);
        intent.putExtra("mipush_error_msg", str2);
        context.sendBroadcast(intent, b.a(str));
    }

    public static void a(String str, byte[] bArr) {
        a.put(str, bArr);
    }
}
