package com.xiaomi.push.service;

import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.push.service.n;
import java.util.Map;
import org.jivesoftware.smack.XMPPException;

class x implements n.b.a {
    final /* synthetic */ XMPushService a;

    x(XMPushService xMPushService) {
        this.a = xMPushService;
    }

    public void a(n.c cVar, n.c cVar2, int i) {
        if (cVar2 == n.c.binded) {
            Map<String, byte[]> a2 = i.a();
            for (String next : a2.keySet()) {
                try {
                    this.a.a(next, a2.get(next));
                } catch (XMPPException e) {
                    this.a.a(10, e);
                }
            }
        } else if (cVar2 == n.c.unbind) {
            i.a(this.a, ErrorCode.ERROR_SERVICE_UNAVAILABLE, " the push is not connected.");
        }
    }
}
