package com.xiaomi.push.service;

import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.XMPushService;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Packet;

public class r extends XMPushService.d {
    private XMPushService a = null;
    private Packet b;

    public r(XMPushService xMPushService, Packet packet) {
        super(4);
        this.a = xMPushService;
        this.b = packet;
    }

    public void a() {
        try {
            this.a.a(this.b);
        } catch (XMPPException e) {
            this.a.a(10, e);
            b.a(e);
        }
    }

    public String b() {
        return "send a message.";
    }
}
