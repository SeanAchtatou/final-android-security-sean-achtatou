package com.xiaomi.push.service;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class l {
    private static final Pattern a = Pattern.compile("([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})");
    private static ThreadPoolExecutor b = new ThreadPoolExecutor(1, 1, 20, TimeUnit.SECONDS, new LinkedBlockingQueue());

    public static void a() {
        if (b.getActiveCount() <= 0) {
            b.execute(new m());
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x0076=Splitter:B:24:0x0076, B:16:0x0068=Splitter:B:16:0x0068} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(java.lang.String r5) {
        /*
            r2 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Network Checkup: Begin to ping "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            com.xiaomi.channel.commonutils.logger.b.a(r0)
            java.lang.String r0 = "ping -W 500 -i 0.2 -c 3 %s"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ IOException -> 0x0066, InterruptedException -> 0x0074, all -> 0x0082 }
            r3 = 0
            r1[r3] = r5     // Catch:{ IOException -> 0x0066, InterruptedException -> 0x0074, all -> 0x0082 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ IOException -> 0x0066, InterruptedException -> 0x0074, all -> 0x0082 }
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0066, InterruptedException -> 0x0074, all -> 0x0082 }
            java.lang.Process r1 = r1.exec(r0)     // Catch:{ IOException -> 0x0066, InterruptedException -> 0x0074, all -> 0x0082 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x009e, InterruptedException -> 0x00a0 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x009e, InterruptedException -> 0x00a0 }
            java.io.InputStream r4 = r1.getInputStream()     // Catch:{ IOException -> 0x009e, InterruptedException -> 0x00a0 }
            r0.<init>(r4)     // Catch:{ IOException -> 0x009e, InterruptedException -> 0x00a0 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x009e, InterruptedException -> 0x00a0 }
            java.lang.String r0 = r3.readLine()     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
        L_0x003d:
            if (r0 == 0) goto L_0x005a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            r2.<init>()     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            java.lang.String r4 = "Network Checkup:"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            com.xiaomi.channel.commonutils.logger.b.a(r0)     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            java.lang.String r0 = r3.readLine()     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            goto L_0x003d
        L_0x005a:
            r1.waitFor()     // Catch:{ IOException -> 0x0093, InterruptedException -> 0x0090, all -> 0x008d }
            r3.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.destroy()
        L_0x0065:
            return
        L_0x0066:
            r0 = move-exception
            r1 = r2
        L_0x0068:
            com.xiaomi.channel.commonutils.logger.b.a(r0)     // Catch:{ all -> 0x00a2 }
            r2.close()     // Catch:{ IOException -> 0x0098 }
        L_0x006e:
            if (r1 == 0) goto L_0x0065
            r1.destroy()
            goto L_0x0065
        L_0x0074:
            r0 = move-exception
            r1 = r2
        L_0x0076:
            com.xiaomi.channel.commonutils.logger.b.a(r0)     // Catch:{ all -> 0x00a2 }
            r2.close()     // Catch:{ IOException -> 0x009a }
        L_0x007c:
            if (r1 == 0) goto L_0x0065
            r1.destroy()
            goto L_0x0065
        L_0x0082:
            r0 = move-exception
            r1 = r2
        L_0x0084:
            r2.close()     // Catch:{ IOException -> 0x009c }
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.destroy()
        L_0x008c:
            throw r0
        L_0x008d:
            r0 = move-exception
            r2 = r3
            goto L_0x0084
        L_0x0090:
            r0 = move-exception
            r2 = r3
            goto L_0x0076
        L_0x0093:
            r0 = move-exception
            r2 = r3
            goto L_0x0068
        L_0x0096:
            r0 = move-exception
            goto L_0x0060
        L_0x0098:
            r0 = move-exception
            goto L_0x006e
        L_0x009a:
            r0 = move-exception
            goto L_0x007c
        L_0x009c:
            r2 = move-exception
            goto L_0x0087
        L_0x009e:
            r0 = move-exception
            goto L_0x0068
        L_0x00a0:
            r0 = move-exception
            goto L_0x0076
        L_0x00a2:
            r0 = move-exception
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.push.service.l.b(java.lang.String):void");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:41:0x0076 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:32:0x0064 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:22:0x0052 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:75:0x0044 */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v14 */
    /* JADX WARN: Type inference failed for: r0v16 */
    /* JADX WARN: Type inference failed for: r0v19 */
    /* JADX WARN: Type inference failed for: r0v21 */
    /* JADX WARN: Type inference failed for: r0v24 */
    /* JADX WARN: Type inference failed for: r0v25 */
    /* JADX WARN: Type inference failed for: r5v0, types: [java.lang.String[]] */
    /* JADX WARN: Type inference failed for: r0v27 */
    /* JADX WARN: Type inference failed for: r0v29 */
    /* JADX WARN: Type inference failed for: r0v31 */
    /* JADX WARN: Type inference failed for: r0v32, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r0v33 */
    /* JADX WARN: Type inference failed for: r0v34 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0066, code lost:
        r2.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0078, code lost:
        r2.destroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0089, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x008a, code lost:
        r1 = r2;
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0093, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0094, code lost:
        r8 = r0;
        r0 = 0;
        r1 = r2;
        r2 = r3;
        r3 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00a6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00a7, code lost:
        r8 = r0;
        r0 = 0;
        r1 = r2;
        r2 = r3;
        r3 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0061 A[SYNTHETIC, Splitter:B:30:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0073 A[SYNTHETIC, Splitter:B:39:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0080 A[SYNTHETIC, Splitter:B:46:0x0080] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0089 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:5:0x0019] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:27:0x005c=Splitter:B:27:0x005c, B:36:0x006e=Splitter:B:36:0x006e} */
    /* JADX WARNING: Unknown variable types count: 5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c() {
        /*
            r1 = 0
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0058, InterruptedException -> 0x006a, all -> 0x007c }
            java.lang.String r2 = "ip route"
            java.lang.Process r3 = r0.exec(r2)     // Catch:{ IOException -> 0x0058, InterruptedException -> 0x006a, all -> 0x007c }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00a1, InterruptedException -> 0x008e, all -> 0x00c2 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x00a1, InterruptedException -> 0x008e, all -> 0x00c2 }
            java.io.InputStream r4 = r3.getInputStream()     // Catch:{ IOException -> 0x00a1, InterruptedException -> 0x008e, all -> 0x00c2 }
            r0.<init>(r4)     // Catch:{ IOException -> 0x00a1, InterruptedException -> 0x008e, all -> 0x00c2 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00a1, InterruptedException -> 0x008e, all -> 0x00c2 }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x00a6, InterruptedException -> 0x0093, all -> 0x0089 }
            boolean r4 = android.text.TextUtils.isEmpty(r0)     // Catch:{ IOException -> 0x00a6, InterruptedException -> 0x0093, all -> 0x0089 }
            if (r4 != 0) goto L_0x00b6
            java.lang.String r4 = "default via"
            boolean r4 = r0.startsWith(r4)     // Catch:{ IOException -> 0x00a6, InterruptedException -> 0x0093, all -> 0x0089 }
            if (r4 == 0) goto L_0x00b6
            java.lang.String r4 = " "
            java.lang.String[] r5 = r0.split(r4)     // Catch:{ IOException -> 0x00a6, InterruptedException -> 0x0093, all -> 0x0089 }
            int r6 = r5.length     // Catch:{ IOException -> 0x00a6, InterruptedException -> 0x0093, all -> 0x0089 }
            r0 = 0
            r4 = r0
        L_0x0034:
            if (r4 >= r6) goto L_0x0049
            r0 = r5[r4]     // Catch:{ IOException -> 0x00ad, InterruptedException -> 0x009a, all -> 0x0089 }
            java.util.regex.Pattern r7 = com.xiaomi.push.service.l.a     // Catch:{ IOException -> 0x00ad, InterruptedException -> 0x009a, all -> 0x0089 }
            java.util.regex.Matcher r7 = r7.matcher(r0)     // Catch:{ IOException -> 0x00ad, InterruptedException -> 0x009a, all -> 0x0089 }
            boolean r7 = r7.matches()     // Catch:{ IOException -> 0x00ad, InterruptedException -> 0x009a, all -> 0x0089 }
            if (r7 == 0) goto L_0x00b4
        L_0x0044:
            int r1 = r4 + 1
            r4 = r1
            r1 = r0
            goto L_0x0034
        L_0x0049:
            r3.waitFor()     // Catch:{ IOException -> 0x00ad, InterruptedException -> 0x009a, all -> 0x0089 }
            r0 = r1
        L_0x004d:
            if (r2 == 0) goto L_0x0052
            r2.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x0052:
            if (r3 == 0) goto L_0x0057
            r3.destroy()
        L_0x0057:
            return r0
        L_0x0058:
            r0 = move-exception
            r3 = r0
            r2 = r1
            r0 = r1
        L_0x005c:
            com.xiaomi.channel.commonutils.logger.b.a(r3)     // Catch:{ all -> 0x00c0 }
            if (r1 == 0) goto L_0x0064
            r1.close()     // Catch:{ IOException -> 0x00ba }
        L_0x0064:
            if (r2 == 0) goto L_0x0057
            r2.destroy()
            goto L_0x0057
        L_0x006a:
            r0 = move-exception
            r3 = r0
            r2 = r1
            r0 = r1
        L_0x006e:
            com.xiaomi.channel.commonutils.logger.b.a(r3)     // Catch:{ all -> 0x00c0 }
            if (r1 == 0) goto L_0x0076
            r1.close()     // Catch:{ IOException -> 0x00bc }
        L_0x0076:
            if (r2 == 0) goto L_0x0057
            r2.destroy()
            goto L_0x0057
        L_0x007c:
            r0 = move-exception
            r3 = r1
        L_0x007e:
            if (r1 == 0) goto L_0x0083
            r1.close()     // Catch:{ IOException -> 0x00be }
        L_0x0083:
            if (r3 == 0) goto L_0x0088
            r3.destroy()
        L_0x0088:
            throw r0
        L_0x0089:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x008c:
            r3 = r2
            goto L_0x007e
        L_0x008e:
            r0 = move-exception
            r2 = r3
            r3 = r0
            r0 = r1
            goto L_0x006e
        L_0x0093:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r2
            r2 = r3
            r3 = r8
            goto L_0x006e
        L_0x009a:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r2
            r2 = r3
            r3 = r8
            goto L_0x006e
        L_0x00a1:
            r0 = move-exception
            r2 = r3
            r3 = r0
            r0 = r1
            goto L_0x005c
        L_0x00a6:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r2
            r2 = r3
            r3 = r8
            goto L_0x005c
        L_0x00ad:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r2
            r2 = r3
            r3 = r8
            goto L_0x005c
        L_0x00b4:
            r0 = r1
            goto L_0x0044
        L_0x00b6:
            r0 = r1
            goto L_0x004d
        L_0x00b8:
            r1 = move-exception
            goto L_0x0052
        L_0x00ba:
            r1 = move-exception
            goto L_0x0064
        L_0x00bc:
            r1 = move-exception
            goto L_0x0076
        L_0x00be:
            r1 = move-exception
            goto L_0x0083
        L_0x00c0:
            r0 = move-exception
            goto L_0x008c
        L_0x00c2:
            r0 = move-exception
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.push.service.l.c():java.lang.String");
    }
}
