package com.xiaomi.push.service;

import android.content.Context;
import com.tencent.tauth.Constants;
import com.xiaomi.push.service.n;

public class f {
    public final String a;
    protected final String b;
    protected final String c;
    protected final String d;
    protected final String e;
    protected final String f;

    public f(String str, String str2, String str3, String str4, String str5, String str6) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
    }

    private static boolean a(Context context) {
        return context.getPackageName().equals("com.xiaomi.xmsf");
    }

    public n.b a(XMPushService xMPushService) {
        n.b bVar = new n.b();
        bVar.a = xMPushService.getPackageName();
        bVar.b = this.a;
        bVar.i = this.c;
        bVar.c = this.b;
        bVar.h = "5";
        bVar.d = "XMPUSH-PASS";
        bVar.e = false;
        bVar.f = "sdk_ver:1";
        bVar.g = String.format("%1$s:%2$s,%3$s:%4$s", "dev_id", g.b(xMPushService), Constants.PARAM_APP_ID, a(xMPushService) ? "1000271" : this.d);
        bVar.k = xMPushService.c();
        return bVar;
    }
}
