package com.xiaomi.push.service;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Pair;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.XMPushService;
import java.util.ArrayList;
import java.util.List;

public class d extends HandlerThread {
    /* access modifiers changed from: private */
    public volatile long a = 0;
    /* access modifiers changed from: private */
    public volatile boolean b = false;
    private volatile Handler c;
    private List<Pair<XMPushService.d, Long>> d = new ArrayList();

    public d(String str) {
        super(str);
    }

    public void a(int i) {
        if (this.c != null) {
            this.c.removeMessages(i);
        }
    }

    public void a(XMPushService.d dVar, long j) {
        if (this.c != null) {
            Message obtain = Message.obtain();
            obtain.what = dVar.d;
            obtain.obj = dVar;
            this.c.sendMessageDelayed(obtain, j);
            return;
        }
        b.a("the job is pended, the controller is not ready.");
        synchronized (this.d) {
            this.d.add(new Pair(dVar, Long.valueOf(j)));
        }
    }

    public boolean a() {
        return this.b && System.currentTimeMillis() - this.a > 600000;
    }

    public boolean b(int i) {
        if (this.c != null) {
            return this.c.hasMessages(i);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLooperPrepared() {
        this.c = new e(this, getLooper());
        synchronized (this.d) {
            for (Pair next : this.d) {
                b.a("executing the pending job.");
                a((XMPushService.d) next.first, ((Long) next.second).longValue());
            }
            this.d.clear();
        }
    }
}
