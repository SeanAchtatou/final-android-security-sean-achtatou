package com.xiaomi.push.service;

import android.app.Service;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.TextUtils;
import com.kenai.jbosh.BOSHClient;
import com.xiaomi.b.a.t;
import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.network.Fallback;
import com.xiaomi.network.HostManager;
import com.xiaomi.push.service.n;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.thrift.TException;
import org.jivesoftware.smack.BOSHConfiguration;
import org.jivesoftware.smack.BOSHConnection;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.CommonPacketExtension;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.StringUtils;

public class XMPushService extends Service implements ConnectionListener {
    public static int a = 1;
    private ConnectionConfiguration b;
    private BOSHConfiguration c;
    private q d;
    private int e;
    private long f = SystemClock.elapsedRealtime();
    private XMPPConnection g;
    private BOSHConnection h;
    /* access modifiers changed from: private */
    public Connection i;
    private b j;
    /* access modifiers changed from: private */
    public PacketSync k = null;
    private com.xiaomi.push.service.a.a l = null;
    /* access modifiers changed from: private */
    public d m = null;
    private PacketListener n = new s(this);

    class a extends d {
        n.b a = null;

        public a(n.b bVar) {
            super(9);
            this.a = bVar;
        }

        public void a() {
            try {
                if (!XMPushService.this.d()) {
                    com.xiaomi.channel.commonutils.logger.b.c("trying bind while the connection is not created, quit!");
                } else if (this.a.m == n.c.unbind) {
                    this.a.a(n.c.binding, 0, 0, null, null);
                    XMPushService.this.i.a(this.a);
                } else {
                    com.xiaomi.channel.commonutils.logger.b.a("trying duplicate bind, ingore! " + this.a.m);
                }
            } catch (XMPPException e) {
                com.xiaomi.channel.commonutils.logger.b.a(e);
                XMPushService.this.a(10, e);
            }
        }

        public String b() {
            return "bind the client. " + this.a.h + ", " + this.a.b;
        }
    }

    public class b extends d {
        b() {
            super(1);
        }

        public void a() {
            if (XMPushService.this.a()) {
                XMPushService.this.l();
            } else {
                com.xiaomi.channel.commonutils.logger.b.a("should not connect. quit the job.");
            }
        }

        public String b() {
            return "do reconnect..";
        }
    }

    public class c extends d {
        public int a;
        public Exception b;

        c(int i, Exception exc) {
            super(2);
            this.a = i;
            this.b = exc;
        }

        public void a() {
            XMPushService.this.a(this.a, this.b);
        }

        public String b() {
            return "disconnect the connection.";
        }
    }

    public static abstract class d {
        protected int d;

        public d(int i) {
            this.d = i;
        }

        public abstract void a();

        public abstract String b();

        public void c() {
            if (!(this.d == 4 || this.d == 8)) {
                com.xiaomi.channel.commonutils.logger.b.a("JOB: " + b());
            }
            a();
        }
    }

    class e extends d {
        public e() {
            super(5);
        }

        public void a() {
            XMPushService.this.m.quit();
        }

        public String b() {
            return "ask the job queue to quit";
        }
    }

    class f extends d {
        private Packet b = null;

        public f(Packet packet) {
            super(8);
            this.b = packet;
        }

        public void a() {
            XMPushService.this.k.a(this.b);
        }

        public String b() {
            return "receive a message.";
        }
    }

    class g extends d {
        public g() {
            super(4);
        }

        public void a() {
            if (XMPushService.this.d()) {
                try {
                    XMPushService.this.i.c();
                } catch (XMPPException e) {
                    XMPushService.this.a(10, e);
                    com.xiaomi.channel.commonutils.logger.b.a(e);
                }
            }
        }

        public String b() {
            return "send ping..";
        }
    }

    class h extends d {
        n.b a = null;

        public h(n.b bVar) {
            super(4);
            this.a = bVar;
        }

        public void a() {
            try {
                this.a.a(n.c.unbind, 1, 16, null, null);
                XMPushService.this.i.a(this.a.h, this.a.b);
                this.a.a(n.c.binding, 1, 16, null, null);
                XMPushService.this.i.a(this.a);
            } catch (XMPPException e) {
                com.xiaomi.channel.commonutils.logger.b.a(e);
                XMPushService.this.a(10, e);
            }
        }

        public String b() {
            return "bind the client. " + this.a.h + ", " + this.a.b;
        }
    }

    class i extends d {
        i() {
            super(3);
        }

        public void a() {
            XMPushService.this.a(11, (Exception) null);
            if (XMPushService.this.a()) {
                XMPushService.this.l();
            }
        }

        public String b() {
            return "reset the connection.";
        }
    }

    class j extends d {
        n.b a = null;
        int b;
        String c;
        String e;

        public j(n.b bVar, int i, String str, String str2) {
            super(9);
            this.a = bVar;
            this.b = i;
            this.c = str;
            this.e = str2;
        }

        public void a() {
            if (!(this.a.m == n.c.unbind || XMPushService.this.i == null)) {
                try {
                    XMPushService.this.i.a(this.a.h, this.a.b);
                } catch (XMPPException e2) {
                    XMPushService.this.a(10, e2);
                }
            }
            this.a.a(n.c.unbind, this.b, 0, this.e, this.c);
        }

        public String b() {
            return "unbind the channel. " + this.a.h + ", " + this.a.b;
        }
    }

    static {
        XMPPConnection.c = true;
        if (com.xiaomi.channel.commonutils.a.a.b || com.xiaomi.channel.commonutils.a.a.e || com.xiaomi.channel.commonutils.a.a.c || com.xiaomi.channel.commonutils.a.a.g) {
            com.xiaomi.channel.commonutils.logger.b.a(0);
        }
    }

    private n.b a(String str, Intent intent) {
        n.b b2 = n.a().b(str, intent.getStringExtra(o.l));
        if (b2 == null) {
            b2 = new n.b();
        }
        b2.h = intent.getStringExtra(o.m);
        b2.b = intent.getStringExtra(o.l);
        b2.c = intent.getStringExtra(o.o);
        b2.a = intent.getStringExtra(o.u);
        b2.f = intent.getStringExtra(o.s);
        b2.g = intent.getStringExtra(o.t);
        b2.e = intent.getBooleanExtra(o.r, false);
        b2.i = intent.getStringExtra(o.q);
        b2.d = intent.getStringExtra(o.p);
        b2.k = this.j;
        b2.l = getApplicationContext();
        n.a().a(b2);
        return b2;
    }

    private String a(String str) {
        return "<iq to='" + str + "' id='0' chid='0' type='get'><ping xmlns='urn:xmpp:ping'>%1$s</ping></iq>";
    }

    private Message a(Message message, String str) {
        byte[] a2 = p.a(str, message.k());
        Message message2 = new Message();
        message2.n(message.n());
        message2.m(message.m());
        message2.k(message.k());
        message2.l(message.l());
        message2.b(true);
        String a3 = p.a(a2, StringUtils.d(message.a()));
        CommonPacketExtension commonPacketExtension = new CommonPacketExtension("s", null, null, null);
        commonPacketExtension.b(a3);
        message2.a(commonPacketExtension);
        return message2;
    }

    private Packet a(Packet packet, String str, String str2, boolean z) {
        n a2 = n.a();
        List<String> b2 = a2.b(str);
        if (b2.isEmpty()) {
            com.xiaomi.channel.commonutils.logger.b.a("open channel should be called first before sending a packet, pkg=" + str);
        } else {
            String l2 = packet.l();
            if (TextUtils.isEmpty(l2)) {
                l2 = b2.get(0);
                packet.l(l2);
            }
            n.b b3 = a2.b(l2, packet.n());
            if (!d()) {
                com.xiaomi.channel.commonutils.logger.b.a("drop a packet as the channel is not connected, chid=" + l2);
            } else if (b3 == null || b3.m != n.c.binded) {
                com.xiaomi.channel.commonutils.logger.b.a("drop a packet as the channel is not opened, chid=" + l2);
            } else if (TextUtils.equals(str2, b3.j)) {
                return (!(packet instanceof Message) || !z) ? packet : a((Message) packet, b3.i);
            } else {
                com.xiaomi.channel.commonutils.logger.b.a("invalid session. " + str2);
            }
        }
        return null;
    }

    private void a(String str, int i2) {
        Collection<n.b> c2 = n.a().c(str);
        if (c2 != null) {
            for (n.b next : c2) {
                if (next != null) {
                    a(new j(next, i2, null, null));
                }
            }
        }
        n.a().a(str);
    }

    /* access modifiers changed from: private */
    public void j() {
        if (g.a(getApplicationContext()) != null) {
            n.b a2 = g.a(getApplicationContext()).a(this);
            a(a2);
            n.a().a(a2);
            if (com.xiaomi.channel.commonutils.b.a.d(getApplicationContext())) {
                a(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (!a()) {
            this.l.a();
        } else if (!this.l.b()) {
            Intent intent = new Intent(o.k);
            intent.setPackage(getPackageName());
            this.l.a((long) SmackConfiguration.d(), intent);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.i != null && this.i.j()) {
            com.xiaomi.channel.commonutils.logger.b.c("try to connect while connecting.");
        } else if (this.i == null || !this.i.k()) {
            this.b.b(com.xiaomi.channel.commonutils.b.a.f(this));
            try {
                this.g.s();
                this.g.a(this.n, new y(this));
                this.i = this.g;
            } catch (XMPPException e2) {
                com.xiaomi.channel.commonutils.logger.b.a("fail to create xmpp connection", e2);
                this.g.a(new Presence(Presence.Type.unavailable), 3, e2);
            }
            if (this.i == null || this.i.p() == 2) {
                try {
                    Fallback fallbacksByHost = HostManager.getInstance().getFallbacksByHost("mibind.chat.gslb.mi-idc.com");
                    if (fallbacksByHost != null) {
                        this.c.a(fallbacksByHost);
                    }
                    this.h.a();
                    this.h.a(this.n, new z(this));
                    this.i = this.h;
                } catch (XMPPException e3) {
                    com.xiaomi.channel.commonutils.logger.b.a("fail to create BOSH connection", e3);
                    this.h.a(new Presence(Presence.Type.unavailable), 3, e3);
                }
            }
            if (this.i == null) {
                l.a();
                n.a().a(this);
            }
        } else {
            com.xiaomi.channel.commonutils.logger.b.c("try to connect while is connected.");
        }
    }

    public XMPPConnection a(ConnectionConfiguration connectionConfiguration) {
        return new XMPPConnection(this, connectionConfiguration);
    }

    public Message a(com.xiaomi.b.a.g gVar) {
        Message message = new Message();
        message.l("5");
        message.m("xiaomi.com");
        message.n(g.a(this).a);
        message.b(true);
        message.f("push");
        String str = g.a(this).a;
        gVar.g.b = str.substring(0, str.indexOf("@"));
        gVar.g.d = str.substring(str.indexOf(CookieSpec.PATH_DELIM) + 1);
        String valueOf = String.valueOf(com.xiaomi.channel.commonutils.c.a.a(p.a(p.a(g.a(this).c, message.k()), t.a(gVar))));
        CommonPacketExtension commonPacketExtension = new CommonPacketExtension("s", null, null, null);
        commonPacketExtension.b(valueOf);
        message.a(commonPacketExtension);
        com.xiaomi.channel.commonutils.logger.b.b("try send mi push message " + gVar.toString());
        return message;
    }

    public Message a(byte[] bArr) {
        com.xiaomi.b.a.g gVar = new com.xiaomi.b.a.g();
        try {
            t.a(gVar, bArr);
            return a(gVar);
        } catch (TException e2) {
            com.xiaomi.channel.commonutils.logger.b.a(e2);
            return null;
        }
    }

    public void a(int i2) {
        this.m.a(i2);
    }

    public void a(int i2, Exception exc) {
        if (this.i != null) {
            this.i.a(new Presence(Presence.Type.unavailable), i2, exc);
            this.i = null;
        }
        a(7);
        a(4);
        n.a().a(this, i2);
    }

    public void a(d dVar) {
        a(dVar, 0);
    }

    public void a(d dVar, long j2) {
        this.m.a(dVar, j2);
    }

    public void a(n.b bVar) {
        bVar.a(new x(this));
    }

    public void a(Exception exc) {
        a(false);
    }

    public void a(String str, String str2, int i2, String str3, String str4) {
        n.b b2 = n.a().b(str, str2);
        if (b2 != null) {
            a(new j(b2, i2, str4, str3));
        }
        n.a().a(str, str2);
    }

    public void a(String str, byte[] bArr) {
        if (this.i != null) {
            Message a2 = a(bArr);
            if (a2 != null) {
                this.i.a(a2);
            } else {
                i.a(this, str, bArr, ErrorCode.ERROR_INVALID_PAYLOAD, "not a valid message");
            }
        } else {
            throw new XMPPException("try send msg while connection is null.");
        }
    }

    public void a(Packet packet) {
        if (this.i != null) {
            this.i.a(packet);
            return;
        }
        throw new XMPPException("try send msg while connection is null.");
    }

    public void a(boolean z) {
        this.d.a(z);
    }

    public void a(Packet[] packetArr) {
        if (this.i != null) {
            this.i.a(packetArr);
            return;
        }
        throw new XMPPException("try send msg while connection is null.");
    }

    public boolean a() {
        return com.xiaomi.channel.commonutils.b.a.d(this) && n.a().c() > 0;
    }

    public b b() {
        return new b();
    }

    public void b(int i2, Exception exc) {
        a(false);
        if (SystemClock.elapsedRealtime() - this.f >= 300000) {
            this.e = 0;
        } else if (com.xiaomi.channel.commonutils.b.a.d(this) && this.i == this.g) {
            this.e++;
            if (this.e >= 2) {
                String e2 = this.g.e();
                com.xiaomi.channel.commonutils.logger.b.a("max short conn time reached, sink down current host:" + e2);
                this.b.a(e2, 0, exc);
                this.e = 0;
            }
        }
    }

    public void b(com.xiaomi.b.a.g gVar) {
        if (this.i != null) {
            this.i.a(a(gVar));
            return;
        }
        throw new XMPPException("try send msg while connection is null.");
    }

    public void b(n.b bVar) {
        if (bVar != null) {
            long a2 = bVar.a();
            com.xiaomi.channel.commonutils.logger.b.a("schedule rebind job in " + (a2 / 1000));
            a(new a(bVar), a2);
        }
    }

    public boolean b(int i2) {
        return this.m.b(i2);
    }

    public b c() {
        return this.j;
    }

    public boolean d() {
        return this.i != null && this.i.k();
    }

    public boolean e() {
        return this.i != null && this.i.j();
    }

    public Connection f() {
        return this.i;
    }

    public void g() {
        com.xiaomi.channel.commonutils.logger.b.b("begin to connect...");
    }

    public void h() {
        a(new aa(this, 10), 120000);
    }

    public void i() {
        this.d.a();
        this.f = SystemClock.elapsedRealtime();
        Iterator<n.b> it = n.a().b().iterator();
        while (it.hasNext()) {
            a(new a(it.next()));
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        HostManager.init(this, null, null, "0");
        this.b = new ConnectionConfiguration(null, 5222, "xiaomi.com", null);
        this.b.b(true);
        this.b.c(false);
        this.g = a(this.b);
        this.g.b(a("xiaomi.com"));
        this.c = new BOSHConfiguration(false, new Fallback("mibind.chat.gslb.mi-idc.com"), 80, "mibind/http-bind", "xiaomi.com", null);
        System.setProperty(BOSHClient.class.getName() + ".emptyRequestDelay", String.valueOf(1000));
        this.h = new BOSHConnection(this, this.c);
        this.j = b();
        this.j.a(this);
        this.l = new com.xiaomi.push.service.a.a(this);
        this.g.a(this);
        this.h.a(this);
        this.k = new PacketSync(this);
        new c().a();
        this.m = new t(this, "Connection Controller Thread");
        this.m.start();
        n a2 = n.a();
        a2.d();
        a2.a(new v(this));
        this.d = new q(this);
    }

    public void onDestroy() {
        a(new c(15, null));
        a(new e());
        n.a().d();
        this.g.b(this);
        this.h.b(this);
        super.onDestroy();
        com.xiaomi.channel.commonutils.logger.b.b("Service destroyed");
    }

    public void onStart(Intent intent, int i2) {
        NetworkInfo networkInfo;
        n.b bVar = null;
        int i3 = 0;
        if (intent == null) {
            com.xiaomi.channel.commonutils.logger.b.c("onStart() with intent NULL");
        } else {
            com.xiaomi.channel.commonutils.logger.b.b("onStart() with intent.Action = " + intent.getAction());
        }
        n a2 = n.a();
        if (intent != null && intent.getAction() != null) {
            if (o.a.equalsIgnoreCase(intent.getAction()) || o.g.equalsIgnoreCase(intent.getAction())) {
                String stringExtra = intent.getStringExtra(o.m);
                String stringExtra2 = intent.getStringExtra(o.v);
                if (stringExtra != null) {
                    n.b a3 = a(stringExtra, intent);
                    boolean z = !TextUtils.isEmpty(a3.j) && !TextUtils.equals(stringExtra2, a3.j);
                    a3.j = stringExtra2;
                    if (!com.xiaomi.channel.commonutils.b.a.d(this)) {
                        this.j.a(this, a3, false, 2, null);
                    } else if (!d()) {
                        a(true);
                    } else if (z) {
                        a(new h(a3));
                    } else if (a3.m == n.c.binding) {
                        com.xiaomi.channel.commonutils.logger.b.a(String.format("the client is binding. %1$s %2$s.", a3.h, a3.b));
                    } else if (a3.m == n.c.binded) {
                        this.j.a(this, a3, true, 0, null);
                    } else {
                        a(new a(a3));
                    }
                } else {
                    com.xiaomi.channel.commonutils.logger.b.c("channel id is empty, do nothing!");
                }
            } else if (o.f.equalsIgnoreCase(intent.getAction())) {
                String stringExtra3 = intent.getStringExtra(o.u);
                String stringExtra4 = intent.getStringExtra(o.m);
                String stringExtra5 = intent.getStringExtra(o.l);
                if (TextUtils.isEmpty(stringExtra4)) {
                    for (String a4 : a2.b(stringExtra3)) {
                        a(a4, 2);
                    }
                } else if (TextUtils.isEmpty(stringExtra5)) {
                    a(stringExtra4, 2);
                } else {
                    a(stringExtra4, stringExtra5, 2, null, null);
                }
            } else if (o.b.equalsIgnoreCase(intent.getAction())) {
                Packet a5 = a(new Message(intent.getBundleExtra("ext_packet")), intent.getStringExtra(o.u), intent.getStringExtra(o.v), intent.getBooleanExtra("ext_encrypt", true));
                if (a5 != null) {
                    a(new r(this, a5));
                }
            } else if (o.d.equalsIgnoreCase(intent.getAction())) {
                String stringExtra6 = intent.getStringExtra(o.u);
                String stringExtra7 = intent.getStringExtra(o.v);
                Parcelable[] parcelableArrayExtra = intent.getParcelableArrayExtra("ext_packets");
                Message[] messageArr = new Message[parcelableArrayExtra.length];
                boolean booleanExtra = intent.getBooleanExtra("ext_encrypt", true);
                while (i3 < parcelableArrayExtra.length) {
                    messageArr[i3] = new Message((Bundle) parcelableArrayExtra[i3]);
                    messageArr[i3] = (Message) a(messageArr[i3], stringExtra6, stringExtra7, booleanExtra);
                    if (messageArr[i3] != null) {
                        i3++;
                    } else {
                        return;
                    }
                }
                a(new a(this, messageArr));
            } else if (o.c.equalsIgnoreCase(intent.getAction())) {
                String stringExtra8 = intent.getStringExtra(o.u);
                String stringExtra9 = intent.getStringExtra(o.v);
                IQ iq = new IQ(intent.getBundleExtra("ext_packet"));
                if (a(iq, stringExtra8, stringExtra9, false) != null) {
                    a(new r(this, iq));
                }
            } else if (o.e.equalsIgnoreCase(intent.getAction())) {
                String stringExtra10 = intent.getStringExtra(o.u);
                String stringExtra11 = intent.getStringExtra(o.v);
                Presence presence = new Presence(intent.getBundleExtra("ext_packet"));
                if (a(presence, stringExtra10, stringExtra11, false) != null) {
                    a(new r(this, presence));
                }
            } else if ("com.xiaomi.push.timer".equalsIgnoreCase(intent.getAction())) {
                com.xiaomi.channel.commonutils.logger.b.a("Service called on timer");
                if (this.m.a()) {
                    com.xiaomi.channel.commonutils.logger.b.c("ERROR, the job controller is blocked.");
                    n.a().a(this, 14);
                    stopSelf();
                } else if (!d()) {
                    a(false);
                } else if (this.i.r()) {
                    a(new g());
                } else {
                    a(new c(17, null));
                }
            } else if ("com.xiaomi.push.network_status_changed".equalsIgnoreCase(intent.getAction())) {
                try {
                    networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
                } catch (Exception e2) {
                    com.xiaomi.channel.commonutils.logger.b.a(e2);
                    networkInfo = null;
                }
                if (networkInfo != null) {
                    com.xiaomi.channel.commonutils.logger.b.a("network changed, " + networkInfo.toString());
                } else {
                    com.xiaomi.channel.commonutils.logger.b.a("network changed, no active network");
                }
                if (!com.xiaomi.channel.commonutils.b.a.d(this)) {
                    a(new c(2, null));
                } else if (!d() && !e()) {
                    this.m.a(1);
                    a(new b());
                }
                k();
            } else if (o.h.equals(intent.getAction())) {
                String stringExtra12 = intent.getStringExtra(o.m);
                if (stringExtra12 != null) {
                    a(stringExtra12, intent).j = intent.getStringExtra(o.v);
                }
                a(new i());
            } else if (o.i.equals(intent.getAction())) {
                String stringExtra13 = intent.getStringExtra(o.u);
                List<String> b2 = a2.b(stringExtra13);
                if (b2.isEmpty()) {
                    com.xiaomi.channel.commonutils.logger.b.a("open channel should be called first before update info, pkg=" + stringExtra13);
                    return;
                }
                String stringExtra14 = intent.getStringExtra(o.m);
                String stringExtra15 = intent.getStringExtra(o.l);
                if (TextUtils.isEmpty(stringExtra14)) {
                    stringExtra14 = b2.get(0);
                }
                if (TextUtils.isEmpty(stringExtra15)) {
                    Collection<n.b> c2 = a2.c(stringExtra14);
                    if (c2 != null && !c2.isEmpty()) {
                        bVar = c2.iterator().next();
                    }
                } else {
                    bVar = a2.b(stringExtra14, stringExtra15);
                }
                if (bVar != null) {
                    if (intent.hasExtra(o.s)) {
                        bVar.f = intent.getStringExtra(o.s);
                    }
                    if (intent.hasExtra(o.t)) {
                        bVar.g = intent.getStringExtra(o.t);
                    }
                }
            } else if ("com.xiaomi.mipush.REGISTER_APP".equals(intent.getAction())) {
                byte[] byteArrayExtra = intent.getByteArrayExtra("mipush_payload");
                String stringExtra16 = intent.getStringExtra("mipush_app_package");
                if (byteArrayExtra == null) {
                    i.a(this, stringExtra16, byteArrayExtra, ErrorCode.ERROR_INVALID_PAYLOAD, "null payload");
                    com.xiaomi.channel.commonutils.logger.b.a("register request without payload");
                    return;
                }
                com.xiaomi.b.a.g gVar = new com.xiaomi.b.a.g();
                try {
                    t.a(gVar, byteArrayExtra);
                    if (gVar.a == com.xiaomi.b.a.a.Registration) {
                        com.xiaomi.b.a.i iVar = new com.xiaomi.b.a.i();
                        try {
                            t.a(iVar, gVar.f());
                            i.a(gVar.i(), byteArrayExtra);
                            a(new h(this, gVar.i(), iVar.d(), iVar.h(), byteArrayExtra));
                        } catch (TException e3) {
                            com.xiaomi.channel.commonutils.logger.b.a(e3);
                            i.a(this, stringExtra16, byteArrayExtra, ErrorCode.ERROR_INVALID_PAYLOAD, " data action error.");
                        }
                    } else {
                        i.a(this, stringExtra16, byteArrayExtra, ErrorCode.ERROR_INVALID_PAYLOAD, " registration action required.");
                        com.xiaomi.channel.commonutils.logger.b.a("register request with invalid payload");
                    }
                } catch (TException e4) {
                    com.xiaomi.channel.commonutils.logger.b.a(e4);
                    i.a(this, stringExtra16, byteArrayExtra, ErrorCode.ERROR_INVALID_PAYLOAD, " data container error.");
                }
            } else if ("com.xiaomi.mipush.SEND_MESSAGE".equals(intent.getAction()) || "com.xiaomi.mipush.UNREGISTER_APP".equals(intent.getAction())) {
                String stringExtra17 = intent.getStringExtra("mipush_app_package");
                byte[] byteArrayExtra2 = intent.getByteArrayExtra("mipush_payload");
                Collection<n.b> c3 = n.a().c("5");
                if (c3.isEmpty()) {
                    i.a(this, stringExtra17, byteArrayExtra2, ErrorCode.ERROR_INTERNAL_ERROR, "the channel is not initialized.");
                } else if (c3.iterator().next().m != n.c.binded) {
                    i.a(this, stringExtra17, byteArrayExtra2, ErrorCode.ERROR_SERVICE_UNAVAILABLE, "the push is not connected.");
                } else {
                    a(new w(this, 4, stringExtra17, byteArrayExtra2));
                }
            }
        }
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        onStart(intent, i3);
        return a;
    }
}
