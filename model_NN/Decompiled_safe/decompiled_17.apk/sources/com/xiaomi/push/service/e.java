package com.xiaomi.push.service;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.xiaomi.push.service.XMPushService;

class e extends Handler {
    final /* synthetic */ d a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(d dVar, Looper looper) {
        super(looper);
        this.a = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.push.service.d.a(com.xiaomi.push.service.d, boolean):boolean
     arg types: [com.xiaomi.push.service.d, int]
     candidates:
      com.xiaomi.push.service.d.a(com.xiaomi.push.service.d, long):long
      com.xiaomi.push.service.d.a(com.xiaomi.push.service.XMPushService$d, long):void
      com.xiaomi.push.service.d.a(com.xiaomi.push.service.d, boolean):boolean */
    public void handleMessage(Message message) {
        boolean unused = this.a.b = true;
        long unused2 = this.a.a = System.currentTimeMillis();
        if (message.obj instanceof XMPushService.d) {
            ((XMPushService.d) message.obj).c();
        }
        boolean unused3 = this.a.b = false;
    }
}
