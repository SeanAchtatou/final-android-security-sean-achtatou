package com.xiaomi.push.service;

import com.xiaomi.push.service.XMPushService;
import org.jivesoftware.smack.XMPPException;

class w extends XMPushService.d {
    final /* synthetic */ String a;
    final /* synthetic */ byte[] b;
    final /* synthetic */ XMPushService c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    w(XMPushService xMPushService, int i, String str, byte[] bArr) {
        super(i);
        this.c = xMPushService;
        this.a = str;
        this.b = bArr;
    }

    public void a() {
        try {
            this.c.a(this.a, this.b);
        } catch (XMPPException e) {
            this.c.a(10, e);
        }
    }

    public String b() {
        return "send mi push message";
    }
}
