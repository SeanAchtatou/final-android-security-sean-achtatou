package com.xiaomi.push.service;

import android.text.TextUtils;
import com.xiaomi.channel.commonutils.logger.b;
import java.net.InetAddress;
import java.net.UnknownHostException;

final class m implements Runnable {
    m() {
    }

    public void run() {
        String b = l.c();
        if (!TextUtils.isEmpty(b)) {
            b.a("Network Checkup: get gateway:" + b);
            l.b(b);
        } else {
            b.a("Network Checkup: cannot get gateway");
        }
        try {
            InetAddress byName = InetAddress.getByName("www.baidu.com");
            b.a("Network Checkup: get address for www.baidu.com:" + byName.getAddress());
            l.b(byName.getHostAddress());
        } catch (UnknownHostException e) {
            b.a("Network Checkup: cannot resolve the host www.baidu.com");
        }
    }
}
