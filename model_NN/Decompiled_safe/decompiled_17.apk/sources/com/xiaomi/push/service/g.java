package com.xiaomi.push.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.baixing.activity.ThirdpartyTransitActivity;
import com.baixing.network.api.ApiParams;
import com.tencent.tauth.Constants;
import com.xiaomi.channel.commonutils.b.a;
import com.xiaomi.channel.commonutils.c.c;
import com.xiaomi.channel.commonutils.logger.b;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class g {
    private static f a;
    private static String b = null;
    private static String c = null;

    public static f a(Context context) {
        f fVar = null;
        synchronized (g.class) {
            try {
                if (a != null) {
                    fVar = a;
                } else {
                    SharedPreferences sharedPreferences = context.getSharedPreferences("mipush_account", 0);
                    String string = sharedPreferences.getString("uuid", null);
                    String string2 = sharedPreferences.getString("token", null);
                    String string3 = sharedPreferences.getString("security", null);
                    String string4 = sharedPreferences.getString("app_id", null);
                    String string5 = sharedPreferences.getString("app_token", null);
                    String string6 = sharedPreferences.getString("package_name", null);
                    String string7 = sharedPreferences.getString("device_id", null);
                    if (!TextUtils.isEmpty(string7) && string7.startsWith("a-")) {
                        string7 = c(context);
                        sharedPreferences.edit().putString("device_id", string7).commit();
                    }
                    if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                        String c2 = c(context);
                        if (TextUtils.isEmpty(c2) || TextUtils.isEmpty(string7) || string7.equals(c2)) {
                            a = new f(string, string2, string3, string4, string5, string6);
                            fVar = a;
                        } else {
                            b.c("erase the old account.");
                            d(context);
                        }
                    }
                }
            } catch (Throwable th) {
                Class<g> cls = g.class;
                throw th;
            }
        }
        return fVar;
    }

    public static f a(Context context, String str, String str2, String str3) {
        String str4;
        String str5;
        PackageInfo packageInfo;
        f fVar = null;
        synchronized (g.class) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("devid", b(context)));
            String str6 = e(context) ? "1000271" : str2;
            try {
                str4 = e(context) ? "420100086271" : str3;
                str5 = e(context) ? "com.xiaomi.xmsf" : str;
                arrayList.add(new BasicNameValuePair(Constants.PARAM_APP_ID, str6));
                arrayList.add(new BasicNameValuePair("apptoken", str4));
                packageInfo = context.getPackageManager().getPackageInfo(str5, 16384);
            } catch (PackageManager.NameNotFoundException e) {
                b.a(e);
                packageInfo = null;
            } catch (Throwable th) {
                Class<g> cls = g.class;
                throw th;
            }
            arrayList.add(new BasicNameValuePair("appversion", packageInfo != null ? String.valueOf(packageInfo.versionCode) : "0"));
            arrayList.add(new BasicNameValuePair("sdkversion", "1"));
            arrayList.add(new BasicNameValuePair("packagename", str5));
            arrayList.add(new BasicNameValuePair("model", Build.MODEL));
            arrayList.add(new BasicNameValuePair("os", Build.VERSION.RELEASE + "-" + Build.VERSION.INCREMENTAL));
            String a2 = a.a(context, a(), arrayList);
            if (!TextUtils.isEmpty(a2)) {
                JSONObject jSONObject = new JSONObject(a2);
                if (jSONObject.getInt("code") == 0) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(ThirdpartyTransitActivity.Key_Data);
                    fVar = new f(jSONObject2.getString(ApiParams.KEY_USERID) + "@xiaomi.com/an" + c.a(6), jSONObject2.getString("token"), jSONObject2.getString("ssecurity"), str6, str4, str5);
                    a(context, fVar);
                    a = fVar;
                } else {
                    i.a(context, jSONObject.getInt("code"), jSONObject.optString(Constants.PARAM_COMMENT));
                    b.a(a2);
                }
            }
        }
        return fVar;
    }

    public static String a() {
        return "https://" + (com.xiaomi.channel.commonutils.a.a.a() ? "sandbox.xmpush.xiaomi.com" : "register.xmpush.xiaomi.com") + "/pass/register";
    }

    private static void a(Context context, f fVar) {
        SharedPreferences.Editor edit = context.getSharedPreferences("mipush_account", 0).edit();
        edit.putString("uuid", fVar.a);
        edit.putString("security", fVar.c);
        edit.putString("token", fVar.b);
        edit.putString("app_id", fVar.d);
        edit.putString("package_name", fVar.f);
        edit.putString("app_token", fVar.e);
        edit.putString("device_id", c(context));
        edit.commit();
    }

    protected static String b(Context context) {
        if (b == null) {
            String f = f(context);
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            String str = null;
            if (Build.VERSION.SDK_INT > 8) {
                str = Build.SERIAL;
            }
            b = "a-" + c.a(f + string + str);
        }
        return b;
    }

    public static String c(Context context) {
        String str;
        synchronized (g.class) {
            try {
                if (c != null) {
                    str = c;
                } else {
                    String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
                    String str2 = null;
                    if (Build.VERSION.SDK_INT > 8) {
                        str2 = Build.SERIAL;
                    }
                    c = c.a(string + str2);
                    str = c;
                }
            } catch (Throwable th) {
                Class<g> cls = g.class;
                throw th;
            }
        }
        return str;
    }

    public static void d(Context context) {
        context.getSharedPreferences("mipush_account", 0).edit().clear().commit();
    }

    private static boolean e(Context context) {
        return context.getPackageName().equals("com.xiaomi.xmsf");
    }

    private static String f(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String deviceId = telephonyManager.getDeviceId();
        int i = 10;
        while (deviceId == null) {
            int i2 = i - 1;
            if (i <= 0) {
                break;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            deviceId = telephonyManager.getDeviceId();
            i = i2;
        }
        return deviceId;
    }
}
