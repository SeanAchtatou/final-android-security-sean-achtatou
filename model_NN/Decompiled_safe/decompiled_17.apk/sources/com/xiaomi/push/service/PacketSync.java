package com.xiaomi.push.service;

import android.os.Parcelable;
import android.text.TextUtils;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.network.Fallback;
import com.xiaomi.network.HostManager;
import com.xiaomi.push.service.n;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMBinder;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.CommonPacketExtension;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;

public class PacketSync {
    private XMPushService a;

    public interface PacketReceiveHandler extends Parcelable {
    }

    PacketSync(XMPushService xMPushService) {
        this.a = xMPushService;
    }

    private void a(CommonPacketExtension commonPacketExtension) {
        String c = commonPacketExtension.c();
        if (!TextUtils.isEmpty(c)) {
            String[] split = c.split(";");
            Fallback fallbacksByHost = HostManager.getInstance().getFallbacksByHost(ConnectionConfiguration.g());
            if (fallbacksByHost != null && split.length > 0) {
                fallbacksByHost.a(split);
                this.a.a(20, (Exception) null);
                this.a.a(true);
            }
        }
    }

    public void a(Packet packet) {
        n.b b;
        if (packet instanceof XMBinder.BindResult) {
            XMBinder.BindResult bindResult = (XMBinder.BindResult) packet;
            XMBinder.BindResult.Type b2 = bindResult.b();
            String l = bindResult.l();
            String m = bindResult.m();
            if (TextUtils.isEmpty(l) || (b = n.a().b(l, m)) == null) {
                return;
            }
            if (b2 == XMBinder.BindResult.Type.a) {
                b.a(n.c.binded, 1, 0, null, null);
                b.a("SMACK: channel bind succeeded, chid=" + l);
                return;
            }
            XMPPError o = bindResult.o();
            b.a("SMACK: channel bind failed, error=" + o.d());
            if (o != null) {
                if (o.b() == XMPPError.Type.AUTH) {
                    b.a(n.c.unbind, 1, 5, o.a(), o.b().name());
                    n.a().a(l, m);
                } else if (o.b() == XMPPError.Type.CANCEL) {
                    b.a(n.c.unbind, 1, 7, o.a(), o.b().name());
                    n.a().a(l, m);
                } else if (o.b() == XMPPError.Type.WAIT) {
                    this.a.b(b);
                    b.a(n.c.unbind, 1, 7, o.a(), o.b().name());
                }
                b.a("SMACK: channel bind failed, chid=" + l + " reason=" + o.a());
                return;
            }
            return;
        }
        String l2 = packet.l();
        if (TextUtils.isEmpty(l2)) {
            l2 = "1";
        }
        if (!l2.equals("0")) {
            if (packet instanceof IQ) {
                CommonPacketExtension o2 = packet.o("kick");
                if (o2 != null) {
                    String m2 = packet.m();
                    String a2 = o2.a("type");
                    String a3 = o2.a("reason");
                    b.a("kicked by server, chid=" + l2 + " userid=" + m2 + " type=" + a2 + " reason=" + a3);
                    if ("wait".equals(a2)) {
                        n.b b3 = n.a().b(l2, m2);
                        if (b3 != null) {
                            this.a.b(b3);
                            b3.a(n.c.unbind, 3, 0, a3, a2);
                            return;
                        }
                        return;
                    }
                    this.a.a(l2, m2, 3, a3, a2);
                    n.a().a(l2, m2);
                    return;
                }
            } else if (packet instanceof Message) {
                Message message = (Message) packet;
                if ("redir".equals(message.b())) {
                    CommonPacketExtension o3 = message.o("hosts");
                    if (o3 != null) {
                        a(o3);
                        return;
                    }
                    return;
                }
            }
            this.a.c().a(this.a, l2, packet);
        } else if ((packet instanceof IQ) && "0".equals(packet.k()) && "result".equals(((IQ) packet).c().toString())) {
            Connection f = this.a.f();
            if (f instanceof XMPPConnection) {
                ((XMPPConnection) f).w();
            }
        }
    }
}
