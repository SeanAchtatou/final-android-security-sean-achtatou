package com.xiaomi.push.service;

import org.jivesoftware.smack.packet.CommonPacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.xmlpull.v1.XmlPullParser;

public class c implements PacketExtensionProvider {
    /* JADX WARN: Failed to insert an additional move for type inference into block B:10:0x003f */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: org.jivesoftware.smack.packet.CommonPacketExtension} */
    /* JADX WARN: Type inference failed for: r6v0 */
    /* JADX WARN: Type inference failed for: r6v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r6v2 */
    /* JADX WARN: Type inference failed for: r6v3, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r6v6 */
    /* JADX WARN: Type inference failed for: r6v7 */
    /* JADX WARN: Type inference failed for: r6v8 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: CFG modification limit reached, blocks count: 127 */
    /* JADX WARNING: Multi-variable type inference failed */
    public static org.jivesoftware.smack.packet.CommonPacketExtension a(org.xmlpull.v1.XmlPullParser r9) {
        /*
            r8 = 2
            r6 = 0
            int r0 = r9.getEventType()
            if (r0 == r8) goto L_0x0009
        L_0x0008:
            return r6
        L_0x0009:
            java.lang.String r1 = r9.getName()
            java.lang.String r2 = r9.getNamespace()
            int r0 = r9.getAttributeCount()
            if (r0 <= 0) goto L_0x006b
            int r0 = r9.getAttributeCount()
            java.lang.String[] r3 = new java.lang.String[r0]
            int r0 = r9.getAttributeCount()
            java.lang.String[] r4 = new java.lang.String[r0]
            r0 = 0
        L_0x0024:
            int r5 = r9.getAttributeCount()
            if (r0 >= r5) goto L_0x0069
            java.lang.String r5 = r9.getAttributeName(r0)
            r3[r0] = r5
            java.lang.String r5 = r9.getAttributeValue(r0)
            java.lang.String r5 = org.jivesoftware.smack.util.StringUtils.b(r5)
            r4[r0] = r5
            int r0 = r0 + 1
            goto L_0x0024
        L_0x003d:
            if (r0 != r8) goto L_0x004f
            if (r6 != 0) goto L_0x0046
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
        L_0x0046:
            org.jivesoftware.smack.packet.CommonPacketExtension r0 = a(r9)
            if (r0 == 0) goto L_0x004f
            r6.add(r0)
        L_0x004f:
            int r0 = r9.next()
            r7 = 3
            if (r0 == r7) goto L_0x0062
            r7 = 4
            if (r0 != r7) goto L_0x003d
            java.lang.String r0 = r9.getText()
            java.lang.String r5 = r0.trim()
            goto L_0x004f
        L_0x0062:
            org.jivesoftware.smack.packet.CommonPacketExtension r0 = new org.jivesoftware.smack.packet.CommonPacketExtension
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r6 = r0
            goto L_0x0008
        L_0x0069:
            r5 = r6
            goto L_0x004f
        L_0x006b:
            r5 = r6
            r4 = r6
            r3 = r6
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.push.service.c.a(org.xmlpull.v1.XmlPullParser):org.jivesoftware.smack.packet.CommonPacketExtension");
    }

    public void a() {
        ProviderManager.a().a("all", "xm:chat", this);
    }

    public CommonPacketExtension b(XmlPullParser xmlPullParser) {
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1 && eventType != 2) {
            eventType = xmlPullParser.next();
        }
        if (eventType == 2) {
            return a(xmlPullParser);
        }
        return null;
    }
}
