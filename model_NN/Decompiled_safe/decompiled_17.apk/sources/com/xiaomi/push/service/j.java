package com.xiaomi.push.service;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.xiaomi.b.a.g;
import com.xiaomi.b.a.t;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.n;
import java.io.IOException;
import java.util.List;
import org.apache.thrift.TException;
import org.jivesoftware.smack.packet.CommonPacketExtension;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.json.JSONException;

public class j {
    private static void a(XMPushService xMPushService, byte[] bArr) {
        g gVar = new g();
        try {
            t.a(gVar, bArr);
            if (!TextUtils.isEmpty(gVar.f)) {
                Intent intent = new Intent("com.xiaomi.mipush.RECEIVE_MESSAGE");
                intent.putExtra("mipush_payload", bArr);
                intent.setPackage(gVar.f);
                if (a(xMPushService, intent)) {
                    xMPushService.sendBroadcast(intent, b.a(gVar.f));
                } else {
                    xMPushService.a(new k(4, xMPushService, gVar));
                }
            } else {
                b.a("receive a mipush message without package name");
            }
        } catch (TException e) {
            b.a(e);
        }
    }

    private static boolean a(XMPushService xMPushService, Intent intent) {
        List<ResolveInfo> queryBroadcastReceivers = xMPushService.getPackageManager().queryBroadcastReceivers(intent, 32);
        return queryBroadcastReceivers != null && !queryBroadcastReceivers.isEmpty();
    }

    public void a(Context context, n.b bVar, boolean z, int i, String str) {
        f a;
        if (!z && (a = g.a(context)) != null && "token-expired".equals(str)) {
            try {
                g.a(context, a.d, a.e, a.f);
            } catch (IOException e) {
                b.a(e);
            } catch (JSONException e2) {
                b.a(e2);
            }
        }
    }

    public void a(XMPushService xMPushService, Packet packet, n.b bVar) {
        if (packet instanceof Message) {
            Message message = (Message) packet;
            CommonPacketExtension o = message.o("s");
            if (o != null) {
                a(xMPushService, p.b(p.a(bVar.i, message.k()), o.c()));
                return;
            }
            return;
        }
        b.a("not a mipush message");
    }
}
