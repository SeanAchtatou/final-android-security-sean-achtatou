package com.xiaomi.push.service;

import com.xiaomi.b.a.a;
import com.xiaomi.b.a.c;
import com.xiaomi.b.a.g;
import com.xiaomi.b.a.h;
import com.xiaomi.b.a.t;
import com.xiaomi.push.service.XMPushService;
import java.nio.ByteBuffer;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Packet;

final class k extends XMPushService.d {
    final /* synthetic */ XMPushService a;
    final /* synthetic */ g b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(int i, XMPushService xMPushService, g gVar) {
        super(i);
        this.a = xMPushService;
        this.b = gVar;
    }

    private g a(g gVar) {
        h hVar = new h();
        hVar.b(gVar.e);
        hVar.c("package uninstalled");
        hVar.a(Packet.j());
        hVar.a(false);
        g gVar2 = new g();
        byte[] a2 = t.a(hVar);
        c cVar = new c();
        cVar.a = 5;
        cVar.b = "fakeid";
        gVar2.a(cVar);
        gVar2.a(ByteBuffer.wrap(a2));
        gVar2.a(a.Notification);
        gVar2.c(true);
        gVar2.b(gVar.f);
        gVar2.a(false);
        gVar2.a(gVar.e);
        return gVar2;
    }

    public void a() {
        try {
            this.a.b(a(this.b));
        } catch (XMPPException e) {
            this.a.a(10, e);
        }
    }

    public String b() {
        return "send app absent message.";
    }
}
