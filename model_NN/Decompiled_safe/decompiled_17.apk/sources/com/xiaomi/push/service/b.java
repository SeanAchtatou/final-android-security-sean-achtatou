package com.xiaomi.push.service;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.xiaomi.push.service.n;
import java.util.Collection;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

public class b {
    private j a = new j();

    public static String a(String str) {
        return str + ".permission.MIPUSH_RECEIVE";
    }

    public void a(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.xiaomi.push.service_started");
        context.sendBroadcast(intent);
    }

    public void a(Context context, n.b bVar, int i) {
        if (!"5".equalsIgnoreCase(bVar.h)) {
            Intent intent = new Intent();
            intent.setAction("com.xiaomi.push.channel_closed");
            intent.setPackage(bVar.a);
            intent.putExtra(o.m, bVar.h);
            intent.putExtra("ext_reason", i);
            intent.putExtra(o.l, bVar.b);
            intent.putExtra(o.v, bVar.j);
            context.sendBroadcast(intent, a(bVar.a));
        }
    }

    public void a(Context context, n.b bVar, String str, String str2) {
        if ("5".equalsIgnoreCase(bVar.h)) {
            com.xiaomi.channel.commonutils.logger.b.c("mipush kicked by server");
            return;
        }
        Intent intent = new Intent();
        intent.setAction("com.xiaomi.push.kicked");
        intent.setPackage(bVar.a);
        intent.putExtra("ext_kick_type", str);
        intent.putExtra("ext_kick_reason", str2);
        intent.putExtra("ext_chid", bVar.h);
        intent.putExtra(o.l, bVar.b);
        intent.putExtra(o.v, bVar.j);
        context.sendBroadcast(intent, a(bVar.a));
    }

    public void a(Context context, n.b bVar, boolean z, int i, String str) {
        if ("5".equalsIgnoreCase(bVar.h)) {
            this.a.a(context, bVar, z, i, str);
            return;
        }
        Intent intent = new Intent();
        intent.setAction("com.xiaomi.push.channel_opened");
        intent.setPackage(bVar.a);
        intent.putExtra("ext_succeeded", z);
        if (!z) {
            intent.putExtra("ext_reason", i);
        }
        if (!TextUtils.isEmpty(str)) {
            intent.putExtra("ext_reason_msg", str);
        }
        intent.putExtra(o.l, bVar.b);
        intent.putExtra(o.v, bVar.j);
        context.sendBroadcast(intent, a(bVar.a));
    }

    public void a(XMPushService xMPushService, String str, Packet packet) {
        String str2;
        Collection<n.b> c = n.a().c(str);
        if (c == null || c.isEmpty()) {
            com.xiaomi.channel.commonutils.logger.b.c("error while notify channel closed! channel " + str + " not registered");
            return;
        }
        n.b next = c.iterator().next();
        if ("5".equalsIgnoreCase(str)) {
            this.a.a(xMPushService, packet, next);
            return;
        }
        String str3 = next.a;
        if (packet instanceof Message) {
            str2 = "com.xiaomi.push.new_msg";
        } else if (packet instanceof IQ) {
            str2 = "com.xiaomi.push.new_iq";
        } else if (packet instanceof Presence) {
            str2 = "com.xiaomi.push.new_pres";
        } else {
            com.xiaomi.channel.commonutils.logger.b.c("unknown packet type, drop it");
            return;
        }
        Intent intent = new Intent();
        intent.setAction(str2);
        intent.setPackage(str3);
        intent.putExtra("ext_packet", packet.d());
        intent.putExtra(o.v, next.j);
        xMPushService.sendBroadcast(intent, a(str3));
    }
}
