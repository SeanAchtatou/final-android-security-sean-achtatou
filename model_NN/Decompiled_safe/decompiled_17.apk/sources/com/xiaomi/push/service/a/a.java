package com.xiaomi.push.service.a;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.xiaomi.channel.commonutils.logger.b;

public class a {
    private PendingIntent a = null;
    private Context b = null;

    public a(Context context) {
        this.b = context;
    }

    private long a(long j) {
        return (SystemClock.elapsedRealtime() / j) * j;
    }

    public void a() {
        synchronized (this) {
            if (this.a != null) {
                ((AlarmManager) this.b.getSystemService("alarm")).cancel(this.a);
                this.a = null;
                b.b("unregister timer");
            }
        }
    }

    public void a(long j, Intent intent) {
        synchronized (this) {
            if (this.a == null) {
                this.a = PendingIntent.getBroadcast(this.b, 0, intent, 0);
                ((AlarmManager) this.b.getSystemService("alarm")).setRepeating(2, a(j), j, this.a);
                b.b("register timer");
            }
        }
    }

    public boolean b() {
        boolean z;
        synchronized (this) {
            z = this.a != null;
        }
        return z;
    }
}
