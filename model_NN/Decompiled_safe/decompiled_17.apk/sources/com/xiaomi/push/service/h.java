package com.xiaomi.push.service;

import com.xiaomi.push.service.XMPushService;

public class h extends XMPushService.d {
    private XMPushService a;
    private byte[] b;
    private String c;
    private String e;
    private String f;

    public h(XMPushService xMPushService, String str, String str2, String str3, byte[] bArr) {
        super(9);
        this.a = xMPushService;
        this.c = str;
        this.b = bArr;
        this.e = str2;
        this.f = str3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r5 = this;
            com.xiaomi.push.service.XMPushService r0 = r5.a
            com.xiaomi.push.service.f r1 = com.xiaomi.push.service.g.a(r0)
            if (r1 != 0) goto L_0x0030
            com.xiaomi.push.service.XMPushService r0 = r5.a     // Catch:{ IOException -> 0x0026, JSONException -> 0x002c }
            java.lang.String r2 = r5.c     // Catch:{ IOException -> 0x0026, JSONException -> 0x002c }
            java.lang.String r3 = r5.e     // Catch:{ IOException -> 0x0026, JSONException -> 0x002c }
            java.lang.String r4 = r5.f     // Catch:{ IOException -> 0x0026, JSONException -> 0x002c }
            com.xiaomi.push.service.f r0 = com.xiaomi.push.service.g.a(r0, r2, r3, r4)     // Catch:{ IOException -> 0x0026, JSONException -> 0x002c }
        L_0x0014:
            if (r0 != 0) goto L_0x0032
            java.lang.String r0 = "no account for mipush"
            com.xiaomi.channel.commonutils.logger.b.c(r0)
            com.xiaomi.push.service.XMPushService r0 = r5.a
            r1 = 70000002(0x42c1d82, float:2.0232052E-36)
            java.lang.String r2 = "no account."
            com.xiaomi.push.service.i.a(r0, r1, r2)
        L_0x0025:
            return
        L_0x0026:
            r0 = move-exception
            com.xiaomi.channel.commonutils.logger.b.a(r0)
            r0 = r1
            goto L_0x0014
        L_0x002c:
            r0 = move-exception
            com.xiaomi.channel.commonutils.logger.b.a(r0)
        L_0x0030:
            r0 = r1
            goto L_0x0014
        L_0x0032:
            com.xiaomi.push.service.n r1 = com.xiaomi.push.service.n.a()
            java.lang.String r2 = "5"
            java.util.Collection r1 = r1.c(r2)
            boolean r2 = r1.isEmpty()
            if (r2 == 0) goto L_0x0075
            com.xiaomi.push.service.XMPushService r1 = r5.a
            com.xiaomi.push.service.n$b r0 = r0.a(r1)
            com.xiaomi.push.service.XMPushService r1 = r5.a
            r1.a(r0)
            com.xiaomi.push.service.n r1 = com.xiaomi.push.service.n.a()
            r1.a(r0)
        L_0x0054:
            com.xiaomi.push.service.XMPushService r1 = r5.a
            boolean r1 = r1.d()
            if (r1 == 0) goto L_0x0096
            com.xiaomi.push.service.n$c r1 = r0.m     // Catch:{ XMPPException -> 0x006c }
            com.xiaomi.push.service.n$c r2 = com.xiaomi.push.service.n.c.binded     // Catch:{ XMPPException -> 0x006c }
            if (r1 != r2) goto L_0x0080
            com.xiaomi.push.service.XMPushService r0 = r5.a     // Catch:{ XMPPException -> 0x006c }
            java.lang.String r1 = r5.c     // Catch:{ XMPPException -> 0x006c }
            byte[] r2 = r5.b     // Catch:{ XMPPException -> 0x006c }
            r0.a(r1, r2)     // Catch:{ XMPPException -> 0x006c }
            goto L_0x0025
        L_0x006c:
            r0 = move-exception
            com.xiaomi.push.service.XMPushService r1 = r5.a
            r2 = 10
            r1.a(r2, r0)
            goto L_0x0025
        L_0x0075:
            java.util.Iterator r0 = r1.iterator()
            java.lang.Object r0 = r0.next()
            com.xiaomi.push.service.n$b r0 = (com.xiaomi.push.service.n.b) r0
            goto L_0x0054
        L_0x0080:
            com.xiaomi.push.service.n$c r1 = r0.m     // Catch:{ XMPPException -> 0x006c }
            com.xiaomi.push.service.n$c r2 = com.xiaomi.push.service.n.c.unbind     // Catch:{ XMPPException -> 0x006c }
            if (r1 != r2) goto L_0x0025
            com.xiaomi.push.service.XMPushService r1 = r5.a     // Catch:{ XMPPException -> 0x006c }
            com.xiaomi.push.service.XMPushService r2 = r5.a     // Catch:{ XMPPException -> 0x006c }
            r2.getClass()     // Catch:{ XMPPException -> 0x006c }
            com.xiaomi.push.service.XMPushService$a r3 = new com.xiaomi.push.service.XMPushService$a     // Catch:{ XMPPException -> 0x006c }
            r3.<init>(r0)     // Catch:{ XMPPException -> 0x006c }
            r1.a(r3)     // Catch:{ XMPPException -> 0x006c }
            goto L_0x0025
        L_0x0096:
            com.xiaomi.push.service.XMPushService r0 = r5.a
            r1 = 1
            r0.a(r1)
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.push.service.h.a():void");
    }

    public String b() {
        return "register app";
    }
}
