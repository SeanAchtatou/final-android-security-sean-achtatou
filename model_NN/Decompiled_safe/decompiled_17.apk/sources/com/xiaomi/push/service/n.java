package com.xiaomi.push.service;

import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class n {
    private static n a;
    private ConcurrentHashMap<String, HashMap<String, b>> b = new ConcurrentHashMap<>();
    private List<a> c = new ArrayList();

    public interface a {
        void a();
    }

    public static class b {
        public String a;
        public String b;
        public String c;
        public String d;
        public boolean e;
        public String f;
        public String g;
        public String h;
        public String i;
        public String j;
        public b k;
        public Context l;
        c m = c.unbind;
        private int n = 0;
        private List<a> o = new ArrayList();

        public interface a {
            void a(c cVar, c cVar2, int i);
        }

        public long a() {
            return 1000 * (((long) ((20.0d * Math.random()) - 10.0d)) + ((long) ((this.n + 1) * 15)));
        }

        public String a(int i2) {
            switch (i2) {
                case 1:
                    return "OPEN";
                case 2:
                    return "CLOSE";
                case 3:
                    return "KICK";
                default:
                    return "unknown";
            }
        }

        public void a(a aVar) {
            this.o.add(aVar);
        }

        public void a(c cVar, int i2, int i3, String str, String str2) {
            boolean z = true;
            for (a a2 : this.o) {
                a2.a(this.m, cVar, i3);
            }
            if (this.m != cVar) {
                com.xiaomi.channel.commonutils.logger.b.a(String.format("update the client status. %1$s->%2$s %3$s %4$s %5$s %6$s", this.m, cVar, a(i2), o.a(i3), str, str2));
                this.m = cVar;
            }
            if (this.k == null) {
                com.xiaomi.channel.commonutils.logger.b.c("status changed while the client dispatcher is missing");
            } else if (i2 == 2) {
                this.k.a(this.l, this, i3);
            } else if (i2 == 3) {
                this.k.a(this.l, this, str2, str);
            } else if (i2 == 1) {
                if (cVar != c.binded) {
                    z = false;
                }
                if (!z && "wait".equals(str2)) {
                    this.n++;
                } else if (z) {
                    this.n = 0;
                }
                this.k.a(this.l, this, z, i3, str);
            }
        }
    }

    public enum c {
        unbind,
        binding,
        binded
    }

    private n() {
    }

    public static n a() {
        n nVar;
        synchronized (n.class) {
            try {
                if (a == null) {
                    a = new n();
                }
                nVar = a;
            } catch (Throwable th) {
                Class<n> cls = n.class;
                throw th;
            }
        }
        return nVar;
    }

    private String d(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        int indexOf = str.indexOf("@");
        return indexOf > 0 ? str.substring(0, indexOf) : str;
    }

    public void a(Context context) {
        synchronized (this) {
            for (HashMap<String, b> values : this.b.values()) {
                for (b a2 : values.values()) {
                    a2.a(c.unbind, 1, 3, null, null);
                }
            }
        }
    }

    public void a(Context context, int i) {
        synchronized (this) {
            for (HashMap<String, b> values : this.b.values()) {
                for (b a2 : values.values()) {
                    a2.a(c.unbind, 2, i, null, null);
                }
            }
        }
    }

    public void a(a aVar) {
        this.c.add(aVar);
    }

    public void a(b bVar) {
        synchronized (this) {
            HashMap hashMap = this.b.get(bVar.h);
            if (hashMap == null) {
                hashMap = new HashMap();
                this.b.put(bVar.h, hashMap);
            }
            hashMap.put(d(bVar.b), bVar);
            for (a a2 : this.c) {
                a2.a();
            }
        }
    }

    public void a(String str) {
        synchronized (this) {
            com.xiaomi.channel.commonutils.a.b.a(!"5".equals(str));
            HashMap hashMap = this.b.get(str);
            if (hashMap != null) {
                for (b bVar : hashMap.values()) {
                    hashMap.remove(d(bVar.b));
                }
                this.b.remove(str);
            }
            for (a a2 : this.c) {
                a2.a();
            }
        }
    }

    public void a(String str, String str2) {
        synchronized (this) {
            com.xiaomi.channel.commonutils.a.b.a(!"5".equals(str));
            HashMap hashMap = this.b.get(str);
            if (hashMap != null) {
                hashMap.remove(d(str2));
                if (hashMap.isEmpty()) {
                    this.b.remove(str);
                }
            }
            for (a a2 : this.c) {
                a2.a();
            }
        }
    }

    public b b(String str, String str2) {
        b bVar;
        synchronized (this) {
            HashMap hashMap = this.b.get(str);
            bVar = hashMap == null ? null : (b) hashMap.get(d(str2));
        }
        return bVar;
    }

    public ArrayList<b> b() {
        ArrayList<b> arrayList;
        synchronized (this) {
            arrayList = new ArrayList<>();
            for (HashMap<String, b> values : this.b.values()) {
                arrayList.addAll(values.values());
            }
        }
        return arrayList;
    }

    public List<String> b(String str) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            for (HashMap<String, b> values : this.b.values()) {
                for (b bVar : values.values()) {
                    if (str.equals(bVar.a)) {
                        arrayList.add(bVar.h);
                    }
                }
            }
        }
        return arrayList;
    }

    public int c() {
        int size;
        synchronized (this) {
            size = this.b.size();
        }
        return size;
    }

    public Collection<b> c(String str) {
        Collection<b> arrayList;
        synchronized (this) {
            arrayList = !this.b.containsKey(str) ? new ArrayList<>() : ((HashMap) this.b.get(str).clone()).values();
        }
        return arrayList;
    }

    public void d() {
        this.c.clear();
    }
}
