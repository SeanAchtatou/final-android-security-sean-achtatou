package com.google.zxing.oned;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.BitArray;

public final class Code128Reader extends OneDReader {
    private static final int CODE_CODE_A = 101;
    private static final int CODE_CODE_B = 100;
    private static final int CODE_CODE_C = 99;
    private static final int CODE_FNC_1 = 102;
    private static final int CODE_FNC_2 = 97;
    private static final int CODE_FNC_3 = 96;
    private static final int CODE_FNC_4_A = 101;
    private static final int CODE_FNC_4_B = 100;
    static final int[][] CODE_PATTERNS;
    private static final int CODE_SHIFT = 98;
    private static final int CODE_START_A = 103;
    private static final int CODE_START_B = 104;
    private static final int CODE_START_C = 105;
    private static final int CODE_STOP = 106;
    private static final int MAX_AVG_VARIANCE = 64;
    private static final int MAX_INDIVIDUAL_VARIANCE = 179;

    static {
        int[][] iArr = new int[107][];
        iArr[0] = new int[]{2, 1, 2, 2, 2, 2};
        iArr[1] = new int[]{2, 2, 2, 1, 2, 2};
        iArr[2] = new int[]{2, 2, 2, 2, 2, 1};
        iArr[3] = new int[]{1, 2, 1, 2, 2, 3};
        iArr[4] = new int[]{1, 2, 1, 3, 2, 2};
        iArr[5] = new int[]{1, 3, 1, 2, 2, 2};
        iArr[6] = new int[]{1, 2, 2, 2, 1, 3};
        iArr[7] = new int[]{1, 2, 2, 3, 1, 2};
        iArr[8] = new int[]{1, 3, 2, 2, 1, 2};
        iArr[9] = new int[]{2, 2, 1, 2, 1, 3};
        iArr[10] = new int[]{2, 2, 1, 3, 1, 2};
        iArr[11] = new int[]{2, 3, 1, 2, 1, 2};
        iArr[12] = new int[]{1, 1, 2, 2, 3, 2};
        iArr[13] = new int[]{1, 2, 2, 1, 3, 2};
        iArr[14] = new int[]{1, 2, 2, 2, 3, 1};
        iArr[15] = new int[]{1, 1, 3, 2, 2, 2};
        iArr[16] = new int[]{1, 2, 3, 1, 2, 2};
        iArr[17] = new int[]{1, 2, 3, 2, 2, 1};
        iArr[18] = new int[]{2, 2, 3, 2, 1, 1};
        iArr[19] = new int[]{2, 2, 1, 1, 3, 2};
        iArr[20] = new int[]{2, 2, 1, 2, 3, 1};
        iArr[21] = new int[]{2, 1, 3, 2, 1, 2};
        iArr[22] = new int[]{2, 2, 3, 1, 1, 2};
        iArr[23] = new int[]{3, 1, 2, 1, 3, 1};
        iArr[24] = new int[]{3, 1, 1, 2, 2, 2};
        iArr[25] = new int[]{3, 2, 1, 1, 2, 2};
        iArr[26] = new int[]{3, 2, 1, 2, 2, 1};
        iArr[27] = new int[]{3, 1, 2, 2, 1, 2};
        iArr[28] = new int[]{3, 2, 2, 1, 1, 2};
        iArr[29] = new int[]{3, 2, 2, 2, 1, 1};
        iArr[30] = new int[]{2, 1, 2, 1, 2, 3};
        iArr[31] = new int[]{2, 1, 2, 3, 2, 1};
        iArr[32] = new int[]{2, 3, 2, 1, 2, 1};
        iArr[33] = new int[]{1, 1, 1, 3, 2, 3};
        iArr[34] = new int[]{1, 3, 1, 1, 2, 3};
        iArr[35] = new int[]{1, 3, 1, 3, 2, 1};
        iArr[36] = new int[]{1, 1, 2, 3, 1, 3};
        iArr[37] = new int[]{1, 3, 2, 1, 1, 3};
        iArr[38] = new int[]{1, 3, 2, 3, 1, 1};
        iArr[39] = new int[]{2, 1, 1, 3, 1, 3};
        iArr[40] = new int[]{2, 3, 1, 1, 1, 3};
        iArr[41] = new int[]{2, 3, 1, 3, 1, 1};
        iArr[42] = new int[]{1, 1, 2, 1, 3, 3};
        iArr[43] = new int[]{1, 1, 2, 3, 3, 1};
        iArr[44] = new int[]{1, 3, 2, 1, 3, 1};
        iArr[45] = new int[]{1, 1, 3, 1, 2, 3};
        iArr[46] = new int[]{1, 1, 3, 3, 2, 1};
        iArr[47] = new int[]{1, 3, 3, 1, 2, 1};
        iArr[48] = new int[]{3, 1, 3, 1, 2, 1};
        iArr[49] = new int[]{2, 1, 1, 3, 3, 1};
        iArr[50] = new int[]{2, 3, 1, 1, 3, 1};
        iArr[51] = new int[]{2, 1, 3, 1, 1, 3};
        iArr[52] = new int[]{2, 1, 3, 3, 1, 1};
        iArr[53] = new int[]{2, 1, 3, 1, 3, 1};
        iArr[54] = new int[]{3, 1, 1, 1, 2, 3};
        iArr[55] = new int[]{3, 1, 1, 3, 2, 1};
        iArr[56] = new int[]{3, 3, 1, 1, 2, 1};
        iArr[57] = new int[]{3, 1, 2, 1, 1, 3};
        iArr[58] = new int[]{3, 1, 2, 3, 1, 1};
        iArr[59] = new int[]{3, 3, 2, 1, 1, 1};
        iArr[60] = new int[]{3, 1, 4, 1, 1, 1};
        iArr[61] = new int[]{2, 2, 1, 4, 1, 1};
        iArr[62] = new int[]{4, 3, 1, 1, 1, 1};
        iArr[63] = new int[]{1, 1, 1, 2, 2, 4};
        iArr[64] = new int[]{1, 1, 1, 4, 2, 2};
        iArr[65] = new int[]{1, 2, 1, 1, 2, 4};
        iArr[66] = new int[]{1, 2, 1, 4, 2, 1};
        iArr[67] = new int[]{1, 4, 1, 1, 2, 2};
        iArr[68] = new int[]{1, 4, 1, 2, 2, 1};
        iArr[69] = new int[]{1, 1, 2, 2, 1, 4};
        iArr[70] = new int[]{1, 1, 2, 4, 1, 2};
        iArr[71] = new int[]{1, 2, 2, 1, 1, 4};
        iArr[72] = new int[]{1, 2, 2, 4, 1, 1};
        iArr[73] = new int[]{1, 4, 2, 1, 1, 2};
        iArr[74] = new int[]{1, 4, 2, 2, 1, 1};
        iArr[75] = new int[]{2, 4, 1, 2, 1, 1};
        iArr[76] = new int[]{2, 2, 1, 1, 1, 4};
        iArr[77] = new int[]{4, 1, 3, 1, 1, 1};
        iArr[78] = new int[]{2, 4, 1, 1, 1, 2};
        iArr[79] = new int[]{1, 3, 4, 1, 1, 1};
        iArr[80] = new int[]{1, 1, 1, 2, 4, 2};
        iArr[81] = new int[]{1, 2, 1, 1, 4, 2};
        iArr[82] = new int[]{1, 2, 1, 2, 4, 1};
        iArr[83] = new int[]{1, 1, 4, 2, 1, 2};
        iArr[84] = new int[]{1, 2, 4, 1, 1, 2};
        iArr[85] = new int[]{1, 2, 4, 2, 1, 1};
        iArr[86] = new int[]{4, 1, 1, 2, 1, 2};
        iArr[87] = new int[]{4, 2, 1, 1, 1, 2};
        iArr[88] = new int[]{4, 2, 1, 2, 1, 1};
        iArr[89] = new int[]{2, 1, 2, 1, 4, 1};
        iArr[90] = new int[]{2, 1, 4, 1, 2, 1};
        iArr[91] = new int[]{4, 1, 2, 1, 2, 1};
        iArr[92] = new int[]{1, 1, 1, 1, 4, 3};
        iArr[93] = new int[]{1, 1, 1, 3, 4, 1};
        iArr[94] = new int[]{1, 3, 1, 1, 4, 1};
        iArr[95] = new int[]{1, 1, 4, 1, 1, 3};
        iArr[CODE_FNC_3] = new int[]{1, 1, 4, 3, 1, 1};
        iArr[CODE_FNC_2] = new int[]{4, 1, 1, 1, 1, 3};
        iArr[CODE_SHIFT] = new int[]{4, 1, 1, 3, 1, 1};
        iArr[99] = new int[]{1, 1, 3, 1, 4, 1};
        iArr[100] = new int[]{1, 1, 4, 1, 3, 1};
        iArr[101] = new int[]{3, 1, 1, 1, 4, 1};
        iArr[102] = new int[]{4, 1, 1, 1, 3, 1};
        iArr[103] = new int[]{2, 1, 1, 4, 1, 2};
        iArr[CODE_START_B] = new int[]{2, 1, 1, 2, 1, 4};
        iArr[CODE_START_C] = new int[]{2, 1, 1, 2, 3, 2};
        iArr[CODE_STOP] = new int[]{2, 3, 3, 1, 1, 1, 2};
        CODE_PATTERNS = iArr;
    }

    private static int[] findStartPattern(BitArray row) throws NotFoundException {
        int width = row.getSize();
        int rowOffset = row.getNextSet(0);
        int counterPosition = 0;
        int[] counters = new int[6];
        int patternStart = rowOffset;
        boolean isWhite = false;
        int patternLength = counters.length;
        for (int i = rowOffset; i < width; i++) {
            if (row.get(i) ^ isWhite) {
                counters[counterPosition] = counters[counterPosition] + 1;
            } else {
                if (counterPosition == patternLength - 1) {
                    int bestVariance = 64;
                    int bestMatch = -1;
                    for (int startCode = 103; startCode <= CODE_START_C; startCode++) {
                        int variance = patternMatchVariance(counters, CODE_PATTERNS[startCode], MAX_INDIVIDUAL_VARIANCE);
                        if (variance < bestVariance) {
                            bestVariance = variance;
                            bestMatch = startCode;
                        }
                    }
                    if (bestMatch < 0 || !row.isRange(Math.max(0, patternStart - ((i - patternStart) / 2)), patternStart, false)) {
                        patternStart += counters[0] + counters[1];
                        System.arraycopy(counters, 2, counters, 0, patternLength - 2);
                        counters[patternLength - 2] = 0;
                        counters[patternLength - 1] = 0;
                        counterPosition--;
                    } else {
                        return new int[]{patternStart, i, bestMatch};
                    }
                } else {
                    counterPosition++;
                }
                counters[counterPosition] = 1;
                isWhite = !isWhite;
            }
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static int decodeCode(BitArray row, int[] counters, int rowOffset) throws NotFoundException {
        recordPattern(row, rowOffset, counters);
        int bestVariance = 64;
        int bestMatch = -1;
        for (int d = 0; d < CODE_PATTERNS.length; d++) {
            int variance = patternMatchVariance(counters, CODE_PATTERNS[d], MAX_INDIVIDUAL_VARIANCE);
            if (variance < bestVariance) {
                bestVariance = variance;
                bestMatch = d;
            }
        }
        if (bestMatch >= 0) {
            return bestMatch;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0054  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.zxing.Result decodeRow(int r39, com.google.zxing.common.BitArray r40, java.util.Map<com.google.zxing.DecodeHintType, ?> r41) throws com.google.zxing.NotFoundException, com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            r38 = this;
            if (r41 == 0) goto L_0x001f
            com.google.zxing.DecodeHintType r32 = com.google.zxing.DecodeHintType.ASSUME_GS1
            r0 = r41
            r1 = r32
            boolean r32 = r0.containsKey(r1)
            if (r32 == 0) goto L_0x001f
            r9 = 1
        L_0x000f:
            int[] r30 = findStartPattern(r40)
            r32 = 2
            r29 = r30[r32]
            switch(r29) {
                case 103: goto L_0x0021;
                case 104: goto L_0x0095;
                case 105: goto L_0x0098;
                default: goto L_0x001a;
            }
        L_0x001a:
            com.google.zxing.FormatException r32 = com.google.zxing.FormatException.getFormatInstance()
            throw r32
        L_0x001f:
            r9 = 0
            goto L_0x000f
        L_0x0021:
            r8 = 101(0x65, float:1.42E-43)
        L_0x0023:
            r12 = 0
            r15 = 0
            java.lang.StringBuilder r26 = new java.lang.StringBuilder
            r32 = 20
            r0 = r26
            r1 = r32
            r0.<init>(r1)
            java.util.ArrayList r24 = new java.util.ArrayList
            r32 = 20
            r0 = r24
            r1 = r32
            r0.<init>(r1)
            r32 = 0
            r18 = r30[r32]
            r32 = 1
            r22 = r30[r32]
            r32 = 6
            r0 = r32
            int[] r11 = new int[r0]
            r17 = 0
            r7 = 0
            r6 = r29
            r21 = 0
            r16 = 1
        L_0x0052:
            if (r12 != 0) goto L_0x01b6
            r31 = r15
            r15 = 0
            r17 = r7
            r0 = r40
            r1 = r22
            int r7 = decodeCode(r0, r11, r1)
            byte r0 = (byte) r7
            r32 = r0
            java.lang.Byte r32 = java.lang.Byte.valueOf(r32)
            r0 = r24
            r1 = r32
            r0.add(r1)
            r32 = 106(0x6a, float:1.49E-43)
            r0 = r32
            if (r7 == r0) goto L_0x0077
            r16 = 1
        L_0x0077:
            r32 = 106(0x6a, float:1.49E-43)
            r0 = r32
            if (r7 == r0) goto L_0x0083
            int r21 = r21 + 1
            int r32 = r21 * r7
            int r6 = r6 + r32
        L_0x0083:
            r18 = r22
            r5 = r11
            int r0 = r5.length
            r20 = r0
            r14 = 0
        L_0x008a:
            r0 = r20
            if (r14 >= r0) goto L_0x009b
            r10 = r5[r14]
            int r22 = r22 + r10
            int r14 = r14 + 1
            goto L_0x008a
        L_0x0095:
            r8 = 100
            goto L_0x0023
        L_0x0098:
            r8 = 99
            goto L_0x0023
        L_0x009b:
            switch(r7) {
                case 103: goto L_0x00ac;
                case 104: goto L_0x00ac;
                case 105: goto L_0x00ac;
                default: goto L_0x009e;
            }
        L_0x009e:
            switch(r8) {
                case 99: goto L_0x0160;
                case 100: goto L_0x010f;
                case 101: goto L_0x00b1;
                default: goto L_0x00a1;
            }
        L_0x00a1:
            if (r31 == 0) goto L_0x0052
            r32 = 101(0x65, float:1.42E-43)
            r0 = r32
            if (r8 != r0) goto L_0x01b2
            r8 = 100
        L_0x00ab:
            goto L_0x0052
        L_0x00ac:
            com.google.zxing.FormatException r32 = com.google.zxing.FormatException.getFormatInstance()
            throw r32
        L_0x00b1:
            r32 = 64
            r0 = r32
            if (r7 >= r0) goto L_0x00c6
            int r32 = r7 + 32
            r0 = r32
            char r0 = (char) r0
            r32 = r0
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x00c6:
            r32 = 96
            r0 = r32
            if (r7 >= r0) goto L_0x00db
            int r32 = r7 + -64
            r0 = r32
            char r0 = (char) r0
            r32 = r0
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x00db:
            r32 = 106(0x6a, float:1.49E-43)
            r0 = r32
            if (r7 == r0) goto L_0x00e3
            r16 = 0
        L_0x00e3:
            switch(r7) {
                case 96: goto L_0x00a1;
                case 97: goto L_0x00a1;
                case 98: goto L_0x00e7;
                case 99: goto L_0x010a;
                case 100: goto L_0x0107;
                case 101: goto L_0x00a1;
                case 102: goto L_0x00eb;
                case 103: goto L_0x00e6;
                case 104: goto L_0x00e6;
                case 105: goto L_0x00e6;
                case 106: goto L_0x010d;
                default: goto L_0x00e6;
            }
        L_0x00e6:
            goto L_0x00a1
        L_0x00e7:
            r15 = 1
            r8 = 100
            goto L_0x00a1
        L_0x00eb:
            if (r9 == 0) goto L_0x00a1
            int r32 = r26.length()
            if (r32 != 0) goto L_0x00fd
            java.lang.String r32 = "]C1"
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x00fd:
            r32 = 29
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x0107:
            r8 = 100
            goto L_0x00a1
        L_0x010a:
            r8 = 99
            goto L_0x00a1
        L_0x010d:
            r12 = 1
            goto L_0x00a1
        L_0x010f:
            r32 = 96
            r0 = r32
            if (r7 >= r0) goto L_0x0125
            int r32 = r7 + 32
            r0 = r32
            char r0 = (char) r0
            r32 = r0
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x0125:
            r32 = 106(0x6a, float:1.49E-43)
            r0 = r32
            if (r7 == r0) goto L_0x012d
            r16 = 0
        L_0x012d:
            switch(r7) {
                case 96: goto L_0x00a1;
                case 97: goto L_0x00a1;
                case 98: goto L_0x0132;
                case 99: goto L_0x0159;
                case 100: goto L_0x00a1;
                case 101: goto L_0x0155;
                case 102: goto L_0x0137;
                case 103: goto L_0x0130;
                case 104: goto L_0x0130;
                case 105: goto L_0x0130;
                case 106: goto L_0x015d;
                default: goto L_0x0130;
            }
        L_0x0130:
            goto L_0x00a1
        L_0x0132:
            r15 = 1
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x00a1
        L_0x0137:
            if (r9 == 0) goto L_0x00a1
            int r32 = r26.length()
            if (r32 != 0) goto L_0x014a
            java.lang.String r32 = "]C1"
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x014a:
            r32 = 29
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x0155:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x00a1
        L_0x0159:
            r8 = 99
            goto L_0x00a1
        L_0x015d:
            r12 = 1
            goto L_0x00a1
        L_0x0160:
            r32 = 100
            r0 = r32
            if (r7 >= r0) goto L_0x017c
            r32 = 10
            r0 = r32
            if (r7 >= r0) goto L_0x0175
            r32 = 48
            r0 = r26
            r1 = r32
            r0.append(r1)
        L_0x0175:
            r0 = r26
            r0.append(r7)
            goto L_0x00a1
        L_0x017c:
            r32 = 106(0x6a, float:1.49E-43)
            r0 = r32
            if (r7 == r0) goto L_0x0184
            r16 = 0
        L_0x0184:
            switch(r7) {
                case 100: goto L_0x0189;
                case 101: goto L_0x01ab;
                case 102: goto L_0x018d;
                case 103: goto L_0x0187;
                case 104: goto L_0x0187;
                case 105: goto L_0x0187;
                case 106: goto L_0x01af;
                default: goto L_0x0187;
            }
        L_0x0187:
            goto L_0x00a1
        L_0x0189:
            r8 = 100
            goto L_0x00a1
        L_0x018d:
            if (r9 == 0) goto L_0x00a1
            int r32 = r26.length()
            if (r32 != 0) goto L_0x01a0
            java.lang.String r32 = "]C1"
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x01a0:
            r32 = 29
            r0 = r26
            r1 = r32
            r0.append(r1)
            goto L_0x00a1
        L_0x01ab:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x00a1
        L_0x01af:
            r12 = 1
            goto L_0x00a1
        L_0x01b2:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x00ab
        L_0x01b6:
            r0 = r40
            r1 = r22
            int r22 = r0.getNextUnset(r1)
            int r32 = r40.getSize()
            int r33 = r22 - r18
            int r33 = r33 / 2
            int r33 = r33 + r22
            int r32 = java.lang.Math.min(r32, r33)
            r33 = 0
            r0 = r40
            r1 = r22
            r2 = r32
            r3 = r33
            boolean r32 = r0.isRange(r1, r2, r3)
            if (r32 != 0) goto L_0x01e1
            com.google.zxing.NotFoundException r32 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r32
        L_0x01e1:
            int r32 = r21 * r17
            int r6 = r6 - r32
            int r32 = r6 % 103
            r0 = r32
            r1 = r17
            if (r0 == r1) goto L_0x01f2
            com.google.zxing.ChecksumException r32 = com.google.zxing.ChecksumException.getChecksumInstance()
            throw r32
        L_0x01f2:
            int r27 = r26.length()
            if (r27 != 0) goto L_0x01fd
            com.google.zxing.NotFoundException r32 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r32
        L_0x01fd:
            if (r27 <= 0) goto L_0x0212
            if (r16 == 0) goto L_0x0212
            r32 = 99
            r0 = r32
            if (r8 != r0) goto L_0x0250
            int r32 = r27 + -2
            r0 = r26
            r1 = r32
            r2 = r27
            r0.delete(r1, r2)
        L_0x0212:
            r32 = 1
            r32 = r30[r32]
            r33 = 0
            r33 = r30[r33]
            int r32 = r32 + r33
            r0 = r32
            float r0 = (float) r0
            r32 = r0
            r33 = 1073741824(0x40000000, float:2.0)
            float r19 = r32 / r33
            int r32 = r22 + r18
            r0 = r32
            float r0 = (float) r0
            r32 = r0
            r33 = 1073741824(0x40000000, float:2.0)
            float r28 = r32 / r33
            int r25 = r24.size()
            r0 = r25
            byte[] r0 = new byte[r0]
            r23 = r0
            r13 = 0
        L_0x023b:
            r0 = r25
            if (r13 >= r0) goto L_0x025c
            r0 = r24
            java.lang.Object r32 = r0.get(r13)
            java.lang.Byte r32 = (java.lang.Byte) r32
            byte r32 = r32.byteValue()
            r23[r13] = r32
            int r13 = r13 + 1
            goto L_0x023b
        L_0x0250:
            int r32 = r27 + -1
            r0 = r26
            r1 = r32
            r2 = r27
            r0.delete(r1, r2)
            goto L_0x0212
        L_0x025c:
            com.google.zxing.Result r32 = new com.google.zxing.Result
            java.lang.String r33 = r26.toString()
            r34 = 2
            r0 = r34
            com.google.zxing.ResultPoint[] r0 = new com.google.zxing.ResultPoint[r0]
            r34 = r0
            r35 = 0
            com.google.zxing.ResultPoint r36 = new com.google.zxing.ResultPoint
            r0 = r39
            float r0 = (float) r0
            r37 = r0
            r0 = r36
            r1 = r19
            r2 = r37
            r0.<init>(r1, r2)
            r34[r35] = r36
            r35 = 1
            com.google.zxing.ResultPoint r36 = new com.google.zxing.ResultPoint
            r0 = r39
            float r0 = (float) r0
            r37 = r0
            r0 = r36
            r1 = r28
            r2 = r37
            r0.<init>(r1, r2)
            r34[r35] = r36
            com.google.zxing.BarcodeFormat r35 = com.google.zxing.BarcodeFormat.CODE_128
            r0 = r32
            r1 = r33
            r2 = r23
            r3 = r34
            r4 = r35
            r0.<init>(r1, r2, r3, r4)
            return r32
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.oned.Code128Reader.decodeRow(int, com.google.zxing.common.BitArray, java.util.Map):com.google.zxing.Result");
    }
}
