package com.quanleimu.activity.wxapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.baixing.activity.PersonalActivity;
import com.baixing.broadcast.CommonIntentAction;
import com.baixing.data.GlobalDataManager;
import com.baixing.sharing.SharingCenter;
import com.baixing.util.ViewUtil;
import com.tencent.mm.sdk.openapi.BaseReq;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.ShowMessageFromWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXAppExtendObject;
import com.tencent.mm.sdk.platformtools.Log;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {
    private static final String WX_APP_ID = "wx862b30c868401dbc";
    private IWXAPI mApi;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(null);
        this.mApi = WXAPIFactory.createWXAPI(this, "wx862b30c868401dbc", false);
        this.mApi.registerApp("wx862b30c868401dbc");
        this.mApi.handleIntent(getIntent(), this);
        super.onCreate(savedInstanceState);
        setVisible(false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0019  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isRunning() {
        /*
            r6 = this;
            java.lang.String r5 = "activity"
            java.lang.Object r0 = r6.getSystemService(r5)
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            r5 = 2147483647(0x7fffffff, float:NaN)
            java.util.List r4 = r0.getRunningTasks(r5)
            java.util.Iterator r1 = r4.iterator()
        L_0x0013:
            boolean r5 = r1.hasNext()
            if (r5 == 0) goto L_0x0044
            java.lang.Object r3 = r1.next()
            android.app.ActivityManager$RunningTaskInfo r3 = (android.app.ActivityManager.RunningTaskInfo) r3
            android.content.ComponentName r5 = r3.topActivity
            java.lang.String r2 = r5.toString()
            java.lang.String r5 = "packagename"
            com.tencent.mm.sdk.platformtools.Log.d(r5, r2)
            java.lang.String r5 = "ComponentInfo{com.quanleimu.activity/com.baixing.activity.MainActivity}"
            boolean r5 = r2.equals(r5)
            if (r5 != 0) goto L_0x0042
            java.lang.String r5 = "ComponentInfo{com.quanleimu.activity/com.baixing.activity.PersonalActivity}"
            boolean r5 = r2.equals(r5)
            if (r5 != 0) goto L_0x0042
            java.lang.String r5 = "ComponentInfo{com.quanleimu.activity/com.baixing.activity.PostActivity}"
            boolean r5 = r2.equals(r5)
            if (r5 == 0) goto L_0x0013
        L_0x0042:
            r5 = 1
        L_0x0043:
            return r5
        L_0x0044:
            r5 = 0
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.quanleimu.activity.wxapi.WXEntryActivity.isRunning():boolean");
    }

    public void onReq(BaseReq req) {
        switch (req.getType()) {
            case 3:
            default:
                return;
            case 4:
                if (req instanceof ShowMessageFromWX.Req) {
                    Log.d("onReq", "is instanceof ShowMessageFromWX.Req");
                } else {
                    Log.d("on Req", "not instance of ShowMessageFromWX.Req!!!!");
                }
                WXAppExtendObject obj = (WXAppExtendObject) ((ShowMessageFromWX.Req) req).message.mediaObject;
                String result = "";
                if (obj == null) {
                    Log.d("onReq", "extend obj is null!!!!!");
                } else {
                    Log.d("onReq", "extend obj is not ~~~~~ null!!!!!");
                    if (obj.fileData != null) {
                        Log.d("onReq", "extend obj's fileData is not ~~~~~ null!!!!!");
                        result = obj.fileData.toString();
                    } else if (obj.extInfo != null) {
                        Log.d("onReq", "extend obj's extInfo is not null!!!!!");
                    }
                    if (obj.filePath != null) {
                        Log.d("onReq", obj.filePath);
                        try {
                            FileInputStream instream = new FileInputStream(obj.filePath);
                            if (instream != null) {
                                BufferedReader buffreader = new BufferedReader(new InputStreamReader(instream));
                                while (true) {
                                    String line = buffreader.readLine();
                                    if (line != null) {
                                        result = result + line;
                                    }
                                }
                            }
                            instream.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("onReq", result);
                        Log.d("onReq", "extend obj's filepath is not null!!!!!");
                    }
                }
                Log.d("on Req, mediaObject", obj.toString());
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFromWX", true);
                bundle.putString("detailFromWX", result);
                if (!isRunning()) {
                    Log.d("", "not running!!!!");
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.quanleimu.activity");
                    launchIntent.putExtras(bundle);
                    startActivity(launchIntent);
                } else {
                    Log.d("", "running~~~!!~!~!~!~");
                    Intent intent = new Intent(this, PersonalActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                finish();
                return;
        }
    }

    public void onResp(BaseResp resp) {
        String result;
        switch (resp.errCode) {
            case -4:
                result = "发送被拒绝";
                SharingCenter.trackShareResult("weixin", false, result);
                break;
            case -3:
            case -1:
            default:
                result = "发送返回";
                SharingCenter.trackShareResult("weixin", false, result);
                break;
            case -2:
                result = "发送取消";
                SharingCenter.trackShareResult("weixin", false, result);
                break;
            case 0:
                result = "发送成功";
                Context ctx = GlobalDataManager.getInstance().getApplicationContext();
                if (ctx != null) {
                    ctx.sendBroadcast(new Intent(CommonIntentAction.ACTION_BROADCAST_SHARE_SUCCEED));
                }
                SharingCenter.trackShareResult("weixin", true, null);
                break;
        }
        ViewUtil.showToast(this, result, true);
        finish();
    }
}
