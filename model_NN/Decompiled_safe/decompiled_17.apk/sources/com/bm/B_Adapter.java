package com.bm;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import java.util.ArrayList;

public class B_Adapter extends BaseAdapter {
    private static boolean isLoadingEnd = true;
    private static boolean refreshLoadingImage = false;
    private B_Handler b_Handler = null;
    private int endIndex;
    private ArrayList<RelativeLayout> layoutData = null;
    private ArrayList<B_ListData> listData = null;
    private B_AdapterListeren listeren = null;
    private Context m_Context = null;
    private HandlerThread m_Ht = null;
    private B_Handler main_Handler = null;
    private int startIndex;
    private String tmpPath = "tmp/";
    private B_UpdataImageViewBitmapListeren updataListeren = null;

    public B_Adapter(Context context, ArrayList<B_ListData> listData2, ArrayList<RelativeLayout> layoutData2, String threadName, String tmpPath2) {
        this.listData = listData2;
        this.layoutData = layoutData2;
        this.m_Context = context;
        this.m_Ht = new HandlerThread(threadName);
        this.m_Ht.start();
        this.b_Handler = new B_Handler(this.m_Ht.getLooper());
        this.main_Handler = new B_Handler(context.getMainLooper());
        this.tmpPath = tmpPath2;
    }

    public void initLoadingImage(int startIndex2, int endIndex2) {
        refreshLoadingImage = true;
        this.startIndex = startIndex2;
        this.endIndex = endIndex2;
        Message m = this.b_Handler.obtainMessage();
        m.what = 0;
        m.sendToTarget();
    }

    public void refreshLoadingImage() {
        do {
        } while (!isLoadingEnd);
        refreshLoadingImage = false;
        startLoadingImage(this.startIndex);
    }

    public void startLoadingImage(int id) {
        isLoadingEnd = false;
        if (!this.listData.get(id).isLoading) {
            Bitmap bit = QE_Downland.getUrlImage(this.listData.get(id).imageUrl);
            if (bit == null) {
                this.listData.get(id).isLoading = false;
            } else {
                this.listData.get(id).isLoading = true;
                if (B_ManageSD.sdCardExist) {
                    String path = "image" + System.currentTimeMillis();
                    B_ManageSD.writeBitmap(this.tmpPath, path, bit);
                    this.listData.get(id).bitmapPathMap.put(this.listData.get(id).imageUrl, String.valueOf(this.tmpPath) + path);
                } else {
                    this.listData.get(id).bitmapMap.put(this.listData.get(id).imageUrl, bit);
                }
            }
        }
        Message m = this.main_Handler.obtainMessage();
        m.what = 1;
        m.arg1 = id;
        m.sendToTarget();
    }

    public void endLoadingImage(int startIndex2) {
        if (this.updataListeren != null) {
            if (B_ManageSD.sdCardExist) {
                this.updataListeren.updataBitmap(startIndex2, B_ManageSD.getBitmap(this.listData.get(startIndex2).bitmapPathMap.get(this.listData.get(startIndex2).imageUrl)), this.layoutData.get(startIndex2));
            } else {
                this.updataListeren.updataBitmap(startIndex2, this.listData.get(startIndex2).bitmapMap.get(this.listData.get(startIndex2).imageUrl), this.layoutData.get(startIndex2));
            }
        }
        isLoadingEnd = true;
        this.startIndex++;
        if (!refreshLoadingImage && this.startIndex < this.endIndex) {
            Message m = this.b_Handler.obtainMessage();
            m.what = 2;
            m.arg1 = this.startIndex;
            m.sendToTarget();
        }
    }

    public int getCount() {
        return this.layoutData.size();
    }

    public Object getItem(int position) {
        return this.layoutData.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.listeren != null) {
            return this.listeren.getView(position, this.listData.get(position), this.layoutData.get(position));
        }
        return null;
    }

    public void setB_AdapterListeren(B_AdapterListeren l) {
        this.listeren = l;
    }

    public void setB_UpdataImageViewBitmapListeren(B_UpdataImageViewBitmapListeren l) {
        this.updataListeren = l;
    }

    public class B_Handler extends Handler {
        public B_Handler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            int id = msg.arg1;
            switch (msg.what) {
                case 0:
                    B_Adapter.this.refreshLoadingImage();
                    return;
                case 1:
                    B_Adapter.this.endLoadingImage(id);
                    return;
                case 2:
                    B_Adapter.this.startLoadingImage(id);
                    return;
                default:
                    return;
            }
        }
    }
}
