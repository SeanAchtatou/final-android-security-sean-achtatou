package com.bm;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class B_ManageSD {
    private static String CODE_PATH = "/default";
    private static String PATH = "";
    public static boolean sdCardExist = false;

    public static boolean getSDPATH() {
        String path = "";
        sdCardExist = Environment.getExternalStorageState().equals("mounted");
        if (sdCardExist) {
            path = Environment.getExternalStorageDirectory().toString();
        } else {
            System.out.println("com.tools.QE_ManageSD ---> not find SD");
        }
        PATH = path;
        return sdCardExist;
    }

    public static void setCode(String codePath) {
        CODE_PATH = codePath;
        if (sdCardExist) {
            File file = new File(String.valueOf(PATH) + CODE_PATH);
            if (!file.exists()) {
                file.mkdir();
            }
        }
    }

    public static boolean writeBitmap(String tempPaht, String fileName, Bitmap bit) {
        boolean bool = false;
        if (bit == null) {
            return false;
        }
        if (sdCardExist) {
            File file = new File(String.valueOf(PATH) + CODE_PATH + tempPaht);
            if (!file.exists()) {
                file.mkdir();
            }
            try {
                File fileText = new File(String.valueOf(PATH) + CODE_PATH + tempPaht + fileName);
                fileText.createNewFile();
                bit.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(fileText));
                bool = true;
            } catch (FileNotFoundException | IOException e) {
            }
        }
        return bool;
    }

    public static Bitmap getBitmap(String fileName) {
        Bitmap bit = null;
        File fileText = new File(String.valueOf(PATH) + CODE_PATH + fileName);
        if (!fileText.exists()) {
            return null;
        }
        try {
            FileInputStream fis = new FileInputStream(fileText);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            bit = BitmapFactory.decodeByteArray(data, 0, data.length);
            fis.close();
            return bit;
        } catch (FileNotFoundException | IOException e) {
            return bit;
        }
    }

    public static void delFile(String fileName) {
        File file = new File(String.valueOf(PATH) + CODE_PATH + fileName);
        file.delete();
        if (file.isDirectory()) {
            String[] children = file.list();
            for (String file2 : children) {
                File temp = new File(file, file2);
                if (temp.isDirectory()) {
                    delFile(temp.getPath());
                } else {
                    temp.delete();
                }
            }
            file.delete();
        }
    }
}
