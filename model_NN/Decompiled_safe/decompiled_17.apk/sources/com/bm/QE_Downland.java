package com.bm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class QE_Downland {
    public static String getXMLData(Context context, String urlString) {
        StringBuffer sb = new StringBuffer("");
        System.out.println(urlString);
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info == null) {
                Toast.makeText(context, "网络连接错误，请稍后再试。", 1).show();
                return "";
            } else if (info.getState() != NetworkInfo.State.CONNECTED) {
                Toast.makeText(context, "网络连接错误，请稍后再试。", 1).show();
                return "";
            } else {
                try {
                    HttpURLConnection urlConn = (HttpURLConnection) new URL(urlString).openConnection();
                    urlConn.setConnectTimeout(30000);
                    urlConn.connect();
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                    while (true) {
                        try {
                            String line = br.readLine();
                            if (line == null) {
                                return sb.toString();
                            }
                            line.trim();
                            sb.append(line);
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e2) {
                    Toast.makeText(context, "网络连接失败", 1).show();
                    return "";
                }
            }
        } else {
            Toast.makeText(context, "网络连接错误，请稍后再试。", 1).show();
            return "";
        }
    }

    public static Bitmap getUrlImage(String urlString) {
        InputStream is = null;
        try {
            HttpURLConnection urlConn = (HttpURLConnection) new URL(urlString).openConnection();
            urlConn.setConnectTimeout(30000);
            is = urlConn.getInputStream();
        } catch (MalformedURLException e) {
            System.out.println("tools.download.QE_Http_Download ---->  getUrlImage error");
            e.printStackTrace();
        } catch (IOException e2) {
            System.out.println("tools.download.QE_Http_Download ---->  getUrlImage error");
            e2.printStackTrace();
        }
        return BitmapFactory.decodeStream(is);
    }

    public static byte[] getUrlImageDataArray(String urlString) {
        InputStream is = null;
        System.out.println(urlString);
        try {
            HttpURLConnection urlConn = (HttpURLConnection) new URL(urlString).openConnection();
            urlConn.setConnectTimeout(30000);
            is = urlConn.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] data = null;
        try {
            data = new byte[is.available()];
            is.read(data, 0, data.length);
            return data;
        } catch (IOException e2) {
            e2.printStackTrace();
            return data;
        }
    }
}
