package com.bm;

import android.graphics.Bitmap;
import java.util.HashMap;

public class B_ListData {
    public HashMap<String, Bitmap> bitmapMap;
    public HashMap<String, String> bitmapPathMap;
    public HashMap<String, String> dataMap;
    public String imageUrl;
    public boolean isLoading;

    public B_ListData(String[] key) {
        this.dataMap = null;
        this.bitmapPathMap = null;
        this.bitmapMap = null;
        this.imageUrl = "";
        this.isLoading = false;
        this.dataMap = new HashMap<>();
        this.bitmapPathMap = new HashMap<>();
        this.bitmapMap = new HashMap<>();
        for (String put : key) {
            this.dataMap.put(put, "");
        }
    }

    public void init(String imgKey) {
        this.imageUrl = this.dataMap.get(imgKey);
        for (int i = 0; i < this.dataMap.size(); i++) {
            this.bitmapPathMap.put(this.imageUrl, "");
        }
    }
}
