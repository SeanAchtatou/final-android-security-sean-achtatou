package com.baidu.mapapi;

import android.content.Context;
import android.util.Log;
import java.io.IOException;

public class BMapManager {
    static boolean b = false;
    Mj a = null;
    private Context c = null;

    public BMapManager(Context context) {
        this.c = context;
    }

    public void destroy() {
        if (b) {
            stop();
        }
        b = false;
        if (this.a != null) {
            if (Mj.f != null) {
                try {
                    Mj.f.close();
                    Mj.f = null;
                } catch (IOException e) {
                    Log.d("baidumap", e.getMessage());
                    Mj.f = null;
                }
            }
            this.a.UnInitMapApiEngine();
            this.a = null;
        }
    }

    public MKLocationManager getLocationManager() {
        return Mj.b;
    }

    public boolean init(String str, MKGeneralListener mKGeneralListener) {
        if (str == null) {
            return false;
        }
        b = false;
        if (this.a != null) {
            return false;
        }
        this.a = new Mj(this, this.c);
        if (!this.a.a(str, mKGeneralListener)) {
            this.a = null;
            return false;
        } else if (!Mj.b.a(this)) {
            return false;
        } else {
            Mj.b.b();
            return true;
        }
    }

    public boolean start() {
        if (b) {
            return true;
        }
        if (this.a == null) {
            return false;
        }
        if (!this.a.a()) {
            return false;
        }
        b = true;
        return true;
    }

    public boolean stop() {
        if (!b) {
            return true;
        }
        if (this.a == null) {
            return false;
        }
        if (!this.a.b()) {
            return false;
        }
        b = false;
        return true;
    }
}
