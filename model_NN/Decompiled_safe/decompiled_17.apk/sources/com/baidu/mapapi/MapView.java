package com.baidu.mapapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ZoomControls;
import com.tencent.tauth.Constants;
import java.io.InputStream;
import java.util.List;
import org.apache.commons.httpclient.HttpStatus;

public class MapView extends ViewGroup {
    private static int g = 0;
    d a = new d(this);
    a b = null;
    private GeoPoint c = new GeoPoint(0, 0);
    private int d = 12;
    private int e = 0;
    private int f = 0;
    private boolean h = false;
    private boolean i = false;
    private Message j = null;
    private Runnable k = null;
    private MapActivity l = null;
    private MapController m;
    private ZoomControls n = new ZoomControls(getContext());
    private ImageView o = new ImageView(getContext());

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public static final int BOTTOM = 80;
        public static final int BOTTOM_CENTER = 81;
        public static final int CENTER = 17;
        public static final int CENTER_HORIZONTAL = 1;
        public static final int CENTER_VERTICAL = 16;
        public static final int LEFT = 3;
        public static final int MODE_MAP = 0;
        public static final int MODE_VIEW = 1;
        public static final int RIGHT = 5;
        public static final int TOP = 48;
        public static final int TOP_LEFT = 51;
        public int alignment;
        public int mode;
        public GeoPoint point;
        public int x;
        public int y;

        public LayoutParams(int i, int i2, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 1;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3) {
            this(i, i2, geoPoint, 0, 0, i3);
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 0;
            this.point = geoPoint;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }
    }

    enum a {
        DRAW_RETICLE_NEVER,
        DRAW_RETICLE_OVER,
        DRAW_RETICLE_UNDER
    }

    public MapView(Context context) {
        super(context);
        a(context);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public MapView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(View view, ViewGroup.LayoutParams layoutParams) {
        view.measure(this.e, this.f);
        int i2 = layoutParams.width;
        int i3 = layoutParams.height;
        int measuredWidth = i2 == -1 ? this.e : i2 == -2 ? view.getMeasuredWidth() : i2;
        if (i3 == -1) {
            i3 = this.f;
        } else if (i3 == -2) {
            i3 = view.getMeasuredHeight();
        }
        if (checkLayoutParams(layoutParams)) {
            LayoutParams layoutParams2 = (LayoutParams) layoutParams;
            int i4 = layoutParams2.x;
            int i5 = layoutParams2.y;
            if (layoutParams2.mode == 0 && layoutParams2.point != null) {
                Point pixels = getProjection().toPixels(layoutParams2.point, null);
                i4 = pixels.x + layoutParams2.x;
                i5 = pixels.y + layoutParams2.y;
            }
            switch (layoutParams2.alignment) {
                case 1:
                    i4 -= measuredWidth / 2;
                    break;
                case 5:
                    i4 -= measuredWidth;
                    break;
                case 16:
                    i5 -= i3 / 2;
                    break;
                case 17:
                    i4 -= measuredWidth / 2;
                    i5 -= i3 / 2;
                    break;
                case 80:
                    i5 -= i3;
                    break;
                case LayoutParams.BOTTOM_CENTER /*81*/:
                    i4 -= measuredWidth / 2;
                    i5 -= i3;
                    break;
            }
            view.layout(i4, i5, measuredWidth + i4, i3 + i5);
            return;
        }
        view.layout(0, 0, measuredWidth, i3);
    }

    private boolean a(Context context) {
        MapActivity mapActivity = (MapActivity) context;
        this.e = Mj.g;
        this.f = Mj.h;
        this.l = mapActivity;
        g++;
        return mapActivity.a(this);
    }

    private void b(int i2) {
        this.d = i2;
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        Mj.MapProc(4354, i2, i3);
    }

    /* access modifiers changed from: package-private */
    public void a(GeoPoint geoPoint) {
        this.c = geoPoint;
        Mj.MapProc(4102, geoPoint.getLongitudeE6(), geoPoint.getLatitudeE6());
    }

    /* access modifiers changed from: package-private */
    public void a(GeoPoint geoPoint, Message message, Runnable runnable) {
        this.c = geoPoint;
        Mj.MapProc(4353, geoPoint.getLongitudeE6(), geoPoint.getLatitudeE6());
        this.j = message;
        this.k = runnable;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (z) {
            Mj.MapProc(4355, 1, 0);
        } else {
            Mj.MapProc(4355, 0, 0);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        char c2 = 0;
        if (this.b == null) {
            this.b = new a(getContext(), this);
        }
        this.m = new MapController(this);
        addView(this.b);
        this.n.setOnZoomOutClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MapView.this.g();
            }
        });
        this.n.setOnZoomInClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MapView.this.f();
            }
        });
        this.n.setFocusable(true);
        this.n.setVisibility(0);
        this.n.measure(0, 0);
        try {
            char[] cArr = {'l', 'h'};
            if (Mj.i > 180) {
                c2 = 1;
            }
            InputStream open = this.l.getAssets().open("baidumap_logo_" + cArr[c2] + ".png");
            Bitmap decodeStream = BitmapFactory.decodeStream(open);
            open.close();
            if (decodeStream != null) {
                this.o.setImageBitmap(decodeStream);
                this.o.setVisibility(0);
                this.o.measure(0, 0);
                this.o.setScaleType(ImageView.ScaleType.FIT_CENTER);
                addView(this.o);
            }
        } catch (Exception e2) {
            Log.d("MapView()", "initMapView() error!");
            Log.d("MapView()", e2.getMessage());
        }
        this.b.setFocusable(true);
        this.b.setFocusableInTouchMode(true);
        this.d = d();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        if (i2 < 3) {
            i2 = 3;
        } else if (i2 > 18) {
            i2 = 18;
        }
        if (this.d != i2) {
            this.d = i2;
            Mj.MapProc(4098, i2, 1);
            c();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, int i4) {
        switch (i2) {
            case 9:
                if (this.b == null) {
                    return true;
                }
                this.b.a = true;
                invalidate();
                return true;
            case HttpStatus.SC_HTTP_VERSION_NOT_SUPPORTED /*505*/:
                b(i3);
                return true;
            case 8020:
                if (this.j != null) {
                    if (this.j.getTarget() == null) {
                        return true;
                    }
                    this.j.getTarget().sendMessage(this.j);
                    return true;
                } else if (this.k == null) {
                    return true;
                } else {
                    this.k.run();
                    return true;
                }
            default:
                return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (g > 0) {
            g--;
            if (g == 0) {
                this.b.a();
                this.b = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3) {
        Mj.MapProc(4103, (this.e / 2) + i2, (this.f / 2) + i3);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        boolean z = true;
        this.n.setIsZoomOutEnabled(this.d > 3);
        ZoomControls zoomControls = this.n;
        if (this.d >= getMaxZoomLevel()) {
            z = false;
        }
        zoomControls.setIsZoomInEnabled(z);
    }

    public boolean canCoverCenter() {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PARAM_ACT, 15010001);
        Mj.sendBundle(bundle);
        return bundle.getInt("r") != 0;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void computeScroll() {
        super.computeScroll();
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return Mj.getNewBundle(10030300, 0, 0).getInt("mapLevel");
    }

    public void displayZoomControls(boolean z) {
        removeView(this.n);
        addView(this.n);
        if (z) {
            requestChildFocus(this.n, this.n);
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = super.getChildAt(i2);
            if (!(childAt == this.o || childAt == this.n || childAt == this.b)) {
                ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
                if ((layoutParams instanceof LayoutParams) && ((LayoutParams) layoutParams).mode == 0) {
                    a(childAt, layoutParams);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        if (this.d >= 18) {
            return false;
        }
        this.b.a(1, this.e / 2, this.f / 2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        if (this.d <= 3) {
            return false;
        }
        this.b.a(-1, this.e / 2, this.f / 2);
        return true;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return super.generateDefaultLayoutParams();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(this.l, attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public MapController getController() {
        return this.m;
    }

    public int getLatitudeSpan() {
        d dVar = (d) getProjection();
        return Math.abs(dVar.fromPixels(0, 0).getLatitudeE6() - dVar.fromPixels(this.e - 1, this.f - 1).getLatitudeE6());
    }

    public int getLongitudeSpan() {
        d dVar = (d) getProjection();
        return Math.abs(dVar.fromPixels(this.e - 1, this.f - 1).getLongitudeE6() - dVar.fromPixels(0, 0).getLongitudeE6());
    }

    public GeoPoint getMapCenter() {
        Bundle GetMapStatus = Mj.GetMapStatus();
        if (GetMapStatus == null) {
            return this.c;
        }
        return new GeoPoint(GetMapStatus.getInt("y"), GetMapStatus.getInt("x"));
    }

    public int getMaxZoomLevel() {
        return 18;
    }

    public final List<Overlay> getOverlays() {
        if (this.b == null) {
            return null;
        }
        return this.b.c();
    }

    public Projection getProjection() {
        return this.a;
    }

    @Deprecated
    public View getZoomControls() {
        return this.n;
    }

    public int getZoomLevel() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public double h() {
        return Math.pow(2.0d, (double) (18 - this.d));
    }

    public boolean isDoubleClickZooming() {
        return this.b.b();
    }

    public boolean isSatellite() {
        return this.i;
    }

    public boolean isStreetView() {
        return false;
    }

    public boolean isTraffic() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeView(this.n);
        removeView(this.b);
        removeView(this.o);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void onFocusChanged(boolean z, int i2, Rect rect) {
        this.b.a(z, i2, rect);
        super.onFocusChanged(z, i2, rect);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.b.a(i2, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (this.b.b(i2, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        this.e = i4 - i2;
        this.f = i5 - i3;
        this.b.setLayoutParams(new ViewGroup.LayoutParams(this.e, this.f));
        this.b.setVisibility(0);
        this.b.layout(0, 0, this.e, this.f);
        this.n.setLayoutParams(new ViewGroup.LayoutParams(this.e, this.f));
        this.n.setVisibility(0);
        this.n.measure(i4 - i2, i5 - i3);
        int measuredWidth = this.n.getMeasuredWidth();
        int measuredHeight = this.n.getMeasuredHeight();
        this.n.layout((i4 - 10) - measuredWidth, ((i5 - 5) - measuredHeight) - i3, i4 - 10, (i5 - 5) - i3);
        this.o.setLayoutParams(new ViewGroup.LayoutParams(this.e, this.f));
        this.o.setVisibility(0);
        this.o.measure(i4 - i2, i5 - i3);
        int measuredWidth2 = this.o.getMeasuredWidth();
        int measuredHeight2 = this.o.getMeasuredHeight();
        if (measuredHeight <= measuredHeight2) {
            measuredHeight = measuredHeight2;
        }
        this.o.layout(10, ((i5 - 5) - measuredHeight) - i3, measuredWidth2 + 10, (i5 - 5) - i3);
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = super.getChildAt(i6);
            if (!(childAt == this.o || childAt == this.n || childAt == this.b)) {
                a(childAt, childAt.getLayoutParams());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }

    public void onRestoreInstanceState(Bundle bundle) {
        int i2 = bundle.getInt("lat");
        int i3 = bundle.getInt("lon");
        if (!(i2 == 0 || i3 == 0)) {
            a(new GeoPoint(i2, i3));
        }
        int i4 = bundle.getInt("zoom");
        if (i4 != 0) {
            a(i4);
        }
        setTraffic(bundle.getBoolean("traffic"));
    }

    public void onSaveInstanceState(Bundle bundle) {
        GeoPoint mapCenter = getMapCenter();
        bundle.putInt("lat", mapCenter.getLatitudeE6());
        bundle.putInt("lon", mapCenter.getLongitudeE6());
        bundle.putInt("zoom", getZoomLevel());
        bundle.putBoolean("traffic", isTraffic());
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.e = i2;
        this.f = i3;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.b == null || !this.b.a(motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (this.b.b(motionEvent)) {
            return true;
        }
        return super.onTrackballEvent(motionEvent);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public void preLoad() {
    }

    public void regMapViewListener(BMapManager bMapManager, MKMapViewListener mKMapViewListener) {
        if (bMapManager != null && bMapManager.a != null) {
            bMapManager.a.a(mKMapViewListener);
        }
    }

    public void setBuiltInZoomControls(boolean z) {
        if (z) {
            removeView(this.n);
            addView(this.n);
            return;
        }
        removeView(this.n);
    }

    public void setDoubleClickZooming(boolean z) {
        this.b.a(z);
    }

    public void setDrawOverlayWhenZooming(boolean z) {
        this.b.b(z);
    }

    public void setReticleDrawMode(a aVar) {
        throw new RuntimeException("this feature is not implemented!!");
    }

    public void setSatellite(boolean z) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PARAM_ACT, 1001);
        bundle.putInt("opt", 10020803);
        if (z) {
            bundle.putInt("on", 1);
        } else {
            bundle.putInt("on", 0);
        }
        Mj.sendBundle(bundle);
        this.i = z;
    }

    public void setStreetView(boolean z) {
        throw new RuntimeException("this feature is not implemented!!");
    }

    public void setTraffic(boolean z) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PARAM_ACT, 1001);
        bundle.putInt("opt", 10020400);
        if (z) {
            bundle.putInt("on", 1);
        } else {
            bundle.putInt("on", 0);
        }
        Mj.sendBundle(bundle);
        this.h = z;
    }
}
