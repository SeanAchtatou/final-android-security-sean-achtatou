package com.baidu.mapapi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import com.tencent.tauth.Constants;

class b extends BroadcastReceiver {
    b() {
    }

    public void onReceive(Context context, Intent intent) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
        Mj.r = activeNetworkInfo != null ? (wifiManager.getWifiState() != 3 || !"WIFI".matches(activeNetworkInfo.getTypeName())) ? 0 : 1 : networkInfo == null ? -1 : (wifiManager.getWifiState() != 3 || !"WIFI".matches(networkInfo.getTypeName())) ? 0 : 1;
        Bundle bundle = new Bundle();
        bundle.putInt("opt", 15010902);
        bundle.putInt(Constants.PARAM_ACT, 15010900);
        bundle.putInt("state", Mj.r);
        Mj.sendBundle(bundle);
        if (-1 != Mj.r) {
            Mj.changeGprsConnect();
        }
    }
}
