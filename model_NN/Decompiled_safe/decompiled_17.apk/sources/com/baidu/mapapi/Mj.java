package com.baidu.mapapi;

import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.devspark.appmsg.AppMsg;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.httpclient.HttpStatus;

public class Mj {
    private static b U = null;
    private static Method V = null;
    private static Method W = null;
    private static Class<?> X = null;
    static e a = null;
    static final MKLocationManager b = new MKLocationManager();
    static int c = 2;
    static MapView d = null;
    static Context e = null;
    static ServerSocket f = null;
    static int g = 0;
    static int h = 0;
    static int i = 0;
    static float j = 1.0f;
    public static int k = 0;
    public static int l = 0;
    public static int m = 0;
    public static int n = 0;
    public static int o = 0;
    static boolean p;
    static boolean q;
    static int r = -1;
    static final Uri s = Uri.parse("content://telephony/carriers/preferapn");
    private static Handler t = null;
    private static String u = "";
    private static String v = "";
    private static String w = "";
    private static String x = "";
    private static String y = "";
    private WifiManager A = null;
    private h B = null;
    private h C = null;
    private h D = null;
    private h E = null;
    private String F;
    private String G;
    private final long H = 3;
    private final long I = 3;
    private List<NeighboringCellInfo> J = null;
    private long K = 0;
    private long L = 0;
    private int M = 0;
    private int N = 0;
    private int O = 0;
    private int P = 0;
    private int Q = 0;
    private int R = 0;
    private List<ScanResult> S = null;
    private List<ScanResult> T = null;
    /* access modifiers changed from: private */
    public Handler Y = new Handler();
    private ArrayList<b> Z = new ArrayList<>();
    private TelephonyManager z = null;

    class a {
        int a = 0;
        int b;
        int c;
        int d;

        a() {
        }
    }

    private class b implements Runnable {
        public int a;
        public int b;

        private b() {
        }

        public void run() {
            Mj.MsgMapProc(1, 8, this.a, 0);
            Mj.this.Y.postDelayed(this, (long) this.b);
        }
    }

    static {
        try {
            System.loadLibrary("BMapApiEngine_v1_3_3");
        } catch (UnsatisfiedLinkError e2) {
            Log.d("BMapApiEngine_v1_3_3", "BMapApiEngine_v1_3_3 library not found!");
            Log.d("BMapApiEngine_v1_3_3", e2.getLocalizedMessage());
        }
    }

    Mj(BMapManager bMapManager, Context context) {
        e = context;
        e();
    }

    public static native int DisableProviderCC(int i2);

    public static native int EnableProviderCC(int i2);

    public static native Bundle GetGPSStatus();

    public static native Bundle GetMapStatus();

    public static native Bundle GetNotifyInternal();

    public static native int InitLocationCC();

    public static native int InitMapControlCC(int i2, int i3);

    public static native int MapProc(int i2, int i3, int i4);

    public static native int MsgMapProc(int i2, int i3, int i4, int i5);

    public static native void SetCellData(int i2, int i3, int i4, int i5, String str, String str2, String str3);

    public static native void SetCellInfo(int i2, int i3, int i4, int i5, String str);

    public static native void SetLocationCoordinateType(int i2);

    public static native int SetNotifyInternal(int i2, int i3);

    public static native int SetProxyInfo(String str, int i2);

    public static native void SetUpdateWifi(String str);

    public static native void UpdataGPS(double d2, double d3, float f2, float f3, float f4, int i2);

    private void a(int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < this.Z.size()) {
                b bVar = this.Z.get(i4);
                if (bVar.a == i2) {
                    if (BMapManager.b) {
                        this.Y.removeCallbacks(bVar);
                    }
                    this.Z.remove(i4);
                    return;
                }
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    private void a(int i2, int i3) {
        Iterator<b> it = this.Z.iterator();
        while (it.hasNext()) {
            if (it.next().a == i2) {
                return;
            }
        }
        b bVar = new b();
        bVar.a = i2;
        bVar.b = i3;
        if (BMapManager.b) {
            this.Y.postDelayed(bVar, 500);
        }
        this.Z.add(bVar);
    }

    public static void changeGprsConnect() {
        String h2;
        int i2 = 80;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) e.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
            if (!activeNetworkInfo.getTypeName().toLowerCase().equals("wifi")) {
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (extraInfo == null) {
                    return;
                }
                if (extraInfo.toLowerCase().contains("wap")) {
                    String defaultHost = Proxy.getDefaultHost();
                    int defaultPort = Proxy.getDefaultPort();
                    if (defaultHost == null) {
                        defaultHost = "10.0.0.172";
                    }
                    if (defaultPort != -1) {
                        i2 = defaultPort;
                    }
                    SetProxyInfo(defaultHost, i2);
                    return;
                }
                SetProxyInfo(null, 0);
            } else if (1 == r) {
                SetProxyInfo(null, 0);
            } else if (r == 0 && (h2 = h()) != null) {
                if (h2.toLowerCase().contains("wap")) {
                    String defaultHost2 = Proxy.getDefaultHost();
                    int defaultPort2 = Proxy.getDefaultPort();
                    if (defaultHost2 == null) {
                        defaultHost2 = "10.0.0.172";
                    }
                    if (defaultPort2 == -1) {
                        defaultPort2 = 80;
                    }
                    SetProxyInfo(defaultHost2, defaultPort2);
                    return;
                }
                SetProxyInfo(null, 0);
            }
        }
    }

    private int d() {
        try {
            X = Class.forName("android.telephony.cdma.CdmaCellLocation");
            V = X.getMethod("getBaseStationId", new Class[0]);
            W = X.getMethod("getNetworkId", new Class[0]);
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    private void e() {
        if (e != null) {
            y = e.getFilesDir().getAbsolutePath();
            if (this.z == null) {
                this.z = (TelephonyManager) e.getSystemService("phone");
            }
            if (this.z != null) {
                u = this.z.getDeviceId();
                v = this.z.getSubscriberId();
                w = Build.MODEL;
                x = Build.VERSION.SDK;
            }
            Display defaultDisplay = ((WindowManager) e.getSystemService("window")).getDefaultDisplay();
            g = defaultDisplay.getWidth();
            h = defaultDisplay.getHeight();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            j = displayMetrics.density;
            if (Integer.parseInt(Build.VERSION.SDK) > 3) {
                try {
                    i = Class.forName("android.util.DisplayMetrics").getField("densityDpi").getInt(displayMetrics);
                } catch (Exception e2) {
                    e2.printStackTrace();
                    i = 160;
                }
            } else {
                i = 160;
            }
            try {
                this.G = e.getPackageManager().getPackageInfo(e.getPackageName(), 0).applicationInfo.loadLabel(e.getPackageManager()).toString();
            } catch (Exception e3) {
                Log.d("baidumap", e3.getMessage());
                this.G = null;
            }
        }
    }

    private static void f() {
        String g2 = g();
        if (g2 == null) {
            SetProxyInfo(null, 0);
        } else if (g2.toLowerCase().contains("wap")) {
            String defaultHost = Proxy.getDefaultHost();
            int defaultPort = Proxy.getDefaultPort();
            if (defaultHost == null) {
                defaultHost = "10.0.0.172";
            }
            if (defaultPort == -1) {
                defaultPort = 80;
            }
            SetProxyInfo(defaultHost, defaultPort);
        } else {
            SetProxyInfo(null, 0);
        }
    }

    private static String g() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) e.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
            if (!activeNetworkInfo.getTypeName().toLowerCase().equals("wifi")) {
                return activeNetworkInfo.getExtraInfo();
            }
            if (1 == r) {
                return null;
            }
            if (r == 0) {
                return h();
            }
        }
        return null;
    }

    public static native Bundle getNewBundle(int i2, int i3, int i4);

    private static String h() {
        Cursor query = e.getContentResolver().query(s, new String[]{"_id", "apn", "type"}, null, null, null);
        query.moveToFirst();
        if (query.isAfterLast()) {
            return null;
        }
        return query.getString(1);
    }

    public static native int initOfflineCC();

    public static native int initSearchCC();

    public static native void nativeDone();

    public static native void nativeInit();

    public static native void nativeRender();

    public static native void nativeResize(int i2, int i3);

    public static native void renderBaiduMap(Bitmap bitmap);

    public static native void renderCalDisScreenPos(Bundle bundle);

    public static native void renderFlsScreenPos(Bundle bundle);

    public static native void renderUpdateScreen(short[] sArr, int i2, int i3);

    public static native int sendBundle(Bundle bundle);

    public static native void sendPhoneInfo(Bundle bundle);

    public native int InitMapApiEngine();

    public void JNI_MapcallBackProc(int i2, int i3, int i4, int i5) {
        a aVar = new a();
        aVar.a = i2;
        aVar.b = i3;
        aVar.c = i4;
        aVar.d = i5;
        Message obtainMessage = t.obtainMessage(1, 1, 1, aVar);
        if (obtainMessage != null) {
            t.sendMessage(obtainMessage);
        }
    }

    public void JNI_callBackProc(int i2, int i3, int i4) {
        switch (i2) {
            case 9:
            case HttpStatus.SC_HTTP_VERSION_NOT_SUPPORTED /*505*/:
                if (d != null) {
                    d.a(i2, i3, i4);
                    return;
                }
                return;
            case 506:
                c();
                return;
            case 511:
                a(i3, i4);
                return;
            case 512:
                a(i3);
                return;
            case AppMsg.LENGTH_LONG /*5000*/:
                if (a != null) {
                    a.a(i2, i3, (long) i4);
                    return;
                }
                return;
            case 10001:
            case 10002:
            case 10003:
            case 10004:
            case 10006:
            case 10010:
            case 10011:
            case 10012:
            case 10015:
            case 10016:
                if (this.C != null) {
                    this.C.a(new MKEvent(i2 - 10000, i3, i4));
                    return;
                }
                return;
            case 10005:
                if (b != null) {
                    b.c();
                    return;
                }
                return;
            case 10007:
            case 10009:
                if (this.B != null) {
                    this.B.a(new MKEvent(i2 - 10000, i3, i4));
                    return;
                }
                return;
            case 10013:
                if (this.D != null) {
                    this.D.a(new MKEvent(i3, i3, i4));
                    return;
                }
                return;
            case 10014:
                if (this.E != null) {
                    if (Math.abs(k - m) > 2 || Math.abs(l - n) > 2 || 1 == o) {
                        this.E.a(new MKEvent(i2 - 10000, i3, i4));
                    }
                    k = 0;
                    l = 0;
                    n = 0;
                    m = 0;
                    o = 0;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public Bundle J_GetDevInfo(int i2) {
        Bundle bundle = new Bundle();
        switch (i2) {
            case 1:
                bundle.putString("im", u);
                break;
            case 2:
                bundle.putString("is", v);
                break;
            case 3:
                bundle.putString("mb", w);
                bundle.putString("os", "Android_" + x);
                bundle.putInt("cx", g);
                bundle.putInt("cy", h);
                bundle.putInt("xd", i);
                bundle.putInt("yd", i);
                break;
            case 4:
                bundle.putString("na", y);
                break;
        }
        return bundle;
    }

    public native int SetCacheDirectoryCC(String str);

    public native int StartApiEngineCC(String str, String str2);

    public native int StopApiEngineCC();

    public native int UnInitMapApiEngine();

    /* access modifiers changed from: package-private */
    public void a(MKMapViewListener mKMapViewListener) {
        this.E = new g(mKMapViewListener);
    }

    /* access modifiers changed from: package-private */
    public void a(MKOfflineMapListener mKOfflineMapListener) {
        this.D = new i(mKOfflineMapListener);
    }

    /* access modifiers changed from: package-private */
    public void a(MKSearchListener mKSearchListener) {
        this.C = new j(mKSearchListener);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        f();
        U = new b();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        e.registerReceiver(U, intentFilter, null, null);
        if (b != null) {
            this.L = 0;
            this.T = null;
            this.R = 0;
            this.Q = 0;
            b.a();
            UpdataGPS(0.0d, 0.0d, 0.0f, 0.0f, 0.0f, 0);
            SetCellData(0, 0, 0, 0, null, null, null);
            if (p) {
                b.enableProvider(0);
            }
            if (q) {
                b.enableProvider(1);
            }
        }
        Iterator<b> it = this.Z.iterator();
        while (it.hasNext()) {
            this.Y.postDelayed(it.next(), 500);
        }
        if (StartApiEngineCC(this.F, this.G) != 0) {
            return true;
        }
        try {
            if (U != null) {
                e.unregisterReceiver(U);
                U = null;
            }
        } catch (Exception e2) {
            Log.d("baidumap", e2.getMessage());
            U = null;
        }
        Iterator<b> it2 = this.Z.iterator();
        while (it2.hasNext()) {
            this.Y.removeCallbacks(it2.next());
        }
        if (b == null) {
            return false;
        }
        b.b();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, MKGeneralListener mKGeneralListener) {
        if (mKGeneralListener != null) {
            this.B = new f(mKGeneralListener);
        }
        this.F = str;
        if (a == null) {
            a = new e(e);
        }
        if (e != null) {
            if (this.z == null) {
                this.z = (TelephonyManager) e.getSystemService("phone");
            }
            if (this.A == null) {
                this.A = (WifiManager) e.getSystemService("wifi");
            }
            if (this.A != null && this.A.isWifiEnabled()) {
                this.A.startScan();
            }
        }
        t = new Handler() {
            public void handleMessage(Message message) {
                a aVar = (a) message.obj;
                Mj.MsgMapProc(aVar.a, aVar.b, aVar.c, aVar.d);
                super.handleMessage(message);
            }
        };
        if (initClass(new Bundle(), 0) == 0) {
            return false;
        }
        if (InitMapApiEngine() == 0) {
            return false;
        }
        this.Z.clear();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        try {
            if (U != null) {
                e.unregisterReceiver(U);
                U = null;
            }
        } catch (Exception e2) {
            Log.d("baidumap", e2.getMessage());
            U = null;
        }
        Iterator<b> it = this.Z.iterator();
        while (it.hasNext()) {
            this.Y.removeCallbacks(it.next());
        }
        if (b != null) {
            b.b();
        }
        return StopApiEngineCC() != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        String str;
        String str2;
        String str3;
        String str4;
        if (a == null) {
            return false;
        }
        if (this.z == null) {
            this.z = (TelephonyManager) e.getSystemService("phone");
        }
        if (this.z != null) {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (currentTimeMillis - this.L > 3) {
                CellLocation cellLocation = this.z.getCellLocation();
                this.L = currentTimeMillis;
                if (cellLocation == null || !(cellLocation instanceof GsmCellLocation)) {
                    try {
                        if (Integer.parseInt(Build.VERSION.SDK) >= 5 && (!(X == null && -1 == d()) && X.isInstance(cellLocation))) {
                            Object invoke = V.invoke(cellLocation, new Object[0]);
                            if (invoke instanceof Integer) {
                                this.P = ((Integer) invoke).intValue();
                                if (this.P < 0) {
                                    this.P = 0;
                                }
                                Object invoke2 = W.invoke(cellLocation, new Object[0]);
                                if (invoke2 instanceof Integer) {
                                    this.O = ((Integer) invoke2).intValue();
                                    if (this.O < 0) {
                                        this.O = 0;
                                    }
                                }
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } else {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                    this.P = gsmCellLocation.getCid();
                    if (this.P < 0) {
                        this.P = 0;
                    }
                    this.O = gsmCellLocation.getLac();
                    if (this.O < 0) {
                        this.O = 0;
                    }
                }
                String networkOperator = this.z.getNetworkOperator();
                if (networkOperator != null && networkOperator.length() > 0) {
                    try {
                        if (networkOperator.length() >= 3) {
                            this.M = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                        }
                        if (networkOperator.length() >= 5) {
                            this.N = Integer.valueOf(networkOperator.substring(3, 5)).intValue();
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
                try {
                    if (X != null && X.isInstance(cellLocation)) {
                        this.N = ((Integer) X.getMethod("getSystemId", new Class[0]).invoke(cellLocation, new Object[0])).intValue();
                    }
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
        } else {
            this.P = 0;
            this.O = 0;
            this.M = 0;
            this.N = 0;
        }
        if (this.A == null) {
            this.A = (WifiManager) e.getSystemService("wifi");
        }
        if (this.A == null || !this.A.isWifiEnabled()) {
            this.S = null;
            SetUpdateWifi("");
        } else {
            long currentTimeMillis2 = System.currentTimeMillis() / 1000;
            if (currentTimeMillis2 - this.K > 3) {
                this.A.startScan();
                this.S = null;
                this.K = currentTimeMillis2;
            }
            this.S = this.A.getScanResults();
            if (this.S == null || this.S.size() <= 0) {
                SetUpdateWifi("");
            }
        }
        String str5 = "";
        if (this.P > 0 && this.O >= 0 && this.M >= 0) {
            str5 = a.a(this.M, this.N, this.O, this.P, this.J, str5);
            if (str5.length() > 0) {
                str = str5;
                if (this.S != null || this.S.size() <= 0) {
                    str2 = "";
                    str3 = "";
                } else {
                    a.a(this.S);
                    String a2 = a.a(this.S, str5);
                    if (a2.length() > 0) {
                        SetUpdateWifi(a2);
                        str4 = a2;
                    } else {
                        str4 = "";
                    }
                    str2 = a.b(this.S, a2);
                    if (str2.length() > 0) {
                        str3 = str4;
                    } else {
                        str2 = "";
                        str3 = str4;
                    }
                }
                if (!(this.R == this.P && this.Q == this.O && a.a(this.S, this.T))) {
                    this.T = this.S;
                    this.R = this.P;
                    this.Q = this.O;
                    SetCellData(this.P, this.O, this.M, this.N, str, str3, str2);
                }
                return true;
            }
        }
        str = "";
        if (this.S != null) {
        }
        str2 = "";
        str3 = "";
        this.T = this.S;
        this.R = this.P;
        this.Q = this.O;
        SetCellData(this.P, this.O, this.M, this.N, str, str3, str2);
        return true;
    }

    public native int initClass(Object obj, int i2);
}
