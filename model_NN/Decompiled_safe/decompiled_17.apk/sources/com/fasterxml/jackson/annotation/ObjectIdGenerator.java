package com.fasterxml.jackson.annotation;

import java.io.Serializable;

public abstract class ObjectIdGenerator<T> implements Serializable {
    public abstract boolean canUseFor(ObjectIdGenerator<?> objectIdGenerator);

    public abstract ObjectIdGenerator<T> forScope(Class<?> cls);

    public abstract T generateId(Object obj);

    public abstract Class<?> getScope();

    public abstract IdKey key(Object obj);

    public abstract ObjectIdGenerator<T> newForSerialization(Object obj);

    public static final class IdKey implements Serializable {
        private static final long serialVersionUID = 1;
        private final int hashCode;
        private final Object key;
        private final Class<?> scope;
        private final Class<?> type;

        public IdKey(Class<?> type2, Class<?> scope2, Object key2) {
            this.type = type2;
            this.scope = scope2;
            this.key = key2;
            int h = key2.hashCode() + type2.getName().hashCode();
            this.hashCode = scope2 != null ? h ^ scope2.getName().hashCode() : h;
        }

        public int hashCode() {
            return this.hashCode;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (o.getClass() != getClass()) {
                return false;
            }
            IdKey other = (IdKey) o;
            if (other.key.equals(this.key) && other.type == this.type && other.scope == this.scope) {
                return true;
            }
            return false;
        }
    }
}
