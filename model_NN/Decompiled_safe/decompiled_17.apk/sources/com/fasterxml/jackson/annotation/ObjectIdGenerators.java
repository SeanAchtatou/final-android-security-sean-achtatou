package com.fasterxml.jackson.annotation;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import java.util.UUID;

public class ObjectIdGenerators {

    public static abstract class None extends ObjectIdGenerator<Object> {
    }

    private static abstract class Base<T> extends ObjectIdGenerator<T> {
        protected final Class<?> _scope;

        public abstract T generateId(Object obj);

        protected Base(Class<?> scope) {
            this._scope = scope;
        }

        public final Class<?> getScope() {
            return this._scope;
        }

        public boolean canUseFor(ObjectIdGenerator<?> gen) {
            return gen.getClass() == getClass() && gen.getScope() == this._scope;
        }
    }

    public static abstract class PropertyGenerator extends Base<Object> {
        private static final long serialVersionUID = 1;

        public /* bridge */ /* synthetic */ boolean canUseFor(ObjectIdGenerator x0) {
            return super.canUseFor(x0);
        }

        protected PropertyGenerator(Class<?> scope) {
            super(scope);
        }
    }

    public static final class IntSequenceGenerator extends Base<Integer> {
        private static final long serialVersionUID = 1;
        protected transient int _nextValue;

        public /* bridge */ /* synthetic */ boolean canUseFor(ObjectIdGenerator x0) {
            return super.canUseFor(x0);
        }

        public IntSequenceGenerator() {
            this(Object.class, -1);
        }

        public IntSequenceGenerator(Class<?> scope, int fv) {
            super(scope);
            this._nextValue = fv;
        }

        /* access modifiers changed from: protected */
        public int initialValue() {
            return 1;
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public ObjectIdGenerator<Integer> forScope(Class<?> scope) {
            return this._scope == scope ? this : new IntSequenceGenerator(scope, this._nextValue);
        }

        public ObjectIdGenerator<Integer> newForSerialization(Object context) {
            return new IntSequenceGenerator(this._scope, initialValue());
        }

        public ObjectIdGenerator.IdKey key(Object key) {
            return new ObjectIdGenerator.IdKey(getClass(), this._scope, key);
        }

        public Integer generateId(Object forPojo) {
            int id = this._nextValue;
            this._nextValue++;
            return Integer.valueOf(id);
        }
    }

    public static final class UUIDGenerator extends Base<UUID> {
        private static final long serialVersionUID = 1;

        public UUIDGenerator() {
            this(Object.class);
        }

        private UUIDGenerator(Class<?> cls) {
            super(Object.class);
        }

        public ObjectIdGenerator<UUID> forScope(Class<?> cls) {
            return this;
        }

        public ObjectIdGenerator<UUID> newForSerialization(Object context) {
            return this;
        }

        public UUID generateId(Object forPojo) {
            return UUID.randomUUID();
        }

        public ObjectIdGenerator.IdKey key(Object key) {
            return new ObjectIdGenerator.IdKey(getClass(), null, key);
        }

        public boolean canUseFor(ObjectIdGenerator<?> gen) {
            return gen.getClass() == getClass();
        }
    }
}
