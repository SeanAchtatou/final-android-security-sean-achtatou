package com.fasterxml.jackson.core;

import org.apache.commons.httpclient.cookie.CookieSpec;

public final class Base64Variants {
    public static final Base64Variant MIME = new Base64Variant("MIME", STD_BASE64_ALPHABET, true, '=', 76);
    public static final Base64Variant MIME_NO_LINEFEEDS = new Base64Variant(MIME, "MIME-NO-LINEFEEDS", Integer.MAX_VALUE);
    public static final Base64Variant MODIFIED_FOR_URL;
    public static final Base64Variant PEM = new Base64Variant(MIME, "PEM", true, '=', 64);
    static final String STD_BASE64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.fasterxml.jackson.core.Base64Variant.<init>(com.fasterxml.jackson.core.Base64Variant, java.lang.String, boolean, char, int):void
      com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.core.Base64Variant.<init>(com.fasterxml.jackson.core.Base64Variant, java.lang.String, boolean, char, int):void
     arg types: [com.fasterxml.jackson.core.Base64Variant, java.lang.String, int, int, int]
     candidates:
      com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void
      com.fasterxml.jackson.core.Base64Variant.<init>(com.fasterxml.jackson.core.Base64Variant, java.lang.String, boolean, char, int):void */
    static {
        StringBuffer sb = new StringBuffer(STD_BASE64_ALPHABET);
        sb.setCharAt(sb.indexOf("+"), '-');
        sb.setCharAt(sb.indexOf(CookieSpec.PATH_DELIM), '_');
        MODIFIED_FOR_URL = new Base64Variant("MODIFIED-FOR-URL", sb.toString(), false, 0, Integer.MAX_VALUE);
    }

    public static Base64Variant getDefaultVariant() {
        return MIME_NO_LINEFEEDS;
    }

    public static Base64Variant valueOf(String name) throws IllegalArgumentException {
        String name2;
        if (MIME._name.equals(name)) {
            return MIME;
        }
        if (MIME_NO_LINEFEEDS._name.equals(name)) {
            return MIME_NO_LINEFEEDS;
        }
        if (PEM._name.equals(name)) {
            return PEM;
        }
        if (MODIFIED_FOR_URL._name.equals(name)) {
            return MODIFIED_FOR_URL;
        }
        if (name == null) {
            name2 = "<null>";
        } else {
            name2 = "'" + name + "'";
        }
        throw new IllegalArgumentException("No Base64Variant with name " + name2);
    }
}
