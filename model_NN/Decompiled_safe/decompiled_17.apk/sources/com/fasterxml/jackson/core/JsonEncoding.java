package com.fasterxml.jackson.core;

import com.umeng.common.b.e;

public enum JsonEncoding {
    UTF8("UTF-8", false),
    UTF16_BE(e.d, true),
    UTF16_LE(e.e, false),
    UTF32_BE("UTF-32BE", true),
    UTF32_LE("UTF-32LE", false);
    
    protected final boolean _bigEndian;
    protected final String _javaName;

    private JsonEncoding(String javaName, boolean bigEndian) {
        this._javaName = javaName;
        this._bigEndian = bigEndian;
    }

    public String getJavaName() {
        return this._javaName;
    }

    public boolean isBigEndian() {
        return this._bigEndian;
    }
}
