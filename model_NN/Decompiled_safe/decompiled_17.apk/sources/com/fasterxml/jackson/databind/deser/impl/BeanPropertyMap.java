package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class BeanPropertyMap implements Iterable<SettableBeanProperty>, Serializable {
    private static final long serialVersionUID = 1;
    private final Bucket[] _buckets;
    private final int _hashMask;
    private int _nextBucketIndex = 0;
    private final int _size;

    public BeanPropertyMap(Collection<SettableBeanProperty> properties) {
        this._size = properties.size();
        int bucketCount = findSize(this._size);
        this._hashMask = bucketCount - 1;
        Bucket[] buckets = new Bucket[bucketCount];
        for (SettableBeanProperty property : properties) {
            String key = property.getName();
            int index = key.hashCode() & this._hashMask;
            Bucket bucket = buckets[index];
            int i = this._nextBucketIndex;
            this._nextBucketIndex = i + 1;
            buckets[index] = new Bucket(bucket, key, property, i);
        }
        this._buckets = buckets;
    }

    private BeanPropertyMap(Bucket[] buckets, int size, int index) {
        this._buckets = buckets;
        this._size = size;
        this._hashMask = buckets.length - 1;
        this._nextBucketIndex = index;
    }

    public BeanPropertyMap withProperty(SettableBeanProperty newProperty) {
        int bcount = this._buckets.length;
        Bucket[] newBuckets = new Bucket[bcount];
        System.arraycopy(this._buckets, 0, newBuckets, 0, bcount);
        String propName = newProperty.getName();
        if (find(newProperty.getName()) == null) {
            int index = propName.hashCode() & this._hashMask;
            Bucket bucket = newBuckets[index];
            int i = this._nextBucketIndex;
            this._nextBucketIndex = i + 1;
            newBuckets[index] = new Bucket(bucket, propName, newProperty, i);
            return new BeanPropertyMap(newBuckets, this._size + 1, this._nextBucketIndex);
        }
        BeanPropertyMap newMap = new BeanPropertyMap(newBuckets, bcount, this._nextBucketIndex);
        newMap.replace(newProperty);
        return newMap;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public BeanPropertyMap renameAll(NameTransformer transformer) {
        JsonDeserializer<Object> newDeser;
        if (transformer == null || transformer == NameTransformer.NOP) {
            return this;
        }
        Iterator<SettableBeanProperty> it = iterator();
        ArrayList<SettableBeanProperty> newProps = new ArrayList<>();
        while (it.hasNext()) {
            SettableBeanProperty prop = it.next();
            SettableBeanProperty prop2 = prop.withName(transformer.transform(prop.getName()));
            JsonDeserializer<Object> deser = prop2.getValueDeserializer();
            if (!(deser == null || (newDeser = deser.unwrappingDeserializer(transformer)) == deser)) {
                prop2 = prop2.withValueDeserializer(newDeser);
            }
            newProps.add(prop2);
        }
        return new BeanPropertyMap(newProps);
    }

    public BeanPropertyMap assignIndexes() {
        int index = 0;
        Bucket[] arr$ = this._buckets;
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            Bucket bucket = arr$[i$];
            int index2 = index;
            while (bucket != null) {
                bucket.value.assignIndex(index2);
                bucket = bucket.next;
                index2++;
            }
            i$++;
            index = index2;
        }
        return this;
    }

    private static final int findSize(int size) {
        int result = 2;
        while (result < (size <= 32 ? size + size : size + (size >> 2))) {
            result += result;
        }
        return result;
    }

    public Iterator<SettableBeanProperty> iterator() {
        return new IteratorImpl(this._buckets);
    }

    public SettableBeanProperty[] getPropertiesInInsertionOrder() {
        SettableBeanProperty[] result = new SettableBeanProperty[this._nextBucketIndex];
        for (Bucket bucket : this._buckets) {
            while (bucket != null) {
                result[bucket.index] = bucket.value;
                bucket = bucket.next;
            }
        }
        return result;
    }

    public int size() {
        return this._size;
    }

    public SettableBeanProperty find(String key) {
        int index = key.hashCode() & this._hashMask;
        Bucket bucket = this._buckets[index];
        if (bucket == null) {
            return null;
        }
        if (bucket.key == key) {
            return bucket.value;
        }
        do {
            bucket = bucket.next;
            if (bucket == null) {
                return _findWithEquals(key, index);
            }
        } while (bucket.key != key);
        return bucket.value;
    }

    public void replace(SettableBeanProperty property) {
        Bucket tail;
        String name = property.getName();
        int index = name.hashCode() & (this._buckets.length - 1);
        int foundIndex = -1;
        Bucket bucket = this._buckets[index];
        Bucket tail2 = null;
        while (bucket != null) {
            if (foundIndex >= 0 || !bucket.key.equals(name)) {
                tail = new Bucket(tail2, bucket.key, bucket.value, bucket.index);
            } else {
                foundIndex = bucket.index;
                tail = tail2;
            }
            bucket = bucket.next;
            tail2 = tail;
        }
        if (foundIndex < 0) {
            throw new NoSuchElementException("No entry '" + property + "' found, can't replace");
        }
        this._buckets[index] = new Bucket(tail2, name, property, foundIndex);
    }

    public void remove(SettableBeanProperty property) {
        Bucket tail;
        String name = property.getName();
        int index = name.hashCode() & (this._buckets.length - 1);
        boolean found = false;
        Bucket bucket = this._buckets[index];
        Bucket tail2 = null;
        while (bucket != null) {
            if (found || !bucket.key.equals(name)) {
                tail = new Bucket(tail2, bucket.key, bucket.value, bucket.index);
            } else {
                found = true;
                tail = tail2;
            }
            bucket = bucket.next;
            tail2 = tail;
        }
        if (!found) {
            throw new NoSuchElementException("No entry '" + property + "' found, can't remove");
        }
        this._buckets[index] = tail2;
    }

    private SettableBeanProperty _findWithEquals(String key, int index) {
        for (Bucket bucket = this._buckets[index]; bucket != null; bucket = bucket.next) {
            if (key.equals(bucket.key)) {
                return bucket.value;
            }
        }
        return null;
    }

    private static final class Bucket implements Serializable {
        private static final long serialVersionUID = 1;
        public final int index;
        public final String key;
        public final Bucket next;
        public final SettableBeanProperty value;

        public Bucket(Bucket next2, String key2, SettableBeanProperty value2, int index2) {
            this.next = next2;
            this.key = key2;
            this.value = value2;
            this.index = index2;
        }
    }

    private static final class IteratorImpl implements Iterator<SettableBeanProperty> {
        private final Bucket[] _buckets;
        private Bucket _currentBucket;
        private int _nextBucketIndex;

        public IteratorImpl(Bucket[] buckets) {
            int i;
            this._buckets = buckets;
            int len = this._buckets.length;
            int i2 = 0;
            while (true) {
                if (i2 >= len) {
                    i = i2;
                    break;
                }
                i = i2 + 1;
                Bucket b = this._buckets[i2];
                if (b != null) {
                    this._currentBucket = b;
                    break;
                }
                i2 = i;
            }
            this._nextBucketIndex = i;
        }

        public boolean hasNext() {
            return this._currentBucket != null;
        }

        public SettableBeanProperty next() {
            Bucket curr = this._currentBucket;
            if (curr == null) {
                throw new NoSuchElementException();
            }
            Bucket b = curr.next;
            while (b == null && this._nextBucketIndex < this._buckets.length) {
                Bucket[] bucketArr = this._buckets;
                int i = this._nextBucketIndex;
                this._nextBucketIndex = i + 1;
                b = bucketArr[i];
            }
            this._currentBucket = b;
            return curr.value;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
