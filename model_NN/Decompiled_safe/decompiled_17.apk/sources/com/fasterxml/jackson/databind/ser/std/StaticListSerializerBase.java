package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonArrayFormatVisitor;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.lang.reflect.Type;
import java.util.Collection;

public abstract class StaticListSerializerBase<T extends Collection<?>> extends StdSerializer<T> {
    /* access modifiers changed from: protected */
    public abstract void acceptContentVisitor(JsonArrayFormatVisitor jsonArrayFormatVisitor);

    /* access modifiers changed from: protected */
    public abstract JsonNode contentSchema();

    public /* bridge */ /* synthetic */ boolean isEmpty(Object x0) {
        return isEmpty((Collection) ((Collection) x0));
    }

    protected StaticListSerializerBase(Class<?> cls) {
        super(cls, false);
    }

    public boolean isEmpty(T value) {
        return value == null || value.size() == 0;
    }

    public JsonNode getSchema(SerializerProvider provider, Type typeHint) {
        ObjectNode o = createSchemaNode("array", true);
        o.put("items", contentSchema());
        return o;
    }

    public void acceptJsonFormatVisitor(JsonFormatVisitorWrapper visitor, JavaType typeHint) {
        acceptContentVisitor(visitor.expectArrayFormat(typeHint));
    }
}
