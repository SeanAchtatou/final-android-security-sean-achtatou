package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.util.NameTransformer;
import com.fasterxml.jackson.databind.util.TokenBuffer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class UnwrappedPropertyHandler {
    protected final ArrayList<SettableBeanProperty> _properties = new ArrayList<>();

    public void addProperty(SettableBeanProperty property) {
        this._properties.add(property);
    }

    public void renameAll(NameTransformer transformer) {
        JsonDeserializer<Object> newDeser;
        Iterator<SettableBeanProperty> it = new ArrayList<>(this._properties).iterator();
        this._properties.clear();
        while (it.hasNext()) {
            SettableBeanProperty prop = (SettableBeanProperty) it.next();
            SettableBeanProperty prop2 = prop.withName(transformer.transform(prop.getName()));
            JsonDeserializer<Object> deser = prop2.getValueDeserializer();
            if (!(deser == null || (newDeser = deser.unwrappingDeserializer(transformer)) == deser)) {
                prop2 = prop2.withValueDeserializer(newDeser);
            }
            this._properties.add(prop2);
        }
    }

    public Object processUnwrapped(JsonParser originalParser, DeserializationContext ctxt, Object bean, TokenBuffer buffered) throws IOException, JsonProcessingException {
        int len = this._properties.size();
        for (int i = 0; i < len; i++) {
            JsonParser jp = buffered.asParser();
            jp.nextToken();
            this._properties.get(i).deserializeAndSet(jp, ctxt, bean);
        }
        return bean;
    }
}
