package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.NumberInput;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.deser.std.JdkDeserializers;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.util.ClassUtil;
import com.fasterxml.jackson.databind.util.EnumResolver;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import org.apache.commons.httpclient.HttpState;

public abstract class StdKeyDeserializer extends KeyDeserializer implements Serializable {
    private static final long serialVersionUID = 1;
    protected final Class<?> _keyClass;

    /* access modifiers changed from: protected */
    public abstract Object _parse(String str, DeserializationContext deserializationContext) throws Exception;

    protected StdKeyDeserializer(Class<?> cls) {
        this._keyClass = cls;
    }

    public final Object deserializeKey(String key, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (key == null) {
            return null;
        }
        try {
            Object result = _parse(key, ctxt);
            if (result != null) {
                return result;
            }
            if (this._keyClass.isEnum() && ctxt.getConfig().isEnabled(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                return null;
            }
            throw ctxt.weirdKeyException(this._keyClass, key, "not a valid representation");
        } catch (Exception re) {
            throw ctxt.weirdKeyException(this._keyClass, key, "not a valid representation: " + re.getMessage());
        }
    }

    public Class<?> getKeyClass() {
        return this._keyClass;
    }

    /* access modifiers changed from: protected */
    public int _parseInt(String key) throws IllegalArgumentException {
        return Integer.parseInt(key);
    }

    /* access modifiers changed from: protected */
    public long _parseLong(String key) throws IllegalArgumentException {
        return Long.parseLong(key);
    }

    /* access modifiers changed from: protected */
    public double _parseDouble(String key) throws IllegalArgumentException {
        return NumberInput.parseDouble(key);
    }

    @JacksonStdImpl
    static final class StringKD extends StdKeyDeserializer {
        private static final StringKD sObject = new StringKD(Object.class);
        private static final StringKD sString = new StringKD(String.class);
        private static final long serialVersionUID = 1;

        private StringKD(Class<?> nominalType) {
            super(nominalType);
        }

        public static StringKD forType(Class<?> nominalType) {
            if (nominalType == String.class) {
                return sString;
            }
            if (nominalType == Object.class) {
                return sObject;
            }
            return new StringKD(nominalType);
        }

        public String _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            return key;
        }
    }

    @JacksonStdImpl
    static final class BoolKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        BoolKD() {
            super(Boolean.class);
        }

        public Boolean _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            if ("true".equals(key)) {
                return Boolean.TRUE;
            }
            if (HttpState.PREEMPTIVE_DEFAULT.equals(key)) {
                return Boolean.FALSE;
            }
            throw ctxt.weirdKeyException(this._keyClass, key, "value not 'true' or 'false'");
        }
    }

    @JacksonStdImpl
    static final class ByteKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        ByteKD() {
            super(Byte.class);
        }

        public Byte _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            int value = _parseInt(key);
            if (value >= -128 && value <= 255) {
                return Byte.valueOf((byte) value);
            }
            throw ctxt.weirdKeyException(this._keyClass, key, "overflow, value can not be represented as 8-bit value");
        }
    }

    @JacksonStdImpl
    static final class ShortKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        ShortKD() {
            super(Integer.class);
        }

        public Short _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            int value = _parseInt(key);
            if (value >= -32768 && value <= 32767) {
                return Short.valueOf((short) value);
            }
            throw ctxt.weirdKeyException(this._keyClass, key, "overflow, value can not be represented as 16-bit value");
        }
    }

    @JacksonStdImpl
    static final class CharKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        CharKD() {
            super(Character.class);
        }

        public Character _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            if (key.length() == 1) {
                return Character.valueOf(key.charAt(0));
            }
            throw ctxt.weirdKeyException(this._keyClass, key, "can only convert 1-character Strings");
        }
    }

    @JacksonStdImpl
    static final class IntKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        IntKD() {
            super(Integer.class);
        }

        public Integer _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            return Integer.valueOf(_parseInt(key));
        }
    }

    @JacksonStdImpl
    static final class LongKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        LongKD() {
            super(Long.class);
        }

        public Long _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            return Long.valueOf(_parseLong(key));
        }
    }

    @JacksonStdImpl
    static final class DoubleKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        DoubleKD() {
            super(Double.class);
        }

        public Double _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            return Double.valueOf(_parseDouble(key));
        }
    }

    @JacksonStdImpl
    static final class FloatKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        FloatKD() {
            super(Float.class);
        }

        public Float _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            return Float.valueOf((float) _parseDouble(key));
        }
    }

    @JacksonStdImpl
    static final class LocaleKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;
        protected JdkDeserializers.LocaleDeserializer _localeDeserializer = new JdkDeserializers.LocaleDeserializer();

        LocaleKD() {
            super(Locale.class);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fasterxml.jackson.databind.deser.std.JdkDeserializers.LocaleDeserializer._deserialize(java.lang.String, com.fasterxml.jackson.databind.DeserializationContext):java.util.Locale
         arg types: [java.lang.String, com.fasterxml.jackson.databind.DeserializationContext]
         candidates:
          com.fasterxml.jackson.databind.deser.std.JdkDeserializers.LocaleDeserializer._deserialize(java.lang.String, com.fasterxml.jackson.databind.DeserializationContext):java.lang.Object
          com.fasterxml.jackson.databind.deser.std.FromStringDeserializer._deserialize(java.lang.String, com.fasterxml.jackson.databind.DeserializationContext):T
          com.fasterxml.jackson.databind.deser.std.JdkDeserializers.LocaleDeserializer._deserialize(java.lang.String, com.fasterxml.jackson.databind.DeserializationContext):java.util.Locale */
        /* access modifiers changed from: protected */
        public Locale _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            try {
                return this._localeDeserializer._deserialize(key, ctxt);
            } catch (IOException e) {
                throw ctxt.weirdKeyException(this._keyClass, key, "unable to parse key as locale");
            }
        }
    }

    static final class DelegatingKD extends KeyDeserializer implements Serializable {
        private static final long serialVersionUID = 1;
        protected final JsonDeserializer<?> _delegate;
        protected final Class<?> _keyClass;

        protected DelegatingKD(Class<?> cls, JsonDeserializer<?> deser) {
            this._keyClass = cls;
            this._delegate = deser;
        }

        public final Object deserializeKey(String key, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            if (key == null) {
                return null;
            }
            try {
                Object result = this._delegate.deserialize(ctxt.getParser(), ctxt);
                if (result != null) {
                    return result;
                }
                throw ctxt.weirdKeyException(this._keyClass, key, "not a valid representation");
            } catch (Exception re) {
                throw ctxt.weirdKeyException(this._keyClass, key, "not a valid representation: " + re.getMessage());
            }
        }

        public Class<?> getKeyClass() {
            return this._keyClass;
        }
    }

    @JacksonStdImpl
    static final class EnumKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;
        protected final AnnotatedMethod _factory;
        protected final EnumResolver<?> _resolver;

        protected EnumKD(EnumResolver<?> er, AnnotatedMethod factory) {
            super(er.getEnumClass());
            this._resolver = er;
            this._factory = factory;
        }

        public Object _parse(String key, DeserializationContext ctxt) throws JsonMappingException {
            if (this._factory != null) {
                try {
                    return this._factory.call1(key);
                } catch (Exception e) {
                    ClassUtil.unwrapAndThrowAsIAE(e);
                }
            }
            Object findEnum = this._resolver.findEnum(key);
            if (findEnum != null || ctxt.getConfig().isEnabled(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                return findEnum;
            }
            throw ctxt.weirdKeyException(this._keyClass, key, "not one of values for Enum class");
        }
    }

    static final class StringCtorKeyDeserializer extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;
        protected final Constructor<?> _ctor;

        public StringCtorKeyDeserializer(Constructor<?> ctor) {
            super(ctor.getDeclaringClass());
            this._ctor = ctor;
        }

        public Object _parse(String key, DeserializationContext ctxt) throws Exception {
            return this._ctor.newInstance(key);
        }
    }

    static final class StringFactoryKeyDeserializer extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;
        final Method _factoryMethod;

        public StringFactoryKeyDeserializer(Method fm) {
            super(fm.getDeclaringClass());
            this._factoryMethod = fm;
        }

        public Object _parse(String key, DeserializationContext ctxt) throws Exception {
            return this._factoryMethod.invoke(null, key);
        }
    }

    @JacksonStdImpl
    static final class DateKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        protected DateKD() {
            super(Date.class);
        }

        public Object _parse(String key, DeserializationContext ctxt) throws IllegalArgumentException, JsonMappingException {
            return ctxt.parseDate(key);
        }
    }

    @JacksonStdImpl
    static final class CalendarKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        protected CalendarKD() {
            super(Calendar.class);
        }

        public Object _parse(String key, DeserializationContext ctxt) throws IllegalArgumentException, JsonMappingException {
            Date date = ctxt.parseDate(key);
            if (date == null) {
                return null;
            }
            return ctxt.constructCalendar(date);
        }
    }

    @JacksonStdImpl
    static final class UuidKD extends StdKeyDeserializer {
        private static final long serialVersionUID = 1;

        protected UuidKD() {
            super(UUID.class);
        }

        public Object _parse(String key, DeserializationContext ctxt) throws IllegalArgumentException, JsonMappingException {
            return UUID.fromString(key);
        }
    }
}
