package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.NoClass;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.databind.util.NameTransformer;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AnnotationIntrospectorPair extends AnnotationIntrospector implements Serializable {
    private static final long serialVersionUID = 1;
    protected final AnnotationIntrospector _primary;
    protected final AnnotationIntrospector _secondary;

    public AnnotationIntrospectorPair(AnnotationIntrospector p, AnnotationIntrospector s) {
        this._primary = p;
        this._secondary = s;
    }

    public Version version() {
        return this._primary.version();
    }

    public static AnnotationIntrospector create(AnnotationIntrospector primary, AnnotationIntrospector secondary) {
        if (primary == null) {
            return secondary;
        }
        if (secondary == null) {
            return primary;
        }
        return new AnnotationIntrospectorPair(primary, secondary);
    }

    public Collection<AnnotationIntrospector> allIntrospectors() {
        return allIntrospectors(new ArrayList());
    }

    public Collection<AnnotationIntrospector> allIntrospectors(Collection<AnnotationIntrospector> result) {
        this._primary.allIntrospectors(result);
        this._secondary.allIntrospectors(result);
        return result;
    }

    public boolean isAnnotationBundle(Annotation ann) {
        return this._primary.isAnnotationBundle(ann) || this._secondary.isAnnotationBundle(ann);
    }

    public PropertyName findRootName(AnnotatedClass ac) {
        PropertyName name2;
        PropertyName name1 = this._primary.findRootName(ac);
        if (name1 == null) {
            return this._secondary.findRootName(ac);
        }
        return (name1.hasSimpleName() || (name2 = this._secondary.findRootName(ac)) == null) ? name1 : name2;
    }

    public String[] findPropertiesToIgnore(Annotated ac) {
        String[] result = this._primary.findPropertiesToIgnore(ac);
        if (result == null) {
            return this._secondary.findPropertiesToIgnore(ac);
        }
        return result;
    }

    public Boolean findIgnoreUnknownProperties(AnnotatedClass ac) {
        Boolean result = this._primary.findIgnoreUnknownProperties(ac);
        if (result == null) {
            return this._secondary.findIgnoreUnknownProperties(ac);
        }
        return result;
    }

    public Boolean isIgnorableType(AnnotatedClass ac) {
        Boolean result = this._primary.isIgnorableType(ac);
        if (result == null) {
            return this._secondary.isIgnorableType(ac);
        }
        return result;
    }

    public Object findFilterId(AnnotatedClass ac) {
        Object id = this._primary.findFilterId(ac);
        if (id == null) {
            return this._secondary.findFilterId(ac);
        }
        return id;
    }

    public Object findNamingStrategy(AnnotatedClass ac) {
        Object str = this._primary.findNamingStrategy(ac);
        if (str == null) {
            return this._secondary.findNamingStrategy(ac);
        }
        return str;
    }

    public VisibilityChecker<?> findAutoDetectVisibility(AnnotatedClass ac, VisibilityChecker<?> checker) {
        return this._primary.findAutoDetectVisibility(ac, this._secondary.findAutoDetectVisibility(ac, checker));
    }

    public TypeResolverBuilder<?> findTypeResolver(MapperConfig<?> config, AnnotatedClass ac, JavaType baseType) {
        TypeResolverBuilder<?> b = this._primary.findTypeResolver(config, ac, baseType);
        if (b == null) {
            return this._secondary.findTypeResolver(config, ac, baseType);
        }
        return b;
    }

    public TypeResolverBuilder<?> findPropertyTypeResolver(MapperConfig<?> config, AnnotatedMember am, JavaType baseType) {
        TypeResolverBuilder<?> b = this._primary.findPropertyTypeResolver(config, am, baseType);
        if (b == null) {
            return this._secondary.findPropertyTypeResolver(config, am, baseType);
        }
        return b;
    }

    public TypeResolverBuilder<?> findPropertyContentTypeResolver(MapperConfig<?> config, AnnotatedMember am, JavaType baseType) {
        TypeResolverBuilder<?> b = this._primary.findPropertyContentTypeResolver(config, am, baseType);
        if (b == null) {
            return this._secondary.findPropertyContentTypeResolver(config, am, baseType);
        }
        return b;
    }

    public List<NamedType> findSubtypes(Annotated a) {
        List<NamedType> types1 = this._primary.findSubtypes(a);
        List<NamedType> types2 = this._secondary.findSubtypes(a);
        if (types1 == null || types1.isEmpty()) {
            return types2;
        }
        if (types2 == null || types2.isEmpty()) {
            return types1;
        }
        ArrayList<NamedType> result = new ArrayList<>(types1.size() + types2.size());
        result.addAll(types1);
        result.addAll(types2);
        return result;
    }

    public String findTypeName(AnnotatedClass ac) {
        String name = this._primary.findTypeName(ac);
        if (name == null || name.length() == 0) {
            return this._secondary.findTypeName(ac);
        }
        return name;
    }

    public AnnotationIntrospector.ReferenceProperty findReferenceType(AnnotatedMember member) {
        AnnotationIntrospector.ReferenceProperty ref = this._primary.findReferenceType(member);
        if (ref == null) {
            return this._secondary.findReferenceType(member);
        }
        return ref;
    }

    public NameTransformer findUnwrappingNameTransformer(AnnotatedMember member) {
        NameTransformer value = this._primary.findUnwrappingNameTransformer(member);
        if (value == null) {
            return this._secondary.findUnwrappingNameTransformer(member);
        }
        return value;
    }

    public Object findInjectableValueId(AnnotatedMember m) {
        Object value = this._primary.findInjectableValueId(m);
        if (value == null) {
            return this._secondary.findInjectableValueId(m);
        }
        return value;
    }

    public boolean hasIgnoreMarker(AnnotatedMember m) {
        return this._primary.hasIgnoreMarker(m) || this._secondary.hasIgnoreMarker(m);
    }

    public Boolean hasRequiredMarker(AnnotatedMember m) {
        Boolean value = this._primary.hasRequiredMarker(m);
        if (value == null) {
            return this._secondary.hasRequiredMarker(m);
        }
        return value;
    }

    public Object findSerializer(Annotated am) {
        Object result = this._primary.findSerializer(am);
        if (result == null) {
            return this._secondary.findSerializer(am);
        }
        return result;
    }

    public Object findKeySerializer(Annotated a) {
        Object result = this._primary.findKeySerializer(a);
        if (result == null || result == JsonSerializer.None.class || result == NoClass.class) {
            return this._secondary.findKeySerializer(a);
        }
        return result;
    }

    public Object findContentSerializer(Annotated a) {
        Object result = this._primary.findContentSerializer(a);
        if (result == null || result == JsonSerializer.None.class || result == NoClass.class) {
            return this._secondary.findContentSerializer(a);
        }
        return result;
    }

    public JsonInclude.Include findSerializationInclusion(Annotated a, JsonInclude.Include defValue) {
        return this._primary.findSerializationInclusion(a, this._secondary.findSerializationInclusion(a, defValue));
    }

    public Class<?> findSerializationType(Annotated a) {
        Class<?> result = this._primary.findSerializationType(a);
        if (result == null) {
            return this._secondary.findSerializationType(a);
        }
        return result;
    }

    public Class<?> findSerializationKeyType(Annotated am, JavaType baseType) {
        Class<?> result = this._primary.findSerializationKeyType(am, baseType);
        if (result == null) {
            return this._secondary.findSerializationKeyType(am, baseType);
        }
        return result;
    }

    public Class<?> findSerializationContentType(Annotated am, JavaType baseType) {
        Class<?> result = this._primary.findSerializationContentType(am, baseType);
        if (result == null) {
            return this._secondary.findSerializationContentType(am, baseType);
        }
        return result;
    }

    public JsonSerialize.Typing findSerializationTyping(Annotated a) {
        JsonSerialize.Typing result = this._primary.findSerializationTyping(a);
        if (result == null) {
            return this._secondary.findSerializationTyping(a);
        }
        return result;
    }

    public Class<?>[] findViews(Annotated a) {
        Class<?>[] result = this._primary.findViews(a);
        if (result == null) {
            return this._secondary.findViews(a);
        }
        return result;
    }

    public Boolean isTypeId(AnnotatedMember member) {
        Boolean b = this._primary.isTypeId(member);
        if (b == null) {
            return this._secondary.isTypeId(member);
        }
        return b;
    }

    public ObjectIdInfo findObjectIdInfo(Annotated ann) {
        ObjectIdInfo result = this._primary.findObjectIdInfo(ann);
        if (result == null) {
            return this._secondary.findObjectIdInfo(ann);
        }
        return result;
    }

    public ObjectIdInfo findObjectReferenceInfo(Annotated ann, ObjectIdInfo objectIdInfo) {
        return this._primary.findObjectReferenceInfo(ann, this._secondary.findObjectReferenceInfo(ann, objectIdInfo));
    }

    public JsonFormat.Value findFormat(Annotated ann) {
        JsonFormat.Value result = this._primary.findFormat(ann);
        if (result == null) {
            return this._secondary.findFormat(ann);
        }
        return result;
    }

    public PropertyName findWrapperName(Annotated ann) {
        PropertyName name2;
        PropertyName name = this._primary.findWrapperName(ann);
        if (name == null) {
            return this._secondary.findWrapperName(ann);
        }
        if (name != PropertyName.USE_DEFAULT || (name2 = this._secondary.findWrapperName(ann)) == null) {
            return name;
        }
        return name2;
    }

    public String[] findSerializationPropertyOrder(AnnotatedClass ac) {
        String[] result = this._primary.findSerializationPropertyOrder(ac);
        if (result == null) {
            return this._secondary.findSerializationPropertyOrder(ac);
        }
        return result;
    }

    public Boolean findSerializationSortAlphabetically(AnnotatedClass ac) {
        Boolean result = this._primary.findSerializationSortAlphabetically(ac);
        if (result == null) {
            return this._secondary.findSerializationSortAlphabetically(ac);
        }
        return result;
    }

    public PropertyName findNameForSerialization(Annotated a) {
        PropertyName n2;
        PropertyName n = this._primary.findNameForSerialization(a);
        if (n == null) {
            return this._secondary.findNameForSerialization(a);
        }
        if (n != PropertyName.USE_DEFAULT || (n2 = this._secondary.findNameForSerialization(a)) == null) {
            return n;
        }
        return n2;
    }

    public boolean hasAsValueAnnotation(AnnotatedMethod am) {
        return this._primary.hasAsValueAnnotation(am) || this._secondary.hasAsValueAnnotation(am);
    }

    public String findEnumValue(Enum<?> value) {
        String result = this._primary.findEnumValue(value);
        if (result == null) {
            return this._secondary.findEnumValue(value);
        }
        return result;
    }

    public Object findDeserializer(Annotated am) {
        Object result = this._primary.findDeserializer(am);
        if (result == null) {
            return this._secondary.findDeserializer(am);
        }
        return result;
    }

    public Object findKeyDeserializer(Annotated am) {
        Object result = this._primary.findKeyDeserializer(am);
        if (result == null || result == KeyDeserializer.None.class || result == NoClass.class) {
            return this._secondary.findKeyDeserializer(am);
        }
        return result;
    }

    public Object findContentDeserializer(Annotated am) {
        Object result = this._primary.findContentDeserializer(am);
        if (result == null || result == JsonDeserializer.None.class || result == NoClass.class) {
            return this._secondary.findContentDeserializer(am);
        }
        return result;
    }

    public Class<?> findDeserializationType(Annotated am, JavaType baseType) {
        Class<?> result = this._primary.findDeserializationType(am, baseType);
        if (result == null) {
            return this._secondary.findDeserializationType(am, baseType);
        }
        return result;
    }

    public Class<?> findDeserializationKeyType(Annotated am, JavaType baseKeyType) {
        Class<?> result = this._primary.findDeserializationKeyType(am, baseKeyType);
        if (result == null) {
            return this._secondary.findDeserializationKeyType(am, baseKeyType);
        }
        return result;
    }

    public Class<?> findDeserializationContentType(Annotated am, JavaType baseContentType) {
        Class<?> result = this._primary.findDeserializationContentType(am, baseContentType);
        if (result == null) {
            return this._secondary.findDeserializationContentType(am, baseContentType);
        }
        return result;
    }

    public Object findValueInstantiator(AnnotatedClass ac) {
        Object result = this._primary.findValueInstantiator(ac);
        if (result == null) {
            return this._secondary.findValueInstantiator(ac);
        }
        return result;
    }

    public Class<?> findPOJOBuilder(AnnotatedClass ac) {
        Class<?> result = this._primary.findPOJOBuilder(ac);
        if (result == null) {
            return this._secondary.findPOJOBuilder(ac);
        }
        return result;
    }

    public JsonPOJOBuilder.Value findPOJOBuilderConfig(AnnotatedClass ac) {
        JsonPOJOBuilder.Value result = this._primary.findPOJOBuilderConfig(ac);
        if (result == null) {
            return this._secondary.findPOJOBuilderConfig(ac);
        }
        return result;
    }

    public PropertyName findNameForDeserialization(Annotated a) {
        PropertyName n2;
        PropertyName n = this._primary.findNameForDeserialization(a);
        if (n == null) {
            return this._secondary.findNameForDeserialization(a);
        }
        if (n != PropertyName.USE_DEFAULT || (n2 = this._secondary.findNameForDeserialization(a)) == null) {
            return n;
        }
        return n2;
    }

    public boolean hasAnySetterAnnotation(AnnotatedMethod am) {
        return this._primary.hasAnySetterAnnotation(am) || this._secondary.hasAnySetterAnnotation(am);
    }

    public boolean hasAnyGetterAnnotation(AnnotatedMethod am) {
        return this._primary.hasAnyGetterAnnotation(am) || this._secondary.hasAnyGetterAnnotation(am);
    }

    public boolean hasCreatorAnnotation(Annotated a) {
        return this._primary.hasCreatorAnnotation(a) || this._secondary.hasCreatorAnnotation(a);
    }

    @Deprecated
    public boolean isHandled(Annotation ann) {
        return this._primary.isHandled(ann) || this._secondary.isHandled(ann);
    }

    @Deprecated
    public String findDeserializationName(AnnotatedMethod am) {
        String str2;
        String result = this._primary.findDeserializationName(am);
        if (result == null) {
            return this._secondary.findDeserializationName(am);
        }
        if (result.length() != 0 || (str2 = this._secondary.findDeserializationName(am)) == null) {
            return result;
        }
        return str2;
    }

    @Deprecated
    public String findDeserializationName(AnnotatedField af) {
        String str2;
        String result = this._primary.findDeserializationName(af);
        if (result == null) {
            return this._secondary.findDeserializationName(af);
        }
        if (result.length() != 0 || (str2 = this._secondary.findDeserializationName(af)) == null) {
            return result;
        }
        return str2;
    }

    @Deprecated
    public String findDeserializationName(AnnotatedParameter param) {
        String result = this._primary.findDeserializationName(param);
        if (result == null) {
            return this._secondary.findDeserializationName(param);
        }
        return result;
    }

    @Deprecated
    public String findSerializationName(AnnotatedMethod am) {
        String str2;
        String result = this._primary.findSerializationName(am);
        if (result == null) {
            return this._secondary.findSerializationName(am);
        }
        if (result.length() != 0 || (str2 = this._secondary.findSerializationName(am)) == null) {
            return result;
        }
        return str2;
    }

    @Deprecated
    public String findSerializationName(AnnotatedField af) {
        String str2;
        String result = this._primary.findSerializationName(af);
        if (result == null) {
            return this._secondary.findSerializationName(af);
        }
        if (result.length() != 0 || (str2 = this._secondary.findSerializationName(af)) == null) {
            return result;
        }
        return str2;
    }
}
