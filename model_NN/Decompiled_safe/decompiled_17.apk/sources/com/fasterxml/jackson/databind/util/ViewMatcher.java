package com.fasterxml.jackson.databind.util;

import java.io.Serializable;

public abstract class ViewMatcher {
    public abstract boolean isVisibleForView(Class<?> cls);

    public static ViewMatcher construct(Class<?>[] views) {
        if (views == null) {
            return Empty.instance;
        }
        switch (views.length) {
            case 0:
                return Empty.instance;
            case 1:
                return new Single(views[0]);
            default:
                return new Multi(views);
        }
    }

    private static final class Empty extends ViewMatcher implements Serializable {
        static final Empty instance = new Empty();
        private static final long serialVersionUID = 1;

        private Empty() {
        }

        public boolean isVisibleForView(Class<?> cls) {
            return false;
        }
    }

    private static final class Single extends ViewMatcher implements Serializable {
        private static final long serialVersionUID = 1;
        private final Class<?> _view;

        public Single(Class<?> v) {
            this._view = v;
        }

        public boolean isVisibleForView(Class<?> activeView) {
            return activeView == this._view || this._view.isAssignableFrom(activeView);
        }
    }

    private static final class Multi extends ViewMatcher implements Serializable {
        private static final long serialVersionUID = 1;
        private final Class<?>[] _views;

        public Multi(Class<?>[] v) {
            this._views = v;
        }

        public boolean isVisibleForView(Class<?> activeView) {
            for (Class<?> view : this._views) {
                if (activeView == view || view.isAssignableFrom(activeView)) {
                    return true;
                }
            }
            return false;
        }
    }
}
