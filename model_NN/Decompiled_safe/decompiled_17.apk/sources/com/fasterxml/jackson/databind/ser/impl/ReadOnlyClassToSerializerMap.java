package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ser.SerializerCache;
import java.util.HashMap;

public final class ReadOnlyClassToSerializerMap {
    protected SerializerCache.TypeKey _cacheKey = null;
    protected final JsonSerializerMap _map;

    private ReadOnlyClassToSerializerMap(JsonSerializerMap map) {
        this._map = map;
    }

    public ReadOnlyClassToSerializerMap instance() {
        return new ReadOnlyClassToSerializerMap(this._map);
    }

    public static ReadOnlyClassToSerializerMap from(HashMap<SerializerCache.TypeKey, JsonSerializer<Object>> src) {
        return new ReadOnlyClassToSerializerMap(new JsonSerializerMap(src));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
     arg types: [com.fasterxml.jackson.databind.JavaType, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void */
    public JsonSerializer<Object> typedValueSerializer(JavaType type) {
        if (this._cacheKey == null) {
            this._cacheKey = new SerializerCache.TypeKey(type, true);
        } else {
            this._cacheKey.resetTyped(type);
        }
        return this._map.find(this._cacheKey);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public JsonSerializer<Object> typedValueSerializer(Class<?> cls) {
        if (this._cacheKey == null) {
            this._cacheKey = new SerializerCache.TypeKey(cls, true);
        } else {
            this._cacheKey.resetTyped(cls);
        }
        return this._map.find(this._cacheKey);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
     arg types: [com.fasterxml.jackson.databind.JavaType, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void */
    public JsonSerializer<Object> untypedValueSerializer(JavaType type) {
        if (this._cacheKey == null) {
            this._cacheKey = new SerializerCache.TypeKey(type, false);
        } else {
            this._cacheKey.resetUntyped(type);
        }
        return this._map.find(this._cacheKey);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public JsonSerializer<Object> untypedValueSerializer(Class<?> cls) {
        if (this._cacheKey == null) {
            this._cacheKey = new SerializerCache.TypeKey(cls, false);
        } else {
            this._cacheKey.resetUntyped(cls);
        }
        return this._map.find(this._cacheKey);
    }
}
