package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.AbstractTypeResolver;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.cfg.DeserializerFactoryConfig;
import com.fasterxml.jackson.databind.deser.impl.FieldProperty;
import com.fasterxml.jackson.databind.deser.impl.MethodProperty;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdReader;
import com.fasterxml.jackson.databind.deser.impl.PropertyBasedObjectIdGenerator;
import com.fasterxml.jackson.databind.deser.impl.SetterlessProperty;
import com.fasterxml.jackson.databind.deser.std.JdkDeserializers;
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.introspect.ObjectIdInfo;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.type.ClassKey;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.ClassUtil;
import com.fasterxml.jackson.databind.util.SimpleBeanPropertyDefinition;
import com.tencent.mm.sdk.message.RMsgInfoDB;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

public class BeanDeserializerFactory extends BasicDeserializerFactory implements Serializable {
    private static final Class<?>[] INIT_CAUSE_PARAMS = {Throwable.class};
    private static final Class<?>[] NO_VIEWS = new Class[0];
    public static final BeanDeserializerFactory instance = new BeanDeserializerFactory(new DeserializerFactoryConfig());
    private static final long serialVersionUID = 1;

    public BeanDeserializerFactory(DeserializerFactoryConfig config) {
        super(config);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public DeserializerFactory withConfig(DeserializerFactoryConfig config) {
        if (this._factoryConfig == config) {
            return this;
        }
        if (getClass() == BeanDeserializerFactory.class) {
            return new BeanDeserializerFactory(config);
        }
        throw new IllegalStateException("Subtype of BeanDeserializerFactory (" + getClass().getName() + ") has not properly overridden method 'withAdditionalDeserializers': can not instantiate subtype with " + "additional deserializer definitions");
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _findCustomBeanDeserializer(JavaType type, DeserializationConfig config, BeanDescription beanDesc) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findBeanDeserializer(type, config, beanDesc);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    public JsonDeserializer<Object> createBeanDeserializer(DeserializationContext ctxt, JavaType type, BeanDescription beanDesc) throws JsonMappingException {
        JavaType concreteType;
        DeserializationConfig config = ctxt.getConfig();
        JsonDeserializer<Object> custom = _findCustomBeanDeserializer(type, config, beanDesc);
        if (custom != null) {
            return custom;
        }
        if (type.isThrowable()) {
            return buildThrowableDeserializer(ctxt, type, beanDesc);
        }
        if (type.isAbstract() && (concreteType = materializeAbstractType(config, beanDesc)) != null) {
            return buildBeanDeserializer(ctxt, concreteType, config.introspect(concreteType));
        }
        JsonDeserializer<Object> deser = findStdDeserializer(config, type);
        if (deser != null) {
            return deser;
        }
        if (!isPotentialBeanType(type.getRawClass())) {
            return null;
        }
        return buildBeanDeserializer(ctxt, type, beanDesc);
    }

    public JsonDeserializer<Object> createBuilderBasedDeserializer(DeserializationContext ctxt, JavaType valueType, BeanDescription beanDesc, Class<?> builderClass) throws JsonMappingException {
        return buildBuilderBasedDeserializer(ctxt, valueType, ctxt.getConfig().introspectForBuilder(ctxt.constructType(builderClass)));
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> findStdDeserializer(DeserializationConfig config, JavaType type) throws JsonMappingException {
        JavaType referencedType;
        Class<?> cls = type.getRawClass();
        JsonDeserializer<Object> deser = (JsonDeserializer) _simpleDeserializers.get(new ClassKey(cls));
        if (deser != null) {
            return deser;
        }
        if (AtomicReference.class.isAssignableFrom(cls)) {
            JavaType[] params = config.getTypeFactory().findTypeParameters(type, AtomicReference.class);
            if (params == null || params.length < 1) {
                referencedType = TypeFactory.unknownType();
            } else {
                referencedType = params[0];
            }
            return new JdkDeserializers.AtomicReferenceDeserializer(referencedType);
        }
        JsonDeserializer<Object> d = this.optionalHandlers.findDeserializer(type, config);
        if (d != null) {
            return d;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JavaType materializeAbstractType(DeserializationConfig config, BeanDescription beanDesc) throws JsonMappingException {
        JavaType abstractType = beanDesc.getType();
        for (AbstractTypeResolver r : this._factoryConfig.abstractTypeResolvers()) {
            JavaType concrete = r.resolveAbstractType(config, abstractType);
            if (concrete != null) {
                return concrete;
            }
        }
        return null;
    }

    public JsonDeserializer<Object> buildBeanDeserializer(DeserializationContext ctxt, JavaType type, BeanDescription beanDesc) throws JsonMappingException {
        JsonDeserializer<?> deserializer;
        ValueInstantiator valueInstantiator = findValueInstantiator(ctxt, beanDesc);
        BeanDeserializerBuilder builder = constructBeanDeserializerBuilder(ctxt, beanDesc);
        builder.setValueInstantiator(valueInstantiator);
        addBeanProps(ctxt, beanDesc, builder);
        addObjectIdReader(ctxt, beanDesc, builder);
        addReferenceProperties(ctxt, beanDesc, builder);
        addInjectables(ctxt, beanDesc, builder);
        DeserializationConfig config = ctxt.getConfig();
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod : this._factoryConfig.deserializerModifiers()) {
                builder = mod.updateBuilder(config, beanDesc, builder);
            }
        }
        if (!type.isAbstract() || valueInstantiator.canInstantiate()) {
            deserializer = builder.build();
        } else {
            deserializer = builder.buildAbstract();
        }
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod2 : this._factoryConfig.deserializerModifiers()) {
                deserializer = mod2.modifyDeserializer(config, beanDesc, deserializer);
            }
        }
        return deserializer;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> buildBuilderBasedDeserializer(DeserializationContext ctxt, JavaType valueType, BeanDescription builderDesc) throws JsonMappingException {
        ValueInstantiator valueInstantiator = findValueInstantiator(ctxt, builderDesc);
        DeserializationConfig config = ctxt.getConfig();
        BeanDeserializerBuilder builder = constructBeanDeserializerBuilder(ctxt, builderDesc);
        builder.setValueInstantiator(valueInstantiator);
        addBeanProps(ctxt, builderDesc, builder);
        addObjectIdReader(ctxt, builderDesc, builder);
        addReferenceProperties(ctxt, builderDesc, builder);
        addInjectables(ctxt, builderDesc, builder);
        JsonPOJOBuilder.Value builderConfig = builderDesc.findPOJOBuilderConfig();
        String buildMethodName = builderConfig == null ? "build" : builderConfig.buildMethodName;
        AnnotatedMethod buildMethod = builderDesc.findMethod(buildMethodName, null);
        if (buildMethod != null && config.canOverrideAccessModifiers()) {
            ClassUtil.checkAndFixAccess(buildMethod.getMember());
        }
        builder.setPOJOBuilder(buildMethod, builderConfig);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod : this._factoryConfig.deserializerModifiers()) {
                builder = mod.updateBuilder(config, builderDesc, builder);
            }
        }
        JsonDeserializer<?> deserializer = builder.buildBuilderBased(valueType, buildMethodName);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod2 : this._factoryConfig.deserializerModifiers()) {
                deserializer = mod2.modifyDeserializer(config, builderDesc, deserializer);
            }
        }
        return deserializer;
    }

    /* access modifiers changed from: protected */
    public void addObjectIdReader(DeserializationContext ctxt, BeanDescription beanDesc, BeanDeserializerBuilder builder) throws JsonMappingException {
        JavaType idType;
        SettableBeanProperty idProp;
        ObjectIdGenerator<?> gen;
        ObjectIdInfo objectIdInfo = beanDesc.getObjectIdInfo();
        if (objectIdInfo != null) {
            Class<?> implClass = objectIdInfo.getGeneratorType();
            if (implClass == ObjectIdGenerators.PropertyGenerator.class) {
                String propName = objectIdInfo.getPropertyName();
                idProp = builder.findProperty(propName);
                if (idProp == null) {
                    throw new IllegalArgumentException("Invalid Object Id definition for " + beanDesc.getBeanClass().getName() + ": can not find property with name '" + propName + "'");
                }
                idType = idProp.getType();
                gen = new PropertyBasedObjectIdGenerator(objectIdInfo.getScope());
            } else {
                idType = ctxt.getTypeFactory().findTypeParameters(ctxt.constructType(implClass), ObjectIdGenerator.class)[0];
                idProp = null;
                gen = ctxt.objectIdGeneratorInstance(beanDesc.getClassInfo(), objectIdInfo);
            }
            builder.setObjectIdReader(ObjectIdReader.construct(idType, objectIdInfo.getPropertyName(), gen, ctxt.findRootValueDeserializer(idType), idProp));
        }
    }

    public JsonDeserializer<Object> buildThrowableDeserializer(DeserializationContext ctxt, JavaType type, BeanDescription beanDesc) throws JsonMappingException {
        SettableBeanProperty prop;
        DeserializationConfig config = ctxt.getConfig();
        BeanDeserializerBuilder builder = constructBeanDeserializerBuilder(ctxt, beanDesc);
        builder.setValueInstantiator(findValueInstantiator(ctxt, beanDesc));
        addBeanProps(ctxt, beanDesc, builder);
        AnnotatedMethod am = beanDesc.findMethod("initCause", INIT_CAUSE_PARAMS);
        if (!(am == null || (prop = constructSettableProperty(ctxt, beanDesc, new SimpleBeanPropertyDefinition(am, "cause"), am.getGenericParameterType(0))) == null)) {
            builder.addOrReplaceProperty(prop, true);
        }
        builder.addIgnorable("localizedMessage");
        builder.addIgnorable("suppressed");
        builder.addIgnorable(RMsgInfoDB.TABLE);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod : this._factoryConfig.deserializerModifiers()) {
                builder = mod.updateBuilder(config, beanDesc, builder);
            }
        }
        JsonDeserializer<?> deserializer = builder.build();
        if (deserializer instanceof BeanDeserializer) {
            deserializer = new ThrowableDeserializer((BeanDeserializer) deserializer);
        }
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod2 : this._factoryConfig.deserializerModifiers()) {
                deserializer = mod2.modifyDeserializer(config, beanDesc, deserializer);
            }
        }
        return deserializer;
    }

    /* access modifiers changed from: protected */
    public BeanDeserializerBuilder constructBeanDeserializerBuilder(DeserializationContext ctxt, BeanDescription beanDesc) {
        return new BeanDeserializerBuilder(beanDesc, ctxt.getConfig());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addBeanProps(com.fasterxml.jackson.databind.DeserializationContext r34, com.fasterxml.jackson.databind.BeanDescription r35, com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder r36) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r33 = this;
            com.fasterxml.jackson.databind.deser.ValueInstantiator r5 = r36.getValueInstantiator()
            com.fasterxml.jackson.databind.DeserializationConfig r6 = r34.getConfig()
            com.fasterxml.jackson.databind.deser.SettableBeanProperty[] r15 = r5.getFromObjectArguments(r6)
            com.fasterxml.jackson.databind.AnnotationIntrospector r21 = r34.getAnnotationIntrospector()
            r19 = 0
            com.fasterxml.jackson.databind.introspect.AnnotatedClass r5 = r35.getClassInfo()
            r0 = r21
            java.lang.Boolean r11 = r0.findIgnoreUnknownProperties(r5)
            if (r11 == 0) goto L_0x0029
            boolean r19 = r11.booleanValue()
            r0 = r36
            r1 = r19
            r0.setIgnoreUnknownProperties(r1)
        L_0x0029:
            com.fasterxml.jackson.databind.introspect.AnnotatedClass r5 = r35.getClassInfo()
            r0 = r21
            java.lang.String[] r5 = r0.findPropertiesToIgnore(r5)
            java.util.HashSet r10 = com.fasterxml.jackson.databind.util.ArrayBuilders.arrayToSet(r5)
            java.util.Iterator r17 = r10.iterator()
        L_0x003b:
            boolean r5 = r17.hasNext()
            if (r5 == 0) goto L_0x004f
            java.lang.Object r28 = r17.next()
            java.lang.String r28 = (java.lang.String) r28
            r0 = r36
            r1 = r28
            r0.addIgnorable(r1)
            goto L_0x003b
        L_0x004f:
            com.fasterxml.jackson.databind.introspect.AnnotatedMethod r12 = r35.findAnySetter()
            if (r12 == 0) goto L_0x0064
            r0 = r33
            r1 = r34
            r2 = r35
            com.fasterxml.jackson.databind.deser.SettableAnyProperty r5 = r0.constructAnySetter(r1, r2, r12)
            r0 = r36
            r0.setAnySetter(r5)
        L_0x0064:
            if (r12 != 0) goto L_0x0084
            java.util.Set r20 = r35.getIgnoredPropertyNames()
            if (r20 == 0) goto L_0x0084
            java.util.Iterator r17 = r20.iterator()
        L_0x0070:
            boolean r5 = r17.hasNext()
            if (r5 == 0) goto L_0x0084
            java.lang.Object r28 = r17.next()
            java.lang.String r28 = (java.lang.String) r28
            r0 = r36
            r1 = r28
            r0.addIgnorable(r1)
            goto L_0x0070
        L_0x0084:
            com.fasterxml.jackson.databind.MapperFeature r5 = com.fasterxml.jackson.databind.MapperFeature.USE_GETTERS_AS_SETTERS
            r0 = r34
            boolean r5 = r0.isEnabled(r5)
            if (r5 == 0) goto L_0x00db
            com.fasterxml.jackson.databind.MapperFeature r5 = com.fasterxml.jackson.databind.MapperFeature.AUTO_DETECT_GETTERS
            r0 = r34
            boolean r5 = r0.isEnabled(r5)
            if (r5 == 0) goto L_0x00db
            r31 = 1
        L_0x009a:
            java.util.List r9 = r35.findProperties()
            r5 = r33
            r6 = r34
            r7 = r35
            r8 = r36
            java.util.List r27 = r5.filterBeanProps(r6, r7, r8, r9, r10)
            r0 = r33
            com.fasterxml.jackson.databind.cfg.DeserializerFactoryConfig r5 = r0._factoryConfig
            boolean r5 = r5.hasDeserializerModifiers()
            if (r5 == 0) goto L_0x00de
            r0 = r33
            com.fasterxml.jackson.databind.cfg.DeserializerFactoryConfig r5 = r0._factoryConfig
            java.lang.Iterable r5 = r5.deserializerModifiers()
            java.util.Iterator r17 = r5.iterator()
        L_0x00c0:
            boolean r5 = r17.hasNext()
            if (r5 == 0) goto L_0x00de
            java.lang.Object r23 = r17.next()
            com.fasterxml.jackson.databind.deser.BeanDeserializerModifier r23 = (com.fasterxml.jackson.databind.deser.BeanDeserializerModifier) r23
            com.fasterxml.jackson.databind.DeserializationConfig r5 = r34.getConfig()
            r0 = r23
            r1 = r35
            r2 = r27
            java.util.List r27 = r0.updateProperties(r5, r1, r2)
            goto L_0x00c0
        L_0x00db:
            r31 = 0
            goto L_0x009a
        L_0x00de:
            java.util.Iterator r17 = r27.iterator()
        L_0x00e2:
            boolean r5 = r17.hasNext()
            if (r5 == 0) goto L_0x01d4
            java.lang.Object r26 = r17.next()
            com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition r26 = (com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition) r26
            r25 = 0
            boolean r5 = r26.hasConstructorParameter()
            if (r5 == 0) goto L_0x0145
            java.lang.String r24 = r26.getName()
            r13 = r15
            int r0 = r13.length
            r22 = r0
            r18 = 0
        L_0x0100:
            r0 = r18
            r1 = r22
            if (r0 >= r1) goto L_0x0116
            r14 = r13[r18]
            java.lang.String r5 = r14.getName()
            r0 = r24
            boolean r5 = r0.equals(r5)
            if (r5 == 0) goto L_0x013a
            r25 = r14
        L_0x0116:
            if (r25 != 0) goto L_0x013d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Could not find creator property with name '"
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = "'"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r0 = r34
            com.fasterxml.jackson.databind.JsonMappingException r5 = r0.mappingException(r5)
            throw r5
        L_0x013a:
            int r18 = r18 + 1
            goto L_0x0100
        L_0x013d:
            r0 = r36
            r1 = r25
            r0.addCreatorProperty(r1)
            goto L_0x00e2
        L_0x0145:
            boolean r5 = r26.hasSetter()
            if (r5 == 0) goto L_0x0186
            com.fasterxml.jackson.databind.introspect.AnnotatedMethod r5 = r26.getSetter()
            r6 = 0
            java.lang.reflect.Type r29 = r5.getGenericParameterType(r6)
            r0 = r33
            r1 = r34
            r2 = r35
            r3 = r26
            r4 = r29
            com.fasterxml.jackson.databind.deser.SettableBeanProperty r25 = r0.constructSettableProperty(r1, r2, r3, r4)
        L_0x0162:
            if (r25 == 0) goto L_0x00e2
            java.lang.Class[] r32 = r26.findViews()
            if (r32 != 0) goto L_0x0176
            com.fasterxml.jackson.databind.MapperFeature r5 = com.fasterxml.jackson.databind.MapperFeature.DEFAULT_VIEW_INCLUSION
            r0 = r34
            boolean r5 = r0.isEnabled(r5)
            if (r5 != 0) goto L_0x0176
            java.lang.Class<?>[] r32 = com.fasterxml.jackson.databind.deser.BeanDeserializerFactory.NO_VIEWS
        L_0x0176:
            r0 = r25
            r1 = r32
            r0.setViews(r1)
            r0 = r36
            r1 = r25
            r0.addProperty(r1)
            goto L_0x00e2
        L_0x0186:
            boolean r5 = r26.hasField()
            if (r5 == 0) goto L_0x01a3
            com.fasterxml.jackson.databind.introspect.AnnotatedField r5 = r26.getField()
            java.lang.reflect.Type r29 = r5.getGenericType()
            r0 = r33
            r1 = r34
            r2 = r35
            r3 = r26
            r4 = r29
            com.fasterxml.jackson.databind.deser.SettableBeanProperty r25 = r0.constructSettableProperty(r1, r2, r3, r4)
            goto L_0x0162
        L_0x01a3:
            if (r31 == 0) goto L_0x0162
            boolean r5 = r26.hasGetter()
            if (r5 == 0) goto L_0x0162
            com.fasterxml.jackson.databind.introspect.AnnotatedMethod r16 = r26.getGetter()
            java.lang.Class r30 = r16.getRawType()
            java.lang.Class<java.util.Collection> r5 = java.util.Collection.class
            r0 = r30
            boolean r5 = r5.isAssignableFrom(r0)
            if (r5 != 0) goto L_0x01c7
            java.lang.Class<java.util.Map> r5 = java.util.Map.class
            r0 = r30
            boolean r5 = r5.isAssignableFrom(r0)
            if (r5 == 0) goto L_0x0162
        L_0x01c7:
            r0 = r33
            r1 = r34
            r2 = r35
            r3 = r26
            com.fasterxml.jackson.databind.deser.SettableBeanProperty r25 = r0.constructSetterlessProperty(r1, r2, r3)
            goto L_0x0162
        L_0x01d4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.BeanDeserializerFactory.addBeanProps(com.fasterxml.jackson.databind.DeserializationContext, com.fasterxml.jackson.databind.BeanDescription, com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder):void");
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyDefinition> filterBeanProps(DeserializationContext ctxt, BeanDescription beanDesc, BeanDeserializerBuilder builder, List<BeanPropertyDefinition> propDefsIn, Set<String> ignored) throws JsonMappingException {
        ArrayList<BeanPropertyDefinition> result = new ArrayList<>(Math.max(4, propDefsIn.size()));
        HashMap<Class<?>, Boolean> ignoredTypes = new HashMap<>();
        for (BeanPropertyDefinition property : propDefsIn) {
            String name = property.getName();
            if (!ignored.contains(name)) {
                if (!property.hasConstructorParameter()) {
                    Class<?> rawPropertyType = null;
                    if (property.hasSetter()) {
                        rawPropertyType = property.getSetter().getRawParameterType(0);
                    } else if (property.hasField()) {
                        rawPropertyType = property.getField().getRawType();
                    }
                    if (rawPropertyType != null && isIgnorableType(ctxt.getConfig(), beanDesc, rawPropertyType, ignoredTypes)) {
                        builder.addIgnorable(name);
                    }
                }
                result.add(property);
            }
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public void addReferenceProperties(DeserializationContext ctxt, BeanDescription beanDesc, BeanDeserializerBuilder builder) throws JsonMappingException {
        Type genericType;
        Map<String, AnnotatedMember> refs = beanDesc.findBackReferenceProperties();
        if (refs != null) {
            for (Map.Entry<String, AnnotatedMember> en : refs.entrySet()) {
                String name = (String) en.getKey();
                AnnotatedMember m = (AnnotatedMember) en.getValue();
                if (m instanceof AnnotatedMethod) {
                    genericType = ((AnnotatedMethod) m).getGenericParameterType(0);
                } else {
                    genericType = m.getRawType();
                }
                builder.addBackReferenceProperty(name, constructSettableProperty(ctxt, beanDesc, new SimpleBeanPropertyDefinition(m), genericType));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addInjectables(DeserializationContext ctxt, BeanDescription beanDesc, BeanDeserializerBuilder builder) throws JsonMappingException {
        Map<Object, AnnotatedMember> raw = beanDesc.findInjectables();
        if (raw != null) {
            boolean fixAccess = ctxt.canOverrideAccessModifiers();
            for (Map.Entry<Object, AnnotatedMember> entry : raw.entrySet()) {
                AnnotatedMember m = (AnnotatedMember) entry.getValue();
                if (fixAccess) {
                    m.fixAccess();
                }
                builder.addInjectable(m.getName(), beanDesc.resolveType(m.getGenericType()), beanDesc.getClassAnnotations(), m, entry.getKey());
            }
        }
    }

    /* access modifiers changed from: protected */
    public SettableAnyProperty constructAnySetter(DeserializationContext ctxt, BeanDescription beanDesc, AnnotatedMethod setter) throws JsonMappingException {
        if (ctxt.canOverrideAccessModifiers()) {
            setter.fixAccess();
        }
        JavaType type = beanDesc.bindingsForBeanType().resolveType(setter.getGenericParameterType(1));
        BeanProperty.Std property = new BeanProperty.Std(setter.getName(), type, beanDesc.getClassAnnotations(), setter);
        JavaType type2 = resolveType(ctxt, beanDesc, type, setter);
        JsonDeserializer<Object> deser = findDeserializerFromAnnotation(ctxt, setter);
        if (deser != null) {
            return new SettableAnyProperty(property, setter, type2, deser);
        }
        return new SettableAnyProperty(property, setter, modifyTypeByAnnotation(ctxt, setter, type2), (JsonDeserializer<Object>) null);
    }

    /* access modifiers changed from: protected */
    public SettableBeanProperty constructSettableProperty(DeserializationContext ctxt, BeanDescription beanDesc, BeanPropertyDefinition propDef, Type jdkType) throws JsonMappingException {
        SettableBeanProperty prop;
        AnnotatedMember mutator = propDef.getMutator();
        if (ctxt.canOverrideAccessModifiers()) {
            mutator.fixAccess();
        }
        JavaType t0 = beanDesc.resolveType(jdkType);
        BeanProperty.Std property = new BeanProperty.Std(propDef.getName(), t0, beanDesc.getClassAnnotations(), mutator);
        JavaType type = resolveType(ctxt, beanDesc, t0, mutator);
        if (type != t0) {
            BeanProperty.Std property2 = property.withType(type);
        }
        JsonDeserializer<Object> propDeser = findDeserializerFromAnnotation(ctxt, mutator);
        JavaType type2 = modifyTypeByAnnotation(ctxt, mutator, type);
        TypeDeserializer typeDeser = (TypeDeserializer) type2.getTypeHandler();
        if (mutator instanceof AnnotatedMethod) {
            prop = new MethodProperty(propDef, type2, typeDeser, beanDesc.getClassAnnotations(), (AnnotatedMethod) mutator);
        } else {
            prop = new FieldProperty(propDef, type2, typeDeser, beanDesc.getClassAnnotations(), (AnnotatedField) mutator);
        }
        if (propDeser != null) {
            prop = prop.withValueDeserializer(propDeser);
        }
        AnnotationIntrospector.ReferenceProperty ref = propDef.findReferenceType();
        if (ref != null && ref.isManagedReference()) {
            prop.setManagedReferenceName(ref.getName());
        }
        return prop;
    }

    /* access modifiers changed from: protected */
    public SettableBeanProperty constructSetterlessProperty(DeserializationContext ctxt, BeanDescription beanDesc, BeanPropertyDefinition propDef) throws JsonMappingException {
        AnnotatedMethod getter = propDef.getGetter();
        if (ctxt.canOverrideAccessModifiers()) {
            getter.fixAccess();
        }
        JavaType type = getter.getType(beanDesc.bindingsForBeanType());
        JsonDeserializer<Object> propDeser = findDeserializerFromAnnotation(ctxt, getter);
        JavaType type2 = modifyTypeByAnnotation(ctxt, getter, type);
        SettableBeanProperty prop = new SetterlessProperty(propDef, type2, (TypeDeserializer) type2.getTypeHandler(), beanDesc.getClassAnnotations(), getter);
        if (propDeser != null) {
            return prop.withValueDeserializer(propDeser);
        }
        return prop;
    }

    /* access modifiers changed from: protected */
    public boolean isPotentialBeanType(Class<?> type) {
        String typeStr = ClassUtil.canBeABeanType(type);
        if (typeStr != null) {
            throw new IllegalArgumentException("Can not deserialize Class " + type.getName() + " (of type " + typeStr + ") as a Bean");
        } else if (ClassUtil.isProxyType(type)) {
            throw new IllegalArgumentException("Can not deserialize Proxy class " + type.getName() + " as a Bean");
        } else {
            String typeStr2 = ClassUtil.isLocalType(type, true);
            if (typeStr2 == null) {
                return true;
            }
            throw new IllegalArgumentException("Can not deserialize Class " + type.getName() + " (of type " + typeStr2 + ") as a Bean");
        }
    }

    /* access modifiers changed from: protected */
    public boolean isIgnorableType(DeserializationConfig config, BeanDescription beanDesc, Class<?> type, Map<Class<?>, Boolean> ignoredTypes) {
        Boolean status = ignoredTypes.get(type);
        if (status == null) {
            status = config.getAnnotationIntrospector().isIgnorableType(config.introspectClassAnnotations(type).getClassInfo());
            if (status == null) {
                status = Boolean.FALSE;
            }
        }
        return status.booleanValue();
    }
}
