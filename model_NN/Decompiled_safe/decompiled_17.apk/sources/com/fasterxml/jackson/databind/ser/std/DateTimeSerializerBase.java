package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonValueFormat;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public abstract class DateTimeSerializerBase<T> extends StdScalarSerializer<T> implements ContextualSerializer {
    protected final DateFormat _customFormat;
    protected final boolean _useTimestamp;

    /* access modifiers changed from: protected */
    public abstract long _timestamp(Object obj);

    public abstract void serialize(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException;

    public abstract DateTimeSerializerBase<T> withFormat(boolean z, DateFormat dateFormat);

    protected DateTimeSerializerBase(Class<T> type, boolean useTimestamp, DateFormat customFormat) {
        super(type);
        this._useTimestamp = useTimestamp;
        this._customFormat = customFormat;
    }

    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        JsonFormat.Value format;
        DateFormat df;
        if (property == null || (format = prov.getAnnotationIntrospector().findFormat((Annotated) property.getMember())) == null) {
            return this;
        }
        if (format.getShape().isNumeric()) {
            return withFormat(true, null);
        }
        TimeZone tz = format.getTimeZone();
        String pattern = format.getPattern();
        if (pattern.length() > 0) {
            Locale loc = format.getLocale();
            if (loc == null) {
                loc = prov.getLocale();
            }
            SimpleDateFormat df2 = new SimpleDateFormat(pattern, loc);
            if (tz == null) {
                tz = prov.getTimeZone();
            }
            df2.setTimeZone(tz);
            return withFormat(false, df2);
        } else if (tz == null) {
            return this;
        } else {
            DateFormat df3 = prov.getConfig().getDateFormat();
            if (df3.getClass() == StdDateFormat.class) {
                df = StdDateFormat.getISO8601Format(tz);
            } else {
                df = (DateFormat) df3.clone();
                df.setTimeZone(tz);
            }
            return withFormat(false, df);
        }
    }

    public boolean isEmpty(T value) {
        return value == null || _timestamp(value) == 0;
    }

    public JsonNode getSchema(SerializerProvider provider, Type typeHint) {
        boolean asNumber = this._useTimestamp;
        if (!asNumber && this._customFormat == null) {
            asNumber = provider.isEnabled(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        }
        return createSchemaNode(asNumber ? "number" : "string", true);
    }

    public void acceptJsonFormatVisitor(JsonFormatVisitorWrapper visitor, JavaType typeHint) {
        boolean asNumber = this._useTimestamp;
        if (!asNumber && this._customFormat == null) {
            asNumber = visitor.getProvider().isEnabled(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        }
        if (asNumber) {
            visitor.expectNumberFormat(typeHint).format(JsonValueFormat.UTC_MILLISEC);
        } else {
            visitor.expectStringFormat(typeHint).format(JsonValueFormat.DATE_TIME);
        }
    }
}
