package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.util.Annotations;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Map;

public final class ManagedReferenceProperty extends SettableBeanProperty {
    private static final long serialVersionUID = 1;
    protected final SettableBeanProperty _backProperty;
    protected final boolean _isContainer;
    protected final SettableBeanProperty _managedProperty;
    protected final String _referenceName;

    public ManagedReferenceProperty(SettableBeanProperty forward, String refName, SettableBeanProperty backward, Annotations contextAnnotations, boolean isContainer) {
        super(forward.getName(), forward.getType(), forward.getValueTypeDeserializer(), contextAnnotations);
        this._referenceName = refName;
        this._managedProperty = forward;
        this._backProperty = backward;
        this._isContainer = isContainer;
    }

    protected ManagedReferenceProperty(ManagedReferenceProperty src, JsonDeserializer<?> deser) {
        super(src, deser);
        this._referenceName = src._referenceName;
        this._isContainer = src._isContainer;
        this._managedProperty = src._managedProperty;
        this._backProperty = src._backProperty;
    }

    protected ManagedReferenceProperty(ManagedReferenceProperty src, String newName) {
        super(src, newName);
        this._referenceName = src._referenceName;
        this._isContainer = src._isContainer;
        this._managedProperty = src._managedProperty;
        this._backProperty = src._backProperty;
    }

    public ManagedReferenceProperty withName(String newName) {
        return new ManagedReferenceProperty(this, newName);
    }

    public ManagedReferenceProperty withValueDeserializer(JsonDeserializer<?> deser) {
        return new ManagedReferenceProperty(this, deser);
    }

    public <A extends Annotation> A getAnnotation(Class<A> acls) {
        return this._managedProperty.getAnnotation(acls);
    }

    public AnnotatedMember getMember() {
        return this._managedProperty.getMember();
    }

    public void deserializeAndSet(JsonParser jp, DeserializationContext ctxt, Object instance) throws IOException, JsonProcessingException {
        set(instance, this._managedProperty.deserialize(jp, ctxt));
    }

    public Object deserializeSetAndReturn(JsonParser jp, DeserializationContext ctxt, Object instance) throws IOException, JsonProcessingException {
        return setAndReturn(instance, deserialize(jp, ctxt));
    }

    public final void set(Object instance, Object value) throws IOException {
        setAndReturn(instance, value);
    }

    public Object setAndReturn(Object instance, Object value) throws IOException {
        Object result = this._managedProperty.setAndReturn(instance, value);
        if (value != null) {
            if (!this._isContainer) {
                this._backProperty.set(value, instance);
            } else if (value instanceof Object[]) {
                for (Object ob : (Object[]) value) {
                    if (ob != null) {
                        this._backProperty.set(ob, instance);
                    }
                }
            } else if (value instanceof Collection) {
                for (Object ob2 : (Collection) value) {
                    if (ob2 != null) {
                        this._backProperty.set(ob2, instance);
                    }
                }
            } else if (value instanceof Map) {
                for (Object ob3 : ((Map) value).values()) {
                    if (ob3 != null) {
                        this._backProperty.set(ob3, instance);
                    }
                }
            } else {
                throw new IllegalStateException("Unsupported container type (" + value.getClass().getName() + ") when resolving reference '" + this._referenceName + "'");
            }
        }
        return result;
    }
}
