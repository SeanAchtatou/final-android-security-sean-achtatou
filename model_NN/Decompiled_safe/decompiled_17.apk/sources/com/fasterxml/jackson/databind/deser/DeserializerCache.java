package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.annotation.NoClass;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.util.ClassUtil;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public final class DeserializerCache implements Serializable {
    private static final long serialVersionUID = 1;
    protected final ConcurrentHashMap<JavaType, JsonDeserializer<Object>> _cachedDeserializers = new ConcurrentHashMap<>(64, 0.75f, 2);
    protected final HashMap<JavaType, JsonDeserializer<Object>> _incompleteDeserializers = new HashMap<>(8);

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        this._incompleteDeserializers.clear();
        return this;
    }

    public int cachedDeserializersCount() {
        return this._cachedDeserializers.size();
    }

    public void flushCachedDeserializers() {
        this._cachedDeserializers.clear();
    }

    public JsonDeserializer<Object> findValueDeserializer(DeserializationContext ctxt, DeserializerFactory factory, JavaType propertyType) throws JsonMappingException {
        JsonDeserializer<Object> deser = _findCachedDeserializer(propertyType);
        if (deser != null) {
            return deser;
        }
        JsonDeserializer<Object> deser2 = _createAndCacheValueDeserializer(ctxt, factory, propertyType);
        if (deser2 == null) {
            deser2 = _handleUnknownValueDeserializer(propertyType);
        }
        return deser2;
    }

    public KeyDeserializer findKeyDeserializer(DeserializationContext ctxt, DeserializerFactory factory, JavaType type) throws JsonMappingException {
        KeyDeserializer kd = factory.createKeyDeserializer(ctxt, type);
        if (kd == null) {
            return _handleUnknownKeyDeserializer(type);
        }
        if (!(kd instanceof ResolvableDeserializer)) {
            return kd;
        }
        ((ResolvableDeserializer) kd).resolve(ctxt);
        return kd;
    }

    public boolean hasValueDeserializerFor(DeserializationContext ctxt, DeserializerFactory factory, JavaType type) {
        JsonDeserializer<Object> deser = _findCachedDeserializer(type);
        if (deser == null) {
            try {
                deser = _createAndCacheValueDeserializer(ctxt, factory, type);
            } catch (Exception e) {
                return false;
            }
        }
        if (deser != null) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _findCachedDeserializer(JavaType type) {
        if (type != null) {
            return this._cachedDeserializers.get(type);
        }
        throw new IllegalArgumentException("Null JavaType passed");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return r2;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object> _createAndCacheValueDeserializer(com.fasterxml.jackson.databind.DeserializationContext r6, com.fasterxml.jackson.databind.deser.DeserializerFactory r7, com.fasterxml.jackson.databind.JavaType r8) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r5 = this;
            java.util.HashMap<com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object>> r3 = r5._incompleteDeserializers
            monitor-enter(r3)
            com.fasterxml.jackson.databind.JsonDeserializer r1 = r5._findCachedDeserializer(r8)     // Catch:{ all -> 0x0036 }
            if (r1 == 0) goto L_0x000c
            monitor-exit(r3)     // Catch:{ all -> 0x0036 }
            r2 = r1
        L_0x000b:
            return r2
        L_0x000c:
            java.util.HashMap<com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object>> r2 = r5._incompleteDeserializers     // Catch:{ all -> 0x0036 }
            int r0 = r2.size()     // Catch:{ all -> 0x0036 }
            if (r0 <= 0) goto L_0x0021
            java.util.HashMap<com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object>> r2 = r5._incompleteDeserializers     // Catch:{ all -> 0x0036 }
            java.lang.Object r1 = r2.get(r8)     // Catch:{ all -> 0x0036 }
            com.fasterxml.jackson.databind.JsonDeserializer r1 = (com.fasterxml.jackson.databind.JsonDeserializer) r1     // Catch:{ all -> 0x0036 }
            if (r1 == 0) goto L_0x0021
            monitor-exit(r3)     // Catch:{ all -> 0x0036 }
            r2 = r1
            goto L_0x000b
        L_0x0021:
            com.fasterxml.jackson.databind.JsonDeserializer r2 = r5._createAndCache2(r6, r7, r8)     // Catch:{ all -> 0x0039 }
            if (r0 != 0) goto L_0x0034
            java.util.HashMap<com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0036 }
            int r4 = r4.size()     // Catch:{ all -> 0x0036 }
            if (r4 <= 0) goto L_0x0034
            java.util.HashMap<com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0036 }
            r4.clear()     // Catch:{ all -> 0x0036 }
        L_0x0034:
            monitor-exit(r3)     // Catch:{ all -> 0x0036 }
            goto L_0x000b
        L_0x0036:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0036 }
            throw r2
        L_0x0039:
            r2 = move-exception
            if (r0 != 0) goto L_0x0049
            java.util.HashMap<com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0036 }
            int r4 = r4.size()     // Catch:{ all -> 0x0036 }
            if (r4 <= 0) goto L_0x0049
            java.util.HashMap<com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Object>> r4 = r5._incompleteDeserializers     // Catch:{ all -> 0x0036 }
            r4.clear()     // Catch:{ all -> 0x0036 }
        L_0x0049:
            throw r2     // Catch:{ all -> 0x0036 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.DeserializerCache._createAndCacheValueDeserializer(com.fasterxml.jackson.databind.DeserializationContext, com.fasterxml.jackson.databind.deser.DeserializerFactory, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.JsonDeserializer");
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _createAndCache2(DeserializationContext ctxt, DeserializerFactory factory, JavaType type) throws JsonMappingException {
        try {
            JsonDeserializer<Object> deser = _createDeserializer(ctxt, factory, type);
            if (deser == null) {
                return null;
            }
            boolean isResolvable = deser instanceof ResolvableDeserializer;
            boolean addToCache = deser.isCachable();
            if (isResolvable) {
                this._incompleteDeserializers.put(type, deser);
                ((ResolvableDeserializer) deser).resolve(ctxt);
                this._incompleteDeserializers.remove(type);
            }
            if (!addToCache) {
                return deser;
            }
            this._cachedDeserializers.put(type, deser);
            return deser;
        } catch (IllegalArgumentException iae) {
            throw new JsonMappingException(iae.getMessage(), null, iae);
        }
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _createDeserializer(DeserializationContext ctxt, DeserializerFactory factory, JavaType type) throws JsonMappingException {
        JsonFormat.Value format;
        DeserializationConfig config = ctxt.getConfig();
        if (type.isAbstract() || type.isMapLikeType() || type.isCollectionLikeType()) {
            type = factory.mapAbstractType(config, type);
        }
        BeanDescription beanDesc = config.introspect(type);
        JsonDeserializer<Object> deser = findDeserializerFromAnnotation(ctxt, beanDesc.getClassInfo());
        if (deser != null) {
            return deser;
        }
        JavaType newType = modifyTypeByAnnotation(ctxt, beanDesc.getClassInfo(), type);
        if (newType != type) {
            type = newType;
            beanDesc = config.introspect(newType);
        }
        Class<?> builder = beanDesc.findPOJOBuilder();
        if (builder != null) {
            return factory.createBuilderBasedDeserializer(ctxt, type, beanDesc, builder);
        }
        if (type.isEnumType()) {
            return factory.createEnumDeserializer(ctxt, type, beanDesc);
        }
        if (type.isContainerType()) {
            if (type.isArrayType()) {
                return factory.createArrayDeserializer(ctxt, (ArrayType) type, beanDesc);
            }
            if (type.isMapLikeType()) {
                MapLikeType mlt = (MapLikeType) type;
                if (mlt.isTrueMapType()) {
                    return factory.createMapDeserializer(ctxt, (MapType) mlt, beanDesc);
                }
                return factory.createMapLikeDeserializer(ctxt, mlt, beanDesc);
            } else if (type.isCollectionLikeType() && ((format = beanDesc.findExpectedFormat(null)) == null || format.getShape() != JsonFormat.Shape.OBJECT)) {
                CollectionLikeType clt = (CollectionLikeType) type;
                if (clt.isTrueCollectionType()) {
                    return factory.createCollectionDeserializer(ctxt, (CollectionType) clt, beanDesc);
                }
                return factory.createCollectionLikeDeserializer(ctxt, clt, beanDesc);
            }
        }
        if (JsonNode.class.isAssignableFrom(type.getRawClass())) {
            return factory.createTreeDeserializer(config, type, beanDesc);
        }
        return factory.createBeanDeserializer(ctxt, type, beanDesc);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> findDeserializerFromAnnotation(DeserializationContext ctxt, Annotated ann) throws JsonMappingException {
        Object deserDef = ctxt.getAnnotationIntrospector().findDeserializer(ann);
        if (deserDef == null) {
            return null;
        }
        return ctxt.deserializerInstance(ann, deserDef);
    }

    private JavaType modifyTypeByAnnotation(DeserializationContext ctxt, Annotated a, JavaType type) throws JsonMappingException {
        Object cdDef;
        Object kdDef;
        KeyDeserializer kd;
        AnnotationIntrospector intr = ctxt.getAnnotationIntrospector();
        Class<?> subclass = intr.findDeserializationType(a, type);
        if (subclass != null) {
            try {
                type = type.narrowBy(subclass);
            } catch (IllegalArgumentException iae) {
                throw new JsonMappingException("Failed to narrow type " + type + " with concrete-type annotation (value " + subclass.getName() + "), method '" + a.getName() + "': " + iae.getMessage(), null, iae);
            }
        }
        if (!type.isContainerType()) {
            return type;
        }
        Class<?> keyClass = intr.findDeserializationKeyType(a, type.getKeyType());
        if (keyClass != null) {
            if (!(type instanceof MapLikeType)) {
                throw new JsonMappingException("Illegal key-type annotation: type " + type + " is not a Map(-like) type");
            }
            try {
                type = ((MapLikeType) type).narrowKey(keyClass);
            } catch (IllegalArgumentException iae2) {
                throw new JsonMappingException("Failed to narrow key type " + type + " with key-type annotation (" + keyClass.getName() + "): " + iae2.getMessage(), null, iae2);
            }
        }
        JavaType keyType = type.getKeyType();
        if (!(keyType == null || keyType.getValueHandler() != null || (kdDef = intr.findKeyDeserializer(a)) == null || (kd = ctxt.keyDeserializerInstance(a, kdDef)) == null)) {
            type = ((MapLikeType) type).withKeyValueHandler(kd);
            JavaType keyType2 = type.getKeyType();
        }
        Class<?> cc = intr.findDeserializationContentType(a, type.getContentType());
        if (cc != null) {
            try {
                type = type.narrowContentsBy(cc);
            } catch (IllegalArgumentException iae3) {
                throw new JsonMappingException("Failed to narrow content type " + type + " with content-type annotation (" + cc.getName() + "): " + iae3.getMessage(), null, iae3);
            }
        }
        if (type.getContentType().getValueHandler() != null || (cdDef = intr.findContentDeserializer(a)) == null) {
            return type;
        }
        JsonDeserializer<?> cd = null;
        if (cdDef instanceof JsonDeserializer) {
            Object cdDef2 = (JsonDeserializer) cdDef;
        } else {
            Class<?> cdClass = _verifyAsClass(cdDef, "findContentDeserializer", JsonDeserializer.None.class);
            if (cdClass != null) {
                cd = ctxt.deserializerInstance(a, cdClass);
            }
        }
        if (cd != null) {
            return type.withContentValueHandler(cd);
        }
        return type;
    }

    private Class<?> _verifyAsClass(Object src, String methodName, Class<?> noneClass) {
        if (src == null) {
            return null;
        }
        if (!(src instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector." + methodName + "() returned value of type " + src.getClass().getName() + ": expected type JsonSerializer or Class<JsonSerializer> instead");
        }
        Class<?> cls = (Class) src;
        if (cls == noneClass || cls == NoClass.class) {
            return null;
        }
        return cls;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _handleUnknownValueDeserializer(JavaType type) throws JsonMappingException {
        if (!ClassUtil.isConcrete(type.getRawClass())) {
            throw new JsonMappingException("Can not find a Value deserializer for abstract type " + type);
        }
        throw new JsonMappingException("Can not find a Value deserializer for type " + type);
    }

    /* access modifiers changed from: protected */
    public KeyDeserializer _handleUnknownKeyDeserializer(JavaType type) throws JsonMappingException {
        throw new JsonMappingException("Can not find a (Map) Key deserializer for type " + type);
    }
}
