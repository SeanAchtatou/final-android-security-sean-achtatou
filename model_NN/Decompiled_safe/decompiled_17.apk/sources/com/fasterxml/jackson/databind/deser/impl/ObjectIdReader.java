package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import java.io.Serializable;

public final class ObjectIdReader implements Serializable {
    private static final long serialVersionUID = 1;
    public final JsonDeserializer<Object> deserializer;
    public final ObjectIdGenerator<?> generator;
    public final SettableBeanProperty idProperty;
    public final JavaType idType;
    public final String propertyName;

    protected ObjectIdReader(JavaType t, String propName, ObjectIdGenerator<?> gen, JsonDeserializer<?> deser, SettableBeanProperty idProp) {
        this.idType = t;
        this.propertyName = propName;
        this.generator = gen;
        this.deserializer = deser;
        this.idProperty = idProp;
    }

    public static ObjectIdReader construct(JavaType idType2, String propName, ObjectIdGenerator<?> generator2, JsonDeserializer<?> deser, SettableBeanProperty idProp) {
        return new ObjectIdReader(idType2, propName, generator2, deser, idProp);
    }
}
