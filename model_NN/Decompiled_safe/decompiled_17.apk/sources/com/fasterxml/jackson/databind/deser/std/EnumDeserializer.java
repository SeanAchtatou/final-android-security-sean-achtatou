package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.util.ClassUtil;
import com.fasterxml.jackson.databind.util.EnumResolver;
import java.io.IOException;
import java.lang.reflect.Method;

public class EnumDeserializer extends StdScalarDeserializer<Enum<?>> {
    private static final long serialVersionUID = -5893263645879532318L;
    protected final EnumResolver<?> _resolver;

    public EnumDeserializer(EnumResolver<?> res) {
        super(Enum.class);
        this._resolver = res;
    }

    public static JsonDeserializer<?> deserializerForCreator(DeserializationConfig config, Class<?> enumClass, AnnotatedMethod factory) {
        Class<?> paramClass;
        Class<?> paramClass2 = factory.getRawParameterType(0);
        if (paramClass2 == String.class) {
            paramClass = null;
        } else if (paramClass2 == Integer.TYPE || paramClass2 == Integer.class) {
            paramClass = Integer.class;
        } else if (paramClass2 == Long.TYPE || paramClass2 == Long.class) {
            paramClass = Long.class;
        } else {
            throw new IllegalArgumentException("Parameter #0 type for factory method (" + factory + ") not suitable, must be java.lang.String or int/Integer/long/Long");
        }
        if (config.canOverrideAccessModifiers()) {
            ClassUtil.checkAndFixAccess(factory.getMember());
        }
        return new FactoryBasedDeserializer(enumClass, factory, paramClass);
    }

    public boolean isCachable() {
        return true;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Enum<?>] */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.lang.Enum<?>] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Enum<?> deserialize(com.fasterxml.jackson.core.JsonParser r10, com.fasterxml.jackson.databind.DeserializationContext r11) throws java.io.IOException, com.fasterxml.jackson.core.JsonProcessingException {
        /*
            r9 = this;
            com.fasterxml.jackson.core.JsonToken r0 = r10.getCurrentToken()
            com.fasterxml.jackson.core.JsonToken r5 = com.fasterxml.jackson.core.JsonToken.VALUE_STRING
            if (r0 == r5) goto L_0x000c
            com.fasterxml.jackson.core.JsonToken r5 = com.fasterxml.jackson.core.JsonToken.FIELD_NAME
            if (r0 != r5) goto L_0x002f
        L_0x000c:
            java.lang.String r2 = r10.getText()
            com.fasterxml.jackson.databind.util.EnumResolver<?> r5 = r9._resolver
            java.lang.Enum r3 = r5.findEnum(r2)
            if (r3 != 0) goto L_0x002d
            com.fasterxml.jackson.databind.DeserializationFeature r5 = com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL
            boolean r5 = r11.isEnabled(r5)
            if (r5 != 0) goto L_0x002d
            com.fasterxml.jackson.databind.util.EnumResolver<?> r5 = r9._resolver
            java.lang.Class r5 = r5.getEnumClass()
            java.lang.String r6 = "value not one of declared Enum instance names"
            com.fasterxml.jackson.databind.JsonMappingException r5 = r11.weirdStringException(r2, r5, r6)
            throw r5
        L_0x002d:
            r4 = r3
        L_0x002e:
            return r4
        L_0x002f:
            com.fasterxml.jackson.core.JsonToken r5 = com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_INT
            if (r0 != r5) goto L_0x0086
            com.fasterxml.jackson.databind.DeserializationFeature r5 = com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS
            boolean r5 = r11.isEnabled(r5)
            if (r5 == 0) goto L_0x0042
            java.lang.String r5 = "Not allowed to deserialize Enum value out of JSON number (disable DeserializationConfig.DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS to allow)"
            com.fasterxml.jackson.databind.JsonMappingException r5 = r11.mappingException(r5)
            throw r5
        L_0x0042:
            int r1 = r10.getIntValue()
            com.fasterxml.jackson.databind.util.EnumResolver<?> r5 = r9._resolver
            java.lang.Enum r3 = r5.getEnum(r1)
            if (r3 != 0) goto L_0x0084
            com.fasterxml.jackson.databind.DeserializationFeature r5 = com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL
            boolean r5 = r11.isEnabled(r5)
            if (r5 != 0) goto L_0x0084
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)
            com.fasterxml.jackson.databind.util.EnumResolver<?> r6 = r9._resolver
            java.lang.Class r6 = r6.getEnumClass()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "index value outside legal index range [0.."
            java.lang.StringBuilder r7 = r7.append(r8)
            com.fasterxml.jackson.databind.util.EnumResolver<?> r8 = r9._resolver
            int r8 = r8.lastValidIndex()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "]"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            com.fasterxml.jackson.databind.JsonMappingException r5 = r11.weirdNumberException(r5, r6, r7)
            throw r5
        L_0x0084:
            r4 = r3
            goto L_0x002e
        L_0x0086:
            com.fasterxml.jackson.databind.util.EnumResolver<?> r5 = r9._resolver
            java.lang.Class r5 = r5.getEnumClass()
            com.fasterxml.jackson.databind.JsonMappingException r5 = r11.mappingException(r5)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.std.EnumDeserializer.deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext):java.lang.Enum");
    }

    protected static class FactoryBasedDeserializer extends StdScalarDeserializer<Object> {
        private static final long serialVersionUID = -7775129435872564122L;
        protected final Class<?> _enumClass;
        protected final Method _factory;
        protected final Class<?> _inputType;

        public FactoryBasedDeserializer(Class<?> cls, AnnotatedMethod f, Class<?> inputType) {
            super(Enum.class);
            this._enumClass = cls;
            this._factory = f.getAnnotated();
            this._inputType = inputType;
        }

        public Object deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            Object valueOf;
            if (this._inputType == null) {
                valueOf = jp.getText();
            } else if (this._inputType == Integer.class) {
                valueOf = Integer.valueOf(jp.getValueAsInt());
            } else if (this._inputType == Long.class) {
                valueOf = Long.valueOf(jp.getValueAsLong());
            } else {
                throw ctxt.mappingException(this._enumClass);
            }
            try {
                return this._factory.invoke(this._enumClass, valueOf);
            } catch (Exception e) {
                ClassUtil.unwrapAndThrowAsIAE(e);
                return null;
            }
        }
    }
}
