package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.deser.CreatorProperty;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.fasterxml.jackson.databind.deser.std.StdValueInstantiator;
import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedWithParams;
import com.fasterxml.jackson.databind.util.ClassUtil;
import java.lang.reflect.Member;
import java.util.HashMap;

public class CreatorCollector {
    protected final BeanDescription _beanDesc;
    protected AnnotatedWithParams _booleanCreator;
    protected final boolean _canFixAccess;
    protected AnnotatedWithParams _defaultConstructor;
    protected CreatorProperty[] _delegateArgs;
    protected AnnotatedWithParams _delegateCreator;
    protected AnnotatedWithParams _doubleCreator;
    protected AnnotatedWithParams _intCreator;
    protected AnnotatedWithParams _longCreator;
    protected CreatorProperty[] _propertyBasedArgs = null;
    protected AnnotatedWithParams _propertyBasedCreator;
    protected AnnotatedWithParams _stringCreator;

    public CreatorCollector(BeanDescription beanDesc, boolean canFixAccess) {
        this._beanDesc = beanDesc;
        this._canFixAccess = canFixAccess;
    }

    public ValueInstantiator constructValueInstantiator(DeserializationConfig config) {
        JavaType delegateType;
        StdValueInstantiator inst = new StdValueInstantiator(config, this._beanDesc.getType());
        if (this._delegateCreator == null) {
            delegateType = null;
        } else {
            int ix = 0;
            if (this._delegateArgs != null) {
                int i = 0;
                int len = this._delegateArgs.length;
                while (true) {
                    if (i >= len) {
                        break;
                    } else if (this._delegateArgs[i] == null) {
                        ix = i;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            delegateType = this._beanDesc.bindingsForBeanType().resolveType(this._delegateCreator.getGenericParameterType(ix));
        }
        inst.configureFromObjectSettings(this._defaultConstructor, this._delegateCreator, delegateType, this._delegateArgs, this._propertyBasedCreator, this._propertyBasedArgs);
        inst.configureFromStringCreator(this._stringCreator);
        inst.configureFromIntCreator(this._intCreator);
        inst.configureFromLongCreator(this._longCreator);
        inst.configureFromDoubleCreator(this._doubleCreator);
        inst.configureFromBooleanCreator(this._booleanCreator);
        return inst;
    }

    @Deprecated
    public void setDefaultConstructor(AnnotatedConstructor ctor) {
        this._defaultConstructor = (AnnotatedWithParams) _fixAccess(ctor);
    }

    public void setDefaultCreator(AnnotatedWithParams creator) {
        if (creator instanceof AnnotatedConstructor) {
            setDefaultConstructor((AnnotatedConstructor) creator);
        } else {
            this._defaultConstructor = (AnnotatedWithParams) _fixAccess(creator);
        }
    }

    public void addStringCreator(AnnotatedWithParams creator) {
        this._stringCreator = verifyNonDup(creator, this._stringCreator, "String");
    }

    public void addIntCreator(AnnotatedWithParams creator) {
        this._intCreator = verifyNonDup(creator, this._intCreator, "int");
    }

    public void addLongCreator(AnnotatedWithParams creator) {
        this._longCreator = verifyNonDup(creator, this._longCreator, "long");
    }

    public void addDoubleCreator(AnnotatedWithParams creator) {
        this._doubleCreator = verifyNonDup(creator, this._doubleCreator, "double");
    }

    public void addBooleanCreator(AnnotatedWithParams creator) {
        this._booleanCreator = verifyNonDup(creator, this._booleanCreator, "boolean");
    }

    public void addDelegatingCreator(AnnotatedWithParams creator, CreatorProperty[] injectables) {
        this._delegateCreator = verifyNonDup(creator, this._delegateCreator, "delegate");
        this._delegateArgs = injectables;
    }

    public void addPropertyCreator(AnnotatedWithParams creator, CreatorProperty[] properties) {
        Integer old;
        this._propertyBasedCreator = verifyNonDup(creator, this._propertyBasedCreator, "property-based");
        if (properties.length > 1) {
            HashMap<String, Integer> names = new HashMap<>();
            int len = properties.length;
            for (int i = 0; i < len; i++) {
                String name = properties[i].getName();
                if ((name.length() != 0 || properties[i].getInjectableValueId() == null) && (old = (Integer) names.put(name, Integer.valueOf(i))) != null) {
                    throw new IllegalArgumentException("Duplicate creator property \"" + name + "\" (index " + old + " vs " + i + ")");
                }
            }
        }
        this._propertyBasedArgs = properties;
    }

    public boolean hasDefaultCreator() {
        return this._defaultConstructor != null;
    }

    private <T extends AnnotatedMember> T _fixAccess(T member) {
        if (member != null && this._canFixAccess) {
            ClassUtil.checkAndFixAccess((Member) member.getAnnotated());
        }
        return member;
    }

    /* access modifiers changed from: protected */
    public AnnotatedWithParams verifyNonDup(AnnotatedWithParams newOne, AnnotatedWithParams oldOne, String type) {
        if (oldOne == null || oldOne.getClass() != newOne.getClass()) {
            return (AnnotatedWithParams) _fixAccess(newOne);
        }
        throw new IllegalArgumentException("Conflicting " + type + " creators: already had " + oldOne + ", encountered " + newOne);
    }
}
