package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.type.ClassKey;
import java.io.Serializable;

public class RootNameLookup implements Serializable {
    private static final long serialVersionUID = 1;
    protected LRUMap<ClassKey, SerializedString> _rootNames;

    public SerializedString findRootName(JavaType rootType, MapperConfig<?> config) {
        return findRootName(rootType.getRawClass(), config);
    }

    public synchronized SerializedString findRootName(Class<?> rootType, MapperConfig<?> config) {
        SerializedString name;
        String nameStr;
        ClassKey key = new ClassKey(rootType);
        if (this._rootNames == null) {
            this._rootNames = new LRUMap<>(20, 200);
        } else {
            name = this._rootNames.get(key);
            if (name != null) {
            }
        }
        PropertyName pname = config.getAnnotationIntrospector().findRootName(config.introspectClassAnnotations(rootType).getClassInfo());
        if (pname == null || !pname.hasSimpleName()) {
            nameStr = rootType.getSimpleName();
        } else {
            nameStr = pname.getSimpleName();
        }
        name = new SerializedString(nameStr);
        this._rootNames.put(key, name);
        return name;
    }
}
