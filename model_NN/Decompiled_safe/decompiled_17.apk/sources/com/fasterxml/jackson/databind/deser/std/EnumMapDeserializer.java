package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import java.io.IOException;
import java.util.EnumMap;

public class EnumMapDeserializer extends StdDeserializer<EnumMap<?, ?>> implements ContextualDeserializer {
    protected final Class<?> _enumClass;
    protected JsonDeserializer<Enum<?>> _keyDeserializer;
    protected final JavaType _mapType;
    protected JsonDeserializer<Object> _valueDeserializer;

    /* JADX WARN: Type inference failed for: r3v0, types: [com.fasterxml.jackson.databind.JsonDeserializer<?>, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.Enum<?>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public EnumMapDeserializer(com.fasterxml.jackson.databind.JavaType r2, com.fasterxml.jackson.databind.JsonDeserializer<?> r3, com.fasterxml.jackson.databind.JsonDeserializer<?> r4) {
        /*
            r1 = this;
            java.lang.Class<java.util.EnumMap> r0 = java.util.EnumMap.class
            r1.<init>(r0)
            r1._mapType = r2
            com.fasterxml.jackson.databind.JavaType r0 = r2.getKeyType()
            java.lang.Class r0 = r0.getRawClass()
            r1._enumClass = r0
            r1._keyDeserializer = r3
            r1._valueDeserializer = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.std.EnumMapDeserializer.<init>(com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JsonDeserializer, com.fasterxml.jackson.databind.JsonDeserializer):void");
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public EnumMapDeserializer withResolved(JsonDeserializer<?> keyDeserializer, JsonDeserializer<?> valueDeserializer) {
        return (keyDeserializer == this._keyDeserializer && valueDeserializer == this._valueDeserializer) ? this : new EnumMapDeserializer(this._mapType, keyDeserializer, valueDeserializer);
    }

    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        JsonDeserializer<?> kd = this._keyDeserializer;
        if (kd == null) {
            kd = ctxt.findContextualValueDeserializer(this._mapType.getKeyType(), property);
        }
        JsonDeserializer<?> vd = this._valueDeserializer;
        if (vd == null) {
            vd = ctxt.findContextualValueDeserializer(this._mapType.getContentType(), property);
        } else if (vd instanceof ContextualDeserializer) {
            vd = ((ContextualDeserializer) vd).createContextual(ctxt, property);
        }
        return withResolved(kd, vd);
    }

    public boolean isCachable() {
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [java.lang.Enum<?>, java.lang.Object]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    public EnumMap<?, ?> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
            throw ctxt.mappingException(EnumMap.class);
        }
        EnumMap result = constructMap();
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            Enum<?> key = this._keyDeserializer.deserialize(jp, ctxt);
            if (key != null) {
                result.put((Enum) key, jp.nextToken() == JsonToken.VALUE_NULL ? null : this._valueDeserializer.deserialize(jp, ctxt));
            } else if (!ctxt.isEnabled(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                String value = null;
                try {
                    if (jp.hasCurrentToken()) {
                        value = jp.getText();
                    }
                } catch (Exception e) {
                }
                throw ctxt.weirdStringException(value, this._enumClass, "value not one of declared Enum instance names");
            } else {
                jp.nextToken();
                jp.skipChildren();
            }
        }
        return result;
    }

    public Object deserializeWithType(JsonParser jp, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException, JsonProcessingException {
        return typeDeserializer.deserializeTypedFromObject(jp, ctxt);
    }

    private EnumMap<?, ?> constructMap() {
        return new EnumMap<>(this._enumClass);
    }
}
