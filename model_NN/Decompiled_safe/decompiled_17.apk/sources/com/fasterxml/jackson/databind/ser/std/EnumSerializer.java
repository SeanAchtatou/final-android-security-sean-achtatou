package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonStringFormatVisitor;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.util.EnumValues;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

@JacksonStdImpl
public class EnumSerializer extends StdScalarSerializer<Enum<?>> implements ContextualSerializer {
    protected final Boolean _serializeAsIndex;
    protected final EnumValues _values;

    public /* bridge */ /* synthetic */ void serialize(Object x0, JsonGenerator x1, SerializerProvider x2) throws IOException, JsonGenerationException {
        serialize((Enum<?>) ((Enum) x0), x1, x2);
    }

    @Deprecated
    public EnumSerializer(EnumValues v) {
        this(v, null);
    }

    public EnumSerializer(EnumValues v, Boolean serializeAsIndex) {
        super(Enum.class, false);
        this._values = v;
        this._serializeAsIndex = serializeAsIndex;
    }

    public static EnumSerializer construct(Class<Enum<?>> enumClass, SerializationConfig config, BeanDescription beanDesc, JsonFormat.Value format) {
        AnnotationIntrospector intr = config.getAnnotationIntrospector();
        return new EnumSerializer(config.isEnabled(SerializationFeature.WRITE_ENUMS_USING_TO_STRING) ? EnumValues.constructFromToString(enumClass, intr) : EnumValues.constructFromName(enumClass, intr), _isShapeWrittenUsingIndex(enumClass, format, true));
    }

    @Deprecated
    public static EnumSerializer construct(Class<Enum<?>> enumClass, SerializationConfig config, BeanDescription beanDesc) {
        return construct(enumClass, config, beanDesc, beanDesc.findExpectedFormat(null));
    }

    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        JsonFormat.Value format;
        Boolean serializeAsIndex;
        if (property == null || (format = prov.getAnnotationIntrospector().findFormat((Annotated) property.getMember())) == null || (serializeAsIndex = _isShapeWrittenUsingIndex(property.getType().getRawClass(), format, false)) == this._serializeAsIndex) {
            return this;
        }
        return new EnumSerializer(this._values, serializeAsIndex);
    }

    public EnumValues getEnumValues() {
        return this._values;
    }

    public final void serialize(Enum<?> en, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        if (_serializeAsIndex(provider)) {
            jgen.writeNumber(en.ordinal());
        } else {
            jgen.writeString(this._values.serializedValueFor(en));
        }
    }

    public JsonNode getSchema(SerializerProvider provider, Type typeHint) {
        if (_serializeAsIndex(provider)) {
            return createSchemaNode("integer", true);
        }
        ObjectNode objectNode = createSchemaNode("string", true);
        if (typeHint == null || !provider.constructType(typeHint).isEnumType()) {
            return objectNode;
        }
        ArrayNode enumNode = objectNode.putArray("enum");
        for (SerializedString value : this._values.values()) {
            enumNode.add(value.getValue());
        }
        return objectNode;
    }

    public void acceptJsonFormatVisitor(JsonFormatVisitorWrapper visitor, JavaType typeHint) {
        if (visitor.getProvider().isEnabled(SerializationFeature.WRITE_ENUMS_USING_INDEX)) {
            visitor.expectIntegerFormat(typeHint);
            return;
        }
        JsonStringFormatVisitor stringVisitor = visitor.expectStringFormat(typeHint);
        if (typeHint != null && typeHint.isEnumType()) {
            Set<String> enums = new HashSet<>();
            for (SerializedString value : this._values.values()) {
                enums.add(value.getValue());
            }
            stringVisitor.enumTypes(enums);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean _serializeAsIndex(SerializerProvider provider) {
        if (this._serializeAsIndex != null) {
            return this._serializeAsIndex.booleanValue();
        }
        return provider.isEnabled(SerializationFeature.WRITE_ENUMS_USING_INDEX);
    }

    protected static Boolean _isShapeWrittenUsingIndex(Class<?> enumClass, JsonFormat.Value format, boolean fromClass) {
        JsonFormat.Shape shape = format == null ? null : format.getShape();
        if (shape == null || shape == JsonFormat.Shape.ANY || shape == JsonFormat.Shape.SCALAR) {
            return null;
        }
        if (shape == JsonFormat.Shape.STRING) {
            return Boolean.FALSE;
        }
        if (shape.isNumeric()) {
            return Boolean.TRUE;
        }
        throw new IllegalArgumentException("Unsupported serialization shape (" + shape + ") for Enum " + enumClass.getName() + ", not supported as " + (fromClass ? "class" : "property") + " annotation");
    }
}
