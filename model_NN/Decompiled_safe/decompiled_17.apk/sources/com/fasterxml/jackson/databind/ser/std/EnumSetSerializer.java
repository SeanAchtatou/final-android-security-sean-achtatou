package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Iterator;

public class EnumSetSerializer extends AsArraySerializerBase<EnumSet<? extends Enum<?>>> {
    public /* bridge */ /* synthetic */ boolean hasSingleElement(Object x0) {
        return hasSingleElement((EnumSet<? extends Enum<?>>) ((EnumSet) x0));
    }

    public /* bridge */ /* synthetic */ boolean isEmpty(Object x0) {
        return isEmpty((EnumSet<? extends Enum<?>>) ((EnumSet) x0));
    }

    public /* bridge */ /* synthetic */ void serializeContents(Object x0, JsonGenerator x1, SerializerProvider x2) throws IOException, JsonGenerationException {
        serializeContents((EnumSet<? extends Enum<?>>) ((EnumSet) x0), x1, x2);
    }

    public EnumSetSerializer(JavaType elemType, BeanProperty property) {
        super(EnumSet.class, elemType, true, null, property, null);
    }

    public EnumSetSerializer(EnumSetSerializer src, BeanProperty property, TypeSerializer vts, JsonSerializer<?> valueSerializer) {
        super(src, property, vts, valueSerializer);
    }

    public EnumSetSerializer _withValueTypeSerializer(TypeSerializer vts) {
        return this;
    }

    public EnumSetSerializer withResolved(BeanProperty property, TypeSerializer vts, JsonSerializer<?> elementSerializer) {
        return new EnumSetSerializer(this, property, vts, elementSerializer);
    }

    public boolean isEmpty(EnumSet<? extends Enum<?>> value) {
        return value == null || value.isEmpty();
    }

    public boolean hasSingleElement(EnumSet<? extends Enum<?>> value) {
        return value.size() == 1;
    }

    public void serializeContents(EnumSet<? extends Enum<?>> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        JsonSerializer<Object> enumSer = this._elementSerializer;
        Iterator i$ = value.iterator();
        while (i$.hasNext()) {
            Enum<?> en = (Enum) i$.next();
            if (enumSer == null) {
                enumSer = provider.findValueSerializer(en.getDeclaringClass(), this._property);
            }
            enumSer.serialize(en, jgen, provider);
        }
    }
}
