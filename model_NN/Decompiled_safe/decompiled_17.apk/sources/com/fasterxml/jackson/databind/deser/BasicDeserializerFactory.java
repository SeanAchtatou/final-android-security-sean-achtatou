package com.fasterxml.jackson.databind.deser;

import com.fasterxml.jackson.databind.AbstractTypeResolver;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.annotation.NoClass;
import com.fasterxml.jackson.databind.cfg.DeserializerFactoryConfig;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import com.fasterxml.jackson.databind.deser.impl.CreatorCollector;
import com.fasterxml.jackson.databind.deser.std.CollectionDeserializer;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.fasterxml.jackson.databind.deser.std.EnumDeserializer;
import com.fasterxml.jackson.databind.deser.std.EnumMapDeserializer;
import com.fasterxml.jackson.databind.deser.std.EnumSetDeserializer;
import com.fasterxml.jackson.databind.deser.std.JacksonDeserializers;
import com.fasterxml.jackson.databind.deser.std.JdkDeserializers;
import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.fasterxml.jackson.databind.deser.std.MapDeserializer;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;
import com.fasterxml.jackson.databind.deser.std.ObjectArrayDeserializer;
import com.fasterxml.jackson.databind.deser.std.PrimitiveArrayDeserializers;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdKeyDeserializers;
import com.fasterxml.jackson.databind.deser.std.StringCollectionDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.deser.std.UntypedObjectDeserializer;
import com.fasterxml.jackson.databind.ext.OptionalHandlerFactory;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.fasterxml.jackson.databind.type.ClassKey;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.util.ClassUtil;
import com.fasterxml.jackson.databind.util.EnumResolver;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public abstract class BasicDeserializerFactory extends DeserializerFactory implements Serializable {
    protected static final HashMap<JavaType, JsonDeserializer<Object>> _arrayDeserializers = PrimitiveArrayDeserializers.getAll();
    static final HashMap<String, Class<? extends Collection>> _collectionFallbacks = new HashMap<>();
    protected static final HashMap<JavaType, KeyDeserializer> _keyDeserializers = StdKeyDeserializers.constructAll();
    static final HashMap<String, Class<? extends Map>> _mapFallbacks = new HashMap<>();
    protected static final HashMap<ClassKey, JsonDeserializer<Object>> _simpleDeserializers = new HashMap<>();
    private static final long serialVersionUID = 1;
    protected final DeserializerFactoryConfig _factoryConfig;
    protected OptionalHandlerFactory optionalHandlers = OptionalHandlerFactory.instance;

    /* access modifiers changed from: protected */
    public abstract DeserializerFactory withConfig(DeserializerFactoryConfig deserializerFactoryConfig);

    static {
        _add(_simpleDeserializers, Object.class, new UntypedObjectDeserializer());
        StdDeserializer<?> strDeser = new StringDeserializer();
        _add(_simpleDeserializers, String.class, strDeser);
        _add(_simpleDeserializers, CharSequence.class, strDeser);
        _add(_simpleDeserializers, NumberDeserializers.all());
        _add(_simpleDeserializers, DateDeserializers.all());
        _add(_simpleDeserializers, JdkDeserializers.all());
        _add(_simpleDeserializers, JacksonDeserializers.all());
        _mapFallbacks.put(Map.class.getName(), LinkedHashMap.class);
        _mapFallbacks.put(ConcurrentMap.class.getName(), ConcurrentHashMap.class);
        _mapFallbacks.put(SortedMap.class.getName(), TreeMap.class);
        _mapFallbacks.put("java.util.NavigableMap", TreeMap.class);
        try {
            Class<?> key = Class.forName("java.util.concurrent.ConcurrentNavigableMap");
            _mapFallbacks.put(key.getName(), Class.forName("java.util.concurrent.ConcurrentSkipListMap"));
        } catch (ClassNotFoundException | SecurityException e) {
        }
        _collectionFallbacks.put(Collection.class.getName(), ArrayList.class);
        _collectionFallbacks.put(List.class.getName(), ArrayList.class);
        _collectionFallbacks.put(Set.class.getName(), HashSet.class);
        _collectionFallbacks.put(SortedSet.class.getName(), TreeSet.class);
        _collectionFallbacks.put(Queue.class.getName(), LinkedList.class);
        _collectionFallbacks.put("java.util.Deque", LinkedList.class);
        _collectionFallbacks.put("java.util.NavigableSet", TreeSet.class);
    }

    private static void _add(Map<ClassKey, JsonDeserializer<Object>> desers, StdDeserializer<?>[] serializers) {
        for (StdDeserializer<?> ser : serializers) {
            _add(desers, ser.getValueClass(), ser);
        }
    }

    private static void _add(Map<ClassKey, JsonDeserializer<Object>> desers, Class<?> valueClass, StdDeserializer<?> stdDeser) {
        desers.put(new ClassKey(valueClass), stdDeser);
    }

    protected BasicDeserializerFactory(DeserializerFactoryConfig config) {
        this._factoryConfig = config;
    }

    public DeserializerFactoryConfig getFactoryConfig() {
        return this._factoryConfig;
    }

    public final DeserializerFactory withAdditionalDeserializers(Deserializers additional) {
        return withConfig(this._factoryConfig.withAdditionalDeserializers(additional));
    }

    public final DeserializerFactory withAdditionalKeyDeserializers(KeyDeserializers additional) {
        return withConfig(this._factoryConfig.withAdditionalKeyDeserializers(additional));
    }

    public final DeserializerFactory withDeserializerModifier(BeanDeserializerModifier modifier) {
        return withConfig(this._factoryConfig.withDeserializerModifier(modifier));
    }

    public final DeserializerFactory withAbstractTypeResolver(AbstractTypeResolver resolver) {
        return withConfig(this._factoryConfig.withAbstractTypeResolver(resolver));
    }

    public final DeserializerFactory withValueInstantiators(ValueInstantiators instantiators) {
        return withConfig(this._factoryConfig.withValueInstantiators(instantiators));
    }

    public JavaType mapAbstractType(DeserializationConfig config, JavaType type) throws JsonMappingException {
        JavaType next;
        while (true) {
            next = _mapAbstractType2(config, type);
            if (next == null) {
                return type;
            }
            Class<?> prevCls = type.getRawClass();
            Class<?> nextCls = next.getRawClass();
            if (prevCls != nextCls && prevCls.isAssignableFrom(nextCls)) {
                type = next;
            }
        }
        throw new IllegalArgumentException("Invalid abstract type resolution from " + type + " to " + next + ": latter is not a subtype of former");
    }

    private JavaType _mapAbstractType2(DeserializationConfig config, JavaType type) throws JsonMappingException {
        Class<?> currClass = type.getRawClass();
        if (this._factoryConfig.hasAbstractTypeResolvers()) {
            for (AbstractTypeResolver resolver : this._factoryConfig.abstractTypeResolvers()) {
                JavaType concrete = resolver.findTypeMapping(config, type);
                if (concrete != null && concrete.getRawClass() != currClass) {
                    return concrete;
                }
            }
        }
        return null;
    }

    public ValueInstantiator findValueInstantiator(DeserializationContext ctxt, BeanDescription beanDesc) throws JsonMappingException {
        DeserializationConfig config = ctxt.getConfig();
        ValueInstantiator instantiator = null;
        AnnotatedClass ac = beanDesc.getClassInfo();
        Object instDef = ctxt.getAnnotationIntrospector().findValueInstantiator(ac);
        if (instDef != null) {
            instantiator = _valueInstantiatorInstance(config, ac, instDef);
        }
        if (instantiator == null && (instantiator = _findStdValueInstantiator(config, beanDesc)) == null) {
            instantiator = _constructDefaultValueInstantiator(ctxt, beanDesc);
        }
        if (this._factoryConfig.hasValueInstantiators()) {
            for (ValueInstantiators insts : this._factoryConfig.valueInstantiators()) {
                instantiator = insts.findValueInstantiator(config, beanDesc, instantiator);
                if (instantiator == null) {
                    throw new JsonMappingException("Broken registered ValueInstantiators (of type " + insts.getClass().getName() + "): returned null ValueInstantiator");
                }
            }
        }
        return instantiator;
    }

    private ValueInstantiator _findStdValueInstantiator(DeserializationConfig config, BeanDescription beanDesc) throws JsonMappingException {
        return JacksonDeserializers.findValueInstantiator(config, beanDesc);
    }

    /* access modifiers changed from: protected */
    public ValueInstantiator _constructDefaultValueInstantiator(DeserializationContext ctxt, BeanDescription beanDesc) throws JsonMappingException {
        CreatorCollector creators = new CreatorCollector(beanDesc, ctxt.canOverrideAccessModifiers());
        AnnotationIntrospector intr = ctxt.getAnnotationIntrospector();
        DeserializationConfig config = ctxt.getConfig();
        VisibilityChecker<?> vchecker = intr.findAutoDetectVisibility(beanDesc.getClassInfo(), config.getDefaultVisibilityChecker());
        _addDeserializerFactoryMethods(ctxt, beanDesc, vchecker, intr, creators);
        if (beanDesc.getType().isConcrete()) {
            _addDeserializerConstructors(ctxt, beanDesc, vchecker, intr, creators);
        }
        return creators.constructValueInstantiator(config);
    }

    public ValueInstantiator _valueInstantiatorInstance(DeserializationConfig config, Annotated annotated, Object instDef) throws JsonMappingException {
        if (instDef == null) {
            return null;
        }
        if (instDef instanceof ValueInstantiator) {
            return (ValueInstantiator) instDef;
        }
        if (!(instDef instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned key deserializer definition of type " + instDef.getClass().getName() + "; expected type KeyDeserializer or Class<KeyDeserializer> instead");
        }
        Class<?> instClass = (Class) instDef;
        if (instClass == NoClass.class) {
            return null;
        }
        if (!ValueInstantiator.class.isAssignableFrom(instClass)) {
            throw new IllegalStateException("AnnotationIntrospector returned Class " + instClass.getName() + "; expected Class<ValueInstantiator>");
        }
        HandlerInstantiator hi = config.getHandlerInstantiator();
        if (hi != null) {
            return hi.valueInstantiatorInstance(config, annotated, instClass);
        }
        return (ValueInstantiator) ClassUtil.createInstance(instClass, config.canOverrideAccessModifiers());
    }

    /* access modifiers changed from: protected */
    public void _addDeserializerConstructors(DeserializationContext ctxt, BeanDescription beanDesc, VisibilityChecker<?> vchecker, AnnotationIntrospector intr, CreatorCollector creators) throws JsonMappingException {
        AnnotatedConstructor defaultCtor = beanDesc.findDefaultConstructor();
        if (defaultCtor != null && (!creators.hasDefaultCreator() || intr.hasCreatorAnnotation(defaultCtor))) {
            creators.setDefaultCreator(defaultCtor);
        }
        for (AnnotatedConstructor ctor : beanDesc.getConstructors()) {
            int argCount = ctor.getParameterCount();
            boolean isCreator = intr.hasCreatorAnnotation(ctor);
            boolean isVisible = vchecker.isCreatorVisible(ctor);
            if (argCount == 1) {
                _handleSingleArgumentConstructor(ctxt, beanDesc, vchecker, intr, creators, ctor, isCreator, isVisible);
            } else if (isCreator || isVisible) {
                AnnotatedParameter nonAnnotatedParam = null;
                int namedCount = 0;
                int injectCount = 0;
                CreatorProperty[] properties = new CreatorProperty[argCount];
                for (int i = 0; i < argCount; i++) {
                    AnnotatedParameter param = ctor.getParameter(i);
                    PropertyName pn = param == null ? null : intr.findNameForDeserialization(param);
                    String name = pn == null ? null : pn.getSimpleName();
                    Object injectId = intr.findInjectableValueId(param);
                    if (name != null && name.length() > 0) {
                        namedCount++;
                        properties[i] = constructCreatorProperty(ctxt, beanDesc, name, i, param, injectId);
                    } else if (injectId != null) {
                        injectCount++;
                        properties[i] = constructCreatorProperty(ctxt, beanDesc, name, i, param, injectId);
                    } else if (nonAnnotatedParam == null) {
                        nonAnnotatedParam = param;
                    }
                }
                if (isCreator || namedCount > 0 || injectCount > 0) {
                    if (namedCount + injectCount == argCount) {
                        creators.addPropertyCreator(ctor, properties);
                    } else if (namedCount == 0 && injectCount + 1 == argCount) {
                        creators.addDelegatingCreator(ctor, properties);
                    } else {
                        throw new IllegalArgumentException("Argument #" + nonAnnotatedParam.getIndex() + " of constructor " + ctor + " has no property name annotation; must have name when multiple-paramater constructor annotated as Creator");
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean _handleSingleArgumentConstructor(DeserializationContext ctxt, BeanDescription beanDesc, VisibilityChecker<?> visibilityChecker, AnnotationIntrospector intr, CreatorCollector creators, AnnotatedConstructor ctor, boolean isCreator, boolean isVisible) throws JsonMappingException {
        AnnotatedParameter param = ctor.getParameter(0);
        PropertyName pn = param == null ? null : intr.findNameForDeserialization(param);
        String name = pn == null ? null : pn.getSimpleName();
        Object injectId = intr.findInjectableValueId(param);
        if (injectId != null || (name != null && name.length() > 0)) {
            creators.addPropertyCreator(ctor, new CreatorProperty[]{constructCreatorProperty(ctxt, beanDesc, name, 0, param, injectId)});
            return true;
        }
        Class<?> type = ctor.getRawParameterType(0);
        if (type == String.class) {
            if (isCreator || isVisible) {
                creators.addStringCreator(ctor);
            }
            return true;
        } else if (type == Integer.TYPE || type == Integer.class) {
            if (isCreator || isVisible) {
                creators.addIntCreator(ctor);
            }
            return true;
        } else if (type == Long.TYPE || type == Long.class) {
            if (isCreator || isVisible) {
                creators.addLongCreator(ctor);
            }
            return true;
        } else if (type == Double.TYPE || type == Double.class) {
            if (isCreator || isVisible) {
                creators.addDoubleCreator(ctor);
            }
            return true;
        } else if (!isCreator) {
            return false;
        } else {
            creators.addDelegatingCreator(ctor, null);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void _addDeserializerFactoryMethods(DeserializationContext ctxt, BeanDescription beanDesc, VisibilityChecker<?> vchecker, AnnotationIntrospector intr, CreatorCollector creators) throws JsonMappingException {
        DeserializationConfig config = ctxt.getConfig();
        for (AnnotatedMethod factory : beanDesc.getFactoryMethods()) {
            boolean isCreator = intr.hasCreatorAnnotation(factory);
            int argCount = factory.getParameterCount();
            if (argCount != 0) {
                if (argCount == 1) {
                    AnnotatedParameter param = factory.getParameter(0);
                    PropertyName pn = param == null ? null : intr.findNameForDeserialization(param);
                    String name = pn == null ? null : pn.getSimpleName();
                    if (intr.findInjectableValueId(param) == null && (name == null || name.length() == 0)) {
                        _handleSingleArgumentFactory(config, beanDesc, vchecker, intr, creators, factory, isCreator);
                    }
                } else if (!intr.hasCreatorAnnotation(factory)) {
                    continue;
                }
                AnnotatedParameter nonAnnotatedParam = null;
                CreatorProperty[] properties = new CreatorProperty[argCount];
                int namedCount = 0;
                int injectCount = 0;
                for (int i = 0; i < argCount; i++) {
                    AnnotatedParameter param2 = factory.getParameter(i);
                    PropertyName pn2 = param2 == null ? null : intr.findNameForDeserialization(param2);
                    String name2 = pn2 == null ? null : pn2.getSimpleName();
                    Object injectId = intr.findInjectableValueId(param2);
                    if (name2 != null && name2.length() > 0) {
                        namedCount++;
                        properties[i] = constructCreatorProperty(ctxt, beanDesc, name2, i, param2, injectId);
                    } else if (injectId != null) {
                        injectCount++;
                        properties[i] = constructCreatorProperty(ctxt, beanDesc, name2, i, param2, injectId);
                    } else if (nonAnnotatedParam == null) {
                        nonAnnotatedParam = param2;
                    }
                }
                if (isCreator || namedCount > 0 || injectCount > 0) {
                    if (namedCount + injectCount == argCount) {
                        creators.addPropertyCreator(factory, properties);
                    } else if (namedCount == 0 && injectCount + 1 == argCount) {
                        creators.addDelegatingCreator(factory, properties);
                    } else {
                        throw new IllegalArgumentException("Argument #" + nonAnnotatedParam.getIndex() + " of factory method " + factory + " has no property name annotation; must have name when multiple-paramater constructor annotated as Creator");
                    }
                }
            } else if (isCreator) {
                creators.setDefaultCreator(factory);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean _handleSingleArgumentFactory(DeserializationConfig config, BeanDescription beanDesc, VisibilityChecker<?> vchecker, AnnotationIntrospector intr, CreatorCollector creators, AnnotatedMethod factory, boolean isCreator) throws JsonMappingException {
        Class<?> type = factory.getRawParameterType(0);
        if (type == String.class) {
            if (!isCreator && !vchecker.isCreatorVisible(factory)) {
                return true;
            }
            creators.addStringCreator(factory);
            return true;
        } else if (type == Integer.TYPE || type == Integer.class) {
            if (!isCreator && !vchecker.isCreatorVisible(factory)) {
                return true;
            }
            creators.addIntCreator(factory);
            return true;
        } else if (type == Long.TYPE || type == Long.class) {
            if (!isCreator && !vchecker.isCreatorVisible(factory)) {
                return true;
            }
            creators.addLongCreator(factory);
            return true;
        } else if (type == Double.TYPE || type == Double.class) {
            if (!isCreator && !vchecker.isCreatorVisible(factory)) {
                return true;
            }
            creators.addDoubleCreator(factory);
            return true;
        } else if (type == Boolean.TYPE || type == Boolean.class) {
            if (!isCreator && !vchecker.isCreatorVisible(factory)) {
                return true;
            }
            creators.addBooleanCreator(factory);
            return true;
        } else if (!intr.hasCreatorAnnotation(factory)) {
            return false;
        } else {
            creators.addDelegatingCreator(factory, null);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public CreatorProperty constructCreatorProperty(DeserializationContext ctxt, BeanDescription beanDesc, String name, int index, AnnotatedParameter param, Object injectableValueId) throws JsonMappingException {
        DeserializationConfig config = ctxt.getConfig();
        JavaType t0 = config.getTypeFactory().constructType(param.getParameterType(), beanDesc.bindingsForBeanType());
        BeanProperty.Std property = new BeanProperty.Std(name, t0, beanDesc.getClassAnnotations(), param);
        JavaType type = resolveType(ctxt, beanDesc, t0, param);
        if (type != t0) {
            BeanProperty.Std property2 = property.withType(type);
        }
        JsonDeserializer<Object> deser = findDeserializerFromAnnotation(ctxt, param);
        JavaType type2 = modifyTypeByAnnotation(ctxt, param, type);
        TypeDeserializer typeDeser = (TypeDeserializer) type2.getTypeHandler();
        if (typeDeser == null) {
            typeDeser = findTypeDeserializer(config, type2);
        }
        CreatorProperty prop = new CreatorProperty(name, type2, typeDeser, beanDesc.getClassAnnotations(), param, index, injectableValueId);
        if (deser != null) {
            return prop.withValueDeserializer((JsonDeserializer<?>) deser);
        }
        return prop;
    }

    public JsonDeserializer<?> createArrayDeserializer(DeserializationContext ctxt, ArrayType type, BeanDescription beanDesc) throws JsonMappingException {
        JavaType elemType = type.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) elemType.getValueHandler();
        if (contentDeser == null) {
            JsonDeserializer<?> deser = _arrayDeserializers.get(elemType);
            if (deser != null) {
                JsonDeserializer<?> custom = _findCustomArrayDeserializer(type, ctxt.getConfig(), beanDesc, null, contentDeser);
                return custom != null ? custom : deser;
            } else if (elemType.isPrimitive()) {
                throw new IllegalArgumentException("Internal error: primitive type (" + type + ") passed, no array deserializer found");
            }
        }
        TypeDeserializer elemTypeDeser = (TypeDeserializer) elemType.getTypeHandler();
        if (elemTypeDeser == null) {
            elemTypeDeser = findTypeDeserializer(ctxt.getConfig(), elemType);
        }
        JsonDeserializer<?> custom2 = _findCustomArrayDeserializer(type, ctxt.getConfig(), beanDesc, elemTypeDeser, contentDeser);
        if (custom2 != null) {
            return custom2;
        }
        return new ObjectArrayDeserializer(type, contentDeser, elemTypeDeser);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomArrayDeserializer(ArrayType type, DeserializationConfig config, BeanDescription beanDesc, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findArrayDeserializer(type, config, beanDesc, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    public JsonDeserializer<?> createCollectionDeserializer(DeserializationContext ctxt, CollectionType type, BeanDescription beanDesc) throws JsonMappingException {
        JavaType contentType = type.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) contentType.getValueHandler();
        TypeDeserializer contentTypeDeser = (TypeDeserializer) contentType.getTypeHandler();
        if (contentTypeDeser == null) {
            contentTypeDeser = findTypeDeserializer(ctxt.getConfig(), contentType);
        }
        JsonDeserializer<?> custom = _findCustomCollectionDeserializer(type, ctxt.getConfig(), beanDesc, contentTypeDeser, contentDeser);
        if (custom != null) {
            return custom;
        }
        Class<?> collectionClass = type.getRawClass();
        if (contentDeser == null && EnumSet.class.isAssignableFrom(collectionClass)) {
            return new EnumSetDeserializer(contentType, null);
        }
        if (type.isInterface() || type.isAbstract()) {
            Class<?> fallback = _collectionFallbacks.get(collectionClass.getName());
            if (fallback == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Collection type " + type);
            }
            type = (CollectionType) ctxt.getConfig().constructSpecializedType(type, fallback);
            beanDesc = ctxt.getConfig().introspectForCreation(type);
        }
        ValueInstantiator inst = findValueInstantiator(ctxt, beanDesc);
        if (contentType.getRawClass() == String.class) {
            return new StringCollectionDeserializer(type, contentDeser, inst);
        }
        return new CollectionDeserializer(type, contentDeser, contentTypeDeser, inst);
    }

    public JsonDeserializer<?> createCollectionLikeDeserializer(DeserializationContext ctxt, CollectionLikeType type, BeanDescription beanDesc) throws JsonMappingException {
        JavaType contentType = type.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) contentType.getValueHandler();
        TypeDeserializer contentTypeDeser = (TypeDeserializer) contentType.getTypeHandler();
        if (contentTypeDeser == null) {
            contentTypeDeser = findTypeDeserializer(ctxt.getConfig(), contentType);
        }
        return _findCustomCollectionLikeDeserializer(type, ctxt.getConfig(), beanDesc, contentTypeDeser, contentDeser);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomCollectionDeserializer(CollectionType type, DeserializationConfig config, BeanDescription beanDesc, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findCollectionDeserializer(type, config, beanDesc, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomCollectionLikeDeserializer(CollectionLikeType type, DeserializationConfig config, BeanDescription beanDesc, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findCollectionLikeDeserializer(type, config, beanDesc, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    public JsonDeserializer<?> createMapDeserializer(DeserializationContext ctxt, MapType type, BeanDescription beanDesc) throws JsonMappingException {
        DeserializationConfig config = ctxt.getConfig();
        JavaType keyType = type.getKeyType();
        JavaType contentType = type.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) contentType.getValueHandler();
        KeyDeserializer keyDes = (KeyDeserializer) keyType.getValueHandler();
        TypeDeserializer contentTypeDeser = (TypeDeserializer) contentType.getTypeHandler();
        if (contentTypeDeser == null) {
            contentTypeDeser = findTypeDeserializer(config, contentType);
        }
        JsonDeserializer<?> custom = _findCustomMapDeserializer(type, config, beanDesc, keyDes, contentTypeDeser, contentDeser);
        if (custom != null) {
            return custom;
        }
        Class<?> mapClass = type.getRawClass();
        if (EnumMap.class.isAssignableFrom(mapClass)) {
            Class<?> kt = keyType.getRawClass();
            if (kt != null && kt.isEnum()) {
                return new EnumMapDeserializer(type, null, contentDeser);
            }
            throw new IllegalArgumentException("Can not construct EnumMap; generic (key) type not available");
        }
        if (type.isInterface() || type.isAbstract()) {
            Class<?> fallback = _mapFallbacks.get(mapClass.getName());
            if (fallback == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Map type " + type);
            }
            type = (MapType) config.constructSpecializedType(type, fallback);
            beanDesc = config.introspectForCreation(type);
        }
        MapDeserializer md = new MapDeserializer(type, findValueInstantiator(ctxt, beanDesc), keyDes, contentDeser, contentTypeDeser);
        md.setIgnorableProperties(config.getAnnotationIntrospector().findPropertiesToIgnore(beanDesc.getClassInfo()));
        return md;
    }

    public JsonDeserializer<?> createMapLikeDeserializer(DeserializationContext ctxt, MapLikeType type, BeanDescription beanDesc) throws JsonMappingException {
        JavaType keyType = type.getKeyType();
        JavaType contentType = type.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) contentType.getValueHandler();
        KeyDeserializer keyDes = (KeyDeserializer) keyType.getValueHandler();
        TypeDeserializer contentTypeDeser = (TypeDeserializer) contentType.getTypeHandler();
        if (contentTypeDeser == null) {
            contentTypeDeser = findTypeDeserializer(ctxt.getConfig(), contentType);
        }
        return _findCustomMapLikeDeserializer(type, ctxt.getConfig(), beanDesc, keyDes, contentTypeDeser, contentDeser);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomMapDeserializer(MapType type, DeserializationConfig config, BeanDescription beanDesc, KeyDeserializer keyDeserializer, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findMapDeserializer(type, config, beanDesc, keyDeserializer, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomMapLikeDeserializer(MapLikeType type, DeserializationConfig config, BeanDescription beanDesc, KeyDeserializer keyDeserializer, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findMapLikeDeserializer(type, config, beanDesc, keyDeserializer, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    public JsonDeserializer<?> createEnumDeserializer(DeserializationContext ctxt, JavaType type, BeanDescription beanDesc) throws JsonMappingException {
        Class<?> enumClass = type.getRawClass();
        JsonDeserializer<?> custom = _findCustomEnumDeserializer(enumClass, ctxt.getConfig(), beanDesc);
        if (custom != null) {
            return custom;
        }
        for (AnnotatedMethod factory : beanDesc.getFactoryMethods()) {
            if (ctxt.getAnnotationIntrospector().hasCreatorAnnotation(factory)) {
                if (factory.getParameterCount() == 1 && factory.getRawReturnType().isAssignableFrom(enumClass)) {
                    return EnumDeserializer.deserializerForCreator(ctxt.getConfig(), enumClass, factory);
                }
                throw new IllegalArgumentException("Unsuitable method (" + factory + ") decorated with @JsonCreator (for Enum type " + enumClass.getName() + ")");
            }
        }
        return new EnumDeserializer(constructEnumResolver(enumClass, ctxt.getConfig(), beanDesc.findJsonValueMethod()));
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomEnumDeserializer(Class<?> type, DeserializationConfig config, BeanDescription beanDesc) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findEnumDeserializer(type, config, beanDesc);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    public JsonDeserializer<?> createTreeDeserializer(DeserializationConfig config, JavaType nodeType, BeanDescription beanDesc) throws JsonMappingException {
        Class<?> rawClass = nodeType.getRawClass();
        JsonDeserializer<?> custom = _findCustomTreeNodeDeserializer(rawClass, config, beanDesc);
        return custom != null ? custom : JsonNodeDeserializer.getDeserializer(rawClass);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomTreeNodeDeserializer(Class<? extends JsonNode> type, DeserializationConfig config, BeanDescription beanDesc) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findTreeNodeDeserializer(type, config, beanDesc);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    public TypeDeserializer findTypeDeserializer(DeserializationConfig config, JavaType baseType) throws JsonMappingException {
        JavaType defaultType;
        AnnotatedClass ac = config.introspectClassAnnotations(baseType.getRawClass()).getClassInfo();
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder findTypeResolver = ai.findTypeResolver(config, ac, baseType);
        Collection<NamedType> subtypes = null;
        if (findTypeResolver == null) {
            findTypeResolver = config.getDefaultTyper(baseType);
            if (findTypeResolver == null) {
                return null;
            }
        } else {
            subtypes = config.getSubtypeResolver().collectAndResolveSubtypes(ac, config, ai);
        }
        if (findTypeResolver.getDefaultImpl() == null && baseType.isAbstract() && (defaultType = mapAbstractType(config, baseType)) != null && defaultType.getRawClass() != baseType.getRawClass()) {
            findTypeResolver = findTypeResolver.defaultImpl(defaultType.getRawClass());
        }
        return findTypeResolver.buildTypeDeserializer(config, baseType, subtypes);
    }

    public KeyDeserializer createKeyDeserializer(DeserializationContext ctxt, JavaType type) throws JsonMappingException {
        DeserializationConfig config = ctxt.getConfig();
        if (this._factoryConfig.hasKeyDeserializers()) {
            BeanDescription beanDesc = config.introspectClassAnnotations(type.getRawClass());
            for (KeyDeserializers d : this._factoryConfig.keyDeserializers()) {
                KeyDeserializer deser = d.findKeyDeserializer(type, config, beanDesc);
                if (deser != null) {
                    return deser;
                }
            }
        }
        Class<?> raw = type.getRawClass();
        if (raw == String.class || raw == Object.class) {
            return StdKeyDeserializers.constructStringKeyDeserializer(config, type);
        }
        KeyDeserializer kdes = _keyDeserializers.get(type);
        if (kdes != null) {
            return kdes;
        }
        if (type.isEnumType()) {
            return _createEnumKeyDeserializer(ctxt, type);
        }
        return StdKeyDeserializers.findStringBasedKeyDeserializer(config, type);
    }

    private KeyDeserializer _createEnumKeyDeserializer(DeserializationContext ctxt, JavaType type) throws JsonMappingException {
        DeserializationConfig config = ctxt.getConfig();
        BeanDescription beanDesc = config.introspect(type);
        JsonDeserializer<?> des = findDeserializerFromAnnotation(ctxt, beanDesc.getClassInfo());
        if (des != null) {
            return StdKeyDeserializers.constructDelegatingKeyDeserializer(config, type, des);
        }
        Class<?> enumClass = type.getRawClass();
        if (_findCustomEnumDeserializer(enumClass, config, beanDesc) != null) {
            return StdKeyDeserializers.constructDelegatingKeyDeserializer(config, type, des);
        }
        EnumResolver<?> enumRes = constructEnumResolver(enumClass, config, beanDesc.findJsonValueMethod());
        for (AnnotatedMethod factory : beanDesc.getFactoryMethods()) {
            if (config.getAnnotationIntrospector().hasCreatorAnnotation(factory)) {
                if (factory.getParameterCount() != 1 || !factory.getRawReturnType().isAssignableFrom(enumClass)) {
                    throw new IllegalArgumentException("Unsuitable method (" + factory + ") decorated with @JsonCreator (for Enum type " + enumClass.getName() + ")");
                } else if (factory.getGenericParameterType(0) != String.class) {
                    throw new IllegalArgumentException("Parameter #0 type for factory method (" + factory + ") not suitable, must be java.lang.String");
                } else {
                    if (config.canOverrideAccessModifiers()) {
                        ClassUtil.checkAndFixAccess(factory.getMember());
                    }
                    return StdKeyDeserializers.constructEnumKeyDeserializer(enumRes, factory);
                }
            }
        }
        return StdKeyDeserializers.constructEnumKeyDeserializer(enumRes);
    }

    public TypeDeserializer findPropertyTypeDeserializer(DeserializationConfig config, JavaType baseType, AnnotatedMember annotated) throws JsonMappingException {
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder<?> b = ai.findPropertyTypeResolver(config, annotated, baseType);
        if (b == null) {
            return findTypeDeserializer(config, baseType);
        }
        return b.buildTypeDeserializer(config, baseType, config.getSubtypeResolver().collectAndResolveSubtypes(annotated, config, ai, baseType));
    }

    public TypeDeserializer findPropertyContentTypeDeserializer(DeserializationConfig config, JavaType containerType, AnnotatedMember propertyEntity) throws JsonMappingException {
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder<?> b = ai.findPropertyContentTypeResolver(config, propertyEntity, containerType);
        JavaType contentType = containerType.getContentType();
        if (b == null) {
            return findTypeDeserializer(config, contentType);
        }
        return b.buildTypeDeserializer(config, contentType, config.getSubtypeResolver().collectAndResolveSubtypes(propertyEntity, config, ai, contentType));
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> findDeserializerFromAnnotation(DeserializationContext ctxt, Annotated ann) throws JsonMappingException {
        Object deserDef = ctxt.getAnnotationIntrospector().findDeserializer(ann);
        if (deserDef == null) {
            return null;
        }
        return ctxt.deserializerInstance(ann, deserDef);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected <T extends com.fasterxml.jackson.databind.JavaType> T modifyTypeByAnnotation(com.fasterxml.jackson.databind.DeserializationContext r17, com.fasterxml.jackson.databind.introspect.Annotated r18, T r19) throws com.fasterxml.jackson.databind.JsonMappingException {
        /*
            r16 = this;
            com.fasterxml.jackson.databind.AnnotationIntrospector r7 = r17.getAnnotationIntrospector()
            r0 = r18
            r1 = r19
            java.lang.Class r12 = r7.findDeserializationType(r0, r1)
            if (r12 == 0) goto L_0x0014
            r0 = r19
            com.fasterxml.jackson.databind.JavaType r19 = r0.narrowBy(r12)     // Catch:{ IllegalArgumentException -> 0x004d }
        L_0x0014:
            boolean r13 = r19.isContainerType()
            if (r13 == 0) goto L_0x00f7
            com.fasterxml.jackson.databind.JavaType r13 = r19.getKeyType()
            r0 = r18
            java.lang.Class r10 = r7.findDeserializationKeyType(r0, r13)
            if (r10 == 0) goto L_0x009d
            r0 = r19
            boolean r13 = r0 instanceof com.fasterxml.jackson.databind.type.MapLikeType
            if (r13 != 0) goto L_0x0094
            com.fasterxml.jackson.databind.JsonMappingException r13 = new com.fasterxml.jackson.databind.JsonMappingException
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Illegal key-type annotation: type "
            java.lang.StringBuilder r14 = r14.append(r15)
            r0 = r19
            java.lang.StringBuilder r14 = r14.append(r0)
            java.lang.String r15 = " is not a Map(-like) type"
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r13.<init>(r14)
            throw r13
        L_0x004d:
            r6 = move-exception
            com.fasterxml.jackson.databind.JsonMappingException r13 = new com.fasterxml.jackson.databind.JsonMappingException
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Failed to narrow type "
            java.lang.StringBuilder r14 = r14.append(r15)
            r0 = r19
            java.lang.StringBuilder r14 = r14.append(r0)
            java.lang.String r15 = " with concrete-type annotation (value "
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r12.getName()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = "), method '"
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r18.getName()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = "': "
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r6.getMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = 0
            r13.<init>(r14, r15, r6)
            throw r13
        L_0x0094:
            r0 = r19
            com.fasterxml.jackson.databind.type.MapLikeType r0 = (com.fasterxml.jackson.databind.type.MapLikeType) r0     // Catch:{ IllegalArgumentException -> 0x00f8 }
            r13 = r0
            com.fasterxml.jackson.databind.JavaType r19 = r13.narrowKey(r10)     // Catch:{ IllegalArgumentException -> 0x00f8 }
        L_0x009d:
            com.fasterxml.jackson.databind.JavaType r11 = r19.getKeyType()
            if (r11 == 0) goto L_0x00c5
            java.lang.Object r13 = r11.getValueHandler()
            if (r13 != 0) goto L_0x00c5
            r0 = r18
            java.lang.Object r9 = r7.findKeyDeserializer(r0)
            r0 = r17
            r1 = r18
            com.fasterxml.jackson.databind.KeyDeserializer r8 = r0.keyDeserializerInstance(r1, r9)
            if (r8 == 0) goto L_0x00c5
            com.fasterxml.jackson.databind.type.MapLikeType r19 = (com.fasterxml.jackson.databind.type.MapLikeType) r19
            r0 = r19
            com.fasterxml.jackson.databind.type.MapLikeType r19 = r0.withKeyValueHandler(r8)
            com.fasterxml.jackson.databind.JavaType r11 = r19.getKeyType()
        L_0x00c5:
            com.fasterxml.jackson.databind.JavaType r13 = r19.getContentType()
            r0 = r18
            java.lang.Class r2 = r7.findDeserializationContentType(r0, r13)
            if (r2 == 0) goto L_0x00d7
            r0 = r19
            com.fasterxml.jackson.databind.JavaType r19 = r0.narrowContentsBy(r2)     // Catch:{ IllegalArgumentException -> 0x0131 }
        L_0x00d7:
            com.fasterxml.jackson.databind.JavaType r5 = r19.getContentType()
            java.lang.Object r13 = r5.getValueHandler()
            if (r13 != 0) goto L_0x00f7
            r0 = r18
            java.lang.Object r4 = r7.findContentDeserializer(r0)
            r0 = r17
            r1 = r18
            com.fasterxml.jackson.databind.JsonDeserializer r3 = r0.deserializerInstance(r1, r4)
            if (r3 == 0) goto L_0x00f7
            r0 = r19
            com.fasterxml.jackson.databind.JavaType r19 = r0.withContentValueHandler(r3)
        L_0x00f7:
            return r19
        L_0x00f8:
            r6 = move-exception
            com.fasterxml.jackson.databind.JsonMappingException r13 = new com.fasterxml.jackson.databind.JsonMappingException
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Failed to narrow key type "
            java.lang.StringBuilder r14 = r14.append(r15)
            r0 = r19
            java.lang.StringBuilder r14 = r14.append(r0)
            java.lang.String r15 = " with key-type annotation ("
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r10.getName()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = "): "
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r6.getMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = 0
            r13.<init>(r14, r15, r6)
            throw r13
        L_0x0131:
            r6 = move-exception
            com.fasterxml.jackson.databind.JsonMappingException r13 = new com.fasterxml.jackson.databind.JsonMappingException
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "Failed to narrow content type "
            java.lang.StringBuilder r14 = r14.append(r15)
            r0 = r19
            java.lang.StringBuilder r14 = r14.append(r0)
            java.lang.String r15 = " with content-type annotation ("
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r2.getName()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = "): "
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = r6.getMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            r15 = 0
            r13.<init>(r14, r15, r6)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.BasicDeserializerFactory.modifyTypeByAnnotation(com.fasterxml.jackson.databind.DeserializationContext, com.fasterxml.jackson.databind.introspect.Annotated, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.JavaType");
    }

    /* access modifiers changed from: protected */
    public JavaType resolveType(DeserializationContext ctxt, BeanDescription beanDesc, JavaType type, AnnotatedMember member) throws JsonMappingException {
        TypeDeserializer valueTypeDeser;
        TypeDeserializer contentTypeDeser;
        KeyDeserializer kd;
        if (type.isContainerType()) {
            AnnotationIntrospector intr = ctxt.getAnnotationIntrospector();
            if (!(type.getKeyType() == null || (kd = ctxt.keyDeserializerInstance(member, intr.findKeyDeserializer(member))) == null)) {
                type = ((MapLikeType) type).withKeyValueHandler(kd);
                JavaType keyType = type.getKeyType();
            }
            JsonDeserializer<?> cd = ctxt.deserializerInstance(member, intr.findContentDeserializer(member));
            if (cd != null) {
                type = type.withContentValueHandler(cd);
            }
            if ((member instanceof AnnotatedMember) && (contentTypeDeser = findPropertyContentTypeDeserializer(ctxt.getConfig(), type, member)) != null) {
                type = type.withContentTypeHandler(contentTypeDeser);
            }
        }
        if (member instanceof AnnotatedMember) {
            valueTypeDeser = findPropertyTypeDeserializer(ctxt.getConfig(), type, member);
        } else {
            valueTypeDeser = findTypeDeserializer(ctxt.getConfig(), type);
        }
        if (valueTypeDeser != null) {
            return type.withTypeHandler(valueTypeDeser);
        }
        return type;
    }

    /* access modifiers changed from: protected */
    public EnumResolver<?> constructEnumResolver(Class<?> enumClass, DeserializationConfig config, AnnotatedMethod jsonValueMethod) {
        if (jsonValueMethod != null) {
            Method accessor = jsonValueMethod.getAnnotated();
            if (config.canOverrideAccessModifiers()) {
                ClassUtil.checkAndFixAccess(accessor);
            }
            return EnumResolver.constructUnsafeUsingMethod(enumClass, accessor);
        } else if (config.isEnabled(DeserializationFeature.READ_ENUMS_USING_TO_STRING)) {
            return EnumResolver.constructUnsafeUsingToString(enumClass);
        } else {
            return EnumResolver.constructUnsafe(enumClass, config.getAnnotationIntrospector());
        }
    }

    /* access modifiers changed from: protected */
    public AnnotatedMethod _findJsonValueFor(DeserializationConfig config, JavaType enumType) {
        if (enumType == null) {
            return null;
        }
        return config.introspect(enumType).findJsonValueMethod();
    }
}
