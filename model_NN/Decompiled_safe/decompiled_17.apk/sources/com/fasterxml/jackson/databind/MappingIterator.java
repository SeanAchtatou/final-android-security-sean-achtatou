package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MappingIterator<T> implements Iterator<T>, Closeable {
    protected static final MappingIterator<?> EMPTY_ITERATOR = new MappingIterator<>(null, null, null, null, false, null);
    protected final boolean _closeParser;
    protected final DeserializationContext _context;
    protected final JsonDeserializer<T> _deserializer;
    protected boolean _hasNextChecked;
    protected JsonParser _parser;
    protected final JavaType _type;
    protected final T _updatedValue;

    @Deprecated
    protected MappingIterator(JavaType type, JsonParser jp, DeserializationContext ctxt, JsonDeserializer<?> deser) {
        this(type, jp, ctxt, deser, true, null);
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [com.fasterxml.jackson.databind.JsonDeserializer<T>, com.fasterxml.jackson.databind.JsonDeserializer<?>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected MappingIterator(com.fasterxml.jackson.databind.JavaType r3, com.fasterxml.jackson.core.JsonParser r4, com.fasterxml.jackson.databind.DeserializationContext r5, com.fasterxml.jackson.databind.JsonDeserializer<?> r6, boolean r7, java.lang.Object r8) {
        /*
            r2 = this;
            r2.<init>()
            r2._type = r3
            r2._parser = r4
            r2._context = r5
            r2._deserializer = r6
            r2._closeParser = r7
            if (r8 != 0) goto L_0x0022
            r0 = 0
            r2._updatedValue = r0
        L_0x0012:
            if (r7 == 0) goto L_0x0021
            if (r4 == 0) goto L_0x0021
            com.fasterxml.jackson.core.JsonToken r0 = r4.getCurrentToken()
            com.fasterxml.jackson.core.JsonToken r1 = com.fasterxml.jackson.core.JsonToken.START_ARRAY
            if (r0 != r1) goto L_0x0021
            r4.clearCurrentToken()
        L_0x0021:
            return
        L_0x0022:
            r2._updatedValue = r8
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.MappingIterator.<init>(com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext, com.fasterxml.jackson.databind.JsonDeserializer, boolean, java.lang.Object):void");
    }

    protected static <T> MappingIterator<T> emptyIterator() {
        return EMPTY_ITERATOR;
    }

    public boolean hasNext() {
        try {
            return hasNextValue();
        } catch (JsonMappingException e) {
            throw new RuntimeJsonMappingException(e.getMessage(), e);
        } catch (IOException e2) {
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    public T next() {
        try {
            return nextValue();
        } catch (JsonMappingException e) {
            throw new RuntimeJsonMappingException(e.getMessage(), e);
        } catch (IOException e2) {
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public void close() throws IOException {
        if (this._parser != null) {
            this._parser.close();
        }
    }

    public boolean hasNextValue() throws IOException {
        JsonToken t;
        if (this._parser == null) {
            return false;
        }
        if (!this._hasNextChecked) {
            JsonToken t2 = this._parser.getCurrentToken();
            this._hasNextChecked = true;
            if (t2 == null && ((t = this._parser.nextToken()) == null || t == JsonToken.END_ARRAY)) {
                JsonParser jp = this._parser;
                this._parser = null;
                if (!this._closeParser) {
                    return false;
                }
                jp.close();
                return false;
            }
        }
        return true;
    }

    public T nextValue() throws IOException {
        T result;
        if (!this._hasNextChecked && !hasNextValue()) {
            throw new NoSuchElementException();
        } else if (this._parser == null) {
            throw new NoSuchElementException();
        } else {
            this._hasNextChecked = false;
            if (this._updatedValue == null) {
                result = this._deserializer.deserialize(this._parser, this._context);
            } else {
                this._deserializer.deserialize(this._parser, this._context, this._updatedValue);
                result = this._updatedValue;
            }
            this._parser.clearCurrentToken();
            return result;
        }
    }
}
