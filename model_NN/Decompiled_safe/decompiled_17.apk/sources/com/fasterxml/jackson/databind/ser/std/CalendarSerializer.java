package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;

@JacksonStdImpl
public class CalendarSerializer extends DateTimeSerializerBase<Calendar> {
    public static CalendarSerializer instance = new CalendarSerializer();

    public CalendarSerializer() {
        this(false, null);
    }

    public CalendarSerializer(boolean useTimestamp, DateFormat customFormat) {
        super(Calendar.class, useTimestamp, customFormat);
    }

    public CalendarSerializer withFormat(boolean timestamp, DateFormat customFormat) {
        if (timestamp) {
            return new CalendarSerializer(true, null);
        }
        return new CalendarSerializer(false, customFormat);
    }

    /* access modifiers changed from: protected */
    public long _timestamp(Calendar value) {
        if (value == null) {
            return 0;
        }
        return value.getTimeInMillis();
    }

    public void serialize(Calendar value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        if (this._useTimestamp) {
            jgen.writeNumber(_timestamp(value));
        } else if (this._customFormat != null) {
            synchronized (this._customFormat) {
                jgen.writeString(this._customFormat.format(value));
            }
        } else {
            provider.defaultSerializeDateValue(value.getTime(), jgen);
        }
    }
}
