package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import java.io.IOException;

public abstract class FromStringDeserializer<T> extends StdScalarDeserializer<T> {
    /* access modifiers changed from: protected */
    public abstract T _deserialize(String str, DeserializationContext deserializationContext) throws IOException, JsonProcessingException;

    protected FromStringDeserializer(Class<?> vc) {
        super(vc);
    }

    public final T deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String text = jp.getValueAsString();
        if (text != null) {
            if (text.length() == 0) {
                return null;
            }
            String text2 = text.trim();
            if (text2.length() == 0) {
                return null;
            }
            try {
                T result = _deserialize(text2, ctxt);
                if (result != null) {
                    return result;
                }
            } catch (IllegalArgumentException e) {
            }
            throw ctxt.weirdStringException(text2, this._valueClass, "not a valid textual representation");
        } else if (jp.getCurrentToken() == JsonToken.VALUE_EMBEDDED_OBJECT) {
            Object ob = jp.getEmbeddedObject();
            if (ob == null) {
                return null;
            }
            if (this._valueClass.isAssignableFrom(ob.getClass())) {
                return ob;
            }
            return _deserializeEmbedded(ob, ctxt);
        } else {
            throw ctxt.mappingException(this._valueClass);
        }
    }

    /* access modifiers changed from: protected */
    public T _deserializeEmbedded(Object ob, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        throw ctxt.mappingException("Don't know how to convert embedded Object of type " + ob.getClass().getName() + " into " + this._valueClass.getName());
    }
}
