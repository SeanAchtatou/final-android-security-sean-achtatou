package com.fasterxml.jackson.databind;

public class PropertyName {
    public static final PropertyName NO_NAME = new PropertyName(new String("#disabled"), null);
    public static final PropertyName USE_DEFAULT = new PropertyName("", null);
    protected final String _namespace;
    protected final String _simpleName;

    public PropertyName(String simpleName) {
        this(simpleName, null);
    }

    public PropertyName(String simpleName, String namespace) {
        this._simpleName = simpleName == null ? "" : simpleName;
        this._namespace = namespace;
    }

    public static PropertyName construct(String simpleName, String ns) {
        if (simpleName == null) {
            simpleName = "";
        }
        if (ns == null && simpleName.length() == 0) {
            return USE_DEFAULT;
        }
        return new PropertyName(simpleName, ns);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public PropertyName withSimpleName(String simpleName) {
        if (simpleName == null) {
            simpleName = "";
        }
        return simpleName.equals(this._simpleName) ? this : new PropertyName(simpleName, this._namespace);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public PropertyName withNamespace(String ns) {
        if (ns == null) {
            if (this._namespace == null) {
                return this;
            }
        } else if (ns.equals(this._namespace)) {
            return this;
        }
        return new PropertyName(this._simpleName, ns);
    }

    public String getSimpleName() {
        return this._simpleName;
    }

    public String getNamespace() {
        return this._namespace;
    }

    public boolean hasSimpleName() {
        return this._simpleName.length() > 0;
    }

    public boolean hasNamespace() {
        return this._namespace != null;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (o == this) {
            return true;
        }
        if (o == null || o.getClass() != PropertyName.class) {
            return false;
        }
        if (USE_DEFAULT == o) {
            if (this != USE_DEFAULT) {
                z = false;
            }
            return z;
        }
        if (this._simpleName == null) {
            if (this._simpleName != null) {
                return false;
            }
        } else if (!this._simpleName.equals(this._simpleName)) {
            return false;
        }
        if (this._namespace != null) {
            return this._namespace.equals(this._namespace);
        }
        if (this._namespace != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        if (this._namespace == null) {
            return this._simpleName.hashCode();
        }
        return this._namespace.hashCode() ^ this._simpleName.hashCode();
    }

    public String toString() {
        if (this._namespace == null) {
            return this._simpleName;
        }
        return "{" + this._namespace + "}" + this._simpleName;
    }
}
