package com.fasterxml.jackson.databind.util;

public interface Converter<IN, OUT> {
    OUT convert(IN in);
}
