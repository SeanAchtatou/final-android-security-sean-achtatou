package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;

public class SimpleBeanPropertyDefinition extends BeanPropertyDefinition {
    protected final AnnotatedMember _member;
    protected final String _name;

    public SimpleBeanPropertyDefinition(AnnotatedMember member) {
        this(member, member.getName());
    }

    public SimpleBeanPropertyDefinition(AnnotatedMember member, String name) {
        this._member = member;
        this._name = name;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public SimpleBeanPropertyDefinition withName(String newName) {
        return this._name.equals(newName) ? this : new SimpleBeanPropertyDefinition(this._member, newName);
    }

    public String getName() {
        return this._name;
    }

    public String getInternalName() {
        return getName();
    }

    public boolean isExplicitlyIncluded() {
        return false;
    }

    public boolean hasGetter() {
        return getGetter() != null;
    }

    public boolean hasSetter() {
        return getSetter() != null;
    }

    public boolean hasField() {
        return this._member instanceof AnnotatedField;
    }

    public boolean hasConstructorParameter() {
        return this._member instanceof AnnotatedParameter;
    }

    public AnnotatedMethod getGetter() {
        if (!(this._member instanceof AnnotatedMethod) || ((AnnotatedMethod) this._member).getParameterCount() != 0) {
            return null;
        }
        return (AnnotatedMethod) this._member;
    }

    public AnnotatedMethod getSetter() {
        if (!(this._member instanceof AnnotatedMethod) || ((AnnotatedMethod) this._member).getParameterCount() != 1) {
            return null;
        }
        return (AnnotatedMethod) this._member;
    }

    public AnnotatedField getField() {
        if (this._member instanceof AnnotatedField) {
            return (AnnotatedField) this._member;
        }
        return null;
    }

    public AnnotatedParameter getConstructorParameter() {
        if (this._member instanceof AnnotatedParameter) {
            return (AnnotatedParameter) this._member;
        }
        return null;
    }

    public AnnotatedMember getAccessor() {
        AnnotatedMember acc = getGetter();
        if (acc == null) {
            return getField();
        }
        return acc;
    }

    public AnnotatedMember getMutator() {
        AnnotatedMember acc = getConstructorParameter();
        if (acc != null) {
            return acc;
        }
        AnnotatedMember acc2 = getSetter();
        if (acc2 == null) {
            return getField();
        }
        return acc2;
    }

    public AnnotatedMember getPrimaryMember() {
        return this._member;
    }
}
