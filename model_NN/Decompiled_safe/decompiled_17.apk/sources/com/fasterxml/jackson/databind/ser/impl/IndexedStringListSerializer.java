package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonArrayFormatVisitor;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StaticListSerializerBase;
import java.io.IOException;
import java.util.List;

@JacksonStdImpl
public final class IndexedStringListSerializer extends StaticListSerializerBase<List<String>> implements ContextualSerializer {
    public static final IndexedStringListSerializer instance = new IndexedStringListSerializer();
    protected final JsonSerializer<String> _serializer;

    public /* bridge */ /* synthetic */ void serialize(Object x0, JsonGenerator x1, SerializerProvider x2) throws IOException, JsonGenerationException {
        serialize((List<String>) ((List) x0), x1, x2);
    }

    public /* bridge */ /* synthetic */ void serializeWithType(Object x0, JsonGenerator x1, SerializerProvider x2, TypeSerializer x3) throws IOException, JsonProcessingException {
        serializeWithType((List<String>) ((List) x0), x1, x2, x3);
    }

    protected IndexedStringListSerializer() {
        this(null);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.fasterxml.jackson.databind.JsonSerializer<?>, com.fasterxml.jackson.databind.JsonSerializer<java.lang.String>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IndexedStringListSerializer(com.fasterxml.jackson.databind.JsonSerializer<?> r2) {
        /*
            r1 = this;
            java.lang.Class<java.util.List> r0 = java.util.List.class
            r1.<init>(r0)
            r1._serializer = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.ser.impl.IndexedStringListSerializer.<init>(com.fasterxml.jackson.databind.JsonSerializer):void");
    }

    /* access modifiers changed from: protected */
    public JsonNode contentSchema() {
        return createSchemaNode("string", true);
    }

    /* access modifiers changed from: protected */
    public void acceptContentVisitor(JsonArrayFormatVisitor visitor) {
        visitor.itemsFormat(JsonFormatTypes.STRING);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public JsonSerializer<?> createContextual(SerializerProvider provider, BeanProperty property) throws JsonMappingException {
        AnnotatedMember m;
        Object serDef;
        JsonSerializer<?> ser = null;
        if (!(property == null || (m = property.getMember()) == null || (serDef = provider.getAnnotationIntrospector().findContentSerializer(m)) == null)) {
            ser = provider.serializerInstance(m, serDef);
        }
        if (ser == null) {
            ser = this._serializer;
        }
        if (ser == null) {
            ser = provider.findValueSerializer(String.class, property);
        } else if (ser instanceof ContextualSerializer) {
            ser = ((ContextualSerializer) ser).createContextual(provider, property);
        }
        if (isDefaultSerializer(ser)) {
            ser = null;
        }
        return ser == this._serializer ? this : new IndexedStringListSerializer(ser);
    }

    public void serialize(List<String> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        jgen.writeStartArray();
        if (this._serializer == null) {
            serializeContents(value, jgen, provider);
        } else {
            serializeUsingCustom(value, jgen, provider);
        }
        jgen.writeEndArray();
    }

    public void serializeWithType(List<String> value, JsonGenerator jgen, SerializerProvider provider, TypeSerializer typeSer) throws IOException, JsonGenerationException {
        typeSer.writeTypePrefixForArray(value, jgen);
        if (this._serializer == null) {
            serializeContents(value, jgen, provider);
        } else {
            serializeUsingCustom(value, jgen, provider);
        }
        typeSer.writeTypeSuffixForArray(value, jgen);
    }

    private final void serializeContents(List<String> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        try {
            int len = value.size();
            for (int i = 0; i < len; i++) {
                String str = value.get(i);
                if (str == null) {
                    provider.defaultSerializeNull(jgen);
                } else {
                    jgen.writeString(str);
                }
            }
        } catch (Exception e) {
            wrapAndThrow(provider, e, value, 0);
        }
    }

    private final void serializeUsingCustom(List<String> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        try {
            int len = value.size();
            JsonSerializer<String> ser = this._serializer;
            for (int i = 0; i < len; i++) {
                String str = value.get(i);
                if (str == null) {
                    provider.defaultSerializeNull(jgen);
                } else {
                    ser.serialize(str, jgen, provider);
                }
            }
        } catch (Exception e) {
            wrapAndThrow(provider, e, value, 0);
        }
    }
}
