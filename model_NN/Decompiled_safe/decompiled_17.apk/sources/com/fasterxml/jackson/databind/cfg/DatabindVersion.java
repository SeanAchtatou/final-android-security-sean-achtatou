package com.fasterxml.jackson.databind.cfg;

import com.fasterxml.jackson.core.util.VersionUtil;

public class DatabindVersion extends VersionUtil {
    public static final DatabindVersion instance = new DatabindVersion();
}
