package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonObjectFormatVisitor;
import com.fasterxml.jackson.databind.jsonschema.JsonSchema;
import com.fasterxml.jackson.databind.jsonschema.SchemaAware;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.ContainerSerializer;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.util.EnumValues;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.EnumMap;
import java.util.Map;

@JacksonStdImpl
public class EnumMapSerializer extends ContainerSerializer<EnumMap<? extends Enum<?>, ?>> implements ContextualSerializer {
    protected final EnumValues _keyEnums;
    protected final BeanProperty _property;
    protected final boolean _staticTyping;
    protected final JsonSerializer<Object> _valueSerializer;
    protected final JavaType _valueType;
    protected final TypeSerializer _valueTypeSerializer;

    public /* bridge */ /* synthetic */ boolean hasSingleElement(Object x0) {
        return hasSingleElement((EnumMap<? extends Enum<?>, ?>) ((EnumMap) x0));
    }

    public /* bridge */ /* synthetic */ boolean isEmpty(Object x0) {
        return isEmpty((EnumMap<? extends Enum<?>, ?>) ((EnumMap) x0));
    }

    public /* bridge */ /* synthetic */ void serialize(Object x0, JsonGenerator x1, SerializerProvider x2) throws IOException, JsonGenerationException {
        serialize((EnumMap<? extends Enum<?>, ?>) ((EnumMap) x0), x1, x2);
    }

    public /* bridge */ /* synthetic */ void serializeWithType(Object x0, JsonGenerator x1, SerializerProvider x2, TypeSerializer x3) throws IOException, JsonProcessingException {
        serializeWithType((EnumMap<? extends Enum<?>, ?>) ((EnumMap) x0), x1, x2, x3);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public EnumMapSerializer(JavaType valueType, boolean staticTyping, EnumValues keyEnums, TypeSerializer vts, JsonSerializer<Object> valueSerializer) {
        super(EnumMap.class, false);
        boolean z = false;
        this._property = null;
        if (staticTyping || (valueType != null && valueType.isFinal())) {
            z = true;
        }
        this._staticTyping = z;
        this._valueType = valueType;
        this._keyEnums = keyEnums;
        this._valueTypeSerializer = vts;
        this._valueSerializer = valueSerializer;
    }

    public EnumMapSerializer(EnumMapSerializer src, BeanProperty property, JsonSerializer<?> ser) {
        super(src);
        this._property = property;
        this._staticTyping = src._staticTyping;
        this._valueType = src._valueType;
        this._keyEnums = src._keyEnums;
        this._valueTypeSerializer = src._valueTypeSerializer;
        this._valueSerializer = ser;
    }

    public EnumMapSerializer _withValueTypeSerializer(TypeSerializer vts) {
        return new EnumMapSerializer(this._valueType, this._staticTyping, this._keyEnums, vts, this._valueSerializer);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public EnumMapSerializer withValueSerializer(BeanProperty prop, JsonSerializer<?> ser) {
        return (this._property == prop && ser == this._valueSerializer) ? this : new EnumMapSerializer(this, prop, ser);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public JsonSerializer<?> createContextual(SerializerProvider provider, BeanProperty property) throws JsonMappingException {
        AnnotatedMember m;
        Object serDef;
        JsonSerializer<?> ser = null;
        if (!(property == null || (m = property.getMember()) == null || (serDef = provider.getAnnotationIntrospector().findContentSerializer(m)) == null)) {
            ser = provider.serializerInstance(m, serDef);
        }
        if (ser == null) {
            ser = this._valueSerializer;
        }
        if (ser == null) {
            if (this._staticTyping) {
                return withValueSerializer(property, provider.findValueSerializer(this._valueType, property));
            }
        } else if (this._valueSerializer instanceof ContextualSerializer) {
            ser = ((ContextualSerializer) ser).createContextual(provider, property);
        }
        if (ser != this._valueSerializer) {
            return withValueSerializer(property, ser);
        }
        return this;
    }

    public JavaType getContentType() {
        return this._valueType;
    }

    public JsonSerializer<?> getContentSerializer() {
        return this._valueSerializer;
    }

    public boolean isEmpty(EnumMap<? extends Enum<?>, ?> value) {
        return value == null || value.isEmpty();
    }

    public boolean hasSingleElement(EnumMap<? extends Enum<?>, ?> value) {
        return value.size() == 1;
    }

    public void serialize(EnumMap<? extends Enum<?>, ?> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        jgen.writeStartObject();
        if (!value.isEmpty()) {
            serializeContents(value, jgen, provider);
        }
        jgen.writeEndObject();
    }

    public void serializeWithType(EnumMap<? extends Enum<?>, ?> value, JsonGenerator jgen, SerializerProvider provider, TypeSerializer typeSer) throws IOException, JsonGenerationException {
        typeSer.writeTypePrefixForObject(value, jgen);
        if (!value.isEmpty()) {
            serializeContents(value, jgen, provider);
        }
        typeSer.writeTypeSuffixForObject(value, jgen);
    }

    /* access modifiers changed from: protected */
    public void serializeContents(EnumMap<? extends Enum<?>, ?> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        JsonSerializer<Object> currSerializer;
        if (this._valueSerializer != null) {
            serializeContentsUsing(value, jgen, provider, this._valueSerializer);
            return;
        }
        JsonSerializer<Object> prevSerializer = null;
        Class<?> prevClass = null;
        EnumValues keyEnums = this._keyEnums;
        for (Map.Entry<? extends Enum<?>, ?> entry : value.entrySet()) {
            Enum<?> key = (Enum) entry.getKey();
            if (keyEnums == null) {
                keyEnums = ((EnumSerializer) ((StdSerializer) provider.findValueSerializer(key.getDeclaringClass(), this._property))).getEnumValues();
            }
            jgen.writeFieldName(keyEnums.serializedValueFor(key));
            Object valueElem = entry.getValue();
            if (valueElem == null) {
                provider.defaultSerializeNull(jgen);
            } else {
                Class<?> cc = valueElem.getClass();
                if (cc == prevClass) {
                    currSerializer = prevSerializer;
                } else {
                    currSerializer = provider.findValueSerializer(cc, this._property);
                    prevSerializer = currSerializer;
                    prevClass = cc;
                }
                try {
                    currSerializer.serialize(valueElem, jgen, provider);
                } catch (Exception e) {
                    wrapAndThrow(provider, e, value, ((Enum) entry.getKey()).name());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void serializeContentsUsing(EnumMap<? extends Enum<?>, ?> value, JsonGenerator jgen, SerializerProvider provider, JsonSerializer<Object> valueSer) throws IOException, JsonGenerationException {
        EnumValues keyEnums = this._keyEnums;
        for (Map.Entry<? extends Enum<?>, ?> entry : value.entrySet()) {
            Enum<?> key = (Enum) entry.getKey();
            if (keyEnums == null) {
                keyEnums = ((EnumSerializer) ((StdSerializer) provider.findValueSerializer(key.getDeclaringClass(), this._property))).getEnumValues();
            }
            jgen.writeFieldName(keyEnums.serializedValueFor(key));
            Object valueElem = entry.getValue();
            if (valueElem == null) {
                provider.defaultSerializeNull(jgen);
            } else {
                try {
                    valueSer.serialize(valueElem, jgen, provider);
                } catch (Exception e) {
                    wrapAndThrow(provider, e, value, ((Enum) entry.getKey()).name());
                }
            }
        }
    }

    public JsonNode getSchema(SerializerProvider provider, Type typeHint) throws JsonMappingException {
        ObjectNode o = createSchemaNode("object", true);
        if (typeHint instanceof ParameterizedType) {
            Type[] typeArgs = ((ParameterizedType) typeHint).getActualTypeArguments();
            if (typeArgs.length == 2) {
                JavaType enumType = provider.constructType(typeArgs[0]);
                JavaType valueType = provider.constructType(typeArgs[1]);
                ObjectNode propsNode = JsonNodeFactory.instance.objectNode();
                for (Enum<?> enumValue : (Enum[]) enumType.getRawClass().getEnumConstants()) {
                    JsonSerializer<Object> ser = provider.findValueSerializer(valueType.getRawClass(), this._property);
                    propsNode.put(provider.getConfig().getAnnotationIntrospector().findEnumValue(enumValue), ser instanceof SchemaAware ? ((SchemaAware) ser).getSchema(provider, null) : JsonSchema.getDefaultSchemaNode());
                }
                o.put("properties", propsNode);
            }
        }
        return o;
    }

    public void acceptJsonFormatVisitor(JsonFormatVisitorWrapper visitor, JavaType typeHint) throws JsonMappingException {
        JsonObjectFormatVisitor objectVisitor = visitor.expectObjectFormat(typeHint);
        JavaType valueType = typeHint.containedType(1);
        if (valueType == null) {
            valueType = visitor.getProvider().constructType(Object.class);
        }
        JsonSerializer<Object> ser = this._valueSerializer;
        for (Map.Entry<?, SerializedString> entry : this._keyEnums.internalMap().entrySet()) {
            String name = ((SerializedString) entry.getValue()).getValue();
            if (ser == null) {
                ser = visitor.getProvider().findValueSerializer(entry.getKey().getClass(), this._property);
            }
            objectVisitor.property(name, ser, valueType);
        }
    }
}
