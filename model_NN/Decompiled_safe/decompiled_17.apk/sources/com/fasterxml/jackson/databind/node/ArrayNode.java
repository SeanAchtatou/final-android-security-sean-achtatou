package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.node.ContainerNode;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ArrayNode extends ContainerNode<ArrayNode> {
    protected List<JsonNode> _children;

    public ArrayNode(JsonNodeFactory nc) {
        super(nc);
    }

    protected ArrayNode(JsonNodeFactory nc, List<JsonNode> children) {
        super(nc);
        this._children = children;
    }

    public ArrayNode deepCopy() {
        if (getClass() == ArrayNode.class) {
            return _defaultDeepCopy();
        }
        throw new IllegalStateException("ArrayNode subtype (" + getClass().getName() + " does not override deepCopy(), needs to");
    }

    /* access modifiers changed from: protected */
    public ArrayNode _defaultDeepCopy() {
        if (this._children == null) {
            return new ArrayNode(this._nodeFactory);
        }
        int len = this._children.size();
        List<JsonNode> newKids = _createList(Math.max(4, len));
        for (int i = 0; i < len; i++) {
            newKids.add(this._children.get(i).deepCopy());
        }
        return new ArrayNode(this._nodeFactory, newKids);
    }

    public JsonToken asToken() {
        return JsonToken.START_ARRAY;
    }

    public boolean isArray() {
        return true;
    }

    public int size() {
        if (this._children == null) {
            return 0;
        }
        return this._children.size();
    }

    public Iterator<JsonNode> elements() {
        return this._children == null ? ContainerNode.NoNodesIterator.instance() : this._children.iterator();
    }

    public JsonNode get(int index) {
        if (index < 0 || this._children == null || index >= this._children.size()) {
            return null;
        }
        return this._children.get(index);
    }

    public JsonNode get(String fieldName) {
        return null;
    }

    public JsonNode path(String fieldName) {
        return MissingNode.getInstance();
    }

    public JsonNode path(int index) {
        if (index < 0 || this._children == null || index >= this._children.size()) {
            return MissingNode.getInstance();
        }
        return this._children.get(index);
    }

    public final void serialize(JsonGenerator jg, SerializerProvider provider) throws IOException, JsonProcessingException {
        jg.writeStartArray();
        if (this._children != null) {
            Iterator i$ = this._children.iterator();
            while (i$.hasNext()) {
                ((BaseJsonNode) i$.next()).serialize(jg, provider);
            }
        }
        jg.writeEndArray();
    }

    public void serializeWithType(JsonGenerator jg, SerializerProvider provider, TypeSerializer typeSer) throws IOException, JsonProcessingException {
        typeSer.writeTypePrefixForArray(this, jg);
        if (this._children != null) {
            Iterator i$ = this._children.iterator();
            while (i$.hasNext()) {
                ((BaseJsonNode) i$.next()).serialize(jg, provider);
            }
        }
        typeSer.writeTypeSuffixForArray(this, jg);
    }

    public JsonNode findValue(String fieldName) {
        if (this._children != null) {
            for (JsonNode node : this._children) {
                JsonNode value = node.findValue(fieldName);
                if (value != null) {
                    return value;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findValues(String fieldName, List<JsonNode> foundSoFar) {
        if (this._children != null) {
            for (JsonNode node : this._children) {
                foundSoFar = node.findValues(fieldName, foundSoFar);
            }
        }
        return foundSoFar;
    }

    public List<String> findValuesAsText(String fieldName, List<String> foundSoFar) {
        if (this._children != null) {
            for (JsonNode node : this._children) {
                foundSoFar = node.findValuesAsText(fieldName, foundSoFar);
            }
        }
        return foundSoFar;
    }

    public ObjectNode findParent(String fieldName) {
        if (this._children != null) {
            for (JsonNode node : this._children) {
                JsonNode parent = node.findParent(fieldName);
                if (parent != null) {
                    return (ObjectNode) parent;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findParents(String fieldName, List<JsonNode> foundSoFar) {
        if (this._children != null) {
            for (JsonNode node : this._children) {
                foundSoFar = node.findParents(fieldName, foundSoFar);
            }
        }
        return foundSoFar;
    }

    public JsonNode set(int index, JsonNode value) {
        if (value == null) {
            value = nullNode();
        }
        return _set(index, value);
    }

    public ArrayNode add(JsonNode value) {
        if (value == null) {
            value = nullNode();
        }
        _add(value);
        return this;
    }

    public ArrayNode addAll(ArrayNode other) {
        int len = other.size();
        if (len > 0) {
            if (this._children == null) {
                this._children = _createList(len + 2);
            }
            other.addContentsTo(this._children);
        }
        return this;
    }

    public ArrayNode addAll(Collection<JsonNode> nodes) {
        if (nodes.size() > 0) {
            if (this._children == null) {
                this._children = _createList(nodes.size());
            }
            this._children.addAll(nodes);
        }
        return this;
    }

    public ArrayNode insert(int index, JsonNode value) {
        if (value == null) {
            value = nullNode();
        }
        _insert(index, value);
        return this;
    }

    public JsonNode remove(int index) {
        if (index < 0 || this._children == null || index >= this._children.size()) {
            return null;
        }
        return this._children.remove(index);
    }

    public ArrayNode removeAll() {
        this._children = null;
        return this;
    }

    public ArrayNode addArray() {
        ArrayNode n = arrayNode();
        _add(n);
        return n;
    }

    public ObjectNode addObject() {
        ObjectNode n = objectNode();
        _add(n);
        return n;
    }

    public ArrayNode addPOJO(Object value) {
        if (value == null) {
            addNull();
        } else {
            _add(POJONode(value));
        }
        return this;
    }

    public ArrayNode addNull() {
        _add(nullNode());
        return this;
    }

    public ArrayNode add(int v) {
        _add(numberNode(v));
        return this;
    }

    public ArrayNode add(Integer value) {
        if (value == null) {
            return addNull();
        }
        return _add(numberNode(value.intValue()));
    }

    public ArrayNode add(long v) {
        return _add(numberNode(v));
    }

    public ArrayNode add(Long value) {
        if (value == null) {
            return addNull();
        }
        return _add(numberNode(value.longValue()));
    }

    public ArrayNode add(float v) {
        return _add(numberNode(v));
    }

    public ArrayNode add(Float value) {
        if (value == null) {
            return addNull();
        }
        return _add(numberNode(value.floatValue()));
    }

    public ArrayNode add(double v) {
        return _add(numberNode(v));
    }

    public ArrayNode add(Double value) {
        if (value == null) {
            return addNull();
        }
        return _add(numberNode(value.doubleValue()));
    }

    public ArrayNode add(BigDecimal v) {
        if (v == null) {
            return addNull();
        }
        return _add(numberNode(v));
    }

    public ArrayNode add(String v) {
        if (v == null) {
            return addNull();
        }
        return _add(textNode(v));
    }

    public ArrayNode add(boolean v) {
        return _add(booleanNode(v));
    }

    public ArrayNode add(Boolean value) {
        if (value == null) {
            return addNull();
        }
        return _add(booleanNode(value.booleanValue()));
    }

    public ArrayNode add(byte[] v) {
        if (v == null) {
            return addNull();
        }
        return _add(binaryNode(v));
    }

    public ArrayNode insertArray(int index) {
        ArrayNode n = arrayNode();
        _insert(index, n);
        return n;
    }

    public ObjectNode insertObject(int index) {
        ObjectNode n = objectNode();
        _insert(index, n);
        return n;
    }

    public ArrayNode insertPOJO(int index, Object value) {
        if (value == null) {
            return insertNull(index);
        }
        return _insert(index, POJONode(value));
    }

    public ArrayNode insertNull(int index) {
        _insert(index, nullNode());
        return this;
    }

    public ArrayNode insert(int index, int v) {
        _insert(index, numberNode(v));
        return this;
    }

    public ArrayNode insert(int index, Integer value) {
        if (value == null) {
            insertNull(index);
        } else {
            _insert(index, numberNode(value.intValue()));
        }
        return this;
    }

    public ArrayNode insert(int index, long v) {
        return _insert(index, numberNode(v));
    }

    public ArrayNode insert(int index, Long value) {
        if (value == null) {
            return insertNull(index);
        }
        return _insert(index, numberNode(value.longValue()));
    }

    public ArrayNode insert(int index, float v) {
        return _insert(index, numberNode(v));
    }

    public ArrayNode insert(int index, Float value) {
        if (value == null) {
            return insertNull(index);
        }
        return _insert(index, numberNode(value.floatValue()));
    }

    public ArrayNode insert(int index, double v) {
        return _insert(index, numberNode(v));
    }

    public ArrayNode insert(int index, Double value) {
        if (value == null) {
            return insertNull(index);
        }
        return _insert(index, numberNode(value.doubleValue()));
    }

    public ArrayNode insert(int index, BigDecimal v) {
        if (v == null) {
            return insertNull(index);
        }
        return _insert(index, numberNode(v));
    }

    public ArrayNode insert(int index, String v) {
        if (v == null) {
            return insertNull(index);
        }
        return _insert(index, textNode(v));
    }

    public ArrayNode insert(int index, boolean v) {
        return _insert(index, booleanNode(v));
    }

    public ArrayNode insert(int index, Boolean value) {
        if (value == null) {
            return insertNull(index);
        }
        return _insert(index, booleanNode(value.booleanValue()));
    }

    public ArrayNode insert(int index, byte[] v) {
        if (v == null) {
            return insertNull(index);
        }
        return _insert(index, binaryNode(v));
    }

    /* access modifiers changed from: protected */
    public void addContentsTo(List<JsonNode> dst) {
        if (this._children != null) {
            for (JsonNode n : this._children) {
                dst.add(n);
            }
        }
    }

    /* access modifiers changed from: protected */
    public List<JsonNode> _createList() {
        return new ArrayList();
    }

    /* access modifiers changed from: protected */
    public List<JsonNode> _createList(int defaultSize) {
        return new ArrayList(defaultSize);
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o.getClass() != getClass()) {
            return false;
        }
        ArrayNode other = (ArrayNode) o;
        if (this._children != null && this._children.size() != 0) {
            return other._sameChildren(this._children);
        }
        if (other.size() != 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this._children == null) {
            return 1;
        }
        int hash = this._children.size();
        for (JsonNode n : this._children) {
            if (n != null) {
                hash ^= n.hashCode();
            }
        }
        return hash;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 16);
        sb.append('[');
        if (this._children != null) {
            int len = this._children.size();
            for (int i = 0; i < len; i++) {
                if (i > 0) {
                    sb.append(',');
                }
                sb.append(this._children.get(i).toString());
            }
        }
        sb.append(']');
        return sb.toString();
    }

    public JsonNode _set(int index, JsonNode value) {
        if (this._children != null && index >= 0 && index < this._children.size()) {
            return this._children.set(index, value);
        }
        throw new IndexOutOfBoundsException("Illegal index " + index + ", array size " + size());
    }

    private ArrayNode _add(JsonNode node) {
        if (this._children == null) {
            this._children = _createList();
        }
        this._children.add(node);
        return this;
    }

    private ArrayNode _insert(int index, JsonNode node) {
        if (this._children == null) {
            this._children = _createList();
            this._children.add(node);
        } else if (index < 0) {
            this._children.add(0, node);
        } else if (index >= this._children.size()) {
            this._children.add(node);
        } else {
            this._children.add(index, node);
        }
        return this;
    }

    private boolean _sameChildren(List<JsonNode> otherChildren) {
        int len = otherChildren.size();
        if (size() != len) {
            return false;
        }
        for (int i = 0; i < len; i++) {
            if (!this._children.get(i).equals(otherChildren.get(i))) {
                return false;
            }
        }
        return true;
    }
}
