package com.fasterxml.jackson.databind.introspect;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.util.BeanUtil;
import com.fasterxml.jackson.databind.util.ClassUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class POJOPropertiesCollector {
    protected final AnnotationIntrospector _annotationIntrospector;
    protected LinkedList<AnnotatedMember> _anyGetters = null;
    protected LinkedList<AnnotatedMethod> _anySetters = null;
    protected final AnnotatedClass _classDef;
    protected final MapperConfig<?> _config;
    protected LinkedList<POJOPropertyBuilder> _creatorProperties = null;
    protected final boolean _forSerialization;
    protected HashSet<String> _ignoredPropertyNames;
    protected LinkedHashMap<Object, AnnotatedMember> _injectables;
    protected LinkedList<AnnotatedMethod> _jsonValueGetters = null;
    protected final String _mutatorPrefix;
    protected final LinkedHashMap<String, POJOPropertyBuilder> _properties = new LinkedHashMap<>();
    protected final JavaType _type;
    protected final VisibilityChecker<?> _visibilityChecker;

    protected POJOPropertiesCollector(MapperConfig<?> config, boolean forSerialization, JavaType type, AnnotatedClass classDef, String mutatorPrefix) {
        AnnotationIntrospector annotationIntrospector = null;
        this._config = config;
        this._forSerialization = forSerialization;
        this._type = type;
        this._classDef = classDef;
        this._mutatorPrefix = mutatorPrefix == null ? "set" : mutatorPrefix;
        this._annotationIntrospector = config.isAnnotationProcessingEnabled() ? this._config.getAnnotationIntrospector() : annotationIntrospector;
        if (this._annotationIntrospector == null) {
            this._visibilityChecker = this._config.getDefaultVisibilityChecker();
        } else {
            this._visibilityChecker = this._annotationIntrospector.findAutoDetectVisibility(classDef, this._config.getDefaultVisibilityChecker());
        }
    }

    public MapperConfig<?> getConfig() {
        return this._config;
    }

    public JavaType getType() {
        return this._type;
    }

    public AnnotatedClass getClassDef() {
        return this._classDef;
    }

    public AnnotationIntrospector getAnnotationIntrospector() {
        return this._annotationIntrospector;
    }

    public List<BeanPropertyDefinition> getProperties() {
        return new ArrayList(this._properties.values());
    }

    public Map<Object, AnnotatedMember> getInjectables() {
        return this._injectables;
    }

    public AnnotatedMethod getJsonValueMethod() {
        if (this._jsonValueGetters == null) {
            return null;
        }
        if (this._jsonValueGetters.size() > 1) {
            reportProblem("Multiple value properties defined (" + this._jsonValueGetters.get(0) + " vs " + this._jsonValueGetters.get(1) + ")");
        }
        return this._jsonValueGetters.get(0);
    }

    public AnnotatedMember getAnyGetter() {
        if (this._anyGetters == null) {
            return null;
        }
        if (this._anyGetters.size() > 1) {
            reportProblem("Multiple 'any-getters' defined (" + this._anyGetters.get(0) + " vs " + this._anyGetters.get(1) + ")");
        }
        return this._anyGetters.getFirst();
    }

    public AnnotatedMethod getAnySetterMethod() {
        if (this._anySetters == null) {
            return null;
        }
        if (this._anySetters.size() > 1) {
            reportProblem("Multiple 'any-setters' defined (" + this._anySetters.get(0) + " vs " + this._anySetters.get(1) + ")");
        }
        return this._anySetters.getFirst();
    }

    public Set<String> getIgnoredPropertyNames() {
        return this._ignoredPropertyNames;
    }

    public ObjectIdInfo getObjectIdInfo() {
        if (this._annotationIntrospector == null) {
            return null;
        }
        ObjectIdInfo info = this._annotationIntrospector.findObjectIdInfo(this._classDef);
        if (info != null) {
            return this._annotationIntrospector.findObjectReferenceInfo(this._classDef, info);
        }
        return info;
    }

    public Class<?> findPOJOBuilderClass() {
        return this._annotationIntrospector.findPOJOBuilder(this._classDef);
    }

    /* access modifiers changed from: protected */
    public Map<String, POJOPropertyBuilder> getPropertyMap() {
        return this._properties;
    }

    public POJOPropertiesCollector collect() {
        this._properties.clear();
        _addFields();
        _addMethods();
        _addCreators();
        _addInjectables();
        _removeUnwantedProperties();
        _renameProperties();
        PropertyNamingStrategy naming = _findNamingStrategy();
        if (naming != null) {
            _renameUsing(naming);
        }
        for (POJOPropertyBuilder property : this._properties.values()) {
            property.trimByVisibility();
        }
        for (POJOPropertyBuilder property2 : this._properties.values()) {
            property2.mergeAnnotations(this._forSerialization);
        }
        if (this._config.isEnabled(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME)) {
            _renameWithWrappers();
        }
        _sortProperties();
        return this;
    }

    /* access modifiers changed from: protected */
    public void _sortProperties() {
        boolean sort;
        Map<String, POJOPropertyBuilder> all;
        String[] propertyOrder = null;
        AnnotationIntrospector intr = this._annotationIntrospector;
        Boolean alpha = intr == null ? null : intr.findSerializationSortAlphabetically(this._classDef);
        if (alpha == null) {
            sort = this._config.shouldSortPropertiesAlphabetically();
        } else {
            sort = alpha.booleanValue();
        }
        if (intr != null) {
            propertyOrder = intr.findSerializationPropertyOrder(this._classDef);
        }
        if (sort || this._creatorProperties != null || propertyOrder != null) {
            int size = this._properties.size();
            if (sort) {
                all = new TreeMap<>();
            } else {
                all = new LinkedHashMap<>(size + size);
            }
            for (POJOPropertyBuilder prop : this._properties.values()) {
                all.put(prop.getName(), prop);
            }
            Map<String, POJOPropertyBuilder> ordered = new LinkedHashMap<>(size + size);
            if (propertyOrder != null) {
                for (String name : propertyOrder) {
                    POJOPropertyBuilder w = (POJOPropertyBuilder) all.get(name);
                    if (w == null) {
                        Iterator i$ = this._properties.values().iterator();
                        while (true) {
                            if (!i$.hasNext()) {
                                break;
                            }
                            POJOPropertyBuilder prop2 = i$.next();
                            if (name.equals(prop2.getInternalName())) {
                                w = prop2;
                                name = prop2.getName();
                                break;
                            }
                        }
                    }
                    if (w != null) {
                        ordered.put(name, w);
                    }
                }
            }
            if (this._creatorProperties != null) {
                Iterator i$2 = this._creatorProperties.iterator();
                while (i$2.hasNext()) {
                    POJOPropertyBuilder prop3 = i$2.next();
                    ordered.put(prop3.getName(), prop3);
                }
            }
            ordered.putAll(all);
            this._properties.clear();
            this._properties.putAll(ordered);
        }
    }

    /* access modifiers changed from: protected */
    public void _addFields() {
        String explName;
        boolean visible;
        boolean ignored;
        AnnotationIntrospector ai = this._annotationIntrospector;
        for (AnnotatedField f : this._classDef.fields()) {
            String implName = f.getName();
            if (ai == null) {
                explName = null;
            } else if (this._forSerialization) {
                PropertyName pn = ai.findNameForSerialization(f);
                explName = pn == null ? null : pn.getSimpleName();
            } else {
                PropertyName pn2 = ai.findNameForDeserialization(f);
                explName = pn2 == null ? null : pn2.getSimpleName();
            }
            if ("".equals(explName)) {
                explName = implName;
            }
            if (explName != null) {
                visible = true;
            } else {
                visible = false;
            }
            if (!visible) {
                visible = this._visibilityChecker.isFieldVisible(f);
            }
            if (ai == null || !ai.hasIgnoreMarker(f)) {
                ignored = false;
            } else {
                ignored = true;
            }
            _property(implName).addField(f, explName, visible, ignored);
        }
    }

    /* access modifiers changed from: protected */
    public void _addCreators() {
        AnnotationIntrospector ai = this._annotationIntrospector;
        if (ai != null) {
            for (AnnotatedConstructor ctor : this._classDef.getConstructors()) {
                if (this._creatorProperties == null) {
                    this._creatorProperties = new LinkedList<>();
                }
                int len = ctor.getParameterCount();
                for (int i = 0; i < len; i++) {
                    AnnotatedParameter param = ctor.getParameter(i);
                    PropertyName pn = ai.findNameForDeserialization(param);
                    String name = pn == null ? null : pn.getSimpleName();
                    if (name != null) {
                        POJOPropertyBuilder prop = _property(name);
                        prop.addCtor(param, name, true, false);
                        this._creatorProperties.add(prop);
                    }
                }
            }
            for (AnnotatedMethod factory : this._classDef.getStaticMethods()) {
                if (this._creatorProperties == null) {
                    this._creatorProperties = new LinkedList<>();
                }
                int len2 = factory.getParameterCount();
                for (int i2 = 0; i2 < len2; i2++) {
                    AnnotatedParameter param2 = factory.getParameter(i2);
                    PropertyName pn2 = ai.findNameForDeserialization(param2);
                    String name2 = pn2 == null ? null : pn2.getSimpleName();
                    if (name2 != null) {
                        POJOPropertyBuilder prop2 = _property(name2);
                        prop2.addCtor(param2, name2, true, false);
                        this._creatorProperties.add(prop2);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addMethods() {
        AnnotationIntrospector ai = this._annotationIntrospector;
        for (AnnotatedMethod m : this._classDef.memberMethods()) {
            int argCount = m.getParameterCount();
            if (argCount == 0) {
                _addGetterMethod(m, ai);
            } else if (argCount == 1) {
                _addSetterMethod(m, ai);
            } else if (argCount == 2 && ai != null && ai.hasAnySetterAnnotation(m)) {
                if (this._anySetters == null) {
                    this._anySetters = new LinkedList<>();
                }
                this._anySetters.add(m);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addGetterMethod(AnnotatedMethod m, AnnotationIntrospector ai) {
        String implName;
        boolean visible;
        String explName = null;
        if (ai != null) {
            if (ai.hasAnyGetterAnnotation(m)) {
                if (this._anyGetters == null) {
                    this._anyGetters = new LinkedList<>();
                }
                this._anyGetters.add(m);
                return;
            } else if (ai.hasAsValueAnnotation(m)) {
                if (this._jsonValueGetters == null) {
                    this._jsonValueGetters = new LinkedList<>();
                }
                this._jsonValueGetters.add(m);
                return;
            }
        }
        PropertyName pn = ai == null ? null : ai.findNameForSerialization(m);
        if (pn != null) {
            explName = pn.getSimpleName();
        }
        if (explName == null) {
            implName = BeanUtil.okNameForRegularGetter(m, m.getName());
            if (implName == null) {
                implName = BeanUtil.okNameForIsGetter(m, m.getName());
                if (implName != null) {
                    visible = this._visibilityChecker.isIsGetterVisible(m);
                } else {
                    return;
                }
            } else {
                visible = this._visibilityChecker.isGetterVisible(m);
            }
        } else {
            implName = BeanUtil.okNameForGetter(m);
            if (implName == null) {
                implName = m.getName();
            }
            if (explName.length() == 0) {
                explName = implName;
            }
            visible = true;
        }
        _property(implName).addGetter(m, explName, visible, ai == null ? false : ai.hasIgnoreMarker(m));
    }

    /* access modifiers changed from: protected */
    public void _addSetterMethod(AnnotatedMethod m, AnnotationIntrospector ai) {
        String implName;
        boolean visible;
        String explName = null;
        PropertyName pn = ai == null ? null : ai.findNameForDeserialization(m);
        if (pn != null) {
            explName = pn.getSimpleName();
        }
        if (explName == null) {
            implName = BeanUtil.okNameForMutator(m, this._mutatorPrefix);
            if (implName != null) {
                visible = this._visibilityChecker.isSetterVisible(m);
            } else {
                return;
            }
        } else {
            implName = BeanUtil.okNameForMutator(m, this._mutatorPrefix);
            if (implName == null) {
                implName = m.getName();
            }
            if (explName.length() == 0) {
                explName = implName;
            }
            visible = true;
        }
        _property(implName).addSetter(m, explName, visible, ai == null ? false : ai.hasIgnoreMarker(m));
    }

    /* access modifiers changed from: protected */
    public void _addInjectables() {
        AnnotationIntrospector ai = this._annotationIntrospector;
        if (ai != null) {
            for (AnnotatedField f : this._classDef.fields()) {
                _doAddInjectable(ai.findInjectableValueId(f), f);
            }
            for (AnnotatedMethod m : this._classDef.memberMethods()) {
                if (m.getParameterCount() == 1) {
                    _doAddInjectable(ai.findInjectableValueId(m), m);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _doAddInjectable(Object id, AnnotatedMember m) {
        if (id != null) {
            if (this._injectables == null) {
                this._injectables = new LinkedHashMap<>();
            }
            if (this._injectables.put(id, m) != null) {
                throw new IllegalArgumentException("Duplicate injectable value with id '" + String.valueOf(id) + "' (of type " + (id == null ? "[null]" : id.getClass().getName()) + ")");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _removeUnwantedProperties() {
        Iterator<Map.Entry<String, POJOPropertyBuilder>> it = this._properties.entrySet().iterator();
        while (it.hasNext()) {
            POJOPropertyBuilder prop = (POJOPropertyBuilder) it.next().getValue();
            if (!prop.anyVisible()) {
                it.remove();
            } else {
                if (prop.anyIgnorals()) {
                    if (!prop.isExplicitlyIncluded()) {
                        it.remove();
                        _addIgnored(prop.getName());
                    } else {
                        prop.removeIgnored();
                        if (!this._forSerialization && !prop.couldDeserialize()) {
                            _addIgnored(prop.getName());
                        }
                    }
                }
                prop.removeNonVisible();
            }
        }
    }

    private void _addIgnored(String name) {
        if (!this._forSerialization) {
            if (this._ignoredPropertyNames == null) {
                this._ignoredPropertyNames = new HashSet<>();
            }
            this._ignoredPropertyNames.add(name);
        }
    }

    /* access modifiers changed from: protected */
    public void _renameProperties() {
        Iterator<Map.Entry<String, POJOPropertyBuilder>> it = this._properties.entrySet().iterator();
        LinkedList<POJOPropertyBuilder> renamed = null;
        while (it.hasNext()) {
            POJOPropertyBuilder prop = (POJOPropertyBuilder) it.next().getValue();
            String newName = prop.findNewName();
            if (newName != null) {
                if (renamed == null) {
                    renamed = new LinkedList<>();
                }
                renamed.add(prop.withName(newName));
                it.remove();
            }
        }
        if (renamed != null) {
            Iterator i$ = renamed.iterator();
            while (i$.hasNext()) {
                POJOPropertyBuilder prop2 = (POJOPropertyBuilder) i$.next();
                String name = prop2.getName();
                POJOPropertyBuilder old = this._properties.get(name);
                if (old == null) {
                    this._properties.put(name, prop2);
                } else {
                    old.addAll(prop2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _renameUsing(PropertyNamingStrategy naming) {
        this._properties.clear();
        for (POJOPropertyBuilder prop : (POJOPropertyBuilder[]) this._properties.values().toArray(new POJOPropertyBuilder[this._properties.size()])) {
            String name = prop.getName();
            if (this._forSerialization) {
                if (prop.hasGetter()) {
                    name = naming.nameForGetterMethod(this._config, prop.getGetter(), name);
                } else if (prop.hasField()) {
                    name = naming.nameForField(this._config, prop.getField(), name);
                }
            } else if (prop.hasSetter()) {
                name = naming.nameForSetterMethod(this._config, prop.getSetter(), name);
            } else if (prop.hasConstructorParameter()) {
                name = naming.nameForConstructorParameter(this._config, prop.getConstructorParameter(), name);
            } else if (prop.hasField()) {
                name = naming.nameForField(this._config, prop.getField(), name);
            } else if (prop.hasGetter()) {
                name = naming.nameForGetterMethod(this._config, prop.getGetter(), name);
            }
            if (!name.equals(prop.getName())) {
                prop = prop.withName(name);
            }
            POJOPropertyBuilder old = this._properties.get(name);
            if (old == null) {
                this._properties.put(name, prop);
            } else {
                old.addAll(prop);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _renameWithWrappers() {
        PropertyName wrapperName;
        Iterator<Map.Entry<String, POJOPropertyBuilder>> it = this._properties.entrySet().iterator();
        LinkedList<POJOPropertyBuilder> renamed = null;
        while (it.hasNext()) {
            POJOPropertyBuilder prop = (POJOPropertyBuilder) it.next().getValue();
            AnnotatedMember member = prop.getPrimaryMember();
            if (!(member == null || (wrapperName = this._annotationIntrospector.findWrapperName(member)) == null || !wrapperName.hasSimpleName())) {
                String name = wrapperName.getSimpleName();
                if (!name.equals(prop.getName())) {
                    if (renamed == null) {
                        renamed = new LinkedList<>();
                    }
                    renamed.add(prop.withName(name));
                    it.remove();
                }
            }
        }
        if (renamed != null) {
            Iterator i$ = renamed.iterator();
            while (i$.hasNext()) {
                POJOPropertyBuilder prop2 = (POJOPropertyBuilder) i$.next();
                String name2 = prop2.getName();
                POJOPropertyBuilder old = this._properties.get(name2);
                if (old == null) {
                    this._properties.put(name2, prop2);
                } else {
                    old.addAll(prop2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void reportProblem(String msg) {
        throw new IllegalArgumentException("Problem with definition of " + this._classDef + ": " + msg);
    }

    /* access modifiers changed from: protected */
    public POJOPropertyBuilder _property(String implName) {
        POJOPropertyBuilder prop = this._properties.get(implName);
        if (prop != null) {
            return prop;
        }
        POJOPropertyBuilder prop2 = new POJOPropertyBuilder(implName, this._annotationIntrospector, this._forSerialization);
        this._properties.put(implName, prop2);
        return prop2;
    }

    private PropertyNamingStrategy _findNamingStrategy() {
        Object namingDef = this._annotationIntrospector == null ? null : this._annotationIntrospector.findNamingStrategy(this._classDef);
        if (namingDef == null) {
            return this._config.getPropertyNamingStrategy();
        }
        if (namingDef instanceof PropertyNamingStrategy) {
            return (PropertyNamingStrategy) namingDef;
        }
        if (!(namingDef instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned PropertyNamingStrategy definition of type " + namingDef.getClass().getName() + "; expected type PropertyNamingStrategy or Class<PropertyNamingStrategy> instead");
        }
        Class<?> namingClass = (Class) namingDef;
        if (!PropertyNamingStrategy.class.isAssignableFrom(namingClass)) {
            throw new IllegalStateException("AnnotationIntrospector returned Class " + namingClass.getName() + "; expected Class<PropertyNamingStrategy>");
        }
        HandlerInstantiator hi = this._config.getHandlerInstantiator();
        if (hi != null) {
            return hi.namingStrategyInstance(this._config, this._classDef, namingClass);
        }
        return (PropertyNamingStrategy) ClassUtil.createInstance(namingClass, this._config.canOverrideAccessModifiers());
    }
}
