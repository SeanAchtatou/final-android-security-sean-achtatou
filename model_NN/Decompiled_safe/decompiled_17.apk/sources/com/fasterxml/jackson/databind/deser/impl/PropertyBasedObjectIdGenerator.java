package com.fasterxml.jackson.databind.deser.impl;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

public class PropertyBasedObjectIdGenerator extends ObjectIdGenerators.PropertyGenerator {
    public PropertyBasedObjectIdGenerator(Class<?> scope) {
        super(scope);
    }

    public Object generateId(Object forPojo) {
        throw new UnsupportedOperationException();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public ObjectIdGenerator<Object> forScope(Class<?> scope) {
        return scope == this._scope ? this : new PropertyBasedObjectIdGenerator(scope);
    }

    public ObjectIdGenerator<Object> newForSerialization(Object context) {
        return this;
    }

    public ObjectIdGenerator.IdKey key(Object key) {
        return new ObjectIdGenerator.IdKey(getClass(), this._scope, key);
    }
}
