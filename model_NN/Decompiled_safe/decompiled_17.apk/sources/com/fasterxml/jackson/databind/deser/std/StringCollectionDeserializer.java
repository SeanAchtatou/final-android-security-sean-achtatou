package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import java.io.IOException;
import java.util.Collection;

@JacksonStdImpl
public final class StringCollectionDeserializer extends ContainerDeserializerBase<Collection<String>> implements ContextualDeserializer {
    private static final long serialVersionUID = 1;
    protected final JavaType _collectionType;
    protected final JsonDeserializer<Object> _delegateDeserializer;
    protected final JsonDeserializer<String> _valueDeserializer;
    protected final ValueInstantiator _valueInstantiator;

    public /* bridge */ /* synthetic */ Object deserialize(JsonParser x0, DeserializationContext x1, Object x2) throws IOException, JsonProcessingException {
        return deserialize(x0, x1, (Collection<String>) ((Collection) x2));
    }

    public StringCollectionDeserializer(JavaType collectionType, JsonDeserializer<?> valueDeser, ValueInstantiator valueInstantiator) {
        this(collectionType, valueInstantiator, null, valueDeser);
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [com.fasterxml.jackson.databind.JsonDeserializer<?>, com.fasterxml.jackson.databind.JsonDeserializer<java.lang.String>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected StringCollectionDeserializer(com.fasterxml.jackson.databind.JavaType r2, com.fasterxml.jackson.databind.deser.ValueInstantiator r3, com.fasterxml.jackson.databind.JsonDeserializer<?> r4, com.fasterxml.jackson.databind.JsonDeserializer<?> r5) {
        /*
            r1 = this;
            java.lang.Class r0 = r2.getRawClass()
            r1.<init>(r0)
            r1._collectionType = r2
            r1._valueDeserializer = r5
            r1._valueInstantiator = r3
            r1._delegateDeserializer = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.deser.std.StringCollectionDeserializer.<init>(com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.deser.ValueInstantiator, com.fasterxml.jackson.databind.JsonDeserializer, com.fasterxml.jackson.databind.JsonDeserializer):void");
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: protected */
    public StringCollectionDeserializer withResolved(JsonDeserializer<?> delegateDeser, JsonDeserializer<?> valueDeser) {
        return (this._valueDeserializer == valueDeser && this._delegateDeserializer == delegateDeser) ? this : new StringCollectionDeserializer(this._collectionType, this._valueInstantiator, delegateDeser, valueDeser);
    }

    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        JsonDeserializer<Object> delegate = null;
        if (!(this._valueInstantiator == null || this._valueInstantiator.getDelegateCreator() == null)) {
            delegate = findDeserializer(ctxt, this._valueInstantiator.getDelegateType(ctxt.getConfig()), property);
        }
        JsonDeserializer<?> valueDeser = this._valueDeserializer;
        if (valueDeser == null) {
            valueDeser = ctxt.findContextualValueDeserializer(this._collectionType.getContentType(), property);
        } else if (valueDeser instanceof ContextualDeserializer) {
            valueDeser = ((ContextualDeserializer) valueDeser).createContextual(ctxt, property);
        }
        if (isDefaultDeserializer(valueDeser)) {
            valueDeser = null;
        }
        return withResolved(delegate, valueDeser);
    }

    public JavaType getContentType() {
        return this._collectionType.getContentType();
    }

    public JsonDeserializer<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    public Collection<String> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (this._delegateDeserializer != null) {
            return (Collection) this._valueInstantiator.createUsingDelegate(ctxt, this._delegateDeserializer.deserialize(jp, ctxt));
        }
        return deserialize(jp, ctxt, (Collection) this._valueInstantiator.createUsingDefault(ctxt));
    }

    public Collection<String> deserialize(JsonParser jp, DeserializationContext ctxt, Collection<String> result) throws IOException, JsonProcessingException {
        if (!jp.isExpectedStartArrayToken()) {
            return handleNonArray(jp, ctxt, result);
        }
        if (this._valueDeserializer != null) {
            return deserializeUsingCustom(jp, ctxt, result, this._valueDeserializer);
        }
        while (true) {
            JsonToken t = jp.nextToken();
            if (t == JsonToken.END_ARRAY) {
                return result;
            }
            result.add(t == JsonToken.VALUE_NULL ? null : _parseString(jp, ctxt));
        }
    }

    private Collection<String> deserializeUsingCustom(JsonParser jp, DeserializationContext ctxt, Collection<String> result, JsonDeserializer<String> deser) throws IOException, JsonProcessingException {
        String value;
        while (true) {
            JsonToken t = jp.nextToken();
            if (t == JsonToken.END_ARRAY) {
                return result;
            }
            if (t == JsonToken.VALUE_NULL) {
                value = null;
            } else {
                value = deser.deserialize(jp, ctxt);
            }
            result.add(value);
        }
    }

    public Object deserializeWithType(JsonParser jp, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException, JsonProcessingException {
        return typeDeserializer.deserializeTypedFromArray(jp, ctxt);
    }

    private final Collection<String> handleNonArray(JsonParser jp, DeserializationContext ctxt, Collection<String> result) throws IOException, JsonProcessingException {
        String value;
        if (!ctxt.isEnabled(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)) {
            throw ctxt.mappingException(this._collectionType.getRawClass());
        }
        JsonDeserializer<String> valueDes = this._valueDeserializer;
        if (jp.getCurrentToken() == JsonToken.VALUE_NULL) {
            value = null;
        } else {
            value = valueDes == null ? _parseString(jp, ctxt) : valueDes.deserialize(jp, ctxt);
        }
        result.add(value);
        return result;
    }
}
