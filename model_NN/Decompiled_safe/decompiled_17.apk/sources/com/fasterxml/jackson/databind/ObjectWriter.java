package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.FormatSchema;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.Versioned;
import com.fasterxml.jackson.core.io.SegmentedStringWriter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.ByteArrayBuilder;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.core.util.Instantiatable;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.fasterxml.jackson.databind.cfg.DatabindVersion;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.Writer;
import java.text.DateFormat;
import java.util.Locale;
import java.util.TimeZone;

public class ObjectWriter implements Versioned, Serializable {
    protected static final PrettyPrinter NULL_PRETTY_PRINTER = new MinimalPrettyPrinter();
    private static final long serialVersionUID = -7024829992408267532L;
    protected final SerializationConfig _config;
    protected final JsonFactory _jsonFactory;
    protected final PrettyPrinter _prettyPrinter;
    protected final JsonSerializer<Object> _rootSerializer;
    protected final JavaType _rootType;
    protected final FormatSchema _schema;
    protected final SerializerFactory _serializerFactory;
    protected final DefaultSerializerProvider _serializerProvider;

    protected ObjectWriter(ObjectMapper mapper, SerializationConfig config, JavaType rootType, PrettyPrinter pp) {
        this._config = config;
        this._serializerProvider = mapper._serializerProvider;
        this._serializerFactory = mapper._serializerFactory;
        this._jsonFactory = mapper._jsonFactory;
        this._rootType = rootType;
        this._prettyPrinter = pp;
        this._schema = null;
        this._rootSerializer = _prefetchRootSerializer(config, rootType);
    }

    protected ObjectWriter(ObjectMapper mapper, SerializationConfig config) {
        this._config = config;
        this._serializerProvider = mapper._serializerProvider;
        this._serializerFactory = mapper._serializerFactory;
        this._jsonFactory = mapper._jsonFactory;
        this._rootType = null;
        this._rootSerializer = null;
        this._prettyPrinter = null;
        this._schema = null;
    }

    protected ObjectWriter(ObjectMapper mapper, SerializationConfig config, FormatSchema s) {
        this._config = config;
        this._serializerProvider = mapper._serializerProvider;
        this._serializerFactory = mapper._serializerFactory;
        this._jsonFactory = mapper._jsonFactory;
        this._rootType = null;
        this._rootSerializer = null;
        this._prettyPrinter = null;
        this._schema = s;
    }

    protected ObjectWriter(ObjectWriter base, SerializationConfig config, JavaType rootType, JsonSerializer<Object> rootSer, PrettyPrinter pp, FormatSchema s) {
        this._config = config;
        this._serializerProvider = base._serializerProvider;
        this._serializerFactory = base._serializerFactory;
        this._jsonFactory = base._jsonFactory;
        this._rootType = rootType;
        this._rootSerializer = rootSer;
        this._prettyPrinter = pp;
        this._schema = s;
    }

    protected ObjectWriter(ObjectWriter base, SerializationConfig config) {
        this._config = config;
        this._serializerProvider = base._serializerProvider;
        this._serializerFactory = base._serializerFactory;
        this._jsonFactory = base._jsonFactory;
        this._schema = base._schema;
        this._rootType = base._rootType;
        this._rootSerializer = base._rootSerializer;
        this._prettyPrinter = base._prettyPrinter;
    }

    public Version version() {
        return DatabindVersion.instance.version();
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter with(SerializationFeature feature) {
        SerializationConfig newConfig = this._config.with(feature);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter with(SerializationFeature first, SerializationFeature... other) {
        SerializationConfig newConfig = this._config.with(first, other);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter withFeatures(SerializationFeature... features) {
        SerializationConfig newConfig = this._config.withFeatures(features);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter without(SerializationFeature feature) {
        SerializationConfig newConfig = this._config.without(feature);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter without(SerializationFeature first, SerializationFeature... other) {
        SerializationConfig newConfig = this._config.without(first, other);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter withoutFeatures(SerializationFeature... features) {
        SerializationConfig newConfig = this._config.withoutFeatures(features);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter with(DateFormat df) {
        SerializationConfig newConfig = this._config.with(df);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    public ObjectWriter withDefaultPrettyPrinter() {
        return with(new DefaultPrettyPrinter());
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter with(FilterProvider filterProvider) {
        return filterProvider == this._config.getFilterProvider() ? this : new ObjectWriter(this, this._config.withFilters(filterProvider));
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public ObjectWriter with(PrettyPrinter pp) {
        if (pp == this._prettyPrinter) {
            return this;
        }
        if (pp == null) {
            pp = NULL_PRETTY_PRINTER;
        }
        return new ObjectWriter(this, this._config, this._rootType, this._rootSerializer, pp, this._schema);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter withRootName(String rootName) {
        SerializationConfig newConfig = this._config.withRootName(rootName);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public ObjectWriter withSchema(FormatSchema schema) {
        return this._schema == schema ? this : new ObjectWriter(this, this._config, this._rootType, this._rootSerializer, this._prettyPrinter, schema);
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public ObjectWriter withType(JavaType rootType) {
        JsonSerializer<Object> rootSer = _prefetchRootSerializer(this._config, rootType);
        if (rootType == this._rootType) {
            return this;
        }
        return new ObjectWriter(this, this._config, rootType, rootSer, this._prettyPrinter, this._schema);
    }

    public ObjectWriter withType(Class<?> rootType) {
        return withType(this._config.constructType(rootType));
    }

    public ObjectWriter withType(TypeReference<?> rootType) {
        return withType(this._config.getTypeFactory().constructType(rootType.getType()));
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter withView(Class<?> view) {
        SerializationConfig newConfig = this._config.withView(view);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter with(Locale l) {
        SerializationConfig newConfig = this._config.with(l);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter with(TimeZone tz) {
        SerializationConfig newConfig = this._config.with(tz);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public ObjectWriter with(Base64Variant b64variant) {
        SerializationConfig newConfig = this._config.with(b64variant);
        return newConfig == this._config ? this : new ObjectWriter(this, newConfig);
    }

    public boolean isEnabled(SerializationFeature f) {
        return this._config.isEnabled(f);
    }

    public boolean isEnabled(MapperFeature f) {
        return this._config.isEnabled(f);
    }

    public boolean isEnabled(JsonParser.Feature f) {
        return this._jsonFactory.isEnabled(f);
    }

    public JsonFactory getJsonFactory() {
        return this._jsonFactory;
    }

    public TypeFactory getTypeFactory() {
        return this._config.getTypeFactory();
    }

    public void writeValue(JsonGenerator jgen, Object value) throws IOException, JsonGenerationException, JsonMappingException {
        _configureJsonGenerator(jgen);
        if (!this._config.isEnabled(SerializationFeature.CLOSE_CLOSEABLE) || !(value instanceof Closeable)) {
            if (this._rootType == null) {
                _serializerProvider(this._config).serializeValue(jgen, value);
            } else {
                _serializerProvider(this._config).serializeValue(jgen, value, this._rootType, this._rootSerializer);
            }
            if (this._config.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
                jgen.flush();
                return;
            }
            return;
        }
        _writeCloseableValue(jgen, value, this._config);
    }

    public void writeValue(File resultFile, Object value) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(resultFile, JsonEncoding.UTF8), value);
    }

    public void writeValue(OutputStream out, Object value) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(out, JsonEncoding.UTF8), value);
    }

    public void writeValue(Writer w, Object value) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(w), value);
    }

    public String writeValueAsString(Object value) throws JsonProcessingException {
        SegmentedStringWriter sw = new SegmentedStringWriter(this._jsonFactory._getBufferRecycler());
        try {
            _configAndWriteValue(this._jsonFactory.createJsonGenerator(sw), value);
            return sw.getAndClear();
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw JsonMappingException.fromUnexpectedIOE(e2);
        }
    }

    public byte[] writeValueAsBytes(Object value) throws JsonProcessingException {
        ByteArrayBuilder bb = new ByteArrayBuilder(this._jsonFactory._getBufferRecycler());
        try {
            _configAndWriteValue(this._jsonFactory.createJsonGenerator(bb, JsonEncoding.UTF8), value);
            byte[] result = bb.toByteArray();
            bb.release();
            return result;
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw JsonMappingException.fromUnexpectedIOE(e2);
        }
    }

    public boolean canSerialize(Class<?> type) {
        return _serializerProvider(this._config).hasSerializerFor(type);
    }

    /* access modifiers changed from: protected */
    public DefaultSerializerProvider _serializerProvider(SerializationConfig config) {
        return this._serializerProvider.createInstance(config, this._serializerFactory);
    }

    /* access modifiers changed from: protected */
    public final void _configAndWriteValue(JsonGenerator jgen, Object value) throws IOException, JsonGenerationException, JsonMappingException {
        _configureJsonGenerator(jgen);
        if (!this._config.isEnabled(SerializationFeature.CLOSE_CLOSEABLE) || !(value instanceof Closeable)) {
            boolean closed = false;
            try {
                if (this._rootType == null) {
                    _serializerProvider(this._config).serializeValue(jgen, value);
                } else {
                    _serializerProvider(this._config).serializeValue(jgen, value, this._rootType, this._rootSerializer);
                }
                closed = true;
                jgen.close();
            } finally {
                if (!closed) {
                    try {
                        jgen.close();
                    } catch (IOException e) {
                    }
                }
            }
        } else {
            _writeCloseable(jgen, value, this._config);
        }
    }

    private final void _writeCloseable(JsonGenerator jgen, Object value, SerializationConfig cfg) throws IOException, JsonGenerationException, JsonMappingException {
        Closeable toClose = (Closeable) value;
        try {
            if (this._rootType == null) {
                _serializerProvider(cfg).serializeValue(jgen, value);
            } else {
                _serializerProvider(cfg).serializeValue(jgen, value, this._rootType, this._rootSerializer);
            }
            JsonGenerator tmpJgen = jgen;
            jgen = null;
            tmpJgen.close();
            Closeable tmpToClose = toClose;
            toClose = null;
            tmpToClose.close();
        } finally {
            if (jgen != null) {
                try {
                    jgen.close();
                } catch (IOException e) {
                }
            }
            if (toClose != null) {
                try {
                    toClose.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    private final void _writeCloseableValue(JsonGenerator jgen, Object value, SerializationConfig cfg) throws IOException, JsonGenerationException, JsonMappingException {
        Closeable toClose = (Closeable) value;
        try {
            if (this._rootType == null) {
                _serializerProvider(cfg).serializeValue(jgen, value);
            } else {
                _serializerProvider(cfg).serializeValue(jgen, value, this._rootType, this._rootSerializer);
            }
            if (this._config.isEnabled(SerializationFeature.FLUSH_AFTER_WRITE_VALUE)) {
                jgen.flush();
            }
            Closeable tmpToClose = toClose;
            toClose = null;
            tmpToClose.close();
        } finally {
            if (toClose != null) {
                try {
                    toClose.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(com.fasterxml.jackson.databind.JavaType, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object>
     arg types: [com.fasterxml.jackson.databind.JavaType, int, ?[OBJECT, ARRAY]]
     candidates:
      com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object>
      com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(com.fasterxml.jackson.databind.JavaType, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object> */
    /* access modifiers changed from: protected */
    public final JsonSerializer<Object> _prefetchRootSerializer(SerializationConfig config, JavaType valueType) {
        if (valueType == null || !this._config.isEnabled(SerializationFeature.EAGER_SERIALIZER_FETCH)) {
            return null;
        }
        try {
            return _serializerProvider(config).findTypedValueSerializer(valueType, true, (BeanProperty) null);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    private final void _configureJsonGenerator(JsonGenerator jgen) {
        if (this._prettyPrinter != null) {
            PrettyPrinter pp = this._prettyPrinter;
            if (pp == NULL_PRETTY_PRINTER) {
                jgen.setPrettyPrinter(null);
            } else {
                if (pp instanceof Instantiatable) {
                    pp = (PrettyPrinter) ((Instantiatable) pp).createInstance();
                }
                jgen.setPrettyPrinter(pp);
            }
        } else if (this._config.isEnabled(SerializationFeature.INDENT_OUTPUT)) {
            jgen.useDefaultPrettyPrinter();
        }
        if (this._schema != null) {
            jgen.setSchema(this._schema);
        }
    }
}
