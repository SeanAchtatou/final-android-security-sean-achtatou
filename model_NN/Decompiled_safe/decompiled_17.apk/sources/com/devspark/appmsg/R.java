package com.devspark.appmsg;

public final class R {

    public static final class color {
        public static final int alert = 2131099654;
        public static final int confirm = 2131099655;
        public static final int info = 2131099656;
    }

    public static final class layout {
        public static final int app_msg = 2130903043;
    }
}
