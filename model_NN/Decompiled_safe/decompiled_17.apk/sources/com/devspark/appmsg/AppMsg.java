package com.devspark.appmsg;

import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.TextView;

public class AppMsg {
    public static final int LENGTH_LONG = 5000;
    public static final int LENGTH_SHORT = 3000;
    public static final Style STYLE_ALERT = new Style(LENGTH_LONG, R.color.alert);
    public static final Style STYLE_CONFIRM = new Style(LENGTH_SHORT, R.color.confirm);
    public static final Style STYLE_INFO = new Style(LENGTH_SHORT, R.color.info);
    private final Activity mContext;
    private int mDuration = LENGTH_SHORT;
    private boolean mFloating;
    private Animation mInAnimation;
    private ViewGroup.LayoutParams mLayoutParams;
    private Animation mOutAnimation;
    private View mView;

    public AppMsg(Activity context) {
        this.mContext = context;
    }

    public static AppMsg makeText(Activity context, CharSequence text, Style style) {
        return makeText(context, text, style, R.layout.app_msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.devspark.appmsg.AppMsg.makeText(android.app.Activity, java.lang.CharSequence, com.devspark.appmsg.AppMsg$Style, android.view.View, boolean):com.devspark.appmsg.AppMsg
     arg types: [android.app.Activity, java.lang.CharSequence, com.devspark.appmsg.AppMsg$Style, android.view.View, int]
     candidates:
      com.devspark.appmsg.AppMsg.makeText(android.app.Activity, int, com.devspark.appmsg.AppMsg$Style, android.view.View, boolean):com.devspark.appmsg.AppMsg
      com.devspark.appmsg.AppMsg.makeText(android.app.Activity, java.lang.CharSequence, com.devspark.appmsg.AppMsg$Style, android.view.View, boolean):com.devspark.appmsg.AppMsg */
    public static AppMsg makeText(Activity context, CharSequence text, Style style, int layoutId) {
        return makeText(context, text, style, ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(layoutId, (ViewGroup) null), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.devspark.appmsg.AppMsg.makeText(android.app.Activity, java.lang.CharSequence, com.devspark.appmsg.AppMsg$Style, android.view.View, boolean):com.devspark.appmsg.AppMsg
     arg types: [android.app.Activity, java.lang.CharSequence, com.devspark.appmsg.AppMsg$Style, android.view.View, int]
     candidates:
      com.devspark.appmsg.AppMsg.makeText(android.app.Activity, int, com.devspark.appmsg.AppMsg$Style, android.view.View, boolean):com.devspark.appmsg.AppMsg
      com.devspark.appmsg.AppMsg.makeText(android.app.Activity, java.lang.CharSequence, com.devspark.appmsg.AppMsg$Style, android.view.View, boolean):com.devspark.appmsg.AppMsg */
    public static AppMsg makeText(Activity context, CharSequence text, Style style, View customView) {
        return makeText(context, text, style, customView, false);
    }

    private static AppMsg makeText(Activity context, CharSequence text, Style style, View view, boolean floating) {
        AppMsg result = new AppMsg(context);
        view.setBackgroundResource(style.background);
        ((TextView) view.findViewById(16908299)).setText(text);
        result.mView = view;
        result.mDuration = style.duration;
        result.mFloating = floating;
        return result;
    }

    public static AppMsg makeText(Activity context, int resId, Style style, View customView, boolean floating) {
        return makeText(context, context.getResources().getText(resId), style, customView, floating);
    }

    public static AppMsg makeText(Activity context, int resId, Style style) throws Resources.NotFoundException {
        return makeText(context, context.getResources().getText(resId), style);
    }

    public static AppMsg makeText(Activity context, int resId, Style style, int layoutId) throws Resources.NotFoundException {
        return makeText(context, context.getResources().getText(resId), style, layoutId);
    }

    public void show() {
        MsgManager.getInstance().add(this);
    }

    public boolean isShowing() {
        if (this.mFloating) {
            if (this.mView == null || this.mView.getParent() == null) {
                return false;
            }
            return true;
        } else if (this.mView.getVisibility() != 0) {
            return false;
        } else {
            return true;
        }
    }

    public void cancel() {
        MsgManager.getInstance().clearMsg(this);
    }

    public static void cancelAll() {
        MsgManager.getInstance().clearAllMsg();
    }

    public Activity getActivity() {
        return this.mContext;
    }

    public void setView(View view) {
        this.mView = view;
    }

    public View getView() {
        return this.mView;
    }

    public void setDuration(int duration) {
        this.mDuration = duration;
    }

    public int getDuration() {
        return this.mDuration;
    }

    public void setText(int resId) {
        setText(this.mContext.getText(resId));
    }

    public void setText(CharSequence s) {
        if (this.mView == null) {
            throw new RuntimeException("This AppMsg was not created with AppMsg.makeText()");
        }
        TextView tv = (TextView) this.mView.findViewById(16908299);
        if (tv == null) {
            throw new RuntimeException("This AppMsg was not created with AppMsg.makeText()");
        }
        tv.setText(s);
    }

    public ViewGroup.LayoutParams getLayoutParams() {
        if (this.mLayoutParams == null) {
            this.mLayoutParams = new ViewGroup.LayoutParams(-1, -2);
        }
        return this.mLayoutParams;
    }

    public AppMsg setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        this.mLayoutParams = layoutParams;
        return this;
    }

    public Animation getInAnimation() {
        return this.mInAnimation;
    }

    public AppMsg setInAnimation(Animation inAnimation) {
        this.mInAnimation = inAnimation;
        return this;
    }

    public Animation getOutAnimation() {
        return this.mOutAnimation;
    }

    public AppMsg setOutAnimation(Animation outAnimation) {
        this.mOutAnimation = outAnimation;
        return this;
    }

    public AppMsg setLayoutGravity(int gravity) {
        this.mLayoutParams = new FrameLayout.LayoutParams(-1, -2, gravity);
        return this;
    }

    public boolean isFloating() {
        return this.mFloating;
    }

    public void setFloating(boolean mFloating2) {
        this.mFloating = mFloating2;
    }

    public static class Style {
        /* access modifiers changed from: private */
        public final int background;
        /* access modifiers changed from: private */
        public final int duration;

        public Style(int duration2, int resId) {
            this.duration = duration2;
            this.background = resId;
        }

        public int getDuration() {
            return this.duration;
        }

        public int getBackground() {
            return this.background;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Style)) {
                return false;
            }
            Style style = (Style) o;
            if (style.duration == this.duration && style.background == this.background) {
                return true;
            }
            return false;
        }
    }
}
