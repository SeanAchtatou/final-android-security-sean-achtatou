package com.devspark.appmsg;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import java.util.LinkedList;
import java.util.Queue;

class MsgManager extends Handler {
    private static final int MESSAGE_ADD_VIEW = -1040157475;
    private static final int MESSAGE_DISPLAY = 794631;
    private static final int MESSAGE_REMOVE = -1040155167;
    private static MsgManager mInstance;
    private Animation inAnimation;
    private Queue<AppMsg> msgQueue = new LinkedList();
    private Animation outAnimation;

    private MsgManager() {
    }

    static synchronized MsgManager getInstance() {
        MsgManager msgManager;
        synchronized (MsgManager.class) {
            if (mInstance == null) {
                mInstance = new MsgManager();
            }
            msgManager = mInstance;
        }
        return msgManager;
    }

    /* access modifiers changed from: package-private */
    public void add(AppMsg appMsg) {
        this.msgQueue.add(appMsg);
        Animation inAnimation2 = appMsg.getInAnimation();
        this.inAnimation = inAnimation2;
        if (inAnimation2 == null) {
            this.inAnimation = AnimationUtils.loadAnimation(appMsg.getActivity(), 17432576);
        }
        Animation outAnimation2 = appMsg.getOutAnimation();
        this.outAnimation = outAnimation2;
        if (outAnimation2 == null) {
            this.outAnimation = AnimationUtils.loadAnimation(appMsg.getActivity(), 17432577);
        }
        displayMsg();
    }

    /* access modifiers changed from: package-private */
    public void clearMsg(AppMsg appMsg) {
        if (this.msgQueue.contains(appMsg)) {
            removeMessages(MESSAGE_REMOVE);
            this.msgQueue.remove(appMsg);
            removeMsg(appMsg);
        }
    }

    /* access modifiers changed from: package-private */
    public void clearAllMsg() {
        if (this.msgQueue != null) {
            this.msgQueue.clear();
        }
        removeMessages(MESSAGE_DISPLAY);
        removeMessages(MESSAGE_ADD_VIEW);
        removeMessages(MESSAGE_REMOVE);
    }

    private void displayMsg() {
        if (!this.msgQueue.isEmpty()) {
            AppMsg appMsg = this.msgQueue.peek();
            if (appMsg.getActivity() == null) {
                this.msgQueue.poll();
            }
            if (!appMsg.isShowing()) {
                Message msg = obtainMessage(MESSAGE_ADD_VIEW);
                msg.obj = appMsg;
                sendMessage(msg);
                return;
            }
            sendMessageDelayed(obtainMessage(MESSAGE_DISPLAY), ((long) appMsg.getDuration()) + this.inAnimation.getDuration() + this.outAnimation.getDuration());
        }
    }

    private void removeMsg(AppMsg appMsg) {
        ViewGroup parent = (ViewGroup) appMsg.getView().getParent();
        if (parent != null) {
            this.outAnimation.setAnimationListener(new OutAnimationListener(appMsg));
            appMsg.getView().startAnimation(this.outAnimation);
            this.msgQueue.poll();
            if (appMsg.isFloating()) {
                parent.removeView(appMsg.getView());
            } else {
                appMsg.getView().setVisibility(4);
            }
            sendMessage(obtainMessage(MESSAGE_DISPLAY));
        }
    }

    private void addMsgToView(AppMsg appMsg) {
        View view = appMsg.getView();
        if (view.getParent() == null) {
            appMsg.getActivity().addContentView(view, appMsg.getLayoutParams());
        }
        view.startAnimation(this.inAnimation);
        if (view.getVisibility() != 0) {
            view.setVisibility(0);
        }
        Message msg = obtainMessage(MESSAGE_REMOVE);
        msg.obj = appMsg;
        sendMessageDelayed(msg, (long) appMsg.getDuration());
    }

    public void handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_ADD_VIEW /*-1040157475*/:
                addMsgToView((AppMsg) msg.obj);
                return;
            case MESSAGE_REMOVE /*-1040155167*/:
                removeMsg((AppMsg) msg.obj);
                return;
            case MESSAGE_DISPLAY /*794631*/:
                displayMsg();
                return;
            default:
                super.handleMessage(msg);
                return;
        }
    }

    private static class OutAnimationListener implements Animation.AnimationListener {
        private AppMsg appMsg;

        private OutAnimationListener(AppMsg appMsg2) {
            this.appMsg = appMsg2;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            if (!this.appMsg.isFloating()) {
                this.appMsg.getView().setVisibility(8);
            }
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }
}
