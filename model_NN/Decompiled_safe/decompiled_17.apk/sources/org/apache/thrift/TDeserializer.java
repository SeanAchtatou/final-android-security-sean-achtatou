package org.apache.thrift;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TMemoryInputTransport;

public class TDeserializer {
    private final TProtocol a;
    private final TMemoryInputTransport b;

    public TDeserializer() {
        this(new TBinaryProtocol.Factory());
    }

    public TDeserializer(TProtocolFactory tProtocolFactory) {
        this.b = new TMemoryInputTransport();
        this.a = tProtocolFactory.a(this.b);
    }

    public void a(TBase tBase, byte[] bArr) {
        try {
            this.b.a(bArr);
            tBase.a(this.a);
        } finally {
            this.a.y();
        }
    }
}
