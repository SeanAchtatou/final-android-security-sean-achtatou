package org.apache.thrift.transport;

import org.apache.thrift.TException;

public class TTransportException extends TException {
    protected int a = 0;

    public TTransportException() {
    }

    public TTransportException(String str) {
        super(str);
    }
}
