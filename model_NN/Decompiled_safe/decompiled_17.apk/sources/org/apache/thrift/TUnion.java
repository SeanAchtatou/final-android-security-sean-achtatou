package org.apache.thrift;

import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.TUnion;
import org.apache.thrift.protocol.TField;

public abstract class TUnion<T extends TUnion, F extends TFieldIdEnum> implements TBase<T, F> {
    protected Object a = null;
    protected F b = null;

    protected TUnion() {
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        int min = Math.min(bArr.length, 128);
        for (int i = 0; i < min; i++) {
            if (i != 0) {
                sb.append(" ");
            }
            String hexString = Integer.toHexString(bArr[i] & 255);
            if (hexString.length() <= 1) {
                hexString = "0" + hexString;
            }
            sb.append(hexString);
        }
        if (bArr.length > 128) {
            sb.append(" ...");
        }
        return sb.toString();
    }

    public F a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public abstract TField a(F f);

    public Object b() {
        return this.a;
    }

    public String toString() {
        String str;
        String str2 = "<" + getClass().getSimpleName() + " ";
        if (a() != null) {
            Object b2 = b();
            str = String.valueOf(str2) + a(a()).a + ":" + (b2 instanceof byte[] ? a((byte[]) b2) : b2.toString());
        } else {
            str = str2;
        }
        return String.valueOf(str) + ">";
    }
}
