package org.apache.thrift.meta_data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.apache.thrift.TBase;
import org.apache.thrift.TFieldIdEnum;

public class FieldMetaData implements Serializable {
    private static Map<Class<? extends TBase>, Map<? extends TFieldIdEnum, FieldMetaData>> d = new HashMap();
    public final String a;
    public final byte b;
    public final FieldValueMetaData c;

    public FieldMetaData(String str, byte b2, FieldValueMetaData fieldValueMetaData) {
        this.a = str;
        this.b = b2;
        this.c = fieldValueMetaData;
    }

    public static void a(Class<? extends TBase> cls, Map<? extends TFieldIdEnum, FieldMetaData> map) {
        d.put(cls, map);
    }
}
