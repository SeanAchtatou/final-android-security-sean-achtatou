package org.apache.thrift.meta_data;

import org.apache.thrift.TEnum;

public class EnumMetaData extends FieldValueMetaData {
    public final Class<? extends TEnum> a;

    public EnumMetaData(byte b, Class<? extends TEnum> cls) {
        super(b);
        this.a = cls;
    }
}
