package org.apache.thrift;

import java.io.Serializable;
import org.apache.thrift.TBase;
import org.apache.thrift.TFieldIdEnum;
import org.apache.thrift.protocol.TProtocol;

public interface TBase<T extends TBase, F extends TFieldIdEnum> extends Serializable, Comparable<T> {
    void a(TProtocol tProtocol);

    void b(TProtocol tProtocol);
}
