package org.apache.thrift;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TMemoryBuffer;

public class TSerializer {
    private final TByteArrayOutputStream a;
    private final TMemoryBuffer b;
    private TProtocol c;

    public TSerializer() {
        this(new TBinaryProtocol.Factory());
    }

    public TSerializer(TProtocolFactory tProtocolFactory) {
        this.a = new TByteArrayOutputStream();
        this.b = new TMemoryBuffer(this.a);
        this.c = tProtocolFactory.a(this.b);
    }

    public byte[] a(TBase tBase) {
        this.a.reset();
        tBase.b(this.c);
        return this.a.toByteArray();
    }
}
