package org.apache.thrift;

import com.tencent.mm.sdk.message.RMsgInfoDB;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TStruct;

public class TApplicationException extends TException {
    private static final TStruct b = new TStruct("TApplicationException");
    private static final TField c = new TField(RMsgInfoDB.TABLE, (byte) 11, 1);
    private static final TField d = new TField("type", (byte) 8, 2);
    protected int a = 0;
}
