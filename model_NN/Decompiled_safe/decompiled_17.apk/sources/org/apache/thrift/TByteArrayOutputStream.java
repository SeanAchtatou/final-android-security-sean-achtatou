package org.apache.thrift;

import java.io.ByteArrayOutputStream;

public class TByteArrayOutputStream extends ByteArrayOutputStream {
    public byte[] a() {
        return this.buf;
    }

    public int b() {
        return this.count;
    }
}
