package org.apache.thrift.protocol;

public final class TMessage {
    public final String a;
    public final byte b;
    public final int c;

    public TMessage() {
        this("", (byte) 0, 0);
    }

    public TMessage(String str, byte b2, int i) {
        this.a = str;
        this.b = b2;
        this.c = i;
    }

    public boolean a(TMessage tMessage) {
        return this.a.equals(tMessage.a) && this.b == tMessage.b && this.c == tMessage.c;
    }

    public boolean equals(Object obj) {
        if (obj instanceof TMessage) {
            return a((TMessage) obj);
        }
        return false;
    }

    public String toString() {
        return "<TMessage name:'" + this.a + "' type: " + ((int) this.b) + " seqid:" + this.c + ">";
    }
}
