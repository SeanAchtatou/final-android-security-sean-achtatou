package org.apache.thrift.protocol;

public final class TMap {
    public final byte a;
    public final byte b;
    public final int c;

    public TMap() {
        this((byte) 0, (byte) 0, 0);
    }

    public TMap(byte b2, byte b3, int i) {
        this.a = b2;
        this.b = b3;
        this.c = i;
    }
}
