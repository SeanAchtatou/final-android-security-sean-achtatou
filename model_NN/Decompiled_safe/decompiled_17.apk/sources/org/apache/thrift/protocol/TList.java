package org.apache.thrift.protocol;

public final class TList {
    public final byte a;
    public final int b;

    public TList() {
        this((byte) 0, 0);
    }

    public TList(byte b2, int i) {
        this.a = b2;
        this.b = i;
    }
}
