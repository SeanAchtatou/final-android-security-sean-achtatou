package org.apache.thrift.protocol;

import org.apache.thrift.TException;

public class TProtocolException extends TException {
    protected int a = 0;

    public TProtocolException() {
    }

    public TProtocolException(String str) {
        super(str);
    }
}
