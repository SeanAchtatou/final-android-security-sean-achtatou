package org.apache.thrift.protocol;

import com.baidu.mapapi.MKEvent;
import org.apache.thrift.TException;

public class TProtocolUtil {
    private static int a = Integer.MAX_VALUE;

    public static void a(TProtocol tProtocol, byte b) {
        a(tProtocol, b, a);
    }

    public static void a(TProtocol tProtocol, byte b, int i) {
        int i2 = 0;
        if (i <= 0) {
            throw new TException("Maximum skip depth exceeded");
        }
        switch (b) {
            case 2:
                tProtocol.q();
                return;
            case 3:
                tProtocol.r();
                return;
            case 4:
                tProtocol.v();
                return;
            case 5:
            case 7:
            case 9:
            default:
                return;
            case 6:
                tProtocol.s();
                return;
            case 8:
                tProtocol.t();
                return;
            case 10:
                tProtocol.u();
                return;
            case 11:
                tProtocol.x();
                return;
            case 12:
                tProtocol.g();
                while (true) {
                    TField i3 = tProtocol.i();
                    if (i3.b == 0) {
                        tProtocol.h();
                        return;
                    } else {
                        a(tProtocol, i3.b, i - 1);
                        tProtocol.j();
                    }
                }
            case 13:
                TMap k = tProtocol.k();
                while (i2 < k.c) {
                    a(tProtocol, k.a, i - 1);
                    a(tProtocol, k.b, i - 1);
                    i2++;
                }
                tProtocol.l();
                return;
            case MKEvent.MKEVENT_MAP_MOVE_FINISH:
                TSet o = tProtocol.o();
                while (i2 < o.b) {
                    a(tProtocol, o.a, i - 1);
                    i2++;
                }
                tProtocol.p();
                return;
            case 15:
                TList m = tProtocol.m();
                while (i2 < m.b) {
                    a(tProtocol, m.a, i - 1);
                    i2++;
                }
                tProtocol.n();
                return;
        }
    }
}
