package org.apache.thrift.protocol;

import java.nio.ByteBuffer;
import org.apache.thrift.transport.TTransport;

public abstract class TProtocol {
    protected TTransport e;

    private TProtocol() {
    }

    protected TProtocol(TTransport tTransport) {
        this.e = tTransport;
    }

    public abstract void a();

    public abstract void a(int i);

    public abstract void a(long j);

    public abstract void a(String str);

    public abstract void a(ByteBuffer byteBuffer);

    public abstract void a(TField tField);

    public abstract void a(TList tList);

    public abstract void a(TMap tMap);

    public abstract void a(TSet tSet);

    public abstract void a(TStruct tStruct);

    public abstract void a(boolean z);

    public abstract void b();

    public abstract void c();

    public abstract void d();

    public abstract void e();

    public abstract void f();

    public abstract TStruct g();

    public abstract void h();

    public abstract TField i();

    public abstract void j();

    public abstract TMap k();

    public abstract void l();

    public abstract TList m();

    public abstract void n();

    public abstract TSet o();

    public abstract void p();

    public abstract boolean q();

    public abstract byte r();

    public abstract short s();

    public abstract int t();

    public abstract long u();

    public abstract double v();

    public abstract String w();

    public abstract ByteBuffer x();

    public void y() {
    }
}
