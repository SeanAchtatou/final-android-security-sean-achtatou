package org.apache.thrift.protocol;

public final class TSet {
    public final byte a;
    public final int b;

    public TSet() {
        this((byte) 0, 0);
    }

    public TSet(byte b2, int i) {
        this.a = b2;
        this.b = i;
    }

    public TSet(TList tList) {
        this(tList.a, tList.b);
    }
}
