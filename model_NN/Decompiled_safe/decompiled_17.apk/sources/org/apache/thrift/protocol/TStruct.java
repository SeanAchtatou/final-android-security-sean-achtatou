package org.apache.thrift.protocol;

public final class TStruct {
    public final String a;

    public TStruct() {
        this("");
    }

    public TStruct(String str) {
        this.a = str;
    }
}
