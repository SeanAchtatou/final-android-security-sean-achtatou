package org.apache.commons.httpclient.methods;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import org.apache.commons.httpclient.HeaderElement;
import org.apache.commons.httpclient.NameValuePair;

public class StringRequestEntity implements RequestEntity {
    private String charset;
    private byte[] content;
    private String contentType;

    public StringRequestEntity(String content2) {
        if (content2 == null) {
            throw new IllegalArgumentException("The content cannot be null");
        }
        this.contentType = null;
        this.charset = null;
        this.content = content2.getBytes();
    }

    public StringRequestEntity(String content2, String contentType2, String charset2) throws UnsupportedEncodingException {
        if (content2 == null) {
            throw new IllegalArgumentException("The content cannot be null");
        }
        this.contentType = contentType2;
        this.charset = charset2;
        if (contentType2 != null) {
            HeaderElement[] values = HeaderElement.parseElements(contentType2);
            NameValuePair charsetPair = null;
            int i = 0;
            while (i < values.length && (charsetPair = values[i].getParameterByName("charset")) == null) {
                i++;
            }
            if (charset2 == null && charsetPair != null) {
                this.charset = charsetPair.getValue();
            } else if (charset2 != null && charsetPair == null) {
                this.contentType = new StringBuffer().append(contentType2).append("; charset=").append(charset2).toString();
            }
        }
        if (this.charset != null) {
            this.content = content2.getBytes(this.charset);
        } else {
            this.content = content2.getBytes();
        }
    }

    public String getContentType() {
        return this.contentType;
    }

    public boolean isRepeatable() {
        return true;
    }

    public void writeRequest(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        out.write(this.content);
        out.flush();
    }

    public long getContentLength() {
        return (long) this.content.length;
    }

    public String getContent() {
        if (this.charset == null) {
            return new String(this.content);
        }
        try {
            return new String(this.content, this.charset);
        } catch (UnsupportedEncodingException e) {
            return new String(this.content);
        }
    }

    public String getCharset() {
        return this.charset;
    }
}
