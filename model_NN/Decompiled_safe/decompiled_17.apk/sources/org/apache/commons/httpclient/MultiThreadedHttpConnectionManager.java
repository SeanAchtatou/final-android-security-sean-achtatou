package org.apache.commons.httpclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.WeakHashMap;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.IdleConnectionHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MultiThreadedHttpConnectionManager implements HttpConnectionManager {
    private static WeakHashMap ALL_CONNECTION_MANAGERS = new WeakHashMap();
    public static final int DEFAULT_MAX_HOST_CONNECTIONS = 2;
    public static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 20;
    private static final Log LOG;
    private static final ReferenceQueue REFERENCE_QUEUE = new ReferenceQueue();
    private static ReferenceQueueThread REFERENCE_QUEUE_THREAD;
    private static final Map REFERENCE_TO_CONNECTION_SOURCE = new HashMap();
    static Class class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager;
    private ConnectionPool connectionPool = new ConnectionPool(this, null);
    private HttpConnectionManagerParams params = new HttpConnectionManagerParams();
    private volatile boolean shutdown = false;

    /* renamed from: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$1  reason: invalid class name */
    static class AnonymousClass1 {
    }

    static HostConfiguration access$1100(MultiThreadedHttpConnectionManager x0, HttpConnection x1) {
        return x0.configurationForConnection(x1);
    }

    static boolean access$1200(MultiThreadedHttpConnectionManager x0) {
        return x0.shutdown;
    }

    static void access$1300(HttpConnectionWithReference x0) {
        removeReferenceToConnection(x0);
    }

    static Map access$1400() {
        return REFERENCE_TO_CONNECTION_SOURCE;
    }

    static ReferenceQueue access$1500() {
        return REFERENCE_QUEUE;
    }

    static void access$600(ConnectionPool x0) {
        shutdownCheckedOutConnections(x0);
    }

    static Log access$700() {
        return LOG;
    }

    static HttpConnectionManagerParams access$800(MultiThreadedHttpConnectionManager x0) {
        return x0.params;
    }

    static void access$900(HttpConnectionWithReference x0, HostConfiguration x1, ConnectionPool x2) {
        storeReferenceToConnection(x0, x1, x2);
    }

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager == null) {
            cls = class$("org.apache.commons.httpclient.MultiThreadedHttpConnectionManager");
            class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager = cls;
        } else {
            cls = class$org$apache$commons$httpclient$MultiThreadedHttpConnectionManager;
        }
        LOG = LogFactory.getLog(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public static void shutdownAll() {
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            synchronized (ALL_CONNECTION_MANAGERS) {
                MultiThreadedHttpConnectionManager[] connManagers = (MultiThreadedHttpConnectionManager[]) ALL_CONNECTION_MANAGERS.keySet().toArray(new MultiThreadedHttpConnectionManager[ALL_CONNECTION_MANAGERS.size()]);
                for (int i = 0; i < connManagers.length; i++) {
                    if (connManagers[i] != null) {
                        connManagers[i].shutdown();
                    }
                }
            }
            if (REFERENCE_QUEUE_THREAD != null) {
                REFERENCE_QUEUE_THREAD.shutdown();
                REFERENCE_QUEUE_THREAD = null;
            }
            REFERENCE_TO_CONNECTION_SOURCE.clear();
        }
    }

    private static void storeReferenceToConnection(HttpConnectionWithReference connection, HostConfiguration hostConfiguration, ConnectionPool connectionPool2) {
        ConnectionSource source = new ConnectionSource(null);
        source.connectionPool = connectionPool2;
        source.hostConfiguration = hostConfiguration;
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            if (REFERENCE_QUEUE_THREAD == null) {
                REFERENCE_QUEUE_THREAD = new ReferenceQueueThread();
                REFERENCE_QUEUE_THREAD.start();
            }
            REFERENCE_TO_CONNECTION_SOURCE.put(connection.reference, source);
        }
    }

    private static void shutdownCheckedOutConnections(ConnectionPool connectionPool2) {
        ArrayList connectionsToClose = new ArrayList();
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            Iterator referenceIter = REFERENCE_TO_CONNECTION_SOURCE.keySet().iterator();
            while (referenceIter.hasNext()) {
                Reference ref = (Reference) referenceIter.next();
                if (((ConnectionSource) REFERENCE_TO_CONNECTION_SOURCE.get(ref)).connectionPool == connectionPool2) {
                    referenceIter.remove();
                    HttpConnection connection = (HttpConnection) ref.get();
                    if (connection != null) {
                        connectionsToClose.add(connection);
                    }
                }
            }
        }
        Iterator i = connectionsToClose.iterator();
        while (i.hasNext()) {
            HttpConnection connection2 = (HttpConnection) i.next();
            connection2.close();
            connection2.setHttpConnectionManager(null);
            connection2.releaseConnection();
        }
    }

    private static void removeReferenceToConnection(HttpConnectionWithReference connection) {
        synchronized (REFERENCE_TO_CONNECTION_SOURCE) {
            REFERENCE_TO_CONNECTION_SOURCE.remove(connection.reference);
        }
    }

    public MultiThreadedHttpConnectionManager() {
        synchronized (ALL_CONNECTION_MANAGERS) {
            ALL_CONNECTION_MANAGERS.put(this, null);
        }
    }

    public synchronized void shutdown() {
        synchronized (this.connectionPool) {
            if (!this.shutdown) {
                this.shutdown = true;
                this.connectionPool.shutdown();
            }
        }
    }

    public boolean isConnectionStaleCheckingEnabled() {
        return this.params.isStaleCheckingEnabled();
    }

    public void setConnectionStaleCheckingEnabled(boolean connectionStaleCheckingEnabled) {
        this.params.setStaleCheckingEnabled(connectionStaleCheckingEnabled);
    }

    public void setMaxConnectionsPerHost(int maxHostConnections) {
        this.params.setDefaultMaxConnectionsPerHost(maxHostConnections);
    }

    public int getMaxConnectionsPerHost() {
        return this.params.getDefaultMaxConnectionsPerHost();
    }

    public void setMaxTotalConnections(int maxTotalConnections) {
        this.params.setMaxTotalConnections(maxTotalConnections);
    }

    public int getMaxTotalConnections() {
        return this.params.getMaxTotalConnections();
    }

    public HttpConnection getConnection(HostConfiguration hostConfiguration) {
        while (true) {
            try {
                break;
            } catch (ConnectionPoolTimeoutException e) {
                LOG.debug("Unexpected exception while waiting for connection", e);
            }
        }
        return getConnectionWithTimeout(hostConfiguration, 0);
    }

    public HttpConnection getConnectionWithTimeout(HostConfiguration hostConfiguration, long timeout) throws ConnectionPoolTimeoutException {
        LOG.trace("enter HttpConnectionManager.getConnectionWithTimeout(HostConfiguration, long)");
        if (hostConfiguration == null) {
            throw new IllegalArgumentException("hostConfiguration is null");
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer().append("HttpConnectionManager.getConnection:  config = ").append(hostConfiguration).append(", timeout = ").append(timeout).toString());
        }
        return new HttpConnectionAdapter(doGetConnection(hostConfiguration, timeout));
    }

    public HttpConnection getConnection(HostConfiguration hostConfiguration, long timeout) throws HttpException {
        LOG.trace("enter HttpConnectionManager.getConnection(HostConfiguration, long)");
        try {
            return getConnectionWithTimeout(hostConfiguration, timeout);
        } catch (ConnectionPoolTimeoutException e) {
            throw new HttpException(e.getMessage());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x0108 A[Catch:{ all -> 0x005d }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0122 A[Catch:{ all -> 0x005d }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01c6 A[SYNTHETIC, Splitter:B:79:0x01c6] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x00ea A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.commons.httpclient.HttpConnection doGetConnection(org.apache.commons.httpclient.HostConfiguration r22, long r23) throws org.apache.commons.httpclient.ConnectionPoolTimeoutException {
        /*
            r21 = this;
            r2 = 0
            r0 = r21
            org.apache.commons.httpclient.params.HttpConnectionManagerParams r0 = r0.params
            r17 = r0
            r0 = r17
            r1 = r22
            int r8 = r0.getMaxConnectionsPerHost(r1)
            r0 = r21
            org.apache.commons.httpclient.params.HttpConnectionManagerParams r0 = r0.params
            r17 = r0
            int r9 = r17.getMaxTotalConnections()
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool
            r18 = r0
            monitor-enter(r18)
            org.apache.commons.httpclient.HostConfiguration r6 = new org.apache.commons.httpclient.HostConfiguration     // Catch:{ all -> 0x01f1 }
            r0 = r22
            r6.<init>(r0)     // Catch:{ all -> 0x01f1 }
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            r19 = 1
            r0 = r17
            r1 = r19
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$HostConnectionPool r7 = r0.getHostPool(r6, r1)     // Catch:{ all -> 0x005d }
            r15 = 0
            r19 = 0
            int r17 = (r23 > r19 ? 1 : (r23 == r19 ? 0 : -1))
            if (r17 <= 0) goto L_0x0062
            r14 = 1
        L_0x003f:
            r12 = r23
            r10 = 0
            r4 = 0
            r16 = r15
        L_0x0047:
            if (r2 != 0) goto L_0x01ef
            r0 = r21
            boolean r0 = r0.shutdown     // Catch:{ all -> 0x005d }
            r17 = r0
            if (r17 == 0) goto L_0x0064
            java.lang.IllegalStateException r17 = new java.lang.IllegalStateException     // Catch:{ all -> 0x005d }
            java.lang.String r19 = "Connection factory has been shutdown."
            r0 = r17
            r1 = r19
            r0.<init>(r1)     // Catch:{ all -> 0x005d }
            throw r17     // Catch:{ all -> 0x005d }
        L_0x005d:
            r17 = move-exception
            r22 = r6
        L_0x0060:
            monitor-exit(r18)     // Catch:{ all -> 0x01f1 }
            throw r17
        L_0x0062:
            r14 = 0
            goto L_0x003f
        L_0x0064:
            java.util.LinkedList r0 = r7.freeConnections     // Catch:{ all -> 0x005d }
            r17 = r0
            int r17 = r17.size()     // Catch:{ all -> 0x005d }
            if (r17 <= 0) goto L_0x007b
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            r0 = r17
            org.apache.commons.httpclient.HttpConnection r2 = r0.getFreeConnection(r6)     // Catch:{ all -> 0x005d }
            goto L_0x0047
        L_0x007b:
            int r0 = r7.numConnections     // Catch:{ all -> 0x005d }
            r17 = r0
            r0 = r17
            if (r0 >= r8) goto L_0x009e
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            int r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$200(r17)     // Catch:{ all -> 0x005d }
            r0 = r17
            if (r0 >= r9) goto L_0x009e
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            r0 = r17
            org.apache.commons.httpclient.HttpConnection r2 = r0.createConnection(r6)     // Catch:{ all -> 0x005d }
            goto L_0x0047
        L_0x009e:
            int r0 = r7.numConnections     // Catch:{ all -> 0x005d }
            r17 = r0
            r0 = r17
            if (r0 >= r8) goto L_0x00cd
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            java.util.LinkedList r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$300(r17)     // Catch:{ all -> 0x005d }
            int r17 = r17.size()     // Catch:{ all -> 0x005d }
            if (r17 <= 0) goto L_0x00cd
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            r17.deleteLeastUsedConnection()     // Catch:{ all -> 0x005d }
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            r0 = r17
            org.apache.commons.httpclient.HttpConnection r2 = r0.createConnection(r6)     // Catch:{ all -> 0x005d }
            goto L_0x0047
        L_0x00cd:
            if (r14 == 0) goto L_0x012b
            r19 = 0
            int r17 = (r12 > r19 ? 1 : (r12 == r19 ? 0 : -1))
            if (r17 > 0) goto L_0x012b
            org.apache.commons.httpclient.ConnectionPoolTimeoutException r17 = new org.apache.commons.httpclient.ConnectionPoolTimeoutException     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            java.lang.String r19 = "Timeout waiting for connection"
            r0 = r17
            r1 = r19
            r0.<init>(r1)     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            throw r17     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
        L_0x00e1:
            r3 = move-exception
            r15 = r16
        L_0x00e4:
            boolean r0 = r15.interruptedByConnectionPool     // Catch:{ all -> 0x0101 }
            r17 = r0
            if (r17 != 0) goto L_0x01c6
            org.apache.commons.logging.Log r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.LOG     // Catch:{ all -> 0x0101 }
            java.lang.String r19 = "Interrupted while waiting for connection"
            r0 = r17
            r1 = r19
            r0.debug(r1, r3)     // Catch:{ all -> 0x0101 }
            java.lang.IllegalThreadStateException r17 = new java.lang.IllegalThreadStateException     // Catch:{ all -> 0x0101 }
            java.lang.String r19 = "Interrupted while waiting in MultiThreadedHttpConnectionManager"
            r0 = r17
            r1 = r19
            r0.<init>(r1)     // Catch:{ all -> 0x0101 }
            throw r17     // Catch:{ all -> 0x0101 }
        L_0x0101:
            r17 = move-exception
        L_0x0102:
            boolean r0 = r15.interruptedByConnectionPool     // Catch:{ all -> 0x005d }
            r19 = r0
            if (r19 != 0) goto L_0x0120
            java.util.LinkedList r0 = r7.waitingThreads     // Catch:{ all -> 0x005d }
            r19 = r0
            r0 = r19
            r0.remove(r15)     // Catch:{ all -> 0x005d }
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r19 = r0
            java.util.LinkedList r19 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r19)     // Catch:{ all -> 0x005d }
            r0 = r19
            r0.remove(r15)     // Catch:{ all -> 0x005d }
        L_0x0120:
            if (r14 == 0) goto L_0x012a
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x005d }
            long r19 = r4 - r10
            long r12 = r12 - r19
        L_0x012a:
            throw r17     // Catch:{ all -> 0x005d }
        L_0x012b:
            org.apache.commons.logging.Log r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.LOG     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            boolean r17 = r17.isDebugEnabled()     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            if (r17 == 0) goto L_0x0151
            org.apache.commons.logging.Log r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.LOG     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            java.lang.StringBuffer r19 = new java.lang.StringBuffer     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            r19.<init>()     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            java.lang.String r20 = "Unable to get a connection, waiting..., hostConfig="
            java.lang.StringBuffer r19 = r19.append(r20)     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            r0 = r19
            java.lang.StringBuffer r19 = r0.append(r6)     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            java.lang.String r19 = r19.toString()     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            r0 = r17
            r1 = r19
            r0.debug(r1)     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
        L_0x0151:
            if (r16 != 0) goto L_0x01bb
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread r15 = new org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            r17 = 0
            r0 = r17
            r15.<init>(r0)     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            r15.hostConnectionPool = r7     // Catch:{ InterruptedException -> 0x01f9 }
            java.lang.Thread r17 = java.lang.Thread.currentThread()     // Catch:{ InterruptedException -> 0x01f9 }
            r0 = r17
            r15.thread = r0     // Catch:{ InterruptedException -> 0x01f9 }
        L_0x0166:
            if (r14 == 0) goto L_0x016c
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ InterruptedException -> 0x01f9 }
        L_0x016c:
            java.util.LinkedList r0 = r7.waitingThreads     // Catch:{ InterruptedException -> 0x01f9 }
            r17 = r0
            r0 = r17
            r0.addLast(r15)     // Catch:{ InterruptedException -> 0x01f9 }
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ InterruptedException -> 0x01f9 }
            r17 = r0
            java.util.LinkedList r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r17)     // Catch:{ InterruptedException -> 0x01f9 }
            r0 = r17
            r0.addLast(r15)     // Catch:{ InterruptedException -> 0x01f9 }
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ InterruptedException -> 0x01f9 }
            r17 = r0
            r0 = r17
            r0.wait(r12)     // Catch:{ InterruptedException -> 0x01f9 }
            boolean r0 = r15.interruptedByConnectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            if (r17 != 0) goto L_0x01ad
            java.util.LinkedList r0 = r7.waitingThreads     // Catch:{ all -> 0x005d }
            r17 = r0
            r0 = r17
            r0.remove(r15)     // Catch:{ all -> 0x005d }
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            java.util.LinkedList r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r17)     // Catch:{ all -> 0x005d }
            r0 = r17
            r0.remove(r15)     // Catch:{ all -> 0x005d }
        L_0x01ad:
            if (r14 == 0) goto L_0x01b7
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x005d }
            long r19 = r4 - r10
            long r12 = r12 - r19
        L_0x01b7:
            r16 = r15
            goto L_0x0047
        L_0x01bb:
            r17 = 0
            r0 = r17
            r1 = r16
            r1.interruptedByConnectionPool = r0     // Catch:{ InterruptedException -> 0x00e1, all -> 0x01f4 }
            r15 = r16
            goto L_0x0166
        L_0x01c6:
            boolean r0 = r15.interruptedByConnectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            if (r17 != 0) goto L_0x01e4
            java.util.LinkedList r0 = r7.waitingThreads     // Catch:{ all -> 0x005d }
            r17 = r0
            r0 = r17
            r0.remove(r15)     // Catch:{ all -> 0x005d }
            r0 = r21
            org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$ConnectionPool r0 = r0.connectionPool     // Catch:{ all -> 0x005d }
            r17 = r0
            java.util.LinkedList r17 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.access$500(r17)     // Catch:{ all -> 0x005d }
            r0 = r17
            r0.remove(r15)     // Catch:{ all -> 0x005d }
        L_0x01e4:
            if (r14 == 0) goto L_0x01b7
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x005d }
            long r19 = r4 - r10
            long r12 = r12 - r19
            goto L_0x01b7
        L_0x01ef:
            monitor-exit(r18)     // Catch:{ all -> 0x005d }
            return r2
        L_0x01f1:
            r17 = move-exception
            goto L_0x0060
        L_0x01f4:
            r17 = move-exception
            r15 = r16
            goto L_0x0102
        L_0x01f9:
            r3 = move-exception
            goto L_0x00e4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.doGetConnection(org.apache.commons.httpclient.HostConfiguration, long):org.apache.commons.httpclient.HttpConnection");
    }

    public int getConnectionsInPool(HostConfiguration hostConfiguration) {
        int i = 0;
        synchronized (this.connectionPool) {
            HostConnectionPool hostPool = this.connectionPool.getHostPool(hostConfiguration, false);
            if (hostPool != null) {
                i = hostPool.numConnections;
            }
        }
        return i;
    }

    public int getConnectionsInPool() {
        int access$200;
        synchronized (this.connectionPool) {
            access$200 = ConnectionPool.access$200(this.connectionPool);
        }
        return access$200;
    }

    public int getConnectionsInUse(HostConfiguration hostConfiguration) {
        return getConnectionsInPool(hostConfiguration);
    }

    public int getConnectionsInUse() {
        return getConnectionsInPool();
    }

    public void deleteClosedConnections() {
        this.connectionPool.deleteClosedConnections();
    }

    public void closeIdleConnections(long idleTimeout) {
        this.connectionPool.closeIdleConnections(idleTimeout);
        deleteClosedConnections();
    }

    public void releaseConnection(HttpConnection conn) {
        LOG.trace("enter HttpConnectionManager.releaseConnection(HttpConnection)");
        if (conn instanceof HttpConnectionAdapter) {
            conn = ((HttpConnectionAdapter) conn).getWrappedConnection();
        }
        SimpleHttpConnectionManager.finishLastResponse(conn);
        this.connectionPool.freeConnection(conn);
    }

    private HostConfiguration configurationForConnection(HttpConnection conn) {
        HostConfiguration connectionConfiguration = new HostConfiguration();
        connectionConfiguration.setHost(conn.getHost(), conn.getPort(), conn.getProtocol());
        if (conn.getLocalAddress() != null) {
            connectionConfiguration.setLocalAddress(conn.getLocalAddress());
        }
        if (conn.getProxyHost() != null) {
            connectionConfiguration.setProxy(conn.getProxyHost(), conn.getProxyPort());
        }
        return connectionConfiguration;
    }

    public HttpConnectionManagerParams getParams() {
        return this.params;
    }

    public void setParams(HttpConnectionManagerParams params2) {
        if (params2 == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = params2;
    }

    private class ConnectionPool {
        private LinkedList freeConnections;
        private IdleConnectionHandler idleConnectionHandler;
        private final Map mapHosts;
        private int numConnections;
        private final MultiThreadedHttpConnectionManager this$0;
        private LinkedList waitingThreads;

        private ConnectionPool(MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager) {
            this.this$0 = multiThreadedHttpConnectionManager;
            this.freeConnections = new LinkedList();
            this.waitingThreads = new LinkedList();
            this.mapHosts = new HashMap();
            this.idleConnectionHandler = new IdleConnectionHandler();
            this.numConnections = 0;
        }

        ConnectionPool(MultiThreadedHttpConnectionManager x0, AnonymousClass1 x1) {
            this(x0);
        }

        static int access$200(ConnectionPool x0) {
            return x0.numConnections;
        }

        static LinkedList access$300(ConnectionPool x0) {
            return x0.freeConnections;
        }

        static LinkedList access$500(ConnectionPool x0) {
            return x0.waitingThreads;
        }

        public synchronized void shutdown() {
            Iterator iter = this.freeConnections.iterator();
            while (iter.hasNext()) {
                iter.remove();
                ((HttpConnection) iter.next()).close();
            }
            MultiThreadedHttpConnectionManager.access$600(this);
            Iterator iter2 = this.waitingThreads.iterator();
            while (iter2.hasNext()) {
                WaitingThread waiter = (WaitingThread) iter2.next();
                iter2.remove();
                waiter.interruptedByConnectionPool = true;
                waiter.thread.interrupt();
            }
            this.mapHosts.clear();
            this.idleConnectionHandler.removeAll();
        }

        public synchronized HttpConnection createConnection(HostConfiguration hostConfiguration) {
            HttpConnectionWithReference connection;
            HostConnectionPool hostPool = getHostPool(hostConfiguration, true);
            if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer().append("Allocating new connection, hostConfig=").append(hostConfiguration).toString());
            }
            connection = new HttpConnectionWithReference(hostConfiguration);
            connection.getParams().setDefaults(MultiThreadedHttpConnectionManager.access$800(this.this$0));
            connection.setHttpConnectionManager(this.this$0);
            this.numConnections++;
            hostPool.numConnections++;
            MultiThreadedHttpConnectionManager.access$900(connection, hostConfiguration, this);
            return connection;
        }

        public synchronized void handleLostConnection(HostConfiguration config) {
            HostConnectionPool hostPool = getHostPool(config, true);
            hostPool.numConnections--;
            if (hostPool.numConnections == 0 && hostPool.waitingThreads.isEmpty()) {
                this.mapHosts.remove(config);
            }
            this.numConnections--;
            notifyWaitingThread(config);
        }

        public synchronized HostConnectionPool getHostPool(HostConfiguration hostConfiguration, boolean create) {
            HostConnectionPool listConnections;
            MultiThreadedHttpConnectionManager.access$700().trace("enter HttpConnectionManager.ConnectionPool.getHostPool(HostConfiguration)");
            listConnections = (HostConnectionPool) this.mapHosts.get(hostConfiguration);
            if (listConnections == null && create) {
                listConnections = new HostConnectionPool(null);
                listConnections.hostConfiguration = hostConfiguration;
                this.mapHosts.put(hostConfiguration, listConnections);
            }
            return listConnections;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$HttpConnectionWithReference} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public synchronized org.apache.commons.httpclient.HttpConnection getFreeConnection(org.apache.commons.httpclient.HostConfiguration r7) {
            /*
                r6 = this;
                monitor-enter(r6)
                r1 = 0
                r3 = 0
                org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$HostConnectionPool r2 = r6.getHostPool(r7, r3)     // Catch:{ all -> 0x0073 }
                if (r2 == 0) goto L_0x004e
                java.util.LinkedList r3 = r2.freeConnections     // Catch:{ all -> 0x0073 }
                int r3 = r3.size()     // Catch:{ all -> 0x0073 }
                if (r3 <= 0) goto L_0x004e
                java.util.LinkedList r3 = r2.freeConnections     // Catch:{ all -> 0x0073 }
                java.lang.Object r3 = r3.removeLast()     // Catch:{ all -> 0x0073 }
                r0 = r3
                org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$HttpConnectionWithReference r0 = (org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.HttpConnectionWithReference) r0     // Catch:{ all -> 0x0073 }
                r1 = r0
                java.util.LinkedList r3 = r6.freeConnections     // Catch:{ all -> 0x0073 }
                r3.remove(r1)     // Catch:{ all -> 0x0073 }
                org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$900(r1, r7, r6)     // Catch:{ all -> 0x0073 }
                org.apache.commons.logging.Log r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0073 }
                boolean r3 = r3.isDebugEnabled()     // Catch:{ all -> 0x0073 }
                if (r3 == 0) goto L_0x0047
                org.apache.commons.logging.Log r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0073 }
                java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ all -> 0x0073 }
                r4.<init>()     // Catch:{ all -> 0x0073 }
                java.lang.String r5 = "Getting free connection, hostConfig="
                java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ all -> 0x0073 }
                java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ all -> 0x0073 }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0073 }
                r3.debug(r4)     // Catch:{ all -> 0x0073 }
            L_0x0047:
                org.apache.commons.httpclient.util.IdleConnectionHandler r3 = r6.idleConnectionHandler     // Catch:{ all -> 0x0073 }
                r3.remove(r1)     // Catch:{ all -> 0x0073 }
            L_0x004c:
                monitor-exit(r6)
                return r1
            L_0x004e:
                org.apache.commons.logging.Log r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0073 }
                boolean r3 = r3.isDebugEnabled()     // Catch:{ all -> 0x0073 }
                if (r3 == 0) goto L_0x004c
                org.apache.commons.logging.Log r3 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0073 }
                java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ all -> 0x0073 }
                r4.<init>()     // Catch:{ all -> 0x0073 }
                java.lang.String r5 = "There were no free connections to get, hostConfig="
                java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ all -> 0x0073 }
                java.lang.StringBuffer r4 = r4.append(r7)     // Catch:{ all -> 0x0073 }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0073 }
                r3.debug(r4)     // Catch:{ all -> 0x0073 }
                goto L_0x004c
            L_0x0073:
                r3 = move-exception
                monitor-exit(r6)
                throw r3
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.getFreeConnection(org.apache.commons.httpclient.HostConfiguration):org.apache.commons.httpclient.HttpConnection");
        }

        public synchronized void deleteClosedConnections() {
            Iterator iter = this.freeConnections.iterator();
            while (iter.hasNext()) {
                HttpConnection conn = (HttpConnection) iter.next();
                if (!conn.isOpen()) {
                    iter.remove();
                    deleteConnection(conn);
                }
            }
        }

        public synchronized void closeIdleConnections(long idleTimeout) {
            this.idleConnectionHandler.closeIdleConnections(idleTimeout);
        }

        private synchronized void deleteConnection(HttpConnection connection) {
            HostConfiguration connectionConfiguration = MultiThreadedHttpConnectionManager.access$1100(this.this$0, connection);
            if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer().append("Reclaiming connection, hostConfig=").append(connectionConfiguration).toString());
            }
            connection.close();
            HostConnectionPool hostPool = getHostPool(connectionConfiguration, true);
            hostPool.freeConnections.remove(connection);
            hostPool.numConnections--;
            this.numConnections--;
            if (hostPool.numConnections == 0 && hostPool.waitingThreads.isEmpty()) {
                this.mapHosts.remove(connectionConfiguration);
            }
            this.idleConnectionHandler.remove(connection);
        }

        public synchronized void deleteLeastUsedConnection() {
            HttpConnection connection = (HttpConnection) this.freeConnections.removeFirst();
            if (connection != null) {
                deleteConnection(connection);
            } else if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug("Attempted to reclaim an unused connection but there were none.");
            }
        }

        public synchronized void notifyWaitingThread(HostConfiguration configuration) {
            notifyWaitingThread(getHostPool(configuration, true));
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v20, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public synchronized void notifyWaitingThread(org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.HostConnectionPool r6) {
            /*
                r5 = this;
                monitor-enter(r5)
                r1 = 0
                java.util.LinkedList r2 = r6.waitingThreads     // Catch:{ all -> 0x0078 }
                int r2 = r2.size()     // Catch:{ all -> 0x0078 }
                if (r2 <= 0) goto L_0x004b
                org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0078 }
                boolean r2 = r2.isDebugEnabled()     // Catch:{ all -> 0x0078 }
                if (r2 == 0) goto L_0x0030
                org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0078 }
                java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ all -> 0x0078 }
                r3.<init>()     // Catch:{ all -> 0x0078 }
                java.lang.String r4 = "Notifying thread waiting on host pool, hostConfig="
                java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ all -> 0x0078 }
                org.apache.commons.httpclient.HostConfiguration r4 = r6.hostConfiguration     // Catch:{ all -> 0x0078 }
                java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ all -> 0x0078 }
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0078 }
                r2.debug(r3)     // Catch:{ all -> 0x0078 }
            L_0x0030:
                java.util.LinkedList r2 = r6.waitingThreads     // Catch:{ all -> 0x0078 }
                java.lang.Object r2 = r2.removeFirst()     // Catch:{ all -> 0x0078 }
                r0 = r2
                org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread r0 = (org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.WaitingThread) r0     // Catch:{ all -> 0x0078 }
                r1 = r0
                java.util.LinkedList r2 = r5.waitingThreads     // Catch:{ all -> 0x0078 }
                r2.remove(r1)     // Catch:{ all -> 0x0078 }
            L_0x003f:
                if (r1 == 0) goto L_0x0049
                r2 = 1
                r1.interruptedByConnectionPool = r2     // Catch:{ all -> 0x0078 }
                java.lang.Thread r2 = r1.thread     // Catch:{ all -> 0x0078 }
                r2.interrupt()     // Catch:{ all -> 0x0078 }
            L_0x0049:
                monitor-exit(r5)
                return
            L_0x004b:
                java.util.LinkedList r2 = r5.waitingThreads     // Catch:{ all -> 0x0078 }
                int r2 = r2.size()     // Catch:{ all -> 0x0078 }
                if (r2 <= 0) goto L_0x007b
                org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0078 }
                boolean r2 = r2.isDebugEnabled()     // Catch:{ all -> 0x0078 }
                if (r2 == 0) goto L_0x0066
                org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0078 }
                java.lang.String r3 = "No-one waiting on host pool, notifying next waiting thread."
                r2.debug(r3)     // Catch:{ all -> 0x0078 }
            L_0x0066:
                java.util.LinkedList r2 = r5.waitingThreads     // Catch:{ all -> 0x0078 }
                java.lang.Object r2 = r2.removeFirst()     // Catch:{ all -> 0x0078 }
                r0 = r2
                org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$WaitingThread r0 = (org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.WaitingThread) r0     // Catch:{ all -> 0x0078 }
                r1 = r0
                org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$HostConnectionPool r2 = r1.hostConnectionPool     // Catch:{ all -> 0x0078 }
                java.util.LinkedList r2 = r2.waitingThreads     // Catch:{ all -> 0x0078 }
                r2.remove(r1)     // Catch:{ all -> 0x0078 }
                goto L_0x003f
            L_0x0078:
                r2 = move-exception
                monitor-exit(r5)
                throw r2
            L_0x007b:
                org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0078 }
                boolean r2 = r2.isDebugEnabled()     // Catch:{ all -> 0x0078 }
                if (r2 == 0) goto L_0x003f
                org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.access$700()     // Catch:{ all -> 0x0078 }
                java.lang.String r3 = "Notifying no-one, there are no waiting threads"
                r2.debug(r3)     // Catch:{ all -> 0x0078 }
                goto L_0x003f
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.MultiThreadedHttpConnectionManager.ConnectionPool.notifyWaitingThread(org.apache.commons.httpclient.MultiThreadedHttpConnectionManager$HostConnectionPool):void");
        }

        public void freeConnection(HttpConnection conn) {
            HostConfiguration connectionConfiguration = MultiThreadedHttpConnectionManager.access$1100(this.this$0, conn);
            if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer().append("Freeing connection, hostConfig=").append(connectionConfiguration).toString());
            }
            synchronized (this) {
                if (MultiThreadedHttpConnectionManager.access$1200(this.this$0)) {
                    conn.close();
                    return;
                }
                HostConnectionPool hostPool = getHostPool(connectionConfiguration, true);
                hostPool.freeConnections.add(conn);
                if (hostPool.numConnections == 0) {
                    MultiThreadedHttpConnectionManager.access$700().error(new StringBuffer().append("Host connection pool not found, hostConfig=").append(connectionConfiguration).toString());
                    hostPool.numConnections = 1;
                }
                this.freeConnections.add(conn);
                MultiThreadedHttpConnectionManager.access$1300((HttpConnectionWithReference) conn);
                if (this.numConnections == 0) {
                    MultiThreadedHttpConnectionManager.access$700().error(new StringBuffer().append("Host connection pool not found, hostConfig=").append(connectionConfiguration).toString());
                    this.numConnections = 1;
                }
                this.idleConnectionHandler.add(conn);
                notifyWaitingThread(hostPool);
            }
        }
    }

    private static class ConnectionSource {
        public ConnectionPool connectionPool;
        public HostConfiguration hostConfiguration;

        private ConnectionSource() {
        }

        ConnectionSource(AnonymousClass1 x0) {
            this();
        }
    }

    private static class HostConnectionPool {
        public LinkedList freeConnections;
        public HostConfiguration hostConfiguration;
        public int numConnections;
        public LinkedList waitingThreads;

        private HostConnectionPool() {
            this.freeConnections = new LinkedList();
            this.waitingThreads = new LinkedList();
            this.numConnections = 0;
        }

        HostConnectionPool(AnonymousClass1 x0) {
            this();
        }
    }

    private static class WaitingThread {
        public HostConnectionPool hostConnectionPool;
        public boolean interruptedByConnectionPool;
        public Thread thread;

        private WaitingThread() {
            this.interruptedByConnectionPool = false;
        }

        WaitingThread(AnonymousClass1 x0) {
            this();
        }
    }

    private static class ReferenceQueueThread extends Thread {
        private volatile boolean shutdown = false;

        public ReferenceQueueThread() {
            setDaemon(true);
            setName("MultiThreadedHttpConnectionManager cleanup");
        }

        public void shutdown() {
            this.shutdown = true;
            interrupt();
        }

        private void handleReference(Reference ref) {
            ConnectionSource source;
            synchronized (MultiThreadedHttpConnectionManager.access$1400()) {
                source = (ConnectionSource) MultiThreadedHttpConnectionManager.access$1400().remove(ref);
            }
            if (source != null) {
                if (MultiThreadedHttpConnectionManager.access$700().isDebugEnabled()) {
                    MultiThreadedHttpConnectionManager.access$700().debug(new StringBuffer().append("Connection reclaimed by garbage collector, hostConfig=").append(source.hostConfiguration).toString());
                }
                source.connectionPool.handleLostConnection(source.hostConfiguration);
            }
        }

        public void run() {
            while (!this.shutdown) {
                try {
                    Reference ref = MultiThreadedHttpConnectionManager.access$1500().remove();
                    if (ref != null) {
                        handleReference(ref);
                    }
                } catch (InterruptedException e) {
                    MultiThreadedHttpConnectionManager.access$700().debug("ReferenceQueueThread interrupted", e);
                }
            }
        }
    }

    private static class HttpConnectionWithReference extends HttpConnection {
        public WeakReference reference = new WeakReference(this, MultiThreadedHttpConnectionManager.access$1500());

        public HttpConnectionWithReference(HostConfiguration hostConfiguration) {
            super(hostConfiguration);
        }
    }

    private static class HttpConnectionAdapter extends HttpConnection {
        private HttpConnection wrappedConnection;

        public HttpConnectionAdapter(HttpConnection connection) {
            super(connection.getHost(), connection.getPort(), connection.getProtocol());
            this.wrappedConnection = connection;
        }

        /* access modifiers changed from: protected */
        public boolean hasConnection() {
            return this.wrappedConnection != null;
        }

        /* access modifiers changed from: package-private */
        public HttpConnection getWrappedConnection() {
            return this.wrappedConnection;
        }

        public void close() {
            if (hasConnection()) {
                this.wrappedConnection.close();
            }
        }

        public InetAddress getLocalAddress() {
            if (hasConnection()) {
                return this.wrappedConnection.getLocalAddress();
            }
            return null;
        }

        public boolean isStaleCheckingEnabled() {
            if (hasConnection()) {
                return this.wrappedConnection.isStaleCheckingEnabled();
            }
            return false;
        }

        public void setLocalAddress(InetAddress localAddress) {
            if (hasConnection()) {
                this.wrappedConnection.setLocalAddress(localAddress);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setStaleCheckingEnabled(boolean staleCheckEnabled) {
            if (hasConnection()) {
                this.wrappedConnection.setStaleCheckingEnabled(staleCheckEnabled);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String getHost() {
            if (hasConnection()) {
                return this.wrappedConnection.getHost();
            }
            return null;
        }

        public HttpConnectionManager getHttpConnectionManager() {
            if (hasConnection()) {
                return this.wrappedConnection.getHttpConnectionManager();
            }
            return null;
        }

        public InputStream getLastResponseInputStream() {
            if (hasConnection()) {
                return this.wrappedConnection.getLastResponseInputStream();
            }
            return null;
        }

        public int getPort() {
            if (hasConnection()) {
                return this.wrappedConnection.getPort();
            }
            return -1;
        }

        public Protocol getProtocol() {
            if (hasConnection()) {
                return this.wrappedConnection.getProtocol();
            }
            return null;
        }

        public String getProxyHost() {
            if (hasConnection()) {
                return this.wrappedConnection.getProxyHost();
            }
            return null;
        }

        public int getProxyPort() {
            if (hasConnection()) {
                return this.wrappedConnection.getProxyPort();
            }
            return -1;
        }

        public OutputStream getRequestOutputStream() throws IOException, IllegalStateException {
            if (hasConnection()) {
                return this.wrappedConnection.getRequestOutputStream();
            }
            return null;
        }

        public InputStream getResponseInputStream() throws IOException, IllegalStateException {
            if (hasConnection()) {
                return this.wrappedConnection.getResponseInputStream();
            }
            return null;
        }

        public boolean isOpen() {
            if (hasConnection()) {
                return this.wrappedConnection.isOpen();
            }
            return false;
        }

        public boolean closeIfStale() throws IOException {
            if (hasConnection()) {
                return this.wrappedConnection.closeIfStale();
            }
            return false;
        }

        public boolean isProxied() {
            if (hasConnection()) {
                return this.wrappedConnection.isProxied();
            }
            return false;
        }

        public boolean isResponseAvailable() throws IOException {
            if (hasConnection()) {
                return this.wrappedConnection.isResponseAvailable();
            }
            return false;
        }

        public boolean isResponseAvailable(int timeout) throws IOException {
            if (hasConnection()) {
                return this.wrappedConnection.isResponseAvailable(timeout);
            }
            return false;
        }

        public boolean isSecure() {
            if (hasConnection()) {
                return this.wrappedConnection.isSecure();
            }
            return false;
        }

        public boolean isTransparent() {
            if (hasConnection()) {
                return this.wrappedConnection.isTransparent();
            }
            return false;
        }

        public void open() throws IOException {
            if (hasConnection()) {
                this.wrappedConnection.open();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void print(String data) throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.print(data);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void printLine() throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.printLine();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void printLine(String data) throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.printLine(data);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String readLine() throws IOException, IllegalStateException {
            if (hasConnection()) {
                return this.wrappedConnection.readLine();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String readLine(String charset) throws IOException, IllegalStateException {
            if (hasConnection()) {
                return this.wrappedConnection.readLine(charset);
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void releaseConnection() {
            if (!isLocked() && hasConnection()) {
                HttpConnection wrappedConnection2 = this.wrappedConnection;
                this.wrappedConnection = null;
                wrappedConnection2.releaseConnection();
            }
        }

        public void setConnectionTimeout(int timeout) {
            if (hasConnection()) {
                this.wrappedConnection.setConnectionTimeout(timeout);
            }
        }

        public void setHost(String host) throws IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.setHost(host);
            }
        }

        public void setHttpConnectionManager(HttpConnectionManager httpConnectionManager) {
            if (hasConnection()) {
                this.wrappedConnection.setHttpConnectionManager(httpConnectionManager);
            }
        }

        public void setLastResponseInputStream(InputStream inStream) {
            if (hasConnection()) {
                this.wrappedConnection.setLastResponseInputStream(inStream);
            }
        }

        public void setPort(int port) throws IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.setPort(port);
            }
        }

        public void setProtocol(Protocol protocol) {
            if (hasConnection()) {
                this.wrappedConnection.setProtocol(protocol);
            }
        }

        public void setProxyHost(String host) throws IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.setProxyHost(host);
            }
        }

        public void setProxyPort(int port) throws IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.setProxyPort(port);
            }
        }

        public void setSoTimeout(int timeout) throws SocketException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.setSoTimeout(timeout);
            }
        }

        public void shutdownOutput() {
            if (hasConnection()) {
                this.wrappedConnection.shutdownOutput();
            }
        }

        public void tunnelCreated() throws IllegalStateException, IOException {
            if (hasConnection()) {
                this.wrappedConnection.tunnelCreated();
            }
        }

        public void write(byte[] data, int offset, int length) throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.write(data, offset, length);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void write(byte[] data) throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.write(data);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void writeLine() throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.writeLine();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void writeLine(byte[] data) throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.writeLine(data);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void flushRequestOutputStream() throws IOException {
            if (hasConnection()) {
                this.wrappedConnection.flushRequestOutputStream();
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public int getSoTimeout() throws SocketException {
            if (hasConnection()) {
                return this.wrappedConnection.getSoTimeout();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public String getVirtualHost() {
            if (hasConnection()) {
                return this.wrappedConnection.getVirtualHost();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setVirtualHost(String host) throws IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.setVirtualHost(host);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public int getSendBufferSize() throws SocketException {
            if (hasConnection()) {
                return this.wrappedConnection.getSendBufferSize();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setSendBufferSize(int sendBufferSize) throws SocketException {
            if (hasConnection()) {
                this.wrappedConnection.setSendBufferSize(sendBufferSize);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public HttpConnectionParams getParams() {
            if (hasConnection()) {
                return this.wrappedConnection.getParams();
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setParams(HttpConnectionParams params) {
            if (hasConnection()) {
                this.wrappedConnection.setParams(params);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void print(String data, String charset) throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.print(data, charset);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void printLine(String data, String charset) throws IOException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.printLine(data, charset);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }

        public void setSocketTimeout(int timeout) throws SocketException, IllegalStateException {
            if (hasConnection()) {
                this.wrappedConnection.setSocketTimeout(timeout);
                return;
            }
            throw new IllegalStateException("Connection has been released");
        }
    }
}
