package org.apache.commons.httpclient.protocol;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.util.TimeoutController;

public final class ControllerThreadSocketFactory {
    private ControllerThreadSocketFactory() {
    }

    public static Socket createSocket(ProtocolSocketFactory socketfactory, String host, int port, InetAddress localAddress, int localPort, int timeout) throws IOException, UnknownHostException, ConnectTimeoutException {
        SocketTask task = new SocketTask(socketfactory, host, port, localAddress, localPort) {
            private final String val$host;
            private final InetAddress val$localAddress;
            private final int val$localPort;
            private final int val$port;
            private final ProtocolSocketFactory val$socketfactory;

            {
                this.val$socketfactory = r1;
                this.val$host = r2;
                this.val$port = r3;
                this.val$localAddress = r4;
                this.val$localPort = r5;
            }

            public void doit() throws IOException {
                setSocket(this.val$socketfactory.createSocket(this.val$host, this.val$port, this.val$localAddress, this.val$localPort));
            }
        };
        try {
            TimeoutController.execute(task, (long) timeout);
            Socket socket = task.getSocket();
            if (SocketTask.access$000(task) == null) {
                return socket;
            }
            throw SocketTask.access$000(task);
        } catch (TimeoutController.TimeoutException e) {
            throw new ConnectTimeoutException(new StringBuffer().append("The host did not accept the connection within timeout of ").append(timeout).append(" ms").toString());
        }
    }

    public static Socket createSocket(SocketTask task, int timeout) throws IOException, UnknownHostException, ConnectTimeoutException {
        try {
            TimeoutController.execute(task, (long) timeout);
            Socket socket = task.getSocket();
            if (SocketTask.access$000(task) == null) {
                return socket;
            }
            throw SocketTask.access$000(task);
        } catch (TimeoutController.TimeoutException e) {
            throw new ConnectTimeoutException(new StringBuffer().append("The host did not accept the connection within timeout of ").append(timeout).append(" ms").toString());
        }
    }

    public static abstract class SocketTask implements Runnable {
        private IOException exception;
        private Socket socket;

        public abstract void doit() throws IOException;

        static IOException access$000(SocketTask x0) {
            return x0.exception;
        }

        /* access modifiers changed from: protected */
        public void setSocket(Socket newSocket) {
            this.socket = newSocket;
        }

        /* access modifiers changed from: protected */
        public Socket getSocket() {
            return this.socket;
        }

        public void run() {
            try {
                doit();
            } catch (IOException e) {
                this.exception = e;
            }
        }
    }
}
