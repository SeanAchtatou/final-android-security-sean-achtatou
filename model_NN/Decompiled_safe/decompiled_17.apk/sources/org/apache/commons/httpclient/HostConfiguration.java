package org.apache.commons.httpclient;

import java.net.InetAddress;
import org.apache.commons.httpclient.params.HostParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.LangUtils;

public class HostConfiguration implements Cloneable {
    public static final HostConfiguration ANY_HOST_CONFIGURATION = new HostConfiguration();
    private HttpHost host = null;
    private InetAddress localAddress = null;
    private HostParams params = new HostParams();
    private ProxyHost proxyHost = null;

    public HostConfiguration() {
    }

    public HostConfiguration(HostConfiguration hostConfiguration) {
        init(hostConfiguration);
    }

    private void init(HostConfiguration hostConfiguration) {
        synchronized (hostConfiguration) {
            try {
                if (hostConfiguration.host != null) {
                    this.host = (HttpHost) hostConfiguration.host.clone();
                } else {
                    this.host = null;
                }
                if (hostConfiguration.proxyHost != null) {
                    this.proxyHost = (ProxyHost) hostConfiguration.proxyHost.clone();
                } else {
                    this.proxyHost = null;
                }
                this.localAddress = hostConfiguration.getLocalAddress();
                this.params = (HostParams) hostConfiguration.getParams().clone();
            } catch (CloneNotSupportedException e) {
                throw new IllegalArgumentException("Host configuration could not be cloned");
            }
        }
    }

    public Object clone() {
        try {
            HostConfiguration copy = (HostConfiguration) super.clone();
            copy.init(this);
            return copy;
        } catch (CloneNotSupportedException e) {
            throw new IllegalArgumentException("Host configuration could not be cloned");
        }
    }

    public synchronized String toString() {
        StringBuffer b;
        boolean appendComma = false;
        b = new StringBuffer(50);
        b.append("HostConfiguration[");
        if (this.host != null) {
            appendComma = true;
            b.append("host=").append(this.host);
        }
        if (this.proxyHost != null) {
            if (appendComma) {
                b.append(", ");
            } else {
                appendComma = true;
            }
            b.append("proxyHost=").append(this.proxyHost);
        }
        if (this.localAddress != null) {
            if (appendComma) {
                b.append(", ");
            } else {
                appendComma = true;
            }
            b.append("localAddress=").append(this.localAddress);
            if (appendComma) {
                b.append(", ");
            }
            b.append("params=").append(this.params);
        }
        b.append("]");
        return b.toString();
    }

    public synchronized boolean hostEquals(HttpConnection connection) {
        boolean z = false;
        synchronized (this) {
            if (connection == null) {
                throw new IllegalArgumentException("Connection may not be null");
            } else if (this.host != null && this.host.getHostName().equalsIgnoreCase(connection.getHost())) {
                if (this.host.getPort() == connection.getPort() && this.host.getProtocol().equals(connection.getProtocol()) && (this.localAddress == null ? connection.getLocalAddress() == null : this.localAddress.equals(connection.getLocalAddress()))) {
                    z = true;
                }
            }
        }
        return z;
    }

    public synchronized boolean proxyEquals(HttpConnection connection) {
        boolean z = true;
        synchronized (this) {
            if (connection == null) {
                throw new IllegalArgumentException("Connection may not be null");
            } else if (this.proxyHost != null) {
                if (!this.proxyHost.getHostName().equalsIgnoreCase(connection.getProxyHost()) || this.proxyHost.getPort() != connection.getProxyPort()) {
                    z = false;
                }
            } else if (connection.getProxyHost() != null) {
                z = false;
            }
        }
        return z;
    }

    public synchronized boolean isHostSet() {
        return this.host != null;
    }

    public synchronized void setHost(HttpHost host2) {
        this.host = host2;
    }

    public synchronized void setHost(String host2, int port, String protocol) {
        this.host = new HttpHost(host2, port, Protocol.getProtocol(protocol));
    }

    public synchronized void setHost(String host2, String virtualHost, int port, Protocol protocol) {
        setHost(host2, port, protocol);
        this.params.setVirtualHost(virtualHost);
    }

    public synchronized void setHost(String host2, int port, Protocol protocol) {
        if (host2 == null) {
            throw new IllegalArgumentException("host must not be null");
        } else if (protocol == null) {
            throw new IllegalArgumentException("protocol must not be null");
        } else {
            this.host = new HttpHost(host2, port, protocol);
        }
    }

    public synchronized void setHost(String host2, int port) {
        setHost(host2, port, Protocol.getProtocol("http"));
    }

    public synchronized void setHost(String host2) {
        Protocol defaultProtocol = Protocol.getProtocol("http");
        setHost(host2, defaultProtocol.getDefaultPort(), defaultProtocol);
    }

    public synchronized void setHost(URI uri) {
        try {
            setHost(uri.getHost(), uri.getPort(), uri.getScheme());
        } catch (URIException e) {
            throw new IllegalArgumentException(e.toString());
        }
    }

    public synchronized String getHostURL() {
        if (this.host == null) {
            throw new IllegalStateException("Host must be set to create a host URL");
        }
        return this.host.toURI();
    }

    public synchronized String getHost() {
        String str;
        if (this.host != null) {
            str = this.host.getHostName();
        } else {
            str = null;
        }
        return str;
    }

    public synchronized String getVirtualHost() {
        return this.params.getVirtualHost();
    }

    public synchronized int getPort() {
        int i;
        if (this.host != null) {
            i = this.host.getPort();
        } else {
            i = -1;
        }
        return i;
    }

    public synchronized Protocol getProtocol() {
        Protocol protocol;
        if (this.host != null) {
            protocol = this.host.getProtocol();
        } else {
            protocol = null;
        }
        return protocol;
    }

    public synchronized boolean isProxySet() {
        return this.proxyHost != null;
    }

    public synchronized void setProxyHost(ProxyHost proxyHost2) {
        this.proxyHost = proxyHost2;
    }

    public synchronized void setProxy(String proxyHost2, int proxyPort) {
        this.proxyHost = new ProxyHost(proxyHost2, proxyPort);
    }

    public synchronized String getProxyHost() {
        String str;
        if (this.proxyHost != null) {
            str = this.proxyHost.getHostName();
        } else {
            str = null;
        }
        return str;
    }

    public synchronized int getProxyPort() {
        int i;
        if (this.proxyHost != null) {
            i = this.proxyHost.getPort();
        } else {
            i = -1;
        }
        return i;
    }

    public synchronized void setLocalAddress(InetAddress localAddress2) {
        this.localAddress = localAddress2;
    }

    public synchronized InetAddress getLocalAddress() {
        return this.localAddress;
    }

    public HostParams getParams() {
        return this.params;
    }

    public void setParams(HostParams params2) {
        if (params2 == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = params2;
    }

    public synchronized boolean equals(Object o) {
        boolean z = true;
        synchronized (this) {
            if (!(o instanceof HostConfiguration)) {
                z = false;
            } else if (o != this) {
                HostConfiguration that = (HostConfiguration) o;
                if (!LangUtils.equals(this.host, that.host) || !LangUtils.equals(this.proxyHost, that.proxyHost) || !LangUtils.equals(this.localAddress, that.localAddress)) {
                    z = false;
                }
            }
        }
        return z;
    }

    public synchronized int hashCode() {
        return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(17, this.host), this.proxyHost), this.localAddress);
    }
}
