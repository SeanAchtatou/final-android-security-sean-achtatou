package org.apache.commons.httpclient.protocol;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.apache.commons.httpclient.ConnectTimeoutException;

public final class ReflectionSocketFactory {
    private static Constructor INETSOCKETADDRESS_CONSTRUCTOR = null;
    private static boolean REFLECTION_FAILED = false;
    private static Method SOCKETBIND_METHOD = null;
    private static Method SOCKETCONNECT_METHOD = null;
    private static Class SOCKETTIMEOUTEXCEPTION_CLASS = null;
    static Class class$java$net$InetAddress;
    static Class class$java$net$Socket;

    private ReflectionSocketFactory() {
    }

    public static Socket createSocket(String socketfactoryName, String host, int port, InetAddress localAddress, int localPort, int timeout) throws IOException, UnknownHostException, ConnectTimeoutException {
        Class cls;
        Class cls2;
        Class cls3;
        if (REFLECTION_FAILED) {
            return null;
        }
        try {
            Class socketfactoryClass = Class.forName(socketfactoryName);
            Socket socket = (Socket) socketfactoryClass.getMethod("createSocket", new Class[0]).invoke(socketfactoryClass.getMethod("getDefault", new Class[0]).invoke(null, new Object[0]), new Object[0]);
            if (INETSOCKETADDRESS_CONSTRUCTOR == null) {
                Class addressClass = Class.forName("java.net.InetSocketAddress");
                Class[] clsArr = new Class[2];
                if (class$java$net$InetAddress == null) {
                    cls3 = class$("java.net.InetAddress");
                    class$java$net$InetAddress = cls3;
                } else {
                    cls3 = class$java$net$InetAddress;
                }
                clsArr[0] = cls3;
                clsArr[1] = Integer.TYPE;
                INETSOCKETADDRESS_CONSTRUCTOR = addressClass.getConstructor(clsArr);
            }
            Object remoteaddr = INETSOCKETADDRESS_CONSTRUCTOR.newInstance(InetAddress.getByName(host), new Integer(port));
            Object localaddr = INETSOCKETADDRESS_CONSTRUCTOR.newInstance(localAddress, new Integer(localPort));
            if (SOCKETCONNECT_METHOD == null) {
                if (class$java$net$Socket == null) {
                    cls2 = class$("java.net.Socket");
                    class$java$net$Socket = cls2;
                } else {
                    cls2 = class$java$net$Socket;
                }
                SOCKETCONNECT_METHOD = cls2.getMethod("connect", Class.forName("java.net.SocketAddress"), Integer.TYPE);
            }
            if (SOCKETBIND_METHOD == null) {
                if (class$java$net$Socket == null) {
                    cls = class$("java.net.Socket");
                    class$java$net$Socket = cls;
                } else {
                    cls = class$java$net$Socket;
                }
                SOCKETBIND_METHOD = cls.getMethod("bind", Class.forName("java.net.SocketAddress"));
            }
            SOCKETBIND_METHOD.invoke(socket, localaddr);
            SOCKETCONNECT_METHOD.invoke(socket, remoteaddr, new Integer(timeout));
            return socket;
        } catch (InvocationTargetException e) {
            Throwable cause = e.getTargetException();
            if (SOCKETTIMEOUTEXCEPTION_CLASS == null) {
                try {
                    SOCKETTIMEOUTEXCEPTION_CLASS = Class.forName("java.net.SocketTimeoutException");
                } catch (ClassNotFoundException e2) {
                    REFLECTION_FAILED = true;
                    return null;
                }
            }
            if (SOCKETTIMEOUTEXCEPTION_CLASS.isInstance(cause)) {
                throw new ConnectTimeoutException(new StringBuffer().append("The host did not accept the connection within timeout of ").append(timeout).append(" ms").toString(), cause);
            } else if (!(cause instanceof IOException)) {
                return null;
            } else {
                throw ((IOException) cause);
            }
        } catch (Exception e3) {
            REFLECTION_FAILED = true;
            return null;
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }
}
