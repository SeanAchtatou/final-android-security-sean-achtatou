package org.jivesoftware.smack;

import java.util.concurrent.ThreadFactory;

class a implements ThreadFactory {
    final /* synthetic */ BOSHConnection a;

    a(BOSHConnection bOSHConnection) {
        this.a = bOSHConnection;
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "Smack Listener Processor (" + this.a.o + ")");
    }
}
