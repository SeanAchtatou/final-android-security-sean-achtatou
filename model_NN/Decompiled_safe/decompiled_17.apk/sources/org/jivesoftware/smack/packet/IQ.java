package org.jivesoftware.smack.packet;

import android.os.Bundle;

public class IQ extends Packet {
    private Type a = Type.a;

    public static class Type {
        public static final Type a = new Type("get");
        public static final Type b = new Type("set");
        public static final Type c = new Type("result");
        public static final Type d = new Type("error");
        private String e;

        private Type(String str) {
            this.e = str;
        }

        public static Type a(String str) {
            if (str != null) {
                String lowerCase = str.toLowerCase();
                if (a.toString().equals(lowerCase)) {
                    return a;
                }
                if (b.toString().equals(lowerCase)) {
                    return b;
                }
                if (d.toString().equals(lowerCase)) {
                    return d;
                }
                if (c.toString().equals(lowerCase)) {
                    return c;
                }
            }
            return null;
        }

        public String toString() {
            return this.e;
        }
    }

    public IQ() {
    }

    public IQ(Bundle bundle) {
        super(bundle);
        if (bundle.containsKey("ext_iq_type")) {
            this.a = Type.a(bundle.getString("ext_iq_type"));
        }
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("<iq ");
        if (k() != null) {
            sb.append("id=\"" + k() + "\" ");
        }
        if (m() != null) {
            sb.append("to=\"").append(StringUtils.a(m())).append("\" ");
        }
        if (n() != null) {
            sb.append("from=\"").append(StringUtils.a(n())).append("\" ");
        }
        if (l() != null) {
            sb.append("chid=\"").append(StringUtils.a(l())).append("\" ");
        }
        if (this.a == null) {
            sb.append("type=\"get\">");
        } else {
            sb.append("type=\"").append(c()).append("\">");
        }
        String b = b();
        if (b != null) {
            sb.append(b);
        }
        sb.append(r());
        XMPPError o = o();
        if (o != null) {
            sb.append(o.d());
        }
        sb.append("</iq>");
        return sb.toString();
    }

    public void a(Type type) {
        if (type == null) {
            this.a = Type.a;
        } else {
            this.a = type;
        }
    }

    public String b() {
        return null;
    }

    public Type c() {
        return this.a;
    }

    public Bundle d() {
        Bundle d = super.d();
        if (this.a != null) {
            d.putString("ext_iq_type", this.a.toString());
        }
        return d;
    }
}
