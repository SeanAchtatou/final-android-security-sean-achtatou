package org.jivesoftware.smack.packet;

import android.os.Bundle;

public class Presence extends Packet {
    private Type a = Type.available;
    private String d = null;
    private int e = Integer.MIN_VALUE;
    private Mode f = null;

    public enum Mode {
        chat,
        available,
        away,
        xa,
        dnd
    }

    public enum Type {
        available,
        unavailable,
        subscribe,
        subscribed,
        unsubscribe,
        unsubscribed,
        error,
        probe
    }

    public Presence(Bundle bundle) {
        super(bundle);
        if (bundle.containsKey("ext_pres_type")) {
            this.a = Type.valueOf(bundle.getString("ext_pres_type"));
        }
        if (bundle.containsKey("ext_pres_status")) {
            this.d = bundle.getString("ext_pres_status");
        }
        if (bundle.containsKey("ext_pres_prio")) {
            this.e = bundle.getInt("ext_pres_prio");
        }
        if (bundle.containsKey("ext_pres_mode")) {
            this.f = Mode.valueOf(bundle.getString("ext_pres_mode"));
        }
    }

    public Presence(Type type) {
        a(type);
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("<presence");
        if (s() != null) {
            sb.append(" xmlns=\"").append(s()).append("\"");
        }
        if (k() != null) {
            sb.append(" id=\"").append(k()).append("\"");
        }
        if (m() != null) {
            sb.append(" to=\"").append(StringUtils.a(m())).append("\"");
        }
        if (n() != null) {
            sb.append(" from=\"").append(StringUtils.a(n())).append("\"");
        }
        if (l() != null) {
            sb.append(" chid=\"").append(StringUtils.a(l())).append("\"");
        }
        if (this.a != null) {
            sb.append(" type=\"").append(this.a).append("\"");
        }
        sb.append(">");
        if (this.d != null) {
            sb.append("<status>").append(StringUtils.a(this.d)).append("</status>");
        }
        if (this.e != Integer.MIN_VALUE) {
            sb.append("<priority>").append(this.e).append("</priority>");
        }
        if (!(this.f == null || this.f == Mode.available)) {
            sb.append("<show>").append(this.f).append("</show>");
        }
        sb.append(r());
        XMPPError o = o();
        if (o != null) {
            sb.append(o.d());
        }
        sb.append("</presence>");
        return sb.toString();
    }

    public void a(int i) {
        if (i < -128 || i > 128) {
            throw new IllegalArgumentException("Priority value " + i + " is not valid. Valid range is -128 through 128.");
        }
        this.e = i;
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(Mode mode) {
        this.f = mode;
    }

    public void a(Type type) {
        if (type == null) {
            throw new NullPointerException("Type cannot be null");
        }
        this.a = type;
    }

    public Bundle d() {
        Bundle d2 = super.d();
        if (this.a != null) {
            d2.putString("ext_pres_type", this.a.toString());
        }
        if (this.d != null) {
            d2.putString("ext_pres_status", this.d);
        }
        if (this.e != Integer.MIN_VALUE) {
            d2.putInt("ext_pres_prio", this.e);
        }
        if (!(this.f == null || this.f == Mode.available)) {
            d2.putString("ext_pres_mode", this.f.toString());
        }
        return d2;
    }
}
