package org.jivesoftware.smack.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.StringUtils;

public class Authentication extends IQ {
    private String a = null;
    private String d = null;
    private String e = null;
    private String f = null;

    public Authentication() {
        a(IQ.Type.b);
    }

    public void a(String str) {
        this.a = str;
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"jabber:iq:auth\">");
        if (this.a != null) {
            if (this.a.equals("")) {
                sb.append("<username/>");
            } else {
                sb.append("<username>").append(this.a).append("</username>");
            }
        }
        if (this.e != null) {
            if (this.e.equals("")) {
                sb.append("<digest/>");
            } else {
                sb.append("<digest>").append(this.e).append("</digest>");
            }
        }
        if (this.d != null && this.e == null) {
            if (this.d.equals("")) {
                sb.append("<password/>");
            } else {
                sb.append("<password>").append(StringUtils.a(this.d)).append("</password>");
            }
        }
        if (this.f != null) {
            if (this.f.equals("")) {
                sb.append("<resource/>");
            } else {
                sb.append("<resource>").append(this.f).append("</resource>");
            }
        }
        sb.append("</query>");
        return sb.toString();
    }

    public void b(String str) {
        this.d = str;
    }

    public void c(String str) {
        this.e = str;
    }

    public void d(String str) {
        this.f = str;
    }
}
