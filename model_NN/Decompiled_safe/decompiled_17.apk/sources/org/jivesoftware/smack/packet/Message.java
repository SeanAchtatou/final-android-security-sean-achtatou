package org.jivesoftware.smack.packet;

import android.os.Bundle;
import android.text.TextUtils;

public class Message extends Packet {
    private String a = null;
    private String d = null;
    private String e;
    private String f;
    private String g;
    private boolean h = false;
    private String i;
    private String j = "";
    private String k = "";
    private String l = "";
    private String m = "";
    private boolean n = false;

    public Message() {
    }

    public Message(Bundle bundle) {
        super(bundle);
        this.a = bundle.getString("ext_msg_type");
        this.e = bundle.getString("ext_msg_lang");
        this.d = bundle.getString("ext_msg_thread");
        this.f = bundle.getString("ext_msg_sub");
        this.g = bundle.getString("ext_msg_body");
        this.i = bundle.getString("ext_msg_appid");
        this.h = bundle.getBoolean("ext_msg_trans", false);
        this.j = bundle.getString("ext_msg_seq");
        this.k = bundle.getString("ext_msg_mseq");
        this.l = bundle.getString("ext_msg_fseq");
        this.m = bundle.getString("ext_msg_status");
    }

    public String a() {
        XMPPError o;
        StringBuilder sb = new StringBuilder();
        sb.append("<message");
        if (s() != null) {
            sb.append(" xmlns=\"").append(s()).append("\"");
        }
        if (this.e != null) {
            sb.append(" xml:lang=\"").append(i()).append("\"");
        }
        if (k() != null) {
            sb.append(" id=\"").append(k()).append("\"");
        }
        if (m() != null) {
            sb.append(" to=\"").append(StringUtils.a(m())).append("\"");
        }
        if (!TextUtils.isEmpty(e())) {
            sb.append(" seq=\"").append(e()).append("\"");
        }
        if (!TextUtils.isEmpty(f())) {
            sb.append(" mseq=\"").append(f()).append("\"");
        }
        if (!TextUtils.isEmpty(g())) {
            sb.append(" fseq=\"").append(g()).append("\"");
        }
        if (!TextUtils.isEmpty(h())) {
            sb.append(" status=\"").append(h()).append("\"");
        }
        if (n() != null) {
            sb.append(" from=\"").append(StringUtils.a(n())).append("\"");
        }
        if (l() != null) {
            sb.append(" chid=\"").append(StringUtils.a(l())).append("\"");
        }
        if (this.h) {
            sb.append(" transient=\"true\"");
        }
        if (!TextUtils.isEmpty(this.i)) {
            sb.append(" appid=\"").append(c()).append("\"");
        }
        if (!TextUtils.isEmpty(this.a)) {
            sb.append(" type=\"").append(this.a).append("\"");
        }
        if (this.n) {
            sb.append(" s=\"1\"");
        }
        sb.append(">");
        if (this.f != null) {
            sb.append("<subject>").append(StringUtils.a(this.f));
            sb.append("</subject>");
        }
        if (this.g != null) {
            sb.append("<body>").append(StringUtils.a(this.g)).append("</body>");
        }
        if (this.d != null) {
            sb.append("<thread>").append(this.d).append("</thread>");
        }
        if ("error".equalsIgnoreCase(this.a) && (o = o()) != null) {
            sb.append(o.d());
        }
        sb.append(r());
        sb.append("</message>");
        return sb.toString();
    }

    public void a(String str) {
        this.i = str;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public String b() {
        return this.a;
    }

    public void b(String str) {
        this.j = str;
    }

    public void b(boolean z) {
        this.n = z;
    }

    public String c() {
        return this.i;
    }

    public void c(String str) {
        this.k = str;
    }

    public Bundle d() {
        Bundle d2 = super.d();
        if (!TextUtils.isEmpty(this.a)) {
            d2.putString("ext_msg_type", this.a);
        }
        if (this.e != null) {
            d2.putString("ext_msg_lang", this.e);
        }
        if (this.f != null) {
            d2.putString("ext_msg_sub", this.f);
        }
        if (this.g != null) {
            d2.putString("ext_msg_body", this.g);
        }
        if (this.d != null) {
            d2.putString("ext_msg_thread", this.d);
        }
        if (this.i != null) {
            d2.putString("ext_msg_appid", this.i);
        }
        if (this.h) {
            d2.putBoolean("ext_msg_trans", true);
        }
        if (!TextUtils.isEmpty(this.j)) {
            d2.putString("ext_msg_seq", this.j);
        }
        if (!TextUtils.isEmpty(this.k)) {
            d2.putString("ext_msg_mseq", this.k);
        }
        if (!TextUtils.isEmpty(this.l)) {
            d2.putString("ext_msg_fseq", this.l);
        }
        if (!TextUtils.isEmpty(this.m)) {
            d2.putString("ext_msg_status", this.m);
        }
        return d2;
    }

    public void d(String str) {
        this.l = str;
    }

    public String e() {
        return this.j;
    }

    public void e(String str) {
        this.m = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (r4.g.equals(r5.g) != false) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r4.e.equals(r5.e) != false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0042, code lost:
        if (r4.f.equals(r5.f) != false) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0050, code lost:
        if (r4.d.equals(r5.d) != false) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r1 = 0
            r0 = 1
            if (r4 != r5) goto L_0x0006
        L_0x0004:
            r1 = r0
        L_0x0005:
            return r1
        L_0x0006:
            if (r5 == 0) goto L_0x006e
            java.lang.Class r2 = r4.getClass()
            java.lang.Class r3 = r5.getClass()
            if (r2 != r3) goto L_0x006e
            org.jivesoftware.smack.packet.Message r5 = (org.jivesoftware.smack.packet.Message) r5
            boolean r2 = super.equals(r5)
            if (r2 == 0) goto L_0x006e
            java.lang.String r2 = r4.g
            if (r2 == 0) goto L_0x005a
            java.lang.String r2 = r4.g
            java.lang.String r3 = r5.g
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x006e
        L_0x0028:
            java.lang.String r2 = r4.e
            if (r2 == 0) goto L_0x005f
            java.lang.String r2 = r4.e
            java.lang.String r3 = r5.e
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x006e
        L_0x0036:
            java.lang.String r2 = r4.f
            if (r2 == 0) goto L_0x0064
            java.lang.String r2 = r4.f
            java.lang.String r3 = r5.f
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x006e
        L_0x0044:
            java.lang.String r2 = r4.d
            if (r2 == 0) goto L_0x0069
            java.lang.String r2 = r4.d
            java.lang.String r3 = r5.d
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x006e
        L_0x0052:
            java.lang.String r2 = r4.a
            java.lang.String r3 = r5.a
            if (r2 != r3) goto L_0x0005
            r1 = r0
            goto L_0x0005
        L_0x005a:
            java.lang.String r2 = r5.g
            if (r2 == 0) goto L_0x0028
            goto L_0x0005
        L_0x005f:
            java.lang.String r2 = r5.e
            if (r2 == 0) goto L_0x0036
            goto L_0x0005
        L_0x0064:
            java.lang.String r2 = r5.f
            if (r2 == 0) goto L_0x0044
            goto L_0x0005
        L_0x0069:
            java.lang.String r2 = r5.d
            if (r2 == 0) goto L_0x0052
            goto L_0x0005
        L_0x006e:
            r0 = r1
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.packet.Message.equals(java.lang.Object):boolean");
    }

    public String f() {
        return this.k;
    }

    public void f(String str) {
        this.a = str;
    }

    public String g() {
        return this.l;
    }

    public void g(String str) {
        this.f = str;
    }

    public String h() {
        return this.m;
    }

    public void h(String str) {
        this.g = str;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((this.e != null ? this.e.hashCode() : 0) + (((this.d != null ? this.d.hashCode() : 0) + (((this.g != null ? this.g.hashCode() : 0) + ((this.a != null ? this.a.hashCode() : 0) * 31)) * 31)) * 31)) * 31;
        if (this.f != null) {
            i2 = this.f.hashCode();
        }
        return hashCode + i2;
    }

    public String i() {
        return this.e;
    }

    public void i(String str) {
        this.d = str;
    }

    public void j(String str) {
        this.e = str;
    }
}
