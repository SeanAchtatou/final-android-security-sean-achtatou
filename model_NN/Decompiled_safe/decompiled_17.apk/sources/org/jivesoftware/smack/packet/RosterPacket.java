package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.jivesoftware.smack.util.StringUtils;

public class RosterPacket extends IQ {
    private final List<Item> a = new ArrayList();
    private String d;

    public static class Item {
        private String a;
        private String b;
        private ItemType c = null;
        private ItemStatus d = null;
        private final Set<String> e = new CopyOnWriteArraySet();

        public Item(String str, String str2) {
            this.a = str.toLowerCase();
            this.b = str2;
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            sb.append("<item jid=\"").append(this.a).append("\"");
            if (this.b != null) {
                sb.append(" name=\"").append(StringUtils.a(this.b)).append("\"");
            }
            if (this.c != null) {
                sb.append(" subscription=\"").append(this.c).append("\"");
            }
            if (this.d != null) {
                sb.append(" ask=\"").append(this.d).append("\"");
            }
            sb.append(">");
            for (String a2 : this.e) {
                sb.append("<group>").append(StringUtils.a(a2)).append("</group>");
            }
            sb.append("</item>");
            return sb.toString();
        }

        public void a(String str) {
            this.e.add(str);
        }

        public void a(ItemStatus itemStatus) {
            this.d = itemStatus;
        }

        public void a(ItemType itemType) {
            this.c = itemType;
        }
    }

    public static class ItemStatus {
        public static final ItemStatus a = new ItemStatus("subscribe");
        public static final ItemStatus b = new ItemStatus("unsubscribe");
        private String c;

        private ItemStatus(String str) {
            this.c = str;
        }

        public static ItemStatus a(String str) {
            if (str != null) {
                String lowerCase = str.toLowerCase();
                if ("unsubscribe".equals(lowerCase)) {
                    return b;
                }
                if ("subscribe".equals(lowerCase)) {
                    return a;
                }
            }
            return null;
        }

        public String toString() {
            return this.c;
        }
    }

    public enum ItemType {
        none,
        to,
        from,
        both,
        remove
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(Item item) {
        synchronized (this.a) {
            this.a.add(item);
        }
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"jabber:iq:roster\" ");
        if (this.d != null) {
            sb.append(" ver=\"" + this.d + "\" ");
        }
        sb.append(">");
        synchronized (this.a) {
            for (Item a2 : this.a) {
                sb.append(a2.a());
            }
        }
        sb.append("</query>");
        return sb.toString();
    }
}
