package org.jivesoftware.smack.packet;

import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class Packet {
    private static String a = null;
    protected static final String b = Locale.getDefault().getLanguage().toLowerCase();
    public static final DateFormat c = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private static String d = (StringUtils.a(5) + "-");
    private static long e = 0;
    private String f = a;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private List<CommonPacketExtension> k = new CopyOnWriteArrayList();
    private final Map<String, Object> l = new HashMap();
    private XMPPError m = null;

    static {
        c.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public Packet() {
    }

    public Packet(Bundle bundle) {
        this.h = bundle.getString("ext_to");
        this.i = bundle.getString("ext_from");
        this.j = bundle.getString("ext_chid");
        this.g = bundle.getString("ext_pkt_id");
        Parcelable[] parcelableArray = bundle.getParcelableArray("ext_exts");
        if (parcelableArray != null) {
            this.k = new ArrayList(parcelableArray.length);
            for (Parcelable parcelable : parcelableArray) {
                CommonPacketExtension a2 = CommonPacketExtension.a((Bundle) parcelable);
                if (a2 != null) {
                    this.k.add(a2);
                }
            }
        }
        Bundle bundle2 = bundle.getBundle("ext_ERROR");
        if (bundle2 != null) {
            this.m = new XMPPError(bundle2);
        }
    }

    public static String j() {
        String sb;
        synchronized (Packet.class) {
            try {
                StringBuilder append = new StringBuilder().append(d);
                long j2 = e;
                e = 1 + j2;
                sb = append.append(Long.toString(j2)).toString();
            } catch (Throwable th) {
                Class<Packet> cls = Packet.class;
                throw th;
            }
        }
        return sb;
    }

    public static String t() {
        return b;
    }

    public abstract String a();

    public CommonPacketExtension a(String str, String str2) {
        for (CommonPacketExtension next : this.k) {
            if ((str2 == null || str2.equals(next.b())) && str.equals(next.a())) {
                return next;
            }
        }
        return null;
    }

    public void a(String str, Object obj) {
        synchronized (this) {
            if (!(obj instanceof Serializable)) {
                throw new IllegalArgumentException("Value must be serialiazble");
            }
            this.l.put(str, obj);
        }
    }

    public void a(CommonPacketExtension commonPacketExtension) {
        this.k.add(commonPacketExtension);
    }

    public void a(XMPPError xMPPError) {
        this.m = xMPPError;
    }

    public Bundle d() {
        Bundle bundle = new Bundle();
        if (!TextUtils.isEmpty(this.f)) {
            bundle.putString("ext_ns", this.f);
        }
        if (!TextUtils.isEmpty(this.i)) {
            bundle.putString("ext_from", this.i);
        }
        if (!TextUtils.isEmpty(this.h)) {
            bundle.putString("ext_to", this.h);
        }
        if (!TextUtils.isEmpty(this.g)) {
            bundle.putString("ext_pkt_id", this.g);
        }
        if (!TextUtils.isEmpty(this.j)) {
            bundle.putString("ext_chid", this.j);
        }
        if (this.m != null) {
            bundle.putBundle("ext_ERROR", this.m.c());
        }
        if (this.k != null) {
            Bundle[] bundleArr = new Bundle[this.k.size()];
            Iterator<CommonPacketExtension> it = this.k.iterator();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    break;
                }
                Bundle e2 = it.next().e();
                if (e2 != null) {
                    i2 = i3 + 1;
                    bundleArr[i3] = e2;
                } else {
                    i2 = i3;
                }
            }
            bundle.putParcelableArray("ext_exts", bundleArr);
        }
        return bundle;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        if (r4.i.equals(r5.i) != false) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0046, code lost:
        if (r4.g.equals(r5.g) != false) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0054, code lost:
        if (r4.j.equals(r5.j) != false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0062, code lost:
        if (r4.l.equals(r5.l) != false) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0070, code lost:
        if (r4.h.equals(r5.h) != false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0020, code lost:
        if (r4.m.equals(r5.m) != false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r1 = 0
            r0 = 1
            if (r4 != r5) goto L_0x0006
        L_0x0004:
            r1 = r0
        L_0x0005:
            return r1
        L_0x0006:
            if (r5 == 0) goto L_0x00ac
            java.lang.Class r2 = r4.getClass()
            java.lang.Class r3 = r5.getClass()
            if (r2 != r3) goto L_0x00ac
            org.jivesoftware.smack.packet.Packet r5 = (org.jivesoftware.smack.packet.Packet) r5
            org.jivesoftware.smack.packet.XMPPError r2 = r4.m
            if (r2 == 0) goto L_0x0083
            org.jivesoftware.smack.packet.XMPPError r2 = r4.m
            org.jivesoftware.smack.packet.XMPPError r3 = r5.m
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ac
        L_0x0022:
            java.lang.String r2 = r4.i
            if (r2 == 0) goto L_0x0089
            java.lang.String r2 = r4.i
            java.lang.String r3 = r5.i
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ac
        L_0x0030:
            java.util.List<org.jivesoftware.smack.packet.CommonPacketExtension> r2 = r4.k
            java.util.List<org.jivesoftware.smack.packet.CommonPacketExtension> r3 = r5.k
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ac
            java.lang.String r2 = r4.g
            if (r2 == 0) goto L_0x008f
            java.lang.String r2 = r4.g
            java.lang.String r3 = r5.g
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ac
        L_0x0048:
            java.lang.String r2 = r4.j
            if (r2 == 0) goto L_0x0095
            java.lang.String r2 = r4.j
            java.lang.String r3 = r5.j
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ac
        L_0x0056:
            java.util.Map<java.lang.String, java.lang.Object> r2 = r4.l
            if (r2 == 0) goto L_0x009b
            java.util.Map<java.lang.String, java.lang.Object> r2 = r4.l
            java.util.Map<java.lang.String, java.lang.Object> r3 = r5.l
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ac
        L_0x0064:
            java.lang.String r2 = r4.h
            if (r2 == 0) goto L_0x00a1
            java.lang.String r2 = r4.h
            java.lang.String r3 = r5.h
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00ac
        L_0x0072:
            java.lang.String r2 = r4.f
            if (r2 == 0) goto L_0x00a7
            java.lang.String r2 = r4.f
            java.lang.String r3 = r5.f
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x0081
        L_0x0080:
            r0 = r1
        L_0x0081:
            r1 = r0
            goto L_0x0005
        L_0x0083:
            org.jivesoftware.smack.packet.XMPPError r2 = r5.m
            if (r2 == 0) goto L_0x0022
            goto L_0x0005
        L_0x0089:
            java.lang.String r2 = r5.i
            if (r2 == 0) goto L_0x0030
            goto L_0x0005
        L_0x008f:
            java.lang.String r2 = r5.g
            if (r2 == 0) goto L_0x0048
            goto L_0x0005
        L_0x0095:
            java.lang.String r2 = r5.j
            if (r2 == 0) goto L_0x0056
            goto L_0x0005
        L_0x009b:
            java.util.Map<java.lang.String, java.lang.Object> r2 = r5.l
            if (r2 == 0) goto L_0x0064
            goto L_0x0005
        L_0x00a1:
            java.lang.String r2 = r5.h
            if (r2 == 0) goto L_0x0072
            goto L_0x0005
        L_0x00a7:
            java.lang.String r2 = r5.f
            if (r2 != 0) goto L_0x0080
            goto L_0x0081
        L_0x00ac:
            r0 = r1
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.packet.Packet.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((((((this.j != null ? this.j.hashCode() : 0) + (((this.i != null ? this.i.hashCode() : 0) + (((this.h != null ? this.h.hashCode() : 0) + (((this.g != null ? this.g.hashCode() : 0) + ((this.f != null ? this.f.hashCode() : 0) * 31)) * 31)) * 31)) * 31)) * 31) + this.k.hashCode()) * 31) + this.l.hashCode()) * 31;
        if (this.m != null) {
            i2 = this.m.hashCode();
        }
        return hashCode + i2;
    }

    public String k() {
        if ("ID_NOT_AVAILABLE".equals(this.g)) {
            return null;
        }
        if (this.g == null) {
            this.g = j();
        }
        return this.g;
    }

    public void k(String str) {
        this.g = str;
    }

    public String l() {
        return this.j;
    }

    public void l(String str) {
        this.j = str;
    }

    public String m() {
        return this.h;
    }

    public void m(String str) {
        this.h = str;
    }

    public String n() {
        return this.i;
    }

    public void n(String str) {
        this.i = str;
    }

    public CommonPacketExtension o(String str) {
        return a(str, (String) null);
    }

    public XMPPError o() {
        return this.m;
    }

    public Object p(String str) {
        Object obj;
        synchronized (this) {
            obj = this.l == null ? null : this.l.get(str);
        }
        return obj;
    }

    public Collection<CommonPacketExtension> p() {
        List emptyList;
        synchronized (this) {
            emptyList = this.k == null ? Collections.emptyList() : Collections.unmodifiableList(new ArrayList(this.k));
        }
        return emptyList;
    }

    public Collection<String> q() {
        Set emptySet;
        synchronized (this) {
            emptySet = this.l == null ? Collections.emptySet() : Collections.unmodifiableSet(new HashSet(this.l.keySet()));
        }
        return emptySet;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x012a A[SYNTHETIC, Splitter:B:55:0x012a] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x012f A[SYNTHETIC, Splitter:B:58:0x012f] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x013b A[SYNTHETIC, Splitter:B:64:0x013b] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0140 A[SYNTHETIC, Splitter:B:67:0x0140] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x007f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String r() {
        /*
            r9 = this;
            r4 = 0
            monitor-enter(r9)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0023 }
            r6.<init>()     // Catch:{ all -> 0x0023 }
            java.util.Collection r1 = r9.p()     // Catch:{ all -> 0x0023 }
            java.util.Iterator r2 = r1.iterator()     // Catch:{ all -> 0x0023 }
        L_0x000f:
            boolean r1 = r2.hasNext()     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0026
            java.lang.Object r1 = r2.next()     // Catch:{ all -> 0x0023 }
            org.jivesoftware.smack.packet.CommonPacketExtension r1 = (org.jivesoftware.smack.packet.CommonPacketExtension) r1     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = r1.d()     // Catch:{ all -> 0x0023 }
            r6.append(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x000f
        L_0x0023:
            r1 = move-exception
            monitor-exit(r9)
            throw r1
        L_0x0026:
            java.util.Map<java.lang.String, java.lang.Object> r1 = r9.l     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0149
            java.util.Map<java.lang.String, java.lang.Object> r1 = r9.l     // Catch:{ all -> 0x0023 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0149
            java.lang.String r1 = "<properties xmlns=\"http://www.jivesoftware.com/xmlns/xmpp/properties\">"
            r6.append(r1)     // Catch:{ all -> 0x0023 }
            java.util.Collection r1 = r9.q()     // Catch:{ all -> 0x0023 }
            java.util.Iterator r7 = r1.iterator()     // Catch:{ all -> 0x0023 }
        L_0x003f:
            boolean r1 = r7.hasNext()     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0144
            java.lang.Object r1 = r7.next()     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0023 }
            java.lang.Object r2 = r9.p(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r3 = "<property>"
            r6.append(r3)     // Catch:{ all -> 0x0023 }
            java.lang.String r3 = "<name>"
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = org.jivesoftware.smack.packet.StringUtils.a(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r3 = "</name>"
            r1.append(r3)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "<value type=\""
            r6.append(r1)     // Catch:{ all -> 0x0023 }
            boolean r1 = r2 instanceof java.lang.Integer     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0085
            java.lang.String r1 = "integer\">"
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
        L_0x007f:
            java.lang.String r1 = "</property>"
            r6.append(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x003f
        L_0x0085:
            boolean r1 = r2 instanceof java.lang.Long     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x0099
            java.lang.String r1 = "long\">"
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x0099:
            boolean r1 = r2 instanceof java.lang.Float     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00ad
            java.lang.String r1 = "float\">"
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00ad:
            boolean r1 = r2 instanceof java.lang.Double     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00c1
            java.lang.String r1 = "double\">"
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00c1:
            boolean r1 = r2 instanceof java.lang.Boolean     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00d5
            java.lang.String r1 = "boolean\">"
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00d5:
            boolean r1 = r2 instanceof java.lang.String     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x00ef
            java.lang.String r1 = "string\">"
            r6.append(r1)     // Catch:{ all -> 0x0023 }
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0023 }
            r1 = r0
            java.lang.String r1 = org.jivesoftware.smack.packet.StringUtils.a(r1)     // Catch:{ all -> 0x0023 }
            r6.append(r1)     // Catch:{ all -> 0x0023 }
            java.lang.String r1 = "</value>"
            r6.append(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x007f
        L_0x00ef:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0121, all -> 0x0137 }
            r3.<init>()     // Catch:{ Exception -> 0x0121, all -> 0x0137 }
            java.io.ObjectOutputStream r5 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0158, all -> 0x016c }
            r5.<init>(r3)     // Catch:{ Exception -> 0x0158, all -> 0x016c }
            r5.writeObject(r2)     // Catch:{ Exception -> 0x015e, all -> 0x014f }
            java.lang.String r1 = "java-object\">"
            r6.append(r1)     // Catch:{ Exception -> 0x015e, all -> 0x014f }
            byte[] r1 = r3.toByteArray()     // Catch:{ Exception -> 0x015e, all -> 0x014f }
            java.lang.String r1 = org.jivesoftware.smack.packet.StringUtils.a(r1)     // Catch:{ Exception -> 0x015e, all -> 0x014f }
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ Exception -> 0x015e, all -> 0x014f }
            java.lang.String r2 = "</value>"
            r1.append(r2)     // Catch:{ Exception -> 0x015e, all -> 0x014f }
            if (r5 == 0) goto L_0x0117
            r5.close()     // Catch:{ Exception -> 0x0164 }
        L_0x0117:
            if (r3 == 0) goto L_0x007f
            r3.close()     // Catch:{ Exception -> 0x011e }
            goto L_0x007f
        L_0x011e:
            r1 = move-exception
            goto L_0x007f
        L_0x0121:
            r1 = move-exception
            r2 = r4
            r3 = r1
            r1 = r4
        L_0x0125:
            r3.printStackTrace()     // Catch:{ all -> 0x0152 }
            if (r2 == 0) goto L_0x012d
            r2.close()     // Catch:{ Exception -> 0x0166 }
        L_0x012d:
            if (r1 == 0) goto L_0x007f
            r1.close()     // Catch:{ Exception -> 0x0134 }
            goto L_0x007f
        L_0x0134:
            r1 = move-exception
            goto L_0x007f
        L_0x0137:
            r1 = move-exception
            r3 = r4
        L_0x0139:
            if (r4 == 0) goto L_0x013e
            r4.close()     // Catch:{ Exception -> 0x0168 }
        L_0x013e:
            if (r3 == 0) goto L_0x0143
            r3.close()     // Catch:{ Exception -> 0x016a }
        L_0x0143:
            throw r1     // Catch:{ all -> 0x0023 }
        L_0x0144:
            java.lang.String r1 = "</properties>"
            r6.append(r1)     // Catch:{ all -> 0x0023 }
        L_0x0149:
            java.lang.String r1 = r6.toString()     // Catch:{ all -> 0x0023 }
            monitor-exit(r9)
            return r1
        L_0x014f:
            r1 = move-exception
            r4 = r5
            goto L_0x0139
        L_0x0152:
            r3 = move-exception
            r4 = r2
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x0139
        L_0x0158:
            r1 = move-exception
            r2 = r4
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x0125
        L_0x015e:
            r1 = move-exception
            r2 = r5
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x0125
        L_0x0164:
            r1 = move-exception
            goto L_0x0117
        L_0x0166:
            r2 = move-exception
            goto L_0x012d
        L_0x0168:
            r2 = move-exception
            goto L_0x013e
        L_0x016a:
            r2 = move-exception
            goto L_0x0143
        L_0x016c:
            r1 = move-exception
            goto L_0x0139
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.packet.Packet.r():java.lang.String");
    }

    public String s() {
        return this.f;
    }
}
