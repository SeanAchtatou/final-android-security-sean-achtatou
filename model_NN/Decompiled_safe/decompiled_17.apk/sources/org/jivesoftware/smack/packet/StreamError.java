package org.jivesoftware.smack.packet;

public class StreamError {
    private String a;

    public StreamError(String str) {
        this.a = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("stream:error (").append(this.a).append(")");
        return sb.toString();
    }
}
