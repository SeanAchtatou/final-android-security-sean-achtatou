package org.jivesoftware.smack.packet;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DefaultPacketExtension implements PacketExtension {
    private String a;
    private String b;
    private Map<String, String> c;

    public String a(String str) {
        String str2;
        synchronized (this) {
            str2 = this.c == null ? null : this.c.get(str);
        }
        return str2;
    }

    public Collection<String> a() {
        Set emptySet;
        synchronized (this) {
            emptySet = this.c == null ? Collections.emptySet() : Collections.unmodifiableSet(new HashMap(this.c).keySet());
        }
        return emptySet;
    }

    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(this.a).append(" xmlns=\"").append(this.b).append("\">");
        for (String next : a()) {
            String a2 = a(next);
            sb.append("<").append(next).append(">");
            sb.append(a2);
            sb.append("</").append(next).append(">");
        }
        sb.append("</").append(this.a).append(">");
        return sb.toString();
    }
}
