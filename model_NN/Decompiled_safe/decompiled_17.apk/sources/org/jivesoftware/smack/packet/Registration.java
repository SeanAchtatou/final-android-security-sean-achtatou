package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Registration extends IQ {
    private String a = null;
    private Map<String, String> d = new HashMap();
    private List<String> e = new ArrayList();
    private boolean f = false;
    private boolean g = false;

    public void a(String str) {
        this.a = str;
    }

    public void a(boolean z) {
        this.f = z;
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("<query xmlns=\"jabber:iq:register\">");
        if (this.a != null && !this.g) {
            sb.append("<instructions>").append(this.a).append("</instructions>");
        }
        if (this.d != null && this.d.size() > 0 && !this.g) {
            for (String next : this.d.keySet()) {
                sb.append("<").append(next).append(">");
                sb.append(this.d.get(next));
                sb.append("</").append(next).append(">");
            }
        } else if (this.g) {
            sb.append("</remove>");
        }
        sb.append(r());
        sb.append("</query>");
        return sb.toString();
    }

    public void b(String str) {
        this.e.add(str);
    }

    public void b(String str, String str2) {
        this.d.put(str, str2);
    }
}
