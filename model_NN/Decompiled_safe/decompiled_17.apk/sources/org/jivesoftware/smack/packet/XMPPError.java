package org.jivesoftware.smack.packet;

import android.os.Bundle;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.HttpStatus;

public class XMPPError {
    private int a;
    private Type b;
    private String c;
    private String d;
    private String e;
    private List<CommonPacketExtension> f = null;

    public static class Condition {
        public static final Condition a = new Condition("internal-server-error");
        public static final Condition b = new Condition("forbidden");
        public static final Condition c = new Condition("bad-request");
        public static final Condition d = new Condition("conflict");
        public static final Condition e = new Condition("feature-not-implemented");
        public static final Condition f = new Condition("gone");
        public static final Condition g = new Condition("item-not-found");
        public static final Condition h = new Condition("jid-malformed");
        public static final Condition i = new Condition("not-acceptable");
        public static final Condition j = new Condition("not-allowed");
        public static final Condition k = new Condition("not-authorized");
        public static final Condition l = new Condition("payment-required");
        public static final Condition m = new Condition("recipient-unavailable");
        public static final Condition n = new Condition("redirect");
        public static final Condition o = new Condition("registration-required");
        public static final Condition p = new Condition("remote-server-error");
        public static final Condition q = new Condition("remote-server-not-found");
        public static final Condition r = new Condition("remote-server-timeout");
        public static final Condition s = new Condition("resource-constraint");
        public static final Condition t = new Condition("service-unavailable");
        public static final Condition u = new Condition("subscription-required");
        public static final Condition v = new Condition("undefined-condition");
        public static final Condition w = new Condition("unexpected-request");
        public static final Condition x = new Condition("request-timeout");
        /* access modifiers changed from: private */
        public String y;

        public Condition(String str) {
            this.y = str;
        }

        public String toString() {
            return this.y;
        }
    }

    public enum Type {
        WAIT,
        CANCEL,
        MODIFY,
        AUTH,
        CONTINUE,
        CLIENT
    }

    private static class a {
        private static Map<Condition, a> d = c();
        private int a;
        private Type b;
        private Condition c;

        private a(Condition condition, Type type, int i) {
            this.a = i;
            this.b = type;
            this.c = condition;
        }

        protected static a a(Condition condition) {
            return d.get(condition);
        }

        private static Map<Condition, a> c() {
            HashMap hashMap = new HashMap(22);
            hashMap.put(Condition.a, new a(Condition.a, Type.WAIT, HttpStatus.SC_INTERNAL_SERVER_ERROR));
            hashMap.put(Condition.b, new a(Condition.b, Type.AUTH, HttpStatus.SC_FORBIDDEN));
            hashMap.put(Condition.c, new a(Condition.c, Type.MODIFY, HttpStatus.SC_BAD_REQUEST));
            hashMap.put(Condition.g, new a(Condition.g, Type.CANCEL, HttpStatus.SC_NOT_FOUND));
            hashMap.put(Condition.d, new a(Condition.d, Type.CANCEL, HttpStatus.SC_CONFLICT));
            hashMap.put(Condition.e, new a(Condition.e, Type.CANCEL, HttpStatus.SC_NOT_IMPLEMENTED));
            hashMap.put(Condition.f, new a(Condition.f, Type.MODIFY, HttpStatus.SC_MOVED_TEMPORARILY));
            hashMap.put(Condition.h, new a(Condition.h, Type.MODIFY, HttpStatus.SC_BAD_REQUEST));
            hashMap.put(Condition.i, new a(Condition.i, Type.MODIFY, HttpStatus.SC_NOT_ACCEPTABLE));
            hashMap.put(Condition.j, new a(Condition.j, Type.CANCEL, HttpStatus.SC_METHOD_NOT_ALLOWED));
            hashMap.put(Condition.k, new a(Condition.k, Type.AUTH, HttpStatus.SC_UNAUTHORIZED));
            hashMap.put(Condition.l, new a(Condition.l, Type.AUTH, HttpStatus.SC_PAYMENT_REQUIRED));
            hashMap.put(Condition.m, new a(Condition.m, Type.WAIT, HttpStatus.SC_NOT_FOUND));
            hashMap.put(Condition.n, new a(Condition.n, Type.MODIFY, HttpStatus.SC_MOVED_TEMPORARILY));
            hashMap.put(Condition.o, new a(Condition.o, Type.AUTH, HttpStatus.SC_PROXY_AUTHENTICATION_REQUIRED));
            hashMap.put(Condition.q, new a(Condition.q, Type.CANCEL, HttpStatus.SC_NOT_FOUND));
            hashMap.put(Condition.r, new a(Condition.r, Type.WAIT, HttpStatus.SC_GATEWAY_TIMEOUT));
            hashMap.put(Condition.p, new a(Condition.p, Type.CANCEL, HttpStatus.SC_BAD_GATEWAY));
            hashMap.put(Condition.s, new a(Condition.s, Type.WAIT, HttpStatus.SC_INTERNAL_SERVER_ERROR));
            hashMap.put(Condition.t, new a(Condition.t, Type.CANCEL, HttpStatus.SC_SERVICE_UNAVAILABLE));
            hashMap.put(Condition.u, new a(Condition.u, Type.AUTH, HttpStatus.SC_PROXY_AUTHENTICATION_REQUIRED));
            hashMap.put(Condition.v, new a(Condition.v, Type.WAIT, HttpStatus.SC_INTERNAL_SERVER_ERROR));
            hashMap.put(Condition.w, new a(Condition.w, Type.WAIT, HttpStatus.SC_BAD_REQUEST));
            hashMap.put(Condition.x, new a(Condition.x, Type.CANCEL, HttpStatus.SC_REQUEST_TIMEOUT));
            return hashMap;
        }

        /* access modifiers changed from: protected */
        public Type a() {
            return this.b;
        }

        /* access modifiers changed from: protected */
        public int b() {
            return this.a;
        }
    }

    public XMPPError(int i, Type type, String str, String str2, String str3, List<CommonPacketExtension> list) {
        this.a = i;
        this.b = type;
        this.d = str;
        this.c = str2;
        this.e = str3;
        this.f = list;
    }

    public XMPPError(Bundle bundle) {
        this.a = bundle.getInt("ext_err_code");
        if (bundle.containsKey("ext_err_type")) {
            this.b = Type.valueOf(bundle.getString("ext_err_type"));
        }
        this.c = bundle.getString("ext_err_cond");
        this.d = bundle.getString("ext_err_reason");
        this.e = bundle.getString("ext_err_msg");
        Parcelable[] parcelableArray = bundle.getParcelableArray("ext_exts");
        if (parcelableArray != null) {
            this.f = new ArrayList(parcelableArray.length);
            for (Parcelable parcelable : parcelableArray) {
                CommonPacketExtension a2 = CommonPacketExtension.a((Bundle) parcelable);
                if (a2 != null) {
                    this.f.add(a2);
                }
            }
        }
    }

    public XMPPError(Condition condition) {
        a(condition);
        this.e = null;
    }

    public XMPPError(Condition condition, String str) {
        a(condition);
        this.e = str;
    }

    private void a(Condition condition) {
        a a2 = a.a(condition);
        this.c = condition.y;
        if (a2 != null) {
            this.b = a2.a();
            this.a = a2.b();
        }
    }

    public String a() {
        return this.d;
    }

    public Type b() {
        return this.b;
    }

    public Bundle c() {
        Bundle bundle = new Bundle();
        if (this.b != null) {
            bundle.putString("ext_err_type", this.b.name());
        }
        bundle.putInt("ext_err_code", this.a);
        if (this.d != null) {
            bundle.putString("ext_err_reason", this.d);
        }
        if (this.c != null) {
            bundle.putString("ext_err_cond", this.c);
        }
        if (this.e != null) {
            bundle.putString("ext_err_msg", this.e);
        }
        if (this.f != null) {
            Bundle[] bundleArr = new Bundle[this.f.size()];
            Iterator<CommonPacketExtension> it = this.f.iterator();
            int i = 0;
            while (true) {
                int i2 = i;
                if (!it.hasNext()) {
                    break;
                }
                Bundle e2 = it.next().e();
                if (e2 != null) {
                    i = i2 + 1;
                    bundleArr[i2] = e2;
                } else {
                    i = i2;
                }
            }
            bundle.putParcelableArray("ext_exts", bundleArr);
        }
        return bundle;
    }

    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append("<error code=\"").append(this.a).append("\"");
        if (this.b != null) {
            sb.append(" type=\"");
            sb.append(this.b.name());
            sb.append("\"");
        }
        if (this.d != null) {
            sb.append(" reason=\"");
            sb.append(this.d);
            sb.append("\"");
        }
        sb.append(">");
        if (this.c != null) {
            sb.append("<").append(this.c);
            sb.append(" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\"/>");
        }
        if (this.e != null) {
            sb.append("<text xml:lang=\"en\" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\">");
            sb.append(this.e);
            sb.append("</text>");
        }
        for (CommonPacketExtension d2 : e()) {
            sb.append(d2.d());
        }
        sb.append("</error>");
        return sb.toString();
    }

    public List<CommonPacketExtension> e() {
        List<CommonPacketExtension> emptyList;
        synchronized (this) {
            emptyList = this.f == null ? Collections.emptyList() : Collections.unmodifiableList(this.f);
        }
        return emptyList;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.c != null) {
            sb.append(this.c);
        }
        sb.append("(").append(this.a).append(")");
        if (this.e != null) {
            sb.append(" ").append(this.e);
        }
        return sb.toString();
    }
}
