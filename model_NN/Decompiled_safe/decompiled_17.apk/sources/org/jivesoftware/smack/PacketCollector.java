package org.jivesoftware.smack;

import java.util.concurrent.LinkedBlockingQueue;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;

public class PacketCollector {
    private PacketFilter a;
    private LinkedBlockingQueue<Packet> b;

    /* access modifiers changed from: protected */
    public void a(Packet packet) {
        synchronized (this) {
            if (packet != null) {
                if (this.a == null || this.a.a(packet)) {
                    while (!this.b.offer(packet)) {
                        this.b.poll();
                    }
                }
            }
        }
    }
}
