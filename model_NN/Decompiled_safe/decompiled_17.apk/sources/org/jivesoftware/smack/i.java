package org.jivesoftware.smack;

import com.xiaomi.push.service.XMPushService;

class i extends XMPushService.d {
    final /* synthetic */ int a;
    final /* synthetic */ Exception b;
    final /* synthetic */ XMPPConnection c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(XMPPConnection xMPPConnection, int i, int i2, Exception exc) {
        super(i);
        this.c = xMPPConnection;
        this.a = i2;
        this.b = exc;
    }

    public void a() {
        this.c.x.a(this.a, this.b);
    }

    public String b() {
        return "shutdown the connection. " + this.a + ", " + this.b;
    }
}
