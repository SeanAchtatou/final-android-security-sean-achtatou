package org.jivesoftware.smack;

import com.kenai.jbosh.BOSHClient;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.BOSHClientConnEvent;
import com.kenai.jbosh.BOSHClientConnListener;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyQName;
import com.kenai.jbosh.ComposableBody;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.n;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;

public class BOSHConnection extends Connection {
    /* access modifiers changed from: private */
    public Thread A;
    private String B = null;
    /* access modifiers changed from: private */
    public Object C = new Object();
    protected String a = null;
    protected String b = null;
    private BOSHClient r;
    private final BOSHConfiguration s;
    private boolean t = false;
    private boolean u = false;
    private boolean v = true;
    private boolean w = false;
    /* access modifiers changed from: private */
    public boolean x = false;
    private ExecutorService y;
    /* access modifiers changed from: private */
    public PipedWriter z;

    private class a implements BOSHClientConnListener {
        private final BOSHConnection b;

        public a(BOSHConnection bOSHConnection) {
            this.b = bOSHConnection;
        }

        public void a(BOSHClientConnEvent bOSHClientConnEvent) {
            if (!bOSHClientConnEvent.a()) {
                BOSHConnection.this.a(2, 0, (Exception) null);
                BOSHConnection.this.a((Exception) bOSHClientConnEvent.b());
            }
            synchronized (BOSHConnection.this.C) {
                BOSHConnection.this.C.notifyAll();
            }
        }
    }

    private class b implements Runnable {
        private Packet b;

        public b(Packet packet) {
            this.b = packet;
        }

        public void run() {
            for (Connection.ListenerWrapper a2 : BOSHConnection.this.g.values()) {
                a2.a(this.b);
            }
        }
    }

    public BOSHConnection(XMPushService xMPushService, BOSHConfiguration bOSHConfiguration) {
        super(xMPushService, bOSHConfiguration);
        this.s = bOSHConfiguration;
    }

    private void a(boolean z2) {
        if (!this.w) {
            this.w = z2;
        }
    }

    public void a() {
        if (k()) {
            com.xiaomi.channel.commonutils.logger.b.c("SMACK-BOSH: Already connected to a server.");
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        this.x = false;
        this.b = null;
        this.a = null;
        try {
            a(0, 0, (Exception) null);
            URI f = this.s.f();
            com.xiaomi.channel.commonutils.logger.b.b("SMACK-BOSH: connecting using uri:" + f.toString());
            BOSHClientConfig.Builder a2 = BOSHClientConfig.Builder.a(f, this.s.h());
            if (this.s.a()) {
                a2.a(this.s.b(), this.s.c());
            }
            this.r = BOSHClient.a(a2.a(), this.q.getApplicationContext());
            this.y = Executors.newSingleThreadExecutor(new a(this));
            this.r.a(new a(this));
            this.r.a(new BOSHPacketReader(this));
            if (this.s.l()) {
                b();
                if (this.v) {
                    if (this.k.c() != null) {
                        a(this.k.c(), (PacketFilter) null);
                    }
                    if (this.k.d() != null) {
                        b(this.k.d(), null);
                    }
                }
            }
            this.r.a(ComposableBody.d().a(BodyQName.a("xm", "version"), "102").a());
            synchronized (this.C) {
                if (!k()) {
                    try {
                        this.C.wait((long) (SmackConfiguration.b() * 6));
                    } catch (InterruptedException e) {
                    }
                }
            }
            long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
            if (!k()) {
                this.x = true;
                String str = "Timeout reached for the connection to " + this.s.e() + ":" + g() + ".";
                this.s.d().a(this.s.e(), currentTimeMillis2, 0, null);
                throw new XMPPException(str, new XMPPError(XMPPError.Condition.r, str));
            }
            this.s.d().a(this.s.e(), currentTimeMillis2, -1);
        } catch (Exception e2) {
            throw new XMPPException("Can't connect to " + d(), e2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(ComposableBody composableBody) {
        if (!k()) {
            throw new IllegalStateException("Not connected to a server!");
        } else if (composableBody == null) {
            throw new NullPointerException("Body mustn't be null!");
        } else {
            if (this.b != null) {
                composableBody = composableBody.e().a(BodyQName.a("xm", "sid"), this.b).a();
            }
            this.r.a(composableBody);
        }
    }

    public void a(n.b bVar) {
        synchronized (this) {
            new XMBinder().a(bVar, this.n, this);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
        b(new Presence(Presence.Type.unavailable), 3, exc);
        com.xiaomi.channel.commonutils.logger.b.d("");
        for (ConnectionListener b2 : h()) {
            try {
                b2.b(0, exc);
            } catch (Exception e) {
                com.xiaomi.channel.commonutils.logger.b.c("SMACK-BOSH: notify connection closed error" + e);
            }
        }
    }

    public void a(String str, String str2) {
        synchronized (this) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.l(str);
            presence.n(str2);
            a(presence);
        }
    }

    public void a(Packet packet) {
        if (!this.x) {
            e(packet);
            try {
                a(ComposableBody.d().a(packet.a()).a());
                c(packet);
            } catch (BOSHException e) {
                throw new XMPPException(e);
            }
        } else {
            throw new XMPPException("try send packet while the connection is done.");
        }
    }

    public void a(Presence presence, int i, Exception exc) {
        if (p() != 2) {
            b(presence, i, exc);
            this.h.clear();
            this.g.clear();
            this.f.clear();
            this.i.clear();
            this.w = false;
            this.v = true;
        }
    }

    public void a(Packet[] packetArr) {
        if (!this.x) {
            StringBuilder sb = new StringBuilder();
            for (Packet packet : packetArr) {
                if (packet == null) {
                    throw new NullPointerException("Packet is null.");
                }
                e(packet);
                sb.append(packet.a());
            }
            try {
                a(ComposableBody.d().a(sb.toString()).a());
                for (Packet c : packetArr) {
                    c(c);
                }
            } catch (BOSHException e) {
                throw new XMPPException(e);
            }
        } else {
            throw new XMPPException("try send packet while connection is done.");
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.m = new b(this);
        try {
            this.z = new PipedWriter();
            this.l = new PipedReader(this.z);
        } catch (IOException e) {
        }
        super.b();
        this.r.a(new c(this));
        this.r.a(new d(this));
        this.A = new e(this);
        this.A.setDaemon(true);
        this.A.start();
    }

    /* access modifiers changed from: protected */
    public void b(Packet packet) {
        if (packet != null) {
            for (PacketCollector a2 : i()) {
                a2.a(packet);
            }
            this.y.submit(new b(packet));
        }
    }

    /* access modifiers changed from: protected */
    public void b(Presence presence, int i, Exception exc) {
        a(this.t);
        this.a = null;
        this.b = null;
        this.x = true;
        this.t = false;
        a(2, i, exc);
        this.v = false;
        this.n = "";
        try {
            this.r.b(ComposableBody.d().a("xmpp", "xm").a());
            Thread.sleep(150);
        } catch (Exception e) {
        }
        if (this.r != null) {
            this.r.a();
            this.r = null;
        }
        if (this.z != null) {
            try {
                this.z.close();
            } catch (Throwable th) {
            }
            this.l = null;
        }
        if (this.l != null) {
            try {
                this.l.close();
            } catch (Throwable th2) {
            }
            this.l = null;
        }
        if (this.m != null) {
            try {
                this.m.close();
            } catch (Throwable th3) {
            }
            this.m = null;
        }
        if (this.y != null) {
            this.y.shutdown();
        }
        for (ConnectionListener b2 : h()) {
            try {
                b2.b(i, exc);
            } catch (Exception e2) {
                com.xiaomi.channel.commonutils.logger.b.a("SMACK-BOSH: Error while shut down connection", e2);
            }
        }
        this.A = null;
    }

    public void c() {
        if (k()) {
            com.xiaomi.channel.commonutils.logger.b.b("SMACK-BOSH: scheduling empty request for ping");
            this.r.b();
        }
    }
}
