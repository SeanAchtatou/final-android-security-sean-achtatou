package org.jivesoftware.smack;

import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.n;
import com.xiaomi.push.service.o;
import de.measite.smack.AndroidDebugger;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.jivesoftware.smack.debugger.SmackDebugger;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

public abstract class Connection {
    private static final AtomicInteger a = new AtomicInteger(0);
    public static boolean c;
    private final Collection<ConnectionListener> b = new CopyOnWriteArrayList();
    protected int d = 0;
    protected long e = -1;
    protected final Collection<PacketCollector> f = new ConcurrentLinkedQueue();
    protected final Map<PacketListener, ListenerWrapper> g = new ConcurrentHashMap();
    protected final Map<PacketListener, ListenerWrapper> h = new ConcurrentHashMap();
    protected final Map<PacketInterceptor, InterceptorWrapper> i = new ConcurrentHashMap();
    protected final ArrayList<PacketListener> j = new ArrayList<>();
    protected SmackDebugger k = null;
    protected Reader l;
    protected Writer m;
    protected String n = "";
    protected final int o = a.getAndIncrement();
    protected ConnectionConfiguration p;
    protected XMPushService q;
    private int r = 2;
    private long s = 0;

    protected static class InterceptorWrapper {
        private PacketInterceptor a;
        private PacketFilter b;

        public void a(Packet packet) {
            if (this.b == null || this.b.a(packet)) {
                this.a.a(packet);
            }
        }

        public boolean equals(Object obj) {
            if (obj != null) {
                if (obj instanceof InterceptorWrapper) {
                    return ((InterceptorWrapper) obj).a.equals(this.a);
                }
                if (obj instanceof PacketInterceptor) {
                    return obj.equals(this.a);
                }
            }
            return false;
        }
    }

    protected static class ListenerWrapper {
        private PacketListener a;
        private PacketFilter b;

        public ListenerWrapper(PacketListener packetListener, PacketFilter packetFilter) {
            this.a = packetListener;
            this.b = packetFilter;
        }

        public void a(Packet packet) {
            if (this.b == null || this.b.a(packet)) {
                this.a.a(packet);
            }
        }
    }

    static {
        c = false;
        try {
            c = Boolean.getBoolean("smack.debugEnabled");
        } catch (Exception e2) {
        }
        SmackConfiguration.a();
    }

    protected Connection(XMPushService xMPushService, ConnectionConfiguration connectionConfiguration) {
        this.p = connectionConfiguration;
        this.q = xMPushService;
    }

    private String a(int i2) {
        return i2 == 1 ? "connected" : i2 == 0 ? "connecting" : i2 == 2 ? "disconnected" : "unknown";
    }

    public void a(int i2, int i3, Exception exc) {
        if (i2 != this.r) {
            b.a(String.format("update the connection status. %1$s -> %2$s : %3$s ", a(this.r), a(i2), o.a(i3)));
        }
        if (i2 == 1) {
            this.q.a(10);
            if (this.r != 0) {
                b.a("try set connected while not connecting.");
            }
            this.r = i2;
            for (ConnectionListener i4 : this.b) {
                i4.i();
            }
        } else if (i2 == 0) {
            this.q.h();
            if (this.r != 2) {
                b.a("try set connecting while not disconnected.");
            }
            this.r = i2;
            for (ConnectionListener g2 : this.b) {
                g2.g();
            }
        } else if (i2 == 2) {
            this.q.a(10);
            if (this.r == 0) {
                for (ConnectionListener a2 : this.b) {
                    a2.a(exc == null ? new CancellationException("disconnect while connecting") : exc);
                }
            } else if (this.r == 1) {
                for (ConnectionListener b2 : this.b) {
                    b2.b(i3, exc);
                }
            }
            this.r = i2;
        }
    }

    public abstract void a(n.b bVar);

    public void a(String str) {
        this.n = str;
        a(1, 0, (Exception) null);
    }

    public abstract void a(String str, String str2);

    public void a(ConnectionListener connectionListener) {
        if (connectionListener != null && !this.b.contains(connectionListener)) {
            this.b.add(connectionListener);
        }
    }

    public void a(PacketListener packetListener, PacketFilter packetFilter) {
        if (packetListener == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        this.g.put(packetListener, new ListenerWrapper(packetListener, packetFilter));
    }

    public abstract void a(Packet packet);

    public abstract void a(Presence presence, int i2, Exception exc);

    public abstract void a(Packet[] packetArr);

    /* access modifiers changed from: protected */
    public void b() {
        String str;
        Class<?> cls = null;
        if (this.l != null && this.m != null && this.p.l()) {
            if (this.k == null) {
                try {
                    str = System.getProperty("smack.debuggerClass");
                } catch (Throwable th) {
                    str = null;
                }
                if (str != null) {
                    try {
                        cls = Class.forName(str);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (cls == null) {
                    this.k = new AndroidDebugger(this, this.m, this.l);
                    this.l = this.k.a();
                    this.m = this.k.b();
                    return;
                }
                try {
                    this.k = (SmackDebugger) cls.getConstructor(Connection.class, Writer.class, Reader.class).newInstance(this, this.m, this.l);
                    this.l = this.k.a();
                    this.m = this.k.b();
                } catch (Exception e3) {
                    throw new IllegalArgumentException("Can't initialize the configured debugger!", e3);
                }
            } else {
                this.l = this.k.a(this.l);
                this.m = this.k.a(this.m);
            }
        }
    }

    public void b(ConnectionListener connectionListener) {
        this.b.remove(connectionListener);
    }

    public void b(PacketListener packetListener, PacketFilter packetFilter) {
        if (packetListener == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        this.h.put(packetListener, new ListenerWrapper(packetListener, packetFilter));
    }

    public abstract void c();

    /* access modifiers changed from: protected */
    public void c(Packet packet) {
        for (ListenerWrapper a2 : this.h.values()) {
            a2.a(packet);
        }
    }

    public String d() {
        return this.p.h();
    }

    /* access modifiers changed from: protected */
    public void d(Packet packet) {
        Iterator<PacketListener> it = this.j.iterator();
        while (it.hasNext()) {
            it.next().a(packet);
        }
    }

    public String e() {
        return this.p.k();
    }

    /* access modifiers changed from: protected */
    public void e(Packet packet) {
        if (packet != null) {
            for (InterceptorWrapper a2 : this.i.values()) {
                a2.a(packet);
            }
        }
    }

    public String f() {
        return this.p.i();
    }

    public int g() {
        return this.p.j();
    }

    public Collection<ConnectionListener> h() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public Collection<PacketCollector> i() {
        return this.f;
    }

    public boolean j() {
        return this.r == 0;
    }

    public boolean k() {
        return this.r == 1;
    }

    public int l() {
        return this.d;
    }

    public void m() {
        this.d = 0;
    }

    public long n() {
        return this.e;
    }

    public void o() {
        this.e = -1;
    }

    public int p() {
        return this.r;
    }

    public void q() {
        this.s = System.currentTimeMillis();
    }

    public boolean r() {
        return System.currentTimeMillis() - this.s < ((long) SmackConfiguration.c());
    }
}
