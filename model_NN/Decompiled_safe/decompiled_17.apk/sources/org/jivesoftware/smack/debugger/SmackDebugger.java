package org.jivesoftware.smack.debugger;

import java.io.Reader;
import java.io.Writer;
import org.jivesoftware.smack.PacketListener;

public interface SmackDebugger {
    Reader a();

    Reader a(Reader reader);

    Writer a(Writer writer);

    Writer b();

    PacketListener c();

    PacketListener d();
}
