package org.jivesoftware.smack.debugger;

import java.io.Reader;
import java.io.Writer;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.util.ObservableReader;
import org.jivesoftware.smack.util.ObservableWriter;
import org.jivesoftware.smack.util.ReaderListener;
import org.jivesoftware.smack.util.WriterListener;

public class ConsoleDebugger implements SmackDebugger {
    public static boolean a = false;
    private PacketListener b;
    private Writer c;
    private Reader d;
    private ReaderListener e;
    private WriterListener f;

    public Reader a() {
        return this.d;
    }

    public Reader a(Reader reader) {
        ((ObservableReader) this.d).b(this.e);
        ObservableReader observableReader = new ObservableReader(reader);
        observableReader.a(this.e);
        this.d = observableReader;
        return this.d;
    }

    public Writer a(Writer writer) {
        ((ObservableWriter) this.c).b(this.f);
        ObservableWriter observableWriter = new ObservableWriter(writer);
        observableWriter.a(this.f);
        this.c = observableWriter;
        return this.c;
    }

    public Writer b() {
        return this.c;
    }

    public PacketListener c() {
        return this.b;
    }

    public PacketListener d() {
        return null;
    }
}
