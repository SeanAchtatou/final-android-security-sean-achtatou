package org.jivesoftware.smack;

import android.text.TextUtils;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.xiaomi.channel.commonutils.c.b;
import com.xiaomi.push.service.n;
import java.util.HashMap;
import org.jivesoftware.smack.packet.CommonPacketExtension;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.StringUtils;

public class XMBinder {

    public class Bind extends Packet {
        public Bind(n.b bVar, String str, Connection connection) {
            String str2;
            String str3;
            HashMap hashMap = new HashMap();
            int l = connection.l();
            hashMap.put("challenge", str);
            hashMap.put("token", bVar.c);
            hashMap.put("chid", bVar.h);
            hashMap.put("from", bVar.b);
            hashMap.put(LocaleUtil.INDONESIAN, k());
            hashMap.put("to", "xiaomi.com");
            if (bVar.e) {
                hashMap.put("kick", "1");
            } else {
                hashMap.put("kick", "0");
            }
            if (connection.n() > 0) {
                String format = String.format("conn:%1$d,t:%2$d", Integer.valueOf(l), Long.valueOf(connection.n()));
                hashMap.put("pf", format);
                connection.m();
                connection.o();
                str2 = format;
            } else {
                str2 = null;
            }
            if (!TextUtils.isEmpty(bVar.f)) {
                hashMap.put("client_attrs", bVar.f);
            } else {
                hashMap.put("client_attrs", "");
            }
            if (!TextUtils.isEmpty(bVar.g)) {
                hashMap.put("cloud_attrs", bVar.g);
            } else {
                hashMap.put("cloud_attrs", "");
            }
            if (bVar.d.equals("XIAOMI-PASS") || bVar.d.equals("XMPUSH-PASS")) {
                str3 = b.a(bVar.d, null, hashMap, bVar.i);
            } else {
                if (bVar.d.equals("XIAOMI-SASL")) {
                }
                str3 = null;
            }
            l(bVar.h);
            n(bVar.b);
            m("xiaomi.com");
            CommonPacketExtension commonPacketExtension = new CommonPacketExtension("token", null, null, null);
            commonPacketExtension.b(bVar.c);
            a(commonPacketExtension);
            CommonPacketExtension commonPacketExtension2 = new CommonPacketExtension("kick", null, null, null);
            commonPacketExtension2.b(bVar.e ? "1" : "0");
            a(commonPacketExtension2);
            CommonPacketExtension commonPacketExtension3 = new CommonPacketExtension("sig", null, null, null);
            commonPacketExtension3.b(str3);
            a(commonPacketExtension3);
            CommonPacketExtension commonPacketExtension4 = new CommonPacketExtension("method", null, null, null);
            if (!TextUtils.isEmpty(bVar.d)) {
                commonPacketExtension4.b(bVar.d);
            } else {
                commonPacketExtension4.b("XIAOMI-SASL");
            }
            a(commonPacketExtension4);
            CommonPacketExtension commonPacketExtension5 = new CommonPacketExtension("client_attrs", null, null, null);
            commonPacketExtension5.b(bVar.f == null ? "" : StringUtils.a(bVar.f));
            a(commonPacketExtension5);
            CommonPacketExtension commonPacketExtension6 = new CommonPacketExtension("cloud_attrs", null, null, null);
            commonPacketExtension6.b(bVar.g == null ? "" : StringUtils.a(bVar.g));
            a(commonPacketExtension6);
            if (!TextUtils.isEmpty(str2)) {
                CommonPacketExtension commonPacketExtension7 = new CommonPacketExtension("pf", null, null, null);
                commonPacketExtension7.b(str2);
                a(commonPacketExtension7);
            }
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            sb.append("<bind ");
            if (k() != null) {
                sb.append("id=\"" + k() + "\" ");
            }
            if (m() != null) {
                sb.append("to=\"").append(StringUtils.a(m())).append("\" ");
            }
            if (n() != null) {
                sb.append("from=\"").append(StringUtils.a(n())).append("\" ");
            }
            if (l() != null) {
                sb.append("chid=\"").append(StringUtils.a(l())).append("\">");
            }
            if (p() != null) {
                for (CommonPacketExtension d : p()) {
                    sb.append(d.d());
                }
            }
            sb.append("</bind>");
            return sb.toString();
        }
    }

    public static class BindResult extends Packet {
        private Type a;

        public static class Type {
            public static final Type a = new Type("result");
            public static final Type b = new Type("error");
            private String c;

            private Type(String str) {
                this.c = str;
            }

            public static Type a(String str) {
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    if (b.toString().equals(lowerCase)) {
                        return b;
                    }
                    if (a.toString().equals(lowerCase)) {
                        return a;
                    }
                }
                return null;
            }

            public String toString() {
                return this.c;
            }
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            sb.append("<bind ");
            if (k() != null) {
                sb.append("id=\"" + k() + "\" ");
            }
            if (m() != null) {
                sb.append("to=\"").append(StringUtils.a(m())).append("\" ");
            }
            if (n() != null) {
                sb.append("from=\"").append(StringUtils.a(n())).append("\" ");
            }
            if (l() != null) {
                sb.append(" chid=\"").append(StringUtils.a(l())).append("\" ");
            }
            if (this.a == null) {
                sb.append("type=\"result\">");
            } else {
                sb.append("type=\"").append(b()).append("\">");
            }
            if (p() != null) {
                for (CommonPacketExtension d : p()) {
                    sb.append(d.d());
                }
            }
            XMPPError o = o();
            if (o != null) {
                sb.append(o.d());
            }
            sb.append("</bind>");
            return sb.toString();
        }

        public void a(Type type) {
            if (type == null) {
                this.a = Type.a;
            } else {
                this.a = type;
            }
        }

        public Type b() {
            return this.a;
        }
    }

    public void a(n.b bVar, String str, Connection connection) {
        Bind bind = new Bind(bVar, str, connection);
        connection.a(bind);
        com.xiaomi.channel.commonutils.logger.b.a("SMACK: bind id=" + bind.k());
    }
}
