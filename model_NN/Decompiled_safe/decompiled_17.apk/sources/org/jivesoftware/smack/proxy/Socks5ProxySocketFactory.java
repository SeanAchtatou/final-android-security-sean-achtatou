package org.jivesoftware.smack.proxy;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.SocketFactory;
import org.jivesoftware.smack.proxy.ProxyInfo;

public class Socks5ProxySocketFactory extends SocketFactory {
    private ProxyInfo a;

    public Socks5ProxySocketFactory(ProxyInfo proxyInfo) {
        this.a = proxyInfo;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0060, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0131, code lost:
        throw new org.jivesoftware.smack.proxy.ProxyException(org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS5, r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x015a, code lost:
        throw new java.io.IOException(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x015b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x015c, code lost:
        r12 = r0;
        r0 = null;
        r1 = r12;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005f A[ExcHandler: RuntimeException (r0v12 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x010c A[SYNTHETIC, Splitter:B:31:0x010c] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0155  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:10:0x0055=Splitter:B:10:0x0055, B:26:0x00eb=Splitter:B:26:0x00eb} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.net.Socket a(java.lang.String r14, int r15) {
        /*
            r13 = this;
            r2 = 1
            r1 = 0
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r13.a
            java.lang.String r3 = r0.c()
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r13.a
            int r4 = r0.d()
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r13.a
            java.lang.String r5 = r0.e()
            org.jivesoftware.smack.proxy.ProxyInfo r0 = r13.a
            java.lang.String r6 = r0.f()
            java.net.Socket r0 = new java.net.Socket     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x015b }
            r0.<init>(r3, r4)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x015b }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.io.OutputStream r4 = r0.getOutputStream()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r7 = 1
            r0.setTcpNoDelay(r7)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 0
            r9 = 5
            r7[r8] = r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 1
            r9 = 2
            r7[r8] = r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 2
            r9 = 0
            r7[r8] = r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 3
            r9 = 2
            r7[r8] = r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 0
            r9 = 4
            r4.write(r7, r8, r9)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 2
            r13.a(r3, r7, r8)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 1
            byte r8 = r7[r8]     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = r8 & 255(0xff, float:3.57E-43)
            switch(r8) {
                case 0: goto L_0x0168;
                case 1: goto L_0x0050;
                case 2: goto L_0x0061;
                default: goto L_0x0050;
            }
        L_0x0050:
            if (r1 != 0) goto L_0x00ad
            r0.close()     // Catch:{ Exception -> 0x0161, RuntimeException -> 0x005f }
        L_0x0055:
            org.jivesoftware.smack.proxy.ProxyException r1 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS5     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.lang.String r3 = "fail in SOCKS5 proxy"
            r1.<init>(r2, r3)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            throw r1     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
        L_0x005f:
            r0 = move-exception
            throw r0
        L_0x0061:
            if (r5 == 0) goto L_0x0050
            if (r6 == 0) goto L_0x0050
            r8 = 0
            r9 = 1
            r7[r8] = r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r8 = 1
            int r9 = r5.length()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            byte r9 = (byte) r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r7[r8] = r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            byte[] r8 = r5.getBytes()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r9 = 0
            r10 = 2
            int r11 = r5.length()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.lang.System.arraycopy(r8, r9, r7, r10, r11)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            int r5 = r5.length()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            int r5 = r5 + 2
            int r8 = r5 + 1
            int r9 = r6.length()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            byte r9 = (byte) r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r7[r5] = r9     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            byte[] r5 = r6.getBytes()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r9 = 0
            int r10 = r6.length()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.lang.System.arraycopy(r5, r9, r7, r8, r10)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r5 = 0
            int r6 = r6.length()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            int r6 = r6 + r8
            r4.write(r7, r5, r6)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r5 = 2
            r13.a(r3, r7, r5)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r5 = 1
            byte r5 = r7[r5]     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            if (r5 != 0) goto L_0x0050
            r1 = r2
            goto L_0x0050
        L_0x00ad:
            r1 = 0
            r2 = 5
            r7[r1] = r2     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1 = 1
            r2 = 1
            r7[r1] = r2     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1 = 2
            r2 = 0
            r7[r1] = r2     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            byte[] r1 = r14.getBytes()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            int r2 = r1.length     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r5 = 3
            r6 = 3
            r7[r5] = r6     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r5 = 4
            byte r6 = (byte) r2     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r7[r5] = r6     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r5 = 0
            r6 = 5
            java.lang.System.arraycopy(r1, r5, r7, r6, r2)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            int r1 = r2 + 5
            int r2 = r1 + 1
            int r5 = r15 >>> 8
            byte r5 = (byte) r5     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r7[r1] = r5     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            int r1 = r2 + 1
            r5 = r15 & 255(0xff, float:3.57E-43)
            byte r5 = (byte) r5     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r7[r2] = r5     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r2 = 0
            r4.write(r7, r2, r1)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1 = 4
            r13.a(r3, r7, r1)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1 = 1
            byte r1 = r7[r1]     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            if (r1 == 0) goto L_0x0132
            r0.close()     // Catch:{ Exception -> 0x0164, RuntimeException -> 0x005f }
        L_0x00eb:
            org.jivesoftware.smack.proxy.ProxyException r1 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS5     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r3.<init>()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.lang.String r4 = "server returns "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r4 = 1
            byte r4 = r7[r4]     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1.<init>(r2, r3)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            throw r1     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
        L_0x0109:
            r1 = move-exception
        L_0x010a:
            if (r0 == 0) goto L_0x010f
            r0.close()     // Catch:{ Exception -> 0x0166 }
        L_0x010f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "ProxySOCKS5: "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r1.toString()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            boolean r2 = r1 instanceof java.lang.Throwable
            if (r2 == 0) goto L_0x0155
            org.jivesoftware.smack.proxy.ProxyException r2 = new org.jivesoftware.smack.proxy.ProxyException
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r3 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS5
            r2.<init>(r3, r0, r1)
            throw r2
        L_0x0132:
            r1 = 3
            byte r1 = r7[r1]     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1 = r1 & 255(0xff, float:3.57E-43)
            switch(r1) {
                case 1: goto L_0x013b;
                case 2: goto L_0x013a;
                case 3: goto L_0x0140;
                case 4: goto L_0x014f;
                default: goto L_0x013a;
            }     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
        L_0x013a:
            return r0
        L_0x013b:
            r1 = 6
            r13.a(r3, r7, r1)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            goto L_0x013a
        L_0x0140:
            r1 = 1
            r13.a(r3, r7, r1)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1 = 0
            byte r1 = r7[r1]     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 + 2
            r13.a(r3, r7, r1)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            goto L_0x013a
        L_0x014f:
            r1 = 18
            r13.a(r3, r7, r1)     // Catch:{ RuntimeException -> 0x005f, Exception -> 0x0109 }
            goto L_0x013a
        L_0x0155:
            java.io.IOException r1 = new java.io.IOException
            r1.<init>(r0)
            throw r1
        L_0x015b:
            r0 = move-exception
            r1 = 0
            r12 = r0
            r0 = r1
            r1 = r12
            goto L_0x010a
        L_0x0161:
            r1 = move-exception
            goto L_0x0055
        L_0x0164:
            r1 = move-exception
            goto L_0x00eb
        L_0x0166:
            r0 = move-exception
            goto L_0x010f
        L_0x0168:
            r1 = r2
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.proxy.Socks5ProxySocketFactory.a(java.lang.String, int):java.net.Socket");
    }

    private void a(InputStream inputStream, byte[] bArr, int i) {
        int i2 = 0;
        while (i2 < i) {
            int read = inputStream.read(bArr, i2, i - i2);
            if (read <= 0) {
                throw new ProxyException(ProxyInfo.ProxyType.SOCKS5, "stream is closed");
            }
            i2 += read;
        }
    }

    public Socket createSocket(String str, int i) {
        return a(str, i);
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return a(str, i);
    }

    public Socket createSocket(InetAddress inetAddress, int i) {
        return a(inetAddress.getHostAddress(), i);
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return a(inetAddress.getHostAddress(), i);
    }
}
