package org.jivesoftware.smack.proxy;

import javax.net.SocketFactory;

public class ProxyInfo {
    private String a;
    private int b;
    private String c;
    private String d;
    private ProxyType e;

    public enum ProxyType {
        NONE,
        HTTP,
        SOCKS4,
        SOCKS5
    }

    public ProxyInfo(ProxyType proxyType, String str, int i, String str2, String str3) {
        this.e = proxyType;
        this.a = str;
        this.b = i;
        this.c = str2;
        this.d = str3;
    }

    public static ProxyInfo a() {
        return new ProxyInfo(ProxyType.NONE, null, 0, null, null);
    }

    public ProxyType b() {
        return this.e;
    }

    public String c() {
        return this.a;
    }

    public int d() {
        return this.b;
    }

    public String e() {
        return this.c;
    }

    public String f() {
        return this.d;
    }

    public SocketFactory g() {
        if (this.e == ProxyType.NONE) {
            return new a();
        }
        if (this.e == ProxyType.HTTP) {
            return new HTTPProxySocketFactory(this);
        }
        if (this.e == ProxyType.SOCKS4) {
            return new Socks4ProxySocketFactory(this);
        }
        if (this.e == ProxyType.SOCKS5) {
            return new Socks5ProxySocketFactory(this);
        }
        return null;
    }
}
