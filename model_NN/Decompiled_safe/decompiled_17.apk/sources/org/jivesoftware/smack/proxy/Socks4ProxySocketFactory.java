package org.jivesoftware.smack.proxy;

import java.net.InetAddress;
import java.net.Socket;
import javax.net.SocketFactory;

public class Socks4ProxySocketFactory extends SocketFactory {
    private ProxyInfo a;

    public Socks4ProxySocketFactory(ProxyInfo proxyInfo) {
        this.a = proxyInfo;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0059, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0065, code lost:
        throw new org.jivesoftware.smack.proxy.ProxyException(org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4, r0.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0066, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0067, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x009a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x009b, code lost:
        if (r1 != null) goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ab, code lost:
        throw new org.jivesoftware.smack.proxy.ProxyException(org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4, r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0102, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0103, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0066 A[ExcHandler: RuntimeException (r0v4 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x0019] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009d A[SYNTHETIC, Splitter:B:29:0x009d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.net.Socket a(java.lang.String r12, int r13) {
        /*
            r11 = this;
            r0 = 4
            r2 = 0
            org.jivesoftware.smack.proxy.ProxyInfo r1 = r11.a
            java.lang.String r3 = r1.c()
            org.jivesoftware.smack.proxy.ProxyInfo r1 = r11.a
            int r4 = r1.d()
            org.jivesoftware.smack.proxy.ProxyInfo r1 = r11.a
            java.lang.String r5 = r1.e()
            org.jivesoftware.smack.proxy.ProxyInfo r1 = r11.a
            r1.f()
            java.net.Socket r1 = new java.net.Socket     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x0102 }
            r1.<init>(r3, r4)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x0102 }
            java.io.InputStream r6 = r1.getInputStream()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.io.OutputStream r7 = r1.getOutputStream()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r3 = 1
            r1.setTcpNoDelay(r3)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r8 = new byte[r3]     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r3 = 0
            r4 = 4
            r8[r3] = r4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r3 = 1
            r4 = 1
            r8[r3] = r4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r3 = 2
            int r4 = r13 >>> 8
            byte r4 = (byte) r4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r8[r3] = r4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r3 = 3
            r4 = r13 & 255(0xff, float:3.57E-43)
            byte r4 = (byte) r4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r8[r3] = r4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.net.InetAddress r3 = java.net.InetAddress.getByName(r12)     // Catch:{ UnknownHostException -> 0x0059 }
            byte[] r9 = r3.getAddress()     // Catch:{ UnknownHostException -> 0x0059 }
            r3 = r2
        L_0x004b:
            int r4 = r9.length     // Catch:{ UnknownHostException -> 0x0059 }
            if (r3 >= r4) goto L_0x0068
            int r4 = r0 + 1
            byte r10 = r9[r3]     // Catch:{ UnknownHostException -> 0x0059 }
            r8[r0] = r10     // Catch:{ UnknownHostException -> 0x0059 }
            int r0 = r3 + 1
            r3 = r0
            r0 = r4
            goto L_0x004b
        L_0x0059:
            r0 = move-exception
            org.jivesoftware.smack.proxy.ProxyException r2 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r3 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.String r4 = r0.toString()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r2.<init>(r3, r4, r0)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            throw r2     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
        L_0x0066:
            r0 = move-exception
            throw r0
        L_0x0068:
            if (r5 == 0) goto L_0x007b
            byte[] r3 = r5.getBytes()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r4 = 0
            int r9 = r5.length()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.System.arraycopy(r3, r4, r8, r0, r9)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            int r3 = r5.length()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            int r0 = r0 + r3
        L_0x007b:
            int r3 = r0 + 1
            r4 = 0
            r8[r0] = r4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r0 = 0
            r7.write(r8, r0, r3)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r0 = r2
        L_0x0085:
            r2 = 6
            if (r0 >= r2) goto L_0x00ae
            int r2 = 6 - r0
            int r2 = r6.read(r8, r0, r2)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            if (r2 > 0) goto L_0x00ac
            org.jivesoftware.smack.proxy.ProxyException r0 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.String r3 = "stream is closed"
            r0.<init>(r2, r3)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            throw r0     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
        L_0x009a:
            r0 = move-exception
        L_0x009b:
            if (r1 == 0) goto L_0x00a0
            r1.close()     // Catch:{ Exception -> 0x0107 }
        L_0x00a0:
            org.jivesoftware.smack.proxy.ProxyException r1 = new org.jivesoftware.smack.proxy.ProxyException
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4
            java.lang.String r0 = r0.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00ac:
            int r0 = r0 + r2
            goto L_0x0085
        L_0x00ae:
            r0 = 0
            byte r0 = r8[r0]     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            if (r0 == 0) goto L_0x00d1
            org.jivesoftware.smack.proxy.ProxyException r0 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r3.<init>()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.String r4 = "server returns VN "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r4 = 0
            byte r4 = r8[r4]     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r0.<init>(r2, r3)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            throw r0     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
        L_0x00d1:
            r0 = 1
            byte r0 = r8[r0]     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r2 = 90
            if (r0 == r2) goto L_0x00f9
            r1.close()     // Catch:{ Exception -> 0x0105, RuntimeException -> 0x0066 }
        L_0x00db:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r0.<init>()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.String r2 = "ProxySOCKS4: server returns CD "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r2 = 1
            byte r2 = r8[r2]     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            org.jivesoftware.smack.proxy.ProxyException r2 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r3 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r2.<init>(r3, r0)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            throw r2     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
        L_0x00f9:
            r0 = 2
            byte[] r0 = new byte[r0]     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            r2 = 0
            r3 = 2
            r6.read(r0, r2, r3)     // Catch:{ RuntimeException -> 0x0066, Exception -> 0x009a }
            return r1
        L_0x0102:
            r0 = move-exception
            r1 = 0
            goto L_0x009b
        L_0x0105:
            r0 = move-exception
            goto L_0x00db
        L_0x0107:
            r1 = move-exception
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.proxy.Socks4ProxySocketFactory.a(java.lang.String, int):java.net.Socket");
    }

    public Socket createSocket(String str, int i) {
        return a(str, i);
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return a(str, i);
    }

    public Socket createSocket(InetAddress inetAddress, int i) {
        return a(inetAddress.getHostAddress(), i);
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return a(inetAddress.getHostAddress(), i);
    }
}
