package org.jivesoftware.smack.proxy;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.SocketFactory;
import org.jivesoftware.smack.proxy.ProxyInfo;
import org.jivesoftware.smack.util.Base64;

public class HTTPProxySocketFactory extends SocketFactory {
    private static final Pattern b = Pattern.compile("HTTP/\\S+\\s(\\d+)\\s(.*)\\s*");
    private ProxyInfo a;

    public HTTPProxySocketFactory(ProxyInfo proxyInfo) {
        this.a = proxyInfo;
    }

    private Socket a(String str, int i) {
        String str2;
        String c = this.a.c();
        Socket socket = new Socket(c, this.a.d());
        String str3 = "CONNECT " + str + ":" + i;
        String e = this.a.e();
        if (e == null) {
            str2 = "";
        } else {
            str2 = "\r\nProxy-Authorization: Basic " + new String(Base64.a((e + ":" + this.a.f()).getBytes("UTF-8")));
        }
        socket.getOutputStream().write((str3 + " HTTP/1.1\r\nHost: " + str3 + str2 + "\r\n\r\n").getBytes("UTF-8"));
        InputStream inputStream = socket.getInputStream();
        StringBuilder sb = new StringBuilder(100);
        int i2 = 0;
        do {
            char read = (char) inputStream.read();
            sb.append(read);
            if (sb.length() > 1024) {
                throw new ProxyException(ProxyInfo.ProxyType.HTTP, "Recieved header of >1024 characters from " + c + ", cancelling connection");
            } else if (read == 65535) {
                throw new ProxyException(ProxyInfo.ProxyType.HTTP);
            } else if ((i2 == 0 || i2 == 2) && read == 13) {
                i2++;
                continue;
            } else if ((i2 == 1 || i2 == 3) && read == 10) {
                i2++;
                continue;
            } else {
                i2 = 0;
                continue;
            }
        } while (i2 != 4);
        if (i2 != 4) {
            throw new ProxyException(ProxyInfo.ProxyType.HTTP, "Never received blank line from " + c + ", cancelling connection");
        }
        String readLine = new BufferedReader(new StringReader(sb.toString())).readLine();
        if (readLine == null) {
            throw new ProxyException(ProxyInfo.ProxyType.HTTP, "Empty proxy response from " + c + ", cancelling");
        }
        Matcher matcher = b.matcher(readLine);
        if (!matcher.matches()) {
            throw new ProxyException(ProxyInfo.ProxyType.HTTP, "Unexpected proxy response from " + c + ": " + readLine);
        } else if (Integer.parseInt(matcher.group(1)) == 200) {
            return socket;
        } else {
            throw new ProxyException(ProxyInfo.ProxyType.HTTP);
        }
    }

    public Socket createSocket(String str, int i) {
        return a(str, i);
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return a(str, i);
    }

    public Socket createSocket(InetAddress inetAddress, int i) {
        return a(inetAddress.getHostAddress(), i);
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return a(inetAddress.getHostAddress(), i);
    }
}
