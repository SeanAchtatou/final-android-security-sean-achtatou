package org.jivesoftware.smack;

import android.os.Build;
import com.baixing.util.TraceUtil;
import java.io.IOException;
import java.io.Writer;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

class h {
    private Writer a;
    private XMPPConnection b;

    protected h(XMPPConnection xMPPConnection) {
        this.b = xMPPConnection;
        this.a = xMPPConnection.m;
    }

    private void b(Packet packet) {
        synchronized (this.a) {
            try {
                this.a.write(packet.a() + TraceUtil.LINE);
                this.a.flush();
                this.b.d(packet);
            } catch (IOException e) {
                throw new XMPPException(e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b.i.clear();
        this.b.h.clear();
    }

    public void a(Packet packet) {
        this.b.e(packet);
        b(packet);
        this.b.c(packet);
    }

    public void b() {
        synchronized (this.a) {
            this.a.write("</stream:stream>");
            this.a.flush();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        StringBuilder sb = new StringBuilder();
        sb.append("<stream:stream");
        sb.append(" xmlns=\"xm\"");
        sb.append(" xmlns:stream=\"xm\"");
        sb.append(" to=\"").append(this.b.d()).append("\"");
        sb.append(" version=\"105\"");
        sb.append(" model=\"").append(StringUtils.a(Build.MODEL)).append("\"");
        sb.append(" os=\"").append(StringUtils.a(Build.VERSION.INCREMENTAL)).append("\"");
        sb.append(" connpt=\"").append(StringUtils.a(this.b.f())).append("\"");
        sb.append(" host=\"").append(this.b.e()).append("\"");
        sb.append(">");
        this.a.write(sb.toString());
        this.a.flush();
    }

    public void d() {
        synchronized (this.a) {
            try {
                this.a.write(this.b.t() + TraceUtil.LINE);
                this.a.flush();
                this.b.v();
            } catch (IOException e) {
                throw new XMPPException(e);
            }
        }
    }
}
