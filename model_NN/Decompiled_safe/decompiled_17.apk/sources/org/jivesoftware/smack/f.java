package org.jivesoftware.smack;

import com.tencent.mm.sdk.message.RMsgInfoDB;
import com.xiaomi.channel.commonutils.logger.b;
import java.util.LinkedList;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

class f {
    private Thread a;
    private XMPPConnection b;
    private XmlPullParser c;
    private boolean d;

    protected f(XMPPConnection xMPPConnection) {
        this.b = xMPPConnection;
        a();
    }

    private void a(Packet packet) {
        if (packet != null) {
            for (PacketCollector a2 : this.b.f) {
                a2.a(packet);
            }
            for (Connection.ListenerWrapper a3 : this.b.g.values()) {
                a3.a(packet);
            }
        }
    }

    private void e() {
        this.c = XmlPullParserFactory.newInstance().newPullParser();
        this.c.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        this.c.setInput(this.b.l);
    }

    /* access modifiers changed from: private */
    public void f() {
        e();
        int eventType = this.c.getEventType();
        String str = "";
        do {
            try {
                this.b.q();
                if (eventType == 2) {
                    String name = this.c.getName();
                    if (this.c.getName().equals(RMsgInfoDB.TABLE)) {
                        a(PacketParserUtils.a(this.c));
                        str = name;
                    } else if (this.c.getName().equals("iq")) {
                        a(PacketParserUtils.a(this.c, this.b));
                        str = name;
                    } else if (this.c.getName().equals("presence")) {
                        a(PacketParserUtils.b(this.c));
                        str = name;
                    } else if (this.c.getName().equals("stream")) {
                        String str2 = "";
                        for (int i = 0; i < this.c.getAttributeCount(); i++) {
                            if (this.c.getAttributeName(i).equals("from")) {
                                this.b.p.a(this.c.getAttributeValue(i));
                            } else if (this.c.getAttributeName(i).equals("challenge")) {
                                str2 = this.c.getAttributeValue(i);
                            }
                        }
                        new LinkedList().add("XIAOMI-SASL");
                        this.b.a(str2);
                        str = name;
                    } else if (this.c.getName().equals("error")) {
                        throw new XMPPException(PacketParserUtils.e(this.c));
                    } else {
                        if (this.c.getName().equals("warning")) {
                            this.c.next();
                            if (this.c.getName().equals("multi-login")) {
                                a(6, null);
                                str = name;
                            }
                        } else if (this.c.getName().equals("bind")) {
                            a(PacketParserUtils.c(this.c));
                            str = name;
                        }
                        str = name;
                    }
                } else if (eventType == 3 && this.c.getName().equals("stream")) {
                    a(13, null);
                }
                eventType = this.c.next();
                if (this.d) {
                    break;
                }
            } catch (Exception e) {
                b.a(e);
                if (!this.d) {
                    a(9, e);
                    return;
                } else {
                    b.b("reader is shutdown, ignore the exception.");
                    return;
                }
            }
        } while (eventType != 1);
        if (eventType == 1) {
            throw new Exception("SMACK: server close the connection or timeout happened, last element name=" + str + " host=" + this.b.e());
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.d = false;
        this.a = new g(this, "Smack Packet Reader (" + this.b.o + ")");
    }

    /* access modifiers changed from: package-private */
    public void a(int i, Exception exc) {
        this.d = true;
        this.b.a(i, exc);
    }

    public void b() {
        this.a.start();
    }

    public void c() {
        this.d = true;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.b.g.clear();
        this.b.f.clear();
    }
}
