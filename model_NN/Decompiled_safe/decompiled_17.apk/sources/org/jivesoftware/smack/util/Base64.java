package org.jivesoftware.smack.util;

import com.tencent.mm.sdk.contact.RContact;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Base64 {
    private static final byte[] a = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] b;
    private static final byte[] c = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] d;
    private static final byte[] e = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] f;

    public static class InputStream extends FilterInputStream {
        private boolean a;
        private int b;
        private byte[] c;
        private int d;
        private int e;
        private int f;
        private boolean g;
        private int h;
        private byte[] i;

        public int read() {
            int read;
            if (this.b < 0) {
                if (this.a) {
                    byte[] bArr = new byte[3];
                    int i2 = 0;
                    for (int i3 = 0; i3 < 3; i3++) {
                        try {
                            int read2 = this.in.read();
                            if (read2 >= 0) {
                                bArr[i3] = (byte) read2;
                                i2++;
                            }
                        } catch (IOException e2) {
                            if (i3 == 0) {
                                throw e2;
                            }
                        }
                    }
                    if (i2 <= 0) {
                        return -1;
                    }
                    byte[] unused = Base64.b(bArr, 0, i2, this.c, 0, this.h);
                    this.b = 0;
                    this.e = 4;
                } else {
                    byte[] bArr2 = new byte[4];
                    int i4 = 0;
                    while (i4 < 4) {
                        do {
                            read = this.in.read();
                            if (read < 0) {
                                break;
                            }
                        } while (this.i[read & RContact.MM_CONTACTFLAG_ALL] <= -5);
                        if (read < 0) {
                            break;
                        }
                        bArr2[i4] = (byte) read;
                        i4++;
                    }
                    if (i4 == 4) {
                        this.e = Base64.b(bArr2, 0, this.c, 0, this.h);
                        this.b = 0;
                    } else if (i4 == 0) {
                        return -1;
                    } else {
                        throw new IOException("Improperly padded Base64 input.");
                    }
                }
            }
            if (this.b < 0) {
                throw new IOException("Error in Base64 code reading stream.");
            } else if (this.b >= this.e) {
                return -1;
            } else {
                if (!this.a || !this.g || this.f < 76) {
                    this.f++;
                    byte[] bArr3 = this.c;
                    int i5 = this.b;
                    this.b = i5 + 1;
                    byte b2 = bArr3[i5];
                    if (this.b >= this.d) {
                        this.b = -1;
                    }
                    return b2 & 255;
                }
                this.f = 0;
                return 10;
            }
        }

        public int read(byte[] bArr, int i2, int i3) {
            int i4 = 0;
            while (i4 < i3) {
                int read = read();
                if (read >= 0) {
                    bArr[i2 + i4] = (byte) read;
                    i4++;
                } else if (i4 == 0) {
                    return -1;
                } else {
                    return i4;
                }
            }
            return i4;
        }
    }

    public static class OutputStream extends FilterOutputStream {
        private boolean a;
        private int b;
        private byte[] c;
        private int d;
        private int e;
        private boolean f;
        private byte[] g;
        private boolean h;
        private int i;
        private byte[] j;
        private byte[] k;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public OutputStream(java.io.OutputStream outputStream, int i2) {
            super(outputStream);
            boolean z = true;
            this.f = (i2 & 8) != 8;
            this.a = (i2 & 1) != 1 ? false : z;
            this.d = this.a ? 3 : 4;
            this.c = new byte[this.d];
            this.b = 0;
            this.e = 0;
            this.h = false;
            this.g = new byte[4];
            this.i = i2;
            this.j = Base64.c(i2);
            this.k = Base64.d(i2);
        }

        public void a() {
            if (this.b <= 0) {
                return;
            }
            if (this.a) {
                this.out.write(Base64.b(this.g, this.c, this.b, this.i));
                this.b = 0;
                return;
            }
            throw new IOException("Base64 input not properly padded.");
        }

        public void close() {
            a();
            super.close();
            this.c = null;
            this.out = null;
        }

        public void write(int i2) {
            if (this.h) {
                this.out.write(i2);
            } else if (this.a) {
                byte[] bArr = this.c;
                int i3 = this.b;
                this.b = i3 + 1;
                bArr[i3] = (byte) i2;
                if (this.b >= this.d) {
                    this.out.write(Base64.b(this.g, this.c, this.d, this.i));
                    this.e += 4;
                    if (this.f && this.e >= 76) {
                        this.out.write(10);
                        this.e = 0;
                    }
                    this.b = 0;
                }
            } else if (this.k[i2 & RContact.MM_CONTACTFLAG_ALL] > -5) {
                byte[] bArr2 = this.c;
                int i4 = this.b;
                this.b = i4 + 1;
                bArr2[i4] = (byte) i2;
                if (this.b >= this.d) {
                    this.out.write(this.g, 0, Base64.b(this.c, 0, this.g, 0, this.i));
                    this.b = 0;
                }
            } else if (this.k[i2 & RContact.MM_CONTACTFLAG_ALL] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public void write(byte[] bArr, int i2, int i3) {
            if (this.h) {
                this.out.write(bArr, i2, i3);
                return;
            }
            for (int i4 = 0; i4 < i3; i4++) {
                write(bArr[i2 + i4]);
            }
        }
    }

    static {
        byte[] bArr = new byte[RContact.MM_CONTACTFLAG_ALL];
        // fill-array-data instruction
        bArr[0] = -9;
        bArr[1] = -9;
        bArr[2] = -9;
        bArr[3] = -9;
        bArr[4] = -9;
        bArr[5] = -9;
        bArr[6] = -9;
        bArr[7] = -9;
        bArr[8] = -9;
        bArr[9] = -5;
        bArr[10] = -5;
        bArr[11] = -9;
        bArr[12] = -9;
        bArr[13] = -5;
        bArr[14] = -9;
        bArr[15] = -9;
        bArr[16] = -9;
        bArr[17] = -9;
        bArr[18] = -9;
        bArr[19] = -9;
        bArr[20] = -9;
        bArr[21] = -9;
        bArr[22] = -9;
        bArr[23] = -9;
        bArr[24] = -9;
        bArr[25] = -9;
        bArr[26] = -9;
        bArr[27] = -9;
        bArr[28] = -9;
        bArr[29] = -9;
        bArr[30] = -9;
        bArr[31] = -9;
        bArr[32] = -5;
        bArr[33] = -9;
        bArr[34] = -9;
        bArr[35] = -9;
        bArr[36] = -9;
        bArr[37] = -9;
        bArr[38] = -9;
        bArr[39] = -9;
        bArr[40] = -9;
        bArr[41] = -9;
        bArr[42] = -9;
        bArr[43] = 62;
        bArr[44] = -9;
        bArr[45] = -9;
        bArr[46] = -9;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -9;
        bArr[59] = -9;
        bArr[60] = -9;
        bArr[61] = -1;
        bArr[62] = -9;
        bArr[63] = -9;
        bArr[64] = -9;
        bArr[65] = 0;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -9;
        bArr[92] = -9;
        bArr[93] = -9;
        bArr[94] = -9;
        bArr[95] = -9;
        bArr[96] = -9;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        bArr[123] = -9;
        bArr[124] = -9;
        bArr[125] = -9;
        bArr[126] = -9;
        b = bArr;
        byte[] bArr2 = new byte[RContact.MM_CONTACTFLAG_ALL];
        // fill-array-data instruction
        bArr2[0] = -9;
        bArr2[1] = -9;
        bArr2[2] = -9;
        bArr2[3] = -9;
        bArr2[4] = -9;
        bArr2[5] = -9;
        bArr2[6] = -9;
        bArr2[7] = -9;
        bArr2[8] = -9;
        bArr2[9] = -5;
        bArr2[10] = -5;
        bArr2[11] = -9;
        bArr2[12] = -9;
        bArr2[13] = -5;
        bArr2[14] = -9;
        bArr2[15] = -9;
        bArr2[16] = -9;
        bArr2[17] = -9;
        bArr2[18] = -9;
        bArr2[19] = -9;
        bArr2[20] = -9;
        bArr2[21] = -9;
        bArr2[22] = -9;
        bArr2[23] = -9;
        bArr2[24] = -9;
        bArr2[25] = -9;
        bArr2[26] = -9;
        bArr2[27] = -9;
        bArr2[28] = -9;
        bArr2[29] = -9;
        bArr2[30] = -9;
        bArr2[31] = -9;
        bArr2[32] = -5;
        bArr2[33] = -9;
        bArr2[34] = -9;
        bArr2[35] = -9;
        bArr2[36] = -9;
        bArr2[37] = -9;
        bArr2[38] = -9;
        bArr2[39] = -9;
        bArr2[40] = -9;
        bArr2[41] = -9;
        bArr2[42] = -9;
        bArr2[43] = -9;
        bArr2[44] = -9;
        bArr2[45] = 62;
        bArr2[46] = -9;
        bArr2[47] = -9;
        bArr2[48] = 52;
        bArr2[49] = 53;
        bArr2[50] = 54;
        bArr2[51] = 55;
        bArr2[52] = 56;
        bArr2[53] = 57;
        bArr2[54] = 58;
        bArr2[55] = 59;
        bArr2[56] = 60;
        bArr2[57] = 61;
        bArr2[58] = -9;
        bArr2[59] = -9;
        bArr2[60] = -9;
        bArr2[61] = -1;
        bArr2[62] = -9;
        bArr2[63] = -9;
        bArr2[64] = -9;
        bArr2[65] = 0;
        bArr2[66] = 1;
        bArr2[67] = 2;
        bArr2[68] = 3;
        bArr2[69] = 4;
        bArr2[70] = 5;
        bArr2[71] = 6;
        bArr2[72] = 7;
        bArr2[73] = 8;
        bArr2[74] = 9;
        bArr2[75] = 10;
        bArr2[76] = 11;
        bArr2[77] = 12;
        bArr2[78] = 13;
        bArr2[79] = 14;
        bArr2[80] = 15;
        bArr2[81] = 16;
        bArr2[82] = 17;
        bArr2[83] = 18;
        bArr2[84] = 19;
        bArr2[85] = 20;
        bArr2[86] = 21;
        bArr2[87] = 22;
        bArr2[88] = 23;
        bArr2[89] = 24;
        bArr2[90] = 25;
        bArr2[91] = -9;
        bArr2[92] = -9;
        bArr2[93] = -9;
        bArr2[94] = -9;
        bArr2[95] = 63;
        bArr2[96] = -9;
        bArr2[97] = 26;
        bArr2[98] = 27;
        bArr2[99] = 28;
        bArr2[100] = 29;
        bArr2[101] = 30;
        bArr2[102] = 31;
        bArr2[103] = 32;
        bArr2[104] = 33;
        bArr2[105] = 34;
        bArr2[106] = 35;
        bArr2[107] = 36;
        bArr2[108] = 37;
        bArr2[109] = 38;
        bArr2[110] = 39;
        bArr2[111] = 40;
        bArr2[112] = 41;
        bArr2[113] = 42;
        bArr2[114] = 43;
        bArr2[115] = 44;
        bArr2[116] = 45;
        bArr2[117] = 46;
        bArr2[118] = 47;
        bArr2[119] = 48;
        bArr2[120] = 49;
        bArr2[121] = 50;
        bArr2[122] = 51;
        bArr2[123] = -9;
        bArr2[124] = -9;
        bArr2[125] = -9;
        bArr2[126] = -9;
        d = bArr2;
        byte[] bArr3 = new byte[RContact.MM_CONTACTFLAG_ALL];
        // fill-array-data instruction
        bArr3[0] = -9;
        bArr3[1] = -9;
        bArr3[2] = -9;
        bArr3[3] = -9;
        bArr3[4] = -9;
        bArr3[5] = -9;
        bArr3[6] = -9;
        bArr3[7] = -9;
        bArr3[8] = -9;
        bArr3[9] = -5;
        bArr3[10] = -5;
        bArr3[11] = -9;
        bArr3[12] = -9;
        bArr3[13] = -5;
        bArr3[14] = -9;
        bArr3[15] = -9;
        bArr3[16] = -9;
        bArr3[17] = -9;
        bArr3[18] = -9;
        bArr3[19] = -9;
        bArr3[20] = -9;
        bArr3[21] = -9;
        bArr3[22] = -9;
        bArr3[23] = -9;
        bArr3[24] = -9;
        bArr3[25] = -9;
        bArr3[26] = -9;
        bArr3[27] = -9;
        bArr3[28] = -9;
        bArr3[29] = -9;
        bArr3[30] = -9;
        bArr3[31] = -9;
        bArr3[32] = -5;
        bArr3[33] = -9;
        bArr3[34] = -9;
        bArr3[35] = -9;
        bArr3[36] = -9;
        bArr3[37] = -9;
        bArr3[38] = -9;
        bArr3[39] = -9;
        bArr3[40] = -9;
        bArr3[41] = -9;
        bArr3[42] = -9;
        bArr3[43] = -9;
        bArr3[44] = -9;
        bArr3[45] = 0;
        bArr3[46] = -9;
        bArr3[47] = -9;
        bArr3[48] = 1;
        bArr3[49] = 2;
        bArr3[50] = 3;
        bArr3[51] = 4;
        bArr3[52] = 5;
        bArr3[53] = 6;
        bArr3[54] = 7;
        bArr3[55] = 8;
        bArr3[56] = 9;
        bArr3[57] = 10;
        bArr3[58] = -9;
        bArr3[59] = -9;
        bArr3[60] = -9;
        bArr3[61] = -1;
        bArr3[62] = -9;
        bArr3[63] = -9;
        bArr3[64] = -9;
        bArr3[65] = 11;
        bArr3[66] = 12;
        bArr3[67] = 13;
        bArr3[68] = 14;
        bArr3[69] = 15;
        bArr3[70] = 16;
        bArr3[71] = 17;
        bArr3[72] = 18;
        bArr3[73] = 19;
        bArr3[74] = 20;
        bArr3[75] = 21;
        bArr3[76] = 22;
        bArr3[77] = 23;
        bArr3[78] = 24;
        bArr3[79] = 25;
        bArr3[80] = 26;
        bArr3[81] = 27;
        bArr3[82] = 28;
        bArr3[83] = 29;
        bArr3[84] = 30;
        bArr3[85] = 31;
        bArr3[86] = 32;
        bArr3[87] = 33;
        bArr3[88] = 34;
        bArr3[89] = 35;
        bArr3[90] = 36;
        bArr3[91] = -9;
        bArr3[92] = -9;
        bArr3[93] = -9;
        bArr3[94] = -9;
        bArr3[95] = 37;
        bArr3[96] = -9;
        bArr3[97] = 38;
        bArr3[98] = 39;
        bArr3[99] = 40;
        bArr3[100] = 41;
        bArr3[101] = 42;
        bArr3[102] = 43;
        bArr3[103] = 44;
        bArr3[104] = 45;
        bArr3[105] = 46;
        bArr3[106] = 47;
        bArr3[107] = 48;
        bArr3[108] = 49;
        bArr3[109] = 50;
        bArr3[110] = 51;
        bArr3[111] = 52;
        bArr3[112] = 53;
        bArr3[113] = 54;
        bArr3[114] = 55;
        bArr3[115] = 56;
        bArr3[116] = 57;
        bArr3[117] = 58;
        bArr3[118] = 59;
        bArr3[119] = 60;
        bArr3[120] = 61;
        bArr3[121] = 62;
        bArr3[122] = 63;
        bArr3[123] = -9;
        bArr3[124] = -9;
        bArr3[125] = -9;
        bArr3[126] = -9;
        f = bArr3;
    }

    private Base64() {
    }

    public static String a(byte[] bArr) {
        return a(bArr, 0, bArr.length, 0);
    }

    public static String a(byte[] bArr, int i, int i2, int i3) {
        ByteArrayOutputStream byteArrayOutputStream;
        OutputStream outputStream;
        IOException e2;
        GZIPOutputStream gZIPOutputStream;
        GZIPOutputStream gZIPOutputStream2 = null;
        int i4 = i3 & 8;
        if ((i3 & 2) == 2) {
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    outputStream = new OutputStream(byteArrayOutputStream, i3 | 1);
                    try {
                        gZIPOutputStream = new GZIPOutputStream(outputStream);
                        try {
                            gZIPOutputStream.write(bArr, i, i2);
                            gZIPOutputStream.close();
                            try {
                                gZIPOutputStream.close();
                            } catch (Exception e3) {
                            }
                            try {
                                outputStream.close();
                            } catch (Exception e4) {
                            }
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception e5) {
                            }
                            try {
                                return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                            } catch (UnsupportedEncodingException e6) {
                                return new String(byteArrayOutputStream.toByteArray());
                            }
                        } catch (IOException e7) {
                            e2 = e7;
                            try {
                                e2.printStackTrace();
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e8) {
                                }
                                try {
                                    outputStream.close();
                                } catch (Exception e9) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                    return null;
                                } catch (Exception e10) {
                                    return null;
                                }
                            } catch (Throwable th) {
                                Throwable th2 = th;
                                gZIPOutputStream2 = gZIPOutputStream;
                                th = th2;
                                try {
                                    gZIPOutputStream2.close();
                                } catch (Exception e11) {
                                }
                                try {
                                    outputStream.close();
                                } catch (Exception e12) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e13) {
                                }
                                throw th;
                            }
                        }
                    } catch (IOException e14) {
                        e2 = e14;
                        gZIPOutputStream = null;
                        e2.printStackTrace();
                        gZIPOutputStream.close();
                        outputStream.close();
                        byteArrayOutputStream.close();
                        return null;
                    } catch (Throwable th3) {
                        th = th3;
                        gZIPOutputStream2.close();
                        outputStream.close();
                        byteArrayOutputStream.close();
                        throw th;
                    }
                } catch (IOException e15) {
                    e2 = e15;
                    outputStream = null;
                    gZIPOutputStream = null;
                    e2.printStackTrace();
                    gZIPOutputStream.close();
                    outputStream.close();
                    byteArrayOutputStream.close();
                    return null;
                } catch (Throwable th4) {
                    th = th4;
                    outputStream = null;
                    gZIPOutputStream2.close();
                    outputStream.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e16) {
                e2 = e16;
                outputStream = null;
                byteArrayOutputStream = null;
                gZIPOutputStream = null;
                e2.printStackTrace();
                gZIPOutputStream.close();
                outputStream.close();
                byteArrayOutputStream.close();
                return null;
            } catch (Throwable th5) {
                th = th5;
                outputStream = null;
                byteArrayOutputStream = null;
                gZIPOutputStream2.close();
                outputStream.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = i4 == 0;
            int i5 = (i2 * 4) / 3;
            byte[] bArr2 = new byte[((z ? i5 / 76 : 0) + i5 + (i2 % 3 > 0 ? 4 : 0))];
            int i6 = i2 - 2;
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            while (i7 < i6) {
                b(bArr, i7 + i, 3, bArr2, i8, i3);
                int i10 = i9 + 4;
                if (z && i10 == 76) {
                    bArr2[i8 + 4] = 10;
                    i8++;
                    i10 = 0;
                }
                i8 += 4;
                i7 += 3;
                i9 = i10;
            }
            if (i7 < i2) {
                b(bArr, i7 + i, i2 - i7, bArr2, i8, i3);
                i8 += 4;
            }
            try {
                return new String(bArr2, 0, i8, "UTF-8");
            } catch (UnsupportedEncodingException e17) {
                return new String(bArr2, 0, i8);
            }
        }
    }

    public static byte[] a(String str) {
        return a(str, 0);
    }

    public static byte[] a(String str, int i) {
        byte[] bytes;
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayOutputStream byteArrayOutputStream2;
        GZIPInputStream gZIPInputStream = null;
        try {
            bytes = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
            bytes = str.getBytes();
        }
        byte[] b2 = b(bytes, 0, bytes.length, i);
        if (b2 != null && b2.length >= 4 && 35615 == ((b2[0] & 255) | (65280 & (b2[1] << 8)))) {
            byte[] bArr = new byte[2048];
            try {
                byteArrayOutputStream2 = new ByteArrayOutputStream();
                try {
                    byteArrayInputStream = new ByteArrayInputStream(b2);
                    try {
                        GZIPInputStream gZIPInputStream2 = new GZIPInputStream(byteArrayInputStream);
                        while (true) {
                            try {
                                int read = gZIPInputStream2.read(bArr);
                                if (read < 0) {
                                    break;
                                }
                                byteArrayOutputStream2.write(bArr, 0, read);
                            } catch (IOException e3) {
                                gZIPInputStream = gZIPInputStream2;
                                byteArrayOutputStream = byteArrayOutputStream2;
                            } catch (Throwable th) {
                                th = th;
                                gZIPInputStream = gZIPInputStream2;
                                try {
                                    byteArrayOutputStream2.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    gZIPInputStream.close();
                                } catch (Exception e5) {
                                }
                                try {
                                    byteArrayInputStream.close();
                                } catch (Exception e6) {
                                }
                                throw th;
                            }
                        }
                        b2 = byteArrayOutputStream2.toByteArray();
                        try {
                            byteArrayOutputStream2.close();
                        } catch (Exception e7) {
                        }
                        try {
                            gZIPInputStream2.close();
                        } catch (Exception e8) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (Exception e9) {
                        }
                    } catch (IOException e10) {
                        byteArrayOutputStream = byteArrayOutputStream2;
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e11) {
                        }
                        try {
                            gZIPInputStream.close();
                        } catch (Exception e12) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (Exception e13) {
                        }
                        return b2;
                    } catch (Throwable th2) {
                        th = th2;
                        byteArrayOutputStream2.close();
                        gZIPInputStream.close();
                        byteArrayInputStream.close();
                        throw th;
                    }
                } catch (IOException e14) {
                    byteArrayInputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return b2;
                } catch (Throwable th3) {
                    th = th3;
                    byteArrayInputStream = null;
                    byteArrayOutputStream2.close();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    throw th;
                }
            } catch (IOException e15) {
                byteArrayInputStream = null;
                byteArrayOutputStream = null;
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                return b2;
            } catch (Throwable th4) {
                th = th4;
                byteArrayInputStream = null;
                byteArrayOutputStream2 = null;
                byteArrayOutputStream2.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                throw th;
            }
        }
        return b2;
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        byte[] d2 = d(i3);
        if (bArr[i + 2] == 61) {
            bArr2[i2] = (byte) ((((d2[bArr[i + 1]] & 255) << 12) | ((d2[bArr[i]] & 255) << 18)) >>> 16);
            return 1;
        } else if (bArr[i + 3] == 61) {
            int i4 = ((d2[bArr[i + 2]] & 255) << 6) | ((d2[bArr[i]] & 255) << 18) | ((d2[bArr[i + 1]] & 255) << 12);
            bArr2[i2] = (byte) (i4 >>> 16);
            bArr2[i2 + 1] = (byte) (i4 >>> 8);
            return 2;
        } else {
            try {
                byte b2 = ((d2[bArr[i]] & 255) << 18) | ((d2[bArr[i + 1]] & 255) << 12) | ((d2[bArr[i + 2]] & 255) << 6) | (d2[bArr[i + 3]] & 255);
                bArr2[i2] = (byte) (b2 >> 16);
                bArr2[i2 + 1] = (byte) (b2 >> 8);
                bArr2[i2 + 2] = (byte) b2;
                return 3;
            } catch (Exception e2) {
                System.out.println("" + ((int) bArr[i]) + ": " + ((int) d2[bArr[i]]));
                System.out.println("" + ((int) bArr[i + 1]) + ": " + ((int) d2[bArr[i + 1]]));
                System.out.println("" + ((int) bArr[i + 2]) + ": " + ((int) d2[bArr[i + 2]]));
                System.out.println("" + ((int) bArr[i + 3]) + ": " + ((int) d2[bArr[i + 3]]));
                return -1;
            }
        }
    }

    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        int i4;
        byte[] d2 = d(i3);
        byte[] bArr2 = new byte[((i2 * 3) / 4)];
        byte[] bArr3 = new byte[4];
        int i5 = 0;
        int i6 = 0;
        int i7 = i;
        while (i7 < i + i2) {
            byte b2 = (byte) (bArr[i7] & Byte.MAX_VALUE);
            byte b3 = d2[b2];
            if (b3 >= -5) {
                if (b3 >= -1) {
                    i4 = i6 + 1;
                    bArr3[i6] = b2;
                    if (i4 > 3) {
                        i5 += b(bArr3, 0, bArr2, i5, i3);
                        if (b2 == 61) {
                            break;
                        }
                        i4 = 0;
                    } else {
                        continue;
                    }
                } else {
                    i4 = i6;
                }
                i7++;
                i6 = i4;
            } else {
                System.err.println("Bad Base64 input character at " + i7 + ": " + ((int) bArr[i7]) + "(decimal)");
                return null;
            }
        }
        byte[] bArr4 = new byte[i5];
        System.arraycopy(bArr2, 0, bArr4, 0, i5);
        return bArr4;
    }

    /* access modifiers changed from: private */
    public static byte[] b(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        int i5 = 0;
        byte[] c2 = c(i4);
        int i6 = (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0);
        if (i2 > 2) {
            i5 = (bArr[i + 2] << 24) >>> 24;
        }
        int i7 = i5 | i6;
        switch (i2) {
            case 1:
                bArr2[i3] = c2[i7 >>> 18];
                bArr2[i3 + 1] = c2[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = 61;
                bArr2[i3 + 3] = 61;
                break;
            case 2:
                bArr2[i3] = c2[i7 >>> 18];
                bArr2[i3 + 1] = c2[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = c2[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = 61;
                break;
            case 3:
                bArr2[i3] = c2[i7 >>> 18];
                bArr2[i3 + 1] = c2[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = c2[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = c2[i7 & 63];
                break;
        }
        return bArr2;
    }

    /* access modifiers changed from: private */
    public static byte[] b(byte[] bArr, byte[] bArr2, int i, int i2) {
        b(bArr2, 0, i, bArr, 0, i2);
        return bArr;
    }

    /* access modifiers changed from: private */
    public static final byte[] c(int i) {
        return (i & 16) == 16 ? c : (i & 32) == 32 ? e : a;
    }

    /* access modifiers changed from: private */
    public static final byte[] d(int i) {
        return (i & 16) == 16 ? d : (i & 32) == 32 ? f : b;
    }
}
