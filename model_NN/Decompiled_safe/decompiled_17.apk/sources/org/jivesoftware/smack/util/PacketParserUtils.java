package org.jivesoftware.smack.util;

import android.text.TextUtils;
import com.baixing.activity.BaseFragment;
import com.tencent.mm.sdk.message.RMsgInfoDB;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.c;
import com.xiaomi.push.service.n;
import com.xiaomi.push.service.p;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMBinder;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Authentication;
import org.jivesoftware.smack.packet.CommonPacketExtension;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.packet.RosterPacket;
import org.jivesoftware.smack.packet.StreamError;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.provider.ProviderManager;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class PacketParserUtils {
    private static XmlPullParser a = null;

    public static CommonPacketExtension a(String str, String str2, XmlPullParser xmlPullParser) {
        Object a2 = ProviderManager.a().a("all", "xm:chat");
        if (a2 == null || !(a2 instanceof c)) {
            return null;
        }
        return ((c) a2).b(xmlPullParser);
    }

    public static IQ a(XmlPullParser xmlPullParser, Connection connection) {
        String attributeValue = xmlPullParser.getAttributeValue("", LocaleUtil.INDONESIAN);
        String attributeValue2 = xmlPullParser.getAttributeValue("", "to");
        String attributeValue3 = xmlPullParser.getAttributeValue("", "from");
        String attributeValue4 = xmlPullParser.getAttributeValue("", "chid");
        IQ.Type a2 = IQ.Type.a(xmlPullParser.getAttributeValue("", "type"));
        boolean z = false;
        IQ iq = null;
        XMPPError xMPPError = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("error")) {
                    xMPPError = f(xmlPullParser);
                } else if (name.equals("query") && namespace.equals("jabber:iq:auth")) {
                    iq = h(xmlPullParser);
                } else if (name.equals("query") && namespace.equals("jabber:iq:roster")) {
                    iq = i(xmlPullParser);
                } else if (!name.equals("query") || !namespace.equals("jabber:iq:register")) {
                    iq = new IQ();
                    iq.a(a(name, namespace, xmlPullParser));
                } else {
                    iq = j(xmlPullParser);
                }
            } else if (next == 3 && xmlPullParser.getName().equals("iq")) {
                z = true;
            }
            boolean z2 = z;
            IQ iq2 = iq;
            boolean z3 = z2;
            IQ iq3 = iq2;
            z = z3;
            iq = iq3;
        }
        if (iq == null) {
            if (IQ.Type.a == a2 || IQ.Type.b == a2) {
                a aVar = new a();
                aVar.k(attributeValue);
                aVar.m(attributeValue3);
                aVar.n(attributeValue2);
                aVar.a(IQ.Type.d);
                aVar.l(attributeValue4);
                aVar.a(new XMPPError(XMPPError.Condition.e));
                connection.a(aVar);
                b.c("iq usage error. send packet in packet parser.");
                return null;
            }
            iq = new b();
        }
        iq.k(attributeValue);
        iq.m(attributeValue2);
        iq.l(attributeValue4);
        iq.n(attributeValue3);
        iq.a(a2);
        iq.a(xMPPError);
        return iq;
    }

    public static Packet a(XmlPullParser xmlPullParser) {
        String str;
        boolean z;
        String str2;
        Map<String, Object> map;
        boolean z2 = false;
        String str3 = null;
        if ("1".equals(xmlPullParser.getAttributeValue("", "s"))) {
            String attributeValue = xmlPullParser.getAttributeValue("", "chid");
            String attributeValue2 = xmlPullParser.getAttributeValue("", LocaleUtil.INDONESIAN);
            String attributeValue3 = xmlPullParser.getAttributeValue("", "from");
            String attributeValue4 = xmlPullParser.getAttributeValue("", "to");
            String attributeValue5 = xmlPullParser.getAttributeValue("", "type");
            n.b b = n.a().b(attributeValue, attributeValue4);
            n.b b2 = b == null ? n.a().b(attributeValue, attributeValue3) : b;
            if (b2 == null) {
                throw new XMPPException("the channel id is wrong while receiving a encrypted message");
            }
            Packet packet = null;
            while (!z2) {
                int next = xmlPullParser.next();
                if (next == 2) {
                    if (!"s".equals(xmlPullParser.getName())) {
                        throw new XMPPException("error while receiving a encrypted message with wrong format");
                    } else if (xmlPullParser.next() != 4) {
                        throw new XMPPException("error while receiving a encrypted message with wrong format");
                    } else {
                        String text = xmlPullParser.getText();
                        if ("5".equals(attributeValue)) {
                            Message message = new Message();
                            message.l(attributeValue);
                            message.b(true);
                            message.n(attributeValue3);
                            message.m(attributeValue4);
                            message.k(attributeValue2);
                            message.f(attributeValue5);
                            CommonPacketExtension commonPacketExtension = new CommonPacketExtension("s", null, null, null);
                            commonPacketExtension.b(text);
                            message.a(commonPacketExtension);
                            return message;
                        }
                        a(p.b(p.a(b2.i, attributeValue2), text));
                        a.next();
                        packet = a(a);
                    }
                } else if (next == 3 && xmlPullParser.getName().equals(RMsgInfoDB.TABLE)) {
                    z2 = true;
                }
            }
            if (packet != null) {
                return packet;
            }
            throw new XMPPException("error while receiving a encrypted message with wrong format");
        }
        Message message2 = new Message();
        String attributeValue6 = xmlPullParser.getAttributeValue("", LocaleUtil.INDONESIAN);
        if (attributeValue6 == null) {
            attributeValue6 = "ID_NOT_AVAILABLE";
        }
        message2.k(attributeValue6);
        message2.m(xmlPullParser.getAttributeValue("", "to"));
        message2.n(xmlPullParser.getAttributeValue("", "from"));
        message2.l(xmlPullParser.getAttributeValue("", "chid"));
        message2.a(xmlPullParser.getAttributeValue("", Constants.PARAM_APP_ID));
        try {
            str = xmlPullParser.getAttributeValue("", "transient");
        } catch (Exception e) {
            str = null;
        }
        try {
            String attributeValue7 = xmlPullParser.getAttributeValue("", "seq");
            if (!TextUtils.isEmpty(attributeValue7)) {
                message2.b(attributeValue7);
            }
        } catch (Exception e2) {
        }
        try {
            String attributeValue8 = xmlPullParser.getAttributeValue("", "mseq");
            if (!TextUtils.isEmpty(attributeValue8)) {
                message2.c(attributeValue8);
            }
        } catch (Exception e3) {
        }
        try {
            String attributeValue9 = xmlPullParser.getAttributeValue("", "fseq");
            if (!TextUtils.isEmpty(attributeValue9)) {
                message2.d(attributeValue9);
            }
        } catch (Exception e4) {
        }
        try {
            String attributeValue10 = xmlPullParser.getAttributeValue("", "status");
            if (!TextUtils.isEmpty(attributeValue10)) {
                message2.e(attributeValue10);
            }
        } catch (Exception e5) {
        }
        message2.a(!TextUtils.isEmpty(str) && str.equalsIgnoreCase("true"));
        message2.f(xmlPullParser.getAttributeValue("", "type"));
        String k = k(xmlPullParser);
        if (k == null || "".equals(k.trim())) {
            Packet.t();
        } else {
            message2.j(k);
        }
        boolean z3 = false;
        Map<String, Object> map2 = null;
        while (!z3) {
            int next2 = xmlPullParser.next();
            if (next2 == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (TextUtils.isEmpty(namespace)) {
                    namespace = "xm";
                }
                if (name.equals("subject")) {
                    if (k(xmlPullParser) == null) {
                    }
                    message2.g(g(xmlPullParser));
                    str2 = str3;
                    map = map2;
                } else if (name.equals("body")) {
                    if (k(xmlPullParser) == null) {
                    }
                    message2.h(g(xmlPullParser));
                    str2 = str3;
                    map = map2;
                } else {
                    if (name.equals("thread")) {
                        if (str3 == null) {
                            str2 = xmlPullParser.nextText();
                            map = map2;
                        }
                    } else if (name.equals("error")) {
                        message2.a(f(xmlPullParser));
                        str2 = str3;
                        map = map2;
                    } else if (!name.equals("properties") || !namespace.equals("http://www.jivesoftware.com/xmlns/xmpp/properties")) {
                        message2.a(a(name, namespace, xmlPullParser));
                    } else {
                        String str4 = str3;
                        map = d(xmlPullParser);
                        str2 = str4;
                    }
                    str2 = str3;
                    map = map2;
                }
                map2 = map;
                str3 = str2;
                z = z3;
            } else {
                z = (next2 != 3 || !xmlPullParser.getName().equals(RMsgInfoDB.TABLE)) ? z3 : true;
            }
            z3 = z;
        }
        message2.i(str3);
        if (map2 != null) {
            for (String str5 : map2.keySet()) {
                message2.a(str5, map2.get(str5));
            }
        }
        return message2;
    }

    private static void a(byte[] bArr) {
        if (a == null) {
            try {
                a = XmlPullParserFactory.newInstance().newPullParser();
                a.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
        }
        a.setInput(new InputStreamReader(new ByteArrayInputStream(bArr)));
    }

    public static Presence b(XmlPullParser xmlPullParser) {
        boolean z;
        Presence.Type type = Presence.Type.available;
        String attributeValue = xmlPullParser.getAttributeValue("", "type");
        if (attributeValue != null && !attributeValue.equals("")) {
            try {
                type = Presence.Type.valueOf(attributeValue);
            } catch (IllegalArgumentException e) {
                System.err.println("Found invalid presence type " + attributeValue);
            }
        }
        Presence presence = new Presence(type);
        presence.m(xmlPullParser.getAttributeValue("", "to"));
        presence.n(xmlPullParser.getAttributeValue("", "from"));
        presence.l(xmlPullParser.getAttributeValue("", "chid"));
        String attributeValue2 = xmlPullParser.getAttributeValue("", LocaleUtil.INDONESIAN);
        if (attributeValue2 == null) {
            attributeValue2 = "ID_NOT_AVAILABLE";
        }
        presence.k(attributeValue2);
        boolean z2 = false;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("status")) {
                    presence.a(xmlPullParser.nextText());
                } else if (name.equals("priority")) {
                    try {
                        presence.a(Integer.parseInt(xmlPullParser.nextText()));
                    } catch (NumberFormatException e2) {
                    } catch (IllegalArgumentException e3) {
                        presence.a(0);
                    }
                } else if (name.equals("show")) {
                    String nextText = xmlPullParser.nextText();
                    try {
                        presence.a(Presence.Mode.valueOf(nextText));
                    } catch (IllegalArgumentException e4) {
                        System.err.println("Found invalid presence mode " + nextText);
                    }
                } else if (name.equals("error")) {
                    presence.a(f(xmlPullParser));
                } else if (!name.equals("properties") || !namespace.equals("http://www.jivesoftware.com/xmlns/xmpp/properties")) {
                    presence.a(a(name, namespace, xmlPullParser));
                } else {
                    Map<String, Object> d = d(xmlPullParser);
                    for (String next2 : d.keySet()) {
                        presence.a(next2, d.get(next2));
                    }
                }
                z = z2;
            } else {
                z = (next != 3 || !xmlPullParser.getName().equals("presence")) ? z2 : true;
            }
            z2 = z;
        }
        return presence;
    }

    public static XMBinder.BindResult c(XmlPullParser xmlPullParser) {
        XMBinder.BindResult bindResult = new XMBinder.BindResult();
        String attributeValue = xmlPullParser.getAttributeValue("", LocaleUtil.INDONESIAN);
        String attributeValue2 = xmlPullParser.getAttributeValue("", "to");
        String attributeValue3 = xmlPullParser.getAttributeValue("", "from");
        String attributeValue4 = xmlPullParser.getAttributeValue("", "chid");
        XMBinder.BindResult.Type a2 = XMBinder.BindResult.Type.a(xmlPullParser.getAttributeValue("", "type"));
        bindResult.k(attributeValue);
        bindResult.m(attributeValue2);
        bindResult.n(attributeValue3);
        bindResult.l(attributeValue4);
        bindResult.a(a2);
        boolean z = false;
        XMPPError xMPPError = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("error")) {
                    xMPPError = f(xmlPullParser);
                }
            } else if (next == 3 && xmlPullParser.getName().equals("bind")) {
                z = true;
            }
        }
        bindResult.a(xMPPError);
        return bindResult;
    }

    public static Map<String, Object> d(XmlPullParser xmlPullParser) {
        Object obj;
        HashMap hashMap = new HashMap();
        while (true) {
            int next = xmlPullParser.next();
            if (next == 2 && xmlPullParser.getName().equals("property")) {
                boolean z = false;
                String str = null;
                String str2 = null;
                String str3 = null;
                Object obj2 = null;
                while (!z) {
                    int next2 = xmlPullParser.next();
                    if (next2 == 2) {
                        String name = xmlPullParser.getName();
                        if (name.equals(BaseFragment.ARG_COMMON_TITLE)) {
                            str2 = xmlPullParser.nextText();
                        } else if (name.equals("value")) {
                            str3 = xmlPullParser.getAttributeValue("", "type");
                            str = xmlPullParser.nextText();
                        }
                    } else if (next2 == 3 && xmlPullParser.getName().equals("property")) {
                        if ("integer".equals(str3)) {
                            obj = Integer.valueOf(str);
                        } else if ("long".equals(str3)) {
                            obj = Long.valueOf(str);
                        } else if ("float".equals(str3)) {
                            obj = Float.valueOf(str);
                        } else if ("double".equals(str3)) {
                            obj = Double.valueOf(str);
                        } else if ("boolean".equals(str3)) {
                            obj = Boolean.valueOf(str);
                        } else if ("string".equals(str3)) {
                            obj = str;
                        } else {
                            if ("java-object".equals(str3)) {
                                try {
                                    obj = new ObjectInputStream(new ByteArrayInputStream(StringUtils.c(str))).readObject();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            obj = obj2;
                        }
                        if (!(str2 == null || obj == null)) {
                            hashMap.put(str2, obj);
                        }
                        obj2 = obj;
                        z = true;
                    }
                }
            } else if (next == 3 && xmlPullParser.getName().equals("properties")) {
                return hashMap;
            }
        }
    }

    public static StreamError e(XmlPullParser xmlPullParser) {
        StreamError streamError = null;
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                streamError = new StreamError(xmlPullParser.getName());
            } else if (next == 3 && xmlPullParser.getName().equals("error")) {
                z = true;
            }
        }
        return streamError;
    }

    public static XMPPError f(XmlPullParser xmlPullParser) {
        XMPPError.Type type;
        boolean z = false;
        String str = null;
        ArrayList arrayList = new ArrayList();
        String str2 = "-1";
        String str3 = null;
        String str4 = null;
        int i = 0;
        while (i < xmlPullParser.getAttributeCount()) {
            String attributeValue = xmlPullParser.getAttributeName(i).equals("code") ? xmlPullParser.getAttributeValue("", "code") : str2;
            String attributeValue2 = xmlPullParser.getAttributeName(i).equals("type") ? xmlPullParser.getAttributeValue("", "type") : str3;
            if (xmlPullParser.getAttributeName(i).equals("reason")) {
                str4 = xmlPullParser.getAttributeValue("", "reason");
            }
            i++;
            str2 = attributeValue;
            str3 = attributeValue2;
        }
        String str5 = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("text")) {
                    str5 = xmlPullParser.nextText();
                } else {
                    String name = xmlPullParser.getName();
                    String namespace = xmlPullParser.getNamespace();
                    if ("urn:ietf:params:xml:ns:xmpp-stanzas".equals(namespace)) {
                        str = name;
                    } else {
                        arrayList.add(a(name, namespace, xmlPullParser));
                    }
                }
            } else if (next == 3) {
                if (xmlPullParser.getName().equals("error")) {
                    z = true;
                }
            } else if (next == 4) {
                str5 = xmlPullParser.getText();
            }
        }
        XMPPError.Type type2 = XMPPError.Type.CANCEL;
        if (str3 != null) {
            try {
                type2 = XMPPError.Type.valueOf(str3.toUpperCase());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                type = type2;
            }
        }
        type = type2;
        return new XMPPError(Integer.parseInt(str2), type, str4, str, str5, arrayList);
    }

    private static String g(XmlPullParser xmlPullParser) {
        String str = "";
        int depth = xmlPullParser.getDepth();
        while (true) {
            if (xmlPullParser.next() == 3 && xmlPullParser.getDepth() == depth) {
                return str;
            }
            str = str + xmlPullParser.getText();
        }
    }

    private static Authentication h(XmlPullParser xmlPullParser) {
        Authentication authentication = new Authentication();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("username")) {
                    authentication.a(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("password")) {
                    authentication.b(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("digest")) {
                    authentication.c(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("resource")) {
                    authentication.d(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("query")) {
                z = true;
            }
        }
        return authentication;
    }

    private static RosterPacket i(XmlPullParser xmlPullParser) {
        RosterPacket.Item item;
        boolean z;
        RosterPacket rosterPacket = new RosterPacket();
        boolean z2 = false;
        RosterPacket.Item item2 = null;
        while (!z2) {
            if (xmlPullParser.getEventType() == 2 && xmlPullParser.getName().equals("query")) {
                rosterPacket.a(xmlPullParser.getAttributeValue(null, "ver"));
            }
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("item")) {
                    item2 = new RosterPacket.Item(xmlPullParser.getAttributeValue("", "jid"), xmlPullParser.getAttributeValue("", BaseFragment.ARG_COMMON_TITLE));
                    item2.a(RosterPacket.ItemStatus.a(xmlPullParser.getAttributeValue("", "ask")));
                    String attributeValue = xmlPullParser.getAttributeValue("", "subscription");
                    if (attributeValue == null) {
                        attributeValue = "none";
                    }
                    item2.a(RosterPacket.ItemType.valueOf(attributeValue));
                }
                if (xmlPullParser.getName().equals("group") && item2 != null) {
                    String nextText = xmlPullParser.nextText();
                    if (nextText != null && nextText.trim().length() > 0) {
                        item2.a(nextText);
                    }
                    item = item2;
                    z = z2;
                }
                item = item2;
                z = z2;
            } else {
                if (next == 3) {
                    if (xmlPullParser.getName().equals("item")) {
                        rosterPacket.a(item2);
                    }
                    if (xmlPullParser.getName().equals("query")) {
                        item = item2;
                        z = true;
                    }
                }
                item = item2;
                z = z2;
            }
            z2 = z;
            item2 = item;
        }
        return rosterPacket;
    }

    private static Registration j(XmlPullParser xmlPullParser) {
        Registration registration = new Registration();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getNamespace().equals("jabber:iq:register")) {
                    String name = xmlPullParser.getName();
                    if (xmlPullParser.next() == 4) {
                        String text = xmlPullParser.getText();
                        if (name.equals("instructions")) {
                            registration.a(text);
                        } else {
                            registration.b(name, text);
                        }
                    } else if (name.equals("registered")) {
                        registration.a(true);
                    } else {
                        registration.b(name);
                    }
                } else {
                    registration.a(a(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals("query")) {
                z = true;
            }
        }
        return registration;
    }

    private static String k(XmlPullParser xmlPullParser) {
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            String attributeName = xmlPullParser.getAttributeName(i);
            if ("xml:lang".equals(attributeName) || ("lang".equals(attributeName) && "xml".equals(xmlPullParser.getAttributePrefix(i)))) {
                return xmlPullParser.getAttributeValue(i);
            }
        }
        return null;
    }
}
