package org.jivesoftware.smack.util;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class ObservableWriter extends Writer {
    Writer a = null;
    List b = new ArrayList();

    public ObservableWriter(Writer writer) {
        this.a = writer;
    }

    private void a(String str) {
        WriterListener[] writerListenerArr;
        synchronized (this.b) {
            writerListenerArr = new WriterListener[this.b.size()];
            this.b.toArray(writerListenerArr);
        }
        for (WriterListener a2 : writerListenerArr) {
            a2.a(str);
        }
    }

    public void a(WriterListener writerListener) {
        if (writerListener != null) {
            synchronized (this.b) {
                if (!this.b.contains(writerListener)) {
                    this.b.add(writerListener);
                }
            }
        }
    }

    public void b(WriterListener writerListener) {
        synchronized (this.b) {
            this.b.remove(writerListener);
        }
    }

    public void close() {
        this.a.close();
    }

    public void flush() {
        this.a.flush();
    }

    public void write(int i) {
        this.a.write(i);
    }

    public void write(String str) {
        this.a.write(str);
        a(str);
    }

    public void write(String str, int i, int i2) {
        this.a.write(str, i, i2);
        a(str.substring(i, i + i2));
    }

    public void write(char[] cArr) {
        this.a.write(cArr);
        a(new String(cArr));
    }

    public void write(char[] cArr, int i, int i2) {
        this.a.write(cArr, i, i2);
        a(new String(cArr, i, i2));
    }
}
