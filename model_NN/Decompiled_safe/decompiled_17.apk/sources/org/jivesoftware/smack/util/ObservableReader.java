package org.jivesoftware.smack.util;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class ObservableReader extends Reader {
    Reader a = null;
    List b = new ArrayList();

    public ObservableReader(Reader reader) {
        this.a = reader;
    }

    public void a(ReaderListener readerListener) {
        if (readerListener != null) {
            synchronized (this.b) {
                if (!this.b.contains(readerListener)) {
                    this.b.add(readerListener);
                }
            }
        }
    }

    public void b(ReaderListener readerListener) {
        synchronized (this.b) {
            this.b.remove(readerListener);
        }
    }

    public void close() {
        this.a.close();
    }

    public void mark(int i) {
        this.a.mark(i);
    }

    public boolean markSupported() {
        return this.a.markSupported();
    }

    public int read() {
        return this.a.read();
    }

    public int read(char[] cArr) {
        return this.a.read(cArr);
    }

    public int read(char[] cArr, int i, int i2) {
        ReaderListener[] readerListenerArr;
        int read = this.a.read(cArr, i, i2);
        if (read > 0) {
            String str = new String(cArr, i, read);
            synchronized (this.b) {
                readerListenerArr = new ReaderListener[this.b.size()];
                this.b.toArray(readerListenerArr);
            }
            for (ReaderListener a2 : readerListenerArr) {
                a2.a(str);
            }
        }
        return read;
    }

    public boolean ready() {
        return this.a.ready();
    }

    public void reset() {
        this.a.reset();
    }

    public long skip(long j) {
        return this.a.skip(j);
    }
}
