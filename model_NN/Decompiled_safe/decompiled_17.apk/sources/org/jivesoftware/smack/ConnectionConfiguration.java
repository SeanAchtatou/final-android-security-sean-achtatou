package org.jivesoftware.smack;

import com.xiaomi.channel.commonutils.a.a;
import com.xiaomi.network.Fallback;
import com.xiaomi.network.HostManager;
import java.io.File;
import java.util.Map;
import javax.net.SocketFactory;
import org.jivesoftware.smack.proxy.ProxyInfo;

public class ConnectionConfiguration implements Cloneable {
    private HttpRequestProxy A;
    protected ProxyInfo a;
    private String b;
    private String c;
    private int d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private boolean k = false;
    private boolean l = false;
    private boolean m = false;
    private boolean n = false;
    private boolean o = false;
    private boolean p = false;
    private String q = null;
    private boolean r = false;
    private boolean s = true;
    private boolean t = Connection.c;
    private boolean u = true;
    private SocketFactory v;
    private String w;
    private boolean x = true;
    private boolean y = true;
    private SecurityMode z = SecurityMode.enabled;

    public enum SecurityMode {
        required,
        enabled,
        disabled
    }

    public ConnectionConfiguration(Map<String, Integer> map, int i2, String str, HttpRequestProxy httpRequestProxy) {
        a(map, i2, str, ProxyInfo.a(), httpRequestProxy);
    }

    private void a(Map<String, Integer> map, int i2, String str, ProxyInfo proxyInfo, HttpRequestProxy httpRequestProxy) {
        this.c = g();
        this.d = i2;
        this.b = str;
        this.a = proxyInfo;
        this.A = httpRequestProxy;
        String property = System.getProperty("java.home");
        StringBuilder sb = new StringBuilder();
        sb.append(property).append(File.separator).append("lib");
        sb.append(File.separator).append("security");
        sb.append(File.separator).append("cacerts");
        this.e = sb.toString();
        this.f = "jks";
        this.g = "changeit";
        this.h = System.getProperty("javax.net.ssl.keyStore");
        this.i = "jks";
        this.j = "pkcs11.config";
        this.v = proxyInfo.g();
    }

    public static final String g() {
        return a.e ? "10.237.12.2" : a.a() ? "sandbox.xmpush.xiaomi.com" : a.b ? "58.68.235.106" : "app.chat.xiaomi.net";
    }

    public void a(String str) {
        this.b = str;
    }

    public void a(String str, long j2, Exception exc) {
        Fallback fallbacksByHost = HostManager.getInstance().getFallbacksByHost(k());
        if (fallbacksByHost != null) {
            fallbacksByHost.a(str, -2, j2, 0, exc);
            HostManager.getInstance().persist();
        }
    }

    public void a(boolean z2) {
        this.s = z2;
    }

    public void b(String str) {
        this.w = str;
    }

    public void b(boolean z2) {
        this.t = z2;
    }

    public void c(boolean z2) {
        this.y = z2;
    }

    public String h() {
        return this.b;
    }

    public String i() {
        return this.w;
    }

    public int j() {
        return this.d;
    }

    public String k() {
        return this.c;
    }

    public boolean l() {
        return this.t;
    }
}
