package org.jivesoftware.smack;

import com.kenai.jbosh.BOSHClientResponseListener;

public class BOSHPacketReader implements BOSHClientResponseListener {
    private BOSHConnection a;

    public BOSHPacketReader(BOSHConnection bOSHConnection) {
        this.a = bOSHConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:62:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A[Catch:{ Exception -> 0x015a }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.kenai.jbosh.BOSHMessageEvent r8) {
        /*
            r7 = this;
            r2 = 0
            r1 = 1
            com.kenai.jbosh.AbstractBody r0 = r8.a()
            if (r0 == 0) goto L_0x008d
            org.jivesoftware.smack.BOSHConnection r3 = r7.a     // Catch:{ Exception -> 0x0157 }
            boolean r3 = r3.j()     // Catch:{ Exception -> 0x0157 }
            if (r3 == 0) goto L_0x0028
            java.lang.String r3 = "xm"
            java.lang.String r4 = "challenge"
            com.kenai.jbosh.BodyQName r3 = com.kenai.jbosh.BodyQName.a(r3, r4)     // Catch:{ Exception -> 0x0157 }
            java.lang.String r3 = r0.a(r3)     // Catch:{ Exception -> 0x0157 }
            boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x0157 }
            if (r4 != 0) goto L_0x0028
            org.jivesoftware.smack.BOSHConnection r2 = r7.a     // Catch:{ Exception -> 0x0103 }
            r2.a(r3)     // Catch:{ Exception -> 0x0103 }
            r2 = r1
        L_0x0028:
            org.jivesoftware.smack.BOSHConnection r3 = r7.a     // Catch:{ Exception -> 0x015a }
            java.lang.String r3 = r3.b     // Catch:{ Exception -> 0x015a }
            if (r3 != 0) goto L_0x003f
            org.jivesoftware.smack.BOSHConnection r2 = r7.a     // Catch:{ Exception -> 0x0103 }
            java.lang.String r3 = "xm"
            java.lang.String r4 = "sid"
            com.kenai.jbosh.BodyQName r3 = com.kenai.jbosh.BodyQName.a(r3, r4)     // Catch:{ Exception -> 0x0103 }
            java.lang.String r3 = r0.a(r3)     // Catch:{ Exception -> 0x0103 }
            r2.b = r3     // Catch:{ Exception -> 0x0103 }
            r2 = r1
        L_0x003f:
            org.jivesoftware.smack.BOSHConnection r3 = r7.a     // Catch:{ Exception -> 0x015a }
            java.lang.String r3 = r3.a     // Catch:{ Exception -> 0x015a }
            if (r3 != 0) goto L_0x0056
            org.jivesoftware.smack.BOSHConnection r2 = r7.a     // Catch:{ Exception -> 0x0103 }
            java.lang.String r3 = "xm"
            java.lang.String r4 = "authid"
            com.kenai.jbosh.BodyQName r3 = com.kenai.jbosh.BodyQName.a(r3, r4)     // Catch:{ Exception -> 0x0103 }
            java.lang.String r3 = r0.a(r3)     // Catch:{ Exception -> 0x0103 }
            r2.a = r3     // Catch:{ Exception -> 0x0103 }
            r2 = r1
        L_0x0056:
            org.xmlpull.v1.XmlPullParserFactory r3 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ Exception -> 0x015a }
            org.xmlpull.v1.XmlPullParser r3 = r3.newPullParser()     // Catch:{ Exception -> 0x015a }
            java.lang.String r4 = "http://xmlpull.org/v1/doc/features.html#process-namespaces"
            r5 = 1
            r3.setFeature(r4, r5)     // Catch:{ Exception -> 0x015a }
            java.io.StringReader r4 = new java.io.StringReader     // Catch:{ Exception -> 0x015a }
            java.lang.String r0 = r0.b()     // Catch:{ Exception -> 0x015a }
            r4.<init>(r0)     // Catch:{ Exception -> 0x015a }
            r3.setInput(r4)     // Catch:{ Exception -> 0x015a }
            r3.getEventType()     // Catch:{ Exception -> 0x015a }
        L_0x0073:
            int r0 = r3.next()     // Catch:{ Exception -> 0x015a }
            org.jivesoftware.smack.BOSHConnection r4 = r7.a     // Catch:{ Exception -> 0x015a }
            r4.q()     // Catch:{ Exception -> 0x015a }
            r4 = 2
            if (r0 != r4) goto L_0x008b
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "body"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x008f
        L_0x008b:
            if (r0 != r1) goto L_0x0073
        L_0x008d:
            r1 = r2
        L_0x008e:
            return r1
        L_0x008f:
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "message"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x00a6
            org.jivesoftware.smack.BOSHConnection r2 = r7.a     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.packet.Packet r4 = org.jivesoftware.smack.util.PacketParserUtils.a(r3)     // Catch:{ Exception -> 0x0103 }
            r2.b(r4)     // Catch:{ Exception -> 0x0103 }
            r2 = r1
            goto L_0x008b
        L_0x00a6:
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "iq"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x00bf
            org.jivesoftware.smack.BOSHConnection r2 = r7.a     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.BOSHConnection r4 = r7.a     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.packet.IQ r4 = org.jivesoftware.smack.util.PacketParserUtils.a(r3, r4)     // Catch:{ Exception -> 0x0103 }
            r2.b(r4)     // Catch:{ Exception -> 0x0103 }
            r2 = r1
            goto L_0x008b
        L_0x00bf:
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "presence"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x00d6
            org.jivesoftware.smack.BOSHConnection r2 = r7.a     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.packet.Presence r4 = org.jivesoftware.smack.util.PacketParserUtils.b(r3)     // Catch:{ Exception -> 0x0103 }
            r2.b(r4)     // Catch:{ Exception -> 0x0103 }
            r2 = r1
            goto L_0x008b
        L_0x00d6:
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "challenge"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x00ed
            java.lang.String r2 = r3.nextText()     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.BOSHConnection r4 = r7.a     // Catch:{ Exception -> 0x0103 }
            r4.a(r2)     // Catch:{ Exception -> 0x0103 }
            r2 = r1
            goto L_0x008b
        L_0x00ed:
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "error"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x0113
            org.jivesoftware.smack.XMPPException r0 = new org.jivesoftware.smack.XMPPException     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.packet.StreamError r2 = org.jivesoftware.smack.util.PacketParserUtils.e(r3)     // Catch:{ Exception -> 0x0103 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0103 }
            throw r0     // Catch:{ Exception -> 0x0103 }
        L_0x0103:
            r0 = move-exception
        L_0x0104:
            org.jivesoftware.smack.BOSHConnection r2 = r7.a
            boolean r2 = r2.k()
            if (r2 == 0) goto L_0x008e
            org.jivesoftware.smack.BOSHConnection r2 = r7.a
            r2.a(r0)
            goto L_0x008e
        L_0x0113:
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "warning"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x013f
            r3.next()     // Catch:{ Exception -> 0x0103 }
            java.lang.String r2 = r3.getName()     // Catch:{ Exception -> 0x0103 }
            java.lang.String r4 = "multi-login"
            boolean r2 = r2.equals(r4)     // Catch:{ Exception -> 0x0103 }
            if (r2 == 0) goto L_0x0154
            org.jivesoftware.smack.BOSHConnection r2 = r7.a     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.packet.Presence r4 = new org.jivesoftware.smack.packet.Presence     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.packet.Presence$Type r5 = org.jivesoftware.smack.packet.Presence.Type.unavailable     // Catch:{ Exception -> 0x0103 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0103 }
            r5 = 6
            r6 = 0
            r2.a(r4, r5, r6)     // Catch:{ Exception -> 0x0103 }
            r2 = r1
            goto L_0x008b
        L_0x013f:
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x015a }
            java.lang.String r5 = "bind"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x015a }
            if (r4 == 0) goto L_0x008b
            org.jivesoftware.smack.XMBinder$BindResult r2 = org.jivesoftware.smack.util.PacketParserUtils.c(r3)     // Catch:{ Exception -> 0x0103 }
            org.jivesoftware.smack.BOSHConnection r4 = r7.a     // Catch:{ Exception -> 0x0103 }
            r4.b(r2)     // Catch:{ Exception -> 0x0103 }
        L_0x0154:
            r2 = r1
            goto L_0x008b
        L_0x0157:
            r0 = move-exception
            r1 = r2
            goto L_0x0104
        L_0x015a:
            r0 = move-exception
            r1 = r2
            goto L_0x0104
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.BOSHPacketReader.a(com.kenai.jbosh.BOSHMessageEvent):boolean");
    }
}
