package org.jivesoftware.smack;

import android.text.TextUtils;
import com.xiaomi.network.Fallback;
import java.net.URI;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.jivesoftware.smack.proxy.ProxyInfo;

public class BOSHConfiguration extends ConnectionConfiguration {
    private boolean b;
    private String c;
    private Fallback d = null;
    private String e = "mibind.chat.gslb.mi-idc.com";

    public BOSHConfiguration(boolean z, Fallback fallback, int i, String str, String str2, HttpRequestProxy httpRequestProxy) {
        super(null, i, str2, httpRequestProxy);
        a(false);
        this.d = fallback;
        this.b = z;
        this.c = str == null ? CookieSpec.PATH_DELIM : str;
    }

    public void a(Fallback fallback) {
        if (fallback != null) {
            this.d = fallback;
            this.e = "mibind.chat.gslb.mi-idc.com";
            if (!this.d.c().isEmpty()) {
                String str = this.d.c().get(0);
                if (!TextUtils.isEmpty(str)) {
                    this.e = str;
                }
            }
        }
    }

    public boolean a() {
        return (this.a == null || this.a.b() == ProxyInfo.ProxyType.NONE) ? false : true;
    }

    public String b() {
        if (this.a != null) {
            return this.a.c();
        }
        return null;
    }

    public int c() {
        if (this.a != null) {
            return this.a.d();
        }
        return 8080;
    }

    public Fallback d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public URI f() {
        if (this.c.charAt(0) != '/') {
            this.c = '/' + this.c;
        }
        return new URI((this.b ? "https://" : "http://") + this.e + ":" + j() + this.c);
    }
}
