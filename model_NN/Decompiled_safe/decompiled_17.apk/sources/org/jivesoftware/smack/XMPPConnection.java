package org.jivesoftware.smack;

import android.os.SystemClock;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.network.Fallback;
import com.xiaomi.network.HostManager;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.n;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

public class XMPPConnection extends Connection {
    private final String A = "<pf><p>t:%1$d</p></pf>";
    public Exception a = null;
    protected Socket b;
    String r = null;
    h s;
    f t;
    private String u = null;
    private String v = "";
    private String w;
    /* access modifiers changed from: private */
    public XMPushService x;
    private volatile long y = 0;
    private volatile long z = 0;

    public XMPPConnection(XMPushService xMPushService, ConnectionConfiguration connectionConfiguration) {
        super(xMPushService, connectionConfiguration);
        this.x = xMPushService;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0111  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r13, int r14) {
        /*
            r12 = this;
            r2 = 0
            r0 = 0
            r12.a = r0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.xiaomi.network.Fallback r0 = r12.c(r13)
            if (r0 == 0) goto L_0x0013
            java.util.ArrayList r1 = r0.c()
        L_0x0013:
            boolean r3 = r1.isEmpty()
            if (r3 == 0) goto L_0x001c
            r1.add(r13)
        L_0x001c:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.util.Iterator r9 = r1.iterator()
        L_0x0025:
            boolean r1 = r9.hasNext()
            if (r1 == 0) goto L_0x00a7
            java.lang.Object r1 = r9.next()
            java.lang.String r1 = (java.lang.String) r1
            long r10 = java.lang.System.currentTimeMillis()
            int r3 = r12.d
            int r3 = r3 + 1
            r12.d = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r3.<init>()     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.lang.String r4 = "begin to connect to "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            com.xiaomi.channel.commonutils.logger.b.a(r3)     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.net.Socket r3 = r12.u()     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r12.b = r3     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.net.Socket r3 = r12.b     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r4 = 0
            r3.bind(r4)     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.net.InetSocketAddress r3 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r3.<init>(r1, r14)     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.net.Socket r4 = r12.b     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r5 = 10000(0x2710, float:1.4013E-41)
            r4.connect(r3, r5)     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            java.net.Socket r3 = r12.b     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r4 = 1
            r3.setTcpNoDelay(r4)     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r12.w = r1     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            r12.x()     // Catch:{ IOException -> 0x00ba, XMPPException -> 0x010c }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            long r2 = r2 - r10
            r12.e = r2     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            if (r0 == 0) goto L_0x0084
            long r2 = r12.e     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            r4 = -1
            r0.a(r1, r2, r4)     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
        L_0x0084:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            r2.<init>()     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            java.lang.String r3 = "connected to "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            java.lang.String r3 = " in "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            long r3 = r12.e     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            com.xiaomi.channel.commonutils.logger.b.a(r2)     // Catch:{ IOException -> 0x0160, XMPPException -> 0x015c }
            r2 = 1
        L_0x00a7:
            com.xiaomi.network.HostManager r0 = com.xiaomi.network.HostManager.getInstance()
            r0.persist()
            if (r2 != 0) goto L_0x0165
            org.jivesoftware.smack.XMPPException r0 = new org.jivesoftware.smack.XMPPException
            java.lang.String r1 = r8.toString()
            r0.<init>(r1)
            throw r0
        L_0x00ba:
            r3 = move-exception
            r7 = r2
            r6 = r3
        L_0x00bd:
            if (r0 == 0) goto L_0x00c9
            long r2 = java.lang.System.currentTimeMillis()
            long r2 = r2 - r10
            r4 = 0
            r0.a(r1, r2, r4, r6)
        L_0x00c9:
            r12.a = r6
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SMACK: Could not connect to:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            com.xiaomi.channel.commonutils.logger.b.c(r2)
            java.lang.String r2 = "SMACK: Could not connect to "
            java.lang.StringBuilder r2 = r8.append(r2)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = " port:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r2 = " "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r6.getMessage()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\n"
            r1.append(r2)
            r1 = r7
        L_0x0109:
            r2 = r1
            goto L_0x0025
        L_0x010c:
            r3 = move-exception
            r7 = r2
            r6 = r3
        L_0x010f:
            if (r0 == 0) goto L_0x011b
            long r2 = java.lang.System.currentTimeMillis()
            long r2 = r2 - r10
            r4 = 0
            r0.a(r1, r2, r4, r6)
        L_0x011b:
            r12.a = r6
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SMACK: Could not connect to:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            com.xiaomi.channel.commonutils.logger.b.c(r2)
            java.lang.String r2 = "SMACK: Could not connect to "
            java.lang.StringBuilder r2 = r8.append(r2)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = " port:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r2 = " "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r6.getMessage()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\n"
            r1.append(r2)
            r1 = r7
            goto L_0x0109
        L_0x015c:
            r2 = move-exception
            r7 = 1
            r6 = r2
            goto L_0x010f
        L_0x0160:
            r2 = move-exception
            r7 = 1
            r6 = r2
            goto L_0x00bd
        L_0x0165:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.XMPPConnection.a(java.lang.String, int):void");
    }

    private void a(ConnectionConfiguration connectionConfiguration) {
        a(connectionConfiguration.k(), connectionConfiguration.j());
    }

    private void x() {
        synchronized (this) {
            y();
            this.s = new h(this);
            this.t = new f(this);
            if (this.p.l()) {
                a(this.k.c(), (PacketFilter) null);
                if (this.k.d() != null) {
                    b(this.k.d(), null);
                }
            }
            this.s.c();
            this.t.b();
        }
    }

    private void y() {
        this.l = new BufferedReader(new InputStreamReader(this.b.getInputStream(), "UTF-8"), 4096);
        this.m = new BufferedWriter(new OutputStreamWriter(this.b.getOutputStream(), "UTF-8"));
        if (this.l != null && this.m != null) {
            b();
        }
    }

    public String a() {
        return this.n;
    }

    public void a(int i, Exception exc) {
        this.x.a(new i(this, 2, i, exc));
    }

    public void a(n.b bVar) {
        synchronized (this) {
            new XMBinder().a(bVar, a(), this);
        }
    }

    public void a(String str, String str2) {
        synchronized (this) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.l(str);
            presence.n(str2);
            if (this.s != null) {
                this.s.a(presence);
            }
        }
    }

    public void a(Packet packet) {
        if (this.s != null) {
            this.s.a(packet);
            return;
        }
        throw new XMPPException("the writer is null.");
    }

    public void a(Presence presence, int i, Exception exc) {
        b(presence, i, exc);
    }

    public void a(Packet[] packetArr) {
        for (Packet a2 : packetArr) {
            a(a2);
        }
    }

    public void b(String str) {
        this.v = str;
    }

    /* access modifiers changed from: protected */
    public void b(Presence presence, int i, Exception exc) {
        synchronized (this) {
            if (p() != 2) {
                a(2, i, exc);
                this.n = "";
                if (this.t != null) {
                    this.t.c();
                    this.t.d();
                    this.t = null;
                }
                if (this.s != null) {
                    try {
                        this.s.b();
                    } catch (IOException e) {
                        b.a(e);
                    }
                    this.s.a();
                    this.s = null;
                }
                try {
                    this.b.close();
                } catch (Exception e2) {
                }
                if (this.l != null) {
                    try {
                        this.l.close();
                    } catch (Throwable th) {
                    }
                    this.l = null;
                }
                if (this.m != null) {
                    try {
                        this.m.close();
                    } catch (Throwable th2) {
                    }
                    this.m = null;
                }
                this.y = 0;
                this.z = 0;
            }
        }
        return;
    }

    public Fallback c(String str) {
        return HostManager.getInstance().getFallbacksByHost(str);
    }

    public void c() {
        if (this.s != null) {
            this.s.d();
            return;
        }
        throw new XMPPException("the packetwriter is null.");
    }

    public String e() {
        return this.w;
    }

    public void s() {
        synchronized (this) {
            try {
                if (k() || j()) {
                    b.a("WARNING: current xmpp has connected");
                } else {
                    a(0, 0, (Exception) null);
                    a(this.p);
                }
            } catch (IOException e) {
                throw new XMPPException(e);
            }
        }
    }

    public String t() {
        String str;
        if (this.z == 0 || this.y == 0) {
            str = "";
        } else {
            str = String.format("<pf><p>t:%1$d</p></pf>", Long.valueOf(this.z - this.y));
        }
        return String.format(this.v, str);
    }

    public Socket u() {
        return new Socket();
    }

    public void v() {
        this.y = SystemClock.uptimeMillis();
    }

    public void w() {
        this.z = SystemClock.uptimeMillis();
    }
}
