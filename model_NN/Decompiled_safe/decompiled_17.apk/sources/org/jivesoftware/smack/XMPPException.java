package org.jivesoftware.smack;

import java.io.PrintStream;
import java.io.PrintWriter;
import org.jivesoftware.smack.packet.StreamError;
import org.jivesoftware.smack.packet.XMPPError;

public class XMPPException extends Exception {
    private StreamError a = null;
    private XMPPError b = null;
    private Throwable c = null;

    public XMPPException() {
    }

    public XMPPException(String str) {
        super(str);
    }

    public XMPPException(String str, Throwable th) {
        super(str);
        this.c = th;
    }

    public XMPPException(String str, XMPPError xMPPError) {
        super(str);
        this.b = xMPPError;
    }

    public XMPPException(Throwable th) {
        this.c = th;
    }

    public XMPPException(StreamError streamError) {
        this.a = streamError;
    }

    public String getMessage() {
        String message = super.getMessage();
        return (message != null || this.b == null) ? (message != null || this.a == null) ? message : this.a.toString() : this.b.toString();
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        if (this.c != null) {
            printStream.println("Nested Exception: ");
            this.c.printStackTrace(printStream);
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        if (this.c != null) {
            printWriter.println("Nested Exception: ");
            this.c.printStackTrace(printWriter);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String message = super.getMessage();
        if (message != null) {
            sb.append(message).append(": ");
        }
        if (this.b != null) {
            sb.append(this.b);
        }
        if (this.a != null) {
            sb.append(this.a);
        }
        if (this.c != null) {
            sb.append("\n  -- caused by: ").append(this.c);
        }
        return sb.toString();
    }
}
