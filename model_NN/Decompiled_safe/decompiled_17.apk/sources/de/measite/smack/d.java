package de.measite.smack;

import com.xiaomi.channel.commonutils.logger.b;
import java.util.Date;
import org.jivesoftware.smack.ConnectionListener;

class d implements ConnectionListener {
    final /* synthetic */ AndroidDebugger a;

    d(AndroidDebugger androidDebugger) {
        this.a = androidDebugger;
    }

    public void a(Exception exc) {
        b.b("SMACK " + this.a.b.format(new Date()) + " Reconnection failed due to an exception (" + this.a.c.hashCode() + ")");
        exc.printStackTrace();
    }

    public void b(int i, Exception exc) {
        b.b("SMACK " + this.a.b.format(new Date()) + " Connection closed (" + this.a.c.hashCode() + ")");
    }

    public void g() {
        b.b("SMACK " + this.a.b.format(new Date()) + " Connection started (" + this.a.c.hashCode() + ")");
    }

    public void i() {
        b.b("SMACK " + this.a.b.format(new Date()) + " Connection reconnected (" + this.a.c.hashCode() + ")");
    }
}
