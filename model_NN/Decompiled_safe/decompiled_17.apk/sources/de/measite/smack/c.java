package de.measite.smack;

import com.xiaomi.channel.commonutils.logger.b;
import java.util.Date;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;

class c implements PacketListener {
    final /* synthetic */ AndroidDebugger a;

    c(AndroidDebugger androidDebugger) {
        this.a = androidDebugger;
    }

    public void a(Packet packet) {
        if (AndroidDebugger.a) {
            b.b("SMACK " + this.a.b.format(new Date()) + " RCV PKT (" + this.a.c.hashCode() + "): " + packet.a());
        }
    }
}
