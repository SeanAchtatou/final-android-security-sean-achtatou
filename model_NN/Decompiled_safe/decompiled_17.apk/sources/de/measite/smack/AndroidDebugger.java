package de.measite.smack;

import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.debugger.SmackDebugger;
import org.jivesoftware.smack.util.ObservableReader;
import org.jivesoftware.smack.util.ObservableWriter;
import org.jivesoftware.smack.util.ReaderListener;
import org.jivesoftware.smack.util.WriterListener;

public class AndroidDebugger implements SmackDebugger {
    public static boolean a = false;
    /* access modifiers changed from: private */
    public SimpleDateFormat b = new SimpleDateFormat("hh:mm:ss aaa");
    /* access modifiers changed from: private */
    public Connection c = null;
    private PacketListener d = null;
    private ConnectionListener e = null;
    private Writer f;
    private Reader g;
    private ReaderListener h;
    private WriterListener i;

    public AndroidDebugger(Connection connection, Writer writer, Reader reader) {
        this.c = connection;
        this.f = writer;
        this.g = reader;
        e();
    }

    private void e() {
        ObservableReader observableReader = new ObservableReader(this.g);
        this.h = new a(this);
        observableReader.a(this.h);
        ObservableWriter observableWriter = new ObservableWriter(this.f);
        this.i = new b(this);
        observableWriter.a(this.i);
        this.g = observableReader;
        this.f = observableWriter;
        this.d = new c(this);
        this.e = new d(this);
    }

    public Reader a() {
        return this.g;
    }

    public Reader a(Reader reader) {
        ((ObservableReader) this.g).b(this.h);
        ObservableReader observableReader = new ObservableReader(reader);
        observableReader.a(this.h);
        this.g = observableReader;
        return this.g;
    }

    public Writer a(Writer writer) {
        ((ObservableWriter) this.f).b(this.i);
        ObservableWriter observableWriter = new ObservableWriter(writer);
        observableWriter.a(this.i);
        this.f = observableWriter;
        return this.f;
    }

    public Writer b() {
        return this.f;
    }

    public PacketListener c() {
        return this.d;
    }

    public PacketListener d() {
        return null;
    }
}
