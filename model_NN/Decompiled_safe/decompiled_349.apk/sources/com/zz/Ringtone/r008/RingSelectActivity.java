package com.zz.Ringtone.r008;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class RingSelectActivity extends RingdroidActivity {
    private void startPureEditor(String paramString) {
        Intent intent = new Intent("android.intent.action.EDIT", Uri.parse(paramString));
        intent.putExtra("was_get_content_intent", this.mbGetContentIntent);
        intent.setClassName(this, strEdit);
        startActivityForResult(intent, 1);
    }

    public void onCreate(Bundle bundle) {
        RingdroidActivity.strEdit = "com.lib.RingEditor";
        super.onCreate(bundle);
    }
}
