package com.zzbook.util;

import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

public class GenUtil {
    public static boolean bLog = true;
    private static boolean bPrint = true;

    public static void print(String tag, String msg) {
        if (bPrint) {
            Log.v(tag, msg);
        }
    }

    public static void systemPrint(String msg) {
        if (bPrint) {
            print("startSocketMonitor", msg);
        }
    }

    public static void systemPrintln(String msg) {
        if (bPrint) {
            print("startSocketMonitor", msg);
        }
    }

    public static void printTime(String strTitle) {
        if (bPrint) {
            print("startSocketMonitor", strTitle + "------------->" + new Date().getTime());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static void printFile(String strFile, String strOut) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(strFile), true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        try {
            osw.write(strOut);
            osw.flush();
            osw.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        try {
            fos.close();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }
}
