package com.skyd.bestpuzzle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.ui.LeaderboardsScreenActivity;
import com.scoreloop.client.android.ui.OnScoreSubmitObserver;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.skyd.bestpuzzle.PuzzleScene;
import com.skyd.bestpuzzle.PuzzleView;
import com.skyd.bestpuzzle.n1623.R;
import com.skyd.core.android.Android;
import com.skyd.core.android.CommonActivity;
import com.skyd.core.android.admob.AdmobViewEx;

public class Main extends CommonActivity implements OnScoreSubmitObserver {
    private AdmobViewEx _AD = null;
    private RelativeLayout _relativeLayout1 = null;
    private PuzzleView _view = null;

    public PuzzleView getview() {
        if (this._view == null) {
            this._view = (PuzzleView) findViewById(R.id.view);
        }
        return this._view;
    }

    public AdmobViewEx getAD() {
        if (this._AD == null) {
            this._AD = (AdmobViewEx) findViewById(R.id.AD);
        }
        return this._AD;
    }

    public RelativeLayout getrelativeLayout1() {
        if (this._relativeLayout1 == null) {
            this._relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        }
        return this._relativeLayout1;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Android.setToFullScreen(this);
        setContentView((int) R.layout.main);
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(this);
        if (getIsRegistered("com.skyd.bestpuzzle.key", "SKYDSTUDIOBESTPUZZLE", "REG").booleanValue() || getIsRegistered("com.skyd.bestpuzzle.key.backup", "SKYDSTUDIOBESTPUZZLE", "REG").booleanValue()) {
            getAD().setEnabled(false);
            getAD().setVisibility(8);
            getAD().removeFrom(getrelativeLayout1());
        } else {
            getAD().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Main.this.getAD().setEnabled(false);
                    Main.this.getAD().setVisibility(8);
                    Main.this.getAD().removeFrom(Main.this.getrelativeLayout1());
                }
            });
        }
        getview().addOnMainSceneLoadedListener(new PuzzleView.OnMainSceneLoadedListener() {
            public void OnMainSceneLoadedEvent(Object sender) {
                Main.this.getview().MainScene.addOnIsFinishedChangedListener(new PuzzleScene.OnIsFinishedChangedListener() {
                    public void OnIsFinishedChangedEvent(Object sender, boolean newValue) {
                        if (newValue && !Main.this.getview().MainScene.getIsFinished()) {
                            Main.this.openUrlDialog(R.string.Congratulations, R.string.FinishTip, R.string.Yes, R.string.No, R.string.moreAppUrl);
                        }
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (getview().MainScene != null) {
            getview().MainScene.savePuzzleState();
            getview().MainScene.saveGameState();
        }
        super.onStop();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, 1, 1, (int) R.string.RestartgameMenu);
        menu.add(11, 11, 11, (int) R.string.SavegameMenu);
        menu.add(21, 21, 21, (int) R.string.ViewScoreList);
        menu.add(31, 31, 31, (int) R.string.ViewdemoMenu);
        menu.add(41, 41, 41, (int) R.string.ShareMenu);
        if (!getIsRegistered("com.skyd.bestpuzzle.key", "SKYDSTUDIOBESTPUZZLE", "REG").booleanValue() && !getIsRegistered("com.skyd.bestpuzzle.key.backup", "SKYDSTUDIOBESTPUZZLE", "REG").booleanValue()) {
            menu.add(51, 51, 51, (int) R.string.GetfullversionkeyMenu);
        }
        menu.add(61, 61, 61, (int) R.string.MoreappsinfoMenu);
        menu.add(71, 71, 71, (int) R.string.AboutMenu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage((int) R.string.RestartTip);
                builder.setTitle((int) R.string.Notice);
                builder.setPositiveButton((int) R.string.OK, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Main.this.getview().MainScene.reset();
                    }
                });
                builder.setNegativeButton((int) R.string.Cancel, (DialogInterface.OnClickListener) null);
                builder.create().show();
                break;
            case 11:
                getview().MainScene.savePuzzleState();
                getview().MainScene.saveGameState();
                Toast.makeText(this, (int) R.string.SaveTip, 0).show();
                break;
            case Constant.LIST_ITEM_TYPE_SCORE_HIGHLIGHTED:
                Intent intent = new Intent(this, LeaderboardsScreenActivity.class);
                intent.putExtra("mode", 17);
                intent.putExtra(LeaderboardsScreenActivity.LEADERBOARD, 2);
                startActivity(intent);
                break;
            case 31:
                Android.openUrl(this, (int) R.string.demoVideoUrl);
                break;
            case RequestControllerException.CODE_BUDDY_REMOVE_REQUEST_ALREADY_REMOVED:
                openActivity(Share.class);
                break;
            case 51:
                openActivity(GetFullVersion.class);
                break;
            case 61:
                openActivity(MoreApps.class);
                break;
            case 71:
                openActivity(About.class);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    public void onScoreSubmit(int status, Exception error) {
        if (error != null) {
            Toast.makeText(this, (int) R.string.Submitscoresfailure, 1).show();
            if (getview().MainScene.getIsFinished()) {
                getview().MainScene.SubmitScoreButton.show();
                return;
            }
            return;
        }
        Toast.makeText(this, (int) R.string.Submitscoressuccess, 1).show();
    }
}
