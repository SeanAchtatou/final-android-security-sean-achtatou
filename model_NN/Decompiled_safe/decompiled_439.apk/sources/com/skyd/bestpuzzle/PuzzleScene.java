package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1623.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(384.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(5);
        this.OriginalImage.getOriginalSize().reset(888.0f, 555.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1776.0f, 1110.0f);
        this.Desktop.getCurrentObservePosition().reset(888.0f, 555.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 58, 54, 37));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 52, 48, 31));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(111.0f, 111.0f);
        Puzzle.getMaxRadius().reset(85.1f, 85.1f);
        c1678121771(c);
        c766613480(c);
        c1306745018(c);
        c941471379(c);
        c810689106(c);
        c449670082(c);
        c1322344166(c);
        c665862877(c);
        c2058486715(c);
        c1718853485(c);
        c1184536106(c);
        c1454182021(c);
        c1916013502(c);
        c1844114405(c);
        c1176264661(c);
        c2089533729(c);
        c982950936(c);
        c1974589988(c);
        c547946195(c);
        c881569322(c);
        c2145460891(c);
        c1015754669(c);
        c1279350927(c);
        c1344808513(c);
        c169021583(c);
        c1572338253(c);
        c1941929978(c);
        c1415976045(c);
        c106282070(c);
        c1783474233(c);
        c308183927(c);
        c2135757662(c);
        c1353333595(c);
        c592151660(c);
        c802621906(c);
        c875287721(c);
        c733051384(c);
        c769169869(c);
        c728747098(c);
        c1551699277(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(17);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c1678121771(Context c) {
        Puzzle p1678121771 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1678121771(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1678121771(editor, this);
            }
        };
        p1678121771.setID(1678121771);
        p1678121771.setName("1678121771");
        p1678121771.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1678121771);
        this.Desktop.RandomlyPlaced(p1678121771);
        p1678121771.setTopEdgeType(EdgeType.Flat);
        p1678121771.setBottomEdgeType(EdgeType.Convex);
        p1678121771.setLeftEdgeType(EdgeType.Flat);
        p1678121771.setRightEdgeType(EdgeType.Concave);
        p1678121771.setTopExactPuzzleID(-1);
        p1678121771.setBottomExactPuzzleID(766613480);
        p1678121771.setLeftExactPuzzleID(-1);
        p1678121771.setRightExactPuzzleID(449670082);
        p1678121771.getDisplayImage().loadImageFromResource(c, R.drawable.p1678121771h);
        p1678121771.setExactRow(0);
        p1678121771.setExactColumn(0);
        p1678121771.getSize().reset(111.0f, 140.6f);
        p1678121771.getPositionOffset().reset(-55.5f, -55.5f);
        p1678121771.setIsUseAbsolutePosition(true);
        p1678121771.setIsUseAbsoluteSize(true);
        p1678121771.getImage().setIsUseAbsolutePosition(true);
        p1678121771.getImage().setIsUseAbsoluteSize(true);
        p1678121771.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1678121771.resetPosition();
        getSpiritList().add(p1678121771);
    }

    /* access modifiers changed from: package-private */
    public void c766613480(Context c) {
        Puzzle p766613480 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load766613480(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save766613480(editor, this);
            }
        };
        p766613480.setID(766613480);
        p766613480.setName("766613480");
        p766613480.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p766613480);
        this.Desktop.RandomlyPlaced(p766613480);
        p766613480.setTopEdgeType(EdgeType.Concave);
        p766613480.setBottomEdgeType(EdgeType.Convex);
        p766613480.setLeftEdgeType(EdgeType.Flat);
        p766613480.setRightEdgeType(EdgeType.Convex);
        p766613480.setTopExactPuzzleID(1678121771);
        p766613480.setBottomExactPuzzleID(1306745018);
        p766613480.setLeftExactPuzzleID(-1);
        p766613480.setRightExactPuzzleID(1322344166);
        p766613480.getDisplayImage().loadImageFromResource(c, R.drawable.p766613480h);
        p766613480.setExactRow(1);
        p766613480.setExactColumn(0);
        p766613480.getSize().reset(140.6f, 140.6f);
        p766613480.getPositionOffset().reset(-55.5f, -55.5f);
        p766613480.setIsUseAbsolutePosition(true);
        p766613480.setIsUseAbsoluteSize(true);
        p766613480.getImage().setIsUseAbsolutePosition(true);
        p766613480.getImage().setIsUseAbsoluteSize(true);
        p766613480.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p766613480.resetPosition();
        getSpiritList().add(p766613480);
    }

    /* access modifiers changed from: package-private */
    public void c1306745018(Context c) {
        Puzzle p1306745018 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1306745018(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1306745018(editor, this);
            }
        };
        p1306745018.setID(1306745018);
        p1306745018.setName("1306745018");
        p1306745018.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1306745018);
        this.Desktop.RandomlyPlaced(p1306745018);
        p1306745018.setTopEdgeType(EdgeType.Concave);
        p1306745018.setBottomEdgeType(EdgeType.Concave);
        p1306745018.setLeftEdgeType(EdgeType.Flat);
        p1306745018.setRightEdgeType(EdgeType.Convex);
        p1306745018.setTopExactPuzzleID(766613480);
        p1306745018.setBottomExactPuzzleID(941471379);
        p1306745018.setLeftExactPuzzleID(-1);
        p1306745018.setRightExactPuzzleID(665862877);
        p1306745018.getDisplayImage().loadImageFromResource(c, R.drawable.p1306745018h);
        p1306745018.setExactRow(2);
        p1306745018.setExactColumn(0);
        p1306745018.getSize().reset(140.6f, 111.0f);
        p1306745018.getPositionOffset().reset(-55.5f, -55.5f);
        p1306745018.setIsUseAbsolutePosition(true);
        p1306745018.setIsUseAbsoluteSize(true);
        p1306745018.getImage().setIsUseAbsolutePosition(true);
        p1306745018.getImage().setIsUseAbsoluteSize(true);
        p1306745018.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1306745018.resetPosition();
        getSpiritList().add(p1306745018);
    }

    /* access modifiers changed from: package-private */
    public void c941471379(Context c) {
        Puzzle p941471379 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load941471379(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save941471379(editor, this);
            }
        };
        p941471379.setID(941471379);
        p941471379.setName("941471379");
        p941471379.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p941471379);
        this.Desktop.RandomlyPlaced(p941471379);
        p941471379.setTopEdgeType(EdgeType.Convex);
        p941471379.setBottomEdgeType(EdgeType.Convex);
        p941471379.setLeftEdgeType(EdgeType.Flat);
        p941471379.setRightEdgeType(EdgeType.Convex);
        p941471379.setTopExactPuzzleID(1306745018);
        p941471379.setBottomExactPuzzleID(810689106);
        p941471379.setLeftExactPuzzleID(-1);
        p941471379.setRightExactPuzzleID(2058486715);
        p941471379.getDisplayImage().loadImageFromResource(c, R.drawable.p941471379h);
        p941471379.setExactRow(3);
        p941471379.setExactColumn(0);
        p941471379.getSize().reset(140.6f, 170.2f);
        p941471379.getPositionOffset().reset(-55.5f, -85.1f);
        p941471379.setIsUseAbsolutePosition(true);
        p941471379.setIsUseAbsoluteSize(true);
        p941471379.getImage().setIsUseAbsolutePosition(true);
        p941471379.getImage().setIsUseAbsoluteSize(true);
        p941471379.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p941471379.resetPosition();
        getSpiritList().add(p941471379);
    }

    /* access modifiers changed from: package-private */
    public void c810689106(Context c) {
        Puzzle p810689106 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load810689106(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save810689106(editor, this);
            }
        };
        p810689106.setID(810689106);
        p810689106.setName("810689106");
        p810689106.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p810689106);
        this.Desktop.RandomlyPlaced(p810689106);
        p810689106.setTopEdgeType(EdgeType.Concave);
        p810689106.setBottomEdgeType(EdgeType.Flat);
        p810689106.setLeftEdgeType(EdgeType.Flat);
        p810689106.setRightEdgeType(EdgeType.Convex);
        p810689106.setTopExactPuzzleID(941471379);
        p810689106.setBottomExactPuzzleID(-1);
        p810689106.setLeftExactPuzzleID(-1);
        p810689106.setRightExactPuzzleID(1718853485);
        p810689106.getDisplayImage().loadImageFromResource(c, R.drawable.p810689106h);
        p810689106.setExactRow(4);
        p810689106.setExactColumn(0);
        p810689106.getSize().reset(140.6f, 111.0f);
        p810689106.getPositionOffset().reset(-55.5f, -55.5f);
        p810689106.setIsUseAbsolutePosition(true);
        p810689106.setIsUseAbsoluteSize(true);
        p810689106.getImage().setIsUseAbsolutePosition(true);
        p810689106.getImage().setIsUseAbsoluteSize(true);
        p810689106.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p810689106.resetPosition();
        getSpiritList().add(p810689106);
    }

    /* access modifiers changed from: package-private */
    public void c449670082(Context c) {
        Puzzle p449670082 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load449670082(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save449670082(editor, this);
            }
        };
        p449670082.setID(449670082);
        p449670082.setName("449670082");
        p449670082.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p449670082);
        this.Desktop.RandomlyPlaced(p449670082);
        p449670082.setTopEdgeType(EdgeType.Flat);
        p449670082.setBottomEdgeType(EdgeType.Convex);
        p449670082.setLeftEdgeType(EdgeType.Convex);
        p449670082.setRightEdgeType(EdgeType.Concave);
        p449670082.setTopExactPuzzleID(-1);
        p449670082.setBottomExactPuzzleID(1322344166);
        p449670082.setLeftExactPuzzleID(1678121771);
        p449670082.setRightExactPuzzleID(1184536106);
        p449670082.getDisplayImage().loadImageFromResource(c, R.drawable.p449670082h);
        p449670082.setExactRow(0);
        p449670082.setExactColumn(1);
        p449670082.getSize().reset(140.6f, 140.6f);
        p449670082.getPositionOffset().reset(-85.1f, -55.5f);
        p449670082.setIsUseAbsolutePosition(true);
        p449670082.setIsUseAbsoluteSize(true);
        p449670082.getImage().setIsUseAbsolutePosition(true);
        p449670082.getImage().setIsUseAbsoluteSize(true);
        p449670082.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p449670082.resetPosition();
        getSpiritList().add(p449670082);
    }

    /* access modifiers changed from: package-private */
    public void c1322344166(Context c) {
        Puzzle p1322344166 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1322344166(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1322344166(editor, this);
            }
        };
        p1322344166.setID(1322344166);
        p1322344166.setName("1322344166");
        p1322344166.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1322344166);
        this.Desktop.RandomlyPlaced(p1322344166);
        p1322344166.setTopEdgeType(EdgeType.Concave);
        p1322344166.setBottomEdgeType(EdgeType.Convex);
        p1322344166.setLeftEdgeType(EdgeType.Concave);
        p1322344166.setRightEdgeType(EdgeType.Concave);
        p1322344166.setTopExactPuzzleID(449670082);
        p1322344166.setBottomExactPuzzleID(665862877);
        p1322344166.setLeftExactPuzzleID(766613480);
        p1322344166.setRightExactPuzzleID(1454182021);
        p1322344166.getDisplayImage().loadImageFromResource(c, R.drawable.p1322344166h);
        p1322344166.setExactRow(1);
        p1322344166.setExactColumn(1);
        p1322344166.getSize().reset(111.0f, 140.6f);
        p1322344166.getPositionOffset().reset(-55.5f, -55.5f);
        p1322344166.setIsUseAbsolutePosition(true);
        p1322344166.setIsUseAbsoluteSize(true);
        p1322344166.getImage().setIsUseAbsolutePosition(true);
        p1322344166.getImage().setIsUseAbsoluteSize(true);
        p1322344166.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1322344166.resetPosition();
        getSpiritList().add(p1322344166);
    }

    /* access modifiers changed from: package-private */
    public void c665862877(Context c) {
        Puzzle p665862877 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load665862877(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save665862877(editor, this);
            }
        };
        p665862877.setID(665862877);
        p665862877.setName("665862877");
        p665862877.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p665862877);
        this.Desktop.RandomlyPlaced(p665862877);
        p665862877.setTopEdgeType(EdgeType.Concave);
        p665862877.setBottomEdgeType(EdgeType.Convex);
        p665862877.setLeftEdgeType(EdgeType.Concave);
        p665862877.setRightEdgeType(EdgeType.Convex);
        p665862877.setTopExactPuzzleID(1322344166);
        p665862877.setBottomExactPuzzleID(2058486715);
        p665862877.setLeftExactPuzzleID(1306745018);
        p665862877.setRightExactPuzzleID(1916013502);
        p665862877.getDisplayImage().loadImageFromResource(c, R.drawable.p665862877h);
        p665862877.setExactRow(2);
        p665862877.setExactColumn(1);
        p665862877.getSize().reset(140.6f, 140.6f);
        p665862877.getPositionOffset().reset(-55.5f, -55.5f);
        p665862877.setIsUseAbsolutePosition(true);
        p665862877.setIsUseAbsoluteSize(true);
        p665862877.getImage().setIsUseAbsolutePosition(true);
        p665862877.getImage().setIsUseAbsoluteSize(true);
        p665862877.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p665862877.resetPosition();
        getSpiritList().add(p665862877);
    }

    /* access modifiers changed from: package-private */
    public void c2058486715(Context c) {
        Puzzle p2058486715 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2058486715(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2058486715(editor, this);
            }
        };
        p2058486715.setID(2058486715);
        p2058486715.setName("2058486715");
        p2058486715.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2058486715);
        this.Desktop.RandomlyPlaced(p2058486715);
        p2058486715.setTopEdgeType(EdgeType.Concave);
        p2058486715.setBottomEdgeType(EdgeType.Convex);
        p2058486715.setLeftEdgeType(EdgeType.Concave);
        p2058486715.setRightEdgeType(EdgeType.Concave);
        p2058486715.setTopExactPuzzleID(665862877);
        p2058486715.setBottomExactPuzzleID(1718853485);
        p2058486715.setLeftExactPuzzleID(941471379);
        p2058486715.setRightExactPuzzleID(1844114405);
        p2058486715.getDisplayImage().loadImageFromResource(c, R.drawable.p2058486715h);
        p2058486715.setExactRow(3);
        p2058486715.setExactColumn(1);
        p2058486715.getSize().reset(111.0f, 140.6f);
        p2058486715.getPositionOffset().reset(-55.5f, -55.5f);
        p2058486715.setIsUseAbsolutePosition(true);
        p2058486715.setIsUseAbsoluteSize(true);
        p2058486715.getImage().setIsUseAbsolutePosition(true);
        p2058486715.getImage().setIsUseAbsoluteSize(true);
        p2058486715.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2058486715.resetPosition();
        getSpiritList().add(p2058486715);
    }

    /* access modifiers changed from: package-private */
    public void c1718853485(Context c) {
        Puzzle p1718853485 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1718853485(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1718853485(editor, this);
            }
        };
        p1718853485.setID(1718853485);
        p1718853485.setName("1718853485");
        p1718853485.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1718853485);
        this.Desktop.RandomlyPlaced(p1718853485);
        p1718853485.setTopEdgeType(EdgeType.Concave);
        p1718853485.setBottomEdgeType(EdgeType.Flat);
        p1718853485.setLeftEdgeType(EdgeType.Concave);
        p1718853485.setRightEdgeType(EdgeType.Concave);
        p1718853485.setTopExactPuzzleID(2058486715);
        p1718853485.setBottomExactPuzzleID(-1);
        p1718853485.setLeftExactPuzzleID(810689106);
        p1718853485.setRightExactPuzzleID(1176264661);
        p1718853485.getDisplayImage().loadImageFromResource(c, R.drawable.p1718853485h);
        p1718853485.setExactRow(4);
        p1718853485.setExactColumn(1);
        p1718853485.getSize().reset(111.0f, 111.0f);
        p1718853485.getPositionOffset().reset(-55.5f, -55.5f);
        p1718853485.setIsUseAbsolutePosition(true);
        p1718853485.setIsUseAbsoluteSize(true);
        p1718853485.getImage().setIsUseAbsolutePosition(true);
        p1718853485.getImage().setIsUseAbsoluteSize(true);
        p1718853485.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1718853485.resetPosition();
        getSpiritList().add(p1718853485);
    }

    /* access modifiers changed from: package-private */
    public void c1184536106(Context c) {
        Puzzle p1184536106 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1184536106(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1184536106(editor, this);
            }
        };
        p1184536106.setID(1184536106);
        p1184536106.setName("1184536106");
        p1184536106.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1184536106);
        this.Desktop.RandomlyPlaced(p1184536106);
        p1184536106.setTopEdgeType(EdgeType.Flat);
        p1184536106.setBottomEdgeType(EdgeType.Convex);
        p1184536106.setLeftEdgeType(EdgeType.Convex);
        p1184536106.setRightEdgeType(EdgeType.Convex);
        p1184536106.setTopExactPuzzleID(-1);
        p1184536106.setBottomExactPuzzleID(1454182021);
        p1184536106.setLeftExactPuzzleID(449670082);
        p1184536106.setRightExactPuzzleID(2089533729);
        p1184536106.getDisplayImage().loadImageFromResource(c, R.drawable.p1184536106h);
        p1184536106.setExactRow(0);
        p1184536106.setExactColumn(2);
        p1184536106.getSize().reset(170.2f, 140.6f);
        p1184536106.getPositionOffset().reset(-85.1f, -55.5f);
        p1184536106.setIsUseAbsolutePosition(true);
        p1184536106.setIsUseAbsoluteSize(true);
        p1184536106.getImage().setIsUseAbsolutePosition(true);
        p1184536106.getImage().setIsUseAbsoluteSize(true);
        p1184536106.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1184536106.resetPosition();
        getSpiritList().add(p1184536106);
    }

    /* access modifiers changed from: package-private */
    public void c1454182021(Context c) {
        Puzzle p1454182021 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1454182021(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1454182021(editor, this);
            }
        };
        p1454182021.setID(1454182021);
        p1454182021.setName("1454182021");
        p1454182021.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1454182021);
        this.Desktop.RandomlyPlaced(p1454182021);
        p1454182021.setTopEdgeType(EdgeType.Concave);
        p1454182021.setBottomEdgeType(EdgeType.Concave);
        p1454182021.setLeftEdgeType(EdgeType.Convex);
        p1454182021.setRightEdgeType(EdgeType.Concave);
        p1454182021.setTopExactPuzzleID(1184536106);
        p1454182021.setBottomExactPuzzleID(1916013502);
        p1454182021.setLeftExactPuzzleID(1322344166);
        p1454182021.setRightExactPuzzleID(982950936);
        p1454182021.getDisplayImage().loadImageFromResource(c, R.drawable.p1454182021h);
        p1454182021.setExactRow(1);
        p1454182021.setExactColumn(2);
        p1454182021.getSize().reset(140.6f, 111.0f);
        p1454182021.getPositionOffset().reset(-85.1f, -55.5f);
        p1454182021.setIsUseAbsolutePosition(true);
        p1454182021.setIsUseAbsoluteSize(true);
        p1454182021.getImage().setIsUseAbsolutePosition(true);
        p1454182021.getImage().setIsUseAbsoluteSize(true);
        p1454182021.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1454182021.resetPosition();
        getSpiritList().add(p1454182021);
    }

    /* access modifiers changed from: package-private */
    public void c1916013502(Context c) {
        Puzzle p1916013502 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1916013502(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1916013502(editor, this);
            }
        };
        p1916013502.setID(1916013502);
        p1916013502.setName("1916013502");
        p1916013502.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1916013502);
        this.Desktop.RandomlyPlaced(p1916013502);
        p1916013502.setTopEdgeType(EdgeType.Convex);
        p1916013502.setBottomEdgeType(EdgeType.Convex);
        p1916013502.setLeftEdgeType(EdgeType.Concave);
        p1916013502.setRightEdgeType(EdgeType.Convex);
        p1916013502.setTopExactPuzzleID(1454182021);
        p1916013502.setBottomExactPuzzleID(1844114405);
        p1916013502.setLeftExactPuzzleID(665862877);
        p1916013502.setRightExactPuzzleID(1974589988);
        p1916013502.getDisplayImage().loadImageFromResource(c, R.drawable.p1916013502h);
        p1916013502.setExactRow(2);
        p1916013502.setExactColumn(2);
        p1916013502.getSize().reset(140.6f, 170.2f);
        p1916013502.getPositionOffset().reset(-55.5f, -85.1f);
        p1916013502.setIsUseAbsolutePosition(true);
        p1916013502.setIsUseAbsoluteSize(true);
        p1916013502.getImage().setIsUseAbsolutePosition(true);
        p1916013502.getImage().setIsUseAbsoluteSize(true);
        p1916013502.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1916013502.resetPosition();
        getSpiritList().add(p1916013502);
    }

    /* access modifiers changed from: package-private */
    public void c1844114405(Context c) {
        Puzzle p1844114405 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1844114405(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1844114405(editor, this);
            }
        };
        p1844114405.setID(1844114405);
        p1844114405.setName("1844114405");
        p1844114405.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1844114405);
        this.Desktop.RandomlyPlaced(p1844114405);
        p1844114405.setTopEdgeType(EdgeType.Concave);
        p1844114405.setBottomEdgeType(EdgeType.Concave);
        p1844114405.setLeftEdgeType(EdgeType.Convex);
        p1844114405.setRightEdgeType(EdgeType.Concave);
        p1844114405.setTopExactPuzzleID(1916013502);
        p1844114405.setBottomExactPuzzleID(1176264661);
        p1844114405.setLeftExactPuzzleID(2058486715);
        p1844114405.setRightExactPuzzleID(547946195);
        p1844114405.getDisplayImage().loadImageFromResource(c, R.drawable.p1844114405h);
        p1844114405.setExactRow(3);
        p1844114405.setExactColumn(2);
        p1844114405.getSize().reset(140.6f, 111.0f);
        p1844114405.getPositionOffset().reset(-85.1f, -55.5f);
        p1844114405.setIsUseAbsolutePosition(true);
        p1844114405.setIsUseAbsoluteSize(true);
        p1844114405.getImage().setIsUseAbsolutePosition(true);
        p1844114405.getImage().setIsUseAbsoluteSize(true);
        p1844114405.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1844114405.resetPosition();
        getSpiritList().add(p1844114405);
    }

    /* access modifiers changed from: package-private */
    public void c1176264661(Context c) {
        Puzzle p1176264661 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1176264661(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1176264661(editor, this);
            }
        };
        p1176264661.setID(1176264661);
        p1176264661.setName("1176264661");
        p1176264661.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1176264661);
        this.Desktop.RandomlyPlaced(p1176264661);
        p1176264661.setTopEdgeType(EdgeType.Convex);
        p1176264661.setBottomEdgeType(EdgeType.Flat);
        p1176264661.setLeftEdgeType(EdgeType.Convex);
        p1176264661.setRightEdgeType(EdgeType.Concave);
        p1176264661.setTopExactPuzzleID(1844114405);
        p1176264661.setBottomExactPuzzleID(-1);
        p1176264661.setLeftExactPuzzleID(1718853485);
        p1176264661.setRightExactPuzzleID(881569322);
        p1176264661.getDisplayImage().loadImageFromResource(c, R.drawable.p1176264661h);
        p1176264661.setExactRow(4);
        p1176264661.setExactColumn(2);
        p1176264661.getSize().reset(140.6f, 140.6f);
        p1176264661.getPositionOffset().reset(-85.1f, -85.1f);
        p1176264661.setIsUseAbsolutePosition(true);
        p1176264661.setIsUseAbsoluteSize(true);
        p1176264661.getImage().setIsUseAbsolutePosition(true);
        p1176264661.getImage().setIsUseAbsoluteSize(true);
        p1176264661.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1176264661.resetPosition();
        getSpiritList().add(p1176264661);
    }

    /* access modifiers changed from: package-private */
    public void c2089533729(Context c) {
        Puzzle p2089533729 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2089533729(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2089533729(editor, this);
            }
        };
        p2089533729.setID(2089533729);
        p2089533729.setName("2089533729");
        p2089533729.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2089533729);
        this.Desktop.RandomlyPlaced(p2089533729);
        p2089533729.setTopEdgeType(EdgeType.Flat);
        p2089533729.setBottomEdgeType(EdgeType.Convex);
        p2089533729.setLeftEdgeType(EdgeType.Concave);
        p2089533729.setRightEdgeType(EdgeType.Concave);
        p2089533729.setTopExactPuzzleID(-1);
        p2089533729.setBottomExactPuzzleID(982950936);
        p2089533729.setLeftExactPuzzleID(1184536106);
        p2089533729.setRightExactPuzzleID(2145460891);
        p2089533729.getDisplayImage().loadImageFromResource(c, R.drawable.p2089533729h);
        p2089533729.setExactRow(0);
        p2089533729.setExactColumn(3);
        p2089533729.getSize().reset(111.0f, 140.6f);
        p2089533729.getPositionOffset().reset(-55.5f, -55.5f);
        p2089533729.setIsUseAbsolutePosition(true);
        p2089533729.setIsUseAbsoluteSize(true);
        p2089533729.getImage().setIsUseAbsolutePosition(true);
        p2089533729.getImage().setIsUseAbsoluteSize(true);
        p2089533729.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2089533729.resetPosition();
        getSpiritList().add(p2089533729);
    }

    /* access modifiers changed from: package-private */
    public void c982950936(Context c) {
        Puzzle p982950936 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load982950936(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save982950936(editor, this);
            }
        };
        p982950936.setID(982950936);
        p982950936.setName("982950936");
        p982950936.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p982950936);
        this.Desktop.RandomlyPlaced(p982950936);
        p982950936.setTopEdgeType(EdgeType.Concave);
        p982950936.setBottomEdgeType(EdgeType.Concave);
        p982950936.setLeftEdgeType(EdgeType.Convex);
        p982950936.setRightEdgeType(EdgeType.Convex);
        p982950936.setTopExactPuzzleID(2089533729);
        p982950936.setBottomExactPuzzleID(1974589988);
        p982950936.setLeftExactPuzzleID(1454182021);
        p982950936.setRightExactPuzzleID(1015754669);
        p982950936.getDisplayImage().loadImageFromResource(c, R.drawable.p982950936h);
        p982950936.setExactRow(1);
        p982950936.setExactColumn(3);
        p982950936.getSize().reset(170.2f, 111.0f);
        p982950936.getPositionOffset().reset(-85.1f, -55.5f);
        p982950936.setIsUseAbsolutePosition(true);
        p982950936.setIsUseAbsoluteSize(true);
        p982950936.getImage().setIsUseAbsolutePosition(true);
        p982950936.getImage().setIsUseAbsoluteSize(true);
        p982950936.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p982950936.resetPosition();
        getSpiritList().add(p982950936);
    }

    /* access modifiers changed from: package-private */
    public void c1974589988(Context c) {
        Puzzle p1974589988 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1974589988(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1974589988(editor, this);
            }
        };
        p1974589988.setID(1974589988);
        p1974589988.setName("1974589988");
        p1974589988.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1974589988);
        this.Desktop.RandomlyPlaced(p1974589988);
        p1974589988.setTopEdgeType(EdgeType.Convex);
        p1974589988.setBottomEdgeType(EdgeType.Concave);
        p1974589988.setLeftEdgeType(EdgeType.Concave);
        p1974589988.setRightEdgeType(EdgeType.Convex);
        p1974589988.setTopExactPuzzleID(982950936);
        p1974589988.setBottomExactPuzzleID(547946195);
        p1974589988.setLeftExactPuzzleID(1916013502);
        p1974589988.setRightExactPuzzleID(1279350927);
        p1974589988.getDisplayImage().loadImageFromResource(c, R.drawable.p1974589988h);
        p1974589988.setExactRow(2);
        p1974589988.setExactColumn(3);
        p1974589988.getSize().reset(140.6f, 140.6f);
        p1974589988.getPositionOffset().reset(-55.5f, -85.1f);
        p1974589988.setIsUseAbsolutePosition(true);
        p1974589988.setIsUseAbsoluteSize(true);
        p1974589988.getImage().setIsUseAbsolutePosition(true);
        p1974589988.getImage().setIsUseAbsoluteSize(true);
        p1974589988.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1974589988.resetPosition();
        getSpiritList().add(p1974589988);
    }

    /* access modifiers changed from: package-private */
    public void c547946195(Context c) {
        Puzzle p547946195 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load547946195(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save547946195(editor, this);
            }
        };
        p547946195.setID(547946195);
        p547946195.setName("547946195");
        p547946195.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p547946195);
        this.Desktop.RandomlyPlaced(p547946195);
        p547946195.setTopEdgeType(EdgeType.Convex);
        p547946195.setBottomEdgeType(EdgeType.Convex);
        p547946195.setLeftEdgeType(EdgeType.Convex);
        p547946195.setRightEdgeType(EdgeType.Convex);
        p547946195.setTopExactPuzzleID(1974589988);
        p547946195.setBottomExactPuzzleID(881569322);
        p547946195.setLeftExactPuzzleID(1844114405);
        p547946195.setRightExactPuzzleID(1344808513);
        p547946195.getDisplayImage().loadImageFromResource(c, R.drawable.p547946195h);
        p547946195.setExactRow(3);
        p547946195.setExactColumn(3);
        p547946195.getSize().reset(170.2f, 170.2f);
        p547946195.getPositionOffset().reset(-85.1f, -85.1f);
        p547946195.setIsUseAbsolutePosition(true);
        p547946195.setIsUseAbsoluteSize(true);
        p547946195.getImage().setIsUseAbsolutePosition(true);
        p547946195.getImage().setIsUseAbsoluteSize(true);
        p547946195.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p547946195.resetPosition();
        getSpiritList().add(p547946195);
    }

    /* access modifiers changed from: package-private */
    public void c881569322(Context c) {
        Puzzle p881569322 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load881569322(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save881569322(editor, this);
            }
        };
        p881569322.setID(881569322);
        p881569322.setName("881569322");
        p881569322.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p881569322);
        this.Desktop.RandomlyPlaced(p881569322);
        p881569322.setTopEdgeType(EdgeType.Concave);
        p881569322.setBottomEdgeType(EdgeType.Flat);
        p881569322.setLeftEdgeType(EdgeType.Convex);
        p881569322.setRightEdgeType(EdgeType.Convex);
        p881569322.setTopExactPuzzleID(547946195);
        p881569322.setBottomExactPuzzleID(-1);
        p881569322.setLeftExactPuzzleID(1176264661);
        p881569322.setRightExactPuzzleID(169021583);
        p881569322.getDisplayImage().loadImageFromResource(c, R.drawable.p881569322h);
        p881569322.setExactRow(4);
        p881569322.setExactColumn(3);
        p881569322.getSize().reset(170.2f, 111.0f);
        p881569322.getPositionOffset().reset(-85.1f, -55.5f);
        p881569322.setIsUseAbsolutePosition(true);
        p881569322.setIsUseAbsoluteSize(true);
        p881569322.getImage().setIsUseAbsolutePosition(true);
        p881569322.getImage().setIsUseAbsoluteSize(true);
        p881569322.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p881569322.resetPosition();
        getSpiritList().add(p881569322);
    }

    /* access modifiers changed from: package-private */
    public void c2145460891(Context c) {
        Puzzle p2145460891 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2145460891(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2145460891(editor, this);
            }
        };
        p2145460891.setID(2145460891);
        p2145460891.setName("2145460891");
        p2145460891.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2145460891);
        this.Desktop.RandomlyPlaced(p2145460891);
        p2145460891.setTopEdgeType(EdgeType.Flat);
        p2145460891.setBottomEdgeType(EdgeType.Convex);
        p2145460891.setLeftEdgeType(EdgeType.Convex);
        p2145460891.setRightEdgeType(EdgeType.Concave);
        p2145460891.setTopExactPuzzleID(-1);
        p2145460891.setBottomExactPuzzleID(1015754669);
        p2145460891.setLeftExactPuzzleID(2089533729);
        p2145460891.setRightExactPuzzleID(1572338253);
        p2145460891.getDisplayImage().loadImageFromResource(c, R.drawable.p2145460891h);
        p2145460891.setExactRow(0);
        p2145460891.setExactColumn(4);
        p2145460891.getSize().reset(140.6f, 140.6f);
        p2145460891.getPositionOffset().reset(-85.1f, -55.5f);
        p2145460891.setIsUseAbsolutePosition(true);
        p2145460891.setIsUseAbsoluteSize(true);
        p2145460891.getImage().setIsUseAbsolutePosition(true);
        p2145460891.getImage().setIsUseAbsoluteSize(true);
        p2145460891.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2145460891.resetPosition();
        getSpiritList().add(p2145460891);
    }

    /* access modifiers changed from: package-private */
    public void c1015754669(Context c) {
        Puzzle p1015754669 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1015754669(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1015754669(editor, this);
            }
        };
        p1015754669.setID(1015754669);
        p1015754669.setName("1015754669");
        p1015754669.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1015754669);
        this.Desktop.RandomlyPlaced(p1015754669);
        p1015754669.setTopEdgeType(EdgeType.Concave);
        p1015754669.setBottomEdgeType(EdgeType.Concave);
        p1015754669.setLeftEdgeType(EdgeType.Concave);
        p1015754669.setRightEdgeType(EdgeType.Convex);
        p1015754669.setTopExactPuzzleID(2145460891);
        p1015754669.setBottomExactPuzzleID(1279350927);
        p1015754669.setLeftExactPuzzleID(982950936);
        p1015754669.setRightExactPuzzleID(1941929978);
        p1015754669.getDisplayImage().loadImageFromResource(c, R.drawable.p1015754669h);
        p1015754669.setExactRow(1);
        p1015754669.setExactColumn(4);
        p1015754669.getSize().reset(140.6f, 111.0f);
        p1015754669.getPositionOffset().reset(-55.5f, -55.5f);
        p1015754669.setIsUseAbsolutePosition(true);
        p1015754669.setIsUseAbsoluteSize(true);
        p1015754669.getImage().setIsUseAbsolutePosition(true);
        p1015754669.getImage().setIsUseAbsoluteSize(true);
        p1015754669.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1015754669.resetPosition();
        getSpiritList().add(p1015754669);
    }

    /* access modifiers changed from: package-private */
    public void c1279350927(Context c) {
        Puzzle p1279350927 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1279350927(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1279350927(editor, this);
            }
        };
        p1279350927.setID(1279350927);
        p1279350927.setName("1279350927");
        p1279350927.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1279350927);
        this.Desktop.RandomlyPlaced(p1279350927);
        p1279350927.setTopEdgeType(EdgeType.Convex);
        p1279350927.setBottomEdgeType(EdgeType.Convex);
        p1279350927.setLeftEdgeType(EdgeType.Concave);
        p1279350927.setRightEdgeType(EdgeType.Convex);
        p1279350927.setTopExactPuzzleID(1015754669);
        p1279350927.setBottomExactPuzzleID(1344808513);
        p1279350927.setLeftExactPuzzleID(1974589988);
        p1279350927.setRightExactPuzzleID(1415976045);
        p1279350927.getDisplayImage().loadImageFromResource(c, R.drawable.p1279350927h);
        p1279350927.setExactRow(2);
        p1279350927.setExactColumn(4);
        p1279350927.getSize().reset(140.6f, 170.2f);
        p1279350927.getPositionOffset().reset(-55.5f, -85.1f);
        p1279350927.setIsUseAbsolutePosition(true);
        p1279350927.setIsUseAbsoluteSize(true);
        p1279350927.getImage().setIsUseAbsolutePosition(true);
        p1279350927.getImage().setIsUseAbsoluteSize(true);
        p1279350927.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1279350927.resetPosition();
        getSpiritList().add(p1279350927);
    }

    /* access modifiers changed from: package-private */
    public void c1344808513(Context c) {
        Puzzle p1344808513 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1344808513(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1344808513(editor, this);
            }
        };
        p1344808513.setID(1344808513);
        p1344808513.setName("1344808513");
        p1344808513.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1344808513);
        this.Desktop.RandomlyPlaced(p1344808513);
        p1344808513.setTopEdgeType(EdgeType.Concave);
        p1344808513.setBottomEdgeType(EdgeType.Concave);
        p1344808513.setLeftEdgeType(EdgeType.Concave);
        p1344808513.setRightEdgeType(EdgeType.Concave);
        p1344808513.setTopExactPuzzleID(1279350927);
        p1344808513.setBottomExactPuzzleID(169021583);
        p1344808513.setLeftExactPuzzleID(547946195);
        p1344808513.setRightExactPuzzleID(106282070);
        p1344808513.getDisplayImage().loadImageFromResource(c, R.drawable.p1344808513h);
        p1344808513.setExactRow(3);
        p1344808513.setExactColumn(4);
        p1344808513.getSize().reset(111.0f, 111.0f);
        p1344808513.getPositionOffset().reset(-55.5f, -55.5f);
        p1344808513.setIsUseAbsolutePosition(true);
        p1344808513.setIsUseAbsoluteSize(true);
        p1344808513.getImage().setIsUseAbsolutePosition(true);
        p1344808513.getImage().setIsUseAbsoluteSize(true);
        p1344808513.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1344808513.resetPosition();
        getSpiritList().add(p1344808513);
    }

    /* access modifiers changed from: package-private */
    public void c169021583(Context c) {
        Puzzle p169021583 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load169021583(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save169021583(editor, this);
            }
        };
        p169021583.setID(169021583);
        p169021583.setName("169021583");
        p169021583.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p169021583);
        this.Desktop.RandomlyPlaced(p169021583);
        p169021583.setTopEdgeType(EdgeType.Convex);
        p169021583.setBottomEdgeType(EdgeType.Flat);
        p169021583.setLeftEdgeType(EdgeType.Concave);
        p169021583.setRightEdgeType(EdgeType.Concave);
        p169021583.setTopExactPuzzleID(1344808513);
        p169021583.setBottomExactPuzzleID(-1);
        p169021583.setLeftExactPuzzleID(881569322);
        p169021583.setRightExactPuzzleID(1783474233);
        p169021583.getDisplayImage().loadImageFromResource(c, R.drawable.p169021583h);
        p169021583.setExactRow(4);
        p169021583.setExactColumn(4);
        p169021583.getSize().reset(111.0f, 140.6f);
        p169021583.getPositionOffset().reset(-55.5f, -85.1f);
        p169021583.setIsUseAbsolutePosition(true);
        p169021583.setIsUseAbsoluteSize(true);
        p169021583.getImage().setIsUseAbsolutePosition(true);
        p169021583.getImage().setIsUseAbsoluteSize(true);
        p169021583.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p169021583.resetPosition();
        getSpiritList().add(p169021583);
    }

    /* access modifiers changed from: package-private */
    public void c1572338253(Context c) {
        Puzzle p1572338253 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1572338253(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1572338253(editor, this);
            }
        };
        p1572338253.setID(1572338253);
        p1572338253.setName("1572338253");
        p1572338253.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1572338253);
        this.Desktop.RandomlyPlaced(p1572338253);
        p1572338253.setTopEdgeType(EdgeType.Flat);
        p1572338253.setBottomEdgeType(EdgeType.Concave);
        p1572338253.setLeftEdgeType(EdgeType.Convex);
        p1572338253.setRightEdgeType(EdgeType.Convex);
        p1572338253.setTopExactPuzzleID(-1);
        p1572338253.setBottomExactPuzzleID(1941929978);
        p1572338253.setLeftExactPuzzleID(2145460891);
        p1572338253.setRightExactPuzzleID(308183927);
        p1572338253.getDisplayImage().loadImageFromResource(c, R.drawable.p1572338253h);
        p1572338253.setExactRow(0);
        p1572338253.setExactColumn(5);
        p1572338253.getSize().reset(170.2f, 111.0f);
        p1572338253.getPositionOffset().reset(-85.1f, -55.5f);
        p1572338253.setIsUseAbsolutePosition(true);
        p1572338253.setIsUseAbsoluteSize(true);
        p1572338253.getImage().setIsUseAbsolutePosition(true);
        p1572338253.getImage().setIsUseAbsoluteSize(true);
        p1572338253.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1572338253.resetPosition();
        getSpiritList().add(p1572338253);
    }

    /* access modifiers changed from: package-private */
    public void c1941929978(Context c) {
        Puzzle p1941929978 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1941929978(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1941929978(editor, this);
            }
        };
        p1941929978.setID(1941929978);
        p1941929978.setName("1941929978");
        p1941929978.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1941929978);
        this.Desktop.RandomlyPlaced(p1941929978);
        p1941929978.setTopEdgeType(EdgeType.Convex);
        p1941929978.setBottomEdgeType(EdgeType.Concave);
        p1941929978.setLeftEdgeType(EdgeType.Concave);
        p1941929978.setRightEdgeType(EdgeType.Concave);
        p1941929978.setTopExactPuzzleID(1572338253);
        p1941929978.setBottomExactPuzzleID(1415976045);
        p1941929978.setLeftExactPuzzleID(1015754669);
        p1941929978.setRightExactPuzzleID(2135757662);
        p1941929978.getDisplayImage().loadImageFromResource(c, R.drawable.p1941929978h);
        p1941929978.setExactRow(1);
        p1941929978.setExactColumn(5);
        p1941929978.getSize().reset(111.0f, 140.6f);
        p1941929978.getPositionOffset().reset(-55.5f, -85.1f);
        p1941929978.setIsUseAbsolutePosition(true);
        p1941929978.setIsUseAbsoluteSize(true);
        p1941929978.getImage().setIsUseAbsolutePosition(true);
        p1941929978.getImage().setIsUseAbsoluteSize(true);
        p1941929978.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1941929978.resetPosition();
        getSpiritList().add(p1941929978);
    }

    /* access modifiers changed from: package-private */
    public void c1415976045(Context c) {
        Puzzle p1415976045 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1415976045(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1415976045(editor, this);
            }
        };
        p1415976045.setID(1415976045);
        p1415976045.setName("1415976045");
        p1415976045.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1415976045);
        this.Desktop.RandomlyPlaced(p1415976045);
        p1415976045.setTopEdgeType(EdgeType.Convex);
        p1415976045.setBottomEdgeType(EdgeType.Convex);
        p1415976045.setLeftEdgeType(EdgeType.Concave);
        p1415976045.setRightEdgeType(EdgeType.Concave);
        p1415976045.setTopExactPuzzleID(1941929978);
        p1415976045.setBottomExactPuzzleID(106282070);
        p1415976045.setLeftExactPuzzleID(1279350927);
        p1415976045.setRightExactPuzzleID(1353333595);
        p1415976045.getDisplayImage().loadImageFromResource(c, R.drawable.p1415976045h);
        p1415976045.setExactRow(2);
        p1415976045.setExactColumn(5);
        p1415976045.getSize().reset(111.0f, 170.2f);
        p1415976045.getPositionOffset().reset(-55.5f, -85.1f);
        p1415976045.setIsUseAbsolutePosition(true);
        p1415976045.setIsUseAbsoluteSize(true);
        p1415976045.getImage().setIsUseAbsolutePosition(true);
        p1415976045.getImage().setIsUseAbsoluteSize(true);
        p1415976045.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1415976045.resetPosition();
        getSpiritList().add(p1415976045);
    }

    /* access modifiers changed from: package-private */
    public void c106282070(Context c) {
        Puzzle p106282070 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load106282070(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save106282070(editor, this);
            }
        };
        p106282070.setID(106282070);
        p106282070.setName("106282070");
        p106282070.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p106282070);
        this.Desktop.RandomlyPlaced(p106282070);
        p106282070.setTopEdgeType(EdgeType.Concave);
        p106282070.setBottomEdgeType(EdgeType.Concave);
        p106282070.setLeftEdgeType(EdgeType.Convex);
        p106282070.setRightEdgeType(EdgeType.Convex);
        p106282070.setTopExactPuzzleID(1415976045);
        p106282070.setBottomExactPuzzleID(1783474233);
        p106282070.setLeftExactPuzzleID(1344808513);
        p106282070.setRightExactPuzzleID(592151660);
        p106282070.getDisplayImage().loadImageFromResource(c, R.drawable.p106282070h);
        p106282070.setExactRow(3);
        p106282070.setExactColumn(5);
        p106282070.getSize().reset(170.2f, 111.0f);
        p106282070.getPositionOffset().reset(-85.1f, -55.5f);
        p106282070.setIsUseAbsolutePosition(true);
        p106282070.setIsUseAbsoluteSize(true);
        p106282070.getImage().setIsUseAbsolutePosition(true);
        p106282070.getImage().setIsUseAbsoluteSize(true);
        p106282070.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p106282070.resetPosition();
        getSpiritList().add(p106282070);
    }

    /* access modifiers changed from: package-private */
    public void c1783474233(Context c) {
        Puzzle p1783474233 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1783474233(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1783474233(editor, this);
            }
        };
        p1783474233.setID(1783474233);
        p1783474233.setName("1783474233");
        p1783474233.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1783474233);
        this.Desktop.RandomlyPlaced(p1783474233);
        p1783474233.setTopEdgeType(EdgeType.Convex);
        p1783474233.setBottomEdgeType(EdgeType.Flat);
        p1783474233.setLeftEdgeType(EdgeType.Convex);
        p1783474233.setRightEdgeType(EdgeType.Convex);
        p1783474233.setTopExactPuzzleID(106282070);
        p1783474233.setBottomExactPuzzleID(-1);
        p1783474233.setLeftExactPuzzleID(169021583);
        p1783474233.setRightExactPuzzleID(802621906);
        p1783474233.getDisplayImage().loadImageFromResource(c, R.drawable.p1783474233h);
        p1783474233.setExactRow(4);
        p1783474233.setExactColumn(5);
        p1783474233.getSize().reset(170.2f, 140.6f);
        p1783474233.getPositionOffset().reset(-85.1f, -85.1f);
        p1783474233.setIsUseAbsolutePosition(true);
        p1783474233.setIsUseAbsoluteSize(true);
        p1783474233.getImage().setIsUseAbsolutePosition(true);
        p1783474233.getImage().setIsUseAbsoluteSize(true);
        p1783474233.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1783474233.resetPosition();
        getSpiritList().add(p1783474233);
    }

    /* access modifiers changed from: package-private */
    public void c308183927(Context c) {
        Puzzle p308183927 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load308183927(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save308183927(editor, this);
            }
        };
        p308183927.setID(308183927);
        p308183927.setName("308183927");
        p308183927.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p308183927);
        this.Desktop.RandomlyPlaced(p308183927);
        p308183927.setTopEdgeType(EdgeType.Flat);
        p308183927.setBottomEdgeType(EdgeType.Convex);
        p308183927.setLeftEdgeType(EdgeType.Concave);
        p308183927.setRightEdgeType(EdgeType.Convex);
        p308183927.setTopExactPuzzleID(-1);
        p308183927.setBottomExactPuzzleID(2135757662);
        p308183927.setLeftExactPuzzleID(1572338253);
        p308183927.setRightExactPuzzleID(875287721);
        p308183927.getDisplayImage().loadImageFromResource(c, R.drawable.p308183927h);
        p308183927.setExactRow(0);
        p308183927.setExactColumn(6);
        p308183927.getSize().reset(140.6f, 140.6f);
        p308183927.getPositionOffset().reset(-55.5f, -55.5f);
        p308183927.setIsUseAbsolutePosition(true);
        p308183927.setIsUseAbsoluteSize(true);
        p308183927.getImage().setIsUseAbsolutePosition(true);
        p308183927.getImage().setIsUseAbsoluteSize(true);
        p308183927.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p308183927.resetPosition();
        getSpiritList().add(p308183927);
    }

    /* access modifiers changed from: package-private */
    public void c2135757662(Context c) {
        Puzzle p2135757662 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2135757662(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2135757662(editor, this);
            }
        };
        p2135757662.setID(2135757662);
        p2135757662.setName("2135757662");
        p2135757662.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2135757662);
        this.Desktop.RandomlyPlaced(p2135757662);
        p2135757662.setTopEdgeType(EdgeType.Concave);
        p2135757662.setBottomEdgeType(EdgeType.Concave);
        p2135757662.setLeftEdgeType(EdgeType.Convex);
        p2135757662.setRightEdgeType(EdgeType.Convex);
        p2135757662.setTopExactPuzzleID(308183927);
        p2135757662.setBottomExactPuzzleID(1353333595);
        p2135757662.setLeftExactPuzzleID(1941929978);
        p2135757662.setRightExactPuzzleID(733051384);
        p2135757662.getDisplayImage().loadImageFromResource(c, R.drawable.p2135757662h);
        p2135757662.setExactRow(1);
        p2135757662.setExactColumn(6);
        p2135757662.getSize().reset(170.2f, 111.0f);
        p2135757662.getPositionOffset().reset(-85.1f, -55.5f);
        p2135757662.setIsUseAbsolutePosition(true);
        p2135757662.setIsUseAbsoluteSize(true);
        p2135757662.getImage().setIsUseAbsolutePosition(true);
        p2135757662.getImage().setIsUseAbsoluteSize(true);
        p2135757662.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2135757662.resetPosition();
        getSpiritList().add(p2135757662);
    }

    /* access modifiers changed from: package-private */
    public void c1353333595(Context c) {
        Puzzle p1353333595 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1353333595(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1353333595(editor, this);
            }
        };
        p1353333595.setID(1353333595);
        p1353333595.setName("1353333595");
        p1353333595.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1353333595);
        this.Desktop.RandomlyPlaced(p1353333595);
        p1353333595.setTopEdgeType(EdgeType.Convex);
        p1353333595.setBottomEdgeType(EdgeType.Concave);
        p1353333595.setLeftEdgeType(EdgeType.Convex);
        p1353333595.setRightEdgeType(EdgeType.Concave);
        p1353333595.setTopExactPuzzleID(2135757662);
        p1353333595.setBottomExactPuzzleID(592151660);
        p1353333595.setLeftExactPuzzleID(1415976045);
        p1353333595.setRightExactPuzzleID(769169869);
        p1353333595.getDisplayImage().loadImageFromResource(c, R.drawable.p1353333595h);
        p1353333595.setExactRow(2);
        p1353333595.setExactColumn(6);
        p1353333595.getSize().reset(140.6f, 140.6f);
        p1353333595.getPositionOffset().reset(-85.1f, -85.1f);
        p1353333595.setIsUseAbsolutePosition(true);
        p1353333595.setIsUseAbsoluteSize(true);
        p1353333595.getImage().setIsUseAbsolutePosition(true);
        p1353333595.getImage().setIsUseAbsoluteSize(true);
        p1353333595.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1353333595.resetPosition();
        getSpiritList().add(p1353333595);
    }

    /* access modifiers changed from: package-private */
    public void c592151660(Context c) {
        Puzzle p592151660 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load592151660(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save592151660(editor, this);
            }
        };
        p592151660.setID(592151660);
        p592151660.setName("592151660");
        p592151660.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p592151660);
        this.Desktop.RandomlyPlaced(p592151660);
        p592151660.setTopEdgeType(EdgeType.Convex);
        p592151660.setBottomEdgeType(EdgeType.Convex);
        p592151660.setLeftEdgeType(EdgeType.Concave);
        p592151660.setRightEdgeType(EdgeType.Convex);
        p592151660.setTopExactPuzzleID(1353333595);
        p592151660.setBottomExactPuzzleID(802621906);
        p592151660.setLeftExactPuzzleID(106282070);
        p592151660.setRightExactPuzzleID(728747098);
        p592151660.getDisplayImage().loadImageFromResource(c, R.drawable.p592151660h);
        p592151660.setExactRow(3);
        p592151660.setExactColumn(6);
        p592151660.getSize().reset(140.6f, 170.2f);
        p592151660.getPositionOffset().reset(-55.5f, -85.1f);
        p592151660.setIsUseAbsolutePosition(true);
        p592151660.setIsUseAbsoluteSize(true);
        p592151660.getImage().setIsUseAbsolutePosition(true);
        p592151660.getImage().setIsUseAbsoluteSize(true);
        p592151660.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p592151660.resetPosition();
        getSpiritList().add(p592151660);
    }

    /* access modifiers changed from: package-private */
    public void c802621906(Context c) {
        Puzzle p802621906 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load802621906(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save802621906(editor, this);
            }
        };
        p802621906.setID(802621906);
        p802621906.setName("802621906");
        p802621906.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p802621906);
        this.Desktop.RandomlyPlaced(p802621906);
        p802621906.setTopEdgeType(EdgeType.Concave);
        p802621906.setBottomEdgeType(EdgeType.Flat);
        p802621906.setLeftEdgeType(EdgeType.Concave);
        p802621906.setRightEdgeType(EdgeType.Convex);
        p802621906.setTopExactPuzzleID(592151660);
        p802621906.setBottomExactPuzzleID(-1);
        p802621906.setLeftExactPuzzleID(1783474233);
        p802621906.setRightExactPuzzleID(1551699277);
        p802621906.getDisplayImage().loadImageFromResource(c, R.drawable.p802621906h);
        p802621906.setExactRow(4);
        p802621906.setExactColumn(6);
        p802621906.getSize().reset(140.6f, 111.0f);
        p802621906.getPositionOffset().reset(-55.5f, -55.5f);
        p802621906.setIsUseAbsolutePosition(true);
        p802621906.setIsUseAbsoluteSize(true);
        p802621906.getImage().setIsUseAbsolutePosition(true);
        p802621906.getImage().setIsUseAbsoluteSize(true);
        p802621906.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p802621906.resetPosition();
        getSpiritList().add(p802621906);
    }

    /* access modifiers changed from: package-private */
    public void c875287721(Context c) {
        Puzzle p875287721 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load875287721(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save875287721(editor, this);
            }
        };
        p875287721.setID(875287721);
        p875287721.setName("875287721");
        p875287721.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p875287721);
        this.Desktop.RandomlyPlaced(p875287721);
        p875287721.setTopEdgeType(EdgeType.Flat);
        p875287721.setBottomEdgeType(EdgeType.Concave);
        p875287721.setLeftEdgeType(EdgeType.Concave);
        p875287721.setRightEdgeType(EdgeType.Flat);
        p875287721.setTopExactPuzzleID(-1);
        p875287721.setBottomExactPuzzleID(733051384);
        p875287721.setLeftExactPuzzleID(308183927);
        p875287721.setRightExactPuzzleID(-1);
        p875287721.getDisplayImage().loadImageFromResource(c, R.drawable.p875287721h);
        p875287721.setExactRow(0);
        p875287721.setExactColumn(7);
        p875287721.getSize().reset(111.0f, 111.0f);
        p875287721.getPositionOffset().reset(-55.5f, -55.5f);
        p875287721.setIsUseAbsolutePosition(true);
        p875287721.setIsUseAbsoluteSize(true);
        p875287721.getImage().setIsUseAbsolutePosition(true);
        p875287721.getImage().setIsUseAbsoluteSize(true);
        p875287721.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p875287721.resetPosition();
        getSpiritList().add(p875287721);
    }

    /* access modifiers changed from: package-private */
    public void c733051384(Context c) {
        Puzzle p733051384 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load733051384(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save733051384(editor, this);
            }
        };
        p733051384.setID(733051384);
        p733051384.setName("733051384");
        p733051384.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p733051384);
        this.Desktop.RandomlyPlaced(p733051384);
        p733051384.setTopEdgeType(EdgeType.Convex);
        p733051384.setBottomEdgeType(EdgeType.Convex);
        p733051384.setLeftEdgeType(EdgeType.Concave);
        p733051384.setRightEdgeType(EdgeType.Flat);
        p733051384.setTopExactPuzzleID(875287721);
        p733051384.setBottomExactPuzzleID(769169869);
        p733051384.setLeftExactPuzzleID(2135757662);
        p733051384.setRightExactPuzzleID(-1);
        p733051384.getDisplayImage().loadImageFromResource(c, R.drawable.p733051384h);
        p733051384.setExactRow(1);
        p733051384.setExactColumn(7);
        p733051384.getSize().reset(111.0f, 170.2f);
        p733051384.getPositionOffset().reset(-55.5f, -85.1f);
        p733051384.setIsUseAbsolutePosition(true);
        p733051384.setIsUseAbsoluteSize(true);
        p733051384.getImage().setIsUseAbsolutePosition(true);
        p733051384.getImage().setIsUseAbsoluteSize(true);
        p733051384.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p733051384.resetPosition();
        getSpiritList().add(p733051384);
    }

    /* access modifiers changed from: package-private */
    public void c769169869(Context c) {
        Puzzle p769169869 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load769169869(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save769169869(editor, this);
            }
        };
        p769169869.setID(769169869);
        p769169869.setName("769169869");
        p769169869.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p769169869);
        this.Desktop.RandomlyPlaced(p769169869);
        p769169869.setTopEdgeType(EdgeType.Concave);
        p769169869.setBottomEdgeType(EdgeType.Convex);
        p769169869.setLeftEdgeType(EdgeType.Convex);
        p769169869.setRightEdgeType(EdgeType.Flat);
        p769169869.setTopExactPuzzleID(733051384);
        p769169869.setBottomExactPuzzleID(728747098);
        p769169869.setLeftExactPuzzleID(1353333595);
        p769169869.setRightExactPuzzleID(-1);
        p769169869.getDisplayImage().loadImageFromResource(c, R.drawable.p769169869h);
        p769169869.setExactRow(2);
        p769169869.setExactColumn(7);
        p769169869.getSize().reset(140.6f, 140.6f);
        p769169869.getPositionOffset().reset(-85.1f, -55.5f);
        p769169869.setIsUseAbsolutePosition(true);
        p769169869.setIsUseAbsoluteSize(true);
        p769169869.getImage().setIsUseAbsolutePosition(true);
        p769169869.getImage().setIsUseAbsoluteSize(true);
        p769169869.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p769169869.resetPosition();
        getSpiritList().add(p769169869);
    }

    /* access modifiers changed from: package-private */
    public void c728747098(Context c) {
        Puzzle p728747098 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load728747098(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save728747098(editor, this);
            }
        };
        p728747098.setID(728747098);
        p728747098.setName("728747098");
        p728747098.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p728747098);
        this.Desktop.RandomlyPlaced(p728747098);
        p728747098.setTopEdgeType(EdgeType.Concave);
        p728747098.setBottomEdgeType(EdgeType.Convex);
        p728747098.setLeftEdgeType(EdgeType.Concave);
        p728747098.setRightEdgeType(EdgeType.Flat);
        p728747098.setTopExactPuzzleID(769169869);
        p728747098.setBottomExactPuzzleID(1551699277);
        p728747098.setLeftExactPuzzleID(592151660);
        p728747098.setRightExactPuzzleID(-1);
        p728747098.getDisplayImage().loadImageFromResource(c, R.drawable.p728747098h);
        p728747098.setExactRow(3);
        p728747098.setExactColumn(7);
        p728747098.getSize().reset(111.0f, 140.6f);
        p728747098.getPositionOffset().reset(-55.5f, -55.5f);
        p728747098.setIsUseAbsolutePosition(true);
        p728747098.setIsUseAbsoluteSize(true);
        p728747098.getImage().setIsUseAbsolutePosition(true);
        p728747098.getImage().setIsUseAbsoluteSize(true);
        p728747098.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p728747098.resetPosition();
        getSpiritList().add(p728747098);
    }

    /* access modifiers changed from: package-private */
    public void c1551699277(Context c) {
        Puzzle p1551699277 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1551699277(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1551699277(editor, this);
            }
        };
        p1551699277.setID(1551699277);
        p1551699277.setName("1551699277");
        p1551699277.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1551699277);
        this.Desktop.RandomlyPlaced(p1551699277);
        p1551699277.setTopEdgeType(EdgeType.Concave);
        p1551699277.setBottomEdgeType(EdgeType.Flat);
        p1551699277.setLeftEdgeType(EdgeType.Concave);
        p1551699277.setRightEdgeType(EdgeType.Flat);
        p1551699277.setTopExactPuzzleID(728747098);
        p1551699277.setBottomExactPuzzleID(-1);
        p1551699277.setLeftExactPuzzleID(802621906);
        p1551699277.setRightExactPuzzleID(-1);
        p1551699277.getDisplayImage().loadImageFromResource(c, R.drawable.p1551699277h);
        p1551699277.setExactRow(4);
        p1551699277.setExactColumn(7);
        p1551699277.getSize().reset(111.0f, 111.0f);
        p1551699277.getPositionOffset().reset(-55.5f, -55.5f);
        p1551699277.setIsUseAbsolutePosition(true);
        p1551699277.setIsUseAbsoluteSize(true);
        p1551699277.getImage().setIsUseAbsolutePosition(true);
        p1551699277.getImage().setIsUseAbsoluteSize(true);
        p1551699277.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1551699277.resetPosition();
        getSpiritList().add(p1551699277);
    }
}
