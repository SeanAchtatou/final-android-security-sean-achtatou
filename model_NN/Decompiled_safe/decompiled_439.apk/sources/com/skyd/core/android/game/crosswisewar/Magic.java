package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.ICooling;
import com.skyd.core.game.crosswisewar.IMagic;

public abstract class Magic extends Obj implements IMagic {
    public ICooling getCooling() {
        return CoolingMaster.getCooling(getClass());
    }

    public void destroy() {
        ((Scene) getParentScene()).getSpiritList().remove(this);
    }
}
