package com.skyd.core.android.game;

public interface IGameValueDuct<TargetObjectClass, ValueClass> extends IGameReadonlyValueDuct<TargetObjectClass, ValueClass> {
    void setValueTo(TargetObjectClass targetobjectclass, ValueClass valueclass);
}
