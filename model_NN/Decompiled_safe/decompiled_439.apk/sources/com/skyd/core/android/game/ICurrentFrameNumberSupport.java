package com.skyd.core.android.game;

public interface ICurrentFrameNumberSupport {
    long getCurrentFrameNumber();
}
