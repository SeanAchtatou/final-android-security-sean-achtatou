package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.IBullet;
import com.skyd.core.game.crosswisewar.IEntity;
import com.skyd.core.game.crosswisewar.IObj;
import java.util.Iterator;

public class Bullet extends Obj implements IBullet {
    private int _Height = 0;
    private IEntity _Source = null;
    private int _Speed = 1;

    public IEntity[] getEncounterEntity() {
        Iterator<IObj> it = getParentScene().getChildrenList().iterator();
        while (it.hasNext()) {
            IObj f = it.next();
            if (f instanceof IEntity) {
                IEntity t = (IEntity) f;
                float l = t.getPositionInScene().getX() - ((float) (t.getOccupyWidth() / 2));
                float r = l + ((float) t.getOccupyWidth());
                if (!t.getNation().equals(getSource().getNation()) && getPositionInScene().getX() >= l && getPositionInScene().getX() <= r) {
                    return new IEntity[]{t};
                }
            }
        }
        return null;
    }

    public boolean move() {
        int v;
        IEntity[] t = getEncounterEntity();
        if (t == null) {
            if (getSource().getNation().getIsRighteous()) {
                v = getSpeed();
            } else {
                v = 0 - getSpeed();
            }
            getPositionInScene().setX(getPositionInScene().getX() + ((float) v));
            return true;
        }
        getSource().hurtTo(t);
        removeFromScene();
        return false;
    }

    public int getHeight() {
        return this._Height;
    }

    public void setHeight(int value) {
        this._Height = value;
    }

    public void setHeightToDefault() {
        setHeight(0);
    }

    public IEntity getSource() {
        return this._Source;
    }

    public void setSource(IEntity value) {
        this._Source = value;
    }

    public void setSourceToDefault() {
        setSource(null);
    }

    public int getSpeed() {
        return this._Speed;
    }

    public void setSpeed(int value) {
        this._Speed = value;
    }

    public void setSpeedToDefault() {
        setSpeed(1);
    }
}
