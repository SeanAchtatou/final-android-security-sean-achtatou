package com.skyd.core.android.game.crosswisewar;

import android.graphics.Canvas;
import android.graphics.Rect;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameObjectList;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.game.crosswisewar.IObj;
import com.skyd.core.game.crosswisewar.IScene;
import com.skyd.core.vector.Vector2DF;
import java.util.ArrayList;
import java.util.Iterator;

public class Scene extends GameScene implements IScene {
    private GameImageSpirit _BackgroundImage = null;
    private float _CameraOffset = 0.0f;
    ArrayList<IObj> _ChildrenList;

    public Scene() {
        getStandardDisplayAreaSize().reset(800.0f, 480.0f);
        getSpiritList().addOnChangedListener(new GameObjectList.OnChangedListener() {
            public void OnChangedEvent(Object sender, int newSize) {
                Scene.this.updateChildrenList();
            }
        });
    }

    public float getHorizonPoint() {
        return 400.0f;
    }

    public float getLeftEnterPoint() {
        return -50.0f;
    }

    public float getRightEnterPoint() {
        return (float) (getTotalWidth() + 50);
    }

    public int getTotalWidth() {
        return 2400;
    }

    public float getCameraOffset() {
        return this._CameraOffset;
    }

    public void setCameraOffset(float value) {
        float s = value;
        float screenwidth = ((float) GameMaster.getScreenWidth()) / getDisplayAreaFixScaleForSize().getX();
        if (s < 0.0f) {
            s = 0.0f;
        }
        if (((float) getTotalWidth()) - s < screenwidth) {
            s = ((float) getTotalWidth()) - screenwidth;
        }
        this._CameraOffset = s;
    }

    public void setBackgroundImage(GameImageSpirit value) {
        this._BackgroundImage = value;
        getSpiritList().add(value);
        value.setLevel(-9999.0f);
        value.getImage().getSize().reset((float) getTotalWidth(), 480.0f);
        value.addOnDrawingListener(new GameObject.OnDrawingListener() {
            public boolean OnDrawingEvent(Object sender, Canvas c, Rect drawArea) {
                c.translate(0.0f - (Scene.this.getCameraOffset() * Scene.this.getDisplayAreaFixScaleForSize().getX()), 0.0f);
                c.save();
                return true;
            }
        });
    }

    public ArrayList<IObj> getChildrenList() {
        return this._ChildrenList;
    }

    /* access modifiers changed from: package-private */
    public void updateChildrenList() {
        this._ChildrenList = null;
        this._ChildrenList = new ArrayList<>();
        Iterator<GameSpirit> it = getSpiritList().iterator();
        while (it.hasNext()) {
            GameSpirit f = it.next();
            if (f instanceof IObj) {
                this._ChildrenList.add((IObj) f);
            }
        }
    }

    public GameImageSpirit getBackgroundImage() {
        return this._BackgroundImage;
    }

    public void moveCamera(Vector2DF v) {
        setCameraOffset(getCameraOffset() + (v.getX() / getDisplayAreaFixScaleForSize().getX()));
    }

    public float getPositionInScene(float xInScreen) {
        return getCameraOffset() + (xInScreen / getDisplayAreaFixScaleForSize().getX());
    }
}
