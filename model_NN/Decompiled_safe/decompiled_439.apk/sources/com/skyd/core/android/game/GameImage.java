package com.skyd.core.android.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;

public class GameImage extends GameLeafObject implements IGameImageHolder {
    private BitmapDrawable _HitTestMaskImage = null;
    private BitmapDrawable _Image = null;

    public BitmapDrawable getImage() {
        return this._Image;
    }

    public void setImage(BitmapDrawable value) {
        this._Image = value;
        getSize().reset(getWidth(), getHeight());
    }

    public void setImageToDefault() {
        setImage(null);
    }

    public BitmapDrawable getHitTestMaskImage() {
        if (this._HitTestMaskImage == null) {
            return getImage();
        }
        return this._HitTestMaskImage;
    }

    public void setHitTestMaskImage(BitmapDrawable value) {
        this._HitTestMaskImage = value;
    }

    public void setHitTestMaskImageToDefault() {
        setHitTestMaskImage(null);
    }

    public GameImage() {
    }

    public GameImage(BitmapDrawable bitmapDrawable) {
        setImage(bitmapDrawable);
    }

    public GameImage(Context c, int resourceID) {
        loadImageFromResource(c, resourceID);
    }

    public GameImage(Context c, int resourceID, Bitmap.Config cfg) {
        loadImageFromResource(c, resourceID, cfg);
    }

    public void loadImageFromResource(Context c, int resourceID) {
        setImage(new BitmapDrawable(DrawHelper.loadBitmap(c, resourceID)));
    }

    public void loadImageFromResource(Context c, int resourceID, Bitmap.Config cfg) {
        setImage(new BitmapDrawable(DrawHelper.loadBitmap(c, resourceID, cfg)));
    }

    public void loadHitTestMaskImageFromResource(Context c, int resourceID) {
        setHitTestMaskImage(new BitmapDrawable(DrawHelper.loadBitmap(c, resourceID)));
    }

    public void loadHitTestMaskImageFromResource(Context c, int resourceID, Bitmap.Config cfg) {
        setHitTestMaskImage(new BitmapDrawable(DrawHelper.loadBitmap(c, resourceID, cfg)));
    }

    public float getHeight() {
        return (float) this._Image.getBitmap().getHeight();
    }

    public float getWidth() {
        return (float) this._Image.getBitmap().getWidth();
    }

    /* access modifiers changed from: protected */
    public void drawSelf(Canvas c, Rect drawArea) {
        BitmapDrawable img = getImage();
        try {
            synchronized (img) {
                img.setAlpha((int) (getDisplayOpacity() * 255.0f));
                img.setAntiAlias(true);
                Vector2DF v = getDrawSize();
                img.setBounds(0, 0, v.getIntX(), v.getIntY());
                img.draw(c);
            }
        } catch (Exception e) {
            GameMaster.log(this, "绘图失败，图像对象可能为空");
        }
    }

    /* access modifiers changed from: protected */
    public void updateSelf() {
    }

    public GameImage getDisplayImage() {
        return this;
    }

    public int[] getImagePixels(VectorRect2DF rect) {
        return getImagePixels(rect.getRect());
    }

    public int[] getImagePixels(Rect rect) {
        int[] a = new int[(rect.width() * rect.height())];
        getImage().getBitmap().getPixels(a, 0, rect.width(), rect.left, rect.top, rect.width(), rect.height());
        return a;
    }

    public int[] getImagePixels() {
        int w = getImage().getBitmap().getWidth();
        int h = getImage().getBitmap().getHeight();
        int[] a = new int[(w * h)];
        getImage().getBitmap().getPixels(a, 0, w, 0, 0, w, h);
        return a;
    }

    public int[] getHitTestMaskImagePixels(VectorRect2DF rect) {
        return getHitTestMaskImagePixels(rect.getRect());
    }

    public int[] getHitTestMaskImagePixels(Rect rect) {
        int[] a = new int[(rect.width() * rect.height())];
        getHitTestMaskImage().getBitmap().getPixels(a, 0, rect.width(), rect.left, rect.top, rect.width(), rect.height());
        return a;
    }

    public int[] getHitTestMaskImagePixels() {
        int w = getHitTestMaskImage().getBitmap().getWidth();
        int h = getHitTestMaskImage().getBitmap().getHeight();
        int[] a = new int[(w * h)];
        getHitTestMaskImage().getBitmap().getPixels(a, 0, w, 0, 0, w, h);
        return a;
    }

    public void releaseImage() {
        if (getImage() != null) {
            this._Image.getBitmap().recycle();
            this._Image = null;
        }
    }
}
