package com.skyd.core.android.game;

public class GameAnimationSpirit extends GamePixelImageSpirit implements IGameImageHolder {
    private GameAnimation _Animation;

    public GameAnimationSpirit() {
        this._Animation = null;
        this._Animation = new GameAnimation();
        try {
            this._Animation.setParent(this);
        } catch (GameException e) {
            e.printStackTrace();
        }
    }

    public GameObject getDisplayContentChild() {
        return getAnimation();
    }

    public GameAnimation getAnimation() {
        return this._Animation;
    }

    /* access modifiers changed from: protected */
    public void setAnimation(GameAnimation value) {
        this._Animation = value;
    }

    /* access modifiers changed from: protected */
    public void setAnimationToDefault() {
        setAnimation(null);
    }

    public GameImage getDisplayImage() {
        return getAnimation().getDisplayImage();
    }
}
