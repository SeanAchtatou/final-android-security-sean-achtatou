package com.skyd.core.game.crosswisewar;

import com.skyd.core.vector.Vector2DF;

public interface IObj {
    IEntity[] getEncounterEntity();

    INation getNation();

    IScene getParentScene();

    Vector2DF getPositionInScene();

    void setNation(INation iNation);

    void setParentScene(IScene iScene);

    void setPositionInScene(Vector2DF vector2DF);

    void update();
}
