package com.skyd.core.game.crosswisewar;

import com.skyd.core.vector.Vector2DF;
import java.util.ArrayList;

public interface IScene {
    float getCameraOffset();

    ArrayList<IObj> getChildrenList();

    float getHorizonPoint();

    float getLeftEnterPoint();

    float getPositionInScene(float f);

    float getRightEnterPoint();

    int getTotalWidth();

    void moveCamera(Vector2DF vector2DF);
}
