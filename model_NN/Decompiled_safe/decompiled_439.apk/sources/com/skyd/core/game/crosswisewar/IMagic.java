package com.skyd.core.game.crosswisewar;

public interface IMagic extends IObj, IValuable {
    ICooling getCooling();

    void release();
}
