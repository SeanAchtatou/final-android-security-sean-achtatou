package com.skyd.core.game.crosswisewar;

public interface INation {
    IBase getBase();

    boolean getIsRighteous();

    int getMana();

    int getMoney();
}
