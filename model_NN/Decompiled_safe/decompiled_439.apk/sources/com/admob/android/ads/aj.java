package com.admob.android.ads;

import android.graphics.Color;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: BackgroundColorAnimation */
public final class aj extends Animation {
    private int[] a = new int[4];
    private int[] b = new int[4];
    private View c;

    public aj(int i, int i2, View view) {
        this.c = view;
        this.a[0] = Color.alpha(i);
        this.a[1] = Color.red(i);
        this.a[2] = Color.green(i);
        this.a[3] = Color.blue(i);
        this.b[0] = Color.alpha(i2);
        this.b[1] = Color.red(i2);
        this.b[2] = Color.green(i2);
        this.b[3] = Color.blue(i2);
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        transformation.setTransformationType(Transformation.TYPE_IDENTITY);
        if (((double) f) >= 0.0d && ((double) f) <= 1.0d) {
            int[] iArr = new int[4];
            for (int i = 0; i < 4; i++) {
                iArr[i] = (int) (((float) this.a[i]) + (((float) (this.b[i] - this.a[i])) * f));
            }
            this.c.setBackgroundColor(Color.argb(iArr[0], iArr[1], iArr[2], iArr[3]));
        }
    }
}
