package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseListItem {
    private final Context _context;
    private Drawable _drawable;
    private final LayoutInflater _layoutInflater = ((LayoutInflater) this._context.getSystemService("layout_inflater"));
    private String _title;

    public abstract int getType();

    public abstract View getView(View view, ViewGroup viewGroup);

    public abstract boolean isEnabled();

    public BaseListItem(Context context, Drawable drawable, String title) {
        this._context = context;
        this._drawable = drawable;
        this._title = title;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this._context;
    }

    public Drawable getDrawable() {
        return this._drawable;
    }

    /* access modifiers changed from: protected */
    public LayoutInflater getLayoutInflater() {
        return this._layoutInflater;
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public String getTitle() {
        return this._title;
    }

    public void setDrawable(Drawable drawable) {
        this._drawable = drawable;
    }
}
