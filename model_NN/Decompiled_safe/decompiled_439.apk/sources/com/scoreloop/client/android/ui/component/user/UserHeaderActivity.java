package com.scoreloop.client.android.ui.component.user;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.agent.ManageBuddiesTask;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.scoreloop.client.android.ui.util.ImageDownloader;
import com.skyd.bestpuzzle.n1623.R;
import java.util.List;

public class UserHeaderActivity extends ComponentHeaderActivity implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserHeaderActivity$ControlMode = null;
    private static final int MENU_FLAG_INAPPROPRIATE = 296;
    private static final int MENU_REMOVE_BUDDY = 256;
    private boolean _canRemoveBuddy;
    private MessageController _messageController;
    private ControlMode _mode;

    public enum ControlMode {
        BLANK,
        BUDDY,
        PROFILE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserHeaderActivity$ControlMode() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserHeaderActivity$ControlMode;
        if (iArr == null) {
            iArr = new int[ControlMode.values().length];
            try {
                iArr[ControlMode.BLANK.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ControlMode.BUDDY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ControlMode.PROFILE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserHeaderActivity$ControlMode = iArr;
        }
        return iArr;
    }

    private void disableAddRemoveBuddiesControl() {
        hideControlIcon();
        this._canRemoveBuddy = false;
    }

    private String formatInteger(ValueStore store, String key) {
        Integer value = (Integer) store.getValue(key);
        return value != null ? value.toString() : "";
    }

    private ImageView hideControlIcon() {
        ImageView icon = (ImageView) findViewById(R.id.sl_control_icon);
        icon.setImageDrawable(null);
        icon.setOnClickListener(null);
        icon.setEnabled(false);
        View view = findViewById(R.id.sl_header_layout);
        view.setEnabled(true);
        view.setOnClickListener(null);
        return icon;
    }

    public void onClick(View view) {
        final User user = getUser();
        if (this._mode == ControlMode.PROFILE) {
            getTracker().trackEvent(TrackerEvents.CAT_NAVI, TrackerEvents.NAVI_HEADER_ACCOUNTSETTINGS, null, 0);
            display(getFactory().createProfileSettingsScreenDescription(user));
        } else if (this._mode == ControlMode.BUDDY) {
            disableAddRemoveBuddiesControl();
            ManageBuddiesTask.addBuddy(this, user, getSessionUserValues(), new ManageBuddiesTask.ManageBuddiesContinuation() {
                public void withAddedOrRemovedBuddies(int count) {
                    if (!UserHeaderActivity.this.isPaused()) {
                        UserHeaderActivity.this.showToast(String.format(UserHeaderActivity.this.getString(R.string.sl_format_added_as_friend), user.getDisplayName()));
                    }
                }
            });
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.sl_header_user);
        addObservedKeys(ValueStore.concatenateKeys(Constant.SESSION_USER_VALUES, Constant.USER_BUDDIES), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.USER_NAME), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.USER_IMAGE_URL), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_GAMES), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_BUDDIES), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_GLOBAL_ACHIEVEMENTS));
        this._mode = (ControlMode) getActivityArguments().getValue("mode", ControlMode.BLANK);
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserHeaderActivity$ControlMode()[this._mode.ordinal()]) {
            case 3:
                showControlIcon(R.drawable.sl_button_account_settings, true);
                break;
            default:
                disableAddRemoveBuddiesControl();
                break;
        }
        updateUI();
    }

    public boolean onCreateOptionsMenuForActivityGroup(Menu menu) {
        super.onCreateOptionsMenuForActivityGroup(menu);
        menu.add(0, (int) MENU_REMOVE_BUDDY, 0, (int) R.string.sl_remove_friend).setIcon((int) R.drawable.sl_icon_remove_friend);
        if (isSessionUser()) {
            return true;
        }
        menu.add(0, (int) MENU_FLAG_INAPPROPRIATE, 0, (int) R.string.sl_abuse_report_title).setIcon((int) R.drawable.sl_icon_flag_inappropriate);
        return true;
    }

    public boolean onOptionsItemSelectedForActivityGroup(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_REMOVE_BUDDY /*256*/:
                disableAddRemoveBuddiesControl();
                final User user = getUser();
                ManageBuddiesTask.removeBuddy(this, user, getSessionUserValues(), new ManageBuddiesTask.ManageBuddiesContinuation() {
                    public void withAddedOrRemovedBuddies(int count) {
                        if (!UserHeaderActivity.this.isPaused()) {
                            UserHeaderActivity.this.showToast(String.format(UserHeaderActivity.this.getString(R.string.sl_format_removed_as_friend), user.getDisplayName()));
                        }
                    }
                });
                getTracker().trackEvent(TrackerEvents.CAT_NAVI, TrackerEvents.NAVI_OM_USER_REMOVE, null, 0);
                return true;
            case MENU_FLAG_INAPPROPRIATE /*296*/:
                postAbuseReport();
                getTracker().trackEvent(TrackerEvents.CAT_NAVI, TrackerEvents.NAVI_OM_USER_INAPPROPRIATE, null, 0);
                return true;
            default:
                return super.onOptionsItemSelectedForActivityGroup(item);
        }
    }

    public boolean onPrepareOptionsMenuForActivityGroup(Menu menu) {
        MenuItem item = menu.findItem(MENU_REMOVE_BUDDY);
        if (item != null) {
            item.setVisible(this._canRemoveBuddy);
        }
        return super.onPrepareOptionsMenuForActivityGroup(menu);
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        updateUI();
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (Constant.USER_NAME.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.USER_IMAGE_URL.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.NUMBER_GAMES.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.NUMBER_BUDDIES.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (Constant.NUMBER_GLOBAL_ACHIEVEMENTS.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        } else if (this._mode == ControlMode.BUDDY && Constant.USER_BUDDIES.equals(key)) {
            getSessionUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }

    private void postAbuseReport() {
        this._messageController = new MessageController(getRequestControllerObserver());
        this._messageController.setTarget(getUser());
        this._messageController.setMessageType(MessageController.TYPE_ABUSE_REPORT);
        this._messageController.setText("Inappropriate user in ScoreloopUI");
        this._messageController.addReceiverWithUsers(MessageController.RECEIVER_SYSTEM, new Object[0]);
        if (this._messageController.isSubmitAllowed()) {
            this._messageController.submitMessage();
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController requestController) {
        if (requestController == this._messageController) {
            showToast(getString(R.string.sl_abuse_report_sent));
        }
    }

    private void setNumberBuddies(String text) {
        ((TextView) findViewById(R.id.sl_header_number_friends)).setText(text);
    }

    private void setNumberGames(String text) {
        ((TextView) findViewById(R.id.sl_header_number_games)).setText(text);
    }

    private void setNumberGlobalAchievements(String text) {
        ((TextView) findViewById(R.id.sl_header_number_achievements)).setText(text);
    }

    private void showControlIcon(int resId, boolean isHeaderClickable) {
        ImageView icon = hideControlIcon();
        icon.setImageResource(resId);
        icon.setEnabled(true);
        icon.setOnClickListener(this);
        if (isHeaderClickable) {
            View view = findViewById(R.id.sl_header_layout);
            view.setEnabled(true);
            view.setOnClickListener(this);
        }
    }

    private void updateAddRemoveBuddiesControl() {
        List<User> buddyUsers;
        if (this._mode != ControlMode.BUDDY || (buddyUsers = (List) getSessionUserValues().getValue(Constant.USER_BUDDIES)) == null) {
            return;
        }
        if (getUser() == getSessionUser() || buddyUsers.contains(getUser())) {
            hideControlIcon();
            this._canRemoveBuddy = true;
            return;
        }
        showControlIcon(R.drawable.sl_button_add_friend, false);
        this._canRemoveBuddy = false;
    }

    /* access modifiers changed from: protected */
    public Drawable getDrawableLoading() {
        return getResources().getDrawable(R.drawable.sl_icon_games_loading);
    }

    private void updateUI() {
        ValueStore store = getUserValues();
        String imageUrl = (String) store.getValue(Constant.USER_IMAGE_URL);
        if (imageUrl == null) {
            imageUrl = ((User) store.getValue(Constant.USER)).getImageUrl();
        }
        if (imageUrl != null) {
            ImageDownloader.downloadImage(imageUrl, getDrawableLoading(), getImageView(), null);
        } else if (getUser().getIdentifier() == null) {
            getImageView().setImageDrawable(null);
        } else {
            getImageView().setImageResource(R.drawable.sl_header_icon_user);
        }
        setTitle((String) store.getValue(Constant.USER_NAME));
        setNumberGames(formatInteger(store, Constant.NUMBER_GAMES));
        setNumberBuddies(formatInteger(store, Constant.NUMBER_BUDDIES));
        setNumberGlobalAchievements(formatInteger(store, Constant.NUMBER_GLOBAL_ACHIEVEMENTS));
        updateAddRemoveBuddiesControl();
    }
}
