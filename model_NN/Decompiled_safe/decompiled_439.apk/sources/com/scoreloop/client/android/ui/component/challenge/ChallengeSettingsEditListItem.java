package com.scoreloop.client.android.ui.component.challenge;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.skyd.bestpuzzle.n1623.R;
import java.util.List;

class ChallengeSettingsEditListItem extends ChallengeSettingsListItem {
    private static List<Money> stakes = Session.getCurrentSession().getChallengeStakes();
    /* access modifiers changed from: private */
    public int _modePosition;
    /* access modifiers changed from: private */
    public int _stakePosition;

    private class ViewHolder {
        TextView stakeText;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ChallengeSettingsEditListItem challengeSettingsEditListItem, ViewHolder viewHolder) {
            this();
        }
    }

    public ChallengeSettingsEditListItem(ComponentActivity context) {
        super(context, null);
    }

    /* access modifiers changed from: package-private */
    public Integer getMode() {
        if (getComponentActivity().getGame().hasModes()) {
            return Integer.valueOf(getComponentActivity().getModeForPosition(this._modePosition));
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Money getStake() {
        Money balance = (Money) getComponentActivity().getUserValues().getValue(Constant.USER_BALANCE);
        if (balance == null) {
            return null;
        }
        Money stake = stakes.get(this._stakePosition);
        if (stake.compareTo(balance) <= 0) {
            return stake;
        }
        return null;
    }

    public int getType() {
        return 9;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_challenge_settings_edit, (ViewGroup) null);
        }
        prepareView(view);
        return view;
    }

    public boolean isEnabled() {
        return false;
    }

    private void prepareModeSelector(View view) {
        Spinner modeSelector = (Spinner) view.findViewById(R.id.mode_selector);
        if (getComponentActivity().getGame().hasModes()) {
            modeSelector.setVisibility(0);
            ArrayAdapter<?> adapter = ArrayAdapter.createFromResource(getContext(), getComponentActivity().getConfiguration().getModesResId(), R.layout.sl_spinner_item);
            adapter.setDropDownViewResource(17367049);
            modeSelector.setAdapter((SpinnerAdapter) adapter);
            modeSelector.setSelection(this._modePosition);
            modeSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    ChallengeSettingsEditListItem.this._modePosition = position;
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
            return;
        }
        modeSelector.setVisibility(8);
    }

    private void prepareStakeSelector(View view) {
        SeekBar stakeSelector = (SeekBar) view.findViewById(R.id.stake_selector);
        stakeSelector.setMax(stakes.size() - 1);
        stakeSelector.setProgress(this._stakePosition);
        stakeSelector.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ChallengeSettingsEditListItem.this._stakePosition = progress;
                ChallengeSettingsEditListItem.this.updateStakeText(((ViewHolder) seekBar.getTag()).stakeText);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        ViewHolder holder = new ViewHolder(this, null);
        holder.stakeText = (TextView) view.findViewById(R.id.stake_text);
        stakeSelector.setTag(holder);
    }

    private void prepareStakeText(View view) {
        updateStakeText((TextView) view.findViewById(R.id.stake_text));
    }

    /* access modifiers changed from: protected */
    public void prepareView(View view) {
        prepareStakeText(view);
        prepareModeSelector(view);
        prepareStakeSelector(view);
    }

    /* access modifiers changed from: private */
    public void updateStakeText(TextView stakeText) {
        String string;
        Money stake = getStake();
        if (stake != null) {
            string = StringFormatter.formatMoney(stake, getComponentActivity().getConfiguration());
        } else {
            string = getContext().getResources().getString(R.string.sl_balance_too_low);
        }
        stakeText.setText(string);
    }
}
