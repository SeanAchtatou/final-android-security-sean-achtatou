package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Range;
import java.util.Collections;
import java.util.List;

class a<T> {
    private boolean a = true;
    private boolean b = false;
    private Range c = null;
    private List<T> d = Collections.emptyList();
    private int e = 25;

    a() {
    }

    private void b(List<Game> list) {
        this.d = Collections.unmodifiableList(list);
    }

    private void k() {
        if (!g()) {
            throw new IllegalStateException("_lastRequestedRange must not be null");
        }
    }

    public int a() {
        k();
        return this.c.getLength() + 1;
    }

    public void a(int i) {
        this.c = new Range(i, this.e);
    }

    public void a(List<Game> list) {
        k();
        this.b = this.c.getLocation() > 0;
        this.a = list.size() > this.c.getLength();
        while (list.size() > this.c.getLength()) {
            list.remove(list.size() - 1);
        }
        b(list);
    }

    public int b() {
        k();
        return this.c.getLocation();
    }

    public void b(int i) {
        if (i < 1 || i > 100) {
            throw new IllegalArgumentException("invalid range length");
        }
        this.e = i;
    }

    public int c() {
        k();
        return this.c.getLocation() + this.d.size();
    }

    public void c(int i) {
        k();
        this.c.a(i);
    }

    public List<T> d() {
        return this.d;
    }

    public int e() {
        k();
        return Math.max(0, this.c.getLocation() - this.e);
    }

    public int f() {
        return this.e;
    }

    public boolean g() {
        return this.c != null;
    }

    public boolean h() {
        return this.a;
    }

    public boolean i() {
        return this.b;
    }

    public void j() {
        this.a = true;
        this.b = false;
        this.c = null;
        this.d = Collections.emptyList();
    }
}
