package com.scoreloop.client.android.ui;

import android.content.Context;

public class ScoreloopManagerSingleton {
    private static ScoreloopManager _singleton;

    public static ScoreloopManager get() {
        if (_singleton != null) {
            return _singleton;
        }
        throw new IllegalStateException("ScoreloopManagerSingleton.init() must be called first");
    }

    public static ScoreloopManager init(Context context) {
        if (_singleton != null) {
            throw new IllegalStateException("ScoreloopManagerSingleton.init() can be called only once");
        }
        _singleton = new StandardScoreloopManager(context);
        return _singleton;
    }

    public static ScoreloopManager init(ScoreloopManager manager) {
        if (_singleton != null) {
            throw new IllegalStateException("ScoreloopManagerSingleton.init() can be called only once");
        }
        _singleton = manager;
        return _singleton;
    }

    public static void destroy() {
        if (_singleton != null) {
            _singleton.destroy();
        }
        _singleton = null;
    }
}
