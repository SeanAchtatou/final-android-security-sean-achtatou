package com.scoreloop.client.android.ui.component.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.OkCancelDialog;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1623.R;
import java.util.regex.Pattern;

public class ProfileSettingsListActivity extends ComponentListActivity<BaseListItem> implements RequestControllerObserver, DialogInterface.OnDismissListener {
    private static final String STATE_ERRORMESSAGE = "errorMessage";
    private static final String STATE_ERRORTITLE = "errorTitle";
    private static final String STATE_HINT = "hint";
    private static final String STATE_LASTREQUESTTYPE = "lastRequestType";
    private static final String STATE_LASTUPDATEERROR = "lastUpdateError";
    private static final String STATE_RESTOREEMAIL = "restoreEmail";
    /* access modifiers changed from: private */
    public ProfileListItem _changeEmailItem;
    /* access modifiers changed from: private */
    public ProfileListItem _changePictureItem;
    /* access modifiers changed from: private */
    public ProfileListItem _changeUsernameItem;
    private String _errorMessage;
    private String _errorTitle;
    /* access modifiers changed from: private */
    public String _hint;
    private RequestType _lastRequestType;
    private boolean _lastUpdateError = true;
    /* access modifiers changed from: private */
    public ProfileListItem _mergeAccountItem;
    private String _restoreEmail;
    /* access modifiers changed from: private */
    public UserController _userController;

    class UserProfileListAdapter extends BaseListAdapter<BaseListItem> {
        public UserProfileListAdapter(Context context) {
            super(context);
            add(new CaptionListItem(context, null, ProfileSettingsListActivity.this.getString(R.string.sl_account_settings)));
            add(ProfileSettingsListActivity.this._changePictureItem);
            add(ProfileSettingsListActivity.this._changeUsernameItem);
            if (ProfileSettingsListActivity.this.getSessionUser().getEmailAddress() != null) {
                add(ProfileSettingsListActivity.this._changeEmailItem);
            }
            add(ProfileSettingsListActivity.this._mergeAccountItem);
        }
    }

    enum RequestType {
        USERNAME(12),
        EMAIL(13),
        USERNAME_EMAIL(14),
        MERGE_ACCOUNTS(17);
        
        private final int dialogId;

        private RequestType(int dialogId2) {
            this.dialogId = dialogId2;
        }

        public int getDialogId() {
            return this.dialogId;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_RESTOREEMAIL, this._restoreEmail);
        outState.putString(STATE_ERRORTITLE, this._errorTitle);
        outState.putString(STATE_ERRORMESSAGE, this._errorMessage);
        outState.putString(STATE_HINT, this._hint);
        if (this._lastRequestType != null) {
            outState.putString(STATE_LASTREQUESTTYPE, this._lastRequestType.toString());
        }
        outState.putBoolean(STATE_LASTUPDATEERROR, this._lastUpdateError);
    }

    private void saveUserState() {
        this._restoreEmail = getSessionUser().getEmailAddress();
    }

    private void restoreUserState() {
        getSessionUser().setEmailAddress(this._restoreEmail);
    }

    public void setLastRequestType(RequestType lastRequestType) {
        this._lastRequestType = lastRequestType;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 12:
                return getChangeUsernameDialog();
            case 13:
                return getChangeEmailDialog();
            case 14:
                return getFirstTimeDialog();
            case 15:
                return getMsgDialog();
            case 16:
            default:
                return super.onCreateDialog(id);
            case 17:
                return getMergeAccountDialog();
        }
    }

    private Dialog getMsgDialog() {
        ErrorDialog dialog = new ErrorDialog(this);
        dialog.setOnDismissListener(this);
        return dialog;
    }

    private Dialog getChangeEmailDialog() {
        FieldEditDialog dialog = new FieldEditDialog(this, getString(R.string.sl_change_email), getString(R.string.sl_current), getString(R.string.sl_new), null);
        dialog.setOnActionListener(new BaseDialog.OnActionListener() {
            public void onAction(BaseDialog dialog, int actionId) {
                if (actionId == 0) {
                    FieldEditDialog dlg = (FieldEditDialog) dialog;
                    String newEmail = dlg.getEditText().trim();
                    if (!ProfileSettingsListActivity.this.isValidEmailFormat(newEmail)) {
                        ProfileSettingsListActivity.this._hint = ProfileSettingsListActivity.this.getString(R.string.sl_please_email_valid);
                        dlg.setHint(ProfileSettingsListActivity.this._hint);
                        return;
                    }
                    ProfileSettingsListActivity.this.updateUser(newEmail, null, RequestType.EMAIL);
                }
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(this);
        return dialog;
    }

    private Dialog getMergeAccountDialog() {
        FieldEditDialog dialog = new FieldEditDialog(this, getString(R.string.sl_merge_account_title), null, null, getString(R.string.sl_merge_account_description));
        dialog.setOnActionListener(new BaseDialog.OnActionListener() {
            public void onAction(BaseDialog dialog, int actionId) {
                if (actionId == 0) {
                    FieldEditDialog dlg = (FieldEditDialog) dialog;
                    String newEmail = dlg.getEditText().trim();
                    if (!ProfileSettingsListActivity.this.isValidEmailFormat(newEmail)) {
                        ProfileSettingsListActivity.this._hint = ProfileSettingsListActivity.this.getString(R.string.sl_please_email_valid);
                        dlg.setHint(ProfileSettingsListActivity.this._hint);
                        return;
                    } else if (ProfileSettingsListActivity.this.getSessionUser().getEmailAddress() == null || !ProfileSettingsListActivity.this.getSessionUser().getEmailAddress().equalsIgnoreCase(newEmail)) {
                        ProfileSettingsListActivity.this.updateUser(newEmail, null, RequestType.MERGE_ACCOUNTS);
                    } else {
                        ProfileSettingsListActivity.this._hint = ProfileSettingsListActivity.this.getString(R.string.sl_merge_account_email_current);
                        dlg.setHint(ProfileSettingsListActivity.this._hint);
                        return;
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(this);
        return dialog;
    }

    private Dialog getChangeUsernameDialog() {
        FieldEditDialog dialog = new FieldEditDialog(this, getString(R.string.sl_change_username), getString(R.string.sl_current), getString(R.string.sl_new), null);
        dialog.setOnActionListener(new BaseDialog.OnActionListener() {
            public void onAction(BaseDialog dialog, int actionId) {
                dialog.dismiss();
                if (actionId == 0) {
                    ProfileSettingsListActivity.this.updateUser(null, ((FieldEditDialog) dialog).getEditText().trim(), RequestType.USERNAME);
                }
            }
        });
        dialog.setOnDismissListener(this);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 12:
                FieldEditDialog changeUsernameDialog = (FieldEditDialog) dialog;
                changeUsernameDialog.setCurrentText(getSessionUser().getLogin());
                changeUsernameDialog.setHint(this._hint);
                if (!this._lastUpdateError || !RequestType.USERNAME.equals(this._lastRequestType)) {
                    changeUsernameDialog.setEditText(null);
                    break;
                }
            case 13:
                FieldEditDialog changeEmailDialog = (FieldEditDialog) dialog;
                changeEmailDialog.setHint(this._hint);
                changeEmailDialog.setCurrentText(getSessionUser().getEmailAddress());
                if (!this._lastUpdateError || !RequestType.EMAIL.equals(this._lastRequestType)) {
                    changeEmailDialog.setEditText(null);
                    break;
                }
            case 14:
                ((FirstTimeDialog) dialog).setHint(this._hint);
                break;
            case 15:
            case 16:
            default:
                super.onPrepareDialog(id, dialog);
                break;
            case 17:
                FieldEditDialog mergeAccountsDialog = (FieldEditDialog) dialog;
                mergeAccountsDialog.setHint(this._hint);
                if (!this._lastUpdateError || !RequestType.MERGE_ACCOUNTS.equals(this._lastRequestType)) {
                    mergeAccountsDialog.setEditText(null);
                    break;
                }
        }
        if (dialog instanceof ErrorDialog) {
            ErrorDialog errorDialog = (ErrorDialog) dialog;
            errorDialog.setText(this._errorMessage);
            errorDialog.setTitle(this._errorTitle);
        } else if (dialog instanceof OkCancelDialog) {
            OkCancelDialog okCancelDialog = (OkCancelDialog) dialog;
            okCancelDialog.setText(this._errorMessage);
            okCancelDialog.setTitle(this._errorTitle);
        }
    }

    private Dialog getFirstTimeDialog() {
        FirstTimeDialog dialog = new FirstTimeDialog(this, getSessionUser().getLogin());
        dialog.setOnActionListener(new BaseDialog.OnActionListener() {
            public void onAction(BaseDialog dialog, int actionId) {
                FirstTimeDialog dlg = (FirstTimeDialog) dialog;
                if (actionId == 0) {
                    String newEmail = dlg.getEmailText().trim();
                    String newUsername = dlg.getUsernameText().trim();
                    if (!ProfileSettingsListActivity.this.isValidEmailFormat(newEmail)) {
                        dlg.setHint(ProfileSettingsListActivity.this.getString(R.string.sl_please_email_address));
                        return;
                    }
                    ProfileSettingsListActivity.this.updateUser(newEmail, newUsername, RequestType.USERNAME_EMAIL);
                }
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(this);
        return dialog;
    }

    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this._restoreEmail = savedInstanceState.getString(STATE_RESTOREEMAIL);
            this._errorTitle = savedInstanceState.getString(STATE_ERRORTITLE);
            this._errorMessage = savedInstanceState.getString(STATE_ERRORMESSAGE);
            this._hint = savedInstanceState.getString(STATE_HINT);
            if (savedInstanceState.containsKey(STATE_LASTREQUESTTYPE)) {
                this._lastRequestType = RequestType.valueOf(savedInstanceState.getString(STATE_LASTREQUESTTYPE));
            }
            if (savedInstanceState.containsKey(STATE_LASTUPDATEERROR)) {
                this._lastUpdateError = savedInstanceState.getBoolean(STATE_LASTUPDATEERROR);
            }
        }
        super.onCreate(savedInstanceState);
        User user = getSessionUser();
        this._userController = new UserController(this);
        this._changePictureItem = new ProfileListItem(this, getResources().getDrawable(R.drawable.sl_icon_change_picture), getString(R.string.sl_change_picture), getString(R.string.sl_change_picture_details));
        this._changeUsernameItem = new ProfileListItem(this, getResources().getDrawable(R.drawable.sl_icon_change_username), getString(R.string.sl_change_username), user.getLogin());
        this._changeEmailItem = new ProfileListItem(this, getResources().getDrawable(R.drawable.sl_icon_change_email), getString(R.string.sl_change_email), user.getEmailAddress());
        this._mergeAccountItem = new ProfileListItem(this, getResources().getDrawable(R.drawable.sl_icon_merge_account), getString(R.string.sl_merge_account_title), getString(R.string.sl_merge_account_subtitle));
        if (user.getLogin() == null || user.getEmailAddress() == null) {
            showSpinnerFor(this._userController);
            this._userController.loadUser();
        } else {
            setListAdapter(new UserProfileListAdapter(this));
        }
        setVisibleOptionsMenuAccountSettings(false);
    }

    public void onListItemClick(BaseListItem item) {
        this._hint = null;
        if (item == this._changeUsernameItem) {
            if (getSessionUser().getEmailAddress() == null) {
                showDialogSafe(14, true);
            } else {
                showDialogSafe(12, true);
            }
        } else if (item == this._changePictureItem) {
            display(getFactory().createProfileSettingsPictureScreenDescription(getSessionUser()));
        } else if (item == this._changeEmailItem) {
            showDialogSafe(13, true);
        } else if (item == this._mergeAccountItem) {
            showDialogSafe(17, true);
        }
    }

    public void onRefresh(int flags) {
        this._changeEmailItem.setSubTitle(getSessionUser().getEmailAddress());
        this._changeUsernameItem.setSubTitle(getSessionUser().getLogin());
        if (getBaseListAdapter() != null) {
            getBaseListAdapter().notifyDataSetChanged();
        }
        getManager().persistSessionUserName();
    }

    /* access modifiers changed from: protected */
    public void requestControllerDidFailSafe(RequestController requestController, Exception exception) {
        hideSpinnerFor(requestController);
        if (!(exception instanceof RequestControllerException) || !((RequestControllerException) exception).hasDetail(16) || !RequestType.MERGE_ACCOUNTS.equals(this._lastRequestType)) {
            this._lastUpdateError = true;
            int errorCode = 0;
            if (exception instanceof RequestControllerException) {
                RequestControllerException requestException = (RequestControllerException) exception;
                if (requestException.hasDetail(16)) {
                    this._errorTitle = getString(R.string.sl_error_message_email_already_taken_title);
                    this._errorMessage = getString(R.string.sl_error_message_email_already_taken);
                    showDialogSafe(15, true);
                    errorCode = 16;
                } else if (requestException.hasDetail(8)) {
                    this._hint = getString(R.string.sl_error_message_invalid_email);
                    errorCode = 8;
                    showDialogSafe(this._lastRequestType.getDialogId(), true);
                } else if ((requestException.hasDetail(1) | requestException.hasDetail(4)) || requestException.hasDetail(2)) {
                    this._hint = getString(R.string.sl_error_message_username_already_taken);
                    errorCode = 4;
                    showDialogSafe(this._lastRequestType.getDialogId(), true);
                } else {
                    super.requestControllerDidFailSafe(requestController, exception);
                }
            } else {
                super.requestControllerDidFailSafe(requestController, exception);
            }
            getTracker().trackEvent(TrackerEvents.CAT_REQUEST, getActionSettings(this._lastRequestType), TrackerEvents.LABEL_ERROR, errorCode);
        } else {
            this._errorTitle = getString(R.string.sl_merge_account_success_title);
            this._errorMessage = getString(R.string.sl_merge_account_success);
            showDialogSafe(15, true);
            this._lastUpdateError = false;
            getTracker().trackEvent(TrackerEvents.CAT_REQUEST, getActionSettings(this._lastRequestType), TrackerEvents.LABEL_SUCCESS, 0);
        }
        restoreUserState();
    }

    private String getActionSettings(RequestType requestType) {
        if (RequestType.EMAIL.equals(requestType)) {
            return TrackerEvents.REQ_CHANGE_EMAIL;
        }
        if (RequestType.USERNAME.equals(requestType)) {
            return TrackerEvents.REQ_CHANGE_USERNAME;
        }
        if (RequestType.USERNAME_EMAIL.equals(requestType)) {
            return TrackerEvents.REQ_CHANGE_USERNAME_FIRSTTIME;
        }
        if (RequestType.MERGE_ACCOUNTS.equals(requestType)) {
            return TrackerEvents.REQ_MERGE_ACCOUNT;
        }
        return null;
    }

    public void requestControllerDidReceiveResponseSafe(RequestController controller) {
        ValueStore store = getUserValues();
        store.putValue(Constant.USER_NAME, getSessionUser().getDisplayName());
        store.putValue(Constant.USER_IMAGE_URL, getSessionUser().getImageUrl());
        setListAdapter(new UserProfileListAdapter(this));
        hideSpinnerFor(controller);
        setNeedsRefresh();
        if (RequestType.MERGE_ACCOUNTS.equals(this._lastRequestType)) {
            this._hint = getString(R.string.sl_merge_account_not_found);
            this._lastUpdateError = true;
            showDialogSafe(17, true);
            getTracker().trackEvent(TrackerEvents.CAT_REQUEST, getActionSettings(this._lastRequestType), TrackerEvents.LABEL_ERROR, 0);
        } else if (this._lastRequestType != null) {
            this._lastUpdateError = false;
            getTracker().trackEvent(TrackerEvents.CAT_REQUEST, getActionSettings(this._lastRequestType), TrackerEvents.LABEL_SUCCESS, 0);
        }
    }

    /* access modifiers changed from: private */
    public boolean isValidEmailFormat(String email) {
        return Pattern.compile(".+@.+\\.[a-z]+").matcher(email).matches();
    }

    /* access modifiers changed from: private */
    public void updateUser(String newEmail, String newUsername, RequestType requestType) {
        saveUserState();
        final User user = getSessionUser();
        if (newEmail != null) {
            user.setEmailAddress(newEmail);
        }
        if (newUsername != null) {
            user.setLogin(newUsername);
        }
        setLastRequestType(requestType);
        getHandler().post(new Runnable() {
            public void run() {
                ProfileSettingsListActivity.this.showSpinnerFor(ProfileSettingsListActivity.this._userController);
                ProfileSettingsListActivity.this._userController.setUser(user);
                ProfileSettingsListActivity.this._userController.submitUser();
            }
        });
    }
}
