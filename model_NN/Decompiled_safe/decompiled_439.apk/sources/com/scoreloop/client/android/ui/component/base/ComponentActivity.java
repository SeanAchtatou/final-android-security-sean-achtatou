package com.scoreloop.client.android.ui.component.base;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.scoreloop.client.android.ui.framework.ValueStore;
import com.skyd.bestpuzzle.n1623.R;

public abstract class ComponentActivity extends BaseActivity implements ComponentActivityHooks {
    private RequestControllerObserver _requestControllerObserver;

    private class StandardRequestControllerObserver implements RequestControllerObserver {
        private StandardRequestControllerObserver() {
        }

        /* synthetic */ StandardRequestControllerObserver(ComponentActivity componentActivity, StandardRequestControllerObserver standardRequestControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
            ComponentActivity.this.requestControllerDidFail(aRequestController, anException);
        }

        public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            ComponentActivity.this.requestControllerDidReceiveResponse(aRequestController);
        }
    }

    private Activity getTopParent() {
        if (getParent() != null) {
            return getParent();
        }
        return this;
    }

    private Dialog createErrorDialog(int resId) {
        Dialog dialog = new Dialog(getTopParent());
        dialog.getWindow().requestFeature(1);
        View view = getLayoutInflater().inflate((int) R.layout.sl_dialog_custom, (ViewGroup) null);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true);
        ((TextView) view.findViewById(R.id.message)).setText(getString(resId));
        dialog.setOnDismissListener(this);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return createErrorDialog(R.string.sl_error_message_network);
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void showDialogForExceptionSafe(Exception exception) {
        showDialogSafe(0, true);
    }

    public static boolean isValueChangedFor(String aKey, String theKey, Object oldValue, Object newValue) {
        return aKey.equals(theKey) && newValue != null && !newValue.equals(oldValue);
    }

    public Configuration getConfiguration() {
        return (Configuration) getScreenValues().getValue(Constant.CONFIGURATION);
    }

    public ValueStore getUserValues() {
        return (ValueStore) getScreenValues().getValue(Constant.USER_VALUES);
    }

    public ValueStore getGameValues() {
        return (ValueStore) getScreenValues().getValue(Constant.GAME_VALUES);
    }

    public ValueStore getSessionUserValues() {
        return (ValueStore) getScreenValues().getValue(Constant.SESSION_USER_VALUES);
    }

    public ValueStore getSessionGameValues() {
        return (ValueStore) getScreenValues().getValue(Constant.SESSION_GAME_VALUES);
    }

    public Factory getFactory() {
        return (Factory) getScreenValues().getValue(Constant.FACTORY);
    }

    public Tracker getTracker() {
        return (Tracker) getScreenValues().getValue(Constant.TRACKER);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Game getGame() {
        if (getGameValues() != null) {
            return (Game) getGameValues().getValue(Constant.GAME);
        }
        return null;
    }

    public Manager getManager() {
        return (Manager) getScreenValues().getValue(Constant.MANAGER);
    }

    public int getModeForPosition(int position) {
        return getGame().getMinMode().intValue() + position;
    }

    public String getModeString(int mode) {
        if (!getGame().hasModes()) {
            return "";
        }
        return getResources().getStringArray(getConfiguration().getModesResId())[getPositionForMode(mode)].toString();
    }

    public int getPositionForMode(int mode) {
        return mode - getGame().getMinMode().intValue();
    }

    /* access modifiers changed from: protected */
    public RequestControllerObserver getRequestControllerObserver() {
        if (this._requestControllerObserver == null) {
            this._requestControllerObserver = new StandardRequestControllerObserver(this, null);
        }
        return this._requestControllerObserver;
    }

    public Session getSession() {
        return Session.getCurrentSession();
    }

    public User getSessionUser() {
        return getSession().getUser();
    }

    public User getUser() {
        return (User) getUserValues().getValue(Constant.USER);
    }

    public boolean isSessionGame() {
        Game game = getGame();
        if (game != null) {
            return game.equals(getSession().getGame());
        }
        return false;
    }

    public boolean isSessionUser() {
        User user = getUser();
        if (user != null) {
            return getSession().isOwnedByUser(user);
        }
        return false;
    }

    public final void requestControllerDidFail(RequestController aRequestController, Exception anException) {
        if (!(anException instanceof RequestCancelledException)) {
            hideSpinnerFor(aRequestController);
            if (!isPaused()) {
                requestControllerDidFailSafe(aRequestController, anException);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void requestControllerDidFailSafe(RequestController aRequestController, Exception anException) {
        showDialogForExceptionSafe(anException);
    }

    public void requestControllerDidReceiveResponse(RequestController aRequestController) {
        hideSpinnerFor(aRequestController);
        if (!isPaused()) {
            requestControllerDidReceiveResponseSafe(aRequestController);
        } else {
            setNeedsRefresh();
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
    }

    /* access modifiers changed from: protected */
    public void showDialogSafe(int res, boolean saveDialogState) {
        super.showDialogSafe(res, saveDialogState);
        if (!isPaused()) {
            getTracker().trackEvent(TrackerEvents.CAT_NAVI, String.format(TrackerEvents.NAVI_DIALOG, Integer.valueOf(res)), null, 0);
        }
    }
}
