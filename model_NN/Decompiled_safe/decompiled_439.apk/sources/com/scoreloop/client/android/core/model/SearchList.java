package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.util.SetterIntent;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchList extends BaseEntity {
    public static String a = "search_list";
    private String c;

    public SearchList(String str) {
        super(str);
    }

    SearchList(String str, String str2) {
        this(str);
        this.c = str2;
    }

    public SearchList(JSONObject jSONObject) throws JSONException {
        a(jSONObject);
    }

    static SearchList a(String str) {
        List<SearchList> f;
        if (str == null) {
            throw new IllegalArgumentException();
        }
        Session currentSession = Session.getCurrentSession();
        if (!(currentSession == null || (f = currentSession.getUser().f()) == null)) {
            for (SearchList next : f) {
                if (str.equalsIgnoreCase(next.getIdentifier())) {
                    return next;
                }
            }
        }
        return new SearchList(str, "");
    }

    @PublishedFor__1_0_0
    public static SearchList getBuddiesScoreSearchList() {
        return a("701bb990-80d8-11de-8a39-0800200c9a66");
    }

    @PublishedFor__1_0_0
    public static SearchList getDefaultScoreSearchList() {
        List<SearchList> f;
        Session currentSession = Session.getCurrentSession();
        return (currentSession == null || (f = currentSession.getUser().f()) == null || f.size() <= 0) ? getGlobalScoreSearchList() : f.get(0);
    }

    @PublishedFor__1_0_0
    public static SearchList getGlobalScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a1");
    }

    @PublishedFor__1_0_0
    public static SearchList getLocalScoreSearchList() {
        return a("#local");
    }

    @PublishedFor__1_0_0
    public static SearchList getTwentyFourHourScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a2");
    }

    @PublishedFor__1_0_0
    public static SearchList getUserCountryLocationScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a3");
    }

    @PublishedFor__1_0_0
    public static SearchList getUserNationalityScoreSearchList() {
        return a("428a66d4-e6ca-4ff0-b7ea-f482ba4541a4");
    }

    public String a() {
        return a;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "name", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.c = (String) setterIntent.a();
        }
    }

    public JSONObject d() throws JSONException {
        JSONObject d = super.d();
        d.put("name", getName());
        return d;
    }

    @PublishedFor__1_0_0
    public String getName() {
        return this.c;
    }

    @PublishedFor__1_0_0
    public void setName(String str) {
        this.c = str;
    }

    @PublishedFor__1_0_0
    public String toString() {
        return getName();
    }
}
