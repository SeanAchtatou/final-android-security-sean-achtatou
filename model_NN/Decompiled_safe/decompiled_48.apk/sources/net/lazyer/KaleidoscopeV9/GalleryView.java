package net.lazyer.KaleidoscopeV9;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class GalleryView extends Activity {
    /* access modifiers changed from: private */
    public String album_path;
    /* access modifiers changed from: private */
    public int galleryID;
    /* access modifiers changed from: private */
    public Bitmap mBitmap;
    /* access modifiers changed from: private */
    public ProgressDialog mDialog = null;
    /* access modifiers changed from: private */
    public String mFileName;
    private String[] mGalleryNames;
    /* access modifiers changed from: private */
    public int mGalleryPosition;
    /* access modifiers changed from: private */
    public String mMessage;
    /* access modifiers changed from: private */
    public Handler messageHandler = new Handler() {
        public void handleMessage(Message msg) {
            GalleryView.this.mDialog.dismiss();
            AlertDialog.Builder alert = new AlertDialog.Builder(GalleryView.this);
            alert.setTitle((int) R.string.save_result);
            alert.setMessage(GalleryView.this.mMessage);
            alert.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });
            alert.show();
        }
    };
    private Runnable saveFileRunnable = new Runnable() {
        public void run() {
            try {
                GalleryView.this.saveFile(GalleryView.this.mBitmap, GalleryView.this.mFileName);
                Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                intent.setData(Uri.fromFile(new File(String.valueOf(GalleryView.this.album_path) + GalleryView.this.mFileName)));
                GalleryView.this.sendBroadcast(intent);
                GalleryView.this.mMessage = GalleryView.this.getString(R.string.save_succ);
            } catch (IOException e) {
                GalleryView.this.mMessage = GalleryView.this.getString(R.string.save_failed);
            }
            GalleryView.this.messageHandler.sendMessage(GalleryView.this.messageHandler.obtainMessage());
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideBars();
        setContentView((int) R.layout.image_view);
        this.galleryID = getIntent().getExtras().getInt("GalleryID", 0);
        Gallery g = (Gallery) findViewById(R.id.gallery);
        g.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
            }
        });
        g.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
                GalleryView.this.mGalleryPosition = position;
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.album_path = getPackageName().split("\\.")[2];
        this.album_path = "/sdcard/" + this.album_path + "/";
    }

    private void hideBars() {
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
    }

    public void saveFile(Bitmap bm, String fileName) throws IOException {
        File dirFile = new File(this.album_path);
        if (!dirFile.exists()) {
            dirFile.mkdir();
        }
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(String.valueOf(this.album_path) + fileName)));
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        bos.flush();
        bos.close();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, (int) R.string.save_to_gallery).setIcon(17301582);
        menu.add(1, 1, 1, (int) R.string.set_wallpaper).setIcon(17301567);
        menu.add(2, 2, 2, (int) R.string.share).setIcon(17301586);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        this.mBitmap = BitmapFactory.decodeResource(getResources(), Config.mImageCategories[this.galleryID][this.mGalleryPosition]);
        this.mFileName = String.valueOf(getPackageName().split("\\.")[2]) + "_" + this.galleryID + "_" + this.mGalleryPosition + ".jpg";
        switch (item.getItemId()) {
            case R.styleable.com_admob_android_ads_AdView_testing:
                this.mDialog = ProgressDialog.show(this, getString(R.string.save_image), getString(R.string.save_ing), true);
                new Thread(this.saveFileRunnable).start();
                break;
            case R.styleable.com_admob_android_ads_AdView_backgroundColor:
                try {
                    setWallpaper(this.mBitmap);
                    Toast.makeText(this, getString(R.string.set_wallpaper_ing), 0).show();
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            case R.styleable.com_admob_android_ads_AdView_textColor:
                try {
                    saveFile(this.mBitmap, this.mFileName);
                    Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                    intent.setData(Uri.fromFile(new File(String.valueOf(this.album_path) + this.mFileName)));
                    sendBroadcast(intent);
                } catch (IOException e2) {
                    Toast.makeText(this, (int) R.string.save_failed, 0).show();
                }
                startShareMediaActivity();
                break;
        }
        return false;
    }

    private void startShareMediaActivity() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.setType("image/*");
        intent.putExtra("android.intent.extra.STREAM", Uri.parse("file:" + this.album_path + this.mFileName));
        try {
            startActivity(Intent.createChooser(intent, getString(R.string.share)));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, (int) R.string.no_way_to_share_image, 0).show();
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            this.mContext = c;
        }

        public int getCount() {
            return Config.mImageCategories[GalleryView.this.galleryID].length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(this.mContext);
            i.setImageResource(Config.mImageCategories[GalleryView.this.galleryID][position]);
            i.setScaleType(ImageView.ScaleType.FIT_XY);
            i.setLayoutParams(new Gallery.LayoutParams(-2, -2));
            return i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(long, long):long}
          ClspMth{java.lang.Math.max(float, float):float} */
        public float getAlpha(boolean focused, int offset) {
            return Math.max(0.0f, 1.0f - (0.2f * ((float) Math.abs(offset))));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(long, long):long}
          ClspMth{java.lang.Math.max(float, float):float} */
        public float getScale(boolean focused, int offset) {
            return Math.max(0.0f, 1.0f - (0.2f * ((float) Math.abs(offset))));
        }
    }
}
