package org.anddev.andengine.sensor.orientation;

public interface IOrientationListener {
    void onOrientationChanged(OrientationData orientationData);
}
