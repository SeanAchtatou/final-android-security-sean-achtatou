package org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator;

import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape;
import org.anddev.andengine.util.ArrayUtils;

public class RadialGradientFillBitmapTextureAtlasSourceDecorator extends BaseShapeBitmapTextureAtlasSourceDecorator {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$atlas$bitmap$source$decorator$RadialGradientFillBitmapTextureAtlasSourceDecorator$RadialGradientDirection;
    private static final float[] POSITIONS_DEFAULT = {0.0f, 1.0f};
    protected final int[] mColors;
    protected final float[] mPositions;
    protected final RadialGradientDirection mRadialGradientDirection;

    public enum RadialGradientDirection {
        INSIDE_OUT,
        OUTSIDE_IN
    }

    static /* synthetic */ int[] $SWITCH_TABLE$org$anddev$andengine$opengl$texture$atlas$bitmap$source$decorator$RadialGradientFillBitmapTextureAtlasSourceDecorator$RadialGradientDirection() {
        int[] iArr = $SWITCH_TABLE$org$anddev$andengine$opengl$texture$atlas$bitmap$source$decorator$RadialGradientFillBitmapTextureAtlasSourceDecorator$RadialGradientDirection;
        if (iArr == null) {
            iArr = new int[RadialGradientDirection.values().length];
            try {
                iArr[RadialGradientDirection.INSIDE_OUT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[RadialGradientDirection.OUTSIDE_IN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$org$anddev$andengine$opengl$texture$atlas$bitmap$source$decorator$RadialGradientFillBitmapTextureAtlasSourceDecorator$RadialGradientDirection = iArr;
        }
        return iArr;
    }

    public RadialGradientFillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource pBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape pBitmapTextureAtlasSourceDecoratorShape, int pFromColor, int pToColor, RadialGradientDirection pRadialGradientDirection) {
        this(pBitmapTextureAtlasSource, pBitmapTextureAtlasSourceDecoratorShape, pFromColor, pToColor, pRadialGradientDirection, (BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions) null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RadialGradientFillBitmapTextureAtlasSourceDecorator(org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource r8, org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape r9, int r10, int r11, org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.RadialGradientFillBitmapTextureAtlasSourceDecorator.RadialGradientDirection r12, org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions r13) {
        /*
            r7 = this;
            r0 = 2
            int[] r3 = new int[r0]
            r0 = 0
            r3[r0] = r10
            r0 = 1
            r3[r0] = r11
            float[] r4 = org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.RadialGradientFillBitmapTextureAtlasSourceDecorator.POSITIONS_DEFAULT
            r0 = r7
            r1 = r8
            r2 = r9
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.RadialGradientFillBitmapTextureAtlasSourceDecorator.<init>(org.anddev.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource, org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.shape.IBitmapTextureAtlasSourceDecoratorShape, int, int, org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.RadialGradientFillBitmapTextureAtlasSourceDecorator$RadialGradientDirection, org.anddev.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator$TextureAtlasSourceDecoratorOptions):void");
    }

    public RadialGradientFillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource pBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape pBitmapTextureAtlasSourceDecoratorShape, int[] pColors, float[] pPositions, RadialGradientDirection pRadialGradientDirection) {
        this(pBitmapTextureAtlasSource, pBitmapTextureAtlasSourceDecoratorShape, pColors, pPositions, pRadialGradientDirection, (BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions) null);
    }

    public RadialGradientFillBitmapTextureAtlasSourceDecorator(IBitmapTextureAtlasSource pBitmapTextureAtlasSource, IBitmapTextureAtlasSourceDecoratorShape pBitmapTextureAtlasSourceDecoratorShape, int[] pColors, float[] pPositions, RadialGradientDirection pRadialGradientDirection, BaseBitmapTextureAtlasSourceDecorator.TextureAtlasSourceDecoratorOptions pTextureAtlasSourceDecoratorOptions) {
        super(pBitmapTextureAtlasSource, pBitmapTextureAtlasSourceDecoratorShape, pTextureAtlasSourceDecoratorOptions);
        this.mColors = pColors;
        this.mPositions = pPositions;
        this.mRadialGradientDirection = pRadialGradientDirection;
        this.mPaint.setStyle(Paint.Style.FILL);
        float centerX = 0.5f * ((float) pBitmapTextureAtlasSource.getWidth());
        float centerY = 0.5f * ((float) pBitmapTextureAtlasSource.getHeight());
        float radius = Math.max(centerX, centerY);
        switch ($SWITCH_TABLE$org$anddev$andengine$opengl$texture$atlas$bitmap$source$decorator$RadialGradientFillBitmapTextureAtlasSourceDecorator$RadialGradientDirection()[pRadialGradientDirection.ordinal()]) {
            case 1:
                this.mPaint.setShader(new RadialGradient(centerX, centerY, radius, pColors, pPositions, Shader.TileMode.CLAMP));
                return;
            case 2:
                ArrayUtils.reverse(pColors);
                this.mPaint.setShader(new RadialGradient(centerX, centerY, radius, pColors, pPositions, Shader.TileMode.CLAMP));
                return;
            default:
                return;
        }
    }

    public RadialGradientFillBitmapTextureAtlasSourceDecorator clone() {
        return new RadialGradientFillBitmapTextureAtlasSourceDecorator(this.mBitmapTextureAtlasSource, this.mBitmapTextureAtlasSourceDecoratorShape, this.mColors, this.mPositions, this.mRadialGradientDirection, this.mTextureAtlasSourceDecoratorOptions);
    }
}
