package org.anddev.andengine.opengl.texture.source;

import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

public abstract class BaseTextureAtlasSource implements ITextureAtlasSource {
    protected int mTexturePositionX;
    protected int mTexturePositionY;

    public abstract BaseTextureAtlasSource clone();

    public BaseTextureAtlasSource(int pTexturePositionX, int pTexturePositionY) {
        this.mTexturePositionX = pTexturePositionX;
        this.mTexturePositionY = pTexturePositionY;
    }

    public int getTexturePositionX() {
        return this.mTexturePositionX;
    }

    public int getTexturePositionY() {
        return this.mTexturePositionY;
    }

    public void setTexturePositionX(int pTexturePositionX) {
        this.mTexturePositionX = pTexturePositionX;
    }

    public void setTexturePositionY(int pTexturePositionY) {
        this.mTexturePositionY = pTexturePositionY;
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + "( " + getWidth() + TMXConstants.TAG_OBJECT_ATTRIBUTE_X + getHeight() + " @ " + this.mTexturePositionX + "/" + this.mTexturePositionY + " )";
    }
}
