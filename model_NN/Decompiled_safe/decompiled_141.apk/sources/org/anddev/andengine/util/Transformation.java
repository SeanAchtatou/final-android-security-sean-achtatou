package org.anddev.andengine.util;

import android.util.FloatMath;

public class Transformation {
    private float a = 1.0f;
    private float b = 0.0f;
    private float c = 0.0f;
    private float d = 1.0f;
    private float tx = 0.0f;
    private float ty = 0.0f;

    public String toString() {
        return "Transformation{[" + this.a + ", " + this.c + ", " + this.tx + "][" + this.b + ", " + this.d + ", " + this.ty + "][0.0, 0.0, 1.0]}";
    }

    public void reset() {
        setToIdentity();
    }

    public void setToIdentity() {
        this.a = 1.0f;
        this.d = 1.0f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.tx = 0.0f;
        this.ty = 0.0f;
    }

    public void setTo(Transformation pTransformation) {
        this.a = pTransformation.a;
        this.d = pTransformation.d;
        this.b = pTransformation.b;
        this.c = pTransformation.c;
        this.tx = pTransformation.tx;
        this.ty = pTransformation.ty;
    }

    public void preTranslate(float pX, float pY) {
        preConcat(1.0f, 0.0f, 0.0f, 1.0f, pX, pY);
    }

    public void postTranslate(float pX, float pY) {
        postConcat(1.0f, 0.0f, 0.0f, 1.0f, pX, pY);
    }

    public Transformation setToTranslate(float pX, float pY) {
        this.a = 1.0f;
        this.b = 0.0f;
        this.c = 0.0f;
        this.d = 1.0f;
        this.tx = pX;
        this.ty = pY;
        return this;
    }

    public void preScale(float pScaleX, float pScaleY) {
        preConcat(pScaleX, 0.0f, 0.0f, pScaleY, 0.0f, 0.0f);
    }

    public void postScale(float pScaleX, float pScaleY) {
        postConcat(pScaleX, 0.0f, 0.0f, pScaleY, 0.0f, 0.0f);
    }

    public Transformation setToScale(float pScaleX, float pScaleY) {
        this.a = pScaleX;
        this.b = 0.0f;
        this.c = 0.0f;
        this.d = pScaleY;
        this.tx = 0.0f;
        this.ty = 0.0f;
        return this;
    }

    public void preRotate(float pAngle) {
        float angleRad = MathUtils.degToRad(pAngle);
        float sin = FloatMath.sin(angleRad);
        float cos = FloatMath.cos(angleRad);
        preConcat(cos, sin, -sin, cos, 0.0f, 0.0f);
    }

    public void postRotate(float pAngle) {
        float angleRad = MathUtils.degToRad(pAngle);
        float sin = FloatMath.sin(angleRad);
        float cos = FloatMath.cos(angleRad);
        postConcat(cos, sin, -sin, cos, 0.0f, 0.0f);
    }

    public Transformation setToRotate(float pAngle) {
        float angleRad = MathUtils.degToRad(pAngle);
        float sin = FloatMath.sin(angleRad);
        float cos = FloatMath.cos(angleRad);
        this.a = cos;
        this.b = sin;
        this.c = -sin;
        this.d = cos;
        this.tx = 0.0f;
        this.ty = 0.0f;
        return this;
    }

    public void postConcat(Transformation pTransformation) {
        postConcat(pTransformation.a, pTransformation.b, pTransformation.c, pTransformation.d, pTransformation.tx, pTransformation.ty);
    }

    private void postConcat(float pA, float pB, float pC, float pD, float pTX, float pTY) {
        float a2 = this.a;
        float b2 = this.b;
        float c2 = this.c;
        float d2 = this.d;
        float tx2 = this.tx;
        float ty2 = this.ty;
        this.a = (a2 * pA) + (b2 * pC);
        this.b = (a2 * pB) + (b2 * pD);
        this.c = (c2 * pA) + (d2 * pC);
        this.d = (c2 * pB) + (d2 * pD);
        this.tx = (tx2 * pA) + (ty2 * pC) + pTX;
        this.ty = (tx2 * pB) + (ty2 * pD) + pTY;
    }

    public void preConcat(Transformation pTransformation) {
        preConcat(pTransformation.a, pTransformation.b, pTransformation.c, pTransformation.d, pTransformation.tx, pTransformation.ty);
    }

    private void preConcat(float pA, float pB, float pC, float pD, float pTX, float pTY) {
        float a2 = this.a;
        float b2 = this.b;
        float c2 = this.c;
        float d2 = this.d;
        float tx2 = this.tx;
        float ty2 = this.ty;
        this.a = (pA * a2) + (pB * c2);
        this.b = (pA * b2) + (pB * d2);
        this.c = (pC * a2) + (pD * c2);
        this.d = (pC * b2) + (pD * d2);
        this.tx = (pTX * a2) + (pTY * c2) + tx2;
        this.ty = (pTX * b2) + (pTY * d2) + ty2;
    }

    public void transform(float[] pVertices) {
        int count = pVertices.length / 2;
        int j = 0;
        int i = 0;
        while (true) {
            count--;
            if (count >= 0) {
                int i2 = i + 1;
                float x = pVertices[i];
                i = i2 + 1;
                float y = pVertices[i2];
                int j2 = j + 1;
                pVertices[j] = (this.a * x) + (this.c * y) + this.tx;
                j = j2 + 1;
                pVertices[j2] = (this.b * x) + (this.d * y) + this.ty;
            } else {
                return;
            }
        }
    }
}
