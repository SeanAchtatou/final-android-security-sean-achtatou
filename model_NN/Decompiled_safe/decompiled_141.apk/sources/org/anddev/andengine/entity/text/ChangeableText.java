package org.anddev.andengine.entity.text;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.StringUtils;

public class ChangeableText extends Text {
    private static final String ELLIPSIS = "...";
    private static final int ELLIPSIS_CHARACTER_COUNT = ELLIPSIS.length();
    private int mCharacterCountCurrentText;

    public ChangeableText(float pX, float pY, Font pFont, String pText) {
        this(pX, pY, pFont, pText, pText.length() - StringUtils.countOccurrences(pText, 10));
    }

    public ChangeableText(float pX, float pY, Font pFont, String pText, int pCharactersMaximum) {
        this(pX, pY, pFont, pText, HorizontalAlign.LEFT, pCharactersMaximum);
    }

    public ChangeableText(float pX, float pY, Font pFont, String pText, HorizontalAlign pHorizontalAlign, int pCharactersMaximum) {
        super(pX, pY, pFont, pText, pHorizontalAlign, pCharactersMaximum);
        this.mCharacterCountCurrentText = pText.length() - StringUtils.countOccurrences(pText, 10);
    }

    public void setText(String pText) {
        setText(pText, false);
    }

    public void setText(String pText, boolean pAllowEllipsis) {
        int textCharacterCount = pText.length() - StringUtils.countOccurrences(pText, 10);
        if (textCharacterCount > this.mCharactersMaximum) {
            if (!pAllowEllipsis || this.mCharactersMaximum <= ELLIPSIS_CHARACTER_COUNT) {
                updateText(pText.substring(0, this.mCharactersMaximum));
            } else {
                updateText(pText.substring(0, this.mCharactersMaximum - ELLIPSIS_CHARACTER_COUNT).concat(ELLIPSIS));
            }
            this.mCharacterCountCurrentText = this.mCharactersMaximum;
            return;
        }
        updateText(pText);
        this.mCharacterCountCurrentText = textCharacterCount;
    }

    /* access modifiers changed from: protected */
    public void drawVertices(GL10 pGL, Camera pCamera) {
        pGL.glDrawArrays(4, 0, this.mCharacterCountCurrentText * 6);
    }
}
