package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ParallelModifier;

public class ParallelBackgroundModifier extends ParallelModifier<IBackground> implements IBackgroundModifier {
    public ParallelBackgroundModifier(IBackgroundModifier... pBackgroundModifiers) throws IllegalArgumentException {
        super(pBackgroundModifiers);
    }

    public ParallelBackgroundModifier(IBackgroundModifier.IBackgroundModifierListener pBackgroundModifierListener, IBackgroundModifier... pBackgroundModifiers) throws IllegalArgumentException {
        super(pBackgroundModifierListener, pBackgroundModifiers);
    }

    protected ParallelBackgroundModifier(ParallelBackgroundModifier pParallelBackgroundModifier) throws IModifier.CloneNotSupportedException {
        super(pParallelBackgroundModifier);
    }

    public ParallelBackgroundModifier clone() throws IModifier.CloneNotSupportedException {
        return new ParallelBackgroundModifier(this);
    }
}
