package defpackage;

import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.util.a;
import java.util.HashMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

/* renamed from: o  reason: default package */
public final class o implements i {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        a.e("Invalid " + hashMap.get(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE) + " request error: " + hashMap.get("errors"));
        c g = dVar.g();
        if (g != null) {
            g.a(AdRequest.ErrorCode.INVALID_REQUEST);
        }
    }
}
