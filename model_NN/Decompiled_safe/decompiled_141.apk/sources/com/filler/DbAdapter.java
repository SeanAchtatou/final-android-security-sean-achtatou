package com.filler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbAdapter {
    private static final String DATABASE_CREATE = "create table levelsdata (_id integer primary key autoincrement, mainlevel text not null, easylevel text not null, normallevel text not null , hardlevel text not null, easychallenge text not null, normalchallenge text not null, hardchallenge text not null);";
    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "levelsdata";
    private static final int DATABASE_VERSION = 1;
    public static final String EASYCHALLENGE = "easychallenge";
    public static final String EASYLEVEL = "easylevel";
    public static final String HARDCHALLENGE = "hardchallenge";
    public static final String HARDLEVEL = "hardlevel";
    public static final String MAINLEVEL = "mainlevel";
    public static final String NORMALCHALLENGE = "normalchallenge";
    public static final String NORMALLEVEL = "normallevel";
    public static final String ROWID = "_id";
    private final Context mCtx;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public DbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public DbAdapter open() throws SQLException {
        this.mDbHelper = new DatabaseHelper(this.mCtx);
        this.mDb = this.mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.mDbHelper.close();
    }

    public long createNote() {
        ContentValues initialValues = new ContentValues();
        initialValues.put(MAINLEVEL, "100000");
        initialValues.put(EASYLEVEL, "100000000000000000");
        initialValues.put(NORMALLEVEL, "100000000000000000");
        initialValues.put(HARDLEVEL, "100000000000000000");
        initialValues.put(EASYCHALLENGE, "111111111111111111");
        initialValues.put(NORMALCHALLENGE, "111111111111111111");
        initialValues.put(HARDCHALLENGE, "111111111111111111");
        return this.mDb.insert(DATABASE_TABLE, null, initialValues);
    }

    public Cursor fetchNote() throws SQLException {
        Cursor mCursor = this.mDb.query(true, DATABASE_TABLE, new String[]{ROWID, MAINLEVEL, EASYLEVEL, NORMALLEVEL, HARDLEVEL, EASYCHALLENGE, NORMALCHALLENGE, HARDCHALLENGE}, "_id=1", null, null, null, null, null);
        if (mCursor == null) {
            return null;
        }
        mCursor.moveToFirst();
        return mCursor;
    }

    public Cursor getIdCount() {
        return this.mDb.query(DATABASE_TABLE, new String[]{ROWID}, null, null, null, null, null);
    }

    public boolean resetData() {
        ContentValues args = new ContentValues();
        args.put(MAINLEVEL, "100000");
        args.put(EASYLEVEL, "100000000000000000");
        args.put(NORMALLEVEL, "100000000000000000");
        args.put(HARDLEVEL, "100000000000000000");
        args.put(EASYCHALLENGE, "111111111111111111");
        args.put(NORMALCHALLENGE, "111111111111111111");
        args.put(HARDCHALLENGE, "111111111111111111");
        return this.mDb.update(DATABASE_TABLE, args, "_id=1", null) > 0;
    }

    public boolean updateNote(String id, String value) {
        ContentValues args = new ContentValues();
        args.put(id, value);
        return this.mDb.update(DATABASE_TABLE, args, "_id=1", null) > 0;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, "data", (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DbAdapter.DATABASE_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS notes");
            onCreate(db);
        }
    }
}
