package com.filler;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import java.io.IOException;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.AutoParallaxBackground;
import org.anddev.andengine.entity.scene.background.ParallaxBackground;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.sprite.batch.SpriteBatch;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.MathUtils;

public class ChallengeEasy extends BaseGameActivity implements Scene.IOnSceneTouchListener {
    private static final FixtureDef BOUNCE_DEF = PhysicsFactory.createFixtureDef(1.0f, 1.0f, 0.1f);
    private static int CAMERA_HEIGHT = 0;
    private static int CAMERA_WIDTH = 0;
    private static final FixtureDef NEW_BALL_DEF = PhysicsFactory.createFixtureDef(1.0f, 0.3f, 0.1f);
    private static final FixtureDef WALL_DEF = PhysicsFactory.createFixtureDef(0.0f, 0.0f, 0.0f);
    private static final float grow = 0.01f;
    private static final float speed = 4.0f;
    /* access modifiers changed from: private */
    public Shape[] addShapes;
    private SharedPreferences app_preferences;
    private double areaterritory;
    private TextureRegion background;
    private BitmapTextureAtlas backgroundTexture;
    private Sound badSound;
    /* access modifiers changed from: private */
    public int ballAdded = 4;
    private boolean ball_removed = false;
    private String challange_mode;
    /* access modifiers changed from: private */
    public Sprite circ;
    /* access modifiers changed from: private */
    public boolean circ_on = false;
    private DbAdapter dbadapter;
    /* access modifiers changed from: private */
    public String error_msg;
    /* access modifiers changed from: private */
    public float global_x;
    /* access modifiers changed from: private */
    public float global_y;
    private TextureRegion growBall;
    private int[] grow_arr;
    private int[] grow_arr_1 = {1, 85, 2, 70, 3, 65, 4, 60, 5, 55, 6, 50};
    private int[] grow_arr_2 = {1, 78, 2, 70, 3, 65, 4, 60, 5, 55, 6, 50};
    private int[] grow_arr_3 = {1, 74, 2, 70, 3, 65, 4, 60, 5, 55, 6, 50};
    private double grow_goal;
    private HUD hud;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int level;
    public String level_str;
    private int[] limited_arr = {1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14};
    private int limited_index;
    private Sound lostSound;
    private Font mFont;
    private PhysicsWorld mPhysicsWorld;
    private BitmapTextureAtlas mTexture;
    private int maxBallCanAdd = 10;
    private int minus_h;
    private int minus_w;
    private TextureRegion movingBall;
    /* access modifiers changed from: private */
    public int movingBallCount = 1;
    /* access modifiers changed from: private */
    public Body[] movingBodys;
    /* access modifiers changed from: private */
    public Shape[] movingShapes;
    private boolean nopop = false;
    private int[] nopop_arr = {3, 5, 7, 9, 11, 13};
    private int nopop_index;
    private Double num;
    private Double num2;
    private float obj1w;
    private float obj1x;
    private float obj1y;
    private float obj2w;
    private float obj2x;
    private float obj2y;
    private boolean sound_enabled;
    /* access modifiers changed from: private */
    public float speedx;
    /* access modifiers changed from: private */
    public float speedy;
    private TextureRegion status_back;
    private int status_region = 60;
    private ChangeableText status_text;
    private double territory;
    private BitmapTextureAtlas textTexture;
    private TextureRegion white_ball_left_back;
    private int white_ball_left_region = 30;
    private ChangeableText white_ball_left_text;
    private Sound winSound;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        int w;
        super.onCreate(pSavedInstanceState);
        Bundle bundle = getIntent().getExtras();
        this.level = Integer.valueOf(bundle.getString(LevelConstants.TAG_LEVEL)).intValue();
        this.level_str = bundle.getString("level_str");
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int mWidth = dm.widthPixels;
        int mHeight = dm.heightPixels;
        int rotation = getWindowManager().getDefaultDisplay().getOrientation();
        if (rotation == 1 || rotation == 3) {
            w = (int) (((float) mWidth) / dm.density);
        } else {
            w = (int) (((float) mHeight) / dm.density);
        }
        if (w <= 480) {
            this.grow_arr = this.grow_arr_1;
        } else if (CAMERA_WIDTH <= 534) {
            this.grow_arr = this.grow_arr_2;
        } else {
            this.grow_arr = this.grow_arr_3;
        }
        if (this.level < 7) {
            this.challange_mode = "grow";
            this.maxBallCanAdd = 1;
            this.movingBallCount = this.level;
            this.grow_goal = (double) this.grow_arr[(this.level * 2) - 1];
        } else if (this.level < 13) {
            this.challange_mode = "nopop";
            this.nopop_index = this.nopop_arr[this.level - 7];
            this.movingBallCount = this.nopop_index;
            this.maxBallCanAdd = (this.nopop_index - 1) + 12;
            this.nopop = true;
        } else {
            this.challange_mode = "limited";
            this.limited_index = this.level - 13;
            this.movingBallCount = this.limited_arr[this.limited_index * 2];
            this.maxBallCanAdd = this.limited_arr[(this.limited_index * 2) + 1];
        }
        this.app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.sound_enabled = this.app_preferences.getBoolean("sound_enabled", false);
        this.dbadapter = new DbAdapter(getApplicationContext());
        this.dbadapter.open();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.dbadapter.close();
        this.dbadapter = null;
        this.movingShapes = null;
        this.movingBodys = null;
        super.onDestroy();
        System.gc();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 0, 0, "Restart level").setIcon(17301581);
        if (this.sound_enabled) {
            menu.add(0, 1, 1, "Sound off").setIcon(17301553);
        } else {
            menu.add(0, 1, 1, "Sound on").setIcon(17301554);
        }
        menu.add(0, 2, 2, "Exit level").setIcon(17301580);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                this.mEngine.stop();
                updateLevel();
                break;
            case 1:
                if (this.sound_enabled) {
                    this.sound_enabled = false;
                } else {
                    this.sound_enabled = true;
                }
                SharedPreferences.Editor editor = this.app_preferences.edit();
                editor.putBoolean("sound_enabled", this.sound_enabled);
                editor.commit();
                break;
            case 2:
                finish();
                break;
        }
        return false;
    }

    public Engine onLoadEngine() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int mWidth = dm.widthPixels;
        int mHeight = dm.heightPixels;
        int rotation = getWindowManager().getDefaultDisplay().getOrientation();
        if (rotation == 1 || rotation == 3) {
            CAMERA_WIDTH = (int) (((float) mWidth) / dm.density);
            CAMERA_HEIGHT = (int) (((float) mHeight) / dm.density);
        } else {
            CAMERA_WIDTH = (int) (((float) mHeight) / dm.density);
            CAMERA_HEIGHT = (int) (((float) mWidth) / dm.density);
        }
        this.areaterritory = Math.floor(((double) (CAMERA_WIDTH * CAMERA_HEIGHT)) * 0.6d);
        Camera camera = new Camera(0.0f, 0.0f, (float) CAMERA_WIDTH, (float) CAMERA_HEIGHT);
        this.hud = new HUD();
        camera.setHUD(this.hud);
        EngineOptions engineOptions = new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy((float) CAMERA_WIDTH, (float) CAMERA_HEIGHT), camera).setNeedsSound(true);
        engineOptions.getTouchOptions().setRunOnUpdateThread(true);
        return new Engine(engineOptions);
    }

    public void onLoadResources() {
        this.mTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.textTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        this.growBall = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "grow_ball_white.png", 0, 0);
        this.movingBall = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "moving_ball_24.png", 481, 0);
        this.white_ball_left_back = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "grow_ball_white_left_30.png", 0, 481);
        this.status_back = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "status_60.png", 506, 0);
        this.mFont = new Font(this.textTexture, Typeface.create(Typeface.DEFAULT_BOLD, 1), 16.0f, true, -65536);
        this.backgroundTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.DEFAULT);
        this.background = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.backgroundTexture, this, "main_back_big.jpg", 0, 0);
        SoundFactory.setAssetBasePath("mfx/");
        try {
            this.badSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "bad.mp3");
            this.winSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "win.mp3");
            this.lostSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "lost.mp3");
        } catch (IOException e) {
        }
        this.mEngine.getTextureManager().loadTextures(this.mTexture, this.textTexture, this.backgroundTexture);
        this.mEngine.getFontManager().loadFont(this.mFont);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.MathUtils.random(float, float):float
     arg types: [int, int]
     candidates:
      org.anddev.andengine.util.MathUtils.random(int, int):int
      org.anddev.andengine.util.MathUtils.random(float, float):float */
    public Scene onLoadScene() {
        Scene scene = new Scene();
        AutoParallaxBackground autoParallaxBackground = new AutoParallaxBackground(0.0f, 0.0f, 0.0f, 0.0f);
        autoParallaxBackground.attachParallaxEntity(new ParallaxBackground.ParallaxEntity(0.0f, new Sprite(0.0f, (float) (CAMERA_HEIGHT - this.background.getHeight()), this.background)));
        scene.setBackground(autoParallaxBackground);
        scene.setOnSceneTouchListener(this);
        this.mPhysicsWorld = new FixedStepPhysicsWorld(25, new Vector2(0.0f, 9.80665f), false);
        Sprite sprite = new Sprite(0.0f, 0.0f, this.white_ball_left_back);
        Sprite sprite2 = new Sprite((float) (CAMERA_WIDTH - 60), (float) (CAMERA_HEIGHT - 60), this.status_back);
        this.white_ball_left_text = new ChangeableText(6.0f, 6.0f, this.mFont, String.valueOf(this.maxBallCanAdd), 2);
        this.status_text = new ChangeableText(250.0f, 5.0f, this.mFont, "0%", 4);
        updateText();
        this.hud.attachChild(sprite2);
        this.hud.attachChild(sprite);
        this.hud.attachChild(this.white_ball_left_text);
        this.hud.attachChild(this.status_text);
        this.addShapes = new Shape[(this.ballAdded + this.maxBallCanAdd)];
        Rectangle rectangle = new Rectangle(0.0f, (float) CAMERA_HEIGHT, (float) CAMERA_WIDTH, 1.0f);
        this.addShapes[0] = rectangle;
        Rectangle rectangle2 = new Rectangle(0.0f, -1.0f, (float) CAMERA_WIDTH, 1.0f);
        this.addShapes[1] = rectangle2;
        Rectangle rectangle3 = new Rectangle(-1.0f, 0.0f, 1.0f, (float) CAMERA_HEIGHT);
        this.addShapes[2] = rectangle3;
        Rectangle rectangle4 = new Rectangle((float) CAMERA_WIDTH, 0.0f, 1.0f, (float) CAMERA_HEIGHT);
        this.addShapes[3] = rectangle4;
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle, BodyDef.BodyType.StaticBody, WALL_DEF);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle2, BodyDef.BodyType.StaticBody, WALL_DEF);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle3, BodyDef.BodyType.StaticBody, WALL_DEF);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle4, BodyDef.BodyType.StaticBody, WALL_DEF);
        scene.attachChild(rectangle);
        scene.attachChild(rectangle2);
        scene.attachChild(rectangle3);
        scene.attachChild(rectangle4);
        this.movingShapes = new Shape[this.movingBallCount];
        this.movingBodys = new Body[this.movingBallCount];
        SpriteBatch spriteBatch = new SpriteBatch(this.mTexture, this.movingBallCount);
        this.i = 0;
        while (this.i < this.movingBallCount) {
            float xPos = MathUtils.random(30.0f, ((float) CAMERA_WIDTH) - 30.0f);
            float yPos = MathUtils.random(30.0f, ((float) CAMERA_HEIGHT) - 30.0f);
            float xDir = MathUtils.random(-1.0f, 1.0f);
            float yDir = MathUtils.random(-1.0f, 1.0f);
            Sprite sprite3 = new Sprite(xPos, yPos, this.movingBall);
            Body body = PhysicsFactory.createCircleBody(this.mPhysicsWorld, sprite3, BodyDef.BodyType.DynamicBody, BOUNCE_DEF);
            Vector2 velocity = Vector2Pool.obtain(speed * xDir, speed * yDir);
            body.setLinearVelocity(velocity);
            Vector2Pool.recycle(velocity);
            spriteBatch.attachChild(sprite3);
            this.movingShapes[this.i] = sprite3;
            this.movingBodys[this.i] = body;
            this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite3, body, true, false));
            this.i = this.i + 1;
        }
        spriteBatch.submit();
        scene.attachChild(spriteBatch);
        scene.registerUpdateHandler(this.mPhysicsWorld);
        scene.registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                ChallengeEasy.this.k = ChallengeEasy.this.movingBallCount - 1;
                while (ChallengeEasy.this.k >= 0) {
                    Vector2 vec = ChallengeEasy.this.movingBodys[ChallengeEasy.this.k].getLinearVelocity();
                    if (vec.x < 0.0f) {
                        ChallengeEasy.this.speedx = -4.0f;
                    } else {
                        ChallengeEasy.this.speedx = ChallengeEasy.speed;
                    }
                    if (vec.y < 0.0f) {
                        ChallengeEasy.this.speedy = -4.0f;
                    } else {
                        ChallengeEasy.this.speedy = ChallengeEasy.speed;
                    }
                    Vector2 velocity = Vector2Pool.obtain(ChallengeEasy.this.speedx, ChallengeEasy.this.speedy);
                    ChallengeEasy.this.movingBodys[ChallengeEasy.this.k].setLinearVelocity(velocity);
                    Vector2Pool.recycle(velocity);
                    ChallengeEasy challengeEasy = ChallengeEasy.this;
                    challengeEasy.k = challengeEasy.k - 1;
                }
                if (ChallengeEasy.this.circ_on) {
                    ChallengeEasy.this.circ.setScale(ChallengeEasy.this.circ.getScaleX() + ChallengeEasy.grow);
                    try {
                        ChallengeEasy.this.i = ChallengeEasy.this.ballAdded - 1;
                        while (ChallengeEasy.this.i >= 0) {
                            if (ChallengeEasy.this.i < 4) {
                                if (ChallengeEasy.this.circ_on && ChallengeEasy.this.addShapes[ChallengeEasy.this.i].collidesWith(ChallengeEasy.this.circ)) {
                                    ChallengeEasy.this.addBall(ChallengeEasy.this.global_x, ChallengeEasy.this.global_y, ChallengeEasy.this.circ);
                                }
                            } else if (ChallengeEasy.this.circ_on && ChallengeEasy.this.computeDistance(ChallengeEasy.this.addShapes[ChallengeEasy.this.i], ChallengeEasy.this.circ)) {
                                ChallengeEasy.this.addBall(ChallengeEasy.this.global_x, ChallengeEasy.this.global_y, ChallengeEasy.this.circ);
                            }
                            ChallengeEasy challengeEasy2 = ChallengeEasy.this;
                            challengeEasy2.i = challengeEasy2.i - 1;
                        }
                        if (ChallengeEasy.this.circ_on) {
                            ChallengeEasy.this.j = ChallengeEasy.this.movingBallCount - 1;
                            while (ChallengeEasy.this.j >= 0) {
                                if (ChallengeEasy.this.circ_on && ChallengeEasy.this.computeDistanceMove(ChallengeEasy.this.movingShapes[ChallengeEasy.this.j], ChallengeEasy.this.circ)) {
                                    ChallengeEasy.this.removeBall(ChallengeEasy.this.circ);
                                }
                                ChallengeEasy challengeEasy3 = ChallengeEasy.this;
                                challengeEasy3.j = challengeEasy3.j - 1;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            public void reset() {
            }
        });
        return scene;
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (this.mPhysicsWorld != null) {
            if (pSceneTouchEvent.isActionDown()) {
                if (this.maxBallCanAdd != 0 && !this.circ_on) {
                    Scene scene = this.mEngine.getScene();
                    this.global_x = pSceneTouchEvent.getX();
                    this.global_y = pSceneTouchEvent.getY();
                    this.circ = new Sprite(this.global_x, this.global_y, this.growBall);
                    this.circ.setScale(0.1f);
                    this.circ.setPosition(this.global_x - (this.circ.getWidth() / 2.0f), this.global_y - (this.circ.getWidth() / 2.0f));
                    scene.attachChild(this.circ);
                    this.circ_on = true;
                    this.maxBallCanAdd--;
                    this.white_ball_left_text.setText(String.valueOf(this.maxBallCanAdd));
                    updateText();
                }
                return true;
            } else if (pSceneTouchEvent.isActionMove()) {
                if (this.circ_on) {
                    this.global_x = pSceneTouchEvent.getX();
                    this.global_y = pSceneTouchEvent.getY();
                    this.circ.setPosition(this.global_x - (this.circ.getWidth() / 2.0f), this.global_y - (this.circ.getWidth() / 2.0f));
                }
            } else if (pSceneTouchEvent.isActionUp() && this.circ_on) {
                addBall(pSceneTouchEvent.getX(), pSceneTouchEvent.getY(), this.circ);
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void addBall(float pX, float pY, Sprite c) {
        Scene scene = this.mEngine.getScene();
        scene.detachChild(c);
        Sprite face = new Sprite(pX - (c.getWidth() / 2.0f), pY - (c.getWidth() / 2.0f), this.growBall);
        face.setScale(c.getScaleX());
        Body body = PhysicsFactory.createCircleBody(this.mPhysicsWorld, face, BodyDef.BodyType.DynamicBody, NEW_BALL_DEF);
        scene.attachChild(face);
        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(face, body, true, false));
        this.addShapes[this.ballAdded] = face;
        this.ballAdded++;
        this.circ_on = false;
        this.territory += Math.floor(Math.pow((double) ((c.getWidth() * c.getScaleX()) / 2.0f), 2.0d) * 3.141592653589793d);
        this.status_text.setText(Integer.valueOf((int) Math.floor((this.territory / this.areaterritory) * 100.0d)) + "%");
        updateText();
        checkState();
    }

    /* access modifiers changed from: private */
    public void removeBall(Sprite c) {
        this.mEngine.getScene().detachChild(c);
        this.circ_on = false;
        if (this.nopop) {
            gameOver(3);
            return;
        }
        this.ball_removed = true;
        checkState();
    }

    private void checkState() {
        if (this.challange_mode.equals("grow")) {
            if (((double) Integer.valueOf((int) Math.floor((this.territory / this.areaterritory) * 100.0d)).intValue()) >= this.grow_goal) {
                updateDb();
                challengeComplete();
                return;
            }
            gameOver(2);
        } else if (this.maxBallCanAdd == 0 && this.territory < this.areaterritory) {
            gameOver(1);
        } else if (this.territory >= this.areaterritory) {
            updateDb();
            challengeComplete();
        } else if (this.ball_removed) {
            this.ball_removed = false;
            playSound("bad");
        }
    }

    private void gameOver(int type) {
        switch (type) {
            case 1:
                this.error_msg = " No balls left! ";
                break;
            case 2:
                this.error_msg = "Not reached the size of " + this.grow_goal + "% !";
                break;
            case 3:
                this.error_msg = "You popped a ball!";
                break;
        }
        playSound("lost");
        this.mEngine.stop();
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ChallengeEasy.this);
                dialog.setMessage(ChallengeEasy.this.error_msg);
                dialog.setCancelable(true);
                dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ChallengeEasy.this.updateLevel();
                        dialog.cancel();
                    }
                });
                dialog.setNegativeButton("Menu", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ChallengeEasy.this.finish();
                    }
                });
                dialog.show();
            }
        });
    }

    private void challengeComplete() {
        playSound("win");
        this.mEngine.stop();
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ChallengeEasy.this);
                dialog.setMessage(" Challenge " + ChallengeEasy.this.level + " complete! ");
                dialog.setCancelable(true);
                dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ChallengeEasy.this.updateLevel();
                        dialog.cancel();
                    }
                });
                dialog.setNegativeButton("Menu", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ChallengeEasy.this.finish();
                    }
                });
                dialog.show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateLevel() {
        this.mEngine.start();
        this.hud.detachChildren();
        this.hud = null;
        this.hud = new HUD();
        this.mEngine.getCamera().setHUD(this.hud);
        this.mEngine.clearUpdateHandlers();
        this.mPhysicsWorld.clearPhysicsConnectors();
        this.mPhysicsWorld.dispose();
        this.mPhysicsWorld = null;
        if (this.challange_mode.equals("grow")) {
            this.maxBallCanAdd = 1;
            this.movingBallCount = this.level;
        } else if (this.challange_mode.equals("nopop")) {
            this.movingBallCount = this.nopop_index;
            this.maxBallCanAdd = (this.nopop_index - 1) + 12;
        } else {
            this.movingBallCount = this.limited_arr[this.limited_index * 2];
            this.maxBallCanAdd = this.limited_arr[(this.limited_index * 2) + 1];
        }
        this.territory = 0.0d;
        this.ballAdded = 4;
        this.addShapes = null;
        this.movingShapes = null;
        this.movingBodys = null;
        this.mEngine.setScene(null);
        this.mEngine.setScene(onLoadScene());
    }

    private void updateText() {
        this.minus_w = ((int) (((float) this.white_ball_left_region) - this.white_ball_left_text.getWidth())) / 2;
        this.minus_h = ((int) (((float) this.white_ball_left_region) - this.white_ball_left_text.getHeight())) / 2;
        this.white_ball_left_text.setPosition((float) this.minus_w, (float) this.minus_h);
        this.minus_w = ((int) (((float) this.status_region) - this.status_text.getWidth())) / 2;
        this.minus_h = ((int) (((float) this.status_region) - this.status_text.getHeight())) / 2;
        this.status_text.setPosition((float) ((CAMERA_WIDTH - 60) + this.minus_w), (float) ((CAMERA_HEIGHT - 60) + this.minus_h));
    }

    private void updateDb() {
        this.level_str = replaceCharAt(this.level_str, this.level - 1, '2');
        this.dbadapter.updateNote(DbAdapter.EASYCHALLENGE, this.level_str);
    }

    private void playSound(String mode) {
        if (!this.sound_enabled) {
            return;
        }
        if (mode.equals("win")) {
            this.winSound.play();
        } else if (mode.equals("lost")) {
            this.lostSound.play();
        } else {
            this.badSound.play();
        }
    }

    /* access modifiers changed from: protected */
    public boolean computeDistance(Shape obj1, Shape obj2) {
        this.obj1x = obj1.getX();
        this.obj1y = obj1.getY();
        this.obj2x = obj2.getX();
        this.obj2y = obj2.getY();
        this.num = Double.valueOf(Math.floor(Math.sqrt(Math.pow((double) (this.obj2x - this.obj1x), 2.0d) + Math.pow((double) (this.obj2y - this.obj1y), 2.0d))));
        this.num2 = Double.valueOf(Math.floor((double) (((obj1.getWidth() * obj1.getScaleX()) + (obj2.getWidth() * obj2.getScaleX())) / 2.0f)));
        if (this.num.doubleValue() < this.num2.doubleValue()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean computeDistanceMove(Shape obj1, Shape obj2) {
        this.obj1x = obj1.getX();
        this.obj1y = obj1.getY();
        this.obj1w = obj1.getWidth();
        this.obj2x = obj2.getX();
        this.obj2y = obj2.getY();
        this.obj2w = obj2.getWidth();
        this.num = Double.valueOf(Math.floor(Math.sqrt(Math.pow((double) ((this.obj2x + (this.obj2w / 2.0f)) - this.obj1x), 2.0d) + Math.pow((double) ((this.obj2y + (this.obj2w / 2.0f)) - this.obj1y), 2.0d))));
        this.num2 = Double.valueOf(Math.floor((double) (((this.obj1w * obj1.getScaleX()) / 2.0f) + ((this.obj2w * obj2.getScaleX()) / 2.0f))));
        if (this.num.doubleValue() < this.num2.doubleValue()) {
            return true;
        }
        return false;
    }

    public static String replaceCharAt(String s, int pos, char c) {
        StringBuffer buf = new StringBuffer(s);
        buf.setCharAt(pos, c);
        return buf.toString();
    }

    public void onLoadComplete() {
    }
}
