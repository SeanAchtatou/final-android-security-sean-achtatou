package com.filler;

public final class R {

    public static final class anim {
        public static final int slide_left_in = 2130968576;
        public static final int slide_left_out = 2130968577;
        public static final int slide_right_in = 2130968578;
        public static final int slide_right_out = 2130968579;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int avatar = 2130837504;
        public static final int back_button_1 = 2130837505;
        public static final int back_button_2 = 2130837506;
        public static final int button_back = 2130837507;
        public static final int button_back_2 = 2130837508;
        public static final int button_back_little = 2130837509;
        public static final int button_back_little_2 = 2130837510;
        public static final int button_back_little_lock = 2130837511;
        public static final int button_back_lock = 2130837512;
        public static final int button_fb = 2130837513;
        public static final int circle_big = 2130837514;
        public static final int circle_little = 2130837515;
        public static final int green_level_back = 2130837516;
        public static final int green_level_back_lock = 2130837517;
        public static final int green_level_back_ok = 2130837518;
        public static final int icon = 2130837519;
        public static final int info_pic_1 = 2130837520;
        public static final int info_pic_2 = 2130837521;
        public static final int info_pic_3 = 2130837522;
        public static final int info_pic_4 = 2130837523;
        public static final int info_pic_5 = 2130837524;
        public static final int info_pic_6 = 2130837525;
        public static final int info_pic_7 = 2130837526;
        public static final int main_back = 2130837527;
        public static final int orange_level_back = 2130837528;
        public static final int orange_level_back_lock = 2130837529;
        public static final int orange_level_back_ok = 2130837530;
        public static final int reset_data_1 = 2130837531;
        public static final int reset_data_2 = 2130837532;
        public static final int sound_off = 2130837533;
        public static final int sound_on = 2130837534;
        public static final int white_level_back = 2130837535;
        public static final int white_level_back_lock = 2130837536;
        public static final int white_level_back_ok = 2130837537;
    }

    public static final class id {
        public static final int ad_layout = 2131165216;
        public static final int button_easy_challenges = 2131165194;
        public static final int button_easy_levels = 2131165191;
        public static final int button_hard_challenges = 2131165196;
        public static final int button_hard_levels = 2131165193;
        public static final int button_info = 2131165187;
        public static final int button_normal_challenges = 2131165195;
        public static final int button_normal_levels = 2131165192;
        public static final int button_reset = 2131165189;
        public static final int button_share = 2131165188;
        public static final int button_sound = 2131165190;
        public static final int button_start = 2131165186;
        public static final int info_back = 2131165215;
        public static final int levels_grid = 2131165198;
        public static final int levels_item_text = 2131165184;
        public static final int leveltable_back = 2131165199;
        public static final int list_circ1 = 2131165208;
        public static final int list_circ2 = 2131165209;
        public static final int list_circ3 = 2131165210;
        public static final int list_circ4 = 2131165211;
        public static final int list_circ5 = 2131165212;
        public static final int list_circ6 = 2131165213;
        public static final int list_circ7 = 2131165214;
        public static final int list_flipper = 2131165200;
        public static final int list_image1 = 2131165201;
        public static final int list_image2 = 2131165202;
        public static final int list_image3 = 2131165203;
        public static final int list_image4 = 2131165204;
        public static final int list_image5 = 2131165205;
        public static final int list_image6 = 2131165206;
        public static final int list_image7 = 2131165207;
        public static final int main_flipper = 2131165185;
        public static final int mainlist_back = 2131165197;
    }

    public static final class layout {
        public static final int levels_item = 2130903040;
        public static final int main = 2130903041;
        public static final int splash = 2130903042;
    }

    public static final class raw {
        public static final int click = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
    }
}
