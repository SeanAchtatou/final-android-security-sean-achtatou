package com.filler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

public class Splash extends Activity {
    static final int welcomeScreenDisplay = 2000;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.splash);
        new Thread() {
            int wait = 0;

            public void run() {
                try {
                    super.run();
                    while (this.wait < Splash.welcomeScreenDisplay) {
                        sleep(100);
                        this.wait += 100;
                    }
                } catch (Exception e) {
                } finally {
                    Splash.this.startActivity(new Intent(Splash.this.getApplicationContext(), Main.class));
                    Splash.this.finish();
                }
            }
        }.start();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
