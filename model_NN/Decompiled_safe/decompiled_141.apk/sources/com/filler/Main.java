package com.filler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.badlogic.gdx.physics.box2d.Transform;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import org.anddev.andengine.level.util.constants.LevelConstants;

public class Main extends Activity {
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_MIN_DISTANCE = 60;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final String[] easy_challenge_arr = {"Grow a ball the size of 85% on level 1 !", "Grow a ball the size of 70% on level 2 !", "Grow a ball the size of 65% on level 3 !", "Grow a ball the size of 60% on level 4 !", "Grow a ball the size of 55% on level 5 !", "Grow a ball the size of 50% on level 6 !", "Beat level 3 without popping any balls !", "Beat level 5 without popping any balls !", "Beat level 7 without popping any balls !", "Beat level 9 without popping any balls !", "Beat level 11 without popping any balls !", "Beat level 13 without popping any balls !", "Beat level 1 with only 4 balls !", "Beat level 3 with only 6 balls !", "Beat level 5 with only 8 balls !", "Beat level 7 with only 10 balls !", "Beat level 9 with only 12 balls !", "Beat level 11 with only 14 balls !"};
    private static final String[] easy_challenge_arr_first = {"Grow a ball the size of 85% on level 1 !", "Grow a ball the size of 78% on level 1 !", "Grow a ball the size of 74% on level 1 !"};
    public static final String[] filesnames = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18"};
    private static final String[] hard_challenge_arr = {"Grow 3 ball the size of 27% on level 1 !", "Grow 3 ball the size of 25% on level 2 !", "Grow 3 ball the size of 22% on level 3 !", "Grow 3 ball the size of 20% on level 4 !", "Grow 3 ball the size of 18% on level 5 !", "Grow 3 ball the size of 16% on level 6 !", "Beat level 3 without popping any balls !", "Beat level 5 without popping any balls !", "Beat level 7 without popping any balls !", "Beat level 9 without popping any balls !", "Beat level 11 without popping any balls !", "Beat level 13 without popping any balls !", "Beat level 1 with only 2 balls each color !", "Beat level 3 with only 2 balls each color !", "Beat level 5 with only 3 balls each color !", "Beat level 7 with only 4 balls each color !", "Beat level 9 with only 5 balls each color !", "Beat level 11 with only 6 balls each color !"};
    private static final String[] normal_challenge_arr = {"Grow 2 ball the size of 44% on level 1 !", "Grow 2 ball the size of 35% on level 2 !", "Grow 2 ball the size of 33% on level 3 !", "Grow 2 ball the size of 30% on level 4 !", "Grow 2 ball the size of 28% on level 5 !", "Grow 2 ball the size of 25% on level 6 !", "Beat level 3 without popping any balls !", "Beat level 5 without popping any balls !", "Beat level 7 without popping any balls !", "Beat level 9 without popping any balls !", "Beat level 11 without popping any balls !", "Beat level 13 without popping any balls !", "Beat level 1 with only 3 balls each color !", "Beat level 3 with only 4 balls each color !", "Beat level 5 with only 5 balls each color !", "Beat level 7 with only 6 balls each color !", "Beat level 9 with only 7 balls each color !", "Beat level 11 with only 8 balls each color !"};
    private static final String[] text_arr = {"Easy", "Normal", "Hard", "Easy challenge", "Normal challenge", "Hard challenge"};
    private int CAMERA_WIDTH;
    private RelativeLayout ad_layout;
    private Animation.AnimationListener animlistener = new Animation.AnimationListener() {
        public void onAnimationStart(Animation arg0) {
        }

        public void onAnimationEnd(Animation arg0) {
            Main.this.is_animating = false;
            Main.this.setPoints();
        }

        public void onAnimationRepeat(Animation arg0) {
        }
    };
    /* access modifiers changed from: private */
    public SharedPreferences app_preferences;
    private View.OnTouchListener buttonListener_main = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent event) {
            switch (view.getId()) {
                case R.id.button_start:
                    switch (event.getAction()) {
                        case 0:
                            Main.this.button_start.setBackgroundResource(R.drawable.button_back_2);
                            break;
                        case 1:
                            Main.this.playClick();
                            Main.this.updateMainlist();
                            Main.this.button_start.setBackgroundResource(R.drawable.button_back);
                            Main.this.main_flipper.showNext();
                            break;
                    }
                case R.id.button_info:
                    switch (event.getAction()) {
                        case 0:
                            Main.this.button_info.setBackgroundResource(R.drawable.button_back_2);
                            break;
                        case 1:
                            Main.this.playClick();
                            Main.this.button_info.setBackgroundResource(R.drawable.button_back);
                            Main.this.main_flipper.setDisplayedChild(3);
                            break;
                    }
                case R.id.button_share:
                    switch (event.getAction()) {
                        case 0:
                            Main.this.button_share.setBackgroundResource(R.drawable.button_back_2);
                            break;
                        case 1:
                            Main.this.button_share.setBackgroundResource(R.drawable.button_back);
                            Main.this.playClick();
                            Main.this.addShare();
                            break;
                    }
                case R.id.button_reset:
                    switch (event.getAction()) {
                        case 0:
                            Main.this.button_reset.setBackgroundResource(R.drawable.reset_data_2);
                            break;
                        case 1:
                            Main.this.playClick();
                            Main.this.button_reset.setBackgroundResource(R.drawable.reset_data_1);
                            Main.this.askDelete();
                            break;
                    }
                case R.id.button_sound:
                    switch (event.getAction()) {
                        case 1:
                            if (Main.this.sound_enabled) {
                                Main.this.sound_enabled = false;
                                Main.this.button_sound.setBackgroundResource(R.drawable.sound_off);
                            } else {
                                Main.this.sound_enabled = true;
                                Main.this.button_sound.setBackgroundResource(R.drawable.sound_on);
                            }
                            SharedPreferences.Editor editor = Main.this.app_preferences.edit();
                            editor.putBoolean("sound_enabled", Main.this.sound_enabled);
                            editor.commit();
                            break;
                    }
            }
            return false;
        }
    };
    private View.OnTouchListener buttonListener_mainlist = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent event) {
            switch (view.getId()) {
                case R.id.button_easy_levels:
                    switch (event.getAction()) {
                        case 0:
                            if (Main.this.lock_array[0] == 1) {
                                Main.this.button_easy_levels.setBackgroundResource(R.drawable.button_back_little_2);
                                break;
                            }
                            break;
                        case 1:
                            if (Main.this.lock_array[0] == 1) {
                                Main.this.button_easy_levels.setBackgroundResource(R.drawable.button_back_little);
                                Main.this.playClick();
                                Main.this.mode = "easy";
                                Main.this.selected = true;
                                break;
                            }
                            break;
                    }
                case R.id.button_normal_levels:
                    switch (event.getAction()) {
                        case 0:
                            if (Main.this.lock_array[1] == 1) {
                                Main.this.button_normal_levels.setBackgroundResource(R.drawable.button_back_little_2);
                                break;
                            }
                            break;
                        case 1:
                            if (Main.this.lock_array[1] == 1) {
                                Main.this.button_normal_levels.setBackgroundResource(R.drawable.button_back_little);
                                Main.this.playClick();
                                Main.this.mode = "normal";
                                Main.this.selected = true;
                                break;
                            }
                            break;
                    }
                case R.id.button_hard_levels:
                    switch (event.getAction()) {
                        case 0:
                            if (Main.this.lock_array[2] == 1) {
                                Main.this.button_hard_levels.setBackgroundResource(R.drawable.button_back_little_2);
                                break;
                            }
                            break;
                        case 1:
                            if (Main.this.lock_array[2] == 1) {
                                Main.this.button_hard_levels.setBackgroundResource(R.drawable.button_back_little);
                                Main.this.playClick();
                                Main.this.mode = "hard";
                                Main.this.selected = true;
                                break;
                            }
                            break;
                    }
                case R.id.button_easy_challenges:
                    switch (event.getAction()) {
                        case 0:
                            if (Main.this.lock_array[3] == 1) {
                                Main.this.button_easy_challenges.setBackgroundResource(R.drawable.button_back_little_2);
                                break;
                            }
                            break;
                        case 1:
                            if (Main.this.lock_array[3] == 1) {
                                Main.this.button_easy_challenges.setBackgroundResource(R.drawable.button_back_little);
                                Main.this.playClick();
                                Main.this.mode = "easy_challenges";
                                Main.this.selected = true;
                                break;
                            }
                            break;
                    }
                case R.id.button_normal_challenges:
                    switch (event.getAction()) {
                        case 0:
                            if (Main.this.lock_array[4] == 1) {
                                Main.this.button_normal_challenges.setBackgroundResource(R.drawable.button_back_little_2);
                                break;
                            }
                            break;
                        case 1:
                            if (Main.this.lock_array[4] == 1) {
                                Main.this.button_normal_challenges.setBackgroundResource(R.drawable.button_back_little);
                                Main.this.playClick();
                                Main.this.mode = "normal_challenges";
                                Main.this.selected = true;
                                break;
                            }
                            break;
                    }
                case R.id.button_hard_challenges:
                    switch (event.getAction()) {
                        case 0:
                            if (Main.this.lock_array[5] == 1) {
                                Main.this.button_hard_challenges.setBackgroundResource(R.drawable.button_back_little_2);
                                break;
                            }
                            break;
                        case 1:
                            if (Main.this.lock_array[5] == 1) {
                                Main.this.button_hard_challenges.setBackgroundResource(R.drawable.button_back_little);
                                Main.this.playClick();
                                Main.this.mode = "hard_challenges";
                                Main.this.selected = true;
                                break;
                            }
                            break;
                    }
                case R.id.mainlist_back:
                    switch (event.getAction()) {
                        case 0:
                            Main.this.button_back_mainlist.setBackgroundResource(R.drawable.back_button_2);
                            break;
                        case 1:
                            Main.this.button_back_mainlist.setBackgroundResource(R.drawable.back_button_1);
                            Main.this.playClick();
                            Main.this.main_flipper.showPrevious();
                            break;
                    }
                case R.id.leveltable_back:
                    switch (event.getAction()) {
                        case 0:
                            Main.this.button_back_leveltable.setBackgroundResource(R.drawable.back_button_2);
                            break;
                        case 1:
                            Main.this.button_back_leveltable.setBackgroundResource(R.drawable.back_button_1);
                            Main.this.playClick();
                            Main.this.main_flipper.showPrevious();
                            break;
                    }
                case R.id.info_back:
                    switch (event.getAction()) {
                        case 0:
                            Main.this.button_back_info.setBackgroundResource(R.drawable.back_button_2);
                            break;
                        case 1:
                            Main.this.playClick();
                            Main.this.button_back_info.setBackgroundResource(R.drawable.back_button_1);
                            Main.this.main_flipper.showNext();
                            break;
                    }
            }
            if (Main.this.selected) {
                Main.this.selected = false;
                Main.this.updateLevelTable();
                Main.this.main_flipper.showNext();
            }
            return false;
        }
    };
    /* access modifiers changed from: private */
    public Button button_back_info;
    /* access modifiers changed from: private */
    public Button button_back_leveltable;
    /* access modifiers changed from: private */
    public Button button_back_mainlist;
    /* access modifiers changed from: private */
    public Button button_easy_challenges;
    /* access modifiers changed from: private */
    public Button button_easy_levels;
    /* access modifiers changed from: private */
    public Button button_hard_challenges;
    /* access modifiers changed from: private */
    public Button button_hard_levels;
    /* access modifiers changed from: private */
    public Button button_info;
    /* access modifiers changed from: private */
    public Button button_normal_challenges;
    /* access modifiers changed from: private */
    public Button button_normal_levels;
    /* access modifiers changed from: private */
    public Button button_reset;
    /* access modifiers changed from: private */
    public Button button_share;
    /* access modifiers changed from: private */
    public Button button_sound;
    /* access modifiers changed from: private */
    public Button button_start;
    private ImageView circ1;
    private ImageView circ2;
    private ImageView circ3;
    private ImageView circ4;
    private ImageView circ5;
    private ImageView circ6;
    private ImageView circ7;
    /* access modifiers changed from: private */
    public DbAdapter dbadapter;
    /* access modifiers changed from: private */
    public GestureDetector gestureDetector;
    View.OnTouchListener gestureListener;
    private GridView gridview;
    /* access modifiers changed from: private */
    public boolean is_animating = false;
    private String level_str;
    /* access modifiers changed from: private */
    public int[] lock_array;
    /* access modifiers changed from: private */
    public ViewFlipper main_flipper;
    /* access modifiers changed from: private */
    public String mode = "";
    private MediaPlayer mp;
    /* access modifiers changed from: private */
    public boolean selected = false;
    /* access modifiers changed from: private */
    public Animation slideLeftIn;
    /* access modifiers changed from: private */
    public Animation slideLeftOut;
    /* access modifiers changed from: private */
    public Animation slideRightIn;
    /* access modifiers changed from: private */
    public Animation slideRightOut;
    /* access modifiers changed from: private */
    public boolean sound_enabled;
    /* access modifiers changed from: private */
    public String tablelist_str;
    /* access modifiers changed from: private */
    public ViewFlipper vf;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.main_flipper = (ViewFlipper) findViewById(R.id.main_flipper);
        this.button_start = (Button) findViewById(R.id.button_start);
        this.button_info = (Button) findViewById(R.id.button_info);
        this.button_share = (Button) findViewById(R.id.button_share);
        this.button_reset = (Button) findViewById(R.id.button_reset);
        this.button_sound = (Button) findViewById(R.id.button_sound);
        this.button_start.setOnTouchListener(this.buttonListener_main);
        this.button_info.setOnTouchListener(this.buttonListener_main);
        this.button_share.setOnTouchListener(this.buttonListener_main);
        this.button_reset.setOnTouchListener(this.buttonListener_main);
        this.button_sound.setOnTouchListener(this.buttonListener_main);
        this.button_easy_levels = (Button) findViewById(R.id.button_easy_levels);
        this.button_normal_levels = (Button) findViewById(R.id.button_normal_levels);
        this.button_hard_levels = (Button) findViewById(R.id.button_hard_levels);
        this.button_easy_challenges = (Button) findViewById(R.id.button_easy_challenges);
        this.button_normal_challenges = (Button) findViewById(R.id.button_normal_challenges);
        this.button_hard_challenges = (Button) findViewById(R.id.button_hard_challenges);
        this.button_back_mainlist = (Button) findViewById(R.id.mainlist_back);
        this.button_easy_levels.setOnTouchListener(this.buttonListener_mainlist);
        this.button_normal_levels.setOnTouchListener(this.buttonListener_mainlist);
        this.button_hard_levels.setOnTouchListener(this.buttonListener_mainlist);
        this.button_easy_challenges.setOnTouchListener(this.buttonListener_mainlist);
        this.button_normal_challenges.setOnTouchListener(this.buttonListener_mainlist);
        this.button_hard_challenges.setOnTouchListener(this.buttonListener_mainlist);
        this.button_back_mainlist.setOnTouchListener(this.buttonListener_mainlist);
        this.lock_array = new int[6];
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int mWidth = dm.widthPixels;
        int mHeight = dm.heightPixels;
        int rotation = getWindowManager().getDefaultDisplay().getOrientation();
        if (rotation == 1 || rotation == 3) {
            this.CAMERA_WIDTH = (int) (((float) mWidth) / dm.density);
        } else {
            this.CAMERA_WIDTH = (int) (((float) mHeight) / dm.density);
        }
        this.gridview = (GridView) findViewById(R.id.levels_grid);
        this.button_back_leveltable = (Button) findViewById(R.id.leveltable_back);
        this.button_back_leveltable.setOnTouchListener(this.buttonListener_mainlist);
        this.vf = (ViewFlipper) findViewById(R.id.list_flipper);
        this.button_back_info = (Button) findViewById(R.id.info_back);
        this.button_back_info.setOnTouchListener(this.buttonListener_mainlist);
        this.circ1 = (ImageView) findViewById(R.id.list_circ1);
        this.circ2 = (ImageView) findViewById(R.id.list_circ2);
        this.circ3 = (ImageView) findViewById(R.id.list_circ3);
        this.circ4 = (ImageView) findViewById(R.id.list_circ4);
        this.circ5 = (ImageView) findViewById(R.id.list_circ5);
        this.circ6 = (ImageView) findViewById(R.id.list_circ6);
        this.circ7 = (ImageView) findViewById(R.id.list_circ7);
        this.slideLeftIn = AnimationUtils.loadAnimation(this, R.anim.slide_left_in);
        this.slideLeftOut = AnimationUtils.loadAnimation(this, R.anim.slide_left_out);
        this.slideRightIn = AnimationUtils.loadAnimation(this, R.anim.slide_right_in);
        this.slideRightOut = AnimationUtils.loadAnimation(this, R.anim.slide_right_out);
        this.slideLeftOut.setAnimationListener(this.animlistener);
        this.slideRightOut.setAnimationListener(this.animlistener);
        this.gestureDetector = new GestureDetector(new MyGestureDetector());
        this.gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (Main.this.gestureDetector.onTouchEvent(event)) {
                    return true;
                }
                return false;
            }
        };
        this.ad_layout = (RelativeLayout) findViewById(R.id.ad_layout);
        AdView adView = new AdView(this, AdSize.BANNER, "a14e22b780a8718");
        this.ad_layout.addView(adView);
        adView.loadAd(new AdRequest());
        this.dbadapter = new DbAdapter(getApplicationContext());
        this.dbadapter.open();
        if (this.dbadapter.getIdCount().getCount() == 0) {
            this.dbadapter.createNote();
        }
        setVolumeControlStream(3);
        this.mp = MediaPlayer.create(this, (int) R.raw.click);
        this.app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.sound_enabled = this.app_preferences.getBoolean("sound_enabled", false);
        if (this.sound_enabled) {
            this.button_sound.setBackgroundResource(R.drawable.sound_on);
        } else {
            this.button_sound.setBackgroundResource(R.drawable.sound_off);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        updateMainlist();
        updateLevelTable();
        this.app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.sound_enabled = this.app_preferences.getBoolean("sound_enabled", false);
        if (this.sound_enabled) {
            this.button_sound.setBackgroundResource(R.drawable.sound_on);
        } else {
            this.button_sound.setBackgroundResource(R.drawable.sound_off);
        }
    }

    public class ButtonAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ButtonAdapter(Context c) {
            this.mInflater = LayoutInflater.from(c);
        }

        public int getCount() {
            return Main.filesnames.length;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.levels_item, parent, false);
                holder = new ViewHolder();
                holder.item_button = (Button) convertView.findViewById(R.id.levels_item_text);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.item_button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Main.this.handleMode(position);
                }
            });
            if (Main.this.mode.equals("easy") || Main.this.mode.equals("easy_challenges")) {
                if (String.valueOf(Main.this.tablelist_str.charAt(position)).equals("0")) {
                    holder.item_button.setBackgroundResource(R.drawable.white_level_back_lock);
                } else if (String.valueOf(Main.this.tablelist_str.charAt(position)).equals("1")) {
                    holder.item_button.setBackgroundResource(R.drawable.white_level_back);
                    holder.item_button.setText(Main.filesnames[position]);
                } else {
                    holder.item_button.setBackgroundResource(R.drawable.white_level_back_ok);
                    holder.item_button.setText(Main.filesnames[position]);
                }
            } else if (Main.this.mode.equals("normal") || Main.this.mode.equals("normal_challenges")) {
                if (String.valueOf(Main.this.tablelist_str.charAt(position)).equals("0")) {
                    holder.item_button.setBackgroundResource(R.drawable.orange_level_back_lock);
                } else if (String.valueOf(Main.this.tablelist_str.charAt(position)).equals("1")) {
                    holder.item_button.setBackgroundResource(R.drawable.orange_level_back);
                    holder.item_button.setText(Main.filesnames[position]);
                } else {
                    holder.item_button.setBackgroundResource(R.drawable.orange_level_back_ok);
                    holder.item_button.setText(Main.filesnames[position]);
                }
            } else if (String.valueOf(Main.this.tablelist_str.charAt(position)).equals("0")) {
                holder.item_button.setBackgroundResource(R.drawable.green_level_back_lock);
            } else if (String.valueOf(Main.this.tablelist_str.charAt(position)).equals("1")) {
                holder.item_button.setBackgroundResource(R.drawable.green_level_back);
                holder.item_button.setText(Main.filesnames[position]);
            } else {
                holder.item_button.setBackgroundResource(R.drawable.green_level_back_ok);
                holder.item_button.setText(Main.filesnames[position]);
            }
            return convertView;
        }

        class ViewHolder {
            Button item_button;

            ViewHolder() {
            }
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public void handleMode(final int index) {
        if (this.mode.equals("easy") && !String.valueOf(this.tablelist_str.charAt(index)).equals("0")) {
            playClick();
            Intent intent_easy = new Intent(getApplicationContext(), LevelEasy.class);
            Bundle bundle_easy = new Bundle();
            bundle_easy.putString(LevelConstants.TAG_LEVEL, String.valueOf(index + 1));
            bundle_easy.putString("level_str", this.tablelist_str);
            intent_easy.putExtras(bundle_easy);
            startActivity(intent_easy);
        } else if (this.mode.equals("normal") && !String.valueOf(this.tablelist_str.charAt(index)).equals("0")) {
            playClick();
            Intent intent_normal = new Intent(getApplicationContext(), LevelNormal.class);
            Bundle bundle_normal = new Bundle();
            bundle_normal.putString(LevelConstants.TAG_LEVEL, String.valueOf(index + 1));
            bundle_normal.putString("level_str", this.tablelist_str);
            intent_normal.putExtras(bundle_normal);
            startActivity(intent_normal);
        } else if (this.mode.equals("hard") && !String.valueOf(this.tablelist_str.charAt(index)).equals("0")) {
            playClick();
            Intent intent_hard = new Intent(getApplicationContext(), LevelHard.class);
            Bundle bundle_hard = new Bundle();
            bundle_hard.putString(LevelConstants.TAG_LEVEL, String.valueOf(index + 1));
            bundle_hard.putString("level_str", this.tablelist_str);
            intent_hard.putExtras(bundle_hard);
            startActivity(intent_hard);
        } else if (this.mode.equals("easy_challenges")) {
            playClick();
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            if (index != 0) {
                dialog.setMessage(easy_challenge_arr[index]);
            } else if (this.CAMERA_WIDTH <= 480) {
                dialog.setMessage(easy_challenge_arr_first[0]);
            } else if (this.CAMERA_WIDTH <= 534) {
                dialog.setMessage(easy_challenge_arr_first[1]);
            } else {
                dialog.setMessage(easy_challenge_arr_first[2]);
            }
            dialog.setCancelable(true);
            dialog.setPositiveButton("Start", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent_normal = new Intent(Main.this.getApplicationContext(), ChallengeEasy.class);
                    Bundle bundle_normal = new Bundle();
                    bundle_normal.putString(LevelConstants.TAG_LEVEL, String.valueOf(index + 1));
                    bundle_normal.putString("level_str", Main.this.tablelist_str);
                    intent_normal.putExtras(bundle_normal);
                    Main.this.startActivity(intent_normal);
                    dialog.cancel();
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            dialog.show();
        } else if (this.mode.equals("normal_challenges")) {
            playClick();
            AlertDialog.Builder dialog2 = new AlertDialog.Builder(this);
            dialog2.setMessage(normal_challenge_arr[index]);
            dialog2.setCancelable(true);
            dialog2.setPositiveButton("Start", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent_normal = new Intent(Main.this.getApplicationContext(), ChallengeNormal.class);
                    Bundle bundle_normal = new Bundle();
                    bundle_normal.putString(LevelConstants.TAG_LEVEL, String.valueOf(index + 1));
                    bundle_normal.putString("level_str", Main.this.tablelist_str);
                    intent_normal.putExtras(bundle_normal);
                    Main.this.startActivity(intent_normal);
                    dialog.cancel();
                }
            });
            dialog2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            dialog2.show();
        } else if (this.mode.equals("hard_challenges")) {
            playClick();
            AlertDialog.Builder dialog3 = new AlertDialog.Builder(this);
            dialog3.setMessage(hard_challenge_arr[index]);
            dialog3.setCancelable(true);
            dialog3.setPositiveButton("Start", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent_normal = new Intent(Main.this.getApplicationContext(), ChallengeHard.class);
                    Bundle bundle_normal = new Bundle();
                    bundle_normal.putString(LevelConstants.TAG_LEVEL, String.valueOf(index + 1));
                    bundle_normal.putString("level_str", Main.this.tablelist_str);
                    intent_normal.putExtras(bundle_normal);
                    Main.this.startActivity(intent_normal);
                    dialog.cancel();
                }
            });
            dialog3.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            dialog3.show();
        }
    }

    /* access modifiers changed from: private */
    public void updateMainlist() {
        this.level_str = this.dbadapter.fetchNote().getString(1);
        for (int i = 0; i < this.level_str.length(); i++) {
            if (this.level_str.charAt(i) == '1') {
                this.lock_array[i] = 1;
            } else {
                this.lock_array[i] = 0;
            }
            switch (i) {
                case 0:
                    if (this.lock_array[i] != 1) {
                        this.button_easy_levels.setBackgroundResource(R.drawable.button_back_little_lock);
                        this.button_easy_levels.setText("");
                        break;
                    } else {
                        this.button_easy_levels.setBackgroundResource(R.drawable.button_back_little);
                        this.button_easy_levels.setText(text_arr[i]);
                        break;
                    }
                case 1:
                    if (this.lock_array[i] != 1) {
                        this.button_normal_levels.setBackgroundResource(R.drawable.button_back_little_lock);
                        this.button_normal_levels.setText("");
                        break;
                    } else {
                        this.button_normal_levels.setBackgroundResource(R.drawable.button_back_little);
                        this.button_normal_levels.setText(text_arr[i]);
                        break;
                    }
                case 2:
                    if (this.lock_array[i] != 1) {
                        this.button_hard_levels.setBackgroundResource(R.drawable.button_back_little_lock);
                        this.button_hard_levels.setText("");
                        break;
                    } else {
                        this.button_hard_levels.setBackgroundResource(R.drawable.button_back_little);
                        this.button_hard_levels.setText(text_arr[i]);
                        break;
                    }
                case 3:
                    if (this.lock_array[i] != 1) {
                        this.button_easy_challenges.setBackgroundResource(R.drawable.button_back_little_lock);
                        this.button_easy_challenges.setText("");
                        break;
                    } else {
                        this.button_easy_challenges.setBackgroundResource(R.drawable.button_back_little);
                        this.button_easy_challenges.setText(text_arr[i]);
                        break;
                    }
                case 4:
                    if (this.lock_array[i] != 1) {
                        this.button_normal_challenges.setBackgroundResource(R.drawable.button_back_little_lock);
                        this.button_normal_challenges.setText("");
                        break;
                    } else {
                        this.button_normal_challenges.setBackgroundResource(R.drawable.button_back_little);
                        this.button_normal_challenges.setText(text_arr[i]);
                        break;
                    }
                case Transform.COL2_Y:
                    if (this.lock_array[i] != 1) {
                        this.button_hard_challenges.setBackgroundResource(R.drawable.button_back_little_lock);
                        this.button_hard_challenges.setText("");
                        break;
                    } else {
                        this.button_hard_challenges.setBackgroundResource(R.drawable.button_back_little);
                        this.button_hard_challenges.setText(text_arr[i]);
                        break;
                    }
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateLevelTable() {
        if (!this.mode.equals("")) {
            Cursor d = this.dbadapter.fetchNote();
            if (this.mode.equals("easy")) {
                this.tablelist_str = d.getString(2);
            } else if (this.mode.equals("normal")) {
                this.tablelist_str = d.getString(3);
            } else if (this.mode.equals("hard")) {
                this.tablelist_str = d.getString(4);
            } else if (this.mode.equals("easy_challenges")) {
                this.tablelist_str = d.getString(5);
            } else if (this.mode.equals("normal_challenges")) {
                this.tablelist_str = d.getString(6);
            } else if (this.mode.equals("hard_challenges")) {
                this.tablelist_str = d.getString(7);
            }
            this.gridview.setAdapter((ListAdapter) null);
            this.gridview.setAdapter((ListAdapter) new ButtonAdapter(this));
        }
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        MyGestureDetector() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                    return false;
                }
                if (Main.this.is_animating || e1.getX() - e2.getX() <= 60.0f || Math.abs(velocityX) <= 200.0f) {
                    if (!Main.this.is_animating && e2.getX() - e1.getX() > 60.0f && Math.abs(velocityX) > 200.0f) {
                        Main.this.is_animating = true;
                        Main.this.vf.setInAnimation(Main.this.slideRightIn);
                        Main.this.vf.setOutAnimation(Main.this.slideRightOut);
                        Main.this.vf.showPrevious();
                    }
                    return false;
                }
                Main.this.is_animating = true;
                Main.this.vf.setInAnimation(Main.this.slideLeftIn);
                Main.this.vf.setOutAnimation(Main.this.slideLeftOut);
                Main.this.vf.showNext();
                return false;
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void setPoints() {
        this.circ1.setBackgroundResource(R.drawable.circle_little);
        this.circ2.setBackgroundResource(R.drawable.circle_little);
        this.circ3.setBackgroundResource(R.drawable.circle_little);
        this.circ4.setBackgroundResource(R.drawable.circle_little);
        this.circ5.setBackgroundResource(R.drawable.circle_little);
        this.circ6.setBackgroundResource(R.drawable.circle_little);
        this.circ7.setBackgroundResource(R.drawable.circle_little);
        switch (this.vf.getDisplayedChild()) {
            case 0:
                this.circ1.setBackgroundResource(R.drawable.circle_big);
                return;
            case 1:
                this.circ2.setBackgroundResource(R.drawable.circle_big);
                return;
            case 2:
                this.circ3.setBackgroundResource(R.drawable.circle_big);
                return;
            case 3:
                this.circ4.setBackgroundResource(R.drawable.circle_big);
                return;
            case 4:
                this.circ5.setBackgroundResource(R.drawable.circle_big);
                return;
            case Transform.COL2_Y:
                this.circ6.setBackgroundResource(R.drawable.circle_big);
                return;
            case 6:
                this.circ7.setBackgroundResource(R.drawable.circle_big);
                return;
            default:
                return;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void askDelete() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Reset game?");
        dialog.setCancelable(true);
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                Main.this.dbadapter.resetData();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mp.release();
        this.dbadapter.close();
        this.dbadapter = null;
        super.onDestroy();
        System.gc();
    }

    /* access modifiers changed from: private */
    public void addShare() {
        Intent share = new Intent("android.intent.action.SEND");
        share.setType("text/plain");
        share.putExtra("android.intent.extra.TEXT", "I just played a cool android game: The filler\nits free: https://market.android.com/details?id=com.filler");
        startActivity(Intent.createChooser(share, "Share with:"));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.main_flipper.getDisplayedChild() == 0) {
            System.exit(0);
        } else if (this.main_flipper.getDisplayedChild() == 3) {
            playClick();
            this.main_flipper.showNext();
        } else {
            playClick();
            this.main_flipper.showPrevious();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void playClick() {
        if (this.sound_enabled) {
            this.mp.start();
        }
    }
}
