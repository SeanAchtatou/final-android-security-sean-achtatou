package com.filler;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Transform;
import java.io.IOException;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.AutoParallaxBackground;
import org.anddev.andengine.entity.scene.background.ParallaxBackground;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.sprite.batch.SpriteBatch;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.extension.physics.box2d.PhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.util.Vector2Pool;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.MathUtils;

public class ChallengeNormal extends BaseGameActivity implements Scene.IOnSceneTouchListener {
    private static final FixtureDef BOUNCE_DEF = PhysicsFactory.createFixtureDef(1.0f, 1.0f, 0.1f);
    private static int CAMERA_HEIGHT = 0;
    private static int CAMERA_WIDTH = 0;
    private static final FixtureDef NEW_BALL_DEF = PhysicsFactory.createFixtureDef(1.0f, 0.3f, 0.1f);
    private static final FixtureDef WALL_DEF = PhysicsFactory.createFixtureDef(0.0f, 0.0f, 0.0f);
    private static final float grow = 0.01f;
    private static final float speed = 4.5f;
    /* access modifiers changed from: private */
    public Shape[] addShapes;
    private SharedPreferences app_preferences;
    private double areaterritory;
    private double areaterritory_half;
    private TextureRegion background;
    private BitmapTextureAtlas backgroundTexture;
    private Sound badSound;
    /* access modifiers changed from: private */
    public int ballAdded = 4;
    private boolean ball_removed = false;
    private boolean can_add_white = true;
    private String challange_mode;
    /* access modifiers changed from: private */
    public Sprite circ;
    /* access modifiers changed from: private */
    public boolean circ_on = false;
    private DbAdapter dbadapter;
    /* access modifiers changed from: private */
    public String error_msg;
    /* access modifiers changed from: private */
    public float global_x;
    /* access modifiers changed from: private */
    public float global_y;
    private TextureRegion growBall_orange;
    private TextureRegion growBall_white;
    private int[] grow_arr = {1, 44, 2, 35, 3, 33, 4, 30, 5, 28, 6, 25};
    private double grow_goal;
    private HUD hud;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int level;
    public String level_str;
    private int[] limited_arr = {1, 3, 3, 4, 5, 5, 7, 6, 9, 7, 11, 8};
    private int limited_index;
    private Sound lostSound;
    private Font mFont;
    private PhysicsWorld mPhysicsWorld;
    private BitmapTextureAtlas mTexture;
    private int maxBallCanAdd_orange;
    private int maxBallCanAdd_white;
    private int minus_h;
    private int minus_w;
    private TextureRegion movingBall;
    /* access modifiers changed from: private */
    public int movingBallCount;
    /* access modifiers changed from: private */
    public Body[] movingBodys;
    /* access modifiers changed from: private */
    public Shape[] movingShapes;
    private boolean nopop = false;
    private int[] nopop_arr = {3, 5, 7, 9, 11, 13};
    private int nopop_index;
    private Double num;
    private Double num2;
    private float obj1w;
    private float obj1x;
    private float obj1y;
    private float obj2w;
    private float obj2x;
    private float obj2y;
    private TextureRegion orange_ball_left_back;
    private int orange_ball_left_region = 30;
    private ChangeableText orange_ball_left_text;
    private int selected_color = 1;
    private boolean sound_enabled;
    /* access modifiers changed from: private */
    public float speedx;
    /* access modifiers changed from: private */
    public float speedy;
    private TextureRegion status_back;
    private int status_region = 60;
    private ChangeableText status_text;
    private double territory;
    private double territory2;
    private BitmapTextureAtlas textTexture;
    private TextureRegion white_ball_left_back;
    private int white_ball_left_region = 30;
    private ChangeableText white_ball_left_text;
    private Sound winSound;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        Bundle bundle = getIntent().getExtras();
        this.level = Integer.valueOf(bundle.getString(LevelConstants.TAG_LEVEL)).intValue();
        this.level_str = bundle.getString("level_str");
        if (this.level < 7) {
            this.challange_mode = "grow";
            this.maxBallCanAdd_white = 1;
            this.maxBallCanAdd_orange = 1;
            this.movingBallCount = this.level;
            this.grow_goal = (double) this.grow_arr[(this.level * 2) - 1];
        } else if (this.level < 13) {
            this.challange_mode = "nopop";
            this.nopop_index = this.nopop_arr[this.level - 7];
            this.movingBallCount = this.nopop_index;
            this.maxBallCanAdd_white = (this.nopop_index - 1) + 5;
            this.maxBallCanAdd_orange = (this.nopop_index - 1) + 5;
            this.nopop = true;
        } else {
            this.challange_mode = "limited";
            this.limited_index = this.level - 13;
            this.movingBallCount = this.limited_arr[this.limited_index * 2];
            this.maxBallCanAdd_white = this.limited_arr[(this.limited_index * 2) + 1];
            this.maxBallCanAdd_orange = this.limited_arr[(this.limited_index * 2) + 1];
        }
        this.app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.sound_enabled = this.app_preferences.getBoolean("sound_enabled", false);
        this.dbadapter = new DbAdapter(getApplicationContext());
        this.dbadapter.open();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.dbadapter.close();
        this.dbadapter = null;
        this.movingShapes = null;
        this.movingBodys = null;
        super.onDestroy();
        System.gc();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 0, 0, "Restart level").setIcon(17301581);
        if (this.sound_enabled) {
            menu.add(0, 1, 1, "Sound off").setIcon(17301553);
        } else {
            menu.add(0, 1, 1, "Sound on").setIcon(17301554);
        }
        menu.add(0, 2, 2, "Exit level").setIcon(17301580);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                this.mEngine.stop();
                updateLevel();
                break;
            case 1:
                if (this.sound_enabled) {
                    this.sound_enabled = false;
                } else {
                    this.sound_enabled = true;
                }
                SharedPreferences.Editor editor = this.app_preferences.edit();
                editor.putBoolean("sound_enabled", this.sound_enabled);
                editor.commit();
                break;
            case 2:
                finish();
                break;
        }
        return false;
    }

    public Engine onLoadEngine() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int mWidth = dm.widthPixels;
        int mHeight = dm.heightPixels;
        int rotation = getWindowManager().getDefaultDisplay().getOrientation();
        if (rotation == 1 || rotation == 3) {
            CAMERA_WIDTH = (int) (((float) mWidth) / dm.density);
            CAMERA_HEIGHT = (int) (((float) mHeight) / dm.density);
        } else {
            CAMERA_WIDTH = (int) (((float) mHeight) / dm.density);
            CAMERA_HEIGHT = (int) (((float) mWidth) / dm.density);
        }
        this.areaterritory = Math.floor(((double) (CAMERA_WIDTH * CAMERA_HEIGHT)) * 0.6d);
        this.areaterritory_half = this.areaterritory / 2.0d;
        Camera camera = new Camera(0.0f, 0.0f, (float) CAMERA_WIDTH, (float) CAMERA_HEIGHT);
        this.hud = new HUD();
        camera.setHUD(this.hud);
        EngineOptions engineOptions = new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy((float) CAMERA_WIDTH, (float) CAMERA_HEIGHT), camera).setNeedsSound(true);
        engineOptions.getTouchOptions().setRunOnUpdateThread(true);
        return new Engine(engineOptions);
    }

    public void onLoadResources() {
        this.mTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.textTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        this.growBall_white = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "grow_ball_white.png", 0, 0);
        this.growBall_orange = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "grow_ball_orange.png", 481, 0);
        this.white_ball_left_back = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "grow_ball_white_left_30.png", 962, 0);
        this.orange_ball_left_back = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "grow_ball_orange_left_30.png", 993, 0);
        this.movingBall = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "moving_ball_24.png", 0, 481);
        this.status_back = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mTexture, this, "status_60.png", 0, 506);
        this.backgroundTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.DEFAULT);
        this.background = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.backgroundTexture, this, "main_back_big.jpg", 0, 0);
        this.mFont = new Font(this.textTexture, Typeface.create(Typeface.DEFAULT_BOLD, 1), 16.0f, true, -65536);
        SoundFactory.setAssetBasePath("mfx/");
        try {
            this.badSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "bad.mp3");
            this.winSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "win.mp3");
            this.lostSound = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "lost.mp3");
        } catch (IOException e) {
        }
        this.mFont = new Font(this.textTexture, Typeface.create(Typeface.DEFAULT_BOLD, 1), 16.0f, true, -65536);
        this.mEngine.getTextureManager().loadTextures(this.mTexture, this.textTexture, this.backgroundTexture);
        this.mEngine.getFontManager().loadFont(this.mFont);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.MathUtils.random(float, float):float
     arg types: [int, int]
     candidates:
      org.anddev.andengine.util.MathUtils.random(int, int):int
      org.anddev.andengine.util.MathUtils.random(float, float):float */
    public Scene onLoadScene() {
        Scene scene = new Scene();
        AutoParallaxBackground autoParallaxBackground = new AutoParallaxBackground(0.0f, 0.0f, 0.0f, 0.0f);
        autoParallaxBackground.attachParallaxEntity(new ParallaxBackground.ParallaxEntity(0.0f, new Sprite(0.0f, (float) (CAMERA_HEIGHT - this.background.getHeight()), this.background)));
        scene.setBackground(autoParallaxBackground);
        scene.setOnSceneTouchListener(this);
        this.mPhysicsWorld = new FixedStepPhysicsWorld(25, new Vector2(0.0f, 9.80665f), false);
        Sprite sprite = new Sprite(0.0f, 0.0f, this.white_ball_left_back);
        Sprite sprite2 = new Sprite((float) this.orange_ball_left_region, 0.0f, this.orange_ball_left_back);
        Sprite sprite3 = new Sprite((float) (CAMERA_WIDTH - this.status_region), (float) (CAMERA_HEIGHT - this.status_region), this.status_back);
        this.white_ball_left_text = new ChangeableText(1.0f, 1.0f, this.mFont, String.valueOf(this.maxBallCanAdd_white), 2);
        this.orange_ball_left_text = new ChangeableText(1.0f, 1.0f, this.mFont, String.valueOf(this.maxBallCanAdd_orange), 2);
        this.status_text = new ChangeableText(1.0f, 1.0f, this.mFont, "0%", 4);
        updateText();
        this.hud.attachChild(sprite3);
        this.hud.attachChild(sprite);
        this.hud.attachChild(sprite2);
        this.hud.attachChild(this.white_ball_left_text);
        this.hud.attachChild(this.orange_ball_left_text);
        this.hud.attachChild(this.status_text);
        this.addShapes = new Shape[(this.ballAdded + this.maxBallCanAdd_white + this.maxBallCanAdd_orange)];
        Rectangle rectangle = new Rectangle(0.0f, (float) CAMERA_HEIGHT, (float) CAMERA_WIDTH, 1.0f);
        this.addShapes[0] = rectangle;
        Rectangle rectangle2 = new Rectangle(0.0f, -1.0f, (float) CAMERA_WIDTH, 1.0f);
        this.addShapes[1] = rectangle2;
        Rectangle rectangle3 = new Rectangle(-1.0f, 0.0f, 1.0f, (float) CAMERA_HEIGHT);
        this.addShapes[2] = rectangle3;
        Rectangle rectangle4 = new Rectangle((float) CAMERA_WIDTH, 0.0f, 1.0f, (float) CAMERA_HEIGHT);
        this.addShapes[3] = rectangle4;
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle, BodyDef.BodyType.StaticBody, WALL_DEF);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle2, BodyDef.BodyType.StaticBody, WALL_DEF);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle3, BodyDef.BodyType.StaticBody, WALL_DEF);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, rectangle4, BodyDef.BodyType.StaticBody, WALL_DEF);
        scene.attachChild(rectangle);
        scene.attachChild(rectangle2);
        scene.attachChild(rectangle3);
        scene.attachChild(rectangle4);
        this.movingShapes = new Shape[this.movingBallCount];
        this.movingBodys = new Body[this.movingBallCount];
        SpriteBatch spriteBatch = new SpriteBatch(this.mTexture, this.movingBallCount);
        this.i = 0;
        while (this.i < this.movingBallCount) {
            float xPos = MathUtils.random(30.0f, ((float) CAMERA_WIDTH) - 30.0f);
            float yPos = MathUtils.random(30.0f, ((float) CAMERA_HEIGHT) - 30.0f);
            float xDir = MathUtils.random(-1.0f, 1.0f);
            float yDir = MathUtils.random(-1.0f, 1.0f);
            Sprite sprite4 = new Sprite(xPos, yPos, this.movingBall);
            Body body = PhysicsFactory.createCircleBody(this.mPhysicsWorld, sprite4, BodyDef.BodyType.DynamicBody, BOUNCE_DEF);
            Vector2 velocity = Vector2Pool.obtain(speed * xDir, speed * yDir);
            body.setLinearVelocity(velocity);
            Vector2Pool.recycle(velocity);
            spriteBatch.attachChild(sprite4);
            this.movingShapes[this.i] = sprite4;
            this.movingBodys[this.i] = body;
            this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite4, body, true, false));
            this.i = this.i + 1;
        }
        spriteBatch.submit();
        scene.attachChild(spriteBatch);
        scene.registerUpdateHandler(this.mPhysicsWorld);
        scene.registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                ChallengeNormal.this.k = ChallengeNormal.this.movingBallCount - 1;
                while (ChallengeNormal.this.k >= 0) {
                    Vector2 vec = ChallengeNormal.this.movingBodys[ChallengeNormal.this.k].getLinearVelocity();
                    if (vec.x < 0.0f) {
                        ChallengeNormal.this.speedx = -4.5f;
                    } else {
                        ChallengeNormal.this.speedx = ChallengeNormal.speed;
                    }
                    if (vec.y < 0.0f) {
                        ChallengeNormal.this.speedy = -4.5f;
                    } else {
                        ChallengeNormal.this.speedy = ChallengeNormal.speed;
                    }
                    Vector2 velocity = Vector2Pool.obtain(ChallengeNormal.this.speedx, ChallengeNormal.this.speedy);
                    ChallengeNormal.this.movingBodys[ChallengeNormal.this.k].setLinearVelocity(velocity);
                    Vector2Pool.recycle(velocity);
                    ChallengeNormal challengeNormal = ChallengeNormal.this;
                    challengeNormal.k = challengeNormal.k - 1;
                }
                if (ChallengeNormal.this.circ_on) {
                    ChallengeNormal.this.circ.setScale(ChallengeNormal.this.circ.getScaleX() + ChallengeNormal.grow);
                    try {
                        ChallengeNormal.this.i = ChallengeNormal.this.ballAdded - 1;
                        while (ChallengeNormal.this.i >= 0) {
                            if (ChallengeNormal.this.i < 4) {
                                if (ChallengeNormal.this.circ_on && ChallengeNormal.this.addShapes[ChallengeNormal.this.i].collidesWith(ChallengeNormal.this.circ)) {
                                    ChallengeNormal.this.addBall(ChallengeNormal.this.global_x, ChallengeNormal.this.global_y, ChallengeNormal.this.circ);
                                }
                            } else if (ChallengeNormal.this.circ_on && ChallengeNormal.this.computeDistance(ChallengeNormal.this.addShapes[ChallengeNormal.this.i], ChallengeNormal.this.circ)) {
                                ChallengeNormal.this.addBall(ChallengeNormal.this.global_x, ChallengeNormal.this.global_y, ChallengeNormal.this.circ);
                            }
                            ChallengeNormal challengeNormal2 = ChallengeNormal.this;
                            challengeNormal2.i = challengeNormal2.i - 1;
                        }
                        if (ChallengeNormal.this.circ_on) {
                            ChallengeNormal.this.j = ChallengeNormal.this.movingBallCount - 1;
                            while (ChallengeNormal.this.j >= 0) {
                                if (ChallengeNormal.this.circ_on && ChallengeNormal.this.computeDistanceMove(ChallengeNormal.this.movingShapes[ChallengeNormal.this.j], ChallengeNormal.this.circ)) {
                                    ChallengeNormal.this.removeBall(ChallengeNormal.this.circ);
                                }
                                ChallengeNormal challengeNormal3 = ChallengeNormal.this;
                                challengeNormal3.j = challengeNormal3.j - 1;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            public void reset() {
            }
        });
        return scene;
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (this.mPhysicsWorld != null) {
            if (pSceneTouchEvent.isActionDown()) {
                this.global_x = pSceneTouchEvent.getX();
                this.global_y = pSceneTouchEvent.getY();
                if (this.can_add_white) {
                    Scene scene = this.mEngine.getScene();
                    this.circ = new Sprite(this.global_x, this.global_y, this.growBall_white);
                    this.maxBallCanAdd_white--;
                    this.circ.setScale(0.1f);
                    this.circ.setPosition(this.global_x - (this.circ.getWidth() / 2.0f), this.global_y - (this.circ.getWidth() / 2.0f));
                    scene.attachChild(this.circ);
                    this.circ_on = true;
                    this.white_ball_left_text.setText(String.valueOf(this.maxBallCanAdd_white));
                } else {
                    Scene scene2 = this.mEngine.getScene();
                    this.circ = new Sprite(this.global_x, this.global_y, this.growBall_orange);
                    this.maxBallCanAdd_orange--;
                    this.circ.setScale(0.1f);
                    this.circ.setPosition(this.global_x - (this.circ.getWidth() / 2.0f), this.global_y - (this.circ.getWidth() / 2.0f));
                    scene2.attachChild(this.circ);
                    this.orange_ball_left_text.setText(String.valueOf(this.maxBallCanAdd_orange));
                    this.circ_on = true;
                    this.selected_color = 2;
                }
                updateText();
                return true;
            } else if (pSceneTouchEvent.isActionMove()) {
                if (this.circ_on) {
                    this.global_x = pSceneTouchEvent.getX();
                    this.global_y = pSceneTouchEvent.getY();
                    this.circ.setPosition(this.global_x - (this.circ.getWidth() / 2.0f), this.global_y - (this.circ.getWidth() / 2.0f));
                }
            } else if (pSceneTouchEvent.isActionUp() && this.circ_on) {
                addBall(pSceneTouchEvent.getX(), pSceneTouchEvent.getY(), this.circ);
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void addBall(float pX, float pY, Sprite c) {
        Sprite face;
        Scene scene = this.mEngine.getScene();
        scene.detachChild(c);
        if (this.selected_color == 1) {
            face = new Sprite(pX - (c.getWidth() / 2.0f), pY - (c.getWidth() / 2.0f), this.growBall_white);
        } else {
            face = new Sprite(pX - (c.getWidth() / 2.0f), pY - (c.getWidth() / 2.0f), this.growBall_orange);
        }
        face.setScale(c.getScaleX());
        Body body = PhysicsFactory.createCircleBody(this.mPhysicsWorld, face, BodyDef.BodyType.DynamicBody, NEW_BALL_DEF);
        scene.attachChild(face);
        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(face, body, true, false));
        this.addShapes[this.ballAdded] = face;
        this.ballAdded++;
        this.circ_on = false;
        if (!this.challange_mode.equals("grow")) {
            this.territory += Math.floor(Math.pow((double) ((c.getWidth() * c.getScaleX()) / 2.0f), 2.0d) * 3.141592653589793d);
        } else if (this.selected_color == 1) {
            this.territory += Math.floor(Math.pow((double) ((c.getWidth() * c.getScaleX()) / 2.0f), 2.0d) * 3.141592653589793d);
        } else {
            this.territory2 += Math.floor(Math.pow((double) ((c.getWidth() * c.getScaleX()) / 2.0f), 2.0d) * 3.141592653589793d);
        }
        this.status_text.setText(Integer.valueOf((int) Math.floor(((this.territory + this.territory2) / this.areaterritory) * 100.0d)) + "%");
        updateText();
        checkState();
    }

    /* access modifiers changed from: private */
    public void removeBall(Sprite c) {
        this.mEngine.getScene().detachChild(c);
        this.circ_on = false;
        if (this.nopop) {
            gameOver(4);
            return;
        }
        this.ball_removed = true;
        checkState();
    }

    private void checkState() {
        if (this.challange_mode.equals("grow")) {
            if (this.can_add_white) {
                if (((double) Integer.valueOf((int) Math.floor((this.territory / this.areaterritory) * 100.0d)).intValue()) >= this.grow_goal) {
                    this.can_add_white = false;
                } else {
                    gameOver(2);
                }
            } else if (((double) Integer.valueOf((int) Math.floor((this.territory2 / this.areaterritory) * 100.0d)).intValue()) >= this.grow_goal) {
                updateDb();
                challengeComplete();
            } else {
                gameOver(3);
            }
        } else if (this.can_add_white) {
            if (this.maxBallCanAdd_white == 0 && this.territory < this.areaterritory_half) {
                gameOver(5);
            } else if (this.territory >= this.areaterritory_half) {
                this.can_add_white = false;
                this.territory = this.areaterritory_half;
                this.status_text.setText(Integer.valueOf((int) Math.floor((this.territory / this.areaterritory) * 100.0d)) + "%");
                updateText();
            } else if (this.ball_removed) {
                this.ball_removed = false;
                playSound("bad");
            }
        } else if (this.maxBallCanAdd_orange == 0 && this.territory < this.areaterritory) {
            gameOver(1);
        } else if (this.territory >= this.areaterritory) {
            updateDb();
            challengeComplete();
        } else if (this.ball_removed) {
            this.ball_removed = false;
            playSound("bad");
        }
    }

    private void gameOver(int type) {
        switch (type) {
            case 1:
                this.error_msg = " No balls left! ";
                break;
            case 2:
                this.error_msg = "Not reached the size of " + this.grow_goal + "% with white!";
                break;
            case 3:
                this.error_msg = "Not reached the size of " + this.grow_goal + "% with orange!";
                break;
            case 4:
                this.error_msg = "You popped a ball!";
                break;
            case Transform.COL2_Y:
                this.error_msg = " Not completed 50% with white! ";
                break;
        }
        playSound("lost");
        this.mEngine.stop();
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ChallengeNormal.this);
                dialog.setMessage(ChallengeNormal.this.error_msg);
                dialog.setCancelable(true);
                dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ChallengeNormal.this.updateLevel();
                        dialog.cancel();
                    }
                });
                dialog.setNegativeButton("Menu", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ChallengeNormal.this.finish();
                    }
                });
                dialog.show();
            }
        });
    }

    private void challengeComplete() {
        playSound("win");
        this.mEngine.stop();
        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ChallengeNormal.this);
                dialog.setMessage(" Challenge " + ChallengeNormal.this.level + " complete! ");
                dialog.setCancelable(true);
                dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ChallengeNormal.this.updateLevel();
                        dialog.cancel();
                    }
                });
                dialog.setNegativeButton("Menu", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ChallengeNormal.this.finish();
                    }
                });
                dialog.show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateLevel() {
        this.mEngine.start();
        this.hud.detachChildren();
        this.hud = null;
        this.hud = new HUD();
        this.mEngine.getCamera().setHUD(this.hud);
        this.mEngine.clearUpdateHandlers();
        this.mPhysicsWorld.clearPhysicsConnectors();
        this.mPhysicsWorld.dispose();
        this.mPhysicsWorld = null;
        if (this.challange_mode.equals("grow")) {
            this.maxBallCanAdd_white = 1;
            this.maxBallCanAdd_orange = 1;
            this.movingBallCount = this.level;
        } else if (this.challange_mode.equals("nopop")) {
            this.movingBallCount = this.nopop_index;
            this.maxBallCanAdd_white = (this.nopop_index - 1) + 5;
            this.maxBallCanAdd_orange = (this.nopop_index - 1) + 5;
        } else {
            this.movingBallCount = this.limited_arr[this.limited_index * 2];
            this.maxBallCanAdd_white = this.limited_arr[(this.limited_index * 2) + 1];
            this.maxBallCanAdd_orange = this.limited_arr[(this.limited_index * 2) + 1];
        }
        this.selected_color = 1;
        this.can_add_white = true;
        this.territory = 0.0d;
        this.territory2 = 0.0d;
        this.ballAdded = 4;
        this.addShapes = null;
        this.movingShapes = null;
        this.movingBodys = null;
        this.mEngine.setScene(null);
        this.mEngine.setScene(onLoadScene());
    }

    private void updateText() {
        this.minus_w = ((int) (((float) this.white_ball_left_region) - this.white_ball_left_text.getWidth())) / 2;
        this.minus_h = ((int) (((float) this.white_ball_left_region) - this.white_ball_left_text.getHeight())) / 2;
        this.white_ball_left_text.setPosition((float) this.minus_w, (float) this.minus_h);
        this.minus_w = ((int) (((float) this.orange_ball_left_region) - this.orange_ball_left_text.getWidth())) / 2;
        this.minus_h = ((int) (((float) this.orange_ball_left_region) - this.orange_ball_left_text.getHeight())) / 2;
        this.orange_ball_left_text.setPosition((float) (this.minus_w + this.orange_ball_left_region), (float) this.minus_h);
        this.minus_w = ((int) (((float) this.status_region) - this.status_text.getWidth())) / 2;
        this.minus_h = ((int) (((float) this.status_region) - this.status_text.getHeight())) / 2;
        this.status_text.setPosition((float) ((CAMERA_WIDTH - this.status_region) + this.minus_w), (float) ((CAMERA_HEIGHT - this.status_region) + this.minus_h));
    }

    private void updateDb() {
        this.level_str = replaceCharAt(this.level_str, this.level - 1, '2');
        this.dbadapter.updateNote(DbAdapter.NORMALCHALLENGE, this.level_str);
    }

    private void playSound(String mode) {
        if (!this.sound_enabled) {
            return;
        }
        if (mode.equals("win")) {
            this.winSound.play();
        } else if (mode.equals("lost")) {
            this.lostSound.play();
        } else {
            this.badSound.play();
        }
    }

    /* access modifiers changed from: protected */
    public boolean computeDistance(Shape obj1, Shape obj2) {
        this.obj1x = obj1.getX();
        this.obj1y = obj1.getY();
        this.obj2x = obj2.getX();
        this.obj2y = obj2.getY();
        this.num = Double.valueOf(Math.floor(Math.sqrt(Math.pow((double) (this.obj2x - this.obj1x), 2.0d) + Math.pow((double) (this.obj2y - this.obj1y), 2.0d))));
        this.num2 = Double.valueOf(Math.floor((double) (((obj1.getWidth() * obj1.getScaleX()) + (obj2.getWidth() * obj2.getScaleX())) / 2.0f)));
        if (this.num.doubleValue() < this.num2.doubleValue()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean computeDistanceMove(Shape obj1, Shape obj2) {
        this.obj1x = obj1.getX();
        this.obj1y = obj1.getY();
        this.obj1w = obj1.getWidth();
        this.obj2x = obj2.getX();
        this.obj2y = obj2.getY();
        this.obj2w = obj2.getWidth();
        this.num = Double.valueOf(Math.floor(Math.sqrt(Math.pow((double) ((this.obj2x + (this.obj2w / 2.0f)) - this.obj1x), 2.0d) + Math.pow((double) ((this.obj2y + (this.obj2w / 2.0f)) - this.obj1y), 2.0d))));
        this.num2 = Double.valueOf(Math.floor((double) (((this.obj1w * obj1.getScaleX()) / 2.0f) + ((this.obj2w * obj2.getScaleX()) / 2.0f))));
        if (this.num.doubleValue() < this.num2.doubleValue()) {
            return true;
        }
        return false;
    }

    public static String replaceCharAt(String s, int pos, char c) {
        StringBuffer buf = new StringBuffer(s);
        buf.setCharAt(pos, c);
        return buf.toString();
    }

    public void onLoadComplete() {
    }
}
