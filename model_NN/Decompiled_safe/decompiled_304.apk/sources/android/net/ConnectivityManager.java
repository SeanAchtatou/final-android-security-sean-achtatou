package android.net;

import android.os.Binder;
import android.os.RemoteException;

public class ConnectivityManager {
    public static final String ACTION_BACKGROUND_DATA_SETTING_CHANGED = "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED";
    public static final String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    public static final int DEFAULT_NETWORK_PREFERENCE = 1;
    public static final String EXTRA_EXTRA_INFO = "extraInfo";
    public static final String EXTRA_IS_FAILOVER = "isFailover";
    public static final String EXTRA_NETWORK_INFO = "networkInfo";
    public static final String EXTRA_NO_CONNECTIVITY = "noConnectivity";
    public static final String EXTRA_OTHER_NETWORK_INFO = "otherNetwork";
    public static final String EXTRA_REASON = "reason";
    public static final int MAX_NETWORK_TYPE = 5;
    public static final int MAX_RADIO_TYPE = 1;
    public static final int TYPE_MOBILE = 0;
    public static final int TYPE_MOBILE_DUN = 4;
    public static final int TYPE_MOBILE_HIPRI = 5;
    public static final int TYPE_MOBILE_MMS = 2;
    public static final int TYPE_MOBILE_SUPL = 3;
    public static final int TYPE_WIFI = 1;
    private IConnectivityManager mService;

    private ConnectivityManager() {
    }

    public ConnectivityManager(IConnectivityManager iConnectivityManager) {
        if (iConnectivityManager == null) {
            throw new IllegalArgumentException("ConnectivityManager() cannot be constructed with null service");
        }
        this.mService = iConnectivityManager;
    }

    public static boolean isNetworkTypeValid(int i) {
        return i >= 0 && i <= 5;
    }

    public NetworkInfo getActiveNetworkInfo() {
        try {
            return this.mService.getActiveNetworkInfo();
        } catch (RemoteException e) {
            return null;
        }
    }

    public NetworkInfo[] getAllNetworkInfo() {
        try {
            return this.mService.getAllNetworkInfo();
        } catch (RemoteException e) {
            return null;
        }
    }

    public boolean getBackgroundDataSetting() {
        try {
            return this.mService.getBackgroundDataSetting();
        } catch (RemoteException e) {
            return false;
        }
    }

    public NetworkInfo getNetworkInfo(int i) {
        try {
            return this.mService.getNetworkInfo(i);
        } catch (RemoteException e) {
            return null;
        }
    }

    public int getNetworkPreference() {
        try {
            return this.mService.getNetworkPreference();
        } catch (RemoteException e) {
            return -1;
        }
    }

    public boolean requestRouteToHost(int i, int i2) {
        try {
            return this.mService.requestRouteToHost(i, i2);
        } catch (RemoteException e) {
            return false;
        }
    }

    public void setBackgroundDataSetting(boolean z) {
        try {
            this.mService.setBackgroundDataSetting(z);
        } catch (RemoteException e) {
        }
    }

    public void setNetworkPreference(int i) {
        try {
            this.mService.setNetworkPreference(i);
        } catch (RemoteException e) {
        }
    }

    public boolean setRadio(int i, boolean z) {
        try {
            return this.mService.setRadio(i, z);
        } catch (RemoteException e) {
            return false;
        }
    }

    public boolean setRadios(boolean z) {
        try {
            return this.mService.setRadios(z);
        } catch (RemoteException e) {
            return false;
        }
    }

    public int startUsingNetworkFeature(int i, String str) {
        try {
            return this.mService.startUsingNetworkFeature(i, str, new Binder());
        } catch (RemoteException e) {
            return -1;
        }
    }

    public int stopUsingNetworkFeature(int i, String str) {
        try {
            return this.mService.stopUsingNetworkFeature(i, str);
        } catch (RemoteException e) {
            return -1;
        }
    }
}
