package android.net.http;

import android.content.ContentResolver;
import android.content.Context;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;
import com.android.mms.util.RateController;
import com.android.provider.Settings;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpContext;

public final class AndroidHttpClient implements HttpClient {
    public static long DEFAULT_SYNC_MIN_GZIP_BYTES = 256;
    private static final String TAG = "AndroidHttpClient";
    /* access modifiers changed from: private */
    public static final ThreadLocal<Boolean> sThreadBlocked = new ThreadLocal<>();
    /* access modifiers changed from: private */
    public static final HttpRequestInterceptor sThreadCheckInterceptor = new HttpRequestInterceptor() {
        public void process(HttpRequest httpRequest, HttpContext httpContext) {
            if (AndroidHttpClient.sThreadBlocked.get() != null && ((Boolean) AndroidHttpClient.sThreadBlocked.get()).booleanValue()) {
                throw new RuntimeException("This thread forbids HTTP requests");
            }
        }
    };
    /* access modifiers changed from: private */
    public volatile LoggingConfiguration curlConfiguration;
    private final HttpClient delegate;
    private RuntimeException mLeakedException = new IllegalStateException("AndroidHttpClient created and never closed");

    private class CurlLogger implements HttpRequestInterceptor {
        private CurlLogger() {
        }

        public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
            LoggingConfiguration access$400 = AndroidHttpClient.this.curlConfiguration;
            if (access$400 != null && access$400.isLoggable() && (httpRequest instanceof HttpUriRequest)) {
                access$400.println(AndroidHttpClient.toCurl((HttpUriRequest) httpRequest, access$400.isAuthLoggable()));
            }
        }
    }

    private static class LoggingConfiguration {
        private final int level;
        private final String tag;

        private LoggingConfiguration(String str, int i) {
            this.tag = str;
            this.level = i;
        }

        /* access modifiers changed from: private */
        public boolean isAuthLoggable() {
            return "0".equals(SystemProperties.get("ro.secure")) && Log.isLoggable(new StringBuilder().append(this.tag).append("-auth").toString(), this.level);
        }

        /* access modifiers changed from: private */
        public boolean isLoggable() {
            return Log.isLoggable(this.tag, this.level);
        }

        /* access modifiers changed from: private */
        public void println(String str) {
            Log.println(this.level, this.tag, str);
        }
    }

    private AndroidHttpClient(ClientConnectionManager clientConnectionManager, HttpParams httpParams) {
        this.delegate = new DefaultHttpClient(clientConnectionManager, httpParams) {
            /* access modifiers changed from: protected */
            public HttpContext createHttpContext() {
                BasicHttpContext basicHttpContext = new BasicHttpContext();
                basicHttpContext.setAttribute("http.authscheme-registry", getAuthSchemes());
                basicHttpContext.setAttribute("http.cookiespec-registry", getCookieSpecs());
                basicHttpContext.setAttribute("http.auth.credentials-provider", getCredentialsProvider());
                return basicHttpContext;
            }

            /* access modifiers changed from: protected */
            public BasicHttpProcessor createHttpProcessor() {
                BasicHttpProcessor createHttpProcessor = AndroidHttpClient.super.createHttpProcessor();
                createHttpProcessor.addRequestInterceptor(AndroidHttpClient.sThreadCheckInterceptor);
                createHttpProcessor.addRequestInterceptor(new CurlLogger());
                return createHttpProcessor;
            }
        };
    }

    public static AbstractHttpEntity getCompressedEntity(byte[] bArr, ContentResolver contentResolver) throws IOException {
        if (((long) bArr.length) < getMinGzipSize(contentResolver)) {
            return new ByteArrayEntity(bArr);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        gZIPOutputStream.write(bArr);
        gZIPOutputStream.close();
        AbstractHttpEntity byteArrayEntity = new ByteArrayEntity(byteArrayOutputStream.toByteArray());
        byteArrayEntity.setContentEncoding("gzip");
        return byteArrayEntity;
    }

    public static long getMinGzipSize(ContentResolver contentResolver) {
        String string = Settings.Gservices.getString(contentResolver, Settings.Gservices.SYNC_MIN_GZIP_BYTES);
        if (!TextUtils.isEmpty(string)) {
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException e) {
                Log.w(TAG, "Unable to parse sync_min_gzip_bytes " + string, e);
            }
        }
        return DEFAULT_SYNC_MIN_GZIP_BYTES;
    }

    public static InputStream getUngzippedContent(HttpEntity httpEntity) throws IOException {
        Header contentEncoding;
        String value;
        InputStream content = httpEntity.getContent();
        return (content == null || (contentEncoding = httpEntity.getContentEncoding()) == null || (value = contentEncoding.getValue()) == null || !value.contains("gzip")) ? content : new GZIPInputStream(content);
    }

    public static void modifyRequestToAcceptGzipResponse(HttpRequest httpRequest) {
        httpRequest.addHeader("Accept-Encoding", "gzip");
    }

    public static AndroidHttpClient newInstance(String str) {
        return newInstance(str, null);
    }

    public static AndroidHttpClient newInstance(String str, Context context) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, RateController.ANSWER_TIMEOUT);
        HttpConnectionParams.setSoTimeout(basicHttpParams, RateController.ANSWER_TIMEOUT);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        HttpClientParams.setRedirecting(basicHttpParams, false);
        HttpProtocolParams.setUserAgent(basicHttpParams, str);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        return new AndroidHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    public static void setThreadBlocked(boolean z) {
        sThreadBlocked.set(Boolean.valueOf(z));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String toCurl(org.apache.http.client.methods.HttpUriRequest r9, boolean r10) throws java.io.IOException {
        /*
            java.lang.String r8 = "\""
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r1 = "curl "
            r2.append(r1)
            org.apache.http.Header[] r1 = r9.getAllHeaders()
            int r3 = r1.length
            r4 = 0
        L_0x0012:
            if (r4 >= r3) goto L_0x0049
            r5 = r1[r4]
            if (r10 != 0) goto L_0x0033
            java.lang.String r6 = r5.getName()
            java.lang.String r7 = "Authorization"
            boolean r6 = r6.equals(r7)
            if (r6 != 0) goto L_0x0030
            java.lang.String r6 = r5.getName()
            java.lang.String r7 = "Cookie"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x0033
        L_0x0030:
            int r4 = r4 + 1
            goto L_0x0012
        L_0x0033:
            java.lang.String r6 = "--header \""
            r2.append(r6)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = r5.trim()
            r2.append(r5)
            java.lang.String r5 = "\" "
            r2.append(r5)
            goto L_0x0030
        L_0x0049:
            java.net.URI r3 = r9.getURI()
            boolean r1 = r9 instanceof org.apache.http.impl.client.RequestWrapper
            if (r1 == 0) goto L_0x00b2
            r0 = r9
            org.apache.http.impl.client.RequestWrapper r0 = (org.apache.http.impl.client.RequestWrapper) r0
            r1 = r0
            org.apache.http.HttpRequest r1 = r1.getOriginal()
            boolean r4 = r1 instanceof org.apache.http.client.methods.HttpUriRequest
            if (r4 == 0) goto L_0x00b2
            org.apache.http.client.methods.HttpUriRequest r1 = (org.apache.http.client.methods.HttpUriRequest) r1
            java.net.URI r1 = r1.getURI()
        L_0x0063:
            java.lang.String r3 = "\""
            r2.append(r8)
            r2.append(r1)
            java.lang.String r1 = "\""
            r2.append(r8)
            boolean r1 = r9 instanceof org.apache.http.HttpEntityEnclosingRequest
            if (r1 == 0) goto L_0x00a7
            org.apache.http.HttpEntityEnclosingRequest r9 = (org.apache.http.HttpEntityEnclosingRequest) r9
            org.apache.http.HttpEntity r1 = r9.getEntity()
            if (r1 == 0) goto L_0x00a7
            boolean r3 = r1.isRepeatable()
            if (r3 == 0) goto L_0x00a7
            long r3 = r1.getContentLength()
            r5 = 1024(0x400, double:5.06E-321)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x00ac
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            r1.writeTo(r3)
            java.lang.String r1 = r3.toString()
            java.lang.String r3 = " --data-ascii \""
            java.lang.StringBuilder r3 = r2.append(r3)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = "\""
            r1.append(r8)
        L_0x00a7:
            java.lang.String r1 = r2.toString()
            return r1
        L_0x00ac:
            java.lang.String r1 = " [TOO MUCH DATA TO INCLUDE]"
            r2.append(r1)
            goto L_0x00a7
        L_0x00b2:
            r1 = r3
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: android.net.http.AndroidHttpClient.toCurl(org.apache.http.client.methods.HttpUriRequest, boolean):java.lang.String");
    }

    public void close() {
        if (this.mLeakedException != null) {
            getConnectionManager().shutdown();
            this.mLeakedException = null;
        }
    }

    public void disableCurlLogging() {
        this.curlConfiguration = null;
    }

    public void enableCurlLogging(String str, int i) {
        if (str == null) {
            throw new NullPointerException("name");
        } else if (i < 2 || i > 7) {
            throw new IllegalArgumentException("Level is out of range [2..7]");
        } else {
            this.curlConfiguration = new LoggingConfiguration(str, i);
        }
    }

    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler) throws IOException, ClientProtocolException {
        return this.delegate.execute(httpHost, httpRequest, responseHandler);
    }

    public <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException, ClientProtocolException {
        return this.delegate.execute(httpHost, httpRequest, responseHandler, httpContext);
    }

    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler) throws IOException, ClientProtocolException {
        return this.delegate.execute(httpUriRequest, responseHandler);
    }

    public <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException, ClientProtocolException {
        return this.delegate.execute(httpUriRequest, responseHandler, httpContext);
    }

    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest) throws IOException {
        return this.delegate.execute(httpHost, httpRequest);
    }

    public HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) throws IOException {
        return this.delegate.execute(httpHost, httpRequest, httpContext);
    }

    public HttpResponse execute(HttpUriRequest httpUriRequest) throws IOException {
        return this.delegate.execute(httpUriRequest);
    }

    public HttpResponse execute(HttpUriRequest httpUriRequest, HttpContext httpContext) throws IOException {
        return this.delegate.execute(httpUriRequest, httpContext);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        if (this.mLeakedException != null) {
            Log.e(TAG, "Leak found", this.mLeakedException);
            this.mLeakedException = null;
        }
    }

    public ClientConnectionManager getConnectionManager() {
        return this.delegate.getConnectionManager();
    }

    public HttpParams getParams() {
        return this.delegate.getParams();
    }
}
