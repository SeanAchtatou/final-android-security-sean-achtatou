package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.EnumMap;

public class NetworkInfo implements Parcelable {
    public static final Parcelable.Creator<NetworkInfo> CREATOR = new Parcelable.Creator<NetworkInfo>() {
        public NetworkInfo createFromParcel(Parcel parcel) {
            NetworkInfo networkInfo = new NetworkInfo(parcel.readInt(), parcel.readInt(), parcel.readString(), parcel.readString());
            State unused = networkInfo.mState = State.valueOf(parcel.readString());
            DetailedState unused2 = networkInfo.mDetailedState = DetailedState.valueOf(parcel.readString());
            boolean unused3 = networkInfo.mIsFailover = parcel.readInt() != 0;
            boolean unused4 = networkInfo.mIsAvailable = parcel.readInt() != 0;
            boolean unused5 = networkInfo.mIsRoaming = parcel.readInt() != 0;
            String unused6 = networkInfo.mReason = parcel.readString();
            String unused7 = networkInfo.mExtraInfo = parcel.readString();
            return networkInfo;
        }

        public NetworkInfo[] newArray(int i) {
            return new NetworkInfo[i];
        }
    };
    private static final EnumMap<DetailedState, State> stateMap = new EnumMap<>(DetailedState.class);
    /* access modifiers changed from: private */
    public DetailedState mDetailedState;
    /* access modifiers changed from: private */
    public String mExtraInfo;
    /* access modifiers changed from: private */
    public boolean mIsAvailable;
    /* access modifiers changed from: private */
    public boolean mIsFailover;
    /* access modifiers changed from: private */
    public boolean mIsRoaming;
    private int mNetworkType;
    /* access modifiers changed from: private */
    public String mReason;
    /* access modifiers changed from: private */
    public State mState;
    private int mSubtype;
    private String mSubtypeName;
    private String mTypeName;

    public enum DetailedState {
        IDLE,
        SCANNING,
        CONNECTING,
        AUTHENTICATING,
        OBTAINING_IPADDR,
        CONNECTED,
        SUSPENDED,
        DISCONNECTING,
        DISCONNECTED,
        FAILED
    }

    public enum State {
        CONNECTING,
        CONNECTED,
        SUSPENDED,
        DISCONNECTING,
        DISCONNECTED,
        UNKNOWN
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(android.net.NetworkInfo$DetailedState, android.net.NetworkInfo$State):V}
     arg types: [android.net.NetworkInfo$DetailedState, android.net.NetworkInfo$State]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(android.net.NetworkInfo$DetailedState, android.net.NetworkInfo$State):V}
      ClspMth{java.util.Map.put(android.net.NetworkInfo$DetailedState, android.net.NetworkInfo$State):V}
      ClspMth{java.util.EnumMap.put(android.net.NetworkInfo$DetailedState, android.net.NetworkInfo$State):V} */
    static {
        stateMap.put(DetailedState.IDLE, State.DISCONNECTED);
        stateMap.put(DetailedState.SCANNING, State.DISCONNECTED);
        stateMap.put(DetailedState.CONNECTING, State.CONNECTING);
        stateMap.put(DetailedState.AUTHENTICATING, State.CONNECTING);
        stateMap.put(DetailedState.OBTAINING_IPADDR, State.CONNECTING);
        stateMap.put(DetailedState.CONNECTED, State.CONNECTED);
        stateMap.put(DetailedState.SUSPENDED, State.SUSPENDED);
        stateMap.put(DetailedState.DISCONNECTING, State.DISCONNECTING);
        stateMap.put(DetailedState.DISCONNECTED, State.DISCONNECTED);
        stateMap.put(DetailedState.FAILED, State.DISCONNECTED);
    }

    public NetworkInfo(int i) {
    }

    NetworkInfo(int i, int i2, String str, String str2) {
        if (!ConnectivityManager.isNetworkTypeValid(i)) {
            throw new IllegalArgumentException("Invalid network type: " + i);
        }
        this.mNetworkType = i;
        this.mSubtype = i2;
        this.mTypeName = str;
        this.mSubtypeName = str2;
        setDetailedState(DetailedState.IDLE, null, null);
        this.mState = State.UNKNOWN;
        this.mIsAvailable = false;
        this.mIsRoaming = false;
    }

    public int describeContents() {
        return 0;
    }

    public DetailedState getDetailedState() {
        return this.mDetailedState;
    }

    public String getExtraInfo() {
        return this.mExtraInfo;
    }

    public String getReason() {
        return this.mReason;
    }

    public State getState() {
        return this.mState;
    }

    public int getSubtype() {
        return this.mSubtype;
    }

    public String getSubtypeName() {
        return this.mSubtypeName;
    }

    public int getType() {
        return this.mNetworkType;
    }

    public String getTypeName() {
        return this.mTypeName;
    }

    public boolean isAvailable() {
        return this.mIsAvailable;
    }

    public boolean isConnected() {
        return this.mState == State.CONNECTED;
    }

    public boolean isConnectedOrConnecting() {
        return this.mState == State.CONNECTED || this.mState == State.CONNECTING;
    }

    public boolean isFailover() {
        return this.mIsFailover;
    }

    public boolean isRoaming() {
        return this.mIsRoaming;
    }

    /* access modifiers changed from: package-private */
    public void setDetailedState(DetailedState detailedState, String str, String str2) {
        this.mDetailedState = detailedState;
        this.mState = stateMap.get(detailedState);
        this.mReason = str;
        this.mExtraInfo = str2;
    }

    public void setFailover(boolean z) {
        this.mIsFailover = z;
    }

    public void setIsAvailable(boolean z) {
        this.mIsAvailable = z;
    }

    /* access modifiers changed from: package-private */
    public void setRoaming(boolean z) {
        this.mIsRoaming = z;
    }

    /* access modifiers changed from: package-private */
    public void setSubtype(int i, String str) {
        this.mSubtype = i;
        this.mSubtypeName = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("NetworkInfo: ");
        sb.append("type: ").append(getTypeName()).append("[").append(getSubtypeName()).append("], state: ").append(this.mState).append("/").append(this.mDetailedState).append(", reason: ").append(this.mReason == null ? "(unspecified)" : this.mReason).append(", extra: ").append(this.mExtraInfo == null ? "(none)" : this.mExtraInfo).append(", roaming: ").append(this.mIsRoaming).append(", failover: ").append(this.mIsFailover).append(", isAvailable: ").append(this.mIsAvailable);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mNetworkType);
        parcel.writeInt(this.mSubtype);
        parcel.writeString(this.mTypeName);
        parcel.writeString(this.mSubtypeName);
        parcel.writeString(this.mState.name());
        parcel.writeString(this.mDetailedState.name());
        parcel.writeInt(this.mIsFailover ? 1 : 0);
        parcel.writeInt(this.mIsAvailable ? 1 : 0);
        parcel.writeInt(this.mIsRoaming ? 1 : 0);
        parcel.writeString(this.mReason);
        parcel.writeString(this.mExtraInfo);
    }
}
