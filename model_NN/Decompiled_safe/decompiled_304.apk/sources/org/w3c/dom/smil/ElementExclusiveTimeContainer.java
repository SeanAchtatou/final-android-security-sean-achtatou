package org.w3c.dom.smil;

import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;

public interface ElementExclusiveTimeContainer extends ElementTimeContainer {
    String getEndSync();

    NodeList getPausedElements();

    void setEndSync(String str) throws DOMException;
}
