package org.w3c.dom.smil;

public interface SMILSetElement extends ElementTimeControl, ElementTime, ElementTargetAttributes, SMILElement {
    String getTo();

    void setTo(String str);
}
