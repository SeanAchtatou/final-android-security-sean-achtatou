package org.w3c.dom.smil;

public interface ElementSyncBehavior {
    String getDefaultSyncBehavior();

    float getDefaultSyncTolerance();

    String getSyncBehavior();

    boolean getSyncMaster();

    float getSyncTolerance();
}
