package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

public interface ElementTest {
    boolean getSystemAudioDesc();

    int getSystemBitrate();

    boolean getSystemCaptions();

    String getSystemLanguage();

    String getSystemOverdubOrSubtitle();

    boolean getSystemRequired();

    boolean getSystemScreenDepth();

    boolean getSystemScreenSize();

    void setSystemAudioDesc(boolean z) throws DOMException;

    void setSystemBitrate(int i) throws DOMException;

    void setSystemCaptions(boolean z) throws DOMException;

    void setSystemLanguage(String str) throws DOMException;

    void setSystemOverdubOrSubtitle(String str) throws DOMException;
}
