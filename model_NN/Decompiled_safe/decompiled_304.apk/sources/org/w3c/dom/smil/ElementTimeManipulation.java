package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

public interface ElementTimeManipulation {
    float getAccelerate();

    boolean getAutoReverse();

    float getDecelerate();

    float getSpeed();

    void setAccelerate(float f) throws DOMException;

    void setAutoReverse(boolean z) throws DOMException;

    void setDecelerate(float f) throws DOMException;

    void setSpeed(float f) throws DOMException;
}
