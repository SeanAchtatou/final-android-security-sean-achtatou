package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

public interface ElementParallelTimeContainer extends ElementTimeContainer {
    String getEndSync();

    float getImplicitDuration();

    void setEndSync(String str) throws DOMException;
}
