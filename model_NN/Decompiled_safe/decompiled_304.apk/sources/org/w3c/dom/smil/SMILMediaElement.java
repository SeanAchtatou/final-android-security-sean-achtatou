package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

public interface SMILMediaElement extends ElementTime, SMILElement {
    String getAbstractAttr();

    String getAlt();

    String getAuthor();

    String getClipBegin();

    String getClipEnd();

    String getCopyright();

    String getLongdesc();

    String getPort();

    String getReadIndex();

    String getRtpformat();

    String getSrc();

    String getStripRepeat();

    String getTitle();

    String getTransport();

    String getType();

    void setAbstractAttr(String str) throws DOMException;

    void setAlt(String str) throws DOMException;

    void setAuthor(String str) throws DOMException;

    void setClipBegin(String str) throws DOMException;

    void setClipEnd(String str) throws DOMException;

    void setCopyright(String str) throws DOMException;

    void setLongdesc(String str) throws DOMException;

    void setPort(String str) throws DOMException;

    void setReadIndex(String str) throws DOMException;

    void setRtpformat(String str) throws DOMException;

    void setSrc(String str) throws DOMException;

    void setStripRepeat(String str) throws DOMException;

    void setTitle(String str) throws DOMException;

    void setTransport(String str) throws DOMException;

    void setType(String str) throws DOMException;
}
