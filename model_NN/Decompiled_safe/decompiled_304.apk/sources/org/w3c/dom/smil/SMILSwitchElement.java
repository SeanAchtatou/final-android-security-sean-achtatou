package org.w3c.dom.smil;

import org.w3c.dom.Element;

public interface SMILSwitchElement extends SMILElement {
    Element getSelectedElement();
}
