package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

public interface ElementTime {
    public static final short FILL_AUTO = 2;
    public static final short FILL_FREEZE = 1;
    public static final short FILL_REMOVE = 0;
    public static final short RESTART_ALWAYS = 0;
    public static final short RESTART_NEVER = 1;
    public static final short RESTART_WHEN_NOT_ACTIVE = 2;

    boolean beginElement();

    boolean endElement();

    TimeList getBegin();

    float getDur();

    TimeList getEnd();

    short getFill();

    short getFillDefault();

    float getRepeatCount();

    float getRepeatDur();

    short getRestart();

    void pauseElement();

    void resumeElement();

    void seekElement(float f);

    void setBegin(TimeList timeList) throws DOMException;

    void setDur(float f) throws DOMException;

    void setEnd(TimeList timeList) throws DOMException;

    void setFill(short s) throws DOMException;

    void setFillDefault(short s) throws DOMException;

    void setRepeatCount(float f) throws DOMException;

    void setRepeatDur(float f) throws DOMException;

    void setRestart(short s) throws DOMException;
}
