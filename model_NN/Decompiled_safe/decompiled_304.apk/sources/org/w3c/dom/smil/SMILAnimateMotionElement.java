package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

public interface SMILAnimateMotionElement extends SMILAnimateElement {
    String getOrigin();

    String getPath();

    void setOrigin(String str) throws DOMException;

    void setPath(String str) throws DOMException;
}
