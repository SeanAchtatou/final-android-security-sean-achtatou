package org.w3c.dom.smil;

public interface SMILTopLayoutElement extends SMILElement, ElementLayout {
}
