package org.w3c.dom.smil;

import org.w3c.dom.events.Event;
import org.w3c.dom.views.AbstractView;

public interface TimeEvent extends Event {
    int getDetail();

    AbstractView getView();

    void initTimeEvent(String str, AbstractView abstractView, int i);
}
