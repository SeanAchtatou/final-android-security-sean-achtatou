package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

public interface SMILAnimation extends SMILElement, ElementTargetAttributes, ElementTime, ElementTimeControl {
    public static final short ACCUMULATE_NONE = 0;
    public static final short ACCUMULATE_SUM = 1;
    public static final short ADDITIVE_REPLACE = 0;
    public static final short ADDITIVE_SUM = 1;
    public static final short CALCMODE_DISCRETE = 0;
    public static final short CALCMODE_LINEAR = 1;
    public static final short CALCMODE_PACED = 2;
    public static final short CALCMODE_SPLINE = 3;

    short getAccumulate();

    short getAdditive();

    String getBy();

    short getCalcMode();

    String getFrom();

    String getKeySplines();

    TimeList getKeyTimes();

    String getTo();

    String getValues();

    void setAccumulate(short s) throws DOMException;

    void setAdditive(short s) throws DOMException;

    void setBy(String str) throws DOMException;

    void setCalcMode(short s) throws DOMException;

    void setFrom(String str) throws DOMException;

    void setKeySplines(String str) throws DOMException;

    void setKeyTimes(TimeList timeList) throws DOMException;

    void setTo(String str) throws DOMException;

    void setValues(String str) throws DOMException;
}
