package org.w3c.dom.smil;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

public interface Time {
    public static final short SMIL_TIME_EVENT_BASED = 3;
    public static final short SMIL_TIME_INDEFINITE = 0;
    public static final short SMIL_TIME_MEDIA_MARKER = 5;
    public static final short SMIL_TIME_OFFSET = 1;
    public static final short SMIL_TIME_SYNC_BASED = 2;
    public static final short SMIL_TIME_WALLCLOCK = 4;

    boolean getBaseBegin();

    Element getBaseElement();

    String getEvent();

    String getMarker();

    double getOffset();

    boolean getResolved();

    double getResolvedOffset();

    short getTimeType();

    void setBaseBegin(boolean z) throws DOMException;

    void setBaseElement(Element element) throws DOMException;

    void setEvent(String str) throws DOMException;

    void setMarker(String str) throws DOMException;

    void setOffset(double d) throws DOMException;
}
