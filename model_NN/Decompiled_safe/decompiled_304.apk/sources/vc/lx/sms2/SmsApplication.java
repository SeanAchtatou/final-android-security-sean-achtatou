package vc.lx.sms2;

import android.app.Application;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import com.android.mms.MmsConfig;
import com.android.mms.drm.DrmUtils;
import com.android.mms.layout.LayoutManager;
import com.android.mms.util.ContactInfoCache;
import com.android.mms.util.DownloadManager;
import com.android.mms.util.RateController;
import com.android.mms.util.SmileyParser;
import com.flurry.android.FlurryAgent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import vc.lx.sms.caches.NullDiskCache;
import vc.lx.sms.caches.RemoteResourceManager;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.download.DownloadJob;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.db.SmsTemplateDbService;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.service.MessageObserverService;
import vc.lx.sms.service.PushUpdaterService;
import vc.lx.sms.service.UpdateFavoriteService;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;

public class SmsApplication extends Application {
    public static int DEFAULT_DAOWNLOAD_BUFFER_SIZE = 102400;
    public static int DEFAULT_EXPAND_SIZE = 1000;
    public static int FAVORITE_UPDATER_TIME = 300000;
    public static final String LOG_TAG = "Mms";
    public static int PUSH_UPDATER_COUNT = 5;
    public static int PUSH_UPDATER_TIME = 600000;
    private static SmsApplication instance;
    public int currentDownLoadSize = 0;
    public DisplayMetrics dm = null;
    private InputMethodManager inputMethodManager;
    private boolean isSearchByContact = false;
    public HashMap<String, String> mAllNumAndNameCache = new HashMap<>();
    private Handler mHandler = new Handler();
    private NotificationManager mNotificationManager;
    private ArrayList<DownloadJob> mQueuedDownloads;
    private RemoteResourceManager mRemoteResourceManager;
    private TopMusicService topMusicService;

    private class MediaCardStateBroadcastReceiver extends BroadcastReceiver {
        private MediaCardStateBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.MEDIA_UNMOUNTED".equals(intent.getAction())) {
                SmsApplication.this.getRemoteResourceManager().shutdown();
                SmsApplication.this.loadResourceManagers();
            } else if ("android.intent.action.MEDIA_MOUNTED".equals(intent.getAction())) {
                SmsApplication.this.loadResourceManagers();
            }
        }

        public void register() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
            intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
            intentFilter.addDataScheme("file");
            SmsApplication.this.registerReceiver(this, intentFilter);
        }
    }

    public static SmsApplication getInstance() {
        return instance;
    }

    /* access modifiers changed from: private */
    public void loadResourceManagers() {
        try {
            this.mRemoteResourceManager = new RemoteResourceManager("cache");
        } catch (IllegalStateException e) {
            this.mRemoteResourceManager = new RemoteResourceManager(new NullDiskCache());
        }
    }

    public void addToDownloadList(DownloadJob downloadJob) {
        this.mQueuedDownloads.add(downloadJob);
    }

    public int getCurrentDownLoadSize() {
        return this.currentDownLoadSize;
    }

    public ArrayList<DownloadJob> getDownloadList() {
        return this.mQueuedDownloads;
    }

    public InputMethodManager getInputMethodManager() {
        return this.inputMethodManager;
    }

    public RemoteResourceManager getRemoteResourceManager() {
        return this.mRemoteResourceManager;
    }

    public TopMusicService getTopMusicService() {
        return this.topMusicService;
    }

    public HashMap<String, String> getmAllNumAndNameCache() {
        return this.mAllNumAndNameCache;
    }

    public NotificationManager getmNotificationManager() {
        return this.mNotificationManager;
    }

    public void importInitSmsTemplates() {
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    new AsyncTask<Void, Void, Void>() {
                        /* access modifiers changed from: protected */
                        public Void doInBackground(Void... voidArr) {
                            new SmsTemplateDbService(SmsApplication.this).insertInitSmsTemplates();
                            return null;
                        }
                    }.execute(new Void[0]);
                } catch (Exception e) {
                }
            }
        });
    }

    public boolean isAlreadyInDownloadList(SongItem songItem) {
        Iterator<DownloadJob> it = this.mQueuedDownloads.iterator();
        while (it.hasNext()) {
            if (it.next().getSongInfo().contentid.equals(songItem.contentid)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSearchByContact() {
        return this.isSearchByContact;
    }

    public void onConfigurationChanged(Configuration configuration) {
        LayoutManager.getInstance().onConfigurationChanged(configuration);
    }

    public void onCreate() {
        super.onCreate();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        this.mNotificationManager = (NotificationManager) getSystemService("notification");
        this.inputMethodManager = (InputMethodManager) getSystemService("input_method");
        this.topMusicService = new TopMusicService(getApplicationContext());
        MmsConfig.init(this);
        ContactInfoCache.init(this);
        Contact.init(this);
        instance = this;
        this.mQueuedDownloads = new ArrayList<>();
        Conversation.init(this);
        DownloadManager.init(this);
        RateController.init(this);
        DrmUtils.cleanupStorage(this);
        LayoutManager.init(this);
        SmileyParser.init(this);
        startService(new Intent(getApplicationContext(), PushUpdaterService.class));
        startService(new Intent(getApplicationContext(), MessageObserverService.class));
        startService(new Intent(new Intent(getApplicationContext(), UpdateFavoriteService.class)));
        loadResourceManagers();
        new MediaCardStateBroadcastReceiver().register();
        FlurryAgent.setUserId(Util.getDeviceImie(getApplicationContext()));
    }

    public void onTerminate() {
        DrmUtils.cleanupStorage(this);
    }

    public void removeFromDownloadList(DownloadJob downloadJob) {
        this.mQueuedDownloads.remove(downloadJob);
        updateLocalMusicActivity();
    }

    public void setCurrentDownLoadSize(int i) {
        this.currentDownLoadSize = i;
    }

    public void setSearchByContact(boolean z) {
        this.isSearchByContact = z;
    }

    public void setmAllNumAndNameCache(HashMap<String, String> hashMap) {
        this.mAllNumAndNameCache = hashMap;
    }

    public void updateLocalMusicActivity() {
        Intent intent = new Intent();
        intent.setAction(PrefsUtil.INTENT_UPDATE_DOWNLOAD_LIST);
        intent.putExtras(new Bundle());
        sendBroadcast(intent);
    }
}
