package vc.lx.sms2.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TabHost;
import vc.lx.sms.richtext.ui.FavoriteActivity;
import vc.lx.sms.richtext.ui.MoreActivity;
import vc.lx.sms.richtext.ui.QuareActivity;
import vc.lx.sms.ui.ConversationListActivity;
import vc.lx.sms2.R;

public class MainActivity extends TabActivity {
    private static final int MENU_ABOUT = 4;
    private static final int MENU_ACCOUNT = 2;
    private static final int MENU_FEEDBACK = 3;
    private static final int MENU_SETTINGS = 1;
    protected static final int PROGRESS_DIALOG = 1;
    private LayoutInflater mInflater;
    private TabHost mTabHost;
    private ProgressDialog progressDialog;

    private TabHost.TabSpec createTabSpec(String str, int i, Intent intent) {
        TabHost.TabSpec newTabSpec = this.mTabHost.newTabSpec(str);
        newTabSpec.setIndicator(this.mInflater.inflate(i, (ViewGroup) null));
        newTabSpec.setContent(intent);
        return newTabSpec;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.main_group);
        this.mInflater = LayoutInflater.from(this);
        this.mTabHost = (TabHost) findViewById(16908306);
        this.mTabHost.setup(getLocalActivityManager());
        this.mTabHost.addTab(createTabSpec("TAB_MESSAGE", R.layout.tab_main_message, new Intent(getApplicationContext(), ConversationListActivity.class)));
        this.mTabHost.addTab(createTabSpec("TAB_FAVORITE", R.layout.tab_main_favorite, new Intent(this, FavoriteActivity.class)));
        this.mTabHost.addTab(createTabSpec("TAB_SQUARE", R.layout.tab_main_square, new Intent(this, QuareActivity.class)));
        this.mTabHost.addTab(createTabSpec("TAB_MORE", R.layout.tab_main_more, new Intent(getApplicationContext(), MoreActivity.class)));
        this.mTabHost.setBackgroundResource(R.drawable.tab_bottom_bg);
        this.mTabHost.setCurrentTab(0);
        this.mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String str) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                this.progressDialog = new ProgressDialog(this);
                this.progressDialog.setProgressStyle(0);
                this.progressDialog.setMessage(getString(R.string.loading_update));
                return this.progressDialog;
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        menuItem.getItemId();
        return true;
    }
}
