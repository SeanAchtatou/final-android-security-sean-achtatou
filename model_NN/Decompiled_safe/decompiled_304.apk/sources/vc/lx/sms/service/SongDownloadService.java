package vc.lx.sms.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.download.DownloadJob;
import vc.lx.sms.cmcc.http.download.IDownloadJob;
import vc.lx.sms.cmcc.http.download.IDownloadJobListener;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.SmsApplication;

public class SongDownloadService extends Service {
    public static final String ACTION_ADD_TO_DOWNLOAD = "add_to_download";
    private IDownloadJobListener mDownloadJobListener = new IDownloadJobListener() {
        public void downloadEnded(IDownloadJob iDownloadJob) {
            SmsApplication.getInstance().removeFromDownloadList((DownloadJob) iDownloadJob);
            iDownloadJob.cancelNotification();
        }

        public void downloadStarted() {
        }
    };

    private void addToDownloadQueue(SongItem songItem) {
        if (!SmsApplication.getInstance().isAlreadyInDownloadList(songItem)) {
            DownloadJob downloadJob = new DownloadJob(songItem, getApplicationContext());
            downloadJob.setListener(this.mDownloadJobListener);
            SmsApplication.getInstance().addToDownloadList(downloadJob);
            downloadJob.start();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent != null) {
            String action = intent.getAction();
            Bundle extras = intent.getExtras();
            if (action.equals(ACTION_ADD_TO_DOWNLOAD)) {
                addToDownloadQueue((SongItem) extras.getSerializable(PrefsUtil.KEY_SONG_INFO));
            }
        }
    }
}
