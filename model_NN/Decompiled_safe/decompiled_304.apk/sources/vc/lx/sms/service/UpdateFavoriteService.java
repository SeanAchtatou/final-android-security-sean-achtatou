package vc.lx.sms.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteList;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.cmcc.http.parser.VoteListParser;
import vc.lx.sms.db.ImageService;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class UpdateFavoriteService extends Service {
    private List<String> imagePlugList = null;
    /* access modifiers changed from: private */
    public String jsonStr = "";
    private ImageService mImageService;
    private TopMusicService mMusicService;
    /* access modifiers changed from: private */
    public List<VoteItem> mVoteItemList = null;
    private List<String> musicPlugList = null;
    /* access modifiers changed from: private */
    public Handler pushUpdaterHandler = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    UpdateFavoriteService.this.fillVoteList();
                    UpdateFavoriteService.this.fileImageList();
                    UpdateFavoriteService.this.fillMusicList();
                    String unused = UpdateFavoriteService.this.jsonStr = UpdateFavoriteService.this.createSendJsonObject().toString();
                    new UpdateFavoriteTask(UpdateFavoriteService.this.getApplicationContext(), 1).execute(new Void[0]);
                    UpdateFavoriteService.this.pushUpdaterHandler.sendMessageDelayed(UpdateFavoriteService.this.pushUpdaterHandler.obtainMessage(1), (long) SmsApplication.FAVORITE_UPDATER_TIME);
                    return;
                default:
                    return;
            }
        }
    };

    class UpdateFavoriteTask extends AbstractAsyncTask {
        private int _page;

        public UpdateFavoriteTask(Context context, int i) {
            super(context);
            this._page = i;
        }

        private void compareUpdateAndDBData(ContentValues contentValues, VoteItem voteItem) {
            StringBuilder sb = new StringBuilder();
            if (UpdateFavoriteService.this.mVoteItemList.contains(voteItem) && ((VoteItem) UpdateFavoriteService.this.mVoteItemList.get(UpdateFavoriteService.this.mVoteItemList.indexOf(voteItem))).counter != voteItem.counter) {
                for (int i = 0; i < voteItem.options.size(); i++) {
                    VoteOption voteOption = voteItem.options.get(i);
                    ArrayList<VoteOption> arrayList = ((VoteItem) UpdateFavoriteService.this.mVoteItemList.get(UpdateFavoriteService.this.mVoteItemList.indexOf(voteItem))).options;
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        if (!(arrayList.get(i2).service_id == null || !arrayList.get(i2).service_id.equals(voteOption.service_id) || arrayList.get(i2).counter == voteOption.counter)) {
                            if (arrayList.get(i2).contactNames.size() != voteOption.contactNames.size()) {
                                voteOption.contactNames.removeAll(arrayList.get(i2).contactNames);
                                if (voteOption.contactNames.size() > 0) {
                                    for (String append : voteOption.contactNames) {
                                        sb.append(append).append(",");
                                    }
                                }
                            } else {
                                updateVoteIsNew(voteItem);
                            }
                        }
                    }
                }
                if (sb.toString().length() <= 0 || !Util.getPopupNotification(UpdateFavoriteService.this.getApplicationContext())) {
                    updateVoteIsNew(voteItem);
                    return;
                }
                if (sb.length() > 1) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                voteItem.cli_id = ((VoteItem) UpdateFavoriteService.this.mVoteItemList.get(UpdateFavoriteService.this.mVoteItemList.indexOf(voteItem))).cli_id;
                sb.append(this.mContext.getString(R.string.notification_info));
                Util.voteNotification(voteItem, sb.toString(), UpdateFavoriteService.this.getApplicationContext());
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        private void updateVoteIsNew(VoteItem voteItem) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("is_new", (Integer) 0);
            UpdateFavoriteService.this.getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_questions/" + voteItem.service_id), contentValues, null, null);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.fatchFavoriteUpdate(this.mEnginehost, this.mContext, UpdateFavoriteService.this.jsonStr);
        }

        public String getTag() {
            return "UpdateFavoriteTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteList)) {
                final VoteList voteList = (VoteList) miguType;
                PrefsUtil.saveLastFavoriteUpdateDate(UpdateFavoriteService.this.getApplicationContext(), voteList.tm);
                UpdateFavoriteService.this.pushUpdaterHandler.post(new Runnable() {
                    public void run() {
                        UpdateFavoriteTask.this.updateDbData(voteList, UpdateFavoriteService.this.mVoteItemList);
                        if (voteList.picMusicNotifys.size() > 0) {
                            int i = 0;
                            while (true) {
                                int i2 = i;
                                if (i2 < voteList.picMusicNotifys.size()) {
                                    Util.voteNotification(null, voteList.picMusicNotifys.get(i2), UpdateFavoriteService.this.getApplicationContext());
                                    i = i2 + 1;
                                } else {
                                    return;
                                }
                            }
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new VoteListParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }

        public void updateDbData(VoteList voteList, List<VoteItem> list) {
            for (int i = 0; i < voteList.voteItems.size(); i++) {
                VoteItem voteItem = voteList.voteItems.get(i);
                if (list.contains(voteItem)) {
                    ContentValues contentValues = new ContentValues();
                    compareUpdateAndDBData(contentValues, voteItem);
                    contentValues.put("counter", Integer.valueOf(voteItem.counter));
                    UpdateFavoriteService.this.getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_questions/" + voteItem.service_id), contentValues, null, null);
                    if (voteItem.options.size() > 0 && !voteItem.options.isEmpty()) {
                        for (int i2 = 0; i2 < voteItem.options.size(); i2++) {
                            ContentValues contentValues2 = new ContentValues();
                            contentValues2.put("counter", Integer.valueOf(voteItem.options.get(i2).counter));
                            contentValues2.put("contact_name", voteItem.options.get(i2).contact_name);
                            UpdateFavoriteService.this.getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_options/" + voteItem.options.get(i2).service_id), contentValues2, null, null);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void fileImageList() {
        this.imagePlugList = new ArrayList();
        this.imagePlugList = this.mImageService.getSmsImageMaster();
    }

    /* access modifiers changed from: private */
    public void fillMusicList() {
        this.musicPlugList = new ArrayList();
        List<SongItem> querySongItemByMasterThree = this.mMusicService.querySongItemByMasterThree();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < querySongItemByMasterThree.size()) {
                this.musicPlugList.add(querySongItemByMasterThree.get(i2).plug);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void fillVoteList() {
        this.mVoteItemList = new ArrayList();
        Cursor query = getApplicationContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " tracer_flag =  ? ", new String[]{"1"}, null);
        while (query != null && query.moveToNext()) {
            VoteItem voteItem = new VoteItem();
            voteItem.cli_id = (long) Integer.parseInt(query.getString(query.getColumnIndex("_id")));
            voteItem.title = query.getString(query.getColumnIndex("title"));
            voteItem.service_id = query.getString(query.getColumnIndex("service_id"));
            voteItem.counter = query.getInt(query.getColumnIndex("counter"));
            voteItem.plug = query.getString(query.getColumnIndex(SmsSqliteHelper.PLUG));
            Cursor query2 = getApplicationContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " parent_id = ? ", new String[]{String.valueOf(voteItem.cli_id)}, null);
            while (query2 != null && query2.moveToNext()) {
                voteItem.plugs.add(query2.getString(query2.getColumnIndex(SmsSqliteHelper.PLUG)));
            }
            if (query2 != null) {
                query2.close();
            }
            fetchOption(voteItem, voteItem.cli_id);
            this.mVoteItemList.add(voteItem);
        }
        if (query != null) {
            query.close();
        }
    }

    /* access modifiers changed from: protected */
    public JSONObject createSendJsonObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONArray jSONArray = new JSONArray();
            if (this.mVoteItemList.size() > 0) {
                for (int i = 0; i < this.mVoteItemList.size(); i++) {
                    JSONArray jSONArray2 = new JSONArray();
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("vote_id", this.mVoteItemList.get(i).service_id);
                    if (this.mVoteItemList.get(i).plugs.size() > 0) {
                        for (int i2 = 0; i2 < this.mVoteItemList.get(i).plugs.size(); i2++) {
                            if (this.mVoteItemList.get(i).plugs.get(i2) != null) {
                                jSONArray2.put(this.mVoteItemList.get(i).plugs.get(i2).substring(this.mVoteItemList.get(i).plugs.get(i2).lastIndexOf("/") + 1));
                            }
                        }
                    }
                    jSONObject2.put("plugs", jSONArray2);
                    if (!(this.mVoteItemList.get(i).service_id == null || this.mVoteItemList.get(i).plug == null)) {
                        jSONArray.put(jSONObject2);
                    }
                }
                jSONObject.put("votes", jSONArray);
            }
            if (this.imagePlugList.size() > 0) {
                JSONArray jSONArray3 = new JSONArray();
                for (int i3 = 0; i3 < this.imagePlugList.size(); i3++) {
                    jSONArray3.put(this.imagePlugList.get(i3));
                }
                jSONObject.put(SmsSqliteHelper.IMAGE, jSONArray3);
            }
            if (this.musicPlugList.size() > 0) {
                JSONArray jSONArray4 = new JSONArray();
                for (int i4 = 0; i4 < this.musicPlugList.size(); i4++) {
                    jSONArray4.put(this.musicPlugList.get(i4));
                }
                jSONObject.put(MMHttpDefines.TAG_MUSIC, jSONArray4);
            }
            return jSONObject;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void fetchOption(VoteItem voteItem, long j) {
        Uri parse = Uri.parse("content://vc.lx.richtext.votes/vote_options/");
        Cursor query = getContentResolver().query(parse, null, " vote_id = ? ", new String[]{String.valueOf(j)}, null);
        while (query != null && query.moveToNext()) {
            VoteOption voteOption = new VoteOption();
            voteOption.service_id = query.getString(query.getColumnIndex("service_id"));
            voteOption.counter = query.getInt(query.getColumnIndex("counter"));
            voteOption.contact_name = query.getString(query.getColumnIndex("contact_name"));
            voteOption.display_order = query.getInt(query.getColumnIndex("display_order"));
            voteItem.options.add(voteOption);
        }
        if (query != null) {
            query.close();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.pushUpdaterHandler.sendMessageDelayed(this.pushUpdaterHandler.obtainMessage(1), (long) SmsApplication.FAVORITE_UPDATER_TIME);
        this.mMusicService = new TopMusicService(getApplicationContext());
        this.mImageService = new ImageService(getApplicationContext());
        super.onCreate();
    }
}
