package vc.lx.sms.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.widget.RemoteViews;
import java.io.File;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.VersionInfo;
import vc.lx.sms.cmcc.http.parser.VersionInfoParser;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ApkUpdateService extends Service {
    private String apk_path = "sms.apk";
    private DownloadApkTask mDownloadTask;
    private GetLatestVersionTask mGetVersionTask;

    class DownloadApkTask extends AsyncTask<Void, Integer, File> {
        private String _file;
        private int _totalSize;

        public DownloadApkTask(String str) {
            this._file = str;
        }

        private void notificationDownLoad(int i) {
            NotificationManager notificationManager = SmsApplication.getInstance().getmNotificationManager();
            Notification notification = new Notification(R.drawable.icon, ApkUpdateService.this.getString(R.string.downloading_apk), System.currentTimeMillis());
            PendingIntent activity = PendingIntent.getActivity(ApkUpdateService.this.getApplicationContext(), 0, null, 0);
            RemoteViews remoteViews = new RemoteViews(ApkUpdateService.this.getPackageName(), (int) R.layout.download_notification);
            remoteViews.setProgressBar(R.id.downloadbar, this._totalSize, i, false);
            notification.contentView = remoteViews;
            remoteViews.setTextViewText(R.id.info, ApkUpdateService.this.getString(R.string.downloading_apk));
            notification.contentIntent = activity;
            notificationManager.notify(R.layout.download_notification, notification);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0084 A[SYNTHETIC, Splitter:B:29:0x0084] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0089 A[SYNTHETIC, Splitter:B:32:0x0089] */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x00cb A[SYNTHETIC, Splitter:B:53:0x00cb] */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x00d0 A[SYNTHETIC, Splitter:B:56:0x00d0] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.io.File doInBackground(java.lang.Void... r13) {
            /*
                r12 = this;
                r10 = 0
                r9 = 2130903081(0x7f030029, float:1.741297E38)
                r0 = 0
                r1 = 0
                r2 = 0
                vc.lx.sms.service.ApkUpdateService r4 = vc.lx.sms.service.ApkUpdateService.this     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                java.io.File r4 = r4.getAndRemoveFile()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                java.lang.String r5 = r12._file     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                vc.lx.sms.cmcc.api.CmccHttpApi r6 = vc.lx.sms.cmcc.api.CmccHttpApi.createDefaultApi()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                vc.lx.sms.service.ApkUpdateService r7 = vc.lx.sms.service.ApkUpdateService.this     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                android.content.Context r7 = r7.getApplicationContext()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                r8 = 0
                org.apache.http.message.BasicNameValuePair[] r8 = new org.apache.http.message.BasicNameValuePair[r8]     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                org.apache.http.HttpResponse r5 = r6.realReqExcute(r7, r5, r8)     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                org.apache.http.StatusLine r6 = r5.getStatusLine()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                int r6 = r6.getStatusCode()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                r7 = 200(0xc8, float:2.8E-43)
                if (r6 == r7) goto L_0x0044
                if (r10 == 0) goto L_0x0032
                r0.close()     // Catch:{ IOException -> 0x00df }
            L_0x0032:
                if (r10 == 0) goto L_0x0037
                r1.close()     // Catch:{ IOException -> 0x00e2 }
            L_0x0037:
                vc.lx.sms2.SmsApplication r0 = vc.lx.sms2.SmsApplication.getInstance()
                android.app.NotificationManager r0 = r0.getmNotificationManager()
                r0.cancel(r9)
                r0 = r10
            L_0x0043:
                return r0
            L_0x0044:
                org.apache.http.HttpEntity r5 = r5.getEntity()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                if (r5 == 0) goto L_0x00b0
                long r0 = r5.getContentLength()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                int r0 = (int) r0     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                r12._totalSize = r0     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                java.io.InputStream r0 = r5.getContent()     // Catch:{ Exception -> 0x0101, all -> 0x00c6 }
                java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0106, all -> 0x00f5 }
                java.lang.String r5 = "rws"
                r1.<init>(r4, r5)     // Catch:{ Exception -> 0x0106, all -> 0x00f5 }
                r5 = 8192(0x2000, float:1.14794E-41)
                byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
            L_0x0060:
                int r6 = r0.read(r5)     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                if (r6 < 0) goto L_0x0099
                long r7 = (long) r6     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                long r2 = r2 + r7
                r7 = 0
                r1.write(r5, r7, r6)     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                r6 = 1
                java.lang.Integer[] r6 = new java.lang.Integer[r6]     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                r7 = 0
                int r8 = (int) r2     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                r6[r7] = r8     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                r12.onProgressUpdate(r6)     // Catch:{ Exception -> 0x007b, all -> 0x00fa }
                goto L_0x0060
            L_0x007b:
                r2 = move-exception
                r11 = r2
                r2 = r0
                r0 = r11
            L_0x007f:
                r0.printStackTrace()     // Catch:{ all -> 0x00ff }
                if (r2 == 0) goto L_0x0087
                r2.close()     // Catch:{ IOException -> 0x00ed }
            L_0x0087:
                if (r1 == 0) goto L_0x008c
                r1.close()     // Catch:{ IOException -> 0x00ef }
            L_0x008c:
                vc.lx.sms2.SmsApplication r0 = vc.lx.sms2.SmsApplication.getInstance()
                android.app.NotificationManager r0 = r0.getmNotificationManager()
                r0.cancel(r9)
            L_0x0097:
                r0 = r10
                goto L_0x0043
            L_0x0099:
                if (r0 == 0) goto L_0x009e
                r0.close()     // Catch:{ IOException -> 0x00e5 }
            L_0x009e:
                if (r1 == 0) goto L_0x00a3
                r1.close()     // Catch:{ IOException -> 0x00e7 }
            L_0x00a3:
                vc.lx.sms2.SmsApplication r0 = vc.lx.sms2.SmsApplication.getInstance()
                android.app.NotificationManager r0 = r0.getmNotificationManager()
                r0.cancel(r9)
                r0 = r4
                goto L_0x0043
            L_0x00b0:
                if (r10 == 0) goto L_0x00b5
                r0.close()     // Catch:{ IOException -> 0x00e9 }
            L_0x00b5:
                if (r10 == 0) goto L_0x00ba
                r1.close()     // Catch:{ IOException -> 0x00eb }
            L_0x00ba:
                vc.lx.sms2.SmsApplication r0 = vc.lx.sms2.SmsApplication.getInstance()
                android.app.NotificationManager r0 = r0.getmNotificationManager()
                r0.cancel(r9)
                goto L_0x0097
            L_0x00c6:
                r0 = move-exception
                r1 = r10
                r2 = r10
            L_0x00c9:
                if (r2 == 0) goto L_0x00ce
                r2.close()     // Catch:{ IOException -> 0x00f1 }
            L_0x00ce:
                if (r1 == 0) goto L_0x00d3
                r1.close()     // Catch:{ IOException -> 0x00f3 }
            L_0x00d3:
                vc.lx.sms2.SmsApplication r1 = vc.lx.sms2.SmsApplication.getInstance()
                android.app.NotificationManager r1 = r1.getmNotificationManager()
                r1.cancel(r9)
                throw r0
            L_0x00df:
                r0 = move-exception
                goto L_0x0032
            L_0x00e2:
                r0 = move-exception
                goto L_0x0037
            L_0x00e5:
                r0 = move-exception
                goto L_0x009e
            L_0x00e7:
                r0 = move-exception
                goto L_0x00a3
            L_0x00e9:
                r0 = move-exception
                goto L_0x00b5
            L_0x00eb:
                r0 = move-exception
                goto L_0x00ba
            L_0x00ed:
                r0 = move-exception
                goto L_0x0087
            L_0x00ef:
                r0 = move-exception
                goto L_0x008c
            L_0x00f1:
                r2 = move-exception
                goto L_0x00ce
            L_0x00f3:
                r1 = move-exception
                goto L_0x00d3
            L_0x00f5:
                r1 = move-exception
                r2 = r0
                r0 = r1
                r1 = r10
                goto L_0x00c9
            L_0x00fa:
                r2 = move-exception
                r11 = r2
                r2 = r0
                r0 = r11
                goto L_0x00c9
            L_0x00ff:
                r0 = move-exception
                goto L_0x00c9
            L_0x0101:
                r0 = move-exception
                r1 = r10
                r2 = r10
                goto L_0x007f
            L_0x0106:
                r1 = move-exception
                r2 = r0
                r0 = r1
                r1 = r10
                goto L_0x007f
            */
            throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.service.ApkUpdateService.DownloadApkTask.doInBackground(java.lang.Void[]):java.io.File");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(File file) {
            if (file != null) {
                Intent intent = new Intent();
                intent.setAction(PrefsUtil.INTENT_UPDATE_APK);
                intent.putExtra(PrefsUtil.KEY_FILE_PATH, file.getAbsolutePath());
                ApkUpdateService.this.sendBroadcast(intent);
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... numArr) {
            int intValue = numArr[0].intValue();
            if (this._totalSize > 0) {
                notificationDownLoad(intValue);
            }
        }
    }

    class GetLatestVersionTask extends AbstractAsyncTask {
        public GetLatestVersionTask(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.getLatestVersion(this.mEnginehost, ApkUpdateService.this.getApplicationContext());
        }

        public String getTag() {
            return "GetLatestVersionTask";
        }

        public void onPostLogic(MiguType miguType) {
            File unused = ApkUpdateService.this.getAndRemoveFile();
            if (miguType != null && (miguType instanceof VersionInfo)) {
                VersionInfo versionInfo = (VersionInfo) miguType;
                String str = versionInfo.mVersionCode;
                Intent intent = new Intent();
                intent.setAction(PrefsUtil.INTENT_NEW_VERSION);
                intent.putExtra(PrefsUtil.KEY_FILE_PATH, versionInfo.mFile);
                intent.putExtra(PrefsUtil.KEY_FORCE, versionInfo.mForce);
                intent.putExtra(PrefsUtil.KEY_VERSION_CODE, str);
                ApkUpdateService.this.sendBroadcast(intent);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new VersionInfoParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (IllegalStateException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            } catch (SmsParseException e3) {
                e3.printStackTrace();
                return null;
            } catch (SmsException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public File getAndRemoveFile() {
        File file = new File(new File(Environment.getExternalStorageDirectory(), "redcloud"), "apk");
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(file.getAbsolutePath() + "/" + this.apk_path);
        if (file2.exists()) {
            file2.delete();
        }
        return file2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        String str = "";
        if (intent != null) {
            str = intent.getAction();
        }
        if (PrefsUtil.KEY_LATEST_VERSION.equals(str)) {
            if (this.mGetVersionTask != null) {
                this.mGetVersionTask.cancel(true);
            }
            this.mGetVersionTask = new GetLatestVersionTask(getApplicationContext());
            this.mGetVersionTask.execute(new Void[0]);
        } else if (PrefsUtil.KEY_UPDATE_APK.equals(str)) {
            if (this.mDownloadTask != null) {
                this.mDownloadTask.cancel(true);
            }
            String stringExtra = intent.getStringExtra(PrefsUtil.KEY_FILE_PATH);
            if (stringExtra != null) {
                this.mDownloadTask = new DownloadApkTask(stringExtra);
                this.mDownloadTask.execute(new Void[0]);
            }
        }
    }
}
