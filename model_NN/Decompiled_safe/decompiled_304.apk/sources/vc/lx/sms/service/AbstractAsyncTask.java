package vc.lx.sms.service;

import android.content.Context;
import android.os.AsyncTask;
import java.io.IOException;
import org.apache.http.HttpResponse;
import vc.lx.sms.cmcc.SystemController;
import vc.lx.sms.cmcc.api.CmccHttpApi;
import vc.lx.sms.cmcc.api.WrappedResult;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.util.LogTool;
import vc.lx.sms2.R;

public abstract class AbstractAsyncTask extends AsyncTask<Void, Void, WrappedResult> {
    private int _retryTimes = 3;
    protected CmccHttpApi mCmccHttpApi;
    protected Context mContext;
    protected String mEnginehost;

    public AbstractAsyncTask(Context context) {
        this.mContext = context;
        this.mCmccHttpApi = CmccHttpApi.createDefaultApi();
        this.mEnginehost = this.mContext.getString(R.string.dna_engine_host);
    }

    /* access modifiers changed from: protected */
    public WrappedResult doInBackground(Void... voidArr) {
        HttpResponse httpResponse = null;
        WrappedResult wrappedResult = new WrappedResult();
        boolean checkWapStatus = SystemController.checkWapStatus(this.mContext);
        while (this._retryTimes > 0) {
            if (!checkWapStatus) {
                checkWapStatus = SystemController.checkWapStatus(this.mContext);
            }
            LogTool.i(getTag(), "isConnected : " + String.valueOf(checkWapStatus));
            try {
                httpResponse = getRespFromServer();
                break;
            } catch (IOException e) {
                LogTool.i(getTag(), e.getMessage());
                wrappedResult._status = 0;
                this._retryTimes--;
            }
        }
        if (httpResponse != null) {
            LogTool.i(getTag(), "get response!");
            wrappedResult._status = 1;
        }
        wrappedResult._resp = httpResponse;
        return wrappedResult;
    }

    public abstract HttpResponse getRespFromServer() throws IOException;

    public abstract String getTag();

    /* access modifiers changed from: protected */
    public void onPostExecute(WrappedResult wrappedResult) {
        MiguType miguType = null;
        switch (wrappedResult._status) {
            case 1:
                HttpResponse httpResponse = wrappedResult._resp;
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                LogTool.i("HTTP retCode: " + statusCode);
                if (statusCode != 200) {
                }
                miguType = parseEntity(httpResponse);
                break;
        }
        onPostLogic(miguType);
    }

    public abstract void onPostLogic(MiguType miguType);

    public abstract MiguType parseEntity(HttpResponse httpResponse);
}
