package vc.lx.sms.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import vc.lx.sms.service.MiguPlayerServiceInterface;
import vc.lx.sms.util.LogTool;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class MiguPlayerService extends Service {
    public static final String ACTION_NEXT = "next";
    public static final String ACTION_PLAY = "play";
    public static final String ACTION_PREV = "prev";
    public static final String ACTION_STOP = "stop";
    private static final int NOTIFY_ID = 2130903070;
    /* access modifiers changed from: private */
    public static boolean isPause = false;
    /* access modifiers changed from: private */
    public int currentDownloadPosition = 0;
    private String fileUrl = null;
    private final MiguPlayerServiceInterface.Stub mBinder = new MiguPlayerServiceInterface.Stub() {
        public void cancelDownloadTask() throws RemoteException {
            if (MiguPlayerService.this.mMediaPlayer != null) {
                MiguPlayerService.this.mMediaPlayer.stop();
                MediaPlayer unused = MiguPlayerService.this.mMediaPlayer = null;
            }
            if (MiguPlayerService.this.mDownloadTask != null) {
                LogTool.i("cancel", "true");
                MiguPlayerService.this.mDownloadTask.setState(State.CANCEL);
                MiguPlayerService.this.mDownloadTask.cancel(true);
            }
        }

        public int getCurrentPosition() throws RemoteException {
            if (MiguPlayerService.this.mMediaPlayer != null) {
                return MiguPlayerService.this.mMediaPlayer.getCurrentPosition();
            }
            return 0;
        }

        public int getDuration() throws RemoteException {
            if (MiguPlayerService.this.mMediaPlayer != null) {
                return MiguPlayerService.this.mMediaPlayer.getDuration();
            }
            return 0;
        }

        public boolean isPlaying() throws RemoteException {
            if (MiguPlayerService.this.mMediaPlayer != null) {
                return MiguPlayerService.this.mMediaPlayer.isPlaying();
            }
            return false;
        }

        public void pause() throws RemoteException {
            if (MiguPlayerService.this.mMediaPlayer != null) {
                MiguPlayerService.this.mMediaPlayer.pause();
                boolean unused = MiguPlayerService.isPause = true;
                Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 1, MiguPlayerService.this.mTarget);
            }
        }

        public void resume() throws RemoteException {
            if (MiguPlayerService.this.mMediaPlayer != null) {
                MiguPlayerService.this.mMediaPlayer.start();
                boolean unused = MiguPlayerService.isPause = false;
                Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 0, MiguPlayerService.this.mTarget);
            }
        }

        public void start() throws RemoteException {
        }

        public void stop() throws RemoteException {
            if (MiguPlayerService.this.mMediaPlayer != null) {
                MiguPlayerService.this.mMediaPlayer.stop();
                MediaPlayer unused = MiguPlayerService.this.mMediaPlayer = null;
            }
            if (MiguPlayerService.this.mDownloadTask != null) {
                LogTool.i("cancel", "true");
                MiguPlayerService.this.mDownloadTask.setState(State.CANCEL);
                MiguPlayerService.this.mDownloadTask.cancel(true);
            }
        }
    };
    /* access modifiers changed from: private */
    public DownloadMusicSongTask mDownloadTask = null;
    /* access modifiers changed from: private */
    public MediaPlayer mMediaPlayer = new MediaPlayer();
    /* access modifiers changed from: private */
    public AtomicReference<HttpGet> mMethod;
    private NotificationManager mNotificationManager;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener mOnlineMusicOnCompletionListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            LogTool.i("MiguPlayerService", "onCompletion state:" + MiguPlayerService.this.mSongType);
            switch (AnonymousClass3.$SwitchMap$vc$lx$sms$service$MiguPlayerService$SongType[MiguPlayerService.this.mSongType.ordinal()]) {
                case 1:
                    if (MiguPlayerService.this.mDownloadTask != null) {
                        MiguPlayerService.this.mDownloadTask.setState(State.CANCEL);
                        MiguPlayerService.this.mDownloadTask.cancel(true);
                    }
                    if (MiguPlayerService.this.mMediaPlayer != null && MiguPlayerService.this.mMediaPlayer.isPlaying()) {
                        MiguPlayerService.this.mMediaPlayer.stop();
                    }
                    Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 2, MiguPlayerService.this.mTarget);
                    return;
                case 2:
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public SongType mSongType = SongType.RADIO;
    /* access modifiers changed from: private */
    public String mTarget;
    /* access modifiers changed from: private */
    public int timeOutCount;

    /* renamed from: vc.lx.sms.service.MiguPlayerService$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$vc$lx$sms$service$MiguPlayerService$SongType = new int[SongType.values().length];

        static {
            try {
                $SwitchMap$vc$lx$sms$service$MiguPlayerService$SongType[SongType.RADIO.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$vc$lx$sms$service$MiguPlayerService$SongType[SongType.FAV_SONGS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    class DownloadMusicSongTask extends AsyncTask<String, Long, Void> {
        private DefaultHttpClient _client;
        private long _songLength;
        private volatile State _state = State.INIT;

        public DownloadMusicSongTask() {
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0107 A[Catch:{ all -> 0x0174 }] */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x011c A[SYNTHETIC, Splitter:B:43:0x011c] */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0121 A[SYNTHETIC, Splitter:B:46:0x0121] */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x013c A[SYNTHETIC, Splitter:B:52:0x013c] */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0141 A[SYNTHETIC, Splitter:B:55:0x0141] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.String... r10) {
            /*
                r9 = this;
                r0 = 0
                r8 = 0
                r0 = r10[r0]
                org.apache.http.impl.client.DefaultHttpClient r1 = vc.lx.sms.cmcc.api.CmccHttpApi.createHttpClient()
                r9._client = r1
                vc.lx.sms.service.MiguPlayerService r1 = vc.lx.sms.service.MiguPlayerService.this
                android.content.Context r1 = r1.getApplicationContext()
                boolean r1 = vc.lx.sms.util.Util.isCMWapConnected(r1)
                if (r1 != 0) goto L_0x00bc
            L_0x0016:
                org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet
                r1.<init>(r0)
                vc.lx.sms.service.MiguPlayerService r0 = vc.lx.sms.service.MiguPlayerService.this
                java.util.concurrent.atomic.AtomicReference r0 = r0.mMethod
                r0.set(r1)
                r1 = 0
                vc.lx.sms.service.MiguPlayerService$State r0 = vc.lx.sms.service.MiguPlayerService.State.CONNECT     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                r9._state = r0     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                org.apache.http.impl.client.DefaultHttpClient r3 = r9._client     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                vc.lx.sms.service.MiguPlayerService r0 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.mMethod     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                org.apache.http.client.methods.HttpUriRequest r0 = (org.apache.http.client.methods.HttpUriRequest) r0     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                org.apache.http.HttpResponse r0 = r3.execute(r0)     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                if (r0 == 0) goto L_0x0182
                long r3 = r0.getContentLength()     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                r9._songLength = r3     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                vc.lx.sms.service.MiguPlayerService$State r3 = vc.lx.sms.service.MiguPlayerService.State.DOWNLOADING     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                r9._state = r3     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x00f6, all -> 0x0137 }
                long r3 = r9._songLength     // Catch:{ Exception -> 0x0176, all -> 0x016a }
                vc.lx.sms.service.MiguPlayerService r5 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ Exception -> 0x0176, all -> 0x016a }
                android.content.Context r5 = r5.getApplicationContext()     // Catch:{ Exception -> 0x0176, all -> 0x016a }
                java.lang.String r6 = "weilingyin.tmp"
                java.io.File r3 = vc.lx.sms.util.Util.initCacheFile(r3, r5, r6)     // Catch:{ Exception -> 0x0176, all -> 0x016a }
                java.io.RandomAccessFile r4 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0176, all -> 0x016a }
                java.lang.String r5 = "rws"
                r4.<init>(r3, r5)     // Catch:{ Exception -> 0x0176, all -> 0x016a }
                r3 = 4096(0x1000, float:5.74E-42)
                byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                vc.lx.sms.service.MiguPlayerService r5 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                android.content.Context r5 = r5.getApplicationContext()     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                vc.lx.sms.service.MiguPlayerService r6 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                android.content.res.Resources r6 = r6.getResources()     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                r7 = 2131296620(0x7f09016c, float:1.8211162E38)
                java.lang.String r6 = r6.getString(r7)     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                vc.lx.sms.util.Util.broadcastProgressDlgOpen(r5, r6)     // Catch:{ Exception -> 0x017c, all -> 0x016f }
            L_0x007f:
                int r5 = r0.read(r3)     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                if (r5 < 0) goto L_0x00d2
                long r6 = (long) r5     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                long r1 = r1 + r6
                r6 = 0
                r4.write(r3, r6, r5)     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                r5 = 1
                java.lang.Long[] r5 = new java.lang.Long[r5]     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                r6 = 0
                java.lang.Long r7 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                r5[r6] = r7     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                r9.publishProgress(r5)     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                vc.lx.sms.service.MiguPlayerService$State r5 = r9._state     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                vc.lx.sms.service.MiguPlayerService$State r6 = vc.lx.sms.service.MiguPlayerService.State.CANCEL     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                if (r5 != r6) goto L_0x007f
                if (r0 == 0) goto L_0x00a3
                r0.close()     // Catch:{ IOException -> 0x0157 }
            L_0x00a3:
                if (r4 == 0) goto L_0x00a8
                r4.close()     // Catch:{ IOException -> 0x015a }
            L_0x00a8:
                vc.lx.sms.service.MiguPlayerService r0 = vc.lx.sms.service.MiguPlayerService.this
                java.util.concurrent.atomic.AtomicReference r0 = r0.mMethod
                r0.set(r8)
                org.apache.http.impl.client.DefaultHttpClient r0 = r9._client
                org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
                r0.shutdown()
                r0 = r8
            L_0x00bb:
                return r0
            L_0x00bc:
                org.apache.http.HttpHost r1 = new org.apache.http.HttpHost
                java.lang.String r2 = vc.lx.sms.cmcc.MMHttpDefines.CMCC_WAP_PROXY_HOST
                int r3 = vc.lx.sms.cmcc.MMHttpDefines.CMCC_WAP_PROXY_PORT
                r1.<init>(r2, r3)
                org.apache.http.impl.client.DefaultHttpClient r2 = r9._client
                org.apache.http.params.HttpParams r2 = r2.getParams()
                java.lang.String r3 = "http.route.default-proxy"
                r2.setParameter(r3, r1)
                goto L_0x0016
            L_0x00d2:
                vc.lx.sms.service.MiguPlayerService$State r1 = vc.lx.sms.service.MiguPlayerService.State.SUCCESS     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                r9._state = r1     // Catch:{ Exception -> 0x017c, all -> 0x016f }
                r1 = r0
                r0 = r4
            L_0x00d8:
                if (r1 == 0) goto L_0x00dd
                r1.close()     // Catch:{ IOException -> 0x015d }
            L_0x00dd:
                if (r0 == 0) goto L_0x00e2
                r0.close()     // Catch:{ IOException -> 0x0160 }
            L_0x00e2:
                vc.lx.sms.service.MiguPlayerService r0 = vc.lx.sms.service.MiguPlayerService.this
                java.util.concurrent.atomic.AtomicReference r0 = r0.mMethod
                r0.set(r8)
                org.apache.http.impl.client.DefaultHttpClient r0 = r9._client
                org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
                r0.shutdown()
            L_0x00f4:
                r0 = r8
                goto L_0x00bb
            L_0x00f6:
                r0 = move-exception
                r1 = r8
                r2 = r8
            L_0x00f9:
                vc.lx.sms.service.MiguPlayerService r3 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ all -> 0x0174 }
                vc.lx.sms.service.MiguPlayerService.access$508(r3)     // Catch:{ all -> 0x0174 }
                vc.lx.sms.service.MiguPlayerService r3 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ all -> 0x0174 }
                int r3 = r3.timeOutCount     // Catch:{ all -> 0x0174 }
                r4 = 5
                if (r3 <= r4) goto L_0x0117
                vc.lx.sms.service.MiguPlayerService r3 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ all -> 0x0174 }
                android.content.Context r3 = r3.getApplicationContext()     // Catch:{ all -> 0x0174 }
                r4 = 3
                vc.lx.sms.service.MiguPlayerService r5 = vc.lx.sms.service.MiguPlayerService.this     // Catch:{ all -> 0x0174 }
                java.lang.String r5 = r5.mTarget     // Catch:{ all -> 0x0174 }
                vc.lx.sms.util.Util.sendPlayingNotification(r3, r4, r5)     // Catch:{ all -> 0x0174 }
            L_0x0117:
                r0.printStackTrace()     // Catch:{ all -> 0x0174 }
                if (r2 == 0) goto L_0x011f
                r2.close()     // Catch:{ IOException -> 0x0162 }
            L_0x011f:
                if (r1 == 0) goto L_0x0124
                r1.close()     // Catch:{ IOException -> 0x0164 }
            L_0x0124:
                vc.lx.sms.service.MiguPlayerService r0 = vc.lx.sms.service.MiguPlayerService.this
                java.util.concurrent.atomic.AtomicReference r0 = r0.mMethod
                r0.set(r8)
                org.apache.http.impl.client.DefaultHttpClient r0 = r9._client
                org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
                r0.shutdown()
                goto L_0x00f4
            L_0x0137:
                r0 = move-exception
                r1 = r8
                r2 = r8
            L_0x013a:
                if (r2 == 0) goto L_0x013f
                r2.close()     // Catch:{ IOException -> 0x0166 }
            L_0x013f:
                if (r1 == 0) goto L_0x0144
                r1.close()     // Catch:{ IOException -> 0x0168 }
            L_0x0144:
                vc.lx.sms.service.MiguPlayerService r1 = vc.lx.sms.service.MiguPlayerService.this
                java.util.concurrent.atomic.AtomicReference r1 = r1.mMethod
                r1.set(r8)
                org.apache.http.impl.client.DefaultHttpClient r1 = r9._client
                org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
                r1.shutdown()
                throw r0
            L_0x0157:
                r0 = move-exception
                goto L_0x00a3
            L_0x015a:
                r0 = move-exception
                goto L_0x00a8
            L_0x015d:
                r1 = move-exception
                goto L_0x00dd
            L_0x0160:
                r0 = move-exception
                goto L_0x00e2
            L_0x0162:
                r0 = move-exception
                goto L_0x011f
            L_0x0164:
                r0 = move-exception
                goto L_0x0124
            L_0x0166:
                r2 = move-exception
                goto L_0x013f
            L_0x0168:
                r1 = move-exception
                goto L_0x0144
            L_0x016a:
                r1 = move-exception
                r2 = r0
                r0 = r1
                r1 = r8
                goto L_0x013a
            L_0x016f:
                r1 = move-exception
                r2 = r0
                r0 = r1
                r1 = r4
                goto L_0x013a
            L_0x0174:
                r0 = move-exception
                goto L_0x013a
            L_0x0176:
                r1 = move-exception
                r2 = r0
                r0 = r1
                r1 = r8
                goto L_0x00f9
            L_0x017c:
                r1 = move-exception
                r2 = r0
                r0 = r1
                r1 = r4
                goto L_0x00f9
            L_0x0182:
                r0 = r8
                r1 = r8
                goto L_0x00d8
            */
            throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.service.MiguPlayerService.DownloadMusicSongTask.doInBackground(java.lang.String[]):java.lang.Void");
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
            HttpGet httpGet = (HttpGet) MiguPlayerService.this.mMethod.get();
            if (httpGet != null) {
                httpGet.abort();
                MiguPlayerService.this.mMethod.set(null);
            }
            if (this._client != null) {
                this._client.getConnectionManager().shutdown();
                this._client = null;
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Long... lArr) {
            long longValue = lArr[0].longValue();
            if (this._state != State.CANCEL) {
                LogTool.i("downloadsize", "downloadsize=" + longValue);
                if (this._state == State.DOWNLOADING && longValue > ((long) SmsApplication.DEFAULT_DAOWNLOAD_BUFFER_SIZE) && MiguPlayerService.this.mMediaPlayer == null) {
                    File file = new File(Util.createAndReturnFolderName(MiguPlayerService.this.getApplicationContext(), this._songLength) + "/weilingyin.tmp");
                    Util.broadcastProgressDlgClose(MiguPlayerService.this.getApplicationContext(), "");
                    try {
                        LogTool.i("DownloadMusicSongTask", "try to play the song");
                        MediaPlayer unused = MiguPlayerService.this.mMediaPlayer = new MediaPlayer();
                        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rws");
                        MiguPlayerService.this.mMediaPlayer.setDataSource(randomAccessFile.getFD());
                        randomAccessFile.close();
                        MiguPlayerService.this.mMediaPlayer.setAudioStreamType(3);
                        MiguPlayerService.this.mMediaPlayer.setOnCompletionListener(MiguPlayerService.this.mOnlineMusicOnCompletionListener);
                        MiguPlayerService.this.mMediaPlayer.prepare();
                        MiguPlayerService.this.mMediaPlayer.start();
                        Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 0, MiguPlayerService.this.mTarget);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e2) {
                        e2.printStackTrace();
                    } catch (FileNotFoundException e3) {
                        e3.printStackTrace();
                    } catch (IOException e4) {
                        if (MiguPlayerService.this.mMediaPlayer != null) {
                            MiguPlayerService.this.mMediaPlayer.reset();
                        }
                        e4.printStackTrace();
                    }
                } else if (this._state == State.DOWNLOADING && longValue > ((long) SmsApplication.DEFAULT_DAOWNLOAD_BUFFER_SIZE) && MiguPlayerService.this.mMediaPlayer != null) {
                    int unused2 = MiguPlayerService.this.currentDownloadPosition = (int) longValue;
                    float f = (float) ((longValue * ((long) SmsApplication.DEFAULT_EXPAND_SIZE)) / this._songLength);
                    float f2 = 0.0f;
                    try {
                        if (MiguPlayerService.this.mMediaPlayer.getDuration() > 0) {
                            f2 = (float) ((MiguPlayerService.this.mMediaPlayer.getCurrentPosition() * 1000) / MiguPlayerService.this.mMediaPlayer.getDuration());
                        }
                        LogTool.i("downLoadPercent", "downLoadPercent:" + f);
                        LogTool.i("playTimePercent", "playTimePercent:" + f2);
                        if (f <= ((float) SmsApplication.DEFAULT_EXPAND_SIZE) && f2 <= ((float) SmsApplication.DEFAULT_EXPAND_SIZE)) {
                            if (f - f2 <= 20.0f) {
                                Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 2, MiguPlayerService.this.mTarget);
                                MiguPlayerService.this.mMediaPlayer.pause();
                                SmsApplication.getInstance().setCurrentDownLoadSize(MiguPlayerService.this.currentDownloadPosition);
                            } else if (!MiguPlayerService.this.mMediaPlayer.isPlaying() && !MiguPlayerService.isPause) {
                                if (MiguPlayerService.this.currentDownloadPosition - SmsApplication.getInstance().getCurrentDownLoadSize() >= SmsApplication.DEFAULT_DAOWNLOAD_BUFFER_SIZE / 4) {
                                    MiguPlayerService.this.mMediaPlayer.start();
                                    Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 0, MiguPlayerService.this.mTarget);
                                    return;
                                }
                                Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 2, MiguPlayerService.this.mTarget);
                            }
                        }
                    } catch (IllegalStateException e5) {
                        Util.sendPlayingNotification(MiguPlayerService.this.getApplicationContext(), 3, MiguPlayerService.this.mTarget);
                        e5.printStackTrace();
                    }
                }
            }
        }

        public void setState(State state) {
            this._state = state;
        }
    }

    public enum SongType {
        RADIO,
        LOCAL,
        FAV_SONGS
    }

    public enum State {
        INIT,
        CONNECT,
        DOWNLOADING,
        SAVE,
        ERROR,
        SUCCESS,
        CANCEL,
        PLAYING
    }

    static /* synthetic */ int access$508(MiguPlayerService miguPlayerService) {
        int i = miguPlayerService.timeOutCount;
        miguPlayerService.timeOutCount = i + 1;
        return i;
    }

    private void playLocalSong(Bundle bundle) {
        String string = bundle.getString(PrefsUtil.KEY_SONG_FILE_PAHT);
        try {
            this.mMediaPlayer = new MediaPlayer();
            this.mMediaPlayer.reset();
            this.mMediaPlayer.setDataSource(string);
            this.mMediaPlayer.setAudioStreamType(3);
            this.mMediaPlayer.prepare();
            this.mMediaPlayer.start();
            Util.sendPlayingNotification(getApplicationContext(), 0, this.mTarget);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public IBinder getBinder() {
        return this.mBinder;
    }

    public IBinder onBind(Intent intent) {
        LogTool.i("onBind", "onBind");
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
        this.mNotificationManager = (NotificationManager) getSystemService("notification");
        this.mMethod = new AtomicReference<>();
    }

    public void onDestroy() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.release();
        }
        this.mNotificationManager.cancel(R.layout.conversation_list_screen);
    }

    public void onRebind(Intent intent) {
        LogTool.i("onRebind", "onRebind");
        super.onRebind(intent);
    }

    public void onStart(Intent intent, int i) {
        LogTool.i("onStart", "onStart");
        super.onStart(intent, i);
        if (intent != null) {
            Bundle extras = intent.getExtras();
            String action = intent.getAction();
            this.timeOutCount = 0;
            if ("play".equals(action)) {
                this.mTarget = extras.getString(PrefsUtil.KEY_SONG_TARGET);
                if (this.mDownloadTask != null) {
                    this.mDownloadTask.setState(State.CANCEL);
                    this.mDownloadTask.cancel(true);
                    LogTool.i("DownloadMusicSongTask", "is cancelled : " + this.mDownloadTask.isCancelled());
                }
                if (this.mMediaPlayer != null) {
                    this.mMediaPlayer.release();
                    this.mMediaPlayer = null;
                }
                if (extras.getSerializable(PrefsUtil.KEY_SONG_TYPE) != null) {
                    this.mSongType = (SongType) extras.getSerializable(PrefsUtil.KEY_SONG_TYPE);
                }
                if (this.mSongType == SongType.RADIO) {
                    this.fileUrl = extras.getString(PrefsUtil.KEY_SONG_FILE_PAHT);
                    this.mDownloadTask = (DownloadMusicSongTask) new DownloadMusicSongTask().execute(this.fileUrl);
                } else if (this.mSongType == SongType.LOCAL) {
                    playLocalSong(extras);
                }
            }
        }
    }
}
