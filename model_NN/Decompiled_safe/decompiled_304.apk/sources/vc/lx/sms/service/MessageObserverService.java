package vc.lx.sms.service;

import android.app.Service;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.IBinder;
import com.android.provider.Telephony;

public class MessageObserverService extends Service {
    private Handler mContentObserverHandler = new Handler();
    private ContentObserver mSmsContentObserver = new ContentObserver(this.mContentObserverHandler) {
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00d7  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onChange(boolean r13) {
            /*
                r12 = this;
                r6 = 0
                vc.lx.sms.service.MessageObserverService r0 = vc.lx.sms.service.MessageObserverService.this     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                android.net.Uri r1 = com.android.provider.Telephony.Sms.CONTENT_URI     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                r2 = 4
                java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                r3 = 0
                java.lang.String r4 = "thread_id"
                r2[r3] = r4     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                r3 = 1
                java.lang.String r4 = "date"
                r2[r3] = r4     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                r3 = 2
                java.lang.String r4 = "body"
                r2[r3] = r4     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                r3 = 3
                java.lang.String r4 = "address"
                r2[r3] = r4     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                java.lang.String r3 = "read = 0"
                r4 = 0
                java.lang.String r5 = "date DESC"
                android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00cb, all -> 0x00d3 }
                boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                if (r1 == 0) goto L_0x00c5
                r1 = 0
                long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r3 = 1
                long r3 = r0.getLong(r3)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r5 = 2
                java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r6 = 3
                java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                vc.lx.sms.service.MessageObserverService r7 = vc.lx.sms.service.MessageObserverService.this     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.content.Context r7 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                long r7 = vc.lx.sms.util.PrefsUtil.getLastPopupMsgDate(r7)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                int r7 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
                if (r7 > 0) goto L_0x005b
                if (r0 == 0) goto L_0x005a
                r0.close()
            L_0x005a:
                return
            L_0x005b:
                long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r9 = 3000(0xbb8, double:1.482E-320)
                long r7 = r7 - r9
                int r7 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
                if (r7 <= 0) goto L_0x00c5
                vc.lx.sms.service.MessageObserverService r7 = vc.lx.sms.service.MessageObserverService.this     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.content.Context r7 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                boolean r7 = vc.lx.sms.intents.SsmIntents.isApplicationBroughtToBackground(r7)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                if (r7 == 0) goto L_0x00c5
                vc.lx.sms.service.MessageObserverService r7 = vc.lx.sms.service.MessageObserverService.this     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.content.Context r7 = r7.getApplicationContext()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                vc.lx.sms.util.PrefsUtil.saveLastPopupMsgDate(r7, r3)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.content.Intent r7 = new android.content.Intent     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                vc.lx.sms.service.MessageObserverService r8 = vc.lx.sms.service.MessageObserverService.this     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.content.Context r8 = r8.getApplicationContext()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                java.lang.Class<vc.lx.sms.ui.SmsPopupActivity> r9 = vc.lx.sms.ui.SmsPopupActivity.class
                r7.<init>(r8, r9)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.os.Bundle r8 = new android.os.Bundle     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r8.<init>()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                java.lang.String r9 = "EXTRAS_THREAD_ID"
                r8.putLong(r9, r1)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                java.lang.String r1 = "EXTRAS_TIMESTAMP"
                r8.putLong(r1, r3)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                java.lang.String r1 = "EXTRAS_MESSAGE_BODY"
                r8.putString(r1, r5)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                java.lang.String r1 = "EXTRAS_FROM_ADDRESS"
                r8.putString(r1, r6)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                java.lang.String r1 = "EXTRAS_UNREAD_COUNT"
                int r2 = r0.getCount()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r8.putInt(r1, r2)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r7.putExtras(r8)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r1 = 276824064(0x10800000, float:5.0487098E-29)
                r7.setFlags(r1)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                vc.lx.sms.service.MessageObserverService r1 = vc.lx.sms.service.MessageObserverService.this     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.content.Context r1 = r1.getApplicationContext()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r1.startActivity(r7)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                vc.lx.sms.service.MessageObserverService r1 = vc.lx.sms.service.MessageObserverService.this     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                android.content.Context r1 = r1.getApplicationContext()     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
                r2 = 1
                com.android.mms.transaction.MessagingNotification.updateNewMessageIndicator(r1, r2)     // Catch:{ Exception -> 0x00e0, all -> 0x00db }
            L_0x00c5:
                if (r0 == 0) goto L_0x005a
                r0.close()
                goto L_0x005a
            L_0x00cb:
                r0 = move-exception
                r0 = r6
            L_0x00cd:
                if (r0 == 0) goto L_0x005a
                r0.close()
                goto L_0x005a
            L_0x00d3:
                r0 = move-exception
                r1 = r6
            L_0x00d5:
                if (r1 == 0) goto L_0x00da
                r1.close()
            L_0x00da:
                throw r0
            L_0x00db:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
                goto L_0x00d5
            L_0x00e0:
                r1 = move-exception
                goto L_0x00cd
            */
            throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.service.MessageObserverService.AnonymousClass1.onChange(boolean):void");
        }
    };

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        getContentResolver().registerContentObserver(Telephony.Sms.CONTENT_URI, true, this.mSmsContentObserver);
        super.onCreate();
    }

    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.mSmsContentObserver);
        super.onDestroy();
    }
}
