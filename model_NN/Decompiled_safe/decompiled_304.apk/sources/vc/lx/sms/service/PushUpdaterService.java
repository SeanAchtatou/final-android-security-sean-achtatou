package vc.lx.sms.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.ContentList;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.parser.ContentListParser;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms2.SmsApplication;

public class PushUpdaterService extends Service {
    /* access modifiers changed from: private */
    public TopMusicService mTopMusicService;
    /* access modifiers changed from: private */
    public ArrayList<SongItem> mUnRedSongList;
    /* access modifiers changed from: private */
    public ArrayList<SongItem> mUpdaterSongList;
    /* access modifiers changed from: private */
    public Handler pushUpdaterHandler = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    ArrayList unused = PushUpdaterService.this.mUnRedSongList = (ArrayList) PushUpdaterService.this.mTopMusicService.getSongItemsByReadStatus("no");
                    if (PushUpdaterService.this.mUnRedSongList.size() < SmsApplication.PUSH_UPDATER_COUNT) {
                        new SongListTask(PushUpdaterService.this.getApplicationContext(), 1).execute(new Void[0]);
                    }
                    PushUpdaterService.this.pushUpdaterHandler.sendMessageDelayed(PushUpdaterService.this.pushUpdaterHandler.obtainMessage(1), (long) SmsApplication.PUSH_UPDATER_TIME);
                    return;
                default:
                    return;
            }
        }
    };

    class SongListTask extends AbstractAsyncTask {
        private int _page;

        public SongListTask(Context context, int i) {
            super(context);
            this._page = i;
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.contentlist(this.mEnginehost, this.mContext, String.valueOf(this._page));
        }

        public String getTag() {
            return "SongListTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof ContentList)) {
                ContentList contentList = (ContentList) miguType;
                if (PushUpdaterService.this.mUpdaterSongList == null) {
                    ArrayList unused = PushUpdaterService.this.mUpdaterSongList = new ArrayList();
                }
                PushUpdaterService.this.mUpdaterSongList.addAll(contentList.items);
                PushUpdaterService.this.pushUpdaterHandler.post(new Runnable() {
                    public void run() {
                    }
                });
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new ContentListParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.mTopMusicService = new TopMusicService(getApplicationContext());
        this.pushUpdaterHandler.sendMessageDelayed(this.pushUpdaterHandler.obtainMessage(1), (long) SmsApplication.PUSH_UPDATER_TIME);
        super.onCreate();
    }
}
