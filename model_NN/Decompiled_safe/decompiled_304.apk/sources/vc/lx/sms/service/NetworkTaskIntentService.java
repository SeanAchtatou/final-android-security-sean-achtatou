package vc.lx.sms.service;

import android.app.IntentService;
import android.content.Intent;
import java.io.IOException;
import vc.lx.sms.cmcc.api.CmccHttpApi;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public class NetworkTaskIntentService extends IntentService {
    public static final int LIKE_SONG_ACTION = 1;
    public static final String TAG = "NetworkTaskIntentService";
    public static final int UNLIKE_SONG_ACTION = 2;

    public NetworkTaskIntentService() {
        super(TAG);
    }

    private void songAction(boolean z, String str) {
        try {
            CmccHttpApi.createDefaultApi().songFavoriteAction(getString(R.string.dna_engine_host), getApplicationContext(), str, z);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        int intExtra = intent.getIntExtra(PrefsUtil.KEY_SONG_ACTION, -1);
        String stringExtra = intent.getStringExtra(PrefsUtil.KEY_SONGITEM_PLUG);
        if (stringExtra != null) {
            switch (intExtra) {
                case 1:
                    songAction(true, stringExtra);
                    return;
                case 2:
                    songAction(false, stringExtra);
                    return;
                default:
                    return;
            }
        }
    }
}
