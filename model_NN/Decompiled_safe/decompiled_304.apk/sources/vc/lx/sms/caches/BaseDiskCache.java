package vc.lx.sms.caches;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class BaseDiskCache implements ICache {
    private static final boolean DEBUG = true;
    private static final int MIN_FILE_SIZE_IN_BYTES = 100;
    private static final String NOMEDIA = ".nomedia";
    private static final String TAG = "BaseDiskCache";
    private File mStorageDirectory;

    public BaseDiskCache(String str, String str2) {
        File file = new File(new File(Environment.getExternalStorageDirectory(), str), str2);
        createDirectory(file);
        this.mStorageDirectory = file;
        cleanupSimple();
    }

    private static final void createDirectory(File file) {
        if (!file.exists()) {
            Log.d(TAG, "Trying to create storageDirectory: " + String.valueOf(file.mkdirs()));
            Log.d(TAG, "Exists: " + file + " " + String.valueOf(file.exists()));
            Log.d(TAG, "State: " + Environment.getExternalStorageState());
            Log.d(TAG, "Isdir: " + file + " " + String.valueOf(file.isDirectory()));
            Log.d(TAG, "Readable: " + file + " " + String.valueOf(file.canRead()));
            Log.d(TAG, "Writable: " + file + " " + String.valueOf(file.canWrite()));
            File parentFile = file.getParentFile();
            Log.d(TAG, "Exists: " + parentFile + " " + String.valueOf(parentFile.exists()));
            Log.d(TAG, "Isdir: " + parentFile + " " + String.valueOf(parentFile.isDirectory()));
            Log.d(TAG, "Readable: " + parentFile + " " + String.valueOf(parentFile.canRead()));
            Log.d(TAG, "Writable: " + parentFile + " " + String.valueOf(parentFile.canWrite()));
            File parentFile2 = parentFile.getParentFile();
            Log.d(TAG, "Exists: " + parentFile2 + " " + String.valueOf(parentFile2.exists()));
            Log.d(TAG, "Isdir: " + parentFile2 + " " + String.valueOf(parentFile2.isDirectory()));
            Log.d(TAG, "Readable: " + parentFile2 + " " + String.valueOf(parentFile2.canRead()));
            Log.d(TAG, "Writable: " + parentFile2 + " " + String.valueOf(parentFile2.canWrite()));
        }
        File file2 = new File(file, NOMEDIA);
        if (!file2.exists()) {
            try {
                Log.d(TAG, "Created file: " + file2 + " " + String.valueOf(file2.createNewFile()));
            } catch (IOException e) {
                Log.d(TAG, "Unable to create .nomedia file for some reason.", e);
                throw new IllegalStateException("Unable to create nomedia file.");
            }
        }
        if (!file.isDirectory() || !file2.exists()) {
            throw new RuntimeException("Unable to create storage directory and nomedia file.");
        }
    }

    public void cleanup() {
        String[] list = this.mStorageDirectory.list();
        if (list != null) {
            for (String file : list) {
                File file2 = new File(this.mStorageDirectory, file);
                if (!file2.equals(new File(this.mStorageDirectory, NOMEDIA)) && file2.length() <= 100) {
                    Log.d(TAG, "Deleting: " + file2);
                    file2.delete();
                }
            }
        }
    }

    public void cleanupSimple() {
        String[] list = this.mStorageDirectory.list();
        if (list != null) {
            Log.d(TAG, "Found disk cache length to be: " + list.length);
            if (list.length > 1000) {
                Log.d(TAG, "Disk cache found to : " + list);
                int length = list.length - 1;
                int i = length - 50;
                while (length > i) {
                    File file = new File(this.mStorageDirectory, list[length]);
                    Log.d(TAG, "  deleting: " + file.getName());
                    file.delete();
                    length--;
                }
            }
        }
    }

    public void clear() {
        String[] list = this.mStorageDirectory.list();
        if (list != null) {
            for (String file : list) {
                File file2 = new File(this.mStorageDirectory, file);
                if (!file2.equals(new File(this.mStorageDirectory, NOMEDIA))) {
                    Log.d(TAG, "Deleting: " + file2);
                    file2.delete();
                }
            }
        }
        this.mStorageDirectory.delete();
    }

    public boolean exists(String str) {
        return getFile(str).exists();
    }

    public File getFile(String str) {
        return new File(this.mStorageDirectory.toString() + File.separator + str);
    }

    public InputStream getInputStream(String str) throws IOException {
        return new FileInputStream(getFile(str));
    }

    public void invalidate(String str) {
        getFile(str).delete();
    }

    public void store(String str, InputStream inputStream) {
        Log.d(TAG, "store: " + str);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(getFile(str)));
            byte[] bArr = new byte[2048];
            int i = 0;
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read > 0) {
                    bufferedOutputStream.write(bArr, 0, read);
                    i += read;
                } else {
                    bufferedOutputStream.close();
                    Log.d(TAG, "store complete: " + str);
                    return;
                }
            }
        } catch (IOException e) {
            Log.d(TAG, "store failed to store: " + str, e);
        }
    }
}
