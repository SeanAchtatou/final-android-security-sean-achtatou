package vc.lx.sms.caches;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.Observable;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.api.CmccHttpClient;
import vc.lx.sms.cmcc.http.data.SmsImage;
import vc.lx.sms.cmcc.http.parser.SmsImageParser;
import vc.lx.sms.db.ImagesDbService;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

class RemoteResourceFetcher extends Observable {
    public static final boolean DEBUG = true;
    public static final String TAG = "RemoteResourceFetcher";
    private CmccHttpClient _cmccClient = new CmccHttpClient(this._httpClient);
    private final DefaultHttpClient _httpClient = createHttpClient();
    /* access modifiers changed from: private */
    public ConcurrentHashMap<Request, Callable<Request>> mActiveRequestsMap = new ConcurrentHashMap<>();
    private ExecutorService mExecutor;
    /* access modifiers changed from: private */
    public ICache mResourceCache;

    private static class Request {
        String hash;
        Uri uri;

        public Request(Uri uri2, String str) {
            this.uri = uri2;
            this.hash = str;
        }

        public int hashCode() {
            return this.hash.hashCode();
        }
    }

    public RemoteResourceFetcher(ICache iCache) {
        this.mResourceCache = iCache;
        this.mExecutor = Executors.newCachedThreadPool();
    }

    public static final DefaultHttpClient createHttpClient() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    private Callable<Request> fetchImageDetailRequest(final Request request, final String str) {
        return new Callable<Request>() {
            public Request call() throws Exception {
                try {
                    Context applicationContext = SmsApplication.getInstance().getApplicationContext();
                    Log.d(RemoteResourceFetcher.TAG, "Requesting: " + request.uri);
                    String fullShortenImageUrl = Util.getFullShortenImageUrl(applicationContext, str);
                    try {
                        SmsImage parse = new SmsImageParser().parse(EntityUtils.toString(RemoteResourceFetcher.this.realReqExcute(fullShortenImageUrl, false, new BasicNameValuePair("format", "json")).getEntity()));
                        if (parse != null) {
                            new ImagesDbService(applicationContext).insertSmsImage(parse);
                            Uri parse2 = Uri.parse(parse.mThumb);
                            RemoteResourceFetcher.this.fetch(parse2, Uri.encode(parse2.toString()));
                        }
                    } catch (Exception e) {
                    }
                    Log.d(RemoteResourceFetcher.TAG, "Request successful: " + request.uri);
                } catch (IOException e2) {
                    Log.d(RemoteResourceFetcher.TAG, "IOException", e2);
                } finally {
                    Log.d(RemoteResourceFetcher.TAG, "Request finished: " + request.uri);
                    RemoteResourceFetcher.this.mActiveRequestsMap.remove(request);
                }
                return request;
            }
        };
    }

    public static InputStream getUngzippedContent(HttpEntity httpEntity) throws IOException {
        Header contentEncoding;
        String value;
        InputStream content = httpEntity.getContent();
        return (content == null || (contentEncoding = httpEntity.getContentEncoding()) == null || (value = contentEncoding.getValue()) == null || !value.contains("gzip")) ? content : new GZIPInputStream(content);
    }

    private Callable<Request> newRequestCall(Request request) {
        Matcher matcher = Pattern.compile(SmsApplication.getInstance().getApplicationContext().getString(R.string.shorten_url_img_reg), 32).matcher(request.uri.toString());
        return (matcher == null || !matcher.find()) ? normalResourceRequest(request) : fetchImageDetailRequest(request, matcher.group(1));
    }

    private Callable<Request> normalResourceRequest(final Request request) {
        return new Callable<Request>() {
            public Request call() {
                try {
                    Log.d(RemoteResourceFetcher.TAG, "Requesting: " + request.uri);
                    RemoteResourceFetcher.this.mResourceCache.store(request.hash, RemoteResourceFetcher.getUngzippedContent(RemoteResourceFetcher.this.realReqExcute(request.uri.toString(), true, new BasicNameValuePair[0]).getEntity()));
                    Log.d(RemoteResourceFetcher.TAG, "Request successful: " + request.uri);
                } catch (IOException e) {
                    Log.d(RemoteResourceFetcher.TAG, "IOException", e);
                } finally {
                    Log.d(RemoteResourceFetcher.TAG, "Request finished: " + request.uri);
                    RemoteResourceFetcher.this.mActiveRequestsMap.remove(request);
                    RemoteResourceFetcher.this.notifyObservers(request.uri);
                }
                return request;
            }
        };
    }

    /* access modifiers changed from: private */
    public HttpResponse realReqExcute(String str, boolean z, BasicNameValuePair... basicNameValuePairArr) throws IOException {
        if (Util.isCMWapConnected(SmsApplication.getInstance().getApplicationContext())) {
            HttpGet createHttpGet = this._cmccClient.createHttpGet(str, basicNameValuePairArr);
            if (z) {
                createHttpGet.addHeader("Accept-Encoding", "gzip");
            }
            return this._cmccClient.executeHttpRequest(createHttpGet);
        }
        CmccHttpClient cmccHttpClient = new CmccHttpClient(createHttpClient(), false);
        HttpGet createHttpGet2 = cmccHttpClient.createHttpGet(str, basicNameValuePairArr);
        if (z) {
            createHttpGet2.addHeader("Accept-Encoding", "gzip");
        }
        return cmccHttpClient.executeHttpRequest(createHttpGet2);
    }

    public Future<Request> fetch(Uri uri, String str) {
        Request request = new Request(uri, str);
        synchronized (this.mActiveRequestsMap) {
            Callable<Request> newRequestCall = newRequestCall(request);
            if (this.mActiveRequestsMap.putIfAbsent(request, newRequestCall) == null) {
                Log.d(TAG, "issuing new request for: " + uri);
                Future<Request> submit = this.mExecutor.submit(newRequestCall);
                return submit;
            }
            Log.d(TAG, "Already have a pending request for: " + uri);
            return null;
        }
    }

    public void notifyObservers(Object obj) {
        setChanged();
        super.notifyObservers(obj);
    }

    public void shutdown() {
        this.mExecutor.shutdownNow();
    }
}
