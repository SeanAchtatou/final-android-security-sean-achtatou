package vc.lx.sms.caches;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class NullDiskCache implements ICache {
    public void cleanup() {
    }

    public void clear() {
    }

    public boolean exists(String str) {
        return false;
    }

    public File getFile(String str) {
        return null;
    }

    public InputStream getInputStream(String str) throws IOException {
        return null;
    }

    public void invalidate(String str) {
    }

    public void store(String str, InputStream inputStream) {
    }
}
