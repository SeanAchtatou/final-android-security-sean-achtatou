package vc.lx.sms.caches;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Observable;
import java.util.Observer;

public class RemoteResourceManager extends Observable {
    private static final boolean DEBUG = true;
    private static final String TAG = "RemoteResourceManager";
    private ICache mDiskCache;
    private FetcherObserver mFetcherObserver;
    private RemoteResourceFetcher mRemoteResourceFetcher;

    private class FetcherObserver implements Observer {
        private FetcherObserver() {
        }

        public void update(Observable observable, Object obj) {
            RemoteResourceManager.this.setChanged();
            RemoteResourceManager.this.notifyObservers(obj);
        }
    }

    public static abstract class ResourceRequestObserver implements Observer {
        private Uri mRequestUri;

        public ResourceRequestObserver(Uri uri) {
            this.mRequestUri = uri;
        }

        public abstract void requestReceived(Observable observable, Uri uri);

        public void update(Observable observable, Object obj) {
            Log.d(RemoteResourceManager.TAG, "Recieved update: " + obj);
            Uri uri = (Uri) obj;
            if (uri == this.mRequestUri) {
                Log.d(RemoteResourceManager.TAG, "requestReceived: " + uri);
                requestReceived(observable, uri);
            }
        }
    }

    public RemoteResourceManager(String str) {
        this(new BaseDiskCache("redcloud", str));
    }

    public RemoteResourceManager(ICache iCache) {
        this.mFetcherObserver = new FetcherObserver();
        this.mDiskCache = iCache;
        this.mRemoteResourceFetcher = new RemoteResourceFetcher(this.mDiskCache);
        this.mRemoteResourceFetcher.addObserver(this.mFetcherObserver);
    }

    public void clear() {
        this.mRemoteResourceFetcher.shutdown();
        this.mDiskCache.clear();
    }

    public boolean exists(Uri uri) {
        return this.mDiskCache.exists(Uri.encode(uri.toString()));
    }

    public File getFile(Uri uri) {
        Log.d(TAG, "getInputStream(): " + uri);
        return this.mDiskCache.getFile(Uri.encode(uri.toString()));
    }

    public InputStream getInputStream(Uri uri) throws IOException {
        Log.d(TAG, "getInputStream(): " + uri);
        return this.mDiskCache.getInputStream(Uri.encode(uri.toString()));
    }

    public void invalidate(Uri uri) {
        this.mDiskCache.invalidate(Uri.encode(uri.toString()));
    }

    public void request(Uri uri) {
        Log.d(TAG, "request(): " + uri);
        this.mRemoteResourceFetcher.fetch(uri, Uri.encode(uri.toString()));
    }

    public void shutdown() {
        this.mRemoteResourceFetcher.shutdown();
        this.mDiskCache.cleanup();
    }

    public void store(String str, Bitmap bitmap) {
        if (this.mDiskCache != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            this.mDiskCache.store(str, new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }
    }
}
