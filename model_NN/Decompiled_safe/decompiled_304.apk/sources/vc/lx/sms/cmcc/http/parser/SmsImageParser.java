package vc.lx.sms.cmcc.http.parser;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.SmsImage;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class SmsImageParser extends AbstractJsonParser<SmsImage> {
    public SmsImage parse(String str) throws SmsParseException, SmsException {
        Matcher matcher;
        String group;
        SmsImage smsImage = new SmsImage();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has(SmsSqliteHelper.PLUG)) {
                smsImage.mPlug = jSONObject.getString(SmsSqliteHelper.PLUG);
            }
            if (jSONObject.has("url")) {
                smsImage.mUrl = jSONObject.getString("url");
            }
            if (jSONObject.has(SmsSqliteHelper.THUMB)) {
                smsImage.mThumb = jSONObject.getString(SmsSqliteHelper.THUMB);
            }
            if (jSONObject.has("image_links")) {
                smsImage.mImageLinks = new ArrayList<>();
                JSONArray jSONArray = jSONObject.getJSONArray("image_links");
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    SmsImage.ImageLink imageLink = new SmsImage.ImageLink();
                    if (jSONArray.getJSONObject(i).has("number")) {
                        imageLink.mNumber = jSONArray.getJSONObject(i).getString("number");
                    }
                    if (jSONArray.getJSONObject(i).has("url") && (matcher = Pattern.compile(SmsApplication.getInstance().getString(R.string.shorten_url_img_reg), 32).matcher(jSONArray.getJSONObject(i).getString("url"))) != null && matcher.find() && (group = matcher.group(1)) != null) {
                        imageLink.mPlug = group;
                    }
                    smsImage.mImageLinks.add(imageLink);
                }
            }
            return smsImage;
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }
}
