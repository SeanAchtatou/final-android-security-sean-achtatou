package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VoteList implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String hasmore = "false";
    public List<String> picMusicNotifys = new ArrayList();
    public long tm;
    public List<VoteItem> voteItems = new ArrayList();
}
