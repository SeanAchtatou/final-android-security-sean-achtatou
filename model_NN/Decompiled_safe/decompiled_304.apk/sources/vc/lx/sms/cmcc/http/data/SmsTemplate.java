package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;

public class SmsTemplate implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public int _id;
    public String mCategoryId;
    public String mContent;
    public String mId;
    public int mPage;
    public int mPriority;
    public int mVersion;

    public String toString() {
        return this.mContent;
    }
}
