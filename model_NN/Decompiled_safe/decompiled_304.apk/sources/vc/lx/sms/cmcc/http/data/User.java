package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;

public class User implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String _id;
    public String city;
    public String email;
    public String fav_songs_count;
    public String follower_count;
    public String following_count;
    public boolean isSelected;
    public Boolean is_following;
    public String name;
    public String thumb;
    public String wizard_id;
    public String wizard_name;

    public String toString() {
        return this.name;
    }
}
