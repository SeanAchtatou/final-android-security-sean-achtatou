package vc.lx.sms.cmcc.http.parser;

import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.Error;

public class ErrorParser extends AbstractJsonParser<Error> {
    private static final boolean DEBUG = true;
    private static final Logger LOG = Logger.getLogger(ErrorParser.class.getCanonicalName());

    public Error parse(String str) throws SmsParseException, SmsException {
        Error error = new Error();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("message")) {
                error.validerrorinfo = jSONObject.getString("message");
            }
            if (jSONObject.has("code")) {
                error.validerrorcode = jSONObject.getString("code");
            }
            return error;
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }
}
