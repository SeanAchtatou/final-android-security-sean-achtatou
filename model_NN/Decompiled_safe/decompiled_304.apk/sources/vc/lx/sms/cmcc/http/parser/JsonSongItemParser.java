package vc.lx.sms.cmcc.http.parser;

import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.db.SmsSqliteHelper;

public class JsonSongItemParser extends AbstractJsonParser<SongItem> {
    private static final boolean DEBUG = true;
    private static final Logger LOG = Logger.getLogger(JsonSongItemParser.class.getCanonicalName());

    public SongItem parse(String str) throws SmsParseException, SmsException {
        SongItem songItem = new SongItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("name")) {
                songItem.song = jSONObject.getString("name");
            }
            if (jSONObject.has(SmsSqliteHelper.GROUP_CODE)) {
                songItem.groupcode = jSONObject.getString(SmsSqliteHelper.GROUP_CODE);
            }
            if (jSONObject.has(SmsSqliteHelper.CONTENT_ID)) {
                songItem.contentid = jSONObject.getString(SmsSqliteHelper.CONTENT_ID);
            }
            if (jSONObject.has("singer")) {
                songItem.singer = jSONObject.getString("singer");
            }
            if (jSONObject.has(SmsSqliteHelper.PLUG)) {
                songItem.plug = jSONObject.getString(SmsSqliteHelper.PLUG);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return songItem;
    }
}
