package vc.lx.sms.cmcc.http.parser;

import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.RichTemplateRemoteVersion;

public class RichTemplateRemoteVersionParser extends AbstractJsonParser<RichTemplateRemoteVersion> {
    public RichTemplateRemoteVersion parse(String str) throws SmsParseException, SmsException {
        RichTemplateRemoteVersion richTemplateRemoteVersion = new RichTemplateRemoteVersion();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has(MMHttpDefines.TAG_RESULT)) {
                return richTemplateRemoteVersion;
            }
            richTemplateRemoteVersion.result = jSONObject.getString(MMHttpDefines.TAG_RESULT);
            return richTemplateRemoteVersion;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
