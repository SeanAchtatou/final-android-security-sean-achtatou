package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;

public class VersionInfo implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String mFile;
    public String mForce;
    public String mVersionCode;

    public String toString() {
        return this.mVersionCode;
    }
}
