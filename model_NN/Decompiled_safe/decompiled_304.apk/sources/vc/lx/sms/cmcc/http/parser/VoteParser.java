package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VoteForwardBook;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;

public class VoteParser extends AbstractJsonParser<VoteItem> {
    public VoteItem parse(String str) throws SmsParseException, SmsException {
        VoteItem voteItem = new VoteItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("title")) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("title");
                voteItem.cli_id = Long.valueOf(jSONObject2.getString("cli_id")).longValue();
                voteItem.service_id = jSONObject2.getString("ser_id");
            }
            if (jSONObject.has("url")) {
                voteItem.plug = jSONObject.getString("url");
            }
            if (jSONObject.has("vote_links")) {
                JSONArray jSONArray = jSONObject.getJSONArray("vote_links");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject3 = (JSONObject) jSONArray.get(i);
                    VoteForwardBook voteForwardBook = new VoteForwardBook();
                    voteForwardBook.number = jSONObject3.getString("number");
                    voteForwardBook.url = jSONObject3.getString("url");
                    voteItem.forwardBook.add(voteForwardBook);
                }
            }
            if (!jSONObject.has("options")) {
                return voteItem;
            }
            JSONArray jSONArray2 = jSONObject.getJSONArray("options");
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                JSONObject jSONObject4 = (JSONObject) jSONArray2.get(i2);
                VoteOption voteOption = new VoteOption();
                voteOption.cli_id = Long.valueOf(jSONObject4.getString("cli_id")).longValue();
                voteOption.service_id = jSONObject4.getString("ser_id");
                voteItem.options.add(voteOption);
            }
            return voteItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
