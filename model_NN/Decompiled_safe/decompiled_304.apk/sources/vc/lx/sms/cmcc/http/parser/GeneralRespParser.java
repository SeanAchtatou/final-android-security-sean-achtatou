package vc.lx.sms.cmcc.http.parser;

import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.GeneralResp;
import vc.lx.sms.cmcc.http.data.MiguType;

public class GeneralRespParser extends AbstractJsonParser<MiguType> {
    public GeneralResp parse(String str) throws SmsParseException, SmsException {
        try {
            JSONObject jSONObject = new JSONObject(str);
            GeneralResp generalResp = new GeneralResp();
            if (jSONObject.has(MMHttpDefines.TAG_RESULT)) {
                generalResp.result = jSONObject.getString(MMHttpDefines.TAG_RESULT);
            }
            if (jSONObject.has("code")) {
                generalResp.code = jSONObject.getString("code");
            }
            return generalResp;
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }
}
