package vc.lx.sms.cmcc.http.parser;

import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.db.SmsSqliteHelper;

public class SongItemJsonParser extends AbstractJsonParser<SongItem> {
    public SongItem parse(String str) throws SmsParseException, SmsException {
        SongItem songItem = new SongItem();
        try {
            parseJsonObject(songItem, new JSONObject(str));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return songItem;
    }

    public void parseJsonObject(SongItem songItem, JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("name")) {
            songItem.song = jSONObject.getString("name");
        }
        if (jSONObject.has("singer")) {
            songItem.singer = jSONObject.getString("singer");
        }
        if (jSONObject.has(SmsSqliteHelper.PLUG)) {
            songItem.plug = jSONObject.getString(SmsSqliteHelper.PLUG);
        }
        if (jSONObject.has("url")) {
            songItem.durl = jSONObject.getString("url");
        }
        if (jSONObject.has(SmsSqliteHelper.FORWARD_COUNT)) {
            songItem.forward_count = jSONObject.getString(SmsSqliteHelper.FORWARD_COUNT);
        }
        if (jSONObject.has(SmsSqliteHelper.LISTEN_COUNT)) {
            songItem.listen_count = jSONObject.getString(SmsSqliteHelper.LISTEN_COUNT);
        }
        if (jSONObject.has("tag")) {
            songItem.tag = jSONObject.getString("tag");
        }
    }
}
