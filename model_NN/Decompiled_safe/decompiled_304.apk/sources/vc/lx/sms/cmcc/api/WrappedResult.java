package vc.lx.sms.cmcc.api;

import org.apache.http.HttpResponse;
import vc.lx.sms.cmcc.http.data.MiguType;

public class WrappedResult {
    public static final int CMWAP_ERROR = 0;
    public static final int CMWAP_OK = 1;
    public MiguType _entity;
    public HttpResponse _resp;
    public int _status;
}
