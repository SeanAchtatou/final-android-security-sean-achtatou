package vc.lx.sms.cmcc;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import vc.lx.sms.util.LogTool;

public class XMLParser {
    private Element root;

    public XMLParser(InputStream inputStream) {
        if (inputStream != null) {
            try {
                this.root = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream).getDocumentElement();
            } catch (Exception e) {
                LogTool.e("Create Document Fail: ", e);
            }
        }
    }

    public XMLParser(byte[] bArr) {
        if (bArr != null) {
            try {
                this.root = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(bArr)).getDocumentElement();
            } catch (Exception e) {
                LogTool.e("Create Document Fail: ", e);
            }
        }
    }

    public List getListByTag(String str, Class cls) {
        Node item;
        ArrayList arrayList = new ArrayList();
        NodeList elementsByTagName = this.root.getElementsByTagName(str);
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            return null;
        }
        int length = elementsByTagName.getLength();
        int i = 0;
        while (i < length) {
            try {
                Object newInstance = cls.newInstance();
                Element element = (Element) elementsByTagName.item(i);
                Field[] declaredFields = cls.getDeclaredFields();
                for (Field field : declaredFields) {
                    if (!Modifier.isFinal(field.getModifiers()) && (item = element.getElementsByTagName(field.getName()).item(0)) != null) {
                        Node firstChild = item.getFirstChild();
                        field.set(newInstance, firstChild == null ? null : firstChild.getNodeValue());
                    }
                }
                arrayList.add(newInstance);
                i++;
            } catch (Exception e) {
                LogTool.e("getListByTag(), error: ", e);
                return null;
            }
        }
        return arrayList;
    }

    public List getListByTagAndAttribute(String str, Class cls) {
        ArrayList arrayList = new ArrayList();
        NodeList elementsByTagName = this.root.getElementsByTagName(str);
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            return null;
        }
        int length = elementsByTagName.getLength();
        int i = 0;
        while (i < length) {
            try {
                Object newInstance = cls.newInstance();
                Element element = (Element) elementsByTagName.item(i);
                if (element.getNodeType() == 1) {
                    NamedNodeMap attributes = element.getAttributes();
                    Field[] declaredFields = cls.getDeclaredFields();
                    for (Field field : declaredFields) {
                        if (!Modifier.isFinal(field.getModifiers())) {
                            Node namedItem = attributes.getNamedItem(field.getName());
                            field.set(newInstance, namedItem == null ? null : namedItem.getNodeValue());
                        }
                    }
                    arrayList.add(newInstance);
                }
                i++;
            } catch (Exception e) {
                LogTool.e("getListByTagAndAttribute() error: ", e);
                return null;
            }
        }
        return arrayList;
    }

    public List getListByTagsAndID(String str, String str2, int i, Class cls) {
        ArrayList arrayList = new ArrayList();
        NodeList elementsByTagName = ((Element) this.root.getElementsByTagName(str).item(i)).getElementsByTagName(str2);
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            return null;
        }
        int length = elementsByTagName.getLength();
        int i2 = 0;
        while (i2 < length) {
            try {
                Object newInstance = cls.newInstance();
                Element element = (Element) elementsByTagName.item(i2);
                if (element.getNodeType() == 1) {
                    NamedNodeMap attributes = element.getAttributes();
                    Field[] declaredFields = cls.getDeclaredFields();
                    for (Field field : declaredFields) {
                        if (!Modifier.isFinal(field.getModifiers())) {
                            Node namedItem = attributes.getNamedItem(field.getName());
                            field.set(newInstance, namedItem == null ? null : namedItem.getNodeValue());
                        }
                    }
                    arrayList.add(newInstance);
                }
                i2++;
            } catch (Exception e) {
                LogTool.e("getListByTagsAndID() error: ", e);
                return null;
            }
        }
        return arrayList;
    }

    public List getListForOrderInfo(String str, String str2, Class cls) {
        ArrayList arrayList = new ArrayList();
        NodeList elementsByTagName = ((Element) this.root.getElementsByTagName(str).item(0)).getElementsByTagName(str2);
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            return null;
        }
        int length = elementsByTagName.getLength();
        int i = 0;
        while (i < length) {
            try {
                Object newInstance = cls.newInstance();
                Element element = (Element) elementsByTagName.item(i);
                if (element.getNodeType() == 1) {
                    NamedNodeMap attributes = element.getAttributes();
                    Field[] declaredFields = cls.getDeclaredFields();
                    for (Field field : declaredFields) {
                        if (!Modifier.isFinal(field.getModifiers())) {
                            if (field.getName().endsWith("type")) {
                                Node namedItem = attributes.getNamedItem(field.getName());
                                field.set(newInstance, namedItem == null ? null : namedItem.getNodeValue());
                            } else {
                                field.set(newInstance, element.getFirstChild().getNodeValue());
                            }
                        }
                    }
                    arrayList.add(newInstance);
                }
                i++;
            } catch (Exception e) {
                LogTool.e("getListForOrderInfo() error: ", e);
                return null;
            }
        }
        return arrayList;
    }

    public int getNoteNumberByTagsAndID(String str, String str2, int i, Class cls) {
        NodeList elementsByTagName = ((Element) this.root.getElementsByTagName(str).item(i)).getElementsByTagName(str2);
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            return 0;
        }
        return elementsByTagName.getLength();
    }

    public Element getRoot() {
        return this.root;
    }

    public String getValueByTag(String str) {
        NodeList elementsByTagName = this.root.getElementsByTagName(str);
        if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
            return null;
        }
        Node firstChild = elementsByTagName.item(0).getFirstChild();
        if (firstChild == null) {
            return null;
        }
        return firstChild.getNodeValue();
    }
}
