package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VoteForwardBook;
import vc.lx.sms.cmcc.http.data.VoteItem;

public class VoterForwardParser extends AbstractJsonParser<VoteItem> {
    public VoteItem parse(String str) throws SmsParseException, SmsException {
        VoteItem voteItem = new VoteItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("vote_links")) {
                return voteItem;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("vote_links");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                VoteForwardBook voteForwardBook = new VoteForwardBook();
                voteForwardBook.number = jSONObject2.getString("number");
                voteForwardBook.url = jSONObject2.getString("url");
                voteItem.forwardBook.add(voteForwardBook);
            }
            return voteItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
