package vc.lx.sms.cmcc.http.parser;

import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.db.SmsSqliteHelper;

public class FetchSongParser extends AbstractJsonParser<SongItem> {
    public SongItem parse(String str) throws SmsParseException, SmsException {
        SongItem songItem = new SongItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("name")) {
                return songItem;
            }
            songItem.song = jSONObject.getString("name");
            songItem.singer = jSONObject.getString("singer");
            songItem.durl = jSONObject.getString(SmsSqliteHelper.MP3_FILE);
            songItem.plug = jSONObject.getString(SmsSqliteHelper.PLUG);
            return songItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
