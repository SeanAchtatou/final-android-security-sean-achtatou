package vc.lx.sms.cmcc.http.uploadimage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.android.mms.transaction.SmsMessageSender;
import com.android.provider.Telephony;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.SmsImage;
import vc.lx.sms.cmcc.http.parser.SmsImageParser;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.db.ImagesDbService;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.util.ImageUtils;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class UploadImageJob implements Serializable {
    private static final long serialVersionUID = 1;
    /* access modifiers changed from: private */
    public ContactList mContactList;
    private Context mContext;
    /* access modifiers changed from: private */
    public Conversation mConversation;
    /* access modifiers changed from: private */
    public ImagesDbService mDbService = new ImagesDbService(this.mContext);
    private boolean mIsForwarded;
    /* access modifiers changed from: private */
    public UploadImageJobListener mListener;
    /* access modifiers changed from: private */
    public long mRowId;
    /* access modifiers changed from: private */
    public String mSmsContent;
    /* access modifiers changed from: private */
    public SmsImage mSmsImage;
    private AbstractAsyncTask mTask;

    class ForwardImageTask extends AbstractAsyncTask {
        public ForwardImageTask(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            JSONObject access$000 = UploadImageJob.this.assembleRecipients();
            if (UploadImageJob.this.mSmsImage == null || UploadImageJob.this.mSmsImage.mPlug == null) {
                return null;
            }
            return this.mCmccHttpApi.forwardImage(this.mEnginehost, this.mContext, UploadImageJob.this.mSmsImage.mPlug, access$000.toString());
        }

        public String getTag() {
            return "ForwardImageTask";
        }

        public void onPostLogic(MiguType miguType) {
            Intent intent = new Intent();
            intent.setAction(PrefsUtil.INTENT_IMAGE_FORWARD);
            if (miguType == null || !(miguType instanceof SmsImage)) {
                intent.putExtra(PrefsUtil.KEY_UPLOAD_STATUS, 1);
                this.mContext.sendBroadcast(intent);
                if (UploadImageJob.this.mListener != null) {
                    UploadImageJob.this.mListener.onImageUploadFailed();
                    return;
                }
                return;
            }
            SmsImage smsImage = (SmsImage) miguType;
            String[] numbers = UploadImageJob.this.mContactList.getNumbers();
            ArrayList arrayList = new ArrayList();
            if (smsImage.mImageLinks != null) {
                int size = smsImage.mImageLinks.size();
                for (int i = 0; i < size; i++) {
                    SmsImage.ImageLink imageLink = smsImage.mImageLinks.get(i);
                    SmsImage smsImage2 = new SmsImage();
                    smsImage2.mPlug = imageLink.mPlug;
                    smsImage2.mThumb = UploadImageJob.this.mSmsImage.mThumb;
                    smsImage2.mUrl = UploadImageJob.this.mSmsImage.mUrl;
                    String nameByNumber = UploadImageJob.this.mContactList.getNameByNumber(numbers[i]);
                    UploadImageJob.this.mDbService.insertSmsImage(smsImage2, false);
                    arrayList.add(TextUtils.join(" ", new String[]{UploadImageJob.this.mSmsContent, String.format(this.mContext.getString(R.string.image_send_notify), nameByNumber), Util.getFullShortenImageUrl(this.mContext, imageLink.mPlug)}));
                }
            }
            intent.putExtra(PrefsUtil.KEY_UPLOAD_STATUS, 2);
            this.mContext.sendBroadcast(intent);
            long ensureThreadId = UploadImageJob.this.mConversation.ensureThreadId();
            SmsMessageSender smsMessageSender = new SmsMessageSender(this.mContext, numbers, ensureThreadId);
            try {
                if (arrayList.size() == 0) {
                    String join = TextUtils.join(" ", new String[]{UploadImageJob.this.mSmsContent, Util.getFullShortenImageUrl(this.mContext, smsImage.mPlug)});
                    for (int i2 = 0; i2 < numbers.length; i2++) {
                        arrayList.add(join);
                    }
                }
                smsMessageSender.sendMessage(ensureThreadId, arrayList);
            } catch (Exception e) {
                Log.e(Telephony.ThreadsColumns.ERROR, "Failed to send SMS message, threadId=" + ensureThreadId, e);
            }
            if (UploadImageJob.this.mListener != null) {
                UploadImageJob.this.mListener.onImageUploadFinish();
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            return UploadImageJob.this.parseSmsImage(httpResponse);
        }
    }

    public interface UploadImageJobListener {
        void onImageUploadCancel();

        void onImageUploadFailed();

        void onImageUploadFinish();
    }

    class UploadImageTask extends AbstractAsyncTask {
        private ImagesDbService.ImageDump mDump;

        public UploadImageTask(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            this.mDump = UploadImageJob.this.mDbService.queryImageDumpByRowId(UploadImageJob.this.mRowId);
            JSONObject access$000 = UploadImageJob.this.assembleRecipients();
            if (this.mDump == null || this.mDump.bitmap == null) {
                return null;
            }
            return this.mCmccHttpApi.uploadImage(this.mEnginehost, this.mContext, null, this.mDump.bitmap, access$000.toString());
        }

        public String getTag() {
            return "UploadImageTask";
        }

        public void onPostLogic(MiguType miguType) {
            Intent intent = new Intent();
            intent.setAction(PrefsUtil.INTENT_IMAGE_UPLOAD);
            intent.putExtra(PrefsUtil.KEY_ROW_ID, UploadImageJob.this.mRowId);
            if (miguType == null || !(miguType instanceof SmsImage)) {
                UploadImageJob.this.mDbService.imageUploadFaile(UploadImageJob.this.mRowId);
                intent.putExtra(PrefsUtil.KEY_UPLOAD_STATUS, 1);
                this.mContext.sendBroadcast(intent);
                if (UploadImageJob.this.mListener != null) {
                    UploadImageJob.this.mListener.onImageUploadFailed();
                    return;
                }
                return;
            }
            SmsImage smsImage = (SmsImage) miguType;
            UploadImageJob.this.mDbService.insertSmsImage(smsImage, true);
            String[] numbers = UploadImageJob.this.mContactList.getNumbers();
            ArrayList arrayList = new ArrayList();
            if (smsImage.mImageLinks != null) {
                int size = smsImage.mImageLinks.size();
                for (int i = 0; i < size; i++) {
                    SmsImage.ImageLink imageLink = smsImage.mImageLinks.get(i);
                    SmsImage smsImage2 = new SmsImage();
                    smsImage2.mPlug = imageLink.mPlug;
                    smsImage2.mThumb = smsImage.mThumb;
                    smsImage2.mUrl = smsImage.mUrl;
                    String nameByNumber = UploadImageJob.this.mContactList.getNameByNumber(numbers[i]);
                    UploadImageJob.this.mDbService.insertSmsImage(smsImage2, false);
                    arrayList.add(TextUtils.join(" ", new String[]{this.mDump.smsContent, String.format(this.mContext.getString(R.string.image_send_notify), nameByNumber), Util.getFullShortenImageUrl(this.mContext, imageLink.mPlug)}));
                }
            }
            if (!(this.mDump == null || this.mDump.bitmap == null)) {
                try {
                    SmsApplication.getInstance().getRemoteResourceManager().store(Uri.encode(Uri.parse(smsImage.mThumb).toString()), ImageUtils.resampleImage(this.mDump.bitmap, 240));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            UploadImageJob.this.mDbService.deleteImageDumpByRowId(UploadImageJob.this.mRowId);
            intent.putExtra(PrefsUtil.KEY_UPLOAD_STATUS, 2);
            this.mContext.sendBroadcast(intent);
            long ensureThreadId = UploadImageJob.this.mConversation.ensureThreadId();
            SmsMessageSender smsMessageSender = new SmsMessageSender(this.mContext, numbers, ensureThreadId);
            try {
                if (arrayList.size() == 0) {
                    String join = TextUtils.join(" ", new String[]{this.mDump.smsContent, Util.getFullShortenImageUrl(this.mContext, smsImage.mPlug)});
                    for (int i2 = 0; i2 < numbers.length; i2++) {
                        arrayList.add(join);
                    }
                }
                smsMessageSender.sendMessage(ensureThreadId, arrayList);
            } catch (Exception e2) {
                Log.e(Telephony.ThreadsColumns.ERROR, "Failed to send SMS message, threadId=" + ensureThreadId, e2);
            }
            if (UploadImageJob.this.mListener != null) {
                UploadImageJob.this.mListener.onImageUploadFinish();
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            return UploadImageJob.this.parseSmsImage(httpResponse);
        }
    }

    public UploadImageJob(long j, Context context, ContactList contactList, Conversation conversation) {
        this.mContext = context;
        this.mRowId = j;
        this.mContactList = contactList;
        this.mConversation = conversation;
        this.mIsForwarded = false;
    }

    public UploadImageJob(Context context, ContactList contactList, Conversation conversation, SmsImage smsImage, String str) {
        this.mContext = context;
        this.mContactList = contactList;
        this.mConversation = conversation;
        this.mSmsImage = smsImage;
        this.mIsForwarded = true;
        this.mSmsContent = str;
    }

    /* access modifiers changed from: private */
    public JSONObject assembleRecipients() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        while (i < this.mContactList.getNumbers().length) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("name", ((Contact) this.mContactList.get(i)).getName());
                jSONObject2.put("number", ((Contact) this.mContactList.get(i)).getNumber());
                jSONArray.put(jSONObject2);
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        jSONObject.put("friends", jSONArray);
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public MiguType parseSmsImage(HttpResponse httpResponse) {
        try {
            return new SmsImageParser().parse(EntityUtils.toString(httpResponse.getEntity()));
        } catch (SmsParseException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e2) {
            e2.printStackTrace();
            return null;
        } catch (SmsException e3) {
            e3.printStackTrace();
            return null;
        } catch (IOException e4) {
            e4.printStackTrace();
            return null;
        }
    }

    public void cancel() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
        }
        if (this.mListener != null) {
            this.mListener.onImageUploadCancel();
        }
    }

    public void setUploadImageJobListener(UploadImageJobListener uploadImageJobListener) {
        this.mListener = uploadImageJobListener;
    }

    public void start() {
        if (!this.mIsForwarded) {
            this.mTask = new UploadImageTask(this.mContext);
        } else {
            this.mTask = new ForwardImageTask(this.mContext);
        }
        this.mTask.execute(new Void[0]);
    }
}
