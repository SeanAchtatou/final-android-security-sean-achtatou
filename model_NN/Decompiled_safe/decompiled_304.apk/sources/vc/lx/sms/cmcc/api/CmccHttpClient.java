package vc.lx.sms.cmcc.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.util.LogTool;

public class CmccHttpClient {
    private static final boolean DEBUG = true;
    private static final String TAG = "CmccHttpClient";
    private DefaultHttpClient mHttpClient;

    public CmccHttpClient(DefaultHttpClient defaultHttpClient) {
        this(defaultHttpClient, false);
    }

    public CmccHttpClient(DefaultHttpClient defaultHttpClient, boolean z) {
        this.mHttpClient = defaultHttpClient;
        if (z) {
            this.mHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(MMHttpDefines.CMCC_WAP_PROXY_HOST, MMHttpDefines.CMCC_WAP_PROXY_PORT));
        }
    }

    private List<NameValuePair> stripNulls(NameValuePair... nameValuePairArr) {
        ArrayList arrayList = new ArrayList();
        for (NameValuePair nameValuePair : nameValuePairArr) {
            if (nameValuePair.getValue() != null) {
                LogTool.i(TAG, "Param: " + nameValuePair);
                arrayList.add(nameValuePair);
            }
        }
        return arrayList;
    }

    public HttpDelete createHttpDelete(String str) {
        LogTool.i(TAG, "creating HttpDelete for: " + str);
        String format = URLEncodedUtils.format(new ArrayList(), "UTF-8");
        HttpDelete httpDelete = new HttpDelete((str.contains("?") ? str + "&" + format : str + "?" + format) + "?" + format);
        LogTool.i(TAG, "Delete: " + httpDelete);
        return httpDelete;
    }

    public HttpGet createHttpGet(String str, NameValuePair... nameValuePairArr) {
        LogTool.i(TAG, "creating HttpGet for: " + str);
        HttpGet httpGet = new HttpGet(str + "?" + URLEncodedUtils.format(stripNulls(nameValuePairArr), "UTF-8"));
        LogTool.i(TAG, "Created: " + httpGet.getURI());
        return httpGet;
    }

    public HttpPost createHttpPost(String str, NameValuePair... nameValuePairArr) {
        LogTool.i(TAG, "creating HttpPost for: " + str);
        HttpPost httpPost = new HttpPost(str);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(stripNulls(nameValuePairArr), "UTF-8"));
            LogTool.i(TAG, "Created: " + httpPost);
            return httpPost;
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Unable to encode http parameters.");
        }
    }

    public HttpResponse executeHttpRequest(HttpRequestBase httpRequestBase) throws IOException {
        LogTool.i(TAG, "executing HttpRequest for: " + httpRequestBase.getURI().toString());
        try {
            this.mHttpClient.getConnectionManager().closeExpiredConnections();
            return this.mHttpClient.execute(httpRequestBase);
        } catch (IOException e) {
            httpRequestBase.abort();
            throw e;
        }
    }
}
