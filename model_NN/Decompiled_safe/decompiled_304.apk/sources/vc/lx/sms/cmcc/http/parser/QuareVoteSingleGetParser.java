package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.db.SmsSqliteHelper;

public class QuareVoteSingleGetParser extends AbstractJsonParser<VoteItem> {
    public VoteItem parse(String str) throws SmsParseException, SmsException {
        VoteItem voteItem = new VoteItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has(SmsSqliteHelper.PLUG)) {
                voteItem.plug = jSONObject.getString(SmsSqliteHelper.PLUG);
            }
            if (jSONObject.has("counter")) {
                voteItem.counter = jSONObject.getInt("counter");
            }
            if (!jSONObject.has("options")) {
                return voteItem;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("options");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                VoteOption voteOption = new VoteOption();
                voteOption.service_id = jSONObject2.getString("id");
                voteOption.display_order = Integer.valueOf(jSONObject2.getString(MMHttpDefines.TAG_ORDER)).intValue() + 1;
                voteOption.content = jSONObject2.getString(SmsSqliteHelper.CONTENT);
                voteItem.options.add(voteOption);
            }
            return voteItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
