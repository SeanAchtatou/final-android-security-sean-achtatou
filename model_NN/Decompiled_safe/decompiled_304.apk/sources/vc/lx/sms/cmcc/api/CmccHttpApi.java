package vc.lx.sms.cmcc.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;

public class CmccHttpApi {
    private static final int TIMEOUT = 60;
    private final DefaultHttpClient _httpClient = createHttpClient();
    private final DefaultHttpClient _httpWapClient = createHttpClient();
    private CmccHttpClient mCmccClient = new CmccHttpClient(this._httpClient);
    private CmccHttpClient mCmccWapClient = new CmccHttpClient(this._httpWapClient, true);

    public static CmccHttpApi createDefaultApi() {
        return new CmccHttpApi();
    }

    public static final DefaultHttpClient createHttpClient() {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        HttpParams createHttpParams = createHttpParams();
        return new DefaultHttpClient(new ThreadSafeClientConnManager(createHttpParams, schemeRegistry), createHttpParams);
    }

    private static final HttpParams createHttpParams() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 60000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 60000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        return basicHttpParams;
    }

    public HttpResponse FetchSongItem(String str, Context context, String str2) throws IOException {
        return realReqExcute(context, str + "/s/" + str2, new BasicNameValuePair("format", "json"));
    }

    public HttpResponse FetchSongItemByPlug(String str, Context context, String str2) throws IOException {
        return realReqExcute(context, str + "/s/" + str2, new BasicNameValuePair("format", "json"));
    }

    public HttpResponse categorySearch(String str, Context context) throws IOException {
        return realReqExcute(context, str + "/categories", new BasicNameValuePair("format", "json"));
    }

    public HttpResponse commitCommentTask(Context context, String str, Context context2, String... strArr) throws IOException {
        HttpPost httpPost = new HttpPost(TextUtils.join("", new String[]{str, "/1.0/feedback.json"}));
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair(SmsSqliteHelper.CONTENT, strArr[0]));
        arrayList.add(new BasicNameValuePair("device_name", strArr[1]));
        arrayList.add(new BasicNameValuePair(MMHttpDefines.TAG_DEVICE_ID, strArr[2]));
        arrayList.add(new BasicNameValuePair("number", strArr[3]));
        arrayList.add(new BasicNameValuePair("qq", strArr[4]));
        arrayList.add(new BasicNameValuePair("weibo", strArr[5]));
        httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }

    public HttpResponse contentlist(String str, Context context, String str2) throws IOException {
        return realReqExcute(context, str + "/1.0/song.json/recommend", new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)));
    }

    public HttpResponse fatchDetailById(String str, Context context, String str2) throws IOException {
        return realReqExcute(context, str + "/1.0/vote.json/result", new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)), new BasicNameValuePair("vote_id", str2));
    }

    public HttpResponse fatchFavoriteUpdate(String str, Context context, String str2) throws IOException {
        HttpPost httpPost = new HttpPost(str + "/1.0/result.json?device_id=" + Util.getDeviceImie(context) + "&tm=" + PrefsUtil.getLastFavoriteUpdateDate(context));
        httpPost.setEntity(new StringEntity(str2));
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }

    public HttpResponse fatchMusicList(String str, Context context, String str2) throws IOException {
        return realReqExcute(context, str + "/1.1/song.json/", new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)), new BasicNameValuePair("page_no", str2), new BasicNameValuePair("ver", String.valueOf(Util.getCurrentMusicVersion(context))));
    }

    public HttpResponse fatchMusicVersion(String str, Context context) throws IOException {
        return realReqExcute(context, str + "/1.0/list.json/msc", new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)), new BasicNameValuePair("ver", String.valueOf(Util.getCurrentMusicVersion(context))));
    }

    public HttpResponse fatchPublicVoteById(String str, Context context, long j) throws IOException {
        return realReqExcute(context, str + "/1.0/vote.json/options", new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)), new BasicNameValuePair("vote_id", String.valueOf(j)));
    }

    public HttpResponse fatchPublicVoteList(String str, Context context, int i) throws IOException {
        return realReqExcute(context, str + "/1.0/vote.json/", new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)), new BasicNameValuePair("public", ""), new BasicNameValuePair("page_no", String.valueOf(i)));
    }

    public HttpResponse fetchSmsTemplateVersion(String str, Context context, String str2, String str3) throws IOException {
        return realReqExcute(context, str + "/1.0/list.json/sms", new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)), new BasicNameValuePair("code", str2), new BasicNameValuePair("ver", str3));
    }

    public HttpResponse forwardImage(String str, Context context, String str2, String str3) throws IOException {
        HttpPost httpPost = new HttpPost(str + "/1.1/lximage.json/forward?device_id=" + Util.getDeviceImie(context) + "&plug=" + str2);
        httpPost.setEntity(new StringEntity(str3, "UTF-8"));
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }

    public HttpResponse forwardMusicAction(String str, Context context, String str2, String str3) throws IOException {
        HttpPost httpPost = new HttpPost(str + "/1.1/song.json/forward?device_id=" + Util.getDeviceImie(context) + "&plug=" + str2);
        new ArrayList().add(new BasicNameValuePair(MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)));
        httpPost.setEntity(new StringEntity(str3, "UTF-8"));
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }

    public HttpResponse getLatestVersion(String str, Context context) throws IOException {
        return realReqExcute(context, str + "/1.0/version.json", new BasicNameValuePair("token", "android"));
    }

    public HttpResponse realReqExcute(Context context, String str, BasicNameValuePair... basicNameValuePairArr) throws IOException {
        if (Util.isCMWapConnected(context)) {
            return this.mCmccWapClient.executeHttpRequest(this.mCmccWapClient.createHttpGet(str, basicNameValuePairArr));
        }
        return this.mCmccClient.executeHttpRequest(this.mCmccClient.createHttpGet(str, basicNameValuePairArr));
    }

    public HttpResponse remoteSmsTemplist(String str, Context context, int i, String str2, int i2) throws IOException {
        return realReqExcute(context, str + "/1.0/smstemplate.json", new BasicNameValuePair("code", str2), new BasicNameValuePair("page_no", String.valueOf(i)), new BasicNameValuePair("ver", String.valueOf(i2)), new BasicNameValuePair((String) MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)));
    }

    public HttpResponse songFavoriteAction(String str, Context context, String str2, boolean z) throws IOException {
        String str3 = str + "/1.0/song.json/" + str2 + "/like";
        if (!z) {
            str3 = str + "/1.0/song.json/" + str2 + "/unlike";
        }
        HttpPost httpPost = new HttpPost(str3);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair(MMHttpDefines.TAG_DEVICE_ID, Util.getDeviceImie(context)));
        httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }

    public HttpResponse songSearch(String str, Context context, String str2, String str3) throws IOException {
        return realReqExcute(context, str + "/songs/search", new BasicNameValuePair("page", str2), new BasicNameValuePair("format", "json"), new BasicNameValuePair("keyword", str3));
    }

    public HttpResponse songSearchByCategory(String str, Context context, String str2, String str3) throws IOException {
        return realReqExcute(context, str + "/categories/" + str3, new BasicNameValuePair("page", str2), new BasicNameValuePair("format", "json"));
    }

    public HttpResponse songSearchByPlug(String str, Context context, String str2) throws IOException {
        return realReqExcute(context, str + "/i/" + str2, new BasicNameValuePair("format", "json"));
    }

    public HttpResponse uploadImage(String str, Context context, String str2, Bitmap bitmap) throws IOException {
        String str3 = str + "/1.0/lximage.json?device_id=" + Util.getDeviceImie(context);
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        Bitmap decodeFile = bitmap == null ? BitmapFactory.decodeFile(str2) : bitmap;
        if (decodeFile == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        decodeFile.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        multipartEntity.addPart("image_file", new ByteArrayBody(byteArrayOutputStream.toByteArray(), "user_upload.jpg"));
        CmccHttpClient cmccHttpClient = Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient;
        HttpPost createHttpPost = cmccHttpClient.createHttpPost(str3, new BasicNameValuePair("format", "json"));
        createHttpPost.setEntity(multipartEntity);
        return cmccHttpClient.executeHttpRequest(createHttpPost);
    }

    public HttpResponse uploadImage(String str, Context context, String str2, Bitmap bitmap, String str3) throws IOException {
        String str4 = str + "/1.1/lximage.json?device_id=" + Util.getDeviceImie(context);
        MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        Bitmap decodeFile = bitmap == null ? BitmapFactory.decodeFile(str2) : bitmap;
        if (decodeFile == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        decodeFile.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        multipartEntity.addPart("image_file", new ByteArrayBody(byteArrayOutputStream.toByteArray(), "user_upload.jpg"));
        multipartEntity.addPart("da", new StringBody(str3, Charset.forName("UTF-8")));
        CmccHttpClient cmccHttpClient = Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient;
        HttpPost createHttpPost = cmccHttpClient.createHttpPost(str4, new BasicNameValuePair("format", "json"));
        createHttpPost.setEntity(multipartEntity);
        return cmccHttpClient.executeHttpRequest(createHttpPost);
    }

    public HttpResponse voteCreateAction(String str, Context context, String str2) throws IOException {
        HttpPost httpPost = new HttpPost(str + "/1.0/vote.json?device_id=" + Util.getDeviceImie(context));
        httpPost.setEntity(new StringEntity(str2, "UTF-8"));
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }

    public HttpResponse voteForwardAction(String str, Context context, String str2, String str3, String str4) throws IOException {
        HttpPost httpPost = new HttpPost(str + "/1.0/vote.json/forward?device_id=" + Util.getDeviceImie(context) + "&plug_id=" + str3 + "&vote_id=" + str4);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("plug_id", str3));
        arrayList.add(new BasicNameValuePair("vote_id", str4));
        httpPost.setEntity(new StringEntity(str2, "UTF-8"));
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }

    public HttpResponse voteSearchByPlug(String str, Context context, String str2) throws IOException {
        return realReqExcute(context, str + "/v/" + str2, new BasicNameValuePair("format", "json"));
    }

    public HttpResponse voteSendAnswerAction(String str, Context context, String str2, String str3) throws IOException {
        HttpPost httpPost = new HttpPost(str + "/1.0/vote.json/" + str2 + "/select" + "?option_id=" + str3 + "&device_id=" + Util.getDeviceImie(context));
        new ArrayList();
        return (Util.isCMWapConnected(context) ? this.mCmccWapClient : this.mCmccClient).executeHttpRequest(httpPost);
    }
}
