package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;
import java.util.ArrayList;

public class SmsImage implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public ArrayList<ImageLink> mImageLinks;
    public String mPlug;
    public String mThumb;
    public String mUrl;

    public static class ImageLink {
        public String mNumber;
        public String mPlug;
    }

    public String toString() {
        return this.mPlug;
    }
}
