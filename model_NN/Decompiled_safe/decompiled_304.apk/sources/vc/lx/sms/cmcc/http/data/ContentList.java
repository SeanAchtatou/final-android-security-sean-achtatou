package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;
import java.util.ArrayList;

public class ContentList implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String grouptopic;
    public ArrayList<SongItem> items = new ArrayList<>();
    public String page;
    public String totalcount;
    public String totalpage;
    public String ver;
}
