package vc.lx.sms.cmcc.http.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.widget.RemoteViews;
import java.io.File;
import java.io.FileNotFoundException;
import vc.lx.sms.cmcc.api.WrappedResult;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class DownloadTask extends AsyncTask<Void, Integer, WrappedResult> {
    private int NOTIFICATION_ID = R.layout.download_song_list;
    private int _retryTimes = 3;
    private Context mContext;
    private IDownloadJob mDownloadJob;
    private String mFilePath = null;
    private NotificationManager mNotificationManager = null;

    public DownloadTask(IDownloadJob iDownloadJob, Context context) {
        this.mDownloadJob = iDownloadJob;
        this.mContext = context;
    }

    private String createAndReturnFolderName(long j) {
        String str = this.mContext.getApplicationInfo().dataDir + "/downloads";
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return str;
        }
        String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        StatFs statFs = new StatFs(absolutePath);
        statFs.restat(absolutePath);
        return j < ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()) ? absolutePath + "/12530/downloads" : str;
    }

    private String getTag() {
        return TextUtils.join(" ", new String[]{"DownloadTask :" + this.mDownloadJob.getSongInfo().contentid});
    }

    private File initCacheFile(long j) throws Exception {
        String createAndReturnFolderName = createAndReturnFolderName(j);
        File file = new File(createAndReturnFolderName);
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(createAndReturnFolderName + "/" + this.mDownloadJob.getSongInfo().song + ".mp3");
        file2.deleteOnExit();
        if (file2.exists()) {
            file2.delete();
        }
        file2.createNewFile();
        if (file2.exists()) {
            return file2;
        }
        throw new FileNotFoundException("can not find " + file2.getAbsolutePath());
    }

    private void notificationDownLoad(int i) {
        this.mNotificationManager = SmsApplication.getInstance().getmNotificationManager();
        long currentTimeMillis = System.currentTimeMillis();
        String join = TextUtils.join("-", new String[]{this.mDownloadJob.getSongInfo().singer, this.mDownloadJob.getSongInfo().song});
        Notification notification = new Notification(R.drawable.icon, this.mContext.getString(R.string.downloading_notification_notice) + ":" + join, currentTimeMillis);
        PendingIntent activity = PendingIntent.getActivity(this.mContext, 0, null, 0);
        RemoteViews remoteViews = new RemoteViews(this.mContext.getPackageName(), (int) R.layout.download_notification);
        remoteViews.setProgressBar(R.id.downloadbar, this.mDownloadJob.getTotalSize(), i, false);
        notification.contentView = remoteViews;
        remoteViews.setTextViewText(R.id.info, join);
        notification.contentIntent = activity;
        this.mNotificationManager.notify(this.NOTIFICATION_ID, notification);
    }

    public void cancelNotification() {
        if (this.mNotificationManager != null) {
            this.mNotificationManager.cancel(this.NOTIFICATION_ID);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00aa A[SYNTHETIC, Splitter:B:23:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00af A[SYNTHETIC, Splitter:B:26:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00da A[SYNTHETIC, Splitter:B:46:0x00da] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00df A[SYNTHETIC, Splitter:B:49:0x00df] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f0 A[SYNTHETIC, Splitter:B:55:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f5 A[SYNTHETIC, Splitter:B:58:0x00f5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public vc.lx.sms.cmcc.api.WrappedResult doInBackground(java.lang.Void... r15) {
        /*
            r14 = this;
            r12 = 0
            r11 = 1
            vc.lx.sms.cmcc.api.WrappedResult r0 = new vc.lx.sms.cmcc.api.WrappedResult
            r0.<init>()
            android.content.Context r1 = r14.mContext
            boolean r1 = vc.lx.sms.cmcc.SystemController.checkWapStatus(r1)
            org.apache.http.impl.client.DefaultHttpClient r2 = vc.lx.sms.cmcc.api.CmccHttpApi.createHttpClient()
        L_0x0011:
            int r3 = r14._retryTimes
            if (r3 <= 0) goto L_0x00d1
            if (r1 != 0) goto L_0x001d
            android.content.Context r1 = r14.mContext
            boolean r1 = vc.lx.sms.cmcc.SystemController.checkWapStatus(r1)
        L_0x001d:
            java.lang.String r3 = r14.getTag()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "isConnected : "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = java.lang.String.valueOf(r1)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            vc.lx.sms.util.LogTool.i(r3, r4)
            r3 = 0
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            vc.lx.sms.cmcc.http.download.IDownloadJob r6 = r14.mDownloadJob     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            vc.lx.sms.cmcc.http.data.SongItem r6 = r6.getSongInfo()     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            java.lang.String r6 = r6.durl     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            r5.<init>(r6)     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            org.apache.http.HttpResponse r5 = r2.execute(r5)     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            org.apache.http.HttpEntity r5 = r5.getEntity()     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            if (r5 == 0) goto L_0x013c
            long r6 = r5.getContentLength()     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            vc.lx.sms.cmcc.http.download.IDownloadJob r8 = r14.mDownloadJob     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            int r9 = (int) r6     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            r8.setTotalSize(r9)     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            java.io.InputStream r5 = r5.getContent()     // Catch:{ IOException -> 0x012c, Exception -> 0x00d2, all -> 0x00eb }
            java.io.File r6 = r14.initCacheFile(r6)     // Catch:{ IOException -> 0x0131, Exception -> 0x0120, all -> 0x0110 }
            java.lang.String r7 = r6.getAbsolutePath()     // Catch:{ IOException -> 0x0131, Exception -> 0x0120, all -> 0x0110 }
            r14.mFilePath = r7     // Catch:{ IOException -> 0x0131, Exception -> 0x0120, all -> 0x0110 }
            java.io.RandomAccessFile r7 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0131, Exception -> 0x0120, all -> 0x0110 }
            java.lang.String r8 = "rws"
            r7.<init>(r6, r8)     // Catch:{ IOException -> 0x0131, Exception -> 0x0120, all -> 0x0110 }
            r6 = 8192(0x2000, float:1.14794E-41)
            byte[] r6 = new byte[r6]     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
        L_0x0077:
            int r8 = r5.read(r6)     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            r9 = -1
            if (r8 == r9) goto L_0x00bb
            long r9 = (long) r8     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            long r3 = r3 + r9
            r9 = 0
            r7.write(r6, r9, r8)     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            r8 = 1
            java.lang.Integer[] r8 = new java.lang.Integer[r8]     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            r9 = 0
            int r10 = (int) r3     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            r8[r9] = r10     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            r14.onProgressUpdate(r8)     // Catch:{ IOException -> 0x0093, Exception -> 0x0123, all -> 0x0114 }
            goto L_0x0077
        L_0x0093:
            r3 = move-exception
            r4 = r7
        L_0x0095:
            java.lang.String r6 = r14.getTag()     // Catch:{ all -> 0x011c }
            java.lang.String r3 = r3.getMessage()     // Catch:{ all -> 0x011c }
            vc.lx.sms.util.LogTool.i(r6, r3)     // Catch:{ all -> 0x011c }
            r3 = 0
            r0._status = r3     // Catch:{ all -> 0x011c }
            int r3 = r14._retryTimes     // Catch:{ all -> 0x011c }
            int r3 = r3 - r11
            r14._retryTimes = r3     // Catch:{ all -> 0x011c }
            if (r5 == 0) goto L_0x00ad
            r5.close()     // Catch:{ IOException -> 0x0104 }
        L_0x00ad:
            if (r4 == 0) goto L_0x00b2
            r4.close()     // Catch:{ IOException -> 0x0106 }
        L_0x00b2:
            org.apache.http.conn.ClientConnectionManager r3 = r2.getConnectionManager()
            r3.shutdown()
            goto L_0x0011
        L_0x00bb:
            r3 = r7
            r4 = r5
        L_0x00bd:
            r5 = 1
            r0._status = r5     // Catch:{ IOException -> 0x0135, Exception -> 0x0126, all -> 0x0118 }
            if (r4 == 0) goto L_0x00c5
            r4.close()     // Catch:{ IOException -> 0x0100 }
        L_0x00c5:
            if (r3 == 0) goto L_0x00ca
            r3.close()     // Catch:{ IOException -> 0x0102 }
        L_0x00ca:
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
        L_0x00d1:
            return r0
        L_0x00d2:
            r3 = move-exception
            r4 = r12
            r5 = r12
        L_0x00d5:
            r3.printStackTrace()     // Catch:{ all -> 0x011c }
            if (r5 == 0) goto L_0x00dd
            r5.close()     // Catch:{ IOException -> 0x0108 }
        L_0x00dd:
            if (r4 == 0) goto L_0x00e2
            r4.close()     // Catch:{ IOException -> 0x010a }
        L_0x00e2:
            org.apache.http.conn.ClientConnectionManager r3 = r2.getConnectionManager()
            r3.shutdown()
            goto L_0x0011
        L_0x00eb:
            r0 = move-exception
            r1 = r12
            r3 = r12
        L_0x00ee:
            if (r3 == 0) goto L_0x00f3
            r3.close()     // Catch:{ IOException -> 0x010c }
        L_0x00f3:
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ IOException -> 0x010e }
        L_0x00f8:
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
            throw r0
        L_0x0100:
            r1 = move-exception
            goto L_0x00c5
        L_0x0102:
            r1 = move-exception
            goto L_0x00ca
        L_0x0104:
            r3 = move-exception
            goto L_0x00ad
        L_0x0106:
            r3 = move-exception
            goto L_0x00b2
        L_0x0108:
            r3 = move-exception
            goto L_0x00dd
        L_0x010a:
            r3 = move-exception
            goto L_0x00e2
        L_0x010c:
            r3 = move-exception
            goto L_0x00f3
        L_0x010e:
            r1 = move-exception
            goto L_0x00f8
        L_0x0110:
            r0 = move-exception
            r1 = r12
            r3 = r5
            goto L_0x00ee
        L_0x0114:
            r0 = move-exception
            r1 = r7
            r3 = r5
            goto L_0x00ee
        L_0x0118:
            r0 = move-exception
            r1 = r3
            r3 = r4
            goto L_0x00ee
        L_0x011c:
            r0 = move-exception
            r1 = r4
            r3 = r5
            goto L_0x00ee
        L_0x0120:
            r3 = move-exception
            r4 = r12
            goto L_0x00d5
        L_0x0123:
            r3 = move-exception
            r4 = r7
            goto L_0x00d5
        L_0x0126:
            r5 = move-exception
            r13 = r5
            r5 = r4
            r4 = r3
            r3 = r13
            goto L_0x00d5
        L_0x012c:
            r3 = move-exception
            r4 = r12
            r5 = r12
            goto L_0x0095
        L_0x0131:
            r3 = move-exception
            r4 = r12
            goto L_0x0095
        L_0x0135:
            r5 = move-exception
            r13 = r5
            r5 = r4
            r4 = r3
            r3 = r13
            goto L_0x0095
        L_0x013c:
            r3 = r12
            r4 = r12
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.cmcc.http.download.DownloadTask.doInBackground(java.lang.Void[]):vc.lx.sms.cmcc.api.WrappedResult");
    }

    public void onPostExecute(WrappedResult wrappedResult) {
        this.mDownloadJob.notifyDownloadEnded();
        if (wrappedResult._status == 1) {
            new MediaScannerNotifier(this.mContext, this.mFilePath);
        }
    }

    public void onPreExecute() {
        this.mDownloadJob.notifyDownloadStarted();
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... numArr) {
        int intValue = numArr[0].intValue();
        this.mDownloadJob.setProgress(intValue);
        notificationDownLoad(intValue);
    }
}
