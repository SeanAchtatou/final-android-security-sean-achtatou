package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;

public class VoteSendAnswerParser extends AbstractJsonParser<VoteItem> {
    public VoteItem parse(String str) throws SmsParseException, SmsException {
        VoteItem voteItem = new VoteItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("members_count")) {
                voteItem.counter = Integer.valueOf(jSONObject.getString("members_count")).intValue();
            }
            if (!jSONObject.has("statics")) {
                return voteItem;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("statics");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                VoteOption voteOption = new VoteOption();
                voteOption.service_id = jSONObject2.getString("id");
                voteOption.counter = Integer.valueOf(jSONObject2.getString("votes")).intValue();
                voteItem.options.add(voteOption);
            }
            return voteItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
