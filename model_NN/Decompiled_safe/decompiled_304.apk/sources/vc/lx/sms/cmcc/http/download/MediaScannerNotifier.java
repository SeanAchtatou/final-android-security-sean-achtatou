package vc.lx.sms.cmcc.http.download;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

public class MediaScannerNotifier implements MediaScannerConnection.MediaScannerConnectionClient {
    private MediaScannerConnection mConnection;
    private String mFilePath;

    public MediaScannerNotifier(Context context, String str) {
        this.mConnection = new MediaScannerConnection(context, this);
        this.mFilePath = str;
        this.mConnection.connect();
    }

    public void onMediaScannerConnected() {
        this.mConnection.scanFile(this.mFilePath, null);
    }

    public void onScanCompleted(String str, Uri uri) {
        this.mConnection.disconnect();
    }
}
