package vc.lx.sms.cmcc.http.download;

import android.content.Context;
import java.io.Serializable;
import vc.lx.sms.cmcc.http.data.SongItem;

public class DownloadJob implements IDownloadJob, Serializable {
    private static final long serialVersionUID = 1;
    private Context mContext;
    private DownloadTask mDownloadTask;
    private int mDownloadedSize;
    private IDownloadJobListener mListener;
    private int mProgress = 0;
    private SongItem mSongInfo;
    private int mTotalSize;

    public DownloadJob(SongItem songItem, Context context) {
        this.mSongInfo = songItem;
        this.mContext = context;
    }

    public void cancel() {
        this.mDownloadTask.cancel(true);
    }

    public void cancelNotification() {
        if (this.mDownloadTask != null) {
            this.mDownloadTask.cancelNotification();
        }
    }

    public int getDownloadedSize() {
        return this.mDownloadedSize;
    }

    public int getProgress() {
        return this.mProgress;
    }

    public String getProgressAsPercentage() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.mTotalSize == 0) {
            stringBuffer.append("0");
        } else {
            stringBuffer.append((this.mProgress * 100) / this.mTotalSize);
        }
        return stringBuffer.append("%").toString();
    }

    public SongItem getSongInfo() {
        return this.mSongInfo;
    }

    public int getTotalSize() {
        return this.mTotalSize;
    }

    public void notifyDownloadEnded() {
        this.mListener.downloadEnded(this);
    }

    public void notifyDownloadStarted() {
        this.mListener.downloadStarted();
    }

    public void pause() {
    }

    public void resume() {
    }

    public void setListener(IDownloadJobListener iDownloadJobListener) {
        this.mListener = iDownloadJobListener;
    }

    public void setProgress(int i) {
        this.mProgress = i;
    }

    public void setTotalSize(int i) {
        this.mTotalSize = i;
    }

    public void start() {
        this.mDownloadTask = new DownloadTask(this, this.mContext);
        this.mDownloadTask.execute(new Void[0]);
    }

    public void stop() {
    }
}
