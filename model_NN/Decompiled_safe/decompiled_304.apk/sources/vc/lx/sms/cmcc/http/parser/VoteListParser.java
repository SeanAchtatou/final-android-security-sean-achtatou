package vc.lx.sms.cmcc.http.parser;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteList;
import vc.lx.sms.cmcc.http.data.VoteOption;

public class VoteListParser extends AbstractJsonParser<VoteList> {
    private StringBuilder splitContactName(VoteOption voteOption, String str) {
        StringBuilder sb = new StringBuilder();
        if (str.contains(",")) {
            String[] split = str.substring(1, str.length() - 1).split(",");
            if (split.length > 0) {
                for (int i = 0; i < split.length; i++) {
                    if (!"".equals(split[i])) {
                        voteOption.contactNames.add(split[i].substring(1, split[i].length() - 1));
                        sb.append(split[i].substring(1, split[i].length() - 1)).append(",");
                    }
                }
                if (!"".equals(sb.toString())) {
                    sb.deleteCharAt(sb.length() - 1);
                }
            }
        } else {
            sb.append(str);
        }
        return sb;
    }

    public VoteList parse(String str) throws SmsParseException, SmsException {
        VoteList voteList = new VoteList();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("votes")) {
                JSONArray jSONArray = jSONObject.getJSONArray("votes");
                for (int i = 0; i < jSONArray.length(); i++) {
                    VoteItem voteItem = new VoteItem();
                    parseVoteJsonObject(voteItem, (JSONObject) jSONArray.get(i));
                    voteList.voteItems.add(voteItem);
                }
            }
            if (jSONObject.has("notify")) {
                JSONArray jSONArray2 = jSONObject.getJSONArray("notify");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    voteList.picMusicNotifys.add(jSONArray2.getString(i2));
                }
            }
            if (jSONObject.has("tm")) {
                voteList.tm = Long.parseLong(jSONObject.getString("tm"));
            }
            return voteList;
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }

    public void parseOptionJsonObject(VoteOption voteOption, JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("option_id")) {
            voteOption.service_id = jSONObject.getString("option_id");
        }
        if (jSONObject.has("votes")) {
            voteOption.counter = Integer.parseInt(jSONObject.getString("votes"));
        }
        if (jSONObject.has("display_order")) {
            voteOption.display_order = Integer.parseInt(jSONObject.getString("display_order"));
        }
        if (jSONObject.has("members")) {
            JSONArray jSONArray = jSONObject.getJSONArray("members");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < jSONArray.length(); i++) {
                String trim = ((String) jSONArray.get(i)).trim();
                sb.append(trim).append(",");
                voteOption.contactNames.add(trim);
            }
            if (sb.length() > 0 && sb.toString().contains(",")) {
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            Log.i("name", sb.toString());
            voteOption.contact_name = sb.toString();
        }
    }

    public void parseVoteJsonObject(VoteItem voteItem, JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("vote_id")) {
            voteItem.service_id = jSONObject.getString("vote_id");
        }
        if (jSONObject.has("members_count")) {
            voteItem.counter = Integer.parseInt(jSONObject.getString("members_count"));
        }
        if (jSONObject.has("statics")) {
            JSONArray jSONArray = jSONObject.getJSONArray("statics");
            for (int i = 0; i < jSONArray.length(); i++) {
                VoteOption voteOption = new VoteOption();
                parseOptionJsonObject(voteOption, jSONArray.getJSONObject(i));
                voteItem.options.add(voteOption);
            }
        }
    }
}
