package vc.lx.sms.cmcc.http.uploadimage;

public interface IUploadImageJobListener {
    void uploadImageEnded(UploadImageJob uploadImageJob);
}
