package vc.lx.sms.cmcc;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class SystemController {
    public static boolean checkWapStatus(Context context) {
        NetworkInfo[] allNetworkInfo;
        String extraInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
            for (int i = 0; i < allNetworkInfo.length; i++) {
                if (allNetworkInfo[i].getState() == NetworkInfo.State.CONNECTED && (extraInfo = allNetworkInfo[i].getExtraInfo()) != null && extraInfo.equals("cmwap")) {
                    return true;
                }
            }
        }
        return false;
    }
}
