package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;

public class ForwardSong implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String number;
    public String url;

    public ForwardSong() {
    }

    public ForwardSong(String str, String str2) {
        this.number = str;
        this.url = str2;
    }
}
