package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteList;

public class QuareVoteListParser extends AbstractJsonParser<VoteList> {
    public VoteList parse(String str) throws SmsParseException, SmsException {
        VoteList voteList = new VoteList();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("votes")) {
                JSONArray jSONArray = jSONObject.getJSONArray("votes");
                for (int i = 0; i < jSONArray.length(); i++) {
                    VoteItem voteItem = new VoteItem();
                    parseVoteJsonObject(voteItem, (JSONObject) jSONArray.get(i));
                    voteList.voteItems.add(voteItem);
                }
            }
            if (jSONObject.has("hasmore")) {
                voteList.hasmore = jSONObject.getString("hasmore");
            }
            return voteList;
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }

    public void parseVoteJsonObject(VoteItem voteItem, JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("id")) {
            voteItem.service_id = jSONObject.getString("id");
        }
        if (jSONObject.has("title")) {
            voteItem.title = jSONObject.getString("title");
        }
        if (jSONObject.has("members_count")) {
            voteItem.counter = Integer.parseInt(jSONObject.getString("members_count"));
        }
    }
}
