package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.db.SmsSqliteHelper;

public class VoteSingleGetParser extends AbstractJsonParser<VoteItem> {
    public VoteItem parse(String str) throws SmsParseException, SmsException {
        VoteItem voteItem = new VoteItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("title")) {
                voteItem.title = jSONObject.getString("title");
            }
            if (jSONObject.has("id")) {
                voteItem.service_id = jSONObject.getString("id");
            }
            if (jSONObject.has("members_count")) {
                voteItem.counter = Integer.valueOf(jSONObject.getString("members_count")).intValue();
            }
            if (!jSONObject.has("options")) {
                return voteItem;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("options");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                VoteOption voteOption = new VoteOption();
                voteOption.service_id = jSONObject2.getString("id");
                voteOption.display_order = Integer.valueOf(jSONObject2.getString(MMHttpDefines.TAG_ORDER)).intValue();
                voteOption.content = jSONObject2.getString(SmsSqliteHelper.CONTENT);
                voteItem.options.add(voteOption);
            }
            return voteItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
