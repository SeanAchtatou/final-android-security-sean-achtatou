package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SongItem implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String contentid;
    public String date;
    public String durl;
    public String favoratestatus = "no";
    public List<ForwardSong> forwardSongs = new ArrayList();
    public String forward_count;
    public String groupcode;
    public String hidestatus = "no";
    public String id;
    public boolean isCheck = false;
    public boolean isFavorate = false;
    public boolean isPlayStatus = false;
    public boolean isSlideShow = false;
    public boolean isVisible = false;
    public String listen_count;
    public String master;
    public int play_state;
    public String plug;
    public String readstatus = "no";
    public String singer;
    public String song;
    public String tag;
    public String type;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SongItem songItem = (SongItem) obj;
        if (this.plug == null) {
            if (songItem.plug != null) {
                return false;
            }
        } else if (!this.plug.equals(songItem.plug)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.contentid == null ? 0 : this.contentid.hashCode()) + 31;
    }
}
