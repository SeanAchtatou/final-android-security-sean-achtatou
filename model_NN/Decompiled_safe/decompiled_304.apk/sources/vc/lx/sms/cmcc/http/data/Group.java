package vc.lx.sms.cmcc.http.data;

import java.util.ArrayList;
import java.util.Collection;
import vc.lx.sms.cmcc.http.data.MiguType;

public class Group<T extends MiguType> extends ArrayList<T> implements MiguType {
    private static final long serialVersionUID = 1;
    private String mType;
    public String page;
    public String totalCount;
    public String totalPages;
    public String version;

    public Group() {
    }

    public Group(Collection<T> collection) {
        super(collection);
    }

    public String getType() {
        return this.mType;
    }

    public void setType(String str) {
        this.mType = str;
    }
}
