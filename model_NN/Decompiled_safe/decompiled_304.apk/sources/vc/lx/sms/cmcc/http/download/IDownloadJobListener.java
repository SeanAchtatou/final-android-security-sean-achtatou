package vc.lx.sms.cmcc.http.download;

public interface IDownloadJobListener {
    void downloadEnded(IDownloadJob iDownloadJob);

    void downloadStarted();
}
