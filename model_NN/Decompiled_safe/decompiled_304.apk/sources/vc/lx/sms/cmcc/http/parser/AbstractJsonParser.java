package vc.lx.sms.cmcc.http.parser;

import org.xmlpull.v1.XmlPullParser;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;

public abstract class AbstractJsonParser<T extends MiguType> implements Parser<T> {
    public T parse(XmlPullParser xmlPullParser) throws SmsParseException, SmsException {
        return null;
    }
}
