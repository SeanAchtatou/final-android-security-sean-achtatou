package vc.lx.sms.cmcc.http.parser;

import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.VersionInfo;

public class VersionInfoParser extends AbstractJsonParser<VersionInfo> {
    public VersionInfo parse(String str) throws SmsParseException, SmsException {
        VersionInfo versionInfo = new VersionInfo();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("version_code")) {
                versionInfo.mVersionCode = jSONObject.getString("version_code");
            }
            if (jSONObject.has("file")) {
                versionInfo.mFile = jSONObject.getString("file");
            }
            if (jSONObject.has("force")) {
                versionInfo.mForce = jSONObject.getString("force");
            }
            return versionInfo;
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }
}
