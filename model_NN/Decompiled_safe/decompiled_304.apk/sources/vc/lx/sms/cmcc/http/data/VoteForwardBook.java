package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;

public class VoteForwardBook implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String number;
    public String url;

    public VoteForwardBook() {
    }

    public VoteForwardBook(String str, String str2) {
        this.number = str;
        this.url = str2;
    }
}
