package vc.lx.sms.cmcc.http.parser;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.ContentList;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ContentListParser extends AbstractJsonParser<ContentList> {
    public ContentList parse(String str) throws SmsParseException, SmsException {
        ContentList contentList = new ContentList();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("pages")) {
                contentList.totalpage = jSONObject.getString("pages");
            }
            if (jSONObject.has("page")) {
                contentList.page = jSONObject.getString("page");
            }
            if (jSONObject.has("count")) {
                contentList.totalcount = jSONObject.getString("count");
            }
            if (jSONObject.has("ver")) {
                contentList.ver = jSONObject.getString("ver");
            }
            if (jSONObject.has("songs")) {
                JSONArray jSONArray = jSONObject.getJSONArray("songs");
                ArrayList<SongItem> arrayList = new ArrayList<>();
                SongItemJsonParser songItemJsonParser = new SongItemJsonParser();
                for (int i = 0; i < jSONArray.length(); i++) {
                    SongItem songItem = new SongItem();
                    songItemJsonParser.parseJsonObject(songItem, (JSONObject) jSONArray.get(i));
                    songItem.type = SmsApplication.getInstance().getString(R.string.system_introduce);
                    arrayList.add(songItem);
                }
                contentList.items = arrayList;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contentList;
    }
}
