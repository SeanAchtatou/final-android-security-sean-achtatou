package vc.lx.sms.cmcc.http.parser;

import com.android.provider.Telephony;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;

public abstract class AbstractParser<T extends MiguType> implements Parser<T> {
    private static final boolean DEBUG = false;
    private static final Logger LOG = Logger.getLogger(AbstractParser.class.getCanonicalName());
    private static XmlPullParserFactory sFactory;

    static {
        try {
            sFactory = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            throw new IllegalStateException("Could not create a factory");
        }
    }

    public static final XmlPullParser createXmlPullParser(InputStream inputStream) {
        try {
            XmlPullParser newPullParser = sFactory.newPullParser();
            newPullParser.setInput(inputStream, null);
            return newPullParser;
        } catch (XmlPullParserException e) {
            throw new IllegalArgumentException();
        } catch (IOException e2) {
            throw new IllegalArgumentException();
        }
    }

    public static void skipSubTree(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, null, null);
        int i = 1;
        while (i > 0) {
            int next = xmlPullParser.next();
            if (next == 3) {
                i--;
            } else if (next == 2) {
                i++;
            }
        }
    }

    public final T parse(String str) throws SmsParseException, SmsException {
        return null;
    }

    public final T parse(XmlPullParser xmlPullParser) throws SmsParseException, SmsException {
        try {
            if (xmlPullParser.getEventType() == 0) {
                xmlPullParser.nextTag();
                if (xmlPullParser.getName().equals(Telephony.ThreadsColumns.ERROR)) {
                    return new ErrorParser().parse(xmlPullParser);
                }
            }
            return parseInner(xmlPullParser);
        } catch (IOException e) {
            throw new SmsParseException(e.getMessage());
        } catch (XmlPullParserException e2) {
            throw new SmsParseException(e2.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public abstract T parseInner(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException, SmsException, SmsParseException;
}
