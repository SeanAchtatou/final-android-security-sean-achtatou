package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;

public class Error implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String validerrorcode;
    public String validerrorinfo;
}
