package vc.lx.sms.cmcc.http.parser;

import org.xmlpull.v1.XmlPullParser;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;

public interface Parser<T extends MiguType> {
    T parse(String str) throws SmsParseException, SmsException;

    T parse(XmlPullParser xmlPullParser) throws SmsParseException, SmsException;
}
