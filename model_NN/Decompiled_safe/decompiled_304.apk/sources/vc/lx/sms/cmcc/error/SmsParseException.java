package vc.lx.sms.cmcc.error;

import org.xmlpull.v1.XmlPullParser;

public class SmsParseException extends SmsException {
    private static final long serialVersionUID = 1;
    public XmlPullParser parser;

    public SmsParseException(String str) {
        super(str);
    }

    public SmsParseException(XmlPullParser xmlPullParser) {
        super("MiguParseError");
        this.parser = xmlPullParser;
    }
}
