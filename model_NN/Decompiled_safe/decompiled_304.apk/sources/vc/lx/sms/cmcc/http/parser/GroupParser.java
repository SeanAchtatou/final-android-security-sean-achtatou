package vc.lx.sms.cmcc.http.parser;

import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.Group;
import vc.lx.sms.cmcc.http.data.MiguType;

public class GroupParser extends AbstractParser<Group> {
    private Parser<? extends MiguType> mSubParser;

    public GroupParser(Parser<? extends MiguType> parser) {
        this.mSubParser = parser;
    }

    public Group<MiguType> parseInner(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException, SmsException, SmsParseException {
        Group<MiguType> group = new Group<>();
        group.setType(xmlPullParser.getAttributeValue(null, "type"));
        while (xmlPullParser.nextTag() == 2) {
            group.add(this.mSubParser.parse(xmlPullParser));
        }
        return group;
    }
}
