package vc.lx.sms.cmcc.http.download;

import vc.lx.sms.cmcc.http.data.SongItem;

public interface IDownloadJob {
    void cancel();

    void cancelNotification();

    int getDownloadedSize();

    int getProgress();

    SongItem getSongInfo();

    int getTotalSize();

    void notifyDownloadEnded();

    void notifyDownloadStarted();

    void pause();

    void resume();

    void setListener(IDownloadJobListener iDownloadJobListener);

    void setProgress(int i);

    void setTotalSize(int i);

    void start();

    void stop();
}
