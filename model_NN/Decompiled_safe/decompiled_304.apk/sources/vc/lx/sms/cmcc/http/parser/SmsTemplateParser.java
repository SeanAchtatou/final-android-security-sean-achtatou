package vc.lx.sms.cmcc.http.parser;

import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.SmsTemplate;
import vc.lx.sms.db.SmsSqliteHelper;

public class SmsTemplateParser extends AbstractJsonParser<SmsTemplate> {
    public SmsTemplate parse(String str) throws SmsParseException, SmsException {
        try {
            return parseSmsTemplate(new JSONObject(str));
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }

    public SmsTemplate parseSmsTemplate(JSONObject jSONObject) throws JSONException {
        if (jSONObject == null) {
            return null;
        }
        SmsTemplate smsTemplate = new SmsTemplate();
        if (jSONObject.has("id")) {
            smsTemplate.mId = jSONObject.getString("id");
        }
        if (jSONObject.has(SmsSqliteHelper.CONTENT)) {
            smsTemplate.mContent = jSONObject.getString(SmsSqliteHelper.CONTENT);
        }
        if (!jSONObject.has(SmsSqliteHelper.PRIORITY)) {
            return smsTemplate;
        }
        smsTemplate.mPriority = jSONObject.getInt(SmsSqliteHelper.PRIORITY);
        return smsTemplate;
    }
}
