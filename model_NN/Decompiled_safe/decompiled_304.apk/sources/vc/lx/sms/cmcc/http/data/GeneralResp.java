package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;

public class GeneralResp implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String code;
    public String result;
}
