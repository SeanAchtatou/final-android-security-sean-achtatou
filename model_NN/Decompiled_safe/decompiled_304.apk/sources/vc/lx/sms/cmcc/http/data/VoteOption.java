package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VoteOption implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public long cli_id;
    public List<String> contactNames = new ArrayList();
    public String contact_name;
    public String content;
    public int counter;
    public int display_order;
    public String service_id;

    public VoteOption() {
    }

    public VoteOption(String str, int i) {
        this.content = str;
        this.display_order = i;
    }
}
