package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.ForwardSong;
import vc.lx.sms.cmcc.http.data.SongItem;

public class ForwardSongParser extends AbstractJsonParser<SongItem> {
    public SongItem parse(String str) throws SmsParseException, SmsException {
        SongItem songItem = new SongItem();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("songs_links")) {
                return songItem;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("songs_links");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                ForwardSong forwardSong = new ForwardSong();
                forwardSong.number = jSONObject2.getString("number");
                forwardSong.url = jSONObject2.getString("url");
                songItem.forwardSongs.add(forwardSong);
            }
            return songItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
