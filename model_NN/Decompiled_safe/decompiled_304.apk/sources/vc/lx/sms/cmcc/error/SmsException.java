package vc.lx.sms.cmcc.error;

public class SmsException extends Exception {
    private static final long serialVersionUID = 1;
    private String mExtra;

    public SmsException(String str) {
        super(str);
    }

    public SmsException(String str, String str2) {
        super(str);
        this.mExtra = str2;
    }

    public String getExtra() {
        return this.mExtra;
    }
}
