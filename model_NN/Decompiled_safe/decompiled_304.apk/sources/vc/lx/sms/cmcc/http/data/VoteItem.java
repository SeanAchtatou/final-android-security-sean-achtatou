package vc.lx.sms.cmcc.http.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VoteItem implements MiguType, Serializable {
    private static final long serialVersionUID = 1;
    public String answer_flag = "0";
    public long cli_id = 0;
    public String contact_name;
    public int counter;
    public ArrayList<VoteForwardBook> forwardBook = new ArrayList<>();
    public int isNew = 0;
    public String jsonString;
    public String multi_select;
    public ArrayList<VoteOption> options = new ArrayList<>();
    public int parent_id = 0;
    public String plug;
    public List<String> plugs = new ArrayList();
    public String service_id;
    public String title;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        VoteItem voteItem = (VoteItem) obj;
        if (this.service_id == null) {
            if (voteItem.service_id != null) {
                return false;
            }
        } else if (!this.service_id.equals(voteItem.service_id)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 1 * 31;
        return (this.service_id == null ? 0 : this.service_id.hashCode()) + 31;
    }
}
