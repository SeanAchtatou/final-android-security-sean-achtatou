package vc.lx.sms.cmcc.http.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.Group;
import vc.lx.sms.cmcc.http.data.SmsTemplate;

public class SmsTemplateListParser extends AbstractJsonParser<Group<SmsTemplate>> {
    public Group<SmsTemplate> parse(String str) throws SmsParseException, SmsException {
        Group<SmsTemplate> group = new Group<>();
        SmsTemplateParser smsTemplateParser = new SmsTemplateParser();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("pages")) {
                group.totalPages = jSONObject.getString("pages");
            }
            if (jSONObject.has("count")) {
                group.totalCount = jSONObject.getString("count");
            }
            if (jSONObject.has("page")) {
                group.page = jSONObject.getString("page");
            }
            if (jSONObject.has("ver")) {
                group.version = jSONObject.getString("ver");
            }
            if (jSONObject.has("smstemplates")) {
                JSONArray jSONArray = jSONObject.getJSONArray("smstemplates");
                for (int i = 0; i < jSONArray.length(); i++) {
                    SmsTemplate parseSmsTemplate = smsTemplateParser.parseSmsTemplate(jSONArray.getJSONObject(i));
                    if (group.page != null) {
                        parseSmsTemplate.mPage = Integer.valueOf(group.page).intValue();
                    }
                    if (group.version != null) {
                        parseSmsTemplate.mVersion = Integer.valueOf(group.version).intValue();
                    }
                    if (parseSmsTemplate != null) {
                        group.add(parseSmsTemplate);
                    }
                }
            }
            return group;
        } catch (JSONException e) {
            throw new SmsParseException(e.toString());
        }
    }
}
