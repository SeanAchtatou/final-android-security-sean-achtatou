package vc.lx.sms.data;

import android.content.ContentUris;
import android.content.Context;
import android.database.ContentObserver;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import com.android.mms.util.ContactInfoCache;
import com.android.provider.Telephony;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.TaskStack;
import vc.lx.sms2.R;

public class Contact {
    private static final String TAG = "Contact";
    private static final boolean V = false;
    private static final ContentObserver sContactsObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean z) {
            if (Log.isLoggable(LogTag.APP, 2)) {
                Contact.log("contact changed, invalidate cache");
            }
            Contact.invalidateCache();
        }
    };
    private static final ContentObserver sPresenceObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean z) {
            if (Log.isLoggable(LogTag.APP, 2)) {
                Contact.log("presence changed, invalidate cache");
            }
            Contact.invalidateCache();
        }
    };
    private static final TaskStack sTaskStack = new TaskStack();
    private BitmapDrawable mAvatar;
    /* access modifiers changed from: private */
    public boolean mIsStale;
    private String mLabel;
    private final HashSet<UpdateListener> mListeners;
    public String mName;
    private String mNameAndNumber;
    /* access modifiers changed from: private */
    public String mNumber;
    private boolean mNumberIsModified;
    private long mPersonId;
    private int mPresenceResId;
    private String mPresenceText;
    private long mRecipientId;

    public static class Cache {
        private static Cache sInstance;
        private final List<Contact> mCache = new ArrayList();
        private final Context mContext;

        private Cache(Context context) {
            this.mContext = context;
        }

        static void dump() {
            synchronized (sInstance) {
                Log.d(Contact.TAG, "**** Contact cache dump ****");
                for (Contact contact : sInstance.mCache) {
                    Log.d(Contact.TAG, contact.toString());
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
            return r0;
         */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x001c  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static vc.lx.sms.data.Contact get(java.lang.String r4) {
            /*
                boolean r0 = com.android.provider.Telephony.Mms.isEmailAddress(r4)
                if (r0 == 0) goto L_0x000b
                vc.lx.sms.data.Contact r0 = getEmail(r4)
            L_0x000a:
                return r0
            L_0x000b:
                vc.lx.sms.data.Contact$Cache r1 = vc.lx.sms.data.Contact.Cache.sInstance
                monitor-enter(r1)
                vc.lx.sms.data.Contact$Cache r0 = vc.lx.sms.data.Contact.Cache.sInstance     // Catch:{ all -> 0x0038 }
                java.util.List<vc.lx.sms.data.Contact> r0 = r0.mCache     // Catch:{ all -> 0x0038 }
                java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x0038 }
            L_0x0016:
                boolean r0 = r2.hasNext()     // Catch:{ all -> 0x0038 }
                if (r0 == 0) goto L_0x003b
                java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x0038 }
                vc.lx.sms.data.Contact r0 = (vc.lx.sms.data.Contact) r0     // Catch:{ all -> 0x0038 }
                java.lang.String r3 = r0.mNumber     // Catch:{ all -> 0x0038 }
                boolean r3 = r4.equals(r3)     // Catch:{ all -> 0x0038 }
                if (r3 != 0) goto L_0x0036
                java.lang.String r3 = r0.mNumber     // Catch:{ all -> 0x0038 }
                boolean r3 = android.telephony.PhoneNumberUtils.compare(r4, r3)     // Catch:{ all -> 0x0038 }
                if (r3 == 0) goto L_0x0016
            L_0x0036:
                monitor-exit(r1)     // Catch:{ all -> 0x0038 }
                goto L_0x000a
            L_0x0038:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0038 }
                throw r0
            L_0x003b:
                r0 = 0
                monitor-exit(r1)     // Catch:{ all -> 0x0038 }
                goto L_0x000a
            */
            throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.data.Contact.Cache.get(java.lang.String):vc.lx.sms.data.Contact");
        }

        static List<Contact> getContacts() {
            ArrayList arrayList;
            synchronized (sInstance) {
                arrayList = new ArrayList(sInstance.mCache);
            }
            return arrayList;
        }

        static Context getContext() {
            return sInstance.mContext;
        }

        private static Contact getEmail(String str) {
            Contact contact;
            synchronized (sInstance) {
                Iterator<Contact> it = sInstance.mCache.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        contact = null;
                        break;
                    }
                    contact = it.next();
                    if (str.equalsIgnoreCase(contact.mNumber)) {
                        break;
                    }
                }
            }
            return contact;
        }

        static Cache getInstance() {
            return sInstance;
        }

        static String[] getNumbers() {
            String[] strArr;
            synchronized (sInstance) {
                strArr = new String[sInstance.mCache.size()];
                int i = 0;
                for (Contact number : sInstance.mCache) {
                    strArr[i] = number.getNumber();
                    i++;
                }
            }
            return strArr;
        }

        static void init(Context context) {
            sInstance = new Cache(context);
        }

        static void invalidate() {
            synchronized (sInstance) {
                for (Contact access$302 : sInstance.mCache) {
                    boolean unused = access$302.mIsStale = true;
                }
            }
        }

        static void put(Contact contact) {
            synchronized (sInstance) {
                if (get(contact.mNumber) != null) {
                    throw new IllegalStateException("cache already contains " + contact);
                }
                sInstance.mCache.add(contact);
            }
        }
    }

    public interface UpdateListener {
        void onUpdate(Contact contact);
    }

    public Contact(String str) {
        this.mListeners = new HashSet<>();
        this.mName = "";
        setNumber(str);
        this.mNumberIsModified = false;
        this.mLabel = "";
        this.mPersonId = 0;
        this.mPresenceResId = 0;
        this.mIsStale = true;
    }

    public Contact(String str, String str2) {
        this.mListeners = new HashSet<>();
        this.mName = str2;
        setNumber(str);
        this.mNumberIsModified = false;
        this.mLabel = "";
        this.mPersonId = 0;
        this.mPresenceResId = 0;
        this.mIsStale = true;
    }

    private static void asyncUpdateContact(final Context context, final Contact contact, boolean z) {
        if (contact != null) {
            if (Log.isLoggable(LogTag.APP, 2)) {
                log("asyncUpdateContact for " + contact.toString() + " canBlock: " + z + " isStale: " + contact.mIsStale);
            }
            AnonymousClass3 r0 = new Runnable() {
                public void run() {
                    Contact.updateContact(context, contact);
                }
            };
            if (z) {
                r0.run();
            } else {
                sTaskStack.push(r0);
            }
        }
    }

    private static boolean contactChanged(Contact contact, ContactInfoCache.CacheEntry cacheEntry) {
        if (!emptyIfNull(contact.mName).equals(emptyIfNull(cacheEntry.name))) {
            return true;
        }
        if (!emptyIfNull(contact.mLabel).equals(emptyIfNull(cacheEntry.phoneLabel))) {
            return true;
        }
        if (contact.mPersonId != cacheEntry.person_id) {
            return true;
        }
        return contact.mPresenceResId != cacheEntry.presenceResId;
    }

    public static void dump() {
        Cache.dump();
    }

    private static String emptyIfNull(String str) {
        return str != null ? str : "";
    }

    public static String formatNameAndNumber(String str, String str2) {
        String formatNumber = !Telephony.Mms.isEmailAddress(str2) ? PhoneNumberUtils.formatNumber(str2) : str2;
        return (TextUtils.isEmpty(str) || str.equals(str2)) ? formatNumber : str + " <" + formatNumber + ">";
    }

    public static Contact get(String str, boolean z, Context context) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Contact.get called with null or empty number");
        }
        Contact contact = Cache.get(str);
        if (contact == null) {
            Contact contact2 = new Contact(str, ComposeMessageActivity.cacheNameNum.get(str));
            Cache.put(contact2);
            contact = contact2;
        }
        if (contact.mIsStale) {
            asyncUpdateContact(context, contact, z);
        }
        return contact;
    }

    private static boolean handleLocalNumber(Context context, Contact contact) {
        if (!MessageUtils.isLocalNumber(context, contact.mNumber)) {
            return false;
        }
        contact.mName = Cache.getContext().getString(R.string.me);
        contact.updateNameAndNumber();
        return true;
    }

    public static void init(Context context) {
        Cache.init(context);
        RecipientIdCache.init(context);
    }

    public static void invalidateCache() {
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("invalidateCache");
        }
        ContactInfoCache.getInstance().invalidateCache();
        Cache.invalidate();
    }

    /* access modifiers changed from: private */
    public static void log(String str) {
        Log.d(TAG, str);
    }

    private static void logWithTrace(String str, Object... objArr) {
        Thread currentThread = Thread.currentThread();
        StackTraceElement[] stackTrace = currentThread.getStackTrace();
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(currentThread.getId());
        sb.append("] ");
        sb.append(String.format(str, objArr));
        sb.append(" <- ");
        int length = stackTrace.length > 7 ? 7 : stackTrace.length;
        for (int i = 3; i < length; i++) {
            sb.append(stackTrace[i].getMethodName());
            if (i + 1 != length) {
                sb.append(" <- ");
            }
        }
        Log.d(TAG, sb.toString());
    }

    public static void put(Contact contact) {
        Cache.put(contact);
    }

    public static void startPresenceObserver() {
        Cache.getContext().getContentResolver().registerContentObserver(ContactsContract.Presence.CONTENT_URI, true, sPresenceObserver);
    }

    public static void stopPresenceObserver() {
        Cache.getContext().getContentResolver().unregisterContentObserver(sPresenceObserver);
    }

    /* access modifiers changed from: private */
    public static void updateContact(Context context, Contact contact) {
        if (contact != null && !handleLocalNumber(context, contact)) {
            ContactInfoCache.CacheEntry contactInfo = ContactInfoCache.getInstance().getContactInfo(contact.mNumber);
            synchronized (Cache.getInstance()) {
                if (contactChanged(contact, contactInfo)) {
                    if (Log.isLoggable(LogTag.APP, 2)) {
                        log("updateContact: contact changed for " + contactInfo.name);
                    }
                    contact.mName = contactInfo.name;
                    contact.updateNameAndNumber();
                    contact.mLabel = contactInfo.phoneLabel;
                    contact.mPersonId = contactInfo.person_id;
                    contact.mPresenceResId = contactInfo.presenceResId;
                    contact.mPresenceText = contactInfo.presenceText;
                    contact.mAvatar = contactInfo.mAvatar;
                    contact.mIsStale = false;
                    Iterator<UpdateListener> it = contact.mListeners.iterator();
                    while (it.hasNext()) {
                        it.next().onUpdate(contact);
                    }
                } else {
                    contact.mIsStale = false;
                }
            }
        }
    }

    private void updateNameAndNumber() {
        this.mNameAndNumber = formatNameAndNumber(this.mName, this.mNumber);
    }

    public synchronized void addListener(UpdateListener updateListener) {
        this.mListeners.add(updateListener);
    }

    public synchronized void dumpListeners() {
        Log.i(TAG, "[Contact] dumpListeners(" + this.mNumber + ") size=" + this.mListeners.size());
        Iterator<UpdateListener> it = this.mListeners.iterator();
        int i = 0;
        while (it.hasNext()) {
            int i2 = i + 1;
            Log.i(TAG, "[" + i + "]" + it.next());
            i = i2;
        }
    }

    public synchronized boolean existsInDatabase() {
        return this.mPersonId > 0;
    }

    public Drawable getAvatar(Drawable drawable) {
        return this.mAvatar != null ? this.mAvatar : drawable;
    }

    public synchronized String getLabel() {
        return this.mLabel;
    }

    public synchronized String getName() {
        return TextUtils.isEmpty(this.mName) ? this.mNumber : this.mName;
    }

    public synchronized String getNameAndNumber() {
        return this.mNameAndNumber;
    }

    public synchronized String getNumber() {
        return this.mNumber;
    }

    public long getPersonId() {
        return this.mPersonId;
    }

    public synchronized int getPresenceResId() {
        return this.mPresenceResId;
    }

    public String getPresenceText() {
        return this.mPresenceText;
    }

    public synchronized long getRecipientId() {
        return this.mRecipientId;
    }

    public synchronized Uri getUri() {
        return ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, this.mPersonId);
    }

    public synchronized boolean isEmail() {
        return Telephony.Mms.isEmailAddress(this.mNumber);
    }

    public boolean isNumberModified() {
        return this.mNumberIsModified;
    }

    public synchronized void removeListener(UpdateListener updateListener) {
        this.mListeners.remove(updateListener);
    }

    public void setIsNumberModified(boolean z) {
        this.mNumberIsModified = z;
    }

    public synchronized void setNumber(String str) {
        this.mNumber = str;
        updateNameAndNumber();
        this.mNumberIsModified = true;
    }

    public synchronized void setRecipientId(long j) {
        this.mRecipientId = j;
    }

    public synchronized String toString() {
        return String.format("{ number=%s, name=%s, nameAndNumber=%s, label=%s, person_id=%d }", this.mNumber, this.mName, this.mNameAndNumber, this.mLabel, Long.valueOf(this.mPersonId));
    }
}
