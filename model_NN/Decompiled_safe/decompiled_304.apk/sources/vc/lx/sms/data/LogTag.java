package vc.lx.sms.data;

import android.util.Log;

public class LogTag {
    public static final String APP = "Mms:app";
    public static final String TAG = "Mms";
    public static final String TRANSACTION = "Mms:transaction";

    public static void debug(String str, Object... objArr) {
        Log.d("Mms", logFormat(str, objArr));
    }

    public static void error(String str, Object... objArr) {
        Log.e("Mms", logFormat(str, objArr));
    }

    private static String logFormat(String str, Object... objArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < objArr.length) {
                if (objArr[i2] instanceof String[]) {
                    objArr[i2] = prettyArray((String[]) objArr[i2]);
                }
                i = i2 + 1;
            } else {
                return "[" + Thread.currentThread().getId() + "] " + String.format(str, objArr);
            }
        }
    }

    private static String prettyArray(String[] strArr) {
        if (strArr.length == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder("[");
        int length = strArr.length - 1;
        for (int i = 0; i < length; i++) {
            sb.append(strArr[i]);
            sb.append(", ");
        }
        sb.append(strArr[length]);
        sb.append("]");
        return sb.toString();
    }

    public static void warn(String str, Object... objArr) {
        Log.w("Mms", logFormat(str, objArr));
    }
}
