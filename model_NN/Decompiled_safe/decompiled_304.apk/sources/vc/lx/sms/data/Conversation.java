package vc.lx.sms.data;

import android.content.AsyncQueryHandler;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.android.mms.transaction.MessagingNotification;
import com.android.provider.Telephony;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import vc.lx.sms.util.DraftCache;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms2.R;

public class Conversation {
    private static final String[] ALL_THREADS_PROJECTION = {"_id", "date", Telephony.ThreadsColumns.MESSAGE_COUNT, Telephony.ThreadsColumns.RECIPIENT_IDS, "snippet", Telephony.ThreadsColumns.SNIPPET_CHARSET, "read", Telephony.ThreadsColumns.ERROR, Telephony.ThreadsColumns.HAS_ATTACHMENT};
    private static final int DATE = 1;
    private static final boolean DEBUG = false;
    private static final String[] DRAFT_THREADS_PROJECTION = {"_id", "thread_id"};
    private static final int ERROR = 7;
    private static final int HAS_ATTACHMENT = 8;
    private static final int ID = 0;
    private static final int MESSAGE_COUNT = 2;
    private static final int READ = 6;
    private static final int RECIPIENT_IDS = 3;
    private static final int SNIPPET = 4;
    private static final int SNIPPET_CS = 5;
    private static final String TAG = "Mms/conv";
    private static boolean mLoadingThreads;
    /* access modifiers changed from: private */
    public static ContentValues mReadContentValues;
    private static final Uri sAllThreadsUri = Telephony.Threads.CONTENT_URI.buildUpon().appendQueryParameter("simple", "true").build();
    private static final Uri sDraftThreadsUri = Uri.parse("content://sms/draft");
    /* access modifiers changed from: private */
    public final Context mContext;
    private long mDate;
    private boolean mHasAttachment;
    private boolean mHasError;
    /* access modifiers changed from: private */
    public boolean mHasUnreadMessages;
    private int mMessageCount;
    private ContactList mRecipients;
    private String mSnippet;
    /* access modifiers changed from: private */
    public long mThreadId;
    private int mUnreadMessagesCount = 0;

    private static class Cache {
        private static Cache sInstance = new Cache();
        private final HashSet<Conversation> mCache = new HashSet<>(10);

        private Cache() {
        }

        static void dumpCache() {
        }

        static Conversation get(long j) {
            synchronized (sInstance) {
                dumpCache();
                Iterator<Conversation> it = sInstance.mCache.iterator();
                while (it.hasNext()) {
                    Conversation next = it.next();
                    if (next.getThreadId() == j) {
                        return next;
                    }
                }
                return null;
            }
        }

        static Conversation get(ContactList contactList) {
            synchronized (sInstance) {
                Iterator<Conversation> it = sInstance.mCache.iterator();
                while (it.hasNext()) {
                    Conversation next = it.next();
                    if (next.getRecipients().equals(contactList)) {
                        return next;
                    }
                }
                return null;
            }
        }

        static Cache getInstance() {
            return sInstance;
        }

        static void keepOnly(Set<Long> set) {
            synchronized (sInstance) {
                Iterator<Conversation> it = sInstance.mCache.iterator();
                while (it.hasNext()) {
                    if (!set.contains(Long.valueOf(it.next().getThreadId()))) {
                        it.remove();
                    }
                }
            }
        }

        static void put(Conversation conversation) {
            synchronized (sInstance) {
                if (sInstance.mCache.contains(conversation)) {
                    throw new IllegalStateException("cache already contains " + conversation + " threadId: " + conversation.mThreadId);
                }
                sInstance.mCache.add(conversation);
            }
        }

        static void remove(long j) {
            Iterator<Conversation> it = sInstance.mCache.iterator();
            while (it.hasNext()) {
                Conversation next = it.next();
                if (next.getThreadId() == j) {
                    sInstance.mCache.remove(next);
                    return;
                }
            }
        }
    }

    private Conversation(Context context) {
        this.mContext = context;
        this.mRecipients = new ContactList();
        this.mThreadId = 0;
    }

    private Conversation(Context context, long j, boolean z) {
        this.mContext = context;
        if (!loadFromThreadId(j, z)) {
            this.mRecipients = new ContactList();
            this.mThreadId = 0;
        }
    }

    private Conversation(Context context, Cursor cursor, boolean z) {
        this.mContext = context;
        fillFromCursor(context, this, cursor, z);
    }

    public static void batchDelete(AsyncQueryHandler asyncQueryHandler, int i, Long[] lArr, Context context) {
        final int length = lArr.length;
        if (length != 0) {
            final Long[] lArr2 = lArr;
            final Context context2 = context;
            final AsyncQueryHandler asyncQueryHandler2 = asyncQueryHandler;
            final int i2 = i;
            asyncQueryHandler.post(new Runnable() {
                public void run() {
                    for (int i = 0; i < length - 1; i++) {
                        context2.getContentResolver().delete(ContentUris.withAppendedId(Telephony.Threads.CONTENT_URI, lArr2[i].longValue()), null, null);
                    }
                    Conversation.startDelete(asyncQueryHandler2, i2, true, lArr2[length - 1].longValue());
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void buildReadContentValues() {
        if (mReadContentValues == null) {
            mReadContentValues = new ContentValues(1);
            mReadContentValues.put("read", (Integer) 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.<init>(android.content.Context, android.database.Cursor, boolean):void
     arg types: [android.content.Context, android.database.Cursor, int]
     candidates:
      vc.lx.sms.data.Conversation.<init>(android.content.Context, long, boolean):void
      vc.lx.sms.data.Conversation.<init>(android.content.Context, android.database.Cursor, boolean):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        r6 = new java.util.HashSet();
        r0 = r7.getContentResolver().query(vc.lx.sms.data.Conversation.sAllThreadsUri, vc.lx.sms.data.Conversation.ALL_THREADS_PROJECTION, null, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        if (r0 == null) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        if (r0.moveToNext() == false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        r1 = r0.getLong(0);
        r6.add(java.lang.Long.valueOf(r1));
        r3 = vc.lx.sms.data.Conversation.Cache.getInstance();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r1 = vc.lx.sms.data.Conversation.Cache.get(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0052, code lost:
        if (r1 != null) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r1 = new vc.lx.sms.data.Conversation(r7, r0, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r2 = vc.lx.sms.data.Conversation.Cache.getInstance();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005e, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        vc.lx.sms.data.Conversation.Cache.put(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0062, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        vc.lx.sms.data.LogTag.error("Tried to add duplicate Conversation to Cache", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0071, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0072, code lost:
        if (r0 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0074, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x007b, code lost:
        monitor-enter(vc.lx.sms.data.Conversation.Cache.getInstance());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        vc.lx.sms.data.Conversation.mLoadingThreads = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0080, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0087, code lost:
        fillFromCursor(r7, r1, r0, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x008c, code lost:
        if (r0 == null) goto L_0x0091;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x008e, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0091, code lost:
        r0 = vc.lx.sms.data.Conversation.Cache.getInstance();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0095, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        vc.lx.sms.data.Conversation.mLoadingThreads = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0099, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x009a, code lost:
        vc.lx.sms.data.Conversation.Cache.keepOnly(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void cacheAllThreads(android.content.Context r7) {
        /*
            r3 = 0
            r2 = 0
            java.lang.String r0 = "Mms:app"
            r1 = 2
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x0012
            java.lang.String r0 = "[Conversation] cacheAllThreads"
            java.lang.Object[] r1 = new java.lang.Object[r2]
            vc.lx.sms.data.LogTag.debug(r0, r1)
        L_0x0012:
            vc.lx.sms.data.Conversation$Cache r0 = vc.lx.sms.data.Conversation.Cache.getInstance()
            monitor-enter(r0)
            boolean r1 = vc.lx.sms.data.Conversation.mLoadingThreads     // Catch:{ all -> 0x0081 }
            if (r1 == 0) goto L_0x001d
            monitor-exit(r0)     // Catch:{ all -> 0x0081 }
        L_0x001c:
            return
        L_0x001d:
            r1 = 1
            vc.lx.sms.data.Conversation.mLoadingThreads = r1     // Catch:{ all -> 0x0081 }
            monitor-exit(r0)     // Catch:{ all -> 0x0081 }
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            android.content.ContentResolver r0 = r7.getContentResolver()
            android.net.Uri r1 = vc.lx.sms.data.Conversation.sAllThreadsUri
            java.lang.String[] r2 = vc.lx.sms.data.Conversation.ALL_THREADS_PROJECTION
            r4 = r3
            r5 = r3
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x008c
        L_0x0036:
            boolean r1 = r0.moveToNext()     // Catch:{ all -> 0x0071 }
            if (r1 == 0) goto L_0x008c
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x0071 }
            java.lang.Long r3 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0071 }
            r6.add(r3)     // Catch:{ all -> 0x0071 }
            vc.lx.sms.data.Conversation$Cache r3 = vc.lx.sms.data.Conversation.Cache.getInstance()     // Catch:{ all -> 0x0071 }
            monitor-enter(r3)     // Catch:{ all -> 0x0071 }
            vc.lx.sms.data.Conversation r1 = vc.lx.sms.data.Conversation.Cache.get(r1)     // Catch:{ all -> 0x0084 }
            monitor-exit(r3)     // Catch:{ all -> 0x0084 }
            if (r1 != 0) goto L_0x0087
            vc.lx.sms.data.Conversation r1 = new vc.lx.sms.data.Conversation     // Catch:{ all -> 0x0071 }
            r2 = 1
            r1.<init>(r7, r0, r2)     // Catch:{ all -> 0x0071 }
            vc.lx.sms.data.Conversation$Cache r2 = vc.lx.sms.data.Conversation.Cache.getInstance()     // Catch:{ IllegalStateException -> 0x0067 }
            monitor-enter(r2)     // Catch:{ IllegalStateException -> 0x0067 }
            vc.lx.sms.data.Conversation.Cache.put(r1)     // Catch:{ all -> 0x0064 }
            monitor-exit(r2)     // Catch:{ all -> 0x0064 }
            goto L_0x0036
        L_0x0064:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0064 }
            throw r1     // Catch:{ IllegalStateException -> 0x0067 }
        L_0x0067:
            r1 = move-exception
            java.lang.String r1 = "Tried to add duplicate Conversation to Cache"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0071 }
            vc.lx.sms.data.LogTag.error(r1, r2)     // Catch:{ all -> 0x0071 }
            goto L_0x0036
        L_0x0071:
            r1 = move-exception
            if (r0 == 0) goto L_0x0077
            r0.close()
        L_0x0077:
            vc.lx.sms.data.Conversation$Cache r0 = vc.lx.sms.data.Conversation.Cache.getInstance()
            monitor-enter(r0)
            r2 = 0
            vc.lx.sms.data.Conversation.mLoadingThreads = r2     // Catch:{ all -> 0x00a2 }
            monitor-exit(r0)     // Catch:{ all -> 0x00a2 }
            throw r1
        L_0x0081:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0081 }
            throw r1
        L_0x0084:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0084 }
            throw r1     // Catch:{ all -> 0x0071 }
        L_0x0087:
            r2 = 1
            fillFromCursor(r7, r1, r0, r2)     // Catch:{ all -> 0x0071 }
            goto L_0x0036
        L_0x008c:
            if (r0 == 0) goto L_0x0091
            r0.close()
        L_0x0091:
            vc.lx.sms.data.Conversation$Cache r0 = vc.lx.sms.data.Conversation.Cache.getInstance()
            monitor-enter(r0)
            r1 = 0
            vc.lx.sms.data.Conversation.mLoadingThreads = r1     // Catch:{ all -> 0x009f }
            monitor-exit(r0)     // Catch:{ all -> 0x009f }
            vc.lx.sms.data.Conversation.Cache.keepOnly(r6)
            goto L_0x001c
        L_0x009f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x009f }
            throw r1
        L_0x00a2:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00a2 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.data.Conversation.cacheAllThreads(android.content.Context):void");
    }

    public static void cleanup(Context context) {
        context.getContentResolver().delete(Telephony.Threads.OBSOLETE_THREADS_URI, null, null);
    }

    public static Conversation createNew(Context context) {
        return new Conversation(context);
    }

    private static void fillFromCursor(Context context, Conversation conversation, Cursor cursor, boolean z) {
        synchronized (conversation) {
            conversation.mThreadId = cursor.getLong(0);
            conversation.mDate = cursor.getLong(1);
            conversation.mMessageCount = cursor.getInt(2);
            String extractEncStrFromCursor = MessageUtils.extractEncStrFromCursor(cursor, 4, 5);
            if (TextUtils.isEmpty(extractEncStrFromCursor)) {
                extractEncStrFromCursor = context.getString(R.string.no_subject_view);
            }
            conversation.mSnippet = extractEncStrFromCursor;
            conversation.mHasUnreadMessages = cursor.getInt(6) == 0;
            conversation.mUnreadMessagesCount++;
            conversation.mHasError = cursor.getInt(7) != 0;
            conversation.mHasAttachment = cursor.getInt(8) != 0;
        }
        ContactList byIds = ContactList.getByIds(cursor.getString(3), z, context);
        synchronized (conversation) {
            conversation.mRecipients = byIds;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.<init>(android.content.Context, android.database.Cursor, boolean):void
     arg types: [android.content.Context, android.database.Cursor, int]
     candidates:
      vc.lx.sms.data.Conversation.<init>(android.content.Context, long, boolean):void
      vc.lx.sms.data.Conversation.<init>(android.content.Context, android.database.Cursor, boolean):void */
    public static Conversation from(Context context, Cursor cursor) {
        return new Conversation(context, cursor, false);
    }

    public static Conversation get(Context context, long j, boolean z) {
        Conversation conversation = Cache.get(j);
        if (conversation == null) {
            conversation = new Conversation(context, j, z);
            try {
                Cache.put(conversation);
            } catch (IllegalStateException e) {
                LogTag.error("Tried to add duplicate Conversation to Cache", new Object[0]);
            }
        }
        return conversation;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.lang.String, boolean, int, android.content.Context]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    public static Conversation get(Context context, Uri uri, boolean z) {
        if (uri == null) {
            return createNew(context);
        }
        if (uri.getPathSegments().size() >= 2) {
            try {
                return get(context, Long.parseLong(uri.getPathSegments().get(1)), z);
            } catch (NumberFormatException e) {
                LogTag.error("Invalid URI: " + uri, new Object[0]);
            }
        }
        return get(context, ContactList.getByNumbers(uri.getSchemeSpecificPart(), z, true, context), z);
    }

    public static Conversation get(Context context, ContactList contactList, boolean z) {
        if (contactList.size() < 1) {
            return createNew(context);
        }
        synchronized (Cache.getInstance()) {
            Conversation conversation = Cache.get(contactList);
            if (conversation != null) {
                return conversation;
            }
            Conversation conversation2 = new Conversation(context, getOrCreateThreadId(context, contactList), z);
            try {
                Cache.put(conversation2);
            } catch (IllegalStateException e) {
                LogTag.error("Tried to add duplicate Conversation to Cache", new Object[0]);
            }
            return conversation2;
        }
    }

    private static long getOrCreateThreadId(Context context, ContactList contactList) {
        HashSet hashSet = new HashSet();
        Iterator it = contactList.iterator();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            Contact contact2 = Contact.get(contact.getNumber(), false, context);
            if (contact2 != null) {
                hashSet.add(contact2.getNumber());
            } else {
                hashSet.add(contact.getNumber());
            }
        }
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("getOrCreateThreadId %s", hashSet);
        }
        return Telephony.Threads.getOrCreateThreadId(context, hashSet);
    }

    public static Uri getUri(long j) {
        return ContentUris.withAppendedId(Telephony.Threads.CONTENT_URI, j);
    }

    public static void init(final Context context) {
        new Thread(new Runnable() {
            public void run() {
                Conversation.cacheAllThreads(context);
            }
        }).start();
    }

    /* JADX INFO: finally extract failed */
    private boolean loadFromThreadId(long j, boolean z) {
        Cursor query = this.mContext.getContentResolver().query(sAllThreadsUri, ALL_THREADS_PROJECTION, "_id=" + Long.toString(j), null, null);
        try {
            if (query.moveToFirst()) {
                fillFromCursor(this.mContext, this, query, z);
                query.close();
                return true;
            }
            LogTag.error("loadFromThreadId: Can't find thread ID " + j, new Object[0]);
            query.close();
            return false;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public static boolean loadingThreads() {
        boolean z;
        synchronized (Cache.getInstance()) {
            z = mLoadingThreads;
        }
        return z;
    }

    public static void startDelete(AsyncQueryHandler asyncQueryHandler, int i, boolean z, long j) {
        asyncQueryHandler.startDelete(i, null, ContentUris.withAppendedId(Telephony.Threads.CONTENT_URI, j), z ? null : "locked=0", null);
    }

    public static void startDeleteAll(AsyncQueryHandler asyncQueryHandler, int i, boolean z) {
        asyncQueryHandler.startDelete(i, null, Telephony.Threads.CONTENT_URI, z ? null : "locked=0", null);
    }

    public static void startQueryForAll(AsyncQueryHandler asyncQueryHandler, int i) {
        asyncQueryHandler.cancelOperation(i);
        asyncQueryHandler.startQuery(i, null, sAllThreadsUri, ALL_THREADS_PROJECTION, null, null, "date DESC");
    }

    public static void startQueryForDraft(AsyncQueryHandler asyncQueryHandler, int i) {
        asyncQueryHandler.cancelOperation(i);
        asyncQueryHandler.startQuery(i, null, sDraftThreadsUri, ALL_THREADS_PROJECTION, null, null, "date DESC");
    }

    public static void startQueryHaveLockedMessages(AsyncQueryHandler asyncQueryHandler, long j, int i) {
        asyncQueryHandler.cancelOperation(i);
        Uri uri = Telephony.MmsSms.CONTENT_LOCKED_URI;
        asyncQueryHandler.startQuery(i, new Long(j), j != -1 ? ContentUris.withAppendedId(uri, j) : uri, ALL_THREADS_PROJECTION, null, null, "date DESC");
    }

    public synchronized void clearThreadId() {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("clearThreadId old threadId was: " + this.mThreadId + " now zero", new Object[0]);
        }
        Cache.remove(this.mThreadId);
        this.mThreadId = 0;
    }

    public synchronized long ensureThreadId() {
        if (this.mThreadId <= 0) {
            this.mThreadId = getOrCreateThreadId(this.mContext, this.mRecipients);
        }
        return this.mThreadId;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        r0 = false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean equals(java.lang.Object r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            vc.lx.sms.data.Conversation r3 = (vc.lx.sms.data.Conversation) r3     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
            vc.lx.sms.data.ContactList r0 = r2.mRecipients     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
            vc.lx.sms.data.ContactList r1 = r3.mRecipients     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
            boolean r0 = r0.equals(r1)     // Catch:{ ClassCastException -> 0x000d, all -> 0x0010 }
        L_0x000b:
            monitor-exit(r2)
            return r0
        L_0x000d:
            r0 = move-exception
            r0 = 0
            goto L_0x000b
        L_0x0010:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.data.Conversation.equals(java.lang.Object):boolean");
    }

    public synchronized long getDate() {
        return this.mDate;
    }

    public synchronized int getMessageCount() {
        return this.mMessageCount;
    }

    public synchronized ContactList getRecipients() {
        return this.mRecipients;
    }

    public synchronized String getSnippet() {
        return this.mSnippet;
    }

    public synchronized long getThreadId() {
        return this.mThreadId;
    }

    public synchronized Uri getUri() {
        return this.mThreadId <= 0 ? null : ContentUris.withAppendedId(Telephony.Threads.CONTENT_URI, this.mThreadId);
    }

    public synchronized boolean hasAttachment() {
        return this.mHasAttachment;
    }

    public synchronized boolean hasDraft() {
        return this.mThreadId <= 0 ? false : DraftCache.getInstance().hasDraft(this.mThreadId);
    }

    public synchronized boolean hasError() {
        return this.mHasError;
    }

    public synchronized boolean hasUnreadMessages() {
        return this.mHasUnreadMessages;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0052 A[Catch:{ Exception -> 0x0045, all -> 0x004e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean hasVoteDraft() {
        /*
            r12 = this;
            r10 = 1
            r9 = 0
            r8 = 0
            monitor-enter(r12)
            long r0 = r12.mThreadId     // Catch:{ all -> 0x0056 }
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x000f
            r0 = r8
        L_0x000d:
            monitor-exit(r12)
            return r0
        L_0x000f:
            vc.lx.sms2.SmsApplication r0 = vc.lx.sms2.SmsApplication.getInstance()     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEDRAFT_CONTENT_URI     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r2 = 0
            java.lang.String r3 = " thread_id = ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r5 = 0
            long r6 = r12.mThreadId     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r4[r5] = r6     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
            if (r1 <= 0) goto L_0x003e
            if (r0 == 0) goto L_0x003c
            r0.close()     // Catch:{ all -> 0x0056 }
        L_0x003c:
            r0 = r10
            goto L_0x000d
        L_0x003e:
            if (r0 == 0) goto L_0x0043
            r0.close()     // Catch:{ all -> 0x0056 }
        L_0x0043:
            r0 = r8
            goto L_0x000d
        L_0x0045:
            r0 = move-exception
            r0 = r9
        L_0x0047:
            if (r0 == 0) goto L_0x004c
            r0.close()     // Catch:{ all -> 0x0056 }
        L_0x004c:
            r0 = r8
            goto L_0x000d
        L_0x004e:
            r0 = move-exception
            r1 = r9
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ all -> 0x0056 }
        L_0x0055:
            throw r0     // Catch:{ all -> 0x0056 }
        L_0x0056:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x0059:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x0050
        L_0x005e:
            r1 = move-exception
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.data.Conversation.hasVoteDraft():boolean");
    }

    public synchronized int hashCode() {
        return this.mRecipients.hashCode();
    }

    public synchronized void markAsRead() {
        final Uri uri = getUri();
        new Thread(new Runnable() {
            public void run() {
                if (uri != null) {
                    Conversation.this.buildReadContentValues();
                    Conversation.this.mContext.getContentResolver().update(uri, Conversation.mReadContentValues, "read=0", null);
                    boolean unused = Conversation.this.mHasUnreadMessages = false;
                }
                MessagingNotification.updateAllNotifications(Conversation.this.mContext);
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.lang.String, int, int, android.content.Context]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    public boolean sameRecipient(Uri uri) {
        int size = this.mRecipients.size();
        if (size > 1) {
            return false;
        }
        if (uri == null) {
            return size == 0;
        }
        if (uri.getPathSegments().size() >= 2) {
            return false;
        }
        return this.mRecipients.equals(ContactList.getByNumbers(uri.getSchemeSpecificPart(), false, false, this.mContext));
    }

    public synchronized void setDraftState(boolean z) {
        if (this.mThreadId > 0) {
            if (DraftCache.getInstance() != null) {
                DraftCache.getInstance().setDraftState(this.mThreadId, z);
            }
        }
    }

    public synchronized void setRecipients(ContactList contactList) {
        this.mRecipients = contactList;
        this.mThreadId = 0;
    }

    public synchronized String toString() {
        return String.format("[%s] (tid %d)", this.mRecipients.serialize(), Long.valueOf(this.mThreadId));
    }
}
