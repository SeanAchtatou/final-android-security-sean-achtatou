package vc.lx.sms.data;

import android.content.Context;
import android.text.TextUtils;
import com.android.mms.transaction.MessageSender;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.RecipientIdCache;
import vc.lx.sms.util.MessageUtils;

public class ContactList extends ArrayList<Contact> {
    private static final long serialVersionUID = 1;

    public static ContactList getByIds(String str, boolean z, Context context) {
        ContactList contactList = new ContactList();
        for (RecipientIdCache.Entry next : RecipientIdCache.getAddresses(str)) {
            if (next != null && !TextUtils.isEmpty(next.number)) {
                Contact contact = Contact.get(next.number, z, context);
                contact.setRecipientId(next.id);
                contactList.add(contact);
            }
        }
        return contactList;
    }

    public static ContactList getByNumbers(Iterable<String> iterable, boolean z, Context context) {
        ContactList contactList = new ContactList();
        for (String next : iterable) {
            if (!TextUtils.isEmpty(next)) {
                contactList.add(Contact.get(next, z, context));
            }
        }
        return contactList;
    }

    public static ContactList getByNumbers(String str, boolean z, boolean z2, Context context) {
        ContactList contactList = new ContactList();
        for (String str2 : str.split(MessageSender.RECIPIENTS_SEPARATOR)) {
            if (!TextUtils.isEmpty(str2)) {
                Contact contact = Contact.get(str2, z, context);
                if (z2) {
                    contact.setNumber(str2);
                }
                contactList.add(contact);
            }
        }
        return contactList;
    }

    public static ContactList getByNumbers(ArrayList<String> arrayList, boolean z, boolean z2, Context context) {
        ContactList contactList = new ContactList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList.size()) {
                return contactList;
            }
            String str = arrayList.get(i2);
            if (!TextUtils.isEmpty(str)) {
                Contact contact = Contact.get(str, z, context);
                if (z2) {
                    contact.setNumber(str);
                }
                contactList.add(contact);
            }
            i = i2 + 1;
        }
    }

    public static ContactList getByNumbers(List<String> list, boolean z, boolean z2, Context context) {
        ContactList contactList = new ContactList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return contactList;
            }
            String str = list.get(i2);
            if (!TextUtils.isEmpty(str)) {
                Contact contact = Contact.get(str, z, context);
                if (z2) {
                    contact.setNumber(str);
                }
                contactList.add(contact);
            }
            i = i2 + 1;
        }
    }

    public void addListeners(Contact.UpdateListener updateListener) {
        Iterator it = iterator();
        while (it.hasNext()) {
            ((Contact) it.next()).addListener(updateListener);
        }
    }

    public boolean containsEmail() {
        Iterator it = iterator();
        while (it.hasNext()) {
            if (((Contact) it.next()).isEmail()) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        try {
            ContactList contactList = (ContactList) obj;
            if (size() != contactList.size()) {
                return false;
            }
            Iterator it = iterator();
            while (it.hasNext()) {
                if (!contactList.contains((Contact) it.next())) {
                    return false;
                }
            }
            return true;
        } catch (ClassCastException e) {
            return false;
        }
    }

    public String formatNames(String str) {
        String[] strArr = new String[size()];
        int i = 0;
        Iterator it = iterator();
        while (it.hasNext()) {
            strArr[i] = ((Contact) it.next()).getName();
            i++;
        }
        return TextUtils.join(str, strArr);
    }

    public String formatNamesAndNumbers(String str) {
        String[] strArr = new String[size()];
        int i = 0;
        Iterator it = iterator();
        while (it.hasNext()) {
            strArr[i] = ((Contact) it.next()).getNameAndNumber();
            i++;
        }
        return TextUtils.join(str, strArr);
    }

    public String getNameByNumber(String str) {
        Iterator it = iterator();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            if (str != null && str.equals(contact.getNumber())) {
                return contact.getName();
            }
        }
        return "";
    }

    public String getNameByNumberAllowNull(String str) {
        Iterator it = iterator();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            if (str != null && str.equals(contact.getNumber())) {
                return contact.mName;
            }
        }
        return "";
    }

    public String[] getNumbers() {
        return getNumbers(false);
    }

    public String[] getNumbers(boolean z) {
        ArrayList arrayList = new ArrayList();
        Iterator it = iterator();
        while (it.hasNext()) {
            String number = ((Contact) it.next()).getNumber();
            if (z) {
                number = MessageUtils.parseMmsAddress(number);
            }
            if (!TextUtils.isEmpty(number) && !arrayList.contains(number)) {
                arrayList.add(number);
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public int getPresenceResId() {
        if (size() != 1) {
            return 0;
        }
        return ((Contact) get(0)).getPresenceResId();
    }

    public void removeListeners(Contact.UpdateListener updateListener) {
        Iterator it = iterator();
        while (it.hasNext()) {
            ((Contact) it.next()).removeListener(updateListener);
        }
    }

    public String serialize() {
        return TextUtils.join(MessageSender.RECIPIENTS_SEPARATOR, getNumbers());
    }
}
