package vc.lx.sms.data;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import vc.lx.sms.util.SqliteWrapper;

public class RecipientIdCache {
    private static final String TAG = "Mms/cache";
    private static Uri sAllCanonical = Uri.parse("content://mms-sms/canonical-addresses");
    private static RecipientIdCache sInstance;
    private final Map<Long, String> mCache = new HashMap();
    private final Context mContext;

    public static class Entry {
        public long id;
        public String number;

        public Entry(long j, String str) {
            this.id = j;
            this.number = str;
        }
    }

    RecipientIdCache(Context context) {
        this.mContext = context;
    }

    public static void dump() {
        synchronized (sInstance) {
            Log.d(TAG, "*** Recipient ID cache dump ***");
            for (Long next : sInstance.mCache.keySet()) {
                Log.d(TAG, next + ": " + sInstance.mCache.get(next));
            }
        }
    }

    public static void fill() {
        Context context = sInstance.mContext;
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), sAllCanonical, null, null, null, null);
        try {
            synchronized (sInstance) {
                sInstance.mCache.clear();
                while (query.moveToNext()) {
                    long j = query.getLong(0);
                    sInstance.mCache.put(Long.valueOf(j), query.getString(1));
                }
            }
        } finally {
            query.close();
        }
    }

    public static List<Entry> getAddresses(String str) {
        ArrayList arrayList;
        String str2;
        synchronized (sInstance) {
            arrayList = new ArrayList();
            for (String parseLong : str.split(" ")) {
                try {
                    long parseLong2 = Long.parseLong(parseLong);
                    String str3 = sInstance.mCache.get(Long.valueOf(parseLong2));
                    if (str3 == null) {
                        Log.w(TAG, "RecipientId " + parseLong2 + " not in cache!");
                        dump();
                        fill();
                        str2 = sInstance.mCache.get(Long.valueOf(parseLong2));
                    } else {
                        str2 = str3;
                    }
                    if (TextUtils.isEmpty(str2)) {
                        Log.w(TAG, "RecipientId " + parseLong2 + " has empty number!");
                    } else {
                        arrayList.add(new Entry(parseLong2, str2));
                    }
                } catch (NumberFormatException e) {
                }
            }
        }
        return arrayList;
    }

    static RecipientIdCache getInstance() {
        return sInstance;
    }

    static void init(Context context) {
        sInstance = new RecipientIdCache(context);
        new Thread(new Runnable() {
            public void run() {
                RecipientIdCache.fill();
            }
        }).start();
    }

    private void updateCanonicalAddressInDb(long j, String str) {
    }

    public static void updateNumbers(long j, ContactList contactList) {
        Iterator it = contactList.iterator();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            if (contact.isNumberModified()) {
                contact.setIsNumberModified(false);
                long recipientId = contact.getRecipientId();
                if (recipientId != 0) {
                    String number = contact.getNumber();
                    String str = sInstance.mCache.get(Long.valueOf(recipientId));
                    if (Log.isLoggable(LogTag.APP, 2)) {
                        Log.d(TAG, "[RecipientIdCache] updateNumbers: comparing " + number + " with " + str);
                    }
                    if (!number.equalsIgnoreCase(str)) {
                        sInstance.mCache.put(Long.valueOf(recipientId), number);
                        sInstance.updateCanonicalAddressInDb(recipientId, number);
                    }
                }
            }
        }
    }
}
