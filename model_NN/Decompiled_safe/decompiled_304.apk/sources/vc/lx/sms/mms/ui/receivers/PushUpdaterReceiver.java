package vc.lx.sms.mms.ui.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import vc.lx.sms.service.PushUpdaterService;
import vc.lx.sms.service.UpdateFavoriteService;

public class PushUpdaterReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, PushUpdaterService.class));
        context.startService(new Intent(context, UpdateFavoriteService.class));
    }
}
