package vc.lx.sms.mms.ui.receivers;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import com.android.provider.Telephony;

public class SsmMessageReceiver extends BroadcastReceiver {
    static PowerManager.WakeLock mStartingService;
    static final Object mStartingServiceSync = new Object();

    public static void beginStartingService(Context context, Intent intent) {
        synchronized (mStartingServiceSync) {
            if (mStartingService == null) {
                mStartingService = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "StartingAlertService");
                mStartingService.setReferenceCounted(false);
            }
            mStartingService.acquire();
            context.startService(intent);
        }
    }

    public static void finishStartingService(Service service, int i) {
        synchronized (mStartingServiceSync) {
            if (mStartingService != null && service.stopSelfResult(i)) {
                mStartingService.release();
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            Log.v("SsmMessageReceiver", "SSM handleSmsReceived");
            abortBroadcast();
            intent.setClass(context, SsmMessageReceiverService.class);
            beginStartingService(context, intent);
            abortBroadcast();
        }
    }
}
