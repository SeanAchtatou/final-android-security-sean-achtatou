package vc.lx.sms.mms.ui.receivers;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;
import android.util.Log;
import com.android.mms.transaction.MessagingNotification;
import com.android.mms.ui.ClassZeroActivity;
import com.android.provider.Telephony;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.parser.SongItemJsonParser;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.intents.MessageWrapper;
import vc.lx.sms.intents.SsmIntents;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.ui.SmsPopupActivity;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Recycler;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class SsmMessageReceiverService extends Service {
    private static final int REPLACE_COLUMN_ID = 0;
    /* access modifiers changed from: private */
    public static final String[] REPLACE_PROJECTION = {"_id", "address", Telephony.TextBasedSmsColumns.PROTOCOL};
    private static final String TAG = "SsmMessageReceiverService";
    private ServiceHandler mServiceHandler;
    private Looper mServiceLooper;
    /* access modifiers changed from: private */
    public SongItem mSongItem;
    /* access modifiers changed from: private */
    public TopMusicService topMusicService;

    private final class ServiceHandler extends Handler {

        class SongListTask extends AbstractAsyncTask {
            private String _from;
            private String _keyword;
            private int _page;

            public SongListTask(Context context, String str, String str2) {
                super(context);
                this._keyword = str;
                this._from = str2;
            }

            public HttpResponse getRespFromServer() throws IOException {
                return this.mCmccHttpApi.songSearchByPlug(this.mEnginehost, this.mContext, this._keyword);
            }

            public String getTag() {
                return "SongListTask";
            }

            public void onPostLogic(MiguType miguType) {
                if (miguType != null && (miguType instanceof SongItem)) {
                    SongItem unused = SsmMessageReceiverService.this.mSongItem = (SongItem) miguType;
                    if (SsmMessageReceiverService.this.mSongItem != null) {
                        SsmMessageReceiverService.this.mSongItem.type = this._from;
                        SsmMessageReceiverService.this.topMusicService.insert(SsmMessageReceiverService.this.mSongItem);
                    }
                }
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
            }

            public MiguType parseEntity(HttpResponse httpResponse) {
                InputStream inputStream = null;
                try {
                    SongItem parse = new SongItemJsonParser().parse(EntityUtils.toString(httpResponse.getEntity()));
                    try {
                        inputStream.close();
                        return parse;
                    } catch (Exception e) {
                        return parse;
                    }
                } catch (SmsParseException e2) {
                    e2.printStackTrace();
                    try {
                        inputStream.close();
                        return null;
                    } catch (Exception e3) {
                        return null;
                    }
                } catch (SmsException e4) {
                    e4.printStackTrace();
                    try {
                        inputStream.close();
                        return null;
                    } catch (Exception e5) {
                        return null;
                    }
                } catch (IllegalStateException e6) {
                    e6.printStackTrace();
                    try {
                        inputStream.close();
                        return null;
                    } catch (Exception e7) {
                        return null;
                    }
                } catch (IOException e8) {
                    e8.printStackTrace();
                    try {
                        inputStream.close();
                        return null;
                    } catch (Exception e9) {
                        return null;
                    }
                } catch (Throwable th) {
                    try {
                        inputStream.close();
                    } catch (Exception e10) {
                    }
                    throw th;
                }
            }
        }

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        private void displayClassZeroMessage(Context context, SmsMessage smsMessage) {
            context.startActivity(new Intent(context, ClassZeroActivity.class).putExtra("pdu", smsMessage.getPdu()).setFlags(402653184));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        private ContentValues extractContentValues(SmsMessage smsMessage) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("address", smsMessage.getDisplayOriginatingAddress());
            contentValues.put("date", new Long(System.currentTimeMillis()));
            contentValues.put(Telephony.TextBasedSmsColumns.PROTOCOL, Integer.valueOf(smsMessage.getProtocolIdentifier()));
            contentValues.put("read", (Integer) 0);
            if (smsMessage.getPseudoSubject().length() > 0) {
                contentValues.put("subject", smsMessage.getPseudoSubject());
            }
            contentValues.put(Telephony.TextBasedSmsColumns.REPLY_PATH_PRESENT, Integer.valueOf(smsMessage.isReplyPathPresent() ? 1 : 0));
            contentValues.put(Telephony.TextBasedSmsColumns.SERVICE_CENTER, smsMessage.getServiceCenterAddress());
            return contentValues;
        }

        private void handleSmsReceived(Intent intent) {
            Matcher matcher;
            SmsMessage[] messagesFromIntent = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            SmsMessage smsMessage = messagesFromIntent[0];
            Log.v(SsmMessageReceiverService.TAG, "handleSmsReceived" + (smsMessage.isReplace() ? "(replace)" : "") + ", address: " + smsMessage.getOriginatingAddress() + ", body: " + smsMessage.getMessageBody());
            Context applicationContext = SsmMessageReceiverService.this.getApplicationContext();
            Log.v(SsmMessageReceiverService.TAG, "SSM handleSmsReceived ");
            Uri insertMessage = insertMessage(applicationContext, messagesFromIntent);
            if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
                Log.v(SsmMessageReceiverService.TAG, "handleSmsReceived" + (smsMessage.isReplace() ? "(replace)" : "") + " messageUri: " + insertMessage + ", address: " + smsMessage.getOriginatingAddress() + ", body: " + smsMessage.getMessageBody());
            }
            if (messagesFromIntent != null) {
                MessagingNotification.updateNewMessageIndicator(applicationContext, true);
            }
            if (messagesFromIntent != null && messagesFromIntent.length > 0 && (matcher = Pattern.compile(SsmMessageReceiverService.this.getString(R.string.shorten_url_reg), 32).matcher(smsMessage.getMessageBody())) != null && matcher.find()) {
                SongItem unused = SsmMessageReceiverService.this.mSongItem = SsmMessageReceiverService.this.topMusicService.getSongItemByPlug(matcher.group(1));
            }
            if (messagesFromIntent != null && messagesFromIntent.length > 0 && SsmIntents.isApplicationBroughtToBackground(applicationContext)) {
                long lastPopupMsgDate = PrefsUtil.getLastPopupMsgDate(SsmMessageReceiverService.this.getApplicationContext());
                long timestampMillis = messagesFromIntent[0].getTimestampMillis();
                if (timestampMillis > lastPopupMsgDate) {
                    PrefsUtil.saveLastPopupMsgDate(SsmMessageReceiverService.this.getApplicationContext(), timestampMillis);
                    Intent intent2 = new Intent(applicationContext, SmsPopupActivity.class);
                    intent2.putExtras(new MessageWrapper(messagesFromIntent[0], applicationContext).toBundle());
                    intent2.setFlags(276824064);
                    applicationContext.startActivity(intent2);
                }
            }
        }

        private Uri insertMessage(Context context, SmsMessage[] smsMessageArr) {
            SmsMessage smsMessage = smsMessageArr[0];
            if (smsMessage.getMessageClass() != SmsMessage.MessageClass.CLASS_0) {
                return smsMessage.isReplace() ? replaceMessage(context, smsMessageArr) : storeMessage(context, smsMessageArr);
            }
            displayClassZeroMessage(context, smsMessage);
            return null;
        }

        /* JADX INFO: finally extract failed */
        private Uri replaceMessage(Context context, SmsMessage[] smsMessageArr) {
            SmsMessage smsMessage = smsMessageArr[0];
            ContentValues extractContentValues = extractContentValues(smsMessage);
            extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, smsMessage.getMessageBody());
            ContentResolver contentResolver = context.getContentResolver();
            String[] strArr = {smsMessage.getOriginatingAddress(), Integer.toString(smsMessage.getProtocolIdentifier())};
            Cursor query = SqliteWrapper.query(context, contentResolver, Telephony.Sms.Inbox.CONTENT_URI, SsmMessageReceiverService.REPLACE_PROJECTION, "address = ? AND protocol = ?", strArr, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        Uri withAppendedId = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, query.getLong(0));
                        SqliteWrapper.update(context, contentResolver, withAppendedId, extractContentValues, null, null);
                        query.close();
                        return withAppendedId;
                    }
                    query.close();
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            return storeMessage(context, smsMessageArr);
        }

        private Uri storeMessage(Context context, SmsMessage[] smsMessageArr) {
            SmsMessage smsMessage = smsMessageArr[0];
            ContentValues extractContentValues = extractContentValues(smsMessage);
            if (r2 == 1) {
                extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, smsMessage.getDisplayMessageBody());
            } else {
                StringBuilder sb = new StringBuilder();
                for (SmsMessage displayMessageBody : smsMessageArr) {
                    sb.append(displayMessageBody.getDisplayMessageBody());
                }
                extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, sb.toString());
            }
            Long asLong = extractContentValues.getAsLong("thread_id");
            String asString = extractContentValues.getAsString("address");
            Contact contact = Contact.get(asString, true, context);
            if (contact != null) {
                asString = contact.getNumber();
            }
            if ((asLong == null || asLong.longValue() == 0) && asString != null) {
                extractContentValues.put("thread_id", Long.valueOf(Telephony.Threads.getOrCreateThreadId(context, asString)));
            }
            Uri insert = SqliteWrapper.insert(context, context.getContentResolver(), Telephony.Sms.Inbox.CONTENT_URI, extractContentValues);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(SsmMessageReceiverService.this.getApplicationContext(), extractContentValues.getAsLong("thread_id").longValue());
            return insert;
        }

        public String getContactNameFromPhoneNum(Context context, String str) {
            String str2;
            String substring = str.substring(0);
            String substring2 = str.contains("+86") ? substring.substring(3) : substring.substring(0);
            ContentResolver contentResolver = context.getContentResolver();
            Cursor query = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "data1 = ?", new String[]{str}, null);
            if (query.getCount() != 0) {
                if (query.moveToFirst()) {
                    str2 = query.getString(query.getColumnIndex("display_name"));
                    query.close();
                }
                str2 = "";
            } else {
                query.close();
                Cursor query2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "data1 = ?", new String[]{substring2}, null);
                if (query2.getCount() != 0 && query2.moveToFirst()) {
                    String string = query2.getString(query2.getColumnIndex("display_name"));
                    query2.close();
                    str2 = string;
                }
                str2 = "";
            }
            return str2.equals("") ? str : str2;
        }

        public void handleMessage(Message message) {
            int i = message.arg1;
            Intent intent = (Intent) message.obj;
            if (intent != null && Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
                handleSmsReceived(intent);
            }
            SsmMessageReceiver.finishStartingService(SsmMessageReceiverService.this, i);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        HandlerThread handlerThread = new HandlerThread(TAG, 10);
        handlerThread.start();
        this.topMusicService = SmsApplication.getInstance().getTopMusicService();
        this.mServiceLooper = handlerThread.getLooper();
        this.mServiceHandler = new ServiceHandler(this.mServiceLooper);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        Message obtainMessage = this.mServiceHandler.obtainMessage();
        obtainMessage.arg1 = i2;
        obtainMessage.obj = intent;
        this.mServiceHandler.sendMessage(obtainMessage);
        return 2;
    }
}
