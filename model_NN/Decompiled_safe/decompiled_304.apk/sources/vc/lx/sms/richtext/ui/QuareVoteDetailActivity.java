package vc.lx.sms.richtext.ui;

import android.app.Dialog;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.cmcc.http.parser.QuareVoteSingleGetParser;
import vc.lx.sms.cmcc.http.parser.VoteSendAnswerParser;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.ui.widget.VoteResultView;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class QuareVoteDetailActivity extends AbstractFlurryActivity {
    private static final int THREAD_LIST_QUERY_TOKEN = 0;
    /* access modifiers changed from: private */
    public int counter;
    int display_coloum = 2;
    /* access modifiers changed from: private */
    public LayoutInflater inflater = null;
    private ImageView mForwardBtn;
    private ListView mListView = null;
    LinearLayout mOptionsContent;
    LinearLayout mResultsContent;
    /* access modifiers changed from: private */
    public String mService_id = "";
    /* access modifiers changed from: private */
    public String mTitle = "";
    /* access modifiers changed from: private */
    public VoteCursorAdapter mVoteAdapter = null;
    VoteItem mVoteItem;
    /* access modifiers changed from: private */
    public VoteOptionAsynQueryHandler mVoteOptionAsynQueryHandler = null;

    class FetchQuareVoteById extends AbstractAsyncTask {
        public FetchQuareVoteById(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.fatchPublicVoteById(this.mEnginehost, this.mContext, Long.valueOf(QuareVoteDetailActivity.this.mService_id).longValue());
        }

        public String getTag() {
            return "FetchQuareVoteById";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                QuareVoteDetailActivity.this.mVoteItem = (VoteItem) miguType;
                if (QuareVoteDetailActivity.this.mVoteItem != null) {
                    QuareVoteDetailActivity.this.mVoteItem.title = QuareVoteDetailActivity.this.mTitle;
                    QuareVoteDetailActivity.this.mVoteItem.service_id = QuareVoteDetailActivity.this.mService_id;
                    QuareVoteDetailActivity.this.mVoteItem.plug = Util.getFullShortenVoteUrl(QuareVoteDetailActivity.this, QuareVoteDetailActivity.this.mVoteItem.plug);
                }
                QuareVoteDetailActivity.this.storeInDb();
                QuareVoteDetailActivity.this.initVoteDisplay();
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new QuareVoteSingleGetParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class VoteCursorAdapter extends CursorAdapter {
        Context mContext;

        public VoteCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            this.mContext = context;
            LayoutInflater unused = QuareVoteDetailActivity.this.inflater = LayoutInflater.from(context);
        }

        public void bindView(View view, Context context, Cursor cursor) {
            String string = cursor.getString(cursor.getColumnIndex("option_text"));
            String string2 = cursor.getString(cursor.getColumnIndex("contact_name"));
            int i = cursor.getInt(cursor.getColumnIndex("counter"));
            ((TextView) view.findViewById(R.id.option_title)).setText(string);
            ((TextView) view.findViewById(R.id.option_contact_name)).setText(string2);
            ((TextView) view.findViewById(R.id.option_count)).setText("(" + i + ")");
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.option_count_image);
            if (QuareVoteDetailActivity.this.mVoteItem.counter >= 1) {
                int access$300 = (i * 100) / QuareVoteDetailActivity.this.counter;
                VoteResultView voteResultView = new VoteResultView(QuareVoteDetailActivity.this.getApplicationContext(), access$300, "", true);
                Log.i(MMHttpDefines.TAG_SIZE, "size=" + access$300 + "name=" + string + "mVoteItem.counter=" + i + "counter=" + QuareVoteDetailActivity.this.counter);
                linearLayout.addView(voteResultView);
            } else {
                linearLayout.setVisibility(8);
            }
            if (i == 0) {
                linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, 0));
                linearLayout.setVisibility(8);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return QuareVoteDetailActivity.this.inflater.inflate((int) R.layout.vote_detail_item, viewGroup, false);
        }
    }

    class VoteOptionAsynQueryHandler extends AsyncQueryHandler {
        public VoteOptionAsynQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case 0:
                    if (cursor != null) {
                        QuareVoteDetailActivity.this.mVoteAdapter.changeCursor(cursor);
                        QuareVoteDetailActivity.this.mVoteAdapter.notifyDataSetInvalidated();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    class VoteSendAnswerTask extends AbstractAsyncTask {
        String option_id;
        private int page;
        String plug;

        public VoteSendAnswerTask(Context context, int i, String str, String str2) {
            super(context);
            this.page = i;
            this.plug = str;
            this.option_id = str2;
            this.mEnginehost = this.mContext.getString(R.string.dna_engine_host_version_b);
        }

        public HttpResponse getRespFromServer() throws IOException {
            Util.logEvent("public_vote_take_part_in", new NameValuePair[0]);
            return this.mCmccHttpApi.voteSendAnswerAction(this.mEnginehost, this.mContext, this.plug, this.option_id);
        }

        public String getTag() {
            return "VoteSendAnswerTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                Util.SetLastTimeOfRecommendingMusicValue(QuareVoteDetailActivity.this.getApplicationContext(), Util.getRelativeDateSpanString(System.currentTimeMillis(), QuareVoteDetailActivity.this.getApplicationContext()).toString());
                VoteItem voteItem = (VoteItem) miguType;
                ((TextView) QuareVoteDetailActivity.this.findViewById(R.id.counter)).setText(String.valueOf(voteItem.counter) + QuareVoteDetailActivity.this.getString(R.string.square_vote_attend));
                voteItem.plug = this.plug;
                ContentValues contentValues = new ContentValues();
                contentValues.put("counter", String.valueOf(voteItem.counter));
                QuareVoteDetailActivity.this.getApplicationContext().getContentResolver().update(Uri.parse(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI + "/plug/#" + voteItem.plug), contentValues, null, null);
                for (int i = 0; i < voteItem.options.size(); i++) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("counter", String.valueOf(voteItem.options.get(i).counter));
                    QuareVoteDetailActivity.this.getApplicationContext().getContentResolver().update(Uri.parse(VoteContentProvider.VOTEOPTIONS_CONTENT_URI + "/ser_id/" + voteItem.options.get(i).service_id), contentValues2, null, null);
                }
                VoteOptionAsynQueryHandler unused = QuareVoteDetailActivity.this.mVoteOptionAsynQueryHandler = new VoteOptionAsynQueryHandler(QuareVoteDetailActivity.this.getContentResolver());
                QuareVoteDetailActivity.this.mVoteOptionAsynQueryHandler.startQuery(0, null, VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null, " vote_id = ? ", new String[]{String.valueOf(QuareVoteDetailActivity.this.mVoteItem.cli_id)}, null);
                QuareVoteDetailActivity.this.mOptionsContent.setVisibility(8);
                QuareVoteDetailActivity.this.mResultsContent.setVisibility(0);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new VoteSendAnswerParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public void createAnswerDialog(final VoteOption voteOption, final String str) {
        final Dialog dialog = new Dialog(this, R.style.CustomDialogTheme);
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.answer_dialog, (ViewGroup) null);
        ((Button) inflate.findViewById(R.id.answer_yes)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                voteOption.display_order + "." + voteOption.content;
                if (Util.checkNetWork(QuareVoteDetailActivity.this.getApplicationContext())) {
                    new VoteSendAnswerTask(QuareVoteDetailActivity.this.getApplicationContext(), 1, str, voteOption.service_id).execute(new Void[0]);
                    dialog.dismiss();
                }
            }
        });
        ((Button) inflate.findViewById(R.id.answer_no)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) inflate.findViewById(R.id.answer_title)).setText(getString(R.string.you_have_selected) + voteOption.content);
        dialog.setContentView(inflate);
        dialog.show();
    }

    /* access modifiers changed from: protected */
    public void initVoteDisplay() {
        if (this.mVoteItem != null) {
            ((TextView) findViewById(R.id.title)).setText(this.mTitle);
            ((TextView) findViewById(R.id.counter)).setText(String.valueOf(this.counter) + getString(R.string.square_vote_attend));
            for (int i = 0; i < this.mVoteItem.options.size(); i++) {
                final VoteOption voteOption = this.mVoteItem.options.get(i);
                Button button = new Button(getApplicationContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                layoutParams.setMargins(10, 10, 10, 0);
                button.setLayoutParams(layoutParams);
                button.setGravity(3);
                button.setText(voteOption.display_order + "." + voteOption.content);
                button.setBackgroundResource(R.drawable.btn_vote_option);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Matcher matcher = Pattern.compile(QuareVoteDetailActivity.this.getApplicationContext().getString(R.string.shorten_url_vote_reg), 32).matcher(QuareVoteDetailActivity.this.mVoteItem.plug);
                        if (matcher != null && matcher.find()) {
                            QuareVoteDetailActivity.this.createAnswerDialog(voteOption, matcher.group(1));
                        }
                    }
                });
                this.mOptionsContent.addView(button);
            }
            this.mVoteAdapter = new VoteCursorAdapter(getApplicationContext(), null);
            this.mListView.setAdapter((ListAdapter) this.mVoteAdapter);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.quare_vote_detail);
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mOptionsContent = (LinearLayout) findViewById(R.id.options_field);
        this.mResultsContent = (LinearLayout) findViewById(R.id.result_field);
        this.mService_id = getIntent().getStringExtra("service_id");
        this.mTitle = getIntent().getStringExtra("title");
        this.counter = getIntent().getIntExtra("counter", 0);
        this.mListView = (ListView) findViewById(R.id.vote_results_listview);
        this.mForwardBtn = (ImageView) findViewById(R.id.forward_button);
        this.mForwardBtn.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View view) {
                if (QuareVoteDetailActivity.this.mVoteItem != null && QuareVoteDetailActivity.this.mVoteItem.cli_id > 0) {
                    Intent createIntent = ComposeMessageActivity.createIntent(QuareVoteDetailActivity.this, 0);
                    createIntent.putExtra("forwarded_message", true);
                    createIntent.putExtra("vote_cli_id", QuareVoteDetailActivity.this.mVoteItem.cli_id);
                    createIntent.putExtra("exit_on_sent", false);
                    createIntent.setClassName(QuareVoteDetailActivity.this, "com.android.mms.ui.ForwardMessageActivity");
                    QuareVoteDetailActivity.this.startActivity(createIntent);
                }
            }
        });
        Cursor query = getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " service_id = ? ", new String[]{this.mService_id}, null);
        if (query.getCount() > 0) {
            if (this.mVoteItem == null) {
                this.mVoteItem = new VoteItem();
            }
            if (query.moveToNext()) {
                this.mVoteItem.cli_id = (long) query.getInt(query.getColumnIndex("_id"));
                this.mVoteItem.title = query.getString(query.getColumnIndex("title"));
                this.mVoteItem.plug = query.getString(query.getColumnIndex(SmsSqliteHelper.PLUG));
                this.mVoteItem.counter = query.getInt(query.getColumnIndex("counter"));
            }
            query.close();
            Cursor query2 = getApplicationContext().getContentResolver().query(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null, " vote_id = ? ", new String[]{String.valueOf(this.mVoteItem.cli_id)}, " display_order asc ");
            while (query2.moveToNext()) {
                VoteOption voteOption = new VoteOption();
                voteOption.display_order = query2.getInt(query2.getColumnIndex("display_order"));
                voteOption.cli_id = (long) query2.getInt(query2.getColumnIndex("_id"));
                voteOption.service_id = query2.getString(query2.getColumnIndex("service_id"));
                voteOption.content = query2.getString(query2.getColumnIndex("option_text"));
                voteOption.counter = query2.getInt(query2.getColumnIndex("counter"));
                this.mVoteItem.options.add(voteOption);
            }
            query2.close();
            initVoteDisplay();
            return;
        }
        query.close();
        new FetchQuareVoteById(getApplicationContext()).execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0096 A[LOOP:0: B:12:0x008c->B:14:0x0096, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0120  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void storeInDb() {
        /*
            r12 = this;
            r9 = 0
            r8 = 0
            java.lang.String r10 = "service_id"
            java.lang.String r0 = "_id"
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.String r1 = "title"
            vc.lx.sms.cmcc.http.data.VoteItem r2 = r12.mVoteItem
            java.lang.String r2 = r2.title
            r0.put(r1, r2)
            java.lang.String r1 = "plug"
            vc.lx.sms.cmcc.http.data.VoteItem r2 = r12.mVoteItem
            java.lang.String r2 = r2.plug
            r0.put(r1, r2)
            java.lang.String r1 = "service_id"
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r12.mVoteItem
            java.lang.String r1 = r1.service_id
            r0.put(r10, r1)
            android.content.Context r1 = r12.getApplicationContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
            r1.notifyChange(r2, r8)
            android.content.Context r1 = r12.getApplicationContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
            android.net.Uri r0 = r1.insert(r2, r0)
            if (r0 == 0) goto L_0x008b
            java.lang.String r0 = r0.getFragment()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            long r5 = r0.longValue()
            android.content.Context r0 = r12.getApplicationContext()     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            java.lang.String r3 = " _id =  ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r7 = 0
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r4[r7] = r5     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            if (r1 == 0) goto L_0x0086
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r12.mVoteItem     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            java.lang.String r2 = "_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            r1.cli_id = r2     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
        L_0x0086:
            if (r0 == 0) goto L_0x008b
            r0.close()
        L_0x008b:
            r1 = r9
        L_0x008c:
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0124
            android.content.ContentValues r2 = new android.content.ContentValues
            r2.<init>()
            java.lang.String r3 = "option_text"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            java.lang.String r0 = r0.content
            r2.put(r3, r0)
            java.lang.String r0 = "vote_id"
            vc.lx.sms.cmcc.http.data.VoteItem r3 = r12.mVoteItem
            long r3 = r3.cli_id
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r2.put(r0, r3)
            java.lang.String r0 = "service_id"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            java.lang.String r0 = r0.service_id
            r2.put(r10, r0)
            java.lang.String r3 = "counter"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            int r0 = r0.counter
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.put(r3, r0)
            java.lang.String r3 = "display_order"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            int r0 = r0.display_order
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.put(r3, r0)
            android.content.Context r0 = r12.getApplicationContext()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r3 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
            r0.notifyChange(r3, r8)
            android.content.Context r0 = r12.getApplicationContext()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r3 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
            r0.insert(r3, r2)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x008c
        L_0x0113:
            r0 = move-exception
            r0 = r8
        L_0x0115:
            if (r0 == 0) goto L_0x008b
            r0.close()
            goto L_0x008b
        L_0x011c:
            r0 = move-exception
            r1 = r8
        L_0x011e:
            if (r1 == 0) goto L_0x0123
            r1.close()
        L_0x0123:
            throw r0
        L_0x0124:
            return
        L_0x0125:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x011e
        L_0x012a:
            r1 = move-exception
            goto L_0x0115
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.richtext.ui.QuareVoteDetailActivity.storeInDb():void");
    }
}
