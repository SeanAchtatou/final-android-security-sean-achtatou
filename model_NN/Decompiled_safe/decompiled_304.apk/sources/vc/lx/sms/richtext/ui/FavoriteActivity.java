package vc.lx.sms.richtext.ui;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.MusicFavoriteActivity;
import vc.lx.sms2.R;

public class FavoriteActivity extends AbstractFlurryActivity {
    /* access modifiers changed from: private */
    public List<FavoriteItem> data = null;
    /* access modifiers changed from: private */
    public LayoutInflater inflater = null;
    private FavoriteAdapter mFavoriteAdapter = null;
    private ListView mListView = null;

    class FavoriteAdapter extends BaseAdapter {
        FavoriteAdapter() {
        }

        public int getCount() {
            return FavoriteActivity.this.data.size();
        }

        public Object getItem(int i) {
            return FavoriteActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(final int i, View view, ViewGroup viewGroup) {
            FavoriteItemView favoriteItemView;
            View view2;
            if (view == null) {
                favoriteItemView = new FavoriteItemView();
                view2 = FavoriteActivity.this.inflater.inflate((int) R.layout.favorite_item, (ViewGroup) null);
                favoriteItemView.mImageView = (ImageView) view2.findViewById(R.id.favorite_icon);
                favoriteItemView.mTitleView = (TextView) view2.findViewById(R.id.favorite_title);
                favoriteItemView.mInfoView = (TextView) view2.findViewById(R.id.favorite_info);
                favoriteItemView.mNewView = (ImageView) view2.findViewById(R.id.is_favorite_new);
                favoriteItemView.mActivityNewView = (ImageView) view2.findViewById(R.id.activity_new);
                view2.setTag(favoriteItemView);
            } else {
                favoriteItemView = (FavoriteItemView) view.getTag();
                view2 = view;
            }
            favoriteItemView.mTitleView.setText(((FavoriteItem) FavoriteActivity.this.data.get(i)).name);
            favoriteItemView.mInfoView.setText(((FavoriteItem) FavoriteActivity.this.data.get(i)).info);
            if (((FavoriteItem) FavoriteActivity.this.data.get(i)).isNew) {
                favoriteItemView.mNewView.setVisibility(0);
            } else {
                favoriteItemView.mNewView.setVisibility(8);
            }
            switch (i) {
                case 0:
                    favoriteItemView.mImageView.setImageResource(R.drawable.symbol_plaza_voting);
                    favoriteItemView.mActivityNewView.setVisibility(8);
                    favoriteItemView.mInfoView.setVisibility(0);
                    break;
                case 1:
                    favoriteItemView.mImageView.setImageResource(R.drawable.symbol_plaza_music);
                    favoriteItemView.mActivityNewView.setVisibility(8);
                    favoriteItemView.mInfoView.setVisibility(0);
                    break;
                case 2:
                    favoriteItemView.mImageView.setImageResource(R.drawable.symbol_favorite_list_activity);
                    favoriteItemView.mActivityNewView.setVisibility(0);
                    favoriteItemView.mInfoView.setVisibility(8);
                    break;
            }
            view2.setClickable(true);
            view2.setFocusable(true);
            view2.setPressed(true);
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    switch (i) {
                        case 0:
                            FavoriteActivity.this.startActivity(new Intent(FavoriteActivity.this.getApplicationContext(), VoteListActivity.class));
                            return;
                        case 1:
                            FavoriteActivity.this.startActivity(new Intent(FavoriteActivity.this.getApplicationContext(), MusicFavoriteActivity.class));
                            return;
                        case 2:
                        default:
                            return;
                    }
                }
            });
            return view2;
        }
    }

    class FavoriteItem {
        String info;
        boolean isNew = false;
        String name;

        FavoriteItem() {
        }
    }

    class FavoriteItemView {
        public ImageView mActivityNewView = null;
        public ImageView mImageView = null;
        public TextView mInfoView = null;
        public ImageView mNewView = null;
        public TextView mTitleView = null;

        FavoriteItemView() {
        }
    }

    private void queryIsNew(FavoriteItem favoriteItem) {
        try {
            Cursor query = getApplicationContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " tracer_flag =  ? ", new String[]{"1"}, null);
            while (true) {
                if (query == null || !query.moveToNext()) {
                    break;
                } else if (query.getInt(query.getColumnIndex("is_new")) == 0) {
                    favoriteItem.isNew = true;
                    break;
                } else {
                    favoriteItem.isNew = false;
                }
            }
            if (query != null) {
                query.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<FavoriteItem> getData() {
        ArrayList arrayList = new ArrayList();
        FavoriteItem favoriteItem = new FavoriteItem();
        favoriteItem.name = getString(R.string.favorite_mutil_vote);
        favoriteItem.info = getString(R.string.favorite_mutil_vote_info);
        queryIsNew(favoriteItem);
        arrayList.add(favoriteItem);
        FavoriteItem favoriteItem2 = new FavoriteItem();
        favoriteItem2.name = getString(R.string.music);
        favoriteItem2.info = getString(R.string.music_info);
        arrayList.add(favoriteItem2);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.favorite);
        this.data = getData();
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mListView = (ListView) findViewById(R.id.favorite_listview);
        this.mFavoriteAdapter = new FavoriteAdapter();
        this.mListView.setAdapter((ListAdapter) this.mFavoriteAdapter);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        queryIsNew(this.data.get(0));
        this.mFavoriteAdapter.notifyDataSetChanged();
        super.onResume();
    }
}
