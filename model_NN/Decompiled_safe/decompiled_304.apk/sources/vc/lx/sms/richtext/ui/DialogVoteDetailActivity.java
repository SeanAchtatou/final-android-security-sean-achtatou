package vc.lx.sms.richtext.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.cmcc.http.parser.QuareVoteSingleGetParser;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class DialogVoteDetailActivity extends Activity implements View.OnClickListener {
    private static final int THREAD_LIST_QUERY_TOKEN = 0;
    ImageView closeDialog;
    private LayoutInflater inflater = null;
    LinearLayout mOptionsContent;
    String mService_id = "";
    Long mThread_id;
    String mTitle = "";
    VoteItem mVoteItem;
    String numbersList = "";
    Button selectBtn;

    class FetchQuareVoteById extends AbstractAsyncTask {
        public FetchQuareVoteById(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.fatchPublicVoteById(this.mEnginehost, this.mContext, Long.valueOf(DialogVoteDetailActivity.this.mService_id).longValue());
        }

        public String getTag() {
            return "FetchQuareVoteById";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                DialogVoteDetailActivity.this.mVoteItem = (VoteItem) miguType;
                if (DialogVoteDetailActivity.this.mVoteItem != null) {
                    DialogVoteDetailActivity.this.mVoteItem.title = DialogVoteDetailActivity.this.mTitle;
                    DialogVoteDetailActivity.this.mVoteItem.service_id = DialogVoteDetailActivity.this.mService_id;
                    DialogVoteDetailActivity.this.mVoteItem.plug = Util.getFullShortenVoteUrl(DialogVoteDetailActivity.this, DialogVoteDetailActivity.this.mVoteItem.plug);
                }
                DialogVoteDetailActivity.this.storeInDb();
                DialogVoteDetailActivity.this.initVoteDisplay();
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new QuareVoteSingleGetParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initVoteDisplay() {
        if (this.mVoteItem != null) {
            ((TextView) findViewById(R.id.title)).setText(this.mTitle);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.mVoteItem.options.size()) {
                    VoteOption voteOption = this.mVoteItem.options.get(i2);
                    TextView textView = new TextView(getApplicationContext());
                    textView.setText(voteOption.display_order + "." + voteOption.content);
                    this.mOptionsContent.addView(textView);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void onClick(View view) {
        if (view == this.selectBtn) {
            Matcher matcher = Pattern.compile(getApplicationContext().getString(R.string.shorten_url_vote_reg), 32).matcher(this.mVoteItem.plug);
            if (matcher != null && matcher.find()) {
                matcher.group(0);
                matcher.group(1);
                Intent intent = new Intent();
                intent.putExtra("vote_cli_id", this.mVoteItem.cli_id);
                intent.putExtra(PrefsUtil.KEY_VOTE_TITLE, this.mVoteItem.title);
                setResult(-1, intent);
                finish();
            }
        } else if (view == this.closeDialog) {
            setResult(-1, new Intent());
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.dialog_vote_detail);
        this.numbersList = getIntent().getStringExtra("address");
        this.mThread_id = Long.valueOf(getIntent().getLongExtra("thread_id", 0));
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mOptionsContent = (LinearLayout) findViewById(R.id.options_field);
        this.mService_id = getIntent().getStringExtra("service_id");
        this.mTitle = getIntent().getStringExtra("title");
        this.selectBtn = (Button) findViewById(R.id.select);
        this.selectBtn.setOnClickListener(this);
        Cursor query = getApplicationContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " service_id = ? ", new String[]{this.mService_id}, null);
        if (query.getCount() > 0) {
            if (this.mVoteItem == null) {
                this.mVoteItem = new VoteItem();
            }
            if (query.moveToNext()) {
                this.mVoteItem.cli_id = (long) query.getInt(query.getColumnIndex("_id"));
                this.mVoteItem.title = query.getString(query.getColumnIndex("title"));
                this.mVoteItem.plug = query.getString(query.getColumnIndex(SmsSqliteHelper.PLUG));
            }
            query.close();
            Cursor query2 = getApplicationContext().getContentResolver().query(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null, " vote_id = ? ", new String[]{String.valueOf(this.mVoteItem.cli_id)}, " display_order asc ");
            while (query2.moveToNext()) {
                VoteOption voteOption = new VoteOption();
                voteOption.display_order = query2.getInt(query2.getColumnIndex("display_order"));
                voteOption.cli_id = (long) query2.getInt(query2.getColumnIndex("_id"));
                voteOption.service_id = query2.getString(query2.getColumnIndex("service_id"));
                voteOption.content = query2.getString(query2.getColumnIndex("option_text"));
                voteOption.counter = query2.getInt(query2.getColumnIndex("counter"));
                this.mVoteItem.options.add(voteOption);
            }
            query2.close();
            initVoteDisplay();
        } else {
            query.close();
            new FetchQuareVoteById(getApplicationContext()).execute(new Void[0]);
        }
        this.closeDialog = (ImageView) findViewById(R.id.close_dialog);
        this.closeDialog.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0096 A[LOOP:0: B:12:0x008c->B:14:0x0096, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0120  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void storeInDb() {
        /*
            r12 = this;
            r9 = 0
            r8 = 0
            java.lang.String r10 = "service_id"
            java.lang.String r0 = "_id"
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.String r1 = "title"
            vc.lx.sms.cmcc.http.data.VoteItem r2 = r12.mVoteItem
            java.lang.String r2 = r2.title
            r0.put(r1, r2)
            java.lang.String r1 = "plug"
            vc.lx.sms.cmcc.http.data.VoteItem r2 = r12.mVoteItem
            java.lang.String r2 = r2.plug
            r0.put(r1, r2)
            java.lang.String r1 = "service_id"
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r12.mVoteItem
            java.lang.String r1 = r1.service_id
            r0.put(r10, r1)
            android.content.Context r1 = r12.getApplicationContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
            r1.notifyChange(r2, r8)
            android.content.Context r1 = r12.getApplicationContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
            android.net.Uri r0 = r1.insert(r2, r0)
            if (r0 == 0) goto L_0x008b
            java.lang.String r0 = r0.getFragment()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            long r5 = r0.longValue()
            android.content.Context r0 = r12.getApplicationContext()     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            java.lang.String r3 = " _id =  ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r7 = 0
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r4[r7] = r5     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0113, all -> 0x011c }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            if (r1 == 0) goto L_0x0086
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r12.mVoteItem     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            java.lang.String r2 = "_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
            r1.cli_id = r2     // Catch:{ Exception -> 0x012a, all -> 0x0125 }
        L_0x0086:
            if (r0 == 0) goto L_0x008b
            r0.close()
        L_0x008b:
            r1 = r9
        L_0x008c:
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x0124
            android.content.ContentValues r2 = new android.content.ContentValues
            r2.<init>()
            java.lang.String r3 = "option_text"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            java.lang.String r0 = r0.content
            r2.put(r3, r0)
            java.lang.String r0 = "vote_id"
            vc.lx.sms.cmcc.http.data.VoteItem r3 = r12.mVoteItem
            long r3 = r3.cli_id
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r2.put(r0, r3)
            java.lang.String r0 = "service_id"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            java.lang.String r0 = r0.service_id
            r2.put(r10, r0)
            java.lang.String r3 = "counter"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            int r0 = r0.counter
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.put(r3, r0)
            java.lang.String r3 = "display_order"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r12.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r1)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            int r0 = r0.display_order
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.put(r3, r0)
            android.content.Context r0 = r12.getApplicationContext()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r3 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
            r0.notifyChange(r3, r8)
            android.content.Context r0 = r12.getApplicationContext()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r3 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
            r0.insert(r3, r2)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x008c
        L_0x0113:
            r0 = move-exception
            r0 = r8
        L_0x0115:
            if (r0 == 0) goto L_0x008b
            r0.close()
            goto L_0x008b
        L_0x011c:
            r0 = move-exception
            r1 = r8
        L_0x011e:
            if (r1 == 0) goto L_0x0123
            r1.close()
        L_0x0123:
            throw r0
        L_0x0124:
            return
        L_0x0125:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x011e
        L_0x012a:
            r1 = move-exception
            goto L_0x0115
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.richtext.ui.DialogVoteDetailActivity.storeInDb():void");
    }
}
