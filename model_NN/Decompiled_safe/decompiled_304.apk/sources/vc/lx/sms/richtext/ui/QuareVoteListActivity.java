package vc.lx.sms.richtext.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteList;
import vc.lx.sms.cmcc.http.parser.QuareVoteListParser;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class QuareVoteListActivity extends AbstractFlurryActivity {
    private static final int THREAD_LIST_QUERY_TOKEN = 0;
    /* access modifiers changed from: private */
    public static int pageNo = 1;
    /* access modifiers changed from: private */
    public QuareListAdapter adapter = null;
    /* access modifiers changed from: private */
    public LayoutInflater inflater = null;
    boolean isFromCompose = false;
    private List<VoteItem> mData = new ArrayList();
    /* access modifiers changed from: private */
    public ListView mListView = null;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;
    Long mThread_id;
    String numbersList = "";

    class FetchQuareVoteList extends AbstractAsyncTask {
        public FetchQuareVoteList(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.fatchPublicVoteList(this.mEnginehost, this.mContext, QuareVoteListActivity.pageNo);
        }

        public String getTag() {
            return "FetchQuareVoteList";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType == null || !(miguType instanceof VoteList)) {
                Toast.makeText(QuareVoteListActivity.this.getApplicationContext(), QuareVoteListActivity.this.getString(R.string.quare_vote_list_fail), 0).show();
            } else {
                VoteList voteList = (VoteList) miguType;
                QuareVoteListActivity.access$012(1);
                if (QuareVoteListActivity.this.adapter == null) {
                    QuareListAdapter unused = QuareVoteListActivity.this.adapter = new QuareListAdapter(voteList.voteItems, voteList.hasmore);
                    QuareVoteListActivity.this.mListView.setAdapter((ListAdapter) QuareVoteListActivity.this.adapter);
                } else {
                    QuareVoteListActivity.this.adapter.addData(voteList.voteItems);
                    QuareVoteListActivity.this.adapter.setHasmore(voteList.hasmore);
                    QuareVoteListActivity.this.adapter.notifyDataSetChanged();
                }
            }
            QuareVoteListActivity.this.mProgressBar.setVisibility(8);
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new QuareVoteListParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class QuareListAdapter extends BaseAdapter {
        private String hasmore;
        View pendingView = null;
        /* access modifiers changed from: private */
        public List<VoteItem> voteItems;

        class VoteItemView {
            public TextView mLoading = null;
            public TextView mMermberCount = null;
            public TextView mTitleView = null;

            VoteItemView() {
            }
        }

        public QuareListAdapter(List<VoteItem> list, String str) {
            this.voteItems = list;
            this.hasmore = str;
        }

        public void addData(List<VoteItem> list) {
            this.voteItems.addAll(list);
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public int getCount() {
            return "true".equals(this.hasmore) ? this.voteItems.size() + 1 : this.voteItems.size();
        }

        public Object getItem(int i) {
            return this.voteItems.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public int getItemViewType(int i) {
            return i == this.voteItems.size() ? -1 : 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            VoteItemView voteItemView;
            View view2;
            if (view == null) {
                voteItemView = new VoteItemView();
                view2 = QuareVoteListActivity.this.inflater.inflate((int) R.layout.quare_vote_list_item, viewGroup, false);
                voteItemView.mTitleView = (TextView) view2.findViewById(R.id.vote_title);
                voteItemView.mMermberCount = (TextView) view2.findViewById(R.id.vote_counter);
                voteItemView.mLoading = (TextView) view2.findViewById(R.id.loading_text);
                view2.setTag(voteItemView);
            } else {
                voteItemView = (VoteItemView) view.getTag();
                view2 = view;
            }
            if (i == this.voteItems.size()) {
                voteItemView.mLoading.setVisibility(0);
                view2.findViewById(R.id.vote_item).setVisibility(8);
                view2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        new FetchQuareVoteList(QuareVoteListActivity.this.getApplicationContext()).execute(new Void[0]);
                    }
                });
            } else {
                voteItemView.mTitleView.setText(this.voteItems.get(i).title);
                voteItemView.mMermberCount.setText(String.valueOf(this.voteItems.get(i).counter) + QuareVoteListActivity.this.getApplicationContext().getString(R.string.square_vote_attend));
                view2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (QuareVoteListActivity.this.isFromCompose) {
                            Intent intent = new Intent(QuareVoteListActivity.this, DialogVoteDetailActivity.class);
                            intent.putExtra("service_id", ((VoteItem) QuareListAdapter.this.voteItems.get(i)).service_id);
                            intent.putExtra("title", ((VoteItem) QuareListAdapter.this.voteItems.get(i)).title);
                            intent.putExtra("thread_id", QuareVoteListActivity.this.mThread_id);
                            intent.putExtra("address", QuareVoteListActivity.this.numbersList);
                            QuareVoteListActivity.this.startActivityForResult(intent, 26);
                            return;
                        }
                        Intent intent2 = new Intent(QuareVoteListActivity.this, QuareVoteDetailActivity.class);
                        intent2.putExtra("service_id", ((VoteItem) QuareListAdapter.this.voteItems.get(i)).service_id);
                        intent2.putExtra("counter", ((VoteItem) QuareListAdapter.this.voteItems.get(i)).counter);
                        intent2.putExtra("title", ((VoteItem) QuareListAdapter.this.voteItems.get(i)).title);
                        QuareVoteListActivity.this.startActivity(intent2);
                    }
                });
            }
            return view2;
        }

        public int getViewTypeCount() {
            return 2;
        }

        public void setHasmore(String str) {
            this.hasmore = str;
        }
    }

    static /* synthetic */ int access$012(int i) {
        int i2 = pageNo + i;
        pageNo = i2;
        return i2;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case ComposeMessageActivity.REQUEST_CODE_VOTE_DETAIL /*26*/:
                if (intent != null && intent.getLongExtra("vote_cli_id", 0) > 0) {
                    setResult(-1, intent);
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.isFromCompose = getIntent().getBooleanExtra("from_compose", false);
        this.numbersList = getIntent().getStringExtra("address");
        this.mThread_id = Long.valueOf(getIntent().getLongExtra("thread_id", 0));
        setContentView((int) R.layout.vote_list);
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mListView = (ListView) findViewById(R.id.vote_listview);
        this.mProgressBar = (ProgressBar) findViewById(R.id.load_progress);
        this.mProgressBar.setVisibility(0);
        if (Util.checkNetWork(getApplicationContext())) {
            new FetchQuareVoteList(getApplicationContext()).execute(new Void[0]);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.network_error), 0).show();
        }
        Util.logEvent("vote_app_open", new NameValuePair[0]);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        pageNo = 0;
        this.adapter = null;
        if (Util.checkNetWork(getApplicationContext())) {
            new FetchQuareVoteList(getApplicationContext()).execute(new Void[0]);
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        pageNo = 1;
    }
}
