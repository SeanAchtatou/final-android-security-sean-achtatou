package vc.lx.sms.richtext.ui;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.GeneralResp;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.parser.GeneralRespParser;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class FeedBackActivity extends AbstractFlurryActivity {
    /* access modifiers changed from: private */
    public EditText commentText = null;
    /* access modifiers changed from: private */
    public EditText contactText = null;
    /* access modifiers changed from: private */
    public Button okButton = null;
    /* access modifiers changed from: private */
    public EditText qqText = null;
    /* access modifiers changed from: private */
    public EditText weiboText = null;

    class CommitCommentTask extends AbstractAsyncTask {
        private String contact = null;
        private Context context = null;
        private String qq = null;
        private String userComment = null;
        private String weibo = null;

        public CommitCommentTask(Context context2) {
            super(context2);
            this.context = context2;
        }

        public CommitCommentTask(Context context2, String str, String str2, String str3, String str4) {
            super(context2);
            this.userComment = str;
            this.context = context2;
            this.contact = str2 == null ? "" : str2;
            this.qq = str2 == null ? "" : str3;
            this.weibo = str2 == null ? "" : str4;
        }

        public HttpResponse getRespFromServer() throws IOException {
            String string = this.mContext.getString(R.string.dna_engine_host);
            String mobileModel = Util.getMobileModel(this.context);
            String deviceImie = Util.getDeviceImie(this.context);
            return this.mCmccHttpApi.commitCommentTask(this.context, string, this.mContext, this.userComment, mobileModel, deviceImie, this.contact, this.qq, this.weibo);
        }

        public String getTag() {
            return "CommitCommentTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType == null || !(miguType instanceof GeneralResp)) {
                Toast.makeText(this.context, (int) R.string.commit_comment_fail, 0).show();
            } else if ("ok".equals(((GeneralResp) miguType).result)) {
                Toast.makeText(this.context, (int) R.string.commit_comment_success, 0).show();
            } else {
                Toast.makeText(this.context, (int) R.string.commit_comment_fail, 0).show();
            }
            FeedBackActivity.this.finish();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            httpResponse.getStatusLine().getStatusCode();
            try {
                return new GeneralRespParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (ParseException e2) {
                e2.printStackTrace();
                return null;
            } catch (SmsException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.feed_back_dlg);
        this.commentText = (EditText) findViewById(R.id.feed_comment);
        this.contactText = (EditText) findViewById(R.id.input_contact_phone);
        this.qqText = (EditText) findViewById(R.id.qq_phone);
        this.weiboText = (EditText) findViewById(R.id.weibo_num);
        this.okButton = (Button) findViewById(R.id.yes);
        this.commentText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    FeedBackActivity.this.okButton.setClickable(true);
                    FeedBackActivity.this.okButton.setPressed(false);
                    return;
                }
                FeedBackActivity.this.okButton.setPressed(true);
                FeedBackActivity.this.okButton.setClickable(false);
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        this.contactText.setFocusable(true);
        this.okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String trim = FeedBackActivity.this.commentText.getText().toString().trim();
                String trim2 = FeedBackActivity.this.contactText.getText().toString().trim();
                String trim3 = FeedBackActivity.this.qqText.getText().toString().trim();
                String trim4 = FeedBackActivity.this.weiboText.getText().toString().trim();
                if (trim.length() > 0) {
                    new CommitCommentTask(FeedBackActivity.this, trim, trim2, trim3, trim4).execute(new Void[0]);
                }
                Util.logEvent("feedback_send", new NameValuePair[0]);
            }
        });
        this.okButton.setPressed(true);
        this.okButton.setClickable(false);
    }
}
