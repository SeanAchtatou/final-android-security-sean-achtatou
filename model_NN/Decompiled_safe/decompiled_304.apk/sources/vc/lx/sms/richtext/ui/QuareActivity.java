package vc.lx.sms.richtext.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.MusicPlatformActivity;
import vc.lx.sms2.R;

public class QuareActivity extends AbstractFlurryActivity {
    /* access modifiers changed from: private */
    public List<FavoriteItem> data = null;
    /* access modifiers changed from: private */
    public LayoutInflater inflater = null;
    private ListView mListView = null;
    private QuareAdapter mQuareAdapter = null;

    class FavoriteItem {
        String info;
        boolean isNew = false;
        String name;

        FavoriteItem() {
        }
    }

    class FavoriteItemView {
        public ImageView mImageView = null;
        public ImageView mSquareNewView = null;
        public TextView mTitleInfoView = null;
        public TextView mTitleView = null;

        FavoriteItemView() {
        }
    }

    class QuareAdapter extends BaseAdapter {
        QuareAdapter() {
        }

        public int getCount() {
            return QuareActivity.this.data.size();
        }

        public Object getItem(int i) {
            return QuareActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(final int i, View view, ViewGroup viewGroup) {
            FavoriteItemView favoriteItemView;
            View view2;
            if (view == null) {
                favoriteItemView = new FavoriteItemView();
                view2 = QuareActivity.this.inflater.inflate((int) R.layout.quare_item, (ViewGroup) null);
                favoriteItemView.mImageView = (ImageView) view2.findViewById(R.id.square_icon);
                favoriteItemView.mTitleView = (TextView) view2.findViewById(R.id.quare_title);
                favoriteItemView.mTitleInfoView = (TextView) view2.findViewById(R.id.quare_info);
                favoriteItemView.mSquareNewView = (ImageView) view2.findViewById(R.id.square_new);
                view2.setTag(favoriteItemView);
            } else {
                favoriteItemView = (FavoriteItemView) view.getTag();
                view2 = view;
            }
            favoriteItemView.mTitleView.setText(((FavoriteItem) QuareActivity.this.data.get(i)).name);
            favoriteItemView.mTitleInfoView.setText(((FavoriteItem) QuareActivity.this.data.get(i)).info);
            switch (i) {
                case 0:
                    favoriteItemView.mImageView.setImageResource(R.drawable.symbol_plaza_voting);
                    break;
                case 1:
                    favoriteItemView.mImageView.setImageResource(R.drawable.symbol_plaza_music);
                    break;
                case 2:
                    favoriteItemView.mImageView.setImageResource(R.drawable.symbol_plaza_sms);
                    break;
                case 3:
                    favoriteItemView.mImageView.setImageResource(R.drawable.symbol_plaza_activity);
                    favoriteItemView.mTitleInfoView.setVisibility(8);
                    favoriteItemView.mSquareNewView.setVisibility(0);
                    break;
            }
            view2.setClickable(true);
            view2.setFocusable(true);
            view2.setPressed(true);
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    switch (i) {
                        case 0:
                            QuareActivity.this.startActivity(new Intent(QuareActivity.this.getApplicationContext(), QuareVoteListActivity.class));
                            return;
                        case 1:
                            QuareActivity.this.startActivity(new Intent(QuareActivity.this.getApplicationContext(), MusicPlatformActivity.class));
                            return;
                        case 2:
                            QuareActivity.this.startActivity(new Intent(QuareActivity.this.getApplicationContext(), SmsTemplateGroupListActivity.class));
                            return;
                        case 3:
                        default:
                            return;
                    }
                }
            });
            return view2;
        }
    }

    public List<FavoriteItem> getData() {
        ArrayList arrayList = new ArrayList();
        FavoriteItem favoriteItem = new FavoriteItem();
        favoriteItem.name = getString(R.string.quare_vote);
        favoriteItem.info = getString(R.string.quare_vote_info);
        arrayList.add(favoriteItem);
        FavoriteItem favoriteItem2 = new FavoriteItem();
        favoriteItem2.name = getString(R.string.quare_music);
        favoriteItem2.info = getString(R.string.quare_music_info);
        arrayList.add(favoriteItem2);
        FavoriteItem favoriteItem3 = new FavoriteItem();
        favoriteItem3.name = getString(R.string.quare_sms);
        favoriteItem3.info = getString(R.string.quare_sms_info);
        arrayList.add(favoriteItem3);
        FavoriteItem favoriteItem4 = new FavoriteItem();
        favoriteItem4.name = getString(R.string.quare_activty);
        favoriteItem4.info = getString(R.string.quare_activty_info);
        arrayList.add(favoriteItem4);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.quare);
        this.data = getData();
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mListView = (ListView) findViewById(R.id.quare_listview);
        this.mQuareAdapter = new QuareAdapter();
        this.mListView.setAdapter((ListAdapter) this.mQuareAdapter);
    }
}
