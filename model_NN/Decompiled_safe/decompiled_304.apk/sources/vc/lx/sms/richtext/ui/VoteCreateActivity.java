package vc.lx.sms.richtext.ui;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.android.mms.transaction.MessagingNotification;
import com.android.provider.Telephony;
import org.apache.http.NameValuePair;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class VoteCreateActivity extends AbstractFlurryActivity implements View.OnClickListener {
    private static final String SMS_DRAFT_WHERE = "type=3";
    ImageView mAddView;
    int mAnswerCount = 0;
    Button mBack;
    Button mConfirm;
    LinearLayout mContent;
    EditText mEditTitle;
    private Handler mHandler;
    LayoutInflater mInflater;
    ImageView mRemoveVote;
    VoteItem mVoteItem;
    AlertDialog.Builder waitingDialog;

    private void asyncDelete(Uri uri, String str, String[] strArr) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("asyncDelete %s where %s", uri, str);
        }
        SqliteWrapper.delete(getApplicationContext(), getContentResolver(), uri, str, strArr);
    }

    private void asyncDeleteDraftSmsMessage(Conversation conversation) {
        long threadId = conversation.getThreadId();
        if (threadId > 0) {
            asyncDelete(ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, threadId), SMS_DRAFT_WHERE, null);
        }
    }

    public static Intent createIntentByThreadId(Context context, long j) {
        Intent intent = new Intent(context, VoteCreateActivity.class);
        intent.putExtra("thread_id", j);
        return intent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void updateDraftSmsMessage(long j, String str) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("updateDraftSmsMessage tid=%d, contents=\"%s\"", Long.valueOf(j), str);
        }
        if (j > 0) {
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("thread_id", Long.valueOf(j));
            contentValues.put(Telephony.TextBasedSmsColumns.BODY, str);
            contentValues.put("type", (Integer) 3);
            SqliteWrapper.insert(this, getContentResolver(), Telephony.Sms.CONTENT_URI, contentValues);
        }
    }

    private void updateDraftSmsMessage(Conversation conversation, String str) {
        long ensureThreadId = conversation.ensureThreadId();
        conversation.setDraftState(true);
        updateDraftSmsMessage(ensureThreadId, str);
    }

    /* access modifiers changed from: protected */
    public void deleteOldVoteDraft() {
    }

    /* access modifiers changed from: protected */
    public void loadVoteDraft() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onClick(View view) {
        if (view == this.mAddView) {
            this.mAnswerCount++;
            final LinearLayout linearLayout = (LinearLayout) this.mInflater.inflate((int) R.layout.vote_create_item, (ViewGroup) this.mContent, false);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(0, 10, 0, 0);
            linearLayout.setLayoutParams(layoutParams);
            EditText editText = (EditText) linearLayout.findViewById(R.id.answer_body);
            editText.setHint(getString(R.string.vote_item_hint));
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
            this.mContent.addView(linearLayout);
            linearLayout.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    VoteCreateActivity.this.mContent.removeView(linearLayout);
                    VoteCreateActivity.this.mAnswerCount--;
                }
            });
            getCurrentFocus().clearFocus();
            editText.requestFocus();
            editText.refreshDrawableState();
        } else if (view == this.mConfirm) {
            if (this.mEditTitle.getText().toString().equals("")) {
                this.waitingDialog.setMessage(getString(R.string.vote_tile_should_not_be_null));
                this.waitingDialog.show();
            } else if (this.mVoteItem == null) {
                this.mVoteItem = new VoteItem();
                this.mVoteItem.title = this.mEditTitle.getText().toString();
                for (int i = 0; i < this.mContent.getChildCount(); i++) {
                    String obj = ((EditText) ((LinearLayout) this.mContent.getChildAt(i)).findViewById(R.id.answer_body)).getText().toString();
                    if (!obj.equals("")) {
                        VoteOption voteOption = new VoteOption();
                        voteOption.content = obj;
                        voteOption.display_order = i + 1;
                        this.mVoteItem.options.add(voteOption);
                    }
                }
                if (this.mVoteItem.options.size() < 2) {
                    this.mVoteItem = null;
                    this.waitingDialog.setMessage(getString(R.string.vote_option_should_be_two));
                    this.waitingDialog.show();
                    return;
                }
                storeInDb();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
                if (inputMethodManager.isActive()) {
                    inputMethodManager.toggleSoftInput(1, 2);
                }
                Intent intent = new Intent();
                intent.putExtra("vote_cli_id", this.mVoteItem.cli_id);
                intent.putExtra(PrefsUtil.KEY_VOTE_TITLE, this.mVoteItem.title);
                setResult(-1, intent);
                Util.logEvent("vote_create", new NameValuePair[0]);
                finish();
            }
        } else if (view == this.mBack) {
            InputMethodManager inputMethodManager2 = (InputMethodManager) getSystemService("input_method");
            if (inputMethodManager2.isActive()) {
                inputMethodManager2.toggleSoftInput(1, 2);
            }
            finish();
        } else if (view == this.mRemoveVote) {
            this.mEditTitle.setText("");
            this.mContent.removeAllViews();
            this.mVoteItem = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.vote_create_activity);
        this.waitingDialog = new AlertDialog.Builder(this);
        this.waitingDialog.setMessage(getString(R.string.waiting_for_send));
        this.mHandler = new Handler();
        this.mContent = (LinearLayout) findViewById(R.id.vote_content);
        this.mInflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mEditTitle = (EditText) findViewById(R.id.vote_title);
        this.mEditTitle.setHint(getString(R.string.theme));
        this.mAddView = (ImageView) findViewById(R.id.add_vote_item);
        this.mAddView.setOnClickListener(this);
        this.mConfirm = (Button) findViewById(R.id.confirm_btn);
        this.mConfirm.setOnClickListener(this);
        this.mBack = (Button) findViewById(R.id.back_btn);
        this.mBack.setOnClickListener(this);
        this.mRemoveVote = (ImageView) findViewById(R.id.vote_delete);
        this.mRemoveVote.setOnClickListener(this);
    }

    public void quickSendSms(String str, String str2) {
        if (!str2.equals("") && str2.length() > 0) {
            MessagingNotification.updateNewMessageIndicator(this, false);
            Util.sendSMS(str, str2, this);
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_QUICK_REPLY, new NameValuePair[0]);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0142  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void storeInDb() {
        /*
            r11 = this;
            r8 = 0
            r9 = 0
            java.lang.String r0 = "_id"
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.String r1 = "title"
            vc.lx.sms.cmcc.http.data.VoteItem r2 = r11.mVoteItem
            java.lang.String r2 = r2.title
            r0.put(r1, r2)
            android.content.Context r1 = r11.getApplicationContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
            r1.notifyChange(r2, r9)
            android.content.Context r1 = r11.getApplicationContext()
            android.content.ContentResolver r1 = r1.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
            android.net.Uri r0 = r1.insert(r2, r0)
            if (r0 == 0) goto L_0x007a
            java.lang.String r0 = r0.getFragment()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            long r5 = r0.longValue()
            android.content.Context r0 = r11.getApplicationContext()     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            java.lang.String r3 = " _id =  ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            r7 = 0
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            r4[r7] = r5     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0125, all -> 0x012e }
            r0.getCount()     // Catch:{ Exception -> 0x0151, all -> 0x014c }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0151, all -> 0x014c }
            if (r1 == 0) goto L_0x0075
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r11.mVoteItem     // Catch:{ Exception -> 0x0151, all -> 0x014c }
            java.lang.String r2 = "_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0151, all -> 0x014c }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0151, all -> 0x014c }
            r1.cli_id = r2     // Catch:{ Exception -> 0x0151, all -> 0x014c }
        L_0x0075:
            if (r0 == 0) goto L_0x007a
            r0.close()
        L_0x007a:
            r6 = r8
        L_0x007b:
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r11.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            int r0 = r0.size()
            if (r6 >= r0) goto L_0x0146
            android.content.ContentValues r1 = new android.content.ContentValues
            r1.<init>()
            java.lang.String r2 = "option_text"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r11.mVoteItem
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options
            java.lang.Object r0 = r0.get(r6)
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
            java.lang.String r0 = r0.content
            r1.put(r2, r0)
            java.lang.String r0 = "vote_id"
            vc.lx.sms.cmcc.http.data.VoteItem r2 = r11.mVoteItem
            long r2 = r2.cli_id
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r1.put(r0, r2)
            java.lang.String r0 = "display_order"
            int r2 = r6 + 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.put(r0, r2)
            android.content.Context r0 = r11.getApplicationContext()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
            r0.notifyChange(r2, r9)
            android.content.Context r0 = r11.getApplicationContext()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
            android.net.Uri r0 = r0.insert(r2, r1)
            if (r0 == 0) goto L_0x0120
            java.lang.String r0 = r0.getFragment()
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            long r7 = r0.longValue()
            android.content.Context r0 = r11.getApplicationContext()     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            java.lang.String r3 = " _id = ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            r5 = 0
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            r4[r5] = r7     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0136, all -> 0x013e }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
            if (r0 == 0) goto L_0x011b
            vc.lx.sms.cmcc.http.data.VoteItem r0 = r11.mVoteItem     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r0.options     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
            vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
            java.lang.String r2 = "_id"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
            long r2 = r1.getLong(r2)     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
            r0.cli_id = r2     // Catch:{ Exception -> 0x0149, all -> 0x0147 }
        L_0x011b:
            if (r1 == 0) goto L_0x0120
            r1.close()
        L_0x0120:
            int r0 = r6 + 1
            r6 = r0
            goto L_0x007b
        L_0x0125:
            r0 = move-exception
            r0 = r9
        L_0x0127:
            if (r0 == 0) goto L_0x007a
            r0.close()
            goto L_0x007a
        L_0x012e:
            r0 = move-exception
            r1 = r9
        L_0x0130:
            if (r1 == 0) goto L_0x0135
            r1.close()
        L_0x0135:
            throw r0
        L_0x0136:
            r0 = move-exception
            r0 = r9
        L_0x0138:
            if (r0 == 0) goto L_0x0120
            r0.close()
            goto L_0x0120
        L_0x013e:
            r0 = move-exception
            r1 = r9
        L_0x0140:
            if (r1 == 0) goto L_0x0145
            r1.close()
        L_0x0145:
            throw r0
        L_0x0146:
            return
        L_0x0147:
            r0 = move-exception
            goto L_0x0140
        L_0x0149:
            r0 = move-exception
            r0 = r1
            goto L_0x0138
        L_0x014c:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0130
        L_0x0151:
            r1 = move-exception
            goto L_0x0127
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.richtext.ui.VoteCreateActivity.storeInDb():void");
    }
}
