package vc.lx.sms.richtext.ui;

import android.app.Dialog;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.widget.VoteResultView;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class VoteDetailActivity extends AbstractFlurryActivity {
    private static final int THREAD_LIST_QUERY_TOKEN = 0;
    /* access modifiers changed from: private */
    public List<String> contactNames = null;
    /* access modifiers changed from: private */
    public LayoutInflater inflater = null;
    private ListView mListView = null;
    /* access modifiers changed from: private */
    public VoteCursorAdapter mVoteAdapter = null;
    private String mVoteContactName = "";
    /* access modifiers changed from: private */
    public int mVoteCounter = 0;
    private long mVoteId = 0;
    private TextView mVoteInfoView = null;
    private VoteOptionAsynQueryHandler mVoteOptionAsynQueryHandler = null;
    private String mVoteTitle = "";
    private TextView mVoteTitleView = null;
    public View.OnClickListener textViewOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            Dialog dialog = new Dialog(VoteDetailActivity.this.getApplicationContext(), R.style.CustomDialogTheme);
            View inflate = LayoutInflater.from(VoteDetailActivity.this.getApplicationContext()).inflate((int) R.layout.show_vote_send_name, (ViewGroup) null);
            ((GridView) inflate.findViewById(R.id.gridview)).setAdapter((ListAdapter) VoteDetailActivity.this.getGridViewAdapter(VoteDetailActivity.this.contactNames));
            dialog.setContentView(inflate);
            dialog.show();
        }
    };

    class VoteCursorAdapter extends CursorAdapter {
        public VoteCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            LayoutInflater unused = VoteDetailActivity.this.inflater = LayoutInflater.from(context);
        }

        public void bindView(View view, Context context, Cursor cursor) {
            String string = cursor.getString(cursor.getColumnIndex("option_text"));
            String string2 = cursor.getString(cursor.getColumnIndex("contact_name"));
            int i = cursor.getInt(cursor.getColumnIndex("counter"));
            ((TextView) view.findViewById(R.id.option_title)).setText(string);
            ((TextView) view.findViewById(R.id.option_contact_name)).setText(string2);
            ((TextView) view.findViewById(R.id.option_count)).setText("(" + i + ")");
            if (VoteDetailActivity.this.mVoteCounter >= 1) {
                int access$500 = (i * 100) / VoteDetailActivity.this.mVoteCounter;
                VoteResultView voteResultView = new VoteResultView(VoteDetailActivity.this.getApplicationContext(), access$500, "", true);
                Log.i(MMHttpDefines.TAG_SIZE, "size=" + access$500 + "name=" + string + "mVoteCounter=" + VoteDetailActivity.this.mVoteCounter + "counter=" + i);
                ((LinearLayout) view.findViewById(R.id.option_count_image)).addView(voteResultView);
            } else {
                ((LinearLayout) view.findViewById(R.id.option_count_image)).setVisibility(8);
            }
            if (i == 0) {
                ((LinearLayout) view.findViewById(R.id.option_count_image)).setVisibility(8);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return VoteDetailActivity.this.inflater.inflate((int) R.layout.vote_detail_item, viewGroup, false);
        }
    }

    class VoteOptionAsynQueryHandler extends AsyncQueryHandler {
        public VoteOptionAsynQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case 0:
                    if (cursor != null) {
                        VoteDetailActivity.this.mVoteAdapter.changeCursor(cursor);
                        VoteDetailActivity.this.mVoteAdapter.notifyDataSetInvalidated();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void createDialog() {
        Dialog dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.setCanceledOnTouchOutside(true);
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.show_vote_send_name, (ViewGroup) null);
        GridView gridView = (GridView) inflate.findViewById(R.id.gridview);
        gridView.setNumColumns(2);
        gridView.setSelector((int) R.color.alpha_00);
        gridView.setAdapter((ListAdapter) getGridViewAdapter(this.contactNames));
        dialog.setContentView(inflate);
        dialog.show();
    }

    /* access modifiers changed from: private */
    public SimpleAdapter getGridViewAdapter(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            HashMap hashMap = new HashMap();
            hashMap.put("item_name", this.contactNames.get(i));
            arrayList.add(hashMap);
        }
        return new SimpleAdapter(this, arrayList, R.layout.vote_grid_view_item, new String[]{"item_name"}, new int[]{R.id.item_name});
    }

    public List<String> getSendContactName(String str) {
        ArrayList arrayList = new ArrayList();
        if (str != null && str.trim().length() > 0) {
            String[] split = str.split(",");
            for (String add : split) {
                arrayList.add(add);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.vote_detail);
        this.mVoteId = getIntent().getLongExtra(PrefsUtil.KEY_VOTE_ID, 1);
        this.mVoteCounter = getIntent().getIntExtra(PrefsUtil.KEY_VOTE_COUNTER, 0);
        this.mVoteContactName = getIntent().getStringExtra(PrefsUtil.KEY_VOTE_CONTACT_NAME);
        this.mVoteTitle = getIntent().getStringExtra(PrefsUtil.KEY_VOTE_TITLE);
        String stringExtra = getIntent().getStringExtra(PrefsUtil.KEY_VOTE_IS_CLICK);
        if (stringExtra != null && "true".equals(stringExtra)) {
            SmsApplication.getInstance().getmNotificationManager().cancel(R.layout.vote_notification);
            Cursor query = getApplicationContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " _id = ? ", new String[]{"1"}, null);
            while (query != null && query.moveToNext()) {
                this.mVoteContactName = query.getString(query.getColumnIndex("contact_name"));
                this.contactNames = getSendContactName(this.mVoteContactName);
            }
        }
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mListView = (ListView) findViewById(R.id.vote_option_listview);
        this.mVoteInfoView = (TextView) findViewById(R.id.vote_info);
        this.mVoteTitleView = (TextView) findViewById(R.id.vote_title);
        this.mVoteTitleView.setText(this.mVoteTitle);
        if (this.mVoteContactName != null) {
            this.contactNames = getSendContactName(this.mVoteContactName);
            this.mVoteInfoView.setText(String.format(getString(R.string.vote_info), Integer.valueOf(this.contactNames.size())));
        } else {
            this.mVoteInfoView.setText(String.format(getString(R.string.vote_info), Integer.valueOf(this.mVoteCounter)));
        }
        this.mVoteInfoView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VoteDetailActivity.this.createDialog();
            }
        });
        this.mVoteAdapter = new VoteCursorAdapter(getApplicationContext(), null);
        this.mListView.setAdapter((ListAdapter) this.mVoteAdapter);
        this.mVoteOptionAsynQueryHandler = new VoteOptionAsynQueryHandler(getContentResolver());
        this.mVoteOptionAsynQueryHandler.startQuery(0, null, VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null, " vote_id = ? ", new String[]{String.valueOf(this.mVoteId)}, null);
    }
}
