package vc.lx.sms.richtext.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public class VoteListActivity extends AbstractFlurryActivity {
    private static final int THREAD_LIST_QUERY_TOKEN = 0;
    /* access modifiers changed from: private */
    public LayoutInflater inflater = null;
    /* access modifiers changed from: private */
    public List<VoteItem> mData = new ArrayList();
    private ListView mListView = null;
    /* access modifiers changed from: private */
    public VoteCursorAdapter mVoteAdapter = null;
    private VoteAsynQueryHandler mVoteAsynQueryHandler = null;
    public AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            Intent intent = new Intent(VoteListActivity.this.getApplicationContext(), VoteDetailActivity.class);
            intent.putExtra(PrefsUtil.KEY_VOTE_ID, ((VoteItem) VoteListActivity.this.mData.get(i)).cli_id);
            intent.putExtra(PrefsUtil.KEY_VOTE_COUNTER, ((VoteItem) VoteListActivity.this.mData.get(i)).counter);
            intent.putExtra(PrefsUtil.KEY_VOTE_CONTACT_NAME, ((VoteItem) VoteListActivity.this.mData.get(i)).contact_name);
            intent.putExtra(PrefsUtil.KEY_VOTE_TITLE, ((VoteItem) VoteListActivity.this.mData.get(i)).title);
            VoteListActivity.this.startActivity(intent);
            ContentValues contentValues = new ContentValues();
            contentValues.put("is_new", (Integer) 1);
            VoteListActivity.this.getContentResolver().update(ContentUris.withAppendedId(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, Long.valueOf(((VoteItem) VoteListActivity.this.mData.get(i)).service_id).longValue()), contentValues, null, null);
        }
    };

    class VoteAsynQueryHandler extends AsyncQueryHandler {
        public VoteAsynQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case 0:
                    if (cursor != null) {
                        VoteListActivity.this.mVoteAdapter.changeCursor(cursor);
                        VoteListActivity.this.mVoteAdapter.notifyDataSetInvalidated();
                        if (cursor.getCount() == 0) {
                            Toast.makeText(VoteListActivity.this.getApplicationContext(), VoteListActivity.this.getString(R.string.no_save_any_vote), 0).show();
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    class VoteCursorAdapter extends CursorAdapter {
        public VoteCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            LayoutInflater unused = VoteListActivity.this.inflater = LayoutInflater.from(context);
        }

        public void bindView(View view, Context context, Cursor cursor) {
            VoteItem voteItem = new VoteItem();
            String string = cursor.getString(cursor.getColumnIndex("title"));
            String string2 = cursor.getString(cursor.getColumnIndex("counter"));
            voteItem.title = string;
            if (string2 == null) {
                string2 = "0";
            }
            voteItem.counter = Integer.parseInt(string2);
            voteItem.service_id = cursor.getString(cursor.getColumnIndex("service_id"));
            voteItem.contact_name = cursor.getString(cursor.getColumnIndex("contact_name"));
            voteItem.cli_id = Long.valueOf(cursor.getString(cursor.getColumnIndex("_id"))).longValue();
            ImageView imageView = (ImageView) view.findViewById(R.id.is_new);
            ((ImageView) view.findViewById(R.id.favorite_icon)).setBackgroundResource(R.drawable.symbol_favorite_list_voting);
            if (cursor.getInt(cursor.getColumnIndex("is_new")) == 0) {
                imageView.setVisibility(0);
            } else {
                imageView.setVisibility(8);
            }
            ((TextView) view.findViewById(R.id.vote_title)).setText(string);
            ((TextView) view.findViewById(R.id.vote_count)).setText(voteItem.counter + VoteListActivity.this.getString(R.string.favorite_vote_attend));
            VoteListActivity.this.mData.add(voteItem);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return VoteListActivity.this.inflater.inflate((int) R.layout.vote_list_item, viewGroup, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.vote_list);
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mListView = (ListView) findViewById(R.id.vote_listview);
        this.mVoteAdapter = new VoteCursorAdapter(getApplicationContext(), null);
        this.mListView.setAdapter((ListAdapter) this.mVoteAdapter);
        this.mVoteAsynQueryHandler = new VoteAsynQueryHandler(getContentResolver());
        this.mVoteAsynQueryHandler.startQuery(0, null, VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " tracer_flag =  ? ", new String[]{"1"}, null);
        this.mListView.setOnItemClickListener(this.onItemClickListener);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mVoteAsynQueryHandler != null) {
            this.mVoteAsynQueryHandler.startQuery(0, null, VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " tracer_flag =  ? ", new String[]{"1"}, null);
        }
    }
}
