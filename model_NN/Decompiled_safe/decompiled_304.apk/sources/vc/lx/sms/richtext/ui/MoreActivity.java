package vc.lx.sms.richtext.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import vc.lx.sms.service.ApkUpdateService;
import vc.lx.sms.ui.AboutActivity;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.ContactsSelectActivity;
import vc.lx.sms.ui.MessagingPreferenceActivity;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class MoreActivity extends AbstractFlurryActivity {
    protected static final int PROGRESS_DIALOG = 1;
    /* access modifiers changed from: private */
    public List<MoreItem> data = null;
    /* access modifiers changed from: private */
    public LayoutInflater inflater = null;
    private MoreAdapter mFavoriteAdapter = null;
    private ListView mListView = null;
    protected BroadcastReceiver mNewVersionReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            final String stringExtra = intent.getStringExtra(PrefsUtil.KEY_FILE_PATH);
            final String stringExtra2 = intent.getStringExtra(PrefsUtil.KEY_FORCE);
            if (String.valueOf(Util.getVersionName(MoreActivity.this.getApplicationContext())).compareTo(intent.getStringExtra(PrefsUtil.KEY_VERSION_CODE)) < 0) {
                if (stringExtra != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MoreActivity.this);
                    builder.setTitle((int) R.string.new_version_title);
                    builder.setMessage((int) R.string.new_version_message);
                    builder.setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                MoreActivity.this.dismissDialog(1);
                            } catch (Exception e) {
                            }
                            Intent intent = new Intent(MoreActivity.this.getApplicationContext(), ApkUpdateService.class);
                            intent.setAction(PrefsUtil.KEY_UPDATE_APK);
                            intent.putExtra(PrefsUtil.KEY_FILE_PATH, stringExtra);
                            MoreActivity.this.startService(intent);
                        }
                    });
                    builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                MoreActivity.this.dismissDialog(1);
                            } catch (Exception e) {
                            }
                            if ("1".equals(stringExtra2)) {
                                MoreActivity.this.finish();
                            }
                            dialogInterface.dismiss();
                            if (MoreActivity.this.progressDialog != null) {
                                MoreActivity.this.progressDialog.dismiss();
                            }
                        }
                    });
                    builder.create().show();
                }
            } else if (MoreActivity.this.progressDialog != null && MoreActivity.this.progressDialog.isShowing()) {
                MoreActivity.this.progressDialog.dismiss();
                AlertDialog.Builder builder2 = new AlertDialog.Builder(MoreActivity.this);
                builder2.setTitle((int) R.string.new_version_title);
                builder2.setMessage((int) R.string.no_version_message);
                builder2.setPositiveButton((int) R.string.confirm, (DialogInterface.OnClickListener) null);
                builder2.create().show();
            }
        }
    };
    protected BroadcastReceiver mUpdateApkReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String stringExtra = intent.getStringExtra(PrefsUtil.KEY_FILE_PATH);
            if (stringExtra != null) {
                Util.logEvent(PrefsUtil.EVENT_APK_UPDATE, new NameValuePair[0]);
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.setDataAndType(Uri.fromFile(new File(stringExtra)), "application/vnd.android.package-archive");
                MoreActivity.this.startActivity(intent2);
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;

    class MoreAdapter extends BaseAdapter {
        MoreAdapter() {
        }

        public int getCount() {
            return MoreActivity.this.data.size();
        }

        public Object getItem(int i) {
            return MoreActivity.this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(final int i, View view, ViewGroup viewGroup) {
            MoreItemView moreItemView;
            View view2;
            if (view == null) {
                moreItemView = new MoreItemView();
                view2 = MoreActivity.this.inflater.inflate((int) R.layout.more_list_item, (ViewGroup) null);
                moreItemView.mTitleView = (TextView) view2.findViewById(R.id.content);
                moreItemView.mImageView = (ImageView) view2.findViewById(R.id.more_icon);
                view2.setTag(moreItemView);
            } else {
                moreItemView = (MoreItemView) view.getTag();
                view2 = view;
            }
            moreItemView.mTitleView.setText(((MoreItem) MoreActivity.this.data.get(i)).name);
            moreItemView.mImageView.setImageResource(((MoreItem) MoreActivity.this.data.get(i)).image);
            if (MoreActivity.this.getString(R.string.feek_back).equals(((MoreItem) MoreActivity.this.data.get(i)).name)) {
                view2.findViewById(R.id.plus_icon).setVisibility(0);
            } else {
                view2.findViewById(R.id.plus_icon).setVisibility(8);
            }
            view2.setClickable(true);
            view2.setFocusable(true);
            view2.setPressed(true);
            view2.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                public void onClick(View view) {
                    switch (i) {
                        case 0:
                            Intent intent = new Intent(MoreActivity.this.getApplicationContext(), ContactsSelectActivity.class);
                            intent.putExtra(PrefsUtil.KEY_INVITE_BY_SMS, true);
                            MoreActivity.this.startActivity(intent);
                            return;
                        case 1:
                            MoreActivity.this.startActivity(new Intent(MoreActivity.this.getApplicationContext(), MessagingPreferenceActivity.class));
                            return;
                        case 2:
                            MoreActivity.this.startActivity(new Intent(MoreActivity.this.getApplicationContext(), FeedBackActivity.class));
                            return;
                        case 3:
                            MoreActivity.this.showDialog(1);
                            MoreActivity.this.startUpdateVersionService();
                            return;
                        case 4:
                            MoreActivity.this.startActivity(new Intent(MoreActivity.this.getApplicationContext(), AboutActivity.class));
                            return;
                        default:
                            return;
                    }
                }
            });
            return view2;
        }
    }

    class MoreItem {
        int image;
        String name;

        MoreItem() {
        }
    }

    class MoreItemView {
        public ImageView mImageView;
        public TextView mTitleView;

        MoreItemView() {
        }
    }

    /* access modifiers changed from: private */
    public void startUpdateVersionService() {
        Intent intent = new Intent(getApplicationContext(), ApkUpdateService.class);
        intent.setAction(PrefsUtil.KEY_LATEST_VERSION);
        startService(intent);
    }

    public List<MoreItem> getData() {
        ArrayList arrayList = new ArrayList();
        MoreItem moreItem = new MoreItem();
        moreItem.name = getString(R.string.invite_friend);
        moreItem.image = R.drawable.symbol_invitation;
        arrayList.add(moreItem);
        MoreItem moreItem2 = new MoreItem();
        moreItem2.name = getString(R.string.sms_setting);
        moreItem2.image = R.drawable.symbol_settings;
        arrayList.add(moreItem2);
        MoreItem moreItem3 = new MoreItem();
        moreItem3.name = getString(R.string.feek_back);
        moreItem3.image = R.drawable.symbol_feedback;
        arrayList.add(moreItem3);
        MoreItem moreItem4 = new MoreItem();
        moreItem4.name = getString(R.string.software_update);
        moreItem4.image = R.drawable.symbol_update;
        arrayList.add(moreItem4);
        MoreItem moreItem5 = new MoreItem();
        moreItem5.name = getString(R.string.sms_about);
        moreItem5.image = R.drawable.symbol_about;
        arrayList.add(moreItem5);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.more);
        this.data = getData();
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        this.mListView = (ListView) findViewById(R.id.moreListView);
        this.mFavoriteAdapter = new MoreAdapter();
        this.mListView.setAdapter((ListAdapter) this.mFavoriteAdapter);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                this.progressDialog = new ProgressDialog(this);
                this.progressDialog.setProgressStyle(0);
                this.progressDialog.setMessage(getString(R.string.loading_update));
                return this.progressDialog;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            if (isFinishing()) {
                unregisterReceiver(this.mUpdateApkReceiver);
                unregisterReceiver(this.mNewVersionReceiver);
            }
        } catch (IllegalStateException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        registerReceiver(this.mUpdateApkReceiver, new IntentFilter(PrefsUtil.INTENT_UPDATE_APK));
        registerReceiver(this.mNewVersionReceiver, new IntentFilter(PrefsUtil.INTENT_NEW_VERSION));
    }
}
