package vc.lx.sms.richtext.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import vc.lx.sms.intents.SsmIntents;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms2.R;

public class InviteFriendActivity extends AbstractFlurryActivity {
    public static final boolean DEBUG = true;
    public static final String TAG = "InviteSettingActivity";

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            Toast.makeText(getApplicationContext(), "HAVE SENDED", 1).show();
        } else {
            if (i2 == 0) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.d(TAG, "onCreate()");
        requestWindowFeature(1);
        setContentView((int) R.layout.invite_firend);
        ListView listView = (ListView) findViewById(R.id.invite_friend_list_view);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 2; i++) {
            HashMap hashMap = new HashMap();
            if (i == 0) {
                hashMap.put("ItemTitle", getResources().getString(R.string.invite_contact));
            } else {
                hashMap.put("ItemTitle", getResources().getString(R.string.invite_mailbox_friend));
            }
            hashMap.put("ItemImage", Integer.valueOf((int) R.drawable.arrow));
            arrayList.add(hashMap);
        }
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.invite_friend_item, new String[]{"ItemTitle", "ItemImage"}, new int[]{R.id.settingitem, R.id.settingitemImage}));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                TextView textView = (TextView) ((LinearLayout) view).getChildAt(0);
                if (textView.getText().equals(InviteFriendActivity.this.getApplicationContext().getResources().getString(R.string.invite_contact))) {
                    InviteFriendActivity.this.openSMS();
                } else if (textView.getText().equals(InviteFriendActivity.this.getApplicationContext().getResources().getString(R.string.invite_mailbox_friend))) {
                    InviteFriendActivity.this.openMailInface();
                }
            }
        });
    }

    public void openAlertDialog() {
        AlertDialog create = new AlertDialog.Builder(this).create();
        create.setMessage(getResources().getString(R.string.choose_mailbox));
        create.setButton(getResources().getString(R.string.viaEmail), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        create.setButton(getResources().getString(R.string.via) + "XX", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        create.show();
    }

    public void openMailInface() {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", getResources().getString(R.string.invite_content_SMS));
        intent.putExtra("android.intent.extra.SUBJECT", getResources().getString(R.string.invite_content_SMS));
        intent.setType("message/rfc822");
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.choose_mailbox)), 0);
    }

    public void openSMS() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.putExtra("sms_body", getResources().getString(R.string.invite_content_SMS));
        intent.setType(SsmIntents.SMS_MIME_TYPE);
        startActivityForResult(intent, 0);
    }
}
