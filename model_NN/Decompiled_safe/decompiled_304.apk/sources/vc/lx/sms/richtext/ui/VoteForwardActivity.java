package vc.lx.sms.richtext.ui;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.mms.transaction.MessagingNotification;
import com.android.mms.transaction.SmsMessageSender;
import com.android.mms.ui.RecipientsEditor;
import com.android.provider.Telephony;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.ContactsSelectActivity;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Recycler;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class VoteForwardActivity extends AbstractFlurryActivity implements View.OnClickListener {
    public static final int REQUEST_CODE_CONTACTS_SELECT = 1;
    public static HashMap<String, String> cacheNameNum = new HashMap<>();
    String forwardBody = "";
    private Button mAddContactButton;
    LinearLayout mContent;
    Conversation mConversation;
    ImageView mDelete;
    LayoutInflater mInflater;
    private RecipientsEditor mRecipientsEditor;
    ImageView mSend;
    private EditText mSimpleText;
    VoteItem mVoteItem;
    String plug_id;
    AlertDialog.Builder waitingDialog;

    public static Intent createIntent(Context context) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setComponent(new ComponentName(context, VoteForwardActivity.class));
        return intent;
    }

    private void showDraftDiscardDialog() {
        new AlertDialog.Builder(this).setTitle((int) R.string.discard_message).setMessage((int) R.string.discard_message_reason).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                VoteForwardActivity.this.finish();
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    }

    private void showDraftSaveDialog(VoteItem voteItem, final Conversation conversation) {
        new AlertDialog.Builder(this).setMessage((int) R.string.save_draft_or_not).setPositiveButton((int) R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                VoteForwardActivity.this.updateDraftSmsMessage(conversation, VoteForwardActivity.this.mVoteItem.plug);
                Toast.makeText(VoteForwardActivity.this, (int) R.string.message_saved_as_draft, 0).show();
                VoteForwardActivity.this.finish();
            }
        }).setNegativeButton((int) R.string.discard_message, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                VoteForwardActivity.this.finish();
            }
        }).show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void updateDraftSmsMessage(long j, String str) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("updateDraftSmsMessage tid=%d, contents=\"%s\"", Long.valueOf(j), str);
        }
        if (j > 0) {
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("thread_id", Long.valueOf(j));
            contentValues.put(Telephony.TextBasedSmsColumns.BODY, str);
            contentValues.put("type", (Integer) 3);
            SqliteWrapper.insert(this, getContentResolver(), Telephony.Sms.CONTENT_URI, contentValues);
        }
    }

    /* access modifiers changed from: private */
    public void updateDraftSmsMessage(Conversation conversation, String str) {
        long ensureThreadId = conversation.ensureThreadId();
        conversation.setDraftState(true);
        updateDraftSmsMessage(ensureThreadId, str);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1) {
            switch (i) {
                case 1:
                    String[] stringArrayExtra = intent.getStringArrayExtra("selected_names");
                    StringBuffer stringBuffer = new StringBuffer();
                    if (stringArrayExtra != null && stringArrayExtra.length > 0) {
                        for (int i3 = 0; i3 < stringArrayExtra.length; i3++) {
                            String obj = this.mRecipientsEditor.getText().toString();
                            String str = stringArrayExtra[i3];
                            if (str.contains("<") && str.contains(">")) {
                                str = str.substring(str.indexOf("<") + 1, str.indexOf(">"));
                            }
                            if (!obj.contains(str)) {
                                if (!"".equals(obj.trim()) && !obj.trim().endsWith(",")) {
                                    stringBuffer.append(",");
                                }
                                stringBuffer.append(stringArrayExtra[i3]).append(",");
                                cacheNameNum.put(str, stringArrayExtra[i3].substring(0, stringArrayExtra[i3].indexOf("<")));
                            }
                        }
                    }
                    this.mRecipientsEditor.append(stringBuffer.toString());
                    Log.i("TAG", stringBuffer.toString());
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.richtext.ui.VoteForwardActivity, vc.lx.sms.data.ContactList, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.util.List<java.lang.String>, int, int, vc.lx.sms.richtext.ui.VoteForwardActivity]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    public void onClick(View view) {
        if (view == this.mAddContactButton) {
            startActivityForResult(new Intent(this, ContactsSelectActivity.class), 1);
        } else if (view == this.mSend) {
            if (this.mRecipientsEditor.getNumbers().size() != 0) {
                Util.logEvent("quare_vote_forward", new NameValuePair[0]);
                if (!Util.checkNetWork(getApplicationContext()) || this.mVoteItem == null) {
                    Conversation conversation = Conversation.get((Context) this, ContactList.getByNumbers(this.mRecipientsEditor.getNumbers(), false, true, (Context) this), false);
                    String obj = (this.mSimpleText == null || this.mSimpleText.getText().equals("")) ? "" : this.mSimpleText.getText().toString();
                    if (this.mVoteItem != null && !this.mVoteItem.title.equals("")) {
                        obj = obj + this.mVoteItem.title;
                        for (int i = 0; i < this.mVoteItem.options.size(); i++) {
                            obj = obj + "\n" + this.mVoteItem.options.get(i).display_order + "." + this.mVoteItem.options.get(i).content;
                        }
                    }
                    sendSmsWorker(conversation, obj);
                    finish();
                    return;
                }
                this.waitingDialog.show();
                if (this.mVoteItem.service_id != null) {
                    if (getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " service_id = ? and tracer_flag = ? ", new String[]{this.mVoteItem.service_id, "1"}, null).getCount() == 0) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("tracer_flag", "1");
                        getApplicationContext().getContentResolver().update(Uri.parse(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI + "/cli_id/" + this.mVoteItem.cli_id), contentValues, null, null);
                    }
                }
                if (this.mVoteItem != null) {
                    JSONObject jSONObject = new JSONObject();
                    JSONArray jSONArray = new JSONArray();
                    List<String> numbers = this.mRecipientsEditor.getNumbers();
                    if (numbers != null && numbers.size() > 0) {
                        int i2 = 0;
                        while (i2 < numbers.size()) {
                            try {
                                JSONObject jSONObject2 = new JSONObject();
                                jSONObject2.put("name", numbers.get(i2));
                                jSONObject2.put("number", numbers.get(i2));
                                jSONArray.put(jSONObject2);
                                i2++;
                            } catch (JSONException e) {
                                e.printStackTrace();
                                return;
                            }
                        }
                        jSONObject.put("friends", jSONArray);
                    }
                }
            }
        } else if (view == this.mDelete) {
            this.mVoteItem = null;
            this.mDelete.setVisibility(8);
            ((LinearLayout) findViewById(R.id.title_field)).setVisibility(8);
            this.mContent.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.richtext.ui.VoteForwardActivity, long, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.richtext.ui.VoteForwardActivity, vc.lx.sms.data.ContactList, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.lang.String, int, int, vc.lx.sms.richtext.ui.VoteForwardActivity]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.vote_forward_activity);
        this.plug_id = getIntent().getStringExtra("plug_id");
        this.forwardBody = getIntent().getStringExtra("forwar_body");
        this.mContent = (LinearLayout) findViewById(R.id.vote_content);
        this.mSimpleText = (EditText) findViewById(R.id.embedded_text_editor);
        this.mSimpleText.setText(this.forwardBody);
        this.mSend = (ImageView) findViewById(R.id.send);
        this.mDelete = (ImageView) findViewById(R.id.vote_delete);
        this.waitingDialog = new AlertDialog.Builder(this);
        this.waitingDialog.setMessage(getString(R.string.waiting_for_send));
        Long valueOf = Long.valueOf(getIntent().getLongExtra("thread_id", 0));
        if (valueOf.longValue() > 0) {
            this.mConversation = Conversation.get((Context) this, valueOf.longValue(), false);
        } else {
            String stringExtra = getIntent().getStringExtra("address");
            if (!TextUtils.isEmpty(stringExtra)) {
                this.mConversation = Conversation.get((Context) this, ContactList.getByNumbers(stringExtra, false, true, (Context) this), false);
            }
        }
        this.mInflater = (LayoutInflater) getSystemService("layout_inflater");
        ViewStub viewStub = (ViewStub) findViewById(R.id.recipients_editor_stub);
        if (viewStub != null) {
            View inflate = viewStub.inflate();
            this.mRecipientsEditor = (RecipientsEditor) inflate.findViewById(R.id.recipients_editor);
            this.mAddContactButton = (Button) inflate.findViewById(R.id.add_contacts_button);
        } else {
            this.mRecipientsEditor = (RecipientsEditor) findViewById(R.id.recipients_editor);
            this.mAddContactButton = (Button) findViewById(R.id.add_contacts_button);
            this.mRecipientsEditor.setVisibility(0);
        }
        if (this.mConversation != null) {
            String nameAndNumber = ((Contact) this.mConversation.getRecipients().get(0)).getNameAndNumber();
            for (int i = 1; i < this.mConversation.getRecipients().size(); i++) {
                nameAndNumber = TextUtils.join(",", new String[]{nameAndNumber, ((Contact) this.mConversation.getRecipients().get(i)).getNameAndNumber()});
            }
            this.mRecipientsEditor.setText(TextUtils.join(",", new String[]{nameAndNumber, ""}));
        }
        this.mAddContactButton.setOnClickListener(this);
        this.mRecipientsEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (!z) {
                    RecipientsEditor recipientsEditor = (RecipientsEditor) view;
                }
            }
        });
        String fullShortenVoteUrl = Util.getFullShortenVoteUrl(this, this.plug_id);
        Cursor query = getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " plug = ? ", new String[]{fullShortenVoteUrl}, null);
        if (query.getCount() > 0) {
            this.mVoteItem = new VoteItem();
            if (query.moveToNext()) {
                if (query.getInt(query.getColumnIndex("parent_id")) == 0) {
                    this.mVoteItem.cli_id = (long) query.getInt(query.getColumnIndex("_id"));
                    this.mVoteItem.title = query.getString(query.getColumnIndex("title"));
                    this.mVoteItem.plug = fullShortenVoteUrl;
                    this.mVoteItem.service_id = query.getString(query.getColumnIndex("service_id"));
                } else {
                    this.mVoteItem.cli_id = (long) query.getInt(query.getColumnIndex("parent_id"));
                    this.mVoteItem.service_id = query.getString(query.getColumnIndex("service_id"));
                    this.mVoteItem.title = query.getString(query.getColumnIndex("title"));
                    this.mVoteItem.plug = fullShortenVoteUrl;
                }
            }
            query.close();
            Cursor query2 = getContentResolver().query(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null, " vote_id = ? ", new String[]{String.valueOf(this.mVoteItem.cli_id)}, " display_order asc ");
            while (query2.moveToNext()) {
                VoteOption voteOption = new VoteOption();
                voteOption.display_order = query2.getInt(query2.getColumnIndex("display_order"));
                voteOption.cli_id = (long) query2.getInt(query2.getColumnIndex("_id"));
                voteOption.service_id = query2.getString(query2.getColumnIndex("service_id"));
                voteOption.content = query2.getString(query2.getColumnIndex("option_text"));
                this.mVoteItem.options.add(voteOption);
            }
            query2.close();
            TextView textView = (TextView) findViewById(R.id.vote_title);
            textView.setText(getString(R.string.theme) + this.mVoteItem.title);
            textView.setTextSize(16.0f);
            for (int i2 = 0; i2 < this.mVoteItem.options.size(); i2++) {
                LinearLayout linearLayout = new LinearLayout(this);
                linearLayout.setBackgroundResource(R.drawable.bg_vote_item);
                linearLayout.setGravity(16);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                layoutParams.setMargins(0, 10, 0, 0);
                linearLayout.setLayoutParams(layoutParams);
                TextView textView2 = new TextView(getApplicationContext());
                textView2.setText(this.mVoteItem.options.get(i2).display_order + "." + this.mVoteItem.options.get(i2).content);
                textView2.setGravity(16);
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams2.setMargins(10, 0, 0, 0);
                textView2.setLayoutParams(layoutParams2);
                linearLayout.addView(textView2);
                this.mContent.addView(linearLayout);
            }
            this.mSend.setOnClickListener(this);
            this.mDelete.setOnClickListener(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.richtext.ui.VoteForwardActivity, vc.lx.sms.data.ContactList, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.util.List<java.lang.String>, int, int, vc.lx.sms.richtext.ui.VoteForwardActivity]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                if (this.mVoteItem != null) {
                    if (this.mRecipientsEditor.getNumbers().size() < 1) {
                        showDraftDiscardDialog();
                    } else {
                        List<String> numbers = this.mRecipientsEditor.getNumbers();
                        if (numbers != null && numbers.size() > 0) {
                            showDraftSaveDialog(this.mVoteItem, (numbers == null || numbers.size() <= 0) ? Conversation.createNew(this) : Conversation.get((Context) this, ContactList.getByNumbers(numbers, false, true, (Context) this), false));
                        }
                    }
                    return true;
                }
                break;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void quickSendSms(String str, String str2) {
        if (!str2.equals("") && str2.length() > 0) {
            MessagingNotification.updateNewMessageIndicator(this, false);
            Util.sendSMS(str, str2, this);
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_QUICK_REPLY, new NameValuePair[0]);
            finish();
        }
    }

    public void sendSmsWorker(Conversation conversation, String str) {
        long ensureThreadId = conversation.ensureThreadId();
        try {
            new SmsMessageSender(this, conversation.getRecipients().getNumbers(), str, ensureThreadId).sendMessage(ensureThreadId);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(this, ensureThreadId);
        } catch (Exception e) {
            Log.e(Telephony.ThreadsColumns.ERROR, "Failed to send SMS message, threadId=" + ensureThreadId, e);
        }
    }

    public void sendSmsWorker(Conversation conversation, ArrayList<String> arrayList) {
        long ensureThreadId = conversation.ensureThreadId();
        try {
            new SmsMessageSender(this, conversation.getRecipients().getNumbers(), ensureThreadId).sendMessage(ensureThreadId, arrayList);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(this, ensureThreadId);
        } catch (Exception e) {
            Log.e(Telephony.ThreadsColumns.ERROR, "Failed to send SMS message, threadId=" + ensureThreadId, e);
        }
    }
}
