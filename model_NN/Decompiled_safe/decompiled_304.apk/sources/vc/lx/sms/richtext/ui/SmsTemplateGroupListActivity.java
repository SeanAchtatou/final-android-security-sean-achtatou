package vc.lx.sms.richtext.ui;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import vc.lx.sms.template.ui.SmsTemplateListActivity;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public class SmsTemplateGroupListActivity extends ListActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.sms_template_group_list);
        ArrayList arrayList = new ArrayList();
        String[] stringArray = getResources().getStringArray(R.array.default_bottom_sms_tab);
        HashMap hashMap = new HashMap();
        hashMap.put("name", stringArray[0]);
        hashMap.put("icon", Integer.valueOf((int) R.drawable.icon_bless_normal));
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("name", stringArray[1]);
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.icon_festival_normal));
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("name", stringArray[2]);
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.icon_function_normal));
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("name", stringArray[3]);
        hashMap4.put("icon", Integer.valueOf((int) R.drawable.icon_favorite_normal));
        arrayList.add(hashMap4);
        getListView().setAdapter((ListAdapter) new SimpleAdapter(getApplicationContext(), arrayList, R.layout.sms_template_group_list_item, new String[]{"name", "icon"}, new int[]{R.id.name, R.id.icon}));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        Intent intent = new Intent(getApplicationContext(), SmsTemplateListActivity.class);
        intent.putExtra(PrefsUtil.KEY_TEMPLATE_FROM, "square");
        intent.putExtra(PrefsUtil.KEY_TEMPLATE_CATEGORY_ID, i);
        intent.putExtra(PrefsUtil.KEY_TEMPLATE_CATEGORY_NAME, getApplicationContext().getResources().getStringArray(R.array.default_bottom_sms_tab)[i]);
        startActivity(intent);
    }
}
