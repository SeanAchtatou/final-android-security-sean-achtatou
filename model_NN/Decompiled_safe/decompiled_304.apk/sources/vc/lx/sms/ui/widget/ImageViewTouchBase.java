package vc.lx.sms.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ImageViewTouchBase extends ImageView {
    static final float SCALE_RATE = 1.25f;
    private static final String TAG = "ImageViewTouchBase";
    protected Matrix mBaseMatrix = new Matrix();
    protected final RotateBitmap mBitmapDisplayed = new RotateBitmap(null);
    private final Matrix mDisplayMatrix = new Matrix();
    protected Handler mHandler = new Handler();
    protected int mLastXTouchPos;
    protected int mLastYTouchPos;
    private final float[] mMatrixValues = new float[9];
    float mMaxZoom;
    private Runnable mOnLayoutRunnable = null;
    private Recycler mRecycler;
    protected Matrix mSuppMatrix = new Matrix();
    int mThisHeight = -1;
    int mThisWidth = -1;

    public interface Recycler {
        void recycle(Bitmap bitmap);
    }

    public ImageViewTouchBase(Context context) {
        super(context);
        init();
    }

    public ImageViewTouchBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void getProperBaseMatrix(RotateBitmap rotateBitmap, Matrix matrix) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float width2 = (float) rotateBitmap.getWidth();
        float height2 = (float) rotateBitmap.getHeight();
        matrix.reset();
        float min = Math.min(Math.min(width / width2, 3.0f), Math.min(height / height2, 3.0f));
        matrix.postConcat(rotateBitmap.getRotateMatrix());
        matrix.postScale(min, min);
        matrix.postTranslate((width - (width2 * min)) / 2.0f, (height - (height2 * min)) / 2.0f);
    }

    private void init() {
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    private void setImageBitmap(Bitmap bitmap, int i) {
        super.setImageBitmap(bitmap);
        Drawable drawable = getDrawable();
        if (drawable != null) {
            drawable.setDither(true);
        }
        Bitmap bitmap2 = this.mBitmapDisplayed.getBitmap();
        this.mBitmapDisplayed.setBitmap(bitmap);
        this.mBitmapDisplayed.setRotation(i);
        if (bitmap2 != null && bitmap2 != bitmap && this.mRecycler != null) {
            this.mRecycler.recycle(bitmap2);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void center(boolean r8, boolean r9) {
        /*
            r7 = this;
            r6 = 1073741824(0x40000000, float:2.0)
            r5 = 0
            vc.lx.sms.ui.widget.RotateBitmap r0 = r7.mBitmapDisplayed
            android.graphics.Bitmap r0 = r0.getBitmap()
            if (r0 != 0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            android.graphics.Matrix r0 = r7.getImageViewMatrix()
            android.graphics.RectF r1 = new android.graphics.RectF
            vc.lx.sms.ui.widget.RotateBitmap r2 = r7.mBitmapDisplayed
            android.graphics.Bitmap r2 = r2.getBitmap()
            int r2 = r2.getWidth()
            float r2 = (float) r2
            vc.lx.sms.ui.widget.RotateBitmap r3 = r7.mBitmapDisplayed
            android.graphics.Bitmap r3 = r3.getBitmap()
            int r3 = r3.getHeight()
            float r3 = (float) r3
            r1.<init>(r5, r5, r2, r3)
            r0.mapRect(r1)
            float r0 = r1.height()
            float r2 = r1.width()
            if (r9 == 0) goto L_0x0099
            int r3 = r7.getHeight()
            float r4 = (float) r3
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x0066
            float r3 = (float) r3
            float r0 = r3 - r0
            float r0 = r0 / r6
            float r3 = r1.top
            float r0 = r0 - r3
        L_0x0048:
            if (r8 == 0) goto L_0x0097
            int r3 = r7.getWidth()
            float r4 = (float) r3
            int r4 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x0080
            float r3 = (float) r3
            float r2 = r3 - r2
            float r2 = r2 / r6
            float r1 = r1.left
            float r1 = r2 - r1
        L_0x005b:
            r7.postTranslate(r1, r0)
            android.graphics.Matrix r0 = r7.getImageViewMatrix()
            r7.setImageMatrix(r0)
            goto L_0x000b
        L_0x0066:
            float r0 = r1.top
            int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x0070
            float r0 = r1.top
            float r0 = -r0
            goto L_0x0048
        L_0x0070:
            float r0 = r1.bottom
            float r3 = (float) r3
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 >= 0) goto L_0x0099
            int r0 = r7.getHeight()
            float r0 = (float) r0
            float r3 = r1.bottom
            float r0 = r0 - r3
            goto L_0x0048
        L_0x0080:
            float r2 = r1.left
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 <= 0) goto L_0x008a
            float r1 = r1.left
            float r1 = -r1
            goto L_0x005b
        L_0x008a:
            float r2 = r1.right
            float r4 = (float) r3
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x0097
            float r2 = (float) r3
            float r1 = r1.right
            float r1 = r2 - r1
            goto L_0x005b
        L_0x0097:
            r1 = r5
            goto L_0x005b
        L_0x0099:
            r0 = r5
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.widget.ImageViewTouchBase.center(boolean, boolean):void");
    }

    public void clear() {
        setImageBitmapResetBase(null, true);
    }

    /* access modifiers changed from: protected */
    public Matrix getImageViewMatrix() {
        this.mDisplayMatrix.set(this.mBaseMatrix);
        this.mDisplayMatrix.postConcat(this.mSuppMatrix);
        return this.mDisplayMatrix;
    }

    public float getScale() {
        return getScale(this.mSuppMatrix);
    }

    public float getScale(Matrix matrix) {
        return getValue(matrix, 0);
    }

    /* access modifiers changed from: protected */
    public float getValue(Matrix matrix, int i) {
        matrix.getValues(this.mMatrixValues);
        return this.mMatrixValues[i];
    }

    /* access modifiers changed from: protected */
    public float maxZoom() {
        if (this.mBitmapDisplayed.getBitmap() == null) {
            return 1.0f;
        }
        return Math.max(((float) this.mBitmapDisplayed.getWidth()) / ((float) this.mThisWidth), ((float) this.mBitmapDisplayed.getHeight()) / ((float) this.mThisHeight)) * 4.0f;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.mThisWidth = i3 - i;
        this.mThisHeight = i4 - i2;
        Runnable runnable = this.mOnLayoutRunnable;
        if (runnable != null) {
            this.mOnLayoutRunnable = null;
            runnable.run();
        }
        if (this.mBitmapDisplayed.getBitmap() != null) {
            getProperBaseMatrix(this.mBitmapDisplayed, this.mBaseMatrix);
            setImageMatrix(getImageViewMatrix());
        }
    }

    /* access modifiers changed from: protected */
    public void panBy(float f, float f2) {
        postTranslate(f, f2);
        setImageMatrix(getImageViewMatrix());
    }

    /* access modifiers changed from: protected */
    public void postTranslate(float f, float f2) {
        this.mSuppMatrix.postTranslate(f, f2);
    }

    public void postTranslateCenter(float f, float f2) {
        postTranslate(f, f2);
        center(true, true);
    }

    public void setImageBitmap(Bitmap bitmap) {
        setImageBitmap(bitmap, 0);
    }

    public void setImageBitmapResetBase(Bitmap bitmap, boolean z) {
        setImageRotateBitmapResetBase(new RotateBitmap(bitmap), z);
    }

    public void setImageRotateBitmapResetBase(final RotateBitmap rotateBitmap, final boolean z) {
        if (getWidth() <= 0) {
            this.mOnLayoutRunnable = new Runnable() {
                public void run() {
                    ImageViewTouchBase.this.setImageRotateBitmapResetBase(rotateBitmap, z);
                }
            };
            return;
        }
        if (rotateBitmap.getBitmap() != null) {
            getProperBaseMatrix(rotateBitmap, this.mBaseMatrix);
            setImageBitmap(rotateBitmap.getBitmap(), rotateBitmap.getRotation());
        } else {
            this.mBaseMatrix.reset();
            setImageBitmap(null);
        }
        if (z) {
            this.mSuppMatrix.reset();
        }
        setImageMatrix(getImageViewMatrix());
        this.mMaxZoom = maxZoom();
    }

    public void setRecycler(Recycler recycler) {
        this.mRecycler = recycler;
    }

    public void zoomIn() {
        zoomIn(SCALE_RATE);
    }

    /* access modifiers changed from: protected */
    public void zoomIn(float f) {
        if (getScale() < this.mMaxZoom && this.mBitmapDisplayed.getBitmap() != null) {
            this.mSuppMatrix.postScale(f, f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
            setImageMatrix(getImageViewMatrix());
        }
    }

    public void zoomOut() {
        zoomOut(SCALE_RATE);
    }

    /* access modifiers changed from: protected */
    public void zoomOut(float f) {
        if (this.mBitmapDisplayed.getBitmap() != null) {
            float width = ((float) getWidth()) / 2.0f;
            float height = ((float) getHeight()) / 2.0f;
            Matrix matrix = new Matrix(this.mSuppMatrix);
            matrix.postScale(1.0f / f, 1.0f / f, width, height);
            if (getScale(matrix) < 1.0f) {
                this.mSuppMatrix.setScale(1.0f, 1.0f, width, height);
            } else {
                this.mSuppMatrix.postScale(1.0f / f, 1.0f / f, width, height);
            }
            setImageMatrix(getImageViewMatrix());
            center(true, true);
        }
    }

    /* access modifiers changed from: protected */
    public void zoomTo(float f) {
        zoomTo(f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
    }

    /* access modifiers changed from: protected */
    public void zoomTo(float f, float f2, float f3) {
        float scale = (f > this.mMaxZoom ? this.mMaxZoom : f) / getScale();
        this.mSuppMatrix.postScale(scale, scale, f2, f3);
        setImageMatrix(getImageViewMatrix());
        center(true, true);
    }

    /* access modifiers changed from: protected */
    public void zoomTo(float f, float f2, float f3, float f4) {
        final float scale = (f - getScale()) / f4;
        final float scale2 = getScale();
        final long currentTimeMillis = System.currentTimeMillis();
        final float f5 = f4;
        final float f6 = f2;
        final float f7 = f3;
        this.mHandler.post(new Runnable() {
            public void run() {
                float min = Math.min(f5, (float) (System.currentTimeMillis() - currentTimeMillis));
                ImageViewTouchBase.this.zoomTo(scale2 + (scale * min), f6, f7);
                if (min < f5) {
                    ImageViewTouchBase.this.mHandler.post(this);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void zoomToPoint(float f, float f2, float f3) {
        float width = ((float) getWidth()) / 2.0f;
        float height = ((float) getHeight()) / 2.0f;
        panBy(width - f2, height - f3);
        zoomTo(f, width, height);
    }
}
