package vc.lx.sms.ui;

import android.app.ListActivity;
import android.os.Bundle;
import com.flurry.android.FlurryAgent;
import com.nullwire.trace.ExceptionHandler;

public abstract class AbstractFlurryListActivity extends ListActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ExceptionHandler.register(this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "REV1IKNXJJPGQDAAZHNT");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
