package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import com.android.provider.Telephony;
import org.apache.http.NameValuePair;
import vc.lx.sms.intents.MessageWrapper;
import vc.lx.sms.ui.widget.PopSmsItemView;
import vc.lx.sms.ui.widget.SwipePager;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class SmsPopupActivity extends AbstractFlurryActivity {
    static final int COLUMN_ID = 0;
    static final int COLUMN_SMS_ADDRESS = 2;
    static final int COLUMN_SMS_BODY = 3;
    static final int COLUMN_SMS_DATE = 4;
    static final int COLUMN_SMS_READ = 5;
    static final int COLUMN_SMS_STATUS = 7;
    static final int COLUMN_SMS_TYPE = 6;
    static final int COLUMN_THREAD_ID = 1;
    static final String[] PROJECTION = {"_id", "thread_id", "address", Telephony.TextBasedSmsColumns.BODY, "date", "read", "type", "status"};
    public static final String TAG = "SmsPopupActivity";
    static final String UNREAD_CONDITION = "read = 0";
    private static final int UNREAD_SMS_QUERY_TOKEN = 1;
    private MessageWrapper mMessage;
    private AsyncQueryHandler mQueryUnreadSmsHandler;
    /* access modifiers changed from: private */
    public PopSmsItemView mSelectedPopSmsItemView;
    private SwipePager.OnScreenSwitchListener mSwipePagerOnScreenSwitchListener = new SwipePager.OnScreenSwitchListener() {
        public void onScreenSwitched(int i) {
            if (SmsPopupActivity.this.mSelectedPopSmsItemView != null) {
                PopSmsItemView unused = SmsPopupActivity.this.mSelectedPopSmsItemView = (PopSmsItemView) SmsPopupActivity.this.mSwipeSmsGallery.getChildAt(i);
            }
            SmsPopupActivity.this.mSwipeSmsGallery.requestLayout();
        }
    };
    /* access modifiers changed from: private */
    public SwipePager mSwipeSmsGallery;

    private void initViews() {
        this.mSwipeSmsGallery = (SwipePager) findViewById(R.id.swipe_gallery);
        this.mSwipeSmsGallery.setOnScreenSwitchListener(this.mSwipePagerOnScreenSwitchListener);
        this.mQueryUnreadSmsHandler = new AsyncQueryHandler(getContentResolver()) {
            /* access modifiers changed from: protected */
            public void onQueryComplete(int i, Object obj, Cursor cursor) {
                switch (i) {
                    case 1:
                        SmsPopupActivity.this.mSwipeSmsGallery.removeAllViews();
                        while (cursor.moveToNext()) {
                            PopSmsItemView.PopSmsItem popSmsItem = new PopSmsItemView.PopSmsItem();
                            popSmsItem.mAddress = cursor.getString(0);
                            popSmsItem.mBody = cursor.getString(1);
                            popSmsItem.mTimestamp = cursor.getLong(2);
                            PopSmsItemView popSmsItemView = new PopSmsItemView(SmsPopupActivity.this.getApplicationContext());
                            popSmsItemView.bind(popSmsItem, SmsPopupActivity.this);
                            SmsPopupActivity.this.mSwipeSmsGallery.addView(popSmsItemView);
                        }
                        PopSmsItemView unused = SmsPopupActivity.this.mSelectedPopSmsItemView = (PopSmsItemView) SmsPopupActivity.this.mSwipeSmsGallery.getChildAt(0);
                        SmsPopupActivity.this.mSwipeSmsGallery.requestLayout();
                        return;
                    default:
                        return;
                }
            }
        };
    }

    private void populateValues(Bundle bundle) {
        this.mMessage = new MessageWrapper(getApplicationContext(), bundle);
        if (this.mMessage.mUnreadCount > 1) {
            this.mQueryUnreadSmsHandler.startQuery(1, null, Telephony.Sms.Inbox.CONTENT_URI, new String[]{"address", Telephony.TextBasedSmsColumns.BODY, "date"}, UNREAD_CONDITION, null, "date DESC");
        } else if (this.mMessage.mUnreadCount == 1) {
            this.mSwipeSmsGallery.removeAllViews();
            PopSmsItemView popSmsItemView = new PopSmsItemView(getApplicationContext());
            PopSmsItemView.PopSmsItem popSmsItem = new PopSmsItemView.PopSmsItem();
            popSmsItem.mBody = this.mMessage.mMessageBody;
            popSmsItem.mAddress = this.mMessage.mAddress;
            popSmsItem.mTimestamp = this.mMessage.mTimestamp;
            popSmsItemView.bind(popSmsItem, this);
            this.mSwipeSmsGallery.addView(popSmsItemView);
            this.mSelectedPopSmsItemView = (PopSmsItemView) this.mSwipeSmsGallery.getChildAt(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Log.v(TAG, "SmsPopupActivity : OnCreate");
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.sms_popup);
        initViews();
        populateValues(bundle == null ? getIntent().getExtras() : bundle);
        Util.logEvent(PrefsUtil.EVENT_SMS_POPUP, new NameValuePair[0]);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.v(TAG, "SmsPopupActivity : onNewIntent");
        setIntent(intent);
        populateValues(intent.getExtras());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
