package vc.lx.sms.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class PopupMenuItem {
    public Drawable mDrawable;
    public int mIndex;
    public CharSequence mTitle;

    public PopupMenuItem(Context context, int i, int i2, int i3) {
        this.mDrawable = context.getResources().getDrawable(i);
        this.mTitle = context.getResources().getString(i2);
        this.mIndex = i3;
    }

    public PopupMenuItem(Context context, int i, CharSequence charSequence, int i2) {
        this.mDrawable = context.getResources().getDrawable(i);
        this.mTitle = charSequence;
        this.mIndex = i2;
    }

    public PopupMenuItem(Context context, Drawable drawable, int i, int i2) {
        this.mDrawable = drawable;
        this.mTitle = context.getResources().getString(i);
        this.mIndex = i2;
    }

    public PopupMenuItem(Drawable drawable, CharSequence charSequence, int i) {
        this.mDrawable = drawable;
        this.mTitle = charSequence;
        this.mIndex = i;
    }
}
