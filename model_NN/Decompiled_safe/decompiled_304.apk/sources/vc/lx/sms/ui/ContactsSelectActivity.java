package vc.lx.sms.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TabHost;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import vc.lx.sms.data.CallingCard;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.SmsConstants;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ContactsSelectActivity extends AbstractFlurryTabActivity {
    public static List<CallingCard> mCallingCards = new ArrayList();
    public static HashSet<String> mSelectedNumberOfContacts = new HashSet<>();
    public static HashSet<String> mSelectedNumberOfFavoriteContacts = new HashSet<>();
    private boolean isVisitedFromCallingCard = false;
    private LayoutInflater mInflater;
    private boolean mSelectForInvitation;
    private TabHost smsTabHost;

    public static Intent createIntentByVisitId(Context context, long j) {
        Intent intent = new Intent(context, ContactsSelectActivity.class);
        intent.putExtra(SmsConstants.VISIT_FROM_CALLING_CARD, j);
        return intent;
    }

    private TabHost.TabSpec createTabSpec(String str, int i, Intent intent) {
        TabHost.TabSpec newTabSpec = this.smsTabHost.newTabSpec(str);
        newTabSpec.setIndicator(this.mInflater.inflate(i, (ViewGroup) null));
        newTabSpec.setContent(intent);
        return newTabSpec;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void finish() {
        Intent intent = new Intent();
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        if (!this.isVisitedFromCallingCard || mCallingCards.isEmpty()) {
            Iterator<String> it = mSelectedNumberOfContacts.iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (!hashSet.contains(next)) {
                    arrayList.add(SmsApplication.getInstance().getmAllNumAndNameCache().get(next) + "<" + next + ">");
                }
            }
        } else {
            for (CallingCard next2 : mCallingCards) {
                arrayList.add(next2.displayName + next2.phoneNumber + (next2.mailbox == null ? "" : next2.mailbox) + (next2.address == null ? "" : next2.address) + (next2.company == null ? "" : next2.company));
            }
            mCallingCards.clear();
        }
        String[] strArr = new String[arrayList.size()];
        intent.putExtra("selected_names", (String[]) arrayList.toArray(strArr));
        mSelectedNumberOfContacts.clear();
        mSelectedNumberOfFavoriteContacts.clear();
        if (this.mSelectForInvitation) {
            Intent intent2 = getIntent();
            if (!(intent2 != null ? intent2.getBooleanExtra("no_selection", false) : false)) {
                Intent intent3 = new Intent("android.intent.action.VIEW");
                intent3.setComponent(new ComponentName(getApplicationContext(), ComposeMessageActivity.class));
                intent3.putExtra("exit_on_sent", true);
                intent3.putExtra("forwarded_message", true);
                intent3.putExtra("selected_names", (String[]) arrayList.toArray(strArr));
                intent3.putExtra("sms_body", getString(R.string.invite_content_SMS));
                intent3.setClassName(getApplicationContext(), "com.android.mms.ui.ForwardMessageActivity");
                startActivity(intent3);
                Util.logEvent(PrefsUtil.KEY_INVITE_BY_SMS, new NameValuePair[0]);
            }
        } else {
            setResult(-1, intent);
        }
        super.finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.contacts_select);
        if (getIntent() != null) {
            this.isVisitedFromCallingCard = getIntent().getBooleanExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, false);
            this.mSelectForInvitation = getIntent().getBooleanExtra(PrefsUtil.KEY_INVITE_BY_SMS, false);
        }
        this.mInflater = LayoutInflater.from(this);
        this.smsTabHost = (TabHost) findViewById(16908306);
        this.smsTabHost.setup(getLocalActivityManager());
        Intent intent = new Intent(getApplicationContext(), ContactsAllActivity.class);
        intent.putExtra(PrefsUtil.KEY_INVITE_BY_SMS, this.mSelectForInvitation);
        if (this.isVisitedFromCallingCard) {
            intent.putExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, true);
        }
        this.smsTabHost.addTab(createTabSpec("TAB_RADIO", R.layout.tab_all, intent));
        Intent intent2 = new Intent(getApplicationContext(), ContactsRecentActivity.class);
        intent2.putExtra(PrefsUtil.KEY_INVITE_BY_SMS, this.mSelectForInvitation);
        if (this.isVisitedFromCallingCard) {
            intent2.putExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, true);
        }
        this.smsTabHost.addTab(createTabSpec("TAB_RECENT", R.layout.tab_recent, intent2));
        Intent intent3 = new Intent(getApplicationContext(), ContactsGroupActivity.class);
        intent3.putExtra(PrefsUtil.KEY_INVITE_BY_SMS, this.mSelectForInvitation);
        if (this.isVisitedFromCallingCard) {
            intent3.putExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, true);
        }
        this.smsTabHost.addTab(createTabSpec("TAB_GROUP", R.layout.tab_group, intent3));
        Intent intent4 = new Intent(getApplicationContext(), ContactsFavoriteActivity.class);
        intent4.putExtra(PrefsUtil.KEY_INVITE_BY_SMS, this.mSelectForInvitation);
        if (this.isVisitedFromCallingCard) {
            intent4.putExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, true);
        }
        this.smsTabHost.addTab(createTabSpec("TAB_FAVORITE", R.layout.tab_favorite, intent4));
    }
}
