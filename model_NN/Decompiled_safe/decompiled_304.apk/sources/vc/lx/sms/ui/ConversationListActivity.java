package vc.lx.sms.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;
import com.android.mms.transaction.MessagingNotification;
import com.android.mms.transaction.SmsRejectedReceiver;
import com.android.mms.ui.SearchActivity;
import com.android.provider.Settings;
import com.android.provider.Telephony;
import com.google.android.mms.pdu.PduHeaders;
import com.google.android.mms.pdu.PduPart;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.richtext.ui.VoteCreateActivity;
import vc.lx.sms.service.ApkUpdateService;
import vc.lx.sms.service.PushUpdaterService;
import vc.lx.sms.ui.ConversationListAdapter;
import vc.lx.sms.ui.widget.ImageTextButton;
import vc.lx.sms.ui.widget.PopupGridMenusWindow;
import vc.lx.sms.ui.widget.PopupMenuItem;
import vc.lx.sms.ui.widget.PopupMenusWindow;
import vc.lx.sms.util.DraftCache;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Recycler;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ConversationListActivity extends AbstractFlurryListActivity implements DraftCache.OnDraftChangedListener, View.OnClickListener {
    private static final String CHECKED_MESSAGE_LIMITS = "checked_message_limits";
    private static final boolean DEBUG = false;
    public static final int DELETE_CONVERSATION_TOKEN = 1801;
    public static final int HAVE_LOCKED_MESSAGES_TOKEN = 1802;
    private static final boolean LOCAL_LOGV = false;
    private static final int MENU_ABOUT = 14;
    public static final int MENU_ADD_TO_CONTACTS = 3;
    private static final int MENU_CANCEL_MULTI_MODE = 9;
    public static final int MENU_COMPOSE_NEW = 0;
    public static final int MENU_DELETE = 0;
    public static final int MENU_DELETE_ALL = 3;
    private static final int MENU_DELETE_SELECTED = 8;
    protected static final int MENU_DOWNLOAD = 21;
    public static final int MENU_DRAFT_BOX = 4;
    private static final int MENU_FEEDBACK = 13;
    protected static final int MENU_MORE = 24;
    private static final int MENU_MULTI_MODE = 7;
    private static final int MENU_MUSIC_BOARDS = 11;
    public static final int MENU_PREFERENCES = 6;
    protected static final int MENU_RINGING = 22;
    protected static final int MENU_RINGTONE = 23;
    public static final int MENU_SEARCH = 1;
    public static final int MENU_SECRET_BOX = 5;
    private static final int MENU_SHARE = 10;
    private static final int MENU_UPDATE = 12;
    public static final int MENU_VIEW = 1;
    public static final int MENU_VIEW_CONTACT = 2;
    protected static final int PROGRESS_DIALOG = 25;
    private static final String TAG = "ConversationList";
    private static final int THREAD_LIST_QUERY_TOKEN = 1701;
    private HashMap<String, View> cacheAppCount = new HashMap<>();
    private HashMap<String, ViewStub> cacheViewStub = new HashMap<>();
    private String currentAppId = "";
    private DisplayMetrics dm;
    public AdapterView.OnItemClickListener galleryItemClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (ConversationListActivity.this.mPlayViewStub.isClickable()) {
                ConversationListActivity.this.mPlayViewStub.setVisibility(8);
                ConversationListActivity.this.mPlayViewStub.setClickable(false);
                return;
            }
            ConversationListActivity.this.mPlayViewStub.setVisibility(0);
            ConversationListActivity.this.mPlayViewStub.setClickable(true);
        }
    };
    private LayoutInflater inflater = null;
    private HorizontalScrollView mAppScrollView;
    private Button mCancelMultiModeBtn;
    private ImageTextButton mCommendView;
    private LinearLayout mContainer = null;
    private final ConversationListAdapter.OnContentChangedListener mContentChangedListener = new ConversationListAdapter.OnContentChangedListener() {
        public void onContentChanged(ConversationListAdapter conversationListAdapter) {
            ConversationListActivity.this.startAsyncQuery();
        }
    };
    private final View.OnCreateContextMenuListener mConvListOnCreateContextMenuListener = new View.OnCreateContextMenuListener() {
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            Cursor cursor = ConversationListActivity.this.mListAdapter.getCursor();
            if (cursor.getPosition() >= 0) {
                ContactList recipients = Conversation.from(ConversationListActivity.this, cursor).getRecipients();
                contextMenu.setHeaderTitle(recipients.formatNames(","));
                if (((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position >= 0) {
                    contextMenu.add(0, 1, 0, (int) R.string.menu_view);
                    if (recipients.size() == 1) {
                        if (((Contact) recipients.get(0)).existsInDatabase()) {
                            contextMenu.add(0, 2, 0, (int) R.string.menu_view_contact);
                        } else {
                            contextMenu.add(0, 3, 0, (int) R.string.menu_add_to_contacts);
                        }
                    }
                    contextMenu.add(0, 0, 0, (int) R.string.menu_delete);
                }
            }
        }
    };
    private Button mDelAllBtn;
    private Button mDelSelectedBtn;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public boolean mIsMultiMode = false;
    /* access modifiers changed from: private */
    public ConversationListAdapter mListAdapter;
    private MusicSubAdapter mMusicSubAdapter = null;
    private ImageView mNewMsgView;
    protected BroadcastReceiver mNewVersionReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            final String stringExtra = intent.getStringExtra(PrefsUtil.KEY_FILE_PATH);
            final String stringExtra2 = intent.getStringExtra(PrefsUtil.KEY_FORCE);
            if (String.valueOf(Util.getVersionName(ConversationListActivity.this.getApplicationContext())).compareTo(intent.getStringExtra(PrefsUtil.KEY_VERSION_CODE)) < 0) {
                if (stringExtra != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ConversationListActivity.this);
                    builder.setTitle((int) R.string.new_version_title);
                    builder.setMessage((int) R.string.new_version_message);
                    builder.setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                ConversationListActivity.this.dismissDialog(25);
                            } catch (Exception e) {
                            }
                            Intent intent = new Intent(ConversationListActivity.this.getApplicationContext(), ApkUpdateService.class);
                            intent.setAction(PrefsUtil.KEY_UPDATE_APK);
                            intent.putExtra(PrefsUtil.KEY_FILE_PATH, stringExtra);
                            ConversationListActivity.this.startService(intent);
                        }
                    });
                    builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                ConversationListActivity.this.dismissDialog(25);
                            } catch (Exception e) {
                            }
                            if ("1".equals(stringExtra2)) {
                                ConversationListActivity.this.finish();
                            }
                            dialogInterface.dismiss();
                            if (ConversationListActivity.this.progressDialog != null) {
                                ConversationListActivity.this.progressDialog.dismiss();
                            }
                        }
                    });
                    builder.create().show();
                }
            } else if (ConversationListActivity.this.progressDialog != null && ConversationListActivity.this.progressDialog.isShowing()) {
                ConversationListActivity.this.progressDialog.dismiss();
                AlertDialog.Builder builder2 = new AlertDialog.Builder(ConversationListActivity.this);
                builder2.setTitle((int) R.string.new_version_title);
                builder2.setMessage((int) R.string.no_version_message);
                builder2.setPositiveButton((int) R.string.confirm, (DialogInterface.OnClickListener) null);
                builder2.create().show();
            }
        }
    };
    private PopupMenusWindow.OnMenuItemClickListener mOnMenuItemClickListener = new PopupMenusWindow.OnMenuItemClickListener() {
        private void showProgressDlg() {
            ProgressDialog unused = ConversationListActivity.this.progressDialog = new ProgressDialog(ConversationListActivity.this.getApplicationContext());
            ConversationListActivity.this.progressDialog.setProgressStyle(1);
            ConversationListActivity.this.progressDialog.setMessage(ConversationListActivity.this.getString(R.string.loading_update));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onMenuItemClicked(PopupMenusWindow popupMenusWindow, int i) {
            PopupMenuItem menuItem = ConversationListActivity.this.mPopupMenuWindow.getMenuItem(i);
            if (menuItem != null) {
                switch (menuItem.mIndex) {
                    case 0:
                        ConversationListActivity.this.createNewMessage();
                        return;
                    case 1:
                        showProgressDlg();
                        ConversationListActivity.this.startSearchDialog();
                        return;
                    case 2:
                    case 5:
                    case 10:
                    default:
                        return;
                    case 3:
                        ConversationListActivity.confirmDeleteThread(-1, ConversationListActivity.this.mQueryHandler);
                        return;
                    case 4:
                        ConversationListActivity.this.openDraftBox();
                        return;
                    case 6:
                        Intent intent = new Intent(ConversationListActivity.this.getApplicationContext(), MessagingPreferenceActivity.class);
                        intent.putExtra(PrefsUtil.KEY_IS_SYS_SETTING, true);
                        ConversationListActivity.this.startActivity(intent);
                        return;
                    case 7:
                        boolean unused = ConversationListActivity.this.mIsMultiMode = true;
                        ConversationListActivity.this.findViewById(R.id.multi_actions_view).setVisibility(0);
                        ConversationListActivity.this.mListAdapter.setMultiMode(ConversationListActivity.this.mIsMultiMode);
                        ConversationListActivity.this.mListAdapter.notifyDataSetInvalidated();
                        return;
                    case 8:
                        ConversationListActivity.this.deleteSelectedConversations();
                        return;
                    case 9:
                        ConversationListActivity.this.cancelMultiMode();
                        return;
                    case 11:
                        ConversationListActivity.this.startActivity(new Intent(ConversationListActivity.this.getApplicationContext(), MusicPlatformActivity.class));
                        return;
                    case 12:
                        ConversationListActivity.this.showDialog(25);
                        ConversationListActivity.this.startUpdateVersionService();
                        return;
                    case 13:
                        Util.createCommentDialog(ConversationListActivity.this);
                        return;
                    case 14:
                        ConversationListActivity.this.startActivity(new Intent(ConversationListActivity.this.getApplicationContext(), AboutActivity.class));
                        return;
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public ViewStub mPlayViewStub = null;
    /* access modifiers changed from: private */
    public PopupGridMenusWindow mPopupMenuWindow;
    private PopupWindow mPopupWindow;
    /* access modifiers changed from: private */
    public SharedPreferences mPrefs;
    /* access modifiers changed from: private */
    public ThreadListQueryHandler mQueryHandler;
    private AsyncTask<?, ?, ?> mSongListTask;
    private final View.OnKeyListener mThreadListKeyListener = new View.OnKeyListener() {
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                switch (i) {
                    case 67:
                        long selectedItemId = ConversationListActivity.this.getListView().getSelectedItemId();
                        if (selectedItemId > 0) {
                            ConversationListActivity.confirmDeleteThread(selectedItemId, ConversationListActivity.this.mQueryHandler);
                        }
                        return true;
                }
            }
            return false;
        }
    };
    /* access modifiers changed from: private */
    public CharSequence mTitle;
    protected BroadcastReceiver mUpdateApkReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String stringExtra = intent.getStringExtra(PrefsUtil.KEY_FILE_PATH);
            if (stringExtra != null) {
                Util.logEvent(PrefsUtil.EVENT_APK_UPDATE, new NameValuePair[0]);
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.setDataAndType(Uri.fromFile(new File(stringExtra)), "application/vnd.android.package-archive");
                ConversationListActivity.this.startActivity(intent2);
            }
        }
    };
    private ViewFlipper mViewFlipper = null;
    public Handler myHandler = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            InputMethodManager inputMethodManager = SmsApplication.getInstance().getInputMethodManager();
            SmsApplication.getInstance().getInputMethodManager();
            inputMethodManager.toggleSoftInput(0, 2);
        }
    };
    public CompoundButton.OnCheckedChangeListener onCheckListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            switch (compoundButton.getId()) {
                case R.id.rbSearchContent /*2131493113*/:
                    ConversationListActivity.this.searchKey.setText("");
                    SmsApplication.getInstance().setSearchByContact(false);
                    return;
                case R.id.rbSearchContact /*2131493114*/:
                    ConversationListActivity.this.searchKey.setText("");
                    SmsApplication.getInstance().setSearchByContact(true);
                    return;
                default:
                    return;
            }
        }
    };
    public View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return false;
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public AutoCompleteTextView searchKey = null;
    private Button settingBtn;
    public View.OnClickListener settingClickListener = new View.OnClickListener() {
        public void onClick(View view) {
        }
    };
    private TopMusicService topMusicService;

    public static class DeleteThreadListener implements DialogInterface.OnClickListener {
        /* access modifiers changed from: private */
        public final Context mContext;
        /* access modifiers changed from: private */
        public boolean mDeleteLockedMessages;
        /* access modifiers changed from: private */
        public final AsyncQueryHandler mHandler;
        /* access modifiers changed from: private */
        public final long mThreadId;

        public DeleteThreadListener(long j, AsyncQueryHandler asyncQueryHandler, Context context) {
            this.mThreadId = j;
            this.mHandler = asyncQueryHandler;
            this.mContext = context;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            MessageUtils.handleReadReport(this.mContext, this.mThreadId, 129, new Runnable() {
                public void run() {
                    if (DeleteThreadListener.this.mThreadId == -1) {
                        Conversation.startDeleteAll(DeleteThreadListener.this.mHandler, ConversationListActivity.DELETE_CONVERSATION_TOKEN, DeleteThreadListener.this.mDeleteLockedMessages);
                        DraftCache.getInstance().refresh();
                        ((ConversationListActivity) DeleteThreadListener.this.mContext).cancelMultiMode();
                        return;
                    }
                    Conversation.startDelete(DeleteThreadListener.this.mHandler, ConversationListActivity.DELETE_CONVERSATION_TOKEN, DeleteThreadListener.this.mDeleteLockedMessages, DeleteThreadListener.this.mThreadId);
                    DraftCache.getInstance().setDraftState(DeleteThreadListener.this.mThreadId, false);
                }
            });
        }

        public void setDeleteLockedMessage(boolean z) {
            this.mDeleteLockedMessages = z;
        }
    }

    public class SongItemView {
        public Button moreBtn;
        public ImageView pictureView;
        public ToggleButton playBtn;
        public ProgressBar playProgress;
        public Button shareBtn;
        public TextView singerView;
        public TextView songView;

        public SongItemView() {
        }
    }

    private final class ThreadListQueryHandler extends AsyncQueryHandler {
        public ThreadListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onDeleteComplete(int i, Object obj, int i2) {
            switch (i) {
                case ConversationListActivity.DELETE_CONVERSATION_TOKEN /*1801*/:
                    Conversation.init(ConversationListActivity.this);
                    MessagingNotification.updateNewMessageIndicator(ConversationListActivity.this);
                    MessagingNotification.updateSendFailedNotification(ConversationListActivity.this);
                    ConversationListActivity.this.startAsyncQuery();
                    ConversationListActivity.this.onContentChanged();
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case ConversationListActivity.THREAD_LIST_QUERY_TOKEN /*1701*/:
                    ConversationListActivity.this.mListAdapter.changeCursor(cursor);
                    ConversationListActivity.this.setTitle(ConversationListActivity.this.mTitle);
                    ConversationListActivity.this.setProgressBarIndeterminateVisibility(false);
                    return;
                case ConversationListActivity.HAVE_LOCKED_MESSAGES_TOKEN /*1802*/:
                    long longValue = ((Long) obj).longValue();
                    ConversationListActivity.confirmDeleteThreadDialog(new DeleteThreadListener(longValue, ConversationListActivity.this.mQueryHandler, ConversationListActivity.this), longValue == -1, cursor != null && cursor.getCount() > 0, ConversationListActivity.this);
                    return;
                default:
                    Log.e(ConversationListActivity.TAG, "onQueryComplete called with unknown token " + i);
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void cancelMultiMode() {
        this.mIsMultiMode = false;
        findViewById(R.id.multi_actions_view).setVisibility(8);
        this.mListAdapter.setMultiMode(this.mIsMultiMode);
        this.mListAdapter.clearSelectedIds();
        this.mListAdapter.notifyDataSetInvalidated();
    }

    public static void confirmDeleteThread(long j, AsyncQueryHandler asyncQueryHandler) {
        Conversation.startQueryHaveLockedMessages(asyncQueryHandler, j, HAVE_LOCKED_MESSAGES_TOKEN);
    }

    public static void confirmDeleteThreadDialog(final DeleteThreadListener deleteThreadListener, boolean z, boolean z2, Context context) {
        View inflate = View.inflate(context, R.layout.delete_thread_dialog_view, null);
        ((TextView) inflate.findViewById(R.id.message)).setText(z ? R.string.confirm_delete_all_conversations : R.string.confirm_delete_conversation);
        final CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.delete_locked);
        if (!z2) {
            checkBox.setVisibility(8);
        } else {
            deleteThreadListener.setDeleteLockedMessage(checkBox.isChecked());
            checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    deleteThreadListener.setDeleteLockedMessage(checkBox.isChecked());
                }
            });
        }
        new AlertDialog.Builder(context).setTitle((int) R.string.confirm_dialog_title).setIcon(17301543).setCancelable(true).setPositiveButton((int) R.string.delete, deleteThreadListener).setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null).setView(inflate).show();
    }

    public static Intent createAddContactIntent(String str) {
        Intent intent = new Intent("android.intent.action.INSERT_OR_EDIT");
        intent.setType("vnd.android.cursor.item/contact");
        if (Telephony.Mms.isEmailAddress(str)) {
            intent.putExtra("email", str);
        } else {
            intent.putExtra("phone", str);
            intent.putExtra("phone_type", 2);
        }
        intent.setFlags(524288);
        return intent;
    }

    /* access modifiers changed from: private */
    public void createNewMessage() {
        startActivity(ComposeMessageActivity.createIntentByThreadId(this, 0));
    }

    /* access modifiers changed from: private */
    public void deleteSelectedConversations() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.menu_delete_selected_items);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setMessage((int) R.string.confirm_delete_selected_messages);
        builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ConversationListActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        HashSet<Long> selectedIds = ConversationListActivity.this.mListAdapter.getSelectedIds();
                        if (selectedIds.size() != 0) {
                            Long[] lArr = new Long[selectedIds.size()];
                            selectedIds.toArray(lArr);
                            Conversation.batchDelete(ConversationListActivity.this.mQueryHandler, ConversationListActivity.DELETE_CONVERSATION_TOKEN, lArr, ConversationListActivity.this);
                        }
                    }
                });
            }
        });
        builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    private void initListAdapter() {
        this.mListAdapter = new ConversationListAdapter(this, null);
        this.mListAdapter.setOnContentChangedListener(this.mContentChangedListener);
        setListAdapter(this.mListAdapter);
        getListView().setRecyclerListener(this.mListAdapter);
    }

    private void initPopupSeachWindow() {
        this.mViewFlipper = new ViewFlipper(this);
        this.mViewFlipper.setInAnimation(getApplicationContext(), R.anim.menu_in);
        this.mViewFlipper.setOutAnimation(this, R.anim.menu_out);
        View inflate = this.inflater.inflate((int) R.layout.pop_search, (ViewGroup) null);
        Button button = (Button) inflate.findViewById(R.id.search_btn);
        button.setClickable(false);
        SmsApplication.getInstance().setSearchByContact(true);
        final RadioButton radioButton = (RadioButton) inflate.findViewById(R.id.rbSearchContact);
        this.searchKey = (AutoCompleteTextView) inflate.findViewById(R.id.keyword);
        radioButton.setOnCheckedChangeListener(this.onCheckListener);
        ((RadioButton) inflate.findViewById(R.id.rbSearchContent)).setOnCheckedChangeListener(this.onCheckListener);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String obj = ConversationListActivity.this.searchKey.getText().toString();
                if (obj != null && !"".equals(obj)) {
                    if (radioButton.isChecked()) {
                        Util.logEvent("search_contact", new NameValuePair[0]);
                    } else {
                        Util.logEvent("search_sms", new NameValuePair[0]);
                    }
                    Intent intent = new Intent(ConversationListActivity.this.getApplicationContext(), SearchActivity.class);
                    intent.putExtra(PrefsUtil.KEY_SMS_SEARCH_KEY, ConversationListActivity.this.searchKey.getText().toString());
                    ConversationListActivity.this.startActivity(intent);
                }
            }
        });
        this.mViewFlipper.addView(inflate);
        this.mViewFlipper.setFlipInterval(30000);
        this.mPopupWindow = new PopupWindow(this.mViewFlipper, -1, -2);
        this.mPopupWindow.setFocusable(true);
        this.mPopupWindow.update();
    }

    /* access modifiers changed from: private */
    public void log(String str, Object... objArr) {
        Log.d(TAG, "[" + Thread.currentThread().getId() + "] " + String.format(str, objArr));
    }

    private void logAmountOfUsage() {
        int amountOfUsageValue = Util.getAmountOfUsageValue(getApplicationContext());
        if (amountOfUsageValue != 0 && amountOfUsageValue % 5 == 0) {
            Util.logEvent(PrefsUtil.KEY_AMOUNT_OF_USAGE, new BasicNameValuePair((String) Settings.NameValueTable.VALUE, String.valueOf(amountOfUsageValue)));
        }
        Util.setAmountOfUsageValue(getApplicationContext(), amountOfUsageValue + 1);
    }

    /* access modifiers changed from: private */
    public void markCheckedMessageLimit() {
        SharedPreferences.Editor edit = this.mPrefs.edit();
        edit.putBoolean(CHECKED_MESSAGE_LIMITS, true);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void openDraftBox() {
        startActivity(DraftBoxActivity.createIntent(this, 0));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void openThread(long j) {
        Intent createIntentByThreadId = ComposeMessageActivity.createIntentByThreadId(this, j);
        createIntentByThreadId.putExtra("need_reset", true);
        startActivity(createIntentByThreadId);
    }

    private void openVoteThread(long j) {
        startActivity(VoteCreateActivity.createIntentByThreadId(this, j));
    }

    private void showUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.new_version_info));
        builder.setMessage(getString(R.string.new_version_content));
        builder.setNegativeButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
        PrefsUtil.saveUpdateState(getApplicationContext(), "true");
    }

    /* access modifiers changed from: private */
    public void startAsyncQuery() {
        try {
            setTitle(getString(R.string.refreshing));
            setProgressBarIndeterminateVisibility(true);
            Conversation.startQueryForAll(this.mQueryHandler, THREAD_LIST_QUERY_TOKEN);
        } catch (SQLiteException e) {
            e.printStackTrace();
            SqliteWrapper.checkSQLiteException(this, e);
        }
    }

    /* access modifiers changed from: private */
    public void startSearchDialog() {
        if (this.mPopupWindow.isShowing()) {
            this.mPopupWindow.dismiss();
            return;
        }
        this.searchKey.setText("");
        this.mPopupWindow.setFocusable(true);
        this.mPopupWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.pop_search_bg));
        this.mPopupWindow.showAtLocation(findViewById(R.id.header), 48, 0, this.dm.heightPixels == 480 ? 25 : (this.dm.heightPixels == 800 || this.dm.heightPixels == 854) ? 38 : 38);
        this.mViewFlipper.startFlipping();
        this.myHandler.sendMessageDelayed(this.myHandler.obtainMessage(1), 500);
    }

    /* access modifiers changed from: private */
    public void startUpdateVersionService() {
        Intent intent = new Intent(getApplicationContext(), ApkUpdateService.class);
        intent.setAction(PrefsUtil.KEY_LATEST_VERSION);
        startService(intent);
    }

    public void clearTextState(String str) {
        if (this.mContainer != null) {
            for (int i = 0; i < this.mContainer.getChildCount(); i++) {
                FrameLayout frameLayout = (FrameLayout) this.mContainer.getChildAt(i);
                TextView textView = (TextView) frameLayout.getChildAt(0);
                textView.setTextColor(Color.rgb(150, (int) PduHeaders.MBOX_QUOTAS, (int) PduPart.P_CONTENT_DISPOSITION));
                frameLayout.getChildAt(2).setVisibility(8);
                textView.setBackgroundResource(R.drawable.app_item_bg);
            }
        }
        if (!"".equals(this.currentAppId) && !this.currentAppId.equals(str)) {
            this.cacheViewStub.get(this.currentAppId).setVisibility(8);
        }
    }

    public void onClick(View view) {
        if (view == this.mDelAllBtn) {
            confirmDeleteThread(-1, this.mQueryHandler);
        }
        if (view == this.mDelSelectedBtn) {
            deleteSelectedConversations();
        }
        if (view == this.mCancelMultiModeBtn) {
            cancelMultiMode();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        Cursor cursor = this.mListAdapter.getCursor();
        if (cursor.getPosition() >= 0) {
            Conversation from = Conversation.from(this, cursor);
            long threadId = from.getThreadId();
            switch (menuItem.getItemId()) {
                case 0:
                    confirmDeleteThread(threadId, this.mQueryHandler);
                    break;
                case 1:
                    openThread(threadId);
                    break;
                case 2:
                    Intent intent = new Intent("android.intent.action.VIEW", ((Contact) from.getRecipients().get(0)).getUri());
                    intent.setFlags(524288);
                    startActivity(intent);
                    break;
                case 3:
                    startActivity(createAddContactIntent(((Contact) from.getRecipients().get(0)).getNumber()));
                    break;
            }
        }
        return super.onContextItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.conversation_list_screen);
        DraftCache.init(this);
        Util.logEvent(PrefsUtil.EVENT_APP_OPEN, new NameValuePair[0]);
        this.mQueryHandler = new ThreadListQueryHandler(getContentResolver());
        this.mPopupMenuWindow = new PopupGridMenusWindow(getApplicationContext());
        this.mPopupMenuWindow.setOnMenuItemClickListener(this.mOnMenuItemClickListener);
        this.inflater = (LayoutInflater) getApplicationContext().getSystemService("layout_inflater");
        initPopupSeachWindow();
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        SmsApplication.getInstance().dm = this.dm;
        this.topMusicService = SmsApplication.getInstance().getTopMusicService();
        ListView listView = getListView();
        this.mNewMsgView = (ImageView) findViewById(R.id.newmsg);
        this.settingBtn = (Button) findViewById(R.id.setting);
        this.settingBtn.setOnClickListener(this.settingClickListener);
        this.mNewMsgView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ConversationListActivity.this.createNewMessage();
            }
        });
        listView.setOnCreateContextMenuListener(this.mConvListOnCreateContextMenuListener);
        listView.setOnKeyListener(this.mThreadListKeyListener);
        initListAdapter();
        this.mTitle = getString(R.string.app_label);
        this.mHandler = new Handler();
        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!this.mPrefs.getBoolean(CHECKED_MESSAGE_LIMITS, false)) {
            runOneTimeStorageLimitCheckForLegacyMessages();
        }
        startUpdateVersionService();
        this.mDelAllBtn = (Button) findViewById(R.id.btn_delete_all);
        this.mDelSelectedBtn = (Button) findViewById(R.id.btn_delete_seleted);
        this.mCancelMultiModeBtn = (Button) findViewById(R.id.btn_cancel_multi);
        this.mDelAllBtn.setOnClickListener(this);
        this.mDelSelectedBtn.setOnClickListener(this);
        this.mCancelMultiModeBtn.setOnClickListener(this);
        if ("false".equals(PrefsUtil.getUpdateState(getApplicationContext()))) {
            showUpdateAlert();
        }
        logAmountOfUsage();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 25:
                this.progressDialog = new ProgressDialog(this);
                this.progressDialog.setProgressStyle(0);
                this.progressDialog.setMessage(getString(R.string.loading_update));
                return this.progressDialog;
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.mPopupMenuWindow.clearAllPopupMenuItems();
        if (!this.mIsMultiMode) {
            this.mPopupMenuWindow.setNumColums(2);
            this.mPopupMenuWindow.addPopupMenuItem(new PopupMenuItem(this, (int) R.drawable.menu_search, (int) R.string.menu_search, 1));
            if (this.mListAdapter.getCount() > 0) {
                this.mPopupMenuWindow.addPopupMenuItem(new PopupMenuItem(this, (int) R.drawable.bg_menu_multi_mode, (int) R.string.menu_multi_mode, 7));
            }
            this.mPopupMenuWindow.show(findViewById(R.id.main));
            this.mPopupMenuWindow.afterShow();
        }
        Util.logEvent("main_menu_pop", new NameValuePair[0]);
        return false;
    }

    public void onDraftChanged(final long j, final boolean z) {
        this.mQueryHandler.post(new Runnable() {
            public void run() {
                if (Log.isLoggable(LogTag.APP, 2)) {
                    ConversationListActivity.this.log("onDraftChanged: threadId=" + j + ", hasDraft=" + z, new Object[0]);
                }
                ConversationListActivity.this.mListAdapter.notifyDataSetChanged();
            }
        });
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                if (!this.mIsMultiMode) {
                    if (this.mPopupWindow.isShowing()) {
                        this.mPopupWindow.dismiss();
                        break;
                    }
                } else {
                    cancelMultiMode();
                    return true;
                }
                break;
            case 84:
                startSearchDialog();
                break;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        if (view instanceof ConversationHeaderView) {
            ConversationHeader conversationHeader = ((ConversationHeaderView) view).getConversationHeader();
            if (conversationHeader.hasVoteDraft()) {
                openThread(conversationHeader.getThreadId());
            } else {
                openThread(conversationHeader.getThreadId());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        privateOnStart();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                createNewMessage();
                break;
            case 1:
                startSearchDialog();
                Util.logEvent("search_dialog_pop", new NameValuePair[0]);
                break;
            case 2:
            case 5:
            default:
                return true;
            case 3:
                confirmDeleteThread(-1, this.mQueryHandler);
                break;
            case 4:
                openDraftBox();
                break;
            case 6:
                startActivity(new Intent(this, MessagingPreferenceActivity.class));
                break;
            case 7:
                this.mIsMultiMode = true;
                findViewById(R.id.multi_actions_view).setVisibility(0);
                this.mListAdapter.setMultiMode(this.mIsMultiMode);
                this.mListAdapter.notifyDataSetInvalidated();
                break;
            case 8:
                deleteSelectedConversations();
                break;
            case 9:
                cancelMultiMode();
                break;
            case 10:
                startSearchDialog();
                break;
            case 11:
                startActivity(new Intent(this, MusicPlatformActivity.class));
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            if (this.mPopupMenuWindow != null && this.mPopupMenuWindow.isShowing()) {
                this.mPopupMenuWindow.dismiss();
            }
            if (isFinishing()) {
                unregisterReceiver(this.mUpdateApkReceiver);
                unregisterReceiver(this.mNewVersionReceiver);
            }
        } catch (IllegalStateException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        Log.i("onSaveInstanceState", "true");
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.mPopupMenuWindow != null && this.mPopupMenuWindow.isShowing()) {
            this.mPopupMenuWindow.dismiss();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    public boolean onSearchRequested() {
        Bundle bundle = new Bundle();
        bundle.putString(PrefsUtil.KEY_CONTACT_NUMBER, "true");
        startSearch(null, false, bundle, false);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        MessagingNotification.cancelNotification(getApplicationContext(), SmsRejectedReceiver.SMS_REJECTED_NOTIFICATION_ID);
        registerReceiver(this.mUpdateApkReceiver, new IntentFilter(PrefsUtil.INTENT_UPDATE_APK));
        registerReceiver(this.mNewVersionReceiver, new IntentFilter(PrefsUtil.INTENT_NEW_VERSION));
        Conversation.cleanup(this);
        DraftCache.getInstance().addOnDraftChangedListener(this);
        privateOnStart();
        startService(new Intent(getApplicationContext(), PushUpdaterService.class));
        if (!Conversation.loadingThreads()) {
            Contact.invalidateCache();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        DraftCache.getInstance().removeOnDraftChangedListener(this);
        this.mListAdapter.changeCursor(null);
        if (this.mPopupWindow != null) {
            this.mPopupWindow.dismiss();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        Log.i("touch", "touch");
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void privateOnStart() {
        startAsyncQuery();
    }

    public synchronized void runOneTimeStorageLimitCheckForLegacyMessages() {
        if (Recycler.isAutoDeleteEnabled(this)) {
            markCheckedMessageLimit();
        } else {
            new Thread(new Runnable() {
                public void run() {
                    if (!Recycler.checkForThreadsOverLimit(ConversationListActivity.this)) {
                        ConversationListActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                SharedPreferences.Editor edit = ConversationListActivity.this.mPrefs.edit();
                                edit.putBoolean(MessagingPreferenceActivity.AUTO_DELETE, true);
                                edit.commit();
                            }
                        });
                    }
                    ConversationListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            ConversationListActivity.this.markCheckedMessageLimit();
                        }
                    });
                }
            }).start();
        }
    }
}
