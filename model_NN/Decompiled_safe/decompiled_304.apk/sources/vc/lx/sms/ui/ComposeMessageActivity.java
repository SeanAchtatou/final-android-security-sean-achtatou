package vc.lx.sms.ui;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemProperties;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.mms.MmsConfig;
import com.android.mms.data.WorkingMessage;
import com.android.mms.model.SlideshowModel;
import com.android.mms.transaction.MessageSender;
import com.android.mms.transaction.MessagingNotification;
import com.android.mms.transaction.SmsMessageSender;
import com.android.mms.ui.AttachmentEditor;
import com.android.mms.ui.AttachmentTypeSelectorAdapter;
import com.android.mms.ui.DeliveryReportActivity;
import com.android.mms.ui.MessageItem;
import com.android.mms.ui.MessageListAdapter;
import com.android.mms.ui.MessageListItem;
import com.android.mms.ui.MessageListView;
import com.android.mms.ui.RecipientsAdapter;
import com.android.mms.ui.RecipientsEditor;
import com.android.mms.util.SmileyParser;
import com.android.provider.Telephony;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduHeaders;
import com.google.android.mms.pdu.PduPart;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.SendReq;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vc.lx.sms.caches.RemoteResourceManager;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.api.WrappedResult;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.SmsImage;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.parser.ForwardSongParser;
import vc.lx.sms.cmcc.http.parser.VoteParser;
import vc.lx.sms.cmcc.http.parser.VoteSingleGetParser;
import vc.lx.sms.cmcc.http.parser.VoterForwardParser;
import vc.lx.sms.cmcc.http.uploadimage.UploadImageJob;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.data.MessagetToComposeDate;
import vc.lx.sms.db.ImageDumpContentProvider;
import vc.lx.sms.db.ImagesDbService;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.richtext.ui.QuareVoteListActivity;
import vc.lx.sms.richtext.ui.VoteCreateActivity;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.template.ui.SmsTemplateListActivity;
import vc.lx.sms.util.ImageUtils;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Recycler;
import vc.lx.sms.util.SmsConstants;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ComposeMessageActivity extends AbstractFlurryActivity implements View.OnClickListener, TextView.OnEditorActionListener, WorkingMessage.MessageStatusListener, Contact.UpdateListener {
    private static final int CHARS_REMAINING_BEFORE_COUNTER_SHOWN = 10;
    private static final boolean DEBUG = false;
    private static final int DELETE_MESSAGE_TOKEN = 9700;
    private static final String EXIT_ECM_RESULT = "exit_ecm_result";
    private static final String EXTRA_DURATION_LIMIT = "android.intent.extra.durationLimit";
    private static final String EXTRA_SIZE_LIMIT = "android.intent.extra.sizeLimit";
    private static final boolean LOCAL_LOGV = false;
    private static final int MENU_ADD_ADDRESS_TO_CONTACTS = 27;
    private static final int MENU_ADD_TO_CONTACTS = 13;
    private static final int MENU_CALL_BACK = 22;
    private static final int MENU_CALL_RECIPIENT = 5;
    private static final int MENU_CANCEL_MULTI_MODE = 34;
    private static final int MENU_COMPOSE_NEW = 2;
    private static final int MENU_CONVERSATION_LIST = 6;
    private static final int MENU_COPY_MESSAGE_TEXT = 24;
    private static final int MENU_COPY_TO_DRM_PROVIDER = 30;
    private static final int MENU_COPY_TO_SDCARD = 25;
    private static final int MENU_DELETE_ALL = 36;
    private static final int MENU_DELETE_MESSAGE = 18;
    private static final int MENU_DELETE_SELECTED = 33;
    private static final int MENU_DELETE_THREAD = 1;
    private static final int MENU_DELIVERY_REPORT = 20;
    private static final int MENU_DISCARD = 3;
    private static final int MENU_EDIT_MESSAGE = 14;
    private static final int MENU_FORWARD_MESSAGE = 21;
    private static final int MENU_LOCK_MESSAGE = 28;
    private static final int MENU_MULTI_MODE = 32;
    public static final int MENU_MUSIC_BOARDS = 35;
    private static final int MENU_RE_SEND = 31;
    private static final int MENU_SEARCH = 19;
    private static final int MENU_SEND = 4;
    private static final int MENU_SEND_EMAIL = 23;
    private static final int MENU_UNLOCK_MESSAGE = 29;
    private static final int MENU_VIEW_CONTACT = 12;
    private static final int MENU_VIEW_MESSAGE_DETAILS = 17;
    private static final int MENU_VIEW_SLIDESHOW = 16;
    private static final int MESSAGE_LIST_QUERY_TOKEN = 9527;
    private static final long NO_DATE_FOR_DIALOG = -1;
    private static final int RECIPIENTS_MAX_LENGTH = 312;
    public static final int REQUEST_CODE_ATTACH_IMAGE = 10;
    public static final int REQUEST_CODE_CALLING_CARD_SELECT = 22;
    public static final int REQUEST_CODE_CAMERA_IMAGE = 20;
    public static final int REQUEST_CODE_CONTACTS_SELECT = 18;
    public static final int REQUEST_CODE_CREATE_VOTE = 25;
    public static final int REQUEST_CODE_ECM_EXIT_DIALOG = 17;
    public static final int REQUEST_CODE_GALLERY_IMAGE = 19;
    public static final int REQUEST_CODE_IMAGE_FILTER = 21;
    public static final int REQUEST_CODE_SELECT_MUSIC = 24;
    public static final int REQUEST_CODE_SMS_TEMPLATE = 23;
    public static final int REQUEST_CODE_VOTE_DETAIL = 26;
    private static final String SMS_DRAFT_WHERE = "type=3";
    private static final String TAG = "Mms/compose";
    private static final boolean TRACE = false;
    public static HashMap<String, String> cacheNameNum = new HashMap<>();
    public static String mRecipientsNames;
    private static ContactList sEmptyContactList;
    private String appType = "";
    private long currentMsgId;
    private String currentMsgType;
    private SongItem currentSongItem;
    private AsyncTask<Void, Void, WrappedResult> execute;
    private boolean isForwardVote;
    private Button mAddContactButton;
    private AttachmentEditor mAttachmentEditor;
    /* access modifiers changed from: private */
    public final Handler mAttachmentEditorHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 2:
                    if (ComposeMessageActivity.this.isPreparedForSending()) {
                        ComposeMessageActivity.this.confirmSendMessageIfNeeded();
                        return;
                    }
                    return;
                case 3:
                case 7:
                case 8:
                case 9:
                    MessageUtils.viewMmsMessageAttachment(ComposeMessageActivity.this, ComposeMessageActivity.this.mWorkingMessage);
                    return;
                case 4:
                case 5:
                case 6:
                case 12:
                case 13:
                case 14:
                default:
                    return;
                case 10:
                    ComposeMessageActivity.this.mWorkingMessage.setAttachment(0, null, false);
                    return;
                case 11:
                    ComposeMessageActivity.this.mWorkingMessage.updateSongShortenUrl(null, null);
                    ComposeMessageActivity.this.mMusicAttachmentView.update(ComposeMessageActivity.this.mWorkingMessage);
                    ComposeMessageActivity.this.updateSendButtonState();
                    return;
                case 15:
                    ComposeMessageActivity.this.mWorkingMessage.updateVoteShortenUrl(null, null);
                    ComposeMessageActivity.this.mVoteAttachmentView.update(ComposeMessageActivity.this.mWorkingMessage);
                    ComposeMessageActivity.this.updateSendButtonState();
                    return;
            }
        }
    };
    private AttachmentTypeSelectorAdapter mAttachmentTypeSelectorAdapter;
    /* access modifiers changed from: private */
    public BackgroundQueryHandler mBackgroundQueryHandler;
    private View mBottomPanel;
    private Button mCancelMultiModeBtn;
    private ContentResolver mContentResolver;
    /* access modifiers changed from: private */
    public Conversation mConversation;
    private final MessageListAdapter.OnDataSetChangedListener mDataSetChangedListener = new MessageListAdapter.OnDataSetChangedListener() {
        public void onContentChanged(MessageListAdapter messageListAdapter) {
            ComposeMessageActivity.this.startMsgListQuery();
        }

        public void onDataSetChanged(MessageListAdapter messageListAdapter) {
            boolean unused = ComposeMessageActivity.this.mPossiblePendingNotification = true;
        }
    };
    private Button mDelAllBtn;
    private Button mDelSelectedBtn;
    float mDensity;
    int mDensityDpi;
    private TextView mDisplayName;
    private boolean mExitOnSent;
    /* access modifiers changed from: private */
    public GridView mGridView;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public ImageView mImageAttachment;
    /* access modifiers changed from: private */
    public View mImageAttachmentView;
    protected BroadcastReceiver mImageForwardReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (ComposeMessageActivity.this.mUploadImageDlg != null) {
                ComposeMessageActivity.this.mUploadImageDlg.dismiss();
            }
            int intExtra = intent.getIntExtra(PrefsUtil.KEY_UPLOAD_STATUS, 1);
            if (intExtra == 1) {
                UploadImageFailedListener uploadImageFailedListener = new UploadImageFailedListener();
                AlertDialog.Builder builder = new AlertDialog.Builder(ComposeMessageActivity.this);
                builder.setTitle((int) R.string.forward_image_failed);
                builder.setPositiveButton((int) R.string.save_as_draft, uploadImageFailedListener);
                builder.setNegativeButton((int) R.string.send_plain_text, uploadImageFailedListener);
                builder.show();
            } else if (intExtra == 2) {
                Util.logEvent(PrefsUtil.EVENT_IMAGE_FORWARD, new NameValuePair[0]);
                ComposeMessageActivity.this.onMessageSent();
                ComposeMessageActivity.this.resetMessage();
            }
        }
    };
    protected BroadcastReceiver mImageUploadReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (ComposeMessageActivity.this.mUploadImageDlg != null) {
                ComposeMessageActivity.this.mUploadImageDlg.dismiss();
            }
            int intExtra = intent.getIntExtra(PrefsUtil.KEY_UPLOAD_STATUS, 1);
            if (intExtra == 1) {
                Toast.makeText(ComposeMessageActivity.this.getApplicationContext(), (int) R.string.uploading_image_failed, 0).show();
            } else if (intExtra == 2) {
                Util.logEvent(PrefsUtil.EVENT_IMAGE_SEND, new NameValuePair[0]);
                ComposeMessageActivity.this.onMessageSent();
                ComposeMessageActivity.this.resetMessage();
            }
        }
    };
    private LayoutInflater mInflater;
    boolean mIsForwardedMessage = false;
    private boolean mIsKeyboardOpen;
    private boolean mIsLandscape;
    /* access modifiers changed from: private */
    public boolean mIsMultiMode = false;
    private int mLastRecipientCount;
    PopupWindow mMessageItemPopup;
    /* access modifiers changed from: private */
    public final Handler mMessageListItemHandler = new Handler() {
        public void handleMessage(Message message) {
            String str = "sms";
            switch (message.what) {
                case 1:
                    str = Phone.APN_TYPE_MMS;
                    break;
                case 2:
                    break;
                case 3:
                    String str2 = ((MessagetToComposeDate) message.obj).msgItem.mType;
                    ComposeMessageActivity.this.displayMessageItemOperation((MessagetToComposeDate) message.obj);
                    return;
                case 4:
                    ComposeMessageActivity.this.mMsgListAdapter.notifyDataSetChangedWithoutChangingPos();
                    return;
                default:
                    Log.w(ComposeMessageActivity.TAG, "Unknown message: " + message.what);
                    return;
            }
            MessageItem access$800 = ComposeMessageActivity.this.getMessageItem(str, ((Long) message.obj).longValue());
            if (access$800 != null) {
                ComposeMessageActivity.this.editMessageItem(access$800);
                ComposeMessageActivity.this.drawBottomPanel();
            }
        }
    };
    /* access modifiers changed from: private */
    public MessageListAdapter mMsgListAdapter;
    private final View.OnCreateContextMenuListener mMsgListMenuCreateListener = new View.OnCreateContextMenuListener() {
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            Cursor cursor = ComposeMessageActivity.this.mMsgListAdapter.getCursor();
            if (cursor != null) {
                String string = cursor.getString(0);
                long j = cursor.getLong(1);
                ComposeMessageActivity.this.addPositionBasedMenuItems(contextMenu, view, contextMenuInfo);
                MessageItem cachedMessageItem = ComposeMessageActivity.this.mMsgListAdapter.getCachedMessageItem(string, j, cursor);
                if (cachedMessageItem == null) {
                    Log.e(ComposeMessageActivity.TAG, "Cannot load message item for type = " + string + ", msgId = " + j);
                    return;
                }
                contextMenu.setHeaderTitle((int) R.string.message_options);
                MsgListMenuClickListener msgListMenuClickListener = new MsgListMenuClickListener();
                if (cachedMessageItem.isMms()) {
                    switch (cachedMessageItem.mBoxId) {
                        case 4:
                            if (ComposeMessageActivity.this.getRecipients().size() == 1) {
                                contextMenu.add(0, 14, 0, (int) R.string.menu_edit).setOnMenuItemClickListener(msgListMenuClickListener);
                                break;
                            }
                            break;
                    }
                    switch (cachedMessageItem.mAttachmentType) {
                        case 0:
                            break;
                        case 1:
                        case 2:
                            if (ComposeMessageActivity.this.haveSomethingToCopyToSDCard(cachedMessageItem.mMsgId)) {
                                contextMenu.add(0, 25, 0, (int) R.string.copy_to_sdcard).setOnMenuItemClickListener(msgListMenuClickListener);
                                break;
                            }
                            break;
                        default:
                            contextMenu.add(0, 16, 0, (int) R.string.view_slideshow).setOnMenuItemClickListener(msgListMenuClickListener);
                            if (ComposeMessageActivity.this.haveSomethingToCopyToSDCard(cachedMessageItem.mMsgId)) {
                                contextMenu.add(0, 25, 0, (int) R.string.copy_to_sdcard).setOnMenuItemClickListener(msgListMenuClickListener);
                            }
                            if (ComposeMessageActivity.this.haveSomethingToCopyToDrmProvider(cachedMessageItem.mMsgId)) {
                                contextMenu.add(0, (int) ComposeMessageActivity.MENU_COPY_TO_DRM_PROVIDER, 0, ComposeMessageActivity.this.getDrmMimeMenuStringRsrc(cachedMessageItem.mMsgId)).setOnMenuItemClickListener(msgListMenuClickListener);
                                break;
                            }
                            break;
                    }
                } else if (ComposeMessageActivity.this.getRecipients().size() == 1 && (cachedMessageItem.mBoxId == 4 || cachedMessageItem.mBoxId == 5)) {
                    contextMenu.add(0, 31, 0, (int) R.string.menu_re_send).setOnMenuItemClickListener(msgListMenuClickListener);
                }
                ComposeMessageActivity.this.addCallAndContactMenuItems(contextMenu, msgListMenuClickListener, cachedMessageItem);
                if (cachedMessageItem.isDownloaded()) {
                    contextMenu.add(0, 21, 0, (int) R.string.menu_forward).setOnMenuItemClickListener(msgListMenuClickListener);
                }
                if (cachedMessageItem.isSms()) {
                    contextMenu.add(0, 24, 0, (int) R.string.copy_message_text).setOnMenuItemClickListener(msgListMenuClickListener);
                }
                contextMenu.add(0, 17, 0, (int) R.string.view_message_details).setOnMenuItemClickListener(msgListMenuClickListener);
                contextMenu.add(0, 18, 0, (int) R.string.delete_message).setOnMenuItemClickListener(msgListMenuClickListener);
                if (cachedMessageItem.mDeliveryReport || cachedMessageItem.mReadReport) {
                    contextMenu.add(0, 20, 0, (int) R.string.view_delivery_report).setOnMenuItemClickListener(msgListMenuClickListener);
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public MessageListView mMsgListView;
    /* access modifiers changed from: private */
    public boolean mMultiRecipient = false;
    /* access modifiers changed from: private */
    public MusicAttachmentView mMusicAttachmentView;
    /* access modifiers changed from: private */
    public boolean mNeedReset = false;
    private LinearLayout mPanelTab;
    /* access modifiers changed from: private */
    public PopupWindow mPopSelectImg;
    /* access modifiers changed from: private */
    public PopupWindow mPopupWindow;
    SimpleAdapter mPopupWindowAdapter;
    ArrayList<HashMap<String, Object>> mPopupWindowMenuData;
    /* access modifiers changed from: private */
    public boolean mPossiblePendingNotification;
    private ImageView mQuickCall;
    /* access modifiers changed from: private */
    public RecipientsEditor mRecipientsEditor;
    private final View.OnCreateContextMenuListener mRecipientsMenuCreateListener = new View.OnCreateContextMenuListener() {
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            if (contextMenuInfo != null) {
                Contact contact = ((RecipientsEditor.RecipientContextMenuInfo) contextMenuInfo).recipient;
                RecipientsMenuClickListener recipientsMenuClickListener = new RecipientsMenuClickListener(contact);
                contextMenu.setHeaderTitle(contact.getName());
                if (contact.existsInDatabase()) {
                    contextMenu.add(0, 12, 0, (int) R.string.menu_view_contact).setOnMenuItemClickListener(recipientsMenuClickListener);
                } else if (ComposeMessageActivity.this.canAddToContacts(contact)) {
                    contextMenu.add(0, 13, 0, (int) R.string.menu_add_to_contacts).setOnMenuItemClickListener(recipientsMenuClickListener);
                }
            }
        }
    };
    private final TextWatcher mRecipientsWatcher = new TextWatcher() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
         arg types: [java.util.List<java.lang.String>, int, int, android.content.Context]
         candidates:
          vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
          vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
          vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
        public void afterTextChanged(Editable editable) {
            if (!ComposeMessageActivity.this.isRecipientsEditorVisible()) {
                IllegalStateException illegalStateException = new IllegalStateException("afterTextChanged called with invisible mRecipientsEditor");
                Log.e(ComposeMessageActivity.TAG, "RecipientsWatcher called incorrectly", illegalStateException);
                throw illegalStateException;
            }
            ComposeMessageActivity.this.mWorkingMessage.setWorkingRecipients(ComposeMessageActivity.this.mRecipientsEditor.getNumbers());
            ComposeMessageActivity.this.mConversation.setRecipients(ContactList.getByNumbers(ComposeMessageActivity.this.mRecipientsEditor.getNumbers(), false, true, ComposeMessageActivity.this.getApplicationContext()));
            ComposeMessageActivity.this.mWorkingMessage.setHasEmail(ComposeMessageActivity.this.mRecipientsEditor.containsEmail(), true);
            ComposeMessageActivity.this.checkForTooManyRecipients();
            int length = editable.length() - 1;
            while (true) {
                if (length < 0) {
                    break;
                }
                char charAt = editable.charAt(length);
                if (charAt == ComposeMessageActivity.MENU_MULTI_MODE) {
                    length--;
                } else if (charAt == ',') {
                    ComposeMessageActivity.this.updateWindowTitle();
                    ComposeMessageActivity.this.initializeContactInfo();
                }
            }
            ComposeMessageActivity.this.updateSendButtonState();
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ComposeMessageActivity.this.onUserInteraction();
        }
    };
    private ImageView mRemoveImageAttachmentButton;
    Runnable mResetMessageRunnable = new Runnable() {
        public void run() {
            ComposeMessageActivity.this.resetMessage();
        }
    };
    private RemoteResourceManagerObserver mResourcesObserver;
    /* access modifiers changed from: private */
    public RemoteResourceManager mRrm;
    int mScreenHeight;
    int mScreenWidth;
    private TabHost mSelTabHost;
    SimpleAdapter mSelectImgAdapter;
    ArrayList<HashMap<String, Object>> mSelectImgDate;
    private ListView mSelectImgList;
    private Button mSelectMusicButon;
    /* access modifiers changed from: private */
    public Button mSendButton;
    /* access modifiers changed from: private */
    public boolean mSentMessage;
    private final View.OnKeyListener mSubjectKeyListener = new View.OnKeyListener() {
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() != 0) {
                return false;
            }
            if (i != 67 || ComposeMessageActivity.this.mSubjectTextEditor.length() != 0) {
                return false;
            }
            ComposeMessageActivity.this.mWorkingMessage.setSubject(null, true);
            return true;
        }
    };
    /* access modifiers changed from: private */
    public EditText mSubjectTextEditor;
    private Button mTabActivityImageView;
    private Button mTabPhotoImageView;
    private Button mTabSmileImageView;
    private Button mTabSmsImageView;
    private TextView mTextCounter;
    /* access modifiers changed from: private */
    public EditText mTextEditor;
    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ComposeMessageActivity.this.onUserInteraction();
            ComposeMessageActivity.this.mWorkingMessage.setText(charSequence);
            ComposeMessageActivity.this.updateSendButtonState();
            ComposeMessageActivity.this.updateCounter(charSequence, i, i2, i3);
        }
    };
    private GridView mTitleGridView;
    /* access modifiers changed from: private */
    public String mTmpUploadImagePath;
    /* access modifiers changed from: private */
    public boolean mToastForDraftSave = true;
    private View mTopPanel;
    /* access modifiers changed from: private */
    public AlertDialog mUploadImageDlg;
    /* access modifiers changed from: private */
    public UploadImageJob mUploadImageJob;
    /* access modifiers changed from: private */
    public VoteAttachmentView mVoteAttachmentView;
    /* access modifiers changed from: private */
    public VoteItem mVoteItem;
    private boolean mWaitingForSubActivity;
    /* access modifiers changed from: private */
    public WorkingMessage mWorkingMessage;
    private GridView menuGrid;
    /* access modifiers changed from: private */
    public String musicPlug = "";
    public View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            if (ComposeMessageActivity.this.mGridView != null && ComposeMessageActivity.this.mGridView.getVisibility() == 0) {
                ComposeMessageActivity.this.mGridView.setVisibility(8);
            }
        }
    };
    private PopupWindow popup;
    /* access modifiers changed from: private */
    public int titleIndex;
    private GridView toolbarGrid;
    /* access modifiers changed from: private */
    public TopMusicService topMusicService;
    /* access modifiers changed from: private */
    public long vote_cli_id = 0;

    private final class BackgroundQueryHandler extends AsyncQueryHandler {
        public BackgroundQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onDeleteComplete(int i, Object obj, int i2) {
            switch (i) {
                case ConversationListActivity.DELETE_CONVERSATION_TOKEN /*1801*/:
                case ComposeMessageActivity.DELETE_MESSAGE_TOKEN /*9700*/:
                    MessagingNotification.updateNewMessageIndicator(ComposeMessageActivity.this);
                    ComposeMessageActivity.this.updateSendFailedNotification();
                    break;
            }
            if (i == 1801) {
                ComposeMessageActivity.this.mWorkingMessage.discard();
                Conversation.init(ComposeMessageActivity.this);
                ComposeMessageActivity.this.finish();
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onQueryComplete(int r10, java.lang.Object r11, android.database.Cursor r12) {
            /*
                r9 = this;
                r8 = 0
                r6 = -1
                r5 = 1
                r4 = -1
                switch(r10) {
                    case 1802: goto L_0x0067;
                    case 9527: goto L_0x0009;
                    default: goto L_0x0008;
                }
            L_0x0008:
                return
            L_0x0009:
                vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                android.content.Intent r0 = r0.getIntent()
                java.lang.String r1 = "select_id"
                long r0 = r0.getLongExtra(r1, r6)
                int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
                if (r2 == 0) goto L_0x0093
                r12.moveToPosition(r4)
            L_0x001c:
                boolean r2 = r12.moveToNext()
                if (r2 == 0) goto L_0x0093
                long r2 = r12.getLong(r5)
                int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
                if (r2 != 0) goto L_0x001c
                int r0 = r12.getPosition()
            L_0x002e:
                vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this
                com.android.mms.ui.MessageListAdapter r1 = r1.mMsgListAdapter
                r1.changeCursor(r12)
                if (r0 == r4) goto L_0x0042
                vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this
                com.android.mms.ui.MessageListView r1 = r1.mMsgListView
                r1.setSelection(r0)
            L_0x0042:
                int r0 = r12.getCount()
                if (r0 != 0) goto L_0x005d
                vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                boolean r0 = r0.isRecipientsEditorVisible()
                if (r0 != 0) goto L_0x005d
                vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                boolean r0 = r0.mSentMessage
                if (r0 != 0) goto L_0x005d
                vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                r0.initRecipientsEditor()
            L_0x005d:
                vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                android.widget.EditText r0 = r0.mTextEditor
                r0.requestFocus()
                goto L_0x0008
            L_0x0067:
                java.lang.Long r11 = (java.lang.Long) r11
                long r0 = r11.longValue()
                vc.lx.sms.ui.ConversationListActivity$DeleteThreadListener r2 = new vc.lx.sms.ui.ConversationListActivity$DeleteThreadListener
                vc.lx.sms.ui.ComposeMessageActivity r3 = vc.lx.sms.ui.ComposeMessageActivity.this
                vc.lx.sms.ui.ComposeMessageActivity$BackgroundQueryHandler r3 = r3.mBackgroundQueryHandler
                vc.lx.sms.ui.ComposeMessageActivity r4 = vc.lx.sms.ui.ComposeMessageActivity.this
                r2.<init>(r0, r3, r4)
                int r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
                if (r0 != 0) goto L_0x008f
                r0 = r5
            L_0x007f:
                if (r12 == 0) goto L_0x0091
                int r1 = r12.getCount()
                if (r1 <= 0) goto L_0x0091
                r1 = r5
            L_0x0088:
                vc.lx.sms.ui.ComposeMessageActivity r3 = vc.lx.sms.ui.ComposeMessageActivity.this
                vc.lx.sms.ui.ConversationListActivity.confirmDeleteThreadDialog(r2, r0, r1, r3)
                goto L_0x0008
            L_0x008f:
                r0 = r8
                goto L_0x007f
            L_0x0091:
                r1 = r8
                goto L_0x0088
            L_0x0093:
                r0 = r4
                goto L_0x002e
            */
            throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.BackgroundQueryHandler.onQueryComplete(int, java.lang.Object, android.database.Cursor):void");
        }
    }

    private class CancelSendingListener implements DialogInterface.OnClickListener {
        private CancelSendingListener() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (ComposeMessageActivity.this.isRecipientsEditorVisible()) {
                ComposeMessageActivity.this.mRecipientsEditor.requestFocus();
            }
        }
    }

    private class DeleteMessageListener implements DialogInterface.OnClickListener {
        private final boolean mDeleteLocked;
        private final Uri mDeleteUri;

        public DeleteMessageListener(long j, String str, boolean z) {
            if (Phone.APN_TYPE_MMS.equals(str)) {
                this.mDeleteUri = ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, j);
            } else {
                this.mDeleteUri = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, j);
            }
            this.mDeleteLocked = z;
        }

        public DeleteMessageListener(Uri uri, boolean z) {
            this.mDeleteUri = uri;
            this.mDeleteLocked = z;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ComposeMessageActivity.this.mBackgroundQueryHandler.startDelete(ComposeMessageActivity.DELETE_MESSAGE_TOKEN, null, this.mDeleteUri, this.mDeleteLocked ? null : "locked=0", null);
        }
    }

    private class DiscardDraftListener implements DialogInterface.OnClickListener {
        boolean mReturnToHome;

        public DiscardDraftListener(boolean z) {
            this.mReturnToHome = z;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (this.mReturnToHome) {
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.setFlags(67108864);
                intent.addCategory("android.intent.category.HOME");
                ComposeMessageActivity.this.startActivity(intent);
                return;
            }
            ComposeMessageActivity.this.mWorkingMessage.discard();
            ComposeMessageActivity.this.finish();
        }
    }

    class ForwardMusicTask extends AbstractAsyncTask {
        private String forwardMusicJsonStr = createForwardMusciJson();

        public ForwardMusicTask(Context context) {
            super(context);
            this.mEnginehost = this.mContext.getString(R.string.dna_engine_host_version_b);
        }

        public String createForwardMusciJson() {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            int i = 0;
            while (i < ComposeMessageActivity.this.mConversation.getRecipients().getNumbers().length) {
                try {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("name", ((Contact) ComposeMessageActivity.this.mConversation.getRecipients().get(i)).getName());
                    jSONObject2.put("number", ((Contact) ComposeMessageActivity.this.mConversation.getRecipients().get(i)).getNumber());
                    jSONArray.put(jSONObject2);
                    i++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            jSONObject.put("friends", jSONArray);
            return jSONObject.toString();
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.forwardMusicAction(this.mEnginehost, this.mContext, ComposeMessageActivity.this.musicPlug, this.forwardMusicJsonStr);
        }

        public String getTag() {
            return "ForwardMusicTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof SongItem)) {
                SongItem songItem = (SongItem) miguType;
                ComposeMessageActivity.this.topMusicService.updateMusicMaster(ComposeMessageActivity.this.musicPlug);
                for (int i = 0; i < songItem.forwardSongs.size(); i++) {
                    if (songItem.forwardSongs.get(i).url != null) {
                        songItem.plug = songItem.forwardSongs.get(i).url.substring(songItem.forwardSongs.get(i).url.lastIndexOf("/") + 1);
                        ComposeMessageActivity.this.topMusicService.insert(songItem, 3);
                    }
                }
                String unused = ComposeMessageActivity.this.musicPlug = "";
                ComposeMessageActivity.this.removeRecipientsListeners();
                ArrayList arrayList = new ArrayList();
                String[] numbers = ComposeMessageActivity.this.mConversation.getRecipients().getNumbers();
                if (ComposeMessageActivity.this.mTextEditor.getText() == null || "".equals(ComposeMessageActivity.this.mTextEditor.getText()) || numbers.length <= 0) {
                    for (int i2 = 0; i2 < numbers.length; i2++) {
                        for (int i3 = 0; i3 < songItem.forwardSongs.size(); i3++) {
                            if (numbers[i2].equals(songItem.forwardSongs.get(i3).number)) {
                                arrayList.add(ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumber(numbers[i2]) + "," + ComposeMessageActivity.this.getString(R.string.music_add_str) + songItem.forwardSongs.get(i3).url);
                            }
                        }
                    }
                    ComposeMessageActivity.this.sendMusicMessage(true, arrayList);
                } else {
                    for (int i4 = 0; i4 < numbers.length; i4++) {
                        for (int i5 = 0; i5 < songItem.forwardSongs.size(); i5++) {
                            if (numbers[i4].equals(songItem.forwardSongs.get(i5).number)) {
                                arrayList.add(((Object) ComposeMessageActivity.this.mTextEditor.getText()) + ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumber(numbers[i4]) + "," + ComposeMessageActivity.this.getString(R.string.music_add_str) + songItem.forwardSongs.get(i5).url);
                            }
                        }
                    }
                    ComposeMessageActivity.this.sendMusicMessage(true, arrayList);
                }
                ((RelativeLayout) ComposeMessageActivity.this.findViewById(R.id.music_panel)).setVisibility(8);
                Util.logEvent(PrefsUtil.EVENT_MUSIC_SEND, new NameValuePair[0]);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new ForwardSongParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    public class MenuTitleAdapter extends BaseAdapter {
        private Context mContext;
        private Button[] mTitleView;

        public MenuTitleAdapter(Context context, int[] iArr, int i, int i2) {
            this.mContext = context;
            this.mTitleView = new Button[iArr.length];
            for (int i3 = 0; i3 < iArr.length; i3++) {
                this.mTitleView[i3] = new Button(this.mContext);
                this.mTitleView[i3].setBackgroundResource(iArr[i3]);
            }
        }

        public int getCount() {
            return this.mTitleView.length;
        }

        public Object getItem(int i) {
            return this.mTitleView[i];
        }

        public long getItemId(int i) {
            return (long) this.mTitleView[i].getId();
        }

        public View getView(final int i, View view, ViewGroup viewGroup) {
            View view2 = view == null ? this.mTitleView[i] : view;
            if (i == 4) {
                Button unused = ComposeMessageActivity.this.mSendButton = (Button) view;
            }
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ComposeMessageActivity.this.onChangeItem(view, i);
                }
            });
            return view2;
        }
    }

    private final class MsgListMenuClickListener implements MenuItem.OnMenuItemClickListener {
        private MsgListMenuClickListener() {
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            Cursor cursor = ComposeMessageActivity.this.mMsgListAdapter.getCursor();
            String string = cursor.getString(0);
            long j = cursor.getLong(1);
            MessageItem access$800 = ComposeMessageActivity.this.getMessageItem(string, j);
            if (access$800 == null) {
                return false;
            }
            switch (menuItem.getItemId()) {
                case 14:
                    ComposeMessageActivity.this.editMessageItem(access$800);
                    ComposeMessageActivity.this.drawBottomPanel();
                    return true;
                case 15:
                case 19:
                case 22:
                case 23:
                case ComposeMessageActivity.REQUEST_CODE_VOTE_DETAIL /*26*/:
                case ComposeMessageActivity.MENU_ADD_ADDRESS_TO_CONTACTS /*27*/:
                default:
                    return false;
                case 16:
                    MessageUtils.viewMmsMessageAttachment(ComposeMessageActivity.this, ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, j), null);
                    return true;
                case 17:
                    new AlertDialog.Builder(ComposeMessageActivity.this).setTitle((int) R.string.message_details_title).setMessage(MessageUtils.getMessageDetails(ComposeMessageActivity.this, cursor, access$800.mMessageSize)).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).setCancelable(true).show();
                    return true;
                case 18:
                    ComposeMessageActivity.this.confirmDeleteDialog(new DeleteMessageListener(access$800.mMessageUri, access$800.mLocked), access$800.mLocked);
                    return true;
                case 20:
                    ComposeMessageActivity.this.showDeliveryReport(j, string);
                    return true;
                case 21:
                    ComposeMessageActivity.this.forwardMessage(access$800);
                    return true;
                case 24:
                    ComposeMessageActivity.this.copyToClipboard(access$800.mBody);
                    return true;
                case 25:
                    Toast.makeText(ComposeMessageActivity.this, ComposeMessageActivity.this.copyMedia(j) ? R.string.copy_to_sdcard_success : R.string.copy_to_sdcard_fail, 0).show();
                    return true;
                case ComposeMessageActivity.MENU_LOCK_MESSAGE /*28*/:
                    ComposeMessageActivity.this.lockMessage(access$800, true);
                    return true;
                case ComposeMessageActivity.MENU_UNLOCK_MESSAGE /*29*/:
                    ComposeMessageActivity.this.lockMessage(access$800, false);
                    return true;
                case ComposeMessageActivity.MENU_COPY_TO_DRM_PROVIDER /*30*/:
                    Toast.makeText(ComposeMessageActivity.this, ComposeMessageActivity.this.getDrmMimeSavedStringRsrc(j, ComposeMessageActivity.this.copyToDrmProvider(j)), 0).show();
                    return true;
                case 31:
                    ComposeMessageActivity.this.editMessageItem(access$800);
                    ComposeMessageActivity.this.drawBottomPanel();
                    ComposeMessageActivity.this.confirmSendMessageIfNeeded();
                    return true;
            }
        }
    }

    private static class PduBodyCache {
        private static PduBody mLastPduBody;
        private static Uri mLastUri;

        private PduBodyCache() {
        }

        public static PduBody getPduBody(Context context, Uri uri) {
            if (uri.equals(mLastUri)) {
                return mLastPduBody;
            }
            try {
                mLastPduBody = SlideshowModel.getPduBody(context, uri);
                mLastUri = uri;
                return mLastPduBody;
            } catch (MmsException e) {
                Log.e(ComposeMessageActivity.TAG, e.getMessage(), e);
                return null;
            }
        }
    }

    private final class RecipientsMenuClickListener implements MenuItem.OnMenuItemClickListener {
        private final Contact mRecipient;

        RecipientsMenuClickListener(Contact contact) {
            this.mRecipient = contact;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case 12:
                    Intent intent = new Intent("android.intent.action.VIEW", this.mRecipient.getUri());
                    intent.setFlags(524288);
                    ComposeMessageActivity.this.startActivity(intent);
                    return true;
                case 13:
                    ComposeMessageActivity.this.startActivity(ConversationListActivity.createAddContactIntent(this.mRecipient.getNumber()));
                    return true;
                default:
                    return false;
            }
        }
    }

    private class RemoteResourceManagerObserver implements Observer {
        private RemoteResourceManagerObserver() {
        }

        public void update(Observable observable, final Object obj) {
            ComposeMessageActivity.this.mHandler.post(new Runnable() {
                /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
                    return;
                 */
                /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r3 = this;
                        vc.lx.sms.ui.ComposeMessageActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ComposeMessageActivity.RemoteResourceManagerObserver.this
                        vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                        com.android.mms.ui.MessageListAdapter r0 = r0.mMsgListAdapter
                        r0.notifyDataSetChanged()
                        java.lang.Object r0 = r4
                        boolean r0 = r0 instanceof android.net.Uri
                        if (r0 == 0) goto L_0x005a
                        vc.lx.sms.ui.ComposeMessageActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ComposeMessageActivity.RemoteResourceManagerObserver.this
                        vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                        com.android.mms.data.WorkingMessage r0 = r0.mWorkingMessage
                        vc.lx.sms.cmcc.http.data.SmsImage r0 = r0.getImageItem()
                        if (r0 == 0) goto L_0x005a
                        java.lang.String r0 = r0.mThumb
                        android.net.Uri r0 = android.net.Uri.parse(r0)
                        vc.lx.sms.ui.ComposeMessageActivity$RemoteResourceManagerObserver r1 = vc.lx.sms.ui.ComposeMessageActivity.RemoteResourceManagerObserver.this
                        vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this
                        vc.lx.sms.caches.RemoteResourceManager r1 = r1.mRrm
                        boolean r1 = r1.exists(r0)
                        if (r1 == 0) goto L_0x005a
                        vc.lx.sms.ui.ComposeMessageActivity$RemoteResourceManagerObserver r1 = vc.lx.sms.ui.ComposeMessageActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        vc.lx.sms.caches.RemoteResourceManager r1 = r1.mRrm     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        java.io.InputStream r0 = r1.getInputStream(r0)     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        vc.lx.sms.ui.ComposeMessageActivity$RemoteResourceManagerObserver r1 = vc.lx.sms.ui.ComposeMessageActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        android.widget.ImageView r1 = r1.mImageAttachment     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        r2 = 0
                        r1.setVisibility(r2)     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        vc.lx.sms.ui.ComposeMessageActivity$RemoteResourceManagerObserver r1 = vc.lx.sms.ui.ComposeMessageActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        android.widget.ImageView r1 = r1.mImageAttachment     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                        r1.setImageBitmap(r0)     // Catch:{ IOException -> 0x005d, all -> 0x005b }
                    L_0x005a:
                        return
                    L_0x005b:
                        r0 = move-exception
                        throw r0
                    L_0x005d:
                        r0 = move-exception
                        goto L_0x005a
                    */
                    throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.RemoteResourceManagerObserver.AnonymousClass1.run():void");
                }
            });
        }
    }

    private class SendIgnoreInvalidRecipientListener implements DialogInterface.OnClickListener {
        private SendIgnoreInvalidRecipientListener() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            ComposeMessageActivity.this.sendMessage(true);
        }
    }

    private class UploadImageFailedListener implements DialogInterface.OnClickListener {
        private UploadImageFailedListener() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            if (i == -1) {
                ComposeMessageActivity.this.finish();
            } else if (i == -2) {
                ComposeMessageActivity.this.mWorkingMessage.send();
                boolean unused = ComposeMessageActivity.this.mSentMessage = true;
                ComposeMessageActivity.this.addRecipientsListeners();
                ComposeMessageActivity.this.bindToContactHeader(ComposeMessageActivity.this.mConversation.getRecipients());
                ComposeMessageActivity.this.mMsgListAdapter.setMultiRecipient(ComposeMessageActivity.this.mMultiRecipient);
                ComposeMessageActivity.this.mMsgListAdapter.notifyDataSetChanged();
            }
        }
    }

    class VoteForwardTask extends AbstractAsyncTask {
        String jsonString;
        private int page;
        String plug_id;
        String vote_id;

        public VoteForwardTask(Context context, int i, String str, String str2, String str3) {
            super(context);
            this.page = i;
            this.jsonString = str;
            this.vote_id = str2;
            this.plug_id = str3;
            this.mEnginehost = this.mContext.getString(R.string.dna_engine_host_version_b);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.voteForwardAction(this.mEnginehost, this.mContext, this.jsonString, this.plug_id, this.vote_id);
        }

        public String getTag() {
            return "VoteForwardTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                Util.SetLastTimeOfRecommendingMusicValue(ComposeMessageActivity.this.getApplicationContext(), Util.getRelativeDateSpanString(System.currentTimeMillis(), ComposeMessageActivity.this.getApplicationContext()).toString());
                VoteItem voteItem = (VoteItem) miguType;
                String[] numbers = ComposeMessageActivity.this.mConversation.getRecipients().getNumbers();
                String str = "";
                for (int i = 0; i < numbers.length; i++) {
                    String nameByNumberAllowNull = ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumberAllowNull(numbers[i]);
                    if (nameByNumberAllowNull == null) {
                        nameByNumberAllowNull = ComposeMessageActivity.this.getApplicationContext().getString(R.string.vote_number) + numbers[i];
                    }
                    str = str.equals("") ? str + nameByNumberAllowNull : str + "," + nameByNumberAllowNull;
                }
                ContentValues contentValues = new ContentValues();
                if (ComposeMessageActivity.this.mVoteItem.parent_id == 0) {
                    contentValues.put("tracer_flag", "1");
                }
                contentValues.put("contact_name", str);
                if ((ComposeMessageActivity.this.mVoteItem != null) && (ComposeMessageActivity.this.mVoteItem.cli_id > 0)) {
                    ComposeMessageActivity.this.getApplicationContext().getContentResolver().update(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, contentValues, " _id = ? ", new String[]{String.valueOf(ComposeMessageActivity.this.mVoteItem.cli_id)});
                }
                for (int i2 = 0; i2 < voteItem.forwardBook.size(); i2++) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("title", ComposeMessageActivity.this.mVoteItem.title);
                    contentValues2.put("phone_num", voteItem.forwardBook.get(i2).number);
                    if (ComposeMessageActivity.this.mVoteItem.parent_id == 0) {
                        contentValues2.put("parent_id", Long.valueOf(ComposeMessageActivity.this.mVoteItem.cli_id));
                    } else {
                        contentValues2.put("parent_id", Integer.valueOf(ComposeMessageActivity.this.mVoteItem.parent_id));
                    }
                    contentValues2.put(SmsSqliteHelper.PLUG, voteItem.forwardBook.get(i2).url);
                    ComposeMessageActivity.this.getApplicationContext().getContentResolver().insert(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, contentValues2);
                }
                ArrayList arrayList = new ArrayList();
                String[] numbers2 = ComposeMessageActivity.this.mConversation.getRecipients().getNumbers();
                if (ComposeMessageActivity.this.mTextEditor.getText() == null || ComposeMessageActivity.this.mTextEditor.getText().equals("")) {
                    for (int i3 = 0; i3 < numbers2.length; i3++) {
                        for (int i4 = 0; i4 < voteItem.forwardBook.size(); i4++) {
                            if (numbers2[i3].equals(voteItem.forwardBook.get(i4).number)) {
                                String nameByNumber = ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumber(voteItem.forwardBook.get(i4).number);
                                arrayList.add(((nameByNumber == null || nameByNumber.equals("")) ? "" : String.format(ComposeMessageActivity.this.getApplicationContext().getString(R.string.vote_message_head), nameByNumber)) + " " + voteItem.forwardBook.get(i4).url);
                            }
                        }
                    }
                    ComposeMessageActivity.this.sendVoteMessage(true, arrayList);
                } else {
                    for (int i5 = 0; i5 < numbers2.length; i5++) {
                        for (int i6 = 0; i6 < voteItem.forwardBook.size(); i6++) {
                            if (numbers2[i5].equals(voteItem.forwardBook.get(i6).number)) {
                                String nameByNumber2 = ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumber(voteItem.forwardBook.get(i6).number);
                                arrayList.add(((Object) ComposeMessageActivity.this.mTextEditor.getText()) + " " + ((nameByNumber2 == null || nameByNumber2.equals("")) ? "" : String.format(ComposeMessageActivity.this.getApplicationContext().getString(R.string.vote_message_head), nameByNumber2)) + " " + voteItem.forwardBook.get(i6).url);
                            }
                        }
                    }
                    ComposeMessageActivity.this.sendVoteMessage(true, arrayList);
                }
                ComposeMessageActivity.this.mTextEditor.setText("");
                ComposeMessageActivity.this.deleteVoteAttachment();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new VoterForwardParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class VoteGetTask extends AbstractAsyncTask {
        private int page;
        long parent_id;
        String plug_id;

        public VoteGetTask(Context context, int i, String str, long j) {
            super(context);
            this.page = i;
            this.plug_id = str;
            this.parent_id = j;
            this.mEnginehost = this.mContext.getString(R.string.dna_vote_engine_host_version_b);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.voteSearchByPlug(this.mEnginehost, this.mContext, this.plug_id);
        }

        public String getTag() {
            return "VoteGetTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                Util.SetLastTimeOfRecommendingMusicValue(ComposeMessageActivity.this.getApplicationContext(), Util.getRelativeDateSpanString(System.currentTimeMillis(), ComposeMessageActivity.this.getApplicationContext()).toString());
                VoteItem voteItem = (VoteItem) miguType;
                ContentValues contentValues = new ContentValues();
                contentValues.put("service_id", voteItem.service_id);
                contentValues.put("parent_id", Long.valueOf(this.parent_id));
                contentValues.put("title", voteItem.title);
                contentValues.put("counter", Integer.valueOf(voteItem.counter));
                ComposeMessageActivity.this.getApplicationContext().getContentResolver().notifyChange(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null);
                ComposeMessageActivity.this.getApplicationContext().getContentResolver().update(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, contentValues, " plug = ?", new String[]{Util.getFullShortenVoteUrl(ComposeMessageActivity.this.getApplicationContext(), this.plug_id)});
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new VoteSingleGetParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class VoteSendTask extends AbstractAsyncTask {
        public VoteSendTask(Context context, int i) {
            super(context);
            this.mEnginehost = this.mContext.getString(R.string.dna_engine_host_version_b);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.voteCreateAction(this.mEnginehost, this.mContext, ComposeMessageActivity.this.mVoteItem.jsonString);
        }

        public String getTag() {
            return "VoteSendTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                Util.SetLastTimeOfRecommendingMusicValue(ComposeMessageActivity.this.getApplicationContext(), Util.getRelativeDateSpanString(System.currentTimeMillis(), ComposeMessageActivity.this.getApplicationContext()).toString());
                VoteItem voteItem = (VoteItem) miguType;
                String[] numbers = ComposeMessageActivity.this.mConversation.getRecipients().getNumbers();
                String str = "";
                for (int i = 0; i < numbers.length; i++) {
                    String nameByNumberAllowNull = ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumberAllowNull(numbers[i]);
                    if (nameByNumberAllowNull == null) {
                        nameByNumberAllowNull = ComposeMessageActivity.this.getApplicationContext().getString(R.string.vote_number) + numbers[i];
                    }
                    str = str.equals("") ? str + nameByNumberAllowNull : str + "," + nameByNumberAllowNull;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("service_id", voteItem.service_id);
                contentValues.put(SmsSqliteHelper.PLUG, voteItem.plug);
                contentValues.put("tracer_flag", "1");
                contentValues.put("contact_name", str);
                if ((ComposeMessageActivity.this.mVoteItem != null) && (ComposeMessageActivity.this.mVoteItem.cli_id > 0)) {
                    ComposeMessageActivity.this.getApplicationContext().getContentResolver().update(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, contentValues, " _id = ? ", new String[]{String.valueOf(ComposeMessageActivity.this.mVoteItem.cli_id)});
                }
                for (int i2 = 0; i2 < voteItem.forwardBook.size(); i2++) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("title", ComposeMessageActivity.this.mVoteItem.title);
                    contentValues2.put("phone_num", voteItem.forwardBook.get(i2).number);
                    contentValues2.put("parent_id", Long.valueOf(ComposeMessageActivity.this.mVoteItem.cli_id));
                    contentValues2.put(SmsSqliteHelper.PLUG, voteItem.forwardBook.get(i2).url);
                    ComposeMessageActivity.this.getApplicationContext().getContentResolver().insert(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, contentValues2);
                }
                for (int i3 = 0; i3 < voteItem.options.size(); i3++) {
                    ContentValues contentValues3 = new ContentValues();
                    contentValues3.put("display_order", Integer.valueOf(i3 + 1));
                    contentValues3.put("service_id", voteItem.options.get(i3).service_id);
                    ComposeMessageActivity.this.getApplicationContext().getContentResolver().update(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, contentValues3, " _id = ? ", new String[]{String.valueOf(voteItem.options.get(i3).cli_id)});
                }
                ArrayList arrayList = new ArrayList();
                String[] numbers2 = ComposeMessageActivity.this.mConversation.getRecipients().getNumbers();
                if (ComposeMessageActivity.this.mTextEditor.getText() == null || ComposeMessageActivity.this.mTextEditor.getText().equals("")) {
                    for (int i4 = 0; i4 < numbers2.length; i4++) {
                        for (int i5 = 0; i5 < voteItem.forwardBook.size(); i5++) {
                            if (numbers2[i4].equals(voteItem.forwardBook.get(i5).number)) {
                                String nameByNumber = ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumber(voteItem.forwardBook.get(i5).number);
                                arrayList.add(((nameByNumber == null || nameByNumber.equals("")) ? "" : String.format(ComposeMessageActivity.this.getApplicationContext().getString(R.string.vote_message_head), nameByNumber)) + " " + voteItem.forwardBook.get(i5).url);
                            }
                        }
                    }
                    ComposeMessageActivity.this.sendVoteMessage(true, arrayList);
                } else {
                    for (int i6 = 0; i6 < numbers2.length; i6++) {
                        for (int i7 = 0; i7 < voteItem.forwardBook.size(); i7++) {
                            if (numbers2[i6].equals(voteItem.forwardBook.get(i7).number)) {
                                String nameByNumber2 = ComposeMessageActivity.this.mConversation.getRecipients().getNameByNumber(voteItem.forwardBook.get(i7).number);
                                arrayList.add(((Object) ComposeMessageActivity.this.mTextEditor.getText()) + " " + ((nameByNumber2 == null || nameByNumber2.equals("")) ? "" : String.format(ComposeMessageActivity.this.getApplicationContext().getString(R.string.vote_message_head), nameByNumber2)) + " " + voteItem.forwardBook.get(i7).url);
                            }
                        }
                    }
                    ComposeMessageActivity.this.sendVoteMessage(true, arrayList);
                }
                ComposeMessageActivity.this.mTextEditor.setText("");
                ComposeMessageActivity.this.deleteVoteAttachment();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new VoteParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public final void addCallAndContactMenuItems(ContextMenu contextMenu, MsgListMenuClickListener msgListMenuClickListener, MessageItem messageItem) {
        String str;
        String str2;
        boolean z;
        StringBuilder sb = new StringBuilder();
        if (messageItem.mBoxId == 1) {
            sb.append(messageItem.mAddress + ": ");
        }
        sb.append(messageItem.mBody);
        SpannableString spannableString = new SpannableString(sb.toString());
        Linkify.addLinks(spannableString, 15);
        ArrayList<String> extractUris = MessageUtils.extractUris((URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class));
        while (extractUris.size() > 0) {
            String remove = extractUris.remove(0);
            while (extractUris.contains(remove)) {
                extractUris.remove(remove);
            }
            int indexOf = remove.indexOf(":");
            if (indexOf >= 0) {
                String substring = remove.substring(0, indexOf);
                str = remove.substring(indexOf + 1);
                str2 = substring;
            } else {
                str = remove;
                str2 = null;
            }
            if ("mailto".equalsIgnoreCase(str2)) {
                String replace = getString(R.string.menu_send_email).replace("%s", str);
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("mailto:" + str));
                intent.setFlags(524288);
                contextMenu.add(0, 23, 0, replace).setOnMenuItemClickListener(msgListMenuClickListener).setIntent(intent);
                z = !haveEmailContact(str);
            } else if ("tel".equalsIgnoreCase(str2)) {
                String replace2 = getString(R.string.menu_call_back).replace("%s", str);
                Intent intent2 = new Intent("android.intent.action.CALL", Uri.parse("tel:" + str));
                intent2.setFlags(524288);
                contextMenu.add(0, 22, 0, replace2).setOnMenuItemClickListener(msgListMenuClickListener).setIntent(intent2);
                z = !isNumberInContacts(str);
            } else {
                z = false;
            }
            if (z) {
                contextMenu.add(0, (int) MENU_ADD_ADDRESS_TO_CONTACTS, 0, getString(R.string.menu_add_address_to_contacts).replace("%s", str)).setOnMenuItemClickListener(msgListMenuClickListener).setIntent(ConversationListActivity.createAddContactIntent(str));
            }
        }
    }

    /* access modifiers changed from: private */
    public void addPositionBasedMenuItems(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        try {
            addUriSpecificMenuItems(contextMenu, view, ((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo");
        }
    }

    /* access modifiers changed from: private */
    public void addRecipientsListeners() {
        getRecipients().addListeners(this);
    }

    private void addUriSpecificMenuItems(ContextMenu contextMenu, View view, int i) {
        Uri selectedUriFromMessageList = getSelectedUriFromMessageList((ListView) view, i);
        if (selectedUriFromMessageList != null) {
            Intent intent = new Intent((String) null, selectedUriFromMessageList);
            intent.addCategory("android.intent.category.SELECTED_ALTERNATIVE");
            contextMenu.addIntentOptions(0, 0, 0, new ComponentName(this, ComposeMessageActivity.class), null, intent, 0, null);
        }
    }

    private void applyImageFilters(Intent intent, int i) {
        String str;
        if (i == 19) {
            try {
                Cursor managedQuery = managedQuery(intent.getData(), new String[]{"_data"}, null, null, null);
                int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
                managedQuery.moveToFirst();
                str = managedQuery.getString(columnIndexOrThrow);
            } catch (Exception e) {
                Toast.makeText(this, getResources().getString(R.string.error_set_photo_load), 0).show();
                str = null;
            }
        } else {
            str = i == 20 ? Environment.getExternalStorageDirectory() + "/.tmp_camera_selection.jpg" : null;
        }
        try {
            String str2 = Environment.getExternalStorageDirectory() + "/.tmp_image_selection.jpg";
            ImageUtils.resampleImageAndSaveToNewLocation(str, str2);
            Intent intent2 = new Intent(getApplicationContext(), ImageFiltersActivity.class);
            intent2.putExtra("file", str2);
            startActivityForResult(intent2, 21);
        } catch (Exception e2) {
            Toast.makeText(this, getResources().getString(R.string.error_set_photo_load), 0).show();
        }
    }

    private void asyncDelete(Uri uri, String str, String[] strArr) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("asyncDelete %s where %s", uri, str);
        }
        SqliteWrapper.delete(getApplicationContext(), getContentResolver(), uri, str, strArr);
    }

    /* access modifiers changed from: private */
    public void asyncDeleteDraftSmsMessage(Conversation conversation) {
        long threadId = conversation.getThreadId();
        if (threadId > 0) {
            asyncDelete(ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, threadId), SMS_DRAFT_WHERE, null);
        }
    }

    /* access modifiers changed from: private */
    public void bindToContactHeader(ContactList contactList) {
        final String[] numbers = contactList.getNumbers();
        if (numbers.length == 1) {
            this.mQuickCall.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ComposeMessageActivity.this.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + numbers[0])));
                }
            });
        }
        for (int i = 0; i < contactList.size(); i++) {
            Log.i("TAG", ((Contact) contactList.get(i)).mName + "---" + ((Contact) contactList.get(i)).getNumber());
        }
        switch (contactList.size()) {
            case 0:
                this.mDisplayName.setText(this.mRecipientsEditor != null ? this.mRecipientsEditor.getText().toString() : "");
                this.mQuickCall.setVisibility(4);
                return;
            case 1:
                this.mDisplayName.setText(contactList.formatNames(""));
                this.mQuickCall.setVisibility(0);
                return;
            default:
                this.mMultiRecipient = true;
                this.mDisplayName.setText(contactList.formatNames(", "));
                this.mQuickCall.setVisibility(4);
                return;
        }
    }

    /* access modifiers changed from: private */
    public boolean canAddToContacts(Contact contact) {
        String name = contact.getName();
        if (!TextUtils.isEmpty(contact.getNumber()) && isSpecialChar(contact.getNumber().charAt(0))) {
            return false;
        }
        if (TextUtils.isEmpty(name) || !isSpecialChar(name.charAt(0))) {
            return Telephony.Mms.isEmailAddress(name) || Telephony.Mms.isPhoneNumber(name);
        }
        return false;
    }

    public static boolean cancelFailedDownloadNotification(Intent intent, Context context) {
        if (!MessagingNotification.isFailedToDownload(intent)) {
            return false;
        }
        MessagingNotification.cancelNotification(context, MessagingNotification.DOWNLOAD_FAILED_NOTIFICATION_ID);
        return true;
    }

    public static boolean cancelFailedToDeliverNotification(Intent intent, Context context) {
        if (!MessagingNotification.isFailedToDeliver(intent)) {
            return false;
        }
        MessagingNotification.cancelNotification(context, MessagingNotification.MESSAGE_FAILED_NOTIFICATION_ID);
        return true;
    }

    /* access modifiers changed from: private */
    public void checkForTooManyRecipients() {
        int recipientLimit = MmsConfig.getRecipientLimit();
        if (recipientLimit != Integer.MAX_VALUE) {
            int recipientCount = recipientCount();
            boolean z = recipientCount > recipientLimit;
            if (recipientCount != this.mLastRecipientCount) {
                this.mLastRecipientCount = recipientCount;
                if (z) {
                    Toast.makeText(this, getString(R.string.too_many_recipients, new Object[]{Integer.valueOf(recipientCount), Integer.valueOf(recipientLimit)}), 1).show();
                }
            }
        }
    }

    private void checkPendingNotification() {
        if (this.mPossiblePendingNotification && hasWindowFocus()) {
            this.mConversation.markAsRead();
            this.mPossiblePendingNotification = false;
        }
    }

    /* access modifiers changed from: private */
    public void confirmDeleteDialog(DialogInterface.OnClickListener onClickListener2, boolean z) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(z ? R.string.confirm_dialog_locked_title : R.string.confirm_dialog_title);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setMessage(z ? R.string.confirm_delete_locked_message : R.string.confirm_delete_message);
        builder.setPositiveButton((int) R.string.delete, onClickListener2);
        builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    /* access modifiers changed from: private */
    public void confirmDeleteThread(long j) {
        Conversation.startQueryHaveLockedMessages(this.mBackgroundQueryHandler, j, ConversationListActivity.HAVE_LOCKED_MESSAGES_TOKEN);
    }

    /* access modifiers changed from: private */
    public void confirmSendMessageIfNeeded() {
        String str;
        String str2;
        if (isRecipientsEditorVisible()) {
            boolean requiresMms = this.mWorkingMessage.requiresMms();
            if (this.mRecipientsEditor.hasInvalidRecipient(requiresMms)) {
                if (this.mRecipientsEditor.hasValidRecipient(requiresMms)) {
                    new AlertDialog.Builder(this).setIcon(17301543).setTitle(getResourcesString(R.string.has_invalid_recipient, this.mRecipientsEditor.formatInvalidNumbers(requiresMms))).setMessage((int) R.string.invalid_recipient_message).setPositiveButton((int) R.string.try_to_send, new SendIgnoreInvalidRecipientListener()).setNegativeButton((int) R.string.no, new CancelSendingListener()).show();
                } else {
                    new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.cannot_send_message).setMessage((int) R.string.cannot_send_message_reason).setPositiveButton((int) R.string.yes, new CancelSendingListener()).show();
                }
            } else if (this.vote_cli_id > 0) {
                if (!Util.checkNetWork(getApplicationContext())) {
                    String obj = (this.mTextEditor == null || this.mTextEditor.getText().equals("")) ? "" : this.mTextEditor.getText().toString();
                    if (!obj.equals("")) {
                        obj = obj + "\n";
                    }
                    if (this.mVoteItem != null && !this.mVoteItem.title.equals("")) {
                        str = str + this.mVoteItem.title;
                        for (int i = 0; i < this.mVoteItem.options.size(); i++) {
                            str = str + "\n" + this.mVoteItem.options.get(i).display_order + "." + this.mVoteItem.options.get(i).content;
                        }
                    }
                    sendSmsWorker(this.mConversation, str);
                    this.mTextEditor.setText("");
                    deleteVoteAttachment();
                } else if (this.mVoteItem.service_id == null || this.mVoteItem.service_id.equals("") || !getIntent().getBooleanExtra("forwarded_message", false)) {
                    this.mVoteItem.jsonString = createSendJsonObject().toString();
                    new VoteSendTask(getApplicationContext(), 1).execute(new Void[0]);
                } else {
                    JSONObject createForwardJsonObject = createForwardJsonObject();
                    this.mVoteItem.jsonString = createForwardJsonObject.toString();
                    Matcher matcher = Pattern.compile(getApplicationContext().getString(R.string.shorten_url_vote_reg), MENU_MULTI_MODE).matcher(this.mVoteItem.plug);
                    if (matcher != null && matcher.find()) {
                        new VoteForwardTask(getApplicationContext(), 1, createForwardJsonObject.toString(), this.mVoteItem.service_id, matcher.group(1)).execute(new Void[0]);
                    }
                }
            } else if (this.musicPlug == null || this.musicPlug.length() <= 0 || this.mWorkingMessage.getToSendImage() != null || this.mWorkingMessage.getImageItem() != null) {
                sendMessage(true);
            } else {
                new ForwardMusicTask(getApplicationContext()).execute(new Void[0]);
            }
        } else if (this.vote_cli_id > 0) {
            if (!Util.checkNetWork(getApplicationContext())) {
                String obj2 = (this.mTextEditor == null || this.mTextEditor.getText().equals("")) ? "" : this.mTextEditor.getText().toString();
                if (!obj2.equals("")) {
                    obj2 = obj2 + "\n";
                }
                if (this.mVoteItem != null && !this.mVoteItem.title.equals("")) {
                    str2 = str2 + this.mVoteItem.title;
                    for (int i2 = 0; i2 < this.mVoteItem.options.size(); i2++) {
                        str2 = str2 + "\n" + this.mVoteItem.options.get(i2).display_order + "." + this.mVoteItem.options.get(i2).content;
                    }
                }
                sendSmsWorker(this.mConversation, str2);
                this.mTextEditor.setText("");
                deleteVoteAttachment();
            } else if (this.mVoteItem.service_id == null || this.mVoteItem.service_id.equals("") || !getIntent().getBooleanExtra("forwarded_message", false)) {
                this.mVoteItem.jsonString = createSendJsonObject().toString();
                new VoteSendTask(getApplicationContext(), 1).execute(new Void[0]);
            } else {
                JSONObject createForwardJsonObject2 = createForwardJsonObject();
                this.mVoteItem.jsonString = createForwardJsonObject2.toString();
                Matcher matcher2 = Pattern.compile(getApplicationContext().getString(R.string.shorten_url_vote_reg), MENU_MULTI_MODE).matcher(this.mVoteItem.plug);
                if (matcher2 != null && matcher2.find()) {
                    new VoteForwardTask(getApplicationContext(), 1, createForwardJsonObject2.toString(), this.mVoteItem.service_id, matcher2.group(1)).execute(new Void[0]);
                }
            }
        } else if (this.musicPlug == null || this.musicPlug.length() <= 0 || this.mWorkingMessage.getToSendImage() != null || this.mWorkingMessage.getImageItem() != null) {
            sendMessage(true);
        } else {
            new ForwardMusicTask(getApplicationContext()).execute(new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public boolean copyMedia(long j) {
        PduBody pduBody = PduBodyCache.getPduBody(this, ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, j));
        if (pduBody == null) {
            return false;
        }
        int partsNum = pduBody.getPartsNum();
        boolean z = true;
        for (int i = 0; i < partsNum; i++) {
            PduPart part = pduBody.getPart(i);
            String str = new String(part.getContentType());
            if (ContentType.isImageType(str) || ContentType.isVideoType(str) || ContentType.isAudioType(str)) {
                z &= copyPart(part);
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00e7 A[SYNTHETIC, Splitter:B:48:0x00e7] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ec A[SYNTHETIC, Splitter:B:51:0x00ec] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x013b A[SYNTHETIC, Splitter:B:75:0x013b] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0140 A[SYNTHETIC, Splitter:B:78:0x0140] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean copyPart(com.google.android.mms.pdu.PduPart r16) {
        /*
            r15 = this;
            r13 = 0
            r12 = -1
            r9 = 0
            java.lang.String r11 = "IOException caught while closing stream"
            java.lang.String r10 = "Mms/compose"
            android.net.Uri r1 = r16.getDataUri()
            r3 = 0
            android.content.ContentResolver r2 = r15.mContentResolver     // Catch:{ IOException -> 0x0165, all -> 0x0136 }
            java.io.InputStream r2 = r2.openInputStream(r1)     // Catch:{ IOException -> 0x0165, all -> 0x0136 }
            boolean r1 = r2 instanceof java.io.FileInputStream     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            if (r1 == 0) goto L_0x016f
            r0 = r2
            java.io.FileInputStream r0 = (java.io.FileInputStream) r0     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r1 = r0
            byte[] r4 = r16.getName()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            if (r4 != 0) goto L_0x0024
            byte[] r4 = r16.getFilename()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
        L_0x0024:
            if (r4 != 0) goto L_0x002a
            byte[] r4 = r16.getContentLocation()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
        L_0x002a:
            java.lang.String r5 = new java.lang.String     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r5.<init>(r4)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r4 = "/sdcard/download/"
            java.lang.String r6 = "."
            int r6 = r5.indexOf(r6)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            if (r6 != r12) goto L_0x00a0
            java.lang.String r6 = new java.lang.String     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            byte[] r7 = r16.getContentType()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r6.<init>(r7)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            android.webkit.MimeTypeMap r7 = android.webkit.MimeTypeMap.getSingleton()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r6 = r7.getExtensionFromMimeType(r6)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r14 = r6
            r6 = r5
            r5 = r14
        L_0x004d:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r7.<init>()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.StringBuilder r4 = r7.append(r4)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.io.File r4 = r15.getUniqueDestination(r4, r5)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.io.File r5 = r4.getParentFile()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            boolean r6 = r5.exists()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            if (r6 != 0) goto L_0x00c6
            boolean r6 = r5.mkdirs()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            if (r6 != 0) goto L_0x00c6
            java.lang.String r1 = "Mms/compose"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r4.<init>()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r6 = "[MMS] copyPart: mkdirs for "
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r5 = r5.getPath()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r5 = " failed!"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            android.util.Log.e(r1, r4)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            if (r2 == 0) goto L_0x0099
            r2.close()     // Catch:{ IOException -> 0x00b2 }
        L_0x0099:
            if (r13 == 0) goto L_0x009e
            r3.close()     // Catch:{ IOException -> 0x00bc }
        L_0x009e:
            r1 = r9
        L_0x009f:
            return r1
        L_0x00a0:
            int r7 = r6 + 1
            int r8 = r5.length()     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            java.lang.String r7 = r5.substring(r7, r8)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r8 = 0
            java.lang.String r5 = r5.substring(r8, r6)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r6 = r5
            r5 = r7
            goto L_0x004d
        L_0x00b2:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x00bc:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x00c6:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r3.<init>(r4)     // Catch:{ IOException -> 0x016a, all -> 0x015a }
            r5 = 8000(0x1f40, float:1.121E-41)
            byte[] r5 = new byte[r5]     // Catch:{ IOException -> 0x00da, all -> 0x015e }
        L_0x00cf:
            int r6 = r1.read(r5)     // Catch:{ IOException -> 0x00da, all -> 0x015e }
            if (r6 == r12) goto L_0x00f1
            r7 = 0
            r3.write(r5, r7, r6)     // Catch:{ IOException -> 0x00da, all -> 0x015e }
            goto L_0x00cf
        L_0x00da:
            r1 = move-exception
            r14 = r3
            r3 = r2
            r2 = r14
        L_0x00de:
            java.lang.String r4 = "Mms/compose"
            java.lang.String r5 = "IOException caught while opening or reading stream"
            android.util.Log.e(r4, r5, r1)     // Catch:{ all -> 0x0163 }
            if (r3 == 0) goto L_0x00ea
            r3.close()     // Catch:{ IOException -> 0x0120 }
        L_0x00ea:
            if (r2 == 0) goto L_0x00ef
            r2.close()     // Catch:{ IOException -> 0x012b }
        L_0x00ef:
            r1 = r9
            goto L_0x009f
        L_0x00f1:
            android.content.Intent r1 = new android.content.Intent     // Catch:{ IOException -> 0x00da, all -> 0x015e }
            java.lang.String r5 = "android.intent.action.MEDIA_SCANNER_SCAN_FILE"
            android.net.Uri r4 = android.net.Uri.fromFile(r4)     // Catch:{ IOException -> 0x00da, all -> 0x015e }
            r1.<init>(r5, r4)     // Catch:{ IOException -> 0x00da, all -> 0x015e }
            r15.sendBroadcast(r1)     // Catch:{ IOException -> 0x00da, all -> 0x015e }
            r1 = r3
        L_0x0100:
            if (r2 == 0) goto L_0x0105
            r2.close()     // Catch:{ IOException -> 0x010c }
        L_0x0105:
            if (r1 == 0) goto L_0x010a
            r1.close()     // Catch:{ IOException -> 0x0116 }
        L_0x010a:
            r1 = 1
            goto L_0x009f
        L_0x010c:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x0116:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x0120:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x012b:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x0136:
            r1 = move-exception
            r2 = r13
            r3 = r13
        L_0x0139:
            if (r3 == 0) goto L_0x013e
            r3.close()     // Catch:{ IOException -> 0x0144 }
        L_0x013e:
            if (r2 == 0) goto L_0x0143
            r2.close()     // Catch:{ IOException -> 0x014f }
        L_0x0143:
            throw r1
        L_0x0144:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x014f:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r10, r11, r1)
            r1 = r9
            goto L_0x009f
        L_0x015a:
            r1 = move-exception
            r3 = r2
            r2 = r13
            goto L_0x0139
        L_0x015e:
            r1 = move-exception
            r14 = r3
            r3 = r2
            r2 = r14
            goto L_0x0139
        L_0x0163:
            r1 = move-exception
            goto L_0x0139
        L_0x0165:
            r1 = move-exception
            r2 = r13
            r3 = r13
            goto L_0x00de
        L_0x016a:
            r1 = move-exception
            r3 = r2
            r2 = r13
            goto L_0x00de
        L_0x016f:
            r1 = r13
            goto L_0x0100
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.copyPart(com.google.android.mms.pdu.PduPart):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00b0 A[SYNTHETIC, Splitter:B:46:0x00b0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean copyPartToDrmProvider(com.google.android.mms.pdu.PduPart r12) {
        /*
            r11 = this;
            r7 = 0
            java.lang.String r9 = "IOException caught while closing stream"
            java.lang.String r8 = "Mms/compose"
            android.net.Uri r3 = r12.getDataUri()
            r1 = 0
            android.content.ContentResolver r2 = r11.mContentResolver     // Catch:{ IOException -> 0x008e, all -> 0x00aa }
            java.io.InputStream r2 = r2.openInputStream(r3)     // Catch:{ IOException -> 0x008e, all -> 0x00aa }
            boolean r1 = r2 instanceof java.io.FileInputStream     // Catch:{ IOException -> 0x00c0 }
            if (r1 == 0) goto L_0x007d
            r0 = r2
            java.io.FileInputStream r0 = (java.io.FileInputStream) r0     // Catch:{ IOException -> 0x00c0 }
            r1 = r0
            byte[] r4 = r12.getName()     // Catch:{ IOException -> 0x00c0 }
            if (r4 != 0) goto L_0x0022
            byte[] r4 = r12.getFilename()     // Catch:{ IOException -> 0x00c0 }
        L_0x0022:
            if (r4 != 0) goto L_0x0028
            byte[] r4 = r12.getContentLocation()     // Catch:{ IOException -> 0x00c0 }
        L_0x0028:
            java.lang.String r5 = new java.lang.String     // Catch:{ IOException -> 0x00c0 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r4 = "."
            int r4 = r5.indexOf(r4)     // Catch:{ IOException -> 0x00c0 }
            r6 = -1
            if (r4 != r6) goto L_0x006d
            java.lang.String r4 = new java.lang.String     // Catch:{ IOException -> 0x00c0 }
            byte[] r6 = r12.getContentType()     // Catch:{ IOException -> 0x00c0 }
            r4.<init>(r6)     // Catch:{ IOException -> 0x00c0 }
            r4 = r5
        L_0x0040:
            android.content.ContentResolver r5 = r11.mContentResolver     // Catch:{ IOException -> 0x00c0 }
            android.content.Intent r1 = com.android.provider.DrmStore.addDrmFile(r5, r1, r4)     // Catch:{ IOException -> 0x00c0 }
            if (r1 != 0) goto L_0x007d
            java.lang.String r1 = "Mms/compose"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00c0 }
            r4.<init>()     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r5 = "unable to add file "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00c0 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r4 = " to DrmProvider"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00c0 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00c0 }
            android.util.Log.w(r1, r3)     // Catch:{ IOException -> 0x00c0 }
            if (r2 == 0) goto L_0x006b
            r2.close()     // Catch:{ IOException -> 0x0073 }
        L_0x006b:
            r1 = r7
        L_0x006c:
            return r1
        L_0x006d:
            r6 = 0
            java.lang.String r4 = r5.substring(r6, r4)     // Catch:{ IOException -> 0x00c0 }
            goto L_0x0040
        L_0x0073:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r8, r9, r1)
            r1 = r7
            goto L_0x006c
        L_0x007d:
            if (r2 == 0) goto L_0x0082
            r2.close()     // Catch:{ IOException -> 0x0084 }
        L_0x0082:
            r1 = 1
            goto L_0x006c
        L_0x0084:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r8, r9, r1)
            r1 = r7
            goto L_0x006c
        L_0x008e:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
        L_0x0092:
            java.lang.String r3 = "Mms/compose"
            java.lang.String r4 = "IOException caught while opening or reading stream"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x00be }
            if (r2 == 0) goto L_0x009e
            r2.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x009e:
            r1 = r7
            goto L_0x006c
        L_0x00a0:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r8, r9, r1)
            r1 = r7
            goto L_0x006c
        L_0x00aa:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
        L_0x00ae:
            if (r2 == 0) goto L_0x00b3
            r2.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x00b3:
            throw r1
        L_0x00b4:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r8, r9, r1)
            r1 = r7
            goto L_0x006c
        L_0x00be:
            r1 = move-exception
            goto L_0x00ae
        L_0x00c0:
            r1 = move-exception
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.copyPartToDrmProvider(com.google.android.mms.pdu.PduPart):boolean");
    }

    /* access modifiers changed from: private */
    public void copyToClipboard(String str) {
        ((ClipboardManager) getSystemService("clipboard")).setText(str);
    }

    /* access modifiers changed from: private */
    public boolean copyToDrmProvider(long j) {
        PduBody pduBody = PduBodyCache.getPduBody(this, ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, j));
        if (pduBody == null) {
            return false;
        }
        int partsNum = pduBody.getPartsNum();
        boolean z = true;
        for (int i = 0; i < partsNum; i++) {
            PduPart part = pduBody.getPart(i);
            if (ContentType.isDrmType(new String(part.getContentType()))) {
                z &= copyPartToDrmProvider(part);
            }
        }
        return z;
    }

    public static Intent createIntent(Context context, long j) {
        Intent intent = new Intent("android.intent.action.VIEW");
        if (j > 0) {
            intent.setData(Conversation.getUri(j));
        } else {
            intent.setComponent(new ComponentName(context, ComposeMessageActivity.class));
        }
        return intent;
    }

    public static Intent createIntentByThreadId(Context context, long j) {
        Intent intent = new Intent(context, ComposeMessageActivity.class);
        intent.putExtra("thread_id", j);
        return intent;
    }

    private TabHost.TabSpec createTabSpec(String str, int i, Intent intent) {
        TabHost.TabSpec newTabSpec = this.mSelTabHost.newTabSpec(str);
        newTabSpec.setIndicator(this.mInflater.inflate(i, (ViewGroup) null));
        newTabSpec.setContent(intent);
        return newTabSpec;
    }

    private void deleteImageAttachment() {
        this.mImageAttachmentView.setVisibility(8);
        this.mWorkingMessage.updateToSendImage(null);
        String join = TextUtils.join(",", getRecipients().getNumbers());
        if (join.length() == 0 && this.mRecipientsEditor != null) {
            join = TextUtils.join(",", this.mRecipientsEditor.getNumbers());
        }
        getContentResolver().delete(ImageDumpContentProvider.CONTENT_URI, "recipient = ?", new String[]{join});
        this.mMusicAttachmentView.update(this.mWorkingMessage);
        updateSendButtonState();
    }

    /* access modifiers changed from: private */
    public void deleteMusicAttachment() {
        this.mWorkingMessage.updateToSendImage(null);
        updateSendButtonState();
    }

    /* access modifiers changed from: private */
    public void deleteVoteAttachment() {
        findViewById(R.id.vote_panel).setVisibility(8);
        this.vote_cli_id = 0;
        this.mVoteItem = null;
        deleteOldVoteDraft();
    }

    /* access modifiers changed from: private */
    public void dialRecipient() {
        startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + ((Contact) getRecipients().get(0)).getNumber())));
    }

    /* access modifiers changed from: private */
    public void displayMessageItemOperation(MessagetToComposeDate messagetToComposeDate) {
        final MessageItem messageItem = getMessageItem(messagetToComposeDate.msgItem.mType, messagetToComposeDate.msgItem.mMsgId);
        View view = messagetToComposeDate.view;
        View inflate = getLayoutInflater().inflate((int) R.layout.message_item_popup, (ViewGroup) null);
        this.mMessageItemPopup = new PopupWindow(inflate, -2, -2);
        this.mMessageItemPopup.setOutsideTouchable(true);
        this.mMessageItemPopup.setBackgroundDrawable(new ColorDrawable(0));
        this.mMessageItemPopup.setFocusable(true);
        inflate.findViewById(R.id.copy_sms).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ComposeMessageActivity.this.copyToClipboard(messageItem.mBody);
                ComposeMessageActivity.this.mMessageItemPopup.dismiss();
            }
        });
        inflate.findViewById(R.id.forward).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ComposeMessageActivity.this.forwardMessage(messageItem);
                ComposeMessageActivity.this.mMessageItemPopup.dismiss();
            }
        });
        inflate.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ComposeMessageActivity.this.confirmDeleteDialog(new DeleteMessageListener(messageItem.mMessageUri, messageItem.mLocked), messageItem.mLocked);
                ComposeMessageActivity.this.mMessageItemPopup.dismiss();
            }
        });
        this.mMessageItemPopup.showAsDropDown(view, 0, -(view.getHeight() + 80));
    }

    /* access modifiers changed from: private */
    public void displayVoteAttachment(long j, String str) {
        if (j > 0) {
            this.vote_cli_id = j;
            getFirstPhaseVoteFromDb(this.vote_cli_id);
            if (str != null) {
                this.mTextEditor.setText(str);
            }
            String str2 = this.mVoteItem.title;
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.vote_panel);
            if (str2 == null) {
                relativeLayout.setVisibility(8);
                return;
            }
            deleteImageAttachment();
            findViewById(R.id.music_panel).setVisibility(8);
            this.musicPlug = "";
            relativeLayout.setVisibility(0);
            ((TextView) relativeLayout.findViewById(R.id.vote_title)).setText(getString(R.string.vote_attachment_info) + str2);
            ((ImageView) relativeLayout.findViewById(R.id.vote_delete)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ComposeMessageActivity.this.deleteVoteAttachment();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void drawBottomPanel() {
        resetCounter();
        if (this.mWorkingMessage.hasSlideshow()) {
            this.mBottomPanel.setVisibility(8);
            this.mAttachmentEditor.requestFocus();
            return;
        }
        this.mBottomPanel.setVisibility(0);
        CharSequence text = this.mWorkingMessage.getText();
        if (text != null) {
            this.mTextEditor.setTextKeepState(SmileyParser.getInstance().addSmileySpans(text));
            return;
        }
        this.mTextEditor.setText("");
    }

    /* access modifiers changed from: private */
    public void editMessageItem(MessageItem messageItem) {
        if ("sms".equals(messageItem.mType)) {
            editSmsMessageItem(messageItem);
        } else {
            editMmsMessageItem(messageItem);
        }
        if (MessageListItem.isFailedMessage(messageItem) && this.mMsgListAdapter.getCount() <= 1) {
            initRecipientsEditor();
        }
    }

    private void editMmsMessageItem(MessageItem messageItem) {
        this.mWorkingMessage.discard();
        this.mWorkingMessage = WorkingMessage.load(this, messageItem.mMessageUri);
        this.mWorkingMessage.setConversation(this.mConversation);
        this.mAttachmentEditor.update(this.mWorkingMessage);
    }

    private void editSmsMessageItem(MessageItem messageItem) {
        SqliteWrapper.delete(this, this.mContentResolver, ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, messageItem.mMsgId), null, null);
        this.mWorkingMessage.setText(messageItem.mBody);
    }

    /* access modifiers changed from: private */
    public void exitComposeMessageActivity(Runnable runnable, boolean z) {
        if (!this.mWorkingMessage.isWorthSaving() && this.vote_cli_id < 1) {
            runnable.run();
        } else if (this.mWaitingForSubActivity) {
        } else {
            if (isRecipientsEditorVisible() && !this.mRecipientsEditor.hasValidRecipient(this.mWorkingMessage.requiresMms())) {
                MessageUtils.showDiscardDraftConfirmDialog(this, new DiscardDraftListener(z));
            } else if (this.mToastForDraftSave) {
                saveDraft();
            } else {
                runnable.run();
            }
        }
    }

    private void extractAttachment(String str) {
        if (!TextUtils.isEmpty(str)) {
            Matcher matcher = Pattern.compile(getString(R.string.shorten_url_reg), MENU_MULTI_MODE).matcher(str);
            SongItem songItem = this.mWorkingMessage.getSongItem();
            if (matcher != null && matcher.find()) {
                loadMusicDraft(matcher.group(1));
                this.mWorkingMessage.updateSongShortenUrl(matcher.group(1), null);
                this.mWorkingMessage.setText(str.replace(matcher.group(), "").trim());
                updateSendButtonState();
            } else if (songItem != null) {
                final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.music_panel);
                deleteImageAttachment();
                this.musicPlug = songItem.plug;
                relativeLayout.setVisibility(0);
                ((TextView) findViewById(R.id.music_info)).setText(songItem.song + "\n" + songItem.singer);
                ((ImageView) findViewById(R.id.music_delete)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        relativeLayout.setVisibility(8);
                        String unused = ComposeMessageActivity.this.musicPlug = "";
                    }
                });
                updateSendButtonState();
            }
            Matcher matcher2 = Pattern.compile(getString(R.string.shorten_url_img_reg), MENU_MULTI_MODE).matcher(str);
            SmsImage imageItem = this.mWorkingMessage.getImageItem();
            if (matcher2 != null && matcher2.find()) {
                String group = matcher2.group(1);
                if (!(group == null || new ImagesDbService(getApplicationContext()).queryByPlug(group) == null)) {
                    this.mWorkingMessage.updateImageShortenUrl(group, null);
                    findViewById(R.id.music_panel).setVisibility(8);
                    this.musicPlug = "";
                    deleteVoteAttachment();
                    this.mImageAttachmentView.setVisibility(0);
                    if (imageItem == null || imageItem.mThumb == null || imageItem.mUrl == null) {
                        this.mRrm.request(Uri.parse(Util.getFullShortenImageUrl(getApplicationContext(), imageItem.mPlug)));
                    } else {
                        this.mWorkingMessage.setText(str.replace(matcher2.group(), "").trim());
                        Uri parse = Uri.parse(imageItem.mThumb);
                        if (!this.mRrm.exists(parse)) {
                            this.mRrm.request(parse);
                        } else {
                            try {
                                Bitmap decodeStream = BitmapFactory.decodeStream(this.mRrm.getInputStream(parse));
                                this.mImageAttachment.setVisibility(0);
                                this.mImageAttachment.setImageBitmap(decodeStream);
                            } catch (IOException e) {
                            }
                        }
                    }
                    updateSendButtonState();
                }
            } else if (imageItem != null) {
                findViewById(R.id.music_panel).setVisibility(8);
                this.musicPlug = "";
                deleteVoteAttachment();
                this.mImageAttachmentView.setVisibility(0);
                if (imageItem == null || imageItem.mThumb == null || imageItem.mUrl == null) {
                    this.mRrm.request(Uri.parse(Util.getFullShortenImageUrl(getApplicationContext(), imageItem.mPlug)));
                } else {
                    Uri parse2 = Uri.parse(imageItem.mThumb);
                    if (!this.mRrm.exists(parse2)) {
                        this.mRrm.request(parse2);
                    } else {
                        try {
                            Bitmap decodeStream2 = BitmapFactory.decodeStream(this.mRrm.getInputStream(parse2));
                            this.mImageAttachment.setVisibility(0);
                            this.mImageAttachment.setImageBitmap(decodeStream2);
                        } catch (IOException e2) {
                        }
                    }
                }
                updateSendButtonState();
            }
            Matcher matcher3 = Pattern.compile(getString(R.string.shorten_url_vote_reg), MENU_MULTI_MODE).matcher(str);
            if (matcher3 == null || matcher3.find()) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void forwardMessage(MessageItem messageItem) {
        Intent createIntent = createIntent(this, 0);
        createIntent.putExtra("exit_on_sent", true);
        createIntent.putExtra("forwarded_message", true);
        createIntent.putExtra(SmsSqliteHelper.PLUG, true);
        Matcher matcher = Pattern.compile(getApplicationContext().getString(R.string.shorten_url_vote_reg), MENU_MULTI_MODE).matcher(messageItem.mBody);
        if (matcher != null && matcher.find()) {
            String group = matcher.group(0);
            matcher.group(1);
            createIntent.putExtra("vote_cli_id", getVoteIdByPlug(group));
            createIntent.putExtra("sms_body", messageItem.mBody.replace(group, ""));
            createIntent.putExtra("exit_on_sent", false);
        } else if (messageItem.mType.equals("sms")) {
            createIntent.putExtra("sms_body", messageItem.mBody);
        } else {
            SendReq sendReq = new SendReq();
            String string = getString(R.string.forward_prefix);
            if (messageItem.mSubject != null) {
                string = string + messageItem.mSubject;
            }
            sendReq.setSubject(new EncodedStringValue(string));
            sendReq.setBody(messageItem.mSlideshow.makeCopy(this));
            try {
                createIntent.putExtra("msg_uri", PduPersister.getPduPersister(this).persist(sendReq, Telephony.Mms.Draft.CONTENT_URI));
                createIntent.putExtra("subject", string);
            } catch (MmsException e) {
                Log.e(TAG, "Failed to copy message: " + messageItem.mMessageUri, e);
                Toast.makeText(this, (int) R.string.cannot_save_message, 0).show();
                return;
            }
        }
        createIntent.setClassName(this, "com.android.mms.ui.ForwardMessageActivity");
        startActivity(createIntent);
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.ui.ComposeMessageActivity, vc.lx.sms.data.ContactList, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.util.List<java.lang.String>, int, int, vc.lx.sms.ui.ComposeMessageActivity]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    private void forwardSmsWithImage() {
        ContactList recipients;
        Conversation conversation;
        Util.logEvent(PrefsUtil.EVENT_IMAGE_FORWARD, new NameValuePair[0]);
        SmsImage imageItem = this.mWorkingMessage.getImageItem();
        List<String> numbers = this.mRecipientsEditor.getNumbers();
        if (this.mConversation.ensureThreadId() > 0) {
            Conversation conversation2 = this.mConversation;
            recipients = this.mConversation.getRecipients();
            conversation = conversation2;
        } else {
            Conversation conversation3 = Conversation.get((Context) this, ContactList.getByNumbers(numbers, false, true, (Context) this), false);
            recipients = this.mConversation.getRecipients();
            conversation = conversation3;
        }
        if (imageItem != null) {
            this.mUploadImageJob = new UploadImageJob(getApplicationContext(), recipients, conversation, imageItem, this.mTextEditor.getText().toString());
            this.mUploadImageJob.start();
            this.mExitOnSent = false;
            showUploadImageDialog();
        }
    }

    /* access modifiers changed from: private */
    public int getDrmMimeMenuStringRsrc(long j) {
        if (isAudioMimeType(getDrmMimeType(j))) {
            return R.string.save_ringtone;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public int getDrmMimeSavedStringRsrc(long j, boolean z) {
        if (isAudioMimeType(getDrmMimeType(j))) {
            return z ? R.string.saved_ringtone : R.string.saved_ringtone_fail;
        }
        return 0;
    }

    private String getDrmMimeType(long j) {
        PduBody pduBody = PduBodyCache.getPduBody(this, ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, j));
        if (pduBody == null) {
            return null;
        }
        int partsNum = pduBody.getPartsNum();
        for (int i = 0; i < partsNum; i++) {
            PduPart part = pduBody.getPart(i);
            if (ContentType.isDrmType(new String(part.getContentType()))) {
                return mimeTypeOfDrmPart(part);
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x009b A[Catch:{ Exception -> 0x00cc }, LOOP:0: B:11:0x0095->B:14:0x009b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void getFirstPhaseVoteFromDb(long r10) {
        /*
            r9 = this;
            r7 = 0
            java.lang.String r0 = "_id"
            vc.lx.sms.cmcc.http.data.VoteItem r0 = new vc.lx.sms.cmcc.http.data.VoteItem
            r0.<init>()
            r9.mVoteItem = r0
            android.content.Context r0 = r9.getApplicationContext()     // Catch:{ Exception -> 0x00d6, all -> 0x00e2 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00d6, all -> 0x00e2 }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI     // Catch:{ Exception -> 0x00d6, all -> 0x00e2 }
            r2 = 0
            java.lang.String r3 = " _id =  ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00d6, all -> 0x00e2 }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x00d6, all -> 0x00e2 }
            r4[r5] = r6     // Catch:{ Exception -> 0x00d6, all -> 0x00e2 }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00d6, all -> 0x00e2 }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0105 }
            if (r1 == 0) goto L_0x0072
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r9.mVoteItem     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = "_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0105 }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x0105 }
            r1.cli_id = r2     // Catch:{ Exception -> 0x0105 }
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r9.mVoteItem     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = "title"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0105 }
            r1.title = r2     // Catch:{ Exception -> 0x0105 }
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r9.mVoteItem     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = "service_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0105 }
            r1.service_id = r2     // Catch:{ Exception -> 0x0105 }
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r9.mVoteItem     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = "plug"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0105 }
            r1.plug = r2     // Catch:{ Exception -> 0x0105 }
            vc.lx.sms.cmcc.http.data.VoteItem r1 = r9.mVoteItem     // Catch:{ Exception -> 0x0105 }
            java.lang.String r2 = "parent_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0105 }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x0105 }
            r1.parent_id = r2     // Catch:{ Exception -> 0x0105 }
        L_0x0072:
            if (r0 == 0) goto L_0x0107
            r0.close()
            r6 = r0
        L_0x0078:
            android.content.Context r0 = r9.getApplicationContext()     // Catch:{ Exception -> 0x00fd, all -> 0x00f0 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00fd, all -> 0x00f0 }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI     // Catch:{ Exception -> 0x00fd, all -> 0x00f0 }
            r2 = 0
            java.lang.String r3 = " vote_id =  ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00fd, all -> 0x00f0 }
            r5 = 0
            java.lang.String r7 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x00fd, all -> 0x00f0 }
            r4[r5] = r7     // Catch:{ Exception -> 0x00fd, all -> 0x00f0 }
            java.lang.String r5 = " display_order asc "
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00fd, all -> 0x00f0 }
        L_0x0095:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x00cc }
            if (r1 == 0) goto L_0x00ea
            vc.lx.sms.cmcc.http.data.VoteOption r1 = new vc.lx.sms.cmcc.http.data.VoteOption     // Catch:{ Exception -> 0x00cc }
            r1.<init>()     // Catch:{ Exception -> 0x00cc }
            java.lang.String r2 = "option_text"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x00cc }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x00cc }
            r1.content = r2     // Catch:{ Exception -> 0x00cc }
            java.lang.String r2 = "display_order"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x00cc }
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x00cc }
            r1.display_order = r2     // Catch:{ Exception -> 0x00cc }
            java.lang.String r2 = "_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x00cc }
            long r2 = r0.getLong(r2)     // Catch:{ Exception -> 0x00cc }
            r1.cli_id = r2     // Catch:{ Exception -> 0x00cc }
            vc.lx.sms.cmcc.http.data.VoteItem r2 = r9.mVoteItem     // Catch:{ Exception -> 0x00cc }
            java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r2 = r2.options     // Catch:{ Exception -> 0x00cc }
            r2.add(r1)     // Catch:{ Exception -> 0x00cc }
            goto L_0x0095
        L_0x00cc:
            r1 = move-exception
        L_0x00cd:
            r1 = 0
            r9.mVoteItem = r1     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x00d5
            r0.close()
        L_0x00d5:
            return
        L_0x00d6:
            r0 = move-exception
            r0 = r7
        L_0x00d8:
            r1 = 0
            r9.mVoteItem = r1     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x0107
            r0.close()
            r6 = r0
            goto L_0x0078
        L_0x00e2:
            r0 = move-exception
            r1 = r7
        L_0x00e4:
            if (r1 == 0) goto L_0x00e9
            r1.close()
        L_0x00e9:
            throw r0
        L_0x00ea:
            if (r0 == 0) goto L_0x00d5
            r0.close()
            goto L_0x00d5
        L_0x00f0:
            r0 = move-exception
            r1 = r6
        L_0x00f2:
            if (r1 == 0) goto L_0x00f7
            r1.close()
        L_0x00f7:
            throw r0
        L_0x00f8:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00f2
        L_0x00fd:
            r0 = move-exception
            r0 = r6
            goto L_0x00cd
        L_0x0100:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00e4
        L_0x0105:
            r1 = move-exception
            goto L_0x00d8
        L_0x0107:
            r6 = r0
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.getFirstPhaseVoteFromDb(long):void");
    }

    private SimpleAdapter getMenuAdapter(String[] strArr, int[] iArr) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < strArr.length; i++) {
            HashMap hashMap = new HashMap();
            hashMap.put("itemImage", Integer.valueOf(iArr[i]));
            hashMap.put("itemText", strArr[i]);
            arrayList.add(hashMap);
        }
        return new SimpleAdapter(this, arrayList, R.layout.bottom_item_menu, new String[]{"itemImage", "itemText"}, new int[]{R.id.item_image, R.id.item_text});
    }

    /* JADX INFO: finally extract failed */
    private long getMessageDate(Uri uri) {
        Cursor query;
        if (!(uri == null || (query = SqliteWrapper.query(this, this.mContentResolver, uri, new String[]{"date"}, null, null, null)) == null)) {
            try {
                if (query.getCount() != 1 || !query.moveToFirst()) {
                    query.close();
                } else {
                    long j = query.getLong(0) * 1000;
                    query.close();
                    return j;
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public MessageItem getMessageItem(String str, long j) {
        Cursor cursor = this.mMsgListAdapter.getCursor();
        if (!cursor.isClosed() && !cursor.isBeforeFirst() && !cursor.isAfterLast()) {
            return this.mMsgListAdapter.getCachedMessageItem(str, j, cursor);
        }
        Log.e(TAG, "Bad cursor.");
        return null;
    }

    /* access modifiers changed from: private */
    public ContactList getRecipients() {
        if (!isRecipientsEditorVisible()) {
            return this.mConversation.getRecipients();
        }
        if (sEmptyContactList == null) {
            sEmptyContactList = new ContactList();
        }
        return sEmptyContactList;
    }

    private String getResourcesString(int i, String str) {
        return getResources().getString(i, str);
    }

    private Uri getSelectedUriFromMessageList(ListView listView, int i) {
        int i2;
        int i3;
        CharSequence charSequence;
        CharSequence charSequence2;
        TextView textView;
        MessageListItem messageListItem = (MessageListItem) listView.getChildAt(i);
        if (messageListItem == null) {
            return null;
        }
        TextView textView2 = (TextView) messageListItem.findViewById(R.id.text_view);
        if (textView2 != null) {
            CharSequence text = textView2.getText();
            int selectionStart = textView2.getSelectionStart();
            i2 = textView2.getSelectionEnd();
            int i4 = selectionStart;
            charSequence = text;
            i3 = i4;
        } else {
            i2 = -1;
            i3 = -1;
            charSequence = null;
        }
        if (i3 != -1 || (textView = (TextView) messageListItem.findViewById(R.id.body_text_view)) == null) {
            charSequence2 = charSequence;
        } else {
            CharSequence text2 = textView.getText();
            i3 = textView.getSelectionStart();
            int selectionEnd = textView.getSelectionEnd();
            charSequence2 = text2;
            i2 = selectionEnd;
        }
        if (i3 != i2) {
            URLSpan[] uRLSpanArr = (URLSpan[]) ((Spanned) charSequence2).getSpans(Math.min(i3, i2), Math.max(i3, i2), URLSpan.class);
            if (uRLSpanArr.length == 1) {
                return Uri.parse(uRLSpanArr[0].getURL());
            }
        }
        return null;
    }

    private File getUniqueDestination(String str, String str2) {
        File file = new File(str + "." + str2);
        int i = 2;
        while (file.exists()) {
            file = new File(str + "_" + i + "." + str2);
            i++;
        }
        return file;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0055  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long getVoteIdByPlug(java.lang.String r12) {
        /*
            r11 = this;
            r9 = 0
            r8 = 1
            r6 = 0
            android.content.Context r0 = r11.getApplicationContext()     // Catch:{ Exception -> 0x0048, all -> 0x0051 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0048, all -> 0x0051 }
            android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI     // Catch:{ Exception -> 0x0048, all -> 0x0051 }
            r2 = 0
            java.lang.String r3 = " plug = ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0048, all -> 0x0051 }
            r5 = 0
            r4[r5] = r12     // Catch:{ Exception -> 0x0048, all -> 0x0051 }
            java.lang.String r5 = " parent_id desc"
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0048, all -> 0x0051 }
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
            if (r1 <= 0) goto L_0x0064
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
            if (r1 == 0) goto L_0x0064
            java.lang.String r1 = "parent_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
            if (r1 >= r8) goto L_0x0046
            java.lang.String r1 = "_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
        L_0x003f:
            if (r0 == 0) goto L_0x0062
            r0.close()
            r0 = r1
        L_0x0045:
            return r0
        L_0x0046:
            long r1 = (long) r1
            goto L_0x003f
        L_0x0048:
            r0 = move-exception
            r0 = r9
        L_0x004a:
            if (r0 == 0) goto L_0x0060
            r0.close()
            r0 = r6
            goto L_0x0045
        L_0x0051:
            r0 = move-exception
            r1 = r9
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()
        L_0x0058:
            throw r0
        L_0x0059:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0053
        L_0x005e:
            r1 = move-exception
            goto L_0x004a
        L_0x0060:
            r0 = r6
            goto L_0x0045
        L_0x0062:
            r0 = r1
            goto L_0x0045
        L_0x0064:
            r1 = r6
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.getVoteIdByPlug(java.lang.String):long");
    }

    /* access modifiers changed from: private */
    public void goToConversationList() {
        finish();
    }

    private boolean handleForwardedMessage() {
        Intent intent = getIntent();
        if (!intent.getBooleanExtra("forwarded_message", false)) {
            return false;
        }
        Long valueOf = Long.valueOf(intent.getLongExtra("vote_cli_id", 0));
        if (valueOf.longValue() > 0) {
            displayVoteAttachment(valueOf.longValue(), intent.getStringExtra("sms_body"));
            this.isForwardVote = true;
            return true;
        }
        this.mIsForwardedMessage = true;
        Uri uri = (Uri) intent.getParcelableExtra("msg_uri");
        if (Log.isLoggable(LogTag.APP, 3)) {
            log("handle forwarded message " + uri);
        }
        if (uri != null) {
            this.mWorkingMessage = WorkingMessage.load(this, uri);
            this.mWorkingMessage.setSubject(intent.getStringExtra("subject"), false);
        } else {
            this.mWorkingMessage.setText(intent.getStringExtra("sms_body"));
        }
        this.appType = intent.getStringExtra("app_type");
        this.mMsgListAdapter.changeCursor(null);
        return true;
    }

    private boolean handleSendIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return false;
        }
        if (!"android.intent.action.SEND".equals(intent.getAction()) || !extras.containsKey("android.intent.extra.TEXT")) {
            return false;
        }
        this.mWorkingMessage.setText(extras.getString("android.intent.extra.TEXT"));
        return true;
    }

    private boolean handleSendMessage(Intent intent) {
        if (!intent.getBooleanExtra("forwarded_message", false)) {
            return false;
        }
        this.mIsForwardedMessage = true;
        Uri uri = (Uri) intent.getParcelableExtra("msg_uri");
        if (Log.isLoggable(LogTag.APP, 3)) {
            log("handle forwarded message " + uri);
        }
        if (uri != null) {
            this.mWorkingMessage = WorkingMessage.load(this, uri);
            this.mWorkingMessage.setSubject(intent.getStringExtra("subject"), false);
        } else {
            this.mWorkingMessage.setText(intent.getStringExtra("sms_body"));
        }
        this.mMsgListAdapter.changeCursor(null);
        return true;
    }

    /* JADX INFO: finally extract failed */
    private boolean haveEmailContact(String str) {
        Cursor query = SqliteWrapper.query(this, getContentResolver(), Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, Uri.encode(str)), new String[]{"display_name"}, null, null, null);
        if (query != null) {
            do {
                try {
                    if (!query.moveToNext()) {
                        query.close();
                    }
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            } while (TextUtils.isEmpty(query.getString(0)));
            query.close();
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean haveSomethingToCopyToDrmProvider(long j) {
        return isAudioMimeType(getDrmMimeType(j));
    }

    /* access modifiers changed from: private */
    public boolean haveSomethingToCopyToSDCard(long j) {
        PduBody pduBody = PduBodyCache.getPduBody(this, ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, j));
        if (pduBody == null) {
            return false;
        }
        int partsNum = pduBody.getPartsNum();
        for (int i = 0; i < partsNum; i++) {
            String str = new String(pduBody.getPart(i).getContentType());
            if (Log.isLoggable(LogTag.APP, 2)) {
                log("[CMA] haveSomethingToCopyToSDCard: part[" + i + "] contentType=" + str);
            }
            if (ContentType.isImageType(str) || ContentType.isVideoType(str) || ContentType.isAudioType(str)) {
                return true;
            }
        }
        return false;
    }

    private void hiddenPanelTab() {
        if (this.mPanelTab != null && this.mPanelTab.getVisibility() == 0) {
            this.mPanelTab.setVisibility(8);
        }
    }

    private void hideOrShowTopPanel() {
        this.mTopPanel.setVisibility(isSubjectEditorVisible() || isRecipientsEditorVisible() ? 0 : 8);
    }

    private void hideRecipientEditor() {
        if (this.mRecipientsEditor != null) {
            this.mRecipientsEditor.setVisibility(8);
            hideOrShowTopPanel();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.ui.ComposeMessageActivity, vc.lx.sms.data.ContactList, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.lang.String, int, int, vc.lx.sms.ui.ComposeMessageActivity]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.ui.ComposeMessageActivity, long, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.ui.ComposeMessageActivity, android.net.Uri, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation */
    private void initActivityState(Bundle bundle, Intent intent) {
        if (bundle != null) {
            this.mConversation = Conversation.get((Context) this, ContactList.getByNumbers(bundle.getString("recipients"), false, true, (Context) this), false);
            addRecipientsListeners();
            this.mExitOnSent = bundle.getBoolean("exit_on_sent", false);
            this.mWorkingMessage.readStateFromBundle(bundle);
            return;
        }
        long longExtra = intent.getLongExtra("thread_id", 0);
        if (longExtra > 0) {
            this.mConversation = Conversation.get((Context) this, longExtra, false);
            this.mQuickCall.setVisibility(0);
        } else if (intent.getData() != null) {
            this.mConversation = Conversation.get((Context) this, intent.getData(), false);
        } else {
            String stringExtra = intent.getStringExtra("address");
            if (!TextUtils.isEmpty(stringExtra)) {
                this.mConversation = Conversation.get((Context) this, ContactList.getByNumbers(stringExtra, false, true, (Context) this), false);
            } else {
                this.mConversation = Conversation.createNew(this);
            }
            this.mQuickCall.setVisibility(4);
        }
        addRecipientsListeners();
        initMusicForward(intent);
    }

    private void initFocus() {
        if (this.mIsKeyboardOpen) {
            if (!isRecipientsEditorVisible() || !TextUtils.isEmpty(this.mRecipientsEditor.getText()) || this.mTextEditor.isFocused()) {
                this.mTextEditor.requestFocus();
            } else {
                this.mRecipientsEditor.requestFocus();
            }
        }
    }

    private void initMessageList() {
        if (this.mMsgListAdapter == null) {
            this.mMsgListAdapter = new MessageListAdapter(this, null, this.mMsgListView, true, getIntent().getStringExtra("highlight"), false, this.mMultiRecipient);
            this.mMsgListAdapter.setRemoteResourceManager(this.mRrm);
            this.mMsgListAdapter.setMsgListItemHandler(this.mMessageListItemHandler);
            this.mMsgListAdapter.setOnDataSetChangedListener(this.mDataSetChangedListener);
            loadImageAttachmentIfNeeded();
            if (this.mConversation.getRecipients() != null) {
                loadVoteDraft();
                if (this.vote_cli_id > 0) {
                    deleteOldVoteDraft();
                }
            }
            this.mMsgListView.setAdapter((ListAdapter) this.mMsgListAdapter);
            this.mMsgListView.setItemsCanFocus(false);
            this.mMsgListView.setVisibility(0);
            this.mMsgListView.setOnCreateContextMenuListener(this.mMsgListMenuCreateListener);
            this.mMsgListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    if (view instanceof MessageListItem) {
                        ((MessageListItem) view).onMessageListItemClick();
                    }
                }
            });
        }
    }

    private void initMusicForward(Intent intent) {
        this.musicPlug = intent.getStringExtra(SmsSqliteHelper.PLUG);
        this.mExitOnSent = intent.getBooleanExtra("exit_on_sent", false);
        if (intent.getStringExtra("sms_body") != null) {
            this.mWorkingMessage.setText(this.mTextEditor.getText().toString() + " " + intent.getStringExtra("sms_body"));
        }
        this.mWorkingMessage.setSubject(intent.getStringExtra("subject"), false);
        String stringExtra = intent.getStringExtra("song");
        String stringExtra2 = intent.getStringExtra("singer");
        final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.music_panel);
        if (stringExtra == null && stringExtra2 == null) {
            relativeLayout.setVisibility(8);
            return;
        }
        deleteImageAttachment();
        extractAttachment(this.mWorkingMessage.getText().toString());
        relativeLayout.setVisibility(0);
        ((TextView) findViewById(R.id.music_info)).setText(stringExtra + "\n" + stringExtra2);
        ((ImageView) findViewById(R.id.music_delete)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                relativeLayout.setVisibility(8);
                String unused = ComposeMessageActivity.this.musicPlug = "";
            }
        });
    }

    private void initPhotoGridView() {
        this.mGridView = (GridView) findViewById(R.id.subgridview);
        this.mGridView.setNumColumns(3);
        this.mGridView.setStretchMode(2);
        this.mGridView.setVerticalSpacing(10);
        this.mGridView.setHorizontalSpacing(10);
        this.mTitleGridView.setSelector((int) R.color.alpha_00);
        this.mGridView.setPadding(10, 10, 10, 10);
        this.mGridView.setGravity(17);
        this.mGridView.setAdapter((ListAdapter) getMenuAdapter(getApplicationContext().getResources().getStringArray(R.array.default_bottom_photo_tab), new int[]{R.drawable.menu_about, R.drawable.menu_about, R.drawable.menu_about, R.drawable.menu_about}));
        this.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            private void activityClickItem(int i) {
                if (i == 0) {
                    Intent createIntentByThreadId = VoteCreateActivity.createIntentByThreadId(ComposeMessageActivity.this.getApplicationContext(), ComposeMessageActivity.this.mConversation.getThreadId());
                    boolean unused = ComposeMessageActivity.this.mToastForDraftSave = false;
                    ComposeMessageActivity.this.startActivityForResult(createIntentByThreadId, 25);
                    boolean unused2 = ComposeMessageActivity.this.mNeedReset = true;
                }
                if (i == 1) {
                    Intent intent = new Intent(ComposeMessageActivity.this, QuareVoteListActivity.class);
                    intent.putExtra("from_compose", true);
                    intent.putExtra("thread_id", ComposeMessageActivity.this.mConversation.getThreadId());
                    if (ComposeMessageActivity.this.mRecipientsEditor != null && ComposeMessageActivity.this.mRecipientsEditor.getNumbers().size() > 0) {
                        String str = "";
                        for (int i2 = 0; i2 < ComposeMessageActivity.this.mRecipientsEditor.getNumbers().size(); i2++) {
                            str = str.equals("") ? str + ComposeMessageActivity.this.mRecipientsEditor.getNumbers().get(i2) : str + MessageSender.RECIPIENTS_SEPARATOR + ComposeMessageActivity.this.mRecipientsEditor.getNumbers().get(i2);
                        }
                        intent.putExtra("address", str);
                    }
                    ComposeMessageActivity.this.startActivityForResult(intent, 25);
                }
                if (i == 2) {
                    Intent createIntentByVisitId = ContactsSelectActivity.createIntentByVisitId(ComposeMessageActivity.this.getApplicationContext(), 1000);
                    createIntentByVisitId.putExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, true);
                    ComposeMessageActivity.this.startActivityForResult(createIntentByVisitId, 22);
                }
                if (i == 3) {
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            private void photoClickItem(int i) {
                if (i == 0) {
                    Intent intent = new Intent("android.intent.action.GET_CONTENT");
                    intent.setType(ContentType.IMAGE_UNSPECIFIED);
                    ComposeMessageActivity.this.startActivityForResult(intent, 19);
                }
                if (i == 1) {
                    Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                    intent2.putExtra("output", Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/.tmp_camera_selection.jpg")));
                    intent2.putExtra("return-data", true);
                    ComposeMessageActivity.this.startActivityForResult(intent2, 20);
                }
                if (i == 2) {
                    Intent intent3 = new Intent(ComposeMessageActivity.this.getApplicationContext(), MusicPlatformActivity.class);
                    intent3.putExtra("from_panel", true);
                    ComposeMessageActivity.this.startActivityForResult(intent3, 24);
                }
            }

            private void smileyClickItem(int i) {
                ComposeMessageActivity.this.mTextEditor.getText().insert(ComposeMessageActivity.this.mTextEditor.getSelectionStart(), SmileyParser.getInstance().addSmileySpans(ComposeMessageActivity.this.getResources().getStringArray(R.array.default_smiley_texts)[i]));
            }

            private void smsTemplateClickItem(int i) {
                Intent intent = new Intent(ComposeMessageActivity.this.getApplicationContext(), SmsTemplateListActivity.class);
                intent.putExtra(PrefsUtil.KEY_TEMPLATE_CATEGORY_ID, i);
                intent.putExtra(PrefsUtil.KEY_TEMPLATE_CATEGORY_NAME, ComposeMessageActivity.this.getApplicationContext().getResources().getStringArray(R.array.default_bottom_sms_tab)[i]);
                ComposeMessageActivity.this.startActivityForResult(intent, 23);
            }

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                switch (ComposeMessageActivity.this.titleIndex) {
                    case 0:
                        photoClickItem(i);
                        return;
                    case 1:
                        activityClickItem(i);
                        return;
                    case 2:
                        smsTemplateClickItem(i);
                        return;
                    case 3:
                        smileyClickItem(i);
                        return;
                    default:
                        return;
                }
            }
        });
        this.mTabPhotoImageView = (Button) this.mTitleGridView.getItemAtPosition(0);
    }

    private void initPopupMenu() {
        this.mInflater = (LayoutInflater) getApplicationContext().getSystemService("layout_inflater");
        this.mTitleGridView = (GridView) findViewById(R.id.gridview);
        this.mTitleGridView.setSelector((int) R.color.alpha_00);
        this.mTitleGridView.setNumColumns(5);
        this.mTitleGridView.setHorizontalSpacing(MENU_COPY_TO_DRM_PROVIDER);
        this.mTitleGridView.setPadding(0, 0, 5, 0);
        this.mTitleGridView.setAdapter((ListAdapter) new MenuTitleAdapter(this, new int[]{R.drawable.category_multi_media_normal, R.drawable.category_rich_texting_norml, R.drawable.category_sms_normal, R.drawable.category_emoji_normal, R.drawable.bg_message_send}, 16, -1));
        this.mTitleGridView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                ComposeMessageActivity.this.onChangeItem(view, i);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.mTitleGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                ComposeMessageActivity.this.onChangeItem(view, i);
            }
        });
        initPhotoGridView();
    }

    private void initPopupWindow() {
        View inflate = getLayoutInflater().inflate((int) R.layout.compose_popupwindow, (ViewGroup) null);
        this.mPopupWindow = new PopupWindow(inflate, -1, -2);
        this.mPopupWindow.setOutsideTouchable(false);
        this.mPopupWindow.setBackgroundDrawable(new ColorDrawable(0));
        this.mPopupWindow.setFocusable(true);
        this.menuGrid = (GridView) inflate.findViewById(R.id.gridview);
        this.menuGrid.setGravity(17);
        this.mPopupWindowMenuData = new ArrayList<>();
        this.mPopupWindowAdapter = new SimpleAdapter(this, this.mPopupWindowMenuData, R.layout.popup_window_menu_item, new String[]{"itemImage", "itemText"}, new int[]{R.id.item_image, R.id.item_text});
        this.menuGrid.setAdapter((ListAdapter) this.mPopupWindowAdapter);
        this.menuGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                switch (((Integer) ((HashMap) adapterView.getItemAtPosition(i)).get("id")).intValue()) {
                    case 1:
                        ComposeMessageActivity.this.confirmDeleteThread(ComposeMessageActivity.this.mConversation.getThreadId());
                        break;
                    case 2:
                        ComposeMessageActivity.this.finish();
                        ComposeMessageActivity.this.startActivity(ComposeMessageActivity.createIntentByThreadId(ComposeMessageActivity.this.getApplicationContext(), 0));
                        break;
                    case 3:
                        ComposeMessageActivity.this.mWorkingMessage.discard();
                        ComposeMessageActivity.this.finish();
                        break;
                    case 4:
                        if (ComposeMessageActivity.this.isPreparedForSending()) {
                            ComposeMessageActivity.this.confirmSendMessageIfNeeded();
                            break;
                        }
                        break;
                    case 5:
                        ComposeMessageActivity.this.dialRecipient();
                        break;
                    case 6:
                        ComposeMessageActivity.this.exitComposeMessageActivity(new Runnable() {
                            public void run() {
                                ComposeMessageActivity.this.goToConversationList();
                            }
                        }, false);
                        break;
                    case 12:
                        ContactList access$3100 = ComposeMessageActivity.this.getRecipients();
                        if (access$3100.size() == 1 && ((Contact) access$3100.get(0)).existsInDatabase()) {
                            Intent intent = new Intent("android.intent.action.VIEW", ((Contact) access$3100.get(0)).getUri());
                            intent.setFlags(524288);
                            ComposeMessageActivity.this.startActivity(intent);
                            break;
                        }
                    case 13:
                        ComposeMessageActivity.this.startActivity(ConversationListActivity.createAddContactIntent(((Contact) ComposeMessageActivity.this.mConversation.getRecipients().get(0)).getNumber()));
                        break;
                    case 19:
                        ComposeMessageActivity.this.onSearchRequested();
                        break;
                    case ComposeMessageActivity.MENU_MULTI_MODE /*32*/:
                        boolean unused = ComposeMessageActivity.this.mIsMultiMode = true;
                        ComposeMessageActivity.this.findViewById(R.id.scrollview).setVisibility(8);
                        ComposeMessageActivity.this.findViewById(R.id.multi_actions_view).setVisibility(0);
                        ComposeMessageActivity.this.mMsgListAdapter.setMultiMode(ComposeMessageActivity.this.mIsMultiMode);
                        ComposeMessageActivity.this.mMsgListAdapter.notifyDataSetInvalidated();
                        break;
                    case ComposeMessageActivity.MENU_DELETE_SELECTED /*33*/:
                        AlertDialog.Builder builder = new AlertDialog.Builder(ComposeMessageActivity.this);
                        builder.setTitle((int) R.string.menu_delete_selected_items);
                        builder.setIcon(17301543);
                        builder.setCancelable(true);
                        builder.setMessage((int) R.string.confirm_delete_selected_messages);
                        builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ComposeMessageActivity.this.mMessageListItemHandler.post(new Runnable() {
                                    public void run() {
                                        HashSet<String> selectedIds = ComposeMessageActivity.this.mMsgListAdapter.getSelectedIds();
                                        if (selectedIds.size() != 0) {
                                            String[] strArr = new String[selectedIds.size()];
                                            selectedIds.toArray(strArr);
                                            for (String str : strArr) {
                                                ComposeMessageActivity.this.mBackgroundQueryHandler.startDelete(ComposeMessageActivity.DELETE_MESSAGE_TOKEN, null, ContentUris.withAppendedId("sms".equals(str.split("#")[0]) ? Telephony.Sms.CONTENT_URI : Telephony.Mms.CONTENT_URI, Long.valueOf(str.split("#")[1]).longValue()), null, null);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                        builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
                        builder.show();
                        break;
                    case ComposeMessageActivity.MENU_CANCEL_MULTI_MODE /*34*/:
                        ComposeMessageActivity.this.cancelMultiMode();
                        break;
                    case ComposeMessageActivity.MENU_MUSIC_BOARDS /*35*/:
                        ComposeMessageActivity.this.startActivity(new Intent(ComposeMessageActivity.this.getApplicationContext(), MusicPlatformActivity.class));
                        break;
                    case ComposeMessageActivity.MENU_DELETE_ALL /*36*/:
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(ComposeMessageActivity.this);
                        builder2.setTitle((int) R.string.menu_delete_all);
                        builder2.setIcon(17301543);
                        builder2.setCancelable(true);
                        builder2.setMessage((int) R.string.confirm_delete_all);
                        builder2.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ComposeMessageActivity.this.mMessageListItemHandler.post(new Runnable() {
                                    public void run() {
                                        Conversation.startDelete(ComposeMessageActivity.this.mBackgroundQueryHandler, ConversationListActivity.DELETE_CONVERSATION_TOKEN, true, ComposeMessageActivity.this.mConversation.getThreadId());
                                    }
                                });
                            }
                        });
                        builder2.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
                        builder2.show();
                        break;
                }
                if (ComposeMessageActivity.this.mPopupWindow.isShowing()) {
                    ComposeMessageActivity.this.mPopupWindow.dismiss();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void initRecipientsEditor() {
        if (!isRecipientsEditorVisible()) {
            ContactList recipients = getRecipients();
            ViewStub viewStub = (ViewStub) findViewById(R.id.recipients_editor_stub);
            if (viewStub != null) {
                View inflate = viewStub.inflate();
                this.mRecipientsEditor = (RecipientsEditor) inflate.findViewById(R.id.recipients_editor);
                this.mAddContactButton = (Button) inflate.findViewById(R.id.add_contacts_button);
            } else {
                this.mRecipientsEditor = (RecipientsEditor) findViewById(R.id.recipients_editor);
                this.mAddContactButton = (Button) findViewById(R.id.add_contacts_button);
                this.mRecipientsEditor.setVisibility(0);
            }
            this.mRecipientsEditor.setAdapter(new RecipientsAdapter(this));
            this.mRecipientsEditor.populate(recipients);
            this.mRecipientsEditor.setOnCreateContextMenuListener(this.mRecipientsMenuCreateListener);
            this.mRecipientsEditor.addTextChangedListener(this.mRecipientsWatcher);
            this.mRecipientsEditor.setFilters(new InputFilter[]{new InputFilter.LengthFilter(RECIPIENTS_MAX_LENGTH)});
            this.mRecipientsEditor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    if (ComposeMessageActivity.this.mRecipientsEditor.getRecipientCount() == 1) {
                        InputMethodManager inputMethodManager = (InputMethodManager) ComposeMessageActivity.this.getSystemService("input_method");
                        if (inputMethodManager == null || !inputMethodManager.isFullscreenMode()) {
                            ComposeMessageActivity.this.mTextEditor.requestFocus();
                        }
                    }
                }
            });
            this.mAddContactButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ComposeMessageActivity.this.startActivityForResult(new Intent(ComposeMessageActivity.this, ContactsSelectActivity.class), 18);
                }
            });
            this.mRecipientsEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View view, boolean z) {
                    if (!z) {
                        ComposeMessageActivity.this.bindToContactHeader(((RecipientsEditor) view).constructContactsFromInput());
                    }
                }
            });
            this.mTopPanel.setVisibility(0);
        }
    }

    private void initResourceRefs() {
        this.mMsgListView = (MessageListView) findViewById(R.id.history);
        this.mMsgListView.setDivider(null);
        this.mBottomPanel = findViewById(R.id.bottom_panel);
        this.mTextEditor = (EditText) findViewById(R.id.embedded_text_editor);
        this.mTextEditor.setOnEditorActionListener(this);
        this.mTextEditor.addTextChangedListener(this.mTextEditorWatcher);
        this.mTextEditor.setOnClickListener(this.onClickListener);
        this.mTextCounter = (TextView) findViewById(R.id.text_counter);
        this.mPanelTab = (LinearLayout) findViewById(R.id.compose_message_bottom_tab_view);
        this.mTopPanel = findViewById(R.id.recipients_subject_linear);
        this.mTopPanel.setFocusable(false);
        showBottomPanel();
        this.mAttachmentEditor = (AttachmentEditor) findViewById(R.id.attachment_editor);
        this.mAttachmentEditor.setHandler(this.mAttachmentEditorHandler);
        this.mMusicAttachmentView = (MusicAttachmentView) findViewById(R.id.music_attachment_view);
        this.mMusicAttachmentView.setHandler(this.mAttachmentEditorHandler);
        this.mVoteAttachmentView = (VoteAttachmentView) findViewById(R.id.vote_attachment_view);
        this.mVoteAttachmentView.setHandler(this.mAttachmentEditorHandler);
        ((ImageView) findViewById(R.id.compose_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ComposeMessageActivity.this.exitComposeMessageActivity(new Runnable() {
                    public void run() {
                        ComposeMessageActivity.this.goToConversationList();
                    }
                }, false);
            }
        });
        this.mDisplayName = (TextView) findViewById(R.id.compose_from);
        this.mQuickCall = (ImageView) findViewById(R.id.compose_call);
        this.mDelAllBtn = (Button) findViewById(R.id.btn_delete_all);
        this.mDelSelectedBtn = (Button) findViewById(R.id.btn_delete_seleted);
        this.mCancelMultiModeBtn = (Button) findViewById(R.id.btn_cancel_multi);
        this.mDelAllBtn.setOnClickListener(this);
        this.mDelSelectedBtn.setOnClickListener(this);
        this.mCancelMultiModeBtn.setOnClickListener(this);
        initPopupWindow();
        if (this.mPanelTab == null || this.mPanelTab.getVisibility() != 8) {
            this.mPanelTab.setVisibility(8);
        } else {
            this.mPanelTab.setVisibility(0);
        }
        initSelectImgePopupWindow();
        this.mImageAttachmentView = findViewById(R.id.image_attachment_view);
        this.mImageAttachment = (ImageView) findViewById(R.id.image_attachment);
        this.mRemoveImageAttachmentButton = (ImageView) findViewById(R.id.image_attachment_delete);
        this.mRemoveImageAttachmentButton.setOnClickListener(this);
    }

    private void initSelectImgePopupWindow() {
        View inflate = getLayoutInflater().inflate((int) R.layout.compose_selectimg_popupwindow, (ViewGroup) null);
        this.mPopSelectImg = new PopupWindow(inflate, (int) PduHeaders.RECOMMENDED_RETRIEVAL_MODE, -2);
        this.mPopSelectImg.setOutsideTouchable(false);
        this.mPopSelectImg.setBackgroundDrawable(new BitmapDrawable());
        this.mPopSelectImg.setFocusable(true);
        this.mSelectImgList = (ListView) inflate.findViewById(R.id.select_img_list);
        this.mSelectImgDate = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("itemImage", Integer.valueOf((int) R.drawable.ic_menu_contact));
        hashMap.put("itemText", getString(R.string.title_from_camera));
        this.mSelectImgDate.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("itemImage", Integer.valueOf((int) R.drawable.ic_menu_contact));
        hashMap2.put("itemText", getString(R.string.title_from_gallery));
        this.mSelectImgDate.add(hashMap2);
        this.mSelectImgAdapter = new SimpleAdapter(this, this.mSelectImgDate, R.layout.compose_selectimg_popup_list_item, new String[]{"itemImage", "itemText"}, new int[]{R.id.item_image, R.id.item_text});
        this.mSelectImgList.setAdapter((ListAdapter) this.mSelectImgAdapter);
        this.mSelectImgList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                HashMap hashMap = (HashMap) adapterView.getItemAtPosition(i);
                if (hashMap.get("itemText").equals(ComposeMessageActivity.this.getString(R.string.title_from_gallery))) {
                    Intent intent = new Intent("android.intent.action.GET_CONTENT");
                    intent.setType(ContentType.IMAGE_UNSPECIFIED);
                    ComposeMessageActivity.this.startActivityForResult(intent, 19);
                } else if (hashMap.get("itemText").equals(ComposeMessageActivity.this.getString(R.string.title_from_camera))) {
                    Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                    intent2.putExtra("output", Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/.tmp_camera_selection.jpg")));
                    intent2.putExtra("return-data", true);
                    ComposeMessageActivity.this.startActivityForResult(intent2, 20);
                }
                if (ComposeMessageActivity.this.mPopSelectImg.isShowing()) {
                    ComposeMessageActivity.this.mPopSelectImg.dismiss();
                }
                if (ComposeMessageActivity.this.mMusicAttachmentView.getVisibility() == 0) {
                    Message.obtain(ComposeMessageActivity.this.mAttachmentEditorHandler, 11).sendToTarget();
                }
            }
        });
    }

    private void initialize(Bundle bundle) {
        boolean z;
        Intent intent = getIntent();
        this.mWorkingMessage = WorkingMessage.createEmpty(this);
        initActivityState(bundle, intent);
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("initialize: savedInstanceState = " + bundle + " intent = " + intent + " recipients = " + getRecipients());
        }
        if (cancelFailedToDeliverNotification(getIntent(), this)) {
            undeliveredMessageDialog(getMessageDate(null));
        }
        cancelFailedDownloadNotification(getIntent(), this);
        bindToContactHeader(this.mConversation.getRecipients());
        initMessageList();
        this.topMusicService = new TopMusicService(getApplicationContext());
        this.mConversation.markAsRead();
        if (!handleSendIntent(intent)) {
            z = handleForwardedMessage();
            if (!z) {
                loadDraft();
            }
            this.mWorkingMessage.checkShortenUrl();
        } else {
            z = false;
        }
        this.mWorkingMessage.setConversation(this.mConversation);
        if (this.mConversation.getThreadId() <= 0) {
            hideRecipientEditor();
            initRecipientsEditor();
            getWindow().setSoftInputMode(20);
        } else {
            hideRecipientEditor();
        }
        updateSendButtonState();
        drawBottomPanel();
        this.mAttachmentEditor.update(this.mWorkingMessage);
        Configuration configuration = getResources().getConfiguration();
        this.mIsKeyboardOpen = configuration.keyboardHidden == 1;
        this.mIsLandscape = configuration.orientation == 2;
        onKeyboardStateChanged(this.mIsKeyboardOpen);
        if (z && isRecipientsEditorVisible()) {
            String[] stringArrayExtra = intent.getStringArrayExtra("selected_names");
            if (stringArrayExtra != null) {
                this.mRecipientsEditor.append(returnSelectedName(stringArrayExtra));
            }
            this.mRecipientsEditor.requestFocus();
            extractAttachment(this.mWorkingMessage.getText().toString());
        }
    }

    /* access modifiers changed from: private */
    public void initializeContactInfo() {
        ContactList recipients = getRecipients();
        if (recipients.size() != 1) {
            updatePresence(null);
        } else {
            updatePresence((Contact) recipients.get(0));
        }
    }

    private boolean isAudioMimeType(String str) {
        return str != null && str.startsWith("audio/");
    }

    private boolean isImageMimeType(String str) {
        return str != null && str.startsWith("image/");
    }

    private boolean isNumberInContacts(String str) {
        return Contact.get(str, false, this).existsInDatabase();
    }

    /* access modifiers changed from: private */
    public boolean isPreparedForSending() {
        int recipientCount = recipientCount();
        if (this.mRecipientsEditor == null || this.mRecipientsEditor.getNumbers().size() != 0) {
            return (recipientCount > 0 && recipientCount <= MmsConfig.getRecipientLimit() && (this.mWorkingMessage.hasAttachment() || this.mWorkingMessage.hasText() || this.mWorkingMessage.hasShortenUrl() || this.mWorkingMessage.getToSendImage() != null)) || !"".equals(this.musicPlug) || this.vote_cli_id > 0;
        }
        if (this.mSendButton != null) {
            this.mSendButton.setEnabled(false);
            this.mSendButton.setClickable(false);
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean isRecipientsEditorVisible() {
        return this.mRecipientsEditor != null && this.mRecipientsEditor.getVisibility() == 0;
    }

    private boolean isSpecialChar(char c) {
        return c == '*' || c == '%' || c == MENU_DELETE_ALL;
    }

    private boolean isSubjectEditorVisible() {
        return this.mSubjectTextEditor != null && this.mSubjectTextEditor.getVisibility() == 0;
    }

    private void loadDraft() {
        if (this.mWorkingMessage.isWorthSaving()) {
            Log.w(TAG, "loadDraft() called with non-empty working message");
            return;
        }
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("loadDraft: call WorkingMessage.loadDraft");
        }
        this.mWorkingMessage = WorkingMessage.loadDraft(this, this.mConversation);
        if (this.mWorkingMessage.hasShortenUrl()) {
            extractAttachment(this.mWorkingMessage.getShortenUrl() + " " + this.mWorkingMessage.getText().toString());
        }
    }

    private void loadImageAttachmentIfNeeded() {
        this.mHandler.post(new Runnable() {
            public void run() {
                String join = TextUtils.join(",", ComposeMessageActivity.this.getRecipients().getNumbers());
                String join2 = join.length() == 0 ? TextUtils.join(",", ComposeMessageActivity.this.mRecipientsEditor.getNumbers()) : join;
                if (join2.length() != 0) {
                    Cursor managedQuery = ComposeMessageActivity.this.managedQuery(ImageDumpContentProvider.CONTENT_URI, null, "recipient = ?", new String[]{join2}, null);
                    if (managedQuery.moveToNext()) {
                        byte[] blob = managedQuery.getBlob(2);
                        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(blob, 0, blob.length);
                        ComposeMessageActivity.this.mWorkingMessage.updateToSendImage(decodeByteArray);
                        ComposeMessageActivity.this.mMusicAttachmentView.update(ComposeMessageActivity.this.mWorkingMessage);
                        if (decodeByteArray != null) {
                            try {
                                Bitmap resampleImage = ImageUtils.resampleImage(decodeByteArray, (int) PduHeaders.PREVIOUSLY_SENT_BY);
                                ComposeMessageActivity.this.mImageAttachment.setVisibility(0);
                                ComposeMessageActivity.this.mImageAttachment.setImageBitmap(resampleImage);
                                ComposeMessageActivity.this.findViewById(R.id.music_panel).setVisibility(8);
                                String unused = ComposeMessageActivity.this.musicPlug = "";
                                ComposeMessageActivity.this.deleteVoteAttachment();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ComposeMessageActivity.this.updateSendButtonState();
                        ComposeMessageActivity.this.mImageAttachmentView.setVisibility(0);
                    }
                }
            }
        });
    }

    private void loadMessageContent() {
        startMsgListQuery();
        initializeContactInfo();
        updateSendFailedNotification();
        drawBottomPanel();
    }

    /* access modifiers changed from: private */
    public void lockMessage(MessageItem messageItem, boolean z) {
        int i = 1;
        final Uri withAppendedId = ContentUris.withAppendedId("sms".equals(messageItem.mType) ? Telephony.Sms.CONTENT_URI : Telephony.Mms.CONTENT_URI, messageItem.mMsgId);
        final ContentValues contentValues = new ContentValues(1);
        if (!z) {
            i = 0;
        }
        contentValues.put("locked", Integer.valueOf(i));
        new Thread(new Runnable() {
            public void run() {
                ComposeMessageActivity.this.getContentResolver().update(withAppendedId, contentValues, null, null);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public static void log(String str) {
        Thread currentThread = Thread.currentThread();
        long id = currentThread.getId();
        Log.d(TAG, "[" + id + "] [" + currentThread.getStackTrace()[3].getMethodName() + "] " + str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0069 A[SYNTHETIC, Splitter:B:35:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007a A[SYNTHETIC, Splitter:B:42:0x007a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String mimeTypeOfDrmPart(com.google.android.mms.pdu.PduPart r10) {
        /*
            r9 = this;
            r7 = 0
            java.lang.String r6 = "IOException caught while closing stream"
            java.lang.String r5 = "Mms/compose"
            android.net.Uri r1 = r10.getDataUri()
            android.content.ContentResolver r2 = r9.mContentResolver     // Catch:{ IOException -> 0x0046, DrmException -> 0x005e, all -> 0x0076 }
            java.io.InputStream r1 = r2.openInputStream(r1)     // Catch:{ IOException -> 0x0046, DrmException -> 0x005e, all -> 0x0076 }
            boolean r2 = r1 instanceof java.io.FileInputStream     // Catch:{ IOException -> 0x0093, DrmException -> 0x008e, all -> 0x0087 }
            if (r2 == 0) goto L_0x0036
            r0 = r1
            java.io.FileInputStream r0 = (java.io.FileInputStream) r0     // Catch:{ IOException -> 0x0093, DrmException -> 0x008e, all -> 0x0087 }
            r9 = r0
            com.android.drm.mobile1.DrmRawContent r2 = new com.android.drm.mobile1.DrmRawContent     // Catch:{ IOException -> 0x0093, DrmException -> 0x008e, all -> 0x0087 }
            int r3 = r9.available()     // Catch:{ IOException -> 0x0093, DrmException -> 0x008e, all -> 0x0087 }
            java.lang.String r4 = "application/vnd.oma.drm.message"
            r2.<init>(r9, r3, r4)     // Catch:{ IOException -> 0x0093, DrmException -> 0x008e, all -> 0x0087 }
            java.lang.String r2 = r2.getContentType()     // Catch:{ IOException -> 0x0093, DrmException -> 0x008e, all -> 0x0087 }
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ IOException -> 0x002d }
        L_0x002b:
            r1 = r2
        L_0x002c:
            return r1
        L_0x002d:
            r1 = move-exception
            java.lang.String r3 = "Mms/compose"
            java.lang.String r3 = "IOException caught while closing stream"
            android.util.Log.e(r5, r6, r1)
            goto L_0x002b
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x003b:
            r1 = r7
            goto L_0x002c
        L_0x003d:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r5, r6, r1)
            goto L_0x003b
        L_0x0046:
            r1 = move-exception
            r2 = r7
        L_0x0048:
            java.lang.String r3 = "Mms/compose"
            java.lang.String r4 = "IOException caught while opening or reading stream"
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x008c }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x003b
        L_0x0055:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r5, r6, r1)
            goto L_0x003b
        L_0x005e:
            r1 = move-exception
            r2 = r7
        L_0x0060:
            java.lang.String r3 = "Mms/compose"
            java.lang.String r4 = "DrmException caught "
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x008c }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x003b
        L_0x006d:
            r1 = move-exception
            java.lang.String r2 = "Mms/compose"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r5, r6, r1)
            goto L_0x003b
        L_0x0076:
            r1 = move-exception
            r2 = r7
        L_0x0078:
            if (r2 == 0) goto L_0x007d
            r2.close()     // Catch:{ IOException -> 0x007e }
        L_0x007d:
            throw r1
        L_0x007e:
            r2 = move-exception
            java.lang.String r3 = "Mms/compose"
            java.lang.String r3 = "IOException caught while closing stream"
            android.util.Log.e(r5, r6, r2)
            goto L_0x007d
        L_0x0087:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0078
        L_0x008c:
            r1 = move-exception
            goto L_0x0078
        L_0x008e:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0060
        L_0x0093:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.mimeTypeOfDrmPart(com.google.android.mms.pdu.PduPart):java.lang.String");
    }

    /* access modifiers changed from: private */
    public void onChangeItem(View view, int i) {
        this.mGridView.setVisibility(0);
        InputMethodManager inputMethodManager = SmsApplication.getInstance().getInputMethodManager();
        IBinder windowToken = getCurrentFocus().getWindowToken();
        SmsApplication.getInstance().getInputMethodManager();
        inputMethodManager.hideSoftInputFromWindow(windowToken, 2);
        int i2 = this.titleIndex;
        this.titleIndex = i;
        this.mGridView = (GridView) findViewById(R.id.subgridview);
        if (i != 4) {
            this.mGridView.setNumColumns(3);
        }
        this.mGridView.setPadding(10, 10, 10, 10);
        this.mGridView.setSelector((int) R.color.alpha_00);
        this.mGridView.setGravity(17);
        if (i != 4) {
            if (i == 3) {
                this.mGridView.setBackgroundResource(R.drawable.background_rich_texting_category_smile);
            } else {
                this.mGridView.setBackgroundResource(R.drawable.background_rich_texting_category);
            }
        }
        switch (this.titleIndex) {
            case 0:
                this.mTabPhotoImageView = (Button) view;
                this.mTabPhotoImageView.setBackgroundResource(R.drawable.category_multi_media_selected);
                if (this.mTabSmsImageView != null) {
                    this.mTabSmsImageView.setBackgroundResource(R.drawable.category_sms_normal);
                }
                if (this.mTabActivityImageView != null) {
                    this.mTabActivityImageView.setBackgroundResource(R.drawable.category_rich_texting_norml);
                }
                if (this.mTabSmileImageView != null) {
                    this.mTabSmileImageView.setBackgroundResource(R.drawable.category_emoji_normal);
                }
                this.mGridView.setAdapter((ListAdapter) getMenuAdapter(getApplicationContext().getResources().getStringArray(R.array.default_bottom_photo_tab), new int[]{R.drawable.icon_gallery_item, R.drawable.icon_camera_item, R.drawable.icon_music_item, R.color.alpha_00}));
                return;
            case 1:
                this.mTabActivityImageView = (Button) view;
                this.mTabActivityImageView.setBackgroundResource(R.drawable.category_rich_texting_selected);
                if (this.mTabPhotoImageView != null) {
                    this.mTabPhotoImageView.setBackgroundResource(R.drawable.category_multi_media_normal);
                }
                if (this.mTabSmsImageView != null) {
                    this.mTabSmsImageView.setBackgroundResource(R.drawable.category_sms_normal);
                }
                if (this.mTabSmileImageView != null) {
                    this.mTabSmileImageView.setBackgroundResource(R.drawable.category_emoji_normal);
                }
                this.mGridView.setAdapter((ListAdapter) getMenuAdapter(getApplicationContext().getResources().getStringArray(R.array.default_bottom_activity_tab), new int[]{R.drawable.icon_voting_item, R.drawable.icon_hot_voting_item, R.drawable.icon_calling_card_item, R.color.alpha_00}));
                return;
            case 2:
                this.mTabSmsImageView = (Button) view;
                this.mTabSmsImageView.setBackgroundResource(R.drawable.category_sms_selected);
                if (this.mTabActivityImageView != null) {
                    this.mTabActivityImageView.setBackgroundResource(R.drawable.category_rich_texting_norml);
                }
                if (this.mTabPhotoImageView != null) {
                    this.mTabPhotoImageView.setBackgroundResource(R.drawable.category_multi_media_normal);
                }
                if (this.mTabSmileImageView != null) {
                    this.mTabSmileImageView.setBackgroundResource(R.drawable.category_emoji_normal);
                }
                this.mGridView.setAdapter((ListAdapter) getMenuAdapter(getApplicationContext().getResources().getStringArray(R.array.default_bottom_sms_tab), new int[]{R.drawable.icon_bless_item, R.drawable.icon_festival_item, R.drawable.icon_function_item, R.drawable.icon_favorite_item}));
                return;
            case 3:
                this.mGridView.setNumColumns(4);
                this.mGridView.setGravity(17);
                this.mGridView.setAdapter((ListAdapter) SmileyParser.getSmileyAdapter(getApplicationContext()));
                this.mTabSmileImageView = (Button) view;
                this.mTabSmileImageView.setBackgroundResource(R.drawable.category_emoji_selected);
                if (this.mTabSmsImageView != null) {
                    this.mTabSmsImageView.setBackgroundResource(R.drawable.category_sms_normal);
                }
                if (this.mTabPhotoImageView != null) {
                    this.mTabPhotoImageView.setBackgroundResource(R.drawable.category_multi_media_normal);
                }
                if (this.mTabActivityImageView != null) {
                    this.mTabActivityImageView.setBackgroundResource(R.drawable.category_rich_texting_norml);
                    return;
                }
                return;
            case 4:
                this.mGridView.setVisibility(8);
                if (isPreparedForSending()) {
                    confirmSendMessageIfNeeded();
                }
                this.titleIndex = i2;
                return;
            default:
                return;
        }
    }

    private void onKeyboardStateChanged(boolean z) {
        if (z) {
            if (this.mRecipientsEditor != null) {
                this.mRecipientsEditor.setFocusableInTouchMode(true);
            }
            if (this.mSubjectTextEditor != null) {
                this.mSubjectTextEditor.setFocusableInTouchMode(true);
            }
            this.mTextEditor.setFocusableInTouchMode(true);
            this.mTextEditor.setHint((int) R.string.type_to_compose_text_enter_to_send);
            return;
        }
        if (this.mRecipientsEditor != null) {
            this.mRecipientsEditor.setFocusable(false);
        }
        if (this.mSubjectTextEditor != null) {
            this.mSubjectTextEditor.setFocusable(false);
        }
        this.mTextEditor.setFocusable(false);
        this.mTextEditor.setHint((int) R.string.open_keyboard_to_compose_message);
    }

    private int recipientCount() {
        return isRecipientsEditorVisible() ? this.mRecipientsEditor.getRecipientCount() : getRecipients().size();
    }

    /* access modifiers changed from: private */
    public void removeRecipientsListeners() {
        getRecipients().removeListeners(this);
    }

    private void resetCounter() {
        this.mTextCounter.setText((int) R.string.text_count_chn);
        this.mTextCounter.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void resetMessage() {
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("resetMessage");
        }
        this.mMusicAttachmentView.hideView();
        this.mImageAttachmentView.setVisibility(8);
        this.mAttachmentEditor.hideView();
        this.mTextEditor.requestFocus();
        this.mTextEditor.removeTextChangedListener(this.mTextEditorWatcher);
        TextKeyListener.clear(this.mTextEditor.getText());
        this.mWorkingMessage = WorkingMessage.createEmpty(this);
        this.mWorkingMessage.setConversation(this.mConversation);
        hideRecipientEditor();
        drawBottomPanel();
        updateWindowTitle();
        this.mAttachmentEditor.update(this.mWorkingMessage);
        updateSendButtonState();
        this.mTextEditor.addTextChangedListener(this.mTextEditorWatcher);
        if (this.mIsLandscape) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.mTextEditor.getWindowToken(), 0);
        }
        this.mLastRecipientCount = 0;
    }

    private void resetMessageAtBeginning() {
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("resetMessageAtBeginning");
        }
        hideRecipientEditor();
        drawBottomPanel();
        updateWindowTitle();
        this.mAttachmentEditor.update(this.mWorkingMessage);
    }

    private String returnSelectedName(String[] strArr) {
        StringBuffer stringBuffer = new StringBuffer();
        if (strArr != null && strArr.length > 0) {
            for (int i = 0; i < strArr.length; i++) {
                String obj = this.mRecipientsEditor.getText().toString();
                String str = strArr[i];
                if (str.contains("<") && str.contains(">")) {
                    str = str.substring(str.indexOf("<") + 1, str.indexOf(">"));
                }
                if (!obj.contains(str)) {
                    if (!"".equals(obj.trim()) && !obj.trim().endsWith(",")) {
                        stringBuffer.append(",");
                    }
                    stringBuffer.append(strArr[i]).append(",");
                    cacheNameNum.put(str, strArr[i].substring(0, strArr[i].indexOf("<")));
                }
            }
        }
        return stringBuffer.toString();
    }

    private void saveDraft() {
        if (this.vote_cli_id > 0 && this.mVoteItem != null) {
            showVoteDraftSaveDialog(this.mVoteItem);
        } else if (!this.mWorkingMessage.isDiscarded() && !this.mWaitingForSubActivity) {
            if (this.mWaitingForSubActivity || this.mWorkingMessage.isWorthSaving()) {
                if (Log.isLoggable(LogTag.APP, 2)) {
                    log("saveDraft: call WorkingMessage.saveDraft");
                }
                this.mWorkingMessage.saveDraft();
                Toast.makeText(this, (int) R.string.message_saved_as_draft, 0).show();
                goToConversationList();
                return;
            }
            if (Log.isLoggable(LogTag.APP, 2)) {
                log("saveDraft: not worth saving, discard WorkingMessage and bail");
            }
            this.mWorkingMessage.discard();
        }
    }

    /* access modifiers changed from: private */
    public void sendMessage(boolean z) {
        if (z && Boolean.parseBoolean(SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
            try {
                startActivityForResult(new Intent(TelephonyIntents.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS, (Uri) null), 17);
                return;
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "Cannot find EmergencyCallbackModeExitDialog", e);
            }
        }
        removeRecipientsListeners();
        if (this.mWorkingMessage.getToSendImage() == null && this.mWorkingMessage.getImageItem() == null) {
            this.mWorkingMessage.send();
        } else if (this.mWorkingMessage.getToSendImage() != null) {
            sendSmsWithImage();
        } else if (this.mWorkingMessage.getImageItem() != null) {
            forwardSmsWithImage();
        }
        this.mSentMessage = true;
        addRecipientsListeners();
        bindToContactHeader(this.mConversation.getRecipients());
        this.mMsgListAdapter.setMultiRecipient(this.mMultiRecipient);
        this.mMsgListAdapter.notifyDataSetChanged();
        if (this.mWorkingMessage.getSongItem() != null) {
            Util.logEvent(PrefsUtil.EVENT_MUSIC_SEND, new NameValuePair[0]);
        }
        Util.logEvent(PrefsUtil.EVENT_SMS_SEND, new NameValuePair[0]);
        if ("sms_template".equals(this.appType)) {
            Util.logEvent("sms_template_send", new NameValuePair[0]);
        }
        if (this.mExitOnSent) {
            finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.ui.ComposeMessageActivity, vc.lx.sms.data.ContactList, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.util.List<java.lang.String>, int, int, vc.lx.sms.ui.ComposeMessageActivity]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    private void sendSmsWithImage() {
        ContactList recipients;
        Conversation conversation;
        Util.logEvent(PrefsUtil.EVENT_IMAGE_SEND, new NameValuePair[0]);
        ImagesDbService imagesDbService = new ImagesDbService(getApplicationContext());
        String join = TextUtils.join(",", getRecipients().getNumbers());
        if (join.length() == 0) {
            join = TextUtils.join(",", this.mRecipientsEditor.getNumbers());
        }
        long insertImageDump = imagesDbService.insertImageDump(this.mWorkingMessage.getToSendImage(), join, this.mTextEditor.getText().toString());
        if (insertImageDump > 0) {
            if (this.mUploadImageJob != null) {
                this.mUploadImageJob.cancel();
            }
            if (this.mRecipientsEditor != null) {
                List<String> numbers = this.mRecipientsEditor.getNumbers();
                if (this.mConversation.ensureThreadId() > 0) {
                    Conversation conversation2 = this.mConversation;
                    recipients = this.mConversation.getRecipients();
                    conversation = conversation2;
                } else {
                    Conversation conversation3 = Conversation.get((Context) this, ContactList.getByNumbers(numbers, false, true, (Context) this), false);
                    recipients = this.mConversation.getRecipients();
                    conversation = conversation3;
                }
                this.mUploadImageJob = new UploadImageJob(insertImageDump, getApplicationContext(), recipients, conversation);
            } else {
                this.mUploadImageJob = new UploadImageJob(insertImageDump, getApplicationContext(), getRecipients(), this.mConversation);
            }
            this.mUploadImageJob.start();
            showUploadImageDialog();
        }
    }

    private void showBottomPanel() {
        initPopupMenu();
        if (this.mPanelTab == null || this.mPanelTab.getVisibility() != 8) {
            this.mPanelTab.setVisibility(8);
        } else {
            this.mPanelTab.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void showDeliveryReport(long j, String str) {
        Intent intent = new Intent(this, DeliveryReportActivity.class);
        intent.putExtra("message_id", j);
        intent.putExtra("message_type", str);
        startActivity(intent);
    }

    private void showImageInTheAttachment() {
        this.mTmpUploadImagePath = Environment.getExternalStorageDirectory() + "/.tmp_image_selection.jpg";
        String join = TextUtils.join(",", getRecipients().getNumbers());
        if (join.length() == 0) {
            join = TextUtils.join(",", this.mRecipientsEditor.getNumbers());
        }
        ImagesDbService imagesDbService = new ImagesDbService(getApplicationContext());
        Bitmap decodeFile = BitmapFactory.decodeFile(this.mTmpUploadImagePath);
        getContentResolver().delete(ImageDumpContentProvider.CONTENT_URI, "recipient = ?", new String[]{join});
        this.mWorkingMessage.updateToSendImage(decodeFile);
        this.mMusicAttachmentView.update(this.mWorkingMessage);
        findViewById(R.id.music_panel).setVisibility(8);
        this.musicPlug = "";
        deleteVoteAttachment();
        if (decodeFile != null) {
            try {
                Bitmap resampleImage = ImageUtils.resampleImage(this.mTmpUploadImagePath, (int) PduHeaders.PREVIOUSLY_SENT_BY);
                this.mImageAttachment.setVisibility(0);
                this.mImageAttachment.setImageBitmap(resampleImage);
                if (join.length() > 0) {
                    imagesDbService.insertImageDump(this.mWorkingMessage.getToSendImage(), join, this.mTextEditor.getText().toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        updateSendButtonState();
        this.mImageAttachmentView.setVisibility(0);
    }

    private void showUploadImageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.uploading_image);
        builder.setMessage((int) R.string.uploading_image_title);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                if (ComposeMessageActivity.this.mUploadImageJob != null) {
                    ComposeMessageActivity.this.mUploadImageJob.cancel();
                }
                String unused = ComposeMessageActivity.this.mTmpUploadImagePath = null;
            }
        });
        this.mUploadImageDlg = builder.create();
        this.mUploadImageDlg.show();
    }

    private void showVoteDraftSaveDialog(VoteItem voteItem) {
        new AlertDialog.Builder(this).setMessage((int) R.string.save_draft_or_not).setPositiveButton((int) R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ComposeMessageActivity.this.saveToVoteDraft();
                if (ComposeMessageActivity.this.mWorkingMessage.getText().toString().equals("")) {
                    ComposeMessageActivity.this.mWorkingMessage.setText(" ");
                }
                ComposeMessageActivity.this.mWorkingMessage.saveDraft();
                ComposeMessageActivity.this.finish();
            }
        }).setNegativeButton((int) R.string.discard_message, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                ComposeMessageActivity.this.mConversation.ensureThreadId();
                ComposeMessageActivity.this.asyncDeleteDraftSmsMessage(ComposeMessageActivity.this.mConversation);
                ComposeMessageActivity.this.finish();
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void startMsgListQuery() {
        Uri uri = this.mConversation.getUri();
        if (uri != null) {
            this.mBackgroundQueryHandler.cancelOperation(MESSAGE_LIST_QUERY_TOKEN);
            try {
                this.mBackgroundQueryHandler.startQuery(MESSAGE_LIST_QUERY_TOKEN, null, uri, MessageListAdapter.PROJECTION, null, null, null);
            } catch (SQLiteException e) {
                SqliteWrapper.checkSQLiteException(this, e);
            }
        }
    }

    private void toastConvertInfo(boolean z) {
        Toast.makeText(this, z ? R.string.converting_to_picture_message : R.string.converting_to_text_message, 0).show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.telephony.SmsMessage.calculateLength(java.lang.CharSequence, boolean):int[]}
     arg types: [java.lang.CharSequence, int]
     candidates:
      ClspMth{android.telephony.SmsMessage.calculateLength(java.lang.String, boolean):int[]}
      ClspMth{android.telephony.SmsMessage.calculateLength(java.lang.CharSequence, boolean):int[]} */
    /* access modifiers changed from: private */
    public void updateCounter(CharSequence charSequence, int i, int i2, int i3) {
        int[] calculateLength = SmsMessage.calculateLength(charSequence, false);
        int i4 = calculateLength[0];
        int i5 = calculateLength[2];
        int i6 = calculateLength[1];
        this.mWorkingMessage.setLengthRequiresMms(i4 >= MmsConfig.getSmsToMmsTextThreshold());
        if (!this.mWorkingMessage.requiresMms() && (i4 > 1 || i6 >= 1)) {
            this.mTextCounter.setText(i5 + " / " + i4);
            this.mTextCounter.setVisibility(0);
            return;
        }
        this.mTextCounter.setVisibility(8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void updateDraftSmsMessage(long j, String str) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("updateDraftSmsMessage tid=%d, contents=\"%s\"", Long.valueOf(j), str);
        }
        if (j > 0) {
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("thread_id", Long.valueOf(j));
            contentValues.put(Telephony.TextBasedSmsColumns.BODY, str);
            contentValues.put("type", (Integer) 3);
            SqliteWrapper.insert(this, getContentResolver(), Telephony.Sms.CONTENT_URI, contentValues);
        }
    }

    private void updateDraftSmsMessage(Conversation conversation, String str) {
        long ensureThreadId = conversation.ensureThreadId();
        conversation.setDraftState(true);
        updateDraftSmsMessage(ensureThreadId, str);
    }

    /* access modifiers changed from: private */
    public void updatePresence(Contact contact) {
    }

    /* access modifiers changed from: private */
    public void updateSendButtonState() {
        boolean z;
        if (!isPreparedForSending()) {
            if (this.mAttachmentEditor != null) {
                this.mAttachmentEditor.setCanSend(false);
            }
            z = false;
        } else if (!this.mWorkingMessage.hasSlideshow()) {
            z = true;
        } else {
            this.mAttachmentEditor.setCanSend(true);
            z = false;
        }
        if (this.mSendButton != null) {
            this.mSendButton.setEnabled(z);
            this.mSendButton.setFocusable(z);
        }
    }

    /* access modifiers changed from: private */
    public void updateSendFailedNotification() {
        final long threadId = this.mConversation.getThreadId();
        if (threadId > 0) {
            new Thread(new Runnable() {
                public void run() {
                    MessagingNotification.updateSendFailedNotificationForThread(ComposeMessageActivity.this, threadId);
                }
            }).run();
        }
    }

    /* access modifiers changed from: private */
    public void updateWindowTitle() {
    }

    public void cancelMultiMode() {
        this.mIsMultiMode = false;
        findViewById(R.id.scrollview).setVisibility(0);
        findViewById(R.id.multi_actions_view).setVisibility(8);
        this.mMsgListAdapter.setMultiMode(this.mIsMultiMode);
        this.mMsgListAdapter.clearSelectedIds();
        this.mMsgListAdapter.notifyDataSetInvalidated();
    }

    /* access modifiers changed from: protected */
    public JSONObject createForwardJsonObject() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        List<String> numbers = this.mRecipientsEditor.getNumbers();
        if (numbers == null || numbers.size() <= 0) {
            return null;
        }
        int i = 0;
        while (i < numbers.size()) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("name", numbers.get(i));
                jSONObject2.put("number", numbers.get(i));
                jSONArray.put(jSONObject2);
                i++;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        jSONObject.put("friends", jSONArray);
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public JSONObject createSendJsonObject() {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("title", this.mVoteItem.title);
            jSONObject2.put("cli_id", this.mVoteItem.cli_id);
            JSONObject jSONObject3 = new JSONObject();
            for (int i = 0; i < this.mVoteItem.options.size(); i++) {
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("cli_id", this.mVoteItem.options.get(i).cli_id);
                jSONObject4.put(SmsSqliteHelper.CONTENT, this.mVoteItem.options.get(i).content);
                jSONObject3.put(MMHttpDefines.TAG_ITEM + this.mVoteItem.options.get(i).display_order, jSONObject4);
            }
            jSONObject2.put("options", jSONObject3);
            jSONObject.put("voteinfo", jSONObject2);
            JSONArray jSONArray = new JSONArray();
            for (int i2 = 0; i2 < this.mConversation.getRecipients().getNumbers().length; i2++) {
                JSONObject jSONObject5 = new JSONObject();
                jSONObject5.put("name", ((Contact) this.mConversation.getRecipients().get(i2)).getName());
                jSONObject5.put("number", ((Contact) this.mConversation.getRecipients().get(i2)).getNumber());
                jSONArray.put(jSONObject5);
            }
            jSONObject.put("friends", jSONArray);
            return jSONObject;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void deleteOldVoteDraft() {
        String join = TextUtils.join(",", getRecipients().getNumbers());
        if (join.length() == 0 && this.mRecipientsEditor != null) {
            join = TextUtils.join(",", this.mRecipientsEditor.getNumbers());
        }
        getApplicationContext().getContentResolver().delete(VoteContentProvider.VOTEDRAFT_CONTENT_URI, " numbers = ? ", new String[]{join});
    }

    public void loadMusicDraft(String str) {
        if (str != null) {
            Util.getFullShortenUrl(getApplicationContext(), str);
            SongItem songItemByPlug = new TopMusicService(getApplicationContext()).getSongItemByPlug(str);
            final RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.music_panel);
            if (songItemByPlug == null) {
                relativeLayout.setVisibility(8);
                return;
            }
            deleteImageAttachment();
            deleteVoteAttachment();
            relativeLayout.setVisibility(0);
            ((TextView) findViewById(R.id.music_info)).setText(songItemByPlug.song + "\n" + songItemByPlug.singer);
            ((ImageView) findViewById(R.id.music_delete)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    relativeLayout.setVisibility(8);
                    String unused = ComposeMessageActivity.this.musicPlug = "";
                    ComposeMessageActivity.this.deleteMusicAttachment();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void loadVoteDraft() {
        this.mHandler.post(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:21:0x00d7  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x00ef  */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x00f3  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r13 = this;
                    r4 = 1
                    r7 = 0
                    r2 = 0
                    java.lang.String r3 = ","
                    java.lang.String r11 = ""
                    java.lang.String r0 = ","
                    vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                    vc.lx.sms.data.ContactList r0 = r0.getRecipients()
                    java.lang.String[] r0 = r0.getNumbers()
                    java.lang.String r0 = android.text.TextUtils.join(r3, r0)
                    vc.lx.sms.cmcc.http.data.VoteItem r6 = new vc.lx.sms.cmcc.http.data.VoteItem
                    r6.<init>()
                    java.lang.String r1 = ""
                    java.lang.String r1 = ""
                    int r1 = r0.length()
                    if (r1 != 0) goto L_0x010f
                    vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this
                    com.android.mms.ui.RecipientsEditor r1 = r1.mRecipientsEditor
                    if (r1 == 0) goto L_0x010f
                    java.lang.String r0 = ","
                    vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                    com.android.mms.ui.RecipientsEditor r0 = r0.mRecipientsEditor
                    java.util.List r0 = r0.getNumbers()
                    java.lang.String r0 = android.text.TextUtils.join(r3, r0)
                    r5 = r0
                L_0x003f:
                    int r0 = r5.length()
                    if (r0 == 0) goto L_0x010b
                    vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                    android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEDRAFT_CONTENT_URI
                    java.lang.String r3 = " numbers = ? "
                    java.lang.String[] r4 = new java.lang.String[r4]
                    r4[r7] = r5
                    r5 = r2
                    android.database.Cursor r0 = r0.managedQuery(r1, r2, r3, r4, r5)
                    boolean r1 = r0.moveToNext()
                    if (r1 == 0) goto L_0x0108
                    vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this
                    java.lang.String r2 = "vote_id"
                    int r2 = r0.getColumnIndex(r2)
                    long r2 = r0.getLong(r2)
                    long unused = r1.vote_cli_id = r2
                    java.lang.String r1 = "numbers"
                    int r1 = r0.getColumnIndex(r1)
                    r0.getString(r1)
                    java.lang.String r1 = "sms_content"
                    int r1 = r0.getColumnIndex(r1)
                    java.lang.String r1 = r0.getString(r1)
                    vc.lx.sms.ui.ComposeMessageActivity r2 = vc.lx.sms.ui.ComposeMessageActivity.this
                    r2.updateSendButtonState()
                    vc.lx.sms.ui.ComposeMessageActivity r2 = vc.lx.sms.ui.ComposeMessageActivity.this
                    android.view.View r2 = r2.mImageAttachmentView
                    r2.setVisibility(r7)
                    r7 = r0
                    r8 = r1
                L_0x008c:
                    vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    r2 = 0
                    java.lang.String r3 = " _id = ? "
                    r4 = 1
                    java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    r5 = 0
                    vc.lx.sms.ui.ComposeMessageActivity r9 = vc.lx.sms.ui.ComposeMessageActivity.this     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    long r9 = r9.vote_cli_id     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    r4[r5] = r9     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    r5 = 0
                    android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00e3, all -> 0x00eb }
                    int r1 = r0.getCount()     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                    if (r1 <= 0) goto L_0x00d0
                    boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                    if (r1 == 0) goto L_0x00d0
                    vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                    long r1 = r1.vote_cli_id     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                    r6.cli_id = r1     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                    java.lang.String r1 = "title"
                    int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                    java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                    r6.title = r1     // Catch:{ Exception -> 0x0106, all -> 0x0101 }
                L_0x00d0:
                    if (r0 == 0) goto L_0x00d5
                    r0.close()
                L_0x00d5:
                    if (r8 == 0) goto L_0x00f3
                    vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                    vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this
                    long r1 = r1.vote_cli_id
                    r0.displayVoteAttachment(r1, r8)
                L_0x00e2:
                    return
                L_0x00e3:
                    r0 = move-exception
                    r0 = r7
                L_0x00e5:
                    if (r0 == 0) goto L_0x00d5
                    r0.close()
                    goto L_0x00d5
                L_0x00eb:
                    r0 = move-exception
                    r1 = r7
                L_0x00ed:
                    if (r1 == 0) goto L_0x00f2
                    r1.close()
                L_0x00f2:
                    throw r0
                L_0x00f3:
                    vc.lx.sms.ui.ComposeMessageActivity r0 = vc.lx.sms.ui.ComposeMessageActivity.this
                    vc.lx.sms.ui.ComposeMessageActivity r1 = vc.lx.sms.ui.ComposeMessageActivity.this
                    long r1 = r1.vote_cli_id
                    java.lang.String r3 = ""
                    r0.displayVoteAttachment(r1, r11)
                    goto L_0x00e2
                L_0x0101:
                    r1 = move-exception
                    r12 = r1
                    r1 = r0
                    r0 = r12
                    goto L_0x00ed
                L_0x0106:
                    r1 = move-exception
                    goto L_0x00e5
                L_0x0108:
                    r7 = r0
                    r8 = r11
                    goto L_0x008c
                L_0x010b:
                    r7 = r2
                    r8 = r11
                    goto L_0x008c
                L_0x010f:
                    r5 = r0
                    goto L_0x003f
                */
                throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ComposeMessageActivity.AnonymousClass12.run():void");
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        this.mWaitingForSubActivity = false;
        addRecipientsListeners();
        if (i != 20) {
            if (intent == null) {
                return;
            }
        } else if (i2 != -1) {
            return;
        }
        switch (i) {
            case 17:
                if (intent.getBooleanExtra(EXIT_ECM_RESULT, false)) {
                    sendMessage(false);
                    return;
                }
                return;
            case 18:
                String returnSelectedName = returnSelectedName(intent.getStringArrayExtra("selected_names"));
                this.mRecipientsEditor.append(returnSelectedName.toString());
                Log.i("TAG", returnSelectedName.toString());
                addRecipientsListeners();
                bindToContactHeader(this.mRecipientsEditor.constructContactsFromInput());
                return;
            case 19:
            case 20:
                applyImageFilters(intent, i);
                return;
            case 21:
                showImageInTheAttachment();
                return;
            case 22:
                String[] stringArrayExtra = intent.getStringArrayExtra("selected_names");
                StringBuffer stringBuffer = new StringBuffer();
                if (this.mTextEditor.getText() != null) {
                    stringBuffer.append(this.mTextEditor.getText().toString());
                }
                String substring = stringBuffer.substring(0, this.mTextEditor.getSelectionEnd());
                String substring2 = stringBuffer.substring(this.mTextEditor.getSelectionEnd(), stringBuffer.length());
                if (stringArrayExtra == null || stringArrayExtra.length <= 0) {
                    str = substring;
                } else {
                    String str2 = substring;
                    for (int i3 = 0; i3 < stringArrayExtra.length; i3++) {
                        stringArrayExtra[i3] = stringArrayExtra[i3].replace("<", ": ");
                        stringArrayExtra[i3] = stringArrayExtra[i3].replace(">", MessageSender.RECIPIENTS_SEPARATOR);
                        str2 = str2 + stringArrayExtra[i3];
                    }
                    str = str2;
                }
                this.mTextEditor.setText(str + substring2);
                this.mTextEditor.setSelection(str.length());
                return;
            case 23:
                String stringExtra = intent.getStringExtra(PrefsUtil.KEY_TEMPLATE_RETURNED);
                if (stringExtra != null) {
                    this.mTextEditor.getText().insert(this.mTextEditor.getSelectionStart(), stringExtra);
                    this.appType = "sms_template";
                    return;
                }
                return;
            case 24:
                initMusicForward(intent);
                if (this.mGridView != null) {
                    this.mGridView.setVisibility(8);
                    return;
                }
                return;
            case 25:
                displayVoteAttachment(intent.getLongExtra("vote_cli_id", 0), intent.getStringExtra("sms_body"));
                return;
            default:
                return;
        }
    }

    public void onAttachmentChanged() {
        drawBottomPanel();
        updateSendButtonState();
        this.mAttachmentEditor.update(this.mWorkingMessage);
    }

    public void onClick(View view) {
        if (view == this.mSelectMusicButon) {
        }
        if (view == this.mRemoveImageAttachmentButton) {
            deleteImageAttachment();
        }
        if (view == this.mDelAllBtn) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.menu_delete_all);
            builder.setIcon(17301543);
            builder.setCancelable(true);
            builder.setMessage((int) R.string.confirm_delete_all);
            builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    ComposeMessageActivity.this.mMessageListItemHandler.post(new Runnable() {
                        public void run() {
                            Conversation.startDelete(ComposeMessageActivity.this.mBackgroundQueryHandler, ConversationListActivity.DELETE_CONVERSATION_TOKEN, true, ComposeMessageActivity.this.mConversation.getThreadId());
                            ComposeMessageActivity.this.cancelMultiMode();
                        }
                    });
                }
            });
            builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
            builder.show();
        }
        if (view == this.mDelSelectedBtn) {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setTitle((int) R.string.menu_delete_selected_items);
            builder2.setIcon(17301543);
            builder2.setCancelable(true);
            builder2.setMessage((int) R.string.confirm_delete_selected_messages);
            builder2.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    ComposeMessageActivity.this.mMessageListItemHandler.post(new Runnable() {
                        public void run() {
                            HashSet<String> selectedIds = ComposeMessageActivity.this.mMsgListAdapter.getSelectedIds();
                            if (selectedIds.size() != 0) {
                                String[] strArr = new String[selectedIds.size()];
                                selectedIds.toArray(strArr);
                                for (String str : strArr) {
                                    ComposeMessageActivity.this.mBackgroundQueryHandler.startDelete(ComposeMessageActivity.DELETE_MESSAGE_TOKEN, null, ContentUris.withAppendedId("sms".equals(str.split("#")[0]) ? Telephony.Sms.CONTENT_URI : Telephony.Mms.CONTENT_URI, Long.valueOf(str.split("#")[1]).longValue()), null, null);
                                }
                            }
                        }
                    });
                }
            });
            builder2.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
            builder2.show();
        }
        if (view == this.mCancelMultiModeBtn) {
            cancelMultiMode();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.mIsKeyboardOpen = configuration.keyboardHidden == 1;
        boolean z = configuration.orientation == 2;
        if (this.mIsLandscape != z) {
            this.mIsLandscape = z;
            this.mAttachmentEditor.update(this.mWorkingMessage);
        }
        onKeyboardStateChanged(this.mIsKeyboardOpen);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.mInflater = LayoutInflater.from(this);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.mScreenWidth = displayMetrics.widthPixels;
        this.mScreenHeight = displayMetrics.heightPixels;
        this.mDensity = displayMetrics.density;
        this.mDensityDpi = displayMetrics.densityDpi;
        setContentView((int) R.layout.compose_message_activity);
        setProgressBarVisibility(false);
        getWindow().setSoftInputMode(18);
        initResourceRefs();
        this.mContentResolver = getContentResolver();
        this.mBackgroundQueryHandler = new BackgroundQueryHandler(this.mContentResolver);
        this.mRrm = SmsApplication.getInstance().getRemoteResourceManager();
        this.mResourcesObserver = new RemoteResourceManagerObserver();
        this.mRrm.addObserver(this.mResourcesObserver);
        initialize(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        preparePopupWindowMenu();
        if (!this.mIsMultiMode) {
            this.mPopupWindow.showAtLocation(findViewById(R.id.compose_main), 81, 0, 0);
            Util.logEvent("compose_message_menu_pop", new NameValuePair[0]);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent == null) {
            if (isPreparedForSending()) {
                confirmSendMessageIfNeeded();
            }
            return true;
        } else if (i == 0) {
            return false;
        } else {
            if (keyEvent.isShiftPressed()) {
                return false;
            }
            if (isPreparedForSending()) {
                confirmSendMessageIfNeeded();
            }
            return true;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                if (this.mIsMultiMode) {
                    cancelMultiMode();
                    return true;
                } else if (this.mGridView == null || this.mGridView.getVisibility() != 0) {
                    exitComposeMessageActivity(new Runnable() {
                        public void run() {
                            ComposeMessageActivity.this.goToConversationList();
                        }
                    }, false);
                    return false;
                } else {
                    this.mGridView.setVisibility(8);
                    return true;
                }
            case 23:
            case 66:
                if (isPreparedForSending()) {
                    confirmSendMessageIfNeeded();
                    return true;
                }
                break;
            case 67:
                if (this.mMsgListAdapter != null && this.mMsgListView.isFocused()) {
                    try {
                        Cursor cursor = (Cursor) this.mMsgListView.getSelectedItem();
                        if (cursor != null) {
                            boolean z = cursor.getInt(19) != 0;
                            confirmDeleteDialog(new DeleteMessageListener(cursor.getLong(1), cursor.getString(0), z), z);
                            return true;
                        }
                    } catch (ClassCastException e) {
                        Log.e(TAG, "Unexpected ClassCastException.", e);
                        return super.onKeyDown(i, keyEvent);
                    }
                }
                break;
            case 84:
                return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onMaxPendingMessagesReached() {
        saveDraft();
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(ComposeMessageActivity.this, (int) R.string.too_many_unsent_mms, 1).show();
            }
        });
    }

    public void onMessageSent() {
        if (this.mMsgListAdapter.getCount() == 0) {
            startMsgListQuery();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.ui.ComposeMessageActivity, long, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
     arg types: [vc.lx.sms.ui.ComposeMessageActivity, android.net.Uri, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation */
    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        boolean z;
        Conversation conversation;
        super.onNewIntent(intent);
        setIntent(intent);
        long longExtra = intent.getLongExtra("thread_id", 0);
        Uri data = intent.getData();
        if (longExtra > 0) {
            conversation = Conversation.get((Context) this, longExtra, false);
            z = false;
        } else {
            if (this.mConversation.getThreadId() == 0) {
                this.mWorkingMessage.syncWorkingRecipients();
                z = this.mConversation.sameRecipient(data);
            } else {
                z = false;
            }
            conversation = !z ? Conversation.get((Context) this, data, false) : null;
        }
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("onNewIntent: data=" + data + ", thread_id extra is " + longExtra);
            log("     new conversation=" + conversation + ", mConversation=" + this.mConversation);
        }
        long threadId = conversation == null ? 0 : conversation.getThreadId();
        if (z || (threadId != 0 && threadId == this.mConversation.getThreadId())) {
            if (Log.isLoggable(LogTag.APP, 2)) {
                log("onNewIntent: same conversation");
            }
            addRecipientsListeners();
            return;
        }
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("onNewIntent: different conversation, initialize...");
        }
        saveDraft();
        initialize(null);
        loadMessageContent();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                confirmDeleteThread(this.mConversation.getThreadId());
                break;
            case 3:
                this.mWorkingMessage.discard();
                finish();
                break;
            case 4:
                if (isPreparedForSending()) {
                    confirmSendMessageIfNeeded();
                    break;
                }
                break;
            case 5:
                dialRecipient();
                break;
            case 6:
                exitComposeMessageActivity(new Runnable() {
                    public void run() {
                        ComposeMessageActivity.this.goToConversationList();
                    }
                }, false);
                break;
            case 12:
                ContactList recipients = getRecipients();
                if (recipients.size() == 1 && ((Contact) recipients.get(0)).existsInDatabase()) {
                    Intent intent = new Intent("android.intent.action.VIEW", ((Contact) recipients.get(0)).getUri());
                    intent.setFlags(524288);
                    startActivity(intent);
                    break;
                }
            case 13:
                startActivity(ConversationListActivity.createAddContactIntent(((Contact) this.mConversation.getRecipients().get(0)).getNumber()));
                return true;
            case 19:
                onSearchRequested();
                break;
            case MENU_ADD_ADDRESS_TO_CONTACTS /*27*/:
                return false;
            case MENU_MULTI_MODE /*32*/:
                this.mIsMultiMode = true;
                findViewById(R.id.multi_actions_view).setVisibility(8);
                this.mMsgListAdapter.setMultiMode(this.mIsMultiMode);
                this.mMsgListAdapter.notifyDataSetInvalidated();
                break;
            case MENU_DELETE_SELECTED /*33*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.menu_delete_selected_items);
                builder.setIcon(17301543);
                builder.setCancelable(true);
                builder.setMessage((int) R.string.confirm_delete_selected_messages);
                builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ComposeMessageActivity.this.mMessageListItemHandler.post(new Runnable() {
                            public void run() {
                                HashSet<String> selectedIds = ComposeMessageActivity.this.mMsgListAdapter.getSelectedIds();
                                if (selectedIds.size() != 0) {
                                    String[] strArr = new String[selectedIds.size()];
                                    selectedIds.toArray(strArr);
                                    for (String str : strArr) {
                                        ComposeMessageActivity.this.mBackgroundQueryHandler.startDelete(ComposeMessageActivity.DELETE_MESSAGE_TOKEN, null, ContentUris.withAppendedId("sms".equals(str.split("#")[0]) ? Telephony.Sms.CONTENT_URI : Telephony.Mms.CONTENT_URI, Long.valueOf(str.split("#")[1]).longValue()), null, null);
                                    }
                                }
                            }
                        });
                    }
                });
                builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
                builder.show();
                break;
            case MENU_CANCEL_MULTI_MODE /*34*/:
                this.mIsMultiMode = false;
                this.mMsgListAdapter.setMultiMode(this.mIsMultiMode);
                this.mMsgListAdapter.clearSelectedIds();
                this.mMsgListAdapter.notifyDataSetInvalidated();
                break;
            case MENU_MUSIC_BOARDS /*35*/:
                startActivity(new Intent(this, MusicPlatformActivity.class));
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        removeRecipientsListeners();
        try {
            unregisterReceiver(this.mImageForwardReceiver);
            if (isFinishing()) {
                unregisterReceiver(this.mImageUploadReceiver);
            }
        } catch (IllegalArgumentException | IllegalStateException e) {
        }
        exitComposeMessageActivity(new Runnable() {
            public void run() {
            }
        }, true);
        if (isFinishing() && this.mRrm != null) {
            this.mRrm.deleteObserver(this.mResourcesObserver);
        }
    }

    public void onPreMessageSent() {
        runOnUiThread(this.mResetMessageRunnable);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (!this.mIsMultiMode) {
            ContactList recipients = getRecipients();
            if (recipients.size() != 1 || !((Contact) recipients.get(0)).existsInDatabase()) {
                menu.add(0, 13, 0, (int) R.string.menu_add_to_contacts).setIcon((int) R.drawable.ic_menu_edit);
            } else {
                menu.add(0, 12, 0, (int) R.string.menu_view_contact).setIcon((int) R.drawable.ic_menu_contact);
            }
            menu.add(0, (int) MENU_MULTI_MODE, 0, (int) R.string.menu_multi_mode).setIcon(17301570);
            if (this.mMsgListAdapter.getCount() > 0) {
                Cursor cursor = this.mMsgListAdapter.getCursor();
                if (cursor != null && cursor.getCount() > 0) {
                    menu.add(0, 1, 0, (int) R.string.delete_thread).setIcon(17301564);
                }
            } else {
                menu.add(0, 3, 0, (int) R.string.discard).setIcon(17301564);
            }
            menu.add(0, 35, 0, (int) R.string.menu_music_boards).setIcon(17301587);
            menu.add(0, 6, 0, (int) R.string.all_threads).setIcon((int) R.drawable.ic_menu_friendslist);
            menu.add(0, 19, 0, (int) R.string.menu_search).setIcon(17301577);
        } else {
            menu.add(0, (int) MENU_DELETE_SELECTED, 0, (int) R.string.menu_delete_selected_items).setIcon(17301564);
            menu.add(0, (int) MENU_CANCEL_MULTI_MODE, 0, (int) R.string.menu_cancel_multi_mode).setIcon(17301582);
        }
        return true;
    }

    public void onProtocolChanged(boolean z) {
        toastConvertInfo(z);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (this.mWorkingMessage.isDiscarded()) {
            this.mWorkingMessage.unDiscard();
        }
        this.mConversation.markAsRead();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        addRecipientsListeners();
        this.mToastForDraftSave = true;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("recipients", getRecipients().serialize());
        this.mWorkingMessage.writeStateToBundle(bundle);
        if (this.mExitOnSent) {
            bundle.putBoolean("exit_on_sent", this.mExitOnSent);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mNeedReset || getIntent().getBooleanExtra("need_reset", false)) {
            this.mNeedReset = false;
        }
        updateWindowTitle();
        initFocus();
        loadMessageContent();
        registerReceiver(this.mImageUploadReceiver, new IntentFilter(PrefsUtil.INTENT_IMAGE_UPLOAD));
        registerReceiver(this.mImageForwardReceiver, new IntentFilter(PrefsUtil.INTENT_IMAGE_FORWARD));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mMsgListAdapter != null) {
            this.mMsgListAdapter.changeCursor(null);
        }
        MessageListItem.stopPlayMusic();
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("onStop: save draft");
        }
        if (!this.mWaitingForSubActivity && !this.mWorkingMessage.isWorthSaving()) {
            if (Log.isLoggable(LogTag.APP, 2)) {
                log("saveDraft: not worth saving, discard WorkingMessage and bail");
            }
            try {
                this.mWorkingMessage.discard();
            } catch (Exception e) {
            }
            if (this.mConversation != null) {
                this.mConversation.setDraftState(false);
            }
        }
    }

    public void onUpdate(final Contact contact) {
        this.mMessageListItemHandler.post(new Runnable() {
            public void run() {
                ContactList constructContactsFromInput = ComposeMessageActivity.this.isRecipientsEditorVisible() ? ComposeMessageActivity.this.mRecipientsEditor.constructContactsFromInput() : ComposeMessageActivity.this.getRecipients();
                if (Log.isLoggable(LogTag.APP, 2)) {
                    ComposeMessageActivity.log("[CMA] onUpdate contact updated: " + contact);
                    ComposeMessageActivity.log("[CMA] onUpdate recipients: " + constructContactsFromInput);
                }
                if (constructContactsFromInput.size() == 1) {
                    ComposeMessageActivity.this.updatePresence((Contact) constructContactsFromInput.get(0));
                } else {
                    ComposeMessageActivity.this.updatePresence(null);
                }
                ComposeMessageActivity.this.mMsgListAdapter.notifyDataSetChanged();
                if (ComposeMessageActivity.this.mRecipientsEditor != null) {
                    ComposeMessageActivity.this.mRecipientsEditor.populate(constructContactsFromInput);
                }
            }
        });
    }

    public void onUserInteraction() {
        checkPendingNotification();
    }

    public void onWindowFocusChanged(boolean z) {
        if (z) {
            checkPendingNotification();
        }
    }

    public void preparePopupWindowMenu() {
        this.mPopupWindowMenuData.clear();
        if (this.mConversation.getRecipients().size() == 0) {
            this.menuGrid.setNumColumns(2);
            HashMap hashMap = new HashMap();
            hashMap.put("itemImage", Integer.valueOf((int) R.drawable.bg_menu_back));
            hashMap.put("itemText", getString(R.string.back));
            hashMap.put("id", 6);
            this.mPopupWindowMenuData.add(hashMap);
            HashMap hashMap2 = new HashMap();
            hashMap2.put("itemImage", Integer.valueOf((int) R.drawable.bg_menu_discard));
            hashMap2.put("itemText", getString(R.string.discard_message));
            hashMap2.put("id", 3);
            this.mPopupWindowMenuData.add(hashMap2);
        } else {
            this.menuGrid.setNumColumns(2);
            if (!this.mIsMultiMode) {
                ContactList recipients = getRecipients();
                if (recipients.size() != 1 || !((Contact) recipients.get(0)).existsInDatabase()) {
                    HashMap hashMap3 = new HashMap();
                    hashMap3.put("itemImage", Integer.valueOf((int) R.drawable.bg_menu_addcontact));
                    hashMap3.put("itemText", getString(R.string.menu_add_to_contacts));
                    hashMap3.put("id", 13);
                    this.mPopupWindowMenuData.add(hashMap3);
                } else {
                    HashMap hashMap4 = new HashMap();
                    hashMap4.put("itemImage", Integer.valueOf((int) R.drawable.bg_menu_view_contact));
                    hashMap4.put("itemText", getString(R.string.menu_view_contact));
                    hashMap4.put("id", 12);
                    this.mPopupWindowMenuData.add(hashMap4);
                }
                HashMap hashMap5 = new HashMap();
                hashMap5.put("itemImage", Integer.valueOf((int) R.drawable.bg_menu_multi_mode));
                hashMap5.put("itemText", getString(R.string.menu_clear_record));
                hashMap5.put("id", Integer.valueOf((int) MENU_MULTI_MODE));
                this.mPopupWindowMenuData.add(hashMap5);
            } else {
                return;
            }
        }
        this.mPopupWindowAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void saveToVoteDraft() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.mConversation.getRecipients().size(); i++) {
            sb.append(((Contact) this.mConversation.getRecipients().get(i)).getNumber()).append(",");
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("vote_id", Long.valueOf(this.mVoteItem.cli_id));
        contentValues.put("thread_id", Long.valueOf(this.mConversation.ensureThreadId()));
        String join = TextUtils.join(",", getRecipients().getNumbers());
        if (join.length() == 0) {
            join = TextUtils.join(",", this.mRecipientsEditor.getNumbers());
        }
        contentValues.put("numbers", join);
        if (!this.mTextEditor.getText().toString().equals("")) {
            contentValues.put("sms_content", this.mTextEditor.getText().toString());
        }
        getApplicationContext().getContentResolver().notifyChange(VoteContentProvider.VOTEDRAFT_CONTENT_URI, null);
        if (getApplicationContext().getContentResolver().insert(VoteContentProvider.VOTEDRAFT_CONTENT_URI, contentValues) != null) {
            Toast.makeText(this, (int) R.string.message_saved_as_draft, 0).show();
        }
    }

    public void sendMusicMessage(boolean z, ArrayList<String> arrayList) {
        if (z && Boolean.parseBoolean(SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
            try {
                startActivityForResult(new Intent(TelephonyIntents.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS, (Uri) null), 17);
                return;
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "Cannot find EmergencyCallbackModeExitDialog", e);
            }
        }
        removeRecipientsListeners();
        if (this.mWorkingMessage != null) {
            this.mWorkingMessage.sendMusicSms(arrayList);
        }
        this.mSentMessage = true;
        addRecipientsListeners();
        bindToContactHeader(this.mConversation.getRecipients());
        this.mMsgListAdapter.setMultiRecipient(this.mMultiRecipient);
        this.mMsgListAdapter.notifyDataSetChanged();
        Util.logEvent(PrefsUtil.EVENT_MUSIC_SEND, new NameValuePair[0]);
        if (this.mExitOnSent) {
            finish();
        }
    }

    public void sendSmsWorker(Conversation conversation, String str) {
        long ensureThreadId = conversation.ensureThreadId();
        try {
            new SmsMessageSender(this, conversation.getRecipients().getNumbers(), str, ensureThreadId).sendMessage(ensureThreadId);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(this, ensureThreadId);
        } catch (Exception e) {
            Log.e(Telephony.ThreadsColumns.ERROR, "Failed to send SMS message, threadId=" + ensureThreadId, e);
        }
    }

    public void sendSmsWorker(Conversation conversation, ArrayList<String> arrayList) {
        long ensureThreadId = conversation.ensureThreadId();
        try {
            new SmsMessageSender(this, conversation.getRecipients().getNumbers(), ensureThreadId).sendMessage(ensureThreadId, arrayList);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(this, ensureThreadId);
        } catch (Exception e) {
            Log.e(Telephony.ThreadsColumns.ERROR, "Failed to send SMS message, threadId=" + ensureThreadId, e);
        }
    }

    public void sendVoteMessage(boolean z, ArrayList<String> arrayList) {
        if (z && Boolean.parseBoolean(SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE))) {
            try {
                startActivityForResult(new Intent(TelephonyIntents.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS, (Uri) null), 17);
                return;
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "Cannot find EmergencyCallbackModeExitDialog", e);
            }
        }
        removeRecipientsListeners();
        if (this.mWorkingMessage != null) {
            this.mWorkingMessage.sendMusicSms(arrayList);
        }
        Util.logEvent("vote_send", new NameValuePair[0]);
        if (this.isForwardVote) {
            Util.logEvent("public_vote_forward", new NameValuePair[0]);
        }
        this.mSentMessage = true;
        addRecipientsListeners();
        bindToContactHeader(this.mConversation.getRecipients());
        this.mMsgListAdapter.setMultiRecipient(this.mMultiRecipient);
        this.mMsgListAdapter.notifyDataSetChanged();
        if (this.mExitOnSent) {
            finish();
        }
    }

    public void startActivityForResult(Intent intent, int i) {
        if (i >= 0) {
            this.mWaitingForSubActivity = true;
        }
        try {
            super.startActivityForResult(intent, i);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void undeliveredMessageDialog(long j) {
        String string;
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.retry_sending_dialog, (ViewGroup) null);
        if (j >= 0) {
            string = getString(R.string.undelivered_msg_dialog_body, new Object[]{MessageUtils.formatTimeStampString(this, j)});
        } else {
            string = getString(R.string.undelivered_sms_dialog_body);
        }
        ((TextView) linearLayout.findViewById(R.id.body_text_view)).setText(string);
        Toast toast = new Toast(this);
        toast.setView(linearLayout);
        toast.setDuration(1);
        toast.show();
    }
}
