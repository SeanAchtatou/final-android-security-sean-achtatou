package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.ui.AbstractContactsListActivity;
import vc.lx.sms.util.SmsConstants;
import vc.lx.sms2.R;

public class ContactsFavoriteActivity extends AbstractContactsListActivity {
    static final String[] CONTACTS_SUMMARY_PROJECTION = {"_id", "display_name", "starred", "times_contacted", "contact_presence", "photo_id", "lookup", "has_phone_number"};
    private static final int QUERY_TOKEN = 42;
    private static final int SUMMARY_ID_COLUMN_INDEX = 0;
    protected static List<String> cacheCheckNums = new ArrayList();
    protected static List<String> cacheSecondNumList = new ArrayList();
    protected List<String> cache = new ArrayList();
    protected List<AbstractContactsListActivity.Contact> data = new ArrayList();
    /* access modifiers changed from: private */
    public ContactsListAdapter mContactsListAdapter;
    private ContactsListQueryHandler mContactsListQueryHandler;

    class ContactView {
        public CheckBox mCheckBox = null;
        public TextView mNameView = null;
        public TextView mNumView = null;
        public TextView mTypeView = null;

        ContactView() {
        }
    }

    class ContactsListAdapter extends BaseAdapter {
        private List<AbstractContactsListActivity.Contact> data = new ArrayList();

        public ContactsListAdapter(Context context, List<AbstractContactsListActivity.Contact> list) {
            this.data = list;
        }

        public void changeData(List<AbstractContactsListActivity.Contact> list) {
            this.data = list;
            notifyDataSetChanged();
        }

        public int getCount() {
            if (this.data == null || this.data.size() == 0) {
                return 0;
            }
            return this.data.size();
        }

        public Object getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ContactView contactView;
            View view2;
            View inflate;
            final AbstractContactsListActivity.Contact contact = this.data.get(i);
            if (view == null) {
                ContactView contactView2 = new ContactView();
                if (!ContactsFavoriteActivity.this.isVisitedFromCallingCard) {
                    inflate = ContactsFavoriteActivity.this.mInflater.inflate((int) R.layout.contact_item, (ViewGroup) null);
                    contactView2.mNumView = (TextView) inflate.findViewById(R.id.address);
                    contactView2.mTypeView = (TextView) inflate.findViewById(R.id.type);
                } else {
                    inflate = ContactsFavoriteActivity.this.mInflater.inflate((int) R.layout.contact_item_calling_card, (ViewGroup) null);
                }
                contactView2.mCheckBox = (CheckBox) inflate.findViewById(16908289);
                contactView2.mNameView = (TextView) inflate.findViewById(R.id.contact_name);
                inflate.setTag(contactView2);
                contactView = contactView2;
                view2 = inflate;
            } else {
                contactView = (ContactView) view.getTag();
                view2 = view;
            }
            contactView.mNameView.setText(contact.name);
            if (!ContactsFavoriteActivity.this.isVisitedFromCallingCard) {
                contactView.mNumView.setText(contact.number);
                contactView.mTypeView.setText(AbstractContactsListActivity.getDisplayNameForPhoneType(ContactsFavoriteActivity.this.getApplicationContext(), contact.type));
            }
            contactView.mCheckBox.setOnCheckedChangeListener(null);
            if (contact.isCheck) {
                contactView.mCheckBox.setChecked(true);
            } else {
                contactView.mCheckBox.setChecked(false);
            }
            if (ContactsFavoriteActivity.this.isSelectAll) {
                contactView.mCheckBox.setChecked(true);
            }
            if (!ContactsFavoriteActivity.this.isVisitedFromCallingCard) {
                if (ContactsSelectActivity.mSelectedNumberOfContacts.contains(contact.number)) {
                    contactView.mCheckBox.setChecked(true);
                }
            } else if (AbstractContactsListActivity.cacheCheckNames.contains(contact.name)) {
                contactView.mCheckBox.setChecked(true);
            } else {
                contactView.mCheckBox.setChecked(false);
            }
            contactView.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (!ContactsFavoriteActivity.this.isVisitedFromCallingCard) {
                        if (z) {
                            if (!ContactsSelectActivity.mSelectedNumberOfContacts.contains(contact.number)) {
                                ContactsFavoriteActivity.cacheCheckNums.add(contact.number);
                                ContactsSelectActivity.mSelectedNumberOfContacts.add(contact.number);
                            }
                            contact.isCheck = true;
                        } else {
                            ContactsFavoriteActivity.cacheCheckNums.remove(contact.number);
                            ContactsSelectActivity.mSelectedNumberOfContacts.remove(contact.number);
                            contact.isCheck = false;
                        }
                        ContactsFavoriteActivity.this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
                    } else {
                        if (z) {
                            AbstractContactsListActivity.cacheCheckNames.add(contact.name);
                            contact.isCheck = true;
                        } else {
                            AbstractContactsListActivity.cacheCheckNames.remove(contact.name);
                            contact.isCheck = false;
                        }
                        ContactsFavoriteActivity.this.mCountView.setText("(" + AbstractContactsListActivity.cacheCheckNames.size() + ")");
                    }
                    ContactsListAdapter.this.notifyDataSetChanged();
                }
            });
            return view2;
        }
    }

    private class ContactsListQueryHandler extends AsyncQueryHandler {
        public ContactsListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case 1:
                    ContactsSelectActivity.mSelectedNumberOfContacts.clear();
                    while (cursor.moveToNext()) {
                        ContactsSelectActivity.mSelectedNumberOfContacts.add(cursor.getString(1));
                    }
                    ContactsFavoriteActivity.this.setProgressBarIndeterminateVisibility(false);
                    return;
                case ContactsFavoriteActivity.QUERY_TOKEN /*42*/:
                    ContactsFavoriteActivity.this.fillData(cursor);
                    ContactsFavoriteActivity.this.setProgressBarIndeterminateVisibility(false);
                    ContactsFavoriteActivity.this.mContactsListAdapter.changeData(ContactsFavoriteActivity.this.data);
                    ContactsFavoriteActivity.this.mContactsListAdapter.notifyDataSetChanged();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void fillData(Cursor cursor) {
        AbstractContactsListActivity.Contact contact;
        Cursor cursor2;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                AbstractContactsListActivity.Contact contact2 = new AbstractContactsListActivity.Contact();
                contact2.name = cursor.getString(cursor.getColumnIndex("display_name"));
                if (!this.isVisitedFromCallingCard) {
                    contact2.type = cursor.getInt(2);
                    if (cursor.getInt(cursor.getColumnIndex("has_phone_number")) != 0) {
                        Cursor queryPhoneNumbers = queryPhoneNumbers(cursor.getLong(0));
                        if (queryPhoneNumbers != null) {
                            if (queryPhoneNumbers.getCount() == 2) {
                                String str = contact2.name;
                                contact2.number = queryPhoneNumbers.getString(queryPhoneNumbers.getColumnIndex("data1"));
                                this.data.add(contact2);
                                queryPhoneNumbers.moveToNext();
                                AbstractContactsListActivity.Contact contact3 = new AbstractContactsListActivity.Contact();
                                contact3.number = queryPhoneNumbers.getString(queryPhoneNumbers.getColumnIndex("data1"));
                                contact3.name = str;
                                this.data.add(contact3);
                                Cursor cursor3 = queryPhoneNumbers;
                                contact = contact3;
                                cursor2 = cursor3;
                            } else {
                                contact2.number = queryPhoneNumbers.getString(queryPhoneNumbers.getColumnIndex("data1"));
                            }
                        }
                        Cursor cursor4 = queryPhoneNumbers;
                        contact = contact2;
                        cursor2 = cursor4;
                    } else {
                        contact = contact2;
                        cursor2 = null;
                    }
                    if (!(cursor2 == null || cursor2.getCount() == 2)) {
                        this.data.add(contact);
                        cursor2.close();
                    }
                } else if (contact2.name.length() > 0) {
                    this.data.add(contact2);
                }
            }
            cursor.close();
        }
        this.mReady = false;
    }

    public void MenuCacelModel() {
        this.isSelectAll = false;
        this.mContactsListAdapter.notifyDataSetChanged();
    }

    public void MenuMulitModel() {
        this.isSelectAll = true;
        this.mContactsListAdapter.notifyDataSetChanged();
    }

    public List<String> getCache() {
        return this.cache;
    }

    public void initDatas(Cursor cursor) {
    }

    public void initViews() {
        this.cache.clear();
        if (getIntent() != null) {
            this.isVisitedFromCallingCard = getIntent().getBooleanExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, false);
        }
        this.mContactsListQueryHandler = new ContactsListQueryHandler(getContentResolver());
        if (!this.isVisitedFromCallingCard) {
            this.mContactsListQueryHandler.startQuery(QUERY_TOKEN, null, ContactsContract.Contacts.CONTENT_STREQUENT_URI, CONTACTS_SUMMARY_PROJECTION, "has_phone_number!= 0 ", null, null);
        } else {
            this.mContactsListQueryHandler.startQuery(QUERY_TOKEN, null, ContactsContract.Contacts.CONTENT_STREQUENT_URI, null, null, null, null);
        }
        this.mContactsListAdapter = new ContactsListAdapter(this, this.data);
        setListAdapter(this.mContactsListAdapter);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok /*2131492968*/:
            case R.id.btn_send_invitation /*2131492972*/:
                cacheSecondNumList.clear();
                if (this.isVisitedFromCallingCard) {
                    fillData();
                }
                finish();
                return;
            case R.id.selCount /*2131492969*/:
            case R.id.invitation_action_bar /*2131492971*/:
            default:
                return;
            case R.id.back /*2131492970*/:
                ContactsSelectActivity.mSelectedNumberOfContacts.clear();
                finish();
                return;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
        if (this.cache != null && this.cache.size() > 0) {
            this.mReady = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        cacheCheckNums.clear();
        super.onStop();
    }
}
