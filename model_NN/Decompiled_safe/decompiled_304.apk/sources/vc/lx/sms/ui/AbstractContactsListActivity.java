package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import vc.lx.sms.data.CallingCard;
import vc.lx.sms.ui.widget.PopupGridMenusWindow;
import vc.lx.sms.ui.widget.PopupMenuItem;
import vc.lx.sms.ui.widget.PopupMenusWindow;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public abstract class AbstractContactsListActivity extends AbstractFlurryListActivity implements AbsListView.OnScrollListener, View.OnClickListener {
    protected static String[] CALLING_CARD_PROJECTION = {"_id", "name", "number"};
    protected static final int COLUMN_DISPLAY_NAME = 3;
    protected static final int COLUMN_ID = 0;
    protected static final int COLUMN_NUMBER = 1;
    protected static final int COLUMN_TYPE = 2;
    protected static final int CONTACTS_GROUP_TOKEN = 2;
    protected static final int CONTACTS_QUERY_TOKEN = 0;
    protected static final int CONTACTS_SELECT_ALL_TOKEN = 1;
    protected static final String[] MCALLPROJECTIONSTRINGS = {"_id", "number", "numbertype", "name"};
    private static final int MENU_CANCEL_MODE = 2;
    private static final int MENU_MULTI_MODE = 1;
    protected static final String[] MGROUPCOUNTSTRINGS = {"_id", "summ_count", "title", "system_id", "_id"};
    protected static final String[] MGROUPSTRINGS = {"_id", "title", "system_id", "sourceid", "summ_count"};
    protected static final String[] MPROJECTIONSTRINGS = {"_id", "data1", "data2", "display_name", "starred"};
    protected static List<String> cacheCheckNames = new ArrayList();
    protected static HashMap<String, String> cacheNum = new HashMap<>();
    protected boolean isClearAll;
    protected boolean isScrool = false;
    protected boolean isSelectAll = false;
    protected boolean isVisitedFromCallingCard = false;
    protected Button mBackBtn;
    /* access modifiers changed from: private */
    public ContactsListGroupQueryHandler mContactsListGroupQueryHandler;
    protected TextView mCountView;
    protected TextView mDialogText;
    Handler mHandler = new Handler();
    protected LayoutInflater mInflater;
    protected Button mInvitationSend;
    protected Button mOkBtn;
    private PopupMenusWindow.OnMenuItemClickListener mOnMenuItemClickListener = new PopupMenusWindow.OnMenuItemClickListener() {
        public void onMenuItemClicked(PopupMenusWindow popupMenusWindow, int i) {
            PopupMenuItem menuItem = AbstractContactsListActivity.this.mPopupMenuWindow.getMenuItem(i);
            if (menuItem != null) {
                switch (menuItem.mIndex) {
                    case 1:
                        AbstractContactsListActivity.this.MenuMulitModel();
                        return;
                    case 2:
                        AbstractContactsListActivity.this.MenuCacelModel();
                        return;
                    default:
                        return;
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public PopupGridMenusWindow mPopupMenuWindow;
    protected String mPrevLetter = "";
    protected boolean mReady;
    protected RemoveWindow mRemoveWindow = new RemoveWindow();
    protected boolean mSelectForInvitation;
    protected boolean mShowing;
    protected WindowManager mWindowManager;

    class Contact {
        public boolean isCheck;
        public String name;
        public String number;
        public int type;

        Contact() {
        }
    }

    private class ContactsListGroupQueryHandler extends AsyncQueryHandler {
        public ContactsListGroupQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case 0:
                    AbstractContactsListActivity.this.initDatas(cursor);
                    ContactsListGroupQueryHandler unused = AbstractContactsListActivity.this.mContactsListGroupQueryHandler = null;
                    return;
                default:
                    return;
            }
        }
    }

    private final class RemoveWindow implements Runnable {
        private RemoveWindow() {
        }

        public void run() {
            AbstractContactsListActivity.this.removeWindow();
        }
    }

    public static String getDisplayNameForPhoneType(Context context, int i) {
        switch (i) {
            case 0:
            case 7:
                return context.getString(R.string.type_other);
            case 1:
                return context.getString(R.string.type_home);
            case 2:
                return context.getString(R.string.type_mobile);
            case 3:
            case 17:
                return context.getString(R.string.type_work);
            case 4:
            case 5:
                return context.getString(R.string.type_fax);
            case 6:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            default:
                return "";
        }
    }

    public abstract void MenuCacelModel();

    public abstract void MenuMulitModel();

    public void fillData() {
        String str;
        String str2;
        Cursor query = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (query.moveToFirst()) {
            while (query.moveToNext()) {
                CallingCard callingCard = new CallingCard();
                int columnIndex = query.getColumnIndex("display_name");
                if (cacheCheckNames.contains(query.getString(columnIndex))) {
                    callingCard.displayName = query.getString(columnIndex) + "\n";
                    Cursor query2 = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + query.getString(query.getColumnIndex("_id")), null, null);
                    while (query2.moveToNext()) {
                        int columnIndex2 = query2.getColumnIndex("data1");
                        int i = query2.getInt(query2.getColumnIndex("data2"));
                        String string = query2.getString(columnIndex2);
                        if (string.length() != 0) {
                            switch (i) {
                                case 0:
                                    str2 = getString(R.string.telephone) + string + "\n";
                                    break;
                                case 1:
                                    str2 = getString(R.string.home_telephone) + string + "\n";
                                    break;
                                case 2:
                                    str2 = getString(R.string.mobile_telephone) + string + "\n";
                                    break;
                                case 3:
                                    str2 = getString(R.string.work_telephone) + string + "\n";
                                    break;
                                case 4:
                                    str2 = getString(R.string.work_fax) + string + "\n";
                                    break;
                                case 5:
                                    str2 = getString(R.string.home_fax) + string + "\n";
                                    break;
                                default:
                                    str2 = getString(R.string.telephone) + string + "\n";
                                    break;
                            }
                            callingCard.phoneNumber += str2;
                        }
                    }
                    if (!query2.isClosed()) {
                        query2.close();
                    }
                    String string2 = query.getString(query.getColumnIndex("_id"));
                    Cursor query3 = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, "contact_id = " + string2, null, null);
                    if (query3.moveToFirst()) {
                        do {
                            int columnIndex3 = query3.getColumnIndex("data1");
                            int i2 = query3.getInt(query3.getColumnIndex("data2"));
                            String string3 = query3.getString(columnIndex3);
                            switch (i2) {
                                case 1:
                                    str = getString(R.string.home_mail) + string3 + "\n";
                                    break;
                                case 2:
                                    str = getString(R.string.work_mail) + string3 + "\n";
                                    break;
                                case 3:
                                    str = getString(R.string.other_mail) + string3 + "\n";
                                    break;
                                case 4:
                                    str = getString(R.string.mobile_mail) + string3 + "\n";
                                    break;
                                default:
                                    str = getString(R.string.email) + string3 + "\n";
                                    break;
                            }
                            callingCard.mailbox += str;
                        } while (query3.moveToNext());
                        if (!query3.isClosed()) {
                            query3.close();
                        }
                    }
                    Cursor query4 = getContentResolver().query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null, "contact_id = " + string2, null, null);
                    if (query4.moveToFirst()) {
                        do {
                            String string4 = query4.getString(query4.getColumnIndex("data10"));
                            String string5 = query4.getString(query4.getColumnIndex("data7"));
                            String string6 = query4.getString(query4.getColumnIndex("data4"));
                            if (!(string4 == null && string5 == null && string6 == null)) {
                                callingCard.address += getString(R.string.address);
                                if (string4 != null) {
                                    callingCard.address += getString(R.string.country) + string4 + " ";
                                }
                                if (string5 != null) {
                                    callingCard.address += getString(R.string.city) + string5 + " ";
                                }
                                if (string6 != null) {
                                    callingCard.address += getString(R.string.street) + string6 + "\n";
                                }
                            }
                        } while (query4.moveToNext());
                    }
                    query4.close();
                    Cursor query5 = getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data1", "data4"}, "contact_id=" + string2 + " AND " + "mimetype" + "='" + "vnd.android.cursor.item/organization" + "'", null, null);
                    if (query5.moveToFirst()) {
                        do {
                            String string7 = query5.getString(query5.getColumnIndex("data1"));
                            String string8 = query5.getString(query5.getColumnIndex("data4"));
                            if (string7 != null) {
                                callingCard.company += getString(R.string.company) + string7 + "\n";
                            }
                            if (string8 != null) {
                                callingCard.company += getString(R.string.title) + string8 + "\n";
                            }
                        } while (query5.moveToNext());
                    }
                    query5.close();
                    ContactsSelectActivity.mCallingCards.add(callingCard);
                }
            }
        }
        cacheCheckNames.clear();
    }

    public abstract List<String> getCache();

    public abstract void initDatas(Cursor cursor);

    public abstract void initViews();

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok /*2131492968*/:
            case R.id.back /*2131492970*/:
            case R.id.btn_send_invitation /*2131492972*/:
                finish();
                return;
            case R.id.selCount /*2131492969*/:
            case R.id.invitation_action_bar /*2131492971*/:
            default:
                return;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(5);
        setContentView((int) R.layout.contacts_list);
        this.mInflater = LayoutInflater.from(this);
        setProgressBarIndeterminateVisibility(true);
        getListView().setOnScrollListener(this);
        this.mWindowManager = (WindowManager) getSystemService("window");
        this.mDialogText = (TextView) ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.list_position, (ViewGroup) null);
        this.mDialogText.setVisibility(4);
        this.mHandler.post(new Runnable() {
            public void run() {
                AbstractContactsListActivity.this.mWindowManager.addView(AbstractContactsListActivity.this.mDialogText, new WindowManager.LayoutParams(-2, -2, 2, 24, -3));
            }
        });
        this.mOkBtn = (Button) findViewById(R.id.ok);
        this.mBackBtn = (Button) findViewById(R.id.back);
        this.mCountView = (TextView) findViewById(R.id.selCount);
        this.mInvitationSend = (Button) findViewById(R.id.btn_send_invitation);
        this.mOkBtn.setOnClickListener(this);
        this.mBackBtn.setOnClickListener(this);
        this.mInvitationSend.setOnClickListener(this);
        this.mPopupMenuWindow = new PopupGridMenusWindow(getApplicationContext());
        this.mPopupMenuWindow.setOnMenuItemClickListener(this.mOnMenuItemClickListener);
        if (getIntent() != null) {
            this.mSelectForInvitation = getIntent().getBooleanExtra(PrefsUtil.KEY_INVITE_BY_SMS, false);
            if (this.mSelectForInvitation) {
                findViewById(R.id.selection_action_bar).setVisibility(8);
                findViewById(R.id.invitation_action_bar).setVisibility(0);
            }
        }
        initViews();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.mPopupMenuWindow.clearAllPopupMenuItems();
        this.mPopupMenuWindow.addPopupMenuItem(new PopupMenuItem(this, (int) R.drawable.bg_menu_multi_mode, (int) R.string.menu_multi_sel_mode, 1));
        this.mPopupMenuWindow.addPopupMenuItem(new PopupMenuItem(this, (int) R.drawable.bg_menu_cancel_multi_mode, (int) R.string.menu_cancel_multi_mode, 2));
        this.mPopupMenuWindow.show(findViewById(R.id.main));
        this.mPopupMenuWindow.setNumColums(2);
        this.mPopupMenuWindow.afterShow();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mWindowManager.removeView(this.mDialogText);
        this.mReady = false;
        this.isScrool = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                ContactsSelectActivity.mSelectedNumberOfContacts.clear();
                if (this.mSelectForInvitation && getParent() != null) {
                    Intent intent = new Intent();
                    intent.putExtra("no_selection", true);
                    getParent().setIntent(intent);
                    break;
                }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        removeWindow();
        this.mReady = false;
        this.isScrool = false;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        int i4 = (i + i2) - 1;
        if (this.mReady && this.isScrool) {
            String str = getCache().get((i2 == 0 ? 5 : i2 / 2) + i);
            String substring = (str != null || !"".equals(str)) ? str.substring(0, 1) : this.mPrevLetter;
            if (!this.mShowing && substring != this.mPrevLetter) {
                this.mShowing = true;
                this.mDialogText.setVisibility(0);
            }
            this.mDialogText.setText(substring);
            this.mHandler.removeCallbacks(this.mRemoveWindow);
            this.mHandler.postDelayed(this.mRemoveWindow, 1000);
            this.mPrevLetter = substring;
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.isScrool = true;
    }

    /* access modifiers changed from: protected */
    public Cursor queryPhoneNumbers(long j) {
        Uri withAppendedPath = Uri.withAppendedPath(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, j), "data");
        Cursor query = getContentResolver().query(withAppendedPath, new String[]{"_id", "data1", "is_super_primary"}, "mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, null);
        if (query == null || !query.moveToFirst()) {
            return null;
        }
        return query;
    }

    public void removeWindow() {
        if (this.mShowing) {
            this.mShowing = false;
            this.mDialogText.setVisibility(4);
        }
    }

    public void startQuery() {
        if (this.mContactsListGroupQueryHandler != null) {
            this.mContactsListGroupQueryHandler.cancelOperation(0);
            this.mContactsListGroupQueryHandler = null;
        }
        this.mContactsListGroupQueryHandler = new ContactsListGroupQueryHandler(getContentResolver());
        this.mContactsListGroupQueryHandler.startQuery(0, null, ContactsContract.Groups.CONTENT_SUMMARY_URI, MGROUPSTRINGS, null, null, null);
    }
}
