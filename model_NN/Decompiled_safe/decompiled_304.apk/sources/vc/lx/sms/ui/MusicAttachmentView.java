package vc.lx.sms.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.mms.data.WorkingMessage;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public class MusicAttachmentView extends LinearLayout {
    public static final int MSG_PLAY_MUSIC = 12;
    public static final int MSG_REMOVE_MUSIC = 11;
    Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private TextView mMusicName;
    private ProgressBar mProgressBar;
    /* access modifiers changed from: private */
    public SongItem mSongItem;

    public MusicAttachmentView(Context context) {
        super(context);
        this.mContext = context;
    }

    public MusicAttachmentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
    }

    private void updateSongItem(SongItem songItem) {
        this.mSongItem = songItem;
        if (songItem != null) {
            setVisibility(0);
            this.mMusicName.setText(this.mSongItem.song + " - " + this.mSongItem.singer);
            return;
        }
        setVisibility(8);
    }

    public SongItem getSongItem() {
        return this.mSongItem;
    }

    public void hideView() {
        setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.mMusicName = (TextView) findViewById(R.id.music_name);
        this.mProgressBar = (ProgressBar) findViewById(R.id.play_progress);
    }

    public void setHandler(Handler handler) {
        this.mHandler = handler;
    }

    public void update(WorkingMessage workingMessage) {
        if (workingMessage != null) {
            updateSongItem(workingMessage.getSongItem());
            if (this.mSongItem != null) {
                ((ImageView) findViewById(R.id.remove_music_button)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Message.obtain(MusicAttachmentView.this.mHandler, 11).sendToTarget();
                    }
                });
                ((ImageView) findViewById(R.id.instead_music_button)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Intent intent = new Intent(MusicAttachmentView.this.mContext, MusicPlatformActivity.class);
                        intent.putExtra(PrefsUtil.PREF_BOARD_FOCUS_PLUG, MusicAttachmentView.this.mSongItem.plug);
                        MusicAttachmentView.this.mContext.startActivity(intent);
                    }
                });
            }
        }
    }

    public void updatePlayState() {
        this.mProgressBar.setVisibility(4);
    }
}
