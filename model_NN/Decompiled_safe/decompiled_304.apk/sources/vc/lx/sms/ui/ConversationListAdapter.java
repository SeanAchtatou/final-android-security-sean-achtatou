package vc.lx.sms.ui;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CursorAdapter;
import java.util.HashSet;
import vc.lx.sms.data.Conversation;
import vc.lx.sms2.R;

public class ConversationListAdapter extends CursorAdapter implements AbsListView.RecyclerListener {
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "ConversationListAdapter";
    private final LayoutInflater mFactory;
    private boolean mIsMultiMode = false;
    private OnContentChangedListener mOnContentChangedListener;
    public HashSet<Long> mSelectedIds = new HashSet<>();

    public interface OnContentChangedListener {
        void onContentChanged(ConversationListAdapter conversationListAdapter);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.content.Context, android.database.Cursor, int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    public ConversationListAdapter(Context context, Cursor cursor) {
        super(context, cursor, false);
        this.mFactory = LayoutInflater.from(context);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        if (!(view instanceof ConversationHeaderView)) {
            Log.e(TAG, "Unexpected bound view: " + view);
        } else {
            ((ConversationHeaderView) view).bind(context, new ConversationHeader(context, Conversation.from(context, cursor)), this.mIsMultiMode, this.mSelectedIds);
        }
    }

    public void clearSelectedIds() {
        this.mSelectedIds.clear();
    }

    public HashSet<Long> getSelectedIds() {
        return this.mSelectedIds;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.mFactory.inflate((int) R.layout.conversation_header, viewGroup, false);
    }

    /* access modifiers changed from: protected */
    public void onContentChanged() {
        if (getCursor() != null && !getCursor().isClosed() && this.mOnContentChangedListener != null) {
            this.mOnContentChangedListener.onContentChanged(this);
        }
    }

    public void onMovedToScrapHeap(View view) {
        ((ConversationHeaderView) view).unbind();
    }

    public void setMultiMode(boolean z) {
        this.mIsMultiMode = z;
    }

    public void setOnContentChangedListener(OnContentChangedListener onContentChangedListener) {
        this.mOnContentChangedListener = onContentChangedListener;
    }
}
