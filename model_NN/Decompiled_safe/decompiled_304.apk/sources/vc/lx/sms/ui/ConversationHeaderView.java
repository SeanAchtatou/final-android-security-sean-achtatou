package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.mms.ui.MessageListAdapter;
import com.android.provider.Telephony;
import java.util.HashSet;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.ui.widget.ImageTextButton;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ConversationHeaderView extends RelativeLayout implements Contact.UpdateListener {
    private static final boolean DEBUG = false;
    private static final int MESSAGE_LIST_QUERY_TOKEN = 9527;
    private static final StyleSpan STYLE_BOLD = new StyleSpan(1);
    private static final String TAG = "ConversationHeaderView";
    private static Drawable sDefaultContactImage;
    private View mAttachmentView;
    private QuickContactBadge mAvatarView;
    private BackgroundQueryHandler mBackgroundQueryHandler;
    private CheckBox mCheckBox;
    private ConversationHeader mConversationHeader;
    private TextView mDateView;
    private View mErrorIndicator;
    private TextView mFromView;
    private Handler mHandler = new Handler();
    private ImageView mPresenceView;
    /* access modifiers changed from: private */
    public ImageTextButton mSmsReadView;
    private TextView mStateView;
    private TextView mSubjectView;

    class BackgroundQueryHandler extends AsyncQueryHandler {
        public BackgroundQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case ConversationHeaderView.MESSAGE_LIST_QUERY_TOKEN /*9527*/:
                    int i2 = 0;
                    while (cursor.moveToNext()) {
                        if (cursor.getInt(cursor.getColumnIndex("read")) == 0) {
                            i2++;
                        }
                    }
                    if (SmsApplication.getInstance().dm.heightPixels == 480) {
                        ConversationHeaderView.this.mSmsReadView.setTextSize(10);
                    }
                    ConversationHeaderView.this.mSmsReadView.setCount(String.valueOf(i2));
                    ConversationHeaderView.this.mSmsReadView.setVisibility(0);
                    ConversationHeaderView.this.mSmsReadView.setClickable(true);
                    ConversationHeaderView.this.mSmsReadView.setFocusable(false);
                    return;
                default:
                    return;
            }
        }
    }

    public ConversationHeaderView(Context context) {
        super(context);
    }

    public ConversationHeaderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (sDefaultContactImage == null) {
            sDefaultContactImage = context.getResources().getDrawable(R.drawable.ic_contact_picture_s);
        }
    }

    private CharSequence formatMessage(ConversationHeader conversationHeader) {
        String from = conversationHeader.getFrom();
        if (from.length() > 15) {
            from = from.substring(0, 12) + "...";
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(from);
        if (conversationHeader.getMessageCount() > 1) {
            spannableStringBuilder.append((CharSequence) (" (" + conversationHeader.getMessageCount() + ") "));
        }
        int length = spannableStringBuilder.length();
        if (conversationHeader.hasDraft()) {
            spannableStringBuilder.append((CharSequence) " ");
            spannableStringBuilder.append((CharSequence) getContext().getResources().getString(R.string.has_draft));
            spannableStringBuilder.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.drawable.text_color_red)), length, spannableStringBuilder.length(), 17);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.drawable.text_color_red)), length, spannableStringBuilder.length(), 17);
        }
        if (!conversationHeader.isRead()) {
            spannableStringBuilder.setSpan(STYLE_BOLD, 0, spannableStringBuilder.length(), 17);
        }
        return spannableStringBuilder;
    }

    private void setConversationHeader(ConversationHeader conversationHeader) {
        this.mConversationHeader = conversationHeader;
    }

    private void updateAvatarView() {
        Drawable drawable;
        ConversationHeader conversationHeader = this.mConversationHeader;
        if (conversationHeader.getContacts().size() == 1) {
            Contact contact = (Contact) conversationHeader.getContacts().get(0);
            Drawable avatar = contact.getAvatar(sDefaultContactImage);
            if (contact.existsInDatabase()) {
                this.mAvatarView.assignContactUri(contact.getUri());
            } else {
                this.mAvatarView.assignContactFromPhone(contact.getNumber(), true);
            }
            drawable = avatar;
        } else {
            drawable = sDefaultContactImage;
            this.mAvatarView.assignContactUri(null);
        }
        this.mAvatarView.setImageDrawable(drawable);
        this.mAvatarView.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void updateFromView() {
        ConversationHeader conversationHeader = this.mConversationHeader;
        conversationHeader.updateRecipients();
        this.mFromView.setText(formatMessage(conversationHeader));
        setPresenceIcon(conversationHeader.getContacts().getPresenceResId());
    }

    public final void bind(Context context, final ConversationHeader conversationHeader, boolean z, final HashSet<Long> hashSet) {
        setConversationHeader(conversationHeader);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.mAttachmentView.getLayoutParams();
        boolean hasError = conversationHeader.hasError();
        if (hasError) {
            layoutParams.addRule(0, R.id.error);
        } else {
            layoutParams.addRule(0, R.id.date);
        }
        boolean hasAttachment = conversationHeader.hasAttachment();
        this.mAttachmentView.setVisibility(hasAttachment ? 0 : 8);
        this.mDateView.setText(" " + conversationHeader.getDate().replaceAll(" ", ""));
        this.mSmsReadView.setVisibility(8);
        if (!conversationHeader.isRead()) {
            Uri withAppendedId = ContentUris.withAppendedId(Telephony.Threads.CONTENT_URI, conversationHeader.getThreadId());
            this.mBackgroundQueryHandler = new BackgroundQueryHandler(getContext().getContentResolver());
            this.mBackgroundQueryHandler.cancelOperation(MESSAGE_LIST_QUERY_TOKEN);
            try {
                this.mBackgroundQueryHandler.startQuery(MESSAGE_LIST_QUERY_TOKEN, null, withAppendedId, MessageListAdapter.PROJECTION, null, null, null);
            } catch (Exception e) {
            }
        }
        this.mFromView.setText(formatMessage(conversationHeader));
        ContactList contacts = conversationHeader.getContacts();
        contacts.addListeners(this);
        setPresenceIcon(contacts.getPresenceResId());
        this.mSubjectView.setText(conversationHeader.getSubject());
        ((RelativeLayout.LayoutParams) this.mSubjectView.getLayoutParams()).addRule(0, hasAttachment ? R.id.attachment : hasError ? R.id.error : R.id.date);
        this.mErrorIndicator.setVisibility(hasError ? 0 : 8);
        this.mCheckBox.setVisibility(z ? 0 : 8);
        this.mDateView.setVisibility(z ? 4 : 0);
        this.mCheckBox.setGravity(17);
        this.mCheckBox.setChecked(hashSet.contains(Long.valueOf(conversationHeader.getThreadId())));
        this.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (z) {
                    hashSet.add(Long.valueOf(conversationHeader.getThreadId()));
                } else {
                    hashSet.remove(Long.valueOf(conversationHeader.getThreadId()));
                }
            }
        });
    }

    public void bind(String str, String str2) {
        this.mFromView.setText(str);
        this.mSubjectView.setText(str2);
    }

    public ConversationHeader getConversationHeader() {
        return this.mConversationHeader;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.mFromView = (TextView) findViewById(R.id.from);
        this.mSubjectView = (TextView) findViewById(R.id.subject);
        this.mDateView = (TextView) findViewById(R.id.date);
        this.mSmsReadView = (ImageTextButton) findViewById(R.id.sms_state);
        this.mAttachmentView = findViewById(R.id.attachment);
        this.mErrorIndicator = findViewById(R.id.error);
        this.mPresenceView = (ImageView) findViewById(R.id.presence);
        this.mCheckBox = (CheckBox) findViewById(R.id.checkbox);
    }

    public void onUpdate(Contact contact) {
        this.mHandler.post(new Runnable() {
            public void run() {
                ConversationHeaderView.this.updateFromView();
            }
        });
    }

    public void setPresenceIcon(int i) {
        if (i == 0) {
            this.mPresenceView.setVisibility(8);
            return;
        }
        this.mPresenceView.setImageResource(i);
        this.mPresenceView.setVisibility(0);
    }

    public final void unbind() {
        this.mConversationHeader.getContacts().removeListeners(this);
    }
}
