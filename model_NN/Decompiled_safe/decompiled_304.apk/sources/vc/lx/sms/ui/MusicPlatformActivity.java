package vc.lx.sms.ui;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.ContentList;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.RichTemplateRemoteVersion;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.parser.ContentListParser;
import vc.lx.sms.cmcc.http.parser.RichTemplateRemoteVersionParser;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.service.MiguPlayerService;
import vc.lx.sms.service.MiguPlayerServiceInterface;
import vc.lx.sms.util.LogTool;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class MusicPlatformActivity extends AbstractFlurryActivity {
    protected static final int MENU_DOWNLOAD_SONG = 2;
    protected static final int MENU_PLAY_SONG = 1;
    protected static final int MENU_PRESENT_RINGTONE = 5;
    protected static final int MENU_RINGING_SONG = 3;
    protected static final int MENU_RINGTONE_SONG = 4;
    protected static final int MENU_WHOLE_SONG = 6;
    public static final int REQUEST_CODE_SELECT_MUSIC = 24;
    private static final String TAG = "MusicPlatformTabInsideActivity";
    /* access modifiers changed from: private */
    public int currentPage = 0;
    public boolean firstLocation = false;
    String focusPlayStatus;
    String focusPlug;
    /* access modifiers changed from: private */
    public boolean isLocalData = true;
    /* access modifiers changed from: private */
    public SongListItem listItem = null;
    /* access modifiers changed from: private */
    public String mCurrentPlaySongPlug = "";
    protected BroadcastReceiver mFetchSongUrlReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            SongItem songItem = (SongItem) intent.getSerializableExtra(PrefsUtil.KEY_SONG_INFO);
            String stringExtra = intent.getStringExtra(PrefsUtil.KEY_SONG_TARGET);
            if (songItem != null && MusicPlatformActivity.this.mTarget.equals(stringExtra)) {
                MusicPlatformActivity.this.playSong(songItem.durl);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mFromPanel = false;
    /* access modifiers changed from: private */
    public Handler mHandler;
    protected LayoutInflater mInflater;
    private ListView mListView;
    protected MiguPlayerServiceInterface mPlayerServiceInterface;
    protected BroadcastReceiver mPlayingStatusReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            int i;
            Bundle extras = intent.getExtras();
            int i2 = extras.getInt(PrefsUtil.KEY_PLAYING_STATUS);
            extras.getString(PrefsUtil.KEY_SONG_TARGET);
            int i3 = 0;
            while (true) {
                if (i3 >= MusicPlatformActivity.this.mSongItemList.size()) {
                    i = -1;
                    break;
                } else if (((SongItem) MusicPlatformActivity.this.mSongItemList.get(i3)).plug.equals(MusicPlatformActivity.this.mCurrentPlaySongPlug)) {
                    i = i3;
                    break;
                } else {
                    i3++;
                }
            }
            if (i != -1) {
                switch (i2) {
                    case 0:
                        ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).play_state = 0;
                        break;
                    case 1:
                        ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).play_state = 0;
                        break;
                    case 2:
                        ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).play_state = 1;
                        break;
                }
                MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;
    /* access modifiers changed from: private */
    public ImageView mRefreshView;
    protected ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            LogTool.i(MusicPlatformActivity.TAG, "onPlayerServiceConnected");
            MusicPlatformActivity.this.mPlayerServiceInterface = MiguPlayerServiceInterface.Stub.asInterface(iBinder);
            try {
                if (MusicPlatformActivity.this.mPlayerServiceInterface == null || MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (IllegalStateException e2) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            Log.i(MusicPlatformActivity.TAG, "onPlayerServiceDisconnected");
            MusicPlatformActivity.this.mPlayerServiceInterface = null;
        }
    };
    /* access modifiers changed from: private */
    public SongItemAdapter mSongItemAdapter = null;
    /* access modifiers changed from: private */
    public ArrayList<SongItem> mSongItemList = new ArrayList<>();
    protected String mTarget;
    /* access modifiers changed from: private */
    public int pageSize = 20;
    public View.OnClickListener refreshViewListener = new View.OnClickListener() {
        public void onClick(View view) {
            MusicPlatformActivity.this.mProgressBar.setVisibility(0);
            MusicPlatformActivity.this.topMusicService.deleteAll();
            new FetchMusicList(MusicPlatformActivity.this.getApplicationContext()).execute(new Void[0]);
        }
    };
    /* access modifiers changed from: private */
    public TopMusicService topMusicService;
    /* access modifiers changed from: private */
    public int totalPage = 0;

    class CheckMusicVersion extends AbstractAsyncTask {
        public CheckMusicVersion(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.fatchMusicVersion(this.mEnginehost, this.mContext);
        }

        public String getTag() {
            return "CheckMusicVersion";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof RichTemplateRemoteVersion)) {
                if ("true".equals(((RichTemplateRemoteVersion) miguType).result)) {
                    MusicPlatformActivity.this.mRefreshView.setVisibility(0);
                    return;
                }
                MusicPlatformActivity.access$912(MusicPlatformActivity.this, 1);
                boolean unused = MusicPlatformActivity.this.isLocalData = true;
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new RichTemplateRemoteVersionParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class FetchMusicList extends AbstractAsyncTask {
        public FetchMusicList(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.fatchMusicList(this.mEnginehost, this.mContext, String.valueOf(MusicPlatformActivity.this.currentPage + 1));
        }

        public String getTag() {
            return "FetchMusicListVersion";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof ContentList)) {
                MusicPlatformActivity.this.mRefreshView.setVisibility(8);
                final ContentList contentList = (ContentList) miguType;
                int unused = MusicPlatformActivity.this.totalPage = Integer.parseInt(contentList.totalpage);
                if (MusicPlatformActivity.this.currentPage < MusicPlatformActivity.this.totalPage) {
                    MusicPlatformActivity.this.mSongItemList.addAll(contentList.items);
                    MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
                    int unused2 = MusicPlatformActivity.this.currentPage = Integer.parseInt(contentList.page);
                    MusicPlatformActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            MusicPlatformActivity.this.topMusicService.insert(contentList.items);
                        }
                    });
                }
                boolean unused3 = MusicPlatformActivity.this.isLocalData = false;
                Util.setCurrentMusicServiceVersion(MusicPlatformActivity.this.getApplicationContext(), Integer.parseInt(contentList.ver));
            }
            if (MusicPlatformActivity.this.mProgressBar != null) {
                MusicPlatformActivity.this.mProgressBar.setVisibility(8);
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new ContentListParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class SongItemAdapter extends BaseAdapter {
        SongItemAdapter() {
        }

        private void setPlayBtnListener(SongListItem songListItem, final SongItem songItem) {
            ToggleButton toggleButton = songListItem.play;
            toggleButton.setOnCheckedChangeListener(null);
            try {
                if (!songItem.plug.equals(MusicPlatformActivity.this.mCurrentPlaySongPlug) || MusicPlatformActivity.this.mPlayerServiceInterface == null || !MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                    toggleButton.setChecked(false);
                    toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                            try {
                                if (MusicPlatformActivity.this.mPlayerServiceInterface != null) {
                                    try {
                                        if (MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                            MusicPlatformActivity.this.mPlayerServiceInterface.pause();
                                        }
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                }
                                for (int i = 0; i < MusicPlatformActivity.this.mSongItemList.size(); i++) {
                                    if (((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).play_state == 1) {
                                        ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).play_state = 0;
                                    }
                                }
                                MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
                                if (!z) {
                                    String unused = MusicPlatformActivity.this.mCurrentPlaySongPlug = null;
                                    try {
                                        if (MusicPlatformActivity.this.mPlayerServiceInterface != null && MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                            MusicPlatformActivity.this.mPlayerServiceInterface.pause();
                                        }
                                    } catch (Exception e2) {
                                        e2.printStackTrace();
                                    }
                                } else if (MusicPlatformActivity.this.mCurrentPlaySongPlug == null || !MusicPlatformActivity.this.mCurrentPlaySongPlug.equals(songItem.plug)) {
                                    if (MusicPlatformActivity.this.mPlayerServiceInterface != null && MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                        MusicPlatformActivity.this.mPlayerServiceInterface.stop();
                                    }
                                    songItem.play_state = 1;
                                    MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
                                    String unused2 = MusicPlatformActivity.this.mCurrentPlaySongPlug = songItem.plug;
                                    Intent intent = new Intent();
                                    intent.setAction(PrefsUtil.INTENT_PLAY_SONG);
                                    intent.putExtra(PrefsUtil.KEY_SONG_INFO, songItem);
                                    intent.putExtra(PrefsUtil.KEY_SONG_TARGET, MusicPlatformActivity.this.mTarget);
                                    MusicPlatformActivity.this.sendBroadcast(intent);
                                    Util.logEvent("play", new NameValuePair[0]);
                                } else if (MusicPlatformActivity.this.mPlayerServiceInterface != null && !MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                    MusicPlatformActivity.this.mPlayerServiceInterface.start();
                                }
                            } catch (Exception e3) {
                                e3.printStackTrace();
                            }
                        }
                    });
                }
                toggleButton.setChecked(true);
                toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        try {
                            if (MusicPlatformActivity.this.mPlayerServiceInterface != null) {
                                try {
                                    if (MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                        MusicPlatformActivity.this.mPlayerServiceInterface.pause();
                                    }
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                            }
                            for (int i = 0; i < MusicPlatformActivity.this.mSongItemList.size(); i++) {
                                if (((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).play_state == 1) {
                                    ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).play_state = 0;
                                }
                            }
                            MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
                            if (!z) {
                                String unused = MusicPlatformActivity.this.mCurrentPlaySongPlug = null;
                                try {
                                    if (MusicPlatformActivity.this.mPlayerServiceInterface != null && MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                        MusicPlatformActivity.this.mPlayerServiceInterface.pause();
                                    }
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            } else if (MusicPlatformActivity.this.mCurrentPlaySongPlug == null || !MusicPlatformActivity.this.mCurrentPlaySongPlug.equals(songItem.plug)) {
                                if (MusicPlatformActivity.this.mPlayerServiceInterface != null && MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                    MusicPlatformActivity.this.mPlayerServiceInterface.stop();
                                }
                                songItem.play_state = 1;
                                MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
                                String unused2 = MusicPlatformActivity.this.mCurrentPlaySongPlug = songItem.plug;
                                Intent intent = new Intent();
                                intent.setAction(PrefsUtil.INTENT_PLAY_SONG);
                                intent.putExtra(PrefsUtil.KEY_SONG_INFO, songItem);
                                intent.putExtra(PrefsUtil.KEY_SONG_TARGET, MusicPlatformActivity.this.mTarget);
                                MusicPlatformActivity.this.sendBroadcast(intent);
                                Util.logEvent("play", new NameValuePair[0]);
                            } else if (MusicPlatformActivity.this.mPlayerServiceInterface != null && !MusicPlatformActivity.this.mPlayerServiceInterface.isPlaying()) {
                                MusicPlatformActivity.this.mPlayerServiceInterface.start();
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                });
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public int getCount() {
            return MusicPlatformActivity.this.mSongItemList.size();
        }

        public Object getItem(int i) {
            return MusicPlatformActivity.this.mSongItemList.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                SongListItem unused = MusicPlatformActivity.this.listItem = new SongListItem();
                View inflate = MusicPlatformActivity.this.mInflater.inflate((int) R.layout.song_item, viewGroup, false);
                MusicPlatformActivity.this.listItem.song = (TextView) inflate.findViewById(R.id.song);
                MusicPlatformActivity.this.listItem.singer = (TextView) inflate.findViewById(R.id.singer);
                MusicPlatformActivity.this.listItem.loading = (TextView) inflate.findViewById(R.id.loading_text);
                MusicPlatformActivity.this.listItem.progress = (ProgressBar) inflate.findViewById(R.id.play_progress);
                MusicPlatformActivity.this.listItem.forwardBtn = (ImageView) inflate.findViewById(R.id.forward);
                MusicPlatformActivity.this.listItem.play = (ToggleButton) inflate.findViewById(R.id.play_btn);
                MusicPlatformActivity.this.listItem.listen_count = (TextView) inflate.findViewById(R.id.listen_count);
                MusicPlatformActivity.this.listItem.music_id = (TextView) inflate.findViewById(R.id.music_id);
                MusicPlatformActivity.this.listItem.tag = (TextView) inflate.findViewById(R.id.tag);
                inflate.setTag(MusicPlatformActivity.this.listItem);
                view2 = inflate;
            } else {
                SongListItem unused2 = MusicPlatformActivity.this.listItem = (SongListItem) view.getTag();
                view2 = view;
            }
            final SongItem songItem = (SongItem) MusicPlatformActivity.this.mSongItemList.get(i);
            MusicPlatformActivity.this.listItem.song.setText(songItem.song);
            MusicPlatformActivity.this.listItem.music_id.setText(String.valueOf(i + 1));
            MusicPlatformActivity.this.listItem.singer.setText(songItem.singer);
            MusicPlatformActivity.this.listItem.listen_count.setText(songItem.forward_count == null ? "0" : songItem.forward_count);
            MusicPlatformActivity.this.listItem.tag.setText(songItem.tag);
            if (i >= MusicPlatformActivity.this.mSongItemList.size() - 1) {
                MusicPlatformActivity.this.mProgressBar.setVisibility(0);
                if (MusicPlatformActivity.this.isLocalData) {
                    ArrayList arrayList = (ArrayList) MusicPlatformActivity.this.topMusicService.queryAllNew(MusicPlatformActivity.this.currentPage * MusicPlatformActivity.this.pageSize, MusicPlatformActivity.this.pageSize);
                    MusicPlatformActivity.access$912(MusicPlatformActivity.this, 1);
                    if (arrayList.size() == 0 || arrayList.size() < MusicPlatformActivity.this.pageSize) {
                        new FetchMusicList(MusicPlatformActivity.this.getApplicationContext()).execute(new Void[0]);
                    } else {
                        MusicPlatformActivity.this.mSongItemList.addAll(arrayList);
                    }
                    MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
                } else {
                    new FetchMusicList(MusicPlatformActivity.this.getApplicationContext()).execute(new Void[0]);
                }
            }
            if (songItem.play_state == 0) {
                MusicPlatformActivity.this.listItem.progress.setVisibility(8);
                MusicPlatformActivity.this.listItem.play.setVisibility(0);
            } else {
                MusicPlatformActivity.this.listItem.progress.setVisibility(0);
                MusicPlatformActivity.this.listItem.play.setVisibility(8);
            }
            setPlayBtnListener(MusicPlatformActivity.this.listItem, songItem);
            MusicPlatformActivity.this.listItem.forwardBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Util.shareMusicBySongItem(MusicPlatformActivity.this, songItem);
                }
            });
            if (MusicPlatformActivity.this.mFromPanel) {
                MusicPlatformActivity.this.listItem.forwardBtn.setEnabled(false);
                MusicPlatformActivity.this.listItem.forwardBtn.setClickable(false);
                MusicPlatformActivity.this.listItem.forwardBtn.setPressed(false);
            }
            view2.setClickable(true);
            view2.setFocusable(true);
            view2.setSelected(true);
            view2.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                public void onClick(View view) {
                    if (MusicPlatformActivity.this.mFromPanel) {
                        Intent intent = new Intent();
                        intent.putExtra("forwarded_message", true);
                        intent.putExtra("sms_body", Util.getFullShortenUrl(MusicPlatformActivity.this.getApplicationContext(), ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).plug));
                        intent.putExtra(SmsSqliteHelper.PLUG, ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).plug);
                        intent.putExtra("song", ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).song);
                        intent.putExtra("singer", ((SongItem) MusicPlatformActivity.this.mSongItemList.get(i)).singer);
                        MusicPlatformActivity.this.setResult(24, intent);
                        MusicPlatformActivity.this.finish();
                    }
                }
            });
            return view2;
        }
    }

    class SongListItem {
        ImageView deleteBtn;
        ImageView forwardBtn;
        TextView forward_count;
        TextView listen_count;
        TextView loading;
        TextView music_id;
        ToggleButton play;
        ProgressBar progress;
        TextView singer;
        TextView song;
        View songItem;
        TextView tag;

        SongListItem() {
        }
    }

    static /* synthetic */ int access$912(MusicPlatformActivity musicPlatformActivity, int i) {
        int i2 = musicPlatformActivity.currentPage + i;
        musicPlatformActivity.currentPage = i2;
        return i2;
    }

    /* access modifiers changed from: private */
    public void playSong(String str) {
        LogTool.i("url", str);
        Intent intent = new Intent(getApplicationContext(), MiguPlayerService.class);
        intent.setAction("play");
        Bundle bundle = new Bundle();
        bundle.putString(PrefsUtil.KEY_SONG_FILE_PAHT, str);
        bundle.putSerializable(PrefsUtil.KEY_SONG_TYPE, MiguPlayerService.SongType.RADIO);
        bundle.putSerializable(PrefsUtil.KEY_SONG_TARGET, this.mTarget);
        intent.putExtras(bundle);
        startService(intent);
    }

    private void showDeleteDialog(final SongItem songItem) {
        new AlertDialog.Builder(this).setMessage((int) R.string.delete_music_confirm).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Util.logEvent(PrefsUtil.EVENT_MUSIC_DELETE, new NameValuePair[0]);
                MusicPlatformActivity.this.mSongItemList.remove(songItem);
                MusicPlatformActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        MusicPlatformActivity.this.topMusicService.deleteSongItem(songItem.plug);
                    }
                });
                MusicPlatformActivity.this.mSongItemAdapter.notifyDataSetChanged();
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.music_main);
        this.mInflater = LayoutInflater.from(this);
        this.mListView = (ListView) findViewById(16908298);
        this.mProgressBar = (ProgressBar) findViewById(R.id.load_progress);
        this.mSongItemList = new ArrayList<>();
        this.mFromPanel = getIntent().getBooleanExtra("from_panel", false);
        this.mHandler = new Handler();
        this.mSongItemAdapter = new SongItemAdapter();
        this.mListView.setAdapter((ListAdapter) this.mSongItemAdapter);
        this.topMusicService = new TopMusicService(getApplicationContext());
        this.mRefreshView = (ImageView) findViewById(R.id.refresh);
        this.mRefreshView.setOnClickListener(this.refreshViewListener);
        if (Util.checkNetWork(getApplicationContext())) {
            this.mSongItemList = (ArrayList) this.topMusicService.queryAllNew(this.currentPage * this.pageSize, this.pageSize);
            this.mSongItemAdapter.notifyDataSetChanged();
            new CheckMusicVersion(getApplicationContext()).execute(new Void[0]);
            this.isLocalData = false;
        } else {
            this.mSongItemList = (ArrayList) this.topMusicService.queryAllNew(this.currentPage * this.pageSize, this.pageSize);
            this.currentPage++;
            this.isLocalData = true;
            this.mSongItemAdapter.notifyDataSetChanged();
        }
        this.mListView.setVisibility(0);
        this.mListView.setLongClickable(true);
        this.mTarget = PrefsUtil.KEY_TARGET_MUSIC_BOARDS;
        Util.logEvent(PrefsUtil.EVENT_MUSIC_APP_OPEN, new NameValuePair[0]);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 84:
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        registerReceiver(this.mFetchSongUrlReceiver, new IntentFilter(PrefsUtil.INTENT_PLAY_SONG));
        registerReceiver(this.mPlayingStatusReceiver, new IntentFilter(PrefsUtil.INTENT_UPDATE_PLAYING_STATUS));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mHandler.post(new Runnable() {
            public void run() {
                Intent intent = new Intent(MusicPlatformActivity.this.getApplicationContext(), MiguPlayerService.class);
                MusicPlatformActivity.this.startService(intent);
                MusicPlatformActivity.this.getApplicationContext().bindService(intent, MusicPlatformActivity.this.mServiceConnection, 1);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mCurrentPlaySongPlug = "";
        this.mFromPanel = false;
        try {
            if (this.mPlayerServiceInterface != null) {
                this.mPlayerServiceInterface.stop();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        try {
            if (this.mServiceConnection != null) {
                try {
                    unbindService(this.mServiceConnection);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            unregisterReceiver(this.mFetchSongUrlReceiver);
            unregisterReceiver(this.mPlayingStatusReceiver);
        } catch (Exception e3) {
        }
    }
}
