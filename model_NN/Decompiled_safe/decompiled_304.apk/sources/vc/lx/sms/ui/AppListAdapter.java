package vc.lx.sms.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CursorAdapter;
import android.widget.TextView;
import java.util.HashSet;
import vc.lx.sms2.R;

public class AppListAdapter extends CursorAdapter implements AbsListView.RecyclerListener {
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "AppListAdapter";
    private final LayoutInflater mFactory;
    private boolean mIsMultiMode = false;
    private OnContentChangedListener mOnContentChangedListener;
    public HashSet<Long> mSelectedIds = new HashSet<>();

    public interface OnContentChangedListener {
        void onContentChanged(AppListAdapter appListAdapter);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.content.Context, android.database.Cursor, int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    public AppListAdapter(Context context, Cursor cursor) {
        super(context, cursor, false);
        cursor.getCount();
        this.mFactory = LayoutInflater.from(context);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        if (cursor != null) {
            ((TextView) view.findViewById(R.id.app_name)).setText(cursor.getString(cursor.getColumnIndex("name")));
        }
    }

    public void clearSelectedIds() {
        this.mSelectedIds.clear();
    }

    public HashSet<Long> getSelectedIds() {
        return this.mSelectedIds;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.mFactory.inflate((int) R.layout.app_setting_item, viewGroup, false);
    }

    /* access modifiers changed from: protected */
    public void onContentChanged() {
        if (getCursor() != null && !getCursor().isClosed() && this.mOnContentChangedListener != null) {
            this.mOnContentChangedListener.onContentChanged(this);
        }
    }

    public void onMovedToScrapHeap(View view) {
    }

    public void setMultiMode(boolean z) {
        this.mIsMultiMode = z;
    }

    public void setOnContentChangedListener(OnContentChangedListener onContentChangedListener) {
        this.mOnContentChangedListener = onContentChangedListener;
    }
}
