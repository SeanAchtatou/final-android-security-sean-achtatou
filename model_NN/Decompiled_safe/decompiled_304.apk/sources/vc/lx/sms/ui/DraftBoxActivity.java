package vc.lx.sms.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.android.mms.transaction.MessageSender;
import com.android.provider.Telephony;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.ui.DraftListAdapter;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms2.R;

public class DraftBoxActivity extends AbstractFlurryListActivity {
    private List<Map<String, Object>> mDraftData;
    DraftListAdapter mListAdapter;
    ListView mListView;

    public static Intent createIntent(Context context, long j) {
        Intent intent = new Intent("android.intent.action.VIEW");
        if (j > 0) {
            intent.setData(Conversation.getUri(j));
        } else {
            intent.setComponent(new ComponentName(context, DraftBoxActivity.class));
        }
        return intent;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0051  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void getAddressByRecipientId(boolean r9, java.lang.StringBuffer r10, java.lang.String r11) {
        /*
            r8 = this;
            r6 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "content://mms-sms/canonical-address/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r0 = r0.toString()
            android.net.Uri r1 = android.net.Uri.parse(r0)
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r8
            android.database.Cursor r0 = r0.managedQuery(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x005a, all -> 0x004d }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x003d }
            if (r1 == 0) goto L_0x005d
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x003d }
        L_0x002c:
            if (r9 == 0) goto L_0x0037
        L_0x002e:
            r10.append(r1)     // Catch:{ Exception -> 0x003d }
            if (r0 == 0) goto L_0x0036
            r0.close()
        L_0x0036:
            return
        L_0x0037:
            java.lang.String r2 = ";"
            r10.append(r2)     // Catch:{ Exception -> 0x003d }
            goto L_0x002e
        L_0x003d:
            r1 = move-exception
        L_0x003e:
            java.lang.StringBuffer r1 = r10.append(r11)     // Catch:{ all -> 0x0055 }
            java.lang.String r2 = " "
            r1.append(r2)     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x0036
            r0.close()
            goto L_0x0036
        L_0x004d:
            r0 = move-exception
            r1 = r6
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()
        L_0x0054:
            throw r0
        L_0x0055:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x004f
        L_0x005a:
            r0 = move-exception
            r0 = r6
            goto L_0x003e
        L_0x005d:
            r1 = r6
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.DraftBoxActivity.getAddressByRecipientId(boolean, java.lang.StringBuffer, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void getDisplayNameByAddress(boolean r9, java.lang.StringBuilder r10, java.lang.String r11) {
        /*
            r8 = this;
            r6 = 0
            android.content.ContentResolver r0 = r8.getContentResolver()     // Catch:{ Exception -> 0x0059, all -> 0x0056 }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0059, all -> 0x0056 }
            r2 = 0
            java.lang.String r3 = "data1 = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0059, all -> 0x0056 }
            r5 = 0
            r4[r5] = r11     // Catch:{ Exception -> 0x0059, all -> 0x0056 }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0059, all -> 0x0056 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0038 }
            if (r1 == 0) goto L_0x005c
            java.lang.String r1 = "display_name"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0038 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0038 }
        L_0x0025:
            if (r9 == 0) goto L_0x0032
        L_0x0027:
            if (r1 == 0) goto L_0x0048
            r10.append(r1)     // Catch:{ Exception -> 0x0038 }
        L_0x002c:
            if (r0 == 0) goto L_0x0031
            r0.close()
        L_0x0031:
            return
        L_0x0032:
            java.lang.String r2 = ","
            r10.append(r2)     // Catch:{ Exception -> 0x0038 }
            goto L_0x0027
        L_0x0038:
            r1 = move-exception
        L_0x0039:
            java.lang.StringBuilder r1 = r10.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.String r2 = " "
            r1.append(r2)     // Catch:{ all -> 0x004c }
            if (r0 == 0) goto L_0x0031
            r0.close()
            goto L_0x0031
        L_0x0048:
            r10.append(r11)     // Catch:{ Exception -> 0x0038 }
            goto L_0x002c
        L_0x004c:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()
        L_0x0055:
            throw r0
        L_0x0056:
            r0 = move-exception
            r1 = r6
            goto L_0x0050
        L_0x0059:
            r0 = move-exception
            r0 = r6
            goto L_0x0039
        L_0x005c:
            r1 = r6
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.DraftBoxActivity.getDisplayNameByAddress(boolean, java.lang.StringBuilder, java.lang.String):void");
    }

    private void initListAdapter() {
        getDraftData();
        this.mListAdapter = new DraftListAdapter(this, this.mDraftData);
        setListAdapter(this.mListAdapter);
    }

    /* access modifiers changed from: private */
    public void openThread(long j) {
        startActivity(ComposeMessageActivity.createIntentByThreadId(this, j));
    }

    public void getDraftData() {
        this.mDraftData = new ArrayList();
        Uri parse = Uri.parse("content://mms-sms/draft");
        try {
            Uri build = Uri.parse("content://mms-sms/conversations").buildUpon().appendQueryParameter("simple", "true").build();
            Cursor managedQuery = managedQuery(parse, new String[]{"thread_id"}, null, null, null);
            HashSet hashSet = new HashSet();
            while (managedQuery.moveToNext()) {
                long j = managedQuery.getLong(0);
                if (!hashSet.contains(Long.valueOf(j))) {
                    hashSet.add(Long.valueOf(j));
                    Cursor managedQuery2 = managedQuery(build, null, "_id = ?", new String[]{String.valueOf(j)}, null);
                    while (managedQuery2.moveToNext()) {
                        String string = managedQuery2.getString(managedQuery2.getColumnIndex(Telephony.ThreadsColumns.RECIPIENT_IDS));
                        long j2 = managedQuery2.getLong(managedQuery2.getColumnIndex("date"));
                        String string2 = managedQuery2.getString(managedQuery2.getColumnIndex("snippet"));
                        StringBuffer stringBuffer = new StringBuffer();
                        for (String addressByRecipientId : string.split(" ")) {
                            getAddressByRecipientId(true, stringBuffer, addressByRecipientId);
                        }
                        StringBuilder sb = new StringBuilder();
                        for (String displayNameByAddress : stringBuffer.toString().split(MessageSender.RECIPIENTS_SEPARATOR)) {
                            getDisplayNameByAddress(true, sb, displayNameByAddress);
                        }
                        HashMap hashMap = new HashMap();
                        hashMap.put("msgbody", string2);
                        hashMap.put("date", MessageUtils.formatTimeStampString(getApplicationContext(), j2));
                        hashMap.put("thread_id", String.valueOf(j));
                        hashMap.put("phonenumber", sb.toString());
                        this.mDraftData.add(hashMap);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.darftbox_screen);
        initListAdapter();
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                DraftBoxActivity.this.openThread(Long.parseLong(((DraftListAdapter.ViewHolder) view.getTag()).thread_id));
            }
        });
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DraftBoxActivity.this.finish();
            }
        });
    }
}
