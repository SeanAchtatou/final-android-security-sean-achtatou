package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.util.SmsConstants;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ContactsAllActivity extends AbstractContactsListActivity implements View.OnClickListener {
    protected static List<String> cacheCheckNums = new ArrayList();
    protected List<String> cacheNames = new ArrayList();
    protected List<String> cacheNums = new ArrayList();
    /* access modifiers changed from: private */
    public boolean isMulit = false;
    /* access modifiers changed from: private */
    public ContactsListAdapter mContactsListAdapter;
    private ContactsListQueryHandler mContactsListQueryHandler;

    private class ContactsListAdapter extends CursorAdapter {
        public ContactsListAdapter(Context context, Cursor cursor) {
            super(context, cursor);
        }

        public void bindView(View view, Context context, Cursor cursor) {
            final String string = !ContactsAllActivity.this.isVisitedFromCallingCard ? cursor.getString(3) : ContactsAllActivity.this.cacheNames.get(cursor.getPosition());
            ((TextView) view.findViewById(R.id.contact_name)).setText(string);
            CheckBox checkBox = (CheckBox) view.findViewById(16908289);
            checkBox.setOnCheckedChangeListener(null);
            if (ContactsAllActivity.this.isMulit) {
                checkBox.setChecked(true);
            }
            if (!ContactsAllActivity.this.isVisitedFromCallingCard) {
                final String string2 = cursor.getString(1);
                int i = cursor.getInt(2);
                ((TextView) view.findViewById(R.id.address)).setText(string2);
                ((TextView) view.findViewById(R.id.type)).setText(AbstractContactsListActivity.getDisplayNameForPhoneType(ContactsAllActivity.this, i));
                if (ContactsAllActivity.cacheCheckNums.contains(string2)) {
                    checkBox.setChecked(true);
                } else {
                    checkBox.setChecked(false);
                }
                if (ContactsSelectActivity.mSelectedNumberOfContacts.contains(string2)) {
                    checkBox.setChecked(true);
                }
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                        ContactsAllActivity.this.mReady = false;
                        if (!z) {
                            ContactsAllActivity.cacheCheckNums.remove(string2);
                            ContactsSelectActivity.mSelectedNumberOfContacts.remove(string2);
                        } else if (!ContactsSelectActivity.mSelectedNumberOfContacts.contains(string2)) {
                            ContactsAllActivity.cacheCheckNums.add(string2);
                            ContactsSelectActivity.mSelectedNumberOfContacts.add(string2);
                        }
                        ContactsAllActivity.this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
                    }
                });
                return;
            }
            if (AbstractContactsListActivity.cacheCheckNames.contains(string)) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    ContactsAllActivity.this.mReady = false;
                    if (z) {
                        AbstractContactsListActivity.cacheCheckNames.add(string);
                    } else {
                        AbstractContactsListActivity.cacheCheckNames.remove(string);
                    }
                    ContactsAllActivity.this.mCountView.setText("(" + AbstractContactsListActivity.cacheCheckNames.size() + ")");
                }
            });
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return ContactsAllActivity.this.isVisitedFromCallingCard ? ContactsAllActivity.this.mInflater.inflate((int) R.layout.contact_item_calling_card, viewGroup, false) : ContactsAllActivity.this.mInflater.inflate((int) R.layout.contact_item, viewGroup, false);
        }
    }

    private class ContactsListQueryHandler extends AsyncQueryHandler {
        public ContactsListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case 0:
                    ContactsAllActivity.this.fillData(cursor);
                    if (ContactsAllActivity.this.mContactsListAdapter != null) {
                        ContactsAllActivity.this.mContactsListAdapter.changeCursor(cursor);
                    }
                    ContactsAllActivity.this.setProgressBarIndeterminateVisibility(false);
                    return;
                case 1:
                    ContactsSelectActivity.mSelectedNumberOfContacts.clear();
                    while (cursor.moveToNext()) {
                        ContactsSelectActivity.mSelectedNumberOfContacts.add(cursor.getString(1));
                    }
                    ContactsAllActivity.this.setProgressBarIndeterminateVisibility(false);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void fillData(Cursor cursor) {
        startManagingCursor(cursor);
        if (cursor != null) {
            if (!this.isVisitedFromCallingCard) {
                while (cursor.moveToNext()) {
                    String string = cursor.getString(3);
                    String string2 = cursor.getString(cursor.getColumnIndex("_id"));
                    this.cacheNames.add(string);
                    String string3 = cursor.getString(1);
                    this.cacheNums.add(string3);
                    cacheNum.put(string2, string3);
                    SmsApplication.getInstance().getmAllNumAndNameCache().put(string3, string);
                }
            } else {
                while (cursor.moveToNext()) {
                    String string4 = cursor.getString(cursor.getColumnIndex("display_name"));
                    if (string4 == null || string4.length() == 0) {
                        this.cacheNames.add(getString(R.string.unknown));
                    } else if (string4.length() > 0) {
                        this.cacheNames.add(string4);
                    }
                }
            }
        }
        this.mReady = true;
    }

    public void MenuCacelModel() {
        this.isMulit = false;
        this.mContactsListAdapter.notifyDataSetChanged();
        ContactsSelectActivity.mSelectedNumberOfContacts.clear();
        cacheCheckNums.clear();
        this.mCountView.setText("(" + cacheCheckNums.size() + ")");
    }

    public void MenuMulitModel() {
        this.isMulit = true;
        this.mContactsListAdapter.notifyDataSetChanged();
        cacheCheckNums.clear();
        cacheCheckNums.addAll(this.cacheNums);
        ContactsSelectActivity.mSelectedNumberOfContacts.addAll(this.cacheNums);
        this.mCountView.setText("(" + cacheCheckNums.size() + ")");
    }

    public List<String> getCache() {
        return this.cacheNames;
    }

    public void initDatas(Cursor cursor) {
    }

    public void initViews() {
        this.cacheNames.clear();
        if (getIntent() != null) {
            this.isVisitedFromCallingCard = getIntent().getBooleanExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, false);
        }
        this.mContactsListQueryHandler = new ContactsListQueryHandler(getContentResolver());
        if (!this.isVisitedFromCallingCard) {
            this.mContactsListQueryHandler.startQuery(0, null, ContactsContract.Data.CONTENT_URI, MPROJECTIONSTRINGS, "mimetype='vnd.android.cursor.item/phone_v2' and in_visible_group=1 ", null, "display_name COLLATE LOCALIZED ASC ");
        } else {
            this.mContactsListQueryHandler.startQuery(0, null, ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        }
        this.mContactsListAdapter = new ContactsListAdapter(this, null);
        setListAdapter(this.mContactsListAdapter);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok /*2131492968*/:
            case R.id.btn_send_invitation /*2131492972*/:
                if (this.isVisitedFromCallingCard) {
                    fillData();
                }
                finish();
                return;
            case R.id.selCount /*2131492969*/:
            case R.id.invitation_action_bar /*2131492971*/:
            default:
                return;
            case R.id.back /*2131492970*/:
                ContactsSelectActivity.mSelectedNumberOfContacts.clear();
                finish();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (getIntent() != null) {
            this.isVisitedFromCallingCard = getIntent().getBooleanExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, false);
        }
        this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
        if (this.cacheNames != null && this.cacheNames.size() > 0) {
            this.mReady = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        cacheCheckNums.clear();
        cacheCheckNames.clear();
        super.onStop();
    }
}
