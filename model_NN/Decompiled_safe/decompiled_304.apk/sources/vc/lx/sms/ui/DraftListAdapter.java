package vc.lx.sms.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import java.util.Map;
import vc.lx.sms2.R;

public class DraftListAdapter extends BaseAdapter {
    private List<Map<String, Object>> items;
    private LayoutInflater mInflater;

    public final class ViewHolder {
        public TextView date;
        public TextView msgBody;
        public TextView phoneNumber;
        public String thread_id;

        public ViewHolder() {
        }
    }

    public DraftListAdapter(Context context, List<Map<String, Object>> list) {
        this.items = list;
        this.mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int i) {
        return this.items.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        View view2;
        if (view == null) {
            viewHolder = new ViewHolder();
            view2 = this.mInflater.inflate((int) R.layout.draftlist_item, (ViewGroup) null);
            viewHolder.phoneNumber = (TextView) view2.findViewById(R.id.phonenumber);
            viewHolder.msgBody = (TextView) view2.findViewById(R.id.msgbody);
            viewHolder.date = (TextView) view2.findViewById(R.id.draftmsgdata);
            viewHolder.thread_id = "";
            view2.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
            view2 = view;
        }
        viewHolder.phoneNumber.setText((String) this.items.get(i).get("phonenumber"));
        viewHolder.msgBody.setText((String) this.items.get(i).get("msgbody"));
        viewHolder.date.setText((String) this.items.get(i).get("date"));
        viewHolder.thread_id = (String) this.items.get(i).get("thread_id");
        return view2;
    }

    public void setData(List<Map<String, Object>> list) {
        this.items = list;
    }
}
