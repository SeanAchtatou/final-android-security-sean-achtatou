package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import com.android.mms.util.SmileyParser;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.util.MessageUtils;

public class ConversationHeader {
    private static final int THREAD_VOTE_QUERY_TOKEN = 0;
    private Context mContext;
    private Conversation mConversation;
    private String mDate;
    private boolean mHasAttachment;
    private boolean mHasDraft;
    private boolean mHasError;
    /* access modifiers changed from: private */
    public boolean mHasVoteDraft;
    private boolean mIsRead;
    private int mMessageCount;
    private int mPresenceResId = 0;
    private String mRecipientString;
    private ContactList mRecipients;
    private CharSequence mSubject;
    private long mThreadId;
    private ThreadVoteDraftQueryHandler mThreadVoteDraftQueryHandler;

    private final class ThreadVoteDraftQueryHandler extends AsyncQueryHandler {
        public ThreadVoteDraftQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            switch (i) {
                case 0:
                    if (cursor.getCount() > 0) {
                        boolean unused = ConversationHeader.this.mHasVoteDraft = true;
                        return;
                    } else {
                        boolean unused2 = ConversationHeader.this.mHasVoteDraft = false;
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public ConversationHeader(Context context, Conversation conversation) {
        this.mContext = context;
        this.mConversation = conversation;
        this.mThreadId = conversation.getThreadId();
        this.mSubject = SmileyParser.getInstance().addSmileySpans(conversation.getSnippet());
        this.mDate = MessageUtils.formatTimeStampString(context, conversation.getDate());
        this.mIsRead = !conversation.hasUnreadMessages();
        this.mHasError = conversation.hasError();
        this.mHasDraft = conversation.hasDraft();
        this.mHasVoteDraft = conversation.hasVoteDraft();
        if (!this.mHasDraft) {
            this.mHasDraft = this.mHasVoteDraft;
        }
        this.mMessageCount = conversation.getMessageCount();
        this.mHasAttachment = conversation.hasAttachment();
        updateRecipients();
    }

    public ContactList getContacts() {
        return this.mRecipients;
    }

    public String getDate() {
        return this.mDate;
    }

    public String getFrom() {
        return this.mRecipientString;
    }

    public int getMessageCount() {
        return this.mMessageCount;
    }

    public int getPresenceResourceId() {
        return this.mPresenceResId;
    }

    public CharSequence getSubject() {
        return this.mSubject;
    }

    public long getThreadId() {
        return this.mThreadId;
    }

    public boolean hasAttachment() {
        return this.mHasAttachment;
    }

    public boolean hasDraft() {
        return this.mHasDraft;
    }

    public boolean hasError() {
        return this.mHasError;
    }

    public boolean hasVoteDraft() {
        return this.mHasVoteDraft;
    }

    public boolean isRead() {
        return this.mIsRead;
    }

    public String toString() {
        return "[ConversationHeader from:" + getFrom() + " subject:" + ((Object) getSubject()) + "]";
    }

    public void updateRecipients() {
        this.mRecipients = this.mConversation.getRecipients();
        this.mRecipientString = this.mRecipients.formatNames(", ");
    }
}
