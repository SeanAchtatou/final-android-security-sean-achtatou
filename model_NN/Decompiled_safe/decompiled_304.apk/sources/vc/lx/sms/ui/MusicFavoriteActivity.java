package vc.lx.sms.ui;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.io.IOException;
import java.util.ArrayList;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.service.MiguPlayerServiceInterface;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class MusicFavoriteActivity extends AbstractFlurryActivity {
    protected static final int MENU_DOWNLOAD_SONG = 2;
    protected static final int MENU_PLAY_SONG = 1;
    protected static final int MENU_PRESENT_RINGTONE = 5;
    protected static final int MENU_RINGING_SONG = 3;
    protected static final int MENU_RINGTONE_SONG = 4;
    protected static final int MENU_WHOLE_SONG = 6;
    private static final String TAG = "MusicFavoriteActivity";
    /* access modifiers changed from: private */
    public int currentPage = 0;
    public boolean firstLocation = false;
    String focusPlayStatus;
    String focusPlug;
    /* access modifiers changed from: private */
    public SongListItem listItem = null;
    /* access modifiers changed from: private */
    public Handler mHandler;
    protected LayoutInflater mInflater;
    private ListView mListView;
    /* access modifiers changed from: private */
    public MediaPlayer mMediaPlayer;
    protected MiguPlayerServiceInterface mPlayerServiceInterface;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;
    private ImageView mRefreshView;
    /* access modifiers changed from: private */
    public SongItemAdapter mSongItemAdapter = null;
    /* access modifiers changed from: private */
    public ArrayList<SongItem> mSongItemList = new ArrayList<>();
    private String mTarget = PrefsUtil.KEY_TARGET_MUSIC_BOARDS;
    private TextView mTitleView;
    /* access modifiers changed from: private */
    public int pageSize = 20;
    /* access modifiers changed from: private */
    public TopMusicService topMusicService;

    class SongItemAdapter extends BaseAdapter {
        SongItemAdapter() {
        }

        private void setPlayBtnListener(SongListItem songListItem, final SongItem songItem) {
            songListItem.play.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    int i = 0;
                    while (i < MusicFavoriteActivity.this.mSongItemList.size()) {
                        try {
                            ((SongItem) MusicFavoriteActivity.this.mSongItemList.get(i)).play_state = 0;
                            i++;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                    if (z) {
                        songItem.play_state = 1;
                        MusicFavoriteActivity.this.mSongItemAdapter.notifyDataSetChanged();
                        if (songItem.durl != null) {
                            MusicFavoriteActivity.this.mHandler.post(new Runnable() {
                                public void run() {
                                    MusicFavoriteActivity.this.playMusic(songItem.durl);
                                }
                            });
                            return;
                        }
                        return;
                    }
                    songItem.play_state = 0;
                    MusicFavoriteActivity.this.mSongItemAdapter.notifyDataSetChanged();
                    try {
                        if (MusicFavoriteActivity.this.mMediaPlayer != null && MusicFavoriteActivity.this.mMediaPlayer.isPlaying()) {
                            MusicFavoriteActivity.this.mMediaPlayer.pause();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            });
        }

        public int getCount() {
            return MusicFavoriteActivity.this.mSongItemList.size();
        }

        public Object getItem(int i) {
            return MusicFavoriteActivity.this.mSongItemList.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                SongListItem unused = MusicFavoriteActivity.this.listItem = new SongListItem();
                View inflate = MusicFavoriteActivity.this.mInflater.inflate((int) R.layout.song_item, viewGroup, false);
                MusicFavoriteActivity.this.listItem.song = (TextView) inflate.findViewById(R.id.song);
                MusicFavoriteActivity.this.listItem.singer = (TextView) inflate.findViewById(R.id.singer);
                MusicFavoriteActivity.this.listItem.loading = (TextView) inflate.findViewById(R.id.loading_text);
                MusicFavoriteActivity.this.listItem.progress = (ProgressBar) inflate.findViewById(R.id.play_progress);
                MusicFavoriteActivity.this.listItem.forwardBtn = (ImageView) inflate.findViewById(R.id.forward);
                MusicFavoriteActivity.this.listItem.play = (ToggleButton) inflate.findViewById(R.id.play_btn);
                MusicFavoriteActivity.this.listItem.listen_count = (TextView) inflate.findViewById(R.id.listen_count);
                MusicFavoriteActivity.this.listItem.music_id = (TextView) inflate.findViewById(R.id.music_id);
                inflate.setTag(MusicFavoriteActivity.this.listItem);
                view2 = inflate;
            } else {
                SongListItem unused2 = MusicFavoriteActivity.this.listItem = (SongListItem) view.getTag();
                view2 = view;
            }
            final SongItem songItem = (SongItem) MusicFavoriteActivity.this.mSongItemList.get(i);
            MusicFavoriteActivity.this.listItem.song.setText(songItem.song);
            MusicFavoriteActivity.this.listItem.music_id.setText(String.valueOf(i + 1));
            MusicFavoriteActivity.this.listItem.singer.setText(songItem.singer);
            MusicFavoriteActivity.this.listItem.listen_count.setVisibility(8);
            MusicFavoriteActivity.this.listItem.listen_count.setText((songItem.listen_count == null ? "0" : songItem.listen_count) + MusicFavoriteActivity.this.getString(R.string.forward_count));
            if (i >= MusicFavoriteActivity.this.mSongItemList.size() - 1) {
                MusicFavoriteActivity.this.mProgressBar.setVisibility(8);
                ArrayList arrayList = (ArrayList) MusicFavoriteActivity.this.topMusicService.getSongItemsByMaster(MusicFavoriteActivity.this.currentPage, MusicFavoriteActivity.this.pageSize);
                MusicFavoriteActivity.access$312(MusicFavoriteActivity.this, 1);
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    if (!MusicFavoriteActivity.this.mSongItemList.contains(arrayList.get(i2))) {
                        MusicFavoriteActivity.this.mSongItemList.add(arrayList.get(i2));
                    }
                }
                MusicFavoriteActivity.this.mSongItemAdapter.notifyDataSetChanged();
            }
            MusicFavoriteActivity.this.listItem.play.setOnCheckedChangeListener(null);
            if (songItem.play_state == 0) {
                MusicFavoriteActivity.this.listItem.play.setChecked(false);
            } else {
                MusicFavoriteActivity.this.listItem.play.setChecked(true);
            }
            MusicFavoriteActivity.this.listItem.play.setClickable(true);
            MusicFavoriteActivity.this.listItem.play.setSelected(true);
            setPlayBtnListener(MusicFavoriteActivity.this.listItem, songItem);
            MusicFavoriteActivity.this.listItem.forwardBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Util.shareMusicBySongItem(MusicFavoriteActivity.this, songItem);
                }
            });
            view2.setClickable(true);
            view2.setFocusable(true);
            view2.setSelected(true);
            return view2;
        }
    }

    class SongListItem {
        ImageView deleteBtn;
        ImageView forwardBtn;
        TextView forward_count;
        TextView listen_count;
        TextView loading;
        TextView music_id;
        ToggleButton play;
        ProgressBar progress;
        TextView singer;
        TextView song;
        View songItem;

        SongListItem() {
        }
    }

    static /* synthetic */ int access$312(MusicFavoriteActivity musicFavoriteActivity, int i) {
        int i2 = musicFavoriteActivity.currentPage + i;
        musicFavoriteActivity.currentPage = i2;
        return i2;
    }

    public void clearAllPlayState() {
        if (this.mSongItemList.size() > 0) {
            for (int i = 0; i < this.mSongItemList.size(); i++) {
                this.mSongItemList.get(i).play_state = 0;
            }
            this.mSongItemAdapter.notifyDataSetChanged();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.music_main);
        this.mInflater = LayoutInflater.from(this);
        this.mTitleView = (TextView) findViewById(R.id.music_title);
        this.mTitleView.setText(getString(R.string.music));
        this.mListView = (ListView) findViewById(16908298);
        this.mProgressBar = (ProgressBar) findViewById(R.id.load_progress);
        this.mSongItemList = new ArrayList<>();
        this.mHandler = new Handler();
        this.mSongItemAdapter = new SongItemAdapter();
        this.mListView.setAdapter((ListAdapter) this.mSongItemAdapter);
        this.topMusicService = new TopMusicService(getApplicationContext());
        this.mRefreshView = (ImageView) findViewById(R.id.refresh);
        this.mRefreshView.setVisibility(8);
        this.mSongItemList = (ArrayList) this.topMusicService.getSongItemsByMaster(this.currentPage * this.pageSize, this.pageSize);
        this.currentPage++;
        this.mSongItemAdapter.notifyDataSetChanged();
        if (this.mSongItemList.size() == 0) {
            Toast.makeText(getApplicationContext(), getString(R.string.no_save_any_music), 0).show();
        }
        this.mListView.setVisibility(0);
        this.mListView.setLongClickable(true);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 84:
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
        }
    }

    public void playMusic(String str) {
        try {
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.stop();
                this.mMediaPlayer.release();
                this.mMediaPlayer = null;
            }
            this.mMediaPlayer = new MediaPlayer();
            this.mMediaPlayer.setDataSource(str);
            this.mMediaPlayer.prepare();
            this.mMediaPlayer.start();
            this.mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.release();
                    MusicFavoriteActivity.this.clearAllPlayState();
                }
            });
            this.mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                    mediaPlayer.release();
                    MusicFavoriteActivity.this.clearAllPlayState();
                    return false;
                }
            });
            this.mProgressBar.setVisibility(8);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }
}
