package vc.lx.sms.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.google.android.mms.pdu.PduHeaders;
import vc.lx.sms2.R;

public class VoteImageView extends ImageView {
    private String count = "";
    private Bitmap image = null;
    private int optionCounter;
    private int totalCounter;

    public VoteImageView(Context context) {
        super(context);
        initRes(context);
    }

    public VoteImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initRes(context);
    }

    public VoteImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initRes(context);
    }

    private void initRes(Context context) {
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_select_img_popup_item_2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, int, int, int, int, boolean, android.graphics.Paint):void}
     arg types: [int[], int, int, int, int, int, int, int, android.graphics.Paint]
     candidates:
      ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, float, float, int, int, boolean, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, int, int, int, int, boolean, android.graphics.Paint):void} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.create(this.count, 1));
        canvas.drawBitmap(new int[]{PduHeaders.RESPONSE_STATUS_ERROR_PERMANENT_REPLY_CHARGING_NOT_SUPPORTED, 123, 140}, 0, 0, 0, 0, 30, 30, true, paint);
        super.onDraw(canvas);
    }
}
