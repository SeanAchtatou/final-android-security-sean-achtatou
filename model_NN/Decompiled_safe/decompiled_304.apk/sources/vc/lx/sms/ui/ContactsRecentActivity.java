package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.util.SmsConstants;
import vc.lx.sms2.R;

public class ContactsRecentActivity extends AbstractContactsListActivity {
    protected static final int COLUMN_DISPLAY_NAME = 3;
    protected static final int COLUMN_NUMBER = 1;
    protected static final int COLUMN_TYPE = 2;
    protected static List<String> cacheCheckNums = new ArrayList();
    protected List<String> cache = new ArrayList();
    /* access modifiers changed from: private */
    public ContactsListAdapter mContactsListAdapter;
    private ContactsListQueryHandler mContactsListQueryHandler;
    protected LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public List<String> nameList = new ArrayList();
    /* access modifiers changed from: private */
    public List<RecentCall> recentCalls = new ArrayList();

    class ContactsListAdapter extends BaseAdapter {
        private List<RecentCall> data = new ArrayList();

        public ContactsListAdapter(Context context, List<RecentCall> list) {
            this.data = list;
        }

        public void changeData(List<RecentCall> list) {
            this.data = list;
            notifyDataSetChanged();
        }

        public int getCount() {
            if (this.data == null || this.data.size() == 0) {
                return 0;
            }
            return this.data.size();
        }

        public Object getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            RecentCallView recentCallView;
            View view2;
            View inflate;
            final RecentCall recentCall = this.data.get(i);
            if (view == null) {
                RecentCallView recentCallView2 = new RecentCallView();
                if (!ContactsRecentActivity.this.isVisitedFromCallingCard) {
                    inflate = ContactsRecentActivity.this.mInflater.inflate((int) R.layout.contact_item, (ViewGroup) null);
                    recentCallView2.mNumView = (TextView) inflate.findViewById(R.id.address);
                    recentCallView2.mTypeView = (TextView) inflate.findViewById(R.id.type);
                } else {
                    inflate = ContactsRecentActivity.this.mInflater.inflate((int) R.layout.contact_item_calling_card, (ViewGroup) null);
                }
                recentCallView2.mNameView = (TextView) inflate.findViewById(R.id.contact_name);
                recentCallView2.mCheckBox = (CheckBox) inflate.findViewById(16908289);
                inflate.setTag(recentCallView2);
                recentCallView = recentCallView2;
                view2 = inflate;
            } else {
                recentCallView = (RecentCallView) view.getTag();
                view2 = view;
            }
            recentCallView.mNameView.setText(recentCall.name);
            if (!ContactsRecentActivity.this.isVisitedFromCallingCard) {
                recentCallView.mNumView.setText(recentCall.number);
                recentCallView.mTypeView.setText(AbstractContactsListActivity.getDisplayNameForPhoneType(ContactsRecentActivity.this, recentCall.type));
            }
            recentCallView.mCheckBox.setOnCheckedChangeListener(null);
            if (recentCall.isCheck) {
                recentCallView.mCheckBox.setChecked(true);
            } else {
                recentCallView.mCheckBox.setChecked(false);
            }
            if (ContactsRecentActivity.this.isSelectAll) {
                recentCallView.mCheckBox.setChecked(true);
            }
            if (!ContactsRecentActivity.this.isVisitedFromCallingCard) {
                if (ContactsSelectActivity.mSelectedNumberOfContacts.contains(recentCall.number)) {
                    recentCallView.mCheckBox.setChecked(true);
                }
            } else if (AbstractContactsListActivity.cacheCheckNames.contains(recentCall.name)) {
                recentCallView.mCheckBox.setChecked(true);
            } else {
                recentCallView.mCheckBox.setChecked(false);
            }
            recentCallView.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (!ContactsRecentActivity.this.isVisitedFromCallingCard) {
                        if (z) {
                            if (!ContactsSelectActivity.mSelectedNumberOfContacts.contains(recentCall.number)) {
                                ContactsRecentActivity.cacheCheckNums.add(recentCall.number);
                                ContactsSelectActivity.mSelectedNumberOfContacts.add(recentCall.number);
                            }
                            boolean unused = recentCall.isCheck = true;
                        } else {
                            ContactsRecentActivity.cacheCheckNums.remove(recentCall.number);
                            ContactsSelectActivity.mSelectedNumberOfContacts.remove(recentCall.number);
                            boolean unused2 = recentCall.isCheck = false;
                        }
                        ContactsListAdapter.this.notifyDataSetChanged();
                        ContactsRecentActivity.this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
                        return;
                    }
                    if (z) {
                        AbstractContactsListActivity.cacheCheckNames.add(recentCall.name);
                        boolean unused3 = recentCall.isCheck = true;
                    } else {
                        AbstractContactsListActivity.cacheCheckNames.remove(recentCall.name);
                        boolean unused4 = recentCall.isCheck = false;
                    }
                    ContactsListAdapter.this.notifyDataSetChanged();
                    ContactsRecentActivity.this.mCountView.setText("(" + AbstractContactsListActivity.cacheCheckNames.size() + ")");
                }
            });
            return view2;
        }
    }

    private class ContactsListQueryHandler extends AsyncQueryHandler {
        public ContactsListQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: CFG modification limit reached, blocks count: 133 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onQueryComplete(int r6, java.lang.Object r7, android.database.Cursor r8) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                switch(r6) {
                    case 0: goto L_0x0020;
                    case 1: goto L_0x0099;
                    default: goto L_0x0005;
                }
            L_0x0005:
                return
            L_0x0006:
                vc.lx.sms.ui.ContactsRecentActivity r2 = vc.lx.sms.ui.ContactsRecentActivity.this
                boolean r2 = r2.isVisitedFromCallingCard
                if (r2 == 0) goto L_0x0047
                if (r1 == 0) goto L_0x0020
                int r2 = r1.length()
                if (r2 <= 0) goto L_0x0020
                java.lang.String unused = r0.name = r1
                vc.lx.sms.ui.ContactsRecentActivity r1 = vc.lx.sms.ui.ContactsRecentActivity.this
                java.util.List r1 = r1.recentCalls
                r1.add(r0)
            L_0x0020:
                if (r8 == 0) goto L_0x007f
                boolean r0 = r8.moveToNext()
                if (r0 == 0) goto L_0x007f
                vc.lx.sms.ui.ContactsRecentActivity$RecentCall r0 = new vc.lx.sms.ui.ContactsRecentActivity$RecentCall
                vc.lx.sms.ui.ContactsRecentActivity r1 = vc.lx.sms.ui.ContactsRecentActivity.this
                r0.<init>()
                r1 = 3
                java.lang.String r1 = r8.getString(r1)
                if (r1 == 0) goto L_0x003e
                java.lang.String r2 = ""
                boolean r2 = r2.equals(r1)
                if (r2 == 0) goto L_0x0006
            L_0x003e:
                vc.lx.sms.ui.ContactsRecentActivity r1 = vc.lx.sms.ui.ContactsRecentActivity.this
                r2 = 2131296685(0x7f0901ad, float:1.8211294E38)
                java.lang.String r1 = r1.getString(r2)
            L_0x0047:
                java.lang.String unused = r0.name = r1
                java.lang.String r2 = r8.getString(r4)
                java.lang.String unused = r0.number = r2
                r2 = 2
                int r2 = r8.getInt(r2)
                int unused = r0.type = r2
                vc.lx.sms.ui.ContactsRecentActivity r2 = vc.lx.sms.ui.ContactsRecentActivity.this
                java.util.List r2 = r2.nameList
                boolean r2 = r2.contains(r1)
                if (r2 != 0) goto L_0x0020
                vc.lx.sms.ui.ContactsRecentActivity r2 = vc.lx.sms.ui.ContactsRecentActivity.this
                java.util.List r2 = r2.nameList
                r2.add(r1)
                vc.lx.sms.ui.ContactsRecentActivity r2 = vc.lx.sms.ui.ContactsRecentActivity.this
                java.util.List r2 = r2.recentCalls
                r2.add(r0)
                vc.lx.sms.ui.ContactsRecentActivity r0 = vc.lx.sms.ui.ContactsRecentActivity.this
                java.util.List<java.lang.String> r0 = r0.cache
                r0.add(r1)
                goto L_0x0020
            L_0x007f:
                vc.lx.sms.ui.ContactsRecentActivity r0 = vc.lx.sms.ui.ContactsRecentActivity.this
                r0.mReady = r3
                vc.lx.sms.ui.ContactsRecentActivity r0 = vc.lx.sms.ui.ContactsRecentActivity.this
                vc.lx.sms.ui.ContactsRecentActivity$ContactsListAdapter r0 = r0.mContactsListAdapter
                vc.lx.sms.ui.ContactsRecentActivity r1 = vc.lx.sms.ui.ContactsRecentActivity.this
                java.util.List r1 = r1.recentCalls
                r0.changeData(r1)
                vc.lx.sms.ui.ContactsRecentActivity r0 = vc.lx.sms.ui.ContactsRecentActivity.this
                r0.setProgressBarIndeterminateVisibility(r3)
                goto L_0x0005
            L_0x0099:
                java.util.HashSet<java.lang.String> r0 = vc.lx.sms.ui.ContactsSelectActivity.mSelectedNumberOfContacts
                r0.clear()
            L_0x009e:
                boolean r0 = r8.moveToNext()
                if (r0 == 0) goto L_0x00ae
                java.util.HashSet<java.lang.String> r0 = vc.lx.sms.ui.ContactsSelectActivity.mSelectedNumberOfContacts
                java.lang.String r1 = r8.getString(r4)
                r0.add(r1)
                goto L_0x009e
            L_0x00ae:
                vc.lx.sms.ui.ContactsRecentActivity r0 = vc.lx.sms.ui.ContactsRecentActivity.this
                r0.setProgressBarIndeterminateVisibility(r3)
                goto L_0x0005
            */
            throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ContactsRecentActivity.ContactsListQueryHandler.onQueryComplete(int, java.lang.Object, android.database.Cursor):void");
        }
    }

    class RecentCall {
        /* access modifiers changed from: private */
        public boolean isCheck = false;
        /* access modifiers changed from: private */
        public String name;
        /* access modifiers changed from: private */
        public String number;
        /* access modifiers changed from: private */
        public int type;

        RecentCall() {
        }
    }

    class RecentCallView {
        public CheckBox mCheckBox = null;
        public TextView mNameView = null;
        public TextView mNumView = null;
        public TextView mTypeView = null;

        RecentCallView() {
        }
    }

    public void MenuCacelModel() {
        int i = 0;
        this.isSelectAll = false;
        while (true) {
            int i2 = i;
            if (i2 < this.recentCalls.size()) {
                if (!(this.recentCalls.get(i2) == null || this.recentCalls.get(i2).name == null)) {
                    ContactsSelectActivity.mSelectedNumberOfContacts.remove(this.recentCalls.get(i2).number);
                }
                i = i2 + 1;
            } else {
                this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
                this.mContactsListAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    public void MenuMulitModel() {
        this.isSelectAll = true;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.recentCalls.size()) {
                if (!(this.recentCalls.get(i2) == null || this.recentCalls.get(i2).name == null)) {
                    ContactsSelectActivity.mSelectedNumberOfContacts.add(this.recentCalls.get(i2).number);
                }
                i = i2 + 1;
            } else {
                this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
                this.mContactsListAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    public List<String> getCache() {
        return this.cache;
    }

    public void initDatas(Cursor cursor) {
    }

    public void initViews() {
        if (getIntent() != null) {
            this.isVisitedFromCallingCard = getIntent().getBooleanExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, false);
        }
        this.mInflater = LayoutInflater.from(this);
        cacheCheckNums.clear();
        this.cache.clear();
        this.mContactsListQueryHandler = new ContactsListQueryHandler(getContentResolver());
        this.mContactsListQueryHandler.startQuery(0, null, CallLog.Calls.CONTENT_URI, MCALLPROJECTIONSTRINGS, "", null, "date DESC");
        this.mContactsListAdapter = new ContactsListAdapter(this, null);
        setListAdapter(this.mContactsListAdapter);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok /*2131492968*/:
            case R.id.btn_send_invitation /*2131492972*/:
                if (this.isVisitedFromCallingCard) {
                    fillData();
                }
                finish();
                return;
            case R.id.selCount /*2131492969*/:
            case R.id.invitation_action_bar /*2131492971*/:
            default:
                return;
            case R.id.back /*2131492970*/:
                ContactsSelectActivity.mSelectedNumberOfContacts.clear();
                finish();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
        if (this.cache != null && this.cache.size() > 0) {
            this.mReady = false;
        }
    }
}
