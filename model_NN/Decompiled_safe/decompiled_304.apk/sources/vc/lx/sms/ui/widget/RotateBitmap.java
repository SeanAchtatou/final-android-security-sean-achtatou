package vc.lx.sms.ui.widget;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class RotateBitmap {
    public static final String TAG = "RotateBitmap";
    private Bitmap mBitmap;
    private int mRotation;

    public RotateBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
        this.mRotation = 0;
    }

    public RotateBitmap(Bitmap bitmap, int i) {
        this.mBitmap = bitmap;
        this.mRotation = i % 360;
    }

    public Bitmap getBitmap() {
        return this.mBitmap;
    }

    public int getHeight() {
        return isOrientationChanged() ? this.mBitmap.getWidth() : this.mBitmap.getHeight();
    }

    public Matrix getRotateMatrix() {
        Matrix matrix = new Matrix();
        if (this.mRotation != 0) {
            matrix.preTranslate((float) (-(this.mBitmap.getWidth() / 2)), (float) (-(this.mBitmap.getHeight() / 2)));
            matrix.postRotate((float) this.mRotation);
            matrix.postTranslate((float) (getWidth() / 2), (float) (getHeight() / 2));
        }
        return matrix;
    }

    public int getRotation() {
        return this.mRotation;
    }

    public int getWidth() {
        return isOrientationChanged() ? this.mBitmap.getHeight() : this.mBitmap.getWidth();
    }

    public boolean isOrientationChanged() {
        return (this.mRotation / 90) % 2 != 0;
    }

    public void recycle() {
        if (this.mBitmap != null) {
            this.mBitmap.recycle();
            this.mBitmap = null;
        }
    }

    public void setBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
    }

    public void setRotation(int i) {
        this.mRotation = i;
    }
}
