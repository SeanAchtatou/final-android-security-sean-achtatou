package vc.lx.sms.ui.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import java.util.ArrayList;
import java.util.List;

public abstract class PopupMenusWindow extends PopupWindow {
    private static final int MEASURE_AND_LAYOUT_DONE = 2;
    private Context mContext;
    private boolean mDismissOnClick;
    private boolean mIsDirty;
    private OnMenuItemClickListener mOnMenuItemClickListener;
    private ArrayList<PopupMenuItem> mPopupMenuItems = new ArrayList<>();
    private int mPopupY;
    private int mPrivateFlags;
    private int mScreenHeight;
    private int mScreenWidth;

    public interface OnMenuItemClickListener {
        void onMenuItemClicked(PopupMenusWindow popupMenusWindow, int i);
    }

    public PopupMenusWindow(Context context) {
        super(context);
        this.mContext = context;
        this.mDismissOnClick = true;
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(true);
        setWidth(-2);
        setHeight(-2);
        WindowManager windowManager = (WindowManager) this.mContext.getSystemService("window");
        this.mScreenWidth = windowManager.getDefaultDisplay().getWidth();
        this.mScreenHeight = windowManager.getDefaultDisplay().getHeight();
    }

    public void addPopupMenuItem(PopupMenuItem popupMenuItem) {
        if (popupMenuItem != null) {
            this.mPopupMenuItems.add(popupMenuItem);
            this.mIsDirty = true;
        }
    }

    public abstract void afterShow();

    public void clearAllPopupMenuItems() {
        if (!this.mPopupMenuItems.isEmpty()) {
            this.mPopupMenuItems.clear();
            this.mIsDirty = true;
        }
    }

    /* access modifiers changed from: protected */
    public void clearPopupMenuItems() {
        if (!this.mPopupMenuItems.isEmpty()) {
            onClearPopupMenuItems();
        }
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.mContext;
    }

    public boolean getDismissOnClick() {
        return this.mDismissOnClick;
    }

    public PopupMenuItem getMenuItem(int i) {
        if (this.mPopupMenuItems == null || this.mPopupMenuItems.size() == 0) {
            return null;
        }
        return this.mPopupMenuItems.get(i);
    }

    /* access modifiers changed from: protected */
    public OnMenuItemClickListener getOnMenuItemClickListener() {
        return this.mOnMenuItemClickListener;
    }

    /* access modifiers changed from: protected */
    public int getScreenHeight() {
        return this.mScreenHeight;
    }

    /* access modifiers changed from: protected */
    public int getScreenWidth() {
        return this.mScreenWidth;
    }

    /* access modifiers changed from: protected */
    public void onClearPopupMenuItems() {
    }

    /* access modifiers changed from: protected */
    public abstract void onMeasureAndLayout(View view);

    /* access modifiers changed from: protected */
    public abstract void populatePopupMenuItems(List<PopupMenuItem> list);

    /* access modifiers changed from: protected */
    public abstract void prepareAnimationStyle();

    public void setContentView(int i) {
        setContentView(LayoutInflater.from(this.mContext).inflate(i, (ViewGroup) null));
    }

    public void setDismissOnClick(boolean z) {
        this.mDismissOnClick = z;
    }

    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.mOnMenuItemClickListener = onMenuItemClickListener;
    }

    /* access modifiers changed from: protected */
    public void setWidgetSpecs(int i) {
        this.mPopupY = i;
        this.mPrivateFlags |= 2;
    }

    public void show(View view) {
        View contentView = getContentView();
        if (contentView == null) {
            throw new IllegalStateException("You need to set the content view using the setContentView method");
        }
        setBackgroundDrawable(new ColorDrawable(0));
        if (this.mIsDirty) {
            clearPopupMenuItems();
            populatePopupMenuItems(this.mPopupMenuItems);
        }
        onMeasureAndLayout(contentView);
        if ((this.mPrivateFlags & 2) != 2) {
            throw new IllegalStateException("onMeasureAndLayout() did not set the widget specification by calling setWidgetSpecs()");
        }
        prepareAnimationStyle();
        showAtLocation(view, 80, 0, this.mPopupY);
    }
}
