package vc.lx.sms.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import com.google.android.mms.pdu.PduHeaders;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VoteResultView extends View {
    private boolean animMode = true;
    /* access modifiers changed from: private */
    public boolean display = true;
    private String displayMode = "%";
    private float endHeight = 10.0f;
    private Paint font_Paint;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what != 1 || VoteResultView.this.indexSize >= VoteResultView.this.maxSize) {
                boolean unused = VoteResultView.this.display = false;
            } else {
                VoteResultView.access$218(VoteResultView.this, 3.5d);
                VoteResultView.access$012(VoteResultView.this, 1);
            }
            VoteResultView.this.invalidate();
        }
    };
    /* access modifiers changed from: private */
    public int indexSize = 0;
    /* access modifiers changed from: private */
    public int maxSize = 43;
    private boolean mode = false;
    private int numWidth = 9;
    private Paint paint;
    private List<Paint> paints = new ArrayList();
    private float startHeight = 170.0f;
    private Thread thread = new Thread() {
        public void run() {
            while (!Thread.currentThread().isInterrupted() && VoteResultView.this.display) {
                Message message = new Message();
                message.what = 1;
                VoteResultView.this.handler.sendMessage(message);
                try {
                    Thread.sleep(15);
                } catch (InterruptedException e) {
                    System.err.println("InterruptedException���������������");
                    interrupt();
                }
            }
        }
    };
    private int viewWidth = 20;

    public VoteResultView(Context context, int i, String str) {
        super(context);
        this.maxSize = i;
        this.displayMode = str;
        init();
    }

    public VoteResultView(Context context, int i, String str, boolean z) {
        super(context);
        this.maxSize = i;
        this.displayMode = str;
        this.mode = z;
        init();
    }

    public VoteResultView(Context context, int i, String str, boolean z, boolean z2) {
        super(context);
        this.maxSize = i;
        this.displayMode = str;
        this.mode = z;
        this.animMode = z2;
        init();
    }

    static /* synthetic */ int access$012(VoteResultView voteResultView, int i) {
        int i2 = voteResultView.indexSize + i;
        voteResultView.indexSize = i2;
        return i2;
    }

    static /* synthetic */ float access$218(VoteResultView voteResultView, double d) {
        float f = (float) (((double) voteResultView.endHeight) + d);
        voteResultView.endHeight = f;
        return f;
    }

    private void init() {
        initPaints();
        this.paint = this.paints.get(new Random().nextInt(this.paints.size() - 1));
        this.font_Paint = new Paint();
        this.font_Paint.setARGB(255, 66, 66, 66);
        this.numWidth = 9;
        if (this.maxSize < 10) {
            this.numWidth = 15;
        } else if (this.maxSize < 100) {
            this.numWidth = 12;
        }
        if (this.animMode) {
            this.thread.start();
            return;
        }
        this.display = false;
        this.indexSize = this.maxSize;
        this.endHeight = this.startHeight - ((float) (((double) this.maxSize) * 1.5d));
        invalidate();
    }

    public void SetResultView(Context context, int i, String str, boolean z) {
        this.maxSize = i;
        this.displayMode = str;
        this.mode = z;
        init();
    }

    public void initPaints() {
        Paint paint2 = new Paint();
        paint2.setColor(Color.rgb(16, (int) PduHeaders.MESSAGE_COUNT, (int) PduHeaders.APPLIC_ID));
        this.paints.add(paint2);
        Paint paint3 = new Paint();
        paint3.setColor(Color.rgb(0, 99, (int) PduHeaders.ADDITIONAL_HEADERS));
        this.paints.add(paint3);
        Paint paint4 = new Paint();
        paint4.setColor(Color.rgb(136, (int) PduHeaders.RECOMMENDED_RETRIEVAL_MODE_TEXT, 22));
        this.paints.add(paint4);
        Paint paint5 = new Paint();
        paint5.setColor(Color.rgb(238, (int) PduHeaders.APPLIC_ID, 0));
        this.paints.add(paint5);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.i("tag", this.endHeight + "," + this.startHeight + "," + this.viewWidth);
        canvas.drawRect(10.0f, 10.0f, this.endHeight, 30.0f, this.paint);
        if (!this.display) {
            canvas.drawRect(10.0f, 10.0f, this.endHeight, 30.0f, this.paint);
        }
        canvas.drawText(this.displayMode, 0.0f, this.startHeight + 15.0f, this.font_Paint);
    }

    public void toInvalidate() {
        invalidate();
    }
}
