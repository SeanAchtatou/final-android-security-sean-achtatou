package vc.lx.sms.ui;

import android.content.Context;
import android.text.method.HideReturnsTransformationMethod;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import vc.lx.sms.cmcc.http.data.SongItem;

public class SmsParserWidget extends RelativeLayout {
    private Context context = null;
    private Button downloadBtn = null;
    private ImageView imageView = null;
    private LayoutInflater inflater = null;
    private TextView linkView = null;
    private Button playBtn = null;
    private Button ringRingBtn = null;
    private Button ringStoneBtn = null;
    private TextView songInfoView = null;
    private SongItem songItem = null;
    private int type;

    public SmsParserWidget(Context context2) {
        super(context2, null);
        this.context = context2;
    }

    public SmsParserWidget(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet, 0);
        this.context = context2;
    }

    public SmsParserWidget(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
    }

    public SongItem getSongItem() {
        return this.songItem;
    }

    public int getType() {
        return this.type;
    }

    public URLSpan[] getUrls() {
        return this.linkView.getUrls();
    }

    public void setBodyText(CharSequence charSequence) {
        if (this.songInfoView != null) {
            this.songInfoView.setText(charSequence);
        } else if (this.linkView != null) {
            this.linkView.setText(charSequence);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.downloadBtn.setOnClickListener(onClickListener);
        this.ringRingBtn.setOnClickListener(onClickListener);
        this.ringStoneBtn.setOnClickListener(onClickListener);
    }

    public void setSongItem(SongItem songItem2) {
        this.songItem = songItem2;
    }

    public void setTransformationMethod(HideReturnsTransformationMethod hideReturnsTransformationMethod) {
        this.linkView.setTransformationMethod(hideReturnsTransformationMethod);
    }

    public void setType(int i) {
        this.type = i;
    }
}
