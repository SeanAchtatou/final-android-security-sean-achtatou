package vc.lx.sms.ui;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.android.provider.DrmStore;
import com.google.android.mms.ContentType;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Observable;
import java.util.Observer;
import vc.lx.sms.caches.RemoteResourceManager;
import vc.lx.sms.ui.widget.ImageViewTouchBase;
import vc.lx.sms.ui.widget.RotateBitmap;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ImageDetailActivity extends AbstractFlurryActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public GestureDetector mGestureDetector;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    ImageViewTouchBase mImage;
    String mPath;
    private RemoteResourceManagerObserver mResourcesObserver;
    /* access modifiers changed from: private */
    public RemoteResourceManager mRrm;
    /* access modifiers changed from: private */
    public Button mSaveBtn;
    private Button mZoomInBtn;
    private Button mZoomOutBtn;

    class ImageGestureDetector extends GestureDetector.SimpleOnGestureListener {
        ImageGestureDetector() {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            ImageViewTouchBase imageViewTouchBase = ImageDetailActivity.this.mImage;
            if (imageViewTouchBase.getScale() <= 1.0f) {
                return true;
            }
            imageViewTouchBase.postTranslateCenter(-f, -f2);
            return true;
        }
    }

    private class RemoteResourceManagerObserver implements Observer {
        private RemoteResourceManagerObserver() {
        }

        public void update(Observable observable, Object obj) {
            ImageDetailActivity.this.mHandler.post(new Runnable() {
                /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
                    return;
                 */
                /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r4 = this;
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this
                        vc.lx.sms.ui.ImageDetailActivity r0 = vc.lx.sms.ui.ImageDetailActivity.this
                        java.lang.String r0 = r0.mPath
                        android.net.Uri r0 = android.net.Uri.parse(r0)
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r1 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r1 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.caches.RemoteResourceManager r1 = r1.mRrm     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        java.io.InputStream r0 = r1.getInputStream(r0)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r1 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r1 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.widget.ImageViewTouchBase r1 = r1.mImage     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.widget.RotateBitmap r2 = new vc.lx.sms.ui.widget.RotateBitmap     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r2.<init>(r0)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r0 = 1
                        r1.setImageRotateBitmapResetBase(r2, r0)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r0 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.widget.ImageViewTouchBase r0 = r0.mImage     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r0.postInvalidate()     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r0 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r1 = 0
                        r0.setProgressBarIndeterminateVisibility(r1)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r0 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r1 = 2131493051(0x7f0c00bb, float:1.8609571E38)
                        android.view.View r0 = r0.findViewById(r1)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r1 = 0
                        r0.setVisibility(r1)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r0 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        android.widget.Button r0 = r0.mSaveBtn     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r1 = 0
                        r0.setVisibility(r1)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r0 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        android.view.GestureDetector r1 = new android.view.GestureDetector     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$ImageGestureDetector r2 = new vc.lx.sms.ui.ImageDetailActivity$ImageGestureDetector     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r3 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r3 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r2.<init>()     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r1.<init>(r2)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        android.view.GestureDetector unused = r0.mGestureDetector = r1     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity$RemoteResourceManagerObserver r0 = vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        vc.lx.sms.ui.ImageDetailActivity r0 = vc.lx.sms.ui.ImageDetailActivity.this     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r1 = 2131493054(0x7f0c00be, float:1.8609577E38)
                        android.view.View r0 = r0.findViewById(r1)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                        r1 = 8
                        r0.setVisibility(r1)     // Catch:{ IOException -> 0x007d, all -> 0x007b }
                    L_0x007a:
                        return
                    L_0x007b:
                        r0 = move-exception
                        throw r0
                    L_0x007d:
                        r0 = move-exception
                        goto L_0x007a
                    */
                    throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ImageDetailActivity.RemoteResourceManagerObserver.AnonymousClass1.run():void");
                }
            });
        }
    }

    private void saveFile(Bitmap bitmap, String str) {
        ContentValues contentValues = new ContentValues();
        File file = new File(Environment.getExternalStorageDirectory(), "redcloud");
        file.mkdirs();
        contentValues.put("_data", new File(file, str).toString());
        contentValues.put("title", str);
        contentValues.put("date_added", Long.valueOf(System.currentTimeMillis()));
        contentValues.put(DrmStore.Columns.MIME_TYPE, ContentType.IMAGE_PNG);
        try {
            OutputStream openOutputStream = getContentResolver().openOutputStream(getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues));
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, openOutputStream);
            openOutputStream.flush();
            openOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        Toast.makeText(getApplicationContext(), (int) R.string.save_image_to_sdcard, 0).show();
    }

    public void onClick(View view) {
        if (view == this.mZoomInBtn) {
            this.mImage.zoomIn();
        }
        if (view == this.mZoomOutBtn) {
            this.mImage.zoomOut();
        }
        if (view == this.mSaveBtn) {
            Uri parse = Uri.parse(this.mPath);
            String str = "redcloud_" + String.valueOf(System.currentTimeMillis());
            if (this.mPath.endsWith("jpg")) {
                str = str + ".jpg";
            } else if (this.mPath.endsWith("png")) {
                str = str + ".png";
            }
            try {
                saveToSDCard(BitmapFactory.decodeStream(this.mRrm.getInputStream(parse)), str);
            } catch (IOException e) {
                Log.e("redcloud", e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.image_detail);
        this.mZoomInBtn = (Button) findViewById(16908313);
        this.mZoomOutBtn = (Button) findViewById(16908314);
        this.mZoomInBtn.setOnClickListener(this);
        this.mZoomOutBtn.setOnClickListener(this);
        this.mSaveBtn = (Button) findViewById(R.id.save_btn);
        this.mSaveBtn.setOnClickListener(this);
        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ImageDetailActivity.this.finish();
            }
        });
        this.mPath = getIntent().getStringExtra("full_url");
        if (this.mPath != null) {
            this.mImage = (ImageViewTouchBase) findViewById(R.id.large_image);
            this.mRrm = SmsApplication.getInstance().getRemoteResourceManager();
            this.mResourcesObserver = new RemoteResourceManagerObserver();
            this.mRrm.addObserver(this.mResourcesObserver);
            Uri parse = Uri.parse(this.mPath);
            if (!this.mRrm.exists(parse)) {
                findViewById(R.id.image_progress).setVisibility(0);
                findViewById(R.id.zoom_panel).setVisibility(8);
                this.mRrm.request(parse);
                return;
            }
            try {
                this.mImage.setImageRotateBitmapResetBase(new RotateBitmap(BitmapFactory.decodeStream(this.mRrm.getInputStream(parse))), true);
                findViewById(R.id.zoom_panel).setVisibility(0);
                this.mSaveBtn.setVisibility(0);
                this.mGestureDetector = new GestureDetector(new ImageGestureDetector());
            } catch (IOException e) {
                e.printStackTrace();
                this.mRrm.request(parse);
            } finally {
                findViewById(R.id.image_progress).setVisibility(8);
            }
        } else {
            findViewById(R.id.image_progress).setVisibility(8);
            findViewById(R.id.zoom_panel).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (isFinishing()) {
            this.mRrm.deleteObserver(this.mResourcesObserver);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.mGestureDetector != null && this.mGestureDetector.onTouchEvent(motionEvent);
    }

    public void saveToSDCard(Bitmap bitmap, String str) {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            saveFile(bitmap, str);
        }
    }
}
