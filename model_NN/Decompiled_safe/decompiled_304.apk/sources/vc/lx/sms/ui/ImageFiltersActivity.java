package vc.lx.sms.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.apache.http.message.BasicNameValuePair;
import vc.lx.sms.util.ImageUtils;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class ImageFiltersActivity extends AbstractFlurryActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public ImageView mBwBtn;
    /* access modifiers changed from: private */
    public Button mConfirmBtn;
    /* access modifiers changed from: private */
    public Bitmap mEffBitmap;
    /* access modifiers changed from: private */
    public String mFile;
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public boolean mIsAppliedFilter = false;
    /* access modifiers changed from: private */
    public ImageView mLomoBtn;
    /* access modifiers changed from: private */
    public ImageView mOldPhotoBtn;
    /* access modifiers changed from: private */
    public Bitmap mOriginBitmap;
    /* access modifiers changed from: private */
    public ImageView mOriginBtn;
    /* access modifiers changed from: private */
    public ImageView mWhitenBtn;

    /* access modifiers changed from: private */
    public void recycleBitmap(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }

    public void onClick(final View view) {
        new AsyncTask<Void, Void, Bitmap>() {
            /* access modifiers changed from: protected */
            public Bitmap doInBackground(Void... voidArr) {
                if (view == ImageFiltersActivity.this.mLomoBtn) {
                    return ImageUtils.lomo(ImageFiltersActivity.this.mOriginBitmap);
                }
                if (view == ImageFiltersActivity.this.mBwBtn) {
                    return ImageUtils.blackAndWhite(ImageFiltersActivity.this.mOriginBitmap);
                }
                if (view == ImageFiltersActivity.this.mWhitenBtn) {
                    return ImageUtils.whiten(ImageFiltersActivity.this.mOriginBitmap);
                }
                if (view == ImageFiltersActivity.this.mOldPhotoBtn) {
                    return ImageUtils.oldPhoto(ImageFiltersActivity.this.mOriginBitmap, ImageFiltersActivity.this.getApplicationContext());
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Bitmap bitmap) {
                ImageFiltersActivity.this.findViewById(R.id.image_progress).setVisibility(8);
                if (view == ImageFiltersActivity.this.mOriginBtn) {
                    ImageFiltersActivity.this.recycleBitmap(ImageFiltersActivity.this.mEffBitmap);
                    ImageFiltersActivity.this.mImageView.setImageBitmap(ImageFiltersActivity.this.mOriginBitmap);
                    boolean unused = ImageFiltersActivity.this.mIsAppliedFilter = false;
                } else if (view == ImageFiltersActivity.this.mConfirmBtn) {
                    Bitmap access$100 = ImageFiltersActivity.this.mOriginBitmap;
                    if (ImageFiltersActivity.this.mIsAppliedFilter) {
                        access$100 = ImageFiltersActivity.this.mEffBitmap;
                    }
                    try {
                        access$100.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(ImageFiltersActivity.this.mFile));
                        ImageFiltersActivity.this.setResult(-1, new Intent());
                        ImageFiltersActivity.this.finish();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        ImageFiltersActivity.this.setResult(0);
                        ImageFiltersActivity.this.finish();
                    }
                } else {
                    ImageFiltersActivity.this.recycleBitmap(ImageFiltersActivity.this.mEffBitmap);
                    Bitmap unused2 = ImageFiltersActivity.this.mEffBitmap = bitmap;
                    ImageFiltersActivity.this.mImageView.setImageBitmap(ImageFiltersActivity.this.mEffBitmap);
                    boolean unused3 = ImageFiltersActivity.this.mIsAppliedFilter = true;
                }
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                ImageFiltersActivity.this.findViewById(R.id.image_progress).setVisibility(0);
                if (view == ImageFiltersActivity.this.mLomoBtn || view == ImageFiltersActivity.this.mBwBtn || view == ImageFiltersActivity.this.mWhitenBtn || view == ImageFiltersActivity.this.mOldPhotoBtn || view == ImageFiltersActivity.this.mOriginBtn) {
                    ImageFiltersActivity.this.mLomoBtn.setBackgroundColor(17170445);
                    ImageFiltersActivity.this.mBwBtn.setBackgroundColor(17170445);
                    ImageFiltersActivity.this.mWhitenBtn.setBackgroundColor(17170445);
                    ImageFiltersActivity.this.mOldPhotoBtn.setBackgroundColor(17170445);
                    ImageFiltersActivity.this.mOriginBtn.setBackgroundColor(17170445);
                    ((ImageView) view).setBackgroundResource(R.drawable.bg_filter_sample);
                    if (view == ImageFiltersActivity.this.mLomoBtn) {
                        Util.logEvent("image_filter_apply", new BasicNameValuePair("type", "lomo"));
                    }
                    if (view == ImageFiltersActivity.this.mBwBtn) {
                        Util.logEvent("image_filter_apply", new BasicNameValuePair("type", "bw"));
                    }
                    if (view == ImageFiltersActivity.this.mWhitenBtn) {
                        Util.logEvent("image_filter_apply", new BasicNameValuePair("type", "whiten"));
                    }
                    if (view == ImageFiltersActivity.this.mOldPhotoBtn) {
                        Util.logEvent("image_filter_apply", new BasicNameValuePair("type", "oldphoto"));
                    }
                    if (view == ImageFiltersActivity.this.mOriginBtn) {
                        Util.logEvent("image_filter_apply", new BasicNameValuePair("type", "origin"));
                    }
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.image_filters);
        this.mFile = getIntent().getStringExtra("file");
        if (this.mFile == null) {
            finish();
        }
        this.mOriginBitmap = Util.decodeFile(this.mFile);
        this.mImageView = (ImageView) findViewById(R.id.image);
        this.mImageView.setImageBitmap(this.mOriginBitmap);
        this.mLomoBtn = (ImageView) findViewById(R.id.lomo_btn);
        this.mLomoBtn.setOnClickListener(this);
        this.mOriginBtn = (ImageView) findViewById(R.id.origin_btn);
        this.mOriginBtn.setOnClickListener(this);
        this.mBwBtn = (ImageView) findViewById(R.id.bw_btn);
        this.mBwBtn.setOnClickListener(this);
        this.mWhitenBtn = (ImageView) findViewById(R.id.whiten_btn);
        this.mWhitenBtn.setOnClickListener(this);
        this.mOldPhotoBtn = (ImageView) findViewById(R.id.old_photo_btn);
        this.mOldPhotoBtn.setOnClickListener(this);
        this.mConfirmBtn = (Button) findViewById(R.id.confirm_btn);
        this.mConfirmBtn.setOnClickListener(this);
        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ImageFiltersActivity.this.setResult(0);
                ImageFiltersActivity.this.finish();
            }
        });
    }
}
