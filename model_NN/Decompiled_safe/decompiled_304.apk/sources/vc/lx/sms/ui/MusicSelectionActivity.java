package vc.lx.sms.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.db.MusicContentProvider;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public class MusicSelectionActivity extends AbstractFlurryActivity {
    private Handler mHandler;
    protected LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public ListView mListView;
    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            Cursor managedQuery = MusicSelectionActivity.this.managedQuery(MusicContentProvider.CONTENT_URI, null, "_id = ?", new String[]{String.valueOf(j)}, null);
            if (managedQuery != null && managedQuery.moveToNext()) {
                SongItem songItem = new SongItem();
                songItem.song = managedQuery.getString(managedQuery.getColumnIndex("name"));
                songItem.singer = managedQuery.getString(managedQuery.getColumnIndex("singer"));
                songItem.plug = managedQuery.getString(managedQuery.getColumnIndex(SmsSqliteHelper.PLUG));
                songItem.contentid = managedQuery.getString(managedQuery.getColumnIndex(SmsSqliteHelper.CONTENT_ID));
                songItem.groupcode = managedQuery.getString(managedQuery.getColumnIndex(SmsSqliteHelper.GROUP_CODE));
                Intent intent = new Intent();
                intent.putExtra(PrefsUtil.KEY_SONG_ITEM, songItem);
                if (MusicSelectionActivity.this.getParent() == null) {
                    MusicSelectionActivity.this.setResult(-1, intent);
                } else {
                    MusicSelectionActivity.this.getParent().setResult(-1, intent);
                }
                MusicSelectionActivity.this.finish();
            }
        }
    };
    /* access modifiers changed from: private */
    public String mSelectionType;
    /* access modifiers changed from: private */
    public SongsAdapter mSongsAdapter;

    private class SongsAdapter extends CursorAdapter {
        public SongsAdapter(Context context, Cursor cursor) {
            super(context, cursor);
        }

        public void bindView(View view, Context context, Cursor cursor) {
            ((TextView) view).setText(TextUtils.join(" - ", new String[]{cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("singer"))}));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return MusicSelectionActivity.this.mInflater.inflate((int) R.layout.music_selection_list_item, viewGroup, false);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.music_selection);
        this.mInflater = LayoutInflater.from(this);
        this.mSelectionType = getIntent().getStringExtra(PrefsUtil.PREF_MUSIC_SELECTION_NAME);
        this.mHandler = new Handler();
        this.mListView = (ListView) findViewById(16908298);
        this.mListView.setOnItemClickListener(this.mOnItemClickListener);
        this.mHandler.post(new Runnable() {
            public void run() {
                SongsAdapter unused = MusicSelectionActivity.this.mSongsAdapter = new SongsAdapter(MusicSelectionActivity.this.getApplicationContext(), MusicSelectionActivity.this.managedQuery(MusicContentProvider.CONTENT_URI, null, MusicSelectionTabActivity.SELECTION_FAVORITE.equals(MusicSelectionActivity.this.mSelectionType) ? "name is not null" + " and favoratestatus = \"yes\"" : "name is not null", null, null));
                MusicSelectionActivity.this.mListView.setAdapter((ListAdapter) MusicSelectionActivity.this.mSongsAdapter);
            }
        });
    }
}
