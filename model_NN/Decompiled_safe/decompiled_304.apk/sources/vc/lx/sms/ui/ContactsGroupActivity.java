package vc.lx.sms.ui;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import vc.lx.sms.util.SmsConstants;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class ContactsGroupActivity extends AbstractContactsListActivity implements ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener {
    protected static final int COLUMN_COUNT = 2;
    protected static final int COLUMN_ID = 0;
    protected static final int COLUMN_NAME = 1;
    protected static final int CONTACTS_QUERY_TOKEN = 0;
    protected static final String[] GROUPSTRINGS = {"_id", "title", "system_id", "sourceid", "summ_count"};
    public static final String RAW_CONTACTS_WHERE = "data1=? and mimetype='vnd.android.cursor.item/group_membership'";
    public static final String[] RAW_PROJECTION = {"raw_contact_id", "display_name", "_id", "data1", "data2"};
    protected static final int SYSTEM_ID = 2;
    protected List<Contact> childData = null;
    protected List<Group> groupData = new ArrayList();
    private ExpandableListView mExpandableListView;
    private MyExpandableListAdapter myExpandableListAdapter;

    class Contact {
        public boolean isCheck;
        public String name;
        public String number;
        public int type;

        Contact() {
        }
    }

    class ExpandableListHolder {
        CheckBox appCheckBox;
        TextView appCount;
        TextView appName;
        TextView appNum;

        ExpandableListHolder() {
        }
    }

    class Group {
        List<Contact> contacts = new ArrayList();
        String count;
        boolean isCheck = false;
        String name;

        Group() {
        }
    }

    public class MyExpandableListAdapter extends BaseExpandableListAdapter {
        protected LayoutInflater mChildInflater;
        protected LayoutInflater mGroupInflater;
        protected List<Group> mGroups;

        public MyExpandableListAdapter(List<Group> list) {
            this.mChildInflater = (LayoutInflater) ContactsGroupActivity.this.getApplicationContext().getSystemService("layout_inflater");
            this.mGroupInflater = (LayoutInflater) ContactsGroupActivity.this.getApplicationContext().getSystemService("layout_inflater");
            this.mGroups = list;
        }

        public Object getChild(int i, int i2) {
            return this.mGroups.get(i).contacts.get(i2);
        }

        public long getChildId(int i, int i2) {
            return (long) i2;
        }

        public View getChildView(final int i, final int i2, boolean z, View view, ViewGroup viewGroup) {
            ExpandableListHolder expandableListHolder;
            View view2;
            if (view == null) {
                expandableListHolder = new ExpandableListHolder();
                if (!ContactsGroupActivity.this.isVisitedFromCallingCard) {
                    view2 = this.mChildInflater.inflate((int) R.layout.contact_child_item, (ViewGroup) null);
                    expandableListHolder.appNum = (TextView) view2.findViewById(R.id.address);
                } else {
                    view2 = this.mChildInflater.inflate((int) R.layout.contact_item_calling_card, (ViewGroup) null);
                }
                expandableListHolder.appName = (TextView) view2.findViewById(R.id.contact_name);
                expandableListHolder.appCheckBox = (CheckBox) view2.findViewById(16908289);
                view2.setTag(expandableListHolder);
            } else {
                expandableListHolder = (ExpandableListHolder) view.getTag();
                view2 = view;
            }
            expandableListHolder.appName.setText(this.mGroups.get(i).contacts.get(i2).name);
            if (!ContactsGroupActivity.this.isVisitedFromCallingCard) {
                expandableListHolder.appNum.setText(this.mGroups.get(i).contacts.get(i2).number);
            }
            expandableListHolder.appCheckBox.setOnCheckedChangeListener(null);
            if (this.mGroups.get(i).contacts.get(i2).isCheck) {
                expandableListHolder.appCheckBox.setChecked(true);
            } else {
                expandableListHolder.appCheckBox.setChecked(false);
            }
            expandableListHolder.appCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    Log.i("TAG", "groupPosition=" + i + ",childPos=" + i2 + "value=" + MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).number);
                    if (!ContactsGroupActivity.this.isVisitedFromCallingCard) {
                        if (z) {
                            MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).isCheck = true;
                            ContactsSelectActivity.mSelectedNumberOfContacts.add(MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).number == null ? "" : MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).number);
                        } else {
                            MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).isCheck = false;
                            if (MyExpandableListAdapter.this.mGroups.get(i).isCheck) {
                                MyExpandableListAdapter.this.mGroups.get(i).isCheck = false;
                            }
                            ContactsSelectActivity.mSelectedNumberOfContacts.remove(MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).number);
                        }
                        ContactsGroupActivity.this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
                    } else {
                        if (z) {
                            AbstractContactsListActivity.cacheCheckNames.add(MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).name);
                            MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).isCheck = true;
                        } else {
                            AbstractContactsListActivity.cacheCheckNames.remove(MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).name);
                            MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i2).isCheck = false;
                        }
                        ContactsGroupActivity.this.mCountView.setText("(" + AbstractContactsListActivity.cacheCheckNames.size() + ")");
                    }
                    MyExpandableListAdapter.this.notifyDataSetChanged();
                }
            });
            return view2;
        }

        public int getChildrenCount(int i) {
            return this.mGroups.get(i).contacts.size();
        }

        public Object getGroup(int i) {
            return this.mGroups.get(i);
        }

        public int getGroupCount() {
            return this.mGroups.size();
        }

        public long getGroupId(int i) {
            return (long) i;
        }

        public View getGroupView(final int i, boolean z, View view, ViewGroup viewGroup) {
            ExpandableListHolder expandableListHolder;
            View view2;
            if (view == null) {
                View inflate = this.mGroupInflater.inflate((int) R.layout.contact_group_item, (ViewGroup) null);
                ExpandableListHolder expandableListHolder2 = new ExpandableListHolder();
                expandableListHolder2.appName = (TextView) inflate.findViewById(R.id.group_name);
                expandableListHolder2.appCount = (TextView) inflate.findViewById(R.id.group_count);
                expandableListHolder2.appCheckBox = (CheckBox) inflate.findViewById(R.id.groupcheckbox);
                inflate.setTag(expandableListHolder2);
                ExpandableListHolder expandableListHolder3 = expandableListHolder2;
                view2 = inflate;
                expandableListHolder = expandableListHolder3;
            } else {
                expandableListHolder = (ExpandableListHolder) view.getTag();
                view2 = view;
            }
            expandableListHolder.appName.setText(this.mGroups.get(i).name);
            expandableListHolder.appCount.setText("(" + this.mGroups.get(i).count + ")");
            expandableListHolder.appCheckBox.setOnCheckedChangeListener(null);
            if (this.mGroups.get(i).isCheck) {
                expandableListHolder.appCheckBox.setChecked(true);
            } else {
                expandableListHolder.appCheckBox.setChecked(false);
            }
            expandableListHolder.appCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < MyExpandableListAdapter.this.mGroups.get(i).contacts.size(); i++) {
                        if (z) {
                            MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i).isCheck = true;
                        } else {
                            MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i).isCheck = false;
                        }
                        arrayList.add(MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i).number == null ? "" : MyExpandableListAdapter.this.mGroups.get(i).contacts.get(i).number);
                    }
                    if (z) {
                        MyExpandableListAdapter.this.mGroups.get(i).isCheck = true;
                        ContactsSelectActivity.mSelectedNumberOfContacts.addAll(arrayList);
                    } else {
                        MyExpandableListAdapter.this.mGroups.get(i).isCheck = false;
                        ContactsSelectActivity.mSelectedNumberOfContacts.removeAll(arrayList);
                    }
                    ContactsGroupActivity.this.mCountView.setText("(" + ContactsSelectActivity.mSelectedNumberOfContacts.size() + ")");
                    MyExpandableListAdapter.this.notifyDataSetChanged();
                }
            });
            return view2;
        }

        public boolean hasStableIds() {
            return false;
        }

        public boolean isChildSelectable(int i, int i2) {
            return true;
        }
    }

    private void queryChildData(Cursor cursor) {
        while (cursor.moveToNext()) {
            Group group = new Group();
            String string = cursor.getString(cursor.getColumnIndex("_id"));
            String string2 = cursor.getString(cursor.getColumnIndex("summ_count"));
            group.name = cursor.getString(1);
            group.count = string2;
            Cursor query = getContentResolver().query(ContactsContract.Data.CONTENT_URI, RAW_PROJECTION, RAW_CONTACTS_WHERE, new String[]{"" + string}, "data1 asc");
            if (query != null) {
                this.childData = new ArrayList();
                while (query.moveToNext()) {
                    Contact contact = new Contact();
                    contact.name = query.getString(query.getColumnIndex("display_name"));
                    if (!this.isVisitedFromCallingCard) {
                        query.getInt(query.getColumnIndex("raw_contact_id"));
                        List<String> queryPhoneNumbers = queryPhoneNumbers(contact.name);
                        if (queryPhoneNumbers.size() == 1) {
                            contact.number = queryPhoneNumbers.get(0);
                        } else if (queryPhoneNumbers.size() == 2) {
                            contact.number = queryPhoneNumbers.get(0);
                            this.childData.add(contact);
                            contact.number = queryPhoneNumbers.get(1);
                        }
                        this.childData.add(contact);
                    } else if (contact.name != null && contact.name.length() > 0) {
                        this.childData.add(contact);
                    }
                }
                if (query != null) {
                    query.close();
                }
            }
            group.contacts.addAll(this.childData);
            this.groupData.add(group);
        }
        if (cursor != null) {
            cursor.close();
        }
    }

    public void MenuCacelModel() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.groupData.size()) {
                this.groupData.get(i2).isCheck = true;
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void MenuMulitModel() {
    }

    public List<String> getCache() {
        return null;
    }

    public void initDatas(Cursor cursor) {
        if (cursor != null) {
            queryChildData(cursor);
        }
        this.myExpandableListAdapter = new MyExpandableListAdapter(this.groupData);
        this.mExpandableListView.setAdapter(this.myExpandableListAdapter);
        setProgressBarIndeterminateVisibility(false);
    }

    public void initViews() {
        if (getIntent() != null) {
            this.isVisitedFromCallingCard = getIntent().getBooleanExtra(SmsConstants.IS_VISIT_FROM_CALLING_CARD, false);
        }
        getListView().setVisibility(8);
        this.mExpandableListView = (ExpandableListView) findViewById(R.id.group_list);
        this.mExpandableListView.setVisibility(0);
        this.mCountView = (TextView) findViewById(R.id.selCount);
        if (this.groupData.size() <= 0) {
            startQuery();
        }
        this.myExpandableListAdapter = new MyExpandableListAdapter(this.groupData);
        this.mExpandableListView.setAdapter(this.myExpandableListAdapter);
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        Toast.makeText(getApplicationContext(), "show", 1).show();
        return false;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok /*2131492968*/:
            case R.id.btn_send_invitation /*2131492972*/:
                if (this.isVisitedFromCallingCard) {
                    fillData();
                }
                finish();
                return;
            case R.id.selCount /*2131492969*/:
            case R.id.invitation_action_bar /*2131492971*/:
            default:
                return;
            case R.id.back /*2131492970*/:
                ContactsSelectActivity.mSelectedNumberOfContacts.clear();
                finish();
                return;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long j) {
        Toast.makeText(getApplicationContext(), "show", 1).show();
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> queryPhoneNumbers(String str) {
        HashMap<String, String> hashMap = SmsApplication.getInstance().getmAllNumAndNameCache();
        ArrayList arrayList = new ArrayList();
        for (String next : hashMap.keySet()) {
            String str2 = hashMap.get(next);
            if (str != null && str.equals(str2)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
