package vc.lx.sms.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageButton;
import vc.lx.sms2.R;

public class ImageTextButton extends ImageButton {
    private String count = "";
    private Bitmap image = null;
    private int textSize = 16;

    public ImageTextButton(Context context) {
        super(context);
        initRes(context);
    }

    public ImageTextButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initRes(context);
    }

    public ImageTextButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initRes(context);
    }

    private void initRes(Context context) {
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.app_is_commend);
    }

    public int getTextSize() {
        return this.textSize;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setTextSize((float) this.textSize);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.create(this.count, 1));
        canvas.drawBitmap(this.image, 0.0f, 0.0f, (Paint) null);
        canvas.drawText(this.count, (float) (this.image.getWidth() / 2), (float) ((this.image.getHeight() / 2) + 3), paint);
        super.onDraw(canvas);
    }

    public void setCount(String str) {
        this.count = str;
    }

    public void setTextSize(int i) {
        this.textSize = i;
    }
}
