package vc.lx.sms.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import vc.lx.sms2.R;

public class ComposeContextDialog extends Dialog {
    Context mContext;
    ListView mListView;
    ComposeMessageActivity mParentActivity;

    public ComposeContextDialog(Context context, ComposeMessageActivity composeMessageActivity) {
        super(context);
        this.mContext = context;
        this.mParentActivity = composeMessageActivity;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.compose_context_dialog);
        this.mListView = (ListView) findViewById(16908298);
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.mContext.getString(R.string.menu_call_back));
        arrayList.add(this.mContext.getString(R.string.menu_call_back));
        arrayList.add(this.mContext.getString(R.string.menu_call_back));
        arrayList.add(this.mContext.getString(R.string.menu_call_back));
        arrayList.add(this.mContext.getString(R.string.menu_call_back));
        arrayList.add(this.mContext.getString(R.string.menu_call_back));
        this.mListView.setAdapter((ListAdapter) new ArrayAdapter(this.mContext, 17367043, new String[]{"aa", "bb", "xx"}));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.d("TAG", "+++++++++++++++++++++++++++");
    }
}
