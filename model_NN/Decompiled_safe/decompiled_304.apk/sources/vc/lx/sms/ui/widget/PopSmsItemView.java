package vc.lx.sms.ui.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.AsyncQueryHandler;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.telephony.Phone;
import com.android.mms.transaction.MessagingNotification;
import com.android.provider.Telephony;
import org.apache.http.NameValuePair;
import vc.lx.sms.intents.MessageWrapper;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class PopSmsItemView extends LinearLayout implements View.OnClickListener {
    private static final int DELETE_MESSAGE_TOKEN = 0;
    protected static final int PHONE_LOOKUP_CONTACT_ID_COLUMN_INDEX = 0;
    protected static final int PHONE_LOOKUP_CONTACT_LOOKUP_KEY_COLUMN_INDEX = 1;
    protected static final String[] PHONE_LOOKUP_PROJECTION = {"_id", "lookup"};
    private static final int TOKEN_CONTACT_INFO = 0;
    private static final int TOKEN_PHONE_LOOKUP = 1;
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public BackgroundQueryHandler mBackgroundQueryHandler;
    private TextView mBody;
    private Button mCloseButton;
    private Uri mContactUri;
    /* access modifiers changed from: private */
    public ContentResolver mContentResolver;
    private Button mDeleteButton;
    /* access modifiers changed from: private */
    public TextView mDisplayNameView;
    /* access modifiers changed from: private */
    public QueryHandler mQueryHandler;
    private EditText mSendBody;
    private Button mSendButton;
    private PopSmsItem mSmsItem;
    private TextView mTimestampView;
    private MessageWrapper mWrapper;

    private final class BackgroundQueryHandler extends AsyncQueryHandler {
        public BackgroundQueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onDeleteComplete(int i, Object obj, int i2) {
            switch (i) {
                case 0:
                    PopSmsItemView.this.mActivity.finish();
                    return;
                default:
                    return;
            }
        }
    }

    private interface ContactQuery {
        public static final String[] COLUMNS = {"_id", "lookup", "photo_id", "display_name"};
        public static final int DISPLAY_NAME = 3;
    }

    private class DeleteMessageListener implements DialogInterface.OnClickListener {
        private final Uri mDeleteUri;

        public DeleteMessageListener(long j, String str) {
            if (Phone.APN_TYPE_MMS.equals(str)) {
                this.mDeleteUri = ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, j);
            } else {
                this.mDeleteUri = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, j);
            }
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_DELETE, new NameValuePair[0]);
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            PopSmsItemView.this.mBackgroundQueryHandler.startDelete(0, null, this.mDeleteUri, null, null);
        }
    }

    public static class PopSmsItem {
        public String mAddress;
        public String mBody;
        public long mTimestamp;
    }

    private class QueryHandler extends AsyncQueryHandler {
        public QueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            try {
                if (this == PopSmsItemView.this.mQueryHandler) {
                    switch (i) {
                        case 0:
                            if (cursor != null) {
                                if (cursor.moveToFirst()) {
                                    PopSmsItemView.this.mDisplayNameView.setText(cursor.getString(3));
                                    PopSmsItemView.this.invalidate();
                                    break;
                                }
                            }
                            break;
                        case 1:
                            if (cursor != null) {
                                if (cursor.moveToFirst()) {
                                    PopSmsItemView.this.bindFromContactUriInternal(ContactsContract.Contacts.getLookupUri(cursor.getLong(0), cursor.getString(1)), false);
                                    break;
                                }
                            }
                            break;
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    public PopSmsItemView(Context context) {
        this(context, null);
    }

    public PopSmsItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initViews();
    }

    /* access modifiers changed from: private */
    public void bindFromContactUriInternal(Uri uri, boolean z) {
        this.mContactUri = uri;
        startContactQuery(uri, z);
    }

    private void confirmDeleteDialog(DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mActivity);
        builder.setTitle((int) R.string.confirm_dialog_title);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setMessage((int) R.string.confirm_delete_message);
        builder.setPositiveButton((int) R.string.delete, onClickListener);
        builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    private void initViews() {
        this.mContentResolver = getContext().getContentResolver();
        this.mBackgroundQueryHandler = new BackgroundQueryHandler(this.mContentResolver);
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.pop_sms_item_view, this);
        this.mBody = (TextView) findViewById(R.id.body);
        this.mBody.setOnClickListener(this);
        this.mDisplayNameView = (TextView) findViewById(R.id.name);
        this.mTimestampView = (TextView) findViewById(R.id.timestamp);
        this.mSendButton = (Button) findViewById(R.id.quickreply_send);
        this.mSendBody = (EditText) findViewById(R.id.quickreply_edittext);
        this.mSendButton.setOnClickListener(this);
        this.mCloseButton = (Button) findViewById(R.id.close);
        this.mDeleteButton = (Button) findViewById(R.id.delete);
        this.mCloseButton.setOnClickListener(this);
        this.mDeleteButton.setOnClickListener(this);
    }

    private void resetAsyncQueryHandler() {
        this.mQueryHandler = new QueryHandler(this.mContentResolver);
    }

    private void startContactQuery(Uri uri, boolean z) {
        if (z) {
            resetAsyncQueryHandler();
        }
        this.mQueryHandler.startQuery(0, null, uri, ContactQuery.COLUMNS, null, null, null);
    }

    public void bind(PopSmsItem popSmsItem, Activity activity) {
        this.mSmsItem = popSmsItem;
        this.mBody.setText(this.mSmsItem.mBody);
        this.mTimestampView.setText(MessageUtils.formatTimeStampString(getContext(), this.mSmsItem.mTimestamp));
        this.mDisplayNameView.setText(this.mSmsItem.mAddress);
        resetAsyncQueryHandler();
        this.mQueryHandler.startQuery(1, this.mSmsItem.mAddress, Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(this.mSmsItem.mAddress)), PHONE_LOOKUP_PROJECTION, null, null, null);
        this.mWrapper = new MessageWrapper(popSmsItem, getContext());
        this.mActivity = activity;
    }

    public void callContact() {
        this.mActivity.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.mWrapper.mAddress)));
        this.mActivity.finish();
    }

    public void deleteMessage() {
        confirmDeleteDialog(new DeleteMessageListener(this.mWrapper.getMessageId(), "sms"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void forwardSms() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.putExtra("exit_on_sent", true);
        intent.putExtra("forwarded_message", true);
        intent.putExtra("sms_body", this.mWrapper.mMessageBody);
        intent.putExtra("exit_on_sent", true);
        intent.putExtra("forwarded_message", true);
        intent.setComponent(new ComponentName(this.mActivity, ComposeMessageActivity.class));
        this.mActivity.startActivity(intent);
        this.mActivity.finish();
    }

    public synchronized Uri getUri() {
        return this.mWrapper.getThreadId() <= 0 ? null : ContentUris.withAppendedId(Telephony.Threads.CONTENT_URI, this.mWrapper.getThreadId());
    }

    public void markAsRead() {
        final Uri uri = getUri();
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
            public void run() {
                if (uri != null) {
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put("read", (Integer) 1);
                    PopSmsItemView.this.mContentResolver.update(uri, contentValues, "read=0", null);
                }
                MessagingNotification.updateNewMessageIndicator(PopSmsItemView.this.mActivity, false);
            }
        }).start();
    }

    public void markMessageAsRead() {
        long messageId = this.mWrapper.getMessageId();
        final Uri withAppendedId = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, messageId);
        if (messageId > 0) {
            new Thread(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                public void run() {
                    if (withAppendedId != null) {
                        ContentValues contentValues = new ContentValues(1);
                        contentValues.put("read", (Integer) 1);
                        PopSmsItemView.this.mContentResolver.update(withAppendedId, contentValues, null, null);
                    }
                    MessagingNotification.updateNewMessageIndicator(PopSmsItemView.this.mActivity, false);
                }
            }).start();
        }
    }

    public void onClick(View view) {
        if (view == this.mSendButton) {
            quickSendSms();
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_QUICK_REPLY, new NameValuePair[0]);
        } else if (view == this.mCloseButton) {
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_CLOSE, new NameValuePair[0]);
            markMessageAsRead();
            this.mActivity.finish();
        } else if (view == this.mDeleteButton) {
            markMessageAsRead();
            deleteMessage();
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_DELETE, new NameValuePair[0]);
        } else if (view == this.mBody) {
            replyToMessage();
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_REPLY, new NameValuePair[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        initViews();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }

    public void quickSendSms() {
        String obj = this.mSendBody.getText().toString();
        if (!obj.equals("") && obj.length() > 0) {
            markAsRead();
            MessagingNotification.updateNewMessageIndicator(this.mActivity, false);
            Util.sendSMS(this.mWrapper.mAddress, this.mSendBody.getText().toString(), this.mActivity);
            Log.v("smsManager", "successful");
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_QUICK_REPLY, new NameValuePair[0]);
            this.mActivity.finish();
        }
    }

    public void replyToMessage() {
        Intent createIntentByThreadId = ComposeMessageActivity.createIntentByThreadId(this.mActivity, this.mWrapper.getThreadId());
        createIntentByThreadId.setFlags(872415232);
        this.mActivity.startActivity(createIntentByThreadId);
        this.mActivity.finish();
    }
}
