package vc.lx.sms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public class MusicSelectionTabActivity extends AbstractFlurryTabActivity {
    public static final String SELECTION_ALL = "ALL";
    public static final String SELECTION_FAVORITE = "FAVORITE";
    private TabHost mTabHost;

    private TabHost.TabSpec createTabSpec(String str, String str2, Intent intent) {
        TabHost.TabSpec newTabSpec = this.mTabHost.newTabSpec(str);
        newTabSpec.setIndicator(str2, null);
        newTabSpec.setContent(intent);
        return newTabSpec;
    }

    public void onCreate(Bundle bundle) {
        Class<MusicSelectionActivity> cls = MusicSelectionActivity.class;
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.music_selection_main);
        this.mTabHost = (TabHost) findViewById(16908306);
        this.mTabHost.setup(getLocalActivityManager());
        Class<MusicSelectionActivity> cls2 = MusicSelectionActivity.class;
        Intent intent = new Intent(this, cls);
        intent.putExtra(PrefsUtil.PREF_MUSIC_SELECTION_NAME, SELECTION_ALL);
        this.mTabHost.addTab(createTabSpec("TAB_HOTTEST_BOARD", getString(R.string.music_selection_all), intent));
        Class<MusicSelectionActivity> cls3 = MusicSelectionActivity.class;
        Intent intent2 = new Intent(this, cls);
        intent2.putExtra(PrefsUtil.PREF_MUSIC_SELECTION_NAME, SELECTION_FAVORITE);
        this.mTabHost.addTab(createTabSpec("TAB_SEARCH", getString(R.string.music_selection_favorite), intent2));
    }
}
