package vc.lx.sms.ui;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.service.MiguPlayerService;
import vc.lx.sms.service.MiguPlayerServiceInterface;
import vc.lx.sms.util.LogTool;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class MusicDetailActivity extends AbstractFlurryListActivity {
    protected static final int MENU_DOWNLOAD_SONG = 2;
    protected static final int MENU_PLAY_SONG = 1;
    protected static final int MENU_PRESENT_RINGTONE = 5;
    protected static final int MENU_RINGING_SONG = 3;
    protected static final int MENU_RINGTONE_SONG = 4;
    protected static final int MENU_WHOLE_SONG = 6;
    private static final String TAG = "MusicPlatformTabInsideActivity";
    private SongItemAdapter mAdapter;
    private TextView mBroadName;
    private String mCurrentPlaySongContentId = "";
    public int mCurrentPlayingPosition;
    protected BroadcastReceiver mFetchSongUrlReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            SongItem songItem = (SongItem) intent.getSerializableExtra(PrefsUtil.KEY_SONG_INFO);
            String stringExtra = intent.getStringExtra(PrefsUtil.KEY_SONG_TARGET);
            if (songItem != null && MusicDetailActivity.this.mTarget.equals(stringExtra)) {
                MusicDetailActivity.this.playSong(songItem.durl);
            }
        }
    };
    protected final Handler mHandler = new Handler();
    protected LayoutInflater mInflater;
    private String mKeyword = "";
    public int mLatestCurrentPage;
    public int mLatestTotalPages;
    private ListView mListView;
    private LinearLayout mLoadingProgress;
    protected MiguPlayerServiceInterface mPlayerServiceInterface;
    String mPlug;
    protected ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            LogTool.i(MusicDetailActivity.TAG, "onPlayerServiceConnected");
            MusicDetailActivity.this.mPlayerServiceInterface = MiguPlayerServiceInterface.Stub.asInterface(iBinder);
            try {
                if (MusicDetailActivity.this.mPlayerServiceInterface == null || MusicDetailActivity.this.mPlayerServiceInterface.isPlaying()) {
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (IllegalStateException e2) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
            Log.i(MusicDetailActivity.TAG, "onPlayerServiceDisconnected");
            MusicDetailActivity.this.mPlayerServiceInterface = null;
        }
    };
    /* access modifiers changed from: private */
    public ArrayList<SongItem> mSongItemList;
    protected String mTarget;
    private TopMusicService topMusicService;

    class SongItemAdapter extends BaseAdapter {
        SongItemAdapter() {
        }

        public int getCount() {
            if (MusicDetailActivity.this.mSongItemList.size() == 0) {
                return 0;
            }
            return MusicDetailActivity.this.mLatestTotalPages > Integer.valueOf(MusicDetailActivity.this.mLatestCurrentPage).intValue() ? MusicDetailActivity.this.mSongItemList.size() + 1 : MusicDetailActivity.this.mSongItemList.size();
        }

        public Object getItem(int i) {
            return MusicDetailActivity.this.mSongItemList.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            SongListItem songListItem;
            View view2;
            if (view == null) {
                songListItem = new SongListItem();
                view2 = MusicDetailActivity.this.mInflater.inflate((int) R.layout.music_detail_item, viewGroup, false);
                songListItem.from = (TextView) view2.findViewById(R.id.music_detail_from);
                songListItem.date = (TextView) view2.findViewById(R.id.music_detail_date);
                songListItem.accessBtn = (Button) view2.findViewById(R.id.music_detail_access);
                view2.setTag(songListItem);
            } else {
                songListItem = (SongListItem) view.getTag();
                view2 = view;
            }
            SongItem songItem = (SongItem) MusicDetailActivity.this.mSongItemList.get(i);
            songListItem.from.setText(songItem.type);
            songListItem.date.setText(songItem.date);
            songListItem.accessBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                }
            });
            return view2;
        }
    }

    class SongListItem {
        Button accessBtn;
        TextView date;
        TextView from;
        TextView song;
        View songItem;

        SongListItem() {
        }
    }

    private void onCheckedAction(CompoundButton compoundButton, boolean z) {
        try {
            itemPlay(z);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void playSong(String str) {
        LogTool.i("url", str);
        Intent intent = new Intent(getApplicationContext(), MiguPlayerService.class);
        intent.setAction("play");
        Bundle bundle = new Bundle();
        bundle.putString(PrefsUtil.KEY_SONG_FILE_PAHT, str);
        bundle.putSerializable(PrefsUtil.KEY_SONG_TYPE, MiguPlayerService.SongType.RADIO);
        bundle.putSerializable(PrefsUtil.KEY_SONG_TARGET, this.mTarget);
        intent.putExtras(bundle);
        startService(intent);
    }

    /* access modifiers changed from: protected */
    public void itemPlay(boolean z) throws RemoteException {
        if (!z) {
            if (this.mPlayerServiceInterface != null && this.mPlayerServiceInterface.isPlaying()) {
                this.mPlayerServiceInterface.pause();
            }
        } else if (this.mPlayerServiceInterface != null && !this.mPlayerServiceInterface.isPlaying()) {
            this.mPlayerServiceInterface.resume();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.music_detail_activity);
        this.mPlug = getIntent().getStringExtra(PrefsUtil.PREF_BOARD_FOCUS_PLUG);
        this.mLatestTotalPages = 0;
        this.mLoadingProgress = (LinearLayout) findViewById(R.id.progress_container);
        this.mSongItemList = new ArrayList<>();
        this.topMusicService = SmsApplication.getInstance().getTopMusicService();
        if (this.mPlug != null) {
            this.mSongItemList = (ArrayList) this.topMusicService.getSongItemsByPlug(this.mPlug);
        }
        this.mInflater = LayoutInflater.from(this);
        this.mListView = (ListView) findViewById(16908298);
        this.mAdapter = new SongItemAdapter();
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        this.mListView.setVisibility(0);
        this.mBroadName = (TextView) findViewById(R.id.board_name);
        if (this.mSongItemList.size() != 0) {
            this.mBroadName.setText(this.mSongItemList.get(0).song);
        }
        this.mTarget = PrefsUtil.KEY_TARGET_MUSIC_BOARDS;
        ((ImageView) findViewById(R.id.music_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MusicDetailActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mHandler.post(new Runnable() {
            public void run() {
                Intent intent = new Intent(MusicDetailActivity.this.getApplicationContext(), MiguPlayerService.class);
                MusicDetailActivity.this.startService(intent);
                MusicDetailActivity.this.getApplicationContext().bindService(intent, MusicDetailActivity.this.mServiceConnection, 1);
            }
        });
    }
}
