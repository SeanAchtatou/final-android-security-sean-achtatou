package vc.lx.sms.ui;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;

public class MusicSubAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public List<SongItem> data;
    private LayoutInflater inflater;

    class MusicSubView {
        /* access modifiers changed from: private */
        public TextView mMoreView = null;
        /* access modifiers changed from: private */
        public Button mPlayBtn = null;
        /* access modifiers changed from: private */
        public TextView mSongSingerView = null;

        MusicSubView() {
        }
    }

    public MusicSubAdapter(Context context2, List<SongItem> list) {
        this.data = null;
        this.context = null;
        this.inflater = null;
        this.data = Collections.synchronizedList(new ArrayList());
        this.data.addAll(list);
        this.context = context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
    }

    public int getCount() {
        if (this.data.size() > 0) {
            return this.data.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return this.data.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(final int i, View view, ViewGroup viewGroup) {
        MusicSubView musicSubView;
        View view2;
        if (view == null) {
            musicSubView = new MusicSubView();
            view2 = this.inflater.inflate((int) R.layout.conversaion_music_sub_item, (ViewGroup) null);
            TextView unused = musicSubView.mSongSingerView = (TextView) view2.findViewById(R.id.singer_song);
            TextView unused2 = musicSubView.mMoreView = (TextView) view2.findViewById(R.id.more);
            Button unused3 = musicSubView.mPlayBtn = (Button) view2.findViewById(R.id.play);
            view2.setTag(musicSubView);
        } else {
            musicSubView = (MusicSubView) view.getTag();
            view2 = view;
        }
        musicSubView.mSongSingerView.setText(this.data.get(i).singer + "-" + this.data.get(i).song);
        if (i == 0) {
            musicSubView.mMoreView.setVisibility(0);
        }
        musicSubView.mSongSingerView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(MusicSubAdapter.this.context, MusicPlatformActivity.class);
                intent.putExtra(PrefsUtil.PREF_BOARD_NAME, MusicSubAdapter.this.context.getString(R.string.music_all));
                intent.putExtra(PrefsUtil.PREF_BOARD_FOCUS_PLUG, ((SongItem) MusicSubAdapter.this.data.get(i)).plug);
                intent.putExtra(PrefsUtil.PREF_BOARD_FOCUS_PLAY_STATUS, "yes");
                intent.setFlags(268435456);
                MusicSubAdapter.this.context.startActivity(intent);
            }
        });
        musicSubView.mPlayBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(MusicSubAdapter.this.context, MusicPlatformActivity.class);
                intent.putExtra(PrefsUtil.PREF_BOARD_NAME, MusicSubAdapter.this.context.getString(R.string.music_all));
                intent.putExtra(PrefsUtil.PREF_BOARD_FOCUS_PLUG, ((SongItem) MusicSubAdapter.this.data.get(i)).plug);
                intent.putExtra(PrefsUtil.PREF_BOARD_FOCUS_PLAY_STATUS, "yes");
                intent.setFlags(268435456);
                MusicSubAdapter.this.context.startActivity(intent);
            }
        });
        musicSubView.mMoreView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(MusicSubAdapter.this.context, MusicPlatformActivity.class);
                intent.putExtra(PrefsUtil.PREF_BOARD_NAME, MusicSubAdapter.this.context.getString(R.string.music_all));
                intent.putExtra(PrefsUtil.PREF_BOARD_FOCUS_PLUG, ((SongItem) MusicSubAdapter.this.data.get(i)).plug);
                intent.setFlags(268435456);
                MusicSubAdapter.this.context.startActivity(intent);
            }
        });
        view2.setClickable(true);
        view2.setSelected(true);
        return view2;
    }

    public void setData(List<SongItem> list) {
        this.data = Collections.synchronizedList(new ArrayList());
        this.data.addAll(list);
    }
}
