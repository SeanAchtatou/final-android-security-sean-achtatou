package vc.lx.sms.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import vc.lx.sms2.R;

public class AboutActivity extends Activity {
    private ImageView backBtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.about);
        this.backBtn = (ImageView) findViewById(R.id.about_back);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AboutActivity.this.finish();
            }
        });
    }
}
