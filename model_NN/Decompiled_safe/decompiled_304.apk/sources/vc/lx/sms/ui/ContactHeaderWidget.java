package vc.lx.sms.ui;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import com.android.provider.Telephony;
import vc.lx.sms2.R;

public class ContactHeaderWidget extends FrameLayout implements View.OnClickListener {
    protected static final int CONTACT_LOOKUP_ID_COLUMN_INDEX = 0;
    protected static final String[] CONTACT_LOOKUP_PROJECTION = {"_id"};
    protected static final int EMAIL_LOOKUP_CONTACT_ID_COLUMN_INDEX = 0;
    protected static final int EMAIL_LOOKUP_CONTACT_LOOKUP_KEY_COLUMN_INDEX = 1;
    protected static final String[] EMAIL_LOOKUP_PROJECTION = {Telephony.Mms.Addr.CONTACT_ID, "lookup"};
    protected static final int PHONE_LOOKUP_CONTACT_ID_COLUMN_INDEX = 0;
    protected static final int PHONE_LOOKUP_CONTACT_LOOKUP_KEY_COLUMN_INDEX = 1;
    protected static final String[] PHONE_LOOKUP_PROJECTION = {"_id", "lookup"};
    private static final String TAG = "ContactHeaderWidget";
    private static final int TOKEN_CONTACT_INFO = 0;
    private static final int TOKEN_EMAIL_LOOKUP = 2;
    private static final int TOKEN_PHONE_LOOKUP = 1;
    private static final int TOKEN_PHOTO_QUERY = 3;
    private ImageView mCallView;
    protected Uri mContactUri;
    private View mContainer;
    protected ContentResolver mContentResolver;
    private TextView mDisplayNameView;
    protected String[] mExcludeMimes;
    private ContactHeaderListener mListener;
    private TextView mMessagesCountView;
    private TextView mMessagesCountViewInfo;
    private int mNoPhotoResource;
    /* access modifiers changed from: private */
    public QuickContactBadge mPhotoView;
    private ImageView mPresenceView;
    /* access modifiers changed from: private */
    public QueryHandler mQueryHandler;
    private CheckBox mStarredView;
    private TextView mTimestampView;

    public interface ContactHeaderListener {
        void onDisplayNameClick(View view);

        void onPhotoClick(View view);
    }

    private interface ContactQuery {
        public static final String[] COLUMNS = {"_id", "lookup", "photo_id", "display_name", "starred", "contact_presence", "contact_status", "contact_status_ts", "contact_status_res_package", "contact_status_label"};
        public static final int CONTACT_PRESENCE_STATUS = 5;
        public static final int CONTACT_STATUS = 6;
        public static final int CONTACT_STATUS_LABEL = 9;
        public static final int CONTACT_STATUS_RES_PACKAGE = 8;
        public static final int CONTACT_STATUS_TIMESTAMP = 7;
        public static final int DISPLAY_NAME = 3;
        public static final int LOOKUP_KEY = 1;
        public static final int PHOTO_ID = 2;
        public static final int STARRED = 4;
        public static final int _ID = 0;
    }

    private interface PhotoQuery {
        public static final String[] COLUMNS = {"data15"};
        public static final int PHOTO = 0;
    }

    private class QueryHandler extends AsyncQueryHandler {
        public QueryHandler(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x003c A[Catch:{ all -> 0x0063 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onQueryComplete(int r6, java.lang.Object r7, android.database.Cursor r8) {
            /*
                r5 = this;
                r1 = 0
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget$QueryHandler r0 = r0.mQueryHandler     // Catch:{ all -> 0x0063 }
                if (r5 == r0) goto L_0x0016
                java.lang.String r0 = "ContactHeaderWidget"
                java.lang.String r1 = "onQueryComplete: discard result, the query handler is reset!"
                android.util.Log.d(r0, r1)     // Catch:{ all -> 0x0063 }
                if (r8 == 0) goto L_0x0015
                r8.close()
            L_0x0015:
                return
            L_0x0016:
                switch(r6) {
                    case 0: goto L_0x006a;
                    case 1: goto L_0x00ab;
                    case 2: goto L_0x00e9;
                    case 3: goto L_0x001f;
                    default: goto L_0x0019;
                }
            L_0x0019:
                if (r8 == 0) goto L_0x0015
                r8.close()
                goto L_0x0015
            L_0x001f:
                if (r8 == 0) goto L_0x0127
                boolean r0 = r8.moveToFirst()     // Catch:{ all -> 0x0063 }
                if (r0 == 0) goto L_0x0127
                r0 = 0
                boolean r0 = r8.isNull(r0)     // Catch:{ all -> 0x0063 }
                if (r0 != 0) goto L_0x0127
                r0 = 0
                byte[] r0 = r8.getBlob(r0)     // Catch:{ all -> 0x0063 }
                r1 = 0
                int r2 = r0.length     // Catch:{ all -> 0x0063 }
                r3 = 0
                android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r0, r1, r2, r3)     // Catch:{ all -> 0x0063 }
            L_0x003a:
                if (r0 != 0) goto L_0x0043
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r1 = 0
                android.graphics.Bitmap r0 = r0.loadPlaceholderPhoto(r1)     // Catch:{ all -> 0x0063 }
            L_0x0043:
                vc.lx.sms.ui.ContactHeaderWidget r1 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                android.widget.QuickContactBadge r1 = r1.mPhotoView     // Catch:{ all -> 0x0063 }
                r1.setImageBitmap(r0)     // Catch:{ all -> 0x0063 }
                if (r7 == 0) goto L_0x005d
                boolean r0 = r7 instanceof android.net.Uri     // Catch:{ all -> 0x0063 }
                if (r0 == 0) goto L_0x005d
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                android.widget.QuickContactBadge r0 = r0.mPhotoView     // Catch:{ all -> 0x0063 }
                android.net.Uri r7 = (android.net.Uri) r7     // Catch:{ all -> 0x0063 }
                r0.assignContactUri(r7)     // Catch:{ all -> 0x0063 }
            L_0x005d:
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r0.invalidate()     // Catch:{ all -> 0x0063 }
                goto L_0x0019
            L_0x0063:
                r0 = move-exception
                if (r8 == 0) goto L_0x0069
                r8.close()
            L_0x0069:
                throw r0
            L_0x006a:
                if (r8 == 0) goto L_0x0096
                boolean r0 = r8.moveToFirst()     // Catch:{ all -> 0x0063 }
                if (r0 == 0) goto L_0x0096
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r0.bindContactInfo(r8)     // Catch:{ all -> 0x0063 }
                r0 = 0
                long r0 = r8.getLong(r0)     // Catch:{ all -> 0x0063 }
                r2 = 1
                java.lang.String r2 = r8.getString(r2)     // Catch:{ all -> 0x0063 }
                android.net.Uri r0 = android.provider.ContactsContract.Contacts.getLookupUri(r0, r2)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r1 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r2 = 2
                long r2 = r8.getLong(r2)     // Catch:{ all -> 0x0063 }
                r4 = 0
                r1.startPhotoQuery(r2, r0, r4)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r0.invalidate()     // Catch:{ all -> 0x0063 }
                goto L_0x0019
            L_0x0096:
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r1 = 0
                r2 = 0
                r0.setDisplayName(r1, r2)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r1 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r2 = 0
                android.graphics.Bitmap r1 = r1.loadPlaceholderPhoto(r2)     // Catch:{ all -> 0x0063 }
                r0.setPhoto(r1)     // Catch:{ all -> 0x0063 }
                goto L_0x0019
            L_0x00ab:
                if (r8 == 0) goto L_0x00c9
                boolean r0 = r8.moveToFirst()     // Catch:{ all -> 0x0063 }
                if (r0 == 0) goto L_0x00c9
                r0 = 0
                long r0 = r8.getLong(r0)     // Catch:{ all -> 0x0063 }
                r2 = 1
                java.lang.String r2 = r8.getString(r2)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r3 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                android.net.Uri r0 = android.provider.ContactsContract.Contacts.getLookupUri(r0, r2)     // Catch:{ all -> 0x0063 }
                r1 = 0
                r3.bindFromContactUriInternal(r0, r1)     // Catch:{ all -> 0x0063 }
                goto L_0x0019
            L_0x00c9:
                java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r1 = 0
                r0.setDisplayName(r7, r1)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r1 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r2 = 0
                android.graphics.Bitmap r1 = r1.loadPlaceholderPhoto(r2)     // Catch:{ all -> 0x0063 }
                r0.setPhoto(r1)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                android.widget.QuickContactBadge r0 = r0.mPhotoView     // Catch:{ all -> 0x0063 }
                r1 = 1
                r0.assignContactFromPhone(r7, r1)     // Catch:{ all -> 0x0063 }
                goto L_0x0019
            L_0x00e9:
                if (r8 == 0) goto L_0x0107
                boolean r0 = r8.moveToFirst()     // Catch:{ all -> 0x0063 }
                if (r0 == 0) goto L_0x0107
                r0 = 0
                long r0 = r8.getLong(r0)     // Catch:{ all -> 0x0063 }
                r2 = 1
                java.lang.String r2 = r8.getString(r2)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r3 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                android.net.Uri r0 = android.provider.ContactsContract.Contacts.getLookupUri(r0, r2)     // Catch:{ all -> 0x0063 }
                r1 = 0
                r3.bindFromContactUriInternal(r0, r1)     // Catch:{ all -> 0x0063 }
                goto L_0x0019
            L_0x0107:
                java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r1 = 0
                r0.setDisplayName(r7, r1)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r1 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                r2 = 0
                android.graphics.Bitmap r1 = r1.loadPlaceholderPhoto(r2)     // Catch:{ all -> 0x0063 }
                r0.setPhoto(r1)     // Catch:{ all -> 0x0063 }
                vc.lx.sms.ui.ContactHeaderWidget r0 = vc.lx.sms.ui.ContactHeaderWidget.this     // Catch:{ all -> 0x0063 }
                android.widget.QuickContactBadge r0 = r0.mPhotoView     // Catch:{ all -> 0x0063 }
                r1 = 1
                r0.assignContactFromEmail(r7, r1)     // Catch:{ all -> 0x0063 }
                goto L_0x0019
            L_0x0127:
                r0 = r1
                goto L_0x003a
            */
            throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.ui.ContactHeaderWidget.QueryHandler.onQueryComplete(int, java.lang.Object, android.database.Cursor):void");
        }
    }

    public ContactHeaderWidget(Context context) {
        this(context, null);
    }

    public ContactHeaderWidget(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ContactHeaderWidget(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mExcludeMimes = null;
        this.mContentResolver = getContext().getContentResolver();
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.contact_header, this);
        this.mContainer = findViewById(R.id.banner);
        this.mDisplayNameView = (TextView) findViewById(R.id.name);
        this.mTimestampView = (TextView) findViewById(R.id.timestamp);
        this.mMessagesCountView = (TextView) findViewById(R.id.messages_count);
        this.mMessagesCountViewInfo = (TextView) findViewById(R.id.messages_count_info);
        this.mCallView = (ImageView) findViewById(R.id.call_btn);
        this.mStarredView = (CheckBox) findViewById(R.id.star);
        this.mStarredView.setOnClickListener(this);
        this.mPhotoView = (QuickContactBadge) findViewById(R.id.photo);
        this.mPresenceView = (ImageView) findViewById(R.id.presence);
        int elapsedRealtime = ((int) SystemClock.elapsedRealtime()) & 15;
        if (elapsedRealtime < 9) {
            this.mNoPhotoResource = R.drawable.ic_contact_picture;
        } else if (elapsedRealtime < 14) {
            this.mNoPhotoResource = R.drawable.ic_contact_picture_2;
        } else {
            this.mNoPhotoResource = R.drawable.ic_contact_picture_3;
        }
        resetAsyncQueryHandler();
    }

    /* access modifiers changed from: private */
    public void bindFromContactUriInternal(Uri uri, boolean z) {
        this.mContactUri = uri;
        startContactQuery(uri, z);
    }

    /* access modifiers changed from: private */
    public Bitmap loadPlaceholderPhoto(BitmapFactory.Options options) {
        if (this.mNoPhotoResource == 0) {
            return null;
        }
        return BitmapFactory.decodeResource(getContext().getResources(), this.mNoPhotoResource, options);
    }

    private void performDisplayNameClick() {
        if (this.mListener != null) {
            this.mListener.onDisplayNameClick(this.mDisplayNameView);
        }
    }

    private void performPhotoClick() {
        if (this.mListener != null) {
            this.mListener.onPhotoClick(this.mPhotoView);
        }
    }

    private void resetAsyncQueryHandler() {
        this.mQueryHandler = new QueryHandler(this.mContentResolver);
    }

    private void startContactQuery(Uri uri, boolean z) {
        if (z) {
            resetAsyncQueryHandler();
        }
        this.mQueryHandler.startQuery(0, null, uri, ContactQuery.COLUMNS, null, null, null);
    }

    /* access modifiers changed from: protected */
    public void bindContactInfo(Cursor cursor) {
        setDisplayName(cursor.getString(3), null);
        this.mStarredView.setChecked(cursor.getInt(4) != 0);
        if (!cursor.isNull(5)) {
            cursor.getInt(5);
            this.mPresenceView.setVisibility(0);
            return;
        }
        this.mPresenceView.setVisibility(8);
    }

    public void bindFromContactLookupUri(Uri uri) {
        bindFromContactUriInternal(uri, true);
    }

    public void bindFromEmail(String str) {
        resetAsyncQueryHandler();
        this.mQueryHandler.startQuery(2, str, Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, Uri.encode(str)), EMAIL_LOOKUP_PROJECTION, null, null, null);
    }

    public void bindFromPhoneNumber(String str) {
        resetAsyncQueryHandler();
        this.mQueryHandler.startQuery(1, str, Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), PHONE_LOOKUP_PROJECTION, null, null, null);
    }

    public void clearContainerBackground() {
        if (this.mContainer != null) {
            this.mContainer.setBackgroundDrawable(null);
        }
    }

    public void enableClickListeners() {
        this.mDisplayNameView.setOnClickListener(this);
        this.mPhotoView.setOnClickListener(this);
    }

    public void hideMessageInfo() {
        if (this.mMessagesCountView != null) {
            this.mMessagesCountView.setVisibility(8);
        }
        if (this.mMessagesCountViewInfo != null) {
            this.mMessagesCountViewInfo.setVisibility(8);
        }
        if (this.mCallView != null) {
            this.mCallView.setVisibility(8);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.photo /*2131492894*/:
                performPhotoClick();
                return;
            case R.id.name /*2131492895*/:
                performDisplayNameClick();
                return;
            case R.id.star /*2131492963*/:
                if (this.mContactUri != null) {
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put("starred", Boolean.valueOf(this.mStarredView.isChecked()));
                    this.mContentResolver.update(this.mContactUri, contentValues, null, null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void setAndDisplayCallButton(final String str) {
        this.mCallView.setVisibility(0);
        this.mCallView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ContactHeaderWidget.this.getContext().startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str)));
            }
        });
    }

    public void setAndDisplayMessagesCount(CharSequence charSequence) {
        if (this.mMessagesCountView == null) {
            this.mMessagesCountView = (TextView) findViewById(R.id.messages_count);
            this.mMessagesCountViewInfo = (TextView) findViewById(R.id.messages_count_info);
        }
        this.mMessagesCountView.setVisibility(0);
        this.mMessagesCountViewInfo.setVisibility(0);
        this.mMessagesCountView.setText(charSequence);
    }

    public void setAndDisplayTimestamp(CharSequence charSequence) {
        if (charSequence != null) {
            if (this.mTimestampView == null) {
                this.mTimestampView = (TextView) findViewById(R.id.timestamp);
            }
            this.mTimestampView.setVisibility(0);
            this.mTimestampView.setText(charSequence);
        }
    }

    public void setContactHeaderListener(ContactHeaderListener contactHeaderListener) {
        this.mListener = contactHeaderListener;
    }

    public void setContactUri(Uri uri) {
        setContactUri(uri, true);
    }

    public void setContactUri(Uri uri, boolean z) {
        this.mContactUri = uri;
        if (z) {
            this.mPhotoView.assignContactUri(uri);
        }
    }

    public void setDisplayName(CharSequence charSequence, CharSequence charSequence2) {
        this.mDisplayNameView.setText(charSequence);
    }

    public void setExcludeMimes(String[] strArr) {
        this.mExcludeMimes = strArr;
        this.mPhotoView.setExcludeMimes(strArr);
    }

    public void setPhoto(Bitmap bitmap) {
        this.mPhotoView.setImageBitmap(bitmap);
    }

    public void setPresence(int i) {
        this.mPresenceView.setBackgroundResource(i);
    }

    public void setStared(boolean z) {
        this.mStarredView.setChecked(z);
    }

    public void showStar(boolean z) {
        this.mStarredView.setVisibility(z ? 0 : 8);
    }

    /* access modifiers changed from: protected */
    public void startPhotoQuery(long j, Uri uri, boolean z) {
        if (z) {
            resetAsyncQueryHandler();
        }
        this.mQueryHandler.startQuery(3, uri, ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, j), PhotoQuery.COLUMNS, null, null, null);
    }

    public void wipeClean() {
        resetAsyncQueryHandler();
        setDisplayName(null, null);
        setPhoto(loadPlaceholderPhoto(null));
        setAndDisplayMessagesCount(null);
        setPresence(0);
        this.mContactUri = null;
        this.mExcludeMimes = null;
    }
}
