package vc.lx.sms.ui;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.android.mms.MmsConfig;
import com.android.mms.ui.ManageSimMessages;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class MessagingPreferenceActivity extends PreferenceActivity {
    public static final String AUTO_DELETE = "pref_key_auto_delete";
    public static final String AUTO_RETRIEVAL = "pref_key_mms_auto_retrieval";
    public static final String EXPIRY_TIME = "pref_key_mms_expiry";
    private static final int MENU_RESTORE_DEFAULTS = 1;
    public static final String MMS_DELIVERY_REPORT_MODE = "pref_key_mms_delivery_reports";
    public static final String MUSIC_VERSION_VALUE = "pref_key_music_version_value";
    public static final String NOTIFICATION_ENABLED = "pref_key_enable_notifications";
    public static final String NOTIFICATION_RINGTONE = "pref_key_ringtone";
    public static final String NOTIFICATION_VIBRATE = "pref_key_vibrate";
    public static final String POP_RECOMMEND_POPUP_MODE = "pref_key_enable_popup_recommendation";
    public static final String POP_RECOMMEND_SONG_MODE = "pref_key_enable_pop_recommendation";
    public static final String PRIORITY = "pref_key_mms_priority";
    public static final String READ_REPORT_MODE = "pref_key_mms_read_reports";
    public static final String RETRIEVAL_DURING_ROAMING = "pref_key_mms_retrieval_during_roaming";
    public static final String SMS_DELIVERY_REPORT_MODE = "pref_key_sms_delivery_reports";
    private Preference mAppSettingPref;
    private Preference mManageSimPref;
    private CheckBoxPreference mPhotoCheckBoxPreference;
    private CheckBoxPreference mPopupCheckBoxPreference;

    private void restoreDefaultPreferences() {
        PreferenceManager.getDefaultSharedPreferences(this).edit().clear().commit();
        setPreferenceScreen(null);
        addPreferencesFromResource(R.xml.preferences);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
        setContentView((int) R.layout.messaging_preference);
        this.mManageSimPref = findPreference("pref_key_manage_sim_messages");
        this.mAppSettingPref = findPreference("pref_key_app_setting");
        this.mPhotoCheckBoxPreference = (CheckBoxPreference) findPreference("pref_key_sms_photo_music_vote_delivery_reports");
        this.mPopupCheckBoxPreference = (CheckBoxPreference) findPreference(POP_RECOMMEND_SONG_MODE);
        if (!((TelephonyManager) getSystemService("phone")).hasIccCard()) {
            ((PreferenceCategory) findPreference("pref_key_sms_settings")).removePreference(this.mManageSimPref);
        }
        if (!MmsConfig.getMmsEnabled()) {
            getPreferenceScreen().removePreference((PreferenceCategory) findPreference("pref_key_mms_settings"));
            ((PreferenceCategory) findPreference("pref_key_storage_settings")).removePreference(findPreference("pref_key_mms_delete_limit"));
        }
        ListView listView = getListView();
        listView.setSelector((int) R.drawable.bg_listitem_green);
        listView.setCacheColorHint(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.clear();
        menu.add(0, 1, 0, (int) R.string.restore_default);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                restoreDefaultPreferences();
                return true;
            default:
                return false;
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == this.mManageSimPref) {
            startActivity(new Intent(this, ManageSimMessages.class));
        } else if (preference != this.mAppSettingPref) {
            if (preference == this.mPhotoCheckBoxPreference) {
                Util.setPopupNotification(getApplicationContext(), this.mPhotoCheckBoxPreference.isChecked());
            } else if (preference == this.mPopupCheckBoxPreference) {
                Util.setPhotoMusicNotification(getApplicationContext(), this.mPopupCheckBoxPreference.isChecked());
            }
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}
