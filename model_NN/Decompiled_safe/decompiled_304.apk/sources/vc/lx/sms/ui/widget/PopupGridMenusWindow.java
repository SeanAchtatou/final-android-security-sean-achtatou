package vc.lx.sms.ui.widget;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import java.util.List;
import vc.lx.sms.ui.widget.PopupMenusWindow;
import vc.lx.sms2.R;

public class PopupGridMenusWindow extends PopupMenusWindow {
    /* access modifiers changed from: private */
    public GridView mGridView;
    private AdapterView.OnItemClickListener mInternalItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            try {
                PopupMenusWindow.OnMenuItemClickListener onMenuItemClickListener = PopupGridMenusWindow.this.getOnMenuItemClickListener();
                if (onMenuItemClickListener != null) {
                    onMenuItemClickListener.onMenuItemClicked(PopupGridMenusWindow.this, i);
                    if (PopupGridMenusWindow.this.getDismissOnClick()) {
                        PopupGridMenusWindow.this.dismiss();
                    }
                } else if (PopupGridMenusWindow.this.getDismissOnClick()) {
                    PopupGridMenusWindow.this.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public PopupGridMenusWindow(Context context) {
        super(context);
        setContentView(R.layout.popup_grid_menus_window);
        this.mGridView = (GridView) getContentView().findViewById(R.id.popup_menus);
        this.mGridView.setFocusableInTouchMode(true);
    }

    public void afterShow() {
        this.mGridView.setFocusable(true);
        this.mGridView.setOnKeyListener(null);
        this.mGridView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i != 82 || !PopupGridMenusWindow.this.isShowing() || keyEvent.getAction() != 0) {
                    return false;
                }
                PopupGridMenusWindow.this.dismiss();
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onMeasureAndLayout(View view) {
        view.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        view.measure(View.MeasureSpec.makeMeasureSpec(getScreenWidth(), 1073741824), -2);
        setWidgetSpecs(0);
    }

    /* access modifiers changed from: protected */
    public void populatePopupMenuItems(final List<PopupMenuItem> list) {
        this.mGridView.setAdapter((ListAdapter) new BaseAdapter() {
            public int getCount() {
                return list.size();
            }

            public Object getItem(int i) {
                return list.get(i);
            }

            public long getItemId(int i) {
                return (long) i;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, android.widget.GridView, int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public View getView(int i, View view, ViewGroup viewGroup) {
                View inflate = view == null ? LayoutInflater.from(PopupGridMenusWindow.this.getContext()).inflate((int) R.layout.popup_window_menu_item, (ViewGroup) PopupGridMenusWindow.this.mGridView, false) : view;
                PopupMenuItem popupMenuItem = (PopupMenuItem) list.get(i);
                ((ImageView) inflate.findViewById(R.id.item_image)).setBackgroundDrawable(popupMenuItem.mDrawable);
                ((TextView) inflate.findViewById(R.id.item_text)).setText(popupMenuItem.mTitle);
                return inflate;
            }
        });
        this.mGridView.setOnItemClickListener(this.mInternalItemClickListener);
    }

    /* access modifiers changed from: protected */
    public void prepareAnimationStyle() {
        setAnimationStyle(R.style.PopupWindow_Menu);
    }

    public void setNumColums(int i) {
        this.mGridView.setGravity(17);
        this.mGridView.setNumColumns(i);
    }
}
