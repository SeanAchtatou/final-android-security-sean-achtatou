package vc.lx.sms.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.mms.data.WorkingMessage;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms2.R;

public class VoteAttachmentView extends LinearLayout {
    public static final int MSG_REMOVE_VOTE = 15;
    private LinearLayout mContent;
    Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private VoteItem mVoteItem;
    private TextView mVoteTitle;

    public VoteAttachmentView(Context context) {
        super(context);
        this.mContext = context;
    }

    public VoteAttachmentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
    }

    private void updateVoteItem(VoteItem voteItem) {
        this.mVoteItem = voteItem;
        if (voteItem != null) {
            setVisibility(0);
            this.mVoteTitle.setText(this.mVoteItem.title);
            this.mVoteTitle.setTextSize(16.0f);
            for (int i = 0; i < this.mVoteItem.options.size(); i++) {
                TextView textView = new TextView(this.mContext);
                textView.setText(this.mVoteItem.options.get(i).display_order + "." + this.mVoteItem.options.get(i).content);
                this.mContent.addView(textView);
            }
            return;
        }
        setVisibility(8);
    }

    public VoteItem getVoteItem() {
        return this.mVoteItem;
    }

    public void hideView() {
        setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.mVoteTitle = (TextView) findViewById(R.id.vote_title);
        this.mContent = (LinearLayout) findViewById(R.id.vote_content);
    }

    public void setHandler(Handler handler) {
        this.mHandler = handler;
    }

    public void update(WorkingMessage workingMessage) {
        if (workingMessage != null) {
            updateVoteItem(workingMessage.getVoteItem());
            if (this.mVoteItem != null) {
                ((ImageView) findViewById(R.id.remove_music_button)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Message.obtain(VoteAttachmentView.this.mHandler, 15).sendToTarget();
                    }
                });
            }
        }
    }
}
