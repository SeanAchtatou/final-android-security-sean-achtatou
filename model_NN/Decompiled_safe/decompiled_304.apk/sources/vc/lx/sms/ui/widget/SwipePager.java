package vc.lx.sms.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Scroller;

public final class SwipePager extends ViewGroup {
    private static final int ANIMATION_SCREEN_SET_DURATION_MILLIS = 500;
    private static final int FRACTION_OF_SCREEN_WIDTH_FOR_SWIPE = 4;
    private static final int INVALID_SCREEN = -1;
    private static final int SNAP_VELOCITY_DIP_PER_SECOND = 600;
    private static final int TOUCH_STATE_HORIZONTAL_SCROLLING = 1;
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_VERTICAL_SCROLLING = -1;
    private static final int VELOCITY_UNIT_PIXELS_PER_SECOND = 1000;
    private int mCurrentScreen;
    private int mDensityAdjustedSnapVelocity;
    private boolean mFirstLayout = true;
    private float mLastMotionX;
    private float mLastMotionY;
    private int mLastSeenLayoutWidth = -1;
    private int mMaximumVelocity;
    private int mNextScreen = -1;
    private OnScreenSwitchListener mOnScreenSwitchListener;
    private Scroller mScroller;
    private int mTouchSlop;
    private int mTouchState = 0;
    private VelocityTracker mVelocityTracker;

    public interface OnScreenSwitchListener {
        void onScreenSwitched(int i);
    }

    public SwipePager(Context context) {
        super(context);
        init();
    }

    public SwipePager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        this.mScroller = new Scroller(getContext());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.mDensityAdjustedSnapVelocity = (int) (displayMetrics.density * 600.0f);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
        this.mMaximumVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    private void snapToDestination() {
        int width = getWidth();
        int scrollX = getScrollX();
        int i = this.mCurrentScreen;
        int i2 = scrollX - (this.mCurrentScreen * width);
        snapToScreen((i2 >= 0 || this.mCurrentScreen == 0 || width / 4 >= (-i2)) ? (i2 <= 0 || this.mCurrentScreen + 1 == getChildCount() || width / 4 >= i2) ? i : i + 1 : i - 1);
    }

    private void snapToScreen(int i) {
        snapToScreen(i, -1);
    }

    private void snapToScreen(int i, int i2) {
        this.mNextScreen = Math.max(0, Math.min(i, getChildCount() - 1));
        int width = (this.mNextScreen * getWidth()) - getScrollX();
        if (i2 < 0) {
            this.mScroller.startScroll(getScrollX(), 0, width, 0, (int) ((((float) Math.abs(width)) / ((float) getWidth())) * 500.0f));
        } else {
            this.mScroller.startScroll(getScrollX(), 0, width, 0, i2);
        }
        invalidate();
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
        } else if (this.mNextScreen != -1) {
            this.mCurrentScreen = Math.max(0, Math.min(this.mNextScreen, getChildCount() - 1));
            if (this.mOnScreenSwitchListener != null) {
                this.mOnScreenSwitchListener.onScreenSwitched(this.mCurrentScreen);
            }
            this.mNextScreen = -1;
        }
    }

    public int getCurrentScreen() {
        return this.mCurrentScreen;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.mLastMotionY = motionEvent.getY();
                this.mLastMotionX = motionEvent.getX();
                return false;
            case 1:
            case 3:
                this.mTouchState = 0;
                return false;
            case 2:
                if (this.mTouchState == 1) {
                    return true;
                }
                if (this.mTouchState == -1) {
                    return false;
                }
                float x = motionEvent.getX();
                if (((int) Math.abs(x - this.mLastMotionX)) > this.mTouchSlop) {
                    this.mTouchState = 1;
                    this.mLastMotionX = x;
                }
                if (((int) Math.abs(motionEvent.getY() - this.mLastMotionY)) > this.mTouchSlop) {
                    this.mTouchState = -1;
                }
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int i5 = 0;
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                childAt.layout(i5, 0, i5 + measuredWidth, childAt.getMeasuredHeight());
                i5 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        super.onMeasure(i, i2);
        int size = View.MeasureSpec.getSize(i);
        if (View.MeasureSpec.getMode(i) != 1073741824) {
            throw new IllegalStateException("ViewSwitcher can only be used in EXACTLY mode.");
        }
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
        }
        int childCount = getChildCount();
        int i4 = 0;
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                childAt.measure(i, View.MeasureSpec.makeMeasureSpec(childAt.getLayoutParams().height, 1073741824));
                i4 = Math.max(i4, childAt.getMeasuredHeight());
            }
        }
        if (this.mCurrentScreen < childCount) {
            View childAt2 = getChildAt(this.mCurrentScreen);
            childAt2.measure(i, View.MeasureSpec.makeMeasureSpec(childAt2.getLayoutParams().height, 1073741824));
            i3 = childAt2.getMeasuredHeight();
        } else {
            i3 = i4;
        }
        setMeasuredDimension(size, i3);
        if (this.mFirstLayout) {
            scrollTo(this.mCurrentScreen * size, 0);
            this.mFirstLayout = false;
        } else if (size != this.mLastSeenLayoutWidth) {
            int width = ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getWidth();
            this.mNextScreen = Math.max(0, Math.min(getCurrentScreen(), getChildCount() - 1));
            this.mScroller.startScroll(getScrollX(), 0, (width * this.mNextScreen) - getScrollX(), 0, 0);
        }
        this.mLastSeenLayoutWidth = size;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int right;
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent);
        int action = motionEvent.getAction();
        float x = motionEvent.getX();
        switch (action) {
            case 0:
                if (!this.mScroller.isFinished()) {
                    this.mScroller.abortAnimation();
                }
                this.mLastMotionX = x;
                if (!this.mScroller.isFinished()) {
                    this.mTouchState = 1;
                    break;
                } else {
                    this.mTouchState = 0;
                    break;
                }
            case 1:
                if (this.mTouchState == 1) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumVelocity);
                    int xVelocity = (int) velocityTracker.getXVelocity();
                    if (xVelocity > this.mDensityAdjustedSnapVelocity && this.mCurrentScreen > 0) {
                        snapToScreen(this.mCurrentScreen - 1);
                    } else if (xVelocity >= (-this.mDensityAdjustedSnapVelocity) || this.mCurrentScreen >= getChildCount() - 1) {
                        snapToDestination();
                    } else {
                        snapToScreen(this.mCurrentScreen + 1);
                    }
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                }
                this.mTouchState = 0;
                break;
            case 2:
                if (((int) Math.abs(x - this.mLastMotionX)) > this.mTouchSlop) {
                    this.mTouchState = 1;
                }
                if (this.mTouchState == 1) {
                    int i = (int) (this.mLastMotionX - x);
                    this.mLastMotionX = x;
                    int scrollX = getScrollX();
                    if (i >= 0) {
                        if (i > 0 && (right = (getChildAt(getChildCount() - 1).getRight() - scrollX) - getWidth()) > 0) {
                            scrollBy(Math.min(right, i), 0);
                            break;
                        }
                    } else if (scrollX > 0) {
                        scrollBy(Math.max(-scrollX, i), 0);
                        break;
                    }
                }
                break;
            case 3:
                this.mTouchState = 0;
                break;
        }
        return true;
    }

    public void setCurrentScreen(int i, boolean z) {
        this.mCurrentScreen = Math.max(0, Math.min(i, getChildCount() - 1));
        if (z) {
            snapToScreen(i, ANIMATION_SCREEN_SET_DURATION_MILLIS);
        } else {
            scrollTo(this.mCurrentScreen * getWidth(), 0);
        }
        invalidate();
    }

    public void setOnScreenSwitchListener(OnScreenSwitchListener onScreenSwitchListener) {
        this.mOnScreenSwitchListener = onScreenSwitchListener;
    }
}
