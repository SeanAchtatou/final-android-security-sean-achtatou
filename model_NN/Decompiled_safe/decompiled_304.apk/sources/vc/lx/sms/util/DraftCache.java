package vc.lx.sms.util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.android.provider.Telephony;
import java.util.HashSet;
import java.util.Iterator;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.db.ImageDumpContentProvider;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.VoteContentProvider;

public class DraftCache {
    static final int COLUMN_DRAFT_THREAD_ID = 0;
    static final String[] DRAFT_PROJECTION = {"thread_id"};
    private static final String TAG = "Mms/draft";
    private static DraftCache sInstance;
    private final HashSet<OnDraftChangedListener> mChangeListeners = new HashSet<>(1);
    private final Context mContext;
    private HashSet<Long> mDraftSet = new HashSet<>(4);

    public interface OnDraftChangedListener {
        void onDraftChanged(long j, boolean z);
    }

    private DraftCache(Context context) {
        this.mContext = context;
        refresh();
    }

    public static DraftCache getInstance() {
        return sInstance;
    }

    public static void init(Context context) {
        sInstance = new DraftCache(context);
    }

    private void log(String str, Object... objArr) {
        Log.d(TAG, "[DraftCache/" + Thread.currentThread().getId() + "] " + String.format(str, objArr));
    }

    /* access modifiers changed from: private */
    public synchronized void rebuildCache() {
        Cursor managedQuery;
        Cursor managedQuery2;
        if (Log.isLoggable(LogTag.APP, 3)) {
            log("rebuildCache", new Object[0]);
        }
        HashSet<Long> hashSet = this.mDraftSet;
        HashSet<Long> hashSet2 = new HashSet<>(hashSet.size());
        Cursor managedQuery3 = ((Activity) this.mContext).managedQuery(Telephony.MmsSms.CONTENT_DRAFT_URI, DRAFT_PROJECTION, null, null, null);
        while (managedQuery3.moveToNext()) {
            try {
                hashSet2.add(Long.valueOf(managedQuery3.getLong(0)));
            } catch (Throwable th) {
                managedQuery3.close();
                throw th;
            }
        }
        managedQuery3.close();
        managedQuery = ((Activity) this.mContext).managedQuery(ImageDumpContentProvider.CONTENT_URI, new String[]{SmsSqliteHelper.RECIPIENT}, null, null, null);
        while (managedQuery.moveToNext()) {
            String[] split = managedQuery.getString(0).split(",");
            if (split.length != 0) {
                HashSet hashSet3 = new HashSet();
                for (String add : split) {
                    hashSet3.add(add);
                }
                try {
                    hashSet2.add(Long.valueOf(Telephony.Threads.getOrCreateThreadId(this.mContext, hashSet3)));
                } catch (Exception e) {
                }
            }
        }
        managedQuery.close();
        managedQuery2 = ((Activity) this.mContext).managedQuery(VoteContentProvider.VOTEDRAFT_CONTENT_URI, new String[]{"numbers"}, null, null, null);
        while (managedQuery2.moveToNext()) {
            String[] split2 = managedQuery2.getString(0).split(",");
            if (split2.length != 0) {
                HashSet hashSet4 = new HashSet();
                for (String add2 : split2) {
                    hashSet4.add(add2);
                }
                try {
                    hashSet2.add(Long.valueOf(Telephony.Threads.getOrCreateThreadId(this.mContext, hashSet4)));
                } catch (Exception e2) {
                }
            }
        }
        managedQuery2.close();
        this.mDraftSet = hashSet2;
        if (Log.isLoggable(LogTag.APP, 2)) {
            dump();
        }
        if (this.mChangeListeners.size() >= 1) {
            HashSet<Long> hashSet5 = new HashSet<>(hashSet2);
            hashSet5.removeAll(hashSet);
            HashSet<Long> hashSet6 = new HashSet<>(hashSet);
            hashSet6.removeAll(hashSet2);
            Iterator<OnDraftChangedListener> it = this.mChangeListeners.iterator();
            while (it.hasNext()) {
                OnDraftChangedListener next = it.next();
                for (Long longValue : hashSet5) {
                    next.onDraftChanged(longValue.longValue(), true);
                }
                for (Long longValue2 : hashSet6) {
                    next.onDraftChanged(longValue2.longValue(), false);
                }
            }
        }
    }

    public synchronized void addOnDraftChangedListener(OnDraftChangedListener onDraftChangedListener) {
        if (Log.isLoggable(LogTag.APP, 3)) {
            log("addOnDraftChangedListener " + onDraftChangedListener, new Object[0]);
        }
        this.mChangeListeners.add(onDraftChangedListener);
    }

    public void dump() {
        Log.i(TAG, "dump:");
        Iterator<Long> it = this.mDraftSet.iterator();
        while (it.hasNext()) {
            Log.i(TAG, "  tid: " + it.next());
        }
    }

    public synchronized boolean hasDraft(long j) {
        return this.mDraftSet.contains(Long.valueOf(j));
    }

    public void refresh() {
        if (Log.isLoggable(LogTag.APP, 3)) {
            log("refresh", new Object[0]);
        }
        new Thread(new Runnable() {
            public void run() {
                DraftCache.this.rebuildCache();
            }
        }).start();
    }

    public synchronized void removeOnDraftChangedListener(OnDraftChangedListener onDraftChangedListener) {
        if (Log.isLoggable(LogTag.APP, 3)) {
            log("removeOnDraftChangedListener " + onDraftChangedListener, new Object[0]);
        }
        this.mChangeListeners.remove(onDraftChangedListener);
    }

    public synchronized void setDraftState(long j, boolean z) {
        if (j > 0) {
            boolean add = z ? this.mDraftSet.add(Long.valueOf(j)) : this.mDraftSet.remove(Long.valueOf(j));
            if (Log.isLoggable(LogTag.APP, 3)) {
                log("setDraftState: tid=" + j + ", value=" + z + ", changed=" + add, new Object[0]);
            }
            if (Log.isLoggable(LogTag.APP, 2)) {
                dump();
            }
            if (add) {
                Iterator<OnDraftChangedListener> it = this.mChangeListeners.iterator();
                while (it.hasNext()) {
                    it.next().onDraftChanged(j, z);
                }
            }
        }
    }
}
