package vc.lx.sms.util;

import android.util.Log;

public class LogTool {
    public static final boolean MOCK = false;
    public static final boolean OLD_API = true;
    public static final boolean PLAY_SONG = true;
    public static final boolean V = true;
    public static final boolean WAP_PROXY = false;

    public static void e(String str, Exception exc) {
        if (exc != null) {
            Log.e(str, exc.getMessage());
        }
    }

    public static void i(String str) {
        i("LEXIN", str);
    }

    public static void i(String str, String str2) {
        if (str2 != null) {
            Log.i(str, str2);
        }
    }
}
