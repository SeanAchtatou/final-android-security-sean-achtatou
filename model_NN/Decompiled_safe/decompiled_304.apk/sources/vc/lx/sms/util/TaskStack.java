package vc.lx.sms.util;

import java.util.Stack;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class TaskStack {
    private final ScheduledThreadPoolExecutor mAsyncLoader = new ScheduledThreadPoolExecutor(1);
    private final Runnable mPopStackRunnable = new Runnable() {
        public void run() {
            Runnable runnable = null;
            synchronized (TaskStack.this.mThingsToLoad) {
                if (!TaskStack.this.mThingsToLoad.empty()) {
                    runnable = (Runnable) TaskStack.this.mThingsToLoad.pop();
                }
            }
            if (runnable != null) {
                runnable.run();
            }
        }
    };
    /* access modifiers changed from: private */
    public final Stack<Runnable> mThingsToLoad = new Stack<>();

    public void clear() {
        synchronized (this.mThingsToLoad) {
            this.mThingsToLoad.clear();
        }
    }

    public void push(Runnable runnable) {
        synchronized (this.mThingsToLoad) {
            this.mThingsToLoad.push(runnable);
        }
        this.mAsyncLoader.execute(this.mPopStackRunnable);
    }
}
