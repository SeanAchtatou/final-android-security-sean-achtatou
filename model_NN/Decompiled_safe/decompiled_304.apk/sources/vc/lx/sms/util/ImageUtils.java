package vc.lx.sms.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.os.Build;
import java.io.FileOutputStream;
import vc.lx.sms2.R;

public class ImageUtils {
    static PorterDuff.Mode[] modes = {PorterDuff.Mode.DARKEN, PorterDuff.Mode.LIGHTEN, PorterDuff.Mode.MULTIPLY, PorterDuff.Mode.SCREEN};

    private ImageUtils() {
    }

    public static Bitmap blackAndWhite(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        paint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{1.2f, 0.0f, 0.0f, 0.0f, -10.0f, 1.2f, 0.0f, 0.0f, 0.0f, -10.0f, 1.2f, 0.0f, 0.0f, 0.0f, -10.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f})));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return createBitmap;
    }

    public static BitmapFactory.Options getBitmapDims(String str) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        return options;
    }

    private static int getClosestResampleSize(int i, int i2, int i3) {
        int i4;
        int max = Math.max(i, i2);
        int i5 = 1;
        while (true) {
            if (i5 >= Integer.MAX_VALUE) {
                i4 = i5;
                break;
            } else if (i5 * i3 > max) {
                i4 = i5 - 1;
                break;
            } else {
                i5++;
            }
        }
        if (i4 > 0) {
            return i4;
        }
        return 1;
    }

    private static BitmapFactory.Options getResampling(int i, int i2, int i3) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        float f = i > i2 ? ((float) i3) / ((float) i) : i2 > i ? ((float) i3) / ((float) i2) : ((float) i3) / ((float) i);
        options.outWidth = (int) ((((float) i) * f) + 0.5f);
        options.outHeight = (int) ((f * ((float) i2)) + 0.5f);
        return options;
    }

    public static Bitmap lomo(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        paint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{1.0f, 0.1f, 0.4f, 0.1f, 0.0f, 0.1f, 1.0f, 0.2f, 0.0f, 0.0f, 0.05f, 0.1f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f})));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(modes[2]));
        paint.setColorFilter(null);
        paint.setShader(new RadialGradient((float) (width / 2), (float) (height / 2), (float) height, new int[]{-1, -5991, -5850159, -8061127}, (float[]) null, Shader.TileMode.CLAMP));
        canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, paint);
        return createBitmap;
    }

    public static Bitmap oldPhoto(Bitmap bitmap, Context context) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        paint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{1.2f, 0.8f, 0.5f, 1.0f, -200.0f, 0.0f, 1.3f, 0.9f, 0.0f, -20.0f, 1.2f, 0.8f, 1.0f, 0.0f, -200.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f})));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(modes[2]));
        paint.setColorFilter(null);
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), R.drawable.mask_old_photo);
        paint.setShader(new BitmapShader(decodeResource, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
        canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, paint);
        if (decodeResource != null && !decodeResource.isRecycled()) {
            decodeResource.recycle();
        }
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resampleImage(Bitmap bitmap, int i) throws Exception {
        Matrix matrix = new Matrix();
        if (bitmap.getWidth() > i || bitmap.getHeight() > i) {
            BitmapFactory.Options resampling = getResampling(bitmap.getWidth(), bitmap.getHeight(), i);
            matrix.postScale(((float) resampling.outWidth) / ((float) bitmap.getWidth()), ((float) resampling.outHeight) / ((float) bitmap.getHeight()));
        }
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resampleImage(String str, int i) throws Exception {
        int exifRotation;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = getClosestResampleSize(options.outWidth, options.outHeight, i);
        Bitmap decodeFile = BitmapFactory.decodeFile(str, options2);
        Matrix matrix = new Matrix();
        if (decodeFile.getWidth() > i || decodeFile.getHeight() > i) {
            BitmapFactory.Options resampling = getResampling(decodeFile.getWidth(), decodeFile.getHeight(), i);
            matrix.postScale(((float) resampling.outWidth) / ((float) decodeFile.getWidth()), ((float) resampling.outHeight) / ((float) decodeFile.getHeight()));
        }
        if (new Integer(Build.VERSION.SDK).intValue() > 4 && (exifRotation = ExifUtils.getExifRotation(str)) != 0) {
            matrix.postRotate((float) exifRotation);
        }
        return Bitmap.createBitmap(decodeFile, 0, 0, decodeFile.getWidth(), decodeFile.getHeight(), matrix, true);
    }

    public static void resampleImageAndSaveToNewLocation(String str, String str2) throws Exception {
        resampleImage(str, 640).compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(str2));
    }

    public static Bitmap whiten(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        paint.setColorFilter(new ColorMatrixColorFilter(new ColorMatrix(new float[]{1.0f, 0.8f, 0.5f, 0.0f, 10.0f, 0.68f, 0.8f, 0.7f, 0.0f, 10.0f, 0.8f, 0.8f, 0.6f, 0.0f, 10.0f, 0.0f, 0.0f, 0.0f, 0.4f, 0.0f})));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(modes[2]));
        paint.setColorFilter(null);
        paint.setShader(new RadialGradient((float) (width / 2), (float) (height / 2), (float) height, new int[]{-1, -1, -2380917, -2380917}, (float[]) null, Shader.TileMode.CLAMP));
        canvas.drawRect(0.0f, 0.0f, (float) width, (float) height, paint);
        return createBitmap;
    }
}
