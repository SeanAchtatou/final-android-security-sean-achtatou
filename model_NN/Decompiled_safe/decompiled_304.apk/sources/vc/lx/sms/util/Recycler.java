package vc.lx.sms.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import com.android.mms.MmsConfig;
import com.android.provider.Telephony;
import vc.lx.sms.ui.MessagingPreferenceActivity;

public abstract class Recycler {
    private static final boolean DEFAULT_AUTO_DELETE = false;
    private static final boolean LOCAL_DEBUG = false;
    private static final String TAG = "Recycler";
    private static MmsRecycler sMmsRecycler;
    private static SmsRecycler sSmsRecycler;

    public static class MmsRecycler extends Recycler {
        private static final String[] ALL_MMS_THREADS_PROJECTION = {"thread_id", "count(*) as msg_count"};
        private static final int COLUMN_ID = 0;
        private static final int COLUMN_MMS_DATE = 2;
        private static final int COLUMN_MMS_READ = 3;
        private static final int COLUMN_THREAD_ID = 1;
        private static final int ID = 0;
        private static final int MESSAGE_COUNT = 1;
        private static final String[] MMS_MESSAGE_PROJECTION = {"_id", "thread_id", "date"};
        private final String MAX_MMS_MESSAGES_PER_THREAD = "MaxMmsMessagesPerThread";

        private void deleteMessagesOlderThanDate(Context context, long j, long j2) {
            long delete = (long) SqliteWrapper.delete(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, "thread_id=" + j + " AND locked=0 AND date<" + j2, null);
        }

        /* access modifiers changed from: protected */
        public boolean anyThreadOverLimit(Context context) {
            long threadId;
            Cursor allThreads = getAllThreads(context);
            int messageLimit = getMessageLimit(context);
            do {
                try {
                    if (allThreads.moveToNext()) {
                        threadId = getThreadId(allThreads);
                    } else {
                        allThreads.close();
                        return false;
                    }
                } finally {
                    allThreads.close();
                }
            } while (SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, MMS_MESSAGE_PROJECTION, "thread_id=" + threadId + " AND locked=0", null, "date DESC").getCount() < messageLimit);
            return true;
        }

        /* access modifiers changed from: protected */
        public void deleteMessagesForThread(Context context, long j, int i) {
            Cursor cursor;
            if (j != 0) {
                try {
                    Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, MMS_MESSAGE_PROJECTION, "thread_id=" + j + " AND locked=0", null, "date DESC");
                    try {
                        if (query.getCount() - i > 0) {
                            query.move(i);
                            long j2 = query.getLong(2);
                            if (query != null) {
                                query.close();
                            }
                            deleteMessagesOlderThanDate(context, j, j2);
                            return;
                        } else if (query != null) {
                            query.close();
                            return;
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        cursor = query;
                        th = th2;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor = null;
                }
            } else {
                return;
            }
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }

        public void deleteOldMessagesInSameThreadAsMessage(Context context, Uri uri) {
            Cursor cursor;
            try {
                Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.CONTENT_URI, MMS_MESSAGE_PROJECTION, "thread_id in (select thread_id from pdu where _id=" + uri.getLastPathSegment() + ") AND locked=0", null, "date DESC");
                try {
                    int count = query.getCount();
                    int messageLimit = getMessageLimit(context);
                    if (count - messageLimit > 0) {
                        query.move(messageLimit);
                        long j = query.getLong(2);
                        long j2 = query.getLong(1);
                        if (query != null) {
                            query.close();
                        }
                        if (j2 != 0) {
                            deleteMessagesOlderThanDate(context, j2, j);
                            return;
                        }
                        return;
                    } else if (query != null) {
                        query.close();
                        return;
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = query;
                    th = th2;
                }
            } catch (Throwable th3) {
                th = th3;
                cursor = null;
            }
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }

        /* access modifiers changed from: protected */
        public void dumpMessage(Cursor cursor, Context context) {
            cursor.getLong(0);
        }

        /* access modifiers changed from: protected */
        public Cursor getAllThreads(Context context) {
            return SqliteWrapper.query(context, context.getContentResolver(), Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "threads"), ALL_MMS_THREADS_PROJECTION, null, null, "date DESC");
        }

        public int getMessageLimit(Context context) {
            return PreferenceManager.getDefaultSharedPreferences(context).getInt("MaxMmsMessagesPerThread", MmsConfig.getDefaultMMSMessagesPerThread());
        }

        /* access modifiers changed from: protected */
        public long getThreadId(Cursor cursor) {
            return cursor.getLong(0);
        }

        public void setMessageLimit(Context context, int i) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
            edit.putInt("MaxMmsMessagesPerThread", i);
            edit.commit();
        }
    }

    public static class SmsRecycler extends Recycler {
        private static final String[] ALL_SMS_THREADS_PROJECTION = {"thread_id", Telephony.Sms.Conversations.MESSAGE_COUNT};
        private static final int COLUMN_ID = 0;
        private static final int COLUMN_SMS_ADDRESS = 2;
        private static final int COLUMN_SMS_BODY = 3;
        private static final int COLUMN_SMS_DATE = 4;
        private static final int COLUMN_SMS_READ = 5;
        private static final int COLUMN_SMS_STATUS = 7;
        private static final int COLUMN_SMS_TYPE = 6;
        private static final int COLUMN_THREAD_ID = 1;
        private static final int ID = 0;
        private static final int MESSAGE_COUNT = 1;
        private static final String[] SMS_MESSAGE_PROJECTION = {"_id", "thread_id", "address", Telephony.TextBasedSmsColumns.BODY, "date", "read", "type", "status"};
        private final String MAX_SMS_MESSAGES_PER_THREAD = "MaxSmsMessagesPerThread";

        /* access modifiers changed from: protected */
        public boolean anyThreadOverLimit(Context context) {
            Cursor allThreads = getAllThreads(context);
            int messageLimit = getMessageLimit(context);
            do {
                try {
                    if (allThreads.moveToNext()) {
                    } else {
                        allThreads.close();
                        return false;
                    }
                } finally {
                    allThreads.close();
                }
            } while (SqliteWrapper.query(context, context.getContentResolver(), ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, getThreadId(allThreads)), SMS_MESSAGE_PROJECTION, "locked=0", null, "date DESC").getCount() < messageLimit);
            return true;
        }

        /* access modifiers changed from: protected */
        public void deleteMessagesForThread(Context context, long j, int i) {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor query = SqliteWrapper.query(context, contentResolver, ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, j), SMS_MESSAGE_PROJECTION, "locked=0", null, "date DESC");
            if (query.getCount() - i > 0) {
                try {
                    query.move(i);
                    long delete = (long) SqliteWrapper.delete(context, contentResolver, ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, j), "locked=0 AND date<" + query.getLong(4), null);
                } finally {
                    query.close();
                }
            }
        }

        /* access modifiers changed from: protected */
        public void dumpMessage(Cursor cursor, Context context) {
            MessageUtils.formatTimeStampString(context, cursor.getLong(4), true);
        }

        /* access modifiers changed from: protected */
        public Cursor getAllThreads(Context context) {
            return SqliteWrapper.query(context, context.getContentResolver(), Telephony.Sms.Conversations.CONTENT_URI, ALL_SMS_THREADS_PROJECTION, null, null, "date DESC");
        }

        public int getMessageLimit(Context context) {
            return PreferenceManager.getDefaultSharedPreferences(context).getInt("MaxSmsMessagesPerThread", MmsConfig.getDefaultSMSMessagesPerThread());
        }

        /* access modifiers changed from: protected */
        public long getThreadId(Cursor cursor) {
            return cursor.getLong(0);
        }

        public void setMessageLimit(Context context, int i) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
            edit.putInt("MaxSmsMessagesPerThread", i);
            edit.commit();
        }
    }

    public static boolean checkForThreadsOverLimit(Context context) {
        getSmsRecycler();
        getMmsRecycler();
        return true;
    }

    public static MmsRecycler getMmsRecycler() {
        if (sMmsRecycler == null) {
            sMmsRecycler = new MmsRecycler();
        }
        return sMmsRecycler;
    }

    public static SmsRecycler getSmsRecycler() {
        if (sSmsRecycler == null) {
            sSmsRecycler = new SmsRecycler();
        }
        return sSmsRecycler;
    }

    public static boolean isAutoDeleteEnabled(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(MessagingPreferenceActivity.AUTO_DELETE, false);
    }

    /* access modifiers changed from: protected */
    public abstract boolean anyThreadOverLimit(Context context);

    /* access modifiers changed from: protected */
    public abstract void deleteMessagesForThread(Context context, long j, int i);

    public void deleteOldMessages(Context context) {
        if (isAutoDeleteEnabled(context)) {
            Cursor allThreads = getAllThreads(context);
            int messageLimit = getMessageLimit(context);
            while (allThreads.moveToNext()) {
                try {
                    deleteMessagesForThread(context, getThreadId(allThreads), messageLimit);
                } finally {
                    allThreads.close();
                }
            }
        }
    }

    public void deleteOldMessagesByThreadId(Context context, long j) {
        if (isAutoDeleteEnabled(context)) {
            deleteMessagesForThread(context, j, getMessageLimit(context));
        }
    }

    /* access modifiers changed from: protected */
    public abstract void dumpMessage(Cursor cursor, Context context);

    /* access modifiers changed from: protected */
    public abstract Cursor getAllThreads(Context context);

    public abstract int getMessageLimit(Context context);

    public int getMessageMaxLimit() {
        return MmsConfig.getMaxMessageCountPerThread();
    }

    public int getMessageMinLimit() {
        return MmsConfig.getMinMessageCountPerThread();
    }

    /* access modifiers changed from: protected */
    public abstract long getThreadId(Cursor cursor);

    public abstract void setMessageLimit(Context context, int i);
}
