package vc.lx.sms.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.android.mms.transaction.SmsMessageSender;
import com.android.provider.Telephony;
import com.flurry.android.FlurryAgent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import org.apache.http.NameValuePair;
import vc.lx.sms.cmcc.GlobalSettingParameter;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.richtext.ui.VoteDetailActivity;
import vc.lx.sms.service.NetworkTaskIntentService;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.ui.MessagingPreferenceActivity;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class Util {
    public static void SetLastTimeOfRecommendingMusicValue(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).edit();
        edit.putString(PrefsUtil.KEY_LAST_TIME_OF_RECOMMENDING_MUSIC, str);
        edit.commit();
    }

    public static void broadcastProgressDlg(Context context, int i, String str) {
        Intent intent = new Intent();
        intent.setAction(PrefsUtil.INTENT_PROGRESS_DLG);
        intent.putExtra(PrefsUtil.KEY_PROGRESS_DLG, i);
        intent.putExtra(PrefsUtil.KEY_PROGRESS_MSG, str);
        intent.putExtras(new Bundle());
        context.sendBroadcast(intent);
    }

    public static void broadcastProgressDlgClose(Context context, String str) {
        broadcastProgressDlg(context, 1, str);
    }

    public static void broadcastProgressDlgOpen(Context context, String str) {
        broadcastProgressDlg(context, 0, str);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String byteArrayToHexString(byte[] r5) {
        /*
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            int r1 = r5.length
            int r1 = r1 * 2
            r0.<init>(r1)
            if (r5 == 0) goto L_0x003a
            r1 = 0
        L_0x000b:
            int r2 = r5.length
            if (r1 >= r2) goto L_0x003a
            byte r2 = r5[r1]
            if (r2 >= 0) goto L_0x0014
            int r2 = r2 + 256
        L_0x0014:
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            int r3 = r2.length()
            int r3 = r3 % 2
            r4 = 1
            if (r3 != r4) goto L_0x0034
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "0"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
        L_0x0034:
            r0.append(r2)
            int r1 = r1 + 1
            goto L_0x000b
        L_0x003a:
            java.lang.String r0 = r0.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.util.Util.byteArrayToHexString(byte[]):java.lang.String");
    }

    public static boolean checkNetWork(Context context) {
        NetworkInfo[] allNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
            for (NetworkInfo state : allNetworkInfo) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkWapStatus(Context context) {
        NetworkInfo[] allNetworkInfo;
        String extraInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
            for (int i = 0; i < allNetworkInfo.length; i++) {
                if (allNetworkInfo[i].getState() == NetworkInfo.State.CONNECTED && (extraInfo = allNetworkInfo[i].getExtraInfo()) != null && extraInfo.equals("cmwap")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String clearCommentFace(String str) {
        return str == null ? "" : (str.startsWith(":)") || str.startsWith(":D") || str.startsWith(";)") || str.startsWith(">(") || str.startsWith(":(")) ? str.substring(2) : str;
    }

    public static int computeInitialSampleSize(BitmapFactory.Options options, int i, int i2) {
        double d = (double) options.outWidth;
        double d2 = (double) options.outHeight;
        int ceil = i2 == -1 ? 1 : (int) Math.ceil(Math.sqrt((d * d2) / ((double) i2)));
        int min = i == -1 ? 128 : (int) Math.min(Math.floor(d / ((double) i)), Math.floor(d2 / ((double) i)));
        if (min < ceil) {
            return ceil;
        }
        if (i2 == -1 && i == -1) {
            return 1;
        }
        return i == -1 ? ceil : min;
    }

    public static int computeSampleSize(BitmapFactory.Options options, int i, int i2) {
        int computeInitialSampleSize = computeInitialSampleSize(options, i, i2);
        if (computeInitialSampleSize > 8) {
            return ((computeInitialSampleSize + 7) / 8) * 8;
        }
        int i3 = 1;
        while (i3 < computeInitialSampleSize) {
            i3 <<= 1;
        }
        return i3;
    }

    public static String createAndReturnFolderName(Context context, long j) {
        String str = context.getApplicationInfo().dataDir + "/tmp";
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return str;
        }
        String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        StatFs statFs = new StatFs(absolutePath);
        statFs.restat(absolutePath);
        return j < ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()) ? absolutePath + "/12530" : str;
    }

    public static void createCommentDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity, R.style.CustomDialogTheme);
        View inflate = LayoutInflater.from(activity).inflate((int) R.layout.feed_back_dlg, (ViewGroup) null);
        final EditText editText = (EditText) inflate.findViewById(R.id.feed_comment);
        final EditText editText2 = (EditText) inflate.findViewById(R.id.input_contact_phone);
        final Button button = (Button) inflate.findViewById(R.id.yes);
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    button.setClickable(true);
                    button.setPressed(false);
                    return;
                }
                button.setPressed(true);
                button.setClickable(false);
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        editText2.setFocusable(true);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String trim = editText.getText().toString().trim();
                editText2.getText().toString().trim();
                if (trim.length() > 0) {
                    dialog.dismiss();
                }
            }
        });
        button.setPressed(true);
        button.setClickable(false);
        ((Button) inflate.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setContentView(inflate);
        dialog.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public static Bitmap createReflectedImage(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, height / 4, width, height / 4, matrix, false);
        Bitmap createBitmap2 = Bitmap.createBitmap(width, (height / 2) + height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap2);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (height + 4), new Paint());
        canvas.drawBitmap(createBitmap, 0.0f, (float) (height + 4), (Paint) null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) bitmap.getHeight(), 0.0f, (float) (createBitmap2.getHeight() + 4), 1895825407, 16777215, Shader.TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (createBitmap2.getHeight() + 4), paint);
        return createBitmap2;
    }

    public static Bitmap decodeFile(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        options.inSampleSize = computeSampleSize(options, -1, 409600);
        options.inJustDecodeBounds = false;
        try {
            return BitmapFactory.decodeFile(str, options);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public static void fillFile(long j, File file) throws Exception {
        if (j > 0) {
            byte[] bArr = new byte[2048];
            RandomAccessFile randomAccessFile = null;
            try {
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
                long j2 = j;
                while (j2 > 0) {
                    try {
                        randomAccessFile2.write(bArr, 0, (int) (j2 > 2048 ? 2048 : j2));
                        j2 -= 2048;
                    } catch (Exception e) {
                        e = e;
                        randomAccessFile = randomAccessFile2;
                        try {
                            throw e;
                        } catch (Throwable th) {
                            th = th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        randomAccessFile = randomAccessFile2;
                        if (randomAccessFile != null) {
                            try {
                                randomAccessFile.close();
                            } catch (IOException e2) {
                            }
                        }
                        throw th;
                    }
                }
                if (randomAccessFile2 != null) {
                    try {
                        randomAccessFile2.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (Exception e4) {
                e = e4;
                throw e;
            }
        }
    }

    public static String fullUrl(Context context, String str) {
        return context.getString(R.string.dna_engine_host) + str;
    }

    public static int getAmountOfUsageValue(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).getInt(PrefsUtil.KEY_AMOUNT_OF_USAGE, 0);
    }

    public static int getCurrentMusicVersion(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_MUSIC_VERSION, 0).getInt(MessagingPreferenceActivity.MUSIC_VERSION_VALUE, 1);
    }

    public static String getDeviceImie(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        return deviceId != null ? deviceId : PrefsUtil.getOrCreateDeviceUid(context);
    }

    public static String getFullShortenImageUrl(Context context, String str) {
        return TextUtils.join("/", new String[]{context.getString(R.string.shorten_url_engine_host), "m", str});
    }

    public static String getFullShortenUrl(Context context, String str) {
        return TextUtils.join("/", new String[]{context.getString(R.string.shorten_url_engine_host), "s", str});
    }

    public static String getFullShortenVoteUrl(Context context, String str) {
        return TextUtils.join("/", new String[]{context.getString(R.string.dna_vote_engine_host_version_b), Telephony.BaseMmsColumns.MMS_VERSION, str});
    }

    public static String getFullUrl(Context context, String str) {
        return TextUtils.join("", new String[]{context.getString(R.string.dna_engine_host), str});
    }

    public static String getLastTimeOfRecommendingMusicValue(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).getString(PrefsUtil.KEY_LAST_TIME_OF_RECOMMENDING_MUSIC, "");
    }

    public static String getMD5(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return byteArrayToHexString(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getMobileModel(Context context) {
        return context.getString(R.string.sms_version_str) + String.valueOf(getVersionName(context)) + "," + Build.MODEL.replaceAll(" ", "") + ",SDK version : " + Build.VERSION.RELEASE + "," + getNetWorkInfo(context);
    }

    public static String getNetWorkInfo(Context context) {
        NetworkInfo[] allNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
            for (int i = 0; i < allNetworkInfo.length; i++) {
                if (allNetworkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                    return allNetworkInfo[i].getTypeName();
                }
            }
        }
        return "";
    }

    public static boolean getNotificationValue(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).getBoolean(PrefsUtil.KEY_NOTIFICATION_VALUE, true);
    }

    public static boolean getPhotoMusicNotification(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_APP_SETTING, 0).getBoolean(MessagingPreferenceActivity.POP_RECOMMEND_SONG_MODE, true);
    }

    public static boolean getPopupNotification(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_POPUP_NOTIFICATION, 0).getBoolean(MessagingPreferenceActivity.POP_RECOMMEND_POPUP_MODE, true);
    }

    public static String getRandKey(String str, String str2) {
        return getMD5((str + str2 + GlobalSettingParameter.LOCAL_PARAM_ENCRYPT_KEY).getBytes());
    }

    public static String getRandKey(String str, String str2, String str3) {
        return getMD5((str + str2 + str3 + GlobalSettingParameter.LOCAL_PARAM_ENCRYPT_KEY).getBytes());
    }

    public static CharSequence getRelativeDateSpanString(long j, Context context) {
        return DateUtils.formatDateTime(context, j, 16);
    }

    public static CharSequence getRelativeTimeSpanString(long j) {
        return DateUtils.getRelativeTimeSpanString(j, new Date().getTime(), 60000, 262144);
    }

    public static String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public static boolean getShakeValue(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).getBoolean(PrefsUtil.KEY_SHAKE_VALUE, true);
    }

    public static boolean getSongValue(Context context) {
        return context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).getBoolean(PrefsUtil.KEY_SOUND_VALUE, true);
    }

    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("vc.lx.sms2", 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Util - Get Package Info", e.getMessage());
            return "-1";
        }
    }

    public static File initCacheFile(long j, Context context, String str) throws Exception {
        String createAndReturnFolderName = createAndReturnFolderName(context, j);
        File file = new File(createAndReturnFolderName);
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(createAndReturnFolderName + "/" + str);
        file2.deleteOnExit();
        if (file2.exists()) {
            file2.delete();
        }
        file2.createNewFile();
        if (!file2.exists()) {
            throw new FileNotFoundException("can not find " + file2.getAbsolutePath());
        }
        fillFile(j, file2);
        return file2;
    }

    public static boolean isCMWapConnected(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null) {
            return false;
        }
        return activeNetworkInfo.getExtraInfo().equals("cmwap");
    }

    public static void logEvent(String str, NameValuePair... nameValuePairArr) {
        HashMap hashMap = new HashMap();
        hashMap.put("channel", AntUtil.LOGGINGCHANEL);
        for (NameValuePair nameValuePair : nameValuePairArr) {
            hashMap.put(nameValuePair.getName(), nameValuePair.getValue());
        }
        FlurryAgent.logEvent(str, hashMap);
    }

    public static String randomUUID() {
        int length = "1234567890qwertyuiopasdfghjklzxcvbnm".length();
        Random random = new Random(System.currentTimeMillis());
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 5; i++) {
            stringBuffer.append("1234567890qwertyuiopasdfghjklzxcvbnm".charAt(random.nextInt(length)));
        }
        return stringBuffer.toString();
    }

    public static void sendPlayingNotification(Context context, int i, String str) {
        Intent intent = new Intent();
        if (PrefsUtil.KEY_TARGET_CONVERSAION_HEAD.equals(str) || PrefsUtil.KEY_TARGET_MUSIC_SEARCH.equals(str) || PrefsUtil.KEY_TARGET_MUSIC_BOARDS.equals(str) || PrefsUtil.KEY_TARGET_CONVERSAION_POP.equals(str)) {
            intent.setAction(PrefsUtil.INTENT_UPDATE_PLAYING_STATUS);
            Bundle bundle = new Bundle();
            bundle.putInt(PrefsUtil.KEY_PLAYING_STATUS, i);
            bundle.putString(PrefsUtil.KEY_SONG_TARGET, str);
            intent.putExtras(bundle);
            context.sendBroadcast(intent);
        } else if (PrefsUtil.KEY_TARGET_CONVERSAION_ITEM.equals(str)) {
            intent.setAction(PrefsUtil.INTENT_CONVERSATION_PLAY_SONG_STATUS);
            Bundle bundle2 = new Bundle();
            bundle2.putInt(PrefsUtil.KEY_PLAYING_STATUS, i);
            bundle2.putString(PrefsUtil.KEY_SONG_TARGET, str);
            intent.putExtras(bundle2);
            context.sendBroadcast(intent);
        }
    }

    public static void sendSMS(String str, String str2, Context context) {
        sendSMS(str, str2, context, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation
     arg types: [android.content.Context, vc.lx.sms.data.ContactList, int]
     candidates:
      vc.lx.sms.data.Conversation.get(android.content.Context, long, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, android.net.Uri, boolean):vc.lx.sms.data.Conversation
      vc.lx.sms.data.Conversation.get(android.content.Context, vc.lx.sms.data.ContactList, boolean):vc.lx.sms.data.Conversation */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
     arg types: [java.lang.String, int, int, android.content.Context]
     candidates:
      vc.lx.sms.data.ContactList.getByNumbers(java.util.ArrayList<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.util.List<java.lang.String>, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList
      vc.lx.sms.data.ContactList.getByNumbers(java.lang.String, boolean, boolean, android.content.Context):vc.lx.sms.data.ContactList */
    public static void sendSMS(String str, String str2, Context context, boolean z) {
        Conversation conversation = Conversation.get(context, ContactList.getByNumbers(str, false, true, context), false);
        long ensureThreadId = conversation.ensureThreadId();
        try {
            new SmsMessageSender(context, conversation.getRecipients().getNumbers(), str2, ensureThreadId).sendMessage(ensureThreadId);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(context, ensureThreadId);
        } catch (Exception e) {
            Log.e("Redcloud Util Send SMS", "Failed to send SMS message, threadId=" + ensureThreadId, e);
        }
        if (z) {
            Toast.makeText(context, (int) R.string.sms_message_sent, 1).show();
        }
    }

    public static void setAmountOfUsageValue(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).edit();
        edit.putInt(PrefsUtil.KEY_AMOUNT_OF_USAGE, i);
        edit.commit();
    }

    public static void setCurrentMusicServiceVersion(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_MUSIC_VERSION, 0).edit();
        edit.putInt(MessagingPreferenceActivity.MUSIC_VERSION_VALUE, i);
        edit.commit();
    }

    public static void setNotificationTimer(Context context) {
        ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + 240000, PendingIntent.getBroadcast(context, 0, new Intent(PrefsUtil.INTENT_NOTIFICATION_TIMER), 268435456));
    }

    public static void setNotificationValue(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).edit();
        edit.putBoolean(PrefsUtil.KEY_NOTIFICATION_VALUE, z);
        edit.commit();
    }

    public static void setPhotoMusicNotification(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_APP_SETTING, 0).edit();
        edit.putBoolean(MessagingPreferenceActivity.POP_RECOMMEND_SONG_MODE, z);
        edit.commit();
    }

    public static void setPopupNotification(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_POPUP_NOTIFICATION, 0).edit();
        edit.putBoolean(MessagingPreferenceActivity.POP_RECOMMEND_POPUP_MODE, z);
        edit.commit();
    }

    public static void setShakeValue(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).edit();
        edit.putBoolean(PrefsUtil.KEY_SHAKE_VALUE, z);
        edit.commit();
    }

    public static void setSongValue(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PrefsUtil.KEY_NOTIFICATION_SHAREDPREFERENCES, 0).edit();
        edit.putBoolean(PrefsUtil.KEY_SOUND_VALUE, z);
        edit.commit();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void shareMusicBySongItem(Activity activity, SongItem songItem) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setComponent(new ComponentName(activity, ComposeMessageActivity.class));
        intent.putExtra("forwarded_message", true);
        intent.putExtra("sms_body", getFullShortenUrl(activity, songItem.plug));
        intent.putExtra(SmsSqliteHelper.PLUG, songItem.plug);
        intent.putExtra("song", songItem.song);
        intent.putExtra("singer", songItem.singer);
        intent.setClassName(activity, "com.android.mms.ui.ForwardMessageActivity");
        activity.startActivity(intent);
    }

    public static void songFavoriteAction(Context context, boolean z, String str) {
        Intent intent = new Intent(context, NetworkTaskIntentService.class);
        intent.putExtra(PrefsUtil.KEY_SONG_ACTION, z ? 1 : 2);
        intent.putExtra(PrefsUtil.KEY_SONGITEM_PLUG, str);
        context.startService(intent);
    }

    public static void voteNotification(VoteItem voteItem, String str, Context context) {
        NotificationManager notificationManager = SmsApplication.getInstance().getmNotificationManager();
        Notification notification = new Notification(R.drawable.icon, str, System.currentTimeMillis());
        Intent intent = null;
        if (voteItem != null) {
            intent = new Intent(context, VoteDetailActivity.class);
            intent.setAction(PrefsUtil.INTENT_NEW_FOLLOWING);
            intent.putExtra(PrefsUtil.KEY_VOTE_ID, voteItem.cli_id);
            intent.putExtra(PrefsUtil.KEY_VOTE_COUNTER, voteItem.counter);
            intent.putExtra(PrefsUtil.KEY_VOTE_CONTACT_NAME, voteItem.contact_name);
            intent.putExtra(PrefsUtil.KEY_VOTE_TITLE, voteItem.title);
            intent.putExtra(PrefsUtil.KEY_VOTE_IS_CLICK, "true");
        }
        PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 0);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.vote_notification);
        notification.contentView = remoteViews;
        remoteViews.setTextViewText(R.id.info, str);
        notification.contentIntent = activity;
        notificationManager.notify(R.layout.vote_notification, notification);
    }
}
