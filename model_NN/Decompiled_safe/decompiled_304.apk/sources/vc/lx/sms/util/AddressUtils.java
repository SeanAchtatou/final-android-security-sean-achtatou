package vc.lx.sms.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.android.provider.Telephony;
import vc.lx.sms2.R;

public class AddressUtils {
    private static final String TAG = "AddressUtils";

    private AddressUtils() {
    }

    public static String getFrom(Context context, Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        Uri.Builder buildUpon = Telephony.Mms.CONTENT_URI.buildUpon();
        buildUpon.appendPath(lastPathSegment).appendPath("addr");
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), buildUpon.build(), new String[]{"address", Telephony.Mms.Addr.CHARSET}, "type=137", null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    query.getString(0);
                }
            } finally {
                query.close();
            }
        }
        return context.getString(R.string.hidden_sender_address);
    }
}
