package vc.lx.sms.util;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.UUID;

public class PrefsUtil {
    public static final String EVENT_APK_UPDATE = "apk_update";
    public static final String EVENT_APP_OPEN = "app_open";
    public static final String EVENT_IMAGE_FORWARD = "image_forward";
    public static final String EVENT_IMAGE_SEND = "image_send";
    public static final String EVENT_MUSIC_APP_OPEN = "music_app_open";
    public static final String EVENT_MUSIC_DELETE = "music_delete";
    public static final String EVENT_MUSIC_FORWARD = "music_forward";
    public static final String EVENT_MUSIC_LIKE = "music_like";
    public static final String EVENT_MUSIC_PLAY = "play";
    public static final String EVENT_MUSIC_SEND = "music_send";
    public static final String EVENT_MUSIC_UNLIKE = "music_unlike";
    public static final String EVENT_SETTINGS_OPEN = "settings_open";
    public static final String EVENT_SETTINGS_POPUP_DISABLE = "settings_popup_disable";
    public static final String EVENT_SETTINGS_POPUP_ENABLE = "settings_popup_enable";
    public static final String EVENT_SMS_POPUP = "sms_popup";
    public static final String EVENT_SMS_POPUP_CLOSE = "sms_popup_close";
    public static final String EVENT_SMS_POPUP_DELETE = "sms_popup_delete";
    public static final String EVENT_SMS_POPUP_DISABLE = "sms_popup_disable";
    public static final String EVENT_SMS_POPUP_QUICK_REPLY = "sms_popup_quick_reply";
    public static final String EVENT_SMS_POPUP_REPLY = "sms_popup_reply";
    public static final String EVENT_SMS_SEND = "sms_send";
    public static final String INTENT_ACCOUNT_MAILBOX = "vc.lx.migu.ACCOUNT_MAILBOX";
    public static final String INTENT_ACCOUNT_MANAGEMENT_TYPE = "vc.lx.migu.ACCOUNT_MANAGEMENT_TYPE";
    public static final String INTENT_ACCOUNT_PASSWORD = "vc.lx.migu.ACCOUNT_PASSWORD";
    public static final String INTENT_COMMENTS_UPDATE = "vc.lx.migu.COMMENTS_UPDATE";
    public static final String INTENT_CONVERSATION_PLAY_SONG_STATUS = "vc.lx.migu.CONVERSATION_PLAY_SONG_STATUS";
    public static final String INTENT_FAV_SONG = "vc.lx.migu.FAV_SONG";
    public static final String INTENT_GET_DOWNLOAD_INFO = "vc.lx.migu.GET_DOWNLOAD_INFO";
    public static final String INTENT_GET_RECOMMEND_SONG = "vc.lx.migu.GET_RECOMEND_SONG";
    public static final String INTENT_IMAGE_FORWARD = "vc.lx.migu.INTENT_IMAGE_FORWARD";
    public static final String INTENT_IMAGE_UPLOAD = "vc.lx.migu.INTENT_IMAGE_UPLOAD";
    public static final String INTENT_LOADING_DATA = "vc.lx.migu.LOADING_DATA";
    public static final String INTENT_MUSIC_UPDATE_COUNT = "vc.lx.migu.MUSIC_UPDATE_COUNT";
    public static final String INTENT_NEW_FOLLOWING = "vc.lx.migu.NEW_FOLLOWING";
    public static final String INTENT_NEW_VERSION = "vc.lx.migu.NEW_VERSION";
    public static final String INTENT_NOTIFICATION_TIMER = "vc.lx.migu.services.NOTIFICATION_TIMER";
    public static final String INTENT_PLAY_NEXT_SONG = "vc.lx.migu.PLAY_NEXT_SONG";
    public static final String INTENT_PLAY_NOTIFICATION = "vc.lx.migu.PLAY_NOTIFICATION";
    public static final String INTENT_PLAY_SONG = "vc.lx.migu.PLAY_SONG";
    public static final String INTENT_PLAY_SONG_STATUS = "vc.lx.migu.PLAY_SONG_STATUS";
    public static final String INTENT_PROGRESS_DLG = "vc.lx.migu.PROGRESS_DLG";
    public static final String INTENT_RAIOD_IMAGE_FINISH = "vc.lx.migu.RADIO_IMAGE_FINISH";
    public static final String INTENT_SELECTED_DNA_TYPE = "vc.lx.migu.SELECTED_DNA_TYPE";
    public static final String INTENT_SERVICE_INFO_UPDATE = "vc.lx.migu.INTENT_SERVICE_INFO_UPDATE";
    public static final String INTENT_UNFAV_SONG = "vc.lx.migu.UNFAV_SONG";
    public static final String INTENT_UPDATE_APK = "vc.lx.migu.UPDATE_APK";
    public static final String INTENT_UPDATE_DOWNLOAD_LIST = "vc.lx.migu.UPDATE_DOWNLOAD_LIST";
    public static final String INTENT_UPDATE_PLAYING_STATUS = "vc.lx.migu.UPDATE_PLAYING_STATUS";
    public static final String INTENT_UPDATE_SONG_INFO = "vc.lx.migu.UPDATE_SONG_INFO";
    public static final String INTENT_UPDATE_STATE = "vc.lx.migu.UPDATE_STATE";
    public static final String INTEN_ACCOUNT_NEW_PASSWORD = "vc.lx.migu.ACCOUNT_NEW_PASSWORD";
    public static final String KEY_ACTION_TYPE = "action_type";
    public static final String KEY_AMOUNT_OF_USAGE = "amount_of_usage";
    public static final String KEY_APP_MUSIC_VISIBLE = "app_music_visible";
    public static final String KEY_APP_SETTING = "app_setting";
    public static final String KEY_CHANGE_WIZARD = "change_wizard";
    public static final String KEY_CM_ID = "cm_id";
    public static final String KEY_CM_UID = "cm_uid";
    public static final String KEY_COME_FORM_TAB = "come_form_tab";
    public static final String KEY_COMMENTS = "comments";
    public static final String KEY_CONTACT_NUMBER = "contact_number";
    public static final String KEY_DOWNLOAD_PERCENT = "download_percent";
    public static final String KEY_FILE_PATH = "file_path";
    public static final String KEY_FORCE = "apk_force";
    public static final String KEY_GENERAL_RESP = "general_resp";
    public static final String KEY_INVITE_BY_SMS = "invite_by_sms";
    public static final String KEY_IS_FOLLOWINGS = "is_followings";
    public static final String KEY_IS_NEW_NOTIFICATION = "is_new_notification";
    public static final String KEY_IS_SYS_SETTING = "is_sys_setting";
    public static final String KEY_IS_UPDATE = "is_update";
    public static final String KEY_LAST_TIME_OF_RECOMMENDING_MUSIC = "last_time_of_recommending_music";
    public static final String KEY_LATEST_VERSION = "get_latest_version";
    public static final String KEY_LOADING_COMMENT_FAIL = "key_loading_comment_fail";
    public static final String KEY_LOCAL_SONG_DATE_MODIFIED = "song_date_modified";
    public static final String KEY_LOCAL_SONG_POSITION = "song_position";
    public static final String KEY_MSG_ID = "msg_id";
    public static final String KEY_MSG_TYPE = "msg_type";
    public static final String KEY_MUSIC_VERSION = "music_version";
    public static final String KEY_NOTIFICATION_INFO = "notification_user";
    public static final String KEY_NOTIFICATION_SHAREDPREFERENCES = "notifiaction_sharedpreferences";
    public static final String KEY_NOTIFICATION_TIME = "notification_time";
    public static final String KEY_NOTIFICATION_USER = "key_notification_user";
    public static final String KEY_NOTIFICATION_USER_ID = "notification_user_id";
    public static final String KEY_NOTIFICATION_VALUE = "notifiaction_value";
    public static final String KEY_PAGE = "page";
    public static final String KEY_PLAYING_STATUS = "playing_status";
    public static final String KEY_POPUP_NOTIFICATION = "popup_music";
    public static final String KEY_PROGRESS_DLG = "progress_dlg";
    public static final String KEY_PROGRESS_MSG = "progress_msg";
    public static final String KEY_RECOMMEND_COUNT = "recommend_count";
    public static final String KEY_RECOMMEND_SONG = "recommend_song";
    public static final String KEY_ROW_ID = "row_id";
    public static final String KEY_SERVICE_INFO = "service_info";
    public static final String KEY_SHAKE_VALUE = "shake_value";
    public static final String KEY_SMS_CONTENT = "sms_content";
    public static final String KEY_SMS_SEARCH_KEY = "sms_search_key";
    public static final String KEY_SONG = "song";
    public static final String KEY_SONGITEM_ISPLAY = "songitem_isplay";
    public static final String KEY_SONGITEM_PLUG = "songitem_plug";
    public static final String KEY_SONG_ACTION = "song_action";
    public static final String KEY_SONG_CONTENTID = "song_contentid";
    public static final String KEY_SONG_FILE_PAHT = "song_file_path";
    public static final String KEY_SONG_GROUPCODE = "song_groupcode";
    public static final String KEY_SONG_INFO = "song_info";
    public static final String KEY_SONG_ITEM = "song_item";
    public static final String KEY_SONG_SINGER = "song_singer";
    public static final String KEY_SONG_TARGET = "song_target";
    public static final String KEY_SONG_TITLE = "song_title";
    public static final String KEY_SONG_TYPE = "song_type";
    public static final String KEY_SOUND_VALUE = "sound_value";
    public static final String KEY_TARGET = "target";
    public static final String KEY_TARGET_CONVERSAION_ADD = "key_target_conversation_add";
    public static final String KEY_TARGET_CONVERSAION_HEAD = "key_target_conversation_head";
    public static final String KEY_TARGET_CONVERSAION_ITEM = "key_target_conversation_item";
    public static final String KEY_TARGET_CONVERSAION_POP = "key_target_conversation_pop";
    public static final String KEY_TARGET_MUSIC_BOARDS = "HottestBoard";
    public static final String KEY_TARGET_MUSIC_SEARCH = "SearchBoard";
    public static final String KEY_TEMPLATE_CATEGORY_ID = "template_category_id";
    public static final String KEY_TEMPLATE_CATEGORY_NAME = "template_category_name";
    public static final String KEY_TEMPLATE_FROM = "template_from";
    public static final String KEY_TEMPLATE_RETURNED = "template_returned";
    public static final String KEY_TOTAL_COUNT = "total_count";
    public static final String KEY_TOTAL_PAGES = "total_pages";
    public static final String KEY_UPDATE_APK = "update_apk";
    public static final String KEY_UPDATE_COUNT = "update_count";
    public static final String KEY_UPLOAD_STATUS = "upload_status";
    public static final String KEY_USER = "user";
    public static final String KEY_VERSION_CODE = "apk_version_code";
    public static final String KEY_VOTE_CONTACT_NAME = "vote_contact_name";
    public static final String KEY_VOTE_COUNTER = "vote_counter";
    public static final String KEY_VOTE_ID = "vote_idKEY_VOTE_ID";
    public static final String KEY_VOTE_IS_CLICK = "vote_is_click";
    public static final String KEY_VOTE_TITLE = "vote_title";
    public static final String PREFS_FAVORITE_NAME = "MIGU_FAVORITE_MUSIC";
    public static final String PREFS_NAME = "MIGU_MUSIC";
    public static final String PREF_BACK_TO_MAIN = "BACKTOMAIN";
    public static final String PREF_BOARD_FOCUS_PLAY_STATUS = "play_status";
    public static final String PREF_BOARD_FOCUS_PLUG = "board_plug";
    public static final String PREF_BOARD_ID = "board_id";
    public static final String PREF_BOARD_NAME = "board_name";
    public static final String PREF_DEVICE_UID = "device_uid";
    public static final String PREF_DNA_TYPE_SELECTION = "pref_dna_type_selection";
    public static final String PREF_LAST_FAVORITE_UPDATE_DATE = "last_favorite_update_date";
    public static final String PREF_LAST_POPUP_MSG_DATE = "last_popup_msg_date";
    public static final String PREF_MAILBOX = "MAILBOX";
    public static final String PREF_MUSIC_SELECTION_NAME = "music_selection_name";
    public static final String PREF_PASSWORD = "PASSWORD";
    public static final String PREF_RADIO_FLIP_FLAG = "RADIO_FLIP_FLAG";
    public static final int TYPE_CHANGE_ACCOUNT = 3;
    public static final int TYPE_CREATE_ACCOUNT = 1;
    public static final int TYPE_MODIFY_ACCOUNT = 2;

    public static boolean getFlagOfBackRadio(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0).getBoolean(PREF_BACK_TO_MAIN, false);
    }

    public static boolean getFlagOfFlipHint(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0).getBoolean(PREF_RADIO_FLIP_FLAG, false);
    }

    public static synchronized long getLastFavoriteUpdateDate(Context context) {
        long j;
        synchronized (PrefsUtil.class) {
            j = context.getSharedPreferences(PREFS_FAVORITE_NAME, 0).getLong(PREF_LAST_FAVORITE_UPDATE_DATE, 0);
        }
        return j;
    }

    public static synchronized long getLastPopupMsgDate(Context context) {
        long j;
        synchronized (PrefsUtil.class) {
            j = context.getSharedPreferences(PREFS_NAME, 0).getLong(PREF_LAST_POPUP_MSG_DATE, -1);
        }
        return j;
    }

    public static String getMailBox(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0).getString(PREF_MAILBOX, "");
    }

    public static String getOrCreateDeviceUid(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        String string = sharedPreferences.getString(PREF_DEVICE_UID, null);
        if (string != null) {
            return string;
        }
        String uuid = UUID.randomUUID().toString();
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(PREF_DEVICE_UID, uuid);
        edit.commit();
        return uuid;
    }

    public static String getPassword(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0).getString(PREF_PASSWORD, "");
    }

    public static int getTypeSelection(Context context) {
        return context.getSharedPreferences(PREFS_NAME, 0).getInt(PREF_DNA_TYPE_SELECTION, 0);
    }

    public static String getUpdateState(Context context) {
        return context.getSharedPreferences(INTENT_UPDATE_STATE, 0).getString(KEY_IS_UPDATE, "false");
    }

    public static void saveAccount(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putString(PREF_MAILBOX, str);
        edit.putString(PREF_PASSWORD, str2);
        edit.commit();
    }

    public static void saveFlagOfBackRadio(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putBoolean(PREF_BACK_TO_MAIN, z);
        edit.commit();
    }

    public static void saveFlagOfFlipHint(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putBoolean(PREF_RADIO_FLIP_FLAG, z);
        edit.commit();
    }

    public static synchronized void saveLastFavoriteUpdateDate(Context context, long j) {
        synchronized (PrefsUtil.class) {
            SharedPreferences.Editor edit = context.getSharedPreferences(PREFS_FAVORITE_NAME, 0).edit();
            edit.putLong(PREF_LAST_FAVORITE_UPDATE_DATE, j);
            edit.commit();
        }
    }

    public static synchronized void saveLastPopupMsgDate(Context context, long j) {
        synchronized (PrefsUtil.class) {
            SharedPreferences.Editor edit = context.getSharedPreferences(PREFS_NAME, 0).edit();
            edit.putLong(PREF_LAST_POPUP_MSG_DATE, j);
            edit.commit();
        }
    }

    public static void savePassword(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putString(PREF_PASSWORD, str);
        edit.commit();
    }

    public static void saveTypeSelection(int i, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PREFS_NAME, 0).edit();
        edit.putInt(PREF_DNA_TYPE_SELECTION, i);
        edit.commit();
    }

    public static void saveUpdateState(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences(INTENT_UPDATE_STATE, 0).edit();
        edit.putString(KEY_IS_UPDATE, str);
        edit.commit();
    }
}
