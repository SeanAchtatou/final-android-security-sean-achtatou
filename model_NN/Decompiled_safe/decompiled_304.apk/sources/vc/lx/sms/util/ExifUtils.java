package vc.lx.sms.util;

import android.media.ExifInterface;
import android.text.TextUtils;
import com.google.android.mms.pdu.PduHeaders;

public class ExifUtils {
    private ExifUtils() {
    }

    public static int getExifRotation(String str) {
        try {
            String attribute = new ExifInterface(str).getAttribute("Orientation");
            if (TextUtils.isEmpty(attribute)) {
                return 0;
            }
            switch (Integer.parseInt(attribute)) {
                case 1:
                    return 0;
                case 2:
                case 4:
                case 5:
                case 7:
                default:
                    return 0;
                case 3:
                    return PduHeaders.RECOMMENDED_RETRIEVAL_MODE;
                case 6:
                    return 90;
                case 8:
                    return 270;
            }
        } catch (Exception e) {
            return 0;
        }
    }
}
