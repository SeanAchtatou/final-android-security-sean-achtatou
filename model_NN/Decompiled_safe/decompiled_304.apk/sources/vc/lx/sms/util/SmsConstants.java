package vc.lx.sms.util;

public class SmsConstants {
    public static final int ID_VISIT_FROM_CALLING_CARD = 1000;
    public static final String IS_VISIT_FROM_CALLING_CARD = "isFromCallingCard";
    public static final String VISIT_FROM_CALLING_CARD = "fromCallingCard";
}
