package vc.lx.sms.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Debug;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.text.style.URLSpan;
import android.util.Log;
import android.widget.Toast;
import com.android.internal.telephony.Phone;
import com.android.mms.MmsConfig;
import com.android.mms.data.WorkingMessage;
import com.android.mms.model.LayoutModel;
import com.android.mms.model.MediaModel;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.transaction.MessageSender;
import com.android.mms.transaction.MmsMessageSender;
import com.android.mms.ui.SlideshowActivity;
import com.android.mms.ui.UriImage;
import com.android.provider.Telephony;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.MultimediaMessagePdu;
import com.google.android.mms.pdu.NotificationInd;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduPart;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.RetrieveConf;
import com.google.android.mms.pdu.SendReq;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import vc.lx.sms.data.LogTag;
import vc.lx.sms2.R;

public class MessageUtils {
    public static final int IMAGE_COMPRESSION_QUALITY = 80;
    public static final int MESSAGE_OVERHEAD = 5000;
    public static final int MINIMUM_IMAGE_COMPRESSION_QUALITY = 50;
    private static final char[] NUMERIC_CHARS_SUGAR = {'-', '.', ',', '(', ')', ' ', '/', '\\', '*', '#', '+'};
    private static final String TAG = "Mms";
    private static HashMap numericSugarMap = new HashMap(NUMERIC_CHARS_SUGAR.length);
    private static String sLocalNumber;
    private static final Map<String, String> sRecipientAddress = new ConcurrentHashMap(20);

    public interface ResizeImageResultCallback {
        void onResizeResult(PduPart pduPart, boolean z);
    }

    static {
        for (int i = 0; i < NUMERIC_CHARS_SUGAR.length; i++) {
            numericSugarMap.put(Character.valueOf(NUMERIC_CHARS_SUGAR[i]), Character.valueOf(NUMERIC_CHARS_SUGAR[i]));
        }
    }

    private MessageUtils() {
    }

    private static void confirmReadReportDialog(Context context, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle((int) R.string.confirm);
        builder.setMessage((int) R.string.message_send_read_report);
        builder.setPositiveButton((int) R.string.yes, onClickListener);
        builder.setNegativeButton((int) R.string.no, onClickListener2);
        builder.setOnCancelListener(onCancelListener);
        builder.show();
    }

    private static String extractEncStr(Context context, EncodedStringValue encodedStringValue) {
        return encodedStringValue != null ? encodedStringValue.getString() : "";
    }

    public static String extractEncStrFromCursor(Cursor cursor, int i, int i2) {
        String string = cursor.getString(i);
        int i3 = cursor.getInt(i2);
        return TextUtils.isEmpty(string) ? "" : i3 != 0 ? new EncodedStringValue(i3, PduPersister.getBytes(string)).getString() : string;
    }

    private static StringBuilder extractIdsToAddresses(Context context, String str, boolean z) {
        boolean z2;
        boolean z3;
        StringBuilder sb = new StringBuilder();
        String[] split = str.split(" ");
        int length = split.length;
        int i = 0;
        boolean z4 = true;
        while (i < length) {
            String str2 = split[i];
            String str3 = sRecipientAddress.get(str2);
            if (str3 == null) {
                if (!z) {
                    return null;
                }
                Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Uri.parse("content://mms-sms/canonical-address/" + str2), null, null, null, null);
                if (query != null) {
                    try {
                        if (query.moveToFirst()) {
                            str3 = query.getString(0);
                            sRecipientAddress.put(str2, str3);
                        }
                    } finally {
                        query.close();
                    }
                }
            }
            if (str3 == null) {
                z3 = z4;
            } else {
                if (z4) {
                    z2 = false;
                } else {
                    sb.append(MessageSender.RECIPIENTS_SEPARATOR);
                    z2 = z4;
                }
                sb.append(str3);
                z3 = z2;
            }
            i++;
            z4 = z3;
        }
        if (sb.length() == 0) {
            return null;
        }
        return sb;
    }

    public static ArrayList<String> extractUris(URLSpan[] uRLSpanArr) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (URLSpan url : uRLSpanArr) {
            arrayList.add(url.getURL());
        }
        return arrayList;
    }

    public static String formatTimeStampString(Context context, long j) {
        return formatTimeStampString(context, j, false);
    }

    public static String formatTimeStampString(Context context, long j, boolean z) {
        Time time = new Time();
        time.set(j);
        Time time2 = new Time();
        time2.setToNow();
        int i = time.year != time2.year ? 527104 | 20 : time.yearDay != time2.yearDay ? 527104 | 16 : 527104 | 1;
        if (z) {
            i |= 17;
        }
        return DateUtils.formatDateTime(context, j, i);
    }

    public static int getAttachmentType(SlideshowModel slideshowModel) {
        if (slideshowModel == null) {
            return 0;
        }
        int size = slideshowModel.size();
        if (size > 1) {
            return 4;
        }
        if (size == 1) {
            SlideModel slideModel = slideshowModel.get(0);
            if (slideModel.hasVideo()) {
                return 2;
            }
            if (slideModel.hasAudio() && slideModel.hasImage()) {
                return 4;
            }
            if (slideModel.hasAudio()) {
                return 3;
            }
            if (slideModel.hasImage()) {
                return 1;
            }
            if (slideModel.hasText()) {
                return 0;
            }
        }
        return 0;
    }

    public static String getLocalNumber(Context context) {
        if (sLocalNumber == null) {
            sLocalNumber = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        }
        return sLocalNumber;
    }

    public static String getMessageDetails(Context context, Cursor cursor, int i) {
        if (cursor == null) {
            return null;
        }
        if (!Phone.APN_TYPE_MMS.equals(cursor.getString(0))) {
            return getTextMessageDetails(context, cursor);
        }
        switch (cursor.getInt(14)) {
            case 128:
            case 132:
                return getMultimediaMessageDetails(context, cursor, i);
            case 129:
            case 131:
            default:
                Log.w("Mms", "No details could be retrieved.");
                return "";
            case 130:
                return getNotificationIndDetails(context, cursor);
        }
    }

    private static String getMultimediaMessageDetails(Context context, Cursor cursor, int i) {
        int i2;
        EncodedStringValue[] bcc;
        if (cursor.getInt(14) == 130) {
            return getNotificationIndDetails(context, cursor);
        }
        StringBuilder sb = new StringBuilder();
        Resources resources = context.getResources();
        Uri withAppendedId = ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, cursor.getLong(1));
        try {
            MultimediaMessagePdu multimediaMessagePdu = (MultimediaMessagePdu) PduPersister.getPduPersister(context).load(withAppendedId);
            sb.append(resources.getString(R.string.message_type_label));
            sb.append(resources.getString(R.string.multimedia_message));
            if (multimediaMessagePdu instanceof RetrieveConf) {
                String extractEncStr = extractEncStr(context, ((RetrieveConf) multimediaMessagePdu).getFrom());
                sb.append(10);
                sb.append(resources.getString(R.string.from_label));
                if (TextUtils.isEmpty(extractEncStr)) {
                    extractEncStr = resources.getString(R.string.hidden_sender_address);
                }
                sb.append(extractEncStr);
            }
            sb.append(10);
            sb.append(resources.getString(R.string.to_address_label));
            EncodedStringValue[] to = multimediaMessagePdu.getTo();
            if (to != null) {
                sb.append(EncodedStringValue.concat(to));
            } else {
                Log.w("Mms", "recipient list is empty!");
            }
            if ((multimediaMessagePdu instanceof SendReq) && (bcc = ((SendReq) multimediaMessagePdu).getBcc()) != null && bcc.length > 0) {
                sb.append(10);
                sb.append(resources.getString(R.string.bcc_label));
                sb.append(EncodedStringValue.concat(bcc));
            }
            sb.append(10);
            int i3 = cursor.getInt(15);
            if (i3 == 3) {
                sb.append(resources.getString(R.string.saved_label));
            } else if (i3 == 1) {
                sb.append(resources.getString(R.string.received_label));
            } else {
                sb.append(resources.getString(R.string.sent_label));
            }
            sb.append(formatTimeStampString(context, multimediaMessagePdu.getDate() * 1000, true));
            sb.append(10);
            sb.append(resources.getString(R.string.subject_label));
            EncodedStringValue subject = multimediaMessagePdu.getSubject();
            if (subject != null) {
                String string = subject.getString();
                sb.append(string);
                i2 = string.length() + i;
            } else {
                i2 = i;
            }
            sb.append(10);
            sb.append(resources.getString(R.string.priority_label));
            sb.append(getPriorityDescription(context, multimediaMessagePdu.getPriority()));
            sb.append(10);
            sb.append(resources.getString(R.string.message_size_label));
            sb.append(((i2 - 1) / 1000) + 1);
            sb.append(" KB");
            return sb.toString();
        } catch (MmsException e) {
            Log.e("Mms", "Failed to load the message: " + withAppendedId, e);
            return context.getResources().getString(R.string.cannot_get_details);
        }
    }

    private static String getNotificationIndDetails(Context context, Cursor cursor) {
        StringBuilder sb = new StringBuilder();
        Resources resources = context.getResources();
        Uri withAppendedId = ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, cursor.getLong(1));
        try {
            NotificationInd notificationInd = (NotificationInd) PduPersister.getPduPersister(context).load(withAppendedId);
            sb.append(resources.getString(R.string.message_type_label));
            sb.append(resources.getString(R.string.multimedia_notification));
            String extractEncStr = extractEncStr(context, notificationInd.getFrom());
            sb.append(10);
            sb.append(resources.getString(R.string.from_label));
            if (TextUtils.isEmpty(extractEncStr)) {
                extractEncStr = resources.getString(R.string.hidden_sender_address);
            }
            sb.append(extractEncStr);
            sb.append(10);
            sb.append(resources.getString(R.string.expire_on, formatTimeStampString(context, notificationInd.getExpiry() * 1000, true)));
            sb.append(10);
            sb.append(resources.getString(R.string.subject_label));
            EncodedStringValue subject = notificationInd.getSubject();
            if (subject != null) {
                sb.append(subject.getString());
            }
            sb.append(10);
            sb.append(resources.getString(R.string.message_class_label));
            sb.append(new String(notificationInd.getMessageClass()));
            sb.append(10);
            sb.append(resources.getString(R.string.message_size_label));
            sb.append(String.valueOf((notificationInd.getMessageSize() + 1023) / 1024));
            sb.append(context.getString(R.string.kilobyte));
            return sb.toString();
        } catch (MmsException e) {
            Log.e("Mms", "Failed to load the message: " + withAppendedId, e);
            return context.getResources().getString(R.string.cannot_get_details);
        }
    }

    private static String getPriorityDescription(Context context, int i) {
        Resources resources = context.getResources();
        switch (i) {
            case 128:
                return resources.getString(R.string.priority_low);
            case 129:
            default:
                return resources.getString(R.string.priority_normal);
            case 130:
                return resources.getString(R.string.priority_high);
        }
    }

    public static String getRecipientsByIds(Context context, String str, boolean z) {
        String str2;
        String str3 = sRecipientAddress.get(str);
        if (str3 != null) {
            return str3;
        }
        if (!TextUtils.isEmpty(str)) {
            StringBuilder extractIdsToAddresses = extractIdsToAddresses(context, str, z);
            if (extractIdsToAddresses == null) {
                return "";
            }
            str2 = extractIdsToAddresses.toString();
        } else {
            str2 = "";
        }
        sRecipientAddress.put(str, str2);
        return str2;
    }

    private static String getTextMessageDetails(Context context, Cursor cursor) {
        StringBuilder sb = new StringBuilder();
        Resources resources = context.getResources();
        sb.append(resources.getString(R.string.message_type_label));
        sb.append(resources.getString(R.string.text_message));
        sb.append(10);
        int i = cursor.getInt(7);
        if (Telephony.Sms.isOutgoingFolder(i)) {
            sb.append(resources.getString(R.string.to_address_label));
        } else {
            sb.append(resources.getString(R.string.from_label));
        }
        sb.append(cursor.getString(3));
        sb.append(10);
        if (i == 3) {
            sb.append(resources.getString(R.string.saved_label));
        } else if (i == 1) {
            sb.append(resources.getString(R.string.received_label));
        } else {
            sb.append(resources.getString(R.string.sent_label));
        }
        sb.append(formatTimeStampString(context, cursor.getLong(5), true));
        return sb.toString();
    }

    public static void handleReadReport(final Context context, long j, final int i, final Runnable runnable) {
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), Telephony.Mms.Inbox.CONTENT_URI, new String[]{"_id", Telephony.BaseMmsColumns.MESSAGE_ID}, j != -1 ? "m_type = 132 AND read = 0 AND rr = 128" + " AND " + "thread_id" + " = " + j : "m_type = 132 AND read = 0 AND rr = 128", null, null);
        if (query != null) {
            final HashMap hashMap = new HashMap();
            try {
                if (query.getCount() == 0) {
                    if (runnable != null) {
                        runnable.run();
                    }
                    return;
                }
                while (query.moveToNext()) {
                    hashMap.put(query.getString(1), AddressUtils.getFrom(context, ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, query.getLong(0))));
                }
                query.close();
                confirmReadReportDialog(context, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (Map.Entry entry : hashMap.entrySet()) {
                            MmsMessageSender.sendReadRec(context, (String) entry.getValue(), (String) entry.getKey(), i);
                        }
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                }, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                }, new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                });
            } finally {
                query.close();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x004b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean hasDraft(android.content.Context r11, long r12) {
        /*
            r9 = 1
            r8 = 0
            r7 = 0
            vc.lx.sms2.SmsApplication r0 = vc.lx.sms2.SmsApplication.getInstance()     // Catch:{ Exception -> 0x003f, all -> 0x0047 }
            vc.lx.sms2.SmsApplication r1 = vc.lx.sms2.SmsApplication.getInstance()     // Catch:{ Exception -> 0x003f, all -> 0x0047 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ Exception -> 0x003f, all -> 0x0047 }
            android.net.Uri r2 = com.android.provider.Telephony.MmsSms.CONTENT_DRAFT_URI     // Catch:{ Exception -> 0x003f, all -> 0x0047 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x003f, all -> 0x0047 }
            r4 = 0
            java.lang.String r5 = "thread_id"
            r3[r4] = r5     // Catch:{ Exception -> 0x003f, all -> 0x0047 }
            r4 = 0
            r5 = 0
            r6 = 0
            android.database.Cursor r0 = vc.lx.sms.util.SqliteWrapper.query(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x003f, all -> 0x0047 }
        L_0x0020:
            if (r0 == 0) goto L_0x0038
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0054, all -> 0x004f }
            if (r1 == 0) goto L_0x0038
            r1 = 0
            long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x0054, all -> 0x004f }
            int r1 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r1 != 0) goto L_0x0020
            if (r0 == 0) goto L_0x0036
            r0.close()
        L_0x0036:
            r0 = r9
        L_0x0037:
            return r0
        L_0x0038:
            if (r0 == 0) goto L_0x003d
            r0.close()
        L_0x003d:
            r0 = r8
            goto L_0x0037
        L_0x003f:
            r0 = move-exception
            r0 = r7
        L_0x0041:
            if (r0 == 0) goto L_0x003d
            r0.close()
            goto L_0x003d
        L_0x0047:
            r0 = move-exception
            r1 = r7
        L_0x0049:
            if (r1 == 0) goto L_0x004e
            r1.close()
        L_0x004e:
            throw r0
        L_0x004f:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0049
        L_0x0054:
            r1 = move-exception
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.util.MessageUtils.hasDraft(android.content.Context, long):boolean");
    }

    public static boolean isAlias(String str) {
        if (!MmsConfig.isAliasEnabled()) {
            return false;
        }
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (Telephony.Mms.isPhoneNumber(str)) {
            return false;
        }
        if (!isAlphaNumeric(str)) {
            return false;
        }
        int length = str.length();
        return length >= MmsConfig.getAliasMinChars() && length <= MmsConfig.getAliasMaxChars();
    }

    public static boolean isAlphaNumeric(String str) {
        char[] charArray = str.toCharArray();
        for (char c : charArray) {
            if ((c < 'a' || c > 'z') && ((c < 'A' || c > 'Z') && (c < '0' || c > '9'))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isLocalNumber(Context context, String str) {
        return PhoneNumberUtils.compare(str, getLocalNumber(context));
    }

    public static boolean isValidMmsAddress(String str) {
        return parseMmsAddress(str) != null;
    }

    private static void log(String str) {
        Log.d("Mms", "[MsgUtils] " + str);
    }

    public static String parseMmsAddress(String str) {
        if (Telephony.Mms.isEmailAddress(str)) {
            return str;
        }
        String parsePhoneNumberForMms = parsePhoneNumberForMms(str);
        if (parsePhoneNumberForMms != null) {
            return parsePhoneNumberForMms;
        }
        if (isAlias(str)) {
            return str;
        }
        return null;
    }

    private static String parsePhoneNumberForMms(String str) {
        StringBuilder sb = new StringBuilder();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '+' && sb.length() == 0) {
                sb.append(charAt);
            } else if (Character.isDigit(charAt)) {
                sb.append(charAt);
            } else if (numericSugarMap.get(Character.valueOf(charAt)) == null) {
                return null;
            }
        }
        return sb.toString();
    }

    public static void recordSound(Context context, int i) {
        if (context instanceof Activity) {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType(ContentType.AUDIO_AMR);
            intent.setClassName("com.android.soundrecorder", "com.android.soundrecorder.SoundRecorder");
            ((Activity) context).startActivityForResult(intent, i);
        }
    }

    public static void resizeImageAsync(final Context context, Uri uri, Handler handler, ResizeImageResultCallback resizeImageResultCallback, boolean z) {
        final AnonymousClass1 r4 = new Runnable() {
            public void run() {
                Toast.makeText(context, (int) R.string.compressing, 0).show();
            }
        };
        handler.postDelayed(r4, 1000);
        final Context context2 = context;
        final Uri uri2 = uri;
        final Handler handler2 = handler;
        final ResizeImageResultCallback resizeImageResultCallback2 = resizeImageResultCallback;
        final boolean z2 = z;
        new Thread(new Runnable() {
            /* JADX INFO: finally extract failed */
            public void run() {
                try {
                    final PduPart resizedImageAsPart = new UriImage(context2, uri2).getResizedImageAsPart(MmsConfig.getMaxImageWidth(), MmsConfig.getMaxImageHeight(), MmsConfig.getMaxMessageSize() - MessageUtils.MESSAGE_OVERHEAD);
                    handler2.removeCallbacks(r4);
                    handler2.post(new Runnable() {
                        public void run() {
                            resizeImageResultCallback2.onResizeResult(resizedImageAsPart, z2);
                        }
                    });
                } catch (Throwable th) {
                    handler2.removeCallbacks(r4);
                    throw th;
                }
            }
        }).start();
    }

    public static Uri saveBitmapAsPart(Context context, Uri uri, Bitmap bitmap) throws MmsException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        PduPart pduPart = new PduPart();
        pduPart.setContentType(ContentType.IMAGE_JPEG.getBytes());
        String str = LayoutModel.IMAGE_REGION_ID + System.currentTimeMillis();
        pduPart.setContentLocation((str + ".jpg").getBytes());
        pduPart.setContentId(str.getBytes());
        pduPart.setData(byteArrayOutputStream.toByteArray());
        Uri persistPart = PduPersister.getPduPersister(context).persistPart(pduPart, ContentUris.parseId(uri));
        if (Log.isLoggable(LogTag.APP, 2)) {
            log("saveBitmapAsPart: persisted part with uri=" + persistPart);
        }
        return persistPart;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void selectAudio(Context context, int i) {
        if (context instanceof Activity) {
            Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
            intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", false);
            intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", false);
            intent.putExtra("android.intent.extra.ringtone.INCLUDE_DRM", false);
            intent.putExtra("android.intent.extra.ringtone.TITLE", context.getString(R.string.select_audio));
            ((Activity) context).startActivityForResult(intent, i);
        }
    }

    public static void selectImage(Context context, int i) {
        selectMediaByType(context, i, ContentType.IMAGE_UNSPECIFIED);
    }

    private static void selectMediaByType(Context context, int i, String str) {
        if (context instanceof Activity) {
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType(str);
            ((Activity) context).startActivityForResult(Intent.createChooser(intent, null), i);
        }
    }

    public static void selectVideo(Context context, int i) {
        selectMediaByType(context, i, ContentType.VIDEO_UNSPECIFIED);
    }

    public static void showDiscardDraftConfirmDialog(Context context, DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(context).setIcon(17301543).setTitle((int) R.string.discard_message).setMessage((int) R.string.discard_message_reason).setPositiveButton((int) R.string.yes, onClickListener).setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null).show();
    }

    public static void showErrorDialog(Context context, String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon((int) R.drawable.ic_sms_mms_not_delivered);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public static void viewMmsMessageAttachment(Context context, Uri uri, SlideshowModel slideshowModel) {
        if (slideshowModel == null ? false : slideshowModel.isSimple()) {
            viewSimpleSlideshow(context, slideshowModel);
            return;
        }
        if (slideshowModel != null) {
            PduPersister pduPersister = PduPersister.getPduPersister(context);
            try {
                PduBody pduBody = slideshowModel.toPduBody();
                pduPersister.updateParts(uri, pduBody);
                slideshowModel.sync(pduBody);
            } catch (MmsException e) {
                Log.e("Mms", "Unable to save message for preview");
                return;
            }
        }
        Intent intent = new Intent(context, SlideshowActivity.class);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static void viewMmsMessageAttachment(Context context, WorkingMessage workingMessage) {
        SlideshowModel slideshow = workingMessage.getSlideshow();
        if (slideshow == null) {
            throw new IllegalStateException("msg.getSlideshow() == null");
        } else if (slideshow.isSimple()) {
            viewSimpleSlideshow(context, slideshow);
        } else {
            viewMmsMessageAttachment(context, workingMessage.saveAsMms(false), slideshow);
        }
    }

    public static void viewSimpleSlideshow(Context context, SlideshowModel slideshowModel) {
        if (!slideshowModel.isSimple()) {
            throw new IllegalArgumentException("viewSimpleSlideshow() called on a non-simple slideshow");
        }
        SlideModel slideModel = slideshowModel.get(0);
        MediaModel image = slideModel.hasImage() ? slideModel.getImage() : slideModel.hasVideo() ? slideModel.getVideo() : null;
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(1);
        intent.setDataAndType(image.getUri(), image.isDrmProtected() ? image.getDrmObject().getContentType() : image.getContentType());
        context.startActivity(intent);
    }

    public static void writeHprofDataToFile() {
        try {
            Debug.dumpHprofData("/sdcard/mms_oom_hprof_data");
            Log.i("Mms", "##### written hprof data to " + "/sdcard/mms_oom_hprof_data");
        } catch (IOException e) {
            Log.e("Mms", "writeHprofDataToFile: caught " + e);
        }
    }
}
