package vc.lx.sms.intents;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import java.util.List;

public class SsmIntents {
    public static final String INTENT_UPDATE_SECRET_MESSAGE = "com.android.mms.harmony.UPDATE_SECRET_MESSAGE";
    public static final String INTENT_UPDATE_SPAM_MESSAGE = "com.android.mms.harmony.UPDATE_SPAM_MESSAGE";
    public static final String RECEIVED_MESSAGE = "receive_message";
    public static final String SMS_MIME_TYPE = "vnd.android-dir/mms-sms";

    public static Intent getSmsInboxIntent() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setType(SMS_MIME_TYPE);
        intent.setFlags(872415232);
        return intent;
    }

    public static boolean isApplicationBroughtToBackground(Context context) {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
        if (!runningTasks.isEmpty()) {
            ComponentName componentName = runningTasks.get(0).topActivity;
            if (!componentName.getPackageName().equals(context.getPackageName()) || "vc.lx.sms.ui.SmsPopupActivity".equals(componentName.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSmsPopupActivityBroughtToFront(Context context) {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
        return !runningTasks.isEmpty() && "vc.lx.sms.ui.SmsPopupActivity".equals(runningTasks.get(0).topActivity.getClassName());
    }
}
