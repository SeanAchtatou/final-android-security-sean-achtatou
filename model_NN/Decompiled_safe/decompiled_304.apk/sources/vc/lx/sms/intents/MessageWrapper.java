package vc.lx.sms.intents;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsMessage;
import com.android.provider.Telephony;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.ui.widget.PopSmsItemView;

public class MessageWrapper {
    public static final String EXTRAS_FROM_ADDRESS = "EXTRAS_FROM_ADDRESS";
    public static final String EXTRAS_MESSAGE_BODY = "EXTRAS_MESSAGE_BODY";
    public static final String EXTRAS_MESSAGE_ID = "EXTRAS_MESSAGE_ID";
    public static final String EXTRAS_THREAD_ID = "EXTRAS_THREAD_ID";
    public static final String EXTRAS_TIMESTAMP = "EXTRAS_TIMESTAMP";
    public static final String EXTRAS_UNREAD_COUNT = "EXTRAS_UNREAD_COUNT";
    public String mAddress = null;
    private Context mContext;
    public String mMessageBody = null;
    private long mMessageId = 0;
    private long mThreadId = 0;
    public long mTimestamp = 0;
    public int mUnreadCount = 0;

    public MessageWrapper(Context context, Bundle bundle) {
        this.mContext = context;
        this.mAddress = bundle.getString(EXTRAS_FROM_ADDRESS);
        this.mMessageBody = bundle.getString(EXTRAS_MESSAGE_BODY);
        this.mTimestamp = bundle.getLong(EXTRAS_TIMESTAMP);
        this.mUnreadCount = bundle.getInt(EXTRAS_UNREAD_COUNT, 1);
        this.mThreadId = bundle.getLong(EXTRAS_THREAD_ID, 0);
        this.mMessageId = bundle.getLong(EXTRAS_MESSAGE_ID, 0);
    }

    public MessageWrapper(SmsMessage smsMessage, Context context) {
        this.mContext = context;
        this.mAddress = smsMessage.getOriginatingAddress();
        this.mMessageBody = smsMessage.getMessageBody();
        this.mTimestamp = smsMessage.getTimestampMillis();
        this.mUnreadCount = getUnreadSmsCount(this.mContext, this.mTimestamp, this.mMessageBody);
    }

    public MessageWrapper(PopSmsItemView.PopSmsItem popSmsItem, Context context) {
        this.mContext = context;
        this.mAddress = popSmsItem.mAddress;
        this.mMessageBody = popSmsItem.mBody;
        this.mTimestamp = popSmsItem.mTimestamp;
        this.mMessageId = 0;
        this.mThreadId = 0;
    }

    /* JADX INFO: finally extract failed */
    private long findSmsMessageId(Context context, long j, long j2, String str) {
        Cursor query;
        String str2 = "body = " + DatabaseUtils.sqlEscapeString(str);
        String[] strArr = {"_id", "date", "thread_id", Telephony.TextBasedSmsColumns.BODY};
        if (j <= 0 || (query = context.getContentResolver().query(ContentUris.withAppendedId(Telephony.MmsSms.CONTENT_CONVERSATIONS_URI, j), strArr, str2, null, "date DESC")) == null) {
            return 0;
        }
        try {
            long j3 = query.moveToFirst() ? query.getLong(0) : 0;
            query.close();
            return j3;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    private long findSmsThreadIdFromAddress(Context context, String str) {
        if (str == null) {
            return 0;
        }
        Uri.Builder buildUpon = Uri.withAppendedPath(Telephony.MmsSms.CONTENT_URI, "threadID").buildUpon();
        buildUpon.appendQueryParameter(SmsSqliteHelper.RECIPIENT, str);
        Cursor query = context.getContentResolver().query(buildUpon.build(), new String[]{"_id"}, null, null, null);
        if (query == null) {
            return 0;
        }
        try {
            long j = query.moveToFirst() ? query.getLong(0) : 0;
            query.close();
            return j;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    private int getUnreadSmsCount(Context context, long j, String str) {
        int i;
        Cursor query = context.getContentResolver().query(Telephony.Sms.Inbox.CONTENT_URI, new String[]{"_id", Telephony.TextBasedSmsColumns.BODY}, "read = 0", null, "date DESC");
        if (query != null) {
            try {
                int count = query.getCount();
                if (str != null && count > 0 && query.moveToFirst() && !str.equals(query.getString(1))) {
                    count++;
                }
                query.close();
                i = count;
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        } else {
            i = 0;
        }
        if (i != 0 || j <= 0) {
            return i;
        }
        return 1;
    }

    public long getMessageId() {
        locateMessageId();
        return this.mMessageId;
    }

    public long getThreadId() {
        locateThreadId();
        return this.mThreadId;
    }

    public void locateMessageId() {
        if (this.mMessageId == 0) {
            if (this.mThreadId == 0) {
                locateThreadId();
            }
            this.mMessageId = findSmsMessageId(this.mContext, this.mThreadId, this.mTimestamp, this.mMessageBody);
        }
    }

    public void locateThreadId() {
        if (this.mThreadId == 0) {
            this.mThreadId = findSmsThreadIdFromAddress(this.mContext, this.mAddress);
        }
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_FROM_ADDRESS, this.mAddress);
        bundle.putString(EXTRAS_MESSAGE_BODY, this.mMessageBody);
        bundle.putLong(EXTRAS_TIMESTAMP, this.mTimestamp);
        bundle.putInt(EXTRAS_UNREAD_COUNT, this.mUnreadCount);
        bundle.putLong(EXTRAS_THREAD_ID, this.mThreadId);
        bundle.putLong(EXTRAS_MESSAGE_ID, this.mMessageId);
        return bundle;
    }
}
