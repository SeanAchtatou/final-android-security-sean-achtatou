package vc.lx.sms.db;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import java.io.ByteArrayOutputStream;
import vc.lx.sms.cmcc.http.data.SmsImage;
import vc.lx.sms.util.Util;

public class ImagesDbService {
    private Context mContext;
    private SmsSqliteHelper sqliteHelper = null;

    public static class ImageDump {
        public Bitmap bitmap;
        public String recipients;
        public long rowId;
        public String smsContent;
        public int status;
    }

    public static class ImageItem {
        public String plug;
        public String thumb;
        public String url;
    }

    public ImagesDbService(Context context) {
        this.sqliteHelper = new SmsSqliteHelper(context);
        this.mContext = context;
    }

    public void deleteImageDumpByRowId(long j) {
        if (j > 0) {
            this.mContext.getContentResolver().delete(ImageDumpContentProvider.CONTENT_URI, "_id = " + j, null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void imageUploadFaile(long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", (Integer) 1);
        this.mContext.getContentResolver().update(ImageDumpContentProvider.CONTENT_URI, contentValues, " _id = " + j, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public long insertImageDump(Bitmap bitmap, String str, String str2) {
        if (bitmap == null) {
            return -1;
        }
        this.mContext.getContentResolver().delete(ImageDumpContentProvider.CONTENT_URI, "recipient = ?", new String[]{str});
        ContentValues contentValues = new ContentValues();
        contentValues.put(SmsSqliteHelper.UUID, Util.randomUUID());
        contentValues.put(SmsSqliteHelper.RECIPIENT, str);
        contentValues.put("sms_content", str2);
        contentValues.put("status", (Integer) 0);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        contentValues.put(SmsSqliteHelper.IMAGE, byteArrayOutputStream.toByteArray());
        Uri insert = this.mContext.getContentResolver().insert(ImageDumpContentProvider.CONTENT_URI, contentValues);
        if (insert != null) {
            return ContentUris.parseId(insert);
        }
        return -1;
    }

    public void insertSmsImage(SmsImage smsImage) {
        insertSmsImage(smsImage, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void insertSmsImage(SmsImage smsImage, boolean z) {
        if (smsImage != null && smsImage.mPlug != null && smsImage.mThumb != null && smsImage.mUrl != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("url", smsImage.mUrl);
            contentValues.put(SmsSqliteHelper.THUMB, smsImage.mThumb);
            if (queryByPlug(smsImage.mPlug) != null) {
                SQLiteDatabase writableDatabase = this.sqliteHelper.getWritableDatabase();
                try {
                    writableDatabase.update(SmsSqliteHelper.TABLE_IMAGES, contentValues, "plug = ?", new String[]{smsImage.mPlug});
                    if (writableDatabase != null) {
                        writableDatabase.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (writableDatabase != null) {
                        writableDatabase.close();
                    }
                } catch (Throwable th) {
                    if (writableDatabase != null) {
                        writableDatabase.close();
                    }
                    throw th;
                }
            } else {
                SQLiteDatabase writableDatabase2 = this.sqliteHelper.getWritableDatabase();
                if (z) {
                    try {
                        contentValues.put(SmsSqliteHelper.MASTER, (Integer) 1);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        if (writableDatabase2 != null) {
                            writableDatabase2.close();
                            return;
                        }
                        return;
                    } catch (Throwable th2) {
                        if (writableDatabase2 != null) {
                            writableDatabase2.close();
                        }
                        throw th2;
                    }
                } else {
                    contentValues.put(SmsSqliteHelper.MASTER, (Integer) 0);
                }
                contentValues.put(SmsSqliteHelper.PLUG, smsImage.mPlug);
                writableDatabase2.insert(SmsSqliteHelper.TABLE_IMAGES, "", contentValues);
                if (writableDatabase2 != null) {
                    writableDatabase2.close();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public vc.lx.sms.db.ImagesDbService.ImageItem queryByPlug(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 0
            vc.lx.sms.db.SmsSqliteHelper r0 = r10.sqliteHelper
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()
            java.lang.String r1 = "images"
            r2 = 0
            java.lang.String r3 = "plug = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0056, all -> 0x0063 }
            r5 = 0
            r4[r5] = r11     // Catch:{ Exception -> 0x0056, all -> 0x0063 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0056, all -> 0x0063 }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            if (r2 == 0) goto L_0x004a
            vc.lx.sms.db.ImagesDbService$ImageItem r2 = new vc.lx.sms.db.ImagesDbService$ImageItem     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            r2.<init>()     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            r2.plug = r11     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            java.lang.String r3 = "url"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            r2.url = r3     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            java.lang.String r3 = "thumb"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            r2.thumb = r3     // Catch:{ Exception -> 0x0075, all -> 0x0070 }
            if (r1 == 0) goto L_0x0043
            r1.close()
        L_0x0043:
            if (r0 == 0) goto L_0x0048
            r0.close()
        L_0x0048:
            r0 = r2
        L_0x0049:
            return r0
        L_0x004a:
            if (r1 == 0) goto L_0x004f
            r1.close()
        L_0x004f:
            if (r0 == 0) goto L_0x0054
            r0.close()
        L_0x0054:
            r0 = r8
            goto L_0x0049
        L_0x0056:
            r1 = move-exception
            r1 = r8
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            r1.close()
        L_0x005d:
            if (r0 == 0) goto L_0x0054
            r0.close()
            goto L_0x0054
        L_0x0063:
            r1 = move-exception
            r2 = r8
        L_0x0065:
            if (r2 == 0) goto L_0x006a
            r2.close()
        L_0x006a:
            if (r0 == 0) goto L_0x006f
            r0.close()
        L_0x006f:
            throw r1
        L_0x0070:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x0065
        L_0x0075:
            r2 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.ImagesDbService.queryByPlug(java.lang.String):vc.lx.sms.db.ImagesDbService$ImageItem");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0066, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0073, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007c, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0072 A[ExcHandler: all (r1v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:5:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public vc.lx.sms.db.ImagesDbService.ImageDump queryImageDumpByRowId(long r9) {
        /*
            r8 = this;
            r6 = 0
            r0 = 0
            int r0 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x0009
            r0 = r6
        L_0x0008:
            return r0
        L_0x0009:
            android.content.Context r0 = r8.mContext     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            android.net.Uri r1 = vc.lx.sms.db.ImageDumpContentProvider.CONTENT_URI     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r3.<init>()     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r4 = "_id = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0061, all -> 0x006a }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0077, all -> 0x0072 }
            if (r1 == 0) goto L_0x0082
            vc.lx.sms.db.ImagesDbService$ImageDump r1 = new vc.lx.sms.db.ImagesDbService$ImageDump     // Catch:{ Exception -> 0x0077, all -> 0x0072 }
            r1.<init>()     // Catch:{ Exception -> 0x0077, all -> 0x0072 }
            r2 = 2
            byte[] r2 = r0.getBlob(r2)     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r3 = 0
            int r4 = r2.length     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeByteArray(r2, r3, r4)     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r1.bitmap = r2     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r1.rowId = r9     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r1.recipients = r2     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r2 = 4
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r1.smsContent = r2     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r2 = 5
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
            r1.status = r2     // Catch:{ Exception -> 0x007b, all -> 0x0072 }
        L_0x005a:
            if (r0 == 0) goto L_0x0080
            r0.close()
            r0 = r1
            goto L_0x0008
        L_0x0061:
            r0 = move-exception
            r0 = r6
            r1 = r6
        L_0x0064:
            if (r1 == 0) goto L_0x0008
            r1.close()
            goto L_0x0008
        L_0x006a:
            r0 = move-exception
            r1 = r6
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()
        L_0x0071:
            throw r0
        L_0x0072:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x006c
        L_0x0077:
            r1 = move-exception
            r1 = r0
            r0 = r6
            goto L_0x0064
        L_0x007b:
            r2 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0064
        L_0x0080:
            r0 = r1
            goto L_0x0008
        L_0x0082:
            r1 = r6
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.ImagesDbService.queryImageDumpByRowId(long):vc.lx.sms.db.ImagesDbService$ImageDump");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0072  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<vc.lx.sms.db.ImagesDbService.ImageDump> queryImageDumpListByRecipient(java.lang.String r10) {
        /*
            r9 = this;
            r7 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            android.content.Context r0 = r9.mContext     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            android.net.Uri r1 = vc.lx.sms.db.ImageDumpContentProvider.CONTENT_URI     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            r3.<init>()     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            java.lang.String r4 = "recipient = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x007b, all -> 0x006e }
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x007b, all -> 0x006e }
        L_0x0028:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            if (r1 == 0) goto L_0x0068
            vc.lx.sms.db.ImagesDbService$ImageDump r1 = new vc.lx.sms.db.ImagesDbService$ImageDump     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r1.<init>()     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r1.rowId = r2     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r2 = 2
            byte[] r2 = r0.getBlob(r2)     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r3 = 0
            int r4 = r2.length     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeByteArray(r2, r3, r4)     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r1.bitmap = r2     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r1.recipients = r2     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r2 = 4
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r1.smsContent = r2     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r2 = 5
            int r2 = r0.getInt(r2)     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r1.status = r2     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            r6.add(r1)     // Catch:{ Exception -> 0x0061, all -> 0x0076 }
            goto L_0x0028
        L_0x0061:
            r1 = move-exception
        L_0x0062:
            if (r0 == 0) goto L_0x0067
            r0.close()
        L_0x0067:
            return r6
        L_0x0068:
            if (r0 == 0) goto L_0x0067
            r0.close()
            goto L_0x0067
        L_0x006e:
            r0 = move-exception
            r1 = r7
        L_0x0070:
            if (r1 == 0) goto L_0x0075
            r1.close()
        L_0x0075:
            throw r0
        L_0x0076:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0070
        L_0x007b:
            r0 = move-exception
            r0 = r7
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.ImagesDbService.queryImageDumpListByRecipient(java.lang.String):java.util.ArrayList");
    }
}
