package vc.lx.sms.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class MusicContentProvider extends ContentProvider {
    public static final int ALL_SONGS = 1;
    public static final String AUTHORITY = "vc.lx.sms.songs";
    public static final Uri CONTENT_URI = Uri.parse("content://vc.lx.sms.songs");
    public static final Uri CONTENT_URL_MUSIC = Uri.parse("content://vc.lx.sms.songs");
    public static final int SINGLE_SONGS = 2;
    public static final String SONGS_ITEM_TYPE = "vnd.android.cursor.item/song";
    public static final String SONGS_TYPE = "vnd.android.cursor.dir/song";
    static final UriMatcher matcher = new UriMatcher(-1);
    private SmsSqliteHelper mDbHelper;

    static {
        matcher.addURI(AUTHORITY, null, 1);
        matcher.addURI(AUTHORITY, "/#", 2);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                writableDatabase.delete(SmsSqliteHelper.TABLE_MUSIC_LOCAL, " _id > ? ", new String[]{"0"});
                break;
        }
        return 0;
    }

    public String getType(Uri uri) {
        switch (matcher.match(uri)) {
            case 1:
                return SONGS_TYPE;
            case 2:
                return SONGS_ITEM_TYPE;
            default:
                return null;
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        long insert;
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                if (contentValues != null) {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_MUSIC_LOCAL, "", contentValues);
                    break;
                } else {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_MUSIC_LOCAL, null, contentValues);
                    break;
                }
            default:
                insert = 0;
                break;
        }
        if (insert > 0) {
            return Uri.parse("content://vc.lx.sms.songs/#" + insert);
        }
        return null;
    }

    public boolean onCreate() {
        this.mDbHelper = new SmsSqliteHelper(getContext());
        return this.mDbHelper != null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        Cursor query;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(SmsSqliteHelper.TABLE_MUSIC_LOCAL);
        switch (matcher.match(uri)) {
            case 1:
                query = sQLiteQueryBuilder.query(this.mDbHelper.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
                break;
            default:
                query = null;
                break;
        }
        if (query != null) {
            query.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                return writableDatabase.update(SmsSqliteHelper.TABLE_MUSIC_LOCAL, contentValues, str, strArr);
            case 2:
                return 0;
            default:
                return 0;
        }
    }
}
