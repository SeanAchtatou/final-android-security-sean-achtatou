package vc.lx.sms.db;

import android.content.Context;

public class ImageService {
    private Context context;
    private SmsSqliteHelper sqliteHelper = null;

    public ImageService(Context context2) {
        this.context = context2;
        this.sqliteHelper = new SmsSqliteHelper(context2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0062  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> getSmsImageMaster() {
        /*
            r7 = this;
            r4 = 0
            vc.lx.sms.db.SmsSqliteHelper r0 = r7.sqliteHelper
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()
            java.lang.String r1 = "select plug ,master from images where master != 1 "
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r3 = 0
            android.database.Cursor r1 = r0.rawQuery(r1, r3)     // Catch:{ Exception -> 0x0071, all -> 0x005e }
        L_0x0013:
            if (r1 == 0) goto L_0x0055
            boolean r3 = r1.moveToNext()     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            if (r3 == 0) goto L_0x0055
            java.lang.String r3 = "TestMusicImage"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            r4.<init>()     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            java.lang.String r5 = "count="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            int r5 = r1.getCount()     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            android.util.Log.i(r3, r4)     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            java.lang.String r3 = "plug"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            r2.add(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0069 }
            goto L_0x0013
        L_0x0045:
            r3 = move-exception
            r6 = r3
            r3 = r1
            r1 = r6
        L_0x0049:
            r1.printStackTrace()     // Catch:{ all -> 0x006e }
            if (r3 == 0) goto L_0x0051
            r3.close()
        L_0x0051:
            r0.close()
        L_0x0054:
            return r2
        L_0x0055:
            if (r1 == 0) goto L_0x005a
            r1.close()
        L_0x005a:
            r0.close()
            goto L_0x0054
        L_0x005e:
            r1 = move-exception
            r2 = r4
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()
        L_0x0065:
            r0.close()
            throw r1
        L_0x0069:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0060
        L_0x006e:
            r1 = move-exception
            r2 = r3
            goto L_0x0060
        L_0x0071:
            r1 = move-exception
            r3 = r4
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.ImageService.getSmsImageMaster():java.util.List");
    }
}
