package vc.lx.sms.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import vc.lx.sms.cmcc.http.data.Group;
import vc.lx.sms.cmcc.http.data.SmsTemplate;

public class SmsTemplateDbService {
    private Context mContext;
    private SmsSqliteHelper sqliteHelper = null;

    public SmsTemplateDbService(Context context) {
        this.sqliteHelper = new SmsSqliteHelper(context);
        this.mContext = context;
    }

    public void insertInitSmsTemplates() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.mContext.getAssets().open(SmsSqliteHelper.TABLE_SMS_TEMPLATES), Charset.forName("UTF-8")));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    String[] split = readLine.split("#");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(SmsSqliteHelper.CATEGORY_ID, split[0]);
                    contentValues.put(SmsSqliteHelper.CONTENT, split[1]);
                    contentValues.put(SmsSqliteHelper.PRIORITY, Integer.valueOf(split[2]));
                    this.mContext.getContentResolver().insert(SmsTemplateContentProvider.CONTENT_URI, contentValues);
                } else {
                    bufferedReader.close();
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Group<SmsTemplate> querySmsTemplates(int i, String str) {
        Group<SmsTemplate> group = new Group<>();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("_id").append(" asc").append(" limit ").append(10);
        if (i > 1) {
            stringBuffer.append(" offset ").append((i - 1) * 10);
        }
        Cursor query = this.mContext.getContentResolver().query(SmsTemplateContentProvider.CONTENT_URI, null, "category_id = ?", new String[]{str}, stringBuffer.toString());
        while (query != null && query.moveToNext()) {
            SmsTemplate smsTemplate = new SmsTemplate();
            smsTemplate._id = query.getInt(0);
            smsTemplate.mCategoryId = query.getString(1);
            smsTemplate.mContent = query.getString(3);
            smsTemplate.mId = String.valueOf(query.getInt(2));
            smsTemplate.mPriority = query.getInt(4);
            smsTemplate.mPage = query.getInt(5);
            smsTemplate.mVersion = query.getInt(6);
            group.add(smsTemplate);
        }
        group.page = String.valueOf(i);
        if (query != null) {
            query.close();
        }
        Cursor query2 = this.mContext.getContentResolver().query(SmsTemplateContentProvider.CONTENT_URI, new String[]{"count(*)"}, "category_id = ?", new String[]{String.valueOf(str)}, null);
        if (query2 != null && query2.moveToFirst()) {
            int i2 = query2.getInt(0);
            group.totalCount = String.valueOf(i2);
            int i3 = i2 / 10;
            group.totalPages = String.valueOf(i2 % 10 != 0 ? i3 + 1 : i3);
        }
        if (query2 != null) {
            query2.close();
        }
        return group;
    }
}
