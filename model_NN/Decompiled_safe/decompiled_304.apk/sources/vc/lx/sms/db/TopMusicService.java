package vc.lx.sms.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms2.R;

public class TopMusicService {
    private Context context;
    private SmsSqliteHelper sqliteHelper = null;

    public TopMusicService(Context context2) {
        this.context = context2;
        this.sqliteHelper = new SmsSqliteHelper(context2);
    }

    public void deleteAll() {
        SQLiteDatabase writableDatabase = this.sqliteHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            writableDatabase.execSQL("DELETE FROM  music_local WHERE _id > 0");
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (Throwable th) {
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
            throw th;
        }
    }

    public void deleteSongItem(String str) {
        SQLiteDatabase writableDatabase = this.sqliteHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            writableDatabase.execSQL("update music_local set hidestatus = 'yes' WHERE plug = '" + str + "' ");
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (Throwable th) {
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0081, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008d, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0094, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0095, code lost:
        r5 = r2;
        r2 = r1;
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009e, code lost:
        r5 = r2;
        r2 = r1;
        r1 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0094 A[ExcHandler: all (r2v8 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x0013] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public vc.lx.sms.cmcc.http.data.SongItem getSongItemByPlug(java.lang.String r7) {
        /*
            r6 = this;
            r4 = 0
            vc.lx.sms.db.SmsSqliteHelper r0 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()
            java.lang.String r1 = "select content_id, name, group_code, singer, plug, hidestatus, date, mp3_file  from music_local where plug = ?"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x007c, all -> 0x0089 }
            r3 = 0
            r2[r3] = r7     // Catch:{ Exception -> 0x007c, all -> 0x0089 }
            android.database.Cursor r1 = r0.rawQuery(r1, r2)     // Catch:{ Exception -> 0x007c, all -> 0x0089 }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x0099, all -> 0x0094 }
            if (r2 == 0) goto L_0x00a2
            vc.lx.sms.cmcc.http.data.SongItem r2 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x0099, all -> 0x0094 }
            r2.<init>()     // Catch:{ Exception -> 0x0099, all -> 0x0094 }
            java.lang.String r3 = "content_id"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            r2.contentid = r3     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = "group_code"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            r2.groupcode = r3     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = "name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            r2.song = r3     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = "singer"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            r2.singer = r3     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = "plug"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            r2.plug = r3     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = "mp3_file"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            r2.durl = r3     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = "hidestatus"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
            r2.hidestatus = r3     // Catch:{ Exception -> 0x009d, all -> 0x0094 }
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()
        L_0x0077:
            r0.close()
            r0 = r2
        L_0x007b:
            return r0
        L_0x007c:
            r1 = move-exception
            r1 = r4
            r2 = r4
        L_0x007f:
            if (r2 == 0) goto L_0x0084
            r2.close()
        L_0x0084:
            r0.close()
            r0 = r1
            goto L_0x007b
        L_0x0089:
            r1 = move-exception
            r2 = r4
        L_0x008b:
            if (r2 == 0) goto L_0x0090
            r2.close()
        L_0x0090:
            r0.close()
            throw r1
        L_0x0094:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x008b
        L_0x0099:
            r2 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x007f
        L_0x009d:
            r3 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x007f
        L_0x00a2:
            r2 = r4
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItemByPlug(java.lang.String):vc.lx.sms.cmcc.http.data.SongItem");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00eb, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00fe, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ff, code lost:
        r5 = r2;
        r2 = r1;
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0106, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0107, code lost:
        r3 = r1;
        r1 = r2;
        r2 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00fe A[ExcHandler: all (r2v9 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x0013] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public vc.lx.sms.cmcc.http.data.SongItem getSongItemNewByPlugCombinedType(java.lang.String r7) {
        /*
            r6 = this;
            r4 = 0
            vc.lx.sms.db.SmsSqliteHelper r0 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()
            java.lang.String r1 = "select content_id, name,master, group_code, singer, plug, date, music_tag ,hidestatus, mp3_file, favoratestatus, forward_count ,readstatus, type, _id  from music_local where plug = ?"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x00e3, all -> 0x00f3 }
            r3 = 0
            r2[r3] = r7     // Catch:{ Exception -> 0x00e3, all -> 0x00f3 }
            android.database.Cursor r1 = r0.rawQuery(r1, r2)     // Catch:{ Exception -> 0x00e3, all -> 0x00f3 }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x0106, all -> 0x00fe }
            if (r2 == 0) goto L_0x0110
            vc.lx.sms.cmcc.http.data.SongItem r2 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x0106, all -> 0x00fe }
            r2.<init>()     // Catch:{ Exception -> 0x0106, all -> 0x00fe }
            java.lang.String r3 = "content_id"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.contentid = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "group_code"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.groupcode = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.song = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "master"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.master = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "singer"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.singer = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "plug"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.plug = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "favoratestatus"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.favoratestatus = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "readstatus"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.readstatus = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "_id"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.id = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "mp3_file"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.durl = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "hidestatus"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.hidestatus = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r3.<init>()     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r4 = "type"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r4 = r1.getString(r4)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r4 = " "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.type = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "music_tag"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.tag = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = "forward_count"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
            r2.forward_count = r3     // Catch:{ Exception -> 0x010b, all -> 0x00fe }
        L_0x00d9:
            if (r1 == 0) goto L_0x00de
            r1.close()
        L_0x00de:
            r0.close()
            r0 = r2
        L_0x00e2:
            return r0
        L_0x00e3:
            r1 = move-exception
            r2 = r4
            r3 = r4
        L_0x00e6:
            r1.printStackTrace()     // Catch:{ all -> 0x0103 }
            if (r3 == 0) goto L_0x00ee
            r3.close()
        L_0x00ee:
            r0.close()
            r0 = r2
            goto L_0x00e2
        L_0x00f3:
            r1 = move-exception
            r2 = r4
        L_0x00f5:
            if (r2 == 0) goto L_0x00fa
            r2.close()
        L_0x00fa:
            r0.close()
            throw r1
        L_0x00fe:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x00f5
        L_0x0103:
            r1 = move-exception
            r2 = r3
            goto L_0x00f5
        L_0x0106:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x00e6
        L_0x010b:
            r3 = move-exception
            r5 = r3
            r3 = r1
            r1 = r5
            goto L_0x00e6
        L_0x0110:
            r2 = r4
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItemNewByPlugCombinedType(java.lang.String):vc.lx.sms.cmcc.http.data.SongItem");
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x018f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> getSongItems(java.util.HashMap<java.lang.String, java.lang.String> r11, java.lang.String r12, java.lang.String r13) {
        /*
            r10 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r10.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "SELECT content_id , name , group_code , singer , plug ,date,  ,hidestatus, favoratestatus ,readstatus , type , mp3_file , _id  FROM music_local"
            r3 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = ""
            r4.<init>(r5)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            if (r11 == 0) goto L_0x0064
            int r6 = r11.size()
            if (r6 <= 0) goto L_0x0064
            java.lang.String r6 = " WHERE "
            r4.append(r6)
            r6 = 0
            java.util.Set r7 = r11.keySet()
            java.util.Iterator r7 = r7.iterator()
        L_0x0030:
            boolean r8 = r7.hasNext()
            if (r8 == 0) goto L_0x0064
            java.lang.Object r10 = r7.next()
            java.lang.String r10 = (java.lang.String) r10
            java.lang.StringBuilder r8 = r4.append(r10)
            java.lang.String r9 = "="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = " ? "
            r8.append(r9)
            java.lang.Object r8 = r11.get(r10)
            r5.add(r8)
            java.util.Set r8 = r11.keySet()
            int r8 = r8.size()
            if (r6 == r8) goto L_0x0061
            java.lang.String r8 = " and "
            r4.append(r8)
        L_0x0061:
            int r6 = r6 + 1
            goto L_0x0030
        L_0x0064:
            if (r13 == 0) goto L_0x0097
            java.lang.String r6 = ""
            boolean r6 = r6.equals(r13)
            if (r6 != 0) goto L_0x0097
            if (r11 == 0) goto L_0x0076
            int r6 = r11.size()
            if (r6 != 0) goto L_0x007b
        L_0x0076:
            java.lang.String r6 = " WHERE "
            r4.append(r6)
        L_0x007b:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = " "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r13)
            java.lang.String r7 = " "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r4.append(r6)
        L_0x0097:
            if (r12 == 0) goto L_0x00b6
            java.lang.String r6 = ""
            boolean r6 = r6.equals(r12)
            if (r6 == 0) goto L_0x00b6
            if (r11 == 0) goto L_0x00a9
            int r6 = r11.size()
            if (r6 != 0) goto L_0x00ae
        L_0x00a9:
            java.lang.String r6 = " WHERE "
            r4.append(r6)
        L_0x00ae:
            java.lang.String r6 = " limit ? "
            r4.append(r6)
            r5.add(r12)
        L_0x00b6:
            java.lang.String r5 = ""
            boolean r5 = r5.equals(r4)
            if (r5 != 0) goto L_0x00cf
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
        L_0x00cf:
            r4 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r4)     // Catch:{ Exception -> 0x0198, all -> 0x018b }
        L_0x00d4:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            if (r3 == 0) goto L_0x0182
            vc.lx.sms.cmcc.http.data.SongItem r3 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.<init>()     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "content_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.contentid = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "group_code"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.groupcode = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "name"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.song = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "singer"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.singer = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "plug"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.plug = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "favoratestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.favoratestatus = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "readstatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.readstatus = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.id = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "mp3_file"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.durl = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = "hidestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.hidestatus = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r4.<init>()     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r5 = "type"
            int r5 = r2.getColumnIndex(r5)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r5 = r2.getString(r5)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r5 = " "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            r3.type = r4     // Catch:{ Exception -> 0x0178, all -> 0x0196 }
            goto L_0x00d4
        L_0x0178:
            r3 = move-exception
        L_0x0179:
            if (r2 == 0) goto L_0x017e
            r2.close()
        L_0x017e:
            r1.close()
        L_0x0181:
            return r0
        L_0x0182:
            if (r2 == 0) goto L_0x0187
            r2.close()
        L_0x0187:
            r1.close()
            goto L_0x0181
        L_0x018b:
            r0 = move-exception
            r2 = r3
        L_0x018d:
            if (r2 == 0) goto L_0x0192
            r2.close()
        L_0x0192:
            r1.close()
            throw r0
        L_0x0196:
            r0 = move-exception
            goto L_0x018d
        L_0x0198:
            r2 = move-exception
            r2 = r3
            goto L_0x0179
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItems(java.util.HashMap, java.lang.String, java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x00c5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> getSongItemsByFavorateStatus() {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r7.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "select content_id, name, group_code, singer, plug, hidestatus,date, mp3_file, favoratestatus, readstatus, type, _id from music_local where favoratestatus = ?"
            r3 = 0
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00ce, all -> 0x00c1 }
            r5 = 0
            java.lang.String r6 = "yes"
            r4[r5] = r6     // Catch:{ Exception -> 0x00ce, all -> 0x00c1 }
            android.database.Cursor r2 = r1.rawQuery(r2, r4)     // Catch:{ Exception -> 0x00ce, all -> 0x00c1 }
        L_0x001a:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            if (r3 == 0) goto L_0x00b8
            vc.lx.sms.cmcc.http.data.SongItem r3 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.<init>()     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "content_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.contentid = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "group_code"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.groupcode = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "name"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.song = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "singer"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.singer = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "plug"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.plug = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "favoratestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.favoratestatus = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "readstatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.readstatus = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "type"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.type = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.id = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "hidestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.hidestatus = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = "mp3_file"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r3.durl = r4     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            r0.add(r3)     // Catch:{ Exception -> 0x00ae, all -> 0x00cc }
            goto L_0x001a
        L_0x00ae:
            r3 = move-exception
        L_0x00af:
            if (r2 == 0) goto L_0x00b4
            r2.close()
        L_0x00b4:
            r1.close()
        L_0x00b7:
            return r0
        L_0x00b8:
            if (r2 == 0) goto L_0x00bd
            r2.close()
        L_0x00bd:
            r1.close()
            goto L_0x00b7
        L_0x00c1:
            r0 = move-exception
            r2 = r3
        L_0x00c3:
            if (r2 == 0) goto L_0x00c8
            r2.close()
        L_0x00c8:
            r1.close()
            throw r0
        L_0x00cc:
            r0 = move-exception
            goto L_0x00c3
        L_0x00ce:
            r2 = move-exception
            r2 = r3
            goto L_0x00af
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItemsByFavorateStatus():java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> getSongItemsByMaster() {
        /*
            r8 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r8.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "select content_id , name , group_code , singer ,hidestatus, plug ,date,favoratestatus,readstatus,type,_id ���master from music_local where master = ?"
            r3 = 0
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00cf, all -> 0x00bf }
            r5 = 0
            java.lang.String r6 = "0"
            r4[r5] = r6     // Catch:{ Exception -> 0x00cf, all -> 0x00bf }
            android.database.Cursor r2 = r1.rawQuery(r2, r4)     // Catch:{ Exception -> 0x00cf, all -> 0x00bf }
        L_0x001a:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            if (r3 == 0) goto L_0x00b6
            vc.lx.sms.cmcc.http.data.SongItem r3 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.<init>()     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "content_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.contentid = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "group_code"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.groupcode = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "name"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.song = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "singer"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.singer = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "plug"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.plug = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "favoratestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.favoratestatus = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "readstatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.readstatus = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "type"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.type = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "hidestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.hidestatus = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = "_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            int r4 = r2.getInt(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r3.id = r4     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            r0.add(r3)     // Catch:{ Exception -> 0x00a6, all -> 0x00ca }
            goto L_0x001a
        L_0x00a6:
            r3 = move-exception
            r7 = r3
            r3 = r2
            r2 = r7
        L_0x00aa:
            r2.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r3 == 0) goto L_0x00b2
            r3.close()
        L_0x00b2:
            r1.close()
        L_0x00b5:
            return r0
        L_0x00b6:
            if (r2 == 0) goto L_0x00bb
            r2.close()
        L_0x00bb:
            r1.close()
            goto L_0x00b5
        L_0x00bf:
            r0 = move-exception
            r2 = r3
        L_0x00c1:
            if (r2 == 0) goto L_0x00c6
            r2.close()
        L_0x00c6:
            r1.close()
            throw r0
        L_0x00ca:
            r0 = move-exception
            goto L_0x00c1
        L_0x00cc:
            r0 = move-exception
            r2 = r3
            goto L_0x00c1
        L_0x00cf:
            r2 = move-exception
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItemsByMaster():java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> getSongItemsByMaster(int r7, int r8) {
        /*
            r6 = this;
            r4 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "select distinct plug  from music_local where master = '1' limit "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = ","
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ Exception -> 0x006f, all -> 0x005f }
        L_0x002e:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            if (r3 == 0) goto L_0x0056
            java.lang.String r3 = "plug"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            vc.lx.sms.cmcc.http.data.SongItem r3 = r6.getSongItemNewByPlugCombinedType(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            r0.add(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            goto L_0x002e
        L_0x0046:
            r3 = move-exception
            r5 = r3
            r3 = r2
            r2 = r5
        L_0x004a:
            r2.printStackTrace()     // Catch:{ all -> 0x006c }
            if (r3 == 0) goto L_0x0052
            r3.close()
        L_0x0052:
            r1.close()
        L_0x0055:
            return r0
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r2.close()
        L_0x005b:
            r1.close()
            goto L_0x0055
        L_0x005f:
            r0 = move-exception
            r2 = r4
        L_0x0061:
            if (r2 == 0) goto L_0x0066
            r2.close()
        L_0x0066:
            r1.close()
            throw r0
        L_0x006a:
            r0 = move-exception
            goto L_0x0061
        L_0x006c:
            r0 = move-exception
            r2 = r3
            goto L_0x0061
        L_0x006f:
            r2 = move-exception
            r3 = r4
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItemsByMaster(int, int):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x00cf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> getSongItemsByPlug(java.lang.String r7) {
        /*
            r6 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "select content_id, name, group_code, singer, plug, date, mp3_file, hidestatus,favoratestatus, readstatus, type, _id  from music_local where plug = ? ORDER BY date DESC "
            r3 = 0
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00d8, all -> 0x00cb }
            r5 = 0
            r4[r5] = r7     // Catch:{ Exception -> 0x00d8, all -> 0x00cb }
            android.database.Cursor r2 = r1.rawQuery(r2, r4)     // Catch:{ Exception -> 0x00d8, all -> 0x00cb }
        L_0x0018:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            if (r3 == 0) goto L_0x00c2
            vc.lx.sms.cmcc.http.data.SongItem r3 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.<init>()     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "content_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.contentid = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "group_code"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.groupcode = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "name"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.song = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "singer"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.singer = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "plug"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.plug = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "favoratestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.favoratestatus = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "readstatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.readstatus = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "type"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.type = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.id = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "date"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.date = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "hidestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.hidestatus = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = "mp3_file"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r3.durl = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            r0.add(r3)     // Catch:{ Exception -> 0x00b8, all -> 0x00d6 }
            goto L_0x0018
        L_0x00b8:
            r3 = move-exception
        L_0x00b9:
            if (r2 == 0) goto L_0x00be
            r2.close()
        L_0x00be:
            r1.close()
        L_0x00c1:
            return r0
        L_0x00c2:
            if (r2 == 0) goto L_0x00c7
            r2.close()
        L_0x00c7:
            r1.close()
            goto L_0x00c1
        L_0x00cb:
            r0 = move-exception
            r2 = r3
        L_0x00cd:
            if (r2 == 0) goto L_0x00d2
            r2.close()
        L_0x00d2:
            r1.close()
            throw r0
        L_0x00d6:
            r0 = move-exception
            goto L_0x00cd
        L_0x00d8:
            r2 = move-exception
            r2 = r3
            goto L_0x00b9
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItemsByPlug(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> getSongItemsByReadStatus(java.lang.String r8) {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r7.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "select content_id , name , group_code , singer ,hidestatus, plug ,date,favoratestatus,readstatus,type,_id  from music_local where readstatus = ?"
            r3 = 0
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00c9, all -> 0x00b9 }
            r5 = 0
            r4[r5] = r8     // Catch:{ Exception -> 0x00c9, all -> 0x00b9 }
            android.database.Cursor r2 = r1.rawQuery(r2, r4)     // Catch:{ Exception -> 0x00c9, all -> 0x00b9 }
        L_0x0018:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            if (r3 == 0) goto L_0x00b0
            vc.lx.sms.cmcc.http.data.SongItem r3 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.<init>()     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "content_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.contentid = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "group_code"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.groupcode = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "name"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.song = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "singer"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.singer = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "plug"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.plug = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "favoratestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.favoratestatus = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "readstatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.readstatus = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "type"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.type = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "hidestatus"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.hidestatus = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = "_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r3.id = r4     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            r0.add(r3)     // Catch:{ Exception -> 0x00a0, all -> 0x00c4 }
            goto L_0x0018
        L_0x00a0:
            r3 = move-exception
            r6 = r3
            r3 = r2
            r2 = r6
        L_0x00a4:
            r2.printStackTrace()     // Catch:{ all -> 0x00c6 }
            if (r3 == 0) goto L_0x00ac
            r3.close()
        L_0x00ac:
            r1.close()
        L_0x00af:
            return r0
        L_0x00b0:
            if (r2 == 0) goto L_0x00b5
            r2.close()
        L_0x00b5:
            r1.close()
            goto L_0x00af
        L_0x00b9:
            r0 = move-exception
            r2 = r3
        L_0x00bb:
            if (r2 == 0) goto L_0x00c0
            r2.close()
        L_0x00c0:
            r1.close()
            throw r0
        L_0x00c4:
            r0 = move-exception
            goto L_0x00bb
        L_0x00c6:
            r0 = move-exception
            r2 = r3
            goto L_0x00bb
        L_0x00c9:
            r2 = move-exception
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getSongItemsByReadStatus(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getTotalCount() {
        /*
            r6 = this;
            r4 = 0
            vc.lx.sms.db.SmsSqliteHelper r0 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()
            java.lang.String r1 = "SELECT count(*) AS count FROM music_local"
            r2 = 0
            r3 = 0
            android.database.Cursor r1 = r0.rawQuery(r1, r3)     // Catch:{ Exception -> 0x002a, all -> 0x0036 }
        L_0x000f:
            boolean r3 = r1.moveToNext()     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            if (r3 == 0) goto L_0x0020
            java.lang.String r3 = "count"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            int r2 = r1.getInt(r3)     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            goto L_0x000f
        L_0x0020:
            if (r1 == 0) goto L_0x0025
            r1.close()
        L_0x0025:
            r0.close()
            r0 = r2
        L_0x0029:
            return r0
        L_0x002a:
            r1 = move-exception
            r1 = r4
        L_0x002c:
            if (r1 == 0) goto L_0x0031
            r1.close()
        L_0x0031:
            r0.close()
            r0 = r2
            goto L_0x0029
        L_0x0036:
            r1 = move-exception
            r2 = r4
        L_0x0038:
            if (r2 == 0) goto L_0x003d
            r2.close()
        L_0x003d:
            r0.close()
            throw r1
        L_0x0041:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0038
        L_0x0046:
            r3 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.getTotalCount():int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void insert(List<SongItem> list) {
        SQLiteDatabase writableDatabase = this.sqliteHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            for (int i = 0; i < list.size(); i++) {
                SongItem songItem = list.get(i);
                ContentValues contentValues = new ContentValues();
                if (songItem.plug != null) {
                    int songItemExistId = songItemExistId(songItem.plug, songItem.type, writableDatabase);
                    if (songItemExistId == -1) {
                        contentValues.put(SmsSqliteHelper.CONTENT_ID, songItem.contentid);
                        contentValues.put("name", songItem.song);
                        contentValues.put(SmsSqliteHelper.GROUP_CODE, songItem.groupcode);
                        contentValues.put("singer", songItem.singer);
                        contentValues.put(SmsSqliteHelper.PLUG, songItem.plug);
                        contentValues.put("type", songItem.type);
                        contentValues.put("date", simpleDateFormat.format(date));
                        contentValues.put(SmsSqliteHelper.FORWARD_COUNT, songItem.forward_count);
                        contentValues.put(SmsSqliteHelper.LISTEN_COUNT, songItem.listen_count);
                        contentValues.put("music_tag", songItem.tag);
                        contentValues.put(SmsSqliteHelper.MASTER, (Integer) 0);
                        contentValues.put(SmsSqliteHelper.MP3_FILE, songItem.durl);
                        contentValues.put(SmsSqliteHelper.READSTATUS, "no");
                        contentValues.put(SmsSqliteHelper.HIDE_STATUS, "no");
                        if (songItem.type != null) {
                            if (songItem.type.equals(this.context.getString(R.string.system_introduce))) {
                                contentValues.put(SmsSqliteHelper.READSTATUS, "no");
                            } else {
                                contentValues.put(SmsSqliteHelper.READSTATUS, "yes");
                            }
                        }
                        contentValues.put(SmsSqliteHelper.FAVORATESTATUS, "no");
                        writableDatabase.insert(SmsSqliteHelper.TABLE_MUSIC_LOCAL, "", contentValues);
                    } else {
                        updateDateById(songItemExistId, simpleDateFormat.format(date));
                    }
                }
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (Throwable th) {
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
            throw th;
        }
    }

    public void insert(SongItem songItem) {
        SQLiteDatabase writableDatabase = this.sqliteHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            ContentValues contentValues = new ContentValues();
            if (songItem.plug == null) {
                writableDatabase.setTransactionSuccessful();
                writableDatabase.endTransaction();
                if (writableDatabase != null) {
                    writableDatabase.close();
                    return;
                }
                return;
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            int songItemExistId = songItemExistId(songItem.plug, songItem.type, writableDatabase);
            if (songItemExistId == -1) {
                contentValues.put(SmsSqliteHelper.CONTENT_ID, songItem.contentid);
                contentValues.put("name", songItem.song);
                contentValues.put(SmsSqliteHelper.GROUP_CODE, songItem.groupcode);
                contentValues.put("singer", songItem.singer);
                contentValues.put(SmsSqliteHelper.PLUG, songItem.plug);
                contentValues.put("type", songItem.type);
                contentValues.put(SmsSqliteHelper.MP3_FILE, songItem.durl);
                contentValues.put("date", simpleDateFormat.format(date));
                contentValues.put(SmsSqliteHelper.HIDE_STATUS, "no");
                contentValues.put(SmsSqliteHelper.READSTATUS, "no");
                if (songItem.type != null) {
                    if (songItem.type.equals(this.context.getString(R.string.system_introduce))) {
                        contentValues.put(SmsSqliteHelper.READSTATUS, "no");
                    } else {
                        contentValues.put(SmsSqliteHelper.READSTATUS, "yes");
                    }
                }
                contentValues.put(SmsSqliteHelper.FAVORATESTATUS, "no");
                writableDatabase.insert(SmsSqliteHelper.TABLE_MUSIC_LOCAL, "", contentValues);
            } else {
                updateDateById(songItemExistId, simpleDateFormat.format(date));
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (Throwable th) {
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
            throw th;
        }
    }

    public void insert(SongItem songItem, int i) {
        SQLiteDatabase writableDatabase = this.sqliteHelper.getWritableDatabase();
        try {
            writableDatabase.beginTransaction();
            ContentValues contentValues = new ContentValues();
            if (songItem.plug == null) {
                writableDatabase.setTransactionSuccessful();
                writableDatabase.endTransaction();
                if (writableDatabase != null) {
                    writableDatabase.close();
                    return;
                }
                return;
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            int songItemExistId = songItemExistId(songItem.plug, songItem.type, writableDatabase);
            if (songItemExistId == -1) {
                contentValues.put(SmsSqliteHelper.CONTENT_ID, songItem.contentid);
                contentValues.put("name", songItem.song);
                contentValues.put(SmsSqliteHelper.GROUP_CODE, songItem.groupcode);
                contentValues.put("singer", songItem.singer);
                contentValues.put(SmsSqliteHelper.PLUG, songItem.plug);
                contentValues.put("type", songItem.type);
                contentValues.put(SmsSqliteHelper.MP3_FILE, songItem.durl);
                contentValues.put(SmsSqliteHelper.MASTER, Integer.valueOf(i));
                contentValues.put("date", simpleDateFormat.format(date));
                contentValues.put(SmsSqliteHelper.HIDE_STATUS, "no");
                contentValues.put(SmsSqliteHelper.READSTATUS, "no");
                if (songItem.type != null) {
                    if (songItem.type.equals(this.context.getString(R.string.system_introduce))) {
                        contentValues.put(SmsSqliteHelper.READSTATUS, "no");
                    } else {
                        contentValues.put(SmsSqliteHelper.READSTATUS, "yes");
                    }
                }
                contentValues.put(SmsSqliteHelper.FAVORATESTATUS, "no");
                writableDatabase.insert(SmsSqliteHelper.TABLE_MUSIC_LOCAL, "", contentValues);
            } else {
                updateDateById(songItemExistId, simpleDateFormat.format(date));
            }
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
        } catch (Throwable th) {
            writableDatabase.setTransactionSuccessful();
            writableDatabase.endTransaction();
            if (writableDatabase != null) {
                writableDatabase.close();
            }
            throw th;
        }
    }

    public List<SongItem> queryAll() {
        return querySongs(-1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> queryAllNew(int r7, int r8) {
        /*
            r6 = this;
            r4 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "select distinct plug  from music_local where hidestatus = 'no' and master = 0 or master = 1 limit "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = ","
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ Exception -> 0x006f, all -> 0x005f }
        L_0x002e:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            if (r3 == 0) goto L_0x0056
            java.lang.String r3 = "plug"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            vc.lx.sms.cmcc.http.data.SongItem r3 = r6.getSongItemNewByPlugCombinedType(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            r0.add(r3)     // Catch:{ Exception -> 0x0046, all -> 0x006a }
            goto L_0x002e
        L_0x0046:
            r3 = move-exception
            r5 = r3
            r3 = r2
            r2 = r5
        L_0x004a:
            r2.printStackTrace()     // Catch:{ all -> 0x006c }
            if (r3 == 0) goto L_0x0052
            r3.close()
        L_0x0052:
            r1.close()
        L_0x0055:
            return r0
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r2.close()
        L_0x005b:
            r1.close()
            goto L_0x0055
        L_0x005f:
            r0 = move-exception
            r2 = r4
        L_0x0061:
            if (r2 == 0) goto L_0x0066
            r2.close()
        L_0x0066:
            r1.close()
            throw r0
        L_0x006a:
            r0 = move-exception
            goto L_0x0061
        L_0x006c:
            r0 = move-exception
            r2 = r3
            goto L_0x0061
        L_0x006f:
            r2 = move-exception
            r3 = r4
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.queryAllNew(int, int):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> queryFavorateAllNew(java.lang.String r7) {
        /*
            r6 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "select distinct plug  from music_local where hidestatus = 'no' and favoratestatus = ?"
            r3 = 0
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0050, all -> 0x0043 }
            r5 = 0
            r4[r5] = r7     // Catch:{ Exception -> 0x0050, all -> 0x0043 }
            android.database.Cursor r2 = r1.rawQuery(r2, r4)     // Catch:{ Exception -> 0x0050, all -> 0x0043 }
        L_0x0018:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x0030, all -> 0x004e }
            if (r3 == 0) goto L_0x003a
            java.lang.String r3 = "plug"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x0030, all -> 0x004e }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x0030, all -> 0x004e }
            vc.lx.sms.cmcc.http.data.SongItem r3 = r6.getSongItemNewByPlugCombinedType(r3)     // Catch:{ Exception -> 0x0030, all -> 0x004e }
            r0.add(r3)     // Catch:{ Exception -> 0x0030, all -> 0x004e }
            goto L_0x0018
        L_0x0030:
            r3 = move-exception
        L_0x0031:
            if (r2 == 0) goto L_0x0036
            r2.close()
        L_0x0036:
            r1.close()
        L_0x0039:
            return r0
        L_0x003a:
            if (r2 == 0) goto L_0x003f
            r2.close()
        L_0x003f:
            r1.close()
            goto L_0x0039
        L_0x0043:
            r0 = move-exception
            r2 = r3
        L_0x0045:
            if (r2 == 0) goto L_0x004a
            r2.close()
        L_0x004a:
            r1.close()
            throw r0
        L_0x004e:
            r0 = move-exception
            goto L_0x0045
        L_0x0050:
            r2 = move-exception
            r2 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.queryFavorateAllNew(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0048  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> querySongItemByMasterThree() {
        /*
            r6 = this;
            r4 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r6.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "select distinct plug  from music_local where hidestatus = 'no' and master = 3 "
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ Exception -> 0x0054, all -> 0x0044 }
        L_0x0013:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x002b, all -> 0x004f }
            if (r3 == 0) goto L_0x003b
            java.lang.String r3 = "plug"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x002b, all -> 0x004f }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x002b, all -> 0x004f }
            vc.lx.sms.cmcc.http.data.SongItem r3 = r6.getSongItemNewByPlugCombinedType(r3)     // Catch:{ Exception -> 0x002b, all -> 0x004f }
            r0.add(r3)     // Catch:{ Exception -> 0x002b, all -> 0x004f }
            goto L_0x0013
        L_0x002b:
            r3 = move-exception
            r5 = r3
            r3 = r2
            r2 = r5
        L_0x002f:
            r2.printStackTrace()     // Catch:{ all -> 0x0051 }
            if (r3 == 0) goto L_0x0037
            r3.close()
        L_0x0037:
            r1.close()
        L_0x003a:
            return r0
        L_0x003b:
            if (r2 == 0) goto L_0x0040
            r2.close()
        L_0x0040:
            r1.close()
            goto L_0x003a
        L_0x0044:
            r0 = move-exception
            r2 = r4
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            r2.close()
        L_0x004b:
            r1.close()
            throw r0
        L_0x004f:
            r0 = move-exception
            goto L_0x0046
        L_0x0051:
            r0 = move-exception
            r2 = r3
            goto L_0x0046
        L_0x0054:
            r2 = move-exception
            r3 = r4
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.querySongItemByMasterThree():java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<vc.lx.sms.cmcc.http.data.SongItem> querySongs(int r6) {
        /*
            r5 = this;
            r4 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            vc.lx.sms.db.SmsSqliteHelper r1 = r5.sqliteHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r2 = "select content_id, name, group_code, singer, plug, date, mp3_file  from music_local where master !=3 order by date asc"
            if (r6 <= 0) goto L_0x0027
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " limit "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
        L_0x0027:
            r3 = 0
            android.database.Cursor r2 = r1.rawQuery(r2, r3)     // Catch:{ Exception -> 0x00a3, all -> 0x0096 }
        L_0x002c:
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            if (r3 == 0) goto L_0x008d
            vc.lx.sms.cmcc.http.data.SongItem r3 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r3.<init>()     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = "content_id"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r3.contentid = r4     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = "group_code"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r3.groupcode = r4     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = "name"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r3.song = r4     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = "singer"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r3.singer = r4     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = "plug"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r3.plug = r4     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = "mp3_file"
            int r4 = r2.getColumnIndex(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            java.lang.String r4 = r2.getString(r4)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r3.durl = r4     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            r0.add(r3)     // Catch:{ Exception -> 0x0083, all -> 0x00a1 }
            goto L_0x002c
        L_0x0083:
            r3 = move-exception
        L_0x0084:
            if (r2 == 0) goto L_0x0089
            r2.close()
        L_0x0089:
            r1.close()
        L_0x008c:
            return r0
        L_0x008d:
            if (r2 == 0) goto L_0x0092
            r2.close()
        L_0x0092:
            r1.close()
            goto L_0x008c
        L_0x0096:
            r0 = move-exception
            r2 = r4
        L_0x0098:
            if (r2 == 0) goto L_0x009d
            r2.close()
        L_0x009d:
            r1.close()
            throw r0
        L_0x00a1:
            r0 = move-exception
            goto L_0x0098
        L_0x00a3:
            r2 = move-exception
            r2 = r4
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.querySongs(int):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x007a, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0086, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        r7 = r2;
        r2 = r1;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0097, code lost:
        r7 = r2;
        r2 = r1;
        r1 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008d A[ExcHandler: all (r2v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x000e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public vc.lx.sms.cmcc.http.data.SongItem qureySingleRowRandom() {
        /*
            r8 = this;
            r6 = 0
            vc.lx.sms.db.SmsSqliteHelper r0 = r8.sqliteHelper
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()
            java.lang.String r1 = "select content_id, name, group_code, singer, plug, mp3_file from music_local"
            r2 = 0
            android.database.Cursor r1 = r0.rawQuery(r1, r2)     // Catch:{ Exception -> 0x0075, all -> 0x0082 }
            double r2 = java.lang.Math.random()     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            int r4 = r1.getCount()     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r5 = 1
            int r4 = r4 - r5
            double r4 = (double) r4     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            double r2 = r2 * r4
            int r2 = (int) r2     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r1.moveToPosition(r2)     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            vc.lx.sms.cmcc.http.data.SongItem r2 = new vc.lx.sms.cmcc.http.data.SongItem     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r2.<init>()     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            java.lang.String r3 = "content_id"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r2.contentid = r3     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = "group_code"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r2.groupcode = r3     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = "name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r2.song = r3     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = "singer"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r2.singer = r3     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = "plug"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r2.plug = r3     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = "mp3_file"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            r2.durl = r3     // Catch:{ Exception -> 0x0096, all -> 0x008d }
            if (r1 == 0) goto L_0x0070
            r1.close()
        L_0x0070:
            r0.close()
            r0 = r2
        L_0x0074:
            return r0
        L_0x0075:
            r1 = move-exception
            r1 = r6
            r2 = r6
        L_0x0078:
            if (r2 == 0) goto L_0x007d
            r2.close()
        L_0x007d:
            r0.close()
            r0 = r1
            goto L_0x0074
        L_0x0082:
            r1 = move-exception
            r2 = r6
        L_0x0084:
            if (r2 == 0) goto L_0x0089
            r2.close()
        L_0x0089:
            r0.close()
            throw r1
        L_0x008d:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0084
        L_0x0092:
            r2 = move-exception
            r2 = r1
            r1 = r6
            goto L_0x0078
        L_0x0096:
            r3 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.qureySingleRowRandom():vc.lx.sms.cmcc.http.data.SongItem");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int songItemExistId(java.lang.String r6, java.lang.String r7, android.database.sqlite.SQLiteDatabase r8) {
        /*
            r5 = this;
            java.lang.String r0 = "SELECT _id FROM music_local WHERE plug = ? and type = ?"
            r1 = 0
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            r3 = 0
            r2[r3] = r6     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            r3 = 1
            r2[r3] = r7     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            android.database.Cursor r0 = r8.rawQuery(r0, r2)     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            if (r1 == 0) goto L_0x0027
            java.lang.String r1 = "_id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            if (r0 == 0) goto L_0x0025
            r0.close()
        L_0x0025:
            r0 = r1
        L_0x0026:
            return r0
        L_0x0027:
            if (r0 == 0) goto L_0x002c
            r0.close()
        L_0x002c:
            r0 = -1
            goto L_0x0026
        L_0x002e:
            r0 = move-exception
            r0 = r1
        L_0x0030:
            if (r0 == 0) goto L_0x002c
            r0.close()
            goto L_0x002c
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0037
        L_0x0042:
            r1 = move-exception
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: vc.lx.sms.db.TopMusicService.songItemExistId(java.lang.String, java.lang.String, android.database.sqlite.SQLiteDatabase):int");
    }

    public void updateDateById(int i, String str) {
        this.sqliteHelper.getReadableDatabase().execSQL("update music_local set date = '" + str + "' where _id = " + i);
    }

    public void updateFavorateStatusByPlug(String str, String str2) {
        this.sqliteHelper.getReadableDatabase().execSQL("update music_local set favoratestatus = '" + str2 + "' where plug = '" + str + "'");
    }

    public void updateMusicMaster(String str) {
        this.sqliteHelper.getReadableDatabase().execSQL("update music_local set  master = '1' where plug = '" + str + "'");
    }

    public void updateReadStatusByPlug(String str, String str2) {
        this.sqliteHelper.getReadableDatabase().execSQL("update music_local set readstatus = '" + str2 + "' where plug = '" + str + "'");
    }
}
