package vc.lx.sms.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import vc.lx.sms2.SmsApplication;

public class SmsSqliteHelper extends SQLiteOpenHelper {
    public static final String APP_heat = "app_heat";
    public static final String APP_id = "app_id";
    public static final String APP_name = "name";
    public static final String APP_visible = "visible";
    public static final String CATEGORY_ID = "category_id";
    public static final int CATEGORY_INDEX = 1;
    public static final String CONTENT = "content";
    public static final String CONTENT_ID = "content_id";
    public static final int CONTENT_INDEX = 3;
    private static final String DATABASE_NAME = "sms.db";
    private static final int DATABASE_VERSION = 3;
    public static final String DATE = "date";
    public static final String FAVORATESTATUS = "favoratestatus";
    public static final String FORWARD_COUNT = "forward_count";
    public static final String GROUP_CODE = "group_code";
    public static final String HIDE_STATUS = "hidestatus";
    public static final String ID = "_id";
    public static final String IMAGE = "image";
    public static final int IMAGE_INDEX = 2;
    public static final String LISTEN_COUNT = "listen_count";
    public static final String MASTER = "master";
    public static final String MP3_FILE = "mp3_file";
    public static final String MUSIC_MASTER = " master";
    public static final String NAME = "name";
    public static final String PAGE = "page";
    public static final int PAGE_INDEX = 5;
    public static final String PLUG = "plug";
    public static final String PRIORITY = "priority";
    public static final int PRIORITY_INDEX = 4;
    public static final String READSTATUS = "readstatus";
    public static final String RECIPIENT = "recipient";
    public static final int RECIPIENT_INDEX = 3;
    public static final String SINGER = "singer";
    public static final String SMS_CONTENT = "sms_content";
    public static final int SMS_CONTENT_INDEX = 4;
    public static final String STATUS = "status";
    public static final int STATUS_FAILED = 1;
    public static final int STATUS_INDEX = 5;
    public static final int STATUS_PENDING = 0;
    public static final int STATUS_SUCCESS = 2;
    public static final String TABLE_APP_LOCAL = "app_local";
    public static final String TABLE_IMAGES = "images";
    public static final String TABLE_IMAGE_DUMP = "image_dump";
    public static final String TABLE_MUSIC_LOCAL = "music_local";
    public static final String TABLE_SMS_TEMPLATES = "sms_templates";
    public static final String TABLE_VOTE_ANSWERS = "vote_answers";
    public static final String TABLE_VOTE_DRAFT = "vote_draft";
    public static final String TABLE_VOTE_OPTIONS = "vote_options";
    public static final String TABLE_VOTE_QUESTIONS = "vote_questions";
    public static final String TEMPLATE_ID = "template_id";
    public static final int TEMPLATE_ID_INDEX = 2;
    public static final String THUMB = "thumb";
    public static final String TYPE = "type";
    public static final String URL = "url";
    public static final String UUID = "uuid";
    public static final int UUID_INDEX = 1;
    public static final String VERSION = "version";
    public static final int VERSION_INDEX = 6;
    private Context mContext;

    public SmsSqliteHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 3);
        this.mContext = context;
    }

    private void alterImageTable(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("ALTER TABLE images ADD COLUMN master INTEGER;");
    }

    private void createImageDumpTable(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("CREATE TABLE image_dump ( _id INTEGER PRIMARY KEY autoincrement, uuid TEXT,  image BLOB, recipient TEXT, sms_content TEXT, status INTEGER  );");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createSmsTemplateTable(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("CREATE TABLE sms_templates ( _id INTEGER PRIMARY KEY autoincrement, category_id TEXT,  template_id Integer,  content TEXT,  priority Integer, page Integer,  version Integer  );");
        } catch (Exception e) {
            e.printStackTrace();
        }
        SmsApplication.getInstance().importInitSmsTemplates();
    }

    private void createVoteTables(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("CREATE TABLE vote_questions (_id INTEGER PRIMARY KEY autoincrement,service_id Integer, title TEXT, contact_name TEXT, tracer_flag TEXT default '0', answer_flag TEXT default '0', multi_select Integer, can_modify Integer, is_new Integer, is_user_created Integer, counter Integer, created_at DATE, parent_id Integer, phone_num TEXT, plug TEXT, updated_at DATE );");
            sQLiteDatabase.execSQL("CREATE TABLE vote_options (_id INTEGER PRIMARY KEY autoincrement, service_id Integer, vote_id Integer, option_text TEXT, counter Integer, contact_name TEXT, display_order Integer, created_at DATE, updated_at DATE , FOREIGN KEY(vote_id) REFERENCES vote_questions(_id) );");
            sQLiteDatabase.execSQL("CREATE TABLE vote_answers ( _id INTEGER PRIMARY KEY autoincrement, vote_id Integer,  option_id Integer,plug TEXT, voted_at DATE , FOREIGN KEY(vote_id) REFERENCES vote_questions(_id) , FOREIGN KEY(option_id) REFERENCES vote_options(_id)  );");
            sQLiteDatabase.execSQL("CREATE TABLE vote_draft ( _id INTEGER PRIMARY KEY autoincrement, vote_id Integer,  thread_id Long,  numbers TEXT,  sms_content TEXT );");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        if (!sQLiteDatabase.isReadOnly()) {
            sQLiteDatabase.execSQL("PRAGMA foreign_keys=ON;");
        }
        sQLiteDatabase.execSQL("CREATE TABLE music_local (content_id INTEGER,name TEXT, group_code TEXT,singer TEXT,plug TEXT ,date DATE,favoratestatus TEXT,readstatus TEXT,type TEXT,hidestatus TEXT, forward_count  TEXT,listen_count  TEXT,master  TEXT,music_tag  TEXT,mp3_file TEXT,_id INTEGER PRIMARY KEY AUTOINCREMENT );");
        sQLiteDatabase.execSQL("CREATE TABLE app_local (id INTEGER PRIMARY KEY autoincrement,app_id TEXT, app_heat Integer, name TEXT,visible TEXT);");
        sQLiteDatabase.execSQL("CREATE TABLE images (_id INTEGER PRIMARY KEY autoincrement,plug TEXT, url TEXT, thumb TEXT);");
        createVoteTables(sQLiteDatabase);
        createSmsTemplateTable(sQLiteDatabase);
        createImageDumpTable(sQLiteDatabase);
        alterImageTable(sQLiteDatabase);
    }

    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        if (!sQLiteDatabase.isReadOnly()) {
            sQLiteDatabase.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i <= 2 && !sQLiteDatabase.isReadOnly()) {
            createSmsTemplateTable(sQLiteDatabase);
            createVoteTables(sQLiteDatabase);
            createImageDumpTable(sQLiteDatabase);
            alterImageTable(sQLiteDatabase);
        }
    }
}
