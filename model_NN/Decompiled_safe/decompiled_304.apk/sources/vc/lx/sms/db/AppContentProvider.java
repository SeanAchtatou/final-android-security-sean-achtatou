package vc.lx.sms.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class AppContentProvider extends ContentProvider {
    public static final int ALL_APPS = 1;
    public static final String AUTHORITY = "mobi.recloud.app";
    public static final Uri CONTENT_URI = Uri.parse("content://mobi.recloud.app");
    public static final int SINGLE_APPS = 2;
    public static final String SONGS_ITEM_TYPE = "vnd.android.cursor.item/app";
    public static final String SONGS_TYPE = "vnd.android.cursor.dir/app";
    static final UriMatcher matcher = new UriMatcher(-1);
    private SmsSqliteHelper mDbHelper;

    static {
        matcher.addURI(AUTHORITY, "app", 1);
        matcher.addURI(AUTHORITY, "app/#", 2);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        switch (matcher.match(uri)) {
            case 1:
                return SONGS_TYPE;
            case 2:
                return SONGS_ITEM_TYPE;
            default:
                return null;
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        long insert;
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                if (contentValues != null) {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_APP_LOCAL, "", contentValues);
                    break;
                } else {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_APP_LOCAL, null, contentValues);
                    break;
                }
            default:
                insert = 0;
                break;
        }
        if (insert > 0) {
            return Uri.parse("content://mobi.recloud.app/" + insert);
        }
        return null;
    }

    public boolean onCreate() {
        this.mDbHelper = new SmsSqliteHelper(getContext());
        return this.mDbHelper != null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        SQLiteDatabase readableDatabase = this.mDbHelper.getReadableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                return readableDatabase.rawQuery("SELECT id as _id  , app_id ,app_heat, name ,visible FROM app_local", null);
            default:
                return null;
        }
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                return writableDatabase.update(SmsSqliteHelper.TABLE_APP_LOCAL, contentValues, str, strArr);
            default:
                return 0;
        }
    }
}
