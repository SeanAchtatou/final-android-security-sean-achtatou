package vc.lx.sms.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class VoteContentProvider extends ContentProvider {
    public static final int ALL_VOTES = 1;
    public static final String AUTHORITY = "vc.lx.richtext.votes";
    public static final Uri CONTENT_URI = Uri.parse("content://vc.lx.richtext.votes");
    public static final int SINGLE_VOTES = 2;
    public static final Uri VOTEDRAFT_CONTENT_URI = Uri.parse("content://vc.lx.richtext.votes/vote_draft");
    public static final Uri VOTEOPTIONS_CONTENT_URI = Uri.parse("content://vc.lx.richtext.votes/vote_options");
    public static final Uri VOTEQUESTIONS_CONTENT_URI = Uri.parse("content://vc.lx.richtext.votes/vote_questions");
    public static final String VOTES_ITEM_TYPE = "vnd.android.cursor.item/vote";
    public static final String VOTES_TYPE = "vnd.android.cursor.dir/vote";
    public static final int VOTE_DRAFT = 12;
    public static final int VOTE_OPTIONS = 4;
    public static final int VOTE_OPTION_UPDATE_COUNTER = 8;
    public static final int VOTE_QUESTIONS = 3;
    public static final int VOTE_UPDATE_COUNTER = 5;
    public static final int VOTE_UPDATE_OPTION_BY_CLI_ID = 7;
    public static final int VOTE_UPDATE_OPTION_BY_SER_ID = 10;
    public static final int VOTE_UPDATE_QUESTION_BY_CLI_ID = 6;
    public static final int VOTE_UPDATE_QUESTION_BY_PLUG = 11;
    public static final int VOTE_UPDATE_QUESTION_BY_SER_ID = 9;
    static final UriMatcher matcher = new UriMatcher(-1);
    private SmsSqliteHelper mDbHelper;

    static {
        matcher.addURI(AUTHORITY, null, 1);
        matcher.addURI(AUTHORITY, "/#", 2);
        matcher.addURI(AUTHORITY, SmsSqliteHelper.TABLE_VOTE_QUESTIONS, 3);
        matcher.addURI(AUTHORITY, SmsSqliteHelper.TABLE_VOTE_OPTIONS, 4);
        matcher.addURI(AUTHORITY, "vote_questions/#", 5);
        matcher.addURI(AUTHORITY, "vote_questions/cli_id/#", 6);
        matcher.addURI(AUTHORITY, "vote_options/cli_id/#", 7);
        matcher.addURI(AUTHORITY, "vote_questions/ser_id/#", 9);
        matcher.addURI(AUTHORITY, "vote_options/ser_id/#", 10);
        matcher.addURI(AUTHORITY, "vote_questions/plug", 11);
        matcher.addURI(AUTHORITY, "vote_options/#", 8);
        matcher.addURI(AUTHORITY, SmsSqliteHelper.TABLE_VOTE_DRAFT, 12);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                return writableDatabase.delete(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, str, strArr);
            case 3:
                return writableDatabase.delete(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, str, strArr);
            case 4:
                return writableDatabase.delete(SmsSqliteHelper.TABLE_VOTE_OPTIONS, str, strArr);
            case 5:
                return writableDatabase.delete(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, " _id = ? ", new String[]{String.valueOf(ContentUris.parseId(uri))});
            case 12:
                return writableDatabase.delete(SmsSqliteHelper.TABLE_VOTE_DRAFT, str, strArr);
            default:
                return 0;
        }
    }

    public String getType(Uri uri) {
        switch (matcher.match(uri)) {
            case 1:
                return VOTES_TYPE;
            case 2:
                return VOTES_ITEM_TYPE;
            default:
                return VOTES_TYPE;
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        long insert;
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                insert = 0;
                break;
            case 3:
                if (contentValues != null) {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, "", contentValues);
                    break;
                } else {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, null, contentValues);
                    break;
                }
            case 4:
                if (contentValues != null) {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_VOTE_OPTIONS, "", contentValues);
                    break;
                } else {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_VOTE_OPTIONS, null, contentValues);
                    break;
                }
            case 12:
                if (contentValues != null) {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_VOTE_DRAFT, "", contentValues);
                    break;
                } else {
                    insert = writableDatabase.insert(SmsSqliteHelper.TABLE_VOTE_DRAFT, null, contentValues);
                    break;
                }
            default:
                insert = 0;
                break;
        }
        if (insert > 0) {
            return Uri.parse("content://vc.lx.richtext.votes/#" + insert);
        }
        return null;
    }

    public boolean onCreate() {
        this.mDbHelper = new SmsSqliteHelper(getContext());
        return this.mDbHelper != null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        Cursor query;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        switch (matcher.match(uri)) {
            case 1:
                sQLiteQueryBuilder.setTables(SmsSqliteHelper.TABLE_VOTE_QUESTIONS);
                query = sQLiteQueryBuilder.query(this.mDbHelper.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
                break;
            case 3:
                sQLiteQueryBuilder.setTables(SmsSqliteHelper.TABLE_VOTE_QUESTIONS);
                query = sQLiteQueryBuilder.query(this.mDbHelper.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
                break;
            case 4:
                sQLiteQueryBuilder.setTables(SmsSqliteHelper.TABLE_VOTE_OPTIONS);
                query = sQLiteQueryBuilder.query(this.mDbHelper.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
                break;
            case 5:
                sQLiteQueryBuilder.setTables("vote_questions LEFT OUTER JOIN vote_options ON (vote_questions._id = vote_options.vote_id)");
                query = sQLiteQueryBuilder.query(this.mDbHelper.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
                break;
            case 12:
                sQLiteQueryBuilder.setTables(SmsSqliteHelper.TABLE_VOTE_DRAFT);
                query = sQLiteQueryBuilder.query(this.mDbHelper.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
                break;
            default:
                query = null;
                break;
        }
        if (query != null) {
            query.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, contentValues, str, strArr);
            case 3:
                return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, contentValues, str, strArr);
            case 4:
                return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_OPTIONS, contentValues, str, strArr);
            case 5:
                return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, contentValues, " service_id = ? ", new String[]{String.valueOf(ContentUris.parseId(uri))});
            case 6:
                long parseId = ContentUris.parseId(uri);
                if (parseId >= 0) {
                    return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, contentValues, " _id = ? ", new String[]{String.valueOf(parseId)});
                }
                break;
            case 7:
                long parseId2 = ContentUris.parseId(uri);
                if (parseId2 >= 0) {
                    return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_OPTIONS, contentValues, " _id = ? ", new String[]{String.valueOf(parseId2)});
                }
                break;
            case 8:
                return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_OPTIONS, contentValues, " service_id = ? ", new String[]{String.valueOf(ContentUris.parseId(uri))});
            case 9:
                long parseId3 = ContentUris.parseId(uri);
                if (parseId3 >= 0) {
                    return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, contentValues, " service_id = ? ", new String[]{String.valueOf(parseId3)});
                }
                break;
            case 10:
                long parseId4 = ContentUris.parseId(uri);
                if (parseId4 >= 0) {
                    return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_OPTIONS, contentValues, " service_id = ? ", new String[]{String.valueOf(parseId4)});
                }
                break;
            case 11:
                String str2 = "http://lexin.cc/v/" + uri.getFragment();
                if (str2 != null && !str2.equals("")) {
                    return writableDatabase.update(SmsSqliteHelper.TABLE_VOTE_QUESTIONS, contentValues, " plug = ? ", new String[]{str2});
                }
        }
        return 0;
    }
}
