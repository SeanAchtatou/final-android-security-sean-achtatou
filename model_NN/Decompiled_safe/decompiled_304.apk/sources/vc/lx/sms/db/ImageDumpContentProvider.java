package vc.lx.sms.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ImageDumpContentProvider extends ContentProvider {
    public static final int ALL_IMAGE_DUMPS = 1;
    public static final String AUTHORITY = "vc.lx.sms.imagedumps";
    public static final Uri CONTENT_URI = Uri.parse("content://vc.lx.sms.imagedumps");
    public static final int SINGLE_IMAGE_DUMPS = 2;
    public static final String TEMPLATES_ITEM_TYPE = "vnd.android.cursor.item/imagedump";
    public static final String TEMPLATES_TYPE = "vnd.android.cursor.dir/imagedump";
    static final UriMatcher matcher = new UriMatcher(-1);
    private SmsSqliteHelper mDbHelper;

    static {
        matcher.addURI(AUTHORITY, null, 1);
        matcher.addURI(AUTHORITY, "/#", 2);
    }

    public int delete(Uri uri, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                return writableDatabase.delete(SmsSqliteHelper.TABLE_IMAGE_DUMP, str, strArr);
            default:
                return 0;
        }
    }

    public String getType(Uri uri) {
        switch (matcher.match(uri)) {
            case 1:
                return TEMPLATES_TYPE;
            case 2:
                return TEMPLATES_ITEM_TYPE;
            default:
                return null;
        }
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        long insert;
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                insert = writableDatabase.insert(SmsSqliteHelper.TABLE_IMAGE_DUMP, "", contentValues);
                break;
            default:
                insert = 0;
                break;
        }
        if (insert > 0) {
            return Uri.parse("content://vc.lx.sms.imagedumps/" + insert);
        }
        return null;
    }

    public boolean onCreate() {
        this.mDbHelper = new SmsSqliteHelper(getContext());
        return this.mDbHelper != null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        Cursor query;
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        sQLiteQueryBuilder.setTables(SmsSqliteHelper.TABLE_IMAGE_DUMP);
        switch (matcher.match(uri)) {
            case 1:
                query = sQLiteQueryBuilder.query(this.mDbHelper.getReadableDatabase(), strArr, str, strArr2, null, null, str2);
                break;
            default:
                query = null;
                break;
        }
        if (query != null) {
            query.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return query;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        SQLiteDatabase writableDatabase = this.mDbHelper.getWritableDatabase();
        switch (matcher.match(uri)) {
            case 1:
                return writableDatabase.update(SmsSqliteHelper.TABLE_IMAGE_DUMP, contentValues, str, strArr);
            case 2:
                return writableDatabase.update(SmsSqliteHelper.TABLE_IMAGE_DUMP, contentValues, " _id = " + ContentUris.parseId(uri), strArr);
            default:
                return 0;
        }
    }
}
