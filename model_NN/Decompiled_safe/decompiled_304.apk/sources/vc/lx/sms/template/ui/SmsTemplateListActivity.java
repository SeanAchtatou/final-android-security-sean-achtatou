package vc.lx.sms.template.ui;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.Group;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.RichTemplateRemoteVersion;
import vc.lx.sms.cmcc.http.data.SmsTemplate;
import vc.lx.sms.cmcc.http.parser.RichTemplateRemoteVersionParser;
import vc.lx.sms.cmcc.http.parser.SmsTemplateListParser;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.SmsTemplateContentProvider;
import vc.lx.sms.db.SmsTemplateDbService;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.ui.AbstractFlurryActivity;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class SmsTemplateListActivity extends AbstractFlurryActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public TemplageListAdapter mAdapter;
    /* access modifiers changed from: private */
    public String mCategoryId;
    /* access modifiers changed from: private */
    public int mCurrentPage;
    private FetchSmsTemplateVersionTask mFetchSmsTemplateVersionTask;
    protected LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public int mLastRemoteTemplateVersion = 0;
    private ListView mListView;
    private AdapterView.OnItemClickListener mListViewOnItemClickListener = new AdapterView.OnItemClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (i >= SmsTemplateListActivity.this.mSmsTemplages.size()) {
                return;
            }
            if ("square".equals(SmsTemplateListActivity.this.mfrom)) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setComponent(new ComponentName(SmsTemplateListActivity.this.getApplicationContext(), ComposeMessageActivity.class));
                intent.putExtra("forwarded_message", true);
                intent.putExtra("sms_body", ((SmsTemplate) SmsTemplateListActivity.this.mSmsTemplages.get(i)).mContent);
                intent.putExtra("app_type", "sms_template");
                intent.setClassName(SmsTemplateListActivity.this, "com.android.mms.ui.ForwardMessageActivity");
                SmsTemplateListActivity.this.startActivity(intent);
                return;
            }
            Intent intent2 = new Intent();
            intent2.putExtra(PrefsUtil.KEY_TEMPLATE_RETURNED, ((SmsTemplate) SmsTemplateListActivity.this.mSmsTemplages.get(i)).mContent);
            SmsTemplateListActivity.this.setResult(-1, intent2);
            SmsTemplateListActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public View mLoadingProgress;
    /* access modifiers changed from: private */
    public LocalTemplateListTask mLocalTemplateListTask;
    /* access modifiers changed from: private */
    public ImageView mRefreshBtn;
    /* access modifiers changed from: private */
    public int mRemoteCurrentPage;
    /* access modifiers changed from: private */
    public RemoteTemplateListTask mRemoteTemplateListTask;
    /* access modifiers changed from: private */
    public int mRemoteTotalPages = 99999;
    /* access modifiers changed from: private */
    public SmsTemplateDbService mSmsTemplagDbService;
    /* access modifiers changed from: private */
    public Group<SmsTemplate> mSmsTemplages;
    /* access modifiers changed from: private */
    public String mfrom;

    class FetchSmsTemplateVersionTask extends AbstractAsyncTask {
        public FetchSmsTemplateVersionTask(Context context) {
            super(context);
        }

        public HttpResponse getRespFromServer() throws IOException {
            String str;
            Cursor query = SmsTemplateListActivity.this.getContentResolver().query(SmsTemplateContentProvider.CONTENT_URI, null, "category_id = ?", new String[]{SmsTemplateListActivity.this.mCategoryId}, "_id desc");
            try {
                String valueOf = query.moveToFirst() ? String.valueOf(query.getInt(6)) : "0";
                if (query != null) {
                    query.close();
                    str = valueOf;
                } else {
                    str = valueOf;
                }
            } catch (Exception e) {
                if (query != null) {
                    query.close();
                    str = "0";
                } else {
                    str = "0";
                }
            } catch (Throwable th) {
                if (query != null) {
                    query.close();
                }
                throw th;
            }
            return this.mCmccHttpApi.fetchSmsTemplateVersion(this.mEnginehost, SmsTemplateListActivity.this.getApplicationContext(), SmsTemplateListActivity.this.mCategoryId, str);
        }

        public String getTag() {
            return "FetchSmsTemplateVersionTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof RichTemplateRemoteVersion) && "true".equals(((RichTemplateRemoteVersion) miguType).result)) {
                SmsTemplateListActivity.this.mRefreshBtn.setVisibility(0);
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new RichTemplateRemoteVersionParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class LocalTemplateListTask extends AsyncTask<Void, Void, Void> {
        private int _page;

        public LocalTemplateListTask(int i) {
            this._page = i;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voidArr) {
            Group<SmsTemplate> querySmsTemplates = SmsTemplateListActivity.this.mSmsTemplagDbService.querySmsTemplates(this._page, SmsTemplateListActivity.this.mCategoryId);
            int size = querySmsTemplates.size();
            if (size > 0) {
                int unused = SmsTemplateListActivity.this.mRemoteCurrentPage = ((SmsTemplate) querySmsTemplates.get(size - 1)).mPage;
                int unused2 = SmsTemplateListActivity.this.mLastRemoteTemplateVersion = ((SmsTemplate) querySmsTemplates.get(size - 1)).mVersion;
            }
            SmsTemplateListActivity.this.mSmsTemplages.addAll(querySmsTemplates);
            SmsTemplateListActivity.this.mSmsTemplages.page = querySmsTemplates.page;
            SmsTemplateListActivity.this.mSmsTemplages.totalCount = querySmsTemplates.totalCount;
            SmsTemplateListActivity.this.mSmsTemplages.totalPages = querySmsTemplates.totalPages;
            if (querySmsTemplates.page == null) {
                return null;
            }
            int unused3 = SmsTemplateListActivity.this.mCurrentPage = Integer.valueOf(querySmsTemplates.page).intValue();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void voidR) {
            SmsTemplateListActivity.this.mAdapter.notifyDataSetChanged();
            SmsTemplateListActivity.this.mLoadingProgress.setVisibility(8);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            SmsTemplateListActivity.this.mLoadingProgress.setVisibility(0);
        }
    }

    class RemoteTemplateListTask extends AbstractAsyncTask {
        private int _page;

        public RemoteTemplateListTask(Context context, int i) {
            super(context);
            this._page = i;
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.remoteSmsTemplist(this.mEnginehost, this.mContext, this._page, SmsTemplateListActivity.this.mCategoryId, SmsTemplateListActivity.this.mLastRemoteTemplateVersion);
        }

        public String getTag() {
            return "RemoteTemplateListTask";
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
          ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
        public void onPostLogic(MiguType miguType) {
            SmsTemplateListActivity.this.mLoadingProgress.setVisibility(8);
            SmsTemplateListActivity.this.mRefreshBtn.setVisibility(8);
            if (miguType != null && (miguType instanceof Group)) {
                try {
                    Group group = (Group) miguType;
                    int intValue = Integer.valueOf(group.version).intValue();
                    if (intValue > SmsTemplateListActivity.this.mLastRemoteTemplateVersion) {
                        SmsTemplateListActivity.this.getContentResolver().delete(SmsTemplateContentProvider.CONTENT_URI, "category_id = ?", new String[]{SmsTemplateListActivity.this.mCategoryId});
                        int unused = SmsTemplateListActivity.this.mLastRemoteTemplateVersion = intValue;
                        int unused2 = SmsTemplateListActivity.this.mRemoteCurrentPage = Integer.valueOf(group.page).intValue();
                        SmsTemplateListActivity.this.mSmsTemplages.clear();
                    }
                    Group group2 = new Group();
                    Iterator it = group.iterator();
                    while (it.hasNext()) {
                        SmsTemplate smsTemplate = (SmsTemplate) it.next();
                        Cursor query = SmsTemplateListActivity.this.getContentResolver().query(SmsTemplateContentProvider.CONTENT_URI, new String[]{SmsSqliteHelper.TEMPLATE_ID}, "template_id = ? and category_id = ?", new String[]{smsTemplate.mId, SmsTemplateListActivity.this.mCategoryId}, null);
                        if (query.getCount() > 0) {
                            query.close();
                        } else {
                            query.close();
                            group2.add(smsTemplate);
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(SmsSqliteHelper.CATEGORY_ID, SmsTemplateListActivity.this.mCategoryId);
                            contentValues.put(SmsSqliteHelper.CONTENT, smsTemplate.mContent);
                            contentValues.put(SmsSqliteHelper.TEMPLATE_ID, Integer.valueOf(smsTemplate.mId));
                            contentValues.put(SmsSqliteHelper.PRIORITY, (Integer) 1);
                            contentValues.put("page", Integer.valueOf(smsTemplate.mPage));
                            contentValues.put("version", Integer.valueOf(smsTemplate.mVersion));
                            SmsTemplateListActivity.this.getContentResolver().insert(SmsTemplateContentProvider.CONTENT_URI, contentValues);
                        }
                    }
                    SmsTemplateListActivity.this.mSmsTemplages.addAll(group2);
                    SmsTemplateListActivity.access$412(SmsTemplateListActivity.this, 1);
                    if (group.totalPages != null) {
                        int unused3 = SmsTemplateListActivity.this.mRemoteTotalPages = Integer.valueOf(group.totalPages).intValue();
                    } else {
                        int unused4 = SmsTemplateListActivity.this.mRemoteTotalPages = -1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            SmsTemplateListActivity.this.mAdapter.notifyDataSetChanged();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            SmsTemplateListActivity.this.mLoadingProgress.setVisibility(0);
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new SmsTemplateListParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class SmsTemplateItem {
        TextView content;
        TextView loading;

        SmsTemplateItem() {
        }
    }

    class TemplageListAdapter extends BaseAdapter {
        TemplageListAdapter() {
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public int getCount() {
            return SmsTemplateListActivity.this.mSmsTemplages.size();
        }

        public Object getItem(int i) {
            return SmsTemplateListActivity.this.mSmsTemplages.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public int getItemViewType(int i) {
            return i == SmsTemplateListActivity.this.mSmsTemplages.size() ? -1 : 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            SmsTemplateItem smsTemplateItem;
            View view2;
            if (view == null) {
                SmsTemplateItem smsTemplateItem2 = new SmsTemplateItem();
                View inflate = SmsTemplateListActivity.this.mInflater.inflate((int) R.layout.sms_template_item, viewGroup, false);
                smsTemplateItem2.content = (TextView) inflate.findViewById(R.id.content);
                smsTemplateItem2.loading = (TextView) inflate.findViewById(R.id.loading_text);
                inflate.setTag(smsTemplateItem2);
                smsTemplateItem = smsTemplateItem2;
                view2 = inflate;
            } else {
                smsTemplateItem = (SmsTemplateItem) view.getTag();
                view2 = view;
            }
            if (i < SmsTemplateListActivity.this.mSmsTemplages.size() - 1) {
                smsTemplateItem.content.setVisibility(0);
                smsTemplateItem.loading.setVisibility(8);
                smsTemplateItem.content.setText(((SmsTemplate) getItem(i)).mContent);
            } else if (SmsTemplateListActivity.this.mSmsTemplages.totalPages == null || SmsTemplateListActivity.this.mCurrentPage < Integer.valueOf(SmsTemplateListActivity.this.mSmsTemplages.totalPages).intValue()) {
                if (SmsTemplateListActivity.this.mLocalTemplateListTask != null) {
                    SmsTemplateListActivity.this.mLocalTemplateListTask.cancel(true);
                }
                LocalTemplateListTask unused = SmsTemplateListActivity.this.mLocalTemplateListTask = new LocalTemplateListTask(SmsTemplateListActivity.this.mCurrentPage + 1);
                SmsTemplateListActivity.this.mLocalTemplateListTask.execute(new Void[0]);
            } else if (SmsTemplateListActivity.this.mRemoteCurrentPage < SmsTemplateListActivity.this.mRemoteTotalPages) {
                if (SmsTemplateListActivity.this.mRemoteTemplateListTask != null) {
                    SmsTemplateListActivity.this.mRemoteTemplateListTask.cancel(true);
                }
                RemoteTemplateListTask unused2 = SmsTemplateListActivity.this.mRemoteTemplateListTask = new RemoteTemplateListTask(SmsTemplateListActivity.this.getApplicationContext(), SmsTemplateListActivity.this.mRemoteCurrentPage + 1);
                SmsTemplateListActivity.this.mRemoteTemplateListTask.execute(new Void[0]);
            }
            return view2;
        }

        public int getViewTypeCount() {
            return 2;
        }
    }

    static /* synthetic */ int access$412(SmsTemplateListActivity smsTemplateListActivity, int i) {
        int i2 = smsTemplateListActivity.mRemoteCurrentPage + i;
        smsTemplateListActivity.mRemoteCurrentPage = i2;
        return i2;
    }

    public void onClick(View view) {
        if (view == this.mRefreshBtn) {
            if (this.mRemoteTemplateListTask != null) {
                this.mRemoteTemplateListTask.cancel(true);
            }
            this.mRemoteTemplateListTask = new RemoteTemplateListTask(getApplicationContext(), 1);
            this.mRemoteTemplateListTask.execute(new Void[0]);
            this.mRefreshBtn.setEnabled(false);
            Util.logEvent("sms_template_refresh_btn_click", new NameValuePair[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.sms_template_list);
        this.mInflater = LayoutInflater.from(this);
        ((TextView) findViewById(R.id.category_name)).setText(getIntent().getStringExtra(PrefsUtil.KEY_TEMPLATE_CATEGORY_NAME));
        this.mLoadingProgress = findViewById(R.id.load_progress);
        int intExtra = getIntent().getIntExtra(PrefsUtil.KEY_TEMPLATE_CATEGORY_ID, 0);
        this.mfrom = getIntent().getStringExtra(PrefsUtil.KEY_TEMPLATE_FROM);
        switch (intExtra) {
            case 0:
                this.mCategoryId = "SMSZF";
                break;
            case 1:
                this.mCategoryId = "SMSJR";
                break;
            case 2:
                this.mCategoryId = "SMSYY";
                break;
            case 3:
                this.mCategoryId = "SMSCY";
                break;
        }
        this.mSmsTemplagDbService = new SmsTemplateDbService(getApplicationContext());
        this.mCurrentPage = 1;
        this.mSmsTemplages = new Group<>(Collections.synchronizedList(new ArrayList()));
        this.mListView = (ListView) findViewById(16908298);
        this.mAdapter = new TemplageListAdapter();
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        this.mListView.setOnItemClickListener(this.mListViewOnItemClickListener);
        this.mRefreshBtn = (ImageView) findViewById(R.id.refresh);
        this.mRefreshBtn.setOnClickListener(this);
        this.mLocalTemplateListTask = new LocalTemplateListTask(this.mCurrentPage);
        this.mLocalTemplateListTask.execute(new Void[0]);
        this.mFetchSmsTemplateVersionTask = new FetchSmsTemplateVersionTask(getApplicationContext());
        this.mFetchSmsTemplateVersionTask.execute(new Void[0]);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SmsTemplateListActivity.this.setResult(0);
                SmsTemplateListActivity.this.finish();
            }
        });
        Util.logEvent("sms_template_app_open", new NameValuePair[0]);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (isFinishing()) {
            if (this.mLocalTemplateListTask != null) {
                this.mLocalTemplateListTask.cancel(true);
            }
            if (this.mRemoteTemplateListTask != null) {
                this.mRemoteTemplateListTask.cancel(true);
            }
            if (this.mFetchSmsTemplateVersionTask != null) {
                this.mFetchSmsTemplateVersionTask.cancel(true);
            }
        }
    }
}
