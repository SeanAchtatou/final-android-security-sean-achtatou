package junit.test;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.AndroidTestCase;
import android.util.Log;
import vc.lx.sms.db.MusicContentProvider;
import vc.lx.sms.db.SmsSqliteHelper;

public class TestMusic extends AndroidTestCase {
    private static final String TAG = "TestMusic";

    public void deleteVote() {
        Log.i(TAG, String.valueOf(getContext().getContentResolver().delete(MusicContentProvider.CONTENT_URI, null, null)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testMusicInsert() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SmsSqliteHelper.CONTENT_ID, (Integer) 100);
        contentValues.put("name", "������������");
        contentValues.put(SmsSqliteHelper.GROUP_CODE, (Integer) 4);
        contentValues.put("singer", "������");
        contentValues.put(SmsSqliteHelper.PLUG, "erwegf");
        contentValues.put("transmit_counter", "10");
        getContext().getContentResolver().notifyChange(MusicContentProvider.CONTENT_URI, null);
        getContext().getContentResolver().insert(MusicContentProvider.CONTENT_URI, contentValues);
    }

    public void testQuery() {
        Cursor query = getContext().getContentResolver().query(MusicContentProvider.CONTENT_URI, null, null, null, null);
        while (query.moveToNext()) {
            Log.i(TAG, query.getString(query.getColumnIndex("_id")) + "," + query.getString(query.getColumnIndex("name")) + " , " + query.getString(query.getColumnIndex(SmsSqliteHelper.CONTENT_ID)) + " , " + query.getInt(query.getColumnIndex(SmsSqliteHelper.GROUP_CODE)) + "," + query.getString(query.getColumnIndex(SmsSqliteHelper.PLUG)) + "," + query.getString(query.getColumnIndex("transmit_counter")) + ",");
        }
    }

    public void testQueryOption() {
        Uri parse = Uri.parse("content://vc.lx.richtext.votes/vote_options/");
        Cursor query = getContext().getContentResolver().query(parse, null, " vote_id = ? ", new String[]{String.valueOf("1")}, null);
        while (query.moveToNext()) {
            Log.i(TAG, query.getString(query.getColumnIndex("_id")) + "," + query.getString(query.getColumnIndex("option_text")) + " , " + query.getString(query.getColumnIndex("vote_id")) + " , " + query.getString(query.getColumnIndex("contact_name")) + " , " + query.getString(query.getColumnIndex("display_order")) + " , " + query.getInt(query.getColumnIndex("counter")) + " , " + query.getString(query.getColumnIndex("service_id")));
        }
    }

    public void testUpdate() {
        Uri parse = Uri.parse("content://mobi.recloud.app/app");
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "������������");
        contentValues.put(SmsSqliteHelper.APP_visible, "yes");
        getContext().getContentResolver().notifyChange(parse, null);
        getContext().getContentResolver().update(parse, contentValues, " app_id = ? ", new String[]{"1"});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testVoteContactName() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("contact_name", "������");
        contentValues.put("counter", (Integer) 0);
        Log.i(TAG, String.valueOf(getContext().getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_options/109"), contentValues, null, null)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testVoteCountUpdate() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("is_new", (Integer) 1);
        Log.i(TAG, String.valueOf(getContext().getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_questions/47"), contentValues, null, null)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testVoteOptionCountUpdate() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("counter", (Integer) 10);
        Log.i(TAG, String.valueOf(getContext().getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_options/8"), contentValues, null, null)));
    }
}
