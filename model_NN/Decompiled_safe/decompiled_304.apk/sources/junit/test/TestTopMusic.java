package junit.test;

import android.test.AndroidTestCase;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.db.TopMusicService;

public class TestTopMusic extends AndroidTestCase {
    private final String TAG = "TestTopMusic";

    public void getSongItemsByReadStatus() {
        Log.i("TestTopMusic", String.valueOf(new TopMusicService(getContext()).getSongItemsByReadStatus("no").size()));
    }

    public void testAllQuery() {
        List<SongItem> queryAll = new TopMusicService(getContext()).queryAll();
        for (int i = 0; i < queryAll.size(); i++) {
            Log.i("TestTopMusic", queryAll.get(i).durl + "," + queryAll.get(i).tag + "," + queryAll.get(i).song + "," + queryAll.get(i).singer + queryAll.get(i).plug + "," + queryAll.get(i).type + "," + queryAll.get(i).master + ",");
        }
    }

    public void testDelAll() {
        new TopMusicService(getContext());
    }

    public void testInsert() {
        TopMusicService topMusicService = new TopMusicService(getContext());
        ArrayList arrayList = new ArrayList();
        SongItem songItem = new SongItem();
        songItem.contentid = "4111144";
        songItem.song = "song name";
        songItem.groupcode = "286515_372917";
        songItem.singer = "singer_name";
        songItem.plug = "S6MqD";
        arrayList.add(songItem);
        topMusicService.insert(arrayList);
    }

    public void testInsertSongMaster() {
        TopMusicService topMusicService = new TopMusicService(getContext());
        SongItem songItem = new SongItem();
        songItem.contentid = "4111144";
        songItem.song = "song name";
        songItem.groupcode = "286515_372917";
        songItem.singer = "singer_name";
        songItem.plug = "S6MqD";
        topMusicService.insert(songItem, 3);
    }

    public void testMasterQuery() {
        List<SongItem> songItemsByMaster = new TopMusicService(getContext()).getSongItemsByMaster(0, 1000);
        for (int i = 0; i < songItemsByMaster.size(); i++) {
            Log.i("TestTopMusic", songItemsByMaster.get(i).durl + "," + songItemsByMaster.get(i).groupcode + "," + songItemsByMaster.get(i).song + "," + songItemsByMaster.get(i).singer + songItemsByMaster.get(i).plug + "," + songItemsByMaster.get(i).readstatus + "," + songItemsByMaster.get(i).forward_count + ",");
        }
    }

    public void testQuery() {
        List<SongItem> queryAllNew = new TopMusicService(getContext()).queryAllNew(0, 20);
        for (int i = 0; i < queryAllNew.size(); i++) {
            Log.i("TestTopMusic", queryAllNew.get(i).durl + "," + queryAllNew.get(i).tag + "," + queryAllNew.get(i).song + "," + queryAllNew.get(i).singer + queryAllNew.get(i).plug + "," + queryAllNew.get(i).type + "," + queryAllNew.get(i).master + ",");
        }
    }

    public void testQueryByMasterThree() {
        List<SongItem> querySongItemByMasterThree = new TopMusicService(getContext()).querySongItemByMasterThree();
        for (int i = 0; i < querySongItemByMasterThree.size(); i++) {
            Log.i("TestTopMusic", querySongItemByMasterThree.get(i).durl + "," + querySongItemByMasterThree.get(i).tag + "," + querySongItemByMasterThree.get(i).song + "," + querySongItemByMasterThree.get(i).singer + querySongItemByMasterThree.get(i).plug + "," + querySongItemByMasterThree.get(i).type + "," + querySongItemByMasterThree.get(i).master + ",");
        }
    }

    public void testTotalCount() {
        Log.i("TestTopMusic", String.valueOf(new TopMusicService(getContext()).getTotalCount()));
    }

    public void updatePlug() {
        new TopMusicService(getContext()).updateMusicMaster("4v7rE");
    }
}
