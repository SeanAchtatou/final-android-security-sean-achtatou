package junit.test;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.AndroidTestCase;
import android.util.Log;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.db.VoteContentProvider;

public class TestVotes extends AndroidTestCase {
    private static final String TAG = "TestVote";

    public void deleteVote() {
        Log.i(TAG, String.valueOf(getContext().getContentResolver().delete(Uri.parse("content://vc.lx.richtext.votes/vote_options/3"), null, null)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testDraftInsert() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("vote_id", "1");
        contentValues.put("sms_content", "������������������");
        contentValues.put("thread_id", (Integer) 9);
        contentValues.put("numbers", "1344345443,1882259433");
        getContext().getContentResolver().notifyChange(VoteContentProvider.VOTEDRAFT_CONTENT_URI, null);
        getContext().getContentResolver().insert(VoteContentProvider.VOTEDRAFT_CONTENT_URI, contentValues);
    }

    public void testDraftQuery() {
        Cursor query = getContext().getContentResolver().query(VoteContentProvider.VOTEDRAFT_CONTENT_URI, null, null, null, null);
        while (query.moveToNext()) {
            Log.i(TAG, query.getString(query.getColumnIndex("_id")) + "," + query.getInt(query.getColumnIndex("sms_content")) + "," + query.getString(query.getColumnIndex("thread_id")) + "," + query.getString(query.getColumnIndex("numbers")) + ",");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testOptionInsert() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("vote_id", "1");
        contentValues.put("option_text", "������������������");
        contentValues.put("service_id", (Integer) 9);
        contentValues.put("display_order", (Integer) 6);
        contentValues.put("contact_name", "���������23");
        getContext().getContentResolver().notifyChange(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null);
        getContext().getContentResolver().insert(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, contentValues);
    }

    public void testQuery() {
        Cursor query = getContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, null, null, null);
        while (query.moveToNext()) {
            Log.i(TAG, query.getString(query.getColumnIndex("_id")) + "," + query.getString(query.getColumnIndex("title")) + " , " + query.getString(query.getColumnIndex("service_id")) + " , " + query.getInt(query.getColumnIndex("counter")) + "," + query.getInt(query.getColumnIndex("is_new")) + "," + query.getString(query.getColumnIndex("contact_name")) + "," + query.getString(query.getColumnIndex("tracer_flag")) + "," + query.getString(query.getColumnIndex("parent_id")) + "," + query.getString(query.getColumnIndex(SmsSqliteHelper.PLUG)) + ",");
        }
    }

    public void testQueryOption() {
        Uri parse = Uri.parse("content://vc.lx.richtext.votes/vote_options/");
        Cursor query = getContext().getContentResolver().query(parse, null, " vote_id = ? ", new String[]{String.valueOf("4")}, null);
        while (query.moveToNext()) {
            Log.i(TAG, query.getString(query.getColumnIndex("_id")) + "," + query.getString(query.getColumnIndex("option_text")) + " , " + query.getString(query.getColumnIndex("vote_id")) + " , " + query.getString(query.getColumnIndex("service_id")) + " , " + query.getString(query.getColumnIndex("contact_name")) + " , " + query.getString(query.getColumnIndex("display_order")) + " , " + query.getInt(query.getColumnIndex("counter")) + " , " + query.getString(query.getColumnIndex("service_id")));
        }
    }

    public void testQueryVoteByClientId() {
        Cursor query = getContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " _id = ? ", new String[]{"2"}, null);
        while (query.moveToNext()) {
            Log.i(TAG, query.getString(query.getColumnIndex("_id")) + "," + query.getString(query.getColumnIndex("title")) + " , " + query.getString(query.getColumnIndex("service_id")) + " , " + query.getInt(query.getColumnIndex("counter")) + "," + query.getInt(query.getColumnIndex("is_new")) + "," + query.getString(query.getColumnIndex("contact_name")) + "," + query.getString(query.getColumnIndex("tracer_flag")) + "," + query.getString(query.getColumnIndex("parent_id")) + "," + query.getString(query.getColumnIndex(SmsSqliteHelper.PLUG)) + ",");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testQuestionInsert() {
        Uri.parse("content://mobi.recloud.richtext.votes/vote");
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", "������������������������������");
        contentValues.put("service_id", (Integer) 4);
        contentValues.put("counter", (Integer) 13);
        contentValues.put(SmsSqliteHelper.PLUG, "sdcece");
        contentValues.put("contact_name", "������,lzhu");
        getContext().getContentResolver().notifyChange(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null);
        getContext().getContentResolver().insert(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, contentValues);
    }

    public void testUpdate() {
        Uri parse = Uri.parse("content://mobi.recloud.app/app");
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "������������");
        contentValues.put(SmsSqliteHelper.APP_visible, "yes");
        getContext().getContentResolver().notifyChange(parse, null);
        getContext().getContentResolver().update(parse, contentValues, " app_id = ? ", new String[]{"1"});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testVoteContactName() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("contact_name", "������,������");
        contentValues.put("counter", (Integer) 0);
        Log.i(TAG, String.valueOf(getContext().getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_options/109"), contentValues, null, null)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testVoteCountUpdate() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("is_new", (Integer) 1);
        Log.i(TAG, String.valueOf(getContext().getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_questions/47"), contentValues, null, null)));
    }

    public void testVoteOptionCountUpdate() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("contact_name", "������,������");
        Log.i(TAG, String.valueOf(getContext().getContentResolver().update(Uri.parse("content://vc.lx.richtext.votes/vote_questions/119"), contentValues, null, null)));
    }

    public void testVoteOptionCountUpdate1() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("service_id", "222");
        Log.i(TAG, String.valueOf(getContext().getContentResolver().update(Uri.parse(VoteContentProvider.VOTEOPTIONS_CONTENT_URI + "/cli_id/" + 32), contentValues, null, null)));
    }
}
