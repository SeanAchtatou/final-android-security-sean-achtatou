package junit.test;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.test.AndroidTestCase;
import android.util.Log;
import vc.lx.sms.db.SmsSqliteHelper;

public class TestApp extends AndroidTestCase {
    private static final String TAG = "TestApp";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void testApp() {
        Uri parse = Uri.parse("content://mobi.recloud.app/app");
        ContentValues contentValues = new ContentValues();
        contentValues.put(SmsSqliteHelper.APP_id, (Integer) 1);
        contentValues.put("name", "������");
        contentValues.put(SmsSqliteHelper.APP_heat, (Integer) 3);
        contentValues.put(SmsSqliteHelper.APP_visible, "yes");
        getContext().getContentResolver().notifyChange(parse, null);
        getContext().getContentResolver().insert(parse, contentValues);
    }

    public void testQuery() {
        Uri parse = Uri.parse("content://mobi.recloud.app/app");
        Cursor query = getContext().getContentResolver().query(parse, new String[]{SmsSqliteHelper.APP_id, "name", SmsSqliteHelper.APP_visible}, null, null, null);
        while (query.moveToNext()) {
            Log.i(TAG, query.getString(query.getColumnIndex(SmsSqliteHelper.APP_id)) + "," + query.getString(query.getColumnIndex("name")) + " , " + query.getString(query.getColumnIndex(SmsSqliteHelper.APP_heat)) + " , " + query.getString(query.getColumnIndex(SmsSqliteHelper.APP_visible)));
        }
    }

    public void testUpdate() {
        Uri parse = Uri.parse("content://mobi.recloud.app/app");
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "������������");
        contentValues.put(SmsSqliteHelper.APP_visible, "yes");
        getContext().getContentResolver().notifyChange(parse, null);
        getContext().getContentResolver().update(parse, contentValues, " app_id = ? ", new String[]{"1"});
    }
}
