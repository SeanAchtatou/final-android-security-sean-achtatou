package com.android.media;

import android.graphics.Bitmap;
import java.io.FileDescriptor;

public class MediaMetadataRetriever {
    public static final int METADATA_KEY_ALBUM = 1;
    public static final int METADATA_KEY_ARTIST = 2;
    public static final int METADATA_KEY_AUTHOR = 3;
    public static final int METADATA_KEY_BIT_RATE = 16;
    public static final int METADATA_KEY_CD_TRACK_NUMBER = 0;
    public static final int METADATA_KEY_CODEC = 12;
    public static final int METADATA_KEY_COMMENT = 14;
    public static final int METADATA_KEY_COMPOSER = 4;
    public static final int METADATA_KEY_COPYRIGHT = 15;
    public static final int METADATA_KEY_DATE = 5;
    public static final int METADATA_KEY_DURATION = 9;
    public static final int METADATA_KEY_FRAME_RATE = 17;
    public static final int METADATA_KEY_GENRE = 6;
    public static final int METADATA_KEY_IS_DRM_CRIPPLED = 11;
    public static final int METADATA_KEY_NUM_TRACKS = 10;
    public static final int METADATA_KEY_RATING = 13;
    public static final int METADATA_KEY_TITLE = 7;
    public static final int METADATA_KEY_VIDEO_FORMAT = 18;
    public static final int METADATA_KEY_VIDEO_HEIGHT = 19;
    public static final int METADATA_KEY_VIDEO_WIDTH = 20;
    public static final int METADATA_KEY_WRITER = 21;
    public static final int METADATA_KEY_YEAR = 8;
    public static final int MODE_CAPTURE_FRAME_ONLY = 2;
    public static final int MODE_GET_METADATA_ONLY = 1;
    private int mNativeContext;

    static {
        System.loadLibrary("media_jni");
        native_init();
    }

    public MediaMetadataRetriever() {
        native_setup();
    }

    private final native void native_finalize();

    private static native void native_init();

    private native void native_setup();

    public native Bitmap captureFrame();

    public native byte[] extractAlbumArt();

    public native String extractMetadata(int i);

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            native_finalize();
        } finally {
            super.finalize();
        }
    }

    public native int getMode();

    public native void release();

    /* JADX WARNING: Removed duplicated region for block: B:37:0x005d A[SYNTHETIC, Splitter:B:37:0x005d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setDataSource(android.content.Context r9, android.net.Uri r10) throws java.lang.IllegalArgumentException, java.lang.SecurityException {
        /*
            r8 = this;
            if (r10 != 0) goto L_0x0008
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x0008:
            java.lang.String r0 = r10.getScheme()
            if (r0 == 0) goto L_0x0016
            java.lang.String r1 = "file"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x001e
        L_0x0016:
            java.lang.String r0 = r10.getPath()
            r8.setDataSource(r0)
        L_0x001d:
            return
        L_0x001e:
            r0 = 0
            android.content.ContentResolver r1 = r9.getContentResolver()     // Catch:{ SecurityException -> 0x0047, all -> 0x0087 }
            java.lang.String r2 = "r"
            android.content.res.AssetFileDescriptor r6 = r1.openAssetFileDescriptor(r10, r2)     // Catch:{ FileNotFoundException -> 0x0040 }
            if (r6 != 0) goto L_0x0049
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r0.<init>()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            throw r0     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
        L_0x0031:
            r0 = move-exception
            r0 = r6
        L_0x0033:
            if (r0 == 0) goto L_0x0038
            r0.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0038:
            java.lang.String r0 = r10.toString()
            r8.setDataSource(r0)
            goto L_0x001d
        L_0x0040:
            r1 = move-exception
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ SecurityException -> 0x0047, all -> 0x0087 }
            r1.<init>()     // Catch:{ SecurityException -> 0x0047, all -> 0x0087 }
            throw r1     // Catch:{ SecurityException -> 0x0047, all -> 0x0087 }
        L_0x0047:
            r1 = move-exception
            goto L_0x0033
        L_0x0049:
            java.io.FileDescriptor r1 = r6.getFileDescriptor()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            boolean r0 = r1.valid()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            if (r0 != 0) goto L_0x0061
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r0.<init>()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            throw r0     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
        L_0x0059:
            r0 = move-exception
            r1 = r6
        L_0x005b:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0060:
            throw r0
        L_0x0061:
            long r2 = r6.getDeclaredLength()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0076
            r8.setDataSource(r1)     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
        L_0x006e:
            if (r6 == 0) goto L_0x001d
            r6.close()     // Catch:{ IOException -> 0x0074 }
            goto L_0x001d
        L_0x0074:
            r0 = move-exception
            goto L_0x001d
        L_0x0076:
            long r2 = r6.getStartOffset()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            long r4 = r6.getDeclaredLength()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r0 = r8
            r0.setDataSource(r1, r2, r4)     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            goto L_0x006e
        L_0x0083:
            r0 = move-exception
            goto L_0x0038
        L_0x0085:
            r1 = move-exception
            goto L_0x0060
        L_0x0087:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.media.MediaMetadataRetriever.setDataSource(android.content.Context, android.net.Uri):void");
    }

    public void setDataSource(FileDescriptor fileDescriptor) throws IllegalArgumentException {
        setDataSource(fileDescriptor, 0, 576460752303423487L);
    }

    public native void setDataSource(FileDescriptor fileDescriptor, long j, long j2) throws IllegalArgumentException;

    public native void setDataSource(String str) throws IllegalArgumentException;

    public native void setMode(int i);
}
