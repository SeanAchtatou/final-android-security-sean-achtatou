package com.android.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.util.HashMap;

public class NetworkConnectivityListener {
    private static final boolean DBG = false;
    private static final String TAG = "NetworkConnectivityListener";
    private Context mContext;
    /* access modifiers changed from: private */
    public HashMap<Handler, Integer> mHandlers = new HashMap<>();
    /* access modifiers changed from: private */
    public boolean mIsFailover;
    /* access modifiers changed from: private */
    public boolean mListening;
    /* access modifiers changed from: private */
    public NetworkInfo mNetworkInfo;
    /* access modifiers changed from: private */
    public NetworkInfo mOtherNetworkInfo;
    /* access modifiers changed from: private */
    public String mReason;
    private ConnectivityBroadcastReceiver mReceiver = new ConnectivityBroadcastReceiver();
    /* access modifiers changed from: private */
    public State mState = State.UNKNOWN;

    private class ConnectivityBroadcastReceiver extends BroadcastReceiver {
        private ConnectivityBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (!intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION) || !NetworkConnectivityListener.this.mListening) {
                Log.w(NetworkConnectivityListener.TAG, "onReceived() called with " + NetworkConnectivityListener.this.mState.toString() + " and " + intent);
                return;
            }
            if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
                State unused = NetworkConnectivityListener.this.mState = State.NOT_CONNECTED;
            } else {
                State unused2 = NetworkConnectivityListener.this.mState = State.CONNECTED;
            }
            NetworkInfo unused3 = NetworkConnectivityListener.this.mNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo unused4 = NetworkConnectivityListener.this.mOtherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
            String unused5 = NetworkConnectivityListener.this.mReason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean unused6 = NetworkConnectivityListener.this.mIsFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);
            for (Handler handler : NetworkConnectivityListener.this.mHandlers.keySet()) {
                handler.sendMessage(Message.obtain(handler, ((Integer) NetworkConnectivityListener.this.mHandlers.get(handler)).intValue()));
            }
        }
    }

    public enum State {
        UNKNOWN,
        CONNECTED,
        NOT_CONNECTED
    }

    public NetworkInfo getNetworkInfo() {
        return this.mNetworkInfo;
    }

    public NetworkInfo getOtherNetworkInfo() {
        return this.mOtherNetworkInfo;
    }

    public String getReason() {
        return this.mReason;
    }

    public State getState() {
        return this.mState;
    }

    public boolean isFailover() {
        return this.mIsFailover;
    }

    public void registerHandler(Handler handler, int i) {
        this.mHandlers.put(handler, Integer.valueOf(i));
    }

    public synchronized void startListening(Context context) {
        if (!this.mListening) {
            this.mContext = context;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            context.registerReceiver(this.mReceiver, intentFilter);
            this.mListening = true;
        }
    }

    public synchronized void stopListening() {
        if (this.mListening) {
            this.mContext.unregisterReceiver(this.mReceiver);
            this.mContext = null;
            this.mNetworkInfo = null;
            this.mOtherNetworkInfo = null;
            this.mIsFailover = false;
            this.mReason = null;
            this.mListening = false;
        }
    }

    public void unregisterHandler(Handler handler) {
        this.mHandlers.remove(handler);
    }
}
