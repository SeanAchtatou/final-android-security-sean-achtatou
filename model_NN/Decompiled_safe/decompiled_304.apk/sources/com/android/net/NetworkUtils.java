package com.android.net;

import com.flurry.android.Constants;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class NetworkUtils {
    public static int lookupHost(String str) {
        try {
            byte[] address = InetAddress.getByName(str).getAddress();
            return (address[0] & Constants.UNKNOWN) | ((address[3] & Constants.UNKNOWN) << 24) | ((address[2] & Constants.UNKNOWN) << 16) | ((address[1] & Constants.UNKNOWN) << 8);
        } catch (UnknownHostException e) {
            return -1;
        }
    }
}
