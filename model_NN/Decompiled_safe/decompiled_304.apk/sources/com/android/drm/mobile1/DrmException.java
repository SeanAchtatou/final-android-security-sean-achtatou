package com.android.drm.mobile1;

public class DrmException extends Exception {
    private DrmException() {
    }

    public DrmException(String str) {
        super(str);
    }
}
