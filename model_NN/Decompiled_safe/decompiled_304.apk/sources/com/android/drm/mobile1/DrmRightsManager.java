package com.android.drm.mobile1;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DrmRightsManager {
    private static final int DRM_MIMETYPE_MESSAGE = 1;
    private static final int DRM_MIMETYPE_RIGHTS_WBXML = 4;
    public static final String DRM_MIMETYPE_RIGHTS_WBXML_STRING = "application/vnd.oma.drm.rights+wbxml";
    private static final int DRM_MIMETYPE_RIGHTS_XML = 3;
    public static final String DRM_MIMETYPE_RIGHTS_XML_STRING = "application/vnd.oma.drm.rights+xml";
    private static final int JNI_DRM_FAILURE = -1;
    private static final int JNI_DRM_SUCCESS = 0;
    private static DrmRightsManager singleton = null;

    static {
        try {
            System.loadLibrary("drm1_jni");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    protected DrmRightsManager() {
    }

    public static synchronized DrmRightsManager getInstance() {
        DrmRightsManager drmRightsManager;
        synchronized (DrmRightsManager.class) {
            if (singleton == null) {
                singleton = new DrmRightsManager();
            }
            drmRightsManager = singleton;
        }
        return drmRightsManager;
    }

    private native int nativeDeleteRights(DrmRights drmRights);

    private native int nativeGetNumOfRights();

    private native int nativeGetRightsList(DrmRights[] drmRightsArr, int i);

    private native int nativeInstallDrmRights(InputStream inputStream, int i, int i2, DrmRights drmRights);

    private native int nativeQueryRights(DrmRawContent drmRawContent, DrmRights drmRights);

    public synchronized void deleteRights(DrmRights drmRights) {
        if (-1 == nativeDeleteRights(drmRights)) {
        }
    }

    public synchronized List getRightsList() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        int nativeGetNumOfRights = nativeGetNumOfRights();
        if (-1 == nativeGetNumOfRights) {
            arrayList = null;
        } else if (nativeGetNumOfRights > 0) {
            DrmRights[] drmRightsArr = new DrmRights[nativeGetNumOfRights];
            for (int i = 0; i < nativeGetNumOfRights; i++) {
                drmRightsArr[i] = new DrmRights();
            }
            int nativeGetRightsList = nativeGetRightsList(drmRightsArr, nativeGetNumOfRights);
            if (-1 == nativeGetRightsList) {
                arrayList = null;
            } else {
                for (int i2 = 0; i2 < nativeGetRightsList; i2++) {
                    arrayList.add(drmRightsArr[i2]);
                }
            }
        }
        return arrayList;
    }

    public synchronized DrmRights installRights(InputStream inputStream, int i, String str) throws DrmException, IOException {
        int i2;
        DrmRights drmRights;
        if (DRM_MIMETYPE_RIGHTS_XML_STRING.equals(str)) {
            i2 = 3;
        } else if (DRM_MIMETYPE_RIGHTS_WBXML_STRING.equals(str)) {
            i2 = 4;
        } else if ("application/vnd.oma.drm.message".equals(str)) {
            i2 = 1;
        } else {
            throw new IllegalArgumentException("mimeType must be DRM_MIMETYPE_RIGHTS_XML or DRM_MIMETYPE_RIGHTS_WBXML or DRM_MIMETYPE_MESSAGE");
        }
        if (i <= 0) {
            drmRights = null;
        } else {
            DrmRights drmRights2 = new DrmRights();
            if (-1 == nativeInstallDrmRights(inputStream, i, i2, drmRights2)) {
                throw new DrmException("nativeInstallDrmRights() returned JNI_DRM_FAILURE");
            }
            drmRights = drmRights2;
        }
        return drmRights;
    }

    public synchronized DrmRights queryRights(DrmRawContent drmRawContent) {
        DrmRights drmRights;
        drmRights = new DrmRights();
        if (-1 == nativeQueryRights(drmRawContent, drmRights)) {
            drmRights = null;
        }
        return drmRights;
    }
}
