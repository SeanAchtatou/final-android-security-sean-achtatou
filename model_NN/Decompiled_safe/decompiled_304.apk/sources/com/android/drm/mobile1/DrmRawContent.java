package com.android.drm.mobile1;

import com.flurry.android.Constants;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import vc.lx.sms.cmcc.MMHttpDefines;

public class DrmRawContent {
    public static final int DRM_COMBINED_DELIVERY = 2;
    public static final int DRM_FORWARD_LOCK = 1;
    private static final int DRM_MIMETYPE_CONTENT = 2;
    public static final String DRM_MIMETYPE_CONTENT_STRING = "application/vnd.oma.drm.content";
    private static final int DRM_MIMETYPE_MESSAGE = 1;
    public static final String DRM_MIMETYPE_MESSAGE_STRING = "application/vnd.oma.drm.message";
    public static final int DRM_SEPARATE_DELIVERY = 3;
    public static final int DRM_SEPARATE_DELIVERY_DM = 4;
    public static final int DRM_UNKNOWN_DATA_LEN = -1;
    private static final int JNI_DRM_EOF = -2;
    private static final int JNI_DRM_FAILURE = -1;
    private static final int JNI_DRM_SUCCESS = 0;
    private static final int JNI_DRM_UNKNOWN_DATA_LEN = -3;
    private int id = -1;
    private BufferedInputStream inData;
    private int inDataLen;
    private String mediaType;
    private int rawType;
    private String rightsIssuer;

    class DrmInputStream extends InputStream {
        private byte[] b = new byte[1];
        private boolean isClosed = false;
        private int offset = 0;

        public DrmInputStream(DrmRights drmRights) {
        }

        public int available() throws IOException {
            int access$000 = DrmRawContent.this.nativeGetContentLength();
            if (-1 == access$000) {
                throw new IOException();
            } else if (-3 == access$000) {
                return 0;
            } else {
                int i = access$000 - this.offset;
                if (i >= 0) {
                    return i;
                }
                throw new IOException();
            }
        }

        public void close() {
            this.isClosed = true;
        }

        public void mark(int i) {
        }

        public boolean markSupported() {
            return false;
        }

        public int read() throws IOException {
            if (-1 == read(this.b, 0, 1)) {
                return -1;
            }
            return this.b[0] & Constants.UNKNOWN;
        }

        public int read(byte[] bArr) throws IOException {
            return read(bArr, 0, bArr.length);
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            if (bArr == null) {
                throw new NullPointerException();
            } else if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException();
            } else if (true == this.isClosed) {
                throw new IOException();
            } else if (i2 == 0) {
                return 0;
            } else {
                int access$100 = DrmRawContent.this.nativeReadContent(bArr, i, i2, this.offset);
                if (-1 == access$100) {
                    throw new IOException();
                } else if (-2 == access$100) {
                    return -1;
                } else {
                    this.offset += access$100;
                    return access$100;
                }
            }
        }

        public void reset() throws IOException {
            throw new IOException();
        }

        public long skip(long j) throws IOException {
            return 0;
        }
    }

    static {
        try {
            System.loadLibrary("drm1_jni");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("WARNING: Could not load libdrm1_jni.so");
        }
    }

    public DrmRawContent(InputStream inputStream, int i, String str) throws DrmException, IOException {
        int i2;
        this.inData = new BufferedInputStream(inputStream, MMHttpDefines.REQ_TYPE_QUERY_MONTH);
        this.inDataLen = i;
        if ("application/vnd.oma.drm.message".equals(str)) {
            i2 = 1;
        } else if ("application/vnd.oma.drm.content".equals(str)) {
            i2 = 2;
        } else {
            throw new IllegalArgumentException("mimeType must be DRM_MIMETYPE_MESSAGE or DRM_MIMETYPE_CONTENT");
        }
        if (i <= 0) {
            throw new IllegalArgumentException("len must be > 0");
        }
        this.id = nativeConstructDrmContent(this.inData, this.inDataLen, i2);
        if (-1 == this.id) {
            throw new DrmException("nativeConstructDrmContent() returned JNI_DRM_FAILURE");
        }
        this.rightsIssuer = nativeGetRightsAddress();
        this.rawType = nativeGetDeliveryMethod();
        if (-1 == this.rawType) {
            throw new DrmException("nativeGetDeliveryMethod() returned JNI_DRM_FAILURE");
        }
        this.mediaType = nativeGetContentType();
        if (this.mediaType == null) {
            throw new DrmException("nativeGetContentType() returned null");
        }
    }

    private native int nativeConstructDrmContent(InputStream inputStream, int i, int i2);

    /* access modifiers changed from: private */
    public native int nativeGetContentLength();

    private native String nativeGetContentType();

    private native int nativeGetDeliveryMethod();

    private native String nativeGetRightsAddress();

    /* access modifiers changed from: private */
    public native int nativeReadContent(byte[] bArr, int i, int i2, int i3);

    /* access modifiers changed from: protected */
    public native void finalize();

    public InputStream getContentInputStream(DrmRights drmRights) {
        if (drmRights != null) {
            return new DrmInputStream(drmRights);
        }
        throw new NullPointerException();
    }

    public int getContentLength(DrmRights drmRights) throws DrmException {
        if (drmRights == null) {
            throw new NullPointerException();
        }
        int nativeGetContentLength = nativeGetContentLength();
        if (-1 == nativeGetContentLength) {
            throw new DrmException("nativeGetContentLength() returned JNI_DRM_FAILURE");
        } else if (-3 == nativeGetContentLength) {
            return -1;
        } else {
            return nativeGetContentLength;
        }
    }

    public String getContentType() {
        return this.mediaType;
    }

    public int getRawType() {
        return this.rawType;
    }

    public String getRightsAddress() {
        return this.rightsIssuer;
    }
}
