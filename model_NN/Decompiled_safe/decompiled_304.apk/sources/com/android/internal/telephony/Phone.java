package com.android.internal.telephony;

public interface Phone {
    public static final int APN_ALREADY_ACTIVE = 0;
    public static final int APN_REQUEST_FAILED = 3;
    public static final int APN_REQUEST_STARTED = 1;
    public static final String APN_TYPE_ALL = "*";
    public static final String APN_TYPE_DEFAULT = "default";
    public static final String APN_TYPE_DUN = "dun";
    public static final String APN_TYPE_HIPRI = "hipri";
    public static final String APN_TYPE_MMS = "mms";
    public static final int APN_TYPE_NOT_AVAILABLE = 2;
    public static final String APN_TYPE_SUPL = "supl";
    public static final String FEATURE_ENABLE_DUN = "enableDUN";
    public static final String FEATURE_ENABLE_HIPRI = "enableHIPRI";
    public static final String FEATURE_ENABLE_MMS = "enableMMS";
    public static final String FEATURE_ENABLE_SUPL = "enableSUPL";
    public static final String STATE_KEY = "state";
}
