package com.android.internal.widget.database;

import android.database.AbstractCursor;
import android.database.CursorWindow;
import java.util.ArrayList;

public class ArrayListCursor extends AbstractCursor {
    private String[] mColumnNames;
    private ArrayList<Object>[] mRows;

    public ArrayListCursor(String[] strArr, ArrayList<ArrayList> arrayList) {
        boolean z;
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (strArr[i].compareToIgnoreCase("_id") == 0) {
                this.mColumnNames = strArr;
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            this.mColumnNames = new String[(length + 1)];
            System.arraycopy(strArr, 0, this.mColumnNames, 0, strArr.length);
            this.mColumnNames[length] = "_id";
        }
        int size = arrayList.size();
        this.mRows = new ArrayList[size];
        for (int i2 = 0; i2 < size; i2++) {
            this.mRows[i2] = arrayList.get(i2);
            if (!z) {
                this.mRows[i2].add(Integer.valueOf(i2));
            }
        }
    }

    public void fillWindow(int i, CursorWindow cursorWindow) {
        if (i >= 0 && i <= getCount()) {
            cursorWindow.acquireReference();
            try {
                int i2 = this.mPos;
                this.mPos = i - 1;
                cursorWindow.clear();
                cursorWindow.setStartPosition(i);
                int columnCount = getColumnCount();
                cursorWindow.setNumColumns(columnCount);
                while (moveToNext() && cursorWindow.allocRow()) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= columnCount) {
                            break;
                        }
                        Object obj = this.mRows[this.mPos].get(i3);
                        if (obj != null) {
                            if (obj instanceof byte[]) {
                                if (!cursorWindow.putBlob((byte[]) obj, this.mPos, i3)) {
                                    cursorWindow.freeLastRow();
                                    break;
                                }
                            } else if (!cursorWindow.putString(obj.toString(), this.mPos, i3)) {
                                cursorWindow.freeLastRow();
                                break;
                            }
                        } else if (!cursorWindow.putNull(this.mPos, i3)) {
                            cursorWindow.freeLastRow();
                            break;
                        }
                        i3++;
                    }
                }
                this.mPos = i2;
            } catch (IllegalStateException e) {
            } finally {
                cursorWindow.releaseReference();
            }
        }
    }

    public byte[] getBlob(int i) {
        return (byte[]) this.mRows[this.mPos].get(i);
    }

    public String[] getColumnNames() {
        return this.mColumnNames;
    }

    public int getCount() {
        return this.mRows.length;
    }

    public double getDouble(int i) {
        return ((Number) this.mRows[this.mPos].get(i)).doubleValue();
    }

    public float getFloat(int i) {
        return ((Number) this.mRows[this.mPos].get(i)).floatValue();
    }

    public int getInt(int i) {
        return ((Number) this.mRows[this.mPos].get(i)).intValue();
    }

    public long getLong(int i) {
        return ((Number) this.mRows[this.mPos].get(i)).longValue();
    }

    public short getShort(int i) {
        return ((Number) this.mRows[this.mPos].get(i)).shortValue();
    }

    public String getString(int i) {
        Object obj = this.mRows[this.mPos].get(i);
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    public boolean isNull(int i) {
        return this.mRows[this.mPos].get(i) == null;
    }
}
