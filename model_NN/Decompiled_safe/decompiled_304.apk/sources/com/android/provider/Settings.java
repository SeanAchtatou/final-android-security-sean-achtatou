package com.android.provider;

import android.content.ContentQueryMap;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Handler;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.AndroidException;
import android.util.Log;
import com.google.android.collect.Maps;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;

public final class Settings {
    public static final String ACTION_ACCESSIBILITY_SETTINGS = "android.settings.ACCESSIBILITY_SETTINGS";
    public static final String ACTION_AIRPLANE_MODE_SETTINGS = "android.settings.AIRPLANE_MODE_SETTINGS";
    public static final String ACTION_APN_SETTINGS = "android.settings.APN_SETTINGS";
    public static final String ACTION_APPLICATION_DEVELOPMENT_SETTINGS = "android.settings.APPLICATION_DEVELOPMENT_SETTINGS";
    public static final String ACTION_APPLICATION_SETTINGS = "android.settings.APPLICATION_SETTINGS";
    public static final String ACTION_BLUETOOTH_SETTINGS = "android.settings.BLUETOOTH_SETTINGS";
    public static final String ACTION_DATA_ROAMING_SETTINGS = "android.settings.DATA_ROAMING_SETTINGS";
    public static final String ACTION_DATE_SETTINGS = "android.settings.DATE_SETTINGS";
    public static final String ACTION_DISPLAY_SETTINGS = "android.settings.DISPLAY_SETTINGS";
    public static final String ACTION_INPUT_METHOD_SETTINGS = "android.settings.INPUT_METHOD_SETTINGS";
    public static final String ACTION_INTERNAL_STORAGE_SETTINGS = "android.settings.INTERNAL_STORAGE_SETTINGS";
    public static final String ACTION_LOCALE_SETTINGS = "android.settings.LOCALE_SETTINGS";
    public static final String ACTION_LOCATION_SOURCE_SETTINGS = "android.settings.LOCATION_SOURCE_SETTINGS";
    public static final String ACTION_MANAGE_APPLICATIONS_SETTINGS = "android.settings.MANAGE_APPLICATIONS_SETTINGS";
    public static final String ACTION_MEMORY_CARD_SETTINGS = "android.settings.MEMORY_CARD_SETTINGS";
    public static final String ACTION_NETWORK_OPERATOR_SETTINGS = "android.settings.NETWORK_OPERATOR_SETTINGS";
    public static final String ACTION_PRIVACY_SETTINGS = "android.settings.PRIVACY_SETTINGS";
    public static final String ACTION_QUICK_LAUNCH_SETTINGS = "android.settings.QUICK_LAUNCH_SETTINGS";
    public static final String ACTION_SECURITY_SETTINGS = "android.settings.SECURITY_SETTINGS";
    public static final String ACTION_SETTINGS = "android.settings.SETTINGS";
    public static final String ACTION_SOUND_SETTINGS = "android.settings.SOUND_SETTINGS";
    public static final String ACTION_SYNC_SETTINGS = "android.settings.SYNC_SETTINGS";
    public static final String ACTION_SYSTEM_UPDATE_SETTINGS = "android.settings.SYSTEM_UPDATE_SETTINGS";
    public static final String ACTION_USER_DICTIONARY_SETTINGS = "android.settings.USER_DICTIONARY_SETTINGS";
    public static final String ACTION_WIFI_IP_SETTINGS = "android.settings.WIFI_IP_SETTINGS";
    public static final String ACTION_WIFI_SETTINGS = "android.settings.WIFI_SETTINGS";
    public static final String ACTION_WIRELESS_SETTINGS = "android.settings.WIRELESS_SETTINGS";
    public static final String AUTHORITY = "settings";
    private static final String JID_RESOURCE_PREFIX = "android";
    private static final String TAG = "Settings";

    public static final class Bookmarks implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://settings/bookmarks");
        public static final String FOLDER = "folder";
        public static final String ID = "_id";
        public static final String INTENT = "intent";
        public static final String ORDERING = "ordering";
        public static final String SHORTCUT = "shortcut";
        private static final String TAG = "Bookmarks";
        public static final String TITLE = "title";
        private static final String[] sIntentProjection = {INTENT};
        private static final String[] sShortcutProjection = {"_id", SHORTCUT};
        private static final String sShortcutSelection = "shortcut=?";

        public static Intent getIntentForShortcut(ContentResolver contentResolver, char c) {
            Cursor query = contentResolver.query(CONTENT_URI, sIntentProjection, sShortcutSelection, new String[]{String.valueOf((int) c)}, ORDERING);
            Intent intent = null;
            while (intent == null) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    intent = Intent.getIntent(query.getString(query.getColumnIndexOrThrow(INTENT)));
                } catch (URISyntaxException e) {
                } catch (IllegalArgumentException e2) {
                    Log.w(TAG, "Intent column not found", e2);
                } catch (Throwable th) {
                    if (query != null) {
                        query.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            return intent;
        }

        public static CharSequence getLabelForFolder(Resources resources, String str) {
            return str;
        }

        public static CharSequence getTitle(Context context, Cursor cursor) {
            int columnIndex = cursor.getColumnIndex("title");
            int columnIndex2 = cursor.getColumnIndex(INTENT);
            if (columnIndex == -1 || columnIndex2 == -1) {
                throw new IllegalArgumentException("The cursor must contain the TITLE and INTENT columns.");
            }
            String string = cursor.getString(columnIndex);
            if (!TextUtils.isEmpty(string)) {
                return string;
            }
            String string2 = cursor.getString(columnIndex2);
            if (TextUtils.isEmpty(string2)) {
                return "";
            }
            try {
                Intent intent = Intent.getIntent(string2);
                PackageManager packageManager = context.getPackageManager();
                ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
                return resolveActivity != null ? resolveActivity.loadLabel(packageManager) : "";
            } catch (URISyntaxException e) {
                return "";
            }
        }
    }

    public static final class Gservices extends NameValueTable {
        public static final String ANR_BUGREPORT_RECIPIENT = "anr_bugreport_recipient";
        public static final String AUTOTEST_CHECKIN_SECONDS = "autotest_checkin_seconds";
        public static final String AUTOTEST_REBOOT_SECONDS = "autotest_reboot_seconds";
        public static final String AUTOTEST_SUITES_FILE = "autotest_suites_file";
        public static final String BATTERY_DISCHARGE_DURATION_THRESHOLD = "battery_discharge_duration_threshold";
        public static final String BATTERY_DISCHARGE_THRESHOLD = "battery_discharge_threshold";
        public static final String CHANGED_ACTION = "com.google.gservices.intent.action.GSERVICES_CHANGED";
        public static final String CHECKIN_DUMPSYS_LIST = "checkin_dumpsys_list";
        public static final String CHECKIN_EVENTS = "checkin_events";
        public static final String CHECKIN_INTERVAL = "checkin_interval";
        public static final String CHECKIN_PACKAGE_LIST = "checkin_package_list";
        public static final Uri CONTENT_URI = Uri.parse("content://settings/gservices");
        public static final String DISK_FREE_CHANGE_REPORTING_THRESHOLD = "disk_free_change_reporting_threshold";
        public static final String GLS_PUBLIC_KEY = "google_login_public_key";
        public static final String GMAIL_BUFFER_SERVER_RESPONSE = "gmail_buffer_server_response";
        public static final String GMAIL_CONFIG_INFO_MIN_SERVER_VERSION = "gmail_config_info_min_server_version";
        public static final String GMAIL_DISALLOW_IMAGE_PREVIEWS = "gmail_disallow_image_previews";
        public static final String GMAIL_DISCARD_ERROR_UPHILL_OP = "gmail_discard_error_uphill_op";
        public static final String GMAIL_DISCARD_ERROR_UPHILL_OP_NEW = "gmail_discard_error_uphill_op_new";
        public static final String GMAIL_MAX_ATTACHMENT_SIZE = "gmail_max_attachment_size_bytes";
        public static final String GMAIL_MAX_GZIP_SIZE = "gmail_max_gzip_size_bytes";
        public static final String GMAIL_NUM_RETRY_UPHILL_OP = "gmail_num_retry_uphill_op";
        public static final String GMAIL_SEND_IMMEDIATELY = "gmail_send_immediately";
        public static final String GMAIL_TIMEOUT_MS = "gmail_timeout_ms";
        public static final String GMAIL_USE_MULTIPART_PROTOBUF = "gmail_use_multipart_protobuf";
        public static final String GMAIL_WAIT_TIME_RETRY_UPHILL_OP = "gmail_wait_time_retry_uphill_op";
        public static final String GOOGLE_CALENDAR_SYNC_WINDOW_DAYS = "google_calendar_sync_window_days";
        public static final String GOOGLE_CALENDAR_SYNC_WINDOW_UPDATE_DAYS = "google_calendar_sync_window_update_days";
        public static final String GOOGLE_LOGIN_GENERIC_AUTH_SERVICE = "google_login_generic_auth_service";
        public static final String GOOGLE_SERVICES_PREFIX = "google_services:";
        public static final String GPRS_REGISTER_CHECK_PERIOD_MS = "gprs_register_check_period_ms";
        public static final String GSYNC_USE_RMQ2_ROUTING_INFO = "gsync_use_rmq2_routing_info";
        public static final String GTALK_CHAT_EXPIRATION_TIME = "gtalk_chat_expiration_time";
        public static final String GTALK_CHAT_MESSAGE_LIFETIME = "gtalk_chat_message_lifetime";
        public static final String GTALK_COMPRESS = "gtalk_compress";
        public static final String GTALK_DATA_MESSAGE_WAKELOCK_MS = "gtalk_data_message_wakelock_ms";
        public static final String GTALK_FLICKR_PHOTO_INFO_URL = "gtalk_flickr_photo_info_url";
        public static final String GTALK_FLICKR_PHOTO_URL = "gtalk_flickr_photo_url";
        public static final String GTALK_MAX_CONNECTION_HISTORY_RECORDS = "gtalk_max_conn_history_records";
        public static final String GTALK_MAX_RETRIES_FOR_AUTH_EXPIRED = "gtalk_max_retries_for_auth_expired";
        public static final String GTALK_OLD_CHAT_MESSAGE_THRESHOLD_IN_SEC = "gtalk_old_chat_msg_threshold_in_sec";
        public static final String GTALK_OTR_MESSAGE_LIFETIME = "gtalk_otr_message_lifetime";
        public static final String GTALK_PICASA_ALBUM_URL = "gtalk_picasa_album_url";
        public static final String GTALK_RMQ2_INCLUDE_STREAM_ID = "gtalk_rmq2_include_stream_id";
        public static final String GTALK_SERVICE_ACTIVE_HEARTBEAT_INTERVAL_MS = "gtalk_active_heartbeat_ping_interval_ms";
        public static final String GTALK_SERVICE_AWAY_HEARTBEAT_INTERVAL_MS = "gtalk_heartbeat_ping_interval_ms";
        public static final String GTALK_SERVICE_CONNECT_ON_AUTO_SYNC = "gtalk_connect_on_auto_sync";
        public static final String GTALK_SERVICE_HEARTBEAT_ACK_TIMEOUT_MS = "gtalk_heartbeat_ack_timeout_ms";
        public static final String GTALK_SERVICE_HOSTNAME = "gtalk_hostname";
        public static final String GTALK_SERVICE_IDLE_TIMEOUT_MS = "gtalk_idle_timeout_ms";
        public static final String GTALK_SERVICE_MAX_RECONNECT_DELAY = "gtalk_max_reconnect_delay";
        public static final String GTALK_SERVICE_MIN_RECONNECT_DELAY_LONG = "gtalk_min_reconnect_delay_long";
        public static final String GTALK_SERVICE_MIN_RECONNECT_DELAY_SHORT = "gtalk_min_reconnect_delay_short";
        public static final String GTALK_SERVICE_NOSYNC_HEARTBEAT_INTERVAL_MS = "gtalk_nosync_heartbeat_ping_interval_ms";
        public static final String GTALK_SERVICE_RECONNECT_VARIANT_LONG = "gtalk_reconnect_variant_long";
        public static final String GTALK_SERVICE_RECONNECT_VARIANT_SHORT = "gtalk_reconnect_variant_short";
        public static final String GTALK_SERVICE_RMQ_ACK_INTERVAL = "gtalk_rmq_ack_interval";
        public static final String GTALK_SERVICE_SECURE_PORT = "gtalk_secure_port";
        public static final String GTALK_SERVICE_SHORT_NETWORK_DOWNTIME = "gtalk_short_network_downtime";
        public static final String GTALK_SERVICE_SYNC_HEARTBEAT_INTERVAL_MS = "gtalk_sync_heartbeat_ping_interval_ms";
        public static final String GTALK_SERVICE_WIFI_MAX_HEARTBEAT_INTERVAL_MS = "gtalk_wifi_max_heartbeat_ping_interval_ms";
        public static final String GTALK_SSL_HANDSHAKE_TIMEOUT_MS = "gtalk_ssl_handshake_timeout_ms";
        public static final String GTALK_SUPPORT_RMQ_AND_RMQ2_PROTOCOLS = "gtalk_support_rmq_and_rmq2";
        public static final String GTALK_TERMS_OF_SERVICE_URL = "gtalk_terms_of_service_url";
        public static final String GTALK_URL_SCRAPING_FOR_JPG = "gtalk_url_scraping_for_jpg";
        public static final String GTALK_USE_BARE_JID_TIMEOUT_MS = "gtalk_use_barejid_timeout_ms";
        public static final String GTALK_USE_RMQ2_PROTOCOL = "gtalk_use_rmq2";
        public static final String GTALK_YOUTUBE_VIDEO_URL = "gtalk_youtube_video_url";
        public static final String LAST_KMSG_KB = "last_kmsg_kb";
        public static final String MARKET_FORCE_CHECKIN = "market_force_checkin";
        public static final String MEMCHECK_EXEC_END_TIME = "memcheck_exec_end_time";
        public static final String MEMCHECK_EXEC_START_TIME = "memcheck_exec_start_time";
        public static final String MEMCHECK_INTERVAL = "memcheck_interval";
        public static final String MEMCHECK_LOG_REALTIME_INTERVAL = "memcheck_log_realtime_interval";
        public static final String MEMCHECK_MIN_ALARM = "memcheck_min_alarm";
        public static final String MEMCHECK_MIN_SCREEN_OFF = "memcheck_min_screen_off";
        public static final String MEMCHECK_PHONE_ENABLED = "memcheck_phone_enabled";
        public static final String MEMCHECK_PHONE_HARD_THRESHOLD = "memcheck_phone_hard";
        public static final String MEMCHECK_PHONE_SOFT_THRESHOLD = "memcheck_phone_soft";
        public static final String MEMCHECK_RECHECK_INTERVAL = "memcheck_recheck_interval";
        public static final String MEMCHECK_SYSTEM_ENABLED = "memcheck_system_enabled";
        public static final String MEMCHECK_SYSTEM_HARD_THRESHOLD = "memcheck_system_hard";
        public static final String MEMCHECK_SYSTEM_SOFT_THRESHOLD = "memcheck_system_soft";
        public static final String MMS_MAXIMUM_MESSAGE_SIZE = "mms_maximum_message_size";
        public static final String MMS_X_WAP_PROFILE_URL = "mms_x_wap_profile_url";
        public static final String NITZ_UPDATE_DIFF = "nitz_update_diff";
        public static final String NITZ_UPDATE_SPACING = "nitz_update_spacing";
        public static final String OVERRIDE_ACTION = "com.google.gservices.intent.action.GSERVICES_OVERRIDE";
        public static final String PARENTAL_CONTROL_APPS_LIST = "parental_control_apps_list";
        public static final String PARENTAL_CONTROL_CHECK_ENABLED = "parental_control_check_enabled";
        public static final String PARENTAL_CONTROL_EXPECTED_RESPONSE = "parental_control_expected_response";
        public static final String PARENTAL_CONTROL_REDIRECT_REGEX = "parental_control_redirect_regex";
        public static final String PARENTAL_CONTROL_TIMEOUT_IN_MS = "parental_control_timeout_in_ms";
        public static final String PDP_WATCHDOG_ERROR_POLL_COUNT = "pdp_watchdog_error_poll_count";
        public static final String PDP_WATCHDOG_ERROR_POLL_INTERVAL_MS = "pdp_watchdog_error_poll_interval_ms";
        public static final String PDP_WATCHDOG_LONG_POLL_INTERVAL_MS = "pdp_watchdog_long_poll_interval_ms";
        public static final String PDP_WATCHDOG_MAX_PDP_RESET_FAIL_COUNT = "pdp_watchdog_max_pdp_reset_fail_count";
        public static final String PDP_WATCHDOG_PING_ADDRESS = "pdp_watchdog_ping_address";
        public static final String PDP_WATCHDOG_PING_DEADLINE = "pdp_watchdog_ping_deadline";
        public static final String PDP_WATCHDOG_POLL_INTERVAL_MS = "pdp_watchdog_poll_interval_ms";
        public static final String PDP_WATCHDOG_TRIGGER_PACKET_COUNT = "pdp_watchdog_trigger_packet_count";
        public static final String PROVISIONING_DIGEST = "digest";
        public static final String PROVISIONING_OVERRIDE = "override";
        public static final String PUSH_MESSAGING_REGISTRATION_URL = "push_messaging_registration_url";
        public static final String REBOOT_INTERVAL = "reboot_interval";
        public static final String REBOOT_START_TIME = "reboot_start_time";
        public static final String REBOOT_WINDOW = "reboot_window";
        public static final String SEARCH_MAX_RESULTS_PER_SOURCE = "search_max_results_per_source";
        public static final String SEARCH_MAX_RESULTS_TO_DISPLAY = "search_max_results_to_display";
        public static final String SEARCH_MAX_SHORTCUTS_RETURNED = "search_max_shortcuts_returned";
        public static final String SEARCH_MAX_SOURCE_EVENT_AGE_MILLIS = "search_max_source_event_age_millis";
        public static final String SEARCH_MAX_STAT_AGE_MILLIS = "search_max_stat_age_millis";
        public static final String SEARCH_MIN_CLICKS_FOR_SOURCE_RANKING = "search_min_clicks_for_source_ranking";
        public static final String SEARCH_MIN_IMPRESSIONS_FOR_SOURCE_RANKING = "search_min_impressions_for_source_ranking";
        public static final String SEARCH_NUM_PROMOTED_SOURCES = "search_num_promoted_sources";
        public static final String SEARCH_PER_SOURCE_CONCURRENT_QUERY_LIMIT = "search_per_source_concurrent_query_limit";
        public static final String SEARCH_PREFILL_MILLIS = "search_prefill_millis";
        public static final String SEARCH_PROMOTED_SOURCE_DEADLINE_MILLIS = "search_promoted_source_deadline_millis";
        public static final String SEARCH_QUERY_THREAD_CORE_POOL_SIZE = "search_query_thread_core_pool_size";
        public static final String SEARCH_QUERY_THREAD_MAX_POOL_SIZE = "search_query_thread_max_pool_size";
        public static final String SEARCH_SHORTCUT_REFRESH_CORE_POOL_SIZE = "search_shortcut_refresh_core_pool_size";
        public static final String SEARCH_SHORTCUT_REFRESH_MAX_POOL_SIZE = "search_shortcut_refresh_max_pool_size";
        public static final String SEARCH_SOURCE_TIMEOUT_MILLIS = "search_source_timeout_millis";
        public static final String SEARCH_THREAD_KEEPALIVE_SECONDS = "search_thread_keepalive_seconds";
        public static final String SEARCH_WEB_RESULTS_OVERRIDE_LIMIT = "search_web_results_override_limit";
        public static final String SEND_ACTION_APP_ERROR = "send_action_app_error";
        public static final String SETTINGS_CONTRIBUTORS_PRETTY_URL = "settings_contributors_pretty_url";
        public static final String SETTINGS_CONTRIBUTORS_URL = "settings_contributors_url";
        public static final String SETTINGS_TOS_PRETTY_URL = "settings_tos_pretty_url";
        public static final String SETTINGS_TOS_URL = "settings_tos_url";
        public static final String SETUP_ANDROID_PRIVACY_URL = "setup_android_privacy_url";
        public static final String SETUP_GOOGLE_PRIVACY_URL = "setup_google_privacy_url";
        public static final String SETUP_GOOGLE_TOS_URL = "setup_google_tos_url";
        public static final String SHORT_KEYLIGHT_DELAY_MS = "short_keylight_delay_ms";
        public static final String SMS_OUTGOING_CEHCK_MAX_COUNT = "sms_outgoing_check_max_count";
        public static final String SMS_OUTGOING_CHECK_INTERVAL_MS = "sms_outgoing_check_interval_ms";
        public static final String SSL_SESSION_CACHE = "ssl_session_cache";
        public static final String SYNC_MAX_RETRY_DELAY_IN_SECONDS = "sync_max_retry_delay_in_seconds";
        public static final String SYNC_MIN_GZIP_BYTES = "sync_min_gzip_bytes";
        public static final String SYS_FREE_STORAGE_LOG_INTERVAL = "sys_free_storage_log_interval";
        public static final String SYS_PROP_SETTING_VERSION = "sys.settings_gservices_version";
        public static final String SYS_STORAGE_THRESHOLD_PERCENTAGE = "sys_storage_threshold_percentage";
        public static final String TRANSCODER_URL = "mobile_transcoder_url";
        public static final String USE_LOCATION_FOR_SERVICES = "use_location";
        public static final String USE_MSISDN_TOKEN = "use_msisdn_token";
        public static final String VENDING_BACKUP_STATE = "vending_backup_state";
        public static final String VENDING_CARRIER_CREDENTIALS_BUFFER_MS = "vending_carrier_cred_buf_ms";
        public static final String VENDING_CARRIER_PROVISIONING_REFRESH_FREQUENCY_MS = "vending_carrier_ref_freq_ms";
        public static final String VENDING_CARRIER_PROVISIONING_RETRY_MS = "vending_carrier_prov_retry_ms";
        public static final String VENDING_DEFAULT_FILTER = "vending_default_filter";
        public static final String VENDING_DISK_INPUT_BUFFER_BYTES = "vending_disk_input_buffer_bytes";
        public static final String VENDING_DISK_OUTPUT_BUFFER_BYTES = "vending_disk_output_buffer_bytes";
        public static final String VENDING_DOWNLOADING_KICK_TIMEOUT_MS = "vending_downloading_kick_ms";
        public static final String VENDING_HEARTBEAT_FREQUENCY_MS = "vending_heartbeat_frequency_ms";
        public static final String VENDING_PENDING_DOWNLOAD_RESEND_FREQUENCY_MS = "vending_pd_resend_frequency_ms";
        public static final String VENDING_PROMO_REFRESH_FREQUENCY_MS = "vending_promo_refresh_freq_ms";
        public static final String VENDING_REQUIRE_SIM_FOR_PURCHASE = "vending_require_sim_for_purchase";
        public static final String VENDING_SUPPORT_URL = "vending_support_url";
        public static final String VENDING_SYNC_FREQUENCY_MS = "vending_sync_frequency_ms";
        public static final String VENDING_TAB_1_RANKING_TYPE = "vending_tab_1_ranking_type";
        public static final String VENDING_TAB_1_TITLE = "vending_tab_1_title";
        public static final String VENDING_TAB_2_RANKING_TYPE = "vending_tab_2_ranking_type";
        public static final String VENDING_TAB_2_TITLE = "vending_tab_2_title";
        public static final String VENDING_TOS_MISSING_URL = "vending_tos_missing_url";
        public static final String VENDING_TOS_URL = "vending_tos_url";
        public static final String VENDING_TOS_VERSION = "vending_tos_version";
        public static final String VENDING_USE_CHECKOUT_QA_SERVICE = "vending_use_checkout_qa_service";
        public static final String WIFI_IDLE_MS = "wifi_idle_ms";
        public static final String YOUTUBE_USE_PROXY = "youtube_use_proxy";
        private static volatile NameValueCache mNameValueCache = null;
        private static final Object mNameValueCacheLock = new Object();

        @Deprecated
        public static class QueryMap extends ContentQueryMap {
            public QueryMap(ContentResolver contentResolver, Cursor cursor, boolean z, Handler handler) {
                super(cursor, "name", z, handler);
            }

            public QueryMap(ContentResolver contentResolver, boolean z, Handler handler) {
                this(contentResolver, contentResolver.query(Gservices.CONTENT_URI, null, null, null, null), z, handler);
            }

            public String getString(String str) {
                ContentValues values = getValues(str);
                if (values == null) {
                    return null;
                }
                return values.getAsString(NameValueTable.VALUE);
            }
        }

        public static int getInt(ContentResolver contentResolver, String str, int i) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return i;
            }
            try {
                return Integer.parseInt(string);
            } catch (NumberFormatException e) {
                return i;
            }
        }

        public static long getLong(ContentResolver contentResolver, String str, long j) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return j;
            }
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException e) {
                return j;
            }
        }

        public static String getString(ContentResolver contentResolver, String str) {
            String string;
            synchronized (mNameValueCacheLock) {
                if (mNameValueCache == null) {
                    mNameValueCache = new NameValueCache(SYS_PROP_SETTING_VERSION, CONTENT_URI);
                }
                string = mNameValueCache.getString(contentResolver, str);
            }
            return string;
        }

        public static Uri getUriFor(String str) {
            return getUriFor(CONTENT_URI, str);
        }

        public static boolean putString(ContentResolver contentResolver, String str, String str2) {
            return putString(contentResolver, CONTENT_URI, str, str2);
        }
    }

    private static class NameValueCache {
        private final Uri mUri;
        private final HashMap<String, String> mValues = Maps.newHashMap();
        private long mValuesVersion = 0;
        private final String mVersionSystemProperty;

        NameValueCache(String str, Uri uri) {
            this.mVersionSystemProperty = str;
            this.mUri = uri;
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0078, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x008f, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0090, code lost:
            r7 = r1;
            r1 = r0;
            r0 = r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0096, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0097, code lost:
            r2 = null;
            r7 = r0;
            r0 = r1;
            r1 = r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
            return r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            return r2;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004a  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0078  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x008f A[ExcHandler: all (r1v7 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:8:0x0038] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00a2  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00a4  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String getString(android.content.ContentResolver r9, java.lang.String r10) {
            /*
                r8 = this;
                r6 = 0
                java.lang.String r0 = r8.mVersionSystemProperty
                r1 = 0
                long r0 = android.os.SystemProperties.getLong(r0, r1)
                long r2 = r8.mValuesVersion
                int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
                if (r2 == 0) goto L_0x0016
                java.util.HashMap<java.lang.String, java.lang.String> r2 = r8.mValues
                r2.clear()
                r8.mValuesVersion = r0
            L_0x0016:
                java.util.HashMap<java.lang.String, java.lang.String> r0 = r8.mValues
                boolean r0 = r0.containsKey(r10)
                if (r0 != 0) goto L_0x0085
                android.net.Uri r1 = r8.mUri     // Catch:{ SQLException -> 0x004f, all -> 0x007d }
                r0 = 1
                java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ SQLException -> 0x004f, all -> 0x007d }
                r0 = 0
                java.lang.String r3 = "value"
                r2[r0] = r3     // Catch:{ SQLException -> 0x004f, all -> 0x007d }
                java.lang.String r3 = "name=?"
                r0 = 1
                java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLException -> 0x004f, all -> 0x007d }
                r0 = 0
                r4[r0] = r10     // Catch:{ SQLException -> 0x004f, all -> 0x007d }
                r5 = 0
                r0 = r9
                android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ SQLException -> 0x004f, all -> 0x007d }
                if (r0 == 0) goto L_0x00a6
                boolean r1 = r0.moveToNext()     // Catch:{ SQLException -> 0x0096, all -> 0x008f }
                if (r1 == 0) goto L_0x00a6
                r1 = 0
                java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLException -> 0x0096, all -> 0x008f }
            L_0x0043:
                java.util.HashMap<java.lang.String, java.lang.String> r2 = r8.mValues     // Catch:{ SQLException -> 0x009c, all -> 0x008f }
                r2.put(r10, r1)     // Catch:{ SQLException -> 0x009c, all -> 0x008f }
                if (r0 == 0) goto L_0x00a4
                r0.close()
                r0 = r1
            L_0x004e:
                return r0
            L_0x004f:
                r0 = move-exception
                r1 = r6
                r2 = r6
            L_0x0052:
                java.lang.String r3 = "Settings"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
                r4.<init>()     // Catch:{ all -> 0x0094 }
                java.lang.String r5 = "Can't get key "
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0094 }
                java.lang.StringBuilder r4 = r4.append(r10)     // Catch:{ all -> 0x0094 }
                java.lang.String r5 = " from "
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0094 }
                android.net.Uri r5 = r8.mUri     // Catch:{ all -> 0x0094 }
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0094 }
                java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0094 }
                android.util.Log.w(r3, r4, r0)     // Catch:{ all -> 0x0094 }
                if (r1 == 0) goto L_0x00a2
                r1.close()
                r0 = r2
                goto L_0x004e
            L_0x007d:
                r0 = move-exception
                r1 = r6
            L_0x007f:
                if (r1 == 0) goto L_0x0084
                r1.close()
            L_0x0084:
                throw r0
            L_0x0085:
                java.util.HashMap<java.lang.String, java.lang.String> r0 = r8.mValues
                java.lang.Object r8 = r0.get(r10)
                java.lang.String r8 = (java.lang.String) r8
                r0 = r8
                goto L_0x004e
            L_0x008f:
                r1 = move-exception
                r7 = r1
                r1 = r0
                r0 = r7
                goto L_0x007f
            L_0x0094:
                r0 = move-exception
                goto L_0x007f
            L_0x0096:
                r1 = move-exception
                r2 = r6
                r7 = r0
                r0 = r1
                r1 = r7
                goto L_0x0052
            L_0x009c:
                r2 = move-exception
                r7 = r2
                r2 = r1
                r1 = r0
                r0 = r7
                goto L_0x0052
            L_0x00a2:
                r0 = r2
                goto L_0x004e
            L_0x00a4:
                r0 = r1
                goto L_0x004e
            L_0x00a6:
                r1 = r6
                goto L_0x0043
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.provider.Settings.NameValueCache.getString(android.content.ContentResolver, java.lang.String):java.lang.String");
        }
    }

    public static class NameValueTable implements BaseColumns {
        public static final String NAME = "name";
        public static final String VALUE = "value";

        public static Uri getUriFor(Uri uri, String str) {
            return Uri.withAppendedPath(uri, str);
        }

        protected static boolean putString(ContentResolver contentResolver, Uri uri, String str, String str2) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", str);
                contentValues.put(VALUE, str2);
                contentResolver.insert(uri, contentValues);
                return true;
            } catch (SQLException e) {
                Log.w(Settings.TAG, "Can't set key " + str + " in " + uri, e);
                return false;
            }
        }
    }

    public static final class Secure extends NameValueTable {
        public static final String ACCESSIBILITY_ENABLED = "accessibility_enabled";
        public static final String ADB_ENABLED = "adb_enabled";
        public static final String ALLOW_MOCK_LOCATION = "mock_location";
        public static final String ANDROID_ID = "android_id";
        public static final String ASSISTED_GPS_ENABLED = "assisted_gps_enabled";
        public static final String BACKGROUND_DATA = "background_data";
        public static final String BACKUP_ENABLED = "backup_enabled";
        public static final String BACKUP_PROVISIONED = "backup_provisioned";
        public static final String BACKUP_TRANSPORT = "backup_transport";
        public static final String BLUETOOTH_ON = "bluetooth_on";
        public static final String CDMA_CELL_BROADCAST_SMS = "cdma_cell_broadcast_sms";
        public static final String CDMA_ROAMING_MODE = "roaming_settings";
        public static final String CDMA_SUBSCRIPTION_MODE = "subscription_mode";
        public static final String CHECKIN_SEND_APANIC_CONSOLE_TIME = "checkin_apanic_console_time";
        public static final String CHECKIN_SEND_APANIC_THREAD_TIME = "checkin_apanic_thread_time";
        public static final String CHECKIN_SEND_LAST_KMSG_TIME = "checkin_kmsg_time";
        public static final Uri CONTENT_URI = Uri.parse("content://settings/secure");
        public static final String DATA_ROAMING = "data_roaming";
        public static final String DEFAULT_INPUT_METHOD = "default_input_method";
        public static final String DEVICE_PROVISIONED = "device_provisioned";
        public static final String ENABLED_ACCESSIBILITY_SERVICES = "enabled_accessibility_services";
        public static final String ENABLED_INPUT_METHODS = "enabled_input_methods";
        public static final String ENHANCED_VOICE_PRIVACY_ENABLED = "enhanced_voice_privacy_enabled";
        public static final String HTTP_PROXY = "http_proxy";
        public static final String INSTALL_NON_MARKET_APPS = "install_non_market_apps";
        public static final String LAST_SETUP_SHOWN = "last_setup_shown";
        public static final String LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed";
        @Deprecated
        public static final String LOGGING_ID = "logging_id";
        public static final String LOGGING_ID2 = "logging_id2";
        public static final String NETWORK_PREFERENCE = "network_preference";
        public static final String PARENTAL_CONTROL_ENABLED = "parental_control_enabled";
        public static final String PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update";
        public static final String PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url";
        public static final String PREFERRED_CDMA_SUBSCRIPTION = "preferred_cdma_subscription";
        public static final String PREFERRED_NETWORK_MODE = "preferred_network_mode";
        public static final String PREFERRED_TTY_MODE = "preferred_tty_mode";
        public static final String SETTINGS_CLASSNAME = "settings_classname";
        public static final String[] SETTINGS_TO_BACKUP = {"adb_enabled", ALLOW_MOCK_LOCATION, "parental_control_enabled", "parental_control_redirect_url", "usb_mass_storage_enabled", ACCESSIBILITY_ENABLED, ENABLED_ACCESSIBILITY_SERVICES, TTS_USE_DEFAULTS, TTS_DEFAULT_RATE, TTS_DEFAULT_PITCH, TTS_DEFAULT_SYNTH, TTS_DEFAULT_LANG, TTS_DEFAULT_COUNTRY, "wifi_networks_available_notification_on", "wifi_networks_available_repeat_delay", "wifi_num_allowed_channels", "wifi_num_open_networks_kept"};
        public static final String SYS_PROP_SETTING_VERSION = "sys.settings_secure_version";
        public static final String TTS_DEFAULT_COUNTRY = "tts_default_country";
        public static final String TTS_DEFAULT_LANG = "tts_default_lang";
        public static final String TTS_DEFAULT_PITCH = "tts_default_pitch";
        public static final String TTS_DEFAULT_RATE = "tts_default_rate";
        public static final String TTS_DEFAULT_SYNTH = "tts_default_synth";
        public static final String TTS_DEFAULT_VARIANT = "tts_default_variant";
        public static final String TTS_USE_DEFAULTS = "tts_use_defaults";
        public static final String TTY_MODE_ENABLED = "tty_mode_enabled";
        public static final String USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled";
        public static final String USE_GOOGLE_MAIL = "use_google_mail";
        public static final String USE_LOCATION_FOR_SERVICES = "use_location";
        public static final String WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count";
        public static final String WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms";
        public static final String WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on";
        public static final String WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay";
        public static final String WIFI_NUM_ALLOWED_CHANNELS = "wifi_num_allowed_channels";
        public static final String WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept";
        public static final String WIFI_ON = "wifi_on";
        public static final String WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage";
        public static final String WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count";
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms";
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled";
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms";
        public static final String WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count";
        public static final String WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks";
        public static final String WIFI_WATCHDOG_ON = "wifi_watchdog_on";
        public static final String WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count";
        public static final String WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms";
        public static final String WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms";
        public static final String WIFI_WATCHDOG_WATCH_LIST = "wifi_watchdog_watch_list";
        private static volatile NameValueCache mNameValueCache = null;

        public static final String getBluetoothA2dpSinkPriorityKey(String str) {
            return "bluetooth_a2dp_sink_priority_" + str.toUpperCase();
        }

        public static final String getBluetoothHeadsetPriorityKey(String str) {
            return "bluetooth_headset_priority_" + str.toUpperCase();
        }

        public static float getFloat(ContentResolver contentResolver, String str) throws SettingNotFoundException {
            try {
                return Float.parseFloat(getString(contentResolver, str));
            } catch (NumberFormatException e) {
                throw new SettingNotFoundException(str);
            }
        }

        public static float getFloat(ContentResolver contentResolver, String str, float f) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return f;
            }
            try {
                return Float.parseFloat(string);
            } catch (NumberFormatException e) {
                return f;
            }
        }

        public static int getInt(ContentResolver contentResolver, String str) throws SettingNotFoundException {
            try {
                return Integer.parseInt(getString(contentResolver, str));
            } catch (NumberFormatException e) {
                throw new SettingNotFoundException(str);
            }
        }

        public static int getInt(ContentResolver contentResolver, String str, int i) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return i;
            }
            try {
                return Integer.parseInt(string);
            } catch (NumberFormatException e) {
                return i;
            }
        }

        public static long getLong(ContentResolver contentResolver, String str) throws SettingNotFoundException {
            try {
                return Long.parseLong(getString(contentResolver, str));
            } catch (NumberFormatException e) {
                throw new SettingNotFoundException(str);
            }
        }

        public static long getLong(ContentResolver contentResolver, String str, long j) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return j;
            }
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException e) {
                return j;
            }
        }

        public static synchronized String getString(ContentResolver contentResolver, String str) {
            String string;
            synchronized (Secure.class) {
                if (mNameValueCache == null) {
                    mNameValueCache = new NameValueCache(SYS_PROP_SETTING_VERSION, CONTENT_URI);
                }
                string = mNameValueCache.getString(contentResolver, str);
            }
            return string;
        }

        public static Uri getUriFor(String str) {
            return getUriFor(CONTENT_URI, str);
        }

        public static final boolean isLocationProviderEnabled(ContentResolver contentResolver, String str) {
            String string = getString(contentResolver, "location_providers_allowed");
            if (string != null) {
                return string.equals(str) || string.contains(new StringBuilder().append(",").append(str).append(",").toString()) || string.startsWith(new StringBuilder().append(str).append(",").toString()) || string.endsWith(new StringBuilder().append(",").append(str).toString());
            }
            return false;
        }

        public static boolean putFloat(ContentResolver contentResolver, String str, float f) {
            return putString(contentResolver, str, Float.toString(f));
        }

        public static boolean putInt(ContentResolver contentResolver, String str, int i) {
            return putString(contentResolver, str, Integer.toString(i));
        }

        public static boolean putLong(ContentResolver contentResolver, String str, long j) {
            return putString(contentResolver, str, Long.toString(j));
        }

        public static boolean putString(ContentResolver contentResolver, String str, String str2) {
            return putString(contentResolver, CONTENT_URI, str, str2);
        }

        public static final void setLocationProviderEnabled(ContentResolver contentResolver, String str, boolean z) {
            putString(contentResolver, "location_providers_allowed", z ? "+" + str : "-" + str);
        }
    }

    public static class SettingNotFoundException extends AndroidException {
        public SettingNotFoundException(String str) {
            super(str);
        }
    }

    public static final class System extends NameValueTable {
        public static final String ACCELEROMETER_ROTATION = "accelerometer_rotation";
        @Deprecated
        public static final String ADB_ENABLED = "adb_enabled";
        public static final String AIRPLANE_MODE_ON = "airplane_mode_on";
        public static final String AIRPLANE_MODE_RADIOS = "airplane_mode_radios";
        public static final String AIRPLANE_MODE_TOGGLEABLE_RADIOS = "airplane_mode_toggleable_radios";
        public static final String ALARM_ALERT = "alarm_alert";
        public static final String ALWAYS_FINISH_ACTIVITIES = "always_finish_activities";
        @Deprecated
        public static final String ANDROID_ID = "android_id";
        public static final String APPEND_FOR_LAST_AUDIBLE = "_last_audible";
        public static final String AUTO_TIME = "auto_time";
        public static final String BLUETOOTH_DISCOVERABILITY = "bluetooth_discoverability";
        public static final String BLUETOOTH_DISCOVERABILITY_TIMEOUT = "bluetooth_discoverability_timeout";
        @Deprecated
        public static final String BLUETOOTH_ON = "bluetooth_on";
        public static final String CALL_AUTO_RETRY = "call_auto_retry";
        public static final String COMPATIBILITY_MODE = "compatibility_mode";
        public static final Uri CONTENT_URI = Uri.parse("content://settings/system");
        @Deprecated
        public static final String DATA_ROAMING = "data_roaming";
        public static final String DATE_FORMAT = "date_format";
        public static final String DEBUG_APP = "debug_app";
        public static final Uri DEFAULT_ALARM_ALERT_URI = getUriFor(ALARM_ALERT);
        public static final Uri DEFAULT_NOTIFICATION_URI = getUriFor(NOTIFICATION_SOUND);
        public static final Uri DEFAULT_RINGTONE_URI = getUriFor(RINGTONE);
        @Deprecated
        public static final String DEVICE_PROVISIONED = "device_provisioned";
        public static final String DIM_SCREEN = "dim_screen";
        public static final String DTMF_TONE_TYPE_WHEN_DIALING = "dtmf_tone_type";
        public static final String DTMF_TONE_WHEN_DIALING = "dtmf_tone";
        public static final String EMERGENCY_TONE = "emergency_tone";
        public static final String END_BUTTON_BEHAVIOR = "end_button_behavior";
        public static final String FANCY_IME_ANIMATIONS = "fancy_ime_animations";
        public static final String FONT_SCALE = "font_scale";
        public static final String HAPTIC_FEEDBACK_ENABLED = "haptic_feedback_enabled";
        public static final String HEARING_AID = "hearing_aid";
        @Deprecated
        public static final String HTTP_PROXY = "http_proxy";
        @Deprecated
        public static final String INSTALL_NON_MARKET_APPS = "install_non_market_apps";
        @Deprecated
        public static final String LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed";
        public static final String LOCK_PATTERN_ENABLED = "lock_pattern_autolock";
        public static final String LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED = "lock_pattern_tactile_feedback_enabled";
        public static final String LOCK_PATTERN_VISIBLE = "lock_pattern_visible_pattern";
        @Deprecated
        public static final String LOGGING_ID = "logging_id";
        public static final String MODE_RINGER = "mode_ringer";
        public static final String MODE_RINGER_STREAMS_AFFECTED = "mode_ringer_streams_affected";
        private static final HashSet<String> MOVED_TO_SECURE = new HashSet<>(30);
        public static final String MUTE_STREAMS_AFFECTED = "mute_streams_affected";
        @Deprecated
        public static final String NETWORK_PREFERENCE = "network_preference";
        public static final String NEXT_ALARM_FORMATTED = "next_alarm_formatted";
        public static final String NOTIFICATIONS_USE_RING_VOLUME = "notifications_use_ring_volume";
        public static final String NOTIFICATION_LIGHT_PULSE = "notification_light_pulse";
        public static final String NOTIFICATION_SOUND = "notification_sound";
        @Deprecated
        public static final String PARENTAL_CONTROL_ENABLED = "parental_control_enabled";
        @Deprecated
        public static final String PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update";
        @Deprecated
        public static final String PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url";
        public static final String RADIO_BLUETOOTH = "bluetooth";
        public static final String RADIO_CELL = "cell";
        public static final String RADIO_WIFI = "wifi";
        public static final String RINGTONE = "ringtone";
        public static final String SCREEN_BRIGHTNESS = "screen_brightness";
        public static final String SCREEN_BRIGHTNESS_MODE = "screen_brightness_mode";
        public static final int SCREEN_BRIGHTNESS_MODE_AUTOMATIC = 1;
        public static final int SCREEN_BRIGHTNESS_MODE_MANUAL = 0;
        public static final String SCREEN_OFF_TIMEOUT = "screen_off_timeout";
        @Deprecated
        public static final String SETTINGS_CLASSNAME = "settings_classname";
        public static final String[] SETTINGS_TO_BACKUP = {STAY_ON_WHILE_PLUGGED_IN, END_BUTTON_BEHAVIOR, WIFI_SLEEP_POLICY, WIFI_USE_STATIC_IP, WIFI_STATIC_IP, WIFI_STATIC_GATEWAY, WIFI_STATIC_NETMASK, WIFI_STATIC_DNS1, WIFI_STATIC_DNS2, BLUETOOTH_DISCOVERABILITY, BLUETOOTH_DISCOVERABILITY_TIMEOUT, DIM_SCREEN, SCREEN_OFF_TIMEOUT, SCREEN_BRIGHTNESS, SCREEN_BRIGHTNESS_MODE, VIBRATE_ON, NOTIFICATIONS_USE_RING_VOLUME, MODE_RINGER, MODE_RINGER_STREAMS_AFFECTED, MUTE_STREAMS_AFFECTED, VOLUME_VOICE, VOLUME_SYSTEM, VOLUME_RING, VOLUME_MUSIC, VOLUME_ALARM, VOLUME_NOTIFICATION, "volume_voice_last_audible", "volume_system_last_audible", "volume_ring_last_audible", "volume_music_last_audible", "volume_alarm_last_audible", "volume_notification_last_audible", TEXT_AUTO_REPLACE, TEXT_AUTO_CAPS, TEXT_AUTO_PUNCTUATE, TEXT_SHOW_PASSWORD, AUTO_TIME, TIME_12_24, DATE_FORMAT, ACCELEROMETER_ROTATION, DTMF_TONE_WHEN_DIALING, DTMF_TONE_TYPE_WHEN_DIALING, EMERGENCY_TONE, CALL_AUTO_RETRY, HEARING_AID, TTY_MODE, SOUND_EFFECTS_ENABLED, HAPTIC_FEEDBACK_ENABLED, SHOW_WEB_SUGGESTIONS, NOTIFICATION_LIGHT_PULSE};
        public static final String SETUP_WIZARD_HAS_RUN = "setup_wizard_has_run";
        public static final String SHOW_GTALK_SERVICE_STATUS = "SHOW_GTALK_SERVICE_STATUS";
        public static final String SHOW_PROCESSES = "show_processes";
        public static final String SHOW_WEB_SUGGESTIONS = "show_web_suggestions";
        public static final String SOUND_EFFECTS_ENABLED = "sound_effects_enabled";
        public static final String STAY_ON_WHILE_PLUGGED_IN = "stay_on_while_plugged_in";
        public static final String SYS_PROP_SETTING_VERSION = "sys.settings_system_version";
        public static final String TEXT_AUTO_CAPS = "auto_caps";
        public static final String TEXT_AUTO_PUNCTUATE = "auto_punctuate";
        public static final String TEXT_AUTO_REPLACE = "auto_replace";
        public static final String TEXT_SHOW_PASSWORD = "show_password";
        public static final String TIME_12_24 = "time_12_24";
        public static final String TRANSITION_ANIMATION_SCALE = "transition_animation_scale";
        public static final String TTY_MODE = "tty_mode";
        @Deprecated
        public static final String USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled";
        @Deprecated
        public static final String USE_GOOGLE_MAIL = "use_google_mail";
        public static final String VIBRATE_ON = "vibrate_on";
        public static final String VOLUME_ALARM = "volume_alarm";
        public static final String VOLUME_MUSIC = "volume_music";
        public static final String VOLUME_NOTIFICATION = "volume_notification";
        public static final String VOLUME_RING = "volume_ring";
        public static final String[] VOLUME_SETTINGS = {VOLUME_VOICE, VOLUME_SYSTEM, VOLUME_RING, VOLUME_MUSIC, VOLUME_ALARM, VOLUME_NOTIFICATION};
        public static final String VOLUME_SYSTEM = "volume_system";
        public static final String VOLUME_VOICE = "volume_voice";
        public static final String WAIT_FOR_DEBUGGER = "wait_for_debugger";
        public static final String WALLPAPER_ACTIVITY = "wallpaper_activity";
        @Deprecated
        public static final String WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count";
        @Deprecated
        public static final String WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms";
        @Deprecated
        public static final String WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on";
        @Deprecated
        public static final String WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay";
        public static final String WIFI_NUM_ALLOWED_CHANNELS = "wifi_num_allowed_channels";
        @Deprecated
        public static final String WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept";
        @Deprecated
        public static final String WIFI_ON = "wifi_on";
        public static final String WIFI_SLEEP_POLICY = "wifi_sleep_policy";
        public static final int WIFI_SLEEP_POLICY_DEFAULT = 0;
        public static final int WIFI_SLEEP_POLICY_NEVER = 2;
        public static final int WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED = 1;
        public static final String WIFI_STATIC_DNS1 = "wifi_static_dns1";
        public static final String WIFI_STATIC_DNS2 = "wifi_static_dns2";
        public static final String WIFI_STATIC_GATEWAY = "wifi_static_gateway";
        public static final String WIFI_STATIC_IP = "wifi_static_ip";
        public static final String WIFI_STATIC_NETMASK = "wifi_static_netmask";
        public static final String WIFI_USE_STATIC_IP = "wifi_use_static_ip";
        @Deprecated
        public static final String WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage";
        @Deprecated
        public static final String WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count";
        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms";
        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled";
        @Deprecated
        public static final String WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms";
        @Deprecated
        public static final String WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count";
        @Deprecated
        public static final String WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks";
        @Deprecated
        public static final String WIFI_WATCHDOG_ON = "wifi_watchdog_on";
        @Deprecated
        public static final String WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count";
        @Deprecated
        public static final String WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms";
        @Deprecated
        public static final String WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms";
        public static final String WINDOW_ANIMATION_SCALE = "window_animation_scale";
        private static volatile NameValueCache mNameValueCache = null;

        static {
            MOVED_TO_SECURE.add("adb_enabled");
            MOVED_TO_SECURE.add("android_id");
            MOVED_TO_SECURE.add("bluetooth_on");
            MOVED_TO_SECURE.add("data_roaming");
            MOVED_TO_SECURE.add("device_provisioned");
            MOVED_TO_SECURE.add("http_proxy");
            MOVED_TO_SECURE.add("install_non_market_apps");
            MOVED_TO_SECURE.add("location_providers_allowed");
            MOVED_TO_SECURE.add("logging_id");
            MOVED_TO_SECURE.add("parental_control_enabled");
            MOVED_TO_SECURE.add("parental_control_last_update");
            MOVED_TO_SECURE.add("parental_control_redirect_url");
            MOVED_TO_SECURE.add("settings_classname");
            MOVED_TO_SECURE.add("usb_mass_storage_enabled");
            MOVED_TO_SECURE.add("use_google_mail");
            MOVED_TO_SECURE.add("wifi_networks_available_notification_on");
            MOVED_TO_SECURE.add("wifi_networks_available_repeat_delay");
            MOVED_TO_SECURE.add("wifi_num_open_networks_kept");
            MOVED_TO_SECURE.add("wifi_on");
            MOVED_TO_SECURE.add("wifi_watchdog_acceptable_packet_loss_percentage");
            MOVED_TO_SECURE.add("wifi_watchdog_ap_count");
            MOVED_TO_SECURE.add("wifi_watchdog_background_check_delay_ms");
            MOVED_TO_SECURE.add("wifi_watchdog_background_check_enabled");
            MOVED_TO_SECURE.add("wifi_watchdog_background_check_timeout_ms");
            MOVED_TO_SECURE.add("wifi_watchdog_initial_ignored_ping_count");
            MOVED_TO_SECURE.add("wifi_watchdog_max_ap_checks");
            MOVED_TO_SECURE.add("wifi_watchdog_on");
            MOVED_TO_SECURE.add("wifi_watchdog_ping_count");
            MOVED_TO_SECURE.add("wifi_watchdog_ping_delay_ms");
            MOVED_TO_SECURE.add("wifi_watchdog_ping_timeout_ms");
        }

        public static void getConfiguration(ContentResolver contentResolver, Configuration configuration) {
            configuration.fontScale = getFloat(contentResolver, FONT_SCALE, configuration.fontScale);
            if (configuration.fontScale < 0.0f) {
                configuration.fontScale = 1.0f;
            }
        }

        public static float getFloat(ContentResolver contentResolver, String str) throws SettingNotFoundException {
            try {
                return Float.parseFloat(getString(contentResolver, str));
            } catch (NumberFormatException e) {
                throw new SettingNotFoundException(str);
            }
        }

        public static float getFloat(ContentResolver contentResolver, String str, float f) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return f;
            }
            try {
                return Float.parseFloat(string);
            } catch (NumberFormatException e) {
                return f;
            }
        }

        public static int getInt(ContentResolver contentResolver, String str) throws SettingNotFoundException {
            try {
                return Integer.parseInt(getString(contentResolver, str));
            } catch (NumberFormatException e) {
                throw new SettingNotFoundException(str);
            }
        }

        public static int getInt(ContentResolver contentResolver, String str, int i) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return i;
            }
            try {
                return Integer.parseInt(string);
            } catch (NumberFormatException e) {
                return i;
            }
        }

        public static long getLong(ContentResolver contentResolver, String str) throws SettingNotFoundException {
            try {
                return Long.parseLong(getString(contentResolver, str));
            } catch (NumberFormatException e) {
                throw new SettingNotFoundException(str);
            }
        }

        public static long getLong(ContentResolver contentResolver, String str, long j) {
            String string = getString(contentResolver, str);
            if (string == null) {
                return j;
            }
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException e) {
                return j;
            }
        }

        public static boolean getShowGTalkServiceStatus(ContentResolver contentResolver) {
            return getInt(contentResolver, SHOW_GTALK_SERVICE_STATUS, 0) != 0;
        }

        public static synchronized String getString(ContentResolver contentResolver, String str) {
            String string;
            synchronized (System.class) {
                if (MOVED_TO_SECURE.contains(str)) {
                    Log.w(Settings.TAG, "Setting " + str + " has moved from android.provider.Settings.System" + " to android.provider.Settings.Secure, returning read-only value.");
                    string = Secure.getString(contentResolver, str);
                } else {
                    if (mNameValueCache == null) {
                        mNameValueCache = new NameValueCache(SYS_PROP_SETTING_VERSION, CONTENT_URI);
                    }
                    string = mNameValueCache.getString(contentResolver, str);
                }
            }
            return string;
        }

        public static Uri getUriFor(String str) {
            if (!MOVED_TO_SECURE.contains(str)) {
                return getUriFor(CONTENT_URI, str);
            }
            Log.w(Settings.TAG, "Setting " + str + " has moved from android.provider.Settings.System" + " to android.provider.Settings.Secure, returning Secure URI.");
            return Secure.getUriFor(Secure.CONTENT_URI, str);
        }

        public static boolean putConfiguration(ContentResolver contentResolver, Configuration configuration) {
            return putFloat(contentResolver, FONT_SCALE, configuration.fontScale);
        }

        public static boolean putFloat(ContentResolver contentResolver, String str, float f) {
            return putString(contentResolver, str, Float.toString(f));
        }

        public static boolean putInt(ContentResolver contentResolver, String str, int i) {
            return putString(contentResolver, str, Integer.toString(i));
        }

        public static boolean putLong(ContentResolver contentResolver, String str, long j) {
            return putString(contentResolver, str, Long.toString(j));
        }

        public static boolean putString(ContentResolver contentResolver, String str, String str2) {
            if (!MOVED_TO_SECURE.contains(str)) {
                return putString(contentResolver, CONTENT_URI, str, str2);
            }
            Log.w(Settings.TAG, "Setting " + str + " has moved from android.provider.Settings.System" + " to android.provider.Settings.Secure, value is unchanged.");
            return false;
        }

        public static void setShowGTalkServiceStatus(ContentResolver contentResolver, boolean z) {
            putInt(contentResolver, SHOW_GTALK_SERVICE_STATUS, z ? 1 : 0);
        }
    }

    public static String getGTalkDeviceId(long j) {
        return "android-" + Long.toHexString(j);
    }
}
