package com.android.provider;

import android.content.Context;
import android.net.Uri;
import android.provider.BaseColumns;

public final class DrmStore {
    private static final String ACCESS_DRM_PERMISSION = "android.permission.ACCESS_DRM";
    public static final String AUTHORITY = "drm";
    private static final String TAG = "DrmStore";

    public interface Audio extends Columns {
        public static final Uri CONTENT_URI = Uri.parse("content://drm/audio");
    }

    public interface Columns extends BaseColumns {
        public static final String DATA = "_data";
        public static final String MIME_TYPE = "mime_type";
        public static final String SIZE = "_size";
        public static final String TITLE = "title";
    }

    public interface Images extends Columns {
        public static final Uri CONTENT_URI = Uri.parse("content://drm/images");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003d A[SYNTHETIC, Splitter:B:21:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0050 A[SYNTHETIC, Splitter:B:29:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.content.Intent addDrmFile(android.content.ContentResolver r8, java.io.File r9, java.lang.String r10) {
        /*
            r6 = 0
            java.lang.String r5 = "IOException in DrmStore.addDrmFile()"
            java.lang.String r4 = "DrmStore"
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0032, all -> 0x004c }
            r0.<init>(r9)     // Catch:{ Exception -> 0x0032, all -> 0x004c }
            if (r10 != 0) goto L_0x0069
            java.lang.String r1 = r9.getName()     // Catch:{ Exception -> 0x0064, all -> 0x005d }
            r2 = 46
            int r2 = r1.lastIndexOf(r2)     // Catch:{ Exception -> 0x0064, all -> 0x005d }
            if (r2 <= 0) goto L_0x001d
            r3 = 0
            java.lang.String r1 = r1.substring(r3, r2)     // Catch:{ Exception -> 0x0064, all -> 0x005d }
        L_0x001d:
            android.content.Intent r1 = addDrmFile(r8, r0, r1)     // Catch:{ Exception -> 0x0064, all -> 0x005d }
            if (r0 == 0) goto L_0x0026
            r0.close()     // Catch:{ IOException -> 0x0028 }
        L_0x0026:
            r0 = r1
        L_0x0027:
            return r0
        L_0x0028:
            r0 = move-exception
            java.lang.String r2 = "DrmStore"
            java.lang.String r2 = "IOException in DrmStore.addDrmFile()"
            android.util.Log.e(r4, r5, r0)
            r0 = r1
            goto L_0x0027
        L_0x0032:
            r0 = move-exception
            r1 = r6
        L_0x0034:
            java.lang.String r2 = "DrmStore"
            java.lang.String r3 = "pushing file failed"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ IOException -> 0x0042 }
        L_0x0040:
            r0 = r6
            goto L_0x0027
        L_0x0042:
            r0 = move-exception
            java.lang.String r1 = "DrmStore"
            java.lang.String r1 = "IOException in DrmStore.addDrmFile()"
            android.util.Log.e(r4, r5, r0)
            r0 = r6
            goto L_0x0027
        L_0x004c:
            r0 = move-exception
            r1 = r6
        L_0x004e:
            if (r1 == 0) goto L_0x0053
            r1.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r0
        L_0x0054:
            r1 = move-exception
            java.lang.String r2 = "DrmStore"
            java.lang.String r2 = "IOException in DrmStore.addDrmFile()"
            android.util.Log.e(r4, r5, r1)
            goto L_0x0053
        L_0x005d:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x004e
        L_0x0062:
            r0 = move-exception
            goto L_0x004e
        L_0x0064:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0034
        L_0x0069:
            r1 = r10
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.provider.DrmStore.addDrmFile(android.content.ContentResolver, java.io.File, java.lang.String):android.content.Intent");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00dc, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00dd, code lost:
        r1 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00e7, code lost:
        r2 = r3;
        r10 = r0;
        r0 = r1;
        r1 = r10;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070 A[SYNTHETIC, Splitter:B:21:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0075 A[Catch:{ IOException -> 0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00aa A[SYNTHETIC, Splitter:B:39:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00af A[Catch:{ IOException -> 0x00b3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ca A[SYNTHETIC, Splitter:B:50:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00cf A[Catch:{ IOException -> 0x00d3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00dc A[ExcHandler: all (th java.lang.Throwable), Splitter:B:10:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.content.Intent addDrmFile(android.content.ContentResolver r11, java.io.FileInputStream r12, java.lang.String r13) {
        /*
            r8 = 0
            java.lang.String r9 = "IOException in DrmStore.addDrmFile()"
            java.lang.String r7 = "DrmStore"
            com.android.drm.mobile1.DrmRawContent r0 = new com.android.drm.mobile1.DrmRawContent     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            int r1 = r12.available()     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r2 = "application/vnd.oma.drm.message"
            r0.<init>(r12, r1, r2)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r1 = r0.getContentType()     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            com.android.drm.mobile1.DrmRightsManager r2 = com.android.drm.mobile1.DrmRightsManager.getInstance()     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            com.android.drm.mobile1.DrmRights r2 = r2.queryRights(r0)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.io.InputStream r0 = r0.getContentInputStream(r2)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            int r2 = r0.available()     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r4 = "audio/"
            boolean r4 = r1.startsWith(r4)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            if (r4 == 0) goto L_0x007a
            android.net.Uri r4 = com.android.provider.DrmStore.Audio.CONTENT_URI     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
        L_0x002f:
            if (r4 == 0) goto L_0x00ed
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            r6 = 3
            r5.<init>(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r6 = "title"
            r5.put(r6, r13)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r6 = "_size"
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            r5.put(r6, r2)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r2 = "mime_type"
            r5.put(r2, r1)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            android.net.Uri r2 = r11.insert(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            if (r2 == 0) goto L_0x00ed
            java.io.OutputStream r3 = r11.openOutputStream(r2)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            r4 = 1000(0x3e8, float:1.401E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x0064, all -> 0x00dc }
        L_0x0058:
            int r5 = r0.read(r4)     // Catch:{ Exception -> 0x0064, all -> 0x00dc }
            r6 = -1
            if (r5 == r6) goto L_0x009f
            r6 = 0
            r3.write(r4, r6, r5)     // Catch:{ Exception -> 0x0064, all -> 0x00dc }
            goto L_0x0058
        L_0x0064:
            r0 = move-exception
            r1 = r8
            r2 = r3
        L_0x0067:
            java.lang.String r3 = "DrmStore"
            java.lang.String r4 = "pushing file failed"
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x00df }
            if (r12 == 0) goto L_0x0073
            r12.close()     // Catch:{ IOException -> 0x00bc }
        L_0x0073:
            if (r2 == 0) goto L_0x0078
            r2.close()     // Catch:{ IOException -> 0x00bc }
        L_0x0078:
            r0 = r1
        L_0x0079:
            return r0
        L_0x007a:
            java.lang.String r4 = "image/"
            boolean r4 = r1.startsWith(r4)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            if (r4 == 0) goto L_0x0085
            android.net.Uri r4 = com.android.provider.DrmStore.Images.CONTENT_URI     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            goto L_0x002f
        L_0x0085:
            java.lang.String r4 = "DrmStore"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            r5.<init>()     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r6 = "unsupported mime type "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.StringBuilder r5 = r5.append(r1)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            android.util.Log.w(r4, r5)     // Catch:{ Exception -> 0x00e2, all -> 0x00c6 }
            r4 = r8
            goto L_0x002f
        L_0x009f:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x0064, all -> 0x00dc }
            r0.<init>()     // Catch:{ Exception -> 0x0064, all -> 0x00dc }
            r0.setDataAndType(r2, r1)     // Catch:{ Exception -> 0x00e6, all -> 0x00dc }
            r1 = r3
        L_0x00a8:
            if (r12 == 0) goto L_0x00ad
            r12.close()     // Catch:{ IOException -> 0x00b3 }
        L_0x00ad:
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ IOException -> 0x00b3 }
            goto L_0x0079
        L_0x00b3:
            r1 = move-exception
            java.lang.String r2 = "DrmStore"
            java.lang.String r2 = "IOException in DrmStore.addDrmFile()"
            android.util.Log.e(r7, r9, r1)
            goto L_0x0079
        L_0x00bc:
            r0 = move-exception
            java.lang.String r2 = "DrmStore"
            java.lang.String r2 = "IOException in DrmStore.addDrmFile()"
            android.util.Log.e(r7, r9, r0)
            r0 = r1
            goto L_0x0079
        L_0x00c6:
            r0 = move-exception
            r1 = r8
        L_0x00c8:
            if (r12 == 0) goto L_0x00cd
            r12.close()     // Catch:{ IOException -> 0x00d3 }
        L_0x00cd:
            if (r1 == 0) goto L_0x00d2
            r1.close()     // Catch:{ IOException -> 0x00d3 }
        L_0x00d2:
            throw r0
        L_0x00d3:
            r1 = move-exception
            java.lang.String r2 = "DrmStore"
            java.lang.String r2 = "IOException in DrmStore.addDrmFile()"
            android.util.Log.e(r7, r9, r1)
            goto L_0x00d2
        L_0x00dc:
            r0 = move-exception
            r1 = r3
            goto L_0x00c8
        L_0x00df:
            r0 = move-exception
            r1 = r2
            goto L_0x00c8
        L_0x00e2:
            r0 = move-exception
            r1 = r8
            r2 = r8
            goto L_0x0067
        L_0x00e6:
            r1 = move-exception
            r2 = r3
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0067
        L_0x00ed:
            r0 = r8
            r1 = r8
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.provider.DrmStore.addDrmFile(android.content.ContentResolver, java.io.FileInputStream, java.lang.String):android.content.Intent");
    }

    public static void enforceAccessDrmPermission(Context context) {
        if (context.checkCallingOrSelfPermission(ACCESS_DRM_PERMISSION) != 0) {
            throw new SecurityException("Requires DRM permission");
        }
    }
}
