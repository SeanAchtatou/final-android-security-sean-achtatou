package com.android.mms;

public final class ResolutionException extends ContentRestrictionException {
    private static final long serialVersionUID = 5509925632215500520L;

    public ResolutionException() {
    }

    public ResolutionException(String str) {
        super(str);
    }
}
