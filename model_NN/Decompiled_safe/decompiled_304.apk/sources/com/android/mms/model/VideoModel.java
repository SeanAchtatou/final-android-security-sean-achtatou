package com.android.mms.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.android.mms.ContentRestrictionException;
import com.android.mms.dom.smil.SmilMediaElementImpl;
import com.android.mms.drm.DrmWrapper;
import com.android.mms.model.MediaModel;
import com.android.provider.DrmStore;
import com.google.android.mms.MmsException;
import java.io.IOException;
import org.w3c.dom.events.Event;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.util.SqliteWrapper;

public class VideoModel extends RegionMediaModel {
    private static final boolean DEBUG = true;
    private static final boolean LOCAL_LOGV = true;
    private static final String TAG = "Mms/media";

    public VideoModel(Context context, Uri uri, RegionModel regionModel) throws MmsException {
        this(context, (String) null, (String) null, uri, regionModel);
        initModelFromUri(uri);
        checkContentRestriction();
    }

    public VideoModel(Context context, String str, String str2, Uri uri, RegionModel regionModel) throws MmsException {
        super(context, SmilHelper.ELEMENT_TAG_VIDEO, str, str2, uri, regionModel);
    }

    public VideoModel(Context context, String str, String str2, DrmWrapper drmWrapper, RegionModel regionModel) throws IOException {
        super(context, SmilHelper.ELEMENT_TAG_VIDEO, str, str2, drmWrapper, regionModel);
    }

    private void initModelFromUri(Uri uri) throws MmsException {
        Cursor query = SqliteWrapper.query(this.mContext, this.mContext.getContentResolver(), uri, null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndexOrThrow("_data"));
                    this.mSrc = string.substring(string.lastIndexOf(47) + 1);
                    this.mContentType = query.getString(query.getColumnIndexOrThrow(DrmStore.Columns.MIME_TYPE));
                    if (TextUtils.isEmpty(this.mContentType)) {
                        throw new MmsException("Type of media is unknown.");
                    }
                    if (Log.isLoggable(LogTag.APP, 2)) {
                        Log.v(TAG, "New VideoModel created: mSrc=" + this.mSrc + " mContentType=" + this.mContentType + " mUri=" + uri);
                    }
                    return;
                }
                throw new MmsException("Nothing found: " + uri);
            } finally {
                query.close();
            }
        } else {
            throw new MmsException("Bad URI: " + uri);
        }
    }

    /* access modifiers changed from: protected */
    public void checkContentRestriction() throws ContentRestrictionException {
        ContentRestrictionFactory.getContentRestriction().checkVideoContentType(this.mContentType);
    }

    public void handleEvent(Event event) {
        MediaModel.MediaAction mediaAction;
        String type = event.getType();
        Log.v(TAG, "[VideoModel] handleEvent " + event.getType() + " on " + this);
        MediaModel.MediaAction mediaAction2 = MediaModel.MediaAction.NO_ACTIVE_ACTION;
        if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_START_EVENT)) {
            mediaAction = MediaModel.MediaAction.START;
            pauseMusicPlayer();
            this.mVisible = true;
        } else if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_END_EVENT)) {
            mediaAction = MediaModel.MediaAction.STOP;
            if (this.mFill != 1) {
                this.mVisible = false;
            }
        } else if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_PAUSE_EVENT)) {
            mediaAction = MediaModel.MediaAction.PAUSE;
            this.mVisible = true;
        } else if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_SEEK_EVENT)) {
            mediaAction = MediaModel.MediaAction.SEEK;
            this.mSeekTo = event.getSeekTo();
            this.mVisible = true;
        } else {
            mediaAction = mediaAction2;
        }
        appendAction(mediaAction);
        notifyModelChanged(false);
    }

    /* access modifiers changed from: protected */
    public boolean isPlayable() {
        return true;
    }
}
