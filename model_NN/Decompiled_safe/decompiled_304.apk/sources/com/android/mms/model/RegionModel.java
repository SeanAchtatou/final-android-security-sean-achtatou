package com.android.mms.model;

public class RegionModel extends Model {
    private static final String DEFAULT_FIT = "meet";
    private String mBackgroundColor;
    private String mFit;
    private int mHeight;
    private int mLeft;
    private final String mRegionId;
    private int mTop;
    private int mWidth;

    public RegionModel(String str, int i, int i2, int i3, int i4) {
        this(str, DEFAULT_FIT, i, i2, i3, i4);
    }

    public RegionModel(String str, String str2, int i, int i2, int i3, int i4) {
        this(str, str2, i, i2, i3, i4, null);
    }

    public RegionModel(String str, String str2, int i, int i2, int i3, int i4, String str3) {
        this.mRegionId = str;
        this.mFit = str2;
        this.mLeft = i;
        this.mTop = i2;
        this.mWidth = i3;
        this.mHeight = i4;
        this.mBackgroundColor = str3;
    }

    public String getBackgroundColor() {
        return this.mBackgroundColor;
    }

    public String getFit() {
        return this.mFit;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getLeft() {
        return this.mLeft;
    }

    public String getRegionId() {
        return this.mRegionId;
    }

    public int getTop() {
        return this.mTop;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public void setBackgroundColor(String str) {
        this.mBackgroundColor = str;
        notifyModelChanged(true);
    }

    public void setFit(String str) {
        this.mFit = str;
        notifyModelChanged(true);
    }

    public void setHeight(int i) {
        this.mHeight = i;
        notifyModelChanged(true);
    }

    public void setLeft(int i) {
        this.mLeft = i;
        notifyModelChanged(true);
    }

    public void setTop(int i) {
        this.mTop = i;
        notifyModelChanged(true);
    }

    public void setWidth(int i) {
        this.mWidth = i;
        notifyModelChanged(true);
    }
}
