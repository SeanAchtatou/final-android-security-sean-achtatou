package com.android.mms.model;

import android.content.ContentResolver;
import com.android.mms.ContentRestrictionException;

public interface ContentRestriction {
    void checkAudioContentType(String str) throws ContentRestrictionException;

    void checkImageContentType(String str) throws ContentRestrictionException;

    void checkMessageSize(int i, int i2, ContentResolver contentResolver) throws ContentRestrictionException;

    void checkResolution(int i, int i2) throws ContentRestrictionException;

    void checkVideoContentType(String str) throws ContentRestrictionException;
}
