package com.android.mms.model;

import android.content.Context;
import android.util.Log;
import com.android.drm.mobile1.DrmException;
import com.android.mms.dom.smil.SmilMediaElementImpl;
import com.android.mms.drm.DrmWrapper;
import com.google.android.mms.pdu.CharacterSets;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.w3c.dom.events.Event;

public class TextModel extends RegionMediaModel {
    private static final String TAG = "Mms/text";
    private final int mCharset;
    private CharSequence mText;

    public TextModel(Context context, String str, String str2, int i, DrmWrapper drmWrapper, RegionModel regionModel) throws IOException {
        super(context, "text", str, str2, drmWrapper, regionModel);
        this.mCharset = i == 0 ? 4 : i;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TextModel(Context context, String str, String str2, int i, byte[] bArr, RegionModel regionModel) {
        super(context, "text", str, str2, bArr != null ? bArr : new byte[0], regionModel);
        this.mCharset = i == 0 ? 4 : i;
        this.mText = extractTextFromData(bArr);
    }

    public TextModel(Context context, String str, String str2, RegionModel regionModel) {
        this(context, str, str2, 106, new byte[0], regionModel);
    }

    private CharSequence extractTextFromData(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        try {
            return this.mCharset == 0 ? new String(bArr) : new String(bArr, CharacterSets.getMimeName(this.mCharset));
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Unsupported encoding: " + this.mCharset, e);
            return new String(bArr);
        }
    }

    public void cloneText() {
        this.mText = new String(this.mText.toString());
    }

    public int getCharset() {
        return this.mCharset;
    }

    public String getText() {
        if (this.mText == null) {
            try {
                this.mText = extractTextFromData(getData());
            } catch (DrmException e) {
                Log.e(TAG, e.getMessage(), e);
                this.mText = e.getMessage();
            }
        }
        if (!(this.mText instanceof String)) {
            this.mText = this.mText.toString();
        }
        return this.mText.toString();
    }

    public void handleEvent(Event event) {
        if (event.getType().equals(SmilMediaElementImpl.SMIL_MEDIA_START_EVENT)) {
            this.mVisible = true;
        } else if (this.mFill != 1) {
            this.mVisible = false;
        }
        notifyModelChanged(false);
    }

    public void setText(CharSequence charSequence) {
        this.mText = charSequence;
        notifyModelChanged(true);
    }
}
