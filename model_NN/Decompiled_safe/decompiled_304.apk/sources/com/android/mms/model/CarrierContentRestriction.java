package com.android.mms.model;

import android.content.ContentResolver;
import com.android.mms.ContentRestrictionException;
import com.android.mms.ExceedMessageSizeException;
import com.android.mms.MmsConfig;
import com.android.mms.ResolutionException;
import com.android.mms.UnsupportContentTypeException;
import com.google.android.mms.ContentType;
import java.util.ArrayList;

public class CarrierContentRestriction implements ContentRestriction {
    private static final ArrayList<String> sSupportedAudioTypes = ContentType.getAudioTypes();
    private static final ArrayList<String> sSupportedImageTypes = ContentType.getImageTypes();
    private static final ArrayList<String> sSupportedVideoTypes = ContentType.getVideoTypes();

    public void checkAudioContentType(String str) throws ContentRestrictionException {
        if (str == null) {
            throw new ContentRestrictionException("Null content type to be check");
        } else if (!sSupportedAudioTypes.contains(str)) {
            throw new UnsupportContentTypeException("Unsupported audio content type : " + str);
        }
    }

    public void checkImageContentType(String str) throws ContentRestrictionException {
        if (str == null) {
            throw new ContentRestrictionException("Null content type to be check");
        } else if (!sSupportedImageTypes.contains(str)) {
            throw new UnsupportContentTypeException("Unsupported image content type : " + str);
        }
    }

    public void checkMessageSize(int i, int i2, ContentResolver contentResolver) throws ContentRestrictionException {
        if (i < 0 || i2 < 0) {
            throw new ContentRestrictionException("Negative message size or increase size");
        }
        int i3 = i + i2;
        if (i3 < 0 || i3 > MmsConfig.getMaxMessageSize()) {
            throw new ExceedMessageSizeException("Exceed message size limitation");
        }
    }

    public void checkResolution(int i, int i2) throws ContentRestrictionException {
        if (i > MmsConfig.getMaxImageWidth() || i2 > MmsConfig.getMaxImageHeight()) {
            throw new ResolutionException("content resolution exceeds restriction.");
        }
    }

    public void checkVideoContentType(String str) throws ContentRestrictionException {
        if (str == null) {
            throw new ContentRestrictionException("Null content type to be check");
        } else if (!sSupportedVideoTypes.contains(str)) {
            throw new UnsupportContentTypeException("Unsupported video content type : " + str);
        }
    }
}
