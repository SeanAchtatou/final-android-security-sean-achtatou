package com.android.mms.model;

import android.text.TextUtils;
import android.util.Log;
import com.android.mms.dom.smil.SmilParElementImpl;
import com.google.android.mms.ContentType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;

public class SlideModel extends Model implements List<MediaModel>, EventListener {
    private static final boolean DEBUG = false;
    private static final int DEFAULT_SLIDE_DURATION = 5000;
    private static final boolean LOCAL_LOGV = false;
    public static final String TAG = "Mms/slideshow";
    private MediaModel mAudio;
    private boolean mCanAddAudio;
    private boolean mCanAddImage;
    private boolean mCanAddVideo;
    private int mDuration;
    private short mFill;
    private MediaModel mImage;
    private final ArrayList<MediaModel> mMedia;
    private SlideshowModel mParent;
    private int mSlideSize;
    private MediaModel mText;
    private MediaModel mVideo;
    private boolean mVisible;

    public SlideModel(int i, SlideshowModel slideshowModel) {
        this.mMedia = new ArrayList<>();
        this.mCanAddImage = true;
        this.mCanAddAudio = true;
        this.mCanAddVideo = true;
        this.mVisible = true;
        this.mDuration = i;
        this.mParent = slideshowModel;
    }

    public SlideModel(int i, ArrayList<MediaModel> arrayList) {
        this.mMedia = new ArrayList<>();
        this.mCanAddImage = true;
        this.mCanAddAudio = true;
        this.mCanAddVideo = true;
        this.mVisible = true;
        this.mDuration = i;
        int i2 = 0;
        Iterator<MediaModel> it = arrayList.iterator();
        while (true) {
            int i3 = i2;
            if (it.hasNext()) {
                MediaModel next = it.next();
                internalAdd(next);
                i2 = next.getDuration();
                if (i2 <= i3) {
                    i2 = i3;
                }
            } else {
                updateDuration(i3);
                return;
            }
        }
    }

    public SlideModel(SlideshowModel slideshowModel) {
        this(5000, slideshowModel);
    }

    private void internalAdd(MediaModel mediaModel) throws IllegalStateException {
        if (mediaModel != null) {
            if (mediaModel.isText()) {
                String contentType = mediaModel.getContentType();
                if (TextUtils.isEmpty(contentType) || ContentType.TEXT_PLAIN.equals(contentType)) {
                    internalAddOrReplace(this.mText, mediaModel);
                    this.mText = mediaModel;
                    return;
                }
                Log.w(TAG, "[SlideModel] content type " + mediaModel.getContentType() + " isn't supported (as text)");
            } else if (mediaModel.isImage()) {
                if (this.mCanAddImage) {
                    internalAddOrReplace(this.mImage, mediaModel);
                    this.mImage = mediaModel;
                    this.mCanAddVideo = false;
                    return;
                }
                throw new IllegalStateException();
            } else if (mediaModel.isAudio()) {
                if (this.mCanAddAudio) {
                    internalAddOrReplace(this.mAudio, mediaModel);
                    this.mAudio = mediaModel;
                    this.mCanAddVideo = false;
                    return;
                }
                throw new IllegalStateException();
            } else if (!mediaModel.isVideo()) {
            } else {
                if (this.mCanAddVideo) {
                    internalAddOrReplace(this.mVideo, mediaModel);
                    this.mVideo = mediaModel;
                    this.mCanAddImage = false;
                    this.mCanAddAudio = false;
                    return;
                }
                throw new IllegalStateException();
            }
        }
    }

    private void internalAddOrReplace(MediaModel mediaModel, MediaModel mediaModel2) {
        int mediaSize = mediaModel2.getMediaSize();
        if (mediaModel == null) {
            if (this.mParent != null) {
                this.mParent.checkMessageSize(mediaSize);
            }
            this.mMedia.add(mediaModel2);
            increaseSlideSize(mediaSize);
            increaseMessageSize(mediaSize);
        } else {
            int mediaSize2 = mediaModel.getMediaSize();
            if (mediaSize > mediaSize2) {
                if (this.mParent != null) {
                    this.mParent.checkMessageSize(mediaSize - mediaSize2);
                }
                increaseSlideSize(mediaSize - mediaSize2);
                increaseMessageSize(mediaSize - mediaSize2);
            } else {
                decreaseSlideSize(mediaSize2 - mediaSize);
                decreaseMessageSize(mediaSize2 - mediaSize);
            }
            this.mMedia.set(this.mMedia.indexOf(mediaModel), mediaModel2);
            mediaModel.unregisterAllModelChangedObservers();
        }
        Iterator it = this.mModelChangedObservers.iterator();
        while (it.hasNext()) {
            mediaModel2.registerModelChangedObserver((IModelChangedObserver) it.next());
        }
    }

    private boolean internalRemove(Object obj) {
        if (!this.mMedia.remove(obj)) {
            return false;
        }
        if (obj instanceof TextModel) {
            this.mText = null;
        } else if (obj instanceof ImageModel) {
            this.mImage = null;
            this.mCanAddVideo = true;
        } else if (obj instanceof AudioModel) {
            this.mAudio = null;
            this.mCanAddVideo = true;
        } else if (obj instanceof VideoModel) {
            this.mVideo = null;
            this.mCanAddImage = true;
            this.mCanAddAudio = true;
        }
        int mediaSize = ((MediaModel) obj).getMediaSize();
        decreaseSlideSize(mediaSize);
        decreaseMessageSize(mediaSize);
        ((Model) obj).unregisterAllModelChangedObservers();
        return true;
    }

    public void add(int i, MediaModel mediaModel) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public boolean add(MediaModel mediaModel) {
        internalAdd(mediaModel);
        notifyModelChanged(true);
        return true;
    }

    public boolean addAll(int i, Collection<? extends MediaModel> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public boolean addAll(Collection<? extends MediaModel> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public void clear() {
        if (this.mMedia.size() > 0) {
            Iterator<MediaModel> it = this.mMedia.iterator();
            while (it.hasNext()) {
                MediaModel next = it.next();
                next.unregisterAllModelChangedObservers();
                int mediaSize = next.getMediaSize();
                decreaseSlideSize(mediaSize);
                decreaseMessageSize(mediaSize);
            }
            this.mMedia.clear();
            this.mText = null;
            this.mImage = null;
            this.mAudio = null;
            this.mVideo = null;
            this.mCanAddImage = true;
            this.mCanAddAudio = true;
            this.mCanAddVideo = true;
            notifyModelChanged(true);
        }
    }

    public boolean contains(Object obj) {
        return this.mMedia.contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.mMedia.containsAll(collection);
    }

    public void decreaseMessageSize(int i) {
        if (i > 0 && this.mParent != null) {
            this.mParent.setCurrentMessageSize(this.mParent.getCurrentMessageSize() - i);
        }
    }

    public void decreaseSlideSize(int i) {
        if (i > 0) {
            this.mSlideSize -= i;
        }
    }

    public MediaModel get(int i) {
        if (this.mMedia.size() == 0) {
            return null;
        }
        return this.mMedia.get(i);
    }

    public AudioModel getAudio() {
        return (AudioModel) this.mAudio;
    }

    public int getDuration() {
        return this.mDuration;
    }

    public short getFill() {
        return this.mFill;
    }

    public ImageModel getImage() {
        return (ImageModel) this.mImage;
    }

    public int getSlideSize() {
        return this.mSlideSize;
    }

    public TextModel getText() {
        return (TextModel) this.mText;
    }

    public VideoModel getVideo() {
        return (VideoModel) this.mVideo;
    }

    public void handleEvent(Event event) {
        if (event.getType().equals(SmilParElementImpl.SMIL_SLIDE_START_EVENT)) {
            this.mVisible = true;
        } else if (this.mFill != 1) {
            this.mVisible = false;
        }
        notifyModelChanged(false);
    }

    public boolean hasAudio() {
        return this.mAudio != null;
    }

    public boolean hasImage() {
        return this.mImage != null;
    }

    public boolean hasText() {
        return this.mText != null;
    }

    public boolean hasVideo() {
        return this.mVideo != null;
    }

    public void increaseMessageSize(int i) {
        if (i > 0 && this.mParent != null) {
            this.mParent.setCurrentMessageSize(this.mParent.getCurrentMessageSize() + i);
        }
    }

    public void increaseSlideSize(int i) {
        if (i > 0) {
            this.mSlideSize += i;
        }
    }

    public int indexOf(Object obj) {
        return this.mMedia.indexOf(obj);
    }

    public boolean isEmpty() {
        return this.mMedia.isEmpty();
    }

    public boolean isVisible() {
        return this.mVisible;
    }

    public Iterator<MediaModel> iterator() {
        return this.mMedia.iterator();
    }

    public int lastIndexOf(Object obj) {
        return this.mMedia.lastIndexOf(obj);
    }

    public ListIterator<MediaModel> listIterator() {
        return this.mMedia.listIterator();
    }

    public ListIterator<MediaModel> listIterator(int i) {
        return this.mMedia.listIterator(i);
    }

    /* access modifiers changed from: protected */
    public void registerModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
        Iterator<MediaModel> it = this.mMedia.iterator();
        while (it.hasNext()) {
            it.next().registerModelChangedObserver(iModelChangedObserver);
        }
    }

    public MediaModel remove(int i) {
        MediaModel mediaModel = this.mMedia.get(i);
        if (mediaModel != null && internalRemove(mediaModel)) {
            notifyModelChanged(true);
        }
        return mediaModel;
    }

    public boolean remove(Object obj) {
        if (obj == null || !(obj instanceof MediaModel) || !internalRemove(obj)) {
            return false;
        }
        notifyModelChanged(true);
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public boolean removeAudio() {
        return remove(this.mAudio);
    }

    public boolean removeImage() {
        return remove(this.mImage);
    }

    public boolean removeText() {
        return remove(this.mText);
    }

    public boolean removeVideo() {
        return remove(this.mVideo);
    }

    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public MediaModel set(int i, MediaModel mediaModel) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public void setDuration(int i) {
        this.mDuration = i;
        notifyModelChanged(true);
    }

    public void setFill(short s) {
        this.mFill = s;
        notifyModelChanged(true);
    }

    public void setParent(SlideshowModel slideshowModel) {
        this.mParent = slideshowModel;
    }

    public void setVisible(boolean z) {
        this.mVisible = z;
        notifyModelChanged(true);
    }

    public int size() {
        return this.mMedia.size();
    }

    public List<MediaModel> subList(int i, int i2) {
        return this.mMedia.subList(i, i2);
    }

    public Object[] toArray() {
        return this.mMedia.toArray();
    }

    public <T> T[] toArray(T[] tArr) {
        return this.mMedia.toArray(tArr);
    }

    /* access modifiers changed from: protected */
    public void unregisterAllModelChangedObserversInDescendants() {
        Iterator<MediaModel> it = this.mMedia.iterator();
        while (it.hasNext()) {
            it.next().unregisterAllModelChangedObservers();
        }
    }

    /* access modifiers changed from: protected */
    public void unregisterModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
        Iterator<MediaModel> it = this.mMedia.iterator();
        while (it.hasNext()) {
            it.next().unregisterModelChangedObserver(iModelChangedObserver);
        }
    }

    public void updateDuration(int i) {
        if (i > 0) {
            if (i > this.mDuration || this.mDuration == 5000) {
                this.mDuration = i;
            }
        }
    }
}
