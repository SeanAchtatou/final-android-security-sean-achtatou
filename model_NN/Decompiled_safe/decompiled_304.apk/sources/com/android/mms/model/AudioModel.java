package com.android.mms.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.android.mms.ContentRestrictionException;
import com.android.mms.dom.smil.SmilMediaElementImpl;
import com.android.mms.drm.DrmWrapper;
import com.android.mms.model.MediaModel;
import com.android.provider.DrmStore;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.events.Event;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.util.SqliteWrapper;

public class AudioModel extends MediaModel {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "Mms/media";
    private final HashMap<String, String> mExtras;

    public AudioModel(Context context, Uri uri) throws MmsException {
        this(context, (String) null, (String) null, uri);
        initModelFromUri(uri);
        checkContentRestriction();
    }

    public AudioModel(Context context, String str, String str2, Uri uri) throws MmsException {
        super(context, SmilHelper.ELEMENT_TAG_AUDIO, str, str2, uri);
        this.mExtras = new HashMap<>();
    }

    public AudioModel(Context context, String str, String str2, DrmWrapper drmWrapper) throws IOException {
        super(context, SmilHelper.ELEMENT_TAG_AUDIO, str, str2, drmWrapper);
        this.mExtras = new HashMap<>();
    }

    private void initModelFromUri(Uri uri) throws MmsException {
        String string;
        Cursor query = SqliteWrapper.query(this.mContext, this.mContext.getContentResolver(), uri, null, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    if (isMmsUri(uri)) {
                        string = query.getString(query.getColumnIndexOrThrow("_data"));
                        this.mContentType = query.getString(query.getColumnIndexOrThrow(Telephony.Mms.Part.CONTENT_TYPE));
                    } else {
                        string = query.getString(query.getColumnIndexOrThrow("_data"));
                        this.mContentType = query.getString(query.getColumnIndexOrThrow(DrmStore.Columns.MIME_TYPE));
                        String string2 = query.getString(query.getColumnIndexOrThrow(MMHttpDefines.TAG_ALBUM));
                        if (!TextUtils.isEmpty(string2)) {
                            this.mExtras.put(MMHttpDefines.TAG_ALBUM, string2);
                        }
                        String string3 = query.getString(query.getColumnIndexOrThrow("artist"));
                        if (!TextUtils.isEmpty(string3)) {
                            this.mExtras.put("artist", string3);
                        }
                    }
                    this.mSrc = string.substring(string.lastIndexOf(47) + 1);
                    if (TextUtils.isEmpty(this.mContentType)) {
                        throw new MmsException("Type of media is unknown.");
                    }
                    return;
                }
                throw new MmsException("Nothing found: " + uri);
            } finally {
                query.close();
            }
        } else {
            throw new MmsException("Bad URI: " + uri);
        }
    }

    /* access modifiers changed from: protected */
    public void checkContentRestriction() throws ContentRestrictionException {
        ContentRestrictionFactory.getContentRestriction().checkAudioContentType(this.mContentType);
    }

    public Map<String, ?> getExtras() {
        return this.mExtras;
    }

    public void handleEvent(Event event) {
        MediaModel.MediaAction mediaAction;
        String type = event.getType();
        MediaModel.MediaAction mediaAction2 = MediaModel.MediaAction.NO_ACTIVE_ACTION;
        if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_START_EVENT)) {
            mediaAction = MediaModel.MediaAction.START;
            pauseMusicPlayer();
        } else if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_END_EVENT)) {
            mediaAction = MediaModel.MediaAction.STOP;
        } else if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_PAUSE_EVENT)) {
            mediaAction = MediaModel.MediaAction.PAUSE;
        } else if (type.equals(SmilMediaElementImpl.SMIL_MEDIA_SEEK_EVENT)) {
            mediaAction = MediaModel.MediaAction.SEEK;
            this.mSeekTo = event.getSeekTo();
        } else {
            mediaAction = mediaAction2;
        }
        appendAction(mediaAction);
        notifyModelChanged(false);
    }

    /* access modifiers changed from: protected */
    public boolean isPlayable() {
        return true;
    }

    public void stop() {
        appendAction(MediaModel.MediaAction.STOP);
        notifyModelChanged(false);
    }
}
