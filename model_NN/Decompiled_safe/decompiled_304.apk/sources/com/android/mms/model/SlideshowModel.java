package com.android.mms.model;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.android.drm.mobile1.DrmException;
import com.android.mms.ContentRestrictionException;
import com.android.mms.dom.smil.parser.SmilXmlSerializer;
import com.android.mms.drm.DrmWrapper;
import com.android.mms.layout.LayoutManager;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.GenericPdu;
import com.google.android.mms.pdu.MultimediaMessagePdu;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduPart;
import com.google.android.mms.pdu.PduPersister;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILLayoutElement;
import org.w3c.dom.smil.SMILMediaElement;
import org.w3c.dom.smil.SMILParElement;
import org.w3c.dom.smil.SMILRegionElement;
import org.w3c.dom.smil.SMILRootLayoutElement;
import vc.lx.sms2.R;

public class SlideshowModel extends Model implements List<SlideModel>, IModelChangedObserver {
    private static final String TAG = "Mms/slideshow";
    private ContentResolver mContentResolver;
    private int mCurrentMessageSize;
    private SMILDocument mDocumentCache;
    private final LayoutModel mLayout;
    private PduBody mPduBodyCache;
    private final ArrayList<SlideModel> mSlides;

    private SlideshowModel(ContentResolver contentResolver) {
        this.mLayout = new LayoutModel();
        this.mSlides = new ArrayList<>();
        this.mContentResolver = contentResolver;
    }

    private SlideshowModel(LayoutModel layoutModel, ArrayList<SlideModel> arrayList, SMILDocument sMILDocument, PduBody pduBody, ContentResolver contentResolver) {
        this.mLayout = layoutModel;
        this.mSlides = arrayList;
        this.mContentResolver = contentResolver;
        this.mDocumentCache = sMILDocument;
        this.mPduBodyCache = pduBody;
        Iterator<SlideModel> it = this.mSlides.iterator();
        while (it.hasNext()) {
            SlideModel next = it.next();
            increaseMessageSize(next.getSlideSize());
            next.setParent(this);
        }
    }

    public static SlideshowModel createFromMessageUri(Context context, Uri uri) throws MmsException {
        return createFromPduBody(context, getPduBody(context, uri));
    }

    public static SlideshowModel createFromPduBody(Context context, PduBody pduBody) throws MmsException {
        SMILDocument document = SmilHelper.getDocument(pduBody);
        SMILLayoutElement layout = document.getLayout();
        SMILRootLayoutElement rootLayout = layout.getRootLayout();
        int width = rootLayout.getWidth();
        int height = rootLayout.getHeight();
        if (width == 0 || height == 0) {
            width = LayoutManager.getInstance().getLayoutParameters().getWidth();
            height = LayoutManager.getInstance().getLayoutParameters().getHeight();
            rootLayout.setWidth(width);
            rootLayout.setHeight(height);
        }
        RegionModel regionModel = new RegionModel(null, 0, 0, width, height);
        ArrayList arrayList = new ArrayList();
        NodeList regions = layout.getRegions();
        int length = regions.getLength();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= length) {
                break;
            }
            SMILRegionElement sMILRegionElement = (SMILRegionElement) regions.item(i2);
            arrayList.add(new RegionModel(sMILRegionElement.getId(), sMILRegionElement.getFit(), sMILRegionElement.getLeft(), sMILRegionElement.getTop(), sMILRegionElement.getWidth(), sMILRegionElement.getHeight(), sMILRegionElement.getBackgroundColor()));
            i = i2 + 1;
        }
        LayoutModel layoutModel = new LayoutModel(regionModel, arrayList);
        NodeList childNodes = document.getBody().getChildNodes();
        int length2 = childNodes.getLength();
        ArrayList arrayList2 = new ArrayList(length2);
        for (int i3 = 0; i3 < length2; i3++) {
            SMILParElement sMILParElement = (SMILParElement) childNodes.item(i3);
            NodeList childNodes2 = sMILParElement.getChildNodes();
            int length3 = childNodes2.getLength();
            ArrayList arrayList3 = new ArrayList(length3);
            for (int i4 = 0; i4 < length3; i4++) {
                SMILMediaElement sMILMediaElement = (SMILMediaElement) childNodes2.item(i4);
                try {
                    MediaModel mediaModel = MediaModelFactory.getMediaModel(context, sMILMediaElement, layoutModel, pduBody);
                    SmilHelper.addMediaElementEventListeners((EventTarget) sMILMediaElement, mediaModel);
                    arrayList3.add(mediaModel);
                } catch (DrmException e) {
                    Log.e("Mms/slideshow", e.getMessage(), e);
                } catch (IOException e2) {
                    Log.e("Mms/slideshow", e2.getMessage(), e2);
                } catch (IllegalArgumentException e3) {
                    Log.e("Mms/slideshow", e3.getMessage(), e3);
                }
            }
            SlideModel slideModel = new SlideModel((int) (sMILParElement.getDur() * 1000.0f), arrayList3);
            slideModel.setFill(sMILParElement.getFill());
            SmilHelper.addParElementEventListeners((EventTarget) sMILParElement, slideModel);
            arrayList2.add(slideModel);
        }
        SlideshowModel slideshowModel = new SlideshowModel(layoutModel, arrayList2, document, pduBody, context.getContentResolver());
        slideshowModel.registerModelChangedObserver(slideshowModel);
        return slideshowModel;
    }

    public static SlideshowModel createNew(Context context) {
        return new SlideshowModel(context.getContentResolver());
    }

    public static PduBody getPduBody(Context context, Uri uri) throws MmsException {
        GenericPdu load = PduPersister.getPduPersister(context).load(uri);
        int messageType = load.getMessageType();
        if (messageType == 128 || messageType == 132) {
            return ((MultimediaMessagePdu) load).getBody();
        }
        throw new MmsException();
    }

    private PduBody makePduBody(Context context, SMILDocument sMILDocument, boolean z) {
        SMILDocument sMILDocument2;
        PduBody pduBody = new PduBody();
        Iterator<SlideModel> it = this.mSlides.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            Iterator<MediaModel> it2 = it.next().iterator();
            boolean z3 = z2;
            while (it2.hasNext()) {
                MediaModel next = it2.next();
                if (!z || !next.isDrmProtected() || next.isAllowedToForward()) {
                    PduPart pduPart = new PduPart();
                    if (next.isText()) {
                        TextModel textModel = (TextModel) next;
                        if (!TextUtils.isEmpty(textModel.getText())) {
                            pduPart.setCharset(textModel.getCharset());
                        }
                    }
                    pduPart.setContentType(next.getContentType().getBytes());
                    String src = next.getSrc();
                    boolean startsWith = src.startsWith("cid:");
                    if (startsWith) {
                        src = src.substring("cid:".length());
                    }
                    pduPart.setContentLocation(src.getBytes());
                    if (startsWith) {
                        pduPart.setContentId(src.getBytes());
                    } else {
                        int lastIndexOf = src.lastIndexOf(".");
                        if (lastIndexOf != -1) {
                            src = src.substring(0, lastIndexOf);
                        }
                        pduPart.setContentId(src.getBytes());
                    }
                    if (next.isDrmProtected()) {
                        DrmWrapper drmObject = next.getDrmObject();
                        pduPart.setDataUri(drmObject.getOriginalUri());
                        pduPart.setData(drmObject.getOriginalData());
                    } else if (next.isText()) {
                        pduPart.setData(((TextModel) next).getText().getBytes());
                    } else if (next.isImage() || next.isVideo() || next.isAudio()) {
                        pduPart.setDataUri(next.getUri());
                    } else {
                        Log.w("Mms/slideshow", "Unsupport media: " + next);
                    }
                    pduBody.addPart(pduPart);
                } else {
                    z3 = true;
                }
            }
            z2 = z3;
        }
        if (!z2 || !z || context == null) {
            sMILDocument2 = sMILDocument;
        } else {
            Toast.makeText(context, context.getString(R.string.cannot_forward_drm_obj), 1).show();
            sMILDocument2 = SmilHelper.getDocument(pduBody);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        SmilXmlSerializer.serialize(sMILDocument2, byteArrayOutputStream);
        PduPart pduPart2 = new PduPart();
        pduPart2.setContentId("smil".getBytes());
        pduPart2.setContentLocation("smil.xml".getBytes());
        pduPart2.setContentType(ContentType.APP_SMIL.getBytes());
        pduPart2.setData(byteArrayOutputStream.toByteArray());
        pduBody.addPart(0, pduPart2);
        return pduBody;
    }

    private PduBody makePduBody(SMILDocument sMILDocument) {
        return makePduBody(null, sMILDocument, false);
    }

    public void add(int i, SlideModel slideModel) {
        if (slideModel != null) {
            int slideSize = slideModel.getSlideSize();
            checkMessageSize(slideSize);
            this.mSlides.add(i, slideModel);
            increaseMessageSize(slideSize);
            slideModel.registerModelChangedObserver(this);
            Iterator it = this.mModelChangedObservers.iterator();
            while (it.hasNext()) {
                slideModel.registerModelChangedObserver((IModelChangedObserver) it.next());
            }
            notifyModelChanged(true);
        }
    }

    public boolean add(SlideModel slideModel) {
        int slideSize = slideModel.getSlideSize();
        checkMessageSize(slideSize);
        if (slideModel == null || !this.mSlides.add(slideModel)) {
            return false;
        }
        increaseMessageSize(slideSize);
        slideModel.registerModelChangedObserver(this);
        Iterator it = this.mModelChangedObservers.iterator();
        while (it.hasNext()) {
            slideModel.registerModelChangedObserver((IModelChangedObserver) it.next());
        }
        notifyModelChanged(true);
        return true;
    }

    public boolean addAll(int i, Collection<? extends SlideModel> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public boolean addAll(Collection<? extends SlideModel> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public void checkMessageSize(int i) throws ContentRestrictionException {
        ContentRestrictionFactory.getContentRestriction().checkMessageSize(this.mCurrentMessageSize, i, this.mContentResolver);
    }

    public void clear() {
        if (this.mSlides.size() > 0) {
            Iterator<SlideModel> it = this.mSlides.iterator();
            while (it.hasNext()) {
                SlideModel next = it.next();
                next.unregisterModelChangedObserver(this);
                Iterator it2 = this.mModelChangedObservers.iterator();
                while (it2.hasNext()) {
                    next.unregisterModelChangedObserver((IModelChangedObserver) it2.next());
                }
            }
            this.mCurrentMessageSize = 0;
            this.mSlides.clear();
            notifyModelChanged(true);
        }
    }

    public boolean contains(Object obj) {
        return this.mSlides.contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.mSlides.containsAll(collection);
    }

    public void decreaseMessageSize(int i) {
        if (i > 0) {
            this.mCurrentMessageSize -= i;
        }
    }

    public SlideModel get(int i) {
        if (this.mSlides.size() == 0) {
            return null;
        }
        return this.mSlides.get(i);
    }

    public int getCurrentMessageSize() {
        return this.mCurrentMessageSize;
    }

    public LayoutModel getLayout() {
        return this.mLayout;
    }

    public void increaseMessageSize(int i) {
        if (i > 0) {
            this.mCurrentMessageSize += i;
        }
    }

    public int indexOf(Object obj) {
        return this.mSlides.indexOf(obj);
    }

    public boolean isEmpty() {
        return this.mSlides.isEmpty();
    }

    public boolean isSimple() {
        if (size() != 1) {
            return false;
        }
        SlideModel slideModel = get(0);
        if (!(slideModel.hasImage() ^ slideModel.hasVideo())) {
            return false;
        }
        return !slideModel.hasAudio();
    }

    public Iterator<SlideModel> iterator() {
        return this.mSlides.iterator();
    }

    public int lastIndexOf(Object obj) {
        return this.mSlides.lastIndexOf(obj);
    }

    public ListIterator<SlideModel> listIterator() {
        return this.mSlides.listIterator();
    }

    public ListIterator<SlideModel> listIterator(int i) {
        return this.mSlides.listIterator(i);
    }

    public PduBody makeCopy(Context context) {
        return makePduBody(context, SmilHelper.getDocument(this), true);
    }

    public void onModelChanged(Model model, boolean z) {
        if (z) {
            this.mDocumentCache = null;
            this.mPduBodyCache = null;
        }
    }

    public void prepareForSend() {
        TextModel text;
        if (size() == 1 && (text = get(0).getText()) != null) {
            text.cloneText();
        }
    }

    /* access modifiers changed from: protected */
    public void registerModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
        this.mLayout.registerModelChangedObserver(iModelChangedObserver);
        Iterator<SlideModel> it = this.mSlides.iterator();
        while (it.hasNext()) {
            it.next().registerModelChangedObserver(iModelChangedObserver);
        }
    }

    public SlideModel remove(int i) {
        SlideModel remove = this.mSlides.remove(i);
        if (remove != null) {
            decreaseMessageSize(remove.getSlideSize());
            remove.unregisterAllModelChangedObservers();
            notifyModelChanged(true);
        }
        return remove;
    }

    public boolean remove(Object obj) {
        if (obj == null || !this.mSlides.remove(obj)) {
            return false;
        }
        SlideModel slideModel = (SlideModel) obj;
        decreaseMessageSize(slideModel.getSlideSize());
        slideModel.unregisterAllModelChangedObservers();
        notifyModelChanged(true);
        return true;
    }

    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    public SlideModel set(int i, SlideModel slideModel) {
        SlideModel slideModel2 = this.mSlides.get(i);
        if (slideModel != null) {
            int slideSize = slideModel.getSlideSize();
            int slideSize2 = slideModel2 != null ? slideModel2.getSlideSize() : 0;
            if (slideSize > slideSize2) {
                checkMessageSize(slideSize - slideSize2);
                increaseMessageSize(slideSize - slideSize2);
            } else {
                decreaseMessageSize(slideSize2 - slideSize);
            }
        }
        SlideModel slideModel3 = this.mSlides.set(i, slideModel);
        if (slideModel3 != null) {
            slideModel3.unregisterAllModelChangedObservers();
        }
        if (slideModel != null) {
            slideModel.registerModelChangedObserver(this);
            Iterator it = this.mModelChangedObservers.iterator();
            while (it.hasNext()) {
                slideModel.registerModelChangedObserver((IModelChangedObserver) it.next());
            }
        }
        notifyModelChanged(true);
        return slideModel3;
    }

    public void setCurrentMessageSize(int i) {
        this.mCurrentMessageSize = i;
    }

    public int size() {
        return this.mSlides.size();
    }

    public List<SlideModel> subList(int i, int i2) {
        return this.mSlides.subList(i, i2);
    }

    public void sync(PduBody pduBody) {
        Iterator<SlideModel> it = this.mSlides.iterator();
        while (it.hasNext()) {
            Iterator<MediaModel> it2 = it.next().iterator();
            while (it2.hasNext()) {
                MediaModel next = it2.next();
                PduPart partByContentLocation = pduBody.getPartByContentLocation(next.getSrc());
                if (partByContentLocation != null) {
                    next.setUri(partByContentLocation.getDataUri());
                }
            }
        }
    }

    public Object[] toArray() {
        return this.mSlides.toArray();
    }

    public <T> T[] toArray(T[] tArr) {
        return this.mSlides.toArray(tArr);
    }

    public PduBody toPduBody() {
        if (this.mPduBodyCache == null) {
            this.mDocumentCache = SmilHelper.getDocument(this);
            this.mPduBodyCache = makePduBody(this.mDocumentCache);
        }
        return this.mPduBodyCache;
    }

    public SMILDocument toSmilDocument() {
        if (this.mDocumentCache == null) {
            this.mDocumentCache = SmilHelper.getDocument(this);
        }
        return this.mDocumentCache;
    }

    /* access modifiers changed from: protected */
    public void unregisterAllModelChangedObserversInDescendants() {
        this.mLayout.unregisterAllModelChangedObservers();
        Iterator<SlideModel> it = this.mSlides.iterator();
        while (it.hasNext()) {
            it.next().unregisterAllModelChangedObservers();
        }
    }

    /* access modifiers changed from: protected */
    public void unregisterModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
        this.mLayout.unregisterModelChangedObserver(iModelChangedObserver);
        Iterator<SlideModel> it = this.mSlides.iterator();
        while (it.hasNext()) {
            it.next().unregisterModelChangedObserver(iModelChangedObserver);
        }
    }
}
