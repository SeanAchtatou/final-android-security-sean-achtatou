package com.android.mms.model;

import android.text.TextUtils;
import android.util.Log;
import com.android.drm.mobile1.DrmException;
import com.android.mms.dom.smil.SmilDocumentImpl;
import com.android.mms.dom.smil.SmilMediaElementImpl;
import com.android.mms.dom.smil.SmilParElementImpl;
import com.android.mms.dom.smil.parser.SmilXmlParser;
import com.android.mms.drm.DrmWrapper;
import com.android.provider.Telephony;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduPart;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILElement;
import org.w3c.dom.smil.SMILLayoutElement;
import org.w3c.dom.smil.SMILMediaElement;
import org.w3c.dom.smil.SMILParElement;
import org.w3c.dom.smil.SMILRegionElement;
import org.w3c.dom.smil.SMILRegionMediaElement;
import org.w3c.dom.smil.SMILRootLayoutElement;
import org.xml.sax.SAXException;

public class SmilHelper {
    private static final boolean DEBUG = false;
    public static final String ELEMENT_TAG_AUDIO = "audio";
    public static final String ELEMENT_TAG_IMAGE = "img";
    public static final String ELEMENT_TAG_PLAYER = "player";
    public static final String ELEMENT_TAG_REF = "ref";
    public static final String ELEMENT_TAG_TEXT = "text";
    public static final String ELEMENT_TAG_VIDEO = "video";
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "Mms/smil";

    private SmilHelper() {
    }

    static void addMediaElementEventListeners(EventTarget eventTarget, MediaModel mediaModel) {
        eventTarget.addEventListener(SmilMediaElementImpl.SMIL_MEDIA_START_EVENT, mediaModel, false);
        eventTarget.addEventListener(SmilMediaElementImpl.SMIL_MEDIA_END_EVENT, mediaModel, false);
        eventTarget.addEventListener(SmilMediaElementImpl.SMIL_MEDIA_PAUSE_EVENT, mediaModel, false);
        eventTarget.addEventListener(SmilMediaElementImpl.SMIL_MEDIA_SEEK_EVENT, mediaModel, false);
    }

    public static SMILParElement addPar(SMILDocument sMILDocument) {
        SMILParElement sMILParElement = (SMILParElement) sMILDocument.createElement("par");
        sMILParElement.setDur(8.0f);
        sMILDocument.getBody().appendChild(sMILParElement);
        return sMILParElement;
    }

    static void addParElementEventListeners(EventTarget eventTarget, SlideModel slideModel) {
        eventTarget.addEventListener(SmilParElementImpl.SMIL_SLIDE_START_EVENT, slideModel, false);
        eventTarget.addEventListener(SmilParElementImpl.SMIL_SLIDE_END_EVENT, slideModel, false);
    }

    public static SMILMediaElement createMediaElement(String str, SMILDocument sMILDocument, String str2) {
        SMILMediaElement sMILMediaElement = (SMILMediaElement) sMILDocument.createElement(str);
        sMILMediaElement.setSrc(escapeXML(str2));
        return sMILMediaElement;
    }

    private static SMILDocument createSmilDocument(SlideshowModel slideshowModel) {
        SMILMediaElement createMediaElement;
        boolean z;
        boolean z2;
        SmilDocumentImpl smilDocumentImpl = new SmilDocumentImpl();
        SMILElement sMILElement = (SMILElement) smilDocumentImpl.createElement("smil");
        smilDocumentImpl.appendChild(sMILElement);
        SMILElement sMILElement2 = (SMILElement) smilDocumentImpl.createElement("head");
        sMILElement.appendChild(sMILElement2);
        SMILLayoutElement sMILLayoutElement = (SMILLayoutElement) smilDocumentImpl.createElement("layout");
        sMILElement2.appendChild(sMILLayoutElement);
        SMILRootLayoutElement sMILRootLayoutElement = (SMILRootLayoutElement) smilDocumentImpl.createElement("root-layout");
        LayoutModel layout = slideshowModel.getLayout();
        sMILRootLayoutElement.setWidth(layout.getLayoutWidth());
        sMILRootLayoutElement.setHeight(layout.getLayoutHeight());
        String backgroundColor = layout.getBackgroundColor();
        if (!TextUtils.isEmpty(backgroundColor)) {
            sMILRootLayoutElement.setBackgroundColor(backgroundColor);
        }
        sMILLayoutElement.appendChild(sMILRootLayoutElement);
        ArrayList<RegionModel> regions = layout.getRegions();
        ArrayList arrayList = new ArrayList();
        Iterator<RegionModel> it = regions.iterator();
        while (it.hasNext()) {
            RegionModel next = it.next();
            SMILRegionElement sMILRegionElement = (SMILRegionElement) smilDocumentImpl.createElement("region");
            sMILRegionElement.setId(next.getRegionId());
            sMILRegionElement.setLeft(next.getLeft());
            sMILRegionElement.setTop(next.getTop());
            sMILRegionElement.setWidth(next.getWidth());
            sMILRegionElement.setHeight(next.getHeight());
            sMILRegionElement.setFit(next.getFit());
            arrayList.add(sMILRegionElement);
        }
        sMILElement.appendChild((SMILElement) smilDocumentImpl.createElement(Telephony.TextBasedSmsColumns.BODY));
        Iterator<SlideModel> it2 = slideshowModel.iterator();
        boolean z3 = false;
        boolean z4 = false;
        while (it2.hasNext()) {
            SlideModel next2 = it2.next();
            SMILParElement addPar = addPar(smilDocumentImpl);
            addPar.setDur(((float) next2.getDuration()) / 1000.0f);
            addParElementEventListeners((EventTarget) addPar, next2);
            Iterator<MediaModel> it3 = next2.iterator();
            boolean z5 = z4;
            boolean z6 = z3;
            while (it3.hasNext()) {
                MediaModel next3 = it3.next();
                String src = next3.getSrc();
                if (next3 instanceof TextModel) {
                    if (!TextUtils.isEmpty(((TextModel) next3).getText())) {
                        SMILMediaElement createMediaElement2 = createMediaElement("text", smilDocumentImpl, src);
                        SMILMediaElement sMILMediaElement = createMediaElement2;
                        z = z6;
                        z2 = setRegion((SMILRegionMediaElement) createMediaElement2, arrayList, sMILLayoutElement, LayoutModel.TEXT_REGION_ID, z5);
                        createMediaElement = sMILMediaElement;
                    }
                } else if (next3 instanceof ImageModel) {
                    SMILMediaElement createMediaElement3 = createMediaElement("img", smilDocumentImpl, src);
                    boolean region = setRegion((SMILRegionMediaElement) createMediaElement3, arrayList, sMILLayoutElement, LayoutModel.IMAGE_REGION_ID, z6);
                    z2 = z5;
                    boolean z7 = region;
                    createMediaElement = createMediaElement3;
                    z = z7;
                } else if (next3 instanceof VideoModel) {
                    SMILMediaElement createMediaElement4 = createMediaElement(ELEMENT_TAG_VIDEO, smilDocumentImpl, src);
                    boolean region2 = setRegion((SMILRegionMediaElement) createMediaElement4, arrayList, sMILLayoutElement, LayoutModel.IMAGE_REGION_ID, z6);
                    z2 = z5;
                    boolean z8 = region2;
                    createMediaElement = createMediaElement4;
                    z = z8;
                } else if (next3 instanceof AudioModel) {
                    createMediaElement = createMediaElement(ELEMENT_TAG_AUDIO, smilDocumentImpl, src);
                    z = z6;
                    z2 = z5;
                } else {
                    Log.w(TAG, "Unsupport media: " + next3);
                }
                int begin = next3.getBegin();
                if (begin != 0) {
                    createMediaElement.setAttribute("begin", String.valueOf(begin / 1000));
                }
                int duration = next3.getDuration();
                if (duration != 0) {
                    createMediaElement.setDur(((float) duration) / 1000.0f);
                }
                addPar.appendChild(createMediaElement);
                addMediaElementEventListeners((EventTarget) createMediaElement, next3);
                z5 = z2;
                z6 = z;
            }
            z3 = z6;
            z4 = z5;
        }
        return smilDocumentImpl;
    }

    private static SMILDocument createSmilDocument(PduBody pduBody) {
        SmilDocumentImpl smilDocumentImpl = new SmilDocumentImpl();
        SMILElement sMILElement = (SMILElement) smilDocumentImpl.createElement("smil");
        sMILElement.setAttribute("xmlns", "http://www.w3.org/2001/SMIL20/Language");
        smilDocumentImpl.appendChild(sMILElement);
        SMILElement sMILElement2 = (SMILElement) smilDocumentImpl.createElement("head");
        sMILElement.appendChild(sMILElement2);
        sMILElement2.appendChild((SMILLayoutElement) smilDocumentImpl.createElement("layout"));
        sMILElement.appendChild((SMILElement) smilDocumentImpl.createElement(Telephony.TextBasedSmsColumns.BODY));
        SMILParElement addPar = addPar(smilDocumentImpl);
        int partsNum = pduBody.getPartsNum();
        if (partsNum == 0) {
            return smilDocumentImpl;
        }
        boolean z = false;
        boolean z2 = false;
        SMILParElement sMILParElement = addPar;
        for (int i = 0; i < partsNum; i++) {
            if (sMILParElement == null || (z && z2)) {
                z2 = false;
                sMILParElement = addPar(smilDocumentImpl);
                z = false;
            }
            PduPart part = pduBody.getPart(i);
            String str = new String(part.getContentType());
            if (ContentType.isDrmType(str)) {
                try {
                    str = new DrmWrapper(str, part.getDataUri(), part.getData()).getContentType();
                } catch (DrmException e) {
                    Log.e(TAG, e.getMessage(), e);
                } catch (IOException e2) {
                    Log.e(TAG, e2.getMessage(), e2);
                }
            }
            if (str.equals(ContentType.TEXT_PLAIN) || str.equalsIgnoreCase(ContentType.APP_WAP_XHTML)) {
                sMILParElement.appendChild(createMediaElement("text", smilDocumentImpl, part.generateLocation()));
                z2 = true;
            } else if (ContentType.isImageType(str)) {
                sMILParElement.appendChild(createMediaElement("img", smilDocumentImpl, part.generateLocation()));
                z = true;
            } else if (ContentType.isVideoType(str)) {
                sMILParElement.appendChild(createMediaElement(ELEMENT_TAG_VIDEO, smilDocumentImpl, part.generateLocation()));
                z = true;
            } else if (ContentType.isAudioType(str)) {
                sMILParElement.appendChild(createMediaElement(ELEMENT_TAG_AUDIO, smilDocumentImpl, part.generateLocation()));
                z = true;
            } else {
                Log.w(TAG, "unsupport media type");
            }
        }
        return smilDocumentImpl;
    }

    public static String escapeXML(String str) {
        return str.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;").replaceAll("'", "&apos;");
    }

    private static SMILRegionElement findRegionElementById(ArrayList<SMILRegionElement> arrayList, String str) {
        Iterator<SMILRegionElement> it = arrayList.iterator();
        while (it.hasNext()) {
            SMILRegionElement next = it.next();
            if (next.getId().equals(str)) {
                return next;
            }
        }
        return null;
    }

    private static PduPart findSmilPart(PduBody pduBody) {
        int partsNum = pduBody.getPartsNum();
        for (int i = 0; i < partsNum; i++) {
            PduPart part = pduBody.getPart(i);
            if (Arrays.equals(part.getContentType(), ContentType.APP_SMIL.getBytes())) {
                return part;
            }
        }
        return null;
    }

    public static SMILDocument getDocument(SlideshowModel slideshowModel) {
        return createSmilDocument(slideshowModel);
    }

    public static SMILDocument getDocument(PduBody pduBody) {
        PduPart findSmilPart = findSmilPart(pduBody);
        SMILDocument smilDocument = findSmilPart != null ? getSmilDocument(findSmilPart) : null;
        return smilDocument == null ? createSmilDocument(pduBody) : smilDocument;
    }

    private static SMILDocument getSmilDocument(PduPart pduPart) {
        try {
            byte[] data = pduPart.getData();
            if (data != null) {
                return validate(new SmilXmlParser().parse(new ByteArrayInputStream(data)));
            }
        } catch (IOException e) {
            Log.e(TAG, "Failed to parse SMIL document.", e);
        } catch (SAXException e2) {
            Log.e(TAG, "Failed to parse SMIL document.", e2);
        } catch (MmsException e3) {
            Log.e(TAG, "Failed to parse SMIL document.", e3);
        }
        return null;
    }

    private static boolean setRegion(SMILRegionMediaElement sMILRegionMediaElement, ArrayList<SMILRegionElement> arrayList, SMILLayoutElement sMILLayoutElement, String str, boolean z) {
        SMILRegionElement findRegionElementById = findRegionElementById(arrayList, str);
        if (z || findRegionElementById == null) {
            return false;
        }
        sMILRegionMediaElement.setRegion(findRegionElementById);
        sMILLayoutElement.appendChild(findRegionElementById);
        return true;
    }

    private static SMILDocument validate(SMILDocument sMILDocument) {
        return sMILDocument;
    }
}
