package com.android.mms.model;

import android.util.Log;
import com.android.mms.layout.LayoutManager;
import com.android.mms.layout.LayoutParameters;
import java.util.ArrayList;
import java.util.Iterator;

public class LayoutModel extends Model {
    private static final boolean DEBUG = false;
    public static final int DEFAULT_LAYOUT_TYPE = 0;
    public static final String IMAGE_REGION_ID = "Image";
    public static final int LAYOUT_BOTTOM_TEXT = 0;
    public static final int LAYOUT_TOP_TEXT = 1;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "Mms/slideshow";
    public static final String TEXT_REGION_ID = "Text";
    private RegionModel mImageRegion;
    private LayoutParameters mLayoutParams = LayoutManager.getInstance().getLayoutParameters();
    private int mLayoutType = 0;
    private ArrayList<RegionModel> mNonStdRegions;
    private RegionModel mRootLayout;
    private RegionModel mTextRegion;

    public LayoutModel() {
        createDefaultRootLayout();
        createDefaultImageRegion();
        createDefaultTextRegion();
    }

    public LayoutModel(RegionModel regionModel, ArrayList<RegionModel> arrayList) {
        this.mRootLayout = regionModel;
        this.mNonStdRegions = new ArrayList<>();
        Iterator<RegionModel> it = arrayList.iterator();
        while (it.hasNext()) {
            RegionModel next = it.next();
            String regionId = next.getRegionId();
            if (regionId.equals(IMAGE_REGION_ID)) {
                this.mImageRegion = next;
            } else if (regionId.equals(TEXT_REGION_ID)) {
                this.mTextRegion = next;
            } else {
                this.mNonStdRegions.add(next);
            }
        }
        validateLayouts();
    }

    private void createDefaultImageRegion() {
        if (this.mRootLayout == null) {
            throw new IllegalStateException("Root-Layout uninitialized.");
        }
        this.mImageRegion = new RegionModel(IMAGE_REGION_ID, 0, 0, this.mRootLayout.getWidth(), this.mLayoutParams.getImageHeight());
    }

    private void createDefaultRootLayout() {
        this.mRootLayout = new RegionModel(null, 0, 0, this.mLayoutParams.getWidth(), this.mLayoutParams.getHeight());
    }

    private void createDefaultTextRegion() {
        if (this.mRootLayout == null) {
            throw new IllegalStateException("Root-Layout uninitialized.");
        }
        this.mTextRegion = new RegionModel(TEXT_REGION_ID, 0, this.mLayoutParams.getImageHeight(), this.mRootLayout.getWidth(), this.mLayoutParams.getTextHeight());
    }

    private void validateLayouts() {
        if (this.mRootLayout == null) {
            createDefaultRootLayout();
        }
        if (this.mImageRegion == null) {
            createDefaultImageRegion();
        }
        if (this.mTextRegion == null) {
            createDefaultTextRegion();
        }
    }

    public void changeTo(int i) {
        if (this.mRootLayout == null) {
            throw new IllegalStateException("Root-Layout uninitialized.");
        }
        if (this.mLayoutParams == null) {
            this.mLayoutParams = LayoutManager.getInstance().getLayoutParameters();
        }
        if (this.mLayoutType != i) {
            switch (i) {
                case 0:
                    this.mImageRegion.setTop(0);
                    this.mTextRegion.setTop(this.mLayoutParams.getImageHeight());
                    this.mLayoutType = i;
                    notifyModelChanged(true);
                    return;
                case 1:
                    this.mImageRegion.setTop(this.mLayoutParams.getTextHeight());
                    this.mTextRegion.setTop(0);
                    this.mLayoutType = i;
                    notifyModelChanged(true);
                    return;
                default:
                    Log.w("Mms/slideshow", "Unknown layout type: " + i);
                    return;
            }
        }
    }

    public RegionModel findRegionById(String str) {
        if (IMAGE_REGION_ID.equals(str)) {
            return this.mImageRegion;
        }
        if (TEXT_REGION_ID.equals(str)) {
            return this.mTextRegion;
        }
        Iterator<RegionModel> it = this.mNonStdRegions.iterator();
        while (it.hasNext()) {
            RegionModel next = it.next();
            if (next.getRegionId().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public String getBackgroundColor() {
        return this.mRootLayout.getBackgroundColor();
    }

    public RegionModel getImageRegion() {
        return this.mImageRegion;
    }

    public int getLayoutHeight() {
        return this.mRootLayout.getHeight();
    }

    public int getLayoutType() {
        return this.mLayoutType;
    }

    public int getLayoutWidth() {
        return this.mRootLayout.getWidth();
    }

    public ArrayList<RegionModel> getRegions() {
        ArrayList<RegionModel> arrayList = new ArrayList<>();
        if (this.mImageRegion != null) {
            arrayList.add(this.mImageRegion);
        }
        if (this.mTextRegion != null) {
            arrayList.add(this.mTextRegion);
        }
        return arrayList;
    }

    public RegionModel getRootLayout() {
        return this.mRootLayout;
    }

    public RegionModel getTextRegion() {
        return this.mTextRegion;
    }

    /* access modifiers changed from: protected */
    public void registerModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
        if (this.mRootLayout != null) {
            this.mRootLayout.registerModelChangedObserver(iModelChangedObserver);
        }
        if (this.mImageRegion != null) {
            this.mImageRegion.registerModelChangedObserver(iModelChangedObserver);
        }
        if (this.mTextRegion != null) {
            this.mTextRegion.registerModelChangedObserver(iModelChangedObserver);
        }
    }

    public void setImageRegion(RegionModel regionModel) {
        this.mImageRegion = regionModel;
    }

    public void setRootLayout(RegionModel regionModel) {
        this.mRootLayout = regionModel;
    }

    public void setTextRegion(RegionModel regionModel) {
        this.mTextRegion = regionModel;
    }

    /* access modifiers changed from: protected */
    public void unregisterAllModelChangedObserversInDescendants() {
        if (this.mRootLayout != null) {
            this.mRootLayout.unregisterAllModelChangedObservers();
        }
        if (this.mImageRegion != null) {
            this.mImageRegion.unregisterAllModelChangedObservers();
        }
        if (this.mTextRegion != null) {
            this.mTextRegion.unregisterAllModelChangedObservers();
        }
    }

    /* access modifiers changed from: protected */
    public void unregisterModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
        if (this.mRootLayout != null) {
            this.mRootLayout.unregisterModelChangedObserver(iModelChangedObserver);
        }
        if (this.mImageRegion != null) {
            this.mImageRegion.unregisterModelChangedObserver(iModelChangedObserver);
        }
        if (this.mTextRegion != null) {
            this.mTextRegion.unregisterModelChangedObserver(iModelChangedObserver);
        }
    }
}
