package com.android.mms.model;

import java.util.ArrayList;
import java.util.Iterator;

public class Model {
    protected ArrayList<IModelChangedObserver> mModelChangedObservers = new ArrayList<>();

    /* access modifiers changed from: protected */
    public void notifyModelChanged(boolean z) {
        Iterator<IModelChangedObserver> it = this.mModelChangedObservers.iterator();
        while (it.hasNext()) {
            it.next().onModelChanged(this, z);
        }
    }

    public void registerModelChangedObserver(IModelChangedObserver iModelChangedObserver) {
        if (!this.mModelChangedObservers.contains(iModelChangedObserver)) {
            this.mModelChangedObservers.add(iModelChangedObserver);
            registerModelChangedObserverInDescendants(iModelChangedObserver);
        }
    }

    /* access modifiers changed from: protected */
    public void registerModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
    }

    public void unregisterAllModelChangedObservers() {
        unregisterAllModelChangedObserversInDescendants();
        this.mModelChangedObservers.clear();
    }

    /* access modifiers changed from: protected */
    public void unregisterAllModelChangedObserversInDescendants() {
    }

    public void unregisterModelChangedObserver(IModelChangedObserver iModelChangedObserver) {
        this.mModelChangedObservers.remove(iModelChangedObserver);
        unregisterModelChangedObserverInDescendants(iModelChangedObserver);
    }

    /* access modifiers changed from: protected */
    public void unregisterModelChangedObserverInDescendants(IModelChangedObserver iModelChangedObserver) {
    }
}
