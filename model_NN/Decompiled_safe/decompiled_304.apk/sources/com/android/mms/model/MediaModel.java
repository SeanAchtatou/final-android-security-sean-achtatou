package com.android.mms.model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.android.drm.mobile1.DrmException;
import com.android.internal.telephony.Phone;
import com.android.media.MediaMetadataRetriever;
import com.android.mms.drm.DrmUtils;
import com.android.mms.drm.DrmWrapper;
import com.google.android.mms.MmsException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.w3c.dom.events.EventListener;
import vc.lx.sms.data.LogTag;
import vc.lx.sms2.R;

public abstract class MediaModel extends Model implements EventListener {
    private static final String MUSIC_SERVICE_ACTION = "com.android.music.musicservicecommand";
    protected static final String TAG = "Mms/media";
    protected int mBegin;
    protected String mContentType;
    protected Context mContext;
    private byte[] mData;
    protected DrmWrapper mDrmObjectWrapper;
    protected int mDuration;
    protected short mFill;
    private final ArrayList<MediaAction> mMediaActions;
    protected int mSeekTo;
    protected int mSize;
    protected String mSrc;
    protected String mTag;
    private Uri mUri;

    public enum MediaAction {
        NO_ACTIVE_ACTION,
        START,
        STOP,
        PAUSE,
        SEEK
    }

    public MediaModel(Context context, String str, String str2, String str3, Uri uri) throws MmsException {
        this.mContext = context;
        this.mTag = str;
        this.mContentType = str2;
        this.mSrc = str3;
        this.mUri = uri;
        initMediaSize();
        this.mMediaActions = new ArrayList<>();
    }

    public MediaModel(Context context, String str, String str2, String str3, DrmWrapper drmWrapper) throws IOException {
        this.mContext = context;
        this.mTag = str;
        this.mContentType = str2;
        this.mSrc = str3;
        this.mDrmObjectWrapper = drmWrapper;
        this.mUri = DrmUtils.insert(context, drmWrapper);
        this.mSize = drmWrapper.getOriginalData().length;
        this.mMediaActions = new ArrayList<>();
    }

    public MediaModel(Context context, String str, String str2, String str3, byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("data may not be null.");
        }
        this.mContext = context;
        this.mTag = str;
        this.mContentType = str2;
        this.mSrc = str3;
        this.mData = bArr;
        this.mSize = bArr.length;
        this.mMediaActions = new ArrayList<>();
    }

    private void initMediaSize() throws MmsException {
        InputStream inputStream = null;
        try {
            inputStream = this.mContext.getContentResolver().openInputStream(this.mUri);
            if (inputStream instanceof FileInputStream) {
                this.mSize = (int) ((FileInputStream) inputStream).getChannel().size();
            } else {
                while (-1 != inputStream.read()) {
                    this.mSize++;
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "IOException caught while closing stream", e);
                }
            }
        } catch (IOException e2) {
            Log.e(TAG, "IOException caught while opening or reading stream", e2);
            if (e2 instanceof FileNotFoundException) {
                throw new MmsException(e2.getMessage());
            } else if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    Log.e(TAG, "IOException caught while closing stream", e3);
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    Log.e(TAG, "IOException caught while closing stream", e4);
                }
            }
            throw th;
        }
    }

    public static boolean isMmsUri(Uri uri) {
        return uri.getAuthority().startsWith(Phone.APN_TYPE_MMS);
    }

    public void appendAction(MediaAction mediaAction) {
        this.mMediaActions.add(mediaAction);
    }

    public int getBegin() {
        return this.mBegin;
    }

    public String getContentType() {
        return this.mContentType;
    }

    public MediaAction getCurrentAction() {
        return this.mMediaActions.size() == 0 ? MediaAction.NO_ACTIVE_ACTION : this.mMediaActions.remove(0);
    }

    public byte[] getData() throws DrmException {
        if (this.mData == null) {
            return null;
        }
        if (!isDrmProtected() || this.mDrmObjectWrapper.consumeRights()) {
            byte[] bArr = new byte[this.mData.length];
            System.arraycopy(this.mData, 0, bArr, 0, this.mData.length);
            return bArr;
        }
        throw new DrmException(this.mContext.getString(R.string.insufficient_drm_rights));
    }

    public DrmWrapper getDrmObject() {
        return this.mDrmObjectWrapper;
    }

    public int getDuration() {
        return this.mDuration;
    }

    public short getFill() {
        return this.mFill;
    }

    public int getMediaSize() {
        return this.mSize;
    }

    public int getSeekTo() {
        return this.mSeekTo;
    }

    public String getSrc() {
        return this.mSrc;
    }

    public String getTag() {
        return this.mTag;
    }

    public Uri getUri() {
        return this.mUri;
    }

    public Uri getUriWithDrmCheck() throws DrmException {
        if (this.mUri == null || !isDrmProtected() || this.mDrmObjectWrapper.consumeRights()) {
            return this.mUri;
        }
        throw new DrmException("Insufficient DRM rights.");
    }

    /* access modifiers changed from: protected */
    public void initMediaDuration() throws MmsException {
        if (this.mUri == null) {
            throw new IllegalArgumentException("Uri may not be null.");
        }
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setMode(1);
        int i = 0;
        try {
            mediaMetadataRetriever.setDataSource(this.mContext, this.mUri);
            String extractMetadata = mediaMetadataRetriever.extractMetadata(9);
            if (extractMetadata != null) {
                i = Integer.parseInt(extractMetadata);
            }
            this.mDuration = i;
            mediaMetadataRetriever.release();
        } catch (Exception e) {
            Log.e(TAG, "MediaMetadataRetriever failed to get duration for " + this.mUri.getPath(), e);
            throw new MmsException(e);
        } catch (Throwable th) {
            mediaMetadataRetriever.release();
            throw th;
        }
    }

    public boolean isAllowedToForward() {
        return this.mDrmObjectWrapper.isAllowedToForward();
    }

    public boolean isAudio() {
        return this.mTag.equals(SmilHelper.ELEMENT_TAG_AUDIO);
    }

    public boolean isDrmProtected() {
        return this.mDrmObjectWrapper != null;
    }

    public boolean isImage() {
        return this.mTag.equals("img");
    }

    /* access modifiers changed from: protected */
    public boolean isPlayable() {
        return false;
    }

    public boolean isText() {
        return this.mTag.equals("text");
    }

    public boolean isVideo() {
        return this.mTag.equals(SmilHelper.ELEMENT_TAG_VIDEO);
    }

    /* access modifiers changed from: protected */
    public void pauseMusicPlayer() {
        if (Log.isLoggable(LogTag.APP, 2)) {
            Log.d(TAG, "pauseMusicPlayer");
        }
        Intent intent = new Intent(MUSIC_SERVICE_ACTION);
        intent.putExtra("command", "pause");
        this.mContext.sendBroadcast(intent);
    }

    public void setBegin(int i) {
        this.mBegin = i;
        notifyModelChanged(true);
    }

    public void setDuration(int i) {
        if (!isPlayable() || i >= 0) {
            this.mDuration = i;
        }
        notifyModelChanged(true);
    }

    public void setFill(short s) {
        this.mFill = s;
        notifyModelChanged(true);
    }

    /* access modifiers changed from: package-private */
    public void setUri(Uri uri) {
        this.mUri = uri;
    }
}
