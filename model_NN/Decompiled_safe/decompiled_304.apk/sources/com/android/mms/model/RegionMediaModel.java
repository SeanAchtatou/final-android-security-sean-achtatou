package com.android.mms.model;

import android.content.Context;
import android.net.Uri;
import com.android.mms.drm.DrmWrapper;
import com.google.android.mms.MmsException;
import java.io.IOException;

public abstract class RegionMediaModel extends MediaModel {
    protected RegionModel mRegion;
    protected boolean mVisible;

    public RegionMediaModel(Context context, String str, Uri uri, RegionModel regionModel) throws MmsException {
        this(context, str, (String) null, (String) null, uri, regionModel);
    }

    public RegionMediaModel(Context context, String str, String str2, String str3, Uri uri, RegionModel regionModel) throws MmsException {
        super(context, str, str2, str3, uri);
        this.mVisible = true;
        this.mRegion = regionModel;
    }

    public RegionMediaModel(Context context, String str, String str2, String str3, DrmWrapper drmWrapper, RegionModel regionModel) throws IOException {
        super(context, str, str2, str3, drmWrapper);
        this.mVisible = true;
        this.mRegion = regionModel;
    }

    public RegionMediaModel(Context context, String str, String str2, String str3, byte[] bArr, RegionModel regionModel) {
        super(context, str, str2, str3, bArr);
        this.mVisible = true;
        this.mRegion = regionModel;
    }

    public RegionModel getRegion() {
        return this.mRegion;
    }

    public boolean isVisible() {
        return this.mVisible;
    }

    public void setRegion(RegionModel regionModel) {
        this.mRegion = regionModel;
        notifyModelChanged(true);
    }

    public void setVisible(boolean z) {
        this.mVisible = z;
    }
}
