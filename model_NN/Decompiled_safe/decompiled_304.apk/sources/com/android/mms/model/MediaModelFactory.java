package com.android.mms.model;

import android.content.Context;
import com.android.drm.mobile1.DrmException;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduPart;
import java.io.IOException;
import org.w3c.dom.smil.SMILMediaElement;
import org.w3c.dom.smil.SMILRegionElement;
import org.w3c.dom.smil.SMILRegionMediaElement;

public class MediaModelFactory {
    private static final String TAG = "Mms:media";

    private static PduPart findPart(PduBody pduBody, String str) {
        PduPart pduPart = null;
        if (str != null) {
            if (str.startsWith("cid:")) {
                pduPart = pduBody.getPartByContentId("<" + str.substring("cid:".length()) + ">");
            } else {
                pduPart = pduBody.getPartByName(str);
                if (pduPart == null && (pduPart = pduBody.getPartByFileName(str)) == null) {
                    pduPart = pduBody.getPartByContentLocation(str);
                }
            }
        }
        if (pduPart != null) {
            return pduPart;
        }
        throw new IllegalArgumentException("No part found for the model.");
    }

    /* JADX WARN: Type inference failed for: r5v15, types: [com.android.mms.model.VideoModel] */
    /* JADX WARN: Type inference failed for: r5v16, types: [com.android.mms.model.ImageModel] */
    /* JADX WARN: Type inference failed for: r5v17, types: [com.android.mms.model.TextModel] */
    /* JADX WARN: Type inference failed for: r5v18, types: [com.android.mms.model.VideoModel] */
    /* JADX WARN: Type inference failed for: r5v19, types: [com.android.mms.model.ImageModel] */
    /* JADX WARN: Type inference failed for: r5v20, types: [com.android.mms.model.TextModel] */
    /* JADX WARN: Type inference failed for: r11v2, types: [com.android.mms.model.VideoModel] */
    /* JADX WARN: Type inference failed for: r11v3, types: [com.android.mms.model.ImageModel] */
    /* JADX WARN: Type inference failed for: r5v39, types: [com.android.mms.model.TextModel] */
    /* JADX WARN: Type inference failed for: r11v5, types: [com.android.mms.model.VideoModel] */
    /* JADX WARN: Type inference failed for: r11v6, types: [com.android.mms.model.ImageModel] */
    /* JADX WARN: Type inference failed for: r5v40, types: [com.android.mms.model.TextModel] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.android.mms.model.MediaModel getGenericMediaModel(android.content.Context r17, java.lang.String r18, java.lang.String r19, org.w3c.dom.smil.SMILMediaElement r20, com.google.android.mms.pdu.PduPart r21, com.android.mms.model.RegionModel r22) throws com.android.drm.mobile1.DrmException, java.io.IOException, com.google.android.mms.MmsException {
        /*
            byte[] r5 = r21.getContentType()
            if (r5 != 0) goto L_0x000e
            java.lang.IllegalArgumentException r17 = new java.lang.IllegalArgumentException
            java.lang.String r18 = "Content-Type of the part may not be null."
            r17.<init>(r18)
            throw r17
        L_0x000e:
            java.lang.String r7 = new java.lang.String
            r7.<init>(r5)
            boolean r5 = com.google.android.mms.ContentType.isDrmType(r7)
            if (r5 == 0) goto L_0x0204
            com.android.mms.drm.DrmWrapper r10 = new com.android.mms.drm.DrmWrapper
            android.net.Uri r5 = r21.getDataUri()
            byte[] r6 = r21.getData()
            r10.<init>(r7, r5, r6)
            java.lang.String r5 = "text"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x010a
            com.android.mms.model.TextModel r5 = new com.android.mms.model.TextModel
            int r9 = r21.getCharset()
            r6 = r17
            r8 = r19
            r11 = r22
            r5.<init>(r6, r7, r8, r9, r10, r11)
            r17 = r5
        L_0x0042:
            r19 = 0
            org.w3c.dom.smil.TimeList r21 = r20.getBegin()
            if (r21 == 0) goto L_0x006a
            int r22 = r21.getLength()
            if (r22 <= 0) goto L_0x006a
            r19 = 0
            r0 = r21
            r1 = r19
            org.w3c.dom.smil.Time r19 = r0.item(r1)
            double r21 = r19.getResolvedOffset()
            r5 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r21 = r21 * r5
            r0 = r21
            int r0 = (int) r0
            r19 = r0
        L_0x006a:
            r0 = r17
            r1 = r19
            r0.setBegin(r1)
            float r21 = r20.getDur()
            r22 = 1148846080(0x447a0000, float:1000.0)
            float r21 = r21 * r22
            r0 = r21
            int r0 = (int) r0
            r21 = r0
            if (r21 > 0) goto L_0x0332
            org.w3c.dom.smil.TimeList r22 = r20.getEnd()
            if (r22 == 0) goto L_0x0332
            int r5 = r22.getLength()
            if (r5 <= 0) goto L_0x0332
            r5 = 0
            r0 = r22
            r1 = r5
            org.w3c.dom.smil.Time r22 = r0.item(r1)
            short r5 = r22.getTimeType()
            if (r5 == 0) goto L_0x0332
            double r21 = r22.getResolvedOffset()
            r5 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r21 = r21 * r5
            r0 = r21
            int r0 = (int) r0
            r21 = r0
            int r19 = r21 - r19
            if (r19 != 0) goto L_0x00fd
            r0 = r17
            boolean r0 = r0 instanceof com.android.mms.model.AudioModel
            r21 = r0
            if (r21 != 0) goto L_0x00be
            r0 = r17
            boolean r0 = r0 instanceof com.android.mms.model.VideoModel
            r21 = r0
            if (r21 == 0) goto L_0x00fd
        L_0x00be:
            int r19 = com.android.mms.MmsConfig.getMinimumSlideElementDuration()
            java.lang.String r21 = "Mms:app"
            r22 = 2
            boolean r21 = android.util.Log.isLoggable(r21, r22)
            if (r21 == 0) goto L_0x00fd
            java.lang.String r21 = "Mms:media"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            r22.<init>()
            java.lang.String r5 = "[MediaModelFactory] compute new duration for "
            r0 = r22
            r1 = r5
            java.lang.StringBuilder r22 = r0.append(r1)
            r0 = r22
            r1 = r18
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r22 = ", duration="
            r0 = r18
            r1 = r22
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r18 = r18.toString()
            r0 = r21
            r1 = r18
            android.util.Log.d(r0, r1)
        L_0x00fd:
            r18 = r19
        L_0x00ff:
            r17.setDuration(r18)
            short r18 = r20.getFill()
            r17.setFill(r18)
            return r17
        L_0x010a:
            java.lang.String r5 = "img"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0126
            com.android.mms.model.ImageModel r11 = new com.android.mms.model.ImageModel
            r12 = r17
            r13 = r7
            r14 = r19
            r15 = r10
            r16 = r22
            r11.<init>(r12, r13, r14, r15, r16)
            r17 = r11
            goto L_0x0042
        L_0x0126:
            java.lang.String r5 = "video"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0142
            com.android.mms.model.VideoModel r11 = new com.android.mms.model.VideoModel
            r12 = r17
            r13 = r7
            r14 = r19
            r15 = r10
            r16 = r22
            r11.<init>(r12, r13, r14, r15, r16)
            r17 = r11
            goto L_0x0042
        L_0x0142:
            java.lang.String r5 = "audio"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x015e
            com.android.mms.model.AudioModel r21 = new com.android.mms.model.AudioModel
            r0 = r21
            r1 = r17
            r2 = r7
            r3 = r19
            r4 = r10
            r0.<init>(r1, r2, r3, r4)
            r17 = r21
            goto L_0x0042
        L_0x015e:
            java.lang.String r5 = "ref"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x01e7
            java.lang.String r5 = r10.getContentType()
            boolean r6 = com.google.android.mms.ContentType.isTextType(r5)
            if (r6 == 0) goto L_0x0186
            com.android.mms.model.TextModel r5 = new com.android.mms.model.TextModel
            int r9 = r21.getCharset()
            r6 = r17
            r8 = r19
            r11 = r22
            r5.<init>(r6, r7, r8, r9, r10, r11)
            r17 = r5
            goto L_0x0042
        L_0x0186:
            boolean r21 = com.google.android.mms.ContentType.isImageType(r5)
            if (r21 == 0) goto L_0x019d
            com.android.mms.model.ImageModel r11 = new com.android.mms.model.ImageModel
            r12 = r17
            r13 = r7
            r14 = r19
            r15 = r10
            r16 = r22
            r11.<init>(r12, r13, r14, r15, r16)
            r17 = r11
            goto L_0x0042
        L_0x019d:
            boolean r21 = com.google.android.mms.ContentType.isVideoType(r5)
            if (r21 == 0) goto L_0x01b4
            com.android.mms.model.VideoModel r11 = new com.android.mms.model.VideoModel
            r12 = r17
            r13 = r7
            r14 = r19
            r15 = r10
            r16 = r22
            r11.<init>(r12, r13, r14, r15, r16)
            r17 = r11
            goto L_0x0042
        L_0x01b4:
            boolean r21 = com.google.android.mms.ContentType.isAudioType(r5)
            if (r21 == 0) goto L_0x01cb
            com.android.mms.model.AudioModel r21 = new com.android.mms.model.AudioModel
            r0 = r21
            r1 = r17
            r2 = r7
            r3 = r19
            r4 = r10
            r0.<init>(r1, r2, r3, r4)
            r17 = r21
            goto L_0x0042
        L_0x01cb:
            com.android.mms.UnsupportContentTypeException r17 = new com.android.mms.UnsupportContentTypeException
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "Unsupported Content-Type: "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r5
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            r17.<init>(r18)
            throw r17
        L_0x01e7:
            java.lang.IllegalArgumentException r17 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r19.<init>()
            java.lang.String r20 = "Unsupported TAG: "
            java.lang.StringBuilder r19 = r19.append(r20)
            r0 = r19
            r1 = r18
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            r17.<init>(r18)
            throw r17
        L_0x0204:
            java.lang.String r5 = "text"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0226
            com.android.mms.model.TextModel r5 = new com.android.mms.model.TextModel
            int r9 = r21.getCharset()
            byte[] r10 = r21.getData()
            r6 = r17
            r8 = r19
            r11 = r22
            r5.<init>(r6, r7, r8, r9, r10, r11)
            r17 = r5
            goto L_0x0042
        L_0x0226:
            java.lang.String r5 = "img"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0244
            com.android.mms.model.ImageModel r5 = new com.android.mms.model.ImageModel
            android.net.Uri r9 = r21.getDataUri()
            r6 = r17
            r8 = r19
            r10 = r22
            r5.<init>(r6, r7, r8, r9, r10)
            r17 = r5
            goto L_0x0042
        L_0x0244:
            java.lang.String r5 = "video"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0262
            com.android.mms.model.VideoModel r5 = new com.android.mms.model.VideoModel
            android.net.Uri r9 = r21.getDataUri()
            r6 = r17
            r8 = r19
            r10 = r22
            r5.<init>(r6, r7, r8, r9, r10)
            r17 = r5
            goto L_0x0042
        L_0x0262:
            java.lang.String r5 = "audio"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0283
            com.android.mms.model.AudioModel r22 = new com.android.mms.model.AudioModel
            android.net.Uri r21 = r21.getDataUri()
            r0 = r22
            r1 = r17
            r2 = r7
            r3 = r19
            r4 = r21
            r0.<init>(r1, r2, r3, r4)
            r17 = r22
            goto L_0x0042
        L_0x0283:
            java.lang.String r5 = "ref"
            r0 = r18
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0315
            boolean r5 = com.google.android.mms.ContentType.isTextType(r7)
            if (r5 == 0) goto L_0x02ab
            com.android.mms.model.TextModel r5 = new com.android.mms.model.TextModel
            int r9 = r21.getCharset()
            byte[] r10 = r21.getData()
            r6 = r17
            r8 = r19
            r11 = r22
            r5.<init>(r6, r7, r8, r9, r10, r11)
            r17 = r5
            goto L_0x0042
        L_0x02ab:
            boolean r5 = com.google.android.mms.ContentType.isImageType(r7)
            if (r5 == 0) goto L_0x02c4
            com.android.mms.model.ImageModel r5 = new com.android.mms.model.ImageModel
            android.net.Uri r9 = r21.getDataUri()
            r6 = r17
            r8 = r19
            r10 = r22
            r5.<init>(r6, r7, r8, r9, r10)
            r17 = r5
            goto L_0x0042
        L_0x02c4:
            boolean r5 = com.google.android.mms.ContentType.isVideoType(r7)
            if (r5 == 0) goto L_0x02dd
            com.android.mms.model.VideoModel r5 = new com.android.mms.model.VideoModel
            android.net.Uri r9 = r21.getDataUri()
            r6 = r17
            r8 = r19
            r10 = r22
            r5.<init>(r6, r7, r8, r9, r10)
            r17 = r5
            goto L_0x0042
        L_0x02dd:
            boolean r22 = com.google.android.mms.ContentType.isAudioType(r7)
            if (r22 == 0) goto L_0x02f9
            com.android.mms.model.AudioModel r22 = new com.android.mms.model.AudioModel
            android.net.Uri r21 = r21.getDataUri()
            r0 = r22
            r1 = r17
            r2 = r7
            r3 = r19
            r4 = r21
            r0.<init>(r1, r2, r3, r4)
            r17 = r22
            goto L_0x0042
        L_0x02f9:
            com.android.mms.UnsupportContentTypeException r17 = new com.android.mms.UnsupportContentTypeException
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "Unsupported Content-Type: "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r7
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            r17.<init>(r18)
            throw r17
        L_0x0315:
            java.lang.IllegalArgumentException r17 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r19.<init>()
            java.lang.String r20 = "Unsupported TAG: "
            java.lang.StringBuilder r19 = r19.append(r20)
            r0 = r19
            r1 = r18
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            r17.<init>(r18)
            throw r17
        L_0x0332:
            r18 = r21
            goto L_0x00ff
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.model.MediaModelFactory.getGenericMediaModel(android.content.Context, java.lang.String, java.lang.String, org.w3c.dom.smil.SMILMediaElement, com.google.android.mms.pdu.PduPart, com.android.mms.model.RegionModel):com.android.mms.model.MediaModel");
    }

    public static MediaModel getMediaModel(Context context, SMILMediaElement sMILMediaElement, LayoutModel layoutModel, PduBody pduBody) throws DrmException, IOException, IllegalArgumentException, MmsException {
        String tagName = sMILMediaElement.getTagName();
        String src = sMILMediaElement.getSrc();
        PduPart findPart = findPart(pduBody, src);
        if (!(sMILMediaElement instanceof SMILRegionMediaElement)) {
            return getGenericMediaModel(context, tagName, src, sMILMediaElement, findPart, null);
        }
        return getRegionMediaModel(context, tagName, src, (SMILRegionMediaElement) sMILMediaElement, layoutModel, findPart);
    }

    private static MediaModel getRegionMediaModel(Context context, String str, String str2, SMILRegionMediaElement sMILRegionMediaElement, LayoutModel layoutModel, PduPart pduPart) throws DrmException, IOException, MmsException {
        SMILRegionElement region = sMILRegionMediaElement.getRegion();
        if (region != null) {
            RegionModel findRegionById = layoutModel.findRegionById(region.getId());
            if (findRegionById != null) {
                return getGenericMediaModel(context, str, str2, sMILRegionMediaElement, pduPart, findRegionById);
            }
        } else {
            RegionModel findRegionById2 = layoutModel.findRegionById(str.equals("text") ? LayoutModel.TEXT_REGION_ID : LayoutModel.IMAGE_REGION_ID);
            if (findRegionById2 != null) {
                return getGenericMediaModel(context, str, str2, sMILRegionMediaElement, pduPart, findRegionById2);
            }
        }
        throw new IllegalArgumentException("Region not found or bad region ID.");
    }
}
