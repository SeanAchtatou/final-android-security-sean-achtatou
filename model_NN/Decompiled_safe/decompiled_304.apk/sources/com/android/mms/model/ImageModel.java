package com.android.mms.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import com.android.drm.mobile1.DrmException;
import com.android.mms.ContentRestrictionException;
import com.android.mms.dom.smil.SmilMediaElementImpl;
import com.android.mms.drm.DrmWrapper;
import com.android.mms.ui.UriImage;
import com.google.android.mms.MmsException;
import java.io.IOException;
import java.lang.ref.SoftReference;
import org.w3c.dom.events.Event;

public class ImageModel extends RegionMediaModel {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "Mms/image";
    private static final int THUMBNAIL_BOUNDS_LIMIT = 480;
    private SoftReference<Bitmap> mBitmapCache = new SoftReference<>(null);
    private int mHeight;
    private int mWidth;

    public ImageModel(Context context, Uri uri, RegionModel regionModel) throws MmsException {
        super(context, "img", uri, regionModel);
        initModelFromUri(uri);
        checkContentRestriction();
    }

    public ImageModel(Context context, String str, String str2, Uri uri, RegionModel regionModel) throws DrmException, MmsException {
        super(context, "img", str, str2, uri, regionModel);
        decodeImageBounds();
    }

    public ImageModel(Context context, String str, String str2, DrmWrapper drmWrapper, RegionModel regionModel) throws IOException {
        super(context, "img", str, str2, drmWrapper, regionModel);
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0097 A[SYNTHETIC, Splitter:B:38:0x0097] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap createThumbnailBitmap(int r8, android.net.Uri r9) {
        /*
            r7 = this;
            r6 = 0
            java.lang.String r5 = "Mms/image"
            int r0 = r7.mWidth
            int r1 = r7.mHeight
            r2 = 1
        L_0x0008:
            int r3 = r0 / r2
            if (r3 > r8) goto L_0x0010
            int r3 = r1 / r2
            if (r3 <= r8) goto L_0x0013
        L_0x0010:
            int r2 = r2 * 2
            goto L_0x0008
        L_0x0013:
            java.lang.String r3 = "Mms:app"
            r4 = 2
            boolean r3 = android.util.Log.isLoggable(r3, r4)
            if (r3 == 0) goto L_0x004a
            java.lang.String r3 = "Mms/image"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "createThumbnailBitmap: scale="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r2)
            java.lang.String r4 = ", w="
            java.lang.StringBuilder r3 = r3.append(r4)
            int r0 = r0 / r2
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = ", h="
            java.lang.StringBuilder r0 = r0.append(r3)
            int r1 = r1 / r2
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r5, r0)
        L_0x004a:
            android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
            r0.<init>()
            r0.inSampleSize = r2
            android.content.Context r1 = r7.mContext     // Catch:{ FileNotFoundException -> 0x0071, OutOfMemoryError -> 0x008e, all -> 0x00a6 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0071, OutOfMemoryError -> 0x008e, all -> 0x00a6 }
            java.io.InputStream r1 = r1.openInputStream(r9)     // Catch:{ FileNotFoundException -> 0x0071, OutOfMemoryError -> 0x008e, all -> 0x00a6 }
            r2 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r1, r2, r0)     // Catch:{ FileNotFoundException -> 0x00ab, OutOfMemoryError -> 0x00a9 }
            if (r1 == 0) goto L_0x0065
            r1.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0065:
            return r0
        L_0x0066:
            r1 = move-exception
            java.lang.String r2 = "Mms/image"
            java.lang.String r2 = r1.getMessage()
            android.util.Log.e(r5, r2, r1)
            goto L_0x0065
        L_0x0071:
            r0 = move-exception
            r1 = r6
        L_0x0073:
            java.lang.String r2 = "Mms/image"
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0094 }
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0094 }
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0081:
            r0 = r6
            goto L_0x0065
        L_0x0083:
            r0 = move-exception
            java.lang.String r1 = "Mms/image"
            java.lang.String r1 = r0.getMessage()
            android.util.Log.e(r5, r1, r0)
            goto L_0x0081
        L_0x008e:
            r0 = move-exception
            r1 = r6
        L_0x0090:
            vc.lx.sms.util.MessageUtils.writeHprofDataToFile()     // Catch:{ all -> 0x0094 }
            throw r0     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r0 = move-exception
        L_0x0095:
            if (r1 == 0) goto L_0x009a
            r1.close()     // Catch:{ IOException -> 0x009b }
        L_0x009a:
            throw r0
        L_0x009b:
            r1 = move-exception
            java.lang.String r2 = "Mms/image"
            java.lang.String r2 = r1.getMessage()
            android.util.Log.e(r5, r2, r1)
            goto L_0x009a
        L_0x00a6:
            r0 = move-exception
            r1 = r6
            goto L_0x0095
        L_0x00a9:
            r0 = move-exception
            goto L_0x0090
        L_0x00ab:
            r0 = move-exception
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.model.ImageModel.createThumbnailBitmap(int, android.net.Uri):android.graphics.Bitmap");
    }

    private void decodeImageBounds() throws DrmException {
        UriImage uriImage = new UriImage(this.mContext, getUriWithDrmCheck());
        this.mWidth = uriImage.getWidth();
        this.mHeight = uriImage.getHeight();
    }

    private void initModelFromUri(Uri uri) throws MmsException {
        UriImage uriImage = new UriImage(this.mContext, uri);
        this.mContentType = uriImage.getContentType();
        if (TextUtils.isEmpty(this.mContentType)) {
            throw new MmsException("Type of media is unknown.");
        }
        this.mSrc = uriImage.getSrc();
        this.mWidth = uriImage.getWidth();
        this.mHeight = uriImage.getHeight();
    }

    private Bitmap internalGetBitmap(Uri uri) {
        Bitmap bitmap = this.mBitmapCache.get();
        if (bitmap == null) {
            try {
                bitmap = createThumbnailBitmap(480, uri);
                if (bitmap != null) {
                    this.mBitmapCache = new SoftReference<>(bitmap);
                }
            } catch (OutOfMemoryError e) {
            }
        }
        return bitmap;
    }

    /* access modifiers changed from: protected */
    public void checkContentRestriction() throws ContentRestrictionException {
        ContentRestriction contentRestriction = ContentRestrictionFactory.getContentRestriction();
        contentRestriction.checkImageContentType(this.mContentType);
        contentRestriction.checkResolution(this.mWidth, this.mHeight);
    }

    public Bitmap getBitmap() {
        return internalGetBitmap(getUri());
    }

    public Bitmap getBitmapWithDrmCheck() throws DrmException {
        return internalGetBitmap(getUriWithDrmCheck());
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public void handleEvent(Event event) {
        if (event.getType().equals(SmilMediaElementImpl.SMIL_MEDIA_START_EVENT)) {
            this.mVisible = true;
        } else if (this.mFill != 1) {
            this.mVisible = false;
        }
        notifyModelChanged(false);
    }
}
