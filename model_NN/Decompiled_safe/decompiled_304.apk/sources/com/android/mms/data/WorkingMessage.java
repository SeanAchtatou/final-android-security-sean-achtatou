package com.android.mms.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import com.android.mms.ExceedMessageSizeException;
import com.android.mms.MmsConfig;
import com.android.mms.ResolutionException;
import com.android.mms.UnsupportContentTypeException;
import com.android.mms.model.AudioModel;
import com.android.mms.model.ImageModel;
import com.android.mms.model.MediaModel;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.model.TextModel;
import com.android.mms.model.VideoModel;
import com.android.mms.transaction.MmsMessageSender;
import com.android.mms.transaction.SmsMessageSender;
import com.android.mms.ui.SlideshowEditor;
import com.android.provider.Telephony;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.SendReq;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.http.data.SmsImage;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.data.Conversation;
import vc.lx.sms.data.LogTag;
import vc.lx.sms.data.RecipientIdCache;
import vc.lx.sms.db.ImagesDbService;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.Recycler;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class WorkingMessage {
    public static final int AUDIO = 3;
    private static final boolean DEBUG = false;
    private static final int FORCE_MMS = 16;
    private static final int HAS_ATTACHMENT = 4;
    private static final int HAS_SUBJECT = 2;
    public static final int IMAGE = 1;
    public static final int IMAGE_TOO_LARGE = -4;
    private static final int LENGTH_REQUIRES_MMS = 8;
    public static final int MESSAGE_SIZE_EXCEEDED = -2;
    private static final String[] MMS_DRAFT_PROJECTION = {"_id", Telephony.BaseMmsColumns.SUBJECT};
    private static final int MMS_ID_INDEX = 0;
    private static final int MMS_MESSAGE_SIZE_INDEX = 1;
    private static final String[] MMS_OUTBOX_PROJECTION = {"_id", Telephony.BaseMmsColumns.MESSAGE_SIZE};
    private static final int MMS_SUBJECT_INDEX = 1;
    public static final int OK = 0;
    private static final int RECIPIENTS_REQUIRE_MMS = 1;
    public static final int SLIDESHOW = 4;
    private static final int SMS_BODY_INDEX = 0;
    private static final String[] SMS_BODY_PROJECTION = {Telephony.TextBasedSmsColumns.BODY};
    private static final String SMS_DRAFT_WHERE = "type=3";
    private static final String TAG = "WorkingMessage";
    public static final int TEXT = 0;
    public static final int UNKNOWN_ERROR = -1;
    public static final int UNSUPPORTED_TYPE = -3;
    public static final int VIDEO = 2;
    private int mAttachmentType;
    /* access modifiers changed from: private */
    public final ContentResolver mContentResolver;
    /* access modifiers changed from: private */
    public final Context mContext;
    private Conversation mConversation;
    private boolean mDiscarded = false;
    private SmsImage mImageItem;
    /* access modifiers changed from: private */
    public Uri mMessageUri;
    private int mMmsState;
    /* access modifiers changed from: private */
    public String mShortenUrl;
    /* access modifiers changed from: private */
    public SlideshowModel mSlideshow;
    private SongItem mSongItem;
    private final MessageStatusListener mStatusListener;
    private CharSequence mSubject;
    private CharSequence mText;
    private Bitmap mToSendImage;
    private VoteItem mVoteItem;
    private List<String> mWorkingRecipients;

    public interface MessageStatusListener {
        void onAttachmentChanged();

        void onMaxPendingMessagesReached();

        void onMessageSent();

        void onPreMessageSent();

        void onProtocolChanged(boolean z);
    }

    private WorkingMessage(ComposeMessageActivity composeMessageActivity) {
        this.mContext = composeMessageActivity;
        this.mContentResolver = this.mContext.getContentResolver();
        this.mStatusListener = composeMessageActivity;
        this.mAttachmentType = 0;
        this.mText = "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.telephony.SmsMessage.calculateLength(java.lang.String, boolean):int[]}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.telephony.SmsMessage.calculateLength(java.lang.CharSequence, boolean):int[]}
      ClspMth{android.telephony.SmsMessage.calculateLength(java.lang.String, boolean):int[]} */
    private boolean addressContainsEmailToMms(Conversation conversation, String str) {
        if (MmsConfig.getEmailGateway() != null) {
            String[] numbers = conversation.getRecipients().getNumbers();
            int length = numbers.length;
            for (int i = 0; i < length; i++) {
                if ((Telephony.Mms.isEmailAddress(numbers[i]) || MessageUtils.isAlias(numbers[i])) && SmsMessage.calculateLength(numbers[i] + " " + str, false)[0] > 1) {
                    updateState(1, true, true);
                    ensureSlideshow();
                    syncTextToSlideshow();
                    return true;
                }
            }
        }
        return false;
    }

    private void appendMedia(int i, Uri uri) throws MmsException {
        MediaModel audioModel;
        if (i != 0) {
            if (!(this.mSlideshow.size() != 1 || this.mSlideshow.isSimple()) || new SlideshowEditor(this.mContext, this.mSlideshow).addNewSlide()) {
                SlideModel slideModel = this.mSlideshow.get(this.mSlideshow.size() - 1);
                if (i == 1) {
                    audioModel = new ImageModel(this.mContext, uri, this.mSlideshow.getLayout().getImageRegion());
                } else if (i == 2) {
                    audioModel = new VideoModel(this.mContext, uri, this.mSlideshow.getLayout().getImageRegion());
                } else if (i == 3) {
                    audioModel = new AudioModel(this.mContext, uri);
                } else {
                    throw new IllegalArgumentException("changeMedia type=" + i + ", uri=" + uri);
                }
                slideModel.add(audioModel);
                if (i == 2 || i == 3) {
                    slideModel.updateDuration(audioModel.getDuration());
                }
            }
        }
    }

    private void asyncDelete(final Uri uri, final String str, final String[] strArr) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("asyncDelete %s where %s", uri, str);
        }
        new Thread(new Runnable() {
            public void run() {
                SqliteWrapper.delete(WorkingMessage.this.mContext, WorkingMessage.this.mContentResolver, uri, str, strArr);
            }
        }).start();
    }

    private void asyncDeleteDraftMmsMessage(long j) {
        asyncDelete(Telephony.Mms.Draft.CONTENT_URI, "thread_id = " + j, null);
    }

    private void asyncDeleteDraftSmsMessage(Conversation conversation) {
        long threadId = conversation.getThreadId();
        if (threadId > 0) {
            asyncDelete(ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, threadId), SMS_DRAFT_WHERE, null);
        }
    }

    private void asyncUpdateDraftMmsMessage(final Conversation conversation) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("asyncUpdateDraftMmsMessage conv=%s mMessageUri=%s", conversation, this.mMessageUri);
        }
        final PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
        final SendReq makeSendReq = makeSendReq(conversation, this.mSubject);
        new Thread(new Runnable() {
            public void run() {
                conversation.ensureThreadId();
                conversation.setDraftState(true);
                if (WorkingMessage.this.mMessageUri == null) {
                    Uri unused = WorkingMessage.this.mMessageUri = WorkingMessage.createDraftMmsMessage(pduPersister, makeSendReq, WorkingMessage.this.mSlideshow);
                } else {
                    WorkingMessage.updateDraftMmsMessage(WorkingMessage.this.mMessageUri, pduPersister, WorkingMessage.this.mSlideshow, makeSendReq);
                }
            }
        }).start();
        asyncDeleteDraftSmsMessage(conversation);
    }

    private void asyncUpdateDraftSmsMessage(final Conversation conversation, final String str) {
        new Thread(new Runnable() {
            public void run() {
                long ensureThreadId = conversation.ensureThreadId();
                conversation.setDraftState(true);
                WorkingMessage.this.updateDraftSmsMessage(ensureThreadId, str);
            }
        }).start();
    }

    private void changeMedia(int i, Uri uri) throws MmsException {
        MediaModel audioModel;
        SlideModel slideModel = this.mSlideshow.get(0);
        if (slideModel == null) {
            Log.w("Mms", "[WorkingMessage] changeMedia: no slides!");
            return;
        }
        slideModel.removeImage();
        slideModel.removeVideo();
        slideModel.removeAudio();
        if (i != 0) {
            if (i == 1) {
                audioModel = new ImageModel(this.mContext, uri, this.mSlideshow.getLayout().getImageRegion());
            } else if (i == 2) {
                audioModel = new VideoModel(this.mContext, uri, this.mSlideshow.getLayout().getImageRegion());
            } else if (i == 3) {
                audioModel = new AudioModel(this.mContext, uri);
            } else {
                throw new IllegalArgumentException("changeMedia type=" + i + ", uri=" + uri);
            }
            slideModel.add(audioModel);
            if (i == 2 || i == 3) {
                slideModel.updateDuration(audioModel.getDuration());
            }
        }
    }

    private void clearShortenUrl() {
        if (!(this.mShortenUrl == null || this.mText == null)) {
            this.mText = this.mText.toString().replace(this.mShortenUrl, "");
        }
        this.mShortenUrl = null;
        this.mSongItem = null;
        this.mVoteItem = null;
        this.mImageItem = null;
    }

    private void correctAttachmentState() {
        int size = this.mSlideshow.size();
        if (size == 0) {
            this.mAttachmentType = 0;
            this.mSlideshow = null;
            asyncDelete(this.mMessageUri, null, null);
            this.mMessageUri = null;
        } else if (size > 1) {
            this.mAttachmentType = 4;
        } else {
            SlideModel slideModel = this.mSlideshow.get(0);
            if (slideModel.hasImage()) {
                this.mAttachmentType = 1;
            } else if (slideModel.hasVideo()) {
                this.mAttachmentType = 2;
            } else if (slideModel.hasAudio()) {
                this.mAttachmentType = 3;
            }
        }
        updateState(4, hasAttachment(), false);
    }

    /* access modifiers changed from: private */
    public static Uri createDraftMmsMessage(PduPersister pduPersister, SendReq sendReq, SlideshowModel slideshowModel) {
        try {
            PduBody pduBody = slideshowModel.toPduBody();
            sendReq.setBody(pduBody);
            Uri persist = pduPersister.persist(sendReq, Telephony.Mms.Draft.CONTENT_URI);
            slideshowModel.sync(pduBody);
            return persist;
        } catch (MmsException e) {
            return null;
        }
    }

    public static WorkingMessage createEmpty(ComposeMessageActivity composeMessageActivity) {
        return new WorkingMessage(composeMessageActivity);
    }

    private void deleteDraftSmsMessage(long j) {
        SqliteWrapper.delete(this.mContext, this.mContentResolver, ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, j), SMS_DRAFT_WHERE, null);
    }

    private void ensureSlideshow() {
        if (this.mSlideshow == null) {
            SlideshowModel createNew = SlideshowModel.createNew(this.mContext);
            createNew.add(new SlideModel(createNew));
            this.mSlideshow = createNew;
        }
    }

    public static WorkingMessage load(ComposeMessageActivity composeMessageActivity, Uri uri) {
        Uri uri2;
        if (!uri.toString().startsWith(Telephony.Mms.Draft.CONTENT_URI.toString())) {
            PduPersister pduPersister = PduPersister.getPduPersister(composeMessageActivity);
            if (Log.isLoggable(LogTag.APP, 2)) {
                LogTag.debug("load: moving %s to drafts", uri);
            }
            try {
                uri2 = pduPersister.move(uri, Telephony.Mms.Draft.CONTENT_URI);
            } catch (MmsException e) {
                LogTag.error("Can't move %s to drafts", uri);
                return null;
            }
        } else {
            uri2 = uri;
        }
        WorkingMessage workingMessage = new WorkingMessage(composeMessageActivity);
        if (workingMessage.loadFromUri(uri2)) {
            return workingMessage;
        }
        return null;
    }

    public static WorkingMessage loadDraft(ComposeMessageActivity composeMessageActivity, Conversation conversation) {
        WorkingMessage workingMessage = new WorkingMessage(composeMessageActivity);
        return workingMessage.loadFromConversation(conversation) ? workingMessage : createEmpty(composeMessageActivity);
    }

    private boolean loadFromConversation(Conversation conversation) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("loadFromConversation %s", conversation);
        }
        long threadId = conversation.getThreadId();
        if (threadId <= 0) {
            return false;
        }
        this.mText = readDraftSmsMessage(this.mContext, threadId, conversation);
        if (!TextUtils.isEmpty(this.mText)) {
            checkShortenUrl();
            return true;
        }
        StringBuilder sb = new StringBuilder();
        Uri readDraftMmsMessage = readDraftMmsMessage(this.mContext, threadId, sb);
        if (readDraftMmsMessage == null || !loadFromUri(readDraftMmsMessage)) {
            return false;
        }
        if (sb.length() > 0) {
            setSubject(sb.toString(), false);
        }
        return true;
    }

    private boolean loadFromUri(Uri uri) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("loadFromUri %s", uri);
        }
        try {
            this.mSlideshow = SlideshowModel.createFromMessageUri(this.mContext, uri);
            this.mMessageUri = uri;
            syncTextFromSlideshow();
            correctAttachmentState();
            return true;
        } catch (MmsException e) {
            LogTag.error("Couldn't load URI %s", uri);
            return false;
        }
    }

    private static SendReq makeSendReq(Conversation conversation, CharSequence charSequence) {
        String[] numbers = conversation.getRecipients().getNumbers(true);
        SendReq sendReq = new SendReq();
        EncodedStringValue[] encodeStrings = EncodedStringValue.encodeStrings(numbers);
        if (encodeStrings != null) {
            sendReq.setTo(encodeStrings);
        }
        if (!TextUtils.isEmpty(charSequence)) {
            sendReq.setSubject(new EncodedStringValue(charSequence.toString()));
        }
        sendReq.setDate(System.currentTimeMillis() / 1000);
        return sendReq;
    }

    private void prepareForSave(boolean z) {
        syncWorkingRecipients();
        if (requiresMms()) {
            ensureSlideshow();
            syncTextToSlideshow();
            removeSubjectIfEmpty(z);
        }
    }

    /* JADX INFO: finally extract failed */
    private static Uri readDraftMmsMessage(Context context, long j, StringBuilder sb) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("readDraftMmsMessage tid=%d", Long.valueOf(j));
        }
        Context context2 = context;
        Cursor query = SqliteWrapper.query(context2, context.getContentResolver(), Telephony.Mms.Draft.CONTENT_URI, MMS_DRAFT_PROJECTION, "thread_id = " + j, null, null);
        try {
            if (query.moveToFirst()) {
                Uri withAppendedId = ContentUris.withAppendedId(Telephony.Mms.Draft.CONTENT_URI, query.getLong(0));
                String string = query.getString(1);
                if (string != null) {
                    sb.append(string);
                }
                query.close();
                return withAppendedId;
            }
            query.close();
            return null;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    private static String readDraftSmsMessage(Context context, long j, Conversation conversation) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("readDraftSmsMessage tid=%d", Long.valueOf(j));
        }
        ContentResolver contentResolver = context.getContentResolver();
        if (j <= 0) {
            return "";
        }
        Uri withAppendedId = ContentUris.withAppendedId(Telephony.Sms.Conversations.CONTENT_URI, j);
        Cursor query = SqliteWrapper.query(context, contentResolver, withAppendedId, SMS_BODY_PROJECTION, SMS_DRAFT_WHERE, null, null);
        try {
            String string = query.moveToFirst() ? query.getString(0) : "";
            query.close();
            SqliteWrapper.delete(context, contentResolver, withAppendedId, SMS_DRAFT_WHERE, null);
            if (conversation.getMessageCount() == 0) {
                conversation.clearThreadId();
            }
            return string;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    private void removeSubjectIfEmpty(boolean z) {
        if (!hasSubject()) {
            setSubject(null, z);
        }
    }

    /* access modifiers changed from: private */
    public void sendMmsWorker(Conversation conversation, Uri uri, PduPersister pduPersister, SlideshowModel slideshowModel, SendReq sendReq) {
        Cursor cursor;
        Uri uri2;
        try {
            Cursor query = SqliteWrapper.query(this.mContext, this.mContentResolver, Telephony.Mms.Outbox.CONTENT_URI, MMS_OUTBOX_PROJECTION, null, null, null);
            if (query != null) {
                try {
                    long maxSizeScaleForPendingMmsAllowed = (long) (MmsConfig.getMaxSizeScaleForPendingMmsAllowed() * MmsConfig.getMaxMessageSize());
                    long j = 0;
                    while (query.moveToNext()) {
                        j += query.getLong(1);
                    }
                    if (j >= maxSizeScaleForPendingMmsAllowed) {
                        unDiscard();
                        this.mStatusListener.onMaxPendingMessagesReached();
                        if (query != null) {
                            query.close();
                            return;
                        }
                        return;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor = query;
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            this.mStatusListener.onPreMessageSent();
            long ensureThreadId = conversation.ensureThreadId();
            if (Log.isLoggable(LogTag.APP, 2)) {
                LogTag.debug("sendMmsWorker: update draft MMS message " + uri, new Object[0]);
            }
            if (uri == null) {
                uri2 = createDraftMmsMessage(pduPersister, sendReq, slideshowModel);
            } else {
                updateDraftMmsMessage(uri, pduPersister, slideshowModel, sendReq);
                uri2 = uri;
            }
            deleteDraftSmsMessage(ensureThreadId);
            try {
                if (!new MmsMessageSender(this.mContext, uri2, (long) slideshowModel.getCurrentMessageSize()).sendMessage(ensureThreadId)) {
                    SqliteWrapper.delete(this.mContext, this.mContentResolver, uri2, null, null);
                }
                Recycler.getMmsRecycler().deleteOldMessagesByThreadId(this.mContext, ensureThreadId);
            } catch (Exception e) {
                Log.e(TAG, "Failed to send message: " + uri2 + ", threadId=" + ensureThreadId, e);
            }
            this.mStatusListener.onMessageSent();
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* access modifiers changed from: private */
    public void sendMusicSmsWorker(Conversation conversation, ArrayList<String> arrayList) {
        this.mStatusListener.onPreMessageSent();
        long ensureThreadId = conversation.ensureThreadId();
        try {
            new SmsMessageSender(this.mContext, conversation.getRecipients().getNumbers(), ensureThreadId).sendMessage(ensureThreadId, arrayList);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(this.mContext, ensureThreadId);
        } catch (Exception e) {
            Log.e(TAG, "Failed to send SMS message, threadId=" + ensureThreadId, e);
        }
        this.mStatusListener.onMessageSent();
    }

    /* access modifiers changed from: private */
    public void sendSmsWorker(Conversation conversation, String str) {
        this.mStatusListener.onPreMessageSent();
        long ensureThreadId = conversation.ensureThreadId();
        try {
            new SmsMessageSender(this.mContext, conversation.getRecipients().getNumbers(), str, ensureThreadId).sendMessage(ensureThreadId);
            Recycler.getSmsRecycler().deleteOldMessagesByThreadId(this.mContext, ensureThreadId);
        } catch (Exception e) {
            Log.e(TAG, "Failed to send SMS message, threadId=" + ensureThreadId, e);
        }
        this.mStatusListener.onMessageSent();
    }

    private static String stateString(int i) {
        if (i == 0) {
            return "<none>";
        }
        StringBuilder sb = new StringBuilder();
        if ((i & 1) > 0) {
            sb.append("RECIPIENTS_REQUIRE_MMS | ");
        }
        if ((i & 2) > 0) {
            sb.append("HAS_SUBJECT | ");
        }
        if ((i & 4) > 0) {
            sb.append("HAS_ATTACHMENT | ");
        }
        if ((i & 8) > 0) {
            sb.append("LENGTH_REQUIRES_MMS | ");
        }
        if ((i & 16) > 0) {
            sb.append("FORCE_MMS | ");
        }
        sb.delete(sb.length() - 3, sb.length());
        return sb.toString();
    }

    private void syncTextFromSlideshow() {
        SlideModel slideModel;
        if (this.mSlideshow.size() == 1 && (slideModel = this.mSlideshow.get(0)) != null && slideModel.hasText()) {
            this.mText = slideModel.getText().getText();
        }
    }

    private void syncTextToSlideshow() {
        TextModel text;
        if (this.mSlideshow != null && this.mSlideshow.size() == 1) {
            SlideModel slideModel = this.mSlideshow.get(0);
            if (!slideModel.hasText()) {
                TextModel textModel = new TextModel(this.mContext, ContentType.TEXT_PLAIN, "text_0.txt", this.mSlideshow.getLayout().getTextRegion());
                slideModel.add((MediaModel) textModel);
                text = textModel;
            } else {
                text = slideModel.getText();
            }
            text.setText(this.mText);
        }
    }

    /* access modifiers changed from: private */
    public static void updateDraftMmsMessage(Uri uri, PduPersister pduPersister, SlideshowModel slideshowModel, SendReq sendReq) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("updateDraftMmsMessage uri=%s", uri);
        }
        if (uri == null) {
            Log.e(TAG, "updateDraftMmsMessage null uri");
            return;
        }
        pduPersister.updateHeaders(uri, sendReq);
        PduBody pduBody = slideshowModel.toPduBody();
        try {
            pduPersister.updateParts(uri, pduBody);
        } catch (MmsException e) {
            Log.e(TAG, "updateDraftMmsMessage: cannot update message " + uri);
        }
        slideshowModel.sync(pduBody);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void updateDraftSmsMessage(long j, String str) {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("updateDraftSmsMessage tid=%d, contents=\"%s\"", Long.valueOf(j), str);
        }
        if (j > 0) {
            ContentValues contentValues = new ContentValues(3);
            contentValues.put("thread_id", Long.valueOf(j));
            contentValues.put(Telephony.TextBasedSmsColumns.BODY, str);
            contentValues.put("type", (Integer) 3);
            SqliteWrapper.insert(this.mContext, this.mContentResolver, Telephony.Sms.CONTENT_URI, contentValues);
            asyncDeleteDraftMmsMessage(j);
        }
    }

    private void updateState(int i, boolean z, boolean z2) {
        int i2 = this.mMmsState;
        if (z) {
            this.mMmsState |= i;
        } else {
            this.mMmsState &= i ^ -1;
        }
        if (this.mMmsState == 16 && (i2 & -17) > 0) {
            this.mMmsState = 0;
        }
        if (z2) {
            if (i2 == 0 && this.mMmsState != 0) {
                this.mStatusListener.onProtocolChanged(true);
            } else if (i2 != 0 && this.mMmsState == 0) {
                this.mStatusListener.onProtocolChanged(false);
            }
        }
        if (i2 != this.mMmsState && Log.isLoggable(LogTag.APP, 2)) {
            Object[] objArr = new Object[3];
            objArr[0] = z ? "+" : "-";
            objArr[1] = stateString(i);
            objArr[2] = stateString(this.mMmsState);
            LogTag.debug("updateState: %s%s = %s", objArr);
        }
    }

    public void checkShortenUrl() {
        String string = this.mContext.getString(R.string.shorten_url_reg);
        String obj = this.mText == null ? "" : this.mText.toString();
        Matcher matcher = Pattern.compile(string, 32).matcher(obj);
        if (matcher == null || !matcher.find()) {
            Matcher matcher2 = Pattern.compile(this.mContext.getString(R.string.shorten_url_img_reg), 32).matcher(obj);
            if (matcher2 != null && matcher2.find()) {
                updateImageShortenUrl(matcher2.group(1), null);
                this.mText = obj.replace(matcher2.group(), "");
            }
            Matcher matcher3 = Pattern.compile(this.mContext.getString(R.string.shorten_url_vote_reg), 32).matcher(obj);
            if (matcher3 != null && matcher3.find()) {
                updateVoteShortenUrl(matcher3.group(1), null);
                this.mText = obj.replace(matcher3.group(), "");
                return;
            }
            return;
        }
        updateSongShortenUrl(matcher.group(1), null);
        this.mText = obj.replace(matcher.group(), "");
    }

    public void discard() {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("discard", new Object[0]);
        }
        if (this.mDiscarded) {
            throw new IllegalStateException("discard() called twice");
        }
        this.mDiscarded = true;
        if (this.mMessageUri != null) {
            asyncDelete(this.mMessageUri, null, null);
        }
        asyncDeleteDraftSmsMessage(this.mConversation);
        if (this.mConversation != null) {
            this.mConversation.setDraftState(false);
        }
    }

    public SmsImage getImageItem() {
        return this.mImageItem;
    }

    public String getShortenUrl() {
        return this.mShortenUrl;
    }

    public SlideshowModel getSlideshow() {
        return this.mSlideshow;
    }

    public SongItem getSongItem() {
        return this.mSongItem;
    }

    public CharSequence getSubject() {
        return this.mSubject;
    }

    public CharSequence getText() {
        return this.mText;
    }

    public Bitmap getToSendImage() {
        return this.mToSendImage;
    }

    public VoteItem getVoteItem() {
        return this.mVoteItem;
    }

    public List<String> getWorkingRecipients() {
        return this.mWorkingRecipients;
    }

    public String getmShortenUrl() {
        return this.mShortenUrl;
    }

    public boolean hasAttachment() {
        return this.mAttachmentType > 0;
    }

    public boolean hasShortenUrl() {
        return this.mShortenUrl != null && this.mShortenUrl.length() > 0;
    }

    public boolean hasSlideshow() {
        return this.mAttachmentType == 4;
    }

    public boolean hasSubject() {
        return !TextUtils.isEmpty(this.mSubject);
    }

    public boolean hasText() {
        return !TextUtils.isEmpty(this.mText);
    }

    public boolean isDiscarded() {
        return this.mDiscarded;
    }

    public boolean isWorthSaving() {
        if (hasText() || hasSubject() || hasAttachment() || hasSlideshow() || hasShortenUrl() || getToSendImage() != null) {
            return true;
        }
        return (this.mMmsState & 16) > 0;
    }

    public void readStateFromBundle(Bundle bundle) {
        if (bundle != null) {
            setSubject(bundle.getString("subject"), false);
            Uri uri = (Uri) bundle.getParcelable("msg_uri");
            if (uri != null) {
                loadFromUri(uri);
            } else {
                this.mText = bundle.getString("sms_body");
            }
        }
    }

    public boolean requiresMms() {
        return this.mMmsState > 0;
    }

    public Uri saveAsMms(boolean z) {
        if (this.mDiscarded) {
            throw new IllegalStateException("save() called after discard()");
        }
        updateState(16, true, z);
        prepareForSave(true);
        this.mConversation.ensureThreadId();
        this.mConversation.setDraftState(true);
        PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
        SendReq makeSendReq = makeSendReq(this.mConversation, this.mSubject);
        if (this.mMessageUri == null) {
            this.mMessageUri = createDraftMmsMessage(pduPersister, makeSendReq, this.mSlideshow);
        } else {
            updateDraftMmsMessage(this.mMessageUri, pduPersister, this.mSlideshow, makeSendReq);
        }
        return this.mMessageUri;
    }

    public void saveDraft() {
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("saveDraft", new Object[0]);
        }
        if (this.mToSendImage != null && this.mText.toString().length() == 0) {
            this.mText = " ";
        }
        if (!this.mDiscarded) {
            if (this.mConversation == null) {
                throw new IllegalStateException("saveDraft() called with no conversation");
            }
            prepareForSave(false);
            if (requiresMms()) {
                asyncUpdateDraftMmsMessage(this.mConversation);
            } else {
                String obj = this.mText.toString();
                if (hasShortenUrl()) {
                    if (obj.length() > 0) {
                        obj = TextUtils.join(" ", new String[]{obj, this.mShortenUrl});
                    } else {
                        obj = this.mShortenUrl;
                    }
                }
                if (!TextUtils.isEmpty(obj)) {
                    asyncUpdateDraftSmsMessage(this.mConversation, obj);
                }
            }
            this.mConversation.setDraftState(true);
        }
    }

    public void send() {
        if (Log.isLoggable(LogTag.TRANSACTION, 2)) {
            LogTag.debug(MMHttpDefines.TAG_SEND, new Object[0]);
        }
        prepareForSave(true);
        final Conversation conversation = this.mConversation;
        String obj = this.mText.toString();
        if (requiresMms() || addressContainsEmailToMms(conversation, obj)) {
            final Uri uri = this.mMessageUri;
            final PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
            final SlideshowModel slideshowModel = this.mSlideshow;
            final SendReq makeSendReq = makeSendReq(conversation, this.mSubject);
            slideshowModel.prepareForSend();
            new Thread(new Runnable() {
                public void run() {
                    WorkingMessage.this.sendMmsWorker(conversation, uri, pduPersister, slideshowModel, makeSendReq);
                }
            }).start();
        } else {
            final String obj2 = this.mText.toString();
            new Thread(new Runnable() {
                public void run() {
                    String str = obj2;
                    if (WorkingMessage.this.hasShortenUrl()) {
                        if (obj2.length() > 0) {
                            str = TextUtils.join(" ", new String[]{obj2, WorkingMessage.this.mShortenUrl});
                        } else {
                            str = WorkingMessage.this.mShortenUrl;
                        }
                    }
                    WorkingMessage.this.sendSmsWorker(conversation, str);
                }
            }).start();
        }
        RecipientIdCache.updateNumbers(conversation.getThreadId(), conversation.getRecipients());
        this.mDiscarded = true;
    }

    public void sendMusicSms(final ArrayList<String> arrayList) {
        prepareForSave(true);
        final Conversation conversation = this.mConversation;
        new Thread(new Runnable() {
            public void run() {
                WorkingMessage.this.sendMusicSmsWorker(conversation, arrayList);
            }
        }).start();
        RecipientIdCache.updateNumbers(conversation.getThreadId(), conversation.getRecipients());
        this.mDiscarded = true;
    }

    public int setAttachment(int i, Uri uri, boolean z) {
        int i2;
        if (Log.isLoggable(LogTag.APP, 2)) {
            LogTag.debug("setAttachment type=%d uri %s", Integer.valueOf(i), uri);
        }
        ensureSlideshow();
        if (z) {
            try {
                appendMedia(i, uri);
            } catch (MmsException e) {
                i2 = -1;
            } catch (UnsupportContentTypeException e2) {
                i2 = -3;
            } catch (ExceedMessageSizeException e3) {
                i2 = -2;
            } catch (ResolutionException e4) {
                i2 = -4;
            }
        } else {
            changeMedia(i, uri);
        }
        i2 = 0;
        if (i2 == 0) {
            this.mAttachmentType = i;
            this.mStatusListener.onAttachmentChanged();
        } else if (z) {
            new SlideshowEditor(this.mContext, this.mSlideshow).removeSlide(this.mSlideshow.size() - 1);
        }
        updateState(4, hasAttachment(), true);
        return i2;
    }

    public int setAttachment(SongItem songItem) {
        return 0;
    }

    public void setConversation(Conversation conversation) {
        this.mConversation = conversation;
        setHasEmail(conversation.getRecipients().containsEmail(), false);
    }

    public void setHasEmail(boolean z, boolean z2) {
        if (MmsConfig.getEmailGateway() != null) {
            updateState(1, false, z2);
        } else {
            updateState(1, z, z2);
        }
    }

    public void setLengthRequiresMms(boolean z) {
        updateState(8, z, true);
    }

    public void setSubject(CharSequence charSequence, boolean z) {
        this.mSubject = charSequence;
        updateState(2, charSequence != null, z);
    }

    public void setText(CharSequence charSequence) {
        this.mText = charSequence;
    }

    public void setWorkingRecipients(List<String> list) {
        this.mWorkingRecipients = list;
    }

    public void setmShortenUrl(String str) {
        this.mShortenUrl = str;
    }

    public void syncWorkingRecipients() {
        if (this.mWorkingRecipients != null) {
            this.mConversation.setRecipients(ContactList.getByNumbers(this.mWorkingRecipients, false, this.mContext));
            this.mWorkingRecipients = null;
        }
    }

    public void unDiscard() {
        this.mDiscarded = false;
    }

    @Deprecated
    public void updateImageShortenUrl(String str, SmsImage smsImage) {
        if (str == null) {
            clearShortenUrl();
            return;
        }
        this.mShortenUrl = Util.getFullShortenImageUrl(this.mContext, str);
        if (smsImage == null) {
            ImagesDbService.ImageItem queryByPlug = new ImagesDbService(this.mContext).queryByPlug(str);
            if (queryByPlug != null) {
                SmsImage smsImage2 = new SmsImage();
                smsImage2.mPlug = str;
                smsImage2.mUrl = queryByPlug.url;
                smsImage2.mThumb = queryByPlug.thumb;
                this.mImageItem = smsImage2;
                return;
            }
            return;
        }
        this.mImageItem = smsImage;
    }

    public void updateSongShortenUrl(String str, SongItem songItem) {
        if (str == null) {
            clearShortenUrl();
            return;
        }
        this.mShortenUrl = Util.getFullShortenUrl(this.mContext, str);
        if (songItem == null) {
            this.mSongItem = new TopMusicService(this.mContext).getSongItemByPlug(str);
        } else {
            this.mSongItem = songItem;
        }
    }

    public void updateToSendImage(Bitmap bitmap) {
        clearShortenUrl();
        if (bitmap == null) {
            this.mToSendImage = null;
            return;
        }
        if (this.mToSendImage != null) {
            this.mToSendImage.recycle();
            this.mToSendImage = null;
        }
        this.mToSendImage = bitmap;
    }

    public void updateVoteShortenUrl(String str, VoteItem voteItem) {
        if (str == null) {
            clearShortenUrl();
            return;
        }
        this.mShortenUrl = Util.getFullShortenVoteUrl(this.mContext, str);
        if (voteItem == null) {
            Cursor query = this.mContext.getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " plug = ? ", new String[]{this.mShortenUrl}, null);
            if (query.getCount() > 0) {
                this.mVoteItem = new VoteItem();
                if (query.moveToNext()) {
                    this.mVoteItem.cli_id = (long) query.getInt(query.getColumnIndex("_id"));
                    this.mVoteItem.title = query.getString(query.getColumnIndex("title"));
                    this.mVoteItem.plug = this.mShortenUrl;
                }
                query.close();
                Cursor query2 = this.mContext.getContentResolver().query(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null, " vote_id = ? ", new String[]{String.valueOf(this.mVoteItem.cli_id)}, " display_order asc ");
                while (query2.moveToNext()) {
                    VoteOption voteOption = new VoteOption();
                    voteOption.display_order = query2.getInt(query2.getColumnIndex("display_order"));
                    voteOption.cli_id = (long) query2.getInt(query2.getColumnIndex("_id"));
                    voteOption.service_id = query2.getString(query2.getColumnIndex("service_id"));
                    voteOption.content = query2.getString(query2.getColumnIndex("option_text"));
                    this.mVoteItem.options.add(voteOption);
                }
                query2.close();
                return;
            }
            return;
        }
        this.mVoteItem = voteItem;
    }

    public void writeStateToBundle(Bundle bundle) {
        if (hasSubject()) {
            bundle.putString("subject", this.mSubject.toString());
        }
        if (this.mMessageUri != null) {
            bundle.putParcelable("msg_uri", this.mMessageUri);
        } else if (hasText()) {
            bundle.putString("sms_body", this.mText.toString());
        }
    }
}
