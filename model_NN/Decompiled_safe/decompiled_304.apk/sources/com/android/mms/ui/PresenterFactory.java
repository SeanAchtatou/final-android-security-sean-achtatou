package com.android.mms.ui;

public class PresenterFactory {
    private static final String PRESENTER_PACKAGE = "com.android.mms.ui.";
    private static final String TAG = "PresenterFactory";

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0068, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0069, code lost:
        android.util.Log.e(com.android.mms.ui.PresenterFactory.TAG, "No such constructor.", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0071, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0072, code lost:
        android.util.Log.e(com.android.mms.ui.PresenterFactory.TAG, "Unexpected InvocationTargetException", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x007b, code lost:
        android.util.Log.e(com.android.mms.ui.PresenterFactory.TAG, "Unexpected IllegalAccessException", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0083, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0084, code lost:
        android.util.Log.e(com.android.mms.ui.PresenterFactory.TAG, "Unexpected InstantiationException", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x008c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008d, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0068 A[ExcHandler: NoSuchMethodException (r0v4 'e' java.lang.NoSuchMethodException A[CUSTOM_DECLARE]), Splitter:B:1:0x0004] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0071 A[ExcHandler: InvocationTargetException (r0v3 'e' java.lang.reflect.InvocationTargetException A[CUSTOM_DECLARE]), Splitter:B:1:0x0004] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007a A[ExcHandler: IllegalAccessException (r0v2 'e' java.lang.IllegalAccessException A[CUSTOM_DECLARE]), Splitter:B:1:0x0004] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0083 A[ExcHandler: InstantiationException (r0v1 'e' java.lang.InstantiationException A[CUSTOM_DECLARE]), Splitter:B:1:0x0004] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.android.mms.ui.Presenter getPresenter(java.lang.String r7, android.content.Context r8, com.android.mms.ui.ViewInterface r9, com.android.mms.model.Model r10) {
        /*
            java.lang.String r5 = "PresenterFactory"
            java.lang.String r0 = "."
            int r0 = r7.indexOf(r0)     // Catch:{ ClassNotFoundException -> 0x004c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r1 = -1
            if (r0 != r1) goto L_0x0091
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x004c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r0.<init>()     // Catch:{ ClassNotFoundException -> 0x004c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            java.lang.String r1 = "com.android.mms.ui."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ ClassNotFoundException -> 0x004c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ ClassNotFoundException -> 0x004c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClassNotFoundException -> 0x004c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
        L_0x001e:
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r2 = 3
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r3 = 0
            java.lang.Class<android.content.Context> r4 = android.content.Context.class
            r2[r3] = r4     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r3 = 1
            java.lang.Class<com.android.mms.ui.ViewInterface> r4 = com.android.mms.ui.ViewInterface.class
            r2[r3] = r4     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r3 = 2
            java.lang.Class<com.android.mms.model.Model> r4 = com.android.mms.model.Model.class
            r2[r3] = r4     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            java.lang.reflect.Constructor r1 = r1.getConstructor(r2)     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r3 = 0
            r2[r3] = r8     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r3 = 1
            r2[r3] = r9     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r3 = 2
            r2[r3] = r10     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            java.lang.Object r7 = r1.newInstance(r2)     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            com.android.mms.ui.Presenter r7 = (com.android.mms.ui.Presenter) r7     // Catch:{ ClassNotFoundException -> 0x008c, NoSuchMethodException -> 0x0068, InvocationTargetException -> 0x0071, IllegalAccessException -> 0x007a, InstantiationException -> 0x0083 }
            r0 = r7
        L_0x004b:
            return r0
        L_0x004c:
            r0 = move-exception
            r1 = r7
        L_0x004e:
            java.lang.String r2 = "PresenterFactory"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Type not found: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r5, r1, r0)
        L_0x0066:
            r0 = 0
            goto L_0x004b
        L_0x0068:
            r0 = move-exception
            java.lang.String r1 = "PresenterFactory"
            java.lang.String r1 = "No such constructor."
            android.util.Log.e(r5, r1, r0)
            goto L_0x0066
        L_0x0071:
            r0 = move-exception
            java.lang.String r1 = "PresenterFactory"
            java.lang.String r1 = "Unexpected InvocationTargetException"
            android.util.Log.e(r5, r1, r0)
            goto L_0x0066
        L_0x007a:
            r0 = move-exception
            java.lang.String r1 = "PresenterFactory"
            java.lang.String r1 = "Unexpected IllegalAccessException"
            android.util.Log.e(r5, r1, r0)
            goto L_0x0066
        L_0x0083:
            r0 = move-exception
            java.lang.String r1 = "PresenterFactory"
            java.lang.String r1 = "Unexpected InstantiationException"
            android.util.Log.e(r5, r1, r0)
            goto L_0x0066
        L_0x008c:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x004e
        L_0x0091:
            r0 = r7
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.ui.PresenterFactory.getPresenter(java.lang.String, android.content.Context, com.android.mms.ui.ViewInterface, com.android.mms.model.Model):com.android.mms.ui.Presenter");
    }
}
