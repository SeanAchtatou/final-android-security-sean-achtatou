package com.android.mms.ui;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.mms.model.IModelChangedObserver;
import com.android.mms.model.Model;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduPersister;
import vc.lx.sms2.R;

public class SlideshowEditActivity extends ListActivity {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final int MENU_ADD_SLIDE = 3;
    private static final int MENU_DISCARD_SLIDESHOW = 4;
    private static final int MENU_MOVE_DOWN = 1;
    private static final int MENU_MOVE_UP = 0;
    private static final int MENU_REMOVE_SLIDE = 2;
    private static final String MESSAGE_URI = "message_uri";
    private static final int REQUEST_CODE_EDIT_SLIDE = 6;
    private static final String SLIDE_INDEX = "slide_index";
    private static final String STATE = "state";
    private static final String TAG = "SlideshowEditActivity";
    /* access modifiers changed from: private */
    public boolean mDirty;
    private ListView mList;
    private final IModelChangedObserver mModelChangedObserver = new IModelChangedObserver() {
        public void onModelChanged(Model model, boolean z) {
            synchronized (SlideshowEditActivity.this) {
                boolean unused = SlideshowEditActivity.this.mDirty = true;
            }
            SlideshowEditActivity.this.setResult(-1, SlideshowEditActivity.this.mResultIntent);
        }
    };
    /* access modifiers changed from: private */
    public Intent mResultIntent;
    private SlideListAdapter mSlideListAdapter;
    private SlideshowEditor mSlideshowEditor = null;
    private SlideshowModel mSlideshowModel = null;
    private Bundle mState;
    private Uri mUri;

    private static class SlideListAdapter extends ArrayAdapter<SlideModel> {
        private final Context mContext;
        private final LayoutInflater mInflater;
        private final int mResource;
        private final SlideshowModel mSlideshow;

        public SlideListAdapter(Context context, int i, SlideshowModel slideshowModel) {
            super(context, i, slideshowModel);
            this.mContext = context;
            this.mResource = i;
            this.mInflater = LayoutInflater.from(context);
            this.mSlideshow = slideshowModel;
        }

        private View createViewFromResource(int i, View view, int i2) {
            SlideListItemView slideListItemView = (SlideListItemView) this.mInflater.inflate(i2, (ViewGroup) null);
            ((TextView) slideListItemView.findViewById(R.id.slide_number_text)).setText(this.mContext.getString(R.string.slide_number, Integer.valueOf(i + 1)));
            int duration = ((SlideModel) getItem(i)).getDuration() / 1000;
            ((TextView) slideListItemView.findViewById(R.id.duration_text)).setText(this.mContext.getResources().getQuantityString(R.plurals.slide_duration, duration, Integer.valueOf(duration)));
            Presenter presenter = PresenterFactory.getPresenter("SlideshowPresenter", this.mContext, slideListItemView, this.mSlideshow);
            ((SlideshowPresenter) presenter).setLocation(i);
            presenter.present();
            return slideListItemView;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return createViewFromResource(i, view, this.mResource);
        }
    }

    private void addNewSlide() {
        if (this.mSlideshowEditor.addNewSlide()) {
            this.mSlideListAdapter.notifyDataSetChanged();
            this.mList.requestFocus();
            this.mList.setSelection(this.mSlideshowModel.size() - 1);
            return;
        }
        Toast.makeText(this, (int) R.string.cannot_add_slide_anymore, 0).show();
    }

    private void cleanupSlideshowModel() {
        if (this.mSlideshowModel != null) {
            this.mSlideshowModel.unregisterModelChangedObserver(this.mModelChangedObserver);
            this.mSlideshowModel = null;
        }
    }

    private View createAddSlideItem() {
        View inflate = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.slideshow_edit_item, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.slide_number_text)).setText((int) R.string.add_slide);
        TextView textView = (TextView) inflate.findViewById(R.id.text_preview);
        textView.setText((int) R.string.add_slide_hint);
        textView.setVisibility(0);
        ((ImageView) inflate.findViewById(R.id.image_preview)).setImageResource(R.drawable.ic_launcher_slideshow_add_sms);
        return inflate;
    }

    private void initSlideList() throws MmsException {
        cleanupSlideshowModel();
        this.mSlideshowModel = SlideshowModel.createFromMessageUri(this, this.mUri);
        this.mSlideshowModel.registerModelChangedObserver(this.mModelChangedObserver);
        this.mSlideshowEditor = new SlideshowEditor(this, this.mSlideshowModel);
        this.mSlideListAdapter = new SlideListAdapter(this, R.layout.slideshow_edit_item, this.mSlideshowModel);
        this.mList.setAdapter((ListAdapter) this.mSlideListAdapter);
    }

    private void openSlide(int i) {
        Intent intent = new Intent(this, SlideEditorActivity.class);
        intent.setData(this.mUri);
        intent.putExtra("slide_index", i);
        startActivityForResult(intent, 6);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1) {
            switch (i) {
                case 6:
                    synchronized (this) {
                        this.mDirty = true;
                    }
                    setResult(-1, this.mResultIntent);
                    if (intent == null || !intent.getBooleanExtra("done", false)) {
                        try {
                            initSlideList();
                            return;
                        } catch (MmsException e) {
                            Log.e(TAG, "Failed to initialize the slide-list.", e);
                            finish();
                            return;
                        }
                    } else {
                        finish();
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mList = getListView();
        this.mList.addFooterView(createAddSlideItem());
        if (bundle != null) {
            this.mState = bundle.getBundle("state");
        }
        if (this.mState != null) {
            this.mUri = Uri.parse(this.mState.getString(MESSAGE_URI));
        } else {
            this.mUri = getIntent().getData();
        }
        if (this.mUri == null) {
            Log.e(TAG, "Cannot startup activity, null Uri.");
            finish();
            return;
        }
        this.mResultIntent = new Intent();
        this.mResultIntent.setData(this.mUri);
        try {
            initSlideList();
        } catch (MmsException e) {
            Log.e(TAG, "Failed to initialize the slide-list.", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        cleanupSlideshowModel();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        if (i == listView.getCount() - 1) {
            addNewSlide();
        } else {
            openSlide(i);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int selectedItemPosition = this.mList.getSelectedItemPosition();
        switch (menuItem.getItemId()) {
            case 0:
                if (selectedItemPosition > 0 && selectedItemPosition < this.mSlideshowModel.size()) {
                    this.mSlideshowEditor.moveSlideUp(selectedItemPosition);
                    this.mSlideListAdapter.notifyDataSetChanged();
                    this.mList.setSelection(selectedItemPosition - 1);
                    break;
                }
            case 1:
                if (selectedItemPosition >= 0 && selectedItemPosition < this.mSlideshowModel.size() - 1) {
                    this.mSlideshowEditor.moveSlideDown(selectedItemPosition);
                    this.mSlideListAdapter.notifyDataSetChanged();
                    this.mList.setSelection(selectedItemPosition + 1);
                    break;
                }
            case 2:
                if (selectedItemPosition >= 0 && selectedItemPosition < this.mSlideshowModel.size()) {
                    this.mSlideshowEditor.removeSlide(selectedItemPosition);
                    this.mSlideListAdapter.notifyDataSetChanged();
                    break;
                }
            case 3:
                addNewSlide();
                break;
            case 4:
                this.mSlideshowEditor.removeAllSlides();
                this.mSlideListAdapter.notifyDataSetChanged();
                finish();
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        synchronized (this) {
            if (this.mDirty) {
                try {
                    PduBody pduBody = this.mSlideshowModel.toPduBody();
                    PduPersister.getPduPersister(this).updateParts(this.mUri, pduBody);
                    this.mSlideshowModel.sync(pduBody);
                } catch (MmsException e) {
                    Log.e(TAG, "Cannot update the message: " + this.mUri, e);
                }
            }
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        int selectedItemPosition = this.mList.getSelectedItemPosition();
        if (selectedItemPosition < 0 || selectedItemPosition == this.mList.getCount() - 1) {
            menu.add(0, 3, 0, (int) R.string.add_slide).setIcon((int) R.drawable.ic_menu_add_slide);
        } else {
            if (selectedItemPosition > 0) {
                menu.add(0, 0, 0, (int) R.string.move_up).setIcon((int) R.drawable.ic_menu_move_up);
            }
            if (selectedItemPosition < this.mSlideListAdapter.getCount() - 1) {
                menu.add(0, 1, 0, (int) R.string.move_down).setIcon((int) R.drawable.ic_menu_move_down);
            }
            menu.add(0, 3, 0, (int) R.string.add_slide).setIcon((int) R.drawable.ic_menu_add_slide);
            menu.add(0, 2, 0, (int) R.string.remove_slide).setIcon(17301564);
        }
        menu.add(0, 4, 0, (int) R.string.discard_slideshow).setIcon((int) R.drawable.ic_menu_delete_played);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mState != null) {
            this.mList.setSelection(this.mState.getInt("slide_index", 0));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mState = new Bundle();
        if (this.mList.getSelectedItemPosition() >= 0) {
            this.mState.putInt("slide_index", this.mList.getSelectedItemPosition());
        }
        if (this.mUri != null) {
            this.mState.putString(MESSAGE_URI, this.mUri.toString());
        }
        bundle.putBundle("state", this.mState);
    }
}
