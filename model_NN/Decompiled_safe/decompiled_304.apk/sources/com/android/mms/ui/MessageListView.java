package com.android.mms.ui;

import android.content.Context;
import android.os.SystemProperties;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ListView;

public final class MessageListView extends ListView {
    public MessageListView(Context context) {
        super(context);
    }

    public MessageListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MessageListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean onKeyShortcut(int i, KeyEvent keyEvent) {
        MessageItem messageItem;
        switch (i) {
            case SystemProperties.PROP_NAME_MAX:
                MessageListItem messageListItem = (MessageListItem) getSelectedView();
                if (!(messageListItem == null || (messageItem = messageListItem.getMessageItem()) == null || !messageItem.isSms())) {
                    ((ClipboardManager) getContext().getSystemService("clipboard")).setText(messageItem.mBody);
                    return true;
                }
        }
        return super.onKeyShortcut(i, keyEvent);
    }
}
