package com.android.mms.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import java.util.List;
import vc.lx.sms2.R;

public class DeliveryReportAdapter extends ArrayAdapter<DeliveryReportItem> {
    static final String LOG_TAG = "DeliveryReportAdapter";

    public DeliveryReportAdapter(Context context, List<DeliveryReportItem> list) {
        super(context, (int) R.layout.delivery_report_list_item, (int) R.id.recipient, list);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        DeliveryReportListItem deliveryReportListItem;
        DeliveryReportItem deliveryReportItem = (DeliveryReportItem) getItem(i);
        if (view == null) {
            deliveryReportListItem = (DeliveryReportListItem) LayoutInflater.from(getContext()).inflate((int) R.layout.delivery_report_list_item, viewGroup, false);
        } else if (!(view instanceof DeliveryReportListItem)) {
            return view;
        } else {
            deliveryReportListItem = (DeliveryReportListItem) view;
        }
        deliveryReportListItem.bind(deliveryReportItem.recipient, deliveryReportItem.status);
        return deliveryReportListItem;
    }
}
