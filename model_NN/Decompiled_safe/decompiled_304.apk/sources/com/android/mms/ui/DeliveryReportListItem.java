package com.android.mms.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import vc.lx.sms.data.Contact;
import vc.lx.sms2.R;

public class DeliveryReportListItem extends LinearLayout {
    private ImageView mIconView;
    private TextView mRecipientView;
    private TextView mStatusView;

    DeliveryReportListItem(Context context) {
        super(context);
    }

    public DeliveryReportListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void bind(String str, String str2) {
        this.mRecipientView.setText(Contact.get(str, false, getContext()).getName());
        this.mStatusView.setText(str2);
        Context context = getContext();
        String string = context.getString(R.string.status_received);
        String string2 = context.getString(R.string.status_failed);
        String string3 = context.getString(R.string.status_pending);
        String string4 = context.getString(R.string.status_rejected);
        if (str2.compareTo(string) == 0) {
            this.mIconView.setImageResource(R.drawable.ic_sms_mms_delivered);
        } else if (str2.compareTo(string2) == 0) {
            this.mIconView.setImageResource(R.drawable.ic_sms_mms_not_delivered);
        } else if (str2.compareTo(string3) == 0) {
            this.mIconView.setImageResource(R.drawable.ic_sms_mms_pending);
        } else if (str2.compareTo(string4) == 0) {
            this.mIconView.setImageResource(R.drawable.ic_sms_mms_not_delivered);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.mRecipientView = (TextView) findViewById(R.id.recipient);
        this.mStatusView = (TextView) findViewById(R.id.status);
        this.mIconView = (ImageView) findViewById(R.id.icon);
    }
}
