package com.android.mms.ui;

import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.android.provider.Telephony;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms.data.Contact;
import vc.lx.sms.data.ContactList;
import vc.lx.sms.ui.ComposeMessageActivity;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms2.R;
import vc.lx.sms2.SmsApplication;

public class SearchActivity extends ListActivity {
    private static final String[] ALL_THREADS_PROJECTION = {"_id", "date", Telephony.ThreadsColumns.MESSAGE_COUNT, Telephony.ThreadsColumns.RECIPIENT_IDS, "snippet", Telephony.ThreadsColumns.SNIPPET_CHARSET, "read", Telephony.ThreadsColumns.ERROR, Telephony.ThreadsColumns.HAS_ATTACHMENT};
    private static final int MESSAGE_COUNT = 2;
    private static final int RECIPIENT_IDS = 3;
    private static final int SNIPPET = 4;
    private static final int SNIPPET_CS = 5;
    private static final int THREAD_LIST_QUERY_TOKEN = 1701;
    private static final Uri sAllThreadsUri = Telephony.Threads.CONTENT_URI.buildUpon().appendQueryParameter("simple", "true").build();
    private Button mBackBtn;
    AsyncQueryHandler mQueryContextHandler;
    AsyncQueryHandler mQueryHandler;
    public View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            SearchActivity.this.finish();
        }
    };

    class ContactNumQuerAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public List<SearchModel> data;
        private LayoutInflater inflater = null;
        /* access modifiers changed from: private */
        public String mSearchString = "";

        public ContactNumQuerAdapter(List<SearchModel> list, String str) {
            this.data = list;
            this.inflater = (LayoutInflater) SearchActivity.this.getApplication().getSystemService("layout_inflater");
            this.mSearchString = str;
        }

        public int getCount() {
            if (this.data.size() > 0) {
                return this.data.size();
            }
            return 0;
        }

        public Object getItem(int i) {
            return this.data.get(i);
        }

        public long getItemId(int i) {
            return this.data.get(i).threadId;
        }

        public View getView(final int i, View view, ViewGroup viewGroup) {
            SearchItemView searchItemView;
            View view2;
            if (view == null) {
                searchItemView = new SearchItemView();
                view2 = this.inflater.inflate((int) R.layout.search_item, (ViewGroup) null);
                searchItemView.mTitleView = (TextView) view2.findViewById(R.id.title);
                searchItemView.mBodyView = (TextView) view2.findViewById(R.id.subtitle);
                searchItemView.mDateView = (TextView) view2.findViewById(R.id.date);
                view2.setTag(searchItemView);
            } else {
                searchItemView = (SearchItemView) view.getTag();
                view2 = view;
            }
            searchItemView.mTitleView.setText(this.data.get(i).title);
            searchItemView.mBodyView.setText(this.data.get(i).body);
            searchItemView.mDateView.setText(this.data.get(i).date);
            view2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(SearchActivity.this, ComposeMessageActivity.class);
                    intent.putExtra("thread_id", ((SearchModel) ContactNumQuerAdapter.this.data.get(i)).threadId);
                    intent.putExtra("highlight", ContactNumQuerAdapter.this.mSearchString);
                    SearchActivity.this.startActivity(intent);
                }
            });
            view2.setClickable(true);
            view2.setFocusable(true);
            return view2;
        }
    }

    class SearchItemView {
        public TextView mBodyView = null;
        public TextView mDateView = null;
        public TextView mTitleView = null;

        SearchItemView() {
        }
    }

    class SearchModel {
        public String body;
        public String date;
        public long threadId;
        public String title;

        SearchModel() {
        }
    }

    private void initContactNumQuery(final String str, ContentResolver contentResolver, final ListView listView) {
        this.mQueryContextHandler = new AsyncQueryHandler(contentResolver) {
            /* access modifiers changed from: protected */
            public void onQueryComplete(int i, Object obj, Cursor cursor) {
                ArrayList arrayList = new ArrayList();
                while (cursor.moveToNext()) {
                    String valueOf = String.valueOf(cursor.getInt(cursor.getColumnIndex(Telephony.ThreadsColumns.MESSAGE_COUNT)));
                    SearchModel searchModel = new SearchModel();
                    String extractEncStrFromCursor = MessageUtils.extractEncStrFromCursor(cursor, 4, 5);
                    if (TextUtils.isEmpty(extractEncStrFromCursor)) {
                        extractEncStrFromCursor = SearchActivity.this.getApplicationContext().getString(R.string.no_subject_view);
                    }
                    searchModel.body = extractEncStrFromCursor;
                    ContactList byIds = ContactList.getByIds(cursor.getString(3), false, SearchActivity.this.getApplicationContext());
                    String name = ((byIds.get(0) == null || ((Contact) byIds.get(0)).getName() != null) && !"".equals(((Contact) byIds.get(0)).getName())) ? ((Contact) byIds.get(0)).getName() : ((Contact) byIds.get(0)).getNumber();
                    searchModel.title = name + " (" + valueOf + ")";
                    searchModel.threadId = cursor.getLong(cursor.getColumnIndex("_id"));
                    searchModel.date = MessageUtils.formatTimeStampString(SearchActivity.this.getApplicationContext(), cursor.getLong(cursor.getColumnIndex("date")));
                    if (name.contains(str)) {
                        arrayList.add(searchModel);
                    }
                }
                SearchActivity.this.setListAdapter(new ContactNumQuerAdapter(arrayList, str));
                listView.setFocusable(true);
                listView.setFocusableInTouchMode(true);
                listView.requestFocus();
            }
        };
    }

    private void initMsgContentQuery(final String str, ContentResolver contentResolver, final ListView listView) {
        this.mQueryHandler = new AsyncQueryHandler(contentResolver) {
            /* access modifiers changed from: protected */
            public void onQueryComplete(int i, Object obj, Cursor cursor) {
                final int columnIndex = cursor.getColumnIndex("thread_id");
                final int columnIndex2 = cursor.getColumnIndex("address");
                final int columnIndex3 = cursor.getColumnIndex(Telephony.TextBasedSmsColumns.BODY);
                final int columnIndex4 = cursor.getColumnIndex("_id");
                final int columnIndex5 = cursor.getColumnIndex("date");
                int count = cursor.getCount();
                SearchActivity.this.setTitle(SearchActivity.this.getResources().getQuantityString(R.plurals.search_results_title, count, Integer.valueOf(count), str));
                SearchActivity.this.setListAdapter(new CursorAdapter(SearchActivity.this, cursor) {
                    public void bindView(View view, Context context, Cursor cursor) {
                        final TextView textView = (TextView) view.findViewById(R.id.title);
                        Contact contact = Contact.get(cursor.getString(columnIndex2), false, SearchActivity.this);
                        contact.addListener(new Contact.UpdateListener() {
                            public void onUpdate(final Contact contact) {
                                SearchActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        textView.setText(contact.getName());
                                    }
                                });
                            }
                        });
                        textView.setText(contact.getName());
                        ((TextView) view.findViewById(R.id.date)).setText(MessageUtils.formatTimeStampString(SearchActivity.this.getApplicationContext(), cursor.getLong(columnIndex5)));
                        ((TextView) view.findViewById(R.id.subtitle)).setText(cursor.getString(columnIndex3));
                        final long j = cursor.getLong(columnIndex);
                        final long j2 = cursor.getLong(columnIndex4);
                        view.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                Intent intent = new Intent(SearchActivity.this, ComposeMessageActivity.class);
                                intent.putExtra("thread_id", j);
                                intent.putExtra("highlight", str);
                                intent.putExtra("select_id", j2);
                                SearchActivity.this.startActivity(intent);
                            }
                        });
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
                     arg types: [?, android.view.ViewGroup, int]
                     candidates:
                      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
                      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
                    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                        return LayoutInflater.from(context).inflate((int) R.layout.search_item, viewGroup, false);
                    }
                });
                listView.setFocusable(true);
                listView.setFocusableInTouchMode(true);
                listView.requestFocus();
            }
        };
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.search_activity);
        String trim = getIntent().getStringExtra(PrefsUtil.KEY_SMS_SEARCH_KEY).trim();
        String trim2 = trim != null ? trim.trim() : trim;
        ContentResolver contentResolver = getContentResolver();
        this.mBackBtn = (Button) findViewById(R.id.back);
        this.mBackBtn.setOnClickListener(this.onClickListener);
        ListView listView = getListView();
        listView.setSelector(17301602);
        listView.setItemsCanFocus(true);
        listView.setFocusable(true);
        listView.setClickable(true);
        setTitle("");
        if (SmsApplication.getInstance().isSearchByContact()) {
            initMsgContentQuery(trim2, contentResolver, listView);
            this.mQueryHandler.startQuery(0, null, Uri.parse("content://sms"), null, "address NOTNULL and body like  ?", new String[]{"%" + trim2 + "%"}, null);
            return;
        }
        initContactNumQuery(trim2, contentResolver, listView);
        this.mQueryContextHandler.cancelOperation(THREAD_LIST_QUERY_TOKEN);
        this.mQueryContextHandler.startQuery(THREAD_LIST_QUERY_TOKEN, null, sAllThreadsUri, ALL_THREADS_PROJECTION, null, null, "date DESC");
    }
}
