package com.android.mms.ui;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.android.mms.data.WorkingMessage;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import vc.lx.sms2.R;

public class AttachmentEditor extends LinearLayout {
    public static final int MSG_EDIT_SLIDESHOW = 1;
    public static final int MSG_PLAY_AUDIO = 8;
    public static final int MSG_PLAY_SLIDESHOW = 3;
    public static final int MSG_PLAY_VIDEO = 7;
    public static final int MSG_REMOVE_ATTACHMENT = 10;
    public static final int MSG_REPLACE_AUDIO = 6;
    public static final int MSG_REPLACE_IMAGE = 4;
    public static final int MSG_REPLACE_VIDEO = 5;
    public static final int MSG_SEND_SLIDESHOW = 2;
    public static final int MSG_VIEW_IMAGE = 9;
    private static final String TAG = "AttachmentEditor";
    private boolean mCanSend;
    private final Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private Presenter mPresenter;
    private Button mSendButton;
    private SlideshowModel mSlideshow;
    private SlideViewInterface mView;

    private class MessageOnClick implements View.OnClickListener {
        private int mWhat;

        public MessageOnClick(int i) {
            this.mWhat = i;
        }

        public void onClick(View view) {
            Message.obtain(AttachmentEditor.this.mHandler, this.mWhat).sendToTarget();
        }
    }

    public AttachmentEditor(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
    }

    private SlideViewInterface createMediaView(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        LinearLayout linearLayout = (LinearLayout) getStubView(i, i2);
        linearLayout.setVisibility(0);
        ((Button) linearLayout.findViewById(i3)).setOnClickListener(new MessageOnClick(i6));
        ((Button) linearLayout.findViewById(i4)).setOnClickListener(new MessageOnClick(i7));
        ((Button) linearLayout.findViewById(i5)).setOnClickListener(new MessageOnClick(i8));
        return (SlideViewInterface) linearLayout;
    }

    private SlideViewInterface createSlideshowView(boolean z) {
        LinearLayout linearLayout = (LinearLayout) getStubView(z ? R.id.slideshow_attachment_view_portrait_stub : R.id.slideshow_attachment_view_landscape_stub, z ? R.id.slideshow_attachment_view_portrait : R.id.slideshow_attachment_view_landscape);
        linearLayout.setVisibility(0);
        this.mSendButton = (Button) linearLayout.findViewById(R.id.send_slideshow_button);
        updateSendButton();
        ((Button) linearLayout.findViewById(R.id.edit_slideshow_button)).setOnClickListener(new MessageOnClick(1));
        this.mSendButton.setOnClickListener(new MessageOnClick(2));
        ((ImageButton) linearLayout.findViewById(R.id.play_slideshow_button)).setOnClickListener(new MessageOnClick(3));
        return (SlideViewInterface) linearLayout;
    }

    private SlideViewInterface createView() {
        boolean inPortraitMode = inPortraitMode();
        if (this.mSlideshow.size() > 1) {
            return createSlideshowView(inPortraitMode);
        }
        SlideModel slideModel = this.mSlideshow.get(0);
        if (slideModel.hasImage()) {
            return createMediaView(inPortraitMode ? R.id.image_attachment_view_portrait_stub : R.id.image_attachment_view_landscape_stub, inPortraitMode ? R.id.image_attachment_view_portrait : R.id.image_attachment_view_landscape, R.id.view_image_button, R.id.replace_image_button, R.id.remove_image_button, 9, 4, 10);
        } else if (slideModel.hasVideo()) {
            return createMediaView(inPortraitMode ? R.id.video_attachment_view_portrait_stub : R.id.video_attachment_view_landscape_stub, inPortraitMode ? R.id.video_attachment_view_portrait : R.id.video_attachment_view_landscape, R.id.view_video_button, R.id.replace_video_button, R.id.remove_video_button, 7, 5, 10);
        } else if (slideModel.hasAudio()) {
            return createMediaView(inPortraitMode ? R.id.audio_attachment_view_portrait_stub : R.id.audio_attachment_view_landscape_stub, inPortraitMode ? R.id.audio_attachment_view_portrait : R.id.audio_attachment_view_landscape, R.id.play_audio_button, R.id.replace_audio_button, R.id.remove_audio_button, 8, 6, 10);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private View getStubView(int i, int i2) {
        View findViewById = findViewById(i2);
        return findViewById == null ? ((ViewStub) findViewById(i)).inflate() : findViewById;
    }

    private boolean inPortraitMode() {
        return this.mContext.getResources().getConfiguration().orientation == 1;
    }

    private void updateSendButton() {
        if (this.mSendButton != null) {
            this.mSendButton.setEnabled(this.mCanSend);
            this.mSendButton.setFocusable(this.mCanSend);
        }
    }

    public void hideView() {
        if (this.mView != null) {
            ((View) this.mView).setVisibility(8);
        }
    }

    public void setCanSend(boolean z) {
        if (this.mCanSend != z) {
            this.mCanSend = z;
            updateSendButton();
        }
    }

    public void setHandler(Handler handler) {
        this.mHandler = handler;
    }

    public void update(WorkingMessage workingMessage) {
        hideView();
        this.mView = null;
        if (workingMessage.hasAttachment()) {
            this.mSlideshow = workingMessage.getSlideshow();
            this.mView = createView();
            if (this.mPresenter == null || !this.mSlideshow.equals(this.mPresenter.getModel())) {
                this.mPresenter = PresenterFactory.getPresenter("MmsThumbnailPresenter", this.mContext, this.mView, this.mSlideshow);
            } else {
                this.mPresenter.setView(this.mView);
            }
            this.mPresenter.present();
        }
    }
}
