package com.android.mms.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import com.android.mms.ExceedMessageSizeException;
import com.android.mms.MmsConfig;
import com.android.mms.ResolutionException;
import com.android.mms.UnsupportContentTypeException;
import com.android.mms.model.IModelChangedObserver;
import com.android.mms.model.Model;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.ui.BasicSlideEditorView;
import com.google.android.mms.ContentType;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.PduBody;
import com.google.android.mms.pdu.PduPart;
import com.google.android.mms.pdu.PduPersister;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms2.R;

public class SlideEditorActivity extends Activity {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final int MENU_ADD_AUDIO = 4;
    private static final int MENU_ADD_PICTURE = 1;
    private static final int MENU_ADD_SLIDE = 7;
    private static final int MENU_ADD_VIDEO = 6;
    private static final int MENU_DEL_AUDIO = 5;
    private static final int MENU_DEL_PICTURE = 3;
    private static final int MENU_DEL_VIDEO = 8;
    private static final int MENU_DURATION = 10;
    private static final int MENU_LAYOUT = 9;
    private static final int MENU_PREVIEW_SLIDESHOW = 11;
    private static final int MENU_RECORD_SOUND = 12;
    private static final int MENU_REMOVE_TEXT = 0;
    private static final int MENU_SUB_AUDIO = 13;
    private static final int MENU_TAKE_PICTURE = 2;
    private static final String MESSAGE_URI = "message_uri";
    private static final int NUM_DIRECT_DURATIONS = 10;
    private static final int REQUEST_CODE_CHANGE_DURATION = 6;
    private static final int REQUEST_CODE_CHANGE_MUSIC = 3;
    private static final int REQUEST_CODE_CHANGE_PICTURE = 1;
    private static final int REQUEST_CODE_CHANGE_VIDEO = 5;
    private static final int REQUEST_CODE_EDIT_TEXT = 0;
    private static final int REQUEST_CODE_RECORD_SOUND = 4;
    private static final int REQUEST_CODE_TAKE_PICTURE = 2;
    public static final String SLIDE_INDEX = "slide_index";
    private static final String TAG = "SlideEditorActivity";
    /* access modifiers changed from: private */
    public boolean mDirty;
    private Button mDone;
    private final View.OnClickListener mDoneClickListener = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("done", true);
            SlideEditorActivity.this.setResult(-1, intent);
            SlideEditorActivity.this.finish();
        }
    };
    private final IModelChangedObserver mModelChangedObserver = new IModelChangedObserver() {
        public void onModelChanged(Model model, boolean z) {
            synchronized (SlideEditorActivity.this) {
                boolean unused = SlideEditorActivity.this.mDirty = true;
            }
            SlideEditorActivity.this.setResult(-1);
        }
    };
    private ImageButton mNextSlide;
    private final View.OnClickListener mOnNavigateBackward = new View.OnClickListener() {
        public void onClick(View view) {
            if (SlideEditorActivity.this.mPosition > 0) {
                SlideEditorActivity.access$110(SlideEditorActivity.this);
                SlideEditorActivity.this.showCurrentSlide();
            }
        }
    };
    private final View.OnClickListener mOnNavigateForward = new View.OnClickListener() {
        public void onClick(View view) {
            if (SlideEditorActivity.this.mPosition < SlideEditorActivity.this.mSlideshowModel.size() - 1) {
                SlideEditorActivity.access$108(SlideEditorActivity.this);
                SlideEditorActivity.this.showCurrentSlide();
            }
        }
    };
    private final View.OnClickListener mOnPreview = new View.OnClickListener() {
        public void onClick(View view) {
            SlideEditorActivity.this.previewSlideshow();
        }
    };
    private final View.OnClickListener mOnRemoveSlide = new View.OnClickListener() {
        public void onClick(View view) {
            if (SlideEditorActivity.this.mPosition >= 0 && SlideEditorActivity.this.mPosition < SlideEditorActivity.this.mSlideshowModel.size()) {
                SlideEditorActivity.this.mSlideshowEditor.removeSlide(SlideEditorActivity.this.mPosition);
                int size = SlideEditorActivity.this.mSlideshowModel.size();
                if (size > 0) {
                    if (SlideEditorActivity.this.mPosition >= size) {
                        SlideEditorActivity.access$110(SlideEditorActivity.this);
                    }
                    SlideEditorActivity.this.showCurrentSlide();
                    return;
                }
                SlideEditorActivity.this.finish();
            }
        }
    };
    private final View.OnClickListener mOnReplaceImage = new View.OnClickListener() {
        public void onClick(View view) {
            SlideModel slideModel = SlideEditorActivity.this.mSlideshowModel.get(SlideEditorActivity.this.mPosition);
            if (slideModel == null || !slideModel.hasVideo()) {
                Intent intent = new Intent("android.intent.action.GET_CONTENT", (Uri) null);
                intent.setType(ContentType.IMAGE_UNSPECIFIED);
                SlideEditorActivity.this.startActivityForResult(intent, 1);
                return;
            }
            Toast.makeText(SlideEditorActivity.this, (int) R.string.cannot_add_picture_and_video, 0).show();
        }
    };
    private final BasicSlideEditorView.OnTextChangedListener mOnTextChangedListener = new BasicSlideEditorView.OnTextChangedListener() {
        public void onTextChanged(String str) {
            SlideEditorActivity.this.mSlideshowEditor.changeText(SlideEditorActivity.this.mPosition, str);
        }
    };
    /* access modifiers changed from: private */
    public int mPosition;
    private ImageButton mPreSlide;
    private SlideshowPresenter mPresenter;
    private Button mPreview;
    private Button mRemoveSlide;
    private Button mReplaceImage;
    private final MessageUtils.ResizeImageResultCallback mResizeImageCallback = new MessageUtils.ResizeImageResultCallback() {
        public void onResizeResult(PduPart pduPart, boolean z) {
            SlideEditorActivity slideEditorActivity = SlideEditorActivity.this;
            if (pduPart == null) {
                Toast.makeText(SlideEditorActivity.this, SlideEditorActivity.this.getResourcesString(R.string.failed_to_add_media, SlideEditorActivity.this.getPictureString()), 0).show();
                return;
            }
            try {
                SlideEditorActivity.this.mSlideshowEditor.changeImage(SlideEditorActivity.this.mPosition, PduPersister.getPduPersister(slideEditorActivity).persistPart(pduPart, ContentUris.parseId(SlideEditorActivity.this.mUri)));
                SlideEditorActivity.this.setReplaceButtonText(R.string.replace_image);
            } catch (MmsException e) {
                SlideEditorActivity.this.notifyUser("add picture failed");
                Toast.makeText(SlideEditorActivity.this, SlideEditorActivity.this.getResourcesString(R.string.failed_to_add_media, SlideEditorActivity.this.getPictureString()), 0).show();
            } catch (UnsupportContentTypeException e2) {
                MessageUtils.showErrorDialog(SlideEditorActivity.this, SlideEditorActivity.this.getResourcesString(R.string.unsupported_media_format, SlideEditorActivity.this.getPictureString()), SlideEditorActivity.this.getResourcesString(R.string.select_different_media, SlideEditorActivity.this.getPictureString()));
            } catch (ResolutionException e3) {
                MessageUtils.showErrorDialog(SlideEditorActivity.this, SlideEditorActivity.this.getResourcesString(R.string.failed_to_resize_image), SlideEditorActivity.this.getResourcesString(R.string.resize_image_error_information));
            } catch (ExceedMessageSizeException e4) {
                MessageUtils.showErrorDialog(SlideEditorActivity.this, SlideEditorActivity.this.getResourcesString(R.string.exceed_message_size_limitation), SlideEditorActivity.this.getResourcesString(R.string.failed_to_add_media, SlideEditorActivity.this.getPictureString()));
            }
        }
    };
    private BasicSlideEditorView mSlideView;
    /* access modifiers changed from: private */
    public SlideshowEditor mSlideshowEditor;
    /* access modifiers changed from: private */
    public SlideshowModel mSlideshowModel;
    /* access modifiers changed from: private */
    public Uri mUri;

    static /* synthetic */ int access$108(SlideEditorActivity slideEditorActivity) {
        int i = slideEditorActivity.mPosition;
        slideEditorActivity.mPosition = i + 1;
        return i;
    }

    static /* synthetic */ int access$110(SlideEditorActivity slideEditorActivity) {
        int i = slideEditorActivity.mPosition;
        slideEditorActivity.mPosition = i - 1;
        return i;
    }

    private String getAudioString() {
        return getResourcesString(R.string.type_audio);
    }

    /* access modifiers changed from: private */
    public String getPictureString() {
        return getResourcesString(R.string.type_picture);
    }

    /* access modifiers changed from: private */
    public String getResourcesString(int i) {
        return getResources().getString(i);
    }

    /* access modifiers changed from: private */
    public String getResourcesString(int i, String str) {
        return getResources().getString(i, str);
    }

    private String getVideoString() {
        return getResourcesString(R.string.type_video);
    }

    private void initActivityState(Bundle bundle, Intent intent) {
        if (bundle != null) {
            this.mUri = (Uri) bundle.getParcelable(MESSAGE_URI);
            this.mPosition = bundle.getInt("slide_index", 0);
            return;
        }
        this.mUri = intent.getData();
        this.mPosition = intent.getIntExtra("slide_index", 0);
    }

    /* access modifiers changed from: private */
    public void notifyUser(String str) {
    }

    /* access modifiers changed from: private */
    public void previewSlideshow() {
        MessageUtils.viewMmsMessageAttachment(this, this.mUri, this.mSlideshowModel);
    }

    /* access modifiers changed from: private */
    public void setReplaceButtonText(int i) {
        this.mReplaceImage.setText(i);
    }

    /* access modifiers changed from: private */
    public void showCurrentSlide() {
        this.mPresenter.setLocation(this.mPosition);
        this.mPresenter.present();
        updateTitle();
        if (this.mSlideshowModel.get(this.mPosition).hasImage()) {
            setReplaceButtonText(R.string.replace_image);
        } else {
            setReplaceButtonText(R.string.add_picture);
        }
    }

    private void showDurationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.ic_mms_duration);
        builder.setTitle(getResources().getString(R.string.duration_selector_title) + (this.mPosition + 1) + "/" + this.mSlideshowModel.size());
        builder.setItems((int) R.array.select_dialog_items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i < 0 || i >= 10) {
                    Intent intent = new Intent(SlideEditorActivity.this, EditSlideDurationActivity.class);
                    intent.putExtra("slide_index", SlideEditorActivity.this.mPosition);
                    intent.putExtra(EditSlideDurationActivity.SLIDE_TOTAL, SlideEditorActivity.this.mSlideshowModel.size());
                    intent.putExtra(EditSlideDurationActivity.SLIDE_DUR, SlideEditorActivity.this.mSlideshowModel.get(SlideEditorActivity.this.mPosition).getDuration() / 1000);
                    SlideEditorActivity.this.startActivityForResult(intent, 6);
                    return;
                }
                SlideEditorActivity.this.mSlideshowEditor.changeDuration(SlideEditorActivity.this.mPosition, (i + 1) * 1000);
            }
        });
        builder.show();
    }

    private void showLayoutSelectorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.ic_mms_layout);
        builder.setTitle(getResources().getString(R.string.layout_selector_title) + (this.mPosition + 1) + "/" + this.mSlideshowModel.size());
        builder.setAdapter(new LayoutSelectorAdapter(this), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        SlideEditorActivity.this.mSlideshowEditor.changeLayout(1);
                        return;
                    case 1:
                        SlideEditorActivity.this.mSlideshowEditor.changeLayout(0);
                        return;
                    default:
                        return;
                }
            }
        });
        builder.show();
    }

    private void updateTitle() {
        setTitle(getString(R.string.slide_show_part, new Object[]{Integer.valueOf(this.mPosition + 1), Integer.valueOf(this.mSlideshowModel.size())}));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Uri data;
        if (i2 == -1) {
            switch (i) {
                case 0:
                    this.mSlideshowEditor.changeText(this.mPosition, intent.getAction());
                    return;
                case 1:
                    try {
                        this.mSlideshowEditor.changeImage(this.mPosition, intent.getData());
                        setReplaceButtonText(R.string.replace_image);
                        return;
                    } catch (MmsException e) {
                        Log.e(TAG, "add image failed", e);
                        notifyUser("add picture failed");
                        Toast.makeText(this, getResourcesString(R.string.failed_to_add_media, getPictureString()), 0).show();
                        return;
                    } catch (UnsupportContentTypeException e2) {
                        MessageUtils.showErrorDialog(this, getResourcesString(R.string.unsupported_media_format, getPictureString()), getResourcesString(R.string.select_different_media, getPictureString()));
                        return;
                    } catch (ResolutionException e3) {
                        MessageUtils.resizeImageAsync(this, intent.getData(), new Handler(), this.mResizeImageCallback, false);
                        return;
                    } catch (ExceedMessageSizeException e4) {
                        MessageUtils.resizeImageAsync(this, intent.getData(), new Handler(), this.mResizeImageCallback, false);
                        return;
                    }
                case 2:
                    Bitmap bitmap = (Bitmap) intent.getParcelableExtra("data");
                    if (bitmap == null) {
                        Toast.makeText(this, getResourcesString(R.string.failed_to_add_media, getPictureString()), 0).show();
                        return;
                    }
                    try {
                        this.mSlideshowEditor.changeImage(this.mPosition, MessageUtils.saveBitmapAsPart(this, this.mUri, bitmap));
                        setReplaceButtonText(R.string.replace_image);
                        return;
                    } catch (MmsException e5) {
                        Log.e(TAG, "add image failed", e5);
                        notifyUser("add picture failed");
                        Toast.makeText(this, getResourcesString(R.string.failed_to_add_media, getPictureString()), 0).show();
                        return;
                    } catch (UnsupportContentTypeException e6) {
                        MessageUtils.showErrorDialog(this, getResourcesString(R.string.unsupported_media_format, getPictureString()), getResourcesString(R.string.select_different_media, getPictureString()));
                        return;
                    } catch (ResolutionException e7) {
                        MessageUtils.resizeImageAsync(this, intent.getData(), new Handler(), this.mResizeImageCallback, false);
                        return;
                    } catch (ExceedMessageSizeException e8) {
                        MessageUtils.resizeImageAsync(this, intent.getData(), new Handler(), this.mResizeImageCallback, false);
                        return;
                    }
                case 3:
                case 4:
                    if (i == 3) {
                        data = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                        if (Settings.System.DEFAULT_RINGTONE_URI.equals(data)) {
                            return;
                        }
                    } else {
                        data = intent.getData();
                    }
                    try {
                        this.mSlideshowEditor.changeAudio(this.mPosition, data);
                        return;
                    } catch (MmsException e9) {
                        Log.e(TAG, "add audio failed", e9);
                        notifyUser("add music failed");
                        Toast.makeText(this, getResourcesString(R.string.failed_to_add_media, getAudioString()), 0).show();
                        return;
                    } catch (UnsupportContentTypeException e10) {
                        MessageUtils.showErrorDialog(this, getResourcesString(R.string.unsupported_media_format, getAudioString()), getResourcesString(R.string.select_different_media, getAudioString()));
                        return;
                    } catch (ExceedMessageSizeException e11) {
                        MessageUtils.showErrorDialog(this, getResourcesString(R.string.exceed_message_size_limitation), getResourcesString(R.string.failed_to_add_media, getAudioString()));
                        return;
                    }
                case 5:
                    try {
                        this.mSlideshowEditor.changeVideo(this.mPosition, intent.getData());
                        return;
                    } catch (MmsException e12) {
                        Log.e(TAG, "add video failed", e12);
                        notifyUser("add video failed");
                        Toast.makeText(this, getResourcesString(R.string.failed_to_add_media, getVideoString()), 0).show();
                        return;
                    } catch (UnsupportContentTypeException e13) {
                        MessageUtils.showErrorDialog(this, getResourcesString(R.string.unsupported_media_format, getVideoString()), getResourcesString(R.string.select_different_media, getVideoString()));
                        return;
                    } catch (ExceedMessageSizeException e14) {
                        MessageUtils.showErrorDialog(this, getResourcesString(R.string.exceed_message_size_limitation), getResourcesString(R.string.failed_to_add_media, getVideoString()));
                        return;
                    }
                case 6:
                    this.mSlideshowEditor.changeDuration(this.mPosition, Integer.valueOf(intent.getAction()).intValue() * 1000);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.edit_slide_activity);
        this.mSlideView = (BasicSlideEditorView) findViewById(R.id.slide_editor_view);
        this.mSlideView.setOnTextChangedListener(this.mOnTextChangedListener);
        this.mPreSlide = (ImageButton) findViewById(R.id.pre_slide_button);
        this.mPreSlide.setOnClickListener(this.mOnNavigateBackward);
        this.mNextSlide = (ImageButton) findViewById(R.id.next_slide_button);
        this.mNextSlide.setOnClickListener(this.mOnNavigateForward);
        this.mPreview = (Button) findViewById(R.id.preview_button);
        this.mPreview.setOnClickListener(this.mOnPreview);
        this.mReplaceImage = (Button) findViewById(R.id.replace_image_button);
        this.mReplaceImage.setOnClickListener(this.mOnReplaceImage);
        this.mRemoveSlide = (Button) findViewById(R.id.remove_slide_button);
        this.mRemoveSlide.setOnClickListener(this.mOnRemoveSlide);
        this.mDone = (Button) findViewById(R.id.done_button);
        this.mDone.setOnClickListener(this.mDoneClickListener);
        initActivityState(bundle, getIntent());
        try {
            this.mSlideshowModel = SlideshowModel.createFromMessageUri(this, this.mUri);
            this.mSlideshowModel.registerModelChangedObserver(this.mModelChangedObserver);
            this.mSlideshowEditor = new SlideshowEditor(this, this.mSlideshowModel);
            this.mPresenter = (SlideshowPresenter) PresenterFactory.getPresenter("SlideshowPresenter", this, this.mSlideView, this.mSlideshowModel);
            if (this.mPosition >= this.mSlideshowModel.size()) {
                this.mPosition = Math.max(0, this.mSlideshowModel.size() - 1);
            } else if (this.mPosition < 0) {
                this.mPosition = 0;
            }
            showCurrentSlide();
        } catch (MmsException e) {
            Log.e(TAG, "Create SlideshowModel failed!", e);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mSlideshowModel != null) {
            this.mSlideshowModel.unregisterModelChangedObserver(this.mModelChangedObserver);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                SlideModel slideModel = this.mSlideshowModel.get(this.mPosition);
                if (slideModel != null) {
                    slideModel.removeText();
                    break;
                }
                break;
            case 1:
                Intent intent = new Intent("android.intent.action.GET_CONTENT", (Uri) null);
                intent.setType(ContentType.IMAGE_UNSPECIFIED);
                startActivityForResult(intent, 1);
                break;
            case 2:
                startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 2);
                break;
            case 3:
                this.mSlideshowEditor.removeImage(this.mPosition);
                setReplaceButtonText(R.string.add_picture);
                break;
            case 4:
                MessageUtils.selectAudio(this, 3);
                break;
            case 5:
                this.mSlideshowEditor.removeAudio(this.mPosition);
                break;
            case 6:
                Intent intent2 = new Intent("android.intent.action.GET_CONTENT");
                intent2.setType(ContentType.VIDEO_UNSPECIFIED);
                startActivityForResult(intent2, 5);
                break;
            case 7:
                this.mPosition++;
                if (!this.mSlideshowEditor.addNewSlide(this.mPosition)) {
                    this.mPosition--;
                    Toast.makeText(this, (int) R.string.cannot_add_slide_anymore, 0).show();
                    break;
                } else {
                    showCurrentSlide();
                    break;
                }
            case 8:
                this.mSlideshowEditor.removeVideo(this.mPosition);
                break;
            case 9:
                showLayoutSelectorDialog();
                break;
            case 10:
                showDurationDialog();
                break;
            case 11:
                previewSlideshow();
                break;
            case 12:
                MessageUtils.recordSound(this, 4);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        synchronized (this) {
            if (this.mDirty) {
                try {
                    PduBody pduBody = this.mSlideshowModel.toPduBody();
                    PduPersister.getPduPersister(this).updateParts(this.mUri, pduBody);
                    this.mSlideshowModel.sync(pduBody);
                } catch (MmsException e) {
                    Log.e(TAG, "Cannot update the message: " + this.mUri, e);
                }
            }
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        SlideModel slideModel = this.mSlideshowModel.get(this.mPosition);
        if (slideModel == null) {
            return false;
        }
        menu.add(0, 11, 0, (int) R.string.preview_slideshow).setIcon((int) R.drawable.ic_menu_play_clip);
        if (slideModel.hasText() && !TextUtils.isEmpty(slideModel.getText().getText())) {
            menu.add(0, 0, 0, (int) R.string.remove_text).setIcon((int) R.drawable.ic_menu_remove_text);
        }
        if (slideModel.hasImage()) {
            menu.add(0, 3, 0, (int) R.string.remove_picture).setIcon((int) R.drawable.ic_menu_remove_picture);
        } else if (!slideModel.hasVideo()) {
            menu.add(0, 1, 0, (int) R.string.add_picture).setIcon((int) R.drawable.ic_menu_picture);
            menu.add(0, 2, 0, (int) R.string.attach_take_photo).setIcon((int) R.drawable.ic_menu_picture);
        }
        if (slideModel.hasAudio()) {
            menu.add(0, 5, 0, (int) R.string.remove_music).setIcon((int) R.drawable.ic_menu_remove_sound);
        } else if (!slideModel.hasVideo()) {
            if (MmsConfig.getAllowAttachAudio()) {
                SubMenu icon = menu.addSubMenu(0, 13, 0, (int) R.string.add_music).setIcon((int) R.drawable.ic_menu_add_sound);
                icon.add(0, 4, 0, (int) R.string.attach_sound);
                icon.add(0, 12, 0, (int) R.string.attach_record_sound);
            } else {
                menu.add(0, 12, 0, (int) R.string.attach_record_sound).setIcon((int) R.drawable.ic_menu_add_sound);
            }
        }
        if (slideModel.hasVideo()) {
            menu.add(0, 8, 0, (int) R.string.remove_video).setIcon((int) R.drawable.ic_menu_remove_video);
        } else if (!slideModel.hasAudio() && !slideModel.hasImage()) {
            menu.add(0, 6, 0, (int) R.string.add_video).setIcon((int) R.drawable.ic_menu_movie);
        }
        menu.add(0, 7, 0, (int) R.string.add_slide).setIcon((int) R.drawable.ic_menu_add_slide);
        menu.add(0, 10, 0, getResources().getString(R.string.duration_sec).replace("%s", String.valueOf(slideModel.getDuration() / 1000))).setIcon((int) R.drawable.ic_menu_duration);
        menu.add(0, 9, 0, this.mSlideshowModel.getLayout().getLayoutType() == 1 ? R.string.layout_top : R.string.layout_bottom).setIcon((int) R.drawable.ic_menu_picture);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("slide_index", this.mPosition);
        bundle.putParcelable(MESSAGE_URI, this.mUri);
    }
}
