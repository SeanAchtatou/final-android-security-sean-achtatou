package com.android.mms.ui;

import android.content.Context;
import com.android.mms.ui.IconListAdapter;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms2.R;

public class LayoutSelectorAdapter extends IconListAdapter {
    public LayoutSelectorAdapter(Context context) {
        super(context, getData(context));
    }

    protected static void addItem(List<IconListAdapter.IconListItem> list, String str, int i) {
        list.add(new IconListAdapter.IconListItem(str, i));
    }

    protected static List<IconListAdapter.IconListItem> getData(Context context) {
        ArrayList arrayList = new ArrayList(2);
        addItem(arrayList, context.getString(R.string.select_top_text), R.drawable.ic_mms_text_top);
        addItem(arrayList, context.getString(R.string.select_bottom_text), R.drawable.ic_mms_text_bottom);
        return arrayList;
    }
}
