package com.android.mms.ui;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.telephony.Phone;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.model.TextModel;
import com.android.mms.ui.MessageListAdapter;
import com.android.mms.util.AddressUtils;
import com.android.provider.Telephony;
import com.google.android.mms.MmsException;
import com.google.android.mms.pdu.EncodedStringValue;
import com.google.android.mms.pdu.MultimediaMessagePdu;
import com.google.android.mms.pdu.NotificationInd;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.RetrieveConf;
import com.google.android.mms.pdu.SendReq;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import vc.lx.sms.caches.RemoteResourceManager;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.data.Contact;
import vc.lx.sms.db.ImagesDbService;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class MessageItem implements Serializable {
    private static String TAG = "MessageItem";
    private static final long serialVersionUID = 1;
    public String mAddress;
    public int mAttachmentType;
    public String mBody;
    public final int mBoxId;
    CharSequence mCachedFormattedMessage;
    String mContact;
    final Context mContext;
    public boolean mDeliveryReport;
    public DeliveryStatus mDeliveryStatus;
    public int mErrorType;
    String mHighlight;
    public boolean mLocked;
    public int mMessageSize;
    public int mMessageType;
    public Uri mMessageUri;
    public final long mMsgId;
    long mOriginalTimestamp;
    public boolean mReadReport;
    public String mRecipientAddress = "";
    public SlideshowModel mSlideshow;
    public String mSmsImagePlug;
    public SongItem mSongItem;
    public String mSubject;
    public String mTimestamp;
    public final String mType;
    public VoteItem mVoteItem;

    public enum DeliveryStatus {
        NONE,
        INFO,
        FAILED,
        PENDING,
        RECEIVED
    }

    MessageItem(Context context, String str, Cursor cursor, MessageListAdapter.ColumnsMap columnsMap, String str2, RemoteResourceManager remoteResourceManager) throws MmsException {
        this.mContext = context;
        this.mMsgId = cursor.getLong(columnsMap.mColumnMsgId);
        this.mHighlight = str2 != null ? str2.toLowerCase() : null;
        this.mType = str;
        if ("sms".equals(str)) {
            this.mReadReport = false;
            this.mDeliveryReport = cursor.getLong(columnsMap.mColumnSmsStatus) != -1;
            long j = cursor.getLong(columnsMap.mColumnSmsStatus);
            if (j == -1) {
                this.mDeliveryStatus = DeliveryStatus.NONE;
            } else if (j >= 128) {
                this.mDeliveryStatus = DeliveryStatus.FAILED;
            } else if (j >= 64) {
                this.mDeliveryStatus = DeliveryStatus.PENDING;
            } else {
                this.mDeliveryStatus = DeliveryStatus.RECEIVED;
            }
            this.mMessageUri = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, this.mMsgId);
            this.mBoxId = cursor.getInt(columnsMap.mColumnSmsType);
            this.mAddress = cursor.getString(columnsMap.mColumnSmsAddress);
            if (Telephony.Sms.isOutgoingFolder(this.mBoxId)) {
                this.mContact = context.getString(R.string.messagelist_sender_self);
            } else {
                this.mContact = Contact.get(this.mAddress, false, this.mContext).getName();
            }
            this.mRecipientAddress = Contact.get(this.mAddress, false, this.mContext).getName();
            this.mBody = cursor.getString(columnsMap.mColumnSmsBody);
            checkBodyForSmsImage(this.mBody, remoteResourceManager);
            long j2 = cursor.getLong(columnsMap.mColumnSmsDate);
            this.mOriginalTimestamp = j2;
            this.mTimestamp = String.format(context.getString(R.string.sent_on), MessageUtils.formatTimeStampString(context, j2));
            this.mLocked = cursor.getInt(columnsMap.mColumnSmsLocked) != 0;
        } else if (Phone.APN_TYPE_MMS.equals(str)) {
            long j3 = cursor.getLong(columnsMap.mColumnSmsDate);
            this.mOriginalTimestamp = 1000 * j3;
            this.mTimestamp = String.format(context.getString(R.string.sent_on), MessageUtils.formatTimeStampString(context, j3));
            this.mMessageUri = ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, this.mMsgId);
            this.mBoxId = cursor.getInt(columnsMap.mColumnMmsMessageBox);
            this.mMessageType = cursor.getInt(columnsMap.mColumnMmsMessageType);
            this.mErrorType = cursor.getInt(columnsMap.mColumnMmsErrorType);
            String string = cursor.getString(columnsMap.mColumnMmsSubject);
            if (!TextUtils.isEmpty(string)) {
                this.mSubject = new EncodedStringValue(cursor.getInt(columnsMap.mColumnMmsSubjectCharset), PduPersister.getBytes(string)).getString();
            }
            this.mLocked = cursor.getInt(columnsMap.mColumnMmsLocked) != 0;
            PduPersister pduPersister = PduPersister.getPduPersister(this.mContext);
            if (130 == this.mMessageType) {
                this.mDeliveryReport = false;
                this.mDeliveryStatus = DeliveryStatus.NONE;
                NotificationInd notificationInd = (NotificationInd) pduPersister.load(this.mMessageUri);
                interpretFrom(notificationInd.getFrom(), this.mMessageUri);
                this.mBody = new String(notificationInd.getContentLocation());
                this.mMessageSize = (int) notificationInd.getMessageSize();
                long expiry = notificationInd.getExpiry() * 1000;
            } else {
                MultimediaMessagePdu multimediaMessagePdu = (MultimediaMessagePdu) pduPersister.load(this.mMessageUri);
                this.mSlideshow = SlideshowModel.createFromPduBody(context, multimediaMessagePdu.getBody());
                this.mAttachmentType = MessageUtils.getAttachmentType(this.mSlideshow);
                if (this.mMessageType == 132) {
                    RetrieveConf retrieveConf = (RetrieveConf) multimediaMessagePdu;
                    interpretFrom(retrieveConf.getFrom(), this.mMessageUri);
                    long date = retrieveConf.getDate() * 1000;
                } else {
                    String string2 = context.getString(R.string.messagelist_sender_self);
                    this.mAddress = string2;
                    this.mContact = string2;
                    long date2 = ((SendReq) multimediaMessagePdu).getDate() * 1000;
                }
                String string3 = cursor.getString(columnsMap.mColumnMmsDeliveryReport);
                if (string3 == null || !this.mAddress.equals(context.getString(R.string.messagelist_sender_self))) {
                    this.mDeliveryStatus = DeliveryStatus.NONE;
                    this.mDeliveryReport = false;
                } else {
                    try {
                        this.mDeliveryReport = Integer.parseInt(string3) == 128;
                    } catch (NumberFormatException e) {
                        Log.e(TAG, "Value for delivery report was invalid.");
                        this.mDeliveryReport = false;
                    }
                    this.mDeliveryStatus = this.mDeliveryReport ? DeliveryStatus.RECEIVED : DeliveryStatus.NONE;
                }
                String string4 = cursor.getString(columnsMap.mColumnMmsReadReport);
                if (string4 == null || !this.mAddress.equals(context.getString(R.string.messagelist_sender_self))) {
                    this.mReadReport = false;
                } else {
                    try {
                        this.mReadReport = Integer.parseInt(string4) == 128;
                    } catch (NumberFormatException e2) {
                        Log.e(TAG, "Value for read report was invalid.");
                        this.mReadReport = false;
                    }
                }
                SlideModel slideModel = this.mSlideshow.get(0);
                if (slideModel != null && slideModel.hasText()) {
                    TextModel text = slideModel.getText();
                    if (text.isDrmProtected()) {
                        this.mBody = this.mContext.getString(R.string.drm_protected_text);
                    } else {
                        this.mBody = text.getText();
                    }
                }
                this.mMessageSize = this.mSlideshow.getCurrentMessageSize();
            }
            if (!isOutgoingMessage()) {
            }
        } else {
            throw new MmsException("Unknown type of the message: " + str);
        }
    }

    private void checkBodyForSmsImage(String str, RemoteResourceManager remoteResourceManager) {
        Matcher matcher;
        String group;
        if (str != null && str.length() >= 10 && (matcher = Pattern.compile(this.mContext.getString(R.string.shorten_url_img_reg), 32).matcher(str)) != null && matcher.find() && (group = matcher.group(1)) != null) {
            ImagesDbService.ImageItem queryByPlug = new ImagesDbService(this.mContext).queryByPlug(group);
            if (queryByPlug == null || queryByPlug.thumb == null || queryByPlug.url == null) {
                remoteResourceManager.request(Uri.parse(Util.getFullShortenImageUrl(this.mContext, group)));
                return;
            }
            Uri parse = Uri.parse(queryByPlug.thumb);
            if (!remoteResourceManager.exists(parse)) {
                remoteResourceManager.request(parse);
            }
        }
    }

    private int getTimestampStrId() {
        return 130 == this.mMessageType ? R.string.expire_on : R.string.sent_on;
    }

    private void interpretFrom(EncodedStringValue encodedStringValue, Uri uri) {
        if (encodedStringValue != null) {
            this.mAddress = encodedStringValue.getString();
        } else {
            this.mAddress = AddressUtils.getFrom(this.mContext, uri);
        }
        this.mContact = TextUtils.isEmpty(this.mAddress) ? "" : Contact.get(this.mAddress, false, this.mContext).getName();
    }

    public String getAddress() {
        return this.mAddress;
    }

    public String getBody() {
        return this.mBody;
    }

    public int getBoxId() {
        return this.mBoxId;
    }

    public CharSequence getCachedFormattedMessage() {
        return this.mCachedFormattedMessage;
    }

    public long getOriginalTimestamp() {
        return this.mOriginalTimestamp;
    }

    public String getType() {
        return this.mType;
    }

    public boolean isDownloaded() {
        return this.mMessageType != 130;
    }

    public boolean isMms() {
        return this.mType.equals(Phone.APN_TYPE_MMS);
    }

    public boolean isOutgoingMessage() {
        return (isMms() && this.mBoxId == 4) || (isSms() && (this.mBoxId == 5 || this.mBoxId == 4 || this.mBoxId == 6));
    }

    public boolean isSms() {
        return this.mType.equals("sms");
    }

    public void setCachedFormattedMessage(CharSequence charSequence) {
        this.mCachedFormattedMessage = charSequence;
    }

    public String toString() {
        return "type: " + this.mType + " box: " + this.mBoxId + " uri: " + this.mMessageUri + " address: " + this.mAddress + " contact: " + this.mContact + " read: " + this.mReadReport + " delivery report: " + this.mDeliveryReport;
    }
}
