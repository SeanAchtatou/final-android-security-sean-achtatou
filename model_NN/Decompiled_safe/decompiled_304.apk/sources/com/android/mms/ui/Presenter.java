package com.android.mms.ui;

import android.content.Context;
import com.android.mms.model.IModelChangedObserver;
import com.android.mms.model.Model;

public abstract class Presenter implements IModelChangedObserver {
    protected final Context mContext;
    protected Model mModel;
    protected ViewInterface mView;

    public Presenter(Context context, ViewInterface viewInterface, Model model) {
        this.mContext = context;
        this.mView = viewInterface;
        this.mModel = model;
        this.mModel.registerModelChangedObserver(this);
    }

    public Model getModel() {
        return this.mModel;
    }

    public ViewInterface getView() {
        return this.mView;
    }

    public abstract void present();

    public void setView(ViewInterface viewInterface) {
        this.mView = viewInterface;
    }
}
