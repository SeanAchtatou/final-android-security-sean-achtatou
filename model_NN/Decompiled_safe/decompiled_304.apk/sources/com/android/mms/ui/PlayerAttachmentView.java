package com.android.mms.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.Map;
import vc.lx.sms2.R;

public class PlayerAttachmentView extends LinearLayout implements SlideViewInterface {
    private ImageView mImageView;

    public PlayerAttachmentView(Context context) {
        super(context);
    }

    public PlayerAttachmentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.mImageView = (ImageView) findViewById(R.id.image_content);
    }

    public void pauseAudio() {
    }

    public void pauseVideo() {
    }

    public void reset() {
        this.mImageView.setImageDrawable(null);
    }

    public void seekAudio(int i) {
    }

    public void seekVideo(int i) {
    }

    public void setAudio(Uri uri, String str, Map<String, ?> map) {
    }

    public void setImage(String str, Bitmap bitmap) {
        this.mImageView.setImageBitmap(bitmap == null ? BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_picture) : bitmap);
    }

    public void setImageRegionFit(String str) {
    }

    public void setImageVisibility(boolean z) {
    }

    public void setText(String str, String str2) {
    }

    public void setTextVisibility(boolean z) {
    }

    public void setVideo(String str, Uri uri) {
    }

    public void setVideoVisibility(boolean z) {
    }

    public void setVisibility(boolean z) {
        setVisibility(z ? 0 : 8);
    }

    public void startAudio() {
    }

    public void startVideo() {
    }

    public void stopAudio() {
    }

    public void stopVideo() {
    }
}
