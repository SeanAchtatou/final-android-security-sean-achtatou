package com.android.mms.ui;

public interface ViewInterface {
    int getHeight();

    int getWidth();

    void reset();

    void setVisibility(boolean z);
}
