package com.android.mms.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.android.mms.transaction.MessagingNotification;
import com.android.provider.Telephony;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;

public class ManageSimMessages extends Activity implements View.OnCreateContextMenuListener {
    private static final Uri ICC_URI = Uri.parse("content://sms/icc");
    private static final int MENU_COPY_TO_PHONE_MEMORY = 0;
    private static final int MENU_DELETE_FROM_SIM = 1;
    private static final int MENU_VIEW = 2;
    private static final int OPTION_MENU_DELETE_ALL = 0;
    private static final int SHOW_BUSY = 2;
    private static final int SHOW_EMPTY = 1;
    private static final int SHOW_LIST = 0;
    public static final int SIM_FULL_NOTIFICATION_ID = 234;
    private static final String TAG = "ManageSimMessages";
    private ContentResolver mContentResolver;
    /* access modifiers changed from: private */
    public Cursor mCursor = null;
    /* access modifiers changed from: private */
    public MessageListAdapter mListAdapter = null;
    private TextView mMessage;
    private AsyncQueryHandler mQueryHandler = null;
    /* access modifiers changed from: private */
    public ListView mSimList;
    private int mState;
    private final ContentObserver simChangeObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean z) {
            ManageSimMessages.this.refreshMessageList();
        }
    };

    private class QueryHandler extends AsyncQueryHandler {
        private final ManageSimMessages mParent;

        public QueryHandler(ContentResolver contentResolver, ManageSimMessages manageSimMessages) {
            super(contentResolver);
            this.mParent = manageSimMessages;
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            Cursor unused = ManageSimMessages.this.mCursor = cursor;
            if (ManageSimMessages.this.mCursor != null) {
                if (!ManageSimMessages.this.mCursor.moveToFirst()) {
                    ManageSimMessages.this.updateState(1);
                } else if (ManageSimMessages.this.mListAdapter == null) {
                    MessageListAdapter unused2 = ManageSimMessages.this.mListAdapter = new MessageListAdapter(this.mParent, ManageSimMessages.this.mCursor, ManageSimMessages.this.mSimList, false, null, true, false);
                    ManageSimMessages.this.mSimList.setAdapter((ListAdapter) ManageSimMessages.this.mListAdapter);
                    ManageSimMessages.this.mSimList.setOnCreateContextMenuListener(this.mParent);
                    ManageSimMessages.this.updateState(0);
                } else {
                    ManageSimMessages.this.mListAdapter.changeCursor(ManageSimMessages.this.mCursor);
                    ManageSimMessages.this.updateState(0);
                }
                ManageSimMessages.this.startManagingCursor(ManageSimMessages.this.mCursor);
                ManageSimMessages.this.registerSimChangeObserver();
                return;
            }
            ManageSimMessages.this.updateState(1);
        }
    }

    private void confirmDeleteDialog(DialogInterface.OnClickListener onClickListener, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.confirm_dialog_title);
        builder.setIcon(17301543);
        builder.setCancelable(true);
        builder.setPositiveButton((int) R.string.yes, onClickListener);
        builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        builder.setMessage(i);
        builder.show();
    }

    private void copyToPhoneMemory(Cursor cursor) {
        String string = cursor.getString(cursor.getColumnIndexOrThrow("address"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.TextBasedSmsColumns.BODY));
        Long valueOf = Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("date")));
        try {
            if (isIncomingMessage(cursor)) {
                Telephony.Sms.Inbox.addMessage(this.mContentResolver, string, string2, null, valueOf, true);
            } else {
                Telephony.Sms.Sent.addMessage(this.mContentResolver, string, string2, null, valueOf);
            }
        } catch (SQLiteException e) {
            SqliteWrapper.checkSQLiteException(this, e);
        }
    }

    /* access modifiers changed from: private */
    public void deleteAllFromSim() {
        Cursor cursor = this.mListAdapter.getCursor();
        if (cursor != null && cursor.moveToFirst()) {
            int count = cursor.getCount();
            for (int i = 0; i < count; i++) {
                deleteFromSim(cursor);
                cursor.moveToNext();
            }
        }
    }

    /* access modifiers changed from: private */
    public void deleteFromSim(Cursor cursor) {
        SqliteWrapper.delete(this, this.mContentResolver, ICC_URI.buildUpon().appendPath(cursor.getString(cursor.getColumnIndexOrThrow("index_on_icc"))).build(), null, null);
    }

    private void init() {
        MessagingNotification.cancelNotification(getApplicationContext(), 234);
        updateState(2);
        startQuery();
    }

    private boolean isIncomingMessage(Cursor cursor) {
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("status"));
        return i == 1 || i == 3;
    }

    /* access modifiers changed from: private */
    public void refreshMessageList() {
        updateState(2);
        if (this.mCursor != null) {
            stopManagingCursor(this.mCursor);
            this.mCursor.close();
        }
        startQuery();
    }

    /* access modifiers changed from: private */
    public void registerSimChangeObserver() {
        this.mContentResolver.registerContentObserver(ICC_URI, true, this.simChangeObserver);
    }

    private void startQuery() {
        try {
            this.mQueryHandler.startQuery(0, null, ICC_URI, null, null, null, null);
        } catch (SQLiteException e) {
            SqliteWrapper.checkSQLiteException(this, e);
        }
    }

    /* access modifiers changed from: private */
    public void updateState(int i) {
        if (this.mState != i) {
            this.mState = i;
            switch (i) {
                case 0:
                    this.mSimList.setVisibility(0);
                    this.mMessage.setVisibility(8);
                    setTitle(getString(R.string.sim_manage_messages_title));
                    setProgressBarIndeterminateVisibility(false);
                    return;
                case 1:
                    this.mSimList.setVisibility(8);
                    this.mMessage.setVisibility(0);
                    setTitle(getString(R.string.sim_manage_messages_title));
                    setProgressBarIndeterminateVisibility(false);
                    return;
                case 2:
                    this.mSimList.setVisibility(8);
                    this.mMessage.setVisibility(8);
                    setTitle(getString(R.string.refreshing));
                    setProgressBarIndeterminateVisibility(true);
                    return;
                default:
                    Log.e(TAG, "Invalid State");
                    return;
            }
        }
    }

    private void viewMessage(Cursor cursor) {
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        try {
            final Cursor cursor = (Cursor) this.mListAdapter.getItem(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
            switch (menuItem.getItemId()) {
                case 0:
                    copyToPhoneMemory(cursor);
                    return true;
                case 1:
                    confirmDeleteDialog(new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ManageSimMessages.this.updateState(2);
                            ManageSimMessages.this.deleteFromSim(cursor);
                        }
                    }, R.string.confirm_delete_SIM_message);
                    return true;
                case 2:
                    viewMessage(cursor);
                    return true;
                default:
                    return super.onContextItemSelected(menuItem);
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "Bad menuInfo.", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(5);
        this.mContentResolver = getContentResolver();
        this.mQueryHandler = new QueryHandler(this.mContentResolver, this);
        setContentView((int) R.layout.sim_list);
        this.mSimList = (ListView) findViewById(R.id.messages);
        this.mMessage = (TextView) findViewById(R.id.empty_message);
        init();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        contextMenu.add(0, 0, 0, (int) R.string.sim_copy_to_phone_memory);
        contextMenu.add(0, 1, 0, (int) R.string.sim_delete);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        init();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                confirmDeleteDialog(new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ManageSimMessages.this.updateState(2);
                        ManageSimMessages.this.deleteAllFromSim();
                    }
                }, R.string.confirm_delete_all_SIM_messages);
                return true;
            default:
                return true;
        }
    }

    public void onPause() {
        super.onPause();
        this.mContentResolver.unregisterContentObserver(this.simChangeObserver);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (this.mCursor == null || this.mCursor.getCount() <= 0 || this.mState != 0) {
            return true;
        }
        menu.add(0, 0, 0, (int) R.string.menu_delete_messages).setIcon(17301564);
        return true;
    }

    public void onResume() {
        super.onResume();
        registerSimChangeObserver();
    }
}
