package com.android.mms.ui;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.Annotation;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import com.android.internal.widget.database.ArrayListCursor;
import com.android.provider.Telephony;
import java.util.ArrayList;
import vc.lx.sms.data.Contact;
import vc.lx.sms2.R;

public class RecipientsAdapter extends ResourceCursorAdapter {
    public static final int CONTACT_ID_INDEX = 1;
    public static final int LABEL_INDEX = 4;
    public static final int NAME_INDEX = 5;
    public static final int NUMBER_INDEX = 3;
    private static final String[] PROJECTION_PHONE = {"_id", Telephony.Mms.Addr.CONTACT_ID, "data2", "data1", "data3", "display_name"};
    private static final String SORT_ORDER = "times_contacted DESC,display_name,data2";
    public static final int TYPE_INDEX = 2;
    private final ContentResolver mContentResolver;
    private final Context mContext;

    public RecipientsAdapter(Context context) {
        super(context, R.layout.recipient_filter_item, null);
        this.mContext = context;
        this.mContentResolver = context.getContentResolver();
    }

    private boolean usefulAsDigits(CharSequence charSequence) {
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            char charAt = charSequence.charAt(i);
            if ((charAt < '0' || charAt > '9') && charAt != ' ' && charAt != '-' && charAt != '(' && charAt != ')' && charAt != '.' && charAt != '+' && charAt != '#' && charAt != '*' && ((charAt < 'A' || charAt > 'Z') && (charAt < 'a' || charAt > 'z'))) {
                return false;
            }
        }
        return true;
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        ((TextView) view.findViewById(R.id.name)).setText(cursor.getString(5));
        ((TextView) view.findViewById(R.id.label)).setText(ContactsContract.CommonDataKinds.Phone.getTypeLabel(this.mContext.getResources(), cursor.getInt(2), cursor.getString(4)));
        ((TextView) view.findViewById(R.id.number)).setText("(" + cursor.getString(3) + ")");
    }

    public final CharSequence convertToString(Cursor cursor) {
        String string = cursor.getString(5);
        int i = cursor.getInt(2);
        String trim = cursor.getString(3).trim();
        CharSequence typeLabel = ContactsContract.CommonDataKinds.Phone.getTypeLabel(this.mContext.getResources(), i, cursor.getString(4));
        if (trim.length() == 0) {
            return trim;
        }
        String replace = string == null ? "" : string.replace(", ", " ").replace(",", " ");
        SpannableString spannableString = new SpannableString(Contact.formatNameAndNumber(replace, trim));
        int length = spannableString.length();
        if (!TextUtils.isEmpty(replace)) {
            spannableString.setSpan(new Annotation("name", replace), 0, length, 33);
        } else {
            spannableString.setSpan(new Annotation("name", trim), 0, length, 33);
        }
        spannableString.setSpan(new Annotation("person_id", cursor.getString(1)), 0, length, 33);
        spannableString.setSpan(new Annotation("label", typeLabel.toString()), 0, length, 33);
        spannableString.setSpan(new Annotation("number", trim), 0, length, 33);
        return spannableString;
    }

    public Cursor runQueryOnBackgroundThread(CharSequence charSequence) {
        String str;
        String str2;
        if (charSequence != null) {
            String obj = charSequence.toString();
            if (usefulAsDigits(obj)) {
                String convertKeypadLettersToDigits = PhoneNumberUtils.convertKeypadLettersToDigits(obj);
                if (convertKeypadLettersToDigits.equals(obj)) {
                    str = obj;
                    str2 = "";
                } else {
                    str = obj;
                    str2 = convertKeypadLettersToDigits.trim();
                }
            } else {
                str = obj;
                str2 = "";
            }
        } else {
            str = null;
            str2 = "";
        }
        Cursor query = this.mContentResolver.query(Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri.encode(str)), PROJECTION_PHONE, String.format("%s=%s OR %s=%s OR %s=%s", "data2", 2, "data2", 17, "data2", 20), null, SORT_ORDER);
        if (str2.length() <= 0) {
            return query;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(-1);
        arrayList.add(-1L);
        arrayList.add(0);
        arrayList.add(str2);
        arrayList.add(" ");
        arrayList.add(str);
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(arrayList);
        return new MergeCursor(new Cursor[]{new ArrayListCursor(PROJECTION_PHONE, arrayList2), query});
    }
}
