package com.android.mms.ui;

import android.content.Context;
import android.graphics.BitmapFactory;
import com.android.mms.model.AudioModel;
import com.android.mms.model.ImageModel;
import com.android.mms.model.Model;
import com.android.mms.model.SlideModel;
import com.android.mms.model.SlideshowModel;
import com.android.mms.model.VideoModel;
import vc.lx.sms2.R;

public class MmsThumbnailPresenter extends Presenter {
    public MmsThumbnailPresenter(Context context, ViewInterface viewInterface, Model model) {
        super(context, viewInterface, model);
    }

    private void presentFirstSlide(SlideViewInterface slideViewInterface, SlideModel slideModel) {
        slideViewInterface.reset();
        if (slideModel.hasImage()) {
            presentImageThumbnail(slideViewInterface, slideModel.getImage());
        } else if (slideModel.hasVideo()) {
            presentVideoThumbnail(slideViewInterface, slideModel.getVideo());
        } else if (slideModel.hasAudio()) {
            presentAudioThumbnail(slideViewInterface, slideModel.getAudio());
        }
    }

    private void presentImageThumbnail(SlideViewInterface slideViewInterface, ImageModel imageModel) {
        if (imageModel.isDrmProtected()) {
            showDrmIcon(slideViewInterface, imageModel.getSrc());
        } else {
            slideViewInterface.setImage(imageModel.getSrc(), imageModel.getBitmap());
        }
    }

    private void presentVideoThumbnail(SlideViewInterface slideViewInterface, VideoModel videoModel) {
        if (videoModel.isDrmProtected()) {
            showDrmIcon(slideViewInterface, videoModel.getSrc());
        } else {
            slideViewInterface.setVideo(videoModel.getSrc(), videoModel.getUri());
        }
    }

    private void showDrmIcon(SlideViewInterface slideViewInterface, String str) {
        slideViewInterface.setImage(str, BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.ic_mms_drm_protected));
    }

    public void onModelChanged(Model model, boolean z) {
    }

    public void present() {
        SlideModel slideModel = ((SlideshowModel) this.mModel).get(0);
        if (slideModel != null) {
            presentFirstSlide((SlideViewInterface) this.mView, slideModel);
        }
    }

    /* access modifiers changed from: protected */
    public void presentAudioThumbnail(SlideViewInterface slideViewInterface, AudioModel audioModel) {
        if (audioModel.isDrmProtected()) {
            showDrmIcon(slideViewInterface, audioModel.getSrc());
        } else {
            slideViewInterface.setAudio(audioModel.getUri(), audioModel.getSrc(), audioModel.getExtras());
        }
    }
}
