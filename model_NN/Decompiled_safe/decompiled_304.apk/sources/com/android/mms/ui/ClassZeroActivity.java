package com.android.mms.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Config;
import android.util.Log;
import com.android.mms.transaction.MessagingNotification;
import com.android.provider.Telephony;
import vc.lx.sms.util.SqliteWrapper;
import vc.lx.sms2.R;

public class ClassZeroActivity extends Activity {
    private static final String BUFFER = "         ";
    private static final int BUFFER_OFFSET = (BUFFER.length() * 2);
    private static final long DEFAULT_TIMER = 300000;
    private static final int ON_AUTO_SAVE = 1;
    private static final int REPLACE_COLUMN_ID = 0;
    private static final String[] REPLACE_PROJECTION = {"_id", "address", Telephony.TextBasedSmsColumns.PROTOCOL};
    private static final String TAG = "display_00";
    private static final String TIMER_FIRE = "timer_fire";
    private final DialogInterface.OnClickListener mCancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            ClassZeroActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public AlertDialog mDialog = null;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            if (message.what == 1) {
                boolean unused = ClassZeroActivity.this.mRead = false;
                ClassZeroActivity.this.mDialog.dismiss();
                ClassZeroActivity.this.saveMessage();
                ClassZeroActivity.this.finish();
            }
        }
    };
    private SmsMessage mMessage = null;
    /* access modifiers changed from: private */
    public boolean mRead = false;
    private final DialogInterface.OnClickListener mSaveListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            boolean unused = ClassZeroActivity.this.mRead = true;
            ClassZeroActivity.this.saveMessage();
            ClassZeroActivity.this.finish();
        }
    };
    private long mTimerSet = 0;

    private ContentValues extractContentValues(SmsMessage smsMessage) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("address", smsMessage.getDisplayOriginatingAddress());
        contentValues.put("date", new Long(System.currentTimeMillis()));
        contentValues.put(Telephony.TextBasedSmsColumns.PROTOCOL, Integer.valueOf(smsMessage.getProtocolIdentifier()));
        contentValues.put("read", Integer.valueOf(this.mRead ? 1 : 0));
        if (smsMessage.getPseudoSubject().length() > 0) {
            contentValues.put("subject", smsMessage.getPseudoSubject());
        }
        contentValues.put(Telephony.TextBasedSmsColumns.REPLY_PATH_PRESENT, Integer.valueOf(smsMessage.isReplyPathPresent() ? 1 : 0));
        contentValues.put(Telephony.TextBasedSmsColumns.SERVICE_CENTER, smsMessage.getServiceCenterAddress());
        return contentValues;
    }

    /* JADX INFO: finally extract failed */
    private Uri replaceMessage(SmsMessage smsMessage) {
        ContentValues extractContentValues = extractContentValues(smsMessage);
        extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, smsMessage.getMessageBody());
        ContentResolver contentResolver = getContentResolver();
        Cursor query = SqliteWrapper.query(this, contentResolver, Telephony.Sms.Inbox.CONTENT_URI, REPLACE_PROJECTION, "address = ? AND protocol = ?", new String[]{smsMessage.getOriginatingAddress(), Integer.toString(smsMessage.getProtocolIdentifier())}, null);
        try {
            if (query.moveToFirst()) {
                Uri withAppendedId = ContentUris.withAppendedId(Telephony.Sms.CONTENT_URI, query.getLong(0));
                SqliteWrapper.update(this, contentResolver, withAppendedId, extractContentValues, null, null);
                query.close();
                return withAppendedId;
            }
            query.close();
            return storeMessage(smsMessage);
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void saveMessage() {
        Uri replaceMessage = this.mMessage.isReplace() ? replaceMessage(this.mMessage) : storeMessage(this.mMessage);
        if (!this.mRead && replaceMessage != null) {
            MessagingNotification.updateNewMessageIndicator(this, true);
        }
    }

    private Uri storeMessage(SmsMessage smsMessage) {
        ContentValues extractContentValues = extractContentValues(smsMessage);
        extractContentValues.put(Telephony.TextBasedSmsColumns.BODY, smsMessage.getDisplayMessageBody());
        ContentResolver contentResolver = getContentResolver();
        if (Config.DEBUG) {
            Log.d(TAG, "storeMessage " + toString());
        }
        return SqliteWrapper.insert(this, contentResolver, Telephony.Sms.Inbox.CONTENT_URI, extractContentValues);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawableResource(R.drawable.class_zero_background);
        this.mMessage = SmsMessage.createFromPdu(getIntent().getByteArrayExtra("pdu"));
        String messageBody = this.mMessage.getMessageBody();
        String obj = messageBody.toString();
        if (TextUtils.isEmpty(obj)) {
            finish();
            return;
        }
        if (obj.length() < BUFFER_OFFSET) {
            messageBody = BUFFER + obj + BUFFER;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        this.mDialog = new AlertDialog.Builder(this).setMessage(messageBody).setPositiveButton((int) R.string.save, this.mSaveListener).setNegativeButton(17039360, this.mCancelListener).setCancelable(false).show();
        this.mTimerSet = uptimeMillis + DEFAULT_TIMER;
        if (bundle != null) {
            this.mTimerSet = bundle.getLong(TIMER_FIRE, this.mTimerSet);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putLong(TIMER_FIRE, this.mTimerSet);
        if (Config.DEBUG) {
            Log.d(TAG, "onSaveInstanceState time = " + Long.toString(this.mTimerSet) + " " + toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mTimerSet <= SystemClock.uptimeMillis()) {
            this.mHandler.sendEmptyMessage(1);
            return;
        }
        this.mHandler.sendEmptyMessageAtTime(1, this.mTimerSet);
        if (Config.DEBUG) {
            Log.d(TAG, "onRestart time = " + Long.toString(this.mTimerSet) + " " + toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mHandler.removeMessages(1);
        if (Config.DEBUG) {
            Log.d(TAG, "onStop time = " + Long.toString(this.mTimerSet) + " " + toString());
        }
    }
}
