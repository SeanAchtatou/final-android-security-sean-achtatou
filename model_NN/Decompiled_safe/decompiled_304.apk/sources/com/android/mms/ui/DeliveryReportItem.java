package com.android.mms.ui;

public class DeliveryReportItem {
    String recipient;
    String status;

    public DeliveryReportItem(String str, String str2) {
        this.recipient = str;
        this.status = str2;
    }
}
