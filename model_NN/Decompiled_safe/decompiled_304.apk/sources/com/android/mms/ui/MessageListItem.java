package com.android.mms.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.android.mms.transaction.MessagingNotification;
import com.android.mms.transaction.TransactionService;
import com.android.mms.ui.MessageItem;
import com.android.mms.util.DownloadManager;
import com.android.mms.util.SmileyParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.util.EntityUtils;
import vc.lx.sms.caches.RemoteResourceManager;
import vc.lx.sms.cmcc.MMHttpDefines;
import vc.lx.sms.cmcc.error.SmsException;
import vc.lx.sms.cmcc.error.SmsParseException;
import vc.lx.sms.cmcc.http.data.MiguType;
import vc.lx.sms.cmcc.http.data.SongItem;
import vc.lx.sms.cmcc.http.data.VoteItem;
import vc.lx.sms.cmcc.http.data.VoteOption;
import vc.lx.sms.cmcc.http.parser.FetchSongParser;
import vc.lx.sms.cmcc.http.parser.JsonSongItemParser;
import vc.lx.sms.cmcc.http.parser.VoteSendAnswerParser;
import vc.lx.sms.cmcc.http.parser.VoteSingleGetParser;
import vc.lx.sms.db.ImagesDbService;
import vc.lx.sms.db.TopMusicService;
import vc.lx.sms.db.VoteContentProvider;
import vc.lx.sms.service.AbstractAsyncTask;
import vc.lx.sms.ui.ImageDetailActivity;
import vc.lx.sms.ui.widget.VoteResultView;
import vc.lx.sms.util.MessageUtils;
import vc.lx.sms.util.PrefsUtil;
import vc.lx.sms.util.Util;
import vc.lx.sms2.R;

public class MessageListItem extends LinearLayout implements SlideViewInterface, View.OnClickListener {
    public static final String EXTRA_URLS = "com.android.mms.ExtraUrls";
    public static final int MSG_LIST_EDIT_MMS = 1;
    public static final int MSG_LIST_EDIT_SMS = 2;
    public static final int MSG_LIST_NOTIFICATION = 4;
    public static final int MSG_LIST_POPUP = 3;
    private static final StyleSpan STYLE_BOLD = new StyleSpan(1);
    private static final String TAG = "MessageListItem";
    private static MediaPlayer mMediaPlayer;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public LayoutInflater inflater;
    private TextView mBodyTextView;
    /* access modifiers changed from: private */
    public Button mDownloadButton;
    /* access modifiers changed from: private */
    public TextView mDownloadingLabel;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public boolean mHasVote = false;
    private ImageView mImageView;
    private TextView mMessageDate;
    /* access modifiers changed from: private */
    public MessageItem mMessageItem;
    private View mMmsLayoutViewParent;
    private View mMmsView;
    private View mMsgListItem;
    private boolean mMultiRecipient = false;
    private ToggleButton mPlayBtn;
    private ProgressBar mProgressBar;
    private ImageView mRightStatusIndicator;
    private RemoteResourceManager mRrm;
    private ImageButton mSlideShowButton;
    private View mSmsImageAttachmentView;
    private View mSmsImageProgressView;
    private ImageView mSmsImageView;
    /* access modifiers changed from: private */
    public List<SongItem> mSongItems;
    private TextView mTimeView;
    private ViewStub mVoteViewStub;
    private View showView;
    /* access modifiers changed from: private */
    public TopMusicService topMusicService;
    private ViewStub viewStub;
    /* access modifiers changed from: private */
    public Button voteDetailBtn;
    private WindowManager windowManager;

    class FetchMusicTask extends AbstractAsyncTask {
        private String plug;

        public FetchMusicTask(Context context, String str) {
            super(context);
            this.plug = str;
            this.mEnginehost = this.mContext.getString(R.string.dna_engine_host_version_b);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.FetchSongItem(this.mEnginehost, this.mContext, this.plug);
        }

        public String getTag() {
            return "FetchMusicTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType == null || !(miguType instanceof SongItem)) {
                Toast.makeText(MessageListItem.this.context, MessageListItem.this.context.getString(R.string.network_error), 0).show();
                return;
            }
            final SongItem songItem = (SongItem) miguType;
            MessageListItem.this.mHandler.post(new Runnable() {
                public void run() {
                    MessageListItem.this.playMusic(songItem.durl);
                }
            });
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new FetchSongParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class FetchSongItem extends AbstractAsyncTask {
        private String formattedMessage;
        private Context mContext;
        private String plug;

        public FetchSongItem(Context context, String str, String str2) {
            super(context);
            this.mContext = context;
            this.plug = str;
            this.formattedMessage = str2;
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.FetchSongItemByPlug(MessageListItem.this.context.getString(R.string.shorten_url_engine_host), this.mContext, this.plug);
        }

        public String getTag() {
            return "FetchSongItem";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null) {
                MessageListItem.this.topMusicService.insert((SongItem) miguType, 2);
                MessageListItem.this.mSongItems.clear();
                List unused = MessageListItem.this.mSongItems = MessageListItem.this.topMusicService.queryAll();
                MessageListItem.this.bindCommonMessage(MessageListItem.this.mMessageItem);
            }
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new JsonSongItemParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class FetchVoteDetailTask extends AbstractAsyncTask {
        private VoteItem mVoteItem;

        public FetchVoteDetailTask(Context context, VoteItem voteItem) {
            super(context);
            this.mVoteItem = voteItem;
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.fatchDetailById(this.mEnginehost, this.mContext, this.mVoteItem.service_id);
        }

        public String getTag() {
            return "FetchVoteDetailTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                VoteItem voteItem = (VoteItem) miguType;
                voteItem.title = this.mVoteItem.title;
                for (int i = 0; i < voteItem.options.size(); i++) {
                    String str = voteItem.options.get(i).service_id;
                    for (int i2 = 0; i2 < this.mVoteItem.options.size(); i2++) {
                        if (str != null && str.equals(this.mVoteItem.options.get(i2).service_id)) {
                            voteItem.options.get(i).content = this.mVoteItem.options.get(i2).content;
                        }
                    }
                }
                MessageListItem.this.createVoteDetailDialog(voteItem);
            }
            if (MessageListItem.this.voteDetailBtn != null) {
                MessageListItem.this.voteDetailBtn.setClickable(true);
                MessageListItem.this.voteDetailBtn.setEnabled(true);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                return new VoteSendAnswerParser().parse(EntityUtils.toString(httpResponse.getEntity()));
            } catch (SmsParseException e) {
                e.printStackTrace();
                return null;
            } catch (SmsException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class VoteDetailView {
        LinearLayout mLinearLayout;
        TextView mOptionTitleCount;
        TextView mOptionTitleView;

        VoteDetailView() {
        }
    }

    class VoteGetTask extends AbstractAsyncTask {
        private int page;
        String plug_id;

        public VoteGetTask(Context context, int i, String str) {
            super(context);
            this.page = i;
            this.plug_id = str;
            this.mEnginehost = this.mContext.getString(R.string.dna_vote_engine_host_version_b);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.voteSearchByPlug(this.mEnginehost, this.mContext, this.plug_id);
        }

        public String getTag() {
            return "VoteGetTask";
        }

        /* JADX WARNING: Removed duplicated region for block: B:18:0x00e1 A[LOOP:0: B:16:0x00d9->B:18:0x00e1, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0150  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPostLogic(vc.lx.sms.cmcc.http.data.MiguType r13) {
            /*
                r12 = this;
                r9 = 0
                r8 = 0
                java.lang.String r10 = "service_id"
                java.lang.String r0 = "_id"
                if (r13 == 0) goto L_0x015f
                boolean r0 = r13 instanceof vc.lx.sms.cmcc.http.data.VoteItem
                if (r0 == 0) goto L_0x015f
                long r0 = java.lang.System.currentTimeMillis()
                com.android.mms.ui.MessageListItem r2 = com.android.mms.ui.MessageListItem.this
                android.content.Context r2 = r2.getContext()
                java.lang.CharSequence r0 = vc.lx.sms.util.Util.getRelativeDateSpanString(r0, r2)
                java.lang.String r0 = r0.toString()
                com.android.mms.ui.MessageListItem r1 = com.android.mms.ui.MessageListItem.this
                android.content.Context r1 = r1.getContext()
                vc.lx.sms.util.Util.SetLastTimeOfRecommendingMusicValue(r1, r0)
                vc.lx.sms.cmcc.http.data.VoteItem r13 = (vc.lx.sms.cmcc.http.data.VoteItem) r13
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "http://lexin.cc/v/"
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r1 = r12.plug_id
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.String r0 = r0.toString()
                r13.plug = r0
                android.content.ContentValues r0 = new android.content.ContentValues
                r0.<init>()
                java.lang.String r1 = "service_id"
                java.lang.String r1 = r13.service_id
                r0.put(r10, r1)
                java.lang.String r1 = "plug"
                com.android.mms.ui.MessageListItem r2 = com.android.mms.ui.MessageListItem.this
                android.content.Context r2 = r2.getContext()
                java.lang.String r3 = r12.plug_id
                java.lang.String r2 = vc.lx.sms.util.Util.getFullShortenVoteUrl(r2, r3)
                r0.put(r1, r2)
                java.lang.String r1 = "title"
                java.lang.String r2 = r13.title
                r0.put(r1, r2)
                java.lang.String r1 = "counter"
                int r2 = r13.counter
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                r0.put(r1, r2)
                com.android.mms.ui.MessageListItem r1 = com.android.mms.ui.MessageListItem.this
                android.content.Context r1 = r1.getContext()
                android.content.ContentResolver r1 = r1.getContentResolver()
                android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
                r1.notifyChange(r2, r8)
                com.android.mms.ui.MessageListItem r1 = com.android.mms.ui.MessageListItem.this
                android.content.Context r1 = r1.getContext()
                android.content.ContentResolver r1 = r1.getContentResolver()
                android.net.Uri r2 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI
                android.net.Uri r0 = r1.insert(r2, r0)
                if (r0 == 0) goto L_0x00d8
                java.lang.String r0 = r0.getFragment()
                java.lang.Long r0 = java.lang.Long.valueOf(r0)
                long r5 = r0.longValue()
                com.android.mms.ui.MessageListItem r0 = com.android.mms.ui.MessageListItem.this     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                android.net.Uri r1 = vc.lx.sms.db.VoteContentProvider.VOTEQUESTIONS_CONTENT_URI     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                r2 = 1
                java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                r3 = 0
                java.lang.String r4 = "_id"
                r2[r3] = r4     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                java.lang.String r3 = " _id = ? "
                r4 = 1
                java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                r7 = 0
                java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                r4[r7] = r5     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                r5 = 0
                android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0144, all -> 0x014c }
                boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0165, all -> 0x0160 }
                if (r1 == 0) goto L_0x00d3
                java.lang.String r1 = "_id"
                int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0165, all -> 0x0160 }
                long r1 = r0.getLong(r1)     // Catch:{ Exception -> 0x0165, all -> 0x0160 }
                r13.cli_id = r1     // Catch:{ Exception -> 0x0165, all -> 0x0160 }
            L_0x00d3:
                if (r0 == 0) goto L_0x00d8
                r0.close()
            L_0x00d8:
                r1 = r9
            L_0x00d9:
                java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r13.options
                int r0 = r0.size()
                if (r1 >= r0) goto L_0x0154
                android.content.ContentValues r2 = new android.content.ContentValues
                r2.<init>()
                java.lang.String r0 = "service_id"
                java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r13.options
                java.lang.Object r0 = r0.get(r1)
                vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
                java.lang.String r0 = r0.service_id
                r2.put(r10, r0)
                java.lang.String r3 = "option_text"
                java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r13.options
                java.lang.Object r0 = r0.get(r1)
                vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
                java.lang.String r0 = r0.content
                r2.put(r3, r0)
                java.lang.String r3 = "display_order"
                java.util.ArrayList<vc.lx.sms.cmcc.http.data.VoteOption> r0 = r13.options
                java.lang.Object r0 = r0.get(r1)
                vc.lx.sms.cmcc.http.data.VoteOption r0 = (vc.lx.sms.cmcc.http.data.VoteOption) r0
                int r0 = r0.display_order
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
                r2.put(r3, r0)
                java.lang.String r0 = "vote_id"
                long r3 = r13.cli_id
                java.lang.Long r3 = java.lang.Long.valueOf(r3)
                r2.put(r0, r3)
                com.android.mms.ui.MessageListItem r0 = com.android.mms.ui.MessageListItem.this
                android.content.Context r0 = r0.getContext()
                android.content.ContentResolver r0 = r0.getContentResolver()
                android.net.Uri r3 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
                r0.notifyChange(r3, r8)
                com.android.mms.ui.MessageListItem r0 = com.android.mms.ui.MessageListItem.this
                android.content.Context r0 = r0.getContext()
                android.content.ContentResolver r0 = r0.getContentResolver()
                android.net.Uri r3 = vc.lx.sms.db.VoteContentProvider.VOTEOPTIONS_CONTENT_URI
                r0.insert(r3, r2)
                int r0 = r1 + 1
                r1 = r0
                goto L_0x00d9
            L_0x0144:
                r0 = move-exception
                r0 = r8
            L_0x0146:
                if (r0 == 0) goto L_0x00d8
                r0.close()
                goto L_0x00d8
            L_0x014c:
                r0 = move-exception
                r1 = r8
            L_0x014e:
                if (r1 == 0) goto L_0x0153
                r1.close()
            L_0x0153:
                throw r0
            L_0x0154:
                com.android.mms.ui.MessageListItem r0 = com.android.mms.ui.MessageListItem.this
                com.android.mms.ui.MessageListItem r1 = com.android.mms.ui.MessageListItem.this
                com.android.mms.ui.MessageItem r1 = r1.mMessageItem
                r0.bindCommonMessage(r1)
            L_0x015f:
                return
            L_0x0160:
                r1 = move-exception
                r11 = r1
                r1 = r0
                r0 = r11
                goto L_0x014e
            L_0x0165:
                r1 = move-exception
                goto L_0x0146
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.mms.ui.MessageListItem.VoteGetTask.onPostLogic(vc.lx.sms.cmcc.http.data.MiguType):void");
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new VoteSingleGetParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    class VoteOptionAdatper extends BaseAdapter {
        private Context mContext;
        private VoteItem mVoteItem;

        public VoteOptionAdatper(Context context, VoteItem voteItem) {
            this.mVoteItem = voteItem;
            this.mContext = context;
        }

        public int getCount() {
            return this.mVoteItem.options.size();
        }

        public Object getItem(int i) {
            return this.mVoteItem.options.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            VoteDetailView voteDetailView;
            View view2;
            if (view == null) {
                voteDetailView = new VoteDetailView();
                view2 = MessageListItem.this.inflater.inflate((int) R.layout.show_vote_detail_item, (ViewGroup) null);
                voteDetailView.mOptionTitleView = (TextView) view2.findViewById(R.id.option_title);
                voteDetailView.mOptionTitleCount = (TextView) view2.findViewById(R.id.option_count);
                voteDetailView.mLinearLayout = (LinearLayout) view2.findViewById(R.id.option_count_image);
                view2.setTag(voteDetailView);
            } else {
                voteDetailView = (VoteDetailView) view.getTag();
                view2 = view;
            }
            voteDetailView.mOptionTitleView.setText(this.mVoteItem.options.get(i).content);
            voteDetailView.mOptionTitleCount.setText("(" + String.valueOf(this.mVoteItem.options.get(i).counter) + ")");
            if (this.mVoteItem.counter > 0) {
                voteDetailView.mLinearLayout.addView(new VoteResultView(this.mContext, (this.mVoteItem.options.get(i).counter * 100) / this.mVoteItem.counter, "", true));
            }
            return view2;
        }
    }

    class VoteSendAnswerTask extends AbstractAsyncTask {
        VoteItem mVoteItem;
        String option_id;
        private int page;
        String plug;

        public VoteSendAnswerTask(Context context, VoteItem voteItem, int i, String str, String str2) {
            super(context);
            this.page = i;
            this.plug = str;
            this.mVoteItem = voteItem;
            this.option_id = str2;
            this.mEnginehost = this.mContext.getString(R.string.dna_engine_host_version_b);
        }

        public HttpResponse getRespFromServer() throws IOException {
            return this.mCmccHttpApi.voteSendAnswerAction(this.mEnginehost, this.mContext, this.plug, this.option_id);
        }

        public String getTag() {
            return "VoteSendAnswerTask";
        }

        public void onPostLogic(MiguType miguType) {
            if (miguType != null && (miguType instanceof VoteItem)) {
                Util.SetLastTimeOfRecommendingMusicValue(MessageListItem.this.getContext(), Util.getRelativeDateSpanString(System.currentTimeMillis(), MessageListItem.this.getContext()).toString());
                VoteItem voteItem = (VoteItem) miguType;
                voteItem.plug = this.plug;
                ContentValues contentValues = new ContentValues();
                contentValues.put("counter", String.valueOf(voteItem.counter));
                MessageListItem.this.getContext().getContentResolver().update(Uri.parse(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI + "/plug/#" + voteItem.plug), contentValues, null, null);
                for (int i = 0; i < voteItem.options.size(); i++) {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("counter", String.valueOf(voteItem.options.get(i).counter));
                    MessageListItem.this.getContext().getContentResolver().update(Uri.parse(VoteContentProvider.VOTEOPTIONS_CONTENT_URI + "/ser_id/" + voteItem.options.get(i).service_id), contentValues2, null, null);
                }
                new FetchVoteDetailTask(MessageListItem.this.getContext(), this.mVoteItem).execute(new Void[0]);
                if (MessageListItem.this.voteDetailBtn != null) {
                    MessageListItem.this.voteDetailBtn.setClickable(false);
                    MessageListItem.this.voteDetailBtn.setEnabled(false);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public MiguType parseEntity(HttpResponse httpResponse) {
            try {
                try {
                    return new VoteSendAnswerParser().parse(EntityUtils.toString(httpResponse.getEntity(), "UTF-8"));
                } catch (SmsParseException e) {
                    e.printStackTrace();
                    return null;
                } catch (SmsException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            }
        }
    }

    public MessageListItem(Context context2) {
        super(context2);
        this.context = context2;
    }

    public MessageListItem(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
    }

    /* access modifiers changed from: private */
    public void bindCommonMessage(MessageItem messageItem) {
        if (this.mDownloadButton != null) {
            this.mDownloadButton.setVisibility(8);
            this.mDownloadingLabel.setVisibility(8);
        }
        CharSequence formatMessage = formatMessage(getDisplayNameByAddress(messageItem.getAddress()), messageItem.mBody, messageItem.mSubject, "", messageItem.mHighlight, this.mMultiRecipient, messageItem);
        messageItem.setCachedFormattedMessage(formatMessage);
        this.mBodyTextView.setText(formatMessage);
        if (formatMessage.length() == 0) {
            this.mBodyTextView.setVisibility(8);
        } else {
            this.mBodyTextView.setVisibility(0);
        }
        this.mMessageDate.setText(MessageUtils.formatTimeStampString(this.context, messageItem.mOriginalTimestamp));
        CheckBox checkBox = (CheckBox) findViewById(R.id.multi_checkbox);
        if (messageItem.mContact.equals(this.context.getString(R.string.messagelist_sender_self))) {
            this.mMmsLayoutViewParent.setBackgroundResource(R.drawable.btn_message_item_me);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            if (checkBox.getVisibility() == 8) {
                layoutParams.addRule(11);
            } else {
                layoutParams.addRule(0, R.id.multi_checkbox);
            }
            layoutParams.addRule(3, R.id.message_date);
            this.mMmsLayoutViewParent.setLayoutParams(layoutParams);
            this.mRightStatusIndicator.setVisibility(0);
        } else {
            this.mMmsLayoutViewParent.setBackgroundResource(R.drawable.btn_message_item_other);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(9);
            layoutParams2.addRule(3, R.id.message_date);
            this.mMmsLayoutViewParent.setLayoutParams(layoutParams2);
            this.mRightStatusIndicator.setVisibility(8);
        }
        if (messageItem.isSms()) {
            hideMmsViewIfNeeded();
        } else {
            PresenterFactory.getPresenter("MmsThumbnailPresenter", getContext(), this, messageItem.mSlideshow).present();
            if (messageItem.mAttachmentType != 0) {
                inflateMmsView();
                this.mMmsView.setVisibility(0);
                setOnClickListener(messageItem);
                drawPlaybackButton(messageItem);
            } else {
                hideMmsViewIfNeeded();
            }
        }
        this.mMmsLayoutViewParent.setFocusable(true);
        this.mMmsLayoutViewParent.setClickable(true);
        this.mMmsLayoutViewParent.setLongClickable(true);
        drawLeftStatusIndicator(messageItem.mBoxId);
        drawRightStatusIndicator(messageItem);
    }

    private void bindNotifInd(final MessageItem messageItem) {
        hideMmsViewIfNeeded();
        this.mBodyTextView.setText(formatMessage(messageItem.mContact, null, messageItem.mSubject, (getContext().getString(R.string.message_size_label) + String.valueOf((messageItem.mMessageSize + MMHttpDefines.REQ_TYPE_GET_ONLINE_LRC) / MMHttpDefines.REQ_TYPE_QUERY_MONTH) + getContext().getString(R.string.kilobyte)) + "\n" + MessageUtils.formatTimeStampString(this.context, messageItem.mOriginalTimestamp), messageItem.mHighlight, this.mMultiRecipient, messageItem));
        this.mMessageDate.setText(MessageUtils.formatTimeStampString(this.context, messageItem.mOriginalTimestamp));
        if (messageItem.mContact.equals(this.context.getString(R.string.messagelist_sender_self))) {
            this.mMmsLayoutViewParent.setBackgroundResource(R.drawable.btn_message_item_me);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(11);
            layoutParams.addRule(3, R.id.message_date);
            this.mMmsLayoutViewParent.setLayoutParams(layoutParams);
            this.mRightStatusIndicator.setVisibility(0);
        } else {
            this.mMmsLayoutViewParent.setBackgroundResource(R.drawable.btn_message_item_other);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(9);
            layoutParams2.addRule(3, R.id.message_date);
            this.mMmsLayoutViewParent.setLayoutParams(layoutParams2);
            this.mRightStatusIndicator.setVisibility(8);
        }
        this.mMmsLayoutViewParent.setFocusable(true);
        this.mMmsLayoutViewParent.setClickable(true);
        this.mMmsLayoutViewParent.setLongClickable(true);
        switch (DownloadManager.getInstance().getState(messageItem.mMessageUri)) {
            case 129:
                inflateDownloadControls();
                this.mDownloadingLabel.setVisibility(0);
                this.mDownloadButton.setVisibility(8);
                break;
            default:
                setLongClickable(true);
                inflateDownloadControls();
                this.mDownloadingLabel.setVisibility(8);
                this.mDownloadButton.setVisibility(0);
                this.mDownloadButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        MessageListItem.this.mDownloadingLabel.setVisibility(0);
                        MessageListItem.this.mDownloadButton.setVisibility(8);
                        Intent intent = new Intent(MessageListItem.this.getContext(), TransactionService.class);
                        intent.putExtra("uri", messageItem.mMessageUri.toString());
                        intent.putExtra("type", 1);
                        MessageListItem.this.getContext().startService(intent);
                    }
                });
                break;
        }
        this.mRightStatusIndicator.setVisibility(8);
        drawLeftStatusIndicator(messageItem.mBoxId);
    }

    private String checkImageShortenUrl(MessageItem messageItem, String str) {
        Matcher matcher = Pattern.compile(getContext().getString(R.string.shorten_url_img_reg), 32).matcher(str);
        if (matcher != null && matcher.find()) {
            String group = matcher.group(1);
            if (group != null) {
                this.mSmsImageAttachmentView.setVisibility(0);
                messageItem.mSmsImagePlug = group;
                String replace = str.replace(matcher.group(), "");
                final ImagesDbService.ImageItem queryByPlug = new ImagesDbService(getContext()).queryByPlug(group);
                if (queryByPlug != null) {
                    Uri parse = Uri.parse(queryByPlug.thumb);
                    if (this.mRrm.exists(parse)) {
                        try {
                            this.mSmsImageView.setImageBitmap(BitmapFactory.decodeStream(this.mRrm.getInputStream(parse)));
                            this.mSmsImageView.setVisibility(0);
                            this.mSmsImageView.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    Intent intent = new Intent(MessageListItem.this.getContext(), ImageDetailActivity.class);
                                    intent.putExtra("full_url", queryByPlug.url);
                                    MessageListItem.this.getContext().startActivity(intent);
                                }
                            });
                            this.mSmsImageProgressView.setVisibility(8);
                            return replace;
                        } catch (IOException e) {
                            this.mSmsImageView.setVisibility(8);
                            return replace;
                        }
                    } else {
                        this.mRrm.request(parse);
                        return replace;
                    }
                } else {
                    this.mRrm.request(Uri.parse(Util.getFullShortenImageUrl(this.context, group)));
                    return replace;
                }
            }
        } else if (this.mSmsImageAttachmentView != null) {
            this.mSmsImageAttachmentView.setVisibility(8);
        }
        return str;
    }

    private String checkMusicShortenUrl(MessageItem messageItem, String str) {
        Matcher matcher = Pattern.compile(getContext().getString(R.string.shorten_url_reg), 32).matcher(str);
        if (matcher != null && matcher.find()) {
            String group = matcher.group(1);
            SongItem songItem = null;
            int i = 0;
            while (true) {
                SongItem songItem2 = songItem;
                if (i >= this.mSongItems.size()) {
                    songItem = songItem2;
                    break;
                }
                if (this.mSongItems.get(i).plug.equals(group)) {
                    songItem = this.mSongItems.get(i);
                    if (songItem != null) {
                        break;
                    }
                } else {
                    songItem = songItem2;
                }
                i++;
            }
            if (this.viewStub != null) {
                this.viewStub.setVisibility(0);
            }
            if (songItem == null && messageItem.mSongItem == null) {
                if (this.viewStub != null) {
                    this.viewStub.setVisibility(8);
                }
                new FetchSongItem(getContext(), group, str).execute(new Void[0]);
            } else {
                String replace = str.replace(matcher.group(), "");
                if (messageItem.mSongItem == null) {
                    messageItem.mSongItem = songItem;
                }
                if (replace.length() == 0) {
                    ((ImageView) findViewById(R.id.split_message_item_music)).setVisibility(8);
                } else {
                    ((ImageView) findViewById(R.id.split_message_item_music)).setVisibility(0);
                }
                commenPlayer(messageItem.mSongItem, replace, messageItem);
                return replace;
            }
        } else if (this.viewStub != null) {
            this.viewStub.setVisibility(8);
        }
        return str;
    }

    private String checkVoteShortenUrl(MessageItem messageItem, String str) {
        Matcher matcher = Pattern.compile(getContext().getString(R.string.shorten_url_vote_reg), 32).matcher(str);
        if (matcher != null && matcher.find()) {
            String group = matcher.group(0);
            String group2 = matcher.group(1);
            if (messageItem.mVoteItem == null) {
                VoteItem voteItem = new VoteItem();
                Cursor query = getContext().getContentResolver().query(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, null, " plug = ? ", new String[]{group}, null);
                if (query.getCount() > 0) {
                    if (query.moveToNext()) {
                        if (query.getInt(query.getColumnIndex("parent_id")) == 0) {
                            voteItem.cli_id = (long) query.getInt(query.getColumnIndex("_id"));
                            voteItem.service_id = query.getString(query.getColumnIndex("service_id"));
                            voteItem.answer_flag = query.getString(query.getColumnIndex("answer_flag"));
                            voteItem.title = query.getString(query.getColumnIndex("title"));
                            voteItem.counter = query.getInt(query.getColumnIndex("counter"));
                            voteItem.plug = group;
                        } else {
                            voteItem.cli_id = (long) query.getInt(query.getColumnIndex("parent_id"));
                            voteItem.service_id = query.getString(query.getColumnIndex("service_id"));
                            voteItem.title = query.getString(query.getColumnIndex("title"));
                            voteItem.answer_flag = query.getString(query.getColumnIndex("answer_flag"));
                            voteItem.counter = query.getInt(query.getColumnIndex("counter"));
                            voteItem.plug = group;
                        }
                        if (voteItem.answer_flag.equals("1")) {
                            this.mHasVote = true;
                        }
                    }
                    query.close();
                    Cursor query2 = getContext().getContentResolver().query(VoteContentProvider.VOTEOPTIONS_CONTENT_URI, null, " vote_id = ? ", new String[]{String.valueOf(voteItem.cli_id)}, " display_order asc ");
                    while (query2.moveToNext()) {
                        VoteOption voteOption = new VoteOption();
                        voteOption.display_order = query2.getInt(query2.getColumnIndex("display_order"));
                        voteOption.cli_id = (long) query2.getInt(query2.getColumnIndex("_id"));
                        voteOption.service_id = query2.getString(query2.getColumnIndex("service_id"));
                        voteOption.content = query2.getString(query2.getColumnIndex("option_text"));
                        voteItem.options.add(voteOption);
                    }
                    messageItem.mVoteItem = voteItem;
                    query2.close();
                } else {
                    new VoteGetTask(getContext(), 1, group2).execute(new Void[0]);
                }
            }
            if (this.mVoteViewStub != null) {
                this.mVoteViewStub.setVisibility(0);
            }
            if (messageItem.mVoteItem != null && messageItem.mVoteItem.plug != null) {
                String replace = str.replace(matcher.group(), "");
                if (replace.length() == 0) {
                    ((ImageView) findViewById(R.id.split_message_item_vote)).setVisibility(8);
                } else {
                    ((ImageView) findViewById(R.id.split_message_item_vote)).setVisibility(0);
                }
                votePlayer(messageItem.mVoteItem, replace, messageItem);
                return replace;
            } else if (this.mVoteViewStub != null) {
                this.mVoteViewStub.setVisibility(8);
            }
        } else if (this.mVoteViewStub != null) {
            this.mVoteViewStub.setVisibility(8);
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void createAnswerDialog(VoteItem voteItem, VoteOption voteOption, String str, String str2) {
        final Dialog dialog = new Dialog(this.context, R.style.CustomDialogTheme);
        View inflate = LayoutInflater.from(this.context).inflate((int) R.layout.answer_dialog, (ViewGroup) null);
        final VoteOption voteOption2 = voteOption;
        final String str3 = str;
        final VoteItem voteItem2 = voteItem;
        final String str4 = str2;
        ((Button) inflate.findViewById(R.id.answer_yes)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MessageListItem.this.quickSendSms(str3, voteOption2.display_order + "." + voteOption2.content);
                boolean unused = MessageListItem.this.mHasVote = true;
                voteItem2.answer_flag = "1";
                ContentValues contentValues = new ContentValues();
                VoteItem voteItem = voteItem2;
                int i = voteItem.counter + 1;
                voteItem.counter = i;
                contentValues.put("counter", Integer.valueOf(i));
                contentValues.put("answer_flag", "1");
                MessageListItem.this.getContext().getContentResolver().update(VoteContentProvider.VOTEQUESTIONS_CONTENT_URI, contentValues, " _id = ? ", new String[]{String.valueOf(voteItem2.cli_id)});
                if (Util.checkNetWork(MessageListItem.this.getContext())) {
                    new VoteSendAnswerTask(MessageListItem.this.getContext(), voteItem2, 1, str4, voteOption2.service_id).execute(new Void[0]);
                    dialog.dismiss();
                }
            }
        });
        ((Button) inflate.findViewById(R.id.answer_no)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) inflate.findViewById(R.id.answer_title)).setText(this.context.getString(R.string.you_have_selected) + voteOption.content);
        dialog.setContentView(inflate);
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void createVoteDetailDialog(VoteItem voteItem) {
        final Dialog dialog = new Dialog(this.context, R.style.CustomDialogTheme);
        View inflate = LayoutInflater.from(this.context).inflate((int) R.layout.show_vote_detail, (ViewGroup) null);
        ((ImageView) inflate.findViewById(R.id.no)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) inflate.findViewById(R.id.vote_title)).setText(this.context.getString(R.string.theme) + voteItem.title);
        ((ListView) inflate.findViewById(R.id.vote_detail_listview)).setAdapter((ListAdapter) new VoteOptionAdatper(getContext(), voteItem));
        dialog.setContentView(inflate);
        dialog.show();
    }

    private void drawLeftStatusIndicator(int i) {
        switch (i) {
        }
    }

    private void drawPlaybackButton(MessageItem messageItem) {
        switch (messageItem.mAttachmentType) {
            case 2:
            case 3:
            case 4:
                this.mSlideShowButton.setTag(messageItem);
                this.mSlideShowButton.setOnClickListener(this);
                this.mSlideShowButton.setVisibility(0);
                setLongClickable(true);
                setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        MessageListItem.this.onMessageListItemClick();
                    }
                });
                return;
            default:
                this.mSlideShowButton.setVisibility(8);
                return;
        }
    }

    private void drawRightStatusIndicator(MessageItem messageItem) {
        if (messageItem.isOutgoingMessage()) {
            if (isFailedMessage(messageItem)) {
                this.mRightStatusIndicator.setImageResource(R.drawable.bg_status_fail);
                setErrorIndicatorClickListener(messageItem);
            } else {
                this.mRightStatusIndicator.setImageResource(R.drawable.bg_status_unaccept);
            }
            this.mRightStatusIndicator.setVisibility(0);
        } else if (messageItem.mDeliveryReport || messageItem.mReadReport) {
            if (messageItem.mDeliveryStatus == MessageItem.DeliveryStatus.FAILED) {
                this.mRightStatusIndicator.setImageResource(R.drawable.bg_status_fail);
                this.mRightStatusIndicator.setVisibility(0);
            } else if (messageItem.mDeliveryStatus == MessageItem.DeliveryStatus.RECEIVED) {
                this.mRightStatusIndicator.setImageResource(R.drawable.bg_status_success);
                this.mRightStatusIndicator.setVisibility(0);
            } else if (messageItem.mDeliveryStatus == MessageItem.DeliveryStatus.PENDING) {
                this.mRightStatusIndicator.setImageResource(R.drawable.bg_status_unaccept);
            } else {
                this.mRightStatusIndicator.setImageResource(R.drawable.bg_status_success);
            }
            this.mRightStatusIndicator.setVisibility(0);
        } else if (messageItem.mLocked) {
            this.mRightStatusIndicator.setImageResource(R.drawable.ic_lock_message_sms);
            this.mRightStatusIndicator.setVisibility(0);
        } else {
            this.mRightStatusIndicator.setImageResource(R.drawable.bg_status_success);
        }
    }

    private CharSequence formatMessage(String str, String str2, String str3, String str4, String str5, boolean z, MessageItem messageItem) {
        SpannableStringBuilder spannableStringBuilder = (!z || str == null) ? new SpannableStringBuilder() : new SpannableStringBuilder(TextUtils.replace(getContext().getResources().getText(R.string.wo_to_other), new String[]{"%s"}, new CharSequence[]{str}));
        boolean z2 = !TextUtils.isEmpty(str3);
        if (z2) {
            spannableStringBuilder.append((CharSequence) getContext().getResources().getString(R.string.inline_subject, str3));
        }
        if (!TextUtils.isEmpty(str2)) {
            String checkVoteShortenUrl = checkVoteShortenUrl(messageItem, checkImageShortenUrl(messageItem, checkMusicShortenUrl(messageItem, str2)));
            if (z2) {
                spannableStringBuilder.append((CharSequence) " - ");
            }
            spannableStringBuilder.append(SmileyParser.getInstance().addSmileySpans(checkVoteShortenUrl));
        }
        if (!TextUtils.isEmpty(str4)) {
            spannableStringBuilder.append((CharSequence) "\n");
            int length = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence) "\n");
            spannableStringBuilder.setSpan(new AbsoluteSizeSpan(3), length, spannableStringBuilder.length(), 33);
            int length2 = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence) str4);
            spannableStringBuilder.setSpan(new AbsoluteSizeSpan(12), length2, spannableStringBuilder.length(), 33);
            spannableStringBuilder.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.timestamp_color)), length2, spannableStringBuilder.length(), 33);
        }
        if (str5 != null) {
            int length3 = str5.length();
            String lowerCase = spannableStringBuilder.toString().toLowerCase();
            int i = 0;
            while (true) {
                int indexOf = lowerCase.indexOf(str5, i);
                if (indexOf == -1) {
                    break;
                }
                spannableStringBuilder.setSpan(new StyleSpan(1), indexOf, indexOf + length3, 0);
                i = indexOf + length3;
            }
        }
        return spannableStringBuilder;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getDisplayNameByAddress(java.lang.String r10) {
        /*
            r9 = this;
            r7 = 0
            java.lang.String r3 = ">"
            java.lang.String r2 = "<"
            java.lang.String r1 = ""
            if (r10 == 0) goto L_0x0011
            java.lang.String r0 = ""
            boolean r0 = r10.equals(r1)
            if (r0 == 0) goto L_0x0015
        L_0x0011:
            java.lang.String r0 = ""
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            java.lang.String r0 = "<"
            boolean r0 = r10.contains(r2)
            if (r0 == 0) goto L_0x0083
            java.lang.String r0 = ">"
            boolean r0 = r10.contains(r3)
            if (r0 == 0) goto L_0x0083
            java.lang.String r0 = "<"
            int r0 = r10.indexOf(r2)
            int r0 = r0 + 1
            java.lang.String r1 = ">"
            int r1 = r10.indexOf(r3)
            java.lang.String r0 = r10.substring(r0, r1)
            r6 = r0
        L_0x0038:
            java.lang.String r0 = ""
            android.content.Context r0 = r9.getContext()     // Catch:{ Exception -> 0x0069, all -> 0x0072 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0069, all -> 0x0072 }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0069, all -> 0x0072 }
            r2 = 0
            java.lang.String r3 = "data1 = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0069, all -> 0x0072 }
            r5 = 0
            r4[r5] = r6     // Catch:{ Exception -> 0x0069, all -> 0x0072 }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0069, all -> 0x0072 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x007f, all -> 0x007a }
            if (r1 == 0) goto L_0x0081
            java.lang.String r1 = "display_name"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x007f, all -> 0x007a }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x007f, all -> 0x007a }
        L_0x0062:
            if (r0 == 0) goto L_0x0067
            r0.close()
        L_0x0067:
            r0 = r1
            goto L_0x0014
        L_0x0069:
            r0 = move-exception
            r0 = r7
        L_0x006b:
            if (r0 == 0) goto L_0x0070
            r0.close()
        L_0x0070:
            r0 = r6
            goto L_0x0014
        L_0x0072:
            r0 = move-exception
            r1 = r7
        L_0x0074:
            if (r1 == 0) goto L_0x0079
            r1.close()
        L_0x0079:
            throw r0
        L_0x007a:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0074
        L_0x007f:
            r1 = move-exception
            goto L_0x006b
        L_0x0081:
            r1 = r6
            goto L_0x0062
        L_0x0083:
            r6 = r10
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.ui.MessageListItem.getDisplayNameByAddress(java.lang.String):java.lang.String");
    }

    private void hideMmsViewIfNeeded() {
        if (this.mMmsView != null) {
            this.mMmsView.setVisibility(8);
        }
    }

    private void inflateDownloadControls() {
        if (this.mDownloadButton == null) {
            findViewById(R.id.mms_downloading_view_stub).setVisibility(0);
            this.mDownloadButton = (Button) findViewById(R.id.btn_download_msg);
            this.mDownloadingLabel = (TextView) findViewById(R.id.label_downloading);
        }
    }

    private void inflateMmsView() {
        if (this.mMmsView == null) {
            findViewById(R.id.mms_layout_view_stub).setVisibility(0);
            this.mMmsView = findViewById(R.id.mms_view);
            this.mImageView = (ImageView) findViewById(R.id.image_view);
            this.mSlideShowButton = (ImageButton) findViewById(R.id.play_slideshow_button);
        }
    }

    public static boolean isFailedMessage(MessageItem messageItem) {
        return (messageItem.isMms() && messageItem.mErrorType >= 10) || (messageItem.isSms() && messageItem.mBoxId == 5);
    }

    private void setErrorIndicatorClickListener(final MessageItem messageItem) {
        final int i = messageItem.mType.equals("sms") ? 2 : 1;
        this.mRightStatusIndicator.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (MessageListItem.this.mHandler != null) {
                    Message obtain = Message.obtain(MessageListItem.this.mHandler, i);
                    obtain.obj = new Long(messageItem.mMsgId);
                    obtain.sendToTarget();
                }
            }
        });
    }

    private void setOnClickListener(final MessageItem messageItem) {
        switch (messageItem.mAttachmentType) {
            case 1:
            case 2:
                this.mImageView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        MessageUtils.viewMmsMessageAttachment(MessageListItem.this.getContext(), null, messageItem.mSlideshow);
                    }
                });
                this.mImageView.setOnLongClickListener(new View.OnLongClickListener() {
                    public boolean onLongClick(View view) {
                        return view.showContextMenu();
                    }
                });
                return;
            default:
                this.mImageView.setOnClickListener(null);
                return;
        }
    }

    public static void stopPlayMusic() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public void bind(MessageItem messageItem, List<SongItem> list, RemoteResourceManager remoteResourceManager) {
        this.topMusicService = new TopMusicService(this.context);
        this.mMessageItem = messageItem;
        if (this.mSongItems == null) {
            this.mSongItems = list;
        }
        this.mRrm = remoteResourceManager;
        setLongClickable(false);
        switch (messageItem.mMessageType) {
            case 130:
                bindNotifInd(messageItem);
                return;
            default:
                bindCommonMessage(messageItem);
                return;
        }
    }

    public void commenPlayer(final SongItem songItem, String str, MessageItem messageItem) {
        ((TextView) findViewById(R.id.song)).setText(songItem.singer + " - " + songItem.song);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.song_in_message_item);
        this.mPlayBtn = (ToggleButton) findViewById(R.id.play_music);
        this.mProgressBar = (ProgressBar) findViewById(R.id.play_progress);
        this.mPlayBtn.setOnCheckedChangeListener(null);
        if (songItem.play_state == 1) {
            this.mPlayBtn.setChecked(true);
        } else {
            this.mPlayBtn.setChecked(false);
        }
        this.mPlayBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (z) {
                    new FetchMusicTask(MessageListItem.this.context, songItem.plug).execute(new Void[0]);
                    MessageListItem.this.mSongItems.size();
                    for (int i = 0; i < MessageListItem.this.mSongItems.size(); i++) {
                        if (MessageListItem.this.mSongItems.get(i) != null && !((SongItem) MessageListItem.this.mSongItems.get(i)).plug.equals(songItem.plug)) {
                            ((SongItem) MessageListItem.this.mSongItems.get(i)).play_state = 0;
                        }
                    }
                    songItem.play_state = 1;
                    MessageListItem.this.mHandler.sendMessage(MessageListItem.this.mHandler.obtainMessage(4));
                    return;
                }
                songItem.play_state = 0;
                MessageListItem.stopPlayMusic();
                MessageListItem.this.mHandler.sendMessage(MessageListItem.this.mHandler.obtainMessage(4));
            }
        });
    }

    public MessageItem getMessageItem() {
        return this.mMessageItem;
    }

    public void onClick(View view) {
        MessageItem messageItem = (MessageItem) view.getTag();
        switch (messageItem.mAttachmentType) {
            case 2:
            case 3:
            case 4:
                MessageUtils.viewMmsMessageAttachment(getContext(), messageItem.mMessageUri, messageItem.mSlideshow);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.mMsgListItem = findViewById(R.id.msg_list_item);
        this.mMessageDate = (TextView) findViewById(R.id.message_date);
        this.mBodyTextView = (TextView) findViewById(R.id.text_view);
        this.mRightStatusIndicator = (ImageView) findViewById(R.id.right_status_indicator);
        this.viewStub = (ViewStub) findViewById(R.id.text_view_stub);
        this.mVoteViewStub = (ViewStub) findViewById(R.id.vote_view_stub);
        this.mMmsLayoutViewParent = findViewById(R.id.mms_layout_view_parent);
        this.mSmsImageView = (ImageView) findViewById(R.id.sms_image);
        this.mSmsImageProgressView = findViewById(R.id.sms_image_progress);
        this.mSmsImageAttachmentView = findViewById(R.id.sms_image_attachment);
        this.windowManager = (WindowManager) this.context.getSystemService("window");
        this.inflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
    }

    public void onMessageListItemClick() {
        URLSpan[] urls = this.mBodyTextView.getUrls();
        if (urls.length != 0) {
            if (urls.length == 1) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(urls[0].getURL()));
                intent.putExtra("com.android.browser.application_id", getContext().getPackageName());
                intent.setFlags(524288);
                getContext().startActivity(intent);
                return;
            }
            final ArrayList<String> extractUris = MessageUtils.extractUris(urls);
            AnonymousClass10 r1 = new ArrayAdapter<String>(getContext(), 17367057, extractUris) {
                public View getView(int i, View view, ViewGroup viewGroup) {
                    View view2 = super.getView(i, view, viewGroup);
                    try {
                        String str = ((String) getItem(i)).toString();
                        TextView textView = (TextView) view2;
                        Drawable activityIcon = getContext().getPackageManager().getActivityIcon(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                        if (activityIcon != null) {
                            activityIcon.setBounds(0, 0, activityIcon.getIntrinsicHeight(), activityIcon.getIntrinsicHeight());
                            textView.setCompoundDrawablePadding(10);
                            textView.setCompoundDrawables(activityIcon, null, null, null);
                        }
                        if (str.startsWith("tel:")) {
                            str = PhoneNumberUtils.formatNumber(str.substring("tel:".length()));
                        }
                        textView.setText(str);
                    } catch (PackageManager.NameNotFoundException e) {
                    }
                    return view2;
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            AnonymousClass11 r3 = new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    if (i >= 0) {
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse((String) extractUris.get(i)));
                        intent.putExtra("com.android.browser.application_id", MessageListItem.this.getContext().getPackageName());
                        intent.setFlags(524288);
                        MessageListItem.this.getContext().startActivity(intent);
                    }
                }
            };
            builder.setTitle((int) R.string.select_link_title);
            builder.setCancelable(true);
            builder.setAdapter(r1, r3);
            builder.setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        }
    }

    public void pauseAudio() {
    }

    public void pauseVideo() {
    }

    public void playMusic(String str) {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(str);
            mMediaPlayer.prepare();
            mMediaPlayer.start();
            this.mProgressBar.setVisibility(8);
            this.mPlayBtn.setVisibility(0);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    public void quickSendSms(String str, String str2) {
        if (!str2.equals("") && str2.length() > 0) {
            MessagingNotification.updateNewMessageIndicator(getContext(), false);
            Util.sendSMS(str, str2, getContext());
            Util.logEvent(PrefsUtil.EVENT_SMS_POPUP_QUICK_REPLY, new NameValuePair[0]);
        }
    }

    public void reset() {
        if (this.mImageView != null) {
            this.mImageView.setVisibility(8);
        }
    }

    public void seekAudio(int i) {
    }

    public void seekVideo(int i) {
    }

    public void setAudio(Uri uri, String str, Map<String, ?> map) {
    }

    public void setImage(String str, Bitmap bitmap) {
        inflateMmsView();
        this.mImageView.setImageBitmap(bitmap == null ? BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_picture) : bitmap);
        this.mImageView.setVisibility(0);
    }

    public void setImageRegionFit(String str) {
    }

    public void setImageVisibility(boolean z) {
    }

    public void setMsgListItemHandler(Handler handler) {
        this.mHandler = handler;
    }

    public void setMultiRecipient(boolean z) {
        this.mMultiRecipient = z;
    }

    public void setText(String str, String str2) {
    }

    public void setTextVisibility(boolean z) {
    }

    public void setVideo(String str, Uri uri) {
        inflateMmsView();
        Bitmap createVideoThumbnail = VideoAttachmentView.createVideoThumbnail(getContext(), uri);
        if (createVideoThumbnail == null) {
            createVideoThumbnail = BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_video);
        }
        this.mImageView.setImageBitmap(createVideoThumbnail);
        this.mImageView.setVisibility(0);
    }

    public void setVideoVisibility(boolean z) {
    }

    public void setVisibility(boolean z) {
    }

    public void startAudio() {
    }

    public void startVideo() {
    }

    public void stopAudio() {
    }

    public void stopVideo() {
    }

    public void votePlayer(final VoteItem voteItem, String str, final MessageItem messageItem) {
        ((TextView) findViewById(R.id.vote_title)).setText(voteItem.title);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.vote_options);
        linearLayout.removeAllViews();
        for (int i = 0; i < voteItem.options.size(); i++) {
            Button button = new Button(getContext());
            final VoteOption voteOption = voteItem.options.get(i);
            if (messageItem.mContact.equals(this.context.getString(R.string.messagelist_sender_self)) || voteItem.answer_flag.equals("1")) {
                button.setBackgroundResource(R.drawable.voting_option_background_disable);
            } else {
                button.setBackgroundResource(R.drawable.btn_vote_option);
            }
            button.setText(voteOption.content);
            button.setTextColor((int) R.color.text_color_black);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(250, -2);
            layoutParams.setMargins(10, 10, 0, 0);
            button.setLayoutParams(layoutParams);
            linearLayout.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Matcher matcher;
                    if (!messageItem.mContact.equals(MessageListItem.this.context.getString(R.string.messagelist_sender_self)) && !voteItem.answer_flag.equals("1") && !MessageListItem.this.mHasVote && (matcher = Pattern.compile(MessageListItem.this.getContext().getString(R.string.shorten_url_vote_reg), 32).matcher(voteItem.plug)) != null && matcher.find()) {
                        MessageListItem.this.createAnswerDialog(voteItem, voteOption, messageItem.mAddress, matcher.group(1));
                    }
                }
            });
        }
        if (messageItem.mContact != null && !messageItem.mContact.equals(this.context.getString(R.string.messagelist_sender_self)) && this.mHasVote) {
            this.voteDetailBtn = new Button(getContext());
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.setMargins(0, 10, 0, 0);
            this.voteDetailBtn.setLayoutParams(layoutParams2);
            this.voteDetailBtn.setText(getContext().getString(R.string.check_vote_detail));
            this.voteDetailBtn.setGravity(1);
            linearLayout.setGravity(1);
            this.voteDetailBtn.setBackgroundResource(R.drawable.btn_button);
            this.voteDetailBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    new FetchVoteDetailTask(MessageListItem.this.getContext(), voteItem).execute(new Void[0]);
                    MessageListItem.this.voteDetailBtn.setClickable(false);
                    MessageListItem.this.voteDetailBtn.setEnabled(false);
                }
            });
            linearLayout.addView(this.voteDetailBtn);
        }
    }
}
