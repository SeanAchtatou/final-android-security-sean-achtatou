package com.android.mms.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.Map;
import vc.lx.sms2.R;

public class VideoAttachmentView extends LinearLayout implements SlideViewInterface {
    private static final String TAG = "VideoAttachmentView";
    private ImageView mThumbnailView;

    public VideoAttachmentView(Context context) {
        super(context);
    }

    public VideoAttachmentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public static Bitmap createVideoThumbnail(Context context, Uri uri) {
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_missing_thumbnail_video);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        this.mThumbnailView = (ImageView) findViewById(R.id.video_thumbnail);
    }

    public void pauseAudio() {
    }

    public void pauseVideo() {
    }

    public void reset() {
    }

    public void seekAudio(int i) {
    }

    public void seekVideo(int i) {
    }

    public void setAudio(Uri uri, String str, Map<String, ?> map) {
    }

    public void setImage(String str, Bitmap bitmap) {
    }

    public void setImageRegionFit(String str) {
    }

    public void setImageVisibility(boolean z) {
    }

    public void setText(String str, String str2) {
    }

    public void setTextVisibility(boolean z) {
    }

    public void setVideo(String str, Uri uri) {
        Bitmap createVideoThumbnail = createVideoThumbnail(getContext(), uri);
        if (createVideoThumbnail == null) {
            createVideoThumbnail = BitmapFactory.decodeResource(getResources(), R.drawable.ic_missing_thumbnail_video);
        }
        this.mThumbnailView.setImageBitmap(createVideoThumbnail);
    }

    public void setVideoVisibility(boolean z) {
    }

    public void setVisibility(boolean z) {
    }

    public void startAudio() {
    }

    public void startVideo() {
    }

    public void stopAudio() {
    }

    public void stopVideo() {
    }
}
