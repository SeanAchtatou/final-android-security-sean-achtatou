package com.android.mms.ui;

import android.content.Context;
import com.android.mms.MmsConfig;
import com.android.mms.ui.IconListAdapter;
import java.util.ArrayList;
import java.util.List;
import vc.lx.sms2.R;

public class AttachmentTypeSelectorAdapter extends IconListAdapter {
    public static final int ADD_IMAGE = 0;
    public static final int ADD_SLIDESHOW = 6;
    public static final int ADD_SOUND = 4;
    public static final int ADD_VIDEO = 2;
    public static final int MODE_WITHOUT_SLIDESHOW = 1;
    public static final int MODE_WITH_SLIDESHOW = 0;
    public static final int RECORD_SOUND = 5;
    public static final int RECORD_VIDEO = 3;
    public static final int TAKE_PICTURE = 1;

    public static class AttachmentListItem extends IconListAdapter.IconListItem {
        private int mCommand;

        public AttachmentListItem(String str, int i, int i2) {
            super(str, i);
            this.mCommand = i2;
        }

        public int getCommand() {
            return this.mCommand;
        }
    }

    public AttachmentTypeSelectorAdapter(Context context, int i) {
        super(context, getData(i, context));
    }

    protected static void addItem(List<IconListAdapter.IconListItem> list, String str, int i, int i2) {
        list.add(new AttachmentListItem(str, i, i2));
    }

    protected static List<IconListAdapter.IconListItem> getData(int i, Context context) {
        ArrayList arrayList = new ArrayList(7);
        addItem(arrayList, context.getString(R.string.attach_image), R.drawable.ic_launcher_gallery, 0);
        addItem(arrayList, context.getString(R.string.attach_take_photo), R.drawable.ic_launcher_camera, 1);
        addItem(arrayList, context.getString(R.string.attach_video), R.drawable.ic_launcher_video_player, 2);
        addItem(arrayList, context.getString(R.string.attach_record_video), R.drawable.ic_launcher_camera_record, 3);
        if (MmsConfig.getAllowAttachAudio()) {
            addItem(arrayList, context.getString(R.string.attach_sound), R.drawable.ic_launcher_musicplayer_2, 4);
        }
        addItem(arrayList, context.getString(R.string.attach_record_sound), R.drawable.ic_launcher_record_audio, 5);
        if (i == 0) {
            addItem(arrayList, context.getString(R.string.attach_slideshow), R.drawable.ic_launcher_slideshow_add_sms, 6);
        }
        return arrayList;
    }

    public int buttonToCommand(int i) {
        return ((AttachmentListItem) getItem(i)).getCommand();
    }
}
