package com.android.mms.ui;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import com.android.mms.model.ImageModel;
import com.android.provider.DrmStore;
import com.android.provider.Telephony;
import com.google.android.mms.pdu.PduPart;
import vc.lx.sms.db.SmsSqliteHelper;
import vc.lx.sms.util.SqliteWrapper;

public class UriImage {
    private static final boolean DEBUG = true;
    private static final boolean LOCAL_LOGV = true;
    private static final int NUMBER_OF_RESIZE_ATTEMPTS = 4;
    private static final String TAG = "Mms/image";
    private String mContentType;
    private final Context mContext;
    private int mHeight;
    private String mPath;
    private String mSrc;
    private final Uri mUri;
    private int mWidth;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public UriImage(Context context, Uri uri) {
        if (context == null || uri == null) {
            throw new IllegalArgumentException();
        }
        if (uri.getScheme().equals(SmsSqliteHelper.CONTENT)) {
            initFromContentUri(context, uri);
        } else if (uri.getScheme().equals("file")) {
            initFromFile(context, uri);
        }
        this.mSrc = this.mPath.substring(this.mPath.lastIndexOf(47) + 1);
        this.mSrc = this.mSrc.replace(' ', '_');
        this.mContext = context;
        this.mUri = uri;
        decodeBoundsInfo();
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0050 A[SYNTHETIC, Splitter:B:23:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void decodeBoundsInfo() {
        /*
            r7 = this;
            r2 = 0
            java.lang.String r5 = "IOException caught while closing stream"
            java.lang.String r4 = "Mms/image"
            android.content.Context r0 = r7.mContext     // Catch:{ FileNotFoundException -> 0x0034, all -> 0x004c }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0034, all -> 0x004c }
            android.net.Uri r1 = r7.mUri     // Catch:{ FileNotFoundException -> 0x0034, all -> 0x004c }
            java.io.InputStream r0 = r0.openInputStream(r1)     // Catch:{ FileNotFoundException -> 0x0034, all -> 0x004c }
            android.graphics.BitmapFactory$Options r1 = new android.graphics.BitmapFactory$Options     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            r2 = 1
            r1.inJustDecodeBounds = r2     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            r2 = 0
            android.graphics.BitmapFactory.decodeStream(r0, r2, r1)     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            int r2 = r1.outWidth     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            r7.mWidth = r2     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            int r1 = r1.outHeight     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            r7.mHeight = r1     // Catch:{ FileNotFoundException -> 0x0064, all -> 0x005d }
            if (r0 == 0) goto L_0x002a
            r0.close()     // Catch:{ IOException -> 0x002b }
        L_0x002a:
            return
        L_0x002b:
            r0 = move-exception
            java.lang.String r1 = "Mms/image"
            java.lang.String r1 = "IOException caught while closing stream"
            android.util.Log.e(r4, r5, r0)
            goto L_0x002a
        L_0x0034:
            r0 = move-exception
            r1 = r2
        L_0x0036:
            java.lang.String r2 = "Mms/image"
            java.lang.String r3 = "IOException caught while opening stream"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ IOException -> 0x0043 }
            goto L_0x002a
        L_0x0043:
            r0 = move-exception
            java.lang.String r1 = "Mms/image"
            java.lang.String r1 = "IOException caught while closing stream"
            android.util.Log.e(r4, r5, r0)
            goto L_0x002a
        L_0x004c:
            r0 = move-exception
            r1 = r2
        L_0x004e:
            if (r1 == 0) goto L_0x0053
            r1.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r0
        L_0x0054:
            r1 = move-exception
            java.lang.String r2 = "Mms/image"
            java.lang.String r2 = "IOException caught while closing stream"
            android.util.Log.e(r4, r5, r1)
            goto L_0x0053
        L_0x005d:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x004e
        L_0x0062:
            r0 = move-exception
            goto L_0x004e
        L_0x0064:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.ui.UriImage.decodeBoundsInfo():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x016c A[Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e, all -> 0x0231 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01c3 A[Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e, all -> 0x0231 }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0222 A[SYNTHETIC, Splitter:B:99:0x0222] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] getResizedImageData(int r18, int r19, int r20) {
        /*
            r17 = this;
            r0 = r17
            int r0 = r0.mWidth
            r2 = r0
            r0 = r17
            int r0 = r0.mHeight
            r3 = r0
            r4 = 1
        L_0x000b:
            int r5 = r2 / r4
            r0 = r5
            r1 = r18
            if (r0 > r1) goto L_0x0019
            int r5 = r3 / r4
            r0 = r5
            r1 = r19
            if (r0 <= r1) goto L_0x001c
        L_0x0019:
            int r4 = r4 * 2
            goto L_0x000b
        L_0x001c:
            java.lang.String r5 = "Mms:app"
            r6 = 2
            boolean r5 = android.util.Log.isLoggable(r5, r6)
            if (r5 == 0) goto L_0x0082
            java.lang.String r5 = "Mms/image"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "getResizedImageData: wlimit="
            java.lang.StringBuilder r6 = r6.append(r7)
            r0 = r6
            r1 = r18
            java.lang.StringBuilder r6 = r0.append(r1)
            java.lang.String r7 = ", hlimit="
            java.lang.StringBuilder r6 = r6.append(r7)
            r0 = r6
            r1 = r19
            java.lang.StringBuilder r6 = r0.append(r1)
            java.lang.String r7 = ", sizeLimit="
            java.lang.StringBuilder r6 = r6.append(r7)
            r0 = r6
            r1 = r20
            java.lang.StringBuilder r6 = r0.append(r1)
            java.lang.String r7 = ", mWidth="
            java.lang.StringBuilder r6 = r6.append(r7)
            r0 = r17
            int r0 = r0.mWidth
            r7 = r0
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = ", mHeight="
            java.lang.StringBuilder r6 = r6.append(r7)
            r0 = r17
            int r0 = r0.mHeight
            r7 = r0
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = ", initialScaleFactor="
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r4)
            java.lang.String r6 = r6.toString()
            android.util.Log.v(r5, r6)
        L_0x0082:
            r5 = 0
            r6 = 0
            r7 = 1
            r15 = r7
            r7 = r4
            r4 = r15
            r16 = r5
            r5 = r6
            r6 = r16
        L_0x008d:
            android.graphics.BitmapFactory$Options r8 = new android.graphics.BitmapFactory$Options     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r8.<init>()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r8.inSampleSize = r7     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r0 = r17
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r9 = r0
            android.content.ContentResolver r9 = r9.getContentResolver()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r0 = r17
            android.net.Uri r0 = r0.mUri     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r10 = r0
            java.io.InputStream r6 = r9.openInputStream(r10)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r9 = 80
            r10 = 0
            android.graphics.Bitmap r10 = android.graphics.BitmapFactory.decodeStream(r6, r10, r8)     // Catch:{ OutOfMemoryError -> 0x023e }
            if (r10 != 0) goto L_0x00c1
            r2 = 0
            if (r6 == 0) goto L_0x00b5
            r6.close()     // Catch:{ IOException -> 0x00b6 }
        L_0x00b5:
            return r2
        L_0x00b6:
            r3 = move-exception
            java.lang.String r4 = "Mms/image"
            java.lang.String r5 = r3.getMessage()
            android.util.Log.e(r4, r5, r3)
            goto L_0x00b5
        L_0x00c1:
            int r11 = r8.outWidth     // Catch:{ OutOfMemoryError -> 0x023e }
            r0 = r11
            r1 = r18
            if (r0 > r1) goto L_0x00cf
            int r8 = r8.outHeight     // Catch:{ OutOfMemoryError -> 0x023e }
            r0 = r8
            r1 = r19
            if (r0 <= r1) goto L_0x011b
        L_0x00cf:
            int r8 = r2 / r7
            int r11 = r3 / r7
            java.lang.String r12 = "Mms:app"
            r13 = 2
            boolean r12 = android.util.Log.isLoggable(r12, r13)     // Catch:{ OutOfMemoryError -> 0x023e }
            if (r12 == 0) goto L_0x00fe
            java.lang.String r12 = "Mms/image"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ OutOfMemoryError -> 0x023e }
            r13.<init>()     // Catch:{ OutOfMemoryError -> 0x023e }
            java.lang.String r14 = "getResizedImageData: retry scaling using Bitmap.createScaledBitmap: w="
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ OutOfMemoryError -> 0x023e }
            java.lang.StringBuilder r8 = r13.append(r8)     // Catch:{ OutOfMemoryError -> 0x023e }
            java.lang.String r13 = ", h="
            java.lang.StringBuilder r8 = r8.append(r13)     // Catch:{ OutOfMemoryError -> 0x023e }
            java.lang.StringBuilder r8 = r8.append(r11)     // Catch:{ OutOfMemoryError -> 0x023e }
            java.lang.String r8 = r8.toString()     // Catch:{ OutOfMemoryError -> 0x023e }
            android.util.Log.v(r12, r8)     // Catch:{ OutOfMemoryError -> 0x023e }
        L_0x00fe:
            int r8 = r2 / r7
            int r11 = r3 / r7
            r12 = 0
            android.graphics.Bitmap r8 = android.graphics.Bitmap.createScaledBitmap(r10, r8, r11, r12)     // Catch:{ OutOfMemoryError -> 0x023e }
            if (r8 != 0) goto L_0x011c
            r2 = 0
            if (r6 == 0) goto L_0x00b5
            r6.close()     // Catch:{ IOException -> 0x0110 }
            goto L_0x00b5
        L_0x0110:
            r3 = move-exception
            java.lang.String r4 = "Mms/image"
            java.lang.String r5 = r3.getMessage()
            android.util.Log.e(r4, r5, r3)
            goto L_0x00b5
        L_0x011b:
            r8 = r10
        L_0x011c:
            java.io.ByteArrayOutputStream r10 = new java.io.ByteArrayOutputStream     // Catch:{ OutOfMemoryError -> 0x023e }
            r10.<init>()     // Catch:{ OutOfMemoryError -> 0x023e }
            android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ OutOfMemoryError -> 0x01e5 }
            r8.compress(r5, r9, r10)     // Catch:{ OutOfMemoryError -> 0x01e5 }
            int r5 = r10.size()     // Catch:{ OutOfMemoryError -> 0x01e5 }
            r0 = r5
            r1 = r20
            if (r0 <= r1) goto L_0x0248
            int r11 = r9 * r20
            int r5 = r11 / r5
            r11 = 50
            if (r5 < r11) goto L_0x0248
            java.lang.String r9 = "Mms:app"
            r11 = 2
            boolean r9 = android.util.Log.isLoggable(r9, r11)     // Catch:{ OutOfMemoryError -> 0x0233 }
            if (r9 == 0) goto L_0x0158
            java.lang.String r9 = "Mms/image"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ OutOfMemoryError -> 0x0233 }
            r11.<init>()     // Catch:{ OutOfMemoryError -> 0x0233 }
            java.lang.String r12 = "getResizedImageData: compress(2) w/ quality="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ OutOfMemoryError -> 0x0233 }
            java.lang.StringBuilder r11 = r11.append(r5)     // Catch:{ OutOfMemoryError -> 0x0233 }
            java.lang.String r11 = r11.toString()     // Catch:{ OutOfMemoryError -> 0x0233 }
            android.util.Log.v(r9, r11)     // Catch:{ OutOfMemoryError -> 0x0233 }
        L_0x0158:
            java.io.ByteArrayOutputStream r9 = new java.io.ByteArrayOutputStream     // Catch:{ OutOfMemoryError -> 0x0233 }
            r9.<init>()     // Catch:{ OutOfMemoryError -> 0x0233 }
            android.graphics.Bitmap$CompressFormat r10 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ OutOfMemoryError -> 0x0239 }
            r8.compress(r10, r5, r9)     // Catch:{ OutOfMemoryError -> 0x0239 }
            r8 = r9
        L_0x0163:
            java.lang.String r9 = "Mms:app"
            r10 = 2
            boolean r9 = android.util.Log.isLoggable(r9, r10)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            if (r9 == 0) goto L_0x01bd
            java.lang.String r9 = "Mms/image"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r10.<init>()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.String r11 = "attempt="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.StringBuilder r10 = r10.append(r4)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.String r11 = " size="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            if (r8 != 0) goto L_0x01f5
            r11 = 0
        L_0x0186:
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.String r11 = " width="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            int r11 = r2 / r7
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.String r11 = " height="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            int r11 = r3 / r7
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.String r11 = " scaleFactor="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.String r11 = " quality="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.StringBuilder r5 = r10.append(r5)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            android.util.Log.v(r9, r5)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
        L_0x01bd:
            int r5 = r7 * 2
            int r4 = r4 + 1
            if (r8 == 0) goto L_0x01cc
            int r7 = r8.size()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r0 = r7
            r1 = r20
            if (r0 <= r1) goto L_0x01cf
        L_0x01cc:
            r7 = 4
            if (r4 < r7) goto L_0x0244
        L_0x01cf:
            if (r8 != 0) goto L_0x01fa
            r2 = 0
        L_0x01d2:
            if (r6 == 0) goto L_0x00b5
            r6.close()     // Catch:{ IOException -> 0x01d9 }
            goto L_0x00b5
        L_0x01d9:
            r3 = move-exception
            java.lang.String r4 = "Mms/image"
            java.lang.String r5 = r3.getMessage()
            android.util.Log.e(r4, r5, r3)
            goto L_0x00b5
        L_0x01e5:
            r5 = move-exception
            r8 = r9
            r9 = r10
        L_0x01e8:
            java.lang.String r10 = "Mms/image"
            java.lang.String r11 = r5.getMessage()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            android.util.Log.e(r10, r11, r5)     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            r5 = r8
            r8 = r9
            goto L_0x0163
        L_0x01f5:
            int r11 = r8.size()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            goto L_0x0186
        L_0x01fa:
            byte[] r2 = r8.toByteArray()     // Catch:{ FileNotFoundException -> 0x01ff, all -> 0x021e }
            goto L_0x01d2
        L_0x01ff:
            r2 = move-exception
            r3 = r6
            java.lang.String r4 = "Mms/image"
            java.lang.String r5 = r2.getMessage()     // Catch:{ all -> 0x0231 }
            android.util.Log.e(r4, r5, r2)     // Catch:{ all -> 0x0231 }
            r2 = 0
            if (r3 == 0) goto L_0x00b5
            r3.close()     // Catch:{ IOException -> 0x0212 }
            goto L_0x00b5
        L_0x0212:
            r3 = move-exception
            java.lang.String r4 = "Mms/image"
            java.lang.String r5 = r3.getMessage()
            android.util.Log.e(r4, r5, r3)
            goto L_0x00b5
        L_0x021e:
            r2 = move-exception
            r3 = r6
        L_0x0220:
            if (r3 == 0) goto L_0x0225
            r3.close()     // Catch:{ IOException -> 0x0226 }
        L_0x0225:
            throw r2
        L_0x0226:
            r3 = move-exception
            java.lang.String r4 = "Mms/image"
            java.lang.String r5 = r3.getMessage()
            android.util.Log.e(r4, r5, r3)
            goto L_0x0225
        L_0x0231:
            r2 = move-exception
            goto L_0x0220
        L_0x0233:
            r8 = move-exception
            r9 = r10
            r15 = r5
            r5 = r8
            r8 = r15
            goto L_0x01e8
        L_0x0239:
            r8 = move-exception
            r15 = r8
            r8 = r5
            r5 = r15
            goto L_0x01e8
        L_0x023e:
            r8 = move-exception
            r15 = r8
            r8 = r9
            r9 = r5
            r5 = r15
            goto L_0x01e8
        L_0x0244:
            r7 = r5
            r5 = r8
            goto L_0x008d
        L_0x0248:
            r5 = r9
            r8 = r10
            goto L_0x0163
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.mms.ui.UriImage.getResizedImageData(int, int, int):byte[]");
    }

    private void initFromContentUri(Context context, Uri uri) {
        String string;
        Cursor query = SqliteWrapper.query(context, context.getContentResolver(), uri, null, null, null, null);
        if (query == null) {
            throw new IllegalArgumentException("Query on " + uri + " returns null result.");
        }
        try {
            if (query.getCount() != 1 || !query.moveToFirst()) {
                throw new IllegalArgumentException("Query on " + uri + " returns 0 or multiple rows.");
            }
            if (ImageModel.isMmsUri(uri)) {
                string = query.getString(query.getColumnIndexOrThrow(Telephony.Mms.Part.FILENAME));
                if (TextUtils.isEmpty(string)) {
                    string = query.getString(query.getColumnIndexOrThrow("_data"));
                }
                this.mContentType = query.getString(query.getColumnIndexOrThrow(Telephony.Mms.Part.CONTENT_TYPE));
            } else {
                string = query.getString(query.getColumnIndexOrThrow("_data"));
                this.mContentType = query.getString(query.getColumnIndexOrThrow(DrmStore.Columns.MIME_TYPE));
            }
            this.mPath = string;
        } finally {
            query.close();
        }
    }

    private void initFromFile(Context context, Uri uri) {
        int lastIndexOf;
        this.mPath = uri.getPath();
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(this.mPath);
        if (TextUtils.isEmpty(fileExtensionFromUrl) && (lastIndexOf = this.mPath.lastIndexOf(46)) >= 0) {
            fileExtensionFromUrl = this.mPath.substring(lastIndexOf + 1);
        }
        this.mContentType = singleton.getMimeTypeFromExtension(fileExtensionFromUrl);
    }

    public String getContentType() {
        return this.mContentType;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public PduPart getResizedImageAsPart(int i, int i2, int i3) {
        PduPart pduPart = new PduPart();
        byte[] resizedImageData = getResizedImageData(i, i2, i3);
        if (resizedImageData == null) {
            Log.v(TAG, "Resize image failed.");
            return null;
        }
        pduPart.setData(resizedImageData);
        pduPart.setContentType(getContentType().getBytes());
        String src = getSrc();
        byte[] bytes = src.getBytes();
        pduPart.setContentLocation(bytes);
        pduPart.setFilename(bytes);
        pduPart.setContentId(src.substring(0, src.lastIndexOf(".")).getBytes());
        return pduPart;
    }

    public String getSrc() {
        return this.mSrc;
    }

    public int getWidth() {
        return this.mWidth;
    }
}
